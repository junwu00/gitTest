require([
    'jquery',
    'common:widget/lib/tools',
    'common:widget/lib/UiFramework'
],function($,t,UiFramework) {
    var layerPackage = UiFramework.layerPackage();

    layerPackage.ew_alert({
        title:errorMsg,
        callback:function(){
        	window.history.go(-1);
        }
    });

    setTimeout(function(){
    	window.history.go(-1);
    },2000);
});