require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'api');
fis.match('*', {
	release:'/api/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});