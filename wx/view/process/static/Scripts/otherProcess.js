define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/otherProcessTpl.html',
	'common:widget/lib/plugins/gdmap/lbs.amap',
	'common:widget/lib/GenericFramework',
	'process:static/Scripts/process_common'
],function($,t,UiFramework,tpl,otherProcessTpl,gdmap,GenericFramework,common) {
	var layerPackage = UiFramework.layerPackage(); 

	var otherProcess = {
		/** 请假固定字段（请假类型、开始时间、结束时间） */
		fix_rest_key : ['input1481340230000', 'input1481340238000', 'input1481340240000'],
		/** 请假隐藏字段（请假时长） */
		hide_rest_key : ['input1481527822000'],
		/** 补录固定字段（补录时间） */
		fix_makeup_key : ["input1481340091000"],
		/** 外出固定字段（开始时间、结束时间） */
		fix_legwork_key : ['','input1481340568000', 'input1481340570000'],
		/** 外出隐藏字段（外出时长） */
		hide_legwork_key : ['input1481527081000'],

		//获取请求类型接口
		fix_rest_type_url : "/wx/index.php?model=workshift&m=ajax&cmd=111",		
		//获取补录时间点接口
		fix_makeup_url : "/wx/index.php?model=index&m=app&cmd=107",
		//获取快捷时间段选择接口：
		other_time_url:"/wx/index.php?model=index&m=app&cmd=108",
		//判断有效时长接口：
		time_count_url:"/wx/index.php?model=index&m=app&cmd=106",

		init:function(params){
			//is_other_proc,target,data,listType
			var is_other_proc = params.is_other_proc,
				target = params.target,
				data = params.data,
				listType = params.listType,
				userId = params.userId,
				formsetinstId = params.formsetinstId,
				state = params.state;

			this.is_other_proc = is_other_proc;

			var pThis = this;
			//创建
			if(listType === undefined || listType === null){
				//var is_other_proc = data.is_other_proc;
				//3 补录
				if(is_other_proc == 3){
					this.get_fix_makeup(target,data,userId,function(result){
						var o1 = target.find(".js_inputs[input_key="+this.fix_makeup_key[0]+"]"),
							o2 = target.find(".js_inputs[data-key="+this.fix_makeup_key[0]+"]");
						var o = o1.length > 0 ? o1 : o2;
						var cacheData = o.data("cacheData");
						var	type = cacheData.type;
						var html = new Array();

						var rotaTime = parseInt(t.getUrlParam("rotaTime"));

						if(type == "select"){
							for (var i = 0; i < result.makeup.length; i++) {
								var d = result.makeup[i];
								html.push("<option value='"+t.dateFormat(d)+"'  "+(((!rotaTime && cacheData.val == d) || rotaTime == d) ? "selected": "")+">"+this.getTime(d)+"</option>");
							};

							if(result.makeup.length == 0){
								html.push("<option value=''></option>");
							}

							var selectObj = o.find("select[input_key="+this.fix_makeup_key[0]+"]");
							selectObj.html(html.join(""));

							if(result.makeup.length == 0){
								selectObj.after("<span class='c-9b' style='display:inline-block;margin-top:10px;'>你暂时没有需要补录的考勤时间喔</span>");
								selectObj.addClass('hide');
							}
						}
					});
					/*this.get_fix_makeup(target,data,null,function(result){
						var o = target.find(".js_inputs[data-key="+this.fix_makeup_key[0]+"]");
						var cacheData = o.data("cacheData");
						var	type = cacheData.type;
						var html = new Array();

						if(type == "select"){
							for (var i = 0; i < result.makeup.length; i++) {
								var d = result.makeup[i];
								html.push("<option value="+d+">"+this.getTime(d)+"</option>");
							};

							if(result.makeup.length == 0){
								html.push("<option value=''></option>");
							}

							o.find("select[input_key="+this.fix_makeup_key[0]+"]").html(html.join(""));
						}
					});*/
				}
				//4 请假 5 外出
				else if(is_other_proc == 4 || is_other_proc == 5){
					if(is_other_proc == 4){
						var o1 = target.find(".js_inputs[input_key="+this.fix_rest_key[0]+"]"),
							o2 = target.find(".js_inputs[data-key="+this.fix_rest_key[0]+"]");
						var o = o1.length > 0 ? o1 : o2;
						var cacheData = o.data("cacheData");

						this.change_fix_rest_type(cacheData.opts_nums,function(result){
							var rest_type = result.rest_type;
							var selectObj = o.find("select[input_key="+this.fix_rest_key[0]+"]");
							var html = new Array();
							var opts = new Array();
							
							for (var i = 0; i < rest_type.length; i++) {
								var typeName = rest_type[i].name;
								opts.push(typeName);
								html.push("<option value='"+typeName+"' data-minunit='"+rest_type[i].min_unit+"' "+(cacheData.val == typeName ? "selected": "")+">"+typeName+"</option>");
							};

							if(rest_type.length == 0){
								html.push("<option value=''></option>");
							}

							selectObj.html(html.join(''));

							if(rest_type.length == 0){
								selectObj.after("<span class='c-9b' style='display:inline-block;margin-top:10px;'>无法申请请假</span>");
								selectObj.addClass('hide');
							}

							if(opts.length > 0){
								cacheData.opts = opts.join(",");
							}

							this.bind_fix_rest_type(target,cacheData);

							this.bind_fix_rest_key(target);
							this.hide_hide_rest_key(target);
						});
					}else if(is_other_proc == 5){
						this.fix_rest_key = this.fix_legwork_key;
						this.hide_rest_key = this.hide_legwork_key;

						this.bind_fix_rest_key(target);
						this.hide_hide_rest_key(target);
					}
				}
			}
			//详情
			else{
				//var is_other_proc = (listType == 1 || listType == 2) ? data.formsetinst.is_other_proc : data.notify.is_other_proc;
				//var userId = data.workitem.creater_id;
				this.userId = userId;

				if(is_other_proc == 3){
					var o = target.find(".js_inputs[input_key="+this.fix_makeup_key[0]+"]");
					var d = o.data("cacheData");
					var dState = state;
					var showEditIcon = d.edit_input == 1 && (dState == 1 || dState == 2 || dState ==3);

					this.set_fix_makeup(o.find(".css_input_deal_div"),d,showEditIcon);
				}
				//4 请假 5 外出
				else if(is_other_proc == 4 || is_other_proc == 5){
					if(is_other_proc == 5){
						this.fix_rest_key = this.fix_legwork_key;
						this.hide_rest_key = this.hide_legwork_key;

						this.getLocationInfo(target,listType,formsetinstId);
					}
					
					this.hide_hide_rest_key(target);

					var t1 = new Date(target.find(".js_inputs[input_key="+this.fix_rest_key[1]+"]").data("cacheData").val.replace('T',' ').replace('-','/').replace('-','/')).getTime() / 1000;
					var t2 = new Date(target.find(".js_inputs[input_key="+this.fix_rest_key[2]+"]").data("cacheData").val.replace('T',' ').replace('-','/').replace('-','/')).getTime() / 1000;

					this.get_time_count(
						target.find(".js_inputs[input_key="+this.hide_rest_key[0]+"]"),
						t1,
						t2,
						userId,function(result){
							target.find("input[input_key="+pThis.hide_rest_key[0]+"]").val(result.hours);
							$("#timeCount").remove();

							var div = target.find(".js_inputs[input_key="+pThis.fix_rest_key[2]+"]");

							pThis.get_time_count_html(div,result);
						}
					);
				}
			}

		},
		edit_init:function(target,data){
			var is_other_proc = data.formsetinst.is_other_proc;
			var pThis = this;

			if(is_other_proc == 3){
				this.get_fix_makeup(target,data,data.workitem.creater_id,function(result){
					var o1 = target.find(".js_inputs[input_key="+this.fix_makeup_key[0]+"]"),
						o2 = target.find(".js_inputs[data-key="+this.fix_makeup_key[0]+"]");
					var o = o1.length > 0 ? o1 : o2;
					var cacheData = o.data("cacheData");
					var	type = cacheData.type;
					var html = new Array();

					if(type == "select"){
						for (var i = 0; i < result.makeup.length; i++) {
							var d = result.makeup[i];
							html.push("<option value="+d+"  "+(cacheData.val == d ? "selected": "")+">"+this.getTime(d)+"</option>");
						};

						if(result.makeup.length == 0){
							html.push("<option value=''></option>");
						}

						o.find("select[input_key="+this.fix_makeup_key[0]+"]").html(html.join(""));
					}
				});
			}
			//4 请假  5 外出
			else if(is_other_proc == 4 || is_other_proc == 5){
				if(is_other_proc == 5){
					this.fix_rest_key = this.fix_legwork_key;
					this.hide_rest_key = this.hide_legwork_key;
				}

				this.bind_fix_rest_key(target);
				this.hide_hide_rest_key(target);
			}
		},
		getInputData:function(data,inputkey){
			var inputs = data.inputs;

			for (var i in inputs) {
				if(i == inputkey){
					return inputs[i];
				}
			};

			return null;
		},
		getTime:function(timestamp,hideTime){
			var time = t.getNowDate(parseInt(timestamp)*1000,1);
			var nowTime = t.getNowDate(null,1);

			return (time.year != nowTime.year ? time.year + "年" : "") + time.month + "月"+time.days+"日"+" " 
			+ "(周"+t.getWeek(time.week)+") " 
			+(hideTime ? "" : time.hours+":"+time.minutes); 
		},
		change_fix_rest_type:function(ids,callback){
			var pThis = this;
			layerPackage.lock_screen({
				"msg":"获取请假类型中..."
			});
			var parmas = {
				url:pThis.fix_rest_type_url,
				data:{
					data:{
						ids :ids.split(",")
					}
				},
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					console.log(result);
					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result]);
					}

					layerPackage.unlock_screen();
				}
			};

			t.ajaxJson(parmas);
		},
		bind_fix_rest_type:function(target,cacheData){
			var pThis = this;
			var selectObj = target.find("select[input_key="+this.fix_rest_key[0]+"]");

			selectObj.on("change",function(){
				target.find("#otherFixRest").remove();
				target.find("#timeCount").remove();

				var s = $(this).find("option:selected");
				var min_unit = s.attr("data-minunit");
				var input_type = min_unit == 1 ? "datetime-local" : "date";
				var format = min_unit == 1 ? "datetime" : "date";

				//开始时间
				var time1_1 = target.find(".js_inputs[input_key="+pThis.fix_rest_key[1]+"]"),
				time1_2 = target.find(".js_inputs[data-key="+pThis.fix_rest_key[1]+"]");
				var time1 = time1_1.length > 0 ? time1_1 : time1_2;
				if(time1.length > 0){
					var time1_data = time1.data("cacheData");
					var input1 = time1.find('input[input_key="'+pThis.fix_rest_key[1]+'"]');
					
					//time1.find('input[input_key="'+pThis.fix_rest_key[1]+'"]').attr("type",input_type).val("");
					var val = input1.val().replace("T"," ");

					input1.attr("value","");
					input1.val("");
					input1.attr("type",input_type);
					input1.attr("format",format);
					

					if(min_unit == 1){
						if(val.indexOf(" ") <= 0){
							val += " 00:00";
						}
						val = t.getDateTimeLocal(val);
					}else{
						if(val.indexOf(" ") >= 0){
							val = val.substring(0,val.indexOf(" "));
						}
					}

					input1.attr("value",val);
					input1.val(val);

					if(val){
						input1.change();
					}

					time1.data("cacheData").format = format;
				}
				

				var time2_1 = target.find(".js_inputs[input_key="+pThis.fix_rest_key[2]+"]"),
				time2_2 = target.find(".js_inputs[data-key="+pThis.fix_rest_key[2]+"]");
				var time2 = time1_1.length > 0 ? time2_1 : time2_2;
				if(time2.length > 0){
					var time2_data = time2.data("cacheData");
					var input2 = time2.find('input[input_key="'+pThis.fix_rest_key[2]+'"]');
					
					//time2.find('input[input_key="'+pThis.fix_rest_key[2]+'"]').attr("type",input_type).val("");
				
					var val = input2.val().replace("T"," ");

					input1.attr("value","");
					input2.val("");
					input2.attr("type",input_type);
					input2.attr("format",format);
					

					if(min_unit == 1){
						if(val.indexOf(" ") <= 0){
							val += " 23:59";
						}
						val = t.getDateTimeLocal(val);
					}else{
						if(val.indexOf(" ") >= 0){
							val = val.substring(0,val.indexOf(" "));
						}
					}

					input2.attr("value",val);
					input2.val(val);

					if(val){
						input2.change();
					}

					time2.data("cacheData").format = format;
				}

				firstChange = false;
			});

			selectObj.change();
		},
		get_fix_makeup:function(target,data,userId,callback){
			var pThis = this;
			layerPackage.lock_screen({
				"msg":"获取补录时间中..."
			});
			var parmas = {
				url:pThis.fix_makeup_url,
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					console.log(result);
					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result]);
					}

					layerPackage.unlock_screen();
				}
			};

			if(userId){
				parmas.data = {
					"data":{
						user_id:userId
					}
				}
			}

			t.ajaxJson(parmas);
		},
		set_fix_makeup:function(target,data,showEditIcon){
			try{
				var val = data.val.replace('T',' ').replace('-','/').replace('-','/')
				target.html(this.getTime(new Date(val).getTime()/1000) 
					+ (showEditIcon ? '<i class="icon-edit fr c-green"></i>' : ''));
			}catch(e){
				console.log(e);
			}
		},
		hide_hide_rest_key:function(target){
			var div1 = target.find(".js_inputs[data-key="+this.hide_rest_key[0]+"]");

			if(div1.length > 0){
				div1.addClass('hide');
				return;
			}else{
				target.find(".js_inputs[input_key="+this.hide_rest_key[0]+"]").addClass('hide');
			}	
		},
		//时间事件
		bind_fix_rest_key:function(target){
			//console.log(target.find(".js_inputs[data-key="+this.fix_rest_key[1]+"] input[input_key="+this.fix_rest_key[0]+"]"));
			var input1 = target.find("input[input_key="+this.fix_rest_key[1]+"]");

			var input2 = target.find("input[input_key="+this.fix_rest_key[2]+"]");
		
			var pThis = this;

			function event1(e){
				var time1 = $(this).val();
				//console.log(e.type,time1);

				if(!$.trim(time1)){
					//return;
				}

				if(new Date(input1.val()).getTime() > new Date(input2.val()).getTime()){
					target.find("input[input_key="+pThis.hide_rest_key[0]+"]").val("0");
					$("#timeCount").remove();
				}

				var select = target.find("select[input_key="+pThis.fix_rest_key[0]+"]");
				//如果是日期类型不显示
				if(select.find("option:selected").attr("data-minunit") == "1" || pThis.is_other_proc == 5){
					pThis.get_other_time_data(target,time1,function(){
						if($.trim(input2.val())){
							input2.change();
						}
					});
				}else{
					if($.trim(input2.val())){
						input2.change();
					}
				}
			}

			input1.on("change",event1);
			input1.on("keyup",event1);

			function event2(e){
				target.find("input[input_key="+pThis.hide_rest_key[0]+"]").val("0");
				if(!$.trim(input1.val())){
					//layerPackage.fail_screen("请补全开始时间!");
					return;
				}

				if(!$.trim(input2.val())){
					//layerPackage.fail_screen("请补全结束时间!");
					return;
				}

				var otherFixRest = target.find("#otherFixRest");
				var div = target.find(".js_inputs[data-key="+pThis.hide_rest_key[0]+"]");
				var o = otherFixRest.length > 0 ? otherFixRest : div;

				var val1 = input1.val();
				val1 = val1.replace('T',' ').replace('-','/').replace('-','/');

				var val2 = input2.val();
				val2 = val2.replace('T',' ').replace('-','/').replace('-','/');

				var t1 = new Date(val1).getTime() / 1000,
					t2 = new Date(val2).getTime() / 1000;

				pThis.get_time_count(
					o,
					t1,
					t2,
					null,
					function(result){
						target.find("input[input_key="+pThis.hide_rest_key[0]+"]").val(result.hours);
						$("#timeCount").remove();
						var otherFixRest = target.find("#otherFixRest");
						var div = target.find(".js_inputs[data-key="+pThis.fix_rest_key[2]+"]");
						var o = otherFixRest.length > 0 ? otherFixRest : div;

						pThis.get_time_count_html(o,result);
					}
				);
			}

			input2.on("change",event2);
			input2.on("keyup",event2);

			if(input1.val()){
				input1.change();
			}

			if(input1.val() && input2.val()){
				input2.change();
			}

			if(!input1.val() && !input2.val()){
				input1.change();
			}
		},
		get_other_time_data:function(target,startTime,callback){
			var pThis = this;

			startTime = startTime ? new Date(startTime).getTime() / 1000 : undefined;

		    if(pThis.abort1 && pThis.abort1.readystate!=4){
				pThis.abort1.abort();
				pThis.abort1 = null;
				//return;
			}

			var parmas = {
				url:pThis.other_time_url,
				data:{
					data:{
						"start_time":startTime
					}
				},
				type:"post",
				dataType:"json"
			};

			pThis.abort1 = $.ajax(parmas).done(function (result, status, e) {
		        if(result.errcode !="0"){
					layerPackage.unlock_screen();//遮罩end
					layerPackage.fail_screen(result.errmsg);
					return;
				}

				var time  = result.time;
				var initTime = result.init;

				target.find("#otherFixRest").remove();

				if(time.length > 0){
					var dom = $($.trim(pThis.get_other_time_html(time,startTime)));

					var o = target.find(".js_inputs[data-key="+pThis.fix_rest_key[2]+"]");
					o.after(dom);
					pThis.bindOtherFixRestDom(target);
				}

				if(callback && t.isFunction(callback)){
					callback();
				}

				pThis.abort1 = null;
		    }).fail(function (msg, status, e) {
		        
		    });

		    return;

			var parmas = {
				url:pThis.other_time_url,
				data:{
					data:{
						start_time:startTime
					}
				},
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					var time  = result.time;

					target.find("#otherFixRest").remove();

					if(time.length > 0){
						var dom = $($.trim(pThis.get_other_time_html(time,startTime)));

						var o = target.find(".js_inputs[data-key="+pThis.hide_rest_key[0]+"]");
						o.after(dom);
						pThis.bindOtherFixRestDom(target);
					}

					if(callback && t.isFunction(callback)){
						callback();
					}
				}
			};

			t.ajaxJson(parmas);
		},
		get_other_time_html:function(data,startTime){
			var html = new Array();

			var newTime = t.getNowDate(startTime*1000,1);

			html.push('<section id="otherFixRest" class="autoH">');
			html.push('<div class="bg-gray">');
			html.push('<p class="c-4a"><i class="rulerIcon rulericon-magic_wand mr6"></i>快速选择时间（'+newTime.month + '月'+newTime.days+'日 周'+t.getWeek(newTime.week)+'）</p>');
			html.push('<div class="mt15 fs12 lhn">');
			for (var i = 0; i < data.length; i++) {
				html.push('<span class="bg-white c-4a" data-starttime="'+data[i].start_time+'" data-endtime="'+data[i].end_time+'">');
				if(data[i].date){
					html.push(data[i].date+" (全天)");
				}else{
					html.push(this.getTime2(data[i].start_time,startTime)+'~'+this.getTime2(data[i].end_time,startTime));
				}
				html.push('</span>');
			};
			html.push('</div>');
			html.push('</div>');
			html.push('</section>');

			return html.join('');
		},
		getTime2:function(timestamp,secondTimestamp){
			var time = t.getNowDate(parseInt(timestamp)*1000,1);
			var nowTime = t.getNowDate(secondTimestamp ? parseInt(secondTimestamp)*1000 : secondTimestamp,1);

			return (time.year != nowTime.year ? time.year + "年" : "") 
			+ (time.month != nowTime.month ? time.month + "月" : "")  
			+ (time.days != nowTime.days ? time.days + "日" : "")  
			+ time.hours+":"+time.minutes; 
		},
		//快捷时间事件
		bindOtherFixRestDom:function(target){
			var pThis = this;
			target.find("#otherFixRest span").on("click",function(){
				var startTime = $(this).attr("data-starttime"),
					endTime = $(this).attr("data-endtime");

				target
				.find(".js_inputs[data-key="+pThis.fix_rest_key[1]+"] input[input_key="+pThis.fix_rest_key[1]+"]")
				.val(t.getDateTimeLocal(t.dateFormat(startTime)));

				target
				.find(".js_inputs[data-key="+pThis.fix_rest_key[2]+"] input[input_key="+pThis.fix_rest_key[2]+"]")
				.val(t.getDateTimeLocal(t.dateFormat(endTime)));

				pThis.get_time_count(target,startTime,endTime,pThis.userId,function(result){
					target.find("input[input_key="+pThis.hide_rest_key[0]+"]").val(result.hours);

					$("#timeCount").remove();
					var otherFixRest = target.find("#otherFixRest");
					var div = target.find(".js_inputs[data-key="+pThis.fix_rest_key[2]+"]");
					var o = otherFixRest.length > 0 ? otherFixRest : div;

					pThis.get_time_count_html(o,result);
				});
			});
		},
		//获取补录总时间
		get_time_count:function(target,startTime,endTime,userId,callback){
			if((this.is_other_proc == 5 && new Date(startTime).getTime() >= new Date(endTime).getTime()) || 
				(this.is_other_proc == 4 && new Date(startTime).getTime() > new Date(endTime).getTime())){
				//layerPackage.fail_screen("结束时间不能比开始时间小!");
				$("#timeCount").remove();
				$("input[input_key="+this.hide_rest_key[0]+"]").val("0");
				return;
			}

			var option = $("select[input_key="+this.fix_rest_key[0]+"] option:selected");
			
			if(this.is_other_proc == 4 && !userId && option.length > 0 && option.attr("data-minunit") == 0){
				endTime += 60*60*24 - 1;
			}

			var pThis = this;
			/*layerPackage.lock_screen({
				"msg":"获取补录时间中..."
			});*/
	    	if(pThis.abort2 && pThis.abort2.readystate!=4){
				pThis.abort2.abort();
				pThis.abort2 = null;
				//return;
			}

			var parmas = {
				url:pThis.time_count_url,
				data:{
					data:{
						"start_time":startTime,
						"end_time":endTime
					}
				},
				type:"post",
				dataType:"json"
			};

			if(userId){
				parmas.data.data.user_id = userId;
			}

			if(this.is_other_proc == 4 && !userId && option.length > 0){
				parmas.data.data.rest_type = option.val();
			}else if(this.is_other_proc ==4 && userId){
				var selectVal = $.trim($("div[input_key="+this.fix_rest_key[0]+"]").find(".js_edit_div").text());
				if(selectVal){
					parmas.data.data.rest_type = selectVal;
				}
			}

			pThis.abort2 = $.ajax(parmas).done(function (result, status, e) {
		        if(result.errcode !="0"){
					layerPackage.unlock_screen();//遮罩end
					//layerPackage.fail_screen(result.errmsg);
					$("#timeCount").remove();
					return;
				}

				console.log(result);

				if(callback && t.isFunction(callback)){
					callback.apply(pThis,[result]);
				}

				pThis.abort2 = null;

				layerPackage.unlock_screen();
		    }).fail(function (msg, status, e) {
		        
		    });

		    return;
			var parmas = {
				url:pThis.time_count_url,
				data:{
					data:{
						"start_time":startTime,
						"end_time":endTime
					}
				},
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					console.log(result);

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result]);
					}

					layerPackage.unlock_screen();
				}
			};

			if(userId){
				parmas.data.data.user_id = userId;
			}

			t.ajaxJson(parmas);
		},
		get_time_count_html:function(target,result){
			var html = new Array();

			html.push('<section class="autoH bg-green2 fs12 c-4a" id="timeCount">');
			if(result.hasOwnProperty("year_balance")){
				if(result.year_balance >= 0){
					html.push('有效时长为<i class="c-red fsn">'+result.hours+'</i>小时，约<i class="c-red fsn">'+result.day+'天</i>。申请后，年假剩余<i class="c-red fsn">'+result.year_balance+'</i>天');
				}else{
					html.push('剩余年假天数不足，当前剩余 <i class="c-red fsn">'+result.all_year+'</i> 天');
				}
			}else{
				if(result.hours == 0){
					html.push("有效时间为0小时，请重新选择");
				}else{
					html.push('申请有效时长是<i class="c-red fsn">'+result.hours+'</i>小时，约<i class="c-red fsn">'+result.day+'</i>天');
				}
			}
			
			html.push('</section>');

			var dom = $($.trim(html.join('')));

			target.after(dom);
		},
		//------定位信息-------------
		getRotaTime:function(time,showSecond){
			time = this.checkTime(time);

			var nowTime = t.getNowDate(time,1);

			return nowTime.hours + ":" + nowTime.minutes + (showSecond ? ":" + nowTime.seconds : "");
		},
		getDiffTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;
			var json = t.getDiffTime(firstTime,secondTime,1);

			return (json.days ? json.days + "天" : "") + (json.hours ? json.hours + "小时" : "") 
			+(json.minutes ? json.minutes + "分钟" : "")+(json.seconds ? json.seconds + "秒" : "");
		},
		getLocationTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;
			var json = t.getDiffTime(firstTime,secondTime,1);

			var str = "";

			var flag = false;
			var m = this.checkTime(firstTime || secondTime);
			var time = t.getNowDate(m,1);

			if(parseInt(time.month) != parseInt(t.getNowDate(null,1).month)){
				str += parseInt(time.month) + "月" + (time.days ?  time.days + "日" : "");
				flag = true;
			}

			if(json.days && !flag){
				if(json.days == 1 && firstTime && !secondTime){
					str += "昨天";
				}else if(json.days == 1 && !firstTime && secondTime){
					str += "明天";
				}
			}else{
				str += time.days+"日";
			}

			return str + " " + time.hours + ":" + time.minutes;
		},
		checkTime:function(time){
			if(time.toString().length == 10){
				time = parseInt(time) * 1000;
			}

			return time;
		},
		getLocationInfo:function(target,listType,formsetinstId){
			var id = t.getUrlParam("id");
			var pThis = this;

			if(!id){return;}

			this.getSignInInfo(formsetinstId,function(result){
				if(listType == 2){
					//this.initMap(target);
				}

				tpl.register('getRotaTime', this.getRotaTime);
				tpl.register('getDiffTime', this.getDiffTime);
				tpl.register('getShortTime', t.getShortTime);
				tpl.register('getLocationTime', this.getLocationTime);
				var dom = tpl(otherProcessTpl,{
					getLocation:1,
					data:result,
					hideBtn:listType != 2 ? true : false
				});
				tpl.unregister('getRotaTime');
				tpl.unregister('getDiffTime');
				tpl.unregister('getShortTime');
				tpl.unregister('getLocationTime');
				
				target.find(".tabList").before($($.trim(dom)));

				GenericFramework.init(wxJsdkConfig);

				if(result.clock_list.length > 0){
					this.getClockList(target,result.clock_list);	
				}

				this.bindLocationDom(target);

				result.time = this.checkTime(result.time || new Date().getTime());
				var i = 0;
				setInterval(function(){
					i++;
					target.find(".clickTime").text(pThis.getRotaTime((result.time + i*1000),1));
				},1000);
			});
		},
		getSignInInfo:function(id,callback){
			var pThis = this;
			var url = "/wx/index.php?model=workshift&m=ajax&cmd=103";

			t.ajaxJson({
				url:url,
				data:{
					data:{
						"formsetinst_id":id
					}
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						//layerPackage.fail_screen(result.errmsg);
						return;
					}

					pThis.data = result;

					callback.apply(pThis,[result]);

					layerPackage.unlock_screen();
				}
			});
		},
		initMap:function(target){
			var point = null;
			var pThis = this;
			require(['amap'], function() {
				window.init = function() {
					gdmap.init(target.find("section.fullPic#gdmap"), {
						zoom:16, //PC [3-18] 移动 [3-19]
						//center: [lng, lat], //测试用  113.320703, 23.165386

						autoMarkCenter: 0, //默认定位中心点
						autoFixedCenter: 0, //默认缩放地图时保持中心点不变

						searchEnable:0,

						callback: {
							onAfterSetCenter:function(position,map){
								console.log(position);
								point = position;
								pThis.position = position;
								var marker = null;
								var addr = "&nbsp;";

								pThis.map = map;

								map.getInfoByPosition(position,function(result){
									var address =  result.regeocode.formattedAddress;

									var time = t.getNowDate();

									marker = this.addMarker({
										position:[position.lng, position.lat],
									},address,map.getMarkerLabel(userInfo.pic,address,time),{x:16,y:19});

									//g.setFitView();
									pThis.marker = marker;
								});
							},
							onAfterMapLoaded: function(map) {
								console.log("地图加载完成");
							}
						}
					});
				}
			});
		},
		getClockList:function(target,data,append){
			tpl.register('getRotaTime', this.getRotaTime);
			tpl.register('getDiffTime', this.getDiffTime);
			tpl.register('getShortTime', t.getShortTime);
			var dom = $($.trim(tpl(otherProcessTpl,{
				getClockList:1,
				data:data,
				append:append
			})));

			GenericFramework.previewImage({
				target:dom.find(".img_div")
			});
			
			tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');
			tpl.unregister('getShortTime');

			if(!append){
				target.find(".clockList").append(dom);
			}else{
				target.find(".clockList").prepend(dom);
			}
			
		},
		bindLocationDom:function(target){
			var pThis = this;

			//定位按钮
			target.find(".locationBtn").on("click",function(){
				pThis.locationContent(target);
			});

			target.on("click",".locationContent",function(){
				$(this).remove();
			});

			/*target.on("click",".clockList > section",function(){
				var o = $(this);
				var lng = o.attr("data-lng");
				var lat = o.attr("data-lat");
				
				if(!lng || !lat){
					layerPackage.fail_screen("获取地址错误");
					return;
				}
				if(!pThis.map || !pThis.marker){
					layerPackage.fail_screen("地图加载完未成！");
					return;
				}

				var newPosition = [lng,lat];
				
				pThis.marker.setPosition(newPosition);
				pThis.map.panTo(newPosition);

				target.find(".amap-marker-label .mTitle").text(o.find(".lPart_div > span").text());
			});*/

			GenericFramework.previewImage({
				target:target.find(".img_div")
			});
		},
		locationContent:function(target){
			var dom = tpl(otherProcessTpl,{
				locationContent:1
			});

			var o = target.find(".locationContent");

			if(o.length > 0){
				o.remove();
			}

			target.append($($.trim(dom)));

			this.locationContentEvent(target);

			setTimeout(function(){
				target.find(".locationContent .content .address").click();
			},0);
		},
		localInfo:{
			address:null,
			longt:null,
			lat:null,
			image_list:[]
		},
		locationContentEvent:function(target){
			var pThis = this;
			target.find(".locationContent .content").on("click",function(e){
				e.preventDefault();
				e.stopPropagation();
			});

			target.find(".locationContent .content .addComment").on("click",function(e){
				$(this).addClass('hide');
				$(this).next().removeClass('hide');
			});

			//刷新获取地理位置
			var freshBtn = target.find(".locationContent .content .address");
			GenericFramework.location({
				target:freshBtn,
				callback:{
					beforeGetLocation:function(){
						freshBtn.find(".add").addClass('hide');
						freshBtn.find(".search-address").removeClass('hide');
					},
					afterGetLocation:function(res,lng,lat){
						var address = res.address;
						pThis.localInfo.address = address;
						pThis.localInfo.longt = lng;
						pThis.localInfo.lat = lat;

						freshBtn.find(".add").removeClass('hide');
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span.add").text(address);
					},
					error:function(msg){
						alert(msg);
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span").text("请重新点击获取");
					}
				}
			});

			var cameraBtn = target.find(".locationContent .content .cameraBtn");
			var picObj = target.find(".camera-pic");
			GenericFramework.uploadFile({
				target:cameraBtn,
				type:"wx",
				sourceType:2, //拍照
				callback:{
					afterUpload:function(imgInfo){
						//console.log(imgInfo);
						picObj.removeClass('hide');
						picObj.find("img").attr("src",imgInfo.image_path);

						//alert(JSON.stringify(imgInfo));

						pThis.localInfo.image_list.push({
							hash:imgInfo.hash,
							size:imgInfo.filesize,
							url:imgInfo.path,
							name:imgInfo.name,
							ext:imgInfo.ext,
							image_path:imgInfo.image_path
						});

						cameraBtn.addClass('hide');
					}
				},
				wx:{
					uploadUrl:"/wx/index.php?model=workshift&m=ajax&a=upload",
					uploadData:{
						data:{
							addr:pThis.localInfo.address
						}
					},
					beforeStartCallback:function(_setting){
						if(!pThis.localInfo.address){
							layerPackage.fail_screen("请获取地址后再拍照!");
							return false;
						}

						_setting.wx.uploadData = {
							data:{
								addr:pThis.localInfo.address
							}
						}

						return true;
					}
				}
			});
	
			GenericFramework.previewImage({
				target:picObj
			});
			
			picObj.on('click','i',function(event){
                event.stopPropagation();
                
                pThis.localInfo.image_list.pop();

                picObj.find("img").attr("src","");

                cameraBtn.removeClass('hide');
                picObj.addClass('hide');
            });

            target.find(".locationContent .content .submitBtn button").on("click",function(e){
            	var data = pThis.data;
            	var localInfo = pThis.localInfo;

            	if(!localInfo.address){
            		layerPackage.fail_screen("请重新获取地址!");
            		return;
            	}

            	if(localInfo.image_list.length == 0 && data.is_photo == 1){
            		layerPackage.fail_screen("请拍照!");
            		return;
            	}

            	var textarea = target.find(".locationContent .content textarea");

            	pThis.setLocationApi({
            		"id":data.legwork_record.record_id,
            		"addr":localInfo.address,
            		"lng":localInfo.longt,
            		"lat":localInfo.lat,
					"file_list":localInfo.image_list,
					"mark":textarea.val()
            	},function(result){
					pThis.getClockList(target, [{
						"addr_name": localInfo.address || "",
						"clock_time": pThis.checkTime(result.time || new Date().getTime()),
						"mark": textarea.val() || "",
						"file_path": localInfo.image_list.length == 0 ? [] : [localInfo.image_list[0].image_path]
					}],1);

            		pThis.localInfo.address =null;
					pThis.localInfo.longt =null;
					pThis.localInfo.lat =null;
					pThis.localInfo.image_list = [];
					textarea.val("");

					target.find(".locationContent").click();
            	});
			});
		},
		setLocationApi:function(data,callback){
			var pThis = this;

			t.ajaxJson({
				url:"/wx/index.php?model=workshift&m=ajax&cmd=104",
				data:{
					data:data
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					if(callback && t.isFunction(callback)){
						callback(result);
					}
				}
			});
		},
		getSaveOrEditUrl:function(is_other_proc,url){
			if(is_other_proc == 0 || is_other_proc == 1 || is_other_proc == 2){
				return url;
			}else if(is_other_proc == 3 || is_other_proc == 4 || is_other_proc == 5){
				return "/wx/index.php?model=workshift&m=ajax&cmd=109";
			}

			return url;
		},
		checkParams:function(is_other_proc){
			if(is_other_proc == 4 || is_other_proc == 5){
				var time1 = $("input[input_key="+this.fix_rest_key[1]+"]").val() || $("div[input_key="+this.fix_rest_key[1]+"]").find(".js_edit_div").text();
				var time2 = $("input[input_key="+this.fix_rest_key[2]+"]").val() || $("div[input_key="+this.fix_rest_key[2]+"]").find(".js_edit_div").text();
				var hideObj = $("input[input_key="+this.hide_rest_key[0]+"]");

				if((is_other_proc == 5 && new Date(time1).getTime() >= new Date(time2).getTime()) || (is_other_proc == 4 && new Date(time1).getTime() > new Date(time2).getTime())){
					layerPackage.unlock_screen();
					layerPackage.fail_screen("开始时间比结束时间大！");
					return;
				}else if(hideObj.val() == "" || hideObj.val() == "0"){
					layerPackage.unlock_screen();
					layerPackage.fail_screen("该时间段没有班次");
					return;
				}
			}

			return true;
		}
	};

	return otherProcess;
});