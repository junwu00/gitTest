define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/scFormCtrl/scFormCtrl-v2.0'
],function($,tools,UiFramework,ScFormCtrl) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//退回节点信息
	var trunbackUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=124",
	backSelectUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=122";

	var trunback = {
		init:function(workitem_id,_setting,detailObj,common){
			this.workitem_id = workitem_id;
			this._setting = _setting;
			this.detailObj = detailObj;
			this.common = common;

			this.getTrunbackInfo(workitem_id,_setting);
		},
		//退回相关事件
		bindTrunbackDom:function(){
			var pThis = this;
			$(window).on('hashchange',function(){
				var hash = window.location.hash;
				var trunbackPageObj = $("body").find("#trunbackPage");

				if(hash != "#trunbackPage"){
					trunbackPageObj.addClass('hide');
				}else{
					trunbackPageObj.removeClass('hide');
				}
			});
		},
		//获取退回节点模板
		getTrunbackNode:function(data){
			var html = new Array();

			html.push('<div class="trunbackNode mt15">');
			html.push('<div class="trunbackNodeTitle fs17 lhn c-9b bg-gray sp">选择退回人员</div>');
			for (var i = 0; i < data.length; i++) {
				var d = data[i];
				html.push('<div class="trunbackNodePart bg-white current">');
				

				var list = d.back_item_list;
				for (var j = 0; j < list.length; j++) {
					var l_d = list[j];
					var time = tools.getNowDate(l_d.complete_time*1000,1);
					var timeStr = time.year+"年"+time.month+"月"+time.days+"日 "+time.hours+":"+time.minutes;

					html.push('<div class="trunbackNodeInfo " data-id="'+l_d.workitem_id+'">');
					html.push('<div class="trunbackUser" data-id="'+l_d.handler_id+'" data-name="'+l_d.hander_name+'">');
					html.push('<div class="fl circle-checkbox mr6"></div>');
					html.push('<img src="'+l_d.hander_pic_url+'" alt="" class="mr6 fl" onerror="this.src='+defaultFace+'"/>');
					html.push('<span class="selectName hideTxt fs17">'+l_d.hander_name+'</span>');

					html.push('<span class="fr c-9b fs14 lhn pr15 smallSelectName hideTxt" style="margin-right:16px;">');
					if(i == 0){
						html.push('<i class="rulericon-self" style="margin-right:2px;"></i>');
						html.push('申请人');
					}else{
						html.push(d.workitem_name);
					}
					html.push('</span>');

					html.push('</div>');
					html.push('<p class="fs14 lhn c-9b tal trunback_p">'+(i == 0 ? "申请时间" : "审批时间")+' : '+timeStr+'</p>');
					html.push('</div>');
					html.push('<div class="wxLine '+(i==0?"mb15":"ml20")+'"></div>');
				};
				html.push('</div>');
			};
			html.push('</div>');

			return html.join('');
		},
		//获取退回节点信息
		getTrunbackInfo:function(workitem_id,_setting){
			window.location.hash = "trunbackPage";

			var pThis = this;
			var trunbackPageObj = $("body").find("#trunbackPage");

			if(trunbackPageObj.length == 0){
				var html = new Array();

				html.push('<div id="trunbackPage" class="w_100 h_100 bg-gray">');
				html.push('<div class="trunbackPageContent">');
				html.push('</div>');
				html.push('</div>');

				$("body").append(html.join(''));

				this.bindTrunbackDom();
			}else{
				//this.clearHistory();
				trunbackPageObj.removeClass('hide');
				layerPackage.unlock_screen();
				return;
			}

			tools.ajaxJson({
				url:trunbackUrl,
				data: {
					"data": {
						"workitem_id":workitem_id
					}
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						//layerPackage.fail_screen(result.errmsg);
						layerPackage.ew_alert({
							title:result.errmsg,
							action_desc:'ok,我知道了',
							top:40
						});
						return;
					}

					var data = result.info;
					console.log(data);
					pThis.data = data;

					var trunbackPageObj = $("body").find("#trunbackPage");
					var trunbackContent = trunbackPageObj.find(".trunbackPageContent");

					//添加意见输入框
					var must = 1,placeholder = "请输入退回意见……",val = "不同意",errmsg = "请填写审批意见";

					trunbackContent.append(ScFormCtrl(pThis.common.getJudgementData(1,"审批意见",val,placeholder,errmsg),{check:1}));

					//添加节点信息
					trunbackContent.append($($.trim(pThis.getTrunbackNode(data))));

					/*trunbackContent.find(".trunbackNode").css({
						"height":"calc(100% - "+trunbackContent.find(".scCtrl").innerHeight()+"px)",
						"height":"-webkit-calc(100% - "+trunbackContent.find(".scCtrl").innerHeight()+"px - 15px)",
					});*/

					//绑定按钮事件
					pThis.bindTrunbackBtn(trunbackPageObj);

					//添加退回底部信息
					pThis.addTrunbackBottom(trunbackPageObj,_setting);

					layerPackage.unlock_screen();
				}
			});
		},
		radioState:false,
		//退回按钮事件
		bindTrunbackBtn:function(trunbackPageObj){
			var pThis = this;
			trunbackPageObj.find('.circle-checkbox').each(function(index){
				var o = $(this),pObj = o.parents(".trunbackUser"),
				id = pObj.attr("data-id"),
				name = pObj.attr("data-name"),
				workitem_id = pObj.parent(".trunbackNodeInfo").attr("data-id");
				pObj.data("cacheData",{
					id:id,name:name,status:false,workitem_id:workitem_id
				});

				UiFramework.btnPackage().checkBoxBtn.init({
					target: o,
					status: false,
					type: 1,
					callback: {
						on: function(obj, self) {
							//清除操作数据
							pThis.clearTrunbackHistory(trunbackPageObj,index);
							pObj.data("cacheData").status = true;
							pThis.radioState = true;
						},
						off: function(obj, self) {
							pObj.data("cacheData").status = false;
							pThis.radioState = false;
						}
					}
				});
			});

			trunbackPageObj.on("click",".trunbackNodeTitle",function(){
				var o = $(this);
				var p = o.parents(".trunbackNodePart");

				if(p.index() == 1){return;}

				if(p.hasClass('current')){
					p.removeClass('current');
				}else{
					p.addClass('current');
					p.siblings(".trunbackNodePart:not(:first)").removeClass('current');
				}
			});
		},
		//添加退回底部信息
		addTrunbackBottom:function(trunbackPageObj,_setting){
			var pThis = this;
			UiFramework.wxBottomMenu.init({
				noFixed:1,
				target:trunbackPageObj,
				menu: [
					{
						width: 100,
						name: "发送",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							var scCtrlTextarea = trunbackPageObj.find(".scCtrl");

							if(scCtrlTextarea.attr("data-checked") == 0){
								//layerPackage.fail_screen("请填写审批意见");
								layerPackage.ew_alert({
									title:"请填写审批意见",
									action_desc:'ok,我知道了',
									top:40
								});
								return;
							}

							if(!pThis.radioState){
								//layerPackage.fail_screen("请选择退回人员");
								layerPackage.ew_alert({
									title:"请选择退回人员",
									action_desc:'ok,我知道了',
									top:40
								});
								return;
							}

							var params = {};
							params.judgement = scCtrlTextarea.data("cacheData").val;
							params.workitem_id = pThis.workitem_id;
					
							trunbackPageObj.find(".trunbackUser").each(function() {
								var o = $(this),data = o.data("cacheData");

								if(data.status){
									params.callback_workitem_id = data.workitem_id;								
								}
							});

							layerPackage.lock_screen();
					
							pThis.detailObj.btnAjax(_setting,backSelectUrl,params,"backSelect");
						}
					}
				]
			});
		},
		//清除其他
		clearTrunbackHistory:function(trunbackPageObj,num){
			var pThis = this;
			trunbackPageObj.find('.circle-checkbox').each(function(index) {
				if(index != num){
					var o = $(this);

					o.html('');
					o.off();

					UiFramework.btnPackage().checkBoxBtn.init({
						target: o,
						status: false,
						type: 1,
						callback: {
							on: function(obj, self) {
								//清除操作数据
								pThis.clearTrunbackHistory(trunbackPageObj,index);
								obj.parents(".trunbackUser").data("cacheData").status = true;
								pThis.radioState = true;
							},
							off: function(obj, self) {
								obj.parents(".trunbackUser").data("cacheData").status = false;
								pThis.radioState = false;
							}
						}
					});

					o.parent().data("cacheData").status = false;
				}
			});
		}
		/*退回逻辑相关*/
	};

	return trunback;
});