require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'process:static/Scripts/list'
],function(UiFramework,t,l) {
//*
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();

	var listUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=113";
	var listObj = $('.advice_list');

	var activekey = t.getHrefParamVal("activekey") || 0;

	var listMenu = UiFramework.listMenuPackage();
	var pageNum = 1,pageCount = 0; //当前页数
	var akey = 0;
	var keyworks = "",sort = 0,order = 1,listMenuObj = null;

	var getPageData = {
		init:function(activekey,keyworks,sort,order){
			if(sort === undefined){sort = 0;}
			if(order === undefined){order = 1;}
			if(!keyworks){keyworks="";}

			layerPackage.lock_screen();
			pageNum = 1;
			pageCount = 0;

            listObj.find("div.line1").remove();
            listObj.find("section").remove();
            UiFramework.bottomInfo.remove();

			UiFramework.scrollLoadingPackage().init({
				target:listObj,
				scroll_target:listObj,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

					if(pageNum != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}
					
					l.getList({
						"listUrl":listUrl,
						"listObj":listObj,
						"page":pageNum,
						"type":activekey,
						"sort":sort,
						"order":order,
						"pro_name":keyworks,
						"listType":1,
						callback:{
							bindDom:function(res){
								var listType = res.listType,id = res.id;

								window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=1&id="+id+"&activekey="+activekey;
							}
						}
					},function(data,pCount){
						layerPackage.unlock_screen();
						//数据空时显示提示
						if(!data || data.length == 0){
							listMenuObj.showRecord();
							return;
						}

						//pageCount = pCount;
						pageCount += data.length;
						//if(pageCount == pCount){UiFramework.bottomInfo.init(listObj);return;}
						
						pageNum ++;
						listObj.attr("is_scroll_loading",false);

						if(data.length < 10 || data.length == pCount || pageCount == pCount){
							listObj.attr("is_scroll_end",true);
						}

						self.remove_scroll_waiting(listObj);

						UiFramework.bottomInfo.init(listObj);
					});
				}
			});

			listObj.scroll();
		}
	};

	var sortArr = [
		{'name':'默认','sort_name':'auto','sort_id':0},
		{'name':'申请人','sort_name':'applyName','sort_id':1},
		{'name':'申请时间','sort_name':'applyTime','sort_id':2},
		{'name':'流程名称','sort_name':'proName','sort_id':3},
		{'name':'状态','sort_name':'status','sort_id':4}
	];

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'待办','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'已办','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:activekey
		},
		sort:{ //配置头部插件排序
			status:true, 
			contents:sortArr
		},
		search:{ //搜索项配置
			status:true //状态 false 取消 true 启用
		},
		callback: {
			onMenuClick: function(data, obj,self){
				listMenuObj = self;
				akey = data.activekey;

	            if(akey == 1){
	            	var newSortArr = t.clone(sortArr);
	            	newSortArr.push({'name':'处理时间','sort_name':'dealTime','sort_id':5});

	            	listMenuObj.resetSortData(newSortArr);
	            }else{
	        		listMenuObj.resetSortData(sortArr);
	            }

            	listMenuObj.setSearchValue("");
	            listMenuObj.showRecord();

				getPageData.init(akey);
			},
			onSortClick:function(data){
				sort = data.sort_id;
				if(data.sort_type === undefined || data.sort_type == "asc"){
					order = 0;
				}else if(data.sort_type == "desc"){
					order = 1;
				}
				getPageData.init(akey,keyworks,sort,order);
			},
			onSearchClick:function(key){
				keyworks = key;
				getPageData.init(akey,key,sort,order);
			}
		},
		noRecord : {
			target:listObj,
			imgSrc : '../view/process/static/Contents/images/logo_empty.png',//图片地址
			title:'暂时没有内容',//内容标题
			desc:'' //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
},function(e){
	requireErrBack(e);
});