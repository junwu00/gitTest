require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/GenericFramework',
	'process:static/Scripts/detail',
	'process:static/Scripts/process_common'
],function(t,UiFramework,GenericFramework,detail,common) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen(); //start

	var container = $("#main-container"),
	id = t.getHrefParamVal("id") || 1,
	listType = t.getHrefParamVal("listType") || 1,
	activekey = t.getHrefParamVal("activekey") || 0;

	var isIndex = t.getHrefParamVal("isIndex") || "",
		menuType = t.getHrefParamVal("menuType") || 1,
		hash = window.location.hash;

	if(isIndex){
		isIndex = "&isIndex="+isIndex;
	}

	if(menuType){
		menuType = "&menuType="+menuType;
	}
	
	//*
	detail.init({
		target:container,
		id:id,
		listType:listType,
		activekey:activekey,
		callback:{
			error_callback:function(result){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen(result.errmsg);

				if(isIndex){
					window.history.go(-1);
				}
			},
			success_callback:function(result,ctrlName){
				if(ctrlName == "urge"){
					layerPackage.unlock_screen();
					result.errmsg = "催办成功";
				}

				if(ctrlName == "sign"){
					layerPackage.unlock_screen();
					window.location.href = result.info.redirect_url;
					return;
				}

				if(ctrlName == "backStart" || ctrlName == "backPrev"){
					//layerPackage.unlock_screen();
					//window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
					return;
				}

				if(ctrlName == "del"){
					//layerPackage.unlock_screen();
					//window.location.href = "/wx/index.php?app=process&a=mineList&debug=1";
					return;
				}

				if(ctrlName == "goToBack"){
					layerPackage.unlock_screen();

					if(isIndex){
						common.toIndexDealList();
						return;
					}

					window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
					return;
				}

				if(ctrlName == "nextNode"){
					return;
				}

				if(ctrlName == "backSelect"){
					if(isIndex){
						common.toIndexDealList();
						return;
					}

					window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
					return;
				}

				layerPackage.success_screen(result.errmsg);
			},
			finish_callback:function(result){
				layerPackage.unlock_screen();

				var mContent = container.find(".mContent");

				if(mContent.length == 0){
					UiFramework.bottomInfo.init(container,77);
				}else{
					UiFramework.bottomInfo.init(container.find(".mContent"),77);
				}

				var pcWxCode = t.getHrefParamVal("pcWxCode");
				//审批
				if(pcWxCode == "1"){
					$("#wx_bottom_menu").find("li:last > div").click();
				}
				//退回
				else if(pcWxCode == "0"){
					$("#wx_bottom_menu").find("li:first > div").click();
				}
			}
		}
	});
	//*/
},function(e){
	requireErrBack(e);
});



