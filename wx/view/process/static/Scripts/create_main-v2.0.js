require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!view/process/Templates/indexList.html',
	'process:static/Scripts/process_common-v2.0',
	//'process:static/Scripts/otherProcess',
	'process:static/Scripts/otherProcess-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-filesPics-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-userDept-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-datetime-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-calc-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-sublist-v2.0',
],function($,t,UiFramework,tpl,listTemplate,common/*,otherProcess*/,otherProcess2,ScFormCtrl) {
	try {
    common.storageClearAll(); // mrc add tmp 先删除所有的表单缓存
  } catch (e) {
    console.log(e);
  }

	var layerPackage = UiFramework.layerPackage();
	layerPackage.lock_screen();//遮罩start

	//jqObj对象
	var container = $("#main-container"),
	getProcessUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=104",
	getFormUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=103",
	saveOrEditUrl = "index.php?model=process&m=ajax&a=index&cmd=105",
	sendUrl = "index.php?model=process&m=ajax&a=index&cmd=106",
	nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
	id = t.getUrlParam("id"),
	code = t.getUrlParam("preview_code");//加密串 预览用

	if(!id){id = 0;}

	var is_other_proc = null;

	//公用操作
	var commonCtrl = {
		link:function(params,callback){
			layerPackage.lock_screen({
				"msg":params.msg || ""
			});//遮罩start

			t.ajaxJson({
				url:params.url,
				data:{
					"data":JSON.stringify(params.data)
				},
				callback:function(result,status){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					if(!callback){return;}
					callback(result,status);
				}
			});
		}
	};

	var menu = new Array(),
	save_menu = {
		width: 100,
		name: "保存",
		iconClass: "",
		btnColor: "w",
		id: null,
		click: function(self, obj, index) {
			//隐藏控件错误信息
			common.ctrlHideErrorMsg();

			//只需要校验表单名和附件
			var c1 = common.checkFormName(),
				c2 = common.checkHasFilesUpload();

			if(!c1 || !c2){return;}

			var data = common.getCtrlData();

			commonCtrl.link({
				msg:"保存中……",
				url:saveOrEditUrl,
				data:data
			},function(){
				layerPackage.success_screen("保存成功");

				common.storageClear();
				layerPackage.unlock_screen();
				common.toMineList(2);
			});
		}
	},
	submit_menu = {
		width: 100,
		name: "发送",
		btnColor: "g",
		iconClass: "",
		draftSendUrl: null,
		id: null,
		click: function(self, obj, index) {
			//校验所有控件
			if(!common.checkAllCtrl()){return;}

			var data = common.getCtrlData();

			console.log(data);
			//return;

			commonCtrl.link({
				msg:"发送中……",
				url:saveOrEditUrl,
				data:data
			},function(result,status){
				common.storageClear();
				layerPackage.unlock_screen();

				var workitem_id = result.info.workitem_id,
				formsetInst_id = result.info.formsetInst_id;

				//common.toSendPage("&workitemId="+workitem_id+"&formsetInstId="+formsetInst_id+"&list=2");
				common.toSendPage({
					workitem_id:workitem_id,
					formsetInst_id:formsetInst_id,
					list:2
				});
			});
		}
	};

	//获取当前用户信息 && 表单信息
	$.when($.ajax({
		url:getProcessUrl,
		type:"post",
		data:{
			"data":{
				"form_id":id,
				"code":code
			}
		}
	}),$.ajax({
		url:getFormUrl,
		type:"post",
		data:{
			"data":{
				"form_id":id,
				"code":code
			}
		}
	})).done(function(r1,r2){
		try{
			if(r1[1] != "success" || r2[1] != "success"){return;}

			var r1Json = $.parseJSON(r1[0]),
			r2Json = $.parseJSON(r2[0]);

			if(r1Json.errcode != 0){
				layerPackage.ew_alert({
					title:r1Json.errmsg,
					action_desc:'ok,我知道了',
					top:40,
					callback:function(){
						window.history.go(-1);
					}
				});
				return;
			}
			if(r2Json.errcode != 0){
				layerPackage.ew_alert({
					title:r2Json.errmsg,
					action_desc:'ok,我知道了',
					top:40,
					callback:function(){
						window.history.go(-1);
					}
				});
				return;
			}

			//初始化
			common.init(container);

			container.append("<div class='mContent'></div>");
			var mContent = container.find(".mContent");

			var data = r2Json.info;
			//预览不走缓存
			if(!code){
				//缓存数据
				common.storageInit(id,data,function(dataObj){
					data = dataObj;
				});
			}
			//
			//$storage.clearAll();
			//console.log(data);

            //表单主体信息
			mContent.append(common.getFormInfo(data));

			var params = {
				platid:platid,
				afterEvent:function(){
					common.scCtrlErrMsg(container,"",1);
				}
			};

			//头部用户信息
			var headerPartData =  common.getHeaderPartData(r1Json.info);
			mContent.append(ScFormCtrl(headerPartData,params));

			//表单控件信息
            for(var d in data.inputs){
				//人员部门选择器
				if(data.inputs[d].type == "people" || data.inputs[d].type == "dept"){
					//continue;
					//console.log(data.inputs[d]);
					mContent.append(ScFormCtrl.getCtrlExtendUserDept(data.inputs[d],userInfo,params));
				}
				//时间控件
				else if(data.inputs[d].type == "date"){
					//continue;
					//console.log(data.inputs[d]);
					mContent.append(ScFormCtrl.getCtrlExtendDatetime(data.inputs[d],{
						afterEvent:function(data,obj){
							otherProcess2.afterEvent({
								data:data,
								target:obj
							});

							params.afterEvent();
						}
					}));
				}
				//子表单
				else if(data.inputs[d].type == "table"){
					//continue;
					//console.log(data.inputs[d]);
					mContent.append(ScFormCtrl.getCtrlSublist(data.inputs[d],{
						platid:platid,
						afterEvent:function(data,obj){
							otherProcess2.afterEvent({
								data:data,
								target:obj
							});

							params.afterEvent();
						}
					}));
				}
				//图片控件
				else if(data.inputs[d].type == "picture"){
					//continue;
					//console.log(data.inputs[d]);
					mContent.append(ScFormCtrl.getCtrlExtendPics(data.inputs[d],params));
				}
				//基础控件
				else{
					//continue;
					//console.log(data.inputs[d]);
					mContent.append(ScFormCtrl(data.inputs[d],{
						afterEvent:function(data,obj){
							otherProcess2.afterEvent({
								data:data,
								target:obj
							});

							params.afterEvent();
						}
					}));
				}
			}

			/*-----特殊表单处理----*/
			is_other_proc = data.is_other_proc;

			saveOrEditUrl = otherProcess2.getSaveOrEditUrl(data.is_other_proc,saveOrEditUrl);

			otherProcess2.init({
				is_other_proc:is_other_proc,
				target:container,
				data:data
			});
			/*-----特殊表单处理----*/

			//附件
			if(data.file_type != 0){
				mContent.append(ScFormCtrl.getCtrlExtendFiles(data));
			}

			//绑定公式插件
			ScFormCtrl.calc("mContent");

			//底部按钮 预览不需要底部菜单
			if(!code){
				menu.push(submit_menu);
				menu.push(save_menu);
				UiFramework.wxBottomMenu.init({
					target:mContent,
					menu: menu,
					noFixed:1
				});

				mContent.css({
					//"position":"fixed",
					"height":"100%",
					"top":"0",
					"width":"100%",
					"overflow-x":"hidden",
					"overflow-y":"auto",
					"-webkit-overflow-scrolling" : "touch"
				});
			}

			layerPackage.unlock_screen();
		}catch(e){
			layerPackage.fail_screen(e);
		}
	}).fail(function(){
		layerPackage.fail_screen("请求错误");
	});
});