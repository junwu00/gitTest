//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/detail-v2.0.html',
	'common:widget/lib/GenericFramework-v2.0',
	'process:static/Scripts/process_common-v2.0',
	'process:static/Scripts/otherProcess-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-filesPics-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-userDept-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-datetime-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-calc-v2.0',
	'common:widget/lib/scFormCtrl/scFormCtrl-sublist-v2.0'
],function(tools,UiFramework,tpl,detailTpl,GenericFramework,common,otherProcess2,ScFormCtrl){
	var layerPackage = UiFramework.layerPackage();
	layerPackage.lock_screen();//遮罩start

	var getDetailUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=108",
	urgeUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=117",
	goToBackUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=116",
	signUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=119",
	delUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=112",
	//指纹验证 
	checkSoterUrl = "/wx/index.php?model=index&m=mine&a=index&cmd=106";

	var isIndex = tools.getHrefParamVal("isIndex") || "",
		hash = window.location.hash;

	var detail = {
		_setting:{
			id:null,
			listType:null,
			activekey:null,
			target:null,
			callback:{
				error_callback:null,
				success_callback:null,
				finish_callback:null
			}
		},
		dom:null,
		$dom:null,
		cacheSetting:null,
		cacheData:null,
		commentPage:1,
		dealPage:1,
		checkParams: function(setting) {
			if(setting.target == null || !(setting.target instanceof jQuery)){
				console.log('错误的jq对象');
				return;
			}

			var _setting = tools.clone(this._setting);
			$.extend(true, _setting, setting);

			this.setting = setting;

			return _setting;
		},
		init: function(_setting) {
			_setting = this.checkParams(_setting);

			if(!_setting.id || !_setting.listType || _setting.activekey === undefined){console.log("配置错误");return;}

			this.cacheSetting = _setting;

			this.getDetail(_setting);
		},
		//获取详情页面
		getDetail:function(_setting){
			var pThis = this,
			listType = _setting.listType,
			activekey = _setting.activekey,
			id = _setting.id,
			params = {
				"listType":listType
			};

			//待办/已办
			if(listType == 1){
				params.workitem_id = id;
			}
			//我的发起
			else if(listType == 2){
				params.formsetinst_id = id;
			}
			//我的知会
			else if(listType == 3){
				params.notify_id = id;
			}

			tools.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":params
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						if(!_setting.callback || !tools.isFunction(_setting.callback.error_callback)){return;}
						_setting.callback.error_callback.apply(pThis,[result]);
						return;
					}

					common.init(_setting.target);

					var data = result.info;
					pThis.cacheData = data;
					_setting.data = data;
					console.log(data);

					var form_vals = null;

					if(listType == 1 || listType == 2){
						form_vals = data.formsetinst.form_vals;
						var state = data.workitem.state,
							formsetinstId = data.formsetinst.id;

						_setting.work_id = data.formsetinst.work_id;
						_setting.workitemId = data.workitem.id;
						_setting.workitemState = data.workitem.state;
						_setting.formsetinstId = formsetinstId;
						_setting.workNodeId = data.workitem.work_node_id;
						_setting.isStart = data.workitem.is_start;
						_setting.workType = data.workitem.work_type;
						_setting.isLinkSign = data.formsetinst.is_linksign;
						_setting.formsetinstState = data.formsetinst.state;
						_setting.isReturnBack = data.workitem.is_returnback;

						_setting.isOtherProc = data.formsetinst.is_other_proc;
						_setting.createId = data.workitem.creater_id;
						_setting.state = state;

						try{_setting.fingerPrint = data.fingerprint;}catch(e){console.log(e);}
					}else{
						form_vals = data.notify.form_vals;
						var state = data.notify.state,
							formsetinstId = data.notify.formsetinst_id;

						_setting.formsetinstId = formsetinstId;

						_setting.isOtherProc = data.notify.is_other_proc;

						_setting.createId = data.notify.creater_id;
						_setting.state = state;

						try{_setting.fingerPrint = data.fingerprint;}catch(e){console.log(e);}
					}

					//工作项ID
					_setting.workitemId = data.workitem_id;

					//草稿
					if(listType == 2 && activekey == 2){
						var contentObj = _setting.target;
						contentObj.append("<div class='mContent'></div>");

						var mContent = contentObj.find(".mContent");

						//表单主体信息
						mContent.append(common.getFormInfo(data.formsetinst));

						var parmas = {
							afterEvent:function(){
								common.scCtrlErrMsg(contentObj,"",1);
							}
						};

						//表单控件信息
						var inputs = data.formsetinst.form_vals;
			            for(var k in inputs){
							var d = inputs[k];

							//人员部门选择器
							if(d.type == "people" || d.type == "dept"){
								mContent.append(ScFormCtrl.getCtrlExtendUserDept(d,userInfo,parmas));
							}
							//时间控件
							else if(d.type == "date"){
								mContent.append(ScFormCtrl.getCtrlExtendDatetime(d,{
									afterEvent:function(data,obj){
										otherProcess2.afterEvent({
											data:data,
											target:obj
										});

										parmas.afterEvent();
									}
								}));
							}
							//子表单
							else if(d.type == "table"){
								mContent.append(ScFormCtrl.getCtrlSublist(d,parmas));
							}
							//图片控件
							else if(d.type == "picture"){
								mContent.append(ScFormCtrl.getCtrlExtendPics(d,parmas));
							}
							//基础控件
							else{
								mContent.append(ScFormCtrl(d,{
									afterEvent:function(data,obj){
										otherProcess2.afterEvent({
											data:data,
											target:obj
										});

										parmas.afterEvent();
									}
								}));
							}
						}

						//其他非表单内容
						//附件
						if(data.workitem.file_type != 0){
							data.workitem.files = data.files;
							mContent.append(ScFormCtrl.getCtrlExtendFiles(data.workitem));
						}

						mContent.css({
							"height":"100%",
							"overflow-x":"hidden",
							"overflow-y":"auto",
							"-webkit-overflow-scrolling" : "touch"
						})

						//-------特殊处理-------
						otherProcess2.init({
							is_other_proc:_setting.isOtherProc,
							target:_setting.target,
							data:data,
							userId:_setting.createId,
							formsetinstId:_setting.formsetinstId,
							state:_setting.state,
							workitemId:_setting.workitemId
						});

						common.setSaveOrEditUrl(otherProcess2.getSaveOrEditUrl(_setting.isOtherProc,common.getSaveOrEditUrl()));

						//绑定公式插件
						ScFormCtrl.calc();

						pThis.bindUploadFileDom(_setting);

						pThis.appendEditBottonBtn(_setting);

						pThis.finish_callback(_setting);
						return;
					}

					//组装表单基本内容
					pThis.appendHtml(_setting,data);

					//组装控件
					var scCtrlContent = _setting.target.find("#scCtrlContent");

					var json = {
						_setting:_setting,
						data:data,
						form_vals:form_vals,
						params:{
							scCtrlState: 1
						},
						listType:listType,
						activekey:activekey,
						appendTarget:scCtrlContent
					};

					pThis.appendScCtrlToHtml(json);

					//审批页面 编辑按钮
					if((listType == 1 && activekey == 0) || (listType == 2 && activekey == 0)){
						pThis.appendFormTableEditBtn(scCtrlContent,data,_setting,form_vals);
					}

					//-------特殊处理-------
					otherProcess2.init({
						is_other_proc:_setting.isOtherProc,
						target:_setting.target,
						data:data,
						listType:listType,
						userId:_setting.createId,
						formsetinstId:_setting.formsetinstId,
						state:_setting.state,
						workitemId:_setting.workitemId
					});

					common.setSaveOrEditUrl(otherProcess2.getSaveOrEditUrl(_setting.isOtherProc,common.getSaveOrEditUrl()));

					//绑定基础事件
					pThis.bindDom(_setting);

					//底部按钮
					pThis.appendBottomBtn(_setting);

					layerPackage.unlock_screen();

					pThis.finish_callback(_setting);
				}
			});
		},
		//绑定基础事件
		bindDom:function(_setting){
			var pThis = this;
			//Tab切换
			_setting.target.on("click",".tab li",function(){
				var li = $(this),index = li.index();

				if(li.hasClass('active')){return;}

				li.addClass('active').siblings('li').removeClass('active');

				li.parents('section')
				.find('.tab-part:eq('+index+')')
				.removeClass('hide')
				.siblings('.tab-part').addClass('hide');
			});

			//流程图
			_setting.target.on("click",".checkFlow",function(){
				window.location.href = "/wx/index.php?model=process&a=processPic&formsetinstId="+_setting.formsetinstId;
			});

			//打印
			_setting.target.on("click",".printForm",function(){
				window.location.href = "/wx/index.php?model=process&a=print_form&formsetinst_id="+_setting.formsetinstId;
			});

			this.bindUploadFileDom(_setting,1);
		},
		btnAjax:function(_setting,url,params,ctrlName,callback){
			var pThis = this;
			tools.ajaxJson({
				url: url,
				data: {
					"data": params
				},
				callback: function(result, status) {
					if (status == false && result == null) {return;}

					if (result.errcode != 0 && ctrlName != "backPrev") {
						if (!_setting.callback || !tools.isFunction(_setting.callback.error_callback)) {return;}
						_setting.callback.error_callback.apply(pThis, [result]);
						layerPackage.unlock_screen();
						return;
					}

					if (!_setting.callback || !tools.isFunction(_setting.callback.success_callback)) {return;}
					_setting.callback.success_callback.apply(pThis, [result, ctrlName]);

					if(!tools.isFunction(callback)){return;}
					callback(result, status);
				}
			});
		},
		//详情模板
		appendHtml:function(_setting,data){
			var pThis = this;
			var dom = null;
			tpl.register('dateFormat', tools.dateFormat);
			tpl.register('bytesToSize', tools.bytesToSize);
			tpl.register('getFileTypeExtIcon', tools.getFileTypeExtIcon);
			tpl.register('checkNewline', tools.checkNewline);
			tpl.register('seeCount', pThis.seeCount);
			dom = tpl(detailTpl,{
				data:data,
				listType:_setting.listType,
				activekey:_setting.activekey,
				cdn_domain:cdn_domain,
				defaultFace:defaultFace,
				media_domain:media_domain,
				platid:platid
			});
			tpl.unregister('dateFormat');
			tpl.unregister('bytesToSize');
			tpl.unregister('getFileTypeExtIcon');
			tpl.unregister('checkNewline');
			tpl.unregister('seeCount');

			this.dom = $.trim(dom);
			this.$dom = $(this.dom);

			//默认插入到开头位置
			_setting.target.html("").append(this.$dom);

			_setting.target.css({
				"position":"fixed",
				"height":_setting.listType == 3 ? "calc(100% - 30px)" : "calc(100% - 56px - 30px)",
				"height": _setting.listType == 3 ? "calc(100% - 30px)" : "-webkit-calc(100% - 56px - 30px)",
				"top":"0",
				"left":"0",
				"width":"calc(100% - 20px)",
				"width":"-webkit-calc(100% - 20px)",
				"overflow-x":"hidden",
				"overflow-y":"auto",
				"-webkit-overflow-scrolling" : "touch",
				"padding":"15px 10px"
			});
		},
		//完成回调
		finish_callback:function(_setting){
			if(!_setting.callback || !tools.isFunction(_setting.callback.finish_callback)){return;}
			_setting.callback.finish_callback.apply(this);
		},
		//附件操作
		bindUploadFileDom:function(_setting,noDel){
			var pThis = this;

			//附件操作
			_setting.target.on("click",".scCtrlUploadFiles .queue-file",function(){
				var md5_hash = $(this).attr("data-hash"),
				ext = $(this).attr("data-ext"),
				//path = $(this).find("input").val(),
				path = $(this).attr("data-path"),
				filename = $(this).find(".filename").text(),
				id = $(this).attr("data-id"),
				workNodeId = $(this).attr("data-worknodeid"),
				createId = $(this).attr("data-createid"),
				filesize = $(this).attr("data-filesize");

				var data = _setting.data;

				var btnMenuPackage = UiFramework.btnMenuPackage();

				var opt = {};
				opt.data = {};
				opt.data.el = $(this);
				opt.data.hash = md5_hash;
				opt.data.ext = ext;
				opt.data.path = path;
				opt.data.name = filename;
				opt.data.id = id;
				opt.menus = [];

				var extClass = tools.getFileTypeExtIcon(ext);

				var url = "/wx/index.php?model=process&m=ajax&a=index&cmd=121&formsetinst_id="+_setting.formsetinstId+"&cf_id="+id;

				//platid
				/*
					1：iPod、iPad、iPhone
					2：android
					3：wp手机
					4：Windows
					5：Mac
					0：其他
				*/

				if(extClass == "doc" || extClass == "ppt" 
					|| extClass == "xls" || extClass == "txt" 
					|| extClass == "pdf"){

					if(filesize){
						if(platid != 1){
							GenericFramework.viewFile(ext,md5_hash);
						}
						return;
					}

					if(platid != 1){
						var m1 = {};
						m1.id = 1;
						m1.name = "预览";
						m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
						m1.fun = function(el, m, o) {
							GenericFramework.viewFile(ext,md5_hash);
						};
						opt.menus.push(m1);
					}

					var m2 = {};
					m2.id = 2;
					m2.name = "下载";
					m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m2.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url,null,ext);
					};
					opt.menus.push(m2);
					if(!noDel && ((workNodeId == _setting.workNodeId 
						&& createId == data.workitem.handler_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)) || !workNodeId)){
						var m3 = {};
						m3.id = 3;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();

							if(_setting.target){
								_setting.target.find(".scCtrl[data-type=files]").data("scCtrlFiles").delFile(md5_hash,ext);
							}
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
					}

					if((platid == 1 || platid == 2) && opt.menus.length == 1){
						GenericFramework.fileDownload(url,null,ext);
					}else{
						btnMenuPackage.init(opt);
					}
				}else if(extClass == "img"){
					var o = $(this),img = o.find("img");
					function imgView(){
						try{
							if(platid == 4 || platid ==5){
								GenericFramework.viewFile(ext,md5_hash);
							}else{
								GenericFramework.init(wxJsdkConfig);

								GenericFramework.getNetworkType(function(networkType){
									if(networkType == "wifi"){
										path = path.replace("resize","origin");
									}

									GenericFramework.previewImage({
										target:img,
										current:path,
										urls:[path]
									});
								});
							}
						}catch(e){
						}
					}

					if(filesize){
						imgView();
						return;
					}

					var m1 = {};
					m1.id = 1;
					m1.name = "预览";
					m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
					m1.fun = function(el, m, o) {
						imgView();
					};
					opt.menus.push(m1);
					var m3 = {};
					m3.id = 2;
					m3.name = "下载";
					m3.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m3.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						//pcWx
						if(platid == 4 || platid == 5){
							var newDownloadUrl = url + "&origin=1";
							GenericFramework.fileDownload(newDownloadUrl);
						}
						//app wx
						else{
							//检测设备网络状态
							GenericFramework.getNetworkType(function(networkType){
								var newDownloadUrl = url;
								var origin = 0;
								if(networkType == "wifi"){
									origin = 1;
								}

								newDownloadUrl += "&origin="+origin;

								GenericFramework.fileDownload(newDownloadUrl);
							});
						}
					};
					opt.menus.push(m3);
					if(!noDel && ((workNodeId == _setting.workNodeId 
						&& createId == data.workitem.handler_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)) || !workNodeId)){
						
						var m2 = {};
						m2.id = 3;
						m2.name = "删除";
						m2.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m2.fun = function(el, m, o) {
							o.el.remove();
							if(_setting.target){
								_setting.target.find(".scCtrl[data-type=files]").data("scCtrlFiles").delFile(md5_hash,ext);
							}
							btnMenuPackage.hide();
						};
						opt.menus.push(m2);
						btnMenuPackage.init(opt);
					}else{
						try{
							if(platid == 4 || platid == 5){
								btnMenuPackage.init(opt);
							}else{
								GenericFramework.init(wxJsdkConfig);

								GenericFramework.getNetworkType(function(networkType){
									if(networkType == "wifi"){
										path = path.replace("resize","origin");
									}
									GenericFramework.previewImage({
										target:$(this),
										current:path,
										urls:[path]
									});
								});
							}
						}catch(e){

						}
					}
				}else{
					if(filesize){return};

				    var m2 = {};
				    m2.id = 1;
			        m2.name = "下载";
			        m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			        m2.fun = function(el, m, o) {
			        	//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
			        };
			        opt.menus.push(m2);
			        if(!noDel && ((workNodeId == _setting.workNodeId 
			        	&& createId == data.workitem.handler_id 
			        	&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)) || !workNodeId)){
						var m3 = {};
						m3.id = 2;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();
							if(_setting.target){
								_setting.target.find(".scCtrl[data-type=files]").data("scCtrlFiles").delFile(md5_hash,ext);
							}
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
						//btnMenuPackage.init(opt);
					}

					if((platid == 1 || platid == 2) && opt.menus.length == 1){
						GenericFramework.fileDownload(url);
					}else{
						btnMenuPackage.init(opt);
					}
				}
			});
		},
		//获取发送数据
		getSendData:function(_setting){
			var data = _setting.data;

			var params = {
				"form_id":data.formsetinst.form_id,
				"form_name":data.formsetinst.form_name,
				"work_id":_setting.work_id,
				//"form_vals":common.getCtrlData(),
				//Edit
				"workitem_id":_setting.workitemId,
				"formsetInst_id":_setting.formsetinstId,
				"work_node_id":data.workitem.work_node_id,
				"file_type":data.workitem.file_type,
				"is_other_proc":_setting.isOtherProc
			};

			return params;
		},
		//保存
		saveProcess:function(_setting,msg,checkData,callback){
			if(msg != "noMsg"){
				layerPackage.lock_screen({
					"msg":msg||""
				});
			}
			
			var data = this.getSendData(_setting);

			common.saveProcess(data,checkData,function(result,status){
				if(!callback){return;}
				callback(result,status);
			});
		},
		//删除表单
		delProcess:function(_setting,callback){
			var pThis = this;
			layerPackage.ew_confirm({
				title:"确定要删除该工作项？",
				ok_callback:function(){
					layerPackage.lock_screen({
						"msg":"删除中"
					});
					pThis.btnAjax(_setting,delUrl,{
						"workitem_id":_setting.workitemId
					},"del",function(){
						layerPackage.unlock_screen();
						if(!callback){return;}
						callback();
					});
				}
			});
		},
		//草稿底部按钮
		appendEditBottonBtn:function(_setting){
			var menu = new Array(),pThis = this;

			menu.push({
				width: 100,
				name: "发起申请",
				iconClass: "",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					layerPackage.unlock_screen();

					pThis.saveProcess(_setting,"发送中……",1,function(result,status){
						var workitem_id = result.info.workitem_id,
						formsetInst_id = result.info.formsetInst_id;
						//common.toSendPage("&workitemId="+workitem_id+"&formsetInstId="+formsetInst_id+"&list=1");
						common.toSendPage({
							workitem_id:workitem_id,
							formsetInst_id:formsetInst_id,
							list:1
						});
					});
				}
			});

			menu.push({
				width: 100,
				name: "继续保存",
				iconClass: "",
				btnColor: "w",
				id: null,
				click: function(self, obj, index) {
					//只需要校验是否有正在上传的附件
					if(!common.checkHasFilesUpload()){return;}

					pThis.saveProcess(_setting,"保存中……",0,function(result,status){
						common.toMineList(2);
					});
				}
			});

			menu.push({
				width: 100,
				name: "删除",
				iconClass: "",
				btnColor: "r",
				id: null,
				click: function(self, obj, index) {
					pThis.delProcess(_setting,function(){
						common.toMineList(2);
					});
				}
			});

			UiFramework.wxBottomMenu.init({
				target:_setting.target.find(".mContent"),
				menu: menu,
				noFixed:1
			});
		},
		//详情底部按钮
		appendBottomBtn:function(_setting){
			if(_setting.listType == 3){return;}

			var menu = new Array();
			var pThis = this;
			var workitemState = _setting.workitemState,
				isStart = _setting.isStart,
				workType = _setting.workType,
				isLinkSign = _setting.isLinkSign,
				formsetinstState = _setting.formsetinstState,
				formsetinstId = _setting.formsetinstId,
				listType = _setting.listType,
				id = _setting.id,
				workitemId = _setting.workitemId;

			var flag = false;

			if((workitemState == 1 || workitemState == 2 || workitemState == 3) && isStart == 0){
				menu.push({
					width: 30,
					name: "退回",
					iconClass: "",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						pThis.useFingerPrint(_setting.fingerPrint,"cancel",function(flag){
							if(!flag){return;}

							//退回逻辑处理
							pThis.trunback(_setting);
						},id);
					}
				});
				
				if(workType == 0 && isLinkSign == 1){
					menu.push({
						width: 70,
						name: "签名",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							pThis.useFingerPrint(_setting.fingerPrint,"apply",function(flag){
								if(!flag){return;}

								pThis.saveProcess(_setting,"保存中……",1,function(result,status){
									pThis.btnAjax(_setting,signUrl,{
										"formsetinst_id":formsetinstId,
										"workitem_id":workitemId,
										"isIndex":isIndex,
										"hash":hash
									},"sign");
								});
							},id);
						}
					});
				}else{
					menu.push({
						width: 70,
						name: workType == 1 ? "确认" : "同意",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							pThis.useFingerPrint(_setting.fingerPrint,"apply",function(flag){
								if(!flag){return;}

								pThis.saveProcess(_setting,"noMsg",1,function(result,status){
									common.toSendPage({
										workitem_id:_setting.workitemId,
										_setting:_setting,
										detail:1, //详情页面过来
									});
								});
							},id);
						}
					});
				}
			}else if((workitemState == 1 || workitemState == 2 || workitemState == 3) && isStart == 1){
				var comonBtnStyle = "background-color:#fff !important;margin-left:0 !important;margin-right:0 !important;"
				menu.push({
					width: 100,
					name: "重新发送",
					iconClass: "",
					btnColor: "w",
					id: null,
					fontClass:"c-green",
					btnStyle:comonBtnStyle,
					click: function(self, obj, index) {
						//pThis.getStep(_setting,"");
						//发送中……
						pThis.saveProcess(_setting,"noMsg",1,function(result,status){
							common.toSendPage({
								workitem_id:_setting.workitemId,
								_setting:_setting,
								detail:1
							});
						});
					}
				});
				menu.push({
					width: 100,
					name: "删除",
					iconClass: "",
					btnColor: "w",
					id: null,
					fontClass:"c-red",
					btnStyle:comonBtnStyle,
					click: function(self, obj, index) {
						pThis.delProcess(_setting,function(){
							common.toMineList();
						});
					}
				});

				flag = true;
			}else{
				menu.push({
					width: 30,
					name: "知会",
					iconClass: "",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						var activekey = tools.getHrefParamVal("activekey") || 0;
						pThis.notify(_setting,activekey);
						return;

						common.toNotifyPage(formsetinstId,listType,id,activekey);
						//window.location.href = "/wx/index.php?model=process&a=notify&degbug=1&formsetinstId="+formsetinstId+"&listType="+listType+"&id="+id;
					}
				});

				if(formsetinstState == 1){
					menu.push({
						width: 70,
						name: "催办",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							layerPackage.lock_screen({
								"msg":"催办中"
							});
							pThis.btnAjax(_setting,urgeUrl,{
								"formsetinst_id":formsetinstId
							},"urge");
						}
					});
				}else if(formsetinstState == 2){
					menu[0].width = 100;
				}
			}

			if(flag){
				var mContent = _setting.target.find(".mContent");
				_setting.target.css({
					"height":"calc(100% - 30px)",
					"height":"-webkit-calc(100% - 30px)",
					"width":"100%",
					"padding":"15px 0"
				});
				mContent.css({
					//"height":"100%",
				});
				UiFramework.wxBottomMenu.init({
					target:_setting.target,
					menu: menu,
					noFixed:1
				});
			}else{
				UiFramework.wxBottomMenu.init({
					target:_setting.target.parent(),
					menu: menu
				});
			}
		},
		/*指纹相关*/
		//是否使用指纹
		useFingerPrint:function(fingerPrint,type,callback,id){
			//PC微信
			if((platid == 4 || platid ==5) && fingerPrint == "1"){
				//弹出二维码
				var pcWxCode = "";
				if(type == "cancel"){
					pcWxCode = 0;
				}else if(type == "apply"){
					pcWxCode = 1;
				}

				var html = new Array();
				html.push('<img width="126px" height="126px" src="/wx/index.php?model=index&m=app&a=qrcode&id='+id+'&pcWxCode='+pcWxCode+'" alt="" />');
				html.push('<p class="fs15 mt15">微信扫描二维码，完成指纹识别</p>');
				html.push('<p class="fs12 c-4a mt5">你已开启指纹识别功能，需要完成指纹识别才能完成</p>');

				layerPackage.ew_alert({
					title:html.join(''),
					action_desc:'取消',
					top:40
				});

				callback(false);
				return false;
			}

			var pThis = this;
			//指纹是否开启
			if(fingerPrint == 1){
				layerPackage.lock_screen();
				GenericFramework.init(wxJsdkConfig,1);
				GenericFramework.getSupportSoter(function(canSupport){
					//手机支持指纹
					if(canSupport){
						GenericFramework.requireSoter(function(res,state){
							if(state){
								tools.ajaxJson({
									url:checkSoterUrl,
									data:{
										"data":{
											"json_str":res.result_json,
											"sign_str":res.result_json_signature
										}
									},
									callback:function(result,status){
										layerPackage.unlock_screen();
										$(".alert_screen").remove();

										if(result.errcode !=0){
											pThis.showSoterErrorMsg("",type);

											callback(false);
											return;
										}
										
										callback(true);
									}
								});
							}else{
								layerPackage.unlock_screen();
								//系统已锁屏
								if(res.err_code == 90011){
									layerPackage.ew_alert({
										"title":"<p class='fs20 fsn'>请锁屏后重来</p><p class='mt15 fs12 c-red'>由于多次识别失败，手机指纹已锁定</p>"
									});
								}
							}
						});
					}else{
						layerPackage.unlock_screen();
						pThis.showSoterErrorMsg("手机不支持指纹识别!",type);
					}
				});
			}else{
				callback(true);
			}
		},
		//----------指纹识别设置-----------
		getErrorHtml:function(errmsg){
			var html = new Array();

			html.push('<i class="errorIcon"></i>');
			html.push('<b class="fs20 lhn fsn block">指纹认证失败！</b>');
			html.push('<span class="c-red fs12 block mt5">');
			if(errmsg){
				html.push(errmsg);
			}else{
				html.push('暂时无法获取指纹信息，请重新尝试，<i class="fsn c-green restartSoter">重新录入</i>');
			}
			html.push('</span>');
			return html.join('');
		},
		//弹出错误页面
		showSoterErrorMsg:function(errmsg,type){
			var pThis = this;
			layerPackage.ew_alert({
				title:pThis.getErrorHtml(errmsg),
				action_desc:'<span class="c-black">我知道了</span>',
				top:27,
				callback:function(){
					
				}
			});
			
			if(errmsg =="" && type == "apply"){
				var alert = $(".alert_screen");
				alert.on("click",".restartSoter",function(){
					//pThis.useFingerPrint(1,type);
					$(".alert_screen").remove();
					$(".comfirm_input").find("#wx_bottom_menu").remove();
					$("#main-container > #wx_bottom_menu").find("li:last > div").click();
				});
			}
		},
		/*指纹相关*/
		/*退回逻辑相关*/
		trunback:function(_setting){
			var workitemId = _setting.workitemId;
			var pThis = this;
			this.saveProcess(_setting,"退回中",0,function(result,status){
				require(['process:static/Scripts/trunbackPage_main'],function(trunback){
					trunback.init(workitemId,_setting,pThis,common);
				});
			})
		},
		/*知会逻辑相关*/
		notify:function(_setting,activekey){
			var pThis = this;
			require(['process:static/Scripts/notify_main-v2.0'],function(notify){
				common.cacheHash = hash;
				notify.init(_setting,pThis,common,activekey);
			});
		},
		//表单编辑按钮
		appendFormTableEditBtn:function(target,data,_setting,form_vals){
			//console.log(data);

			var flag = false;

			for (var i in form_vals) {
				var input = form_vals[i];

				if(input.visit_input == "1" && input.edit_input == "1"){
					flag = true;
					break;
				}

				if(input.type == "table"){
					if(input.rec && input.rec.length > 0){
						var table_arr = input.table_arr;

						for (var k in table_arr) {
							var t_d = table_arr[k];
							if(t_d.visit_input && t_d.edit_input){
								flag = true;
								break;
							}
						};
					}
				}
			};

			//附件
			if(data.workitem.file_type != "0"){
				flag = true;
			}

			if(!flag){return;}

			var html = new Array();
			html.push("<div class='editFormTable'>");
			html.push("<div class='wxLine'></div>");
			html.push("<div class='scCtrlPadding fs17 lh-n c-green tac'>编辑表单</div>");
			html.push("</div>");

			target.append(html.join(''));

			var pThis = this;
			target.find(".editFormTable").on("click",function(){
				pThis.openFormTable(data,target,_setting,form_vals);
			});
		},
		//打开弹出层 -- 表单编辑
		editFormTableHash : "editFormTable",
		openFormTable:function(data,target,_setting,form_vals){
			$("body").find(".dialogEditFormTable").remove();

			var setting = $.extend(true,{}, _setting);
			data = $.extend(true, {} ,data);
			form_vals = $.extend(true, {} ,form_vals);
			
			var html = new Array();
			html.push("<div class='dialogEditFormTable bg-gray'>");
			html.push("<div class='mContent'></div>");
			html.push("</div>");

			$("body").append(html.join(''));

			window.location.hash = this.editFormTableHash;

			var listType = 1;
			var dialogEditFormTable = $("body").find(".dialogEditFormTable");
			var jqObj = dialogEditFormTable.find(".mContent");
			setting.target = jqObj;
			
			//附件额外参数
			data.filesId = "editFormTable";

			//重新初始化对象
			otherProcess2.jqObj = target;

			this.appendScCtrlToHtml({
				_setting:setting,
				data:data,
				form_vals:form_vals,
				params:{
					afterEvent:function(){
						common.scCtrlErrMsg(dialogEditFormTable,"",1);
					}
				},
				listType:1,
				activekey:0,
				appendTarget:jqObj
			});

			var bottomBtnsObj = this.addEditFormTableBtn(dialogEditFormTable,_setting,data);

			//-------特殊处理-------
			otherProcess2.init({
				is_other_proc:setting.isOtherProc,
				target:setting.target,
				data:data,
				listType:listType,
				userId:setting.createId,
				formsetinstId:setting.formsetinstId,
				state:setting.state,
				workitemId:setting.workitemId
			});

			common.setSaveOrEditUrl(otherProcess2.getSaveOrEditUrl(setting.isOtherProc,common.getSaveOrEditUrl()));

			//绑定公式插件
			ScFormCtrl.calc("dialogEditFormTable");

			//事件处理
			this.bindDialogEvent(setting);
			

			$("body").find(".dialogEditFormTable").addClass('animated slideInUp');
		},
		//弹出编辑层事件处理
		bindDialogEvent:function(_setting){
			var pThis = this,target = _setting.target;
			//附件处理
			pThis.bindUploadFileDom(_setting);

			//hash处理
			$(window).on("hashchange",function(){
				var hash = window.location.hash;

				if(hash != "#" + pThis.editFormTableHash){
					target.parent().addClass('hide');
				}else{
					target.parent().removeClass('hide');
				}
			});
		},
		//底部保存按钮
		addEditFormTableBtn:function(target,_setting,data){
			var pThis = this;
			var menu = new Array();
			menu.push({
				width: 100,
				name: "保存",
				iconClass: "",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					if(obj.hasClass('disabled')){return;}

					//校验所有控件
					if(!common.checkAllCtrl(target)){
						return;
					}

					var allData = common.getCtrlData(target);

					//_setting.target.find(".m");
					console.log(allData);

					data.files = allData.files;
					data.filesId = "";

					var json = {
						_setting:_setting,
						data:data,
						form_vals:allData.form_vals,
						params:{
							scCtrlState: 1
						},
						listType:1,
						activekey:0,
						appendTarget:_setting.target.find("#scCtrlContent")
					};

					pThis.appendScCtrlToHtml(json);

					//-------特殊处理-------
					otherProcess2.init({
						is_other_proc:_setting.isOtherProc,
						target:_setting.target,
						data:data,
						listType:json.listType,
						userId:_setting.createId,
						formsetinstId:_setting.formsetinstId,
						state:_setting.state,
						workitemId:_setting.workitemId
					});

					common.setSaveOrEditUrl(otherProcess2.getSaveOrEditUrl(_setting.isOtherProc,common.getSaveOrEditUrl()));

					//审批页面 编辑按钮
					pThis.appendFormTableEditBtn(json.appendTarget,data,_setting,allData.form_vals);

					window.history.go(-1);
				}
			});
				
			return UiFramework.wxBottomMenu.init({
				target:target.find(".mContent"),
				noFixed:1,
				menu: menu
			});
		},
		//组装控件
		appendScCtrlToHtml:function(json){
			var _setting = json._setting, //表单配置
				data = json.data, //表单数据
				form_vals = json.form_vals, //控件数据
				params = json.params || {}, //控件配置
				listType = json.listType,//当前列表类型
				activekey =  json.activekey,//当前类型列表分类
				appendTarget = json.appendTarget; //当前控件插入的对象 

			//清空
			appendTarget.html("");

			//非附件处理
			for(var k in form_vals){
				var d = form_vals[k];

				//人员部门选择器
				if(d.type == "people" || d.type == "dept"){
					appendTarget.append(ScFormCtrl.getCtrlExtendUserDept(d,userInfo,params));
				}
				//时间控件
				else if(d.type == "date"){
					var dateParams = null;

					if((_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3) && _setting.isStart == 1){
						dateParams = {
							scCtrlState:params.scCtrlState,
							afterEvent:function(data,obj){
								otherProcess2.afterEvent({
									data:data,
									target:obj
								});

								if(params.afterEvent){
									params.afterEvent();
								}
							}
						};
					}else{
						dateParams = params;
					}

					appendTarget.append(ScFormCtrl.getCtrlExtendDatetime(d,dateParams));
				}
				//子表单
				else if(d.type == "table"){
					appendTarget.append(ScFormCtrl.getCtrlSublist(d,params));
				}
				//图片控件
				else if(d.type == "picture"){
					appendTarget.append(ScFormCtrl.getCtrlExtendPics(d,params));
				}
				//基础控件
				else{
					var commonParams = {};
					//补录时间问题
					if(listType == 1 && _setting.isOtherProc == 3 && d.type == "select" && d.edit_input == 1){
						commonParams.afterEvent = function(data,obj){
							otherProcess2.afterEvent({
								data:data,
								target:obj
							});

							if(params.afterEvent){
								params.afterEvent();
							}
						};
					}else{
						commonParams = params
					}

					appendTarget.append(ScFormCtrl(d,commonParams));
				}
			};

			//附件处理
			try{
				if(listType != 3){
					data.file_type = data.workitem.file_type;

					if(data.workitem.file_type != 0){
						appendTarget.append(ScFormCtrl.getCtrlExtendFiles(data,params));
					}else if(data.workitem.file_type == 0){
						if(data.files && data.files.length > 0){
							var scCtrlFilesDom = ScFormCtrl.getCtrlExtendFiles(data,params);
							if(scCtrlFilesDom){appendTarget.append(scCtrlFilesDom)};
						}
					}
				}
				//知会
				else{
					if(data.files.length > 0){
						appendTarget.append(ScFormCtrl.getCtrlExtendFiles(data,params));
					}
				}

			}catch(e){
				console.log(e);
			}		
		},
		/*修改已阅读的数量*/
		seeCount:function(notifys){
			var count = 0;
			for(var i in notifys){
				if(notifys[i].state == 2){
					count ++ ;
				}
			}
			return count;
		}
	};

	return detail;
});