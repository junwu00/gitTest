require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	//"process:static/Contents/mv_pic/GooFlow",
	//"process:static/Contents/mv_pic/default",
	"process:static/Scripts/mv_pic/GooFunc",
	"process:static/Scripts/mv_pic/json2",
	"process:static/Scripts/mv_pic/GooFlow"
],function($,t,UiFramework) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var processPicUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=109";

	//jqObj对象
	var container = $("#main-container"),
		formsetinstId = t.getHrefParamVal("formsetinstId") || 1;

	t.ajaxJson({
		url:processPicUrl,
		data:{
			"data":{
				"formsetinst_id":formsetinstId
			}
		},
		callback:function(result,status){
			if(result.errcode !=0){
				layerPackage.fail_screen(result.errmsg);
				return;
			}

			container.find(".hide").removeClass('hide');

			var property={
				toolBtns:[],
				haveHead:false,
				headBtns:[],//如果haveHead=true，则定义HEAD区的按钮
				haveTool:false,
				haveGroup:false,
			};

			var demo;
			var data = result.info;
			var jsondata= $.parseJSON(data.work_pic);
			var json_done =$.parseJSON(data.worknodes_arr);

			container.find(".formsetinst_name").text(data.formsetinst_name);

			var state = "";
			if(data.state == 1){
				state = "运行中"
			}else if(data.state == 2){
				state = "已结束"
			}else if(data.state == 3){
				state = "被终止"
			}

			container.find(".state").text(state);
			
			//**获取节点需要最大的高度和宽度，计算div宽度和高度
			for(var i in jsondata.nodes){
				var node_json = jsondata.nodes[i];
				if(typeof(property.divwidth) == "undefined"||property.divwidth<node_json.left){
					property.divwidth = node_json.left;
				}
				if(typeof(property.divheight) == "undefined"||property.divheight<node_json.top){
					property.divheight = node_json.top;
				}
				if(typeof(property.left) == "undefined"||property.left>node_json.left){
					property.left = node_json.left;
				}
				if(typeof(property.top) == "undefined"||property.top>node_json.top){
					property.top = node_json.top;
				}
			}
			
			property.divwidth = property.divwidth+150-property.left;
			
			property.divheight = property.divheight+80-property.top;
			
			demo=$.createGooFlow($("#demo"),property);
			//demo.onItemDel=function(id,type){
			//	return confirm("确定要删除该单元吗?");
			//}
			
			demo.loadData(jsondata,json_done);

			UiFramework.bottomInfo.init(container);

			layerPackage.unlock_screen();
		}
	});
});