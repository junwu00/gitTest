// process_common
define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/processCommonUI.html',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'common:widget/lib/calc',
	'common:widget/lib/text!'+buildView+'/process/Templates/tableComponents.html',
	'process:static/Scripts/otherProcess',
	'common:widget/lib/storage'
], function($, t, UiFramework, tpl, uiTemplate, depts_users_select,calc,tableTemplate,otherProcess) {
	var layerPackage = UiFramework.layerPackage(),
	base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
	dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
	user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
	user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
	init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
	nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
	saveOrEditUrl = "index.php?model=process&m=ajax&a=index&cmd=105",
	sendUrl = "index.php?model=process&m=ajax&a=index&cmd=106";
	
	// mrc add
	var tag_list = {}
	var wuzhi_list = {}
	var get_wuzhi_data_history = {}
	$.ajax({
		async: false,
		url: './wx/index.php?model=process&m=ajax&cmd=125',
		type: "post",
		dataType: "json",
		success : function(res) {
			if (res.errcode != 0) {
				layerPackage.tips(res.errmsg, 0);
				return false
			}
			tag_list = res.allTag
			for (var index in res.data) {
				wuzhi_list[res.data[index].id] = res.data[index]
			} 
		} 
	})

	// mrc add end
	
	var isIndex = t.getUrlParam("isIndex") || "",
		menuType = t.getUrlParam("menuType") || 1,
		hash = window.location.hash;

	if(isIndex){
		isIndex = "&isIndex="+isIndex;
	}

	if(menuType){
		menuType = "&menuType="+menuType;
	}

	var process_common = {
		money_type:["人民币","美元","欧元","港币"],
		money_type_icon:["¥","$","€","HK$"],
		//default默认 email邮箱地址 phone电话 telephone手机
		default_text_format : "default",
		number_text_format : "number",
		email_text_format : "email",
		phone_text_format : "phone",
		telephone_text_format : "telephone",
		uploadFileObj:null,//上传附件对象
		init:function(container){
			if(!t.checkJqObj(container)){layerPackage.fail_screen("error");return;}

			this.container = container;
			var tableDiv = $("#table_add_div");

			if(tableDiv.length == 0){
				container.parent().append('<div id="table_add_div" class="css_table_add_div"></div>');
				tableDiv = container.parent().find("#table_add_div");
			}

			this.tableDiv = tableDiv;
		},
		//获取当前用户信息 模块
		getHeaderPart: function(data) {
			var dom = tpl(uiTemplate, {
				headerPart: {
					data:data
				}
			});

			var o = $($.trim(dom));

			if(o.find("#header_input").length > 0){
				this.headerInput = true;
			}

			return o;
		},
		getFormInfo:function(data){
			data.form_describe = this.checkStr(data.form_describe);

			tpl.register('checkNewline', t.checkNewline);

			var dom = tpl(uiTemplate, {
				formInfo:data
			});
			tpl.unregister('checkNewline');
			return $($.trim(dom));
		},
		checkStr:function(str){
			if($.trim(str) == ""){
				return "";
			}

			return str;
		},
		getFormPart: function(dataObj,isTable) {
			if(dataObj.val === undefined){
				//dataObj.val = "";
			}
			tpl.register('checkVal', t.checkVal);
			tpl.register('replaceStr', t.replaceStr);
			tpl.register('getArrLastVal', this.getArrLastVal);
			tpl.register('getDateTimeLocal', t.getDateTimeLocal);
			tpl.register('getNowFormatDate', t.getNowFormatDate);
			var dom = tpl(uiTemplate, {
				data: dataObj,
				isTable:isTable,
				money_type:this.money_type,
				tag_list: tag_list,
				s_width : document.body.scrollWidth
			});
			tpl.unregister('checkVal');
			tpl.unregister('replaceStr');
			tpl.unregister('getArrLastVal');
			tpl.unregister('getDateTimeLocal');
			tpl.unregister('getNowFormatDate');
			//数据保存
			var o = $($.trim(dom));
			o.data("cacheData", dataObj);

			//对应的事件绑定
			if(dataObj.type == "money"){
				this.moneyEvent(o);
			}else if(dataObj.type == "radio" || dataObj.type == "checkbox"){
				this.radioCheckBoxEvent(o);
			}else if(dataObj.type == "table"){
				this.tableEvent(o);
			}else if(dataObj.type == "people" || dataObj.type == "dept"){
				this.userSelectEvent(o);
			}else if(dataObj.type == "wuzhi_chose"){  // mrc add
				this.init_wuzhi_chose(o)
			}else if(dataObj.type == "wuzhi_attr"){  // mrc add
				this.init_wuzhi_attr(o)
			}
			return o;
		},
		// mrc add
		init_wuzhi_chose: function(jqObj) {
			var that = this
			var data = jqObj.data("cacheData")
			var tag_id = data.cat && data.cat.id ? [data.cat.id] : []
			if (tag_id && tag_id.length > 0) {
				that._load_wuzhi(jqObj, tag_id)
			}
			jqObj.find('.cat_select').on('change', function(){
				var val = $(this).val()
				that._load_wuzhi(jqObj, [val])
			})
			jqObj.find('.val_select').on('change', function(){
				var val = $(this).val()
				var data = jqObj.data("cacheData")
				data.value = {
					id: jqObj.find('.val_select').val(),
					name: jqObj.find('.val_select option:selected').html(),
					history_id: jqObj.find('.val_select option:selected').attr('data-history_id')
				}
				data.val = data.value.name
				jqObj.data("cacheData", data)
				console.log(data)
				that.init_wuzhi_link_attr()
			})
		},
		init_wuzhi_attr: function(jqObj) {
			var that = this
			var data = jqObj.data("cacheData")
			jqObj.on('change', 'select', function(){
				data.value = $(this).val()
				data.val = $(this).val()
				jqObj.data("cacheData", data)
			})
			jqObj.on('input', 'input', function(){
				data.value = $(this).val()
				data.val = $(this).val()
				jqObj.data("cacheData", data)
			})
		},
		_load_wuzhi: function(jqObj, tag_id) {
			var that = this
			var data = jqObj.data("cacheData")
			layerPackage.lock_screen()
			$.ajax({
				async: true,
				data: {
					data: {
						tag_id: tag_id,
						limit: 999,
					}
				},
				url: './wx/index.php?model=process&m=ajax&cmd=125',
				type: "post",
				dataType: "json",
				success : function(res) {
					if (res.errcode != 0) {
						layerPackage.tips(res.errmsg, 0);
						return false
					}
					var html = ''
					var res_data = res.data;
					html += '<option value="-1">请选择</option>'
					for (var index in res_data) {
						var selected = data.value && data.value.history_id == res_data[index].history_id && tag_id[0] == data.cat.id ? 'selected' : ''
						html += '<option value="'+res_data[index].id+'" data-history_id="'+res_data[index].history_id+'" '+selected+'>'+res_data[index].name+'</option>'
					}
					jqObj.find('.val_select_div select').html(html)
					console.log(1)
					data.cat = {
						id: jqObj.find('.cat_select').val(),
						name: jqObj.find('.cat_select option:selected').html()
					}
					data.value = {
						id: jqObj.find('.val_select').val(),
						name: jqObj.find('.val_select option:selected').html(),
						history_id: jqObj.find('.val_select option:selected').attr('data-history_id')
					}
					data.val = data.value.name
					jqObj.data("cacheData", data)
					that.init_wuzhi_link_attr()
					layerPackage.unlock_screen()
				} 
			})
		},
		init_wuzhi_link_attr: function(input_key) {
			var that = this
			var wuzhi_chose_arr = []
			var cond = input_key ? '[input_key='+input_key+']' : ''
			var is_detail_edit = cond ? 1 : 0
			$('div.js_inputs[data-type=wuzhi_attr]' + cond).each(function(){
				var this_target_attr = $(this)
				var c_data_1 = $(this).data('cacheData') 
				console.log(c_data_1)
				var th_cond = ''
				if(c_data_1.th_key) {th_cond = '#table_add_div '}
				$(th_cond + 'div.js_inputs[data-type=wuzhi_chose]').each(function(){ 
					var c_data_2 = $(this).data('cacheData') 
					console.log(c_data_2)
					if (c_data_1.rule && c_data_1.rule.split('.').length > 0) { // 是否设置规则
						var c_data_1_rule_arr = c_data_1.rule.split('.')
						if (c_data_1_rule_arr[0] && c_data_1_rule_arr[0] == c_data_2.name) {
							if (c_data_2.value && c_data_2.value.id) { 
								if(c_data_2.value.id != -1) {
									var this_wuzhi_data = that.get_wuzhi_data(c_data_2.value.id) // 获取对应物资
											attrs = this_wuzhi_data && this_wuzhi_data.inputs

									for (var key in attrs) { // 所有的attrs
										if(c_data_1_rule_arr[1] && c_data_1_rule_arr[1] == attrs[key].name) {
											var html = ''
											if (attrs[key].type.indexOf('select') != -1) {
												var opts = attrs[key].opts.split(',')
												var opts_nums = attrs[key].opts_nums.split(',')
												html += '<select name="" id="" class="js_input_val  css_input_select">'
												// html += '<option value="">请选择</option>'
												// var default_val = c_data_1.value ? c_data_1.value : attrs[key].default_val ? opts[attrs[key].default_val] : opts[0] 
												var default_index = attrs[key].default_val ? opts_nums.indexOf(attrs[key].default_val) : 0
												var default_val = opts[default_index]
												for (var opt_index in opts) {
													var selected = default_val == opts[opt_index] || (!default_val && opt_index == 0) ? 'selected' : ''
													html += '<option value="' + opts[opt_index] + '" '+selected+'>' + opts[opt_index] + '</option>'
												}
												html += '</select>'
												html += '<span class="right_"></span>'
												c_data_1.val = c_data_1.value = default_val || ''
											} else if (attrs[key].type == 'text'){
												// var t_val = c_data_1.value ? c_data_1.value : attrs[key].the_value ? attrs[key].the_value : ''
												var t_val = attrs[key].the_value ? attrs[key].the_value : ''
												html += '<input type="text" style="text-align: right;" value="'+ t_val +'">'
												c_data_1.val = c_data_1.value = t_val
											}
											if (!is_detail_edit) {
												$('.js_inputs.css_inputs[data-key='+c_data_1.input_key+']').data('cacheData', c_data_1)
												this_target_attr.find('.css_input_info').html(html)
											} else {
												$('#table_add_div .css_edit_input_info').html(html)
											}
											break;
										} else { // 没有对应字段的时候显示无
											if (!is_detail_edit) {
												this_target_attr.find('.css_input_info').html('<input type="text" style="text-align: right;" value="无" disabled>')
											} else {
												$('#table_add_div .css_edit_input_info').html('<input type="text" style="text-align: right;" value="无" disabled>')
											}
										}
									}
								} else {
									if (!is_detail_edit) {
										this_target_attr.find('.css_input_info').html('<input type="text" style="text-align: right;" value="无" disabled>')
									} else {
										$('#table_add_div .css_edit_input_info').html('<input type="text" style="text-align: right;" value="无" disabled>')
									}
								}
							}
							return false // break
						}
					}
				})
			})
		},
		get_wuzhi_data: function(wuzhi_id) {
			return wuzhi_list[wuzhi_id]
			// if (get_wuzhi_data_history[wuzhi_id]) { return get_wuzhi_data_history[wuzhi_id]}
			// var data = {}
			// $.ajax({
			// 	async: false,
			// 	url: './wx/index.php?model=process&m=ajax&cmd=125',
			// 	data: {
			// 		data: {
			// 			id: wuzhi_id,
			// 			limit: 999,
			// 		}
			// 	},
			// 	type: "post",
			// 	dataType: "json",
			// 	success : function(res) {
			// 		if (res.errcode != 0) {
			// 			layerPackage.tips(res.errmsg, 0);
			// 			return false
			// 		}
			// 		data = res.data
			// 		get_wuzhi_data_history[wuzhi_id] = data
			// 	} 
			// })
			// return data
		},
		// mrc add end
		getOtherPart:function(data,callback){
			var dom = tpl(uiTemplate, {
				other:data
			});

			var o = $($.trim(dom));

			this.otherEvent(o,data,callback);

			return o;
		},
		moneyEvent:function(jqObj){
			jqObj.on("input",".js_money_input",function(){		
				var val = Number($(this).val());
				if(val != 0 && !t.regCheck(val,'money')){
					if(this.value.indexOf('.')>0){
						this.value = Number(Number(this.value).toFixed(2));

						if(this.value.toString().length >=13 && this.value > 999999999999){
							this.value = "999999999999.99";
						}
					}else if(val.toString()=='NaN' || val.toString()=='Infinity'){
						this.value = '';
					}else{
						layerPackage.fail_screen('超出数值最大范围!');
						this.value = this.getAttribute('tmpvalue');
						return;
					}
				}
				this.setAttribute('tmpvalue',this.value);
				if($(this).val()==""||$(this).val()==0){
					$(this).parent().parent().find(".js_capitalized_input").html("");
				}
				else{
					var p_div = $(this).parent();
					var capitalized = p_div.attr("data-capitalized");
					var edit = p_div.attr("data-edit");
					var dx = $(this).parent().parent().find(".js_capitalized_input");

					if(capitalized && edit){
						var dx_str = t.DX(this.value);
						if(dx.length == 0){
							p_div.append('<br/>(<span class="js_input_val js_capitalized_input"  style="width: 20%" title="'
							+dx_str+'" >'+dx_str+'</span>)');
						}else{
							dx.html(t.DX(this.value));
						}
					}else{
						dx.html(t.DX(this.value));
					}
				}
			});
		},
		radioCheckBoxEvent:function(jqObj){
			jqObj.on("click",'.js_other_cb', function() {
				if ($(this).is(':checked') == true) {
					$(this).parent().parent().find(".js_other_text").css("display", "");
				} else {
					$(this).parent().parent().find(".js_other_text").val('');
					$(this).parent().parent().find(".js_other_text").css("display", "none");
				}
			});
			jqObj.on("click",".js_opts_radio", function() {
				$(this).parent().parent().parent().find(".js_other_text").val('');
				$(this).parent().parent().parent().find(".js_other_text").css("display", "none");
			});
		},
		tableEvent:function(jqObj,otherParams){
			var pThis = this;
			jqObj.find(".js_detail_add").on('click', function() {
				var input_key = $(this).parent().attr("input_key");
				//get_table_td_edit(input_key, -1)
				//alert("test");
				var table_obj = jqObj.data("cacheData");

				if(otherParams && otherParams.isDeal){
					otherParams.jqObj = jqObj;
				}
				pThis.tableEditEvent(table_obj,-1,otherParams);
			});
		},
		tableEditEvent:function(table_obj,row_num,otherParams){
			var input_key = table_obj.input_key;
			var	table_arr = {};
			var size = 0;
			var pThis = this;

			if(otherParams && otherParams.isDetail){
				if(table_obj.rec === undefined){
					table_arr = table_obj.table_arr;
				}else if(table_obj.rec[row_num] === undefined){
					table_arr = table_obj.table_arr;
					size = table_obj.rec.length;
				}else{
					var true_table_arr = table_obj.table_arr;
					table_arr = table_obj.rec[row_num];
					//更新控件权限
					$.each(true_table_arr,function(key,true_th_obj){
						if(table_arr[key]!==undefined){
							table_arr[key]["edit_input"] = true_th_obj.edit_input;
							table_arr[key]["visit_input"] = true_th_obj.visit_input;
						}
					});
					size = row_num;
				}
			}else{
				//创建的时候
				if(table_obj.rec === undefined){
					table_arr = $.extend(true, {}, table_obj.table_arr);
				}else if(table_obj.rec[row_num] === undefined){
					table_arr = $.extend(true, {}, table_obj.table_arr);
					size = table_obj.rec.length;
				}else{
					table_arr = table_obj.rec[row_num];
					size = row_num;
				}
			}
			//console.log(table_obj);
			//console.log(table_arr);
			//<div id="table_add_div" class="css_table_add_div"></div>
			var tableDiv = this.tableDiv;

			tableDiv.html("").append(pThis.getTableComponents(input_key,row_num,table_obj,size));
			tableDiv.data("cacheData",table_obj);

			tableDiv.on("click",".removeDiv",function(){
				pThis.cancelDiv(function(){
					//pThis.set_table_html(table_input_key);
				});
			});

			$.each(table_arr,function(key,th_obj){
				th_obj.describe = "";
				tableDiv.find(".detailed").append(pThis.getFormPart(th_obj,1));
			});
			var menu = new Array(),
			save_menu = {
				width: 100,
				name: "保存",
				iconClass: "icon-save",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					pThis.saveDiv(table_obj,otherParams);
				}
			};

			if(row_num==-1){//新建
				menu.push(save_menu);
			}else{
				var isEdit = $("div[input_key="+input_key+"]").attr("isedit");

				if(!isEdit){
					save_menu.width = 50;
					var remove_menu = {
						width: 50,
						name: "删除",
						iconClass: "icon-trash",
						btnColor: "r",
						id: null,
						click: function(self, obj, index) {
							layerPackage.ew_confirm({
								title:"确定要删除明细？",
								ok_callback:function(){
									var table_input_key = tableDiv.find("#table_input_key").val();
									var table_edit_num = tableDiv.find("#table_edit_num").val();
									var rec = table_obj.rec;
									rec.splice(table_edit_num,1);
									pThis.cancelDiv(function(){
										pThis.set_table_html(table_obj,table_input_key,otherParams);
										pThis.detailSave(otherParams);
									});
								}
							});
						}
					};

					menu.push(remove_menu);
				}

				menu.push(save_menu);
			}

			UiFramework.wxBottomMenu.init({
				target:tableDiv,
				menu: menu
			});

			var height = this.container.height();
			tableDiv.css("top",height);
			//tableDiv.html(htmlstr);

			// mrc remove ani
			// tableDiv.animate({top:'0px'},"fast",function(){
			// 	pThis.container.css("display","none");
			// 	scroll(0,0);
			// });
			tableDiv.css('top', '0')
			pThis.container.css("display","none")
			scroll(0,0)
			// mrc add end

			var obj = calc.returnTrClassObj(tableDiv);

			calc.bindObjInputClick(obj,1);
			pThis.init_muselect_box($('#table_add_div .muselect_box'));
		},
		getTableComponents:function(input_key,row_num,table_obj,size,inputEdit){
			tpl.register('checkVal', t.checkVal);
			tpl.register('replaceStr', t.replaceStr);
			var dom = tpl(tableTemplate, {
				input_key:input_key,
				row_num:row_num,
				table_obj:table_obj,
				size:size,
				inputEdit:inputEdit
			});
			tpl.unregister('checkVal');
			tpl.unregister('replaceStr');
			//数据保存
			var o = $($.trim(dom));

			return o;
		},
		cancelDiv:function(func){
			var input_key = this.tableDiv.find("#input_key").val();
			var table_input_key = this.tableDiv.find("#table_input_key").val(),
			pThis = this;
			this.container.css("display","");

			if(!table_input_key){
				table_input_key = input_key;
			}
			// mrc remove ani
			this.tableDiv.css({top:'100%'})
			pThis.tableDiv.html('');
			if($("div[input_key="+table_input_key+"]").offset()){
				$(window).scrollTop($("div[input_key="+table_input_key+"]").offset().top);
			}
			if(func!==undefined){
				func();
			}
			// mrc remove ani
		},
		saveDiv:function(table_obj,otherParams,callback){
			var tableDiv = this.tableDiv;
			var pThis = this;
			var input_key = tableDiv.find("#input_key").val();
			var table_input_key = tableDiv.find("#table_input_key").val();
			var table_edit_num = tableDiv.find("#table_edit_num").val();
			if(table_input_key !== undefined){
				//var table_obj = tableDiv.data("cacheData");
				var table_arr = table_obj.table_arr;

				//console.log(table_obj);
				var temp_obj = undefined;
				if(table_edit_num == -1){
					temp_obj = $.extend(true, {}, table_obj.table_arr);
				}else{
					temp_obj = table_obj.rec[table_edit_num];
					if(temp_obj === undefined){
						temp_obj = $.extend(true, {}, table_obj.table_arr);
					}
				}

				$.each(temp_obj,function(th_key,th_obj){
					var th_key = th_obj.th_key;
					var th_type = th_obj.type;

					if(otherParams && otherParams.isDetail){
						var th_edit_input = table_arr[th_key].edit_input;
						var th_visit_input = table_arr[th_key].visit_input===undefined?1:table_arr[th_key].visit_input;
					}

					if(th_type == "text"){
						var th_val = tableDiv.find("input[th_key="+th_key+"]").val();
						th_obj.val = th_val;
					}else if(th_type == "date"){
						var th_val = tableDiv.find("input[th_key="+th_key+"]").val();

						/*针对苹果机器上时间问题*/
						if($.trim(th_val) && th_val.length > 0 && th_val.match(/:/g) && th_val.match(/:/g).length > 1){
							th_val = th_val.substring(0,th_val.lastIndexOf(":"));
						}

						th_val = th_val.replace('T',' ');

						th_obj.val = th_val;
					}else if(th_type == "textarea"){
						var th_val = tableDiv.find("textarea[th_key="+th_key+"]").val();
						th_obj.val = th_val;
					}
					else if(th_type == "select"){
						var th_val = tableDiv.find("select[th_key="+th_key+"]").find("option:selected").val();
						th_obj.val = th_val;
					}
					else if(th_type == "radio" || th_type == "checkbox"){
						var th_val = "";
						var th_c_other = 0;
						var arr = new Array();
						tableDiv.find("input[th_key=" + th_key + "]:checked").each(function() {
							if (th_type == "radio" && $(this).hasClass("js_opts_radio")) {
								arr.push($(this).val());
							}
							if (th_type == "checkbox" && $(this).hasClass("js_opts_checkbox")) {
								arr.push($(this).val());
							}
							if ($(this).hasClass("js_other_cb")) {
								arr.push($(this).parent().parent().find(".js_other_text").val());
								th_c_other = 1;
							}
						});
						if (arr.length > 0) {
							th_val = arr.join(";");
						}
						th_obj.val = th_val;
						th_obj.c_other = th_c_other;
					}
					else if(th_type == "money"){
						var th_val = tableDiv.find("input[th_key="+th_key+"]").val();
						th_obj.val = th_val;
						if(th_obj.capitalized==1){
							th_obj.capitalized_val = tableDiv.find("input[th_key="+th_key+"]").parent().parent().find(".js_capitalized_input").html();
						}
					}
					else if(th_type == "people" || th_type == "dept"){
						var th_input_obj = tableDiv.find("div[th_key="+th_key+"]").parents(".js_inputs");
						selectObj = th_input_obj.data("selectObj");

						//th_obj.val = selectObj.get_depts_users_names();
						//th_obj.ids = selectObj.get_depts_users_ids();

						var arr = selectObj.get_depts_users();
						var aId = new Array(),
							aName = new Array(),
							aPic = new Array();
						for (var i = 0; i < arr.length; i++) {
							aId.push(arr[i].id);
							aName.push(arr[i].name);
							if(arr[i].pic_url != "undefined" || !arr[i].pic_url){
								aPic.push(arr[i].pic_url);
							}
						};

						th_obj.val = aName.join(",");
						th_obj.ids = aId.join(",");
						th_obj.imgs = aPic.join(",");

					}else if(th_type == "muselect"){ // mrc add
						// var obj_val = tableDiv.find("select[input_key="+obj_key+"]").val();
						th_obj.opts = [];

						var val = '',
							arr = [];

						tableDiv.find('.s_box').each(function(){
							arr.push($(this).val());
							val += $(this).find('option[value='+$(this).val()+']').html() + ' ';
						});	
						th_obj.opts = arr;
						th_obj.val = val;
					}else if(th_type == "conflict"){
						var st_time = tableDiv.find("input[th_key="+th_key+"]:eq(0)").val(),
							ed_time = tableDiv.find("input[th_key="+th_key+"]:eq(1)").val();
						/*针对苹果机器上时间问题*/
						if($.trim(st_time) && st_time.length > 0 && st_time.match(/:/g) && st_time.match(/:/g).length > 1){
							st_time = st_time.substring(0,st_time.lastIndexOf(":"));
						}
						if($.trim(ed_time) && ed_time.length > 0 && ed_time.match(/:/g) && ed_time.match(/:/g).length > 1){
							ed_time = ed_time.substring(0,ed_time.lastIndexOf(":"));
						}
						st_time = st_time.replace('T',' ');
						ed_time = ed_time.replace('T',' ');
						var obj_val = st_time + ' - ' + ed_time;
						th_obj.val = obj_val;

						var select_val = tableDiv.find("input[th_key="+th_key+"]").parent().parent().find("select").val();
						th_obj.conflict_type = select_val;
					}else if(th_type == "wuzhi_chose") {
						th_obj.cat = {
							id: tableDiv.find("select.cat_select[th_key="+th_key+"]").val(),
							name: tableDiv.find("select.cat_select[th_key="+th_key+"] option:selected").html()
						}
						th_obj.value = {
							id: tableDiv.find("select.val_select[th_key="+th_key+"]").val(),
							name: tableDiv.find("select.val_select[th_key="+th_key+"] option:selected").html(),
							history_id: tableDiv.find('select.val_select[th_key='+th_key+'] option:selected').attr('data-history_id')
						}
						th_obj.val = th_obj.value.name
					}else if(th_type == "wuzhi_attr") {
						if (tableDiv.find('div[data-type=wuzhi_attr] div[data-th-key='+th_obj.th_key+'] select').length == 1) {
							th_obj.value = tableDiv.find('div[data-type=wuzhi_attr] div[data-th-key='+th_obj.th_key+'] select').val()
						} else if (th_obj.value = tableDiv.find('div[data-type=wuzhi_attr] div[data-th-key='+th_obj.th_key+'] input').length == 1) {
							th_obj.value = th_obj.value = tableDiv.find('div[data-type=wuzhi_attr] div[data-th-key='+th_obj.th_key+'] input').val()
						}
						th_obj.val = th_obj.value
					}
					// mrc add end
				});

				if(!this.checkTable(temp_obj,table_obj)){
					return false;
				}

				if(table_edit_num ==-1){//新增
					if(table_obj.rec===undefined){
						table_obj.rec = new Array();
					}
					table_obj.rec.push(temp_obj);
				}
				else{
					table_obj.rec[table_edit_num]=temp_obj;
				}
				this.cancelDiv(function(){
					pThis.set_table_html(table_obj,table_input_key,otherParams);
					pThis.detailSave(otherParams);

					if(callback && t.isFunction(callback)){
						callback();
					}
				});
			}else{
				var obj = table_obj;

				var temp_obj = $.extend(true, {}, obj);
				var obj_type = temp_obj.type;
				var obj_key = temp_obj.input_key;
				var edit_input = temp_obj.edit_input;

				if(obj_type=="text"){
					var obj_val = tableDiv.find("input[input_key="+obj_key+"]").val();
					temp_obj.val = obj_val;
				}
				else if(obj_type=="textarea"&&edit_input==1){
					var obj_val = tableDiv.find("textarea[input_key="+obj_key+"]").val();
					temp_obj.val = obj_val;
				}
				else if(obj_type=="date"&&edit_input==1){
					var obj_val = tableDiv.find("input[input_key="+obj_key+"]").val();

					/*针对苹果机器上时间问题*/
					if($.trim(obj_val) && obj_val.length > 0 && obj_val.match(/:/g) && obj_val.match(/:/g).length > 1){
						obj_val = obj_val.substring(0,obj_val.lastIndexOf(":"));
					}

					obj_val = obj_val.replace('T',' ');

					temp_obj.val = obj_val;
				}
				// mrc add
				else if(obj_type=="conflict"&&edit_input==1){
					var st_time = tableDiv.find("input[input_key="+obj_key+"]:eq(0)").val(),
						ed_time = tableDiv.find("input[input_key="+obj_key+"]:eq(1)").val();

					/*针对苹果机器上时间问题*/
					if($.trim(st_time) && st_time.length > 0 && st_time.match(/:/g) && st_time.match(/:/g).length > 1){
						st_time = st_time.substring(0,st_time.lastIndexOf(":"));
					}
					if($.trim(ed_time) && ed_time.length > 0 && ed_time.match(/:/g) && ed_time.match(/:/g).length > 1){
						ed_time = ed_time.substring(0,ed_time.lastIndexOf(":"));
					}
					st_time = st_time.replace('T',' ');
					ed_time = ed_time.replace('T',' ');
					var obj_val = st_time + ' - ' + ed_time;
					temp_obj.val = obj_val;

					var select_val = tableDiv.find("select").val();
					temp_obj.conflict_type = select_val;
					// mrc add end
				}
				else if(obj_type=="select"&&edit_input==1){
					var obj_val = tableDiv.find("select[input_key="+obj_key+"]").val();
					temp_obj.val = obj_val;
				}
				else if(obj_type=="muselect"&&edit_input==1){
					// var obj_val = tableDiv.find("select[input_key="+obj_key+"]").val();
					temp_obj.opts = [];

					var val = '',
						arr = [];

					tableDiv.find('.s_box').each(function(){
						arr.push($(this).val());
						val += $(this).find('option[value='+$(this).val()+']').html() + ' ';
					});	

					temp_obj.opts = arr;
					temp_obj.val = val;
				}
				else if((obj_type=="radio"&&edit_input==1) || (obj_type == "checkbox" && edit_input == 1)){
					var obj_val = "";
					var obj_c_other = 0;
					tableDiv.find("input[input_key=" + obj_key + "]:checked").each(function() {
						if (obj_type=="radio" && $(this).hasClass("js_opts_radio")) {
							obj_val += $(this).val() + ";";
						}
						if (obj_type=="checkbox" && $(this).hasClass("js_opts_checkbox")) {
							obj_val += $(this).val() + ";";
						}
						if ($(this).hasClass("js_other_cb")) {
							obj_val += $(this).parent().parent().find(".js_other_text").val() + ";";
							obj_c_other = 1;
						}
					});
					if (obj_val != "") {
						obj_val = obj_val.substring(0, obj_val.length - 1);
					}
					temp_obj.val = obj_val;
					temp_obj.c_other = obj_c_other;
				}
				else if (obj_type == "money" && edit_input == 1) {
					var obj_val = tableDiv.find("input[input_key=" + obj_key + "]").val();
					temp_obj.val = obj_val;

					if (obj.capitalized == 1) {
						temp_obj.capitalized_val = tableDiv.find("input[input_key=" + obj_key + "]").parent().parent().find(".js_capitalized_input").html();
					}
				}
				else if((obj_type == "people" && edit_input == 1)|| (obj_type == "dept" && edit_input == 1)){
					var th_input_obj = tableDiv.find("div[input_key="+obj_key+"]").parents(".js_inputs");
					selectObj = th_input_obj.data("selectObj");

					//temp_obj.val = selectObj.get_depts_users_names();
					//temp_obj.ids = selectObj.get_depts_users_ids();

					var arr = selectObj.get_depts_users();
					var aId = new Array(),
						aName = new Array(),
						aPic = new Array();
					for (var i = 0; i < arr.length; i++) {
						aId.push(arr[i].id);
						aName.push(arr[i].name);
						if(arr[i].pic_url != "undefined" || !arr[i].pic_url){
							aPic.push(arr[i].pic_url);
						}
					};

					temp_obj.val = aName.join(",");
					temp_obj.ids = aId.join(",");
					temp_obj.imgs = aPic.join(",");
				}
				// mrc add
				else if(obj_type == "wuzhi_chose" && edit_input == 1) {
					var that = this
					$('.js_inputs[data-type="wuzhi_attr"]').each(function(){
						var this_dom = $(this)
						var c_data = $(this).data('cacheData')
						var rule_arr = c_data.rule.split('.')
						if(rule_arr.length == 2 && rule_arr[0] && temp_obj.name == rule_arr[0]) {
							if(temp_obj.value.id == '-1'){ return true}
							var this_wuzhi_data = that.get_wuzhi_data(temp_obj.value.id) // 获取对应物资
							attrs = this_wuzhi_data && this_wuzhi_data[0].inputs
							for (var key in attrs) {
								if(rule_arr[1] && rule_arr[1] == attrs[key].name) {
									if (attrs[key].type.indexOf('select') != -1) {
										var opts = attrs[key].opts.split(',')
										var opts_nums = attrs[key].opts_nums.split(',')
										var default_index = attrs[key].default_val ? opts_nums.indexOf(attrs[key].default_val) : 0
										var default_val = attrs[key].default_val ? attrs[key].default_val : 0
										this_dom.find('label').html(opts[default_index])
										c_data.value = opts[default_index]
										c_data.val = opts[default_index]
										this_dom.data('cacheData', c_data)
									} else if (attrs[key].type == 'text'){
										this_dom.find('label').html(attrs[key].the_value)
										c_data.value = attrs[key].the_value
										c_data.val = attrs[key].the_value
										this_dom.data('cacheData', c_data)
									}
									break;
								}
							}
						}
					})
				}
				// mrc add
				// 
				if(!this.check_input(temp_obj)){
					return false;
				}
				otherParams.jqObj.data("cacheData",temp_obj);

				this.cancelDiv(function(){
					pThis.set_input_html(temp_obj,input_key,otherParams);
					pThis.detailSave(otherParams);

					if(callback && t.isFunction(callback)){
						callback();
					}
				});
			}
		},
		detailSave:function(otherParams){
			if(otherParams && otherParams.isDetail){
				this.get_finput_data();
				var data = otherParams.data;
				var params = {
					"form_id":data.formsetinst.form_id,
					"form_name":data.formsetinst.form_name,
					"work_id":data.formsetinst.work_id,
					"form_vals":this.getFormData(1),
					//Edit
					"workitem_id":data.workitem.id,
					"formsetInst_id":data.formsetinst.id,
					"judgement":"",
					"work_node_id":data.workitem.work_node_id
				};

				this.saveProcess(params,1,0); 
			}
		},
		userSelectEvent:function(jqObj){
			var data = jqObj.data("cacheData"),select = null,ids = [];
			//console.log(data.ids);
			//console.log(data.default_val);
			
			var default_type = data.default_type,
				default_val = data.default_val,
				type = data.type;

			try{
				if(default_type == 2 || default_type == 3 && default_val.length > 0){
					for (var i = 0; i < default_val.length; i++) {
						ids.push(default_val[i].id);
					};
				}else if(default_type == 1 && type == "people"){
					ids.push(userInfo.userId);
				}else if(default_type == 1 && type == "dept"){
					ids.push(userInfo.deptId);
				}
			}catch(e){
				console.error("get userInfo error!");
			}

			/*if(data.ids){
				if(t.isArray(data.ids)){
					ids = data.ids;
				}else{
					ids.push(data.ids);
				}
			}*/

			if(data.ids && data.ids.length > 0){
				if(typeof data.ids == "string"){
					var arr = data.ids.split(",");
					for (var i = 0; i < arr.length; i++) {
						ids.push(arr[i]);
					};
				}
				
				//ids = data.ids;
			}

			if(data.type == "people"){
				select = {"user":ids};
			}else if(data.type == "dept"){
				select = {"dept":ids};
			}

			var canEdit = null;
			if(data.edit_input === undefined){
				canEdit = 1;
			}else{
			 	canEdit = data.edit_input;
			}

			var selectObj = this.initUserDept(jqObj.find(".selectObj"),select,canEdit,data.select_max);

			jqObj.data("selectObj",selectObj);
		},
		otherEvent:function(jqObj,data,callback){
			var pThis = this;

			this.file_type = data.file_type;
			//绑定附件事件
			if(data.file_type != 0){
				this.bindUploadFile(jqObj,callback);
			}
		},
		bindUploadFile:function(jqObj,callback){
			var pThis = this;
			require(['common:widget/lib/GenericFramework'],function(GenericFramework){
				var uploadFileObj = GenericFramework.uploadFile({
					target:jqObj.find("#sign-upload")
				});

				jqObj.data("uploadFileObj",uploadFileObj);
				pThis.uploadFileObj = uploadFileObj;

				if(!callback){return;}
				callback(uploadFileObj);
			});
		},
		bindCalc:function(dataObj){
			calc.init(dataObj);
		},
		initUserDept:function (target,select,canEdit,max_select){
			if(!t.checkJqObj(target)){alert("error");return;}

			var opt = {};
			//opt.target = target;
			//opt.select = select;
			opt.target = target;
			opt.select=select;//{'user':select} || {}; //{'user':['80237','80225']};
			if(select.user){
				opt.type = 2;
			}else if(select.dept){
				opt.type = 1;
			}
			opt.max_select = max_select || 0;
			opt.base_dept_url = base_dept_url;
			opt.dept_url = dept_url;
			opt.user_url = user_url;
			opt.user_detail = user_detail;
			opt.init_selected_url = init_selected_url;
			opt.canEdit = canEdit;
			//ob.get_depts_users_ids()
			//ob.get_depts_users()
			var dept = new depts_users_select().init(opt)

			//if(!canEdit){dept.target.off('click');}

			return dept;
		},
		checkTable:function(items,table_obj){
			var is_check = true,pThis = this;

			$.each(items,function(key,item){
				if (table_obj.visit_input==1&&item.visit_input==1&&item.edit_input==1 && item.must == 1 && item.type != 'table' && !t.regCheck(item.val, 'notnull')) {
					layerPackage.fail_screen('请填写' + item.name);
					is_check = false;
					return false;
				}
				if(item.edit_input==1&&item.must==1&&item.type=="wuzhi_chose") {
					if(item.val == "" || item.val == "请选择" || !item.val){		
						layerPackage.fail_screen('请选择完整的' + item.name);
						is_check =  false;
						return false;
					}
				}
				if( item.type == 'text' && item.val!="" && item.format !=pThis.default_text_format ){
					if(item.format == pThis.number_text_format && !t.regCheck(item.val, 'double')){
						layerPackage.fail_screen(item.name+"需要填写数字");
						is_check = false;
						return false;
					}
					else if(item.format == pThis.email_text_format && !t.regCheck(item.val, 'email')){
						layerPackage.fail_screen(item.name+"需要填写邮箱");
						is_check = false;
						return false;
					}
					else if(item.format == pThis.phone_text_format && !t.regCheck(item.val, 'phone')){
						layerPackage.fail_screen(item.name+"需要填写电话号码");
						is_check = false;
						return false;
					}
					else if(item.format == pThis.telephone_text_format && !t.regCheck(item.val, 'mobile')){
						layerPackage.fail_screen(item.name+"需要填写手机号码");
						is_check = false;
						return false;
					}
					
				}
			});

			return is_check;
		},
		//插入table内容
		set_table_html:function(table_obj,table_input_key,otherParams){
			var table_html = $(".js_inputs").find("table[input_key="+table_input_key+"]");
			table_html.data("cacheData",table_obj);

			var pThis = this;
			var html = new Array();
			html.push("<tr>");

			$.each(table_obj.table_arr,function(key,th_obj){
				if(th_obj.visit_input===undefined||th_obj.visit_input==1){
					html.push('<th>'+th_obj["name"]);
					if(th_obj.edit_input==1&&th_obj.must==1){html.push('<em>*</em>');}
					if(th_obj.edit_input == 1){html.push('<i class="icon-edit fr c-green"></i>');}
					html.push('</th>');
				}
			});
			html.push("<td style='min-width:80px'></td>");
			html.push("</tr>");

			for (var i = 0; i < table_obj.rec.length; i++) {
				var items =  table_obj.rec[i];
				html.push("<tr class='js_tr'>");
				var table_arr = table_obj.table_arr;
				$.each(items,function(key,item){
					var th_obj = table_arr[key];
					if(otherParams && otherParams.isDetail){
						if(th_obj.visit_input===undefined||th_obj.visit_input==1){
							html.push("<td>");
						}else{
							html.push("<td class='css_not_visit'>");
						}

						if(item.type=="money"){
							if(item.val!=""){
								html.push(item.val+"("+pThis.money_type[item.money_type]+")");
							}
							html.push("<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/>");
							if(item.capitalized==1&&item.capitalized_val!=""){html.push("<br>("+item.capitalized_val+")");}
						}
						else if(item.type=="textarea"){
							//html.push(item.val.replace(/\n/g,'<br>'));
							html.push("<pre>"+pThis.encodeHtml(item.val)+"</pre>");
						}
						else if(item.type=="text"){
							html.push(item.val+"<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/>");
						}
						// mrc add
						else if(item.type=="conflict"){
							html.push(item.val+ ' ' +item.conflict_type);
						}
						else if(item.type=="wuzhi_chose"){
							html.push((item.value && item.value.id != -1 && item.value.name || ''));
						}
						else if(item.type=="wuzhi_attr"){
							html.push((item.value || ''));
						}
						// mrc add end
						else{
							html.push(item.val);
						}
						html.push("</td>");
					}else{
						if(th_obj.visit_input===undefined||th_obj.visit_input==1){
							if(item.type=="money"){
								html.push("<td>");
								if(item.val!=""){
									html.push(item.val+"("+pThis.money_type[item.money_type]+")");
								}
								html.push("<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/>");
								if(item.capitalized==1&&item.capitalized_val!=""){html.push("<br>("+item.capitalized_val+")");}
								html.push("</td>");
							}
							else if(item.type=="textarea"){
								//html.push("<td>"+item.val.replace(/\n/g,'<br>')+"</td>");
								html.push("<td><pre>"+pThis.encodeHtml(item.val)+"</pre></td>");
							}
							else if(item.type=="text"){
								html.push("<td>"+item.val+"<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/></td>");
							}
							// mrc add
							else if(item.type=="conflict"){
								html.push("<td>" + item.val+ ' ' +item.conflict_type + "</td>");
							}
							else if(item.type=="wuzhi_chose"){
								html.push("<td>" + (item.value && item.value.id != -1 && item.value.name || '') + "</td>");
							}
							else if(item.type=="wuzhi_attr"){
								html.push("<td>" + (item.value || '') + "</td>");
							}
							// mrc add end
							else{
								html.push("<td>"+item.val+"</td>");
							}
						}
					}
				});
				html.push("<td class='tr_edit' style='min-width:80px;text-align:center;color:#80c269;'>编辑</td>");
				html.push("</tr>");
			};

			table_html.html(html.join(""));

			table_html.off().on("click",".tr_edit",function(){
				var index = $(this).parent().index();
				pThis.tableEditEvent(table_obj,index - 1,otherParams);
			});

			calc.bindObjInputClick(table_html);
			table_html.find(".js_func_input").trigger("input");
		},
		checkHeaderInput:function(){
			if(!this.headerInput){return true;}

			var o = this.container.find(".headerPart").find("input");

			if($.trim(o.val()) == ""){
				layerPackage.fail_screen("请补全流程名称");
				o.focus();

				return false;
			}

			return true;
		},
		// mrc add
		init_muselect_box:function(dom){
			if(!dom) { return false}
			// 2017-03-27 11:15:21
			dom.each(function(){
				var parent = $(this).parent();
				var cacheData = parent.data("cacheData");
				var opts_2d = cacheData.opts_2d;
				var edit_data = cacheData.opts?cacheData.opts:[];

				var html = init_html(opts_2d,edit_data);
				$(this).append(html);
				var that = $(this);
				$(this).on('change','select',function(){
					var val = $(this).val();
					var index = $(this).index();
					// 删除后面的
					$(this).parent().find('select:gt('+index+')').remove();
					// 获取前面的来得到下一个数据, 小于等于
					var lt_arr = [];
					$(this).parent().find('select:not(:gt('+index+'))').each(function(){
						lt_arr.push($(this).val());
					});

					var child = return_child_by_index(lt_arr,opts_2d);
					if(child){
						if(child.children){
						    if(child.children.length > 0){
							    that.append(init_select_html(child.children,-1));
						    }
						}
					}
				});
			});
			// data是所有的, edit_data是数据
			function init_html(data,edit_data){
				var html = '';	
				// 没有数据的时候, 
				if(edit_data.length == 0){
					edit_data = [0,0,0,0,0,0,0,0,0];  // 默认是一个很长的递归, 这种写法并不好
					// for(var i=0;i<data.length;i++){
					// 	edit_data.push(0);
					// }
				}
				// 保存前面的
				var lt_arr = [];
				edit_data.forEach(function(ele,index,arr){
					if(index == 0){
						html += init_select_html(data,edit_data[0]);
					}else{
						var child = return_child_by_index(lt_arr,data);
						if(child){
							if(child.children){
							    if(child.children.length > 0){
								    html += init_select_html(child.children,edit_data[index])
							    }
							}
						}
					}
					lt_arr.push(ele);
				});
				return html;
			}
			// 初始化选择框
			function init_select_html(data, is_selected){
				if (!data) {return false}
				var _html = '<select class="js_input_val css_input_select s_box">';
					_html += '<option value="-1">请选择</option>';

				for(var i=0;i<data.length;i++){
					var selected = is_selected == i?'selected':'';
					_html += '<option value="'+i+'" data-id="'+data[i].id+'" '+selected+'>'+data[i].val+'</option>';
				}
					_html += '<select>';
				return _html;
			}
			// 根据当前第几个来返回children
			function return_child_by_index(arr,data){
				if (!arr || !data) {return false}
				var _data = data;

				for(var i=0;i<arr.length;i++){
					if(i == 0){ //一级节点比较特殊
						_data = _data[arr[i]];
					}else{ // 
						if(_data){
							if(_data.children){
								_data = _data.children[arr[i]];
							}
						}
					}
				}
				return _data;
			}
		},
		// mrc add end
		getUploadFileObj:function(){
			return this.uploadFileObj;
		},
		getFormName:function(){
			var headerPartObj = this.container.find(".headerPart");
			return headerPartObj.find(".form_name").text()
			.replace('?',headerPartObj.find('#header_input').val())
			.replace(/\s+/g, '');
		},
		getFormData:function(isCreate,canBeChange){
			var form_vals = {};
		 	this.container.find("div.js_inputs").each(function(){
				var d = $(this).data("cacheData");
				var type = d.type,key = d.input_key;

				if(type == "text" && $(this).find("input[input_key="+key+"]").length !=0){
					d.val = $(this).find("input[input_key="+key+"]").val();
				}
				else if(type == "textarea" && $(this).find("textarea[input_key="+key+"]").length !=0){
					d.val = $(this).find("textarea[input_key="+key+"]").val();
				}
				else if(type == "date" && $(this).find("input[input_key="+key+"]").length !=0){
					var val = $.trim($(this).find("input[input_key="+key+"]").val());

					/*针对苹果机器上时间问题*/
					if($.trim(val) && val.length > 0 && val.match(/:/g) && val.match(/:/g).length > 1){
						val = val.substring(0,val.lastIndexOf(":"));
					}

					if(val !=""){
						d.val =  val.replace('T',' ');
					}else{
						d.val = "";
					}
				}
				else if(type == "conflict" && $(this).find("input[input_key="+key+"]").length !=0){
					var st_time = $(this).find("input[input_key="+key+"]:eq(0)").val(),
						ed_time = $(this).find("input[input_key="+key+"]:eq(1)").val();

					/*针对苹果机器上时间问题*/
					if($.trim(st_time) && st_time.length > 0 && st_time.match(/:/g) && st_time.match(/:/g).length > 1){
						st_time = st_time.substring(0,st_time.lastIndexOf(":"));
					}
					if($.trim(ed_time) && ed_time.length > 0 && ed_time.match(/:/g) && ed_time.match(/:/g).length > 1){
						ed_time = ed_time.substring(0,ed_time.lastIndexOf(":"));
					}

					st_time = st_time.replace('T',' ');
					ed_time = ed_time.replace('T',' ');

					var val = st_time + ' - ' + ed_time;

					if(val !=""){
						d.val =  val.replace('T',' ');
					}else{
						d.val = "";
					}
					// mrc ad conflict
						d.conflict_type = $(this).find("select").val()
					// mrc add end
				}
				else if(type == "select" && $(this).find("select[input_key="+key+"]").length !=0){
					d.val = $(this).find("select[input_key="+key+"]").find("option:selected").val();
				}
				else if((type == "radio" || type == "checkbox") && $(this).find("input[input_key="+key+"]:checked").length !=0){
					var obj_val = "";
					var obj_c_other = 0;
					$(this).find("input[input_key="+key+"]:checked").each(function(){
						if (type == "radio" && $(this).hasClass("js_opts_radio")) {
							obj_val += $(this).val() + ";";
						}
						if (type == "checkbox" && $(this).hasClass("js_opts_checkbox")) {
							obj_val += $(this).val() + ";";
						}
						if ($(this).hasClass("js_other_cb") && $(this).parent().parent().find(".js_other_text").val() != "") {
							obj_val += $(this).parent().parent().find(".js_other_text").val() + ";";
							obj_c_other = 1;
						}
					});
					if (obj_val != "") {
						obj_val = obj_val.substring(0, obj_val.length - 1);
					}
					d.val = obj_val;
					d.c_other = obj_c_other;
				}
				else if(type == "money" && $(this).find("input[input_key="+key+"]").length !=0){
					var o = $(this).find("input[input_key="+key+"]");
					d.val = o.val();
					if(d.capitalized==1){
						d.capitalized_val = o.parent().parent().find(".js_capitalized_input").html();
					}
				}
				else if(type == "people" || type == "dept"){
					var selectObj = $(this).data("selectObj");

					if(!isCreate){
						var arr = selectObj.get_depts_users();
						var aId = new Array(),
							aName = new Array(),
							aPic = new Array();
						for (var i = 0; i < arr.length; i++) {
							aId.push(arr[i].id);
							aName.push(arr[i].name);
							if(arr[i].pic_url != "undefined" || !arr[i].pic_url){
								aPic.push(arr[i].pic_url);
							}
						};
						d.val = aName.join(",");
						d.ids = aId.join(",");
						d.imgs = aPic.join(",");
					}

					if(canBeChange){
						var arr = selectObj.get_depts_users();
						var aId = new Array(),
							aName = new Array(),
							aPic = new Array();
						for (var i = 0; i < arr.length; i++) {
							aId.push(arr[i].id);
							aName.push(arr[i].name);
							if(arr[i].pic_url != "undefined" || !arr[i].pic_url){
								aPic.push(arr[i].pic_url);
							}
						};
						d.val = aName.join(",");
						d.ids = aId.join(",");
						d.imgs = aPic.join(",");
					}
				}else if(type == "muselect" && $(this).find("select").length !=0){ // mrc add
					d.val = '';
					d.opts = [];
					$(this).find('.s_box').each(function(){
						d.opts.push($(this).val());
						d.val += $(this).find('option[value='+$(this).val()+']').html() + ' ';
					});			
				} 
				form_vals[key] = d;
			});
			return form_vals;
		},
		checkApplyData:function(isCreate,data){
			var inputs = data;
			if(!data){
				data = this.getFormData(isCreate);
			}

			var is_check = true ;
			var pThis = this;
			$.each(inputs,function(key,input){
				if(input.edit_input==1&&input.must == 1 && (input.type == "radio" || input.type == "checkbox") && !t.regCheck(input.val || "", 'notnull')){
					layerPackage.fail_screen('请填写' + input.name);
					is_check =  false;
					return false;
				}

				if (input.edit_input==1&&input.must == 1 && input.type != 'table' && !t.regCheck(input.val, 'notnull')) {
					layerPackage.fail_screen('请填写' + input.name);
					is_check =  false;
					return false;
				}
				if(input.edit_input==1&&input.type == 'text' && input.val!="" && input.format !=pThis.default_text_format ){
					if(input.format == pThis.number_text_format && !t.regCheck(input.val, 'double')){
						layerPackage.fail_screen(input.name+"需要填写数字");
						is_check = false;
						return false;
					}
					else if(input.format == pThis.email_text_format && !t.regCheck(input.val, 'email')){
						layerPackage.fail_screen(input.name+"需要填写邮箱");
						is_check = false;
						return false;
					}
					else if(input.format == pThis.phone_text_format && !t.regCheck(input.val, 'phone')){
						layerPackage.fail_screen(input.name+"需要填写电话号码");
						is_check = false;
						return false;
					}
					else if(input.format == pThis.telephone_text_format && !t.regCheck(input.val, 'mobile')){
						layerPackage.fail_screen(input.name+"需要填写手机号码");
						is_check = false;
						return false;
					}
					
				}
				//*
				if(input.edit_input==1&&input.type == 'table'){
					var rec = input.rec;
					if(input.must == 1 && (rec===undefined||rec.length==0)){
						layerPackage.fail_screen(input.name+"需要添加明细");
						is_check = false;
						return false;
					}
				}
				if(input.type == 'table'&&input.rec!==undefined){
					var rec = input.rec;
					var table_arr = input.table_arr;
					for(var i=0;i<rec.length;i++){
						var items = rec[i];
						$.each(items,function(key,item){
							var table_obj = table_arr[key];
							if (input.visit_input==1&&table_obj.visit_input==1&&table_obj.edit_input==1&&item.must == 1 && item.type != 'table' && !t.regCheck(item.val, 'notnull')) {
								layerPackage.fail_screen('请填写'+input.name+"详情"+(i+1)+"的" + item.name);
								is_check = false;
								return false;
							}
							if( item.type == 'text' && item.val!="" && item.format !=pThis.default_text_format ){
								if(item.format == pThis.number_text_format && !t.regCheck(item.val, 'double')){
									layerPackage.fail_screen(input.name+"详情"+(i+1)+"的" + item.name+"需要填写数字");
									is_check = false;
									return false;
								}
								else if(item.format == pThis.email_text_format && !t.regCheck(item.val, 'email')){
									layerPackage.fail_screen(input.name+"详情"+(i+1)+"的" + item.name+"需要填写邮箱");
									is_check = false;
									return false;
								}
								else if(item.format == pThis.phone_text_format && !t.regCheck(item.val, 'phone')){
									layerPackage.fail_screen(input.name+"详情"+(i+1)+"的" + item.name+"需要填写电话号码");
									is_check = false;
									return false;
								}
								else if(item.format == pThis.telephone_text_format && !t.regCheck(item.val, 'mobile')){
									layerPackage.fail_screen(input.name+"详情"+(i+1)+"的" + item.name+"需要填写手机号码");
									is_check = false;
									return false;
								}
								
							}
						});
						if(is_check == false){
							return false;
						}
					}
				}
				//*/
				// mrc add
				// 冲突的内容检测
				if(input.edit_input==1&&input.must==1&&input.type=="muselect") {
					var opts = input.opts.join(',');
					if(opts.indexOf('-1')!=-1){		
						layerPackage.fail_screen('请选择完整的' + input.name);
						is_check =  false;
						return false;
					}
				}
				// 冲突的检测
				if(input.edit_input==1&&input.must==1&&input.type=="conflict") {
					var arr = input.val.split(' - ');
					if(arr.length == 2){
						if(!arr[0] || !arr[1] || !input.conflict_type){
							layerPackage.fail_screen('请填写' + input.name);
							is_check =  false;
							return false;
						}
						if(arr[1]<=arr[0]){
							layerPackage.fail_screen(input.name + '的格式不正确');
							is_check =  false;
							return false;
						}
					}
				}
				if(input.edit_input==1&&input.must==1&&input.type=="wuzhi_chose") {
					console.log(input)
					if(input.val == "" || input.val == "请选择" || !input.val){		
						layerPackage.fail_screen('请选择完整的' + input.name);
						is_check =  false;
						return false;
					}
				}
				if(input.edit_input==1&&input.must==1&&input.type=="wuzhi_attr") {
					if(input.val == "" || (!input.val && input.val !=0) || input.val == "请选择"){		
						layerPackage.fail_screen('请选择完整的' + input.name);
						is_check =  false;
						return false;
					}
				}
				// mrc add end
			});
			if(is_check == false){
				return false;
			}

			if(this.file_type && 
				this.file_type == 2 && 
				this.getUploadFileObj().getUploadFiles().length == 0){
				layerPackage.fail_screen('请上传附件');
				return false;
			}
			return true;
		},
		//-------------------------------------
		getArrLastVal:function(arr){
			return arr[arr.length-1];
		},
		//---------------详情----------------------
		getDetailFormPart:function(dataObj,detailState,data){
			var isEdit = null;
			if(dataObj.type == "table"){
				var table_arr = dataObj.table_arr;

				for(var i in table_arr){
					var input = table_arr[i];

					if(!dataObj.edit_input && (input.visit_input === undefined || input.visit_input==1) && input.edit_input == 1 && (detailState == 1 || detailState == 2 || detailState ==3)){
						isEdit = true;
						break;
					}
				}
			}

			tpl.register('checkVal', t.checkVal);
			tpl.register('replaceStr', t.replaceStr);
			tpl.register('getArrLastVal', this.getArrLastVal);
			tpl.register('getDateTimeLocal', t.getDateTimeLocal);
			tpl.register('getNowFormatDate', this.getNowFormatDate);
			var dom = tpl(uiTemplate, {
				dFormPart: dataObj,
				dState:detailState,
				money_type:this.money_type,
				money_type_icon:this.money_type_icon,
				s_width : document.body.scrollWidth,
				userInfo:userInfo,
				isEdit:isEdit
			});
			tpl.unregister('checkVal');
			tpl.unregister('replaceStr');
			tpl.unregister('getArrLastVal');
			tpl.unregister('getDateTimeLocal');
			tpl.unregister('getNowFormatDate');

			var o = $($.trim(dom));
			o.data("cacheData",dataObj);
			o.attr("isedit",isEdit);

			//对应的事件绑定
			if(dataObj.type == "money"){
				this.moneyEvent(o);
			}else if(dataObj.type == "radio" || dataObj.type == "checkbox"){
				this.radioCheckBoxEvent(o);
				//console.log(dataObj);
			}else if(dataObj.type == "table"){
				this.tableEvent(o,{isDetail:1,data:data});
				//console.log(dataObj);
			}else if(dataObj.type == "people" || dataObj.type == "dept"){
				this.userSelectEvent(o);
			}

			return o;
		},
		getNowFormatDate:function(format) {
		    var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    var hour = date.getHours();
		    var minute = date.getMinutes();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    if(hour>=0 && hour <= 9){
		    	hour = "0" + hour;
		    }
		    if(minute>=0 && minute <= 9){
		    	minute = "0" + minute;
		    }
		    var currentdate = "";
		    if(format=="date"){
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
		    }
		    else if(format=="time"){
		    	currentdate = hour + seperator2 + minute;
		    }
		    else{
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		        + " " + hour + seperator2 + minute;
		    }
		    return currentdate;
		},
		//下一个节点信息
		getNextNode:function(workitem_id,callback){
			t.ajaxJson({
				url:nextNodeUrl,
				data: {
					"data": {
						"workitem_id":workitem_id
					}
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					if(!t.isFunction(callback)){return;}
					callback(result,status);
				}
			});
		},
		saveProcess:function(data,isEdit,isCheckData,callback){
			//特殊表单验证
			try{
				var flag = otherProcess.checkParams(data.is_other_proc);

				if(!flag){return;}
			}catch(e){
				console.log(e);
			}
			
			var data = this.getParams(data,isEdit,isCheckData);
			if(!data){layerPackage.unlock_screen();return;}

			var pThis = this;
			t.ajaxJson({
				url:saveOrEditUrl,
				data: {
					"data": JSON.stringify(data)
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					if(!t.isFunction(callback)){return;}
					callback(result,status);
				}
			});
		},
		getParams:function(data,isEdit,isCheckData){
			var uploadFileObj = this.getUploadFileObj();
			if(uploadFileObj && uploadFileObj.checkUploadStatus()){
				layerPackage.fail_screen('当前还有文件正在上传');
				return;
			}

			if(isCheckData && !this.checkApplyData(isEdit,data.form_vals)){return;}

			var params = {
				"form_id":data.form_id,
				"form_name":data.form_name,
				"work_id":data.work_id,
				"form_vals":data.form_vals
				//修改传入参数
				//"workitem_id":""
				//"formsetInst_id":""
				//"judgement":""
			}

			//if(isEdit){
				params.workitem_id = data.workitem_id;
				params.formsetInst_id = data.formsetInst_id;
				params.judgement = data.judgement;
			//}

			if(uploadFileObj){
				var files = uploadFileObj.getUploadFiles();

				if(data.work_node_id){
					var arr = new Array();
					for (var i = 0; i < files.length; i++) {
						var f = files[i];
						if(((f.work_node_id && f.work_node_id == data.work_node_id) && (f.create_id && f.create_id == userInfo.userId)) 
							|| !f.work_node_id){
							arr.push(f);
						}
					};

					files = arr;
				}

				params.files = files;

				if(data.file_type == 2 && files.length == 0 && isCheckData){
					layerPackage.fail_screen('请上传附件！');
					return null;
				}
			}

			return params;
		},
		send:function(data,callback){
			var pThis = this;
			layerPackage.lock_screen({
				"msg":"发送中……"
			});
			t.ajaxJson({
				url:sendUrl,
				data: {
					"data": data
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						//return;
					}

					if(!t.isFunction(callback)){return;}
					callback(result,status);
				}
			});
		},
		toDealList:function(activekey){
			if(!activekey){activekey = 0;}

			if(isIndex){
				this.toIndexDealList(activekey);
				return;
			}

			window.location.href = "/wx/index.php?model=process&a=dealList&degbug=1&activekey="+activekey;
		},
		toSendPage:function(params){
			if(!params){params = "";}

			window.location.href = "/wx/index.php?model=process&a=sendPage&degbug=1"+params + isIndex+menuType+hash;
		},
		toMineList:function(activekey){
			if(!activekey){activekey = 0;}

			if(isIndex){
				this.toIndexMineList(activekey);
				return;
			}

			window.location.href = "/wx/index.php?model=process&a=mineList&degbug=1&activekey="+activekey;
		},
		toNotifyList:function(activekey){
			if(!activekey){activekey = 0;}

			if(isIndex){
				this.toIndexNotifyList(activekey);
				return;
			}

			window.location.href = "/wx/index.php?model=process&a=notifyList&degbug=1&activekey="+activekey;
		},
		toDetail:function(id,listType,activekey){
			if(!listType){listType=1;}
			if(!activekey){activekey = 0;}
			window.location.href = "/wx/index.php?model=process&a=detail&degbug=1&id="+id+"&listType="+listType+"&activekey="+activekey + isIndex+menuType+hash;
		},
		/*详情编辑*/
		getInputEdit:function(dataObj,data,jqObj){
			var pThis = this;
			var tableDiv = this.tableDiv;
			var input_key = dataObj.input_key;
			var row_num = null,size = 0

			// 这里是插入头 mrc
			tableDiv.html("").append(this.getTableComponents(input_key,row_num,dataObj,size,1));
			tableDiv.data("cacheData",dataObj);

			tableDiv.on("click",".removeDiv",function(){
				pThis.cancelDiv(function(){
					//pThis.set_table_html(table_input_key);
				});
			});

			// 这里才是插入内容 mrc
			tableDiv.find(".detailed").append(this.getInputEditHtml(dataObj));

			//----特殊处理
			//otherProcess.edit_init(tableDiv,data);
			try{
				otherProcess.init({
					is_other_proc:data.formsetinst.is_other_proc,
					target:tableDiv,
					data:data,
					userId:data.workitem.creater_id,
					formsetinstId:data.formsetinst.id,
					state:data.workitem.state
				});
			}catch(e){
			}

			var menu = new Array(),
			save_menu = {
				width: 100,
				name: "保存",
				iconClass: "icon-save",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					pThis.saveDiv(dataObj,{isDetail:1,data:data,jqObj:jqObj},function(){
						//----特殊处理
						try{
							otherProcess.init({
								is_other_proc:data.formsetinst.is_other_proc,
								target:jqObj.parent("#input_content"),
								data:data,
								listType:t.getUrlParam("listType"),
								userId:data.workitem.creater_id,
								formsetinstId:data.formsetinst.id,
								state:data.workitem.state
							});
						}catch(e){
						}
					});
				}
			};

			menu.push(save_menu);
			UiFramework.wxBottomMenu.init({
				target:tableDiv,
				menu: menu
			});

			var height = this.container.height();
			tableDiv.css("top",height);
			//tableDiv.html(htmlstr);

			tableDiv.animate({top:'0px'},"slow",function(){
				pThis.container.css("display","none");
				scroll(0,0);
			});
		},
		getInputEditHtml:function(dataObj){
			tpl.register('checkVal', t.checkVal);
			tpl.register('replaceStr', t.replaceStr);
			tpl.register('getArrLastVal', this.getArrLastVal);
			tpl.register('getDateTimeLocal', t.getDateTimeLocal);
			var dom = tpl(uiTemplate, {
				editData: dataObj,
				money_type:this.money_type,
				s_width : document.body.scrollWidth,
				tag_list: tag_list,
			});
			tpl.unregister('checkVal');
			tpl.unregister('replaceStr');
			tpl.unregister('getArrLastVal');
			tpl.unregister('getDateTimeLocal');
			//数据保存
			var o = $($.trim(dom));
			o.data("cacheData", dataObj);

			//对应的事件绑定
			if(dataObj.type == "money"){
				this.moneyEvent(o);
			}else if(dataObj.type == "radio" || dataObj.type == "checkbox"){
				this.radioCheckBoxEvent(o);
			}else if(dataObj.type == "table"){
				this.tableEvent(o);
			}else if(dataObj.type == "people" || dataObj.type == "dept"){
				this.userSelectEvent(o);
			// mrc add 
			} else if(dataObj.type == "wuzhi_chose"){
				this.init_wuzhi_chose(o)
			} else if(dataObj.type == "wuzhi_attr"){  // mrc add
				var that = this
				this.init_wuzhi_attr(o)
				setTimeout(function(){
					that.init_wuzhi_link_attr(o.data('cacheData').input_key)
				}, 100)
			}
			// mrc add end
			return o;
		},
		get_finput_data:function(){
			this.container.find("div.js_inputs").each(function(){
				var data = $(this).data("cacheData");
				var type = data.type,key = data.input_key;

				if(type == "text"){
					data.val = $(this).find("input[input_key="+key+"]").val();
				}
				else if(type == "money"){
					data.val = $(this).find("input[input_key="+key+"]").val();
					if(data.capitalized==1){
						data.capitalized_val = $("input[input_key="+key+"]").parent().parent().find(".js_capitalized_input").html();
					}
				}
			});
		},
		set_input_html:function(dataObj,input_key,otherParams){
			var type = dataObj.type,jqObj = otherParams.jqObj;
			var input_obj = dataObj;
			if (type == "money") {
				var html = new Array();

				if(input_obj.val !=""){
					html.push(this.money_type_icon[input_obj.money_type]);
					html.push("<label>" + input_obj.val + "</label>");
					html.push("<input type='hidden' input_key='" + input_key + "' class='js_input_val js_money_input js_func_input' value='" + input_obj.val + "'>");
				}else{
					html.push("<label></label><input type='hidden' input_key='" + input_obj.input_key + "' class='js_input_val js_money_input js_func_input' value=''>");
				}
				html.push('<i class="icon-edit fr c-green"></i>');

				if(input_obj.capitalized == 1 && input_obj.capitalized_val != ""){
					html.push('<br>(<span class="js_input_val js_capitalized_input" style="width: 20%">' + input_obj.capitalized_val + '</span>)');
				}

				jqObj.find(".js_edit_div").html(html.join(''));
				calc.bindObjInputClick(jqObj.find(".js_edit_div"));
				jqObj.find(".js_func_input").trigger("input");
			}
			else if(type == "text"){
				var html = new Array();
				if (input_obj.val === undefined) {
					input_obj.val = "";
				}
				html.push('<label>' + input_obj.val + '</label><input type="hidden" input_key="' + input_obj.input_key + '" class="js_input_val js_func_input" value="' + input_obj.val + '">');
				html.push('<i class="icon-edit fr c-green"></i>');
				jqObj.find(".js_edit_div").html(html.join(''));
				calc.bindObjInputClick(jqObj.find(".js_edit_div"));
				jqObj.find(".js_func_input").trigger("input");
			} 
			else if(type == "textarea"){
				if (input_obj.val === undefined) {
					input_obj.val = "";
				}

				jqObj.find("pre").text(input_obj.val);			
			}
			// mrc add
			else if(type == "conflict"){
				if (input_obj.val !== undefined && input_obj.val != "") {
					jqObj.find(".js_edit_div").html(input_obj.val.replace(/\n/g, '<br>') + ' ' + input_obj.conflict_type + '<i class="icon-edit fr c-green"></i>');
				} else {
					jqObj.find(".js_edit_div").html('<i class="icon-edit fr c-green"></i>');
				}	
			}
			else if(type == "wuzhi_chose"){
				var html = input_obj.cat && input_obj.cat.id != -1 && input_obj.value && input_obj.value.id != -1 && input_obj.value.name || ''
						jqObj.find(".js_edit_div").html(html + '<i class="icon-edit fr c-green"></i>');
			} 
			else if(type == "wuzhi_attr"){
				var html = input_obj.value || ''
						jqObj.find(".js_edit_div").html(html + '<i class="icon-edit fr c-green"></i>');
			} 
			// mrc add end
			else {
				if (input_obj.val !== undefined && input_obj.val != "") {
					jqObj.find(".js_edit_div").html(input_obj.val.replace(/\n/g, '<br>') + '<i class="icon-edit fr c-green"></i>');
				} else {
					jqObj.find(".js_edit_div").html('<i class="icon-edit fr c-green"></i>');
				}
			}
		},
		check_input:function (input){
			var is_check = true;
			if (input.edit_input == 1 && input.must == 1 && input.type != 'table' && !t.regCheck(input.val, 'notnull')) {
				layerPackage.fail_screen('请填写' + input.name);
				is_check = false;
				return false;
			}
			if (input.edit_input == 1 && input.type == 'text' && input.val != "" && input.format != this.default_text_format) {
				if (input.format == this.number_text_format && !t.regCheck(input.val, 'double')) {
					layerPackage.fail_screen(input.name + "需要填写数字");
					is_check = false;
					return false;
				} else if (input.format == this.email_text_format && !t.regCheck(input.val, 'email')) {
					layerPackage.fail_screen(input.name + "需要填写邮箱");
					is_check = false;
					return false;
				} else if (input.format == this.phone_text_format && !t.regCheck(input.val, 'phone')) {
					layerPackage.fail_screen(input.name + "需要填写电话号码");

					is_check = false;
					return false;
				} else if (input.format == this.telephone_text_format && !t.regCheck(input.val, 'mobile')) {
					layerPackage.fail_screen(input.name + "需要填写手机号码");
					is_check = false;
					return false;
				}
			}
			return is_check;
		},
		//缓存数据
		storageInit:function(id,myData,callback){
			try{
				var id = t.getHrefParamVal("id"),pThis = this;

				if(!id){return;}

				var storage_key = 'apply_process_'+id;
				$storage.init({
					key:storage_key,
					time:2000,
					init_data : function(data){
						if(data != null){
							var tmp_dataObj = JSON.parse(data);
							var proc_form_id = tmp_dataObj["id"];
							if(proc_form_id!==undefined&&proc_form_id==id){
								myData = tmp_dataObj;
								callback(myData);
							}
							$('.js_inputs').remove();
						}
					},
					save_data : function(){
						var inputs = pThis.getFormData();
						myData["id"] = id;
						myData.inputs = inputs;

						return myData;
					}
				});
			}catch(e){
				console.log(e);
			}
		},
		storageClear:function(key){
			$storage.clear(key);
		},
		storageClearAll:function(){
			$storage.clearAll();
		},
		//附件列表
		getFileListPart:function(filesData){
			tpl.register('getFileTypeExtIcon', t.getFileTypeExtIcon);
			var dom = tpl(uiTemplate, {
				fileList:filesData.files,
				filesData:filesData,
				cdn_domain:cdn_domain,
				defaultFace:defaultFace,
				media_domain:media_domain
			});
			tpl.unregister('getFileTypeExtIcon');

			var o = $($.trim(dom));

			return o;
		},
		//测试数据
		testData: function() {
			return ;
		},
		toNotifyPage:function(formsetinstId,listType,id,activekey){
			window.location.href = "/wx/index.php?model=process&a=notify&degbug=1&formsetinstId="
			+formsetinstId+"&listType="+listType+"&activekey="+activekey+"&id="+id+isIndex+menuType+hash;
		},
		//主页型相关
		toIndexDealList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pDealList";
			
			this.toIndex(menuType,activekey);
		},
		toIndexMineList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pMineList";
			
			this.toIndex(menuType,activekey);
		},
		toIndexNotifyList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pNotifyList";
			
			this.toIndex(menuType,activekey);
		},
		toIndex:function(menuType,activekey){
			var url = "/wx/index.php?debug=1&app=index&a=index"+menuType;
			if(t.getUrlParam("menuType") == 2){
				url +=  "&activekey="+activekey;
			}

			window.location.href = url + hash;
		},
		encodeHtml:function(s){
			return (typeof s != "string") ? s :
          s.replace(/"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g,
                    function($0){
                        var c = $0.charCodeAt(0), r = ["&#"];
                        c = (c == 0x20) ? 0xA0 : c;
                        r.push(c); r.push(";");
                        return r.join("");
                    });
		},
		getSaveOrEditUrl:function(){
			return saveOrEditUrl;
		},
		setSaveOrEditUrl:function(url){
			if(url === undefined || url === null){
				return;
			}

			saveOrEditUrl = url;
		}
	};

	return process_common;
});