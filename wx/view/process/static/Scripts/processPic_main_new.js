require([
  "jquery",
  "common:widget/lib/tools",
  "common:widget/lib/UiFramework",
  "process:static/Scripts/mv_pic_new/GooFunc",
  "process:static/Scripts/mv_pic_new/json2",
  "process:static/Scripts/mv_pic_new/GooFlow",
], function ($, t, UiFramework) {
  var layerPackage = UiFramework.layerPackage();
  layerPackage.lock_screen(); //遮罩start
  //请求地址
  var processPicUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=109";

  //jqObj对象
  var container = $("#main-container"),
    formsetinstId = t.getHrefParamVal("formsetinstId") || 1;

  t.ajaxJson({
    url: processPicUrl,
    data: {
      data: {
        formsetinst_id: formsetinstId,
      },
    },
    callback: function (result, status) {
      if (result.errcode != 0) {
        layerPackage.fail_screen(result.errmsg);
        return;
      }

      if (result.info.state == 3) {
        // 终止
        $(".flowBreak").removeClass("hide");
      } else if (result.info.state == 4) {
        //撤销
        $(".flowRevoke").removeClass("hide");
      } else if (result.info.state == 5) {
        //撤销中
        $(".flowRevokeIng").removeClass("hide");
      } else if (result.info.state == 6) {
        //通过后撤销
        $(".flowRevokeEnd").removeClass("hide");
      } else if (result.info.state == 2) {
        //完成(通过)
        $(".flowCompelet").removeClass("hide");
      } else if (result.info.state == 1) {
        //运行中
        $(".examineDiv>p>span").html(result.info.curr_handler[0].workitem_name);
        $(".examineDiv").removeClass("hide");
        var peopleHtmlArray = [];
        for (var i in result.info.curr_handler) {
          var curr_handler = result.info.curr_handler[i];
          var state = curr_handler.state;
          var stateStr = "";
          if (state == 1) {
            stateStr = "新收到";
          } else if (state == 2) {
            stateStr = "已阅读";
          } else if (state == 3) {
            stateStr = "已保存";
          } else if (state == 7) {
            stateStr = "已处理";
          }
          if (curr_handler.handler_pic_url != null) {
            var imgUrl = curr_handler.handler_pic_url.replace(
              /(.*)(\/|\/0)$/,
              "$1/100"
            );
          } else if (
            curr_handler.handler_pic_url == null &&
            curr_handler.handler_id != "0"
          ) {
            var imgUrl = defaultFace;
          } else {
            var imgUrl = "view/common/static/images/robbot.png";
          }
          //   var opendata_handler_name = t.openData(
          //     "userName",
          //     curr_handler.handler_acct,
          //     curr_handler.handler_name
          //   );
          var opendata_handler_name = curr_handler.handler_name;
          var name =
            curr_handler.handler_id == "0"
              ? "数字员工"
              : curr_handler.handler_name == null
              ? "人员已删除"
              : opendata_handler_name;
          if (
            curr_handler.handler_id == null &&
            curr_handler.handler_name == null &&
            curr_handler.handler_pic_url == null
          ) {
            name = "人员未接单";
          }
          peopleHtmlArray.push("<div>");
          peopleHtmlArray.push("<div>");
          peopleHtmlArray.push(
            "<img onerror=\"this.src='" +
              defaultFace +
              '\'" src="' +
              imgUrl +
              '" alt="">'
          );
          peopleHtmlArray.push(
            '<span class="name hideTxt">' + name + "</span>"
          );
          peopleHtmlArray.push(
            '<span class="reState">' +
              (curr_handler.handler_id != "0" &&
              curr_handler.handler_name == null
                ? ""
                : stateStr) +
              "</span>"
          );
          peopleHtmlArray.push("</div>");
          if (i != result.info.curr_handler.length - 1) {
            peopleHtmlArray.push('<div class="wxLine"></div>');
          }
          peopleHtmlArray.push("</div>");
        }
        var peopleHtml = peopleHtmlArray.join("");
        $(".examineDiv .examineList").html(peopleHtml);
      }

      var property = {
        toolBtns: [],
        haveHead: false,
        headBtns: [], //如果haveHead=true，则定义HEAD区的按钮
        haveTool: false,
        haveGroup: false,
      };

      var demo;
      var data = result.info;
      var jsondata = $.parseJSON(data.work_pic);
      var json_done = $.parseJSON(data.worknodes_arr);

      //**获取节点需要最大的高度和宽度，计算div宽度和高度
      for (var i in jsondata.nodes) {
        var node_json = jsondata.nodes[i];
        if (property.width == undefined) {
          property.width = node_json.left;
        } else {
          if (node_json.left > property.width) {
            property.width = node_json.left;
          }
        }

        if (property.height == undefined) {
          property.height = node_json.top;
        } else {
          if (node_json.top > property.height) {
            property.height = node_json.top;
          }
        }
      }

      property.width = property.width + 180;
      property.height = property.height + 100;

      demo = $.createGooFlow($("#demo"), property);
      console.log(jsondata);
      console.log("json_done", json_done);
      demo.loadData(jsondata, json_done);

      UiFramework.bottomInfo.init(container);

      layerPackage.unlock_screen();
    },
  });
}, function (e) {
  requireErrBack(e);
});
