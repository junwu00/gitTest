//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/detail.html',
	'common:widget/lib/GenericFramework',
	'process:static/Scripts/process_common',
	'process:static/Scripts/otherProcess'
],function(tools,UiFramework,tpl,detailTpl,GenericFramework,common,otherProcess){
	var layerPackage = UiFramework.layerPackage();
	layerPackage.lock_screen();//遮罩start

	var getDetailUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=108",
	urgeUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=117",
	goToBackUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=116",
	signUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=119",
	backStartUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=110",
	backPrevUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=111",
	delUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=112",
	backSelectUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=122",
	//指纹验证 
	checkSoterUrl = "/wx/index.php?model=index&m=mine&a=index&cmd=106";
	///wx/index.php?model=index&m=mine&a=index&cmd=106

	var isIndex = tools.getHrefParamVal("isIndex") || "",
		menuType = tools.getHrefParamVal("menuType") || 1,
		hash = window.location.hash;

	// mrc add
	var notify_is_click = false;	
	var data_info = {}; 

	var detail = {
		_setting:{
			id:null,
			listType:null,
			activekey:null,
			target:null,
			callback:{
				error_callback:null,
				success_callback:null,
				finish_callback:null
			}
		},
		dom:null,
		$dom:null,
		cacheSetting:null,
		cacheData:null,
		commentPage:1,
		dealPage:1,
		checkParams: function(setting) {
			if(setting.target == null || !(setting.target instanceof jQuery)){
				console.log('错误的jq对象');
				return;
			}

			var _setting = tools.clone(this._setting);
			$.extend(true, _setting, setting);

			this.setting = setting;

			return _setting;
		},
		init: function(_setting) {
			_setting = this.checkParams(_setting);

			if(!_setting.id || !_setting.listType || _setting.activekey === undefined){console.log("配置错误");return;}

			this.cacheSetting = _setting;

			this.getDetail(_setting);
		},
		getDetail:function(_setting){
			var pThis = this,
			listType = _setting.listType,
			activekey = _setting.activekey,
			id = _setting.id,
			params = {
				"listType":listType
			};

			if(listType == 1){
				params.workitem_id = id;
			}else if(listType == 2){
				params.formsetinst_id = id;
			}else if(listType == 3){
				params.notify_id = id;
			}

			tools.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":params
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						if(!_setting.callback || !tools.isFunction(_setting.callback.error_callback)){return;}
						_setting.callback.error_callback.apply(pThis,[result]);
						return;
					}

					common.init(_setting.target);

					var data = result.info;
						data_info = data;
					pThis.cacheData = data;
					_setting.data = data;
					console.log(data);

					if(listType == 1 || listType == 2){
						var form_vals = data.formsetinst.form_vals,
							state = data.workitem.state,
							formsetinstId = data.formsetinst.id;

						_setting.work_id = data.formsetinst.work_id;
						_setting.workitemId = data.workitem.id;
						_setting.workitemState = data.workitem.state;
						_setting.formsetinstId = formsetinstId;
						_setting.workNodeId = data.workitem.work_node_id;
						_setting.isStart = data.workitem.is_start;
						_setting.workType = data.workitem.work_type;
						_setting.isLinkSign = data.formsetinst.is_linksign;
						_setting.formsetinstState = data.formsetinst.state;
						_setting.isReturnBack = data.workitem.is_returnback;

						_setting.isOtherProc = data.formsetinst.is_other_proc;
						_setting.createId = data.workitem.creater_id;
						_setting.state = state;

						try{_setting.fingerPrint = data.fingerprint;}catch(e){console.log(e);}
					}else{
						var form_vals = data.notify.form_vals,
							state = data.notify.state,
							formsetinstId = data.notify.formsetinst_id;

						_setting.formsetinstId = formsetinstId;

						_setting.isOtherProc = data.notify.is_other_proc;

						_setting.createId = data.notify.creater_id;
						_setting.state = state;

						try{_setting.fingerPrint = data.fingerprint;}catch(e){console.log(e);}
					}


					//草稿
					if(listType == 2 && activekey == 2){
						//表单主体信息
						_setting.target.append(common.getFormInfo(data.formsetinst));

						//表单控件信息
						var inputs = data.formsetinst.form_vals;
			            for(var d in inputs){
							if(!inputs[d]){continue;}
							_setting.target.append(common.getFormPart(inputs[d]));
						}

						//其他非表单内容
						_setting.target.append(common.getOtherPart(data.workitem,function(uploadFileObj){
							pThis.uploadFileObj = uploadFileObj;
							if(data.files.length > 0){
								//console.log(common.uploadFileObj);
								uploadFileObj.setUploadFiles(data.files);
								//uploadFileObj.addFiles(data.files);
							}
						}));

						_setting.target.find(".fileUpload").after(common.getFileListPart(data));

						//-------特殊处理--------
						otherProcess.init({
							is_other_proc:_setting.isOtherProc,
							target:_setting.target,
							data:data,
							userId:_setting.createId,
							formsetinstId:_setting.formsetinstId,
							state:_setting.state
						});

						common.setSaveOrEditUrl(otherProcess.getSaveOrEditUrl(_setting.isOtherProc,common.getSaveOrEditUrl()));

						//绑定公式插件
						common.bindCalc(inputs);

						pThis.bindTableEditDom(_setting);

						pThis.appendEditBottonBtn(_setting);
						pThis.finish_callback(_setting);

						// mrc add
						common.init_muselect_box();
						// mrc add

						return;
					}

					pThis.appendHtml(_setting,data);

					for(var d in form_vals){
						_setting.target.find("#input_content").append(common.getDetailFormPart(form_vals[d],state,data));
					};

					//-------特殊处理--------
					otherProcess.init({
						is_other_proc:_setting.isOtherProc,
						target:_setting.target,
						data:data,
						listType:listType,
						userId:_setting.createId,
						formsetinstId:_setting.formsetinstId,
						state:_setting.state
					});

					common.setSaveOrEditUrl(otherProcess.getSaveOrEditUrl(_setting.isOtherProc,common.getSaveOrEditUrl()));

					//绑定公式插件
					common.bindCalc(form_vals);

					pThis.bindDom(_setting);

					//附件处理
					try{
						if(listType != 3 && data.workitem.file_type !=0){
							/*
							require(['common:widget/lib/GenericFramework'],function(GenericFramework){
								var uploadFileObj = GenericFramework.uploadFile({
									target:_setting.target.find(".fileUpload #sign-upload")
								});

								pThis.uploadFileObj = uploadFileObj;
								common.uploadFileObj = uploadFileObj;

								if(data.files.length > 0){
									uploadFileObj.setUploadFiles(data.files);
									//uploadFileObj.addFiles(data.file);
								}
							});
							*/

							common.bindUploadFile(_setting.target.find(".fileUpload"),function(uploadFileObj){
								pThis.uploadFileObj = uploadFileObj;

								if(data.files.length > 0){
									uploadFileObj.setUploadFiles(data.files);
									//uploadFileObj.addFiles(data.file);
								}
							});
						}
					}catch(e){
						console.log(e);
					}

					pThis.appendBottomBtn(_setting);

					layerPackage.unlock_screen();

					pThis.finish_callback(_setting);
				}
			});
		},
		bindDom:function(_setting){
			var pThis = this;
			//Tab切换
			_setting.target.on("click",".tab li",function(){
				var li = $(this),index = li.index();

				if(li.hasClass('active')){return;}

				li.addClass('active').siblings('li').removeClass('active');

				li.parents('section')
				.find('.tab-part:eq('+index+')')
				.removeClass('hide')
				.siblings('.tab-part').addClass('hide');
			});

			//流程图
			_setting.target.on("click",".checkFlow",function(){
				window.location.href = "/wx/index.php?model=process&a=processPic&formsetinstId="+_setting.formsetinstId;
			});

			//打印
			_setting.target.on("click",".printForm",function(){
				window.location.href = "/wx/index.php?model=process&a=print_form&formsetinst_id="+_setting.formsetinstId;
			});

			/*//附件操作
			_setting.target.on("click","#files .queue-file",function(){
				var md5_hash = $(this).attr("data-hash"),
				ext = $(this).attr("data-ext"),
				path = $(this).find("input").val(),
				filename = $(this).find(".filename").text(),
				id = $(this).attr("data-id"),
				workNodeId = $(this).attr("data-worknodeid");

				var data = _setting.data;

				var btnMenuPackage = UiFramework.btnMenuPackage();

				var opt = {};
				opt.data = {};
				opt.data.el = $(this);
				opt.data.hash = md5_hash;
				opt.data.ext = ext;
				opt.data.path = path;
				opt.data.name = filename;
				opt.data.id = id;
				opt.menus = [];

				var extClass = tools.getFileTypeExtIcon(ext);

				var url = "/wx/index.php?model=process&m=ajax&a=index&cmd=121&formsetinst_id="+_setting.formsetinstId+"&cf_id="+id;

				if(extClass == "doc" || extClass == "ppt" 
					|| extClass == "xls" || extClass == "txt" 
					|| extClass == "pdf"){

					var m1 = {};
					m1.id = 1;
					m1.name = "预览";
					m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
					m1.fun = function(el, m, o) {
						GenericFramework.viewFile(ext,md5_hash);
					};
					opt.menus.push(m1);
					var m2 = {};
					m2.id = 2;
					m2.name = "下载";
					m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m2.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
					};
					opt.menus.push(m2);
					if(workNodeId == _setting.workNodeId 
						&& userInfo.userId == data.workitem.creater_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						var m3 = {};
						m3.id = 3;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
					}

					btnMenuPackage.init(opt);
				}else if(extClass == "img"){
					if(workNodeId == _setting.workNodeId 
						&& userInfo.userId == data.workitem.creater_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						var m1 = {};
						m1.id = 1;
						m1.name = "预览";
						m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
						m1.fun = function(el, m, o) {
							GenericFramework.init(wxJsdkConfig);

							GenericFramework.previewImage({
								target:o.el,
								current:path,
								urls:[path]
							});
						};
						opt.menus.push(m1);
						var m3 = {};
						m3.id = 2;
						m3.name = "下载";
						m3.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
						m3.fun = function(el, m, o) {
							//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
							GenericFramework.fileDownload(url);
						};
						opt.menus.push(m3);
						var m2 = {};
						m2.id = 3;
						m2.name = "删除";
						m2.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m2.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m2);
						btnMenuPackage.init(opt);
					}else{
						GenericFramework.init(wxJsdkConfig);

						GenericFramework.previewImage({
							target:$(this),
							current:path,
							urls:[path]
						});
					}
				}else{
				    var m2 = {};
				    m2.id = 1;
			        m2.name = "下载";
			        m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			        m2.fun = function(el, m, o) {
			        	//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
			        };
			        opt.menus.push(m2);
			        if(workNodeId == _setting.workNodeId 
			        	&& userInfo.userId == data.workitem.creater_id 
			        	&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						var m3 = {};
						m3.id = 2;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
						btnMenuPackage.init(opt);
					}
			        btnMenuPackage.init(opt);
				}
			});
			*/

			//可编辑
			_setting.target.on("click",".js_edit_div",function(){
				var uploadFileObj = common.getUploadFileObj();
				if(uploadFileObj && uploadFileObj.checkUploadStatus()){
					layerPackage.fail_screen('当前还有文件正在上传');
					return;
				}

				var jqObj = $(this).parents(".css_row");
				var input_id = jqObj.attr("id");
				var dataObj = jqObj.data("cacheData");
				common.getInputEdit(dataObj,_setting.data,jqObj);
				common.init_muselect_box($('.muselect_box'));

			});

			this.bindTableEditDom(_setting);
		},
		bindTableEditDom:function(_setting){
			/*编辑*/
			_setting.target.find("table").on("click",".tr_edit",function(){
				var index = $(this).parent().index();
				var data = $(this).parents("table").parent().parent().data("cacheData");

				var otherParams = undefined;
				if(!(_setting.listType == 2 && _setting.activekey == 2)){
					otherParams = {
						isDetail:1,
						jqObj:$(this).parents("table").parent().parent(),
						data:_setting.data
					};
				}

				common.tableEditEvent(data,index - 1,otherParams);
			});

			this.bindUploadFileDom(_setting);
		},
		bindUploadFileDom:function(_setting){
			var pThis = this;

			//附件操作
			_setting.target.on("click","#files .queue-file",function(){
				var md5_hash = $(this).attr("data-hash"),
				ext = $(this).attr("data-ext"),
				path = $(this).find("input").val(),
				filename = $(this).find(".filename").text(),
				id = $(this).attr("data-id"),
				workNodeId = $(this).attr("data-worknodeid"),
				createId = $(this).attr("data-createid");

				var data = _setting.data;

				var btnMenuPackage = UiFramework.btnMenuPackage();

				var opt = {};
				opt.data = {};
				opt.data.el = $(this);
				opt.data.hash = md5_hash;
				opt.data.ext = ext;
				opt.data.path = path;
				opt.data.name = filename;
				opt.data.id = id;
				opt.menus = [];

				var extClass = tools.getFileTypeExtIcon(ext);

				var url = "/wx/index.php?model=process&m=ajax&a=index&cmd=121&formsetinst_id="+_setting.formsetinstId+"&cf_id="+id;

				//platid
				/*
					1：iPod、iPad、iPhone
					2：android
					3：wp手机
					4：Windows
					5：Mac
					0：其他
				*/

				if(extClass == "doc" || extClass == "ppt" 
					|| extClass == "xls" || extClass == "txt" 
					|| extClass == "pdf"){

					if(platid != 1){
						var m1 = {};
						m1.id = 1;
						m1.name = "预览";
						m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
						m1.fun = function(el, m, o) {
							GenericFramework.viewFile(ext,md5_hash);
						};
						opt.menus.push(m1);
					}

					var m2 = {};
					m2.id = 2;
					m2.name = "下载";
					m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m2.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url,null,ext);
					};
					opt.menus.push(m2);
					if(workNodeId == _setting.workNodeId 
						&& createId == data.workitem.handler_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						var m3 = {};
						m3.id = 3;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
					}

					if((platid == 1 || platid == 2) && opt.menus.length == 1){
						GenericFramework.fileDownload(url,null,ext);
					}else{
						btnMenuPackage.init(opt);
					}
				}else if(extClass == "img"){
					var m1 = {};
					m1.id = 1;
					m1.name = "预览";
					m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
					m1.fun = function(el, m, o) {
						try{
							if(platid == 4 || platid ==5){
								GenericFramework.viewFile(ext,md5_hash);
							}else{
								GenericFramework.init(wxJsdkConfig);

								GenericFramework.getNetworkType(function(networkType){
									if(networkType == "wifi"){
										path = path.replace("resize","origin");
									}

									GenericFramework.previewImage({
										target:o.el,
										current:path,
										urls:[path]
									});
								});
							}
						}catch(e){
						}
					};
					opt.menus.push(m1);
					var m3 = {};
					m3.id = 2;
					m3.name = "下载";
					m3.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m3.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						//pcWx
						if(platid == 4 || platid == 5){
							var newDownloadUrl = url + "&origin=1";
							GenericFramework.fileDownload(newDownloadUrl);
						}
						//app wx
						else{
							//检测设备网络状态
							GenericFramework.getNetworkType(function(networkType){
								var newDownloadUrl = url;
								var origin = 0;
								if(networkType == "wifi"){
									origin = 1;
								}

								newDownloadUrl += "&origin="+origin;

								GenericFramework.fileDownload(newDownloadUrl);
							});
						}
					};
					opt.menus.push(m3);
					if(workNodeId == _setting.workNodeId 
						&& createId == data.workitem.handler_id 
						&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						
						var m2 = {};
						m2.id = 3;
						m2.name = "删除";
						m2.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m2.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m2);
						btnMenuPackage.init(opt);
					}else{
						try{
							if(platid == 4 || platid == 5){
								btnMenuPackage.init(opt);
							}else{
								GenericFramework.init(wxJsdkConfig);

								GenericFramework.getNetworkType(function(networkType){
									if(networkType == "wifi"){
										path = path.replace("resize","origin");
									}
									GenericFramework.previewImage({
										target:$(this),
										current:path,
										urls:[path]
									});
								});
							}
						}catch(e){

						}
					}
				}else{
				    var m2 = {};
				    m2.id = 1;
			        m2.name = "下载";
			        m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			        m2.fun = function(el, m, o) {
			        	//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
			        };
			        opt.menus.push(m2);
			        if(workNodeId == _setting.workNodeId 
			        	&& createId == data.workitem.handler_id 
			        	&& (_setting.workitemState == 1 || _setting.workitemState == 2 || _setting.workitemState == 3)){
						var m3 = {};
						m3.id = 2;
						m3.name = "删除";
						m3.icon_html = '<span><i class="icon icon-remove"></i></span>';
						m3.fun = function(el, m, o) {
							o.el.remove();
							pThis.uploadFileObj.delFile(md5_hash,ext);
							btnMenuPackage.hide();
						};
						opt.menus.push(m3);
						//btnMenuPackage.init(opt);
					}

					if((platid == 1 || platid == 2) && opt.menus.length == 1){
						GenericFramework.fileDownload(url);
					}else{
						btnMenuPackage.init(opt);
					}
				}
			});
		},
		clearData:function(){
		},
		btnAjax:function(_setting,url,params,ctrlName,callback){
			var pThis = this;
			tools.ajaxJson({
				url: url,
				data: {
					"data": params
				},
				callback: function(result, status) {
					if (status == false && result == null) {return;}

					if (result.errcode != 0 && ctrlName != "backPrev") {
						if (!_setting.callback || !tools.isFunction(_setting.callback.error_callback)) {return;}
						_setting.callback.error_callback.apply(pThis, [result]);
						layerPackage.unlock_screen();
						return;
					}

					if (!_setting.callback || !tools.isFunction(_setting.callback.success_callback)) {return;}
					_setting.callback.success_callback.apply(pThis, [result, ctrlName]);

					if(!tools.isFunction(callback)){return;}
					callback(result, status);
				}
			});
		},
		appendHtml:function(_setting,data){
			var dom = null;
			tpl.register('dateFormat', tools.dateFormat);
			tpl.register('bytesToSize', tools.bytesToSize);
			tpl.register('getFileTypeExtIcon', tools.getFileTypeExtIcon);
			tpl.register('checkNewline', tools.checkNewline);


			// mrc add
			// 
			var notifys = data.notifys;
			var temp_data = [];
			var is_read = {};
			var is_in = {};

			// 获取已读的
			for(var i=0;i<notifys.length;i++){ 
			  if(notifys[i].state == '2'){
			    is_read[notifys[i]['notify_id']] = 1
			  }
			}

			// 先插入已读的到数组
			for(var i=0;i<notifys.length;i++){ 
			  if(is_read[notifys[i]['notify_id']] && !is_in[notifys[i]['notify_id']] && notifys[i]['state'] == '2'){
			    temp_data.push(notifys[i]);
			    is_in[notifys[i]['notify_id']] = 1;
			  }
			}

			// 插入未读的
			for(var i=0;i<notifys.length;i++){ 
			  if(!is_in[notifys[i]['notify_id']] && notifys[i]['state'] == '1'){
			    temp_data.push(notifys[i]);
			    is_in[notifys[i]['notify_id']] = 1;
			  }
			}
			data.notifys = temp_data;
			// mrc add end 知会 去重

			dom = tpl(detailTpl,{
				data:data,
				listType:_setting.listType,
				activekey:_setting.activekey,
				cdn_domain:cdn_domain,
				defaultFace:defaultFace,
				media_domain:media_domain,
				platid:platid
			});
			tpl.unregister('dateFormat');
			tpl.unregister('bytesToSize');
			tpl.unregister('getFileTypeExtIcon');
			tpl.unregister('checkNewline');

			this.dom = $.trim(dom);
			this.$dom = $(this.dom);

			//默认插入到开头位置
			_setting.target.html("").append(this.$dom);

			_setting.target.find(".mContent").css({
				"position":"fixed",
				"height":_setting.listType == 3 ? "100%" : "calc(100% - 56px)",
				"height": _setting.listType == 3 ? "100%" : "-webkit-calc(100% - 56px)",
				"top":"0",
				"width":"100%",
				"overflow-x":"hidden",
				"overflow-y":"auto",
				"-webkit-overflow-scrolling" : "touch"
			});
		},
		finish_callback:function(_setting){
			if(!_setting.callback || !tools.isFunction(_setting.callback.finish_callback)){return;}
			_setting.callback.finish_callback.apply(this);
		},
		appendBottomBtn:function(_setting){
			if(_setting.listType == 3){return;}

			var menu = new Array();
			var pThis = this;
			var workitemState = _setting.workitemState,
				isStart = _setting.isStart,
				workType = _setting.workType,
				isLinkSign = _setting.isLinkSign,
				formsetinstState = _setting.formsetinstState,
				formsetinstId = _setting.formsetinstId,
				listType = _setting.listType,
				id = _setting.id,
				workitemId = _setting.workitemId;

			if((workitemState == 1 || workitemState == 2 || workitemState == 3) && isStart == 0){
				menu.push({
					width: 30,
					name: "退回",
					iconClass: "icon-reply",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						// mrc add
						// 退回时, 隐藏冲突信息
						$('#conflict').hide();
						// mrc add end

						pThis.useFingerPrint(_setting.fingerPrint,"cancel",function(flag){
							if(!flag){return;}

							var params = pThis.getCheckData(_setting,"");
						
							//common.saveProcess(params,1,1,function(result,status){

								var c_opt = {};

								if(workType == 0){
									c_opt.placeholder = "请输入退回意见……";
									c_opt.default_t = "不同意";
								}else{
									c_opt.placeholder = "请输入备注信息(选填)";
								}
								
								layerPackage.ew_confirm_input.init(c_opt);

								var o = $("#comfirm_input");

								UiFramework.wxBottomMenu.init({
									target:o.find(".action"),
									menu: [
										{
											width: 30,
											name: "取消",
											iconClass: "icon-remove",
											btnColor: "w",
											id: null,
											click: function(self, obj, index) {
												o.addClass('hide');
											}
										},
										{
											width: 70,
											name: "发送",
											iconClass: "icon-plus-sign",
											btnColor: "g",
											id: null,
											click: function(self, obj, index) {
												var judgement = o.find("textarea").val();
												if($.trim(judgement) == "" && workType == 0){
													layerPackage.ew_alert({
														title:"请填写审批意见"
													});
													layerPackage.unlock_screen();
													return;
												}

												pThis.backStep(_setting,judgement);
											}
										}
									]
								});

							//});
						},id);

					}
				});
				
				if(workType == 0 && isLinkSign == 1){
					menu.push({
						width: 70,
						name: "签名",
						iconClass: "icon-pencil",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							pThis.useFingerPrint(_setting.fingerPrint,"apply",function(flag){
								if(!flag){return;}

								pThis.saveProcess(_setting,"保存中……",function(){
									pThis.btnAjax(_setting,signUrl,{
										"formsetinst_id":formsetinstId,
										"workitem_id":workitemId,
										"isIndex":isIndex,
										"menuType":menuType,
										"hash":hash
									},"sign");
								},1,1);
							},id);
						}
					});
				}else{
					menu.push({
						width: 70,
						name: workType == 1 ? "确认" : "同意",
						iconClass: " icon-ok",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							var params = pThis.getCheckData(_setting,"");
							// mrc 加入冲突
							var url = './wx/index.php?model=process&m=ajax&cmd=124';
							if(params){
								layerPackage.lock_screen();
								$.post(url,{
									data: JSON.stringify(params)
								},function(res){
									layerPackage.unlock_screen();
									var resp = JSON.parse(res);
									if(resp.errcode == 0){
										if(resp.flag == 1){
											has_coflict(resp.data);
										}else{
											no_conflict();
										}
									}else{
								        layerPackage.fail_screen(resp.errmsg);
									}
								});
							}

							function has_coflict(data){
								var desc = '';
								for(var i=0;i<data.length;i++){
								    if(i<5){
									    desc += '<div id="has_coflict">';
									    desc += '<p style="font-size: 14px;font-weight: bold;margin: 5px 0;padding:5px 0;border-top: 1px dashed #ccc">'+ data[i].name + '-' +data[i].time.replace('T',' ')+' - '+data[i].obj+'</p>';
									    desc += '</div>';
								    }
								}

								var obj = {
									title:'存在冲突',
									desc: desc,
									ok_text:"",
									cancel_text:"",
									ok_remove: 1,
									cancel_callback:function(){},
									ok_callback:function(){
									    no_conflict();
									}
								};
								layerPackage.ew_confirm(obj);
							}

							function no_conflict(){
								pThis.useFingerPrint(_setting.fingerPrint,"apply",function(flag){
									if(!flag){return;}
						
									common.saveProcess(params,1,1,function(result,status){

										var c_opt = {};
										c_opt.placeholder = "请输入同意意见……";
										c_opt.default_t = "同意";

										if(workType == 1){
											c_opt.placeholder = "请输入备注信息(选填)";
											c_opt.default_t = "";
										}

										layerPackage.ew_confirm_input.init(c_opt);

										var o = $("#comfirm_input");

										UiFramework.wxBottomMenu.init({
											target:o.find(".action"),
											menu: [
												{
													width: 30,
													name: "取消",
													iconClass: "icon-remove",
													btnColor: "w",
													id: null,
													click: function(self, obj, index) {
														o.addClass('hide');
													}
												},
												{
													width: 70,
													name: "发送",
													iconClass: "icon-plus-sign",
													btnColor: "g",
													id: null,
													click: function(self, obj, index) {
														var judgement = $.trim(o.find("textarea").val());
														if($.trim(judgement) == "" && workType == 0){
															layerPackage.ew_alert({
																title:"请填写审批意见"
															});
															layerPackage.unlock_screen();
															return;
														}

														pThis.getStep(_setting,judgement);
													}
												}
											]
										});

									});
								},id);
							}
						}
					});
				}
			}else if((workitemState == 1 || workitemState == 2 || workitemState == 3) && isStart == 1){
				menu.push({
					width: 30,
					name: "删除",
					iconClass: "icon-trash",
					btnColor: "r",
					id: null,
					click: function(self, obj, index) {
						pThis.delProcess(_setting,function(){
							common.toMineList();
						});

						return;
						layerPackage.ew_confirm({
							title:"确定要删除该工作项？",
							ok_callback:function(){
								layerPackage.lock_screen({
									"msg":"删除中"
								});
								pThis.btnAjax(_setting,delUrl,{
									"workitem_id":workitemId
								},"del")
							}
						});
					}
				});
				menu.push({
					width: 70,
					name: "发送",
					iconClass: "icon-plus-sign",
					btnColor: "g",
					id: null,
					click: function(self, obj, index) {
						pThis.getStep(_setting,"");
					}
				});
			}else{
				menu.push({
					width: 30,
					name: "知会",
					iconClass: "icon-comment-alt",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						var activekey = tools.getHrefParamVal("activekey") || 0;
						common.toNotifyPage(formsetinstId,listType,id,activekey);
						//window.location.href = "/wx/index.php?model=process&a=notify&degbug=1&formsetinstId="+formsetinstId+"&listType="+listType+"&id="+id;
					}
				});

				// mrc add
				menu.push({
					width: 40,
					name: "提醒未读",
					iconClass: "icon-comment-alt",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						if(!notify_is_click){
							notify_is_click = true;
							var notifys = data_info.notifys;
							var receivers = [];
							var receivers_del_has_read = [];
							var has_read = {};
							var not_read = {};

							for(var i=0;i<notifys.length;i++){
								var id = notifys[i].notify_id,
									name = notifys[i].notify_name;

								if(notifys[i].state == "1"){
									if(!not_read[id]){
										var obj = {
											receiver_id: id,
											receiver_name: name
										};
										receivers.push(obj);
										not_read[id] = 1; // 保存没有阅读的去重
									}
								}else{
									// console.log(notifys[i]);
									has_read[id] = 1; // 保存已经阅读的
								}
							}

							// 去掉已经阅读的
							for(var j=0;j<receivers.length;j++){
								var id = receivers[j].receiver_id;
								if(!has_read[id]){
									receivers_del_has_read.push(receivers[j]);
								}
							}

							if(receivers_del_has_read.length){
								var notify_url = '/wx/index.php?model=process&m=ajax&a=index&cmd=118';
								var data = {
									formsetinst_id: getUrlParam('id'),
									receivers: receivers_del_has_read
								};
								$.post(notify_url,{
									data: data
								},function(res){
									res = JSON.parse(res);
									if(res.errcode == 0){
										layerPackage.success_screen('知会成功!')
									}else{
										console.log(res.errmsg);
									}
									notify_is_click = false;
								});
							}else{
								layerPackage.fail_screen('未读人员为空!')
							}
						}

						function getUrlParam(key){
							var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
						    var r = window.location.search.substr(1).match(reg);
							if (r!=null) return unescape(r[2]); return null;
						}
					}
				});
				// mrc add

				if(formsetinstState == 1){
					menu.push({
						width: 30,
						name: "催办",
						iconClass: "icon-bell",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							layerPackage.lock_screen({
								"msg":"催办中"
							});
							pThis.btnAjax(_setting,urgeUrl,{
								"formsetinst_id":formsetinstId
							},"urge");
						}
					});
				}else if(formsetinstState == 2){
					menu[0].width = 50;
					menu[1].width = 50;
				}
			}

			UiFramework.wxBottomMenu.init({
				target:_setting.target,
				menu: menu
			});
		},
		backSetpHtml:function(){
			var html = new Array();
			html.push('<div class="modal-body">');
			html.push('<span class="icon-ok-sign select_work" style="font-size: 25px;color: green;position:relative;top:5px;" val="1"></span>');
			html.push('&nbsp;&nbsp;退回到开始步骤<br>');
			html.push('<span class="icon-ok-sign icon-circle-blank select_work" style="font-size: 25px;color: green;position:relative;top:5px;" val="2"></span>');
			html.push('&nbsp;&nbsp;退回到上一步骤');
			html.push('</div>');

			return html.join('');
		},
		backStep:function(_setting,judgement){
			var pThis = this;
			var index = 0;
			layerPackage.ew_confirm({
				title:"",
				desc:pThis.backSetpHtml(),
				callback:function(obj){
					obj.find('.select_work').click(function (){
						$('.select_work').addClass("icon-circle-blank");
						$(this).toggleClass("icon-circle-blank");
						$(this).css("color","green");

						index = $(this).index();
					});
				},
				ok_callback:function(obj){
					layerPackage.lock_screen({
						"msg":"退回中"
					});

					var url = index == 0 ? backStartUrl : backPrevUrl;
					var ctrlName = index == 0 ? "backStart" : "backPrev";

					var params = pThis.getCheckData(_setting,judgement);
					
					common.saveProcess(params,1,0,function(result,status){
							pThis.btnAjax(_setting,url,{
								"workitem_id":_setting.workitemId,
								"judgement":judgement
							},ctrlName,function(result, status){
								layerPackage.unlock_screen();
								if(result.errcode == 0){
									common.toDealList();
								}else if(result.errcode == 1){
									//122接口
									pThis.getSomeDealList(_setting,result.info,judgement);
								}else{
									layerPackage.fail_screen(result.errmsg);
								}
							})
					});
				}
			});
		},
		getSetpHtml:function(){
			var html = new Array();
			html.push('<div class="modal-body">');
			html.push('<span class="icon-ok-sign select_callback icon-circle-blank" style="font-size: 25px;color: green;position:relative;top:5px;" val="1"></span>');
			html.push('&nbsp;&nbsp;发送到退回步骤<br>');
			html.push('<span class="icon-ok-sign select_callback" style="font-size: 25px;color: green;position:relative;top:5px;" val="2"></span>');
			html.push('&nbsp;&nbsp;发送到下一步骤');
			html.push('</div>');

			return html.join('');
		},
		getStep:function(_setting,judgement){
			var pThis = this;
			var index = 2;

			if(_setting.isReturnBack == 1){
				layerPackage.ew_confirm({
					title:"",
					desc:pThis.getSetpHtml(),
					callback:function(obj){
						obj.find('.select_callback').click(function (){
							$('.select_callback').addClass("icon-circle-blank");
							$(this).toggleClass("icon-circle-blank");
							$(this).css("color","green");

							index = $(this).index();
						});
					},
					ok_callback:function(obj){
						layerPackage.lock_screen({
							"msg":"发送中……"
						});

						if(_setting.isStart == 0){
							if(judgement == "" && _setting.workType == 0){
								layerPackage.ew_alert({
									title:"请填写审批意见"
								});
								layerPackage.unlock_screen();
								return;
							}
						}

						var params = pThis.getCheckData(_setting,judgement);

						common.saveProcess(params,1,1,function(result,status){
							if(index == 0){
								pThis.btnAjax(_setting,goToBackUrl,{
									"workitem_id":_setting.workitemId,
									"judgement":judgement
								},"goToBack");
							}else{
								pThis.toSendPage(_setting.workitemId);
							}
						});
					}
				});
			}else{
				var params = pThis.getCheckData(_setting,judgement);
				
				common.saveProcess(params,1,1,function(result,status){
					pThis.toSendPage(_setting.workitemId);
				});
				
			}
		},
		toSendPage:function(workitemId){
			layerPackage.lock_screen({
				"msg":"发送中……"
			});

			common.toSendPage("&workitemId="+workitemId);
			//window.location.href = "/wx/index.php?model=process&a=sendPage&debug=1&workitemId="+workitemId;
		},
		getCheckData:function(_setting,judgement){
			var data = _setting.data;

			var params = {
				"form_id":data.formsetinst.form_id,
				"form_name":data.formsetinst.form_name,
				"work_id":_setting.work_id,
				"form_vals":common.getFormData(1),
				//Edit
				"workitem_id":_setting.workitemId,
				"formsetInst_id":_setting.formsetinstId,
				"judgement":judgement,
				"work_node_id":data.workitem.work_node_id,
				"file_type":data.workitem.file_type,
				"is_other_proc":_setting.isOtherProc
			};

			return params;
		},
		someDealListHtml:function(data){
			var callback_workitem = data.back_item_list;

			var html = new Array();
			html.push('<div class="modal-body">');
			for(var i = 0;i < callback_workitem.length;i++){
				var time = tools.getNowDate(callback_workitem[i]["complete_time"]*1000);
				html.push("<span class='icon-ok-sign icon-circle-blank select_people' style='font-size: 25px;color: green;position:relative;top:5px;' workitem_id='"+callback_workitem[i]["item_id"]+"' workitem_name='"+callback_workitem[i]["item_name"]+"' handler_name='"+callback_workitem[i]["handler_name"]+"'></span>");
				html.push("&nbsp;&nbsp;["+callback_workitem[i]["item_name"]+"]"+callback_workitem[i]["handler_name"]+"于"+time+"发送<br>");
			}
			html.push('</div>');

			return html.join('');
		},
		getSomeDealList:function(_setting,data,judgement){
			var pThis = this;
			layerPackage.ew_confirm({
				title:"",
				desc:pThis.someDealListHtml(data),
				ok_remove:0,
				callback:function(obj){
					obj.find('.select_people').click(function (){
						$('.select_people').addClass("icon-circle-blank");
						$(this).toggleClass("icon-circle-blank");
						$(this).css("color","green");
					});
				},
				ok_callback:function(obj,selfObj){
					var handler_name ="";
					var workitem_name = "";
					var workitem_id = "";
					var count = 0;
					obj.find(".select_people").each(function(){
						if(!$(this).hasClass("icon-circle-blank")){
							workitem_id = $(this).attr("workitem_id");
							workitem_name = $(this).attr("workitem_name");
							handler_name = $(this).attr("handler_name");
							count++;
						}
					});
					if(count!=1){
						layerPackage.ew_alert({
							"title":'请选择退回的工作项'
						});
						return false;
					}
					var params = {};
					params.workitem_id = data.workitem_id;
					params.callback_workitem_id = workitem_id;
					params.judgement = judgement;

					selfObj.remove();

					layerPackage.lock_screen();
					
					pThis.btnAjax(_setting,backSelectUrl,params,"backSelect");
				}
			});
		},
		appendEditBottonBtn:function(_setting){
			var menu = new Array(),pThis = this;

			menu[0] = {
				width: 30,
				name: "删除",
				iconClass: "icon-trash",
				btnColor: "r",
				id: null,
				click: function(self, obj, index) {
					pThis.delProcess(_setting,function(){
						common.toMineList(2);
					});
				}
			};

			menu[1] = {
				width: 30,
				name: "保存",
				iconClass: "icon-save",
				btnColor: "w",
				id: null,
				click: function(self, obj, index) {
					pThis.saveProcess(_setting,"保存中……",function(){
						common.toMineList(2);
					},0);
				}
			};

			menu[2] = {
				width: 40,
				name: "发送",
				iconClass: "icon-plus-sign",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					layerPackage.unlock_screen();

					var index = 2;

					if(_setting.isReturnBack == 1){
						layerPackage.ew_confirm({
							title:"",
							desc:pThis.getSetpHtml(),
							callback:function(obj){
								obj.find('.select_callback').click(function (){
									$('.select_callback').addClass("icon-circle-blank");
									$(this).toggleClass("icon-circle-blank");
									$(this).css("color","green");

									index = $(this).index();
								});
							},
							ok_callback:function(obj){
								layerPackage.lock_screen({
									"msg":"发送中……"
								});

								if(index == 0){
									/*var params = pThis.getCheckData(_setting,"");
						
									common.saveProcess(params,1,1,function(result,status){
										pThis.btnAjax(_setting,goToBackUrl,{
											"workitem_id":_setting.workitemId,
											"judgement":""
										},"goToBack");
									});*/
									pThis.saveProcess(_setting,"发送中……",function(result,status){
										pThis.btnAjax(_setting,goToBackUrl,{
											"workitem_id":_setting.workitemId,
											"judgement":""
										},"goToBack");
									},1,1,1);
								}else{
									pThis.toSendPage(_setting.workitemId);
								}
							}
						});
					}else{
						pThis.saveProcess(_setting,"发送中……",function(result,status){
							var workitem_id = result.info.workitem_id,
							formsetInst_id = result.info.formsetInst_id;
							common.toSendPage("&workitemId="+workitem_id+"&formsetInstId="+formsetInst_id+"&list=1");
						},1,1,1);
					}
				}
			};

			UiFramework.wxBottomMenu.init({
				target:_setting.target,
				menu: menu
			});
		},
		delProcess:function(_setting,callback){
			var pThis = this;
			layerPackage.ew_confirm({
				title:"确定要删除该工作项？",
				ok_callback:function(){
					layerPackage.lock_screen({
						"msg":"删除中"
					});
					pThis.btnAjax(_setting,delUrl,{
						"workitem_id":_setting.workitemId
					},"del",function(){
						layerPackage.unlock_screen();
						if(!callback){return;}
						callback();
					});
				}
			});
		},
		saveProcess:function(_setting,msg,callback,isEdit,isCheckData,canBeChange){
			layerPackage.lock_screen({
				"msg":msg||""
			});
			var data = _setting.data;
			console.log(data.formsetinst.form_id);
			var params = {
				"form_id":data.formsetinst.form_id,
				"form_name":data.formsetinst.form_name,
				"work_id":data.formsetinst.work_id,
				"form_vals":common.getFormData(isEdit,canBeChange),
				//Edit
				"workitem_id":data.workitem.id,
				"formsetInst_id":data.formsetinst.id,
				"judgement":"",
				"work_node_id":data.workitem.work_node_id,
				"file_type":data.workitem.file_type,
				"is_other_proc":_setting.isOtherProc
			};

			common.saveProcess(params,isEdit,isCheckData,function(result,status){
				if(!callback){return;}
				callback(result,status);
			});
		},
		//是否使用指纹
		useFingerPrint:function(fingerPrint,type,callback,id){
			var ua= window.navigator.userAgent.toLowerCase();
			if (ua.indexOf('wxwork') !== -1 || ua.indexOf('wechatdevtools') !== -1) {
				callback(true)
				return false
			}

			//PC微信
			if((platid == 4 || platid ==5) && fingerPrint == "1"){
				//弹出二维码
				var pcWxCode = "";
				if(type == "cancel"){
					pcWxCode = 0;
				}else if(type == "apply"){
					pcWxCode = 1;
				}

				var html = new Array();
				html.push('<img width="126px" height="126px" src="/wx/index.php?model=index&m=app&a=qrcode&id='+id+'&pcWxCode='+pcWxCode+'" alt="" />');
				html.push('<p class="fs15 mt15">微信扫描二维码，完成指纹识别</p>');
				html.push('<p class="fs12 c-4a mt5">你已开启指纹识别功能，需要完成指纹识别才能完成</p>');

				layerPackage.ew_alert({
					title:html.join(''),
					action_desc:'取消',
					top:40
				});

				callback(false);
				return false;
			}

			var pThis = this;
			//指纹是否开启
			if(fingerPrint == 1){
				GenericFramework.init(wxJsdkConfig,1);
				GenericFramework.getSupportSoter(function(canSupport){
					//手机支持指纹
					if(canSupport){
						GenericFramework.requireSoter(function(res,state){
							if(state){
								tools.ajaxJson({
									url:checkSoterUrl,
									data:{
										"data":{
											"json_str":res.result_json,
											"sign_str":res.result_json_signature
										}
									},
									callback:function(result,status){
										$(".alert_screen").remove();

										if(result.errcode !=0){
											pThis.showSoterErrorMsg("",type);

											callback(false);
											return;
										}

										callback(true);
									}
								});
							}else{
								//系统已锁屏
								if(res.err_code == 90011){
									layerPackage.ew_alert({
										"title":"<p class='fs20 fsn'>请锁屏后重来</p><p class='mt15 fs12 c-red'>由于多次识别失败，手机指纹已锁定</p>"
									});
								}
							}
						});
					}else{
						pThis.showSoterErrorMsg("手机不支持指纹识别!",type);
					}
				});
			}else{
				callback(true);
			}
		},
		//----------指纹识别设置-----------
		getErrorHtml:function(errmsg){
			var html = new Array();

			html.push('<i class="errorIcon"></i>');
			html.push('<b class="fs20 lhn fsn block">指纹认证失败！</b>');
			html.push('<span class="c-red fs12 block mt5">');
			if(errmsg){
				html.push(errmsg);
			}else{
				html.push('暂时无法获取指纹信息，请重新尝试，<i class="fsn c-green restartSoter">重新录入</i>');
			}
			html.push('</span>');
			return html.join('');
		},
		showSoterErrorMsg:function(errmsg,type){
			var pThis = this;
			layerPackage.ew_alert({
				title:pThis.getErrorHtml(errmsg),
				action_desc:'<span class="c-black">我知道了</span>',
				top:27,
				callback:function(){
					
				}
			});
			
			if(errmsg =="" && type == "apply"){
				var alert = $(".alert_screen");
				alert.on("click",".restartSoter",function(){
					//pThis.useFingerPrint(1,type);
					$(".alert_screen").remove();
					$(".comfirm_input").find("#wx_bottom_menu").remove();
					$("#main-container > #wx_bottom_menu").find("li:last > div").click();
				});
			}
		}
	};

	return detail;
});