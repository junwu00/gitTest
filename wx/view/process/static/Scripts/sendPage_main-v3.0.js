define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/sendPage-v3.0.html',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'common:widget/lib/scFormCtrl/scFormCtrl-v2.0'
],function($,t,UiFramework,tpl,senPageTpl,depts_users_select,ScFormCtrl) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
		goToBackUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=116",
		base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
		dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
		user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
		user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
		init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
        get_common_url = "/wx/index.php?model=index&m=index&a=get_recent_user_depts",
        save_common_url = "/wx/index.php?model=index&m=index&a=save_recent_user_depts",
		signUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=119";

	//发送逻辑处理
	var sendPage = {
		init:function(common,params){
			this.common = common;

			this.workitemId = params.workitem_id;
			this.formsetInstId = params.formsetInst_id;
			this.list = params.list;
			this.isIndex = params.isIndex;
			this.hash = params.hash;

			this._setting = params._setting;
			this.detail = params.detail;

			this.isUrl = params.isUrl; //是跳转过来的

			this.noReturnback = params.noReturnback; //强制将退回步骤禁止

			this.getNextNode(this.workitemId);
		},
		bindDom:function(){
			var pThis = this;
			$(window).on('hashchange',function(){
				var hash = window.location.hash;
				var sendPageObj = $("body").find("#sendPage");
				var hasEndSelectPageObj = $("body").find("#hasEndSelect");
				var judgementPageObj = $("body").find("#judgementPage");
				var hasReturnbackPageObj = $("body").find("#hasReturnbackPage");

				//删除弹出框
				$(".alert_screen").remove();

				if(pThis.isUrl){
					if(hash != pThis.hash){
						sendPageObj.removeClass('hide');
					}else{
						sendPageObj.addClass('hide');
					}
				}else{
					if(hash != "#sendPage" 
						&& hash != "#hasEndSelect" 
						&& hash != "#judgementPage" 
						&& hash != "#hasReturnbackPage"){
						sendPageObj.addClass('hide');
						hasEndSelectPageObj.addClass('hide');
						judgementPageObj.addClass('hide');
						hasReturnbackPageObj.addClass('hide');
					}else{
						if(hash == "#sendPage"){
							sendPageObj.removeClass('hide');
							hasEndSelectPageObj.addClass('hide');
							judgementPageObj.addClass('hide');
						}
						
						if(hash == "#hasEndSelect"){
							hasEndSelectPageObj.removeClass('hide');
							sendPageObj.addClass('hide');
						}
						
						if(hash == "#judgementPage"){
							judgementPageObj.removeClass('hide');
							sendPageObj.addClass('hide');
						}

						if(hash == "#hasReturnbackPage"){
							hasReturnbackPageObj.removeClass('hide');
							judgementPageObj.addClass('hide');
							sendPageObj.addClass('hide');
							hasEndSelectPageObj.addClass('hide');
						}
					}
				}
			});
		},
		//选择节点
		chooseNode:function(data){
			var pThis = this;
			//自动发送
			if(data.is_send){
				pThis.autoSend(data,data.is_end);
				return;
			}

			//存在签名
			if(data.is_linksign == "1" && data.linksign_pic){
				pThis.hasPicPage(data);
				return;
			}

			//有退回步骤
			if(data.is_returnback == 1){
				pThis.hasReturnback(data);
				return;
			}

			//填写意见页面
			if(data.judgement_box == "1" || data.judgement_box == "2"){
				pThis.judgementPage(data);
				return;
			}

			//多个节点且没有结束节点
			if(!data.is_end && data.node_list.length > 0){
				pThis.selectSend(data);
			}
			//多个节点和结束节点
			else if(data.is_end && data.node_list.length > 0){
				pThis.hashEndSelect(data);
			}
			//只有结束节点
			else if(data.is_end && data.node_list.length == 0){
				pThis.autoSend(data,1);
			}
		},
		//下一个节点信息
		getNextNode:function(workitem_id,callback){
			var pThis = this;
			var sendPageObj = $("body").find("#sendPage");
			sendPageObj.remove();
			var hasEndSelectPageObj = $("body").find("#hasEndSelect");
			hasEndSelectPageObj.remove();

			t.ajaxJson({
				url:nextNodeUrl,
				data: {
					"data": {
						"workitem_id":workitem_id
					}
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						//layerPackage.fail_screen(result.errmsg);
						layerPackage.ew_alert({
							title:result.errmsg,
							action_desc:'ok,我知道了',
							top:40
						});
						return;
					}

					var data = result.info;
					console.log(data);

					if(pThis.noReturnback){
						data.is_returnback = 0;
					}

					pThis.chooseNode(data);
					/*
					//自动发送
					if(data.is_send){
						pThis.autoSend(data);
					}else{
						//有退回步骤
						if(data.is_returnback == 1){
							pThis.hasReturnback(data);
						}else{
							var judgement_box = data.judgement_box;

							if(judgement_box == "0"){
								//存在签名
								if(data.is_linksign == "1" && data.linksign_pic){
									pThis.hasPicPage(data);
									return;
								}
								//多个节点且没有结束节点
								if(!data.is_end && data.node_list.length > 0){
									pThis.selectSend(data);
								}
								//多个节点和结束节点
								else if(data.is_end && data.node_list.length > 0){
									pThis.hashEndSelect(data);
								}
								//只有结束节点
								else if(data.is_end && data.node_list.length == 0){
									pThis.autoSend(data,1);
								}
							}
							//填写意见页面
							else{
								pThis.judgementPage(data);
							}
						}
					}
					*/
					pThis.bindDom();

					layerPackage.unlock_screen();
				}
			});
		},
		//输入框意见处理
		judgementSolve:function(sendPageObj,data){
			var judgement_box = data.judgement_box;

			if(judgement_box == "0"){return;}

			var pThis = this,must = null,placeholder = null,val = null,title = null;

			if(judgement_box == "1"){
				must = 1;
				placeholder = "请输入同意意见……";
				val = "同意";
				title = "审批意见";
			}else{
				must = 0;
				placeholder = "请输入备注信息(选填)";
				val = "";
				title = "备注信息";
			}

			var dom = ScFormCtrl(pThis.common.getJudgementData(must,title,val,placeholder,"请填写审批意见"),{check:1});

			sendPageObj.find(".nodeName").before(dom);
		},
		//默认选中节点
		selectNodeId : null,
		selectIndex:0,
		sendBackInfo:null,
		bindEventDom:function(sendPageObj){
			var pThis = this;
			sendPageObj.find(".nodePartTitle").on("click",function(){
				var o = $(this),p_o = o.parent(".nodePart"),other_p_o = p_o.siblings('.nodePart');

				if(p_o.hasClass('current')){
					p_o.removeClass('current');
					p_o.find(".nodePartContent").addClass('hide');
					pThis.selectNodeId = null;
					pThis.selectIndex = 0;
					pThis.sendBackInfo = null;
					return;
				}

				pThis.selectNodeId = p_o.attr("data-id");
				pThis.selectIndex = p_o.attr("data-index");
				pThis.sendBackInfo = p_o.attr("data-sendback");

				p_o.addClass('current');
				other_p_o.removeClass('current');

				p_o.find(".nodePartContent").removeClass('hide');
				other_p_o.find(".nodePartContent").addClass('hide');
			});

			var nodePartObj = sendPageObj.find(".nodePart");
			if(nodePartObj.length == 1){
				nodePartObj.find(".nodePartTitle").click();
			}
		},
		//绑定按钮事件
		bindBtn:function(sendPageObj){
			var pThis = this;
			sendPageObj.find('.square-checkbox').each(function(){
				var o = $(this),pObj = o.parents(".user"),
				id = pObj.attr("data-id"),
				name = pObj.attr("data-name");
				pObj.data("cacheData",{
					id:id,name:name,status:false
				});

				if(pObj.hasClass('receiveArr')){
					UiFramework.btnPackage().checkBoxBtn.init({
						target: o,
						status: true,
						type: 2,
						callback: {
						},
						cancelEvent:true
					});

					pObj.data("cacheData").status = true;
				}else if(pObj.hasClass('selectArr')){
					UiFramework.btnPackage().checkBoxBtn.init({
						target: o,
						status: false,
						type: 2,
						callback: {
							on: function(obj, self) {
								pObj.data("cacheData").status = true;
							},
							off: function(obj, self) {
								pObj.data("cacheData").status = false;
							}
						}
					});
				}
			});
		},
		//绑定人员部门选择器
		bindUserDept:function(sendPageObj){
			var pThis = this;
			sendPageObj.find(".clickObj").on('click',function(){
				$(this).next(".getUser").click();
			});

			sendPageObj.find(".getUser").each(function(){
				var o = $(this);
				var pObj = o.parents('.nodePartContent');

				var dept = new depts_users_select().init({
					target:o,
					select:{},
					type :2,
					max_select:0,
					base_dept_url : base_dept_url,
					dept_url : dept_url,
					user_url : user_url,
					user_detail : user_detail,
					init_selected_url : init_selected_url,
					get_common_url : get_common_url,
            		save_common_url : save_common_url,
					canEdit:1,
					ret_fun:function(self){
						var data = self.get_depts_users();
						console.log(data);

						//清除之前已有选项
						pObj.find("div.select").remove();
						pObj.find("div.w-xLine").remove();

						for (var i = 0; i < data.length; i++) {		
							var html = new Array();
							html.push('<div class="h46 user select" data-id="'+data[i].id+'">');
							html.push('<div class="fl square-checkbox mr6"></div>');
							html.push('<img src="'+data[i].pic_url+'" alt="" class="mr6 fl" onerror="this.src=\''+defaultFace+'\'"/>');
							html.push('<span class="selectName fs17 lhn">'+data[i].name+'</span>');
							html.push('</div>');
							html.push('<div class="wxLine"></div>');

							var dom = $($.trim(html.join('')));

							UiFramework.btnPackage().checkBoxBtn.init({
								target:dom.find('.square-checkbox'),
								status:true,
								type:2,
								callback:{
									on:function(obj,self){
										obj.parents(".user").data("cacheData").status = true;
									},
									off:function(obj,self){
										obj.parents(".user").data("cacheData").status = false;
									}
								}
							});

							dom.data("cacheData",{id:data[i].id,name:data[i].name,status:true})

							pObj.prepend(dom);
						};
					}
				});
			});
		},
		//添加底部信息
		addBottom:function(sendPageObj){
			var pThis = this,
				list = pThis.list,
				formsetInstId = pThis.formsetInstId,
				workitemId = pThis.workitemId,
				isSend = pThis.isSend,
				common = pThis.common;

			var clickStatus = true;

			var a = {
				width: 30,
				name: "上一步",
				iconClass: "rulericon-turnback",
				btnColor: "w",
				id: null,
				click: function(self, obj, index) {
					if (list == 2 || list == 1) {
						window.history.back();
					} else {
						common.toDetail(workitemId);
					}
				}
			};

			UiFramework.wxBottomMenu.init({
				noFixed:1,
				target:sendPageObj,
				menu: [{
					width: 100,
					name: "发送",
					iconClass: "rulericon-sendto",
					btnColor: "g",
					id: null,
					click: function(self, obj, index) {
						//自动发送则禁止点击
						/*if(isSend == 1 && !clickStatus){
							return;
						}*/

						var count = 0;
						var data = {};

						var  textareaObj =  sendPageObj.find(".scCtrl");
						if(pThis.detail && textareaObj.length > 0 && textareaObj.attr('data-checked') == "0"){
							layerPackage.ew_alert({
								title:"请填写审批意见"
							});
							return;
						}
						if(textareaObj.length > 0){
							data.judgement = textareaObj.data("cacheData").val;
						}else{
							data.judgement = pThis.judgement_msg;
						}
						
						data.workitem_id = workitemId;
						data.work_nodes = new Array();
						var receivers = new Array(),receiverName = new Array();

						var selectNodeObj = sendPageObj.find(".nodePart[data-id="+pThis.selectNodeId+"][data-index="+pThis.selectIndex+"] .nodePartContent");

						if(selectNodeObj.length == 0){
							layerPackage.ew_alert({
								"title":"请选择发送节点"
							});
							return false;
						}

						var work_node_name = selectNodeObj.eq(0).attr("data-name");

						selectNodeObj.each(function(){
							var work_node = {};
							work_node.id = $(this).attr("data-id");
							work_node.receiver_arr = new Array();


							$(this).find(".user").each(function(){
								var data = $(this).data("cacheData");

								if(data.status == true){
									var receiver = new Object();
									receiver.receiver_id = data.id;
									receiver.receiver_name = data.name;
									work_node.receiver_arr.push(receiver);
									count++;

									receiverName.push(data.name);
								}
							});

							if(work_node.receiver_arr.length>0){
								data.work_nodes.push(work_node);
							}
						});

						if(count==0){
							layerPackage.ew_alert({
								"title":"请选择步骤接收人！"
							});
							return false;
						}

						common.send(data,function(){
							if(list == 2 || list==1){
								common.toMineList();
							}
							else{
								common.toDealList();
							}
						});
					}
				}]
			});
		},
		//自动发送
		autoSend:function(data,isEnd,judgement){
			try{
				//获取固定接收人
				var n = data.node_list,common = this.common;
				var work_nodes = new Array();

				if(!isEnd && n.length > 0){
					work_nodes.push({
						id:n[0].id,
						receiver_arr:n[0].receiver_arr
					});
				}

				if(isEnd){
					work_nodes = [{
						id:data.end_node_id,
						receiver_arr:[]
					}];
				}

				var d = {
					workitem_id:data.workitem_id,
					work_nodes:work_nodes,
					judgement:judgement
				};

				common.send(d,function(){
					common.toDealList();	
				});
			}catch(e){
				console.log("自动发送");
				console.log(e);
			}
		},
		//多个节点且没有结束节点
		selectSend:function(data,noChangeHash,pageObj,linksign){
			$("body").find("#sendPage").remove();
			var pThis = this;

			if(!noChangeHash){
				window.location.hash = "sendPage";
			}

			if(!linksign){
				var html = new Array();
				html.push('<div id="sendPage" class="w_100 h_100 bg-gray">');
				html.push('</div>');

				$("body").append(html.join(''));
			}

			pThis.data = data;
			pThis.isSend = data.is_send;
			pThis.workitemId = data.workitem_id;
			pThis.node_list = data.node_list;
			pThis.isEnd = data.is_end;
			pThis.isReturnBack = data.is_returnback;

			var dom = tpl(senPageTpl,{
				data:data,
				cdn_domain:cdn_domain,
				defaultFace:defaultFace,
				media_domain:media_domain,
				sendPage:1
			});

			var sendPageObj = pageObj ? pageObj :  $("body").find("#sendPage");

			if(linksign){
				sendPageObj.append($($.trim(dom)));
			}else{
				sendPageObj.html($($.trim(dom)));
			}

			//输入框意见处理
			pThis.judgementSolve(sendPageObj,data);

			if(data.node_list && data.node_list.length > 1){
				//绑定基础事件
				pThis.bindEventDom(sendPageObj);
			}else{
				pThis.selectNodeId = data.node_list[0].id;
			}

			//绑定人员部门选择器
			pThis.bindUserDept(sendPageObj);

			//绑定按钮事件
			pThis.bindBtn(sendPageObj);

			//添加底部信息
			pThis.addBottom(sendPageObj);

			layerPackage.unlock_screen();
		},
		//多个节点和结束节点
		hashEndSelect:function(data){
			$("body").find("#hasEndSelect").remove();

			var pThis = this;
			if(!pThis.isUrl){
				window.location.hash = "hasEndSelect";
			}

			var html = new Array();

			html.push('<div id="hasEndSelect" class="w_100 h_100 bg-gray">');
			html.push('</div>');

			$("body").append(html.join(''));

			var dom = tpl(senPageTpl,{
				data:data,
				hashEndSelect:1
			});

			var pageObj = $("body").find("#hasEndSelect");
			pageObj.html($($.trim(dom)));

			//结束审批
			pageObj.on("click","#endNode",function(){
				pThis.autoSend(data,1);
			});

			//其他步骤
			pageObj.on("click","#otherNode",function(){
				pThis.selectSend(data);
			});
		},
		//填写意见页面
		judgementPage:function(data){
			$("body").find("#judgementPage").remove();

			var pThis = this;
			window.location.hash = "judgementPage";

			var html = new Array();

			html.push('<div id="judgementPage" class="w_100 h_100 bg-gray">');
			html.push('<div class="nodeName">');
			html.push('</div>');
			html.push('</div>');

			$("body").append(html.join(''));

			var pageObj = $("body").find("#judgementPage");

			pThis.judgementSolve(pageObj,data);

			pThis.addJudgementPageBottom(pageObj,data);
		},
		addJudgementPageBottom:function(pageObj,data,linksign){
			var pThis = this;

			//多个节点且没有结束节点
			if(!data.is_end && data.node_list.length > 0){
				if(!linksign){
					pThis.selectSend(data,1,pageObj);
				}else{
					pThis.selectSend(data,1,pageObj,linksign);

					/*UiFramework.wxBottomMenu.init({
						noFixed:1,
						target:pageObj,
						menu: [{
							width: 100,
							name: "发送",
							iconClass: "",
							btnColor: "g",
							id: null,
							click: function(self, obj, index) {
								pThis.selectSend(data);
							}
						}]
					});*/
				}
			}
			//多个节点和结束节点
			else if(data.is_end && data.node_list.length > 0){
				//pThis.hashEndSelect(data);

				UiFramework.wxBottomMenu.init({
					noFixed:1,
					target:pageObj,
					menu: [{
						width: 100,
						name: "选择其它步骤",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							var judgementObj = pThis.checkJudgement(pageObj);
							if(!judgementObj){return;}

							pThis.judgement_msg =  judgementObj.length == 0 ? "" : judgementObj.data("cacheData").val;

							data.judgement_box = "0";
							pThis.selectSend(data);
						}
					},{
						width: 100,
						name: "结束审批",
						iconClass: "",
						btnColor: "w",
						id: null,
						click: function(self, obj, index) {
							var judgementObj = pThis.checkJudgement(pageObj);
							if(!judgementObj){return;}

							var val = judgementObj.length == 0 ? "" : judgementObj.data("cacheData").val;

							pThis.autoSend(data,1,val);
						}
					}]
				});
			}
			//只有结束节点
			else if(data.is_end && data.node_list.length == 0){
				UiFramework.wxBottomMenu.init({
					noFixed:1,
					target:pageObj,
					menu: [{
						width: 100,
						name: "结束审批",
						iconClass: "",
						btnColor: "g",
						id: null,
						click: function(self, obj, index) {
							var judgementObj = pThis.checkJudgement(pageObj);
							if(!judgementObj){return;}

							var val = judgementObj.length == 0 ? "" : judgementObj.data("cacheData").val;

							pThis.autoSend(data,1,val);
						}
					}]
				});
			}
		},
		//发送到退回步骤
		sendBack:function(params){
			t.ajaxJson({
				url: goToBackUrl,
				data: {
					"data": {
						"workitem_id":params.workitem_id,
						"judgement":params.judgement || ""
					}
				},
				callback: function(result, status) {
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.unlock_screen();//遮罩end
						//layerPackage.fail_screen(result.errmsg);
						layerPackage.ew_alert({
							title:result.errmsg,
							action_desc:'ok,我知道了',
							top:40
						});
						return;
					}

					layerPackage.success_screen(result.errmsg);

					setTimeout(function(){

						if(params.isIndex){
							params.common.toIndexDealList();
						}else{
							window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
						}

					},1500);
					
				}
			});
		},
		//有退回步骤的页面
		hasReturnback:function(data){
			$("body").find("#hasReturnbackPage").remove();

			var pThis = this;
			window.location.hash = "hasReturnbackPage";

			var html = new Array();

			html.push('<div id="hasReturnbackPage" class="w_100 h_100 bg-gray">');
			html.push('<div class="nodeName" style="margin-bottom:20px;">');
			html.push('</div>');
			html.push('</div>');

			$("body").append(html.join(''));

			var pageObj = $("body").find("#hasReturnbackPage");

			pThis.judgementSolve(pageObj,data);

			pThis.addHasReturnbackPageBottom(pageObj,data);
		},
		addHasReturnbackPageBottom:function(pageObj,data,linksign){
			var pThis = this;

			UiFramework.wxBottomMenu.init({
				noFixed:1,
				target:pageObj,
				menu: [{
					width: 100,
					name: "发送到退回步骤",
					iconClass: "",
					btnColor: "g",
					id: null,
					click: function(self, obj, index) {
						var judgementObj = pThis.checkJudgement(pageObj);
						if(!judgementObj){return;}

						var val = judgementObj.length == 0 ? "" : judgementObj.data("cacheData").val;

						pThis.sendBack({
							workitem_id:data.workitem_id,
							judgement:val,
							isIndex : 1,
							common : pThis.common
						});
					}
				},{
					width: 100,
					name: "重新发送",
					iconClass: "",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						var judgementObj = pThis.checkJudgement(pageObj);
						if(!judgementObj){return;}

						pThis.judgement_msg =  judgementObj.length == 0 ? "" : judgementObj.data("cacheData").val;

						var d = $.extend(true, {}, data);
						d.is_returnback = 0;

						d.judgement_box = "0";

						if(linksign){
							pThis.isUrl = 0;
						}
						
						pThis.chooseNode(d);
					}
				}]
			});
		},
		//检查 judgement 的状态
		checkJudgement:function(pageObj){
			var judgementObj = pageObj.find(".scCtrl");

			if(judgementObj.length > 0 && judgementObj.attr('data-checked') == "0"){
				layerPackage.ew_alert({
					title:"请填写审批意见"
				});
				return;
			}

			return judgementObj;
		},
		//有签名图片的页面
		hasPicPage:function(data){
			$("body").find("#hasPicPage").remove();

			var pThis = this;
			//window.location.hash = "hasPicPage";

			var html = new Array();

			html.push('<div id="hasPicPage" class="w_100 h_100 bg-gray">');
			html.push('<div class="linksign bg-white">');
			html.push('<span class="fs17 lhn fl">你的签字</span>');
			html.push('<img src="'+data.linksign_pic+'" alt="你的签字" class="fl"/>');
			html.push('<i class="rulericon-angle_right fr"></i>');
			html.push('</div>');
			html.push('</div>');

			$("body").append(html.join(''));

			var pageObj = $("body").find("#hasPicPage");

			pThis.addPicPageBottom(pageObj,data);
		},
		addPicPageBottom:function(pageObj,data){
			//签字功能
			var pThis = this;

			var hash = window.location.hash,
				isIndex = t.getUrlParam("isIndex"),
				formsetinstId = data.formsetinstId || t.getUrlParam("formsetinstId");

			//重新签名
			pageObj.on("click",".linksign",function(){
				t.ajaxJson({
					url:signUrl,
					data:{
						"data":{
							"formsetinst_id":formsetinstId,
							"workitem_id":data.workitem_id,
							"isIndex":isIndex,
							"hash":hash,
							"resign":1
						}
					},
					callback:function(result,status){
						if(result.errcode == 0){
							window.location.href = result.info.redirect_url;
						}
					}
				});
			});

			data.is_linksign = "0";

			if(data.is_returnback == 1){
				this.addHasReturnbackPageBottom(pageObj,data,1);
			}else{
				this.addJudgementPageBottom(pageObj,data,1);
			}

		}
	};

	return sendPage;
});