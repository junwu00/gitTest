require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'process:static/Scripts/process_common-v2.0',
	'process:static/Scripts/sendPage_main-v3.0'
],function($,t,UiFramework,common,sendPage) {
	sendPage.init(common,{
		workitem_id : t.getHrefParamVal("workitemId") || 1,
		formsetInst_id : t.getHrefParamVal("formsetInstId") || 0,
		hash:window.location.hash,
		isIndex:t.getHrefParamVal("isIndex") || 0,
		isUrl:1
	});
});