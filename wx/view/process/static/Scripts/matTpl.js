require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/vue',
  'common:widget/lib/vue-router',
  'common:widget/lib/text!'+buildView+'/process/Templates/matTpl.html',
],function($,t,UiFramework,Vue, Router, matTpl) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	
  Vue.use(Router)

  var index = {
    template: matTpl,
    name: 'default',
    data() {
      return {
        loaded: 0,
        fix_data: {
          "form_describe": "",
          "form_name": "",
          "history_id": "0",
          "type": "1",
          "icon": "c-blue rulericon3-e905",
          "tag": []
        },
        input_data: {},
        info: {}
      }
    },
    methods: {
      save () {
        var that = this
        var params = this.fix_data
        var tmp_input_data = JSON.parse(JSON.stringify(this.input_data))

        for (var key in tmp_input_data) {
          if (tmp_input_data[key].the_value == '') {
            layerPackage.fail_screen('请先填写必要字段!')
            return false
          }
          if (tmp_input_data[key].type.indexOf('select') != -1) {
            tmp_input_data[key].opts = tmp_input_data[key].the_value
            tmp_input_data[key].opts_key = 1
            tmp_input_data[key].opts_nums = "1"
          }
        }
        params.inputs = tmp_input_data
        params.tag = this.info.tag
        
        if (!params.form_name || !params.form_describe) {
          layerPackage.fail_screen('请先填写必要字段!')
          return false
        }

        post('./wx/index.php?model=process&m=material&cmd=103', params, function(res) {
          layerPackage.success_screen('操作成功!')
          setTimeout(function() {
            // location.href = "./wx/index.php?app=index&a=index&state=1#pMyTpl"
            location.href = `/wx/index.php?model=process&m=index&a=jumpMaterial&showInputPage=${t.getUrlParam('showInputPage')}`
          }, 1000)
        })
      }
    },
    created() {
      var that = this
      post('./wx/index.php?model=process&m=material&cmd=102', {
        id: this.$route.params.id,
        type: 2
      }, function(res){
        if (res.data && res.data[0]) {
          that.loaded = 1
          that.info = res.data[0]
          that.input_data = res.data[0].inputs
          for (var key in that.input_data) {
            if (that.input_data[key].type.indexOf('select') != -1) {
              that.input_data[key].the_value = that.input_data[key].opts.split(',')[0]
            }
          }
        } else{
          layerPackage.ew_alert({'desc':'找不到对应的物资模板'})
          setTimeout(function(){
            history.go(-1)
          }, 1500)
        }
      })
    }
  }

  var router = new Router({
    scrollBehavior (to, from, savedPosition) { // 回到顶部
      return { x: 0, y: 0 }
    },
    routes: [
      {
        path: '/:id',
        component: index
      }
    ]
  })

  new Vue({
    el: '#main-container',
    router
  })

  function post (url, data, callback) {
    layerPackage.lock_screen({bgColor: 'rgba(0,0,0,0)'})
    $.post(url, {
      data: JSON.stringify(data) 
    }, function(res){
      var resp = JSON.parse(res)
      layerPackage.unlock_screen()
      if (resp.errcode == 0) {
        if (callback && typeof(callback) === 'function') {
          callback(resp)
        } else {
          console.log('some error occur or do nothing!')
        }
      }else{
        layerPackage.tips(resp.errmsg, 0);
      }

    })
  }
});