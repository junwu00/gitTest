require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/indexList.html'
],function($,t,UiFramework,tpl,listTemplate) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var listUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=101",
	getUsefulUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=102";

	//jqObj对象
	var container = $("#main-container");
	
	var indexList = {
		countArr:[],
		index:0,
		getList:function(url,callback){
			var pThis = this;

			t.ajaxJson({
				url: url,
				callback: function(result,status) {
					if(status == false && result == null){return;}

		            if(result.errcode != 0){t.console(result.errmsg,1);return;}

		            var data = result.info.data;

		            var dom = tpl(listTemplate,{
		                listData:data,
		                index:pThis.index
		            });

		            if(data.length >0){
		            	pThis.index = data.length;
		            }

		            var $dom = $($.trim(dom));

		            container.append($dom);

					if(!t.isFunction(callback)){return;}
					callback.apply(pThis);
				}
			});
		},
		bindDom:function(){
			var typeObj = container.find("section.typeObj"),
            clickObj = container.find("section.clickObj");

            var typeObjH = 25,clickObjH = 63,arr = this.countArr;

            var count = 0;
            for(var i = 1;i <= typeObj.length;i++){
            	var h = typeObjH + container.find("section.clickObj[data-part="+i+"]").length * clickObjH;
            	count += h;
            	arr.push(count);
            }

            $(window).scroll(function(e){
				var num = $(this).scrollTop();

				if(num == 0){
					container.find("section.typeObj:eq(0)").removeAttr('style');
					return;
				}
	
				for (var j = 0; j < arr.length; j++) {
					if(num < arr[j]){
						container.find("section.typeObj:eq("+j+")").attr("style","position:fixed;top:0;width:100%;").siblings('.typeObj').removeAttr('style');
						break;
					}
				};
			});

            clickObj.off().on("click",function(){
            	var id = $(this).attr("data-id");
            	//alert(id);
            	window.location.href = "/wx/index.php?model=process&a=create&debug=1&id="+ id;
            });
		}
	};

	//indexList.getList(listUrl,function(){
		indexList.getList(listUrl,function(){
			this.bindDom();
			UiFramework.bottomInfo.init(container,77);
			layerPackage.unlock_screen();
		});
	//});
	
});