require([
	'jquery',
	'common:widget/lib/tools'
],function($,t) {
	t.ajaxJson({
		url:"/wx/index.php?model=process&m=ajax&a=redirection",
		callback:function(result,status){
			if(result.errcode !=0){
				return;
			}

			window.location.href = result.url;
		}
	});
});