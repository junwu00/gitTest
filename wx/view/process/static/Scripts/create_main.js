require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!view/process/Templates/indexList.html',
	'process:static/Scripts/process_common',
	'process:static/Scripts/otherProcess'
],function($,t,UiFramework,tpl,listTemplate,common,otherProcess) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//jqObj对象
	var container = $("#main-container"),
	getProcessUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=104",
	getFormUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=103",
	saveOrEditUrl = "index.php?model=process&m=ajax&a=index&cmd=105",
	sendUrl = "index.php?model=process&m=ajax&a=index&cmd=106",
	nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
	id = t.getUrlParam("id");

	if(!id){id = 0;}

	var is_other_proc = null;

	//公用操作
	var commonCtrl = {
		link:function(params,callback){
			//特殊表单验证
			try{
				var flag = otherProcess.checkParams(is_other_proc);

				if(!flag){return;}
			}catch(e){
				console.log(e);
			}

			layerPackage.lock_screen({
				"msg":params.msg || ""
			});//遮罩start

			t.ajaxJson({
				url:params.url,
				data:{
					"data":JSON.stringify(params.data)
				},
				callback:function(result,status){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					if(!callback){return;}
					callback(result,status);
				}
			});
		},
		delLink:function(id){
			layerPackage.lock_screen();
			t.ajaxJson({
				url: removeDetailUrl,
				data: {
					"data": {
						"id": id
					}
				},
				callback: function(result, status) {
					if (result.errcode != 0) {
						layerPackage.ew_alert({
							desc: result.errmsg
						});
						return;
					}

					layerPackage.success_screen(result.errmsg);

					//window.location.href = "index.php?debug=1&app=advice&a=mineList&activekey=0";
				}
			});
		},
		getParams:function(isCheckData){
			if(!common.checkHeaderInput()){return;}

			var formInfoObj = container.find(".formInfo");

			var uploadFileObj = common.getUploadFileObj();
			if(uploadFileObj && uploadFileObj.checkUploadStatus()){
				layerPackage.fail_screen('当前还有文件正在上传');
				return;
			}

			var data = common.getFormData();
			// console.log(data);

			if(isCheckData && !common.checkApplyData(1,data)){return;}

			var params = {
				"form_id":formInfoObj.attr("data-id"),
				"form_name":common.getFormName(),
				"work_id":formInfoObj.find("input[name=work_id]").val(),
				"form_vals":data
				//修改传入参数
				//"workitem_id":""
				//"formsetInst_id":""
				//"judgement":""
			}

			if(uploadFileObj){
				params.files = uploadFileObj.getUploadFiles();
			}

			return params;
		},
		getSaveAjax:function(data){
			return $.ajax({
				url:saveOrEditUrl,
				type:"post",
				data:{
					"data":data
				}
			});
		}
	};

	var menu = new Array(),
	save_menu = {
		width: 30,
		name: "保存",
		iconClass: "icon-save",
		btnColor: "w",
		id: null,
		click: function(self, obj, index) {
			var params = commonCtrl.getParams();
			if(!params){return;}

			commonCtrl.link({
				msg:"保存中……",
				url:saveOrEditUrl,
				data:params
			},function(){
				common.storageClear();
				layerPackage.unlock_screen();
				common.toMineList(2);
			});
		}
	},
	submit_menu = {
		width: 70,
		name: "发送",
		btnColor: "g",
		iconClass: "icon-plus-sign",
		draftSendUrl: null,
		id: null,
		click: function(self, obj, index) {
			var params = commonCtrl.getParams(1);
			// mrc 加入冲突
			var url = './wx/index.php?model=process&m=ajax&cmd=124';
			if(params){
				layerPackage.lock_screen();
				$.post(url,{
					data: JSON.stringify(params)
				},function(res){
					layerPackage.unlock_screen();
					var resp = JSON.parse(res);
					if(resp.errcode == 0){
						if(resp.flag == 1){
							has_coflict(resp.data);
						}else{
							no_conflict();
						}
					}else{
				        layerPackage.fail_screen(resp.errmsg);
					}
				});
			}

			function has_coflict(data){
				var desc = '';
				for(var i=0;i<data.length;i++){
				    if(i<5){
					    desc += '<div id="has_coflict">';
					    desc += '<p style="font-size: 14px;font-weight: bold;margin: 5px 0;padding:5px 0;border-top: 1px dashed #ccc">'+ data[i].name + '-' +data[i].time.replace('T',' ')+' - '+data[i].obj+'</p>';
					    desc += '</div>';
				    }
				}

				var obj = {
					title:'存在冲突',
					desc: desc,
					ok_text:"",
					cancel_text:"",
					ok_remove: 1,
					cancel_callback:function(){},
					ok_callback:function(){
						no_conflict();
					}
				};
				layerPackage.ew_confirm(obj);
			}

			function no_conflict(){
				layerPackage.lock_screen();
				if(!params){return;}
				commonCtrl.link({
					msg:"发送中……",
					url:saveOrEditUrl,
					data:params
				},function(result,status){
					common.storageClear();
					layerPackage.unlock_screen();

					var workitem_id = result.info.workitem_id,
					formsetInst_id = result.info.formsetInst_id;

					common.toSendPage("&workitemId="+workitem_id+"&formsetInstId="+formsetInst_id+"&list=2");
				});
			}
		}
	};

	$.when($.ajax({
		url:getProcessUrl,
		type:"post",
		data:{
			"data":{
				"form_id":id
			}
		}
	}),$.ajax({
		url:getFormUrl,
		type:"post",
		data:{
			"data":{
				"form_id":id
			}
		}
	})).done(function(r1,r2){
		// try{
			if(r1[1] != "success" || r2[1] != "success"){return;}

			var r1Json = $.parseJSON(r1[0]),
			r2Json = $.parseJSON(r2[0]);

			if(r1Json.errcode != 0){layerPackage.fail_screen(r1Json.errmsg);return;}
			if(r2Json.errcode != 0){layerPackage.fail_screen(r2Json.errmsg);return;}


			//初始化
			common.init(container);

			container.append("<div class='mContent'></div>");
			var mContent = container.find(".mContent");

			//头部用户信息
			mContent.prepend(common.getHeaderPart(r1Json.info));

			var data = r2Json.info;
			console.log(data);
			//*
			// mrc hide
			// common.storageInit(id,data,function(dataObj){
			// 	data = dataObj;
			// });
			// mrc hide
			//*/
			//$storage.clearAll();

			//data.file_upload_type = 1;
            //表单主体信息
			mContent.append(common.getFormInfo(data));

			//var dataObj = common.testData();//console.log(dataObj);
			//表单控件信息
      for(var d in data.inputs){
				if(!data.inputs[d]){continue;}
				mContent.append(common.getFormPart(data.inputs[d]));
			}
			// mrc add
			// common.init_wuzhi_link_attr()
			// mrc add end
			/*-----特殊表单处理----*/
			otherProcess.init({
				is_other_proc:data.is_other_proc,
				target:container,
				data:data
			});

			is_other_proc = data.is_other_proc;

			saveOrEditUrl = otherProcess.getSaveOrEditUrl(data.is_other_proc,saveOrEditUrl);

			/*编辑*/
			container.find("table").on("click",".tr_edit",function(){
				var index = $(this).parent().index();
				var data = $(this).parents("table").parent().parent().data("cacheData");

				var otherParams = undefined;

				otherParams = {
					isDetail:1,
					jqObj:$(this).parents("table").parent().parent(),
					data:data
				};
				
				common.tableEditEvent(data,index - 1,undefined);
			});

			//其他非表单内容
			mContent.append(common.getOtherPart(data));

			//绑定公式插件
			common.bindCalc(data.inputs);

			//底部信息
			UiFramework.bottomInfo.init(mContent,77);

			//底部按钮
			menu.push(save_menu);
			menu.push(submit_menu);
			UiFramework.wxBottomMenu.init({
				target:container,
				menu: menu
			});

			mContent.css({
				"position":"fixed",
				"height":"calc(100% - 56px)",
				"height":"-webkit-calc(100% - 56px)",
				"top":"0",
				"width":"100%",
				"overflow-x":"hidden",
				"overflow-y":"auto",
				"-webkit-overflow-scrolling" : "touch"
			});

			// mrc add
			common.init_muselect_box($('.muselect_box'));
			// mrc add end
			layerPackage.unlock_screen();
		// }catch(e){
		// 	//console.log(e); // mrc add 帮助快速定位错误
		// 	layerPackage.fail_screen(e); 
		// }
	}).fail(function(){
		layerPackage.fail_screen("请求错误");
	});
});