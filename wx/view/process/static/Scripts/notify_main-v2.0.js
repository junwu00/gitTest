define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
],function($,t,UiFramework,depts_users_select) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	//layerPackage.lock_screen();//遮罩start

	//请求地址
	var notifyUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=118",
	base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
	dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
	user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
	user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
	init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
    get_common_url = "/wx/index.php?model=index&m=index&a=get_recent_user_depts",
    save_common_url = "/wx/index.php?model=index&m=index&a=save_recent_user_depts";

	var notify = {
		init:function(_setting,detailObj,common,activekey){
			this._setting = _setting;
			this.detailObj = detailObj;
			this.common = common;
			this.activekey = activekey;

			this.formsetinstId = _setting.formsetinstId;
			this.listType = _setting.listType;
			this.id = _setting.id;

			//获取知会节点信息
			this.getNotifyNode(_setting);
		},
		//获取知会节点信息
		getNotifyNode:function(){
			window.location.hash = "notifyPage";

			var pThis = this;
			var notifyPageObj = $("body").find("#notifyPage");

			if(notifyPageObj.length == 0){
				var html = new Array();

				html.push('<div id="notifyPage" class="w_100 h_100 bg-gray">');
				html.push('<div class="notifyPageContent">');

				html.push('<span class="bg-gray nodeName fs17 lhn c-88 block">知会</span>');
				html.push('<div class="wxLine"></div>');
				html.push('<div class="nodePartContent bg-white" data-id="1250055" data-isend="false" data-name="节点4">');
				html.push('<section class="autoH pl0 pt5">');
				html.push('<p class="fs17 c-green clickObj"> <i class="rulericon-add" style="margin-right: 4px;"></i>添加人员</p>');
				html.push('<div class="getUser fr col2 hideTxt tar h_100 hide deptUserTarget"></div>');
				html.push('</section>');
				html.push('</div>');

				html.push('</div>');
				html.push('</div>');

				$("body").append(html.join(''));

				this.bindDom();
				this.bindUserDept();
				this.addBottom();
			}else{
				notifyPageObj.removeClass('hide');
				return;
			}
		},
		//知会事件绑定
		bindDom:function(){
			var pThis = this;
			$(window).on('hashchange',function(){
				var hash = window.location.hash;
				var notifyPageObj = $("body").find("#notifyPage");

				if(hash != "#notifyPage"){
					notifyPageObj.addClass('hide');
				}else{
					notifyPageObj.removeClass('hide');
				}
			});
		},
		//绑定人员部门选择器
		bindUserDept:function(){
			var pThis = this;
			var notifyPageObj = $("body").find("#notifyPage");

			notifyPageObj.find(".clickObj").on('click',function(){
				$(this).next(".getUser").click();
			});

			var dept = new depts_users_select().init({
				target:notifyPageObj.find(".getUser"),
				select:{},
				type :2,
				max_select:0,
				base_dept_url : base_dept_url,
				dept_url : dept_url,
				user_url : user_url,
				user_detail : user_detail,
				init_selected_url : init_selected_url,
				get_common_url : get_common_url,
            	save_common_url : save_common_url,
				canEdit:1,
				ret_fun:function(self){
					var data = self.get_depts_users();
					console.log(data);

					//清除之前已有选项
					notifyPageObj.find("div.select").remove();
					notifyPageObj.find("div.w-xLine").remove();

					for (var i = 0; i < data.length; i++) {		
						var html = new Array();
						html.push('<div class="h46 user select bg-white" data-id="'+data[i].id+'">');
						html.push('<div class="fl square-checkbox mr6"></div>');
						html.push('<img src="'+data[i].pic_url+'" alt="" class="mr6 fl" onerror="this.src=\''+defaultFace+'\'"/>');
						html.push('<span class="selectName fs14">'+data[i].name+'</span>');
						html.push('</div>');
						html.push('<div class="wxLine ml20"></div>');

						var dom = $($.trim(html.join('')));

						UiFramework.btnPackage().checkBoxBtn.init({
							target:dom.find('.square-checkbox'),
							status:true,
							type:2,
							callback:{
								on:function(obj,self){
									obj.parents(".user").data("cacheData").status = true;
								},
								off:function(obj,self){
									obj.parents(".user").data("cacheData").status = false;
								}
							}
						});

						dom.data("cacheData",{id:data[i].id,name:data[i].name,status:true})

						notifyPageObj.find(".nodePartContent").before(dom);
					};
				}
			});

			this.dept = dept;

			layerPackage.unlock_screen();
		},
		//底部菜单
		addBottom:function(){
			var pThis = this;
			var notifyPageObj = $("body").find("#notifyPage");

			UiFramework.wxBottomMenu.init({
				noFixed:1,
				target:notifyPageObj,
				menu: [{
					width: 100,
					name: "发送",
					iconClass: "",
					btnColor: "g",
					id: null,
					click: function(self, obj, index) {
						var receivers = new Array(),name = new Array();
						var user = notifyPageObj.find(".user");
						$.each(user, function(index, val) {
							var data = $(this).data("cacheData");
							if(data.status == true){
								receivers.push({
									"receiver_id":data.id,
									"receiver_name":data.name
								});

								name.push(data.name);
							}
						});

						if(pThis.dept.get_depts_users().length == 0 || receivers.length == 0){
							layerPackage.ew_alert({
								"title":"至少选择一个知会人！"
							});
							return;
						}

						layerPackage.ew_confirm({
							title:"确定要将该流程知会给 " + name.join(',') + "?",
							ok_callback:function(){
								t.ajaxJson({
									url:notifyUrl,
									data:{
										"data":{
											"formsetinst_id":pThis.formsetinstId,
											"receivers":receivers
										}
									},
									callback:function(result,status){
										if(result.errcode !=0){
											layerPackage.fail_screen(result.errmsg);
											return;
										}

										layerPackage.unlock_screen();
										layerPackage.success_screen(result.errmsg);

										setTimeout(function(){

											pThis.common.toDealList(pThis.activekey);

										},1500)

										//window.location.href = "/wx/index.php?model=process&a=dealList&degbug=1&activekey=1";
									}
								});
							}
						});
					}
				}]
			});
		},
		openUserDept:function(_setting,common){
			var pThis = this;
			var formsetinstId = _setting.formsetinstId;

			var workitemId = _setting.workitemId;

			var target = $("body").find(".notifyBtn");

			if(target.length != 0){target.remove();}

			$("body").append("<div class='notifyBtn hide'>open</div>");

			target = $("body").find(".notifyBtn");

			var dept = new depts_users_select().init({
				target:target,
				select:{},
				type :2,
				max_select:0,
				base_dept_url : base_dept_url,
				dept_url : dept_url,
				user_url : user_url,
				user_detail : user_detail,
				init_selected_url : init_selected_url,
				get_common_url : get_common_url,
            	save_common_url : save_common_url,
				canEdit:1,
				ret_fun:function(self){
					var data = self.get_depts_users();
					console.log(data);

					if(data.length == 0){return;}

					var receivers = new Array();

					for (var i = 0; i < data.length; i++) {
						var d = data[i];
						receivers.push({
							receiver_id:d.id,
							receiver_name:d.name
						});
					};

					t.ajaxJson({
						url:notifyUrl,
						data:{
							"data":{
								//"formsetinst_id":formsetinstId,
								"workitem_id":workitemId,
								"receivers":receivers
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								layerPackage.fail_screen(result.errmsg);
								return;
							}

							layerPackage.unlock_screen();
							layerPackage.success_screen(result.errmsg);

							/*setTimeout(function(){

								common.toDealList(pThis.activekey);

							},1500)*/
						}
					});
				}
			});

			this.dept = dept;

			//模拟点击
			target.click();

			layerPackage.unlock_screen();
		}
	};

	return notify;
});