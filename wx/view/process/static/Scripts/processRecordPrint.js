/**
 * 流程记录打印
 */
require([
 	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/process_form_analyse/js/process_form_analyse',
	'common:widget/lib/plugins/prePlugin/prePlugin'
], function($,tools,UiFramework,plusFormAnalyse,prePlugin) {
	var layerPackage = UiFramework.layerPackage();
	var getFromDataUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=123";

	var entity = {
		init: function() {
			entity._show_form();
		},
		//获取及显示表单
		_show_form: function() {
			layerPackage.lock_screen({msg: '数据加载中...'});
			var id = tools.getUrlParam('formsetinst_id');
			tools.ajaxJson({
				url:getFromDataUrl,
				data:{"data":JSON.stringify({"formsetinst_id":id})},
				callback:function(result,status){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}else{
						var info = result.info;
						//抬头
						var state_html = '';
						switch(Number(info.state)) {
							case 1: state_html = '<span class="c-2b">运行中</span>'; break;
							case 2: state_html = '<span class="c-red">已通过</span>'; break;
							case 3: state_html = '<span class="c-red">已驳回</span>'; break;
							case 4: state_html = '<span class="c-red">已撤销</span>'; break;
							case 5: state_html = '<span class="c-red">撤销中</span>'; break;
							case 6: state_html = '<span class="c-red">通过已撤销</span>'; break;
						}
						$('#form-name').html(info.form_name);
						$('#formsetinst-name').html(info.formsetinst_name + ' (' + state_html + ')');
						$('#create-name').html(info.create_name);
						$('#depts').html(info.depts);
						$('#create-time').html(tools.dateFormat(info.create_time));
						//表单数据
						var form_html = plusFormAnalyse.to_form_html(info.form_vals);
						var deal_html = plusFormAnalyse.to_deal_html(info.workitems, {linksign_url: info.linksign_url});
						var file_html = plusFormAnalyse.to_file_html(info.files);
						$('#table-main').html(form_html + deal_html + file_html);
						$('#table-main').find('.previewImage').on('click', function() {
							var preOpt = {};
							preOpt.target = $('#main');
							preOpt.imgList = [];
							preOpt.imgList.push($(this).attr('src'));
							prePlugin.init_img_url(preOpt);
						})
						//打印说明
						$('#print-name').html(info.print_name);
						$('#print-time').html(tools.dateFormat(info.print_time));

						var img_cnt = $('img').length //需要加载的签名图片总数
						if (img_cnt == 0) {
							setTimeout(() => {
								//无图片，解除遮罩并开始打印
								layerPackage.unlock_screen()
							  	window.print()
							}, 1000);
						} else {
							//有图片，等图片加载完再解除遮罩开始打印
							var load_img_cnt = 0 //当前已加载的签署图片数
							$('img').each(function() {
								$(this).on('load', function() {
								load_img_cnt++
								if (load_img_cnt == img_cnt) {
									setTimeout(() => {
										layerPackage.unlock_screen()
										window.print()
									}, 1000)
								}
								})
							})
						}
						// window.print();
					}
					
				}
			});
		},
	};
	
	entity.init();
	
});