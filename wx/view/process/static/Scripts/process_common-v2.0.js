define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/commonUi.html',
	'process:static/Scripts/otherProcess-v2.0',
	'common:widget/lib/storage'
], function($, t, UiFramework, tpl, uiTemplate,otherProcess) {
	var layerPackage = UiFramework.layerPackage(),
	nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
	saveOrEditUrl = "index.php?model=process&m=ajax&a=index&cmd=105",
	sendUrl = "index.php?model=process&m=ajax&a=index&cmd=106";

	var isIndex = t.getUrlParam("isIndex") || "",
		hash = window.location.hash;

	//是否从主页型过来
	if(isIndex){
		isIndex = "&isIndex="+isIndex;
	}

	var process_common = {
		uploadFileObj:null,//上传附件对象
		init:function(container){
			if(!t.checkJqObj(container)){layerPackage.fail_screen("error");return;}

			this.container = container;
		},
		//获取当前用户信息 模块的模拟数据
		getHeaderPartData:function(data){
			var html = new Array();
	
			var must = 0;

			for (var i = 0; i < data.length; i++) {
				if(data[i].conf_key == 7){
					must = 1;//必填
					html.push('<b class="fs-n otherName c-white tac fw-n fs17">?</b>');
				}else{
					html.push(data[i].name);
				}

				if(i != data.length - 1){
					html.push('&nbsp;-&nbsp;');
				}
			};

			var headerPartData = {
				"type": "text",
				"must": must,
				"describe": "",
				"input_key": "headerPart",
				"name": html.join(''),
				"placeholder": "补全上面的实例名",
				"format": "default",
				"formula": "",
				"formula_str": "",
				"visit_input": 1,
				"edit_input": 1,
				"errorMsg":"补全上面的实例名",
				"className":(must == 0 ? "hide" : "showTitle")
			};

			return headerPartData;
		},
		//表单主体信息
		getFormInfo:function(data){
			data.form_describe = this.checkStr(data.form_describe);

			tpl.register('checkNewline', t.checkNewline);
			var dom = tpl(uiTemplate, {
				formInfo:data
			});
			tpl.unregister('checkNewline');

			return $($.trim(dom));
		},
		//检查空字串
		checkStr:function(str){
			if($.trim(str) == ""){
				return "";
			}

			return str;
		},
		ctrlClassName:"scCtrl",
		//隐藏控件错误信息
		ctrlHideErrorMsg:function(){
			//获取还没验证的控件
			var notChekecd = this.container.find("."+this.ctrlClassName+"[data-checked=0]");

			if(notChekecd.length > 0){
				//触发隐藏错误信息事件
				notChekecd.trigger('hideMsg');
			}
		},
		//校验表单名
		checkFormName:function(){
			var obj = this.container;
			var headerPartObj = obj.find("."+this.ctrlClassName+"[data-key=headerPart]");

			if(headerPartObj.length == 1 && headerPartObj.attr("data-checked") == 0){
				headerPartObj.trigger('check');

				this.scCtrlErrMsg(obj,headerPartObj.data("errorMsg"));

				//定位到当前第一个错误的控件
				var top = headerPartObj[0].offsetTop;
				var mContent = obj.find(".mContent");
				mContent.animate({
				    scrollTop: top
				});
				return ;
			}

			return 1;
		},
		//校验是否有上传中的附件
		checkHasFilesUpload:function(){
			var filesObj = this.container.find("."+this.ctrlClassName+"[data-key=files]");

			if(filesObj.length == 1){
				var scCtrlFilesObj = filesObj.data("scCtrlFiles");

				if(!scCtrlFilesObj.checkUploadStatus()){
					return 1;
				}else{
					return;
				}
			}

			return 1;
		},
		//提交前校验所有控件
		checkAllCtrl:function(jqObj){
			var obj = jqObj ? jqObj : this.container; 

			//获取还没验证的控件
			var notChekecd = obj.find("."+this.ctrlClassName+"[data-checked=0]");

			if(notChekecd.length > 0){
				//触发验证事件
				notChekecd.trigger('check');
				var firstCtrl = notChekecd.eq(0);

				var mContent = obj.find(".mContent");
				this.scCtrlErrMsg(obj,firstCtrl.data("errorMsg"));

				//定位到当前第一个错误的控件
				var top = firstCtrl[0].offsetTop;
				mContent.animate({
				    scrollTop: top
				});
				return;
			}

			return 1;
		},
		//校验必填选项是否有输入值
		checkMustCtrlHasVal:function(jqObj,noAnimated){
			var obj = jqObj ? jqObj : this.container; 

			//获取所有必填事件
			var mustCtrl = obj.find("."+this.ctrlClassName+"[data-must=1]");
			
			var flag = false;
			var firstObj = null;

			mustCtrl.each(function() {
				var o = $(this);

				if(o.attr("data-empty") === undefined || o.attr("data-empty") == 1){
					flag = true;
					firstObj = o;
					return false;
				}
			});

			if(flag){
				if(!noAnimated){
					var mContent = obj.find(".mContent");
					this.scCtrlErrMsg(obj,"1111");

					//定位到当前第一个错误的控件
					var top = firstObj[0].offsetTop;
					mContent.animate({
					    scrollTop: top
					});
				}
				
				return;
			}

			return 1;
		},
		//数据转换
		dataChange:function(data){
			var type = data.type;

			try{
				if(type == "radio" || type == "checkbox" || type == "select"){
					data.opts = data.opts && t.isArray(data.opts) ? data.opts.join(',') : data.opts;
					data.opts_nums = data.opts_nums && t.isArray(data.opts_nums) ? data.opts_nums.join(',') : data.opts_nums;
					data.val = data.val && t.isArray(data.val) ? data.val.join(',') : data.val;
					data.val_id = data.val_id && t.isArray(data.val_id) ? data.val_id.join(',') : data.val_id;
					//data.val_id = "1"; 测试用
				}

				return data;
			}catch(e){
				return data;
			}
		},
		//获取控件数据
		getCtrlData:function(jqObj){
			var params = {};
			var container = jqObj ? jqObj : this.container;
			var scCtrl = container.find("."+this.ctrlClassName+":not(."+this.ctrlClassName+"[data-key=files]):not(."+this.ctrlClassName+"[data-key=headerPart])");

			formInfoObj = container.find(".formInfo");
			headerPartObj = container.find("."+this.ctrlClassName+"[data-key=headerPart]");
			filesCtrl = container.find("."+this.ctrlClassName+"[data-key=files]");

			if(formInfoObj.length > 0){
				params.form_id = formInfoObj.attr("data-id");
				params.form_name = headerPartObj.find("label").text().replace('?',headerPartObj.find('input').val()).replace(/\s+/g, '').replace('(选填)','').replace('*','');
				params.work_id = formInfoObj.find("input[name=work_id]").val();
			}

			if(filesCtrl.length > 0){
				params.files = filesCtrl.data("scCtrlFiles").files;
			}

			var pThis = this;
			params.form_vals = {};
			scCtrl.each(function() {
				var d = $(this).data("cacheData");

				if(d.type == "table" && d.rec){
					for (var i = 0; i < d.rec.length; i++) {
						for(var k in d.rec[i]){
							d.rec[i][k] = pThis.dataChange($.extend({},d.rec[i][k]));
						}
					};
				}else{
					d = pThis.dataChange(d);
				}

				params.form_vals[d.input_key] = d;
			});

			return params;
		},
		//保存表单
		saveProcess:function(data,checkData,callback,jqObj){
			//特殊表单验证
			try{
				//var flag = otherProcess.checkParams(data.is_other_proc);

				//if(!flag){return;}
			}catch(e){
				console.log(e);
			}

			var listType = t.getUrlParam("listType"),
				activekey = t.getUrlParam("activekey");

			//草稿
			if(listType == 2 && activekey == 2){
				if(checkData && !this.checkAllCtrl()){
					layerPackage.unlock_screen();
					return;
				}
			}else{
				if(checkData){
					var tipName = this.checkMustCtrlVal(data);

					this.container.find(".tipDiv").remove();

					if(tipName.length > 0){
						console.log(tipName);
						layerPackage.unlock_screen();
						this.container.find(".editFormTable").before("<div class='tipDiv scPadding' style='font-size:13px;color: #EFB02C;background-color:#FFF9EB;'>需补全信息："+tipName.join("、")+"</div>")
						return;
					}
				}
			}

			/*
			if(checkData && !this.checkAllCtrl()){
				layerPackage.unlock_screen();
				return;
			}
			*/

			var pThis = this;
			var ctrlData = pThis.getCtrlData(jqObj);

			var sendParams = {
				"form_id":data.form_id,
				"form_name":data.form_name,
				"work_id":data.work_id,
				"form_vals":ctrlData.form_vals,
				"workitem_id":data.workitem_id,
				"formsetInst_id":data.formsetInst_id
				//"judgement":""
			};

			var files = ctrlData.files;
			if(files && files.length > 0 && data.work_node_id){
				var arr = new Array();
				for (var i = 0; i < files.length; i++) {
					var f = files[i];
					if(((f.work_node_id && f.work_node_id == data.work_node_id) && (f.create_id && f.create_id == userInfo.userId)) 
						|| !f.work_node_id){
						arr.push(f);
					}
				};

				if(arr.length > 0){sendParams.files = arr;}

				if(listType == 2 && activekey == 2){
					var filesCtrl = this.container.find("."+this.ctrlClassName+"[data-key=files]");
					var filesCtrlData = filesCtrl.data("cacheData");
					var scCtrlFiles = filesCtrl.data("scCtrlFiles");
					
					if(filesCtrlData.file_type == 2 && arr.length == 0 && checkData){
						scCtrlFiles.files = arr;
						filesCtrl.trigger("check");
						layerPackage.unlock_screen();
						return;
					}
				}
			}

			//console.log(sendParams);
			//console.warn(JSON.stringify(sendParams));
			//return;

			t.ajaxJson({
				url:saveOrEditUrl,
				data: {
					"data": JSON.stringify(sendParams)
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.unlock_screen();

						layerPackage.ew_alert({
							title:result.errmsg,
							action_desc:'ok,我知道了',
							top:40,
							callback:function(){
								window.history.go(-1);
							}
						});
						return;
					}

					if(!t.isFunction(callback)){return;}
					callback(result,status);
				}
			});
		},
		send:function(data,callback){
			var pThis = this;
			layerPackage.lock_screen({
				"msg":"发送中……"
			});
			t.ajaxJson({
				url:sendUrl,
				data: {
					"data": data
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						//return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					setTimeout(function(){

						if(!t.isFunction(callback)){return;}
						callback(result,status);
						
					},1500);
					
				}
			});
		},
		//获取保存链接地址
		getSaveOrEditUrl:function(){
			return saveOrEditUrl;
		},
		//替换保存链接地址
		setSaveOrEditUrl:function(url){
			if(url === undefined || url === null){
				return;
			}

			saveOrEditUrl = url;
		},
		//获取意见数据 模拟textarea
		getJudgementData:function(must,name,val,placeholder,errmsg){
			var judgementData = {
				"type": "textarea",
				"must": must,
				"describe": "",
				"input_key": "judgement",
				"name": name,
				"placeholder": placeholder|| "",
				"format": "default",
				"formula": "",
				"formula_str": "",
				"visit_input": 1,
				"edit_input": 1,
				"val":val,
				"className":"processTextrea",
				"errorMsg":errmsg || "请填写意见"
			};

			return judgementData;
		},
		/*跳转操作处理*/
		//跳转到发送页面
		toSendPage:function(params){
			params.isIndex = isIndex;
			params.hash = hash;

			this.cacheHash = hash;

			var pThis = this;

			require(['process:static/Scripts/sendPage_main-v3.0'],function(sendPage){
				sendPage.init(pThis,params);
			});
			//window.location.href = "/wx/index.php?model=process&a=sendPage&degbug=1"+params + isIndex+hash;
		},
		//发送到退回步骤
		toSendBack:function(params){
			params.isIndex = isIndex;
			params.common = this;

			require(['process:static/Scripts/sendPage_main-v3.0'],function(sendPage){
				sendPage.sendBack(params);
			});
		},
		toMineList:function(activekey){
			if(!activekey){activekey = 0;}

			if(isIndex){
				this.toIndex(activekey);
				return;
			}

			window.location.href = "/wx/index.php?model=process&a=mineList&degbug=1&activekey="+activekey;
		},
		toDealList:function(activekey){
			if(!activekey){activekey = 0;}

			if(isIndex){
				this.toIndex(activekey);
				return;
			}

			window.location.href = "/wx/index.php?model=process&a=dealList&degbug=1&activekey="+activekey;
		},
		toDetail:function(id,listType,activekey){
			if(!listType){listType=1;}
			if(!activekey){activekey = 0;}
			window.location.href = "/wx/index.php?model=process&a=detail&degbug=1&id="+id+"&listType="+listType+"&activekey="+activekey + isIndex+hash;
		},
		//主页型相关
		toIndexDealList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pDealList";
			
			this.toIndex(activekey);
		},
		toIndexMineList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pMineList";
			
			this.toIndex(activekey);
		},
		toIndexNotifyList:function(activekey){
			if(activekey === undefined){activekey=0;}
			//window.location.href = "/wx/index.php?debug=1&app=index&a=index&menuType=2&activekey="+activekey+"#pNotifyList";
			
			this.toIndex(activekey);
		},
		toIndex:function(activekey){
			//if(activekey !== undefined && activekey != "0"){activekey="&activekey="+activekey;}else{activekey = "";}
			activekey="&activekey="+activekey;

			if(this.cacheHash){
				hash = this.cacheHash;
			}

			t.ajaxJson({
				url:"/wx/index.php?model=process&m=ajax&a=redirection&send=1",//&send=1&hash="+hash.substring(1) + activekey,
				type:"get",
				callback:function(result,status){
					if(result.errcode !=0){
						return;
					}

					window.location.href = result.url;
				}
			});
		},
		/*跳转操作处理*/
		/*缓存数据 操作*/
		storageInit:function(id,myData,callback){
			try{
				var id = t.getHrefParamVal("id"),pThis = this;

				if(!id){return;}

				var storage_key = 'apply_process_'+id;
				$storage.init({
					key:storage_key,
					time:2000,
					init_data : function(data){
						if(data != null){
							var tmp_dataObj = JSON.parse(data);
							var proc_form_id = tmp_dataObj["id"];
							if(proc_form_id!==undefined&&proc_form_id==id){
								myData = tmp_dataObj;
								callback(myData);
							}
							$('.js_inputs').remove();
						}
					},
					save_data : function(){
						var ctrlData = pThis.getCtrlData();
						myData["id"] = id;
						myData.inputs = ctrlData.form_vals;
						myData.files = ctrlData.files || [];

						return myData;
					}
				});
			}catch(e){
				console.log(e);
			}
		},
		storageClear:function(key){
			$storage.clear(key);
		},
		storageClearAll:function(){
			$storage.clearAll();
		},
		/*缓存数据 操作*/
		//表单错误提示
		scCtrlErrMsg:function(target,errMsg,clear){
			if(!errMsg){return;}

			target.find(".scCtrlErrMsg").remove();
			var mContent = target.find(".mContent");

			if(clear){mContent.removeClass('hasErrMsg');return;}

			//显示错误信息
			mContent.before('<div class="scCtrlErrMsg tac fs15 w_100">'+errMsg+'</div>');
			mContent.addClass('hasErrMsg');
		},
		//检查必填控件
		checkMustCtrlVal:function(data){
			var flag = true;
			var tipName = new Array();

			var container = this.container;
			var scCtrl = container.find("."+this.ctrlClassName+":not(."+this.ctrlClassName+"[data-key=files])");

			filesCtrl = container.find("."+this.ctrlClassName+"[data-key=files]");

			scCtrl.each(function() {
				var d = $(this).data("cacheData");

				if(d.type == "table"){
					if(d.visit_input == "1" && d.edit_input == "1" && d.must == "1" && (d.rec === undefined || d.rec.length == 0)){
						tipName.push(d.name);
					}


					if(!(d.rec === undefined || d.rec.length == 0)){
						var rec = d.rec;

						for (var i = 0; i < rec.length; i++) {
							var r = rec[i];

							for (var j in r) {
								var c_r = r[j];

								if(c_r.visit_input == "1" && c_r.edit_input == "1" && c_r.must == "1" && (c_r.val === undefined || c_r.val == "" || c_r.val == [])){
									tipName.push(d.name+"第"+(i+1)+"行的"+c_r.name);
								}
							};
						};
					}
				}else{
					if(d.visit_input == "1" && d.edit_input == "1" && d.must == "1" && (d.val === undefined || d.val == "" || d.val == [])){
						tipName.push(d.name);
					}
				}
			});

			if(filesCtrl.length > 0){
				var scCtrlFiles = filesCtrl.data("scCtrlFiles");
				var files = scCtrlFiles.files;

				if(data.work_node_id){
					var arr = new Array();
					for (var i = 0; i < files.length; i++) {
						var f = files[i];
						if(((f.work_node_id && f.work_node_id == data.work_node_id) && (f.create_id && f.create_id == userInfo.userId)) 
							|| !f.work_node_id){
							arr.push(f);
						}
					};

					var filesCtrlData = filesCtrl.data("cacheData");
					
					if(filesCtrlData.file_type == 2 && arr.length == 0){
						tipName.push("附件");
					}
				}
			}

			return tipName;
		}
	};

	return process_common;
});


/*
附件数据
{
	"file_name": "1.pdf",
	"md5": "56c3b2c02a51155d2aca2de7ef3f623e",
	"file_path": "wx/file/origin/56c/3b2/c02/56c3b2c02a51155d2aca2de7ef3f623e.pdf",
	"file_ext": "pdf",
	"file_size": 111949,
	"file_hash": "44fc0e8fc0ade1c5a24a561112901d38",
	"isDownload": 1
}
*/