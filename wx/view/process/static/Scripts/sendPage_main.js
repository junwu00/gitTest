require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/sendPage.html',
	'process:static/Scripts/process_common'
],function($,t,UiFramework,depts_users_select,tpl,senPageTpl,common) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var notifyUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=118",
	base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
	dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
	user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
	user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
	init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids';

	//jqObj对象
	var container = $("#main-container"),
		workitemId = t.getHrefParamVal("workitemId") || 1,
		formsetInstId = t.getHrefParamVal("formsetInstId") || 0,
		list = t.getHrefParamVal("list") || 0;

	var params = {};
	var clickStatus = true;

	common.getNextNode(workitemId,function(result,status){
		if(result.errcode !=0){
			layerPackage.fail_screen(result.errmsg);
			return;
		}

		var data = result.info;
		console.log(data);

		params.isSend = data.is_send;
		params.workitemId = data.workitem_id;

		var dom = null;
		dom = tpl(senPageTpl,{
			data:data,
			cdn_domain:cdn_domain,
			defaultFace:defaultFace,
			media_domain:media_domain
		});

		//默认插入到开头位置
		container.html("").append($($.trim(dom)));

		container.on("click",".part",function(){
			var o = $(this),content = o.find('.partContent');

			if(!content.hasClass('hide')){return;}

			o.find('.icon-hand-right').removeClass('c-gray');
			o.find('.partContent').css("display","none").removeClass('hide').slideDown();
			o.siblings().find(".partContent").addClass('hide');
			o.siblings().find('.icon-hand-right').addClass('c-gray')
		});

		container.find('.circle-checkbox').each(function(){
			var o = $(this),pObj = o.parents(".user"),
			id = pObj.attr("data-id"),
			name = pObj.attr("data-name");
			pObj.data("cacheData",{
				id:id,name:name,status:false
			});


			if(pObj.hasClass('receiveArr')){
				UiFramework.btnPackage().checkBoxBtn.init({
					target: o,
					status: true,
					type: 1,
					callback: {
					},
					cancelEvent:true
				});

				pObj.data("cacheData").status = true;
			}else if(pObj.hasClass('selectArr')){
				UiFramework.btnPackage().checkBoxBtn.init({
					target: o,
					status: false,
					type: 1,
					callback: {
						on: function(obj, self) {
							pObj.data("cacheData").status = true;
						},
						off: function(obj, self) {
							pObj.data("cacheData").status = false;
						}
					}
				});
			}
		});

		container.find(".select").on('click',function(){
			$(this).next(".getUser").click();
		});

		container.find(".getUser").each(function(){
			var o = $(this);
			var pObj = $(this).parent().parent();
			var dept = new depts_users_select().init({
				target:o,
				select:{},
				type :2,
				max_select:0,
				base_dept_url : base_dept_url,
				dept_url : dept_url,
				user_url : user_url,
				user_detail : user_detail,
				init_selected_url : init_selected_url,
				canEdit:1,
				ret_fun:function(self){
					var data = self.get_depts_users();
					console.log(data);

					pObj.find("section.select").remove();
					for (var i = 0; i < data.length; i++) {		
						var html = new Array();
						html.push('<section class="bt1 h46 user select" data-id="'+data[i].id+'">');
						html.push('<img src="'+data[i].pic_url+'" alt="" class="fl icon br6" onerror="this.src=\''+defaultFace+'\'"/>');
						html.push('<span>'+data[i].name+'</span>');
						html.push('<span class="fr circle-checkbox"></span>');
						html.push('</section>');

						var dom = $($.trim(html.join('')));

						UiFramework.btnPackage().checkBoxBtn.init({
							target:dom.find('.circle-checkbox'),
							status:true,
							type:1,
							callback:{
								on:function(obj,self){
									obj.parents(".user").data("cacheData").status = true;
								},
								off:function(obj,self){
									obj.parents(".user").data("cacheData").status = false;
								}
							}
						});

						dom.data("cacheData",{id:data[i].id,name:data[i].name,status:true})

						pObj.prepend(dom);
					};
				}
			});
		});

		UiFramework.bottomInfo.init(container,77);

		UiFramework.wxBottomMenu.init({
			target:container,
			menu: [{
				width: 30,
				name: "上一步",
				iconClass: "icon-undo",
				btnColor: "w",
				id: null,
				click: function(self, obj, index) {
					if(list==2){
						common.toDetail(formsetInstId,2,2);
					}
					else if(list==1){
						window.history.back();
						//common.toDetail(formsetInstId,2);
					}
					else{
						common.toDetail(workitemId);
					}
				}
			},{
				width: 70,
				name: "发送",
				iconClass: "icon-plus-sign",
				btnColor: "g",
				id: null,
				click: function(self, obj, index) {
					if(params.isSend == 1 && clickStatus){
						return;
					}

					var count = 0;
					var data = {};
					data.workitem_id = params.workitemId;
					data.work_nodes = new Array();
					var receivers = new Array(),receiverName = new Array();

					var is_end = $(".partContent:not(.hide)").eq(0).attr("data-isend");
					var work_node_name = $(".partContent:not(.hide)").eq(0).attr("data-name");

					$(".partContent:not(.hide)").each(function(){
						var work_node = {};
						work_node.id = $(this).attr("data-id");
						work_node.receiver_arr = new Array();

						is_end = $(this).attr("data-isend");

						if(is_end!="true"){
							$(this).find(".user").each(function(){
								var data = $(this).data("cacheData");

								if(data.status == true){
									var receiver = new Object();
									receiver.receiver_id = data.id;
									receiver.receiver_name = data.name;
									work_node.receiver_arr.push(receiver);
									count++;

									receiverName.push(data.name);
								}
							});
						}
						if(work_node.receiver_arr.length>0||params.isSend==true||is_end=="true"){
							data.work_nodes.push(work_node);
						}
					});

					if(count==0&&params.isSend==false&&is_end!="true"){
						layerPackage.ew_alert({
							"title":"至少选择下一步接收人！"
						});
						return false;
					}

					if(params.isSend==false){
						var content = '确定要将该流程发送到 '+work_node_name+" 步骤,接收人为 "+receiverName.join(',')+" ?";
						if(is_end=="true"){
							content = '确定要将该流程发送到 '+work_node_name+" 步骤?";
						}
						layerPackage.ew_confirm({
							title:content,
							ok_callback:function(){
								common.send(data,function(){
									if(list == 2){
										common.toMineList();
									}
									else if(list==1){
										common.toMineList();
									}
									else{
										common.toDealList();
									}
								});
							}
						});
					}
					else{
						common.send(data,function(){
							common.toDealList();	
						});
					}
					
				}
			}]
		});

		if(params.isSend == 1){
			clickStatus = false;
			var clickObj = $("#wx_bottom_menu").find("li:last .btn_g");
			clickObj.click();
			clickObj.off();
		}

		layerPackage.unlock_screen();
	});
});