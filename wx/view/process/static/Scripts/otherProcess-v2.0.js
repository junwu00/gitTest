define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/otherProcessTpl.html',
	'common:widget/lib/plugins/gdmap/lbs.amap',
	'common:widget/lib/GenericFramework-v2.0'
],function($,t,UiFramework,tpl,otherProcessTpl,gdmap,GenericFramework) {
	var layerPackage = UiFramework.layerPackage(); 

	var otherProcess = {
		/** 请假固定字段（请假类型、开始时间、结束时间） */
		fix_rest_key : ['input1481340230000', 'input1481340238000', 'input1481340240000'],
		/** 请假隐藏字段（请假时长） */
		hide_rest_key : ['input1481527822000'],
		/** 补录固定字段（补录时间） */
		fix_makeup_key : ["input1481340091000"],
		/** 外出固定字段（开始时间、结束时间） */
		fix_legwork_key : ['','input1481340568000', 'input1481340570000'],
		/** 外出隐藏字段（外出时长） */
		hide_legwork_key : ['input1481527081000'],

		//获取请求类型接口
		fix_rest_type_url : "/wx/index.php?model=workshift&m=ajax&cmd=111",		
		//获取补录时间点接口
		fix_makeup_url : "/wx/index.php?model=index&m=app&cmd=107",
		//获取快捷时间段选择接口：
		other_time_url:"/wx/index.php?model=index&m=app&cmd=108",
		//判断有效时长接口：
		time_count_url:"/wx/index.php?model=index&m=app&cmd=106",

		exaccountNum:6, //报销模块 
		fix_expense_key : ['input1484730641000','input1484730627000'],
		fix_expense_table_key :['input1484730627000_1484730645000','input1484730627000_1484730653000','input1484730627000_1484730655000'],
		hide_expense_key:['input1484730627000_1488270941000'],

		//控件基础className
		ctrlClassName:"scCtrl",

		//初始化
		init:function(params){
			//is_other_proc,target,data,listType
			var data = params.data,
				is_other_proc = params.is_other_proc,
				target = params.target, //最外层DOM
				listType = params.listType,
				userId = params.userId,
				formsetinstId = params.formsetinstId,
				state = params.state,
				workitemId = params.workitemId; //工作项ID

			this.is_other_proc = is_other_proc;
			this.jqObj = target;
			this.workitemId = workitemId;

			var pThis = this;
			pThis.cacheData = data; //进行数据保存
			//创建
			if(listType === undefined || listType === null){
				//var is_other_proc = data.is_other_proc;
				//3 补录
				if(is_other_proc == 3){
					var o = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_makeup_key[0]+"]");
					var cacheData = o.data("cacheData"),key = cacheData.input_key;

					this.get_fix_makeup(target,data,userId,function(result){
						var	type = cacheData.type;
						var html = new Array();

						var rotaTime = t.getUrlParam("rotaTime");

						if(type == "select"){
							for (var i = 0; i < result.makeup.length; i++) {
								var d = result.makeup[i];
								var makeup_time = t.dateFormat(d);
								html.push("<option value='"+makeup_time+"'  "+(((!rotaTime && cacheData.val && cacheData.val[0] == makeup_time) || rotaTime == d) ? "selected": "")+">"+this.getTime(d)+"</option>");
							};

							if(result.makeup.length == 0){
								html.push("<option value=''></option>");
							}

							var selectObj = o.find("select");
							selectObj.html(html.join(""));

							if(result.makeup.length == 0){
								selectObj.after("<span class='c-9b' style='display:inline-block;margin-top:10px;'>你暂时没有需要补录的考勤时间喔</span>");
								selectObj.addClass('hide');
							}

							o.trigger('check');
						}
					});
				}
				//4 请假
				else if(is_other_proc == 4){
					var o = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[0]+"]");
					var cacheData = o.data("cacheData"),key = cacheData.input_key;
					
					this.change_fix_rest_type(cacheData.opts_nums,function(result){
						var rest_type = result.rest_type;
						var selectObj = o.find("select");
						var html = new Array();
						var opts = new Array();
						
						for (var i = 0; i < rest_type.length; i++) {
							var rest_type_data = rest_type[i];
							var typeName = rest_type_data.name;
							opts.push(typeName);
							html.push("<option value='"+rest_type_data.id+"' data-minunit='"+rest_type[i].min_unit+"' "+(cacheData.val == typeName ? "selected": "")+">"+typeName+"</option>");
						};

						if(rest_type.length == 0){
							html.push("<option value=''></option>");
						}

						selectObj.html(html.join(''));

						if(rest_type.length == 0){
							selectObj.after("<span class='c-9b' style='display:inline-block;margin-top:10px;'>无法申请请假</span>");
							selectObj.addClass('hide');
						}

						if(opts.length > 0){
							cacheData.opts = opts.join(",");
						}

						//一开始初始化
						o.trigger('check');
						this.hide_hide_rest_key(target);
					});
				}
				// 5 外出
				else if(is_other_proc == 5){
					this.fix_rest_key = this.fix_legwork_key;
					this.hide_rest_key = this.hide_legwork_key;

					//一开始初始化
					this.jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[1]+"]").trigger("afterEvent");
					this.hide_hide_rest_key(target);
				}
			}
			//详情
			else{
				//var is_other_proc = (listType == 1 || listType == 2) ? data.formsetinst.is_other_proc : data.notify.is_other_proc;
				//var userId = data.workitem.creater_id;
				this.userId = userId;

				if(is_other_proc == 3){
					var o = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_makeup_key[0]+"]");
					var cacheData = o.data("cacheData");

					if(!cacheData.edit_input){
						this.set_fix_makeup(o.find(".scCtrlVal"),cacheData);
					}else{
						//o.find('option:last').attr("value","");
						//o.find('option:first').remove();
						var o = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_makeup_key[0]+"]");
						var cacheData = o.data("cacheData");

						var val = cacheData.val[0];
						var selectVal = val,
							selectValTime = new Date(selectVal.replace('-','/').replace('-','/')).getTime() / 1000,
							firstInit = false;

						this.get_fix_makeup(target,data,userId,function(result){
							var	type = cacheData.type;
							var html = new Array();

							var rotaTime = t.getUrlParam("rotaTime");

							if(type == "select"){
								for (var i = 0; i < result.makeup.length; i++) {
									var d = result.makeup[i];

									//插入数据当中选中时间
									if(!firstInit && selectValTime > d){
										html.push("<option value='"+val+"' selected>"+this.getTime(selectValTime)+"</option>");
										firstInit = true;
									}

									var makeup_time = t.dateFormat(d);
									html.push("<option value='"+makeup_time+"'>"+this.getTime(d)+"</option>");
								};

								if(result.makeup.length == 0){
									html.push("<option value=''></option>");
								}

								var selectObj = o.find("select");
								selectObj.html(html.join(""));

								if(result.makeup.length == 0){
									selectObj.after("<span class='c-9b' style='display:inline-block;margin-top:10px;'>你暂时没有需要补录的考勤时间喔</span>");
									selectObj.addClass('hide');
								}

								o.trigger('check');
							}
						});
					}
				}
				//4 请假 5 外出
				else if(is_other_proc == 4 || is_other_proc == 5){

					if(is_other_proc == 5){
						this.fix_rest_key = this.fix_legwork_key;
						this.hide_rest_key = this.hide_legwork_key;

						this.getLocationInfo(target,listType,formsetinstId);
					}

					var startTimeObj = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[1]+"]");
					startTimeObj.trigger('afterEvent');

					var endTimeObj = target.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[2]+"]");
					
					this.hide_hide_rest_key(target);

					var time1 = startTimeObj.data("cacheData").val;
					var time2 = endTimeObj.data("cacheData").val;
					
					var t1 = new Date(time1.replace('-','/').replace('-','/')).getTime() / 1000;
					var t2 = new Date(time2.replace('-','/').replace('-','/')).getTime() / 1000;
					var hide_rest_obj = target.find("div."+pThis.ctrlClassName+"[data-key="+this.hide_rest_key[0]+"]");

					this.get_time_count(
						hide_rest_obj,
						t1,
						t2,
						userId,function(result){
							hide_rest_obj.find("input").val(result.hours != 0 ? result.hours : "");
							hide_rest_obj.trigger('check');
							target.find("#timeCount").remove();

							pThis.get_time_count_html(endTimeObj,result);
						}
					);
				}
			}

		},
		//事后事件
		afterEvent:function(params){
			var data = params.data,
				target = params.target,
				is_other_proc = params.is_other_proc;
			var pThis = this;
			var cacheData = target.data("cacheData"),key = cacheData.input_key;
			var otherTarget = target.siblings();

			//补录
			if(this.is_other_proc == 3){
				var fix_makeup_data = target.data("cacheData");

				if(fix_makeup_data.type == "select" && fix_makeup_data.input_key == this.fix_makeup_key[0]){
					fix_makeup_data.val = fix_makeup_data.val_id;
				}
			}
			//4请假 5 外出
			else if(this.is_other_proc == 4 || this.is_other_proc == 5){
				var jqObj = this.jqObj;

				//开始时间
				var startTimeObj = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[1]+"]"),
					startTimeCacheData = startTimeObj.data("cacheData"),
					startTimeVal = startTimeCacheData.val || "";

				//结束时间
				var endTimeObj = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[2]+"]"),
					endTimeCacheData = endTimeObj.data("cacheData"),
					endTimeVal = endTimeCacheData.val || "";

				//请假类型
				if(key == this.fix_rest_key[0]){
					console.log(0);
					jqObj.find("#otherFixRest").remove();
					jqObj.find("#timeCount").remove();

					var s = target.find("option:selected");
					var min_unit = s.attr("data-minunit");
					var format = min_unit == 1 ? "datetime" : "date";

					//时间格式修改
					if(min_unit == 1){
						if(startTimeVal != "" && startTimeVal.indexOf(" ") <= 0){
							startTimeVal += " 00:00";
						}
					}else{
						if(startTimeVal.indexOf(" ") >= 0){
							startTimeVal = startTimeVal.substring(0,startTimeVal.indexOf(" "));
						}
						if(endTimeVal.indexOf(" ") >= 0){
							endTimeVal = endTimeVal.substring(0,endTimeVal.indexOf(" "));
						}
					}

					//时间类型改变
					startTimeCacheData.format = format;
					startTimeCacheData.val = startTimeVal;
					startTimeObj.trigger("changeType");
					if(min_unit == 1){
						//跳过验证直接执行事后事件
						startTimeObj.trigger("afterEvent");
					}

					endTimeCacheData.format = format;
					endTimeCacheData.val = endTimeVal || startTimeVal;
					endTimeObj.trigger("changeType");

					//改变结束时间最小时间值，以开始时间为准
					if(min_unit == 0 && startTimeVal){
						endTimeCacheData.minVal = startTimeVal;
						endTimeObj.trigger("changeMinTime");
						if(endTimeCacheData.val){
							endTimeObj.trigger("check");
						}
					}
				}
				//开始时间
				else if(key == this.fix_rest_key[1]){
					console.log(1);
					var selectObj = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[0]+"]");
					var option = selectObj.find("select  option:selected");

					function common(){
						endTimeCacheData.minVal = data.val;
						//如果开始时间比结束时间大，则清除
						if(new Date(startTimeVal).getTime() > new Date(endTimeVal).getTime()){
							//清除结束时间控件里的值
							endTimeCacheData.clearVal = 1;
							jqObj.find("#timeCount").remove();
						}
						endTimeObj.trigger("changeMinTime");

						if(!endTimeCacheData.val){return;}
						//检查结束时间状态
						endTimeObj.trigger('check');
					}

					//如果是日期类型不显示
					if(this.is_other_proc == 5 || option.attr("data-minunit") == "1"){
						pThis.get_other_time_data(target,startTimeVal,function(){
							common();
						});
					}else{
						common();
					}
				}
				//结束时间
				else if(key == this.fix_rest_key[2]){
					console.log(2);
					var hide_rest_obj = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.hide_rest_key[0]+"]");
					hide_rest_obj.find("input").val("0");

					if(!startTimeVal || !endTimeVal){return;}

					var otherFixRest = jqObj.find("#otherFixRest");
					var o = otherFixRest.length > 0 ? otherFixRest : endTimeObj;

					startTimeObj.attr("data-checked",1);

					var t1 = new Date(startTimeVal.replace('-','/').replace('-','/')).getTime() / 1000,
					t2 = new Date(endTimeVal.replace('-','/').replace('-','/')).getTime() / 1000;

					pThis.get_time_count(
						o,
						t1,
						t2,
						null,
						function(result){
							hide_rest_obj.find("input").val(result.hours != 0 ? result.hours : "");
							hide_rest_obj.trigger('check');
							jqObj.find("#timeCount").remove();

							pThis.get_time_count_html(o,result);
						}
					);
				}
			}
		},
		getTime:function(timestamp,hideTime){
			var time = t.getNowDate(parseInt(timestamp)*1000,1);
			var nowTime = t.getNowDate(null,1);

			return (time.year != nowTime.year ? time.year + "年" : "") + time.month + "月"+time.days+"日"+" " 
			+ "(周"+t.getWeek(time.week)+") " 
			+(hideTime ? "" : time.hours+":"+time.minutes); 
		},
		//获取补录时间中
		get_fix_makeup:function(target,data,userId,callback){
			var pThis = this;
			layerPackage.lock_screen({
				"msg":"获取补录时间中..."
			});
			var parmas = {
				url:pThis.fix_makeup_url,
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					console.log(result);
					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result]);
					}

					layerPackage.unlock_screen();
				}
			};

			if(userId){
				parmas.data = {
					"data":{
						user_id:userId
					}
				}
			}

			t.ajaxJson(parmas);
		},
		//获取请假类型
		change_fix_rest_type:function(ids,callback){
			if(!ids){return;}
			try{
				if(ids && !t.isArray(ids)){
					ids = ids.split(",");
				}
			}catch(e){}

			var pThis = this;
			layerPackage.lock_screen({
				"msg":"获取请假类型中..."
			});
			var parmas = {
				url:pThis.fix_rest_type_url,
				data:{
					data:{
						ids :ids
					}
				},
				callback:function(result,state){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result]);
					}

					layerPackage.unlock_screen();
				}
			};

			t.ajaxJson(parmas);
		},
		//隐藏请假时长
		hide_hide_rest_key:function(target){
			target.find("div."+this.ctrlClassName+"[data-key="+this.hide_rest_key[0]+"]").addClass('hide');
		},
		//可选时间段
		get_other_time_data:function(target,startTime,callback){
			var pThis = this;
			var jqObj = this.jqObj;

			startTime = startTime ? new Date(startTime).getTime() / 1000 : undefined;

		    if(pThis.abort1 && pThis.abort1.readystate!=4){
				pThis.abort1.abort();
				pThis.abort1 = null;
				//return;
			}

			var parmas = {
				url:pThis.other_time_url,
				data:{
					data:{
						"start_time":startTime
					}
				},
				type:"post",
				dataType:"json"
			};

			pThis.abort1 = $.ajax(parmas).done(function (result, status, e) {
		        if(result.errcode !="0"){
					layerPackage.unlock_screen();//遮罩end
					layerPackage.fail_screen(result.errmsg);
					return;
				}

				var time  = result.time;
				var initTime = result.init;

				jqObj.find("#otherFixRest").remove();

				if(time.length > 0){
					var dom = $($.trim(pThis.get_other_time_html(time,startTime)));

					var o = jqObj.find("div."+pThis.ctrlClassName+"[data-key="+pThis.fix_rest_key[2]+"]");
					o.after(dom);
					pThis.bindOtherFixRestDom();
				}

				if(callback && t.isFunction(callback)){
					callback();
				}

				pThis.abort1 = null;
		    }).fail(function (msg, status, e) {
		        
		    });
		},
		//可选时间段HTML
		get_other_time_html:function(data,startTime){
			var html = new Array();

			var newTime = t.getNowDate(startTime*1000,1);

			html.push('<section id="otherFixRest" class="autoH">');
			html.push('<div class="bg-gray">');
			html.push('<p class="c-4a"><i class="rulerIcon rulericon-magic_wand mr6"></i>快速选择时间（'+newTime.month + '月'+newTime.days+'日 周'+t.getWeek(newTime.week)+'）</p>');
			html.push('<div class="mt15 fs12 lhn">');
			for (var i = 0; i < data.length; i++) {
				html.push('<span class="bg-white c-4a" data-starttime="'+data[i].start_time+'" data-endtime="'+data[i].end_time+'">');
				if(data[i].date){
					html.push(data[i].date+" (全天)");
				}else{
					html.push(this.getTime2(data[i].start_time,startTime)+'~'+this.getTime2(data[i].end_time,startTime));
				}
				html.push('</span>');
			};
			html.push('</div>');
			html.push('</div>');
			html.push('</section>');

			return html.join('');
		},
		getTime2:function(timestamp,secondTimestamp){
			var time = t.getNowDate(parseInt(timestamp)*1000,1);
			var nowTime = t.getNowDate(secondTimestamp ? parseInt(secondTimestamp)*1000 : secondTimestamp,1);

			return (time.year != nowTime.year ? time.year + "年" : "") 
			+ (time.month != nowTime.month ? time.month + "月" : "")  
			+ (time.days != nowTime.days ? time.days + "日" : "")  
			+ time.hours+":"+time.minutes; 
		},
		//快捷时间事件
		bindOtherFixRestDom:function(){
			var pThis = this,jqObj = pThis.jqObj;
			jqObj.find("#otherFixRest span").on("click",function(){
				var startTime = $(this).attr("data-starttime"),
					endTime = $(this).attr("data-endtime");

				//开始时间
				var startTimeObj = jqObj.find("div."+pThis.ctrlClassName+"[data-key="+pThis.fix_rest_key[1]+"]"),
					startTimeCacheData = startTimeObj.data("cacheData");
					startTimeCacheData.val = t.dateFormat(startTime);

				//特别配置，不检查执行afterEvent，并且将验证状态置为通过
				startTimeCacheData.noCheck = 1;
				//只改变值
				startTimeObj.trigger("changeData");

				//结束时间
				var endTimeObj = jqObj.find("div."+pThis.ctrlClassName+"[data-key="+pThis.fix_rest_key[2]+"]"),
					endTimeCacheData = endTimeObj.data("cacheData");
					endTimeCacheData.val = t.dateFormat(endTime);

				endTimeObj.trigger("changeData");
			});
		},
		//获取补录总时间
		get_time_count:function(target,startTime,endTime,userId,callback){
			var jqObj = this.jqObj;
			var fix_rest_obj = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[0]+"]");
			var fix_rest_data = fix_rest_obj.data("cacheData");
			var option = fix_rest_obj.find("select  option:selected");

			var fix_rest_endTime = jqObj.find("div."+this.ctrlClassName+"[data-key="+this.fix_rest_key[2]+"]");
			var fix_rest_endTime_data = fix_rest_endTime.data("cacheData");
			
			if(this.is_other_proc == 4 && fix_rest_endTime_data.edit_input == 1 && ((option.length > 0 && option.attr("data-minunit") == 0) || fix_rest_data.minunit == 0)){
				endTime += 60*60*24 - 1;
			}

			var pThis = this;
			/*layerPackage.lock_screen({
				"msg":"获取补录时间中..."
			});*/
	    	if(pThis.abort2 && pThis.abort2.readystate!=4){
				pThis.abort2.abort();
				pThis.abort2 = null;
				//return;
			}

			var parmas = {
				url:pThis.time_count_url,
				data:{
					data:{
						"start_time":startTime,
						"end_time":endTime,
						"workitem_id":pThis.workitemId
					}
				},
				type:"post",
				dataType:"json"
			};

			if(userId){
				parmas.data.data.user_id = userId;
			}

			if(this.is_other_proc == 4){
				var val = fix_rest_data.val;
				parmas.data.data.rest_type = t.isArray(val) ? val[0] : val;//option.val();
			}

			pThis.abort2 = $.ajax(parmas).done(function (result, status, e) {
		        if(result.errcode !="0"){
					layerPackage.unlock_screen();//遮罩end
					//layerPackage.fail_screen(result.errmsg);
					jqObj.find("#timeCount").remove();
					return;
				}

				console.log(result);

				if(callback && t.isFunction(callback)){
					callback.apply(pThis,[result]);
				}

				pThis.abort2 = null;

				layerPackage.unlock_screen();
		    }).fail(function (msg, status, e) {
		        
		    });
		},
		//获取补录总时间模板HTML
		get_time_count_html:function(target,result){
			var html = new Array();

			html.push('<section class="autoH bg-green2 fs12 c-4a" id="timeCount">');
			if(result.hasOwnProperty("year_balance")){
				if(result.year_balance >= 0){
					html.push('有效时长为<i class="c-red fsn">'+result.hours+'</i>小时，约<i class="c-red fsn">'+result.day+'天</i>。申请后，年假剩余<i class="c-red fsn">'+result.year_balance+'</i>天');
				}else{
					html.push('剩余年假天数不足，当前剩余 <i class="c-red fsn">'+result.all_year+'</i> 天');
				}
			}else{
				if(result.hours == 0){
					html.push("有效时间为0小时，请重新选择");
				}else{
					html.push('申请有效时长是<i class="c-red fsn">'+result.hours+'</i>小时，约<i class="c-red fsn">'+result.day+'</i>天');
				}
			}
			
			html.push('</section>');

			var dom = $($.trim(html.join('')));

			target.after(dom);
		},
		//根据不用表单类型改变保存地址
		getSaveOrEditUrl:function(is_other_proc,url){
			if(is_other_proc == 0 || is_other_proc == 1 || is_other_proc == 2){
				return url;
			}else if(is_other_proc == 3 || is_other_proc == 4 || is_other_proc == 5){
				return "/wx/index.php?model=workshift&m=ajax&cmd=109";
			}else if(is_other_proc == 6){
				//报销
				return "/wx/index.php?model=expense&m=ajax&a=index&cmd=103";
			}

			return url;
		},
		//补录时间修改
		set_fix_makeup:function(target,data){
			try{
				target.html(this.getTime(new Date(data.val[0].replace('-','/').replace('-','/')).getTime() / 1000));
			}catch(e){
				console.log(e);
			}
		},
		//隐藏隐藏控件
		hide_hide_rest_key:function(target){
			target.find("div."+this.ctrlClassName+"[data-key="+this.hide_rest_key[0]+"]").addClass('hide');
		},
		/*------定位信息-------------*/
		getRotaTime:function(time,showSecond){
			time = this.checkTime(time);

			var nowTime = t.getNowDate(time,1);

			return nowTime.hours + ":" + nowTime.minutes + (showSecond ? ":" + nowTime.seconds : "");
		},
		getDiffTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;
			var json = t.getDiffTime(firstTime,secondTime,1);

			return (json.days ? json.days + "天" : "") + (json.hours ? json.hours + "小时" : "") 
			+(json.minutes ? json.minutes + "分钟" : "")+(json.seconds ? json.seconds + "秒" : "");
		},
		getLocationTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;
			var json = t.getDiffTime(firstTime,secondTime,1);

			var str = "";

			var flag = false;
			var m = this.checkTime(firstTime || secondTime);
			var time = t.getNowDate(m,1);

			if(parseInt(time.month) != parseInt(t.getNowDate(null,1).month)){
				str += parseInt(time.month) + "月" + (time.days ?  time.days + "日" : "");
				flag = true;
			}

			if(json.days && !flag){
				if(json.days == 1 && firstTime && !secondTime){
					str += "昨天";
				}else if(json.days == 1 && !firstTime && secondTime){
					str += "明天";
				}
			}else{
				str += time.days+"日";
			}

			return str + " " + time.hours + ":" + time.minutes;
		},
		checkTime:function(time){
			if(time.toString().length == 10){
				time = parseInt(time) * 1000;
			}

			return time;
		},
		getLocationInfo:function(target,listType,formsetinstId){
			var id = t.getUrlParam("id");
			var pThis = this;

			if(!id){return;}

			this.getSignInInfo(formsetinstId,function(result){
				if(listType == 2){
					//this.initMap(target);
				}

				tpl.register('getRotaTime', this.getRotaTime);
				tpl.register('getDiffTime', this.getDiffTime);
				tpl.register('getShortTime', t.getShortTime);
				tpl.register('getLocationTime', this.getLocationTime);
				var dom = tpl(otherProcessTpl,{
					getLocation:1,
					data:result,
					hideBtn:listType != 2 ? true : false
				});
				tpl.unregister('getRotaTime');
				tpl.unregister('getDiffTime');
				tpl.unregister('getShortTime');
				tpl.unregister('getLocationTime');
				
				//防止重复项
				target.find(".otherInfo").remove();
				target.find(".tabList").before("<div class='otherInfo'></div>");
				target.find(".otherInfo").html($($.trim(dom)));

				GenericFramework.init(wxJsdkConfig);

				if(result.clock_list.length > 0){
					this.getClockList(target,result.clock_list);	
				}

				this.bindLocationDom(target);

				result.time = this.checkTime(result.time || new Date().getTime());
				var i = 0;
				setInterval(function(){
					i++;
					target.find(".clickTime").text(pThis.getRotaTime((result.time + i*1000),1));
				},1000);
			});
		},
		getSignInInfo:function(id,callback){
			var pThis = this;
			var url = "/wx/index.php?model=workshift&m=ajax&cmd=103";

			t.ajaxJson({
				url:url,
				data:{
					data:{
						"formsetinst_id":id
					}
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						//layerPackage.fail_screen(result.errmsg);
						return;
					}

					pThis.data = result;

					callback.apply(pThis,[result]);

					layerPackage.unlock_screen();
				}
			});
		},
		getClockList:function(target,data,append){
			tpl.register('getRotaTime', this.getRotaTime);
			tpl.register('getDiffTime', this.getDiffTime);
			tpl.register('getShortTime', t.getShortTime);
			var dom = $($.trim(tpl(otherProcessTpl,{
				getClockList:1,
				data:data,
				append:append
			})));

			GenericFramework.previewImage({
				target:dom.find(".img_div")
			});
			
			tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');
			tpl.unregister('getShortTime');

			if(!append){
				target.find(".clockList").append(dom);
			}else{
				target.find(".clockList").prepend(dom);
			}
		},
		bindLocationDom:function(target){
			var pThis = this;

			//定位按钮
			target.find(".locationBtn").on("click",function(){
				pThis.locationContent(target);
			});

			target.on("click",".locationContent",function(){
				$(this).remove();
			});

			GenericFramework.previewImage({
				target:target.find(".img_div")
			});
		},
		locationContent:function(target){
			var dom = tpl(otherProcessTpl,{
				locationContent:1
			});

			var o = target.find(".locationContent");

			if(o.length > 0){
				o.remove();
			}

			target.append($($.trim(dom)));

			this.locationContentEvent(target);

			setTimeout(function(){
				target.find(".locationContent .content .address").click();
			},0);
		},
		localInfo:{
			address:null,
			longt:null,
			lat:null,
			image_list:[]
		},
		locationContentEvent:function(target){
			var pThis = this;
			target.find(".locationContent .content").on("click",function(e){
				e.preventDefault();
				e.stopPropagation();
			});

			target.find(".locationContent .content .addComment").on("click",function(e){
				$(this).addClass('hide');
				$(this).next().removeClass('hide');
			});

			//刷新获取地理位置
			var freshBtn = target.find(".locationContent .content .address");
			GenericFramework.location({
				target:freshBtn,
				callback:{
					beforeGetLocation:function(){
						freshBtn.find(".add").addClass('hide');
						freshBtn.find(".search-address").removeClass('hide');
					},
					afterGetLocation:function(res,lng,lat){
						var address = res.address;
						pThis.localInfo.address = address;
						pThis.localInfo.longt = lng;
						pThis.localInfo.lat = lat;

						freshBtn.find(".add").removeClass('hide');
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span.add").text(address);
					},
					error:function(msg){
						alert(msg);
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span").text("请重新点击获取");
					}
				}
			});

			var cameraBtn = target.find(".locationContent .content .cameraBtn");
			var picObj = target.find(".camera-pic");
			GenericFramework.uploadFile({
				target:cameraBtn,
				type:"wx",
				sourceType:2, //拍照
				callback:{
					afterUpload:function(imgInfo){
						//console.log(imgInfo);
						picObj.removeClass('hide');
						picObj.find("img").attr("src",imgInfo.image_path);

						//alert(JSON.stringify(imgInfo));

						pThis.localInfo.image_list.push({
							hash:imgInfo.hash,
							size:imgInfo.filesize,
							url:imgInfo.path,
							name:imgInfo.name,
							ext:imgInfo.ext,
							image_path:imgInfo.image_path
						});

						cameraBtn.addClass('hide');
					}
				},
				wx:{
					uploadUrl:"/wx/index.php?model=workshift&m=ajax&a=upload",
					uploadData:{
						data:{
							addr:pThis.localInfo.address
						}
					},
					beforeStartCallback:function(_setting){
						if(!pThis.localInfo.address){
							layerPackage.fail_screen("请获取地址后再拍照!");
							return false;
						}

						_setting.wx.uploadData = {
							data:{
								addr:pThis.localInfo.address
							}
						}

						return true;
					}
				}
			});
	
			GenericFramework.previewImage({
				target:picObj
			});
			
			picObj.on('click','i',function(event){
                event.stopPropagation();
                
                pThis.localInfo.image_list.pop();

                picObj.find("img").attr("src","");

                cameraBtn.removeClass('hide');
                picObj.addClass('hide');
            });

            target.find(".locationContent .content .submitBtn button").on("click",function(e){
            	var data = pThis.data;
            	var localInfo = pThis.localInfo;

            	if(!localInfo.address){
            		layerPackage.fail_screen("请重新获取地址!");
            		return;
            	}

            	if(localInfo.image_list.length == 0 && data.is_photo == 1){
            		layerPackage.fail_screen("请拍照!");
            		return;
            	}

            	var textarea = target.find(".locationContent .content textarea");

            	pThis.setLocationApi({
            		"id":data.legwork_record.record_id,
            		"addr":localInfo.address,
            		"lng":localInfo.longt,
            		"lat":localInfo.lat,
					"file_list":localInfo.image_list,
					"mark":textarea.val()
            	},function(result){
					pThis.getClockList(target, [{
						"addr_name": localInfo.address || "",
						"clock_time": pThis.checkTime(result.time || new Date().getTime()),
						"mark": textarea.val() || "",
						"file_path": localInfo.image_list.length == 0 ? [] : [localInfo.image_list[0].image_path]
					}],1);

            		pThis.localInfo.address =null;
					pThis.localInfo.longt =null;
					pThis.localInfo.lat =null;
					pThis.localInfo.image_list = [];
					textarea.val("");

					target.find(".locationContent").click();
            	});
			});
		},
		setLocationApi:function(data,callback){
			var pThis = this;

			t.ajaxJson({
				url:"/wx/index.php?model=workshift&m=ajax&cmd=104",
				data:{
					data:data
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					if(callback && t.isFunction(callback)){
						callback(result);
					}
				}
			});
		}
		/*------定位信息-------------*/
	};

	return otherProcess;
});