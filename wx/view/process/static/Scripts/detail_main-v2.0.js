require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'process:static/Scripts/detail-v3.0',
	'process:static/Scripts/process_common-v2.0'
],function(t,UiFramework,detail,common) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen(); //start

	var container = $("#main-container"),
	id = t.getHrefParamVal("id") || 1,
	listType = t.getHrefParamVal("listType") || 1,
	activekey = t.getHrefParamVal("activekey") || 0;

	var isIndex = t.getHrefParamVal("isIndex") || "",
		hash = window.location.hash;

	if(isIndex){
		isIndex = "&isIndex="+isIndex;
	}
	
	//*
	detail.init({
		target:container,
		id:id,
		listType:listType,
		activekey:activekey,
		callback:{
			//错误回调处理
			error_callback:function(result){
				layerPackage.unlock_screen();//遮罩end
				//layerPackage.fail_screen(result.errmsg);

				layerPackage.ew_alert({
					title:result.errmsg,
					action_desc:'ok,我知道了',
					top:40,
					callback:function(){
						var corpurl = t.getHrefParamVal('corpurl') || "";

						if(corpurl){
							try{
								require(['common:widget/lib/GenericFramework-v2.0'],function(GenericFramework){
									GenericFramework.wxCloseWindow();
								});
							}catch(e){
								console.log(e);
							}

							return;
						}

						if(isIndex){
							window.history.go(-1);
						}
					}
				});
			},
			//成功回调处理
			success_callback:function(result,ctrlName){
				if(ctrlName == "urge"){
					layerPackage.unlock_screen();
					result.errmsg = "催办成功";
				}

				if(ctrlName == "sign"){
					layerPackage.unlock_screen();
					window.location.href = result.info.redirect_url;
					return;
				}

				if(ctrlName == "backStart" || ctrlName == "backPrev"){
					//layerPackage.unlock_screen();
					//window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
					return;
				}

				if(ctrlName == "del"){
					//layerPackage.unlock_screen();
					//window.location.href = "/wx/index.php?app=process&a=mineList&debug=1";
					return;
				}

				if(ctrlName == "goToBack"){
					layerPackage.unlock_screen();

					if(isIndex){
						common.toIndexDealList();
						return;
					}

					window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
					return;
				}

				if(ctrlName == "nextNode"){
					return;
				}

				if(ctrlName == "backSelect"){
					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					setTimeout(function(){
						if(isIndex){
							common.toIndexDealList();
							return;
						}

						window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
						return;
					},1500);

				}

				layerPackage.success_screen(result.errmsg);
			},
			//完成回调处理s
			finish_callback:function(result){
				layerPackage.unlock_screen();

				var pcWxCode = t.getHrefParamVal("pcWxCode");
				//审批
				if(pcWxCode == "1"){
					$("#wx_bottom_menu").find("li:last > div").click();
				}
				//退回
				else if(pcWxCode == "0"){
					$("#wx_bottom_menu").find("li:first > div").click();
				}
			}
		}
	});
	//*/
},function(e){
	requireErrBack(e);
});



