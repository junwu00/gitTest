require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'process:static/Scripts/process_common'
],function($,t,UiFramework,depts_users_select,common) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var notifyUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=118",
	base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
	dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
	user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
	user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
	init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids';

	//jqObj对象
	var container = $("#main-container"),
		listType = t.getHrefParamVal("listType"),
		id = t.getHrefParamVal("id"),
		formsetinstId = t.getHrefParamVal("formsetinstId");

	container.find(".select").on('click',function(){
		container.find("#getUser").click();
	});

	var dept = new depts_users_select().init({
		target:container.find("#getUser"),
		select:{},
		type :3,
		max_select:0,
		base_dept_url : base_dept_url,
		dept_url : dept_url,
		user_url : user_url,
		user_detail : user_detail,
		init_selected_url : init_selected_url,
		canEdit:1,
		ret_fun:function(self){
			var data = self.get_depts_users();
			console.log(data);
			// mrc add
			// 选择部门后加载所有, 替换全选
			layerPackage.lock_screen()
			var opt = {}
					opt.name = ''
					opt.dept = '';
					opt.offset = 0;
					opt.limit = 9999;

			for(var index = 0; index < data.length; index++) {
				if (data[index].isDept == "true") {
					opt.dept += data[index].id + ',';
				}
			}

			if (opt.dept) {
				$.ajax({
					async: true,
					url: '/wx/index.php?model=legwork&m=ajax&a=contact_list_user' + '&time='+(new Date()).getTime(),
					type:'post',
					data_type:'json',
					data:{data:opt},
					success:function(res){
						try {
							res = $.parseJSON(res);
						} catch (e) {
							console.log(e);
							console.log('返回数据转json出错');
							layerPackage.ew_alert({'desc':'加载数据异常'});
						}
						if(res.errcode != 0){
							layerPackage.fail_screen(res.errmsg);
							return;
						}
						data = data.concat(res.info.data)
						_old_callback()
						layerPackage.unlock_screen()
					},
					error:function(){
						console.log('系统繁忙:查找部门人员失败');
					}
				})
			} else {
				_old_callback()
				layerPackage.unlock_screen()
			}
			// mrc add end
			function _old_callback() {
				container.find("section.user").remove();
				var _record = {} // mrc add
				for (var i = 0; i < data.length; i++) {		
					if (_record[data[i]['id']] || data[i]['isDept'] == 'true') { continue } // mrc add
					_record[data[i]['id']] = 1 // mrc add
					var html = new Array();
					html.push('<section class="bt1 h46 user" data-id="'+data[i].id+'">');
					html.push('<img src="'+data[i].pic_url+'" alt="" class="fl icon br6" onerror="this.src=\''+defaultFace+'\'"/>');
					html.push('<span>'+data[i].name+'</span>');
					html.push('<span class="fr circle-checkbox"></span>');
					html.push('</section>');

					var dom = $($.trim(html.join('')));

					
					UiFramework.btnPackage().checkBoxBtn.init({
						target:dom.find('.circle-checkbox'),
						status:true,
						type:1,
						callback:{
							on:function(obj,self){
								obj.parents(".user").data("cacheData").status = true;
							},
							off:function(obj,self){
								obj.parents(".user").data("cacheData").status = false;
							}
						}
					});

					dom.data("cacheData",{id:data[i].id,name:data[i].name,status:true})

					container.find("section:first").after(dom);
				};
			}
		}
	});

	UiFramework.bottomInfo.init(container,77);

	UiFramework.wxBottomMenu.init({
		target:container,
		menu: [{
			width: 30,
			name: "上一步",
			iconClass: "icon-undo",
			btnColor: "w",
			id: null,
			click: function(self, obj, index) {
				common.toDetail(id,listType);
				//window.location.href = "/wx/index.php?model=process&a=detail&degbug=1&listType="+listType+"&id="+id;
			}
		},{
			width: 70,
			name: "发送",
			iconClass: "icon-plus-sign",
			btnColor: "g",
			id: null,
			click: function(self, obj, index) {
				var receivers = new Array(),name = new Array();
				var user = container.find(".user");
				$.each(user, function(index, val) {
					var data = $(this).data("cacheData");
					if(data.status == true){
						receivers.push({
							"receiver_id":data.id,
							"receiver_name":data.name
						});

						name.push(data.name);
					}
				});

				if(dept.get_depts_users().length == 0 || receivers.length == 0){
					layerPackage.ew_alert({
						"title":"至少选择一个知会人！"
					});
					return;
				}

				layerPackage.ew_confirm({
					title:"确定要将该流程知会给 " + name.join(',') + "?",
					ok_callback:function(){
						t.ajaxJson({
							url:notifyUrl,
							data:{
								"data":{
									"formsetinst_id":formsetinstId,
									"receivers":receivers
								}
							},
							callback:function(result,status){
								if(result.errcode !=0){
									layerPackage.fail_screen(result.errmsg);
									return;
								}

								layerPackage.success_screen(result.errmsg);
								var activekey = t.getHrefParamVal("activekey") || 0;
								common.toDealList(activekey);
								//window.location.href = "/wx/index.php?model=process&a=dealList&degbug=1&activekey=1";
							}
						});
					}
				});
			}
		}]
	});

	layerPackage.unlock_screen();
});