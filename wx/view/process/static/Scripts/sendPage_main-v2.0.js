define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/process/Templates/sendPage-v2.0.html',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'common:widget/lib/scFormCtrl/scFormCtrl-v2.0'
],function($,t,UiFramework,tpl,senPageTpl,depts_users_select,ScFormCtrl) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var nextNodeUrl = "index.php?model=process&m=ajax&a=index&cmd=107",
		goToBackUrl = "/wx/index.php?model=process&m=ajax&a=index&cmd=116",
		base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
		dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
		user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
		user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
		init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids';

	//发送逻辑处理
	var sendPage = {
		init:function(common,params){
			this.common = common;

			this.workitemId = params.workitem_id;
			this.formsetInstId = params.formsetInst_id;
			this.list = params.list;
			this.isIndex = params.isIndex;
			this.hash = params.hash;

			this._setting = params._setting;
			this.detail = params.detail;

			this.isUrl = params.isUrl; //是跳转过来的

			this.getNextNode(this.workitemId);
		},
		bindDom:function(){
			var pThis = this;
			$(window).on('hashchange',function(){
				var hash = window.location.hash;
				var sendPageObj = $("body").find("#sendPage");

				//删除弹出框
				$(".alert_screen").remove();

				if(pThis.isUrl){
					if(hash != pThis.hash){
						sendPageObj.addClass('hide');
					}else{
						sendPageObj.removeClass('hide');
					}
				}else{
					if(hash != "#sendPage"){
						sendPageObj.addClass('hide');
					}else{
						sendPageObj.removeClass('hide');
					}
				}
			});
		},
		//下一个节点信息
		getNextNode:function(workitem_id,callback){
			if(!this.isUrl){
				window.location.hash = "sendPage";
			}

			var pThis = this;
			var sendPageObj = $("body").find("#sendPage");
			sendPageObj.remove();

			//if(sendPageObj.length == 0){
				var html = new Array();

				html.push('<div id="sendPage" class="w_100 h_100 bg-white">');
				html.push('</div>');

				$("body").append(html.join(''));

				this.bindDom();
			/*}else{
				sendPageObj.removeClass('hide');
				return;
			}*/

			t.ajaxJson({
				url:nextNodeUrl,
				data: {
					"data": {
						"workitem_id":workitem_id
					}
				},
				callback:function(result,status){
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					var data = result.info;
					console.log(data);
					pThis.data = data;
					pThis.isSend = data.is_send;
					pThis.workitemId = data.workitem_id;

					var dom = tpl(senPageTpl,{
						data:data,
						cdn_domain:cdn_domain,
						defaultFace:defaultFace,
						media_domain:media_domain
					});

					var sendPageObj = $("body").find("#sendPage");
					sendPageObj.html($($.trim(dom)));

					//输入框意见处理
					pThis.judgementSolve(sendPageObj);

					//绑定基础事件
					pThis.bindEventDom(sendPageObj);

					//绑定人员部门选择器
					pThis.bindUserDept(sendPageObj);

					//绑定按钮事件
					pThis.bindBtn(sendPageObj);

					//添加底部信息
					pThis.addBottom(sendPageObj);

					layerPackage.unlock_screen();
				}
			});
		},
		//输入框意见处理
		judgementSolve:function(sendPageObj){
			var _setting = this._setting;

			if(!_setting || !this.detail){return;}
			var pThis = this,
				workitemState = _setting.workitemState,
				isStart = _setting.isStart,
				workType = _setting.workType,
				isLinkSign = _setting.isLinkSign,
				formsetinstState = _setting.formsetinstState,
				formsetinstId = _setting.formsetinstId,
				listType = _setting.listType,
				id = _setting.id,
				workitemId = _setting.workitemId,
				isReturnBack = _setting.isReturnBack;

			if((workitemState == 1 || workitemState == 2 || workitemState == 3) && isStart == 0){
				//非签名
				if(!(workType == 0 && isLinkSign == 1)){
					var must = 1,
						placeholder = "请输入同意意见……",
						val = "同意",
						title = "审批意见";

					if(workType == 1){
						must = 0;
						placeholder = "请输入备注信息(选填)";
						val = "",
						title = "备注信息";
					}

					var dom = ScFormCtrl(pThis.common.getJudgementData(must,title,val,placeholder,"请填写审批意见"),{check:1});

					sendPageObj.find(".nodeName").before(dom);
				}
			}
		},
		//默认选中节点
		selectNodeId : null,
		selectIndex:0,
		sendBack:null,
		bindEventDom:function(sendPageObj){
			var pThis = this;
			sendPageObj.find(".nodePartTitle").on("click",function(){
				var o = $(this),p_o = o.parent(".nodePart"),other_p_o = p_o.siblings('.nodePart');

				if(p_o.hasClass('current')){
					p_o.removeClass('current');
					p_o.find(".nodePartContent").addClass('hide');
					pThis.selectNodeId = null;
					pThis.selectIndex = 0;
					pThis.sendBack = null;
					return;
				}

				pThis.selectNodeId = p_o.attr("data-id");
				pThis.selectIndex = p_o.attr("data-index");
				pThis.sendBack = p_o.attr("data-sendback");

				p_o.addClass('current');
				other_p_o.removeClass('current');

				p_o.find(".nodePartContent").removeClass('hide');
				other_p_o.find(".nodePartContent").addClass('hide');
			});

			var nodePartObj = sendPageObj.find(".nodePart");
			if(nodePartObj.length == 1){
				nodePartObj.find(".nodePartTitle").click();
			}
		},
		//绑定按钮事件
		bindBtn:function(sendPageObj){
			var pThis = this;
			sendPageObj.find('.square-checkbox').each(function(){
				var o = $(this),pObj = o.parents(".user"),
				id = pObj.attr("data-id"),
				name = pObj.attr("data-name");
				pObj.data("cacheData",{
					id:id,name:name,status:false
				});

				if(pObj.hasClass('receiveArr')){
					UiFramework.btnPackage().checkBoxBtn.init({
						target: o,
						status: true,
						type: 2,
						callback: {
						},
						cancelEvent:true
					});

					pObj.data("cacheData").status = true;
				}else if(pObj.hasClass('selectArr')){
					UiFramework.btnPackage().checkBoxBtn.init({
						target: o,
						status: false,
						type: 2,
						callback: {
							on: function(obj, self) {
								pObj.data("cacheData").status = true;
							},
							off: function(obj, self) {
								pObj.data("cacheData").status = false;
							}
						}
					});
				}
			});
		},
		//绑定人员部门选择器
		bindUserDept:function(sendPageObj){
			var pThis = this;
			sendPageObj.find(".clickObj").on('click',function(){
				$(this).next(".getUser").click();
			});

			sendPageObj.find(".getUser").each(function(){
				var o = $(this);
				var pObj = o.parents('.nodePartContent');

				var dept = new depts_users_select().init({
					target:o,
					select:{},
					type :2,
					max_select:0,
					base_dept_url : base_dept_url,
					dept_url : dept_url,
					user_url : user_url,
					user_detail : user_detail,
					init_selected_url : init_selected_url,
					canEdit:1,
					ret_fun:function(self){
						var data = self.get_depts_users();
						console.log(data);

						//清除之前已有选项
						pObj.find("div.select").remove();
						pObj.find("div.w-xLine").remove();

						for (var i = 0; i < data.length; i++) {		
							var html = new Array();
							html.push('<div class="h46 user select" data-id="'+data[i].id+'">');
							html.push('<div class="fl square-checkbox mr6"></div>');
							html.push('<img src="'+data[i].pic_url+'" alt="" class="mr6 fl" onerror="this.src=\''+defaultFace+'\'"/>');
							html.push('<span class="selectName fs14">'+data[i].name+'</span>');
							html.push('</div>');
							html.push('<div class="wxLine"></div>');

							var dom = $($.trim(html.join('')));

							UiFramework.btnPackage().checkBoxBtn.init({
								target:dom.find('.square-checkbox'),
								status:true,
								type:2,
								callback:{
									on:function(obj,self){
										obj.parents(".user").data("cacheData").status = true;
									},
									off:function(obj,self){
										obj.parents(".user").data("cacheData").status = false;
									}
								}
							});

							dom.data("cacheData",{id:data[i].id,name:data[i].name,status:true})

							pObj.prepend(dom);
						};
					}
				});
			});
		},
		//添加底部信息
		addBottom:function(sendPageObj){
			var pThis = this,
				list = pThis.list,
				formsetInstId = pThis.formsetInstId,
				workitemId = pThis.workitemId,
				isSend = pThis.isSend,
				common = pThis.common;

			var clickStatus = true;

			UiFramework.wxBottomMenu.init({
				target:sendPageObj,
				menu: [{
					width: 30,
					name: "上一步",
					iconClass: "rulericon-turnback",
					btnColor: "w",
					id: null,
					click: function(self, obj, index) {
						if(list==2 || list == 1){
							window.history.back();
						}
						else{
							common.toDetail(workitemId);
						}
					}
				},{
					width: 70,
					name: "发送",
					iconClass: "rulericon-sendto",
					btnColor: "g",
					id: null,
					click: function(self, obj, index) {
						//自动发送则禁止点击
						/*if(isSend == 1 && !clickStatus){
							return;
						}*/

						var count = 0;
						var data = {};

						var  textareaObj =  sendPageObj.find(".scCtrl");
						if(pThis.detail && textareaObj.length > 0 && textareaObj.attr('data-checked') == "0"){
							return;
						}
						if(textareaObj.length > 0){
							data.judgement = textareaObj.data("cacheData").val;
						}
						
						data.workitem_id = workitemId;
						data.work_nodes = new Array();
						var receivers = new Array(),receiverName = new Array();

						var selectNodeObj = sendPageObj.find(".nodePart[data-id="+pThis.selectNodeId+"][data-index="+pThis.selectIndex+"] .nodePartContent");

						if(selectNodeObj.length == 0){
							layerPackage.ew_alert({
								"title":"至少选择一个节点！"
							});
							return false;
						}

						var is_end = selectNodeObj.eq(0).attr("data-isend");
						var work_node_name = selectNodeObj.eq(0).attr("data-name");

						selectNodeObj.each(function(){
							var work_node = {};
							work_node.id = $(this).attr("data-id");
							work_node.receiver_arr = new Array();

							is_end = $(this).attr("data-isend");

							if(is_end!="true"){
								$(this).find(".user").each(function(){
									var data = $(this).data("cacheData");

									if(data.status == true){
										var receiver = new Object();
										receiver.receiver_id = data.id;
										receiver.receiver_name = data.name;
										work_node.receiver_arr.push(receiver);
										count++;

										receiverName.push(data.name);
									}
								});
							}
							if(work_node.receiver_arr.length>0||isSend==true||is_end=="true"){
								data.work_nodes.push(work_node);
							}
						});

						if(count==0&&isSend==false&&is_end!="true"){
							layerPackage.ew_alert({
								"title":"至少选择下一步接收人！"
							});
							return false;
						}

						if(isSend==false){
							var work_node_name_str =  pThis.sendBack == "1" ? work_node_name : '发送到 '+work_node_name + ' 步骤';
							var content = '确定要将该流程'+work_node_name_str+",接收人为 "+receiverName.join(',')+" ?";
							if(is_end=="true"){
								content = '确定要将该流程'+work_node_name_str+"?";
							}
							layerPackage.ew_confirm({
								title:content,
								ok_callback:function(){
									if(pThis.sendBack == "1"){
										t.ajaxJson({
											url: goToBackUrl,
											data: {
												"data": {
													"workitem_id":workitemId,
													"judgement":data.judgement
												}
											},
											callback: function(result, status) {
												if (status == false && result == null) {return;}

												if (result.errcode != 0) {
													layerPackage.unlock_screen();//遮罩end
													layerPackage.fail_screen(result.errmsg);
													return;
												}

												layerPackage.success_screen(result.errmsg);


												setTimeout(function(){

													if(pThis.isIndex){
														common.toIndexDealList();
													}else{
														window.location.href = "/wx/index.php?app=process&a=dealList&debug=1";
													}

												},1500);
												
											}
										});
									}else{
										common.send(data,function(){
											if(list == 2 || list==1){
												common.toMineList();
											}
											else{
												common.toDealList();
											}
										});
									}
								}
							});
						}
						else{
							common.send(data,function(){
								common.toDealList();	
							});
						}
						
					}
				}]
			});
	
			//自动发生触发点击事件
			if(isSend == 1 && sendPageObj.find(".nodePart").length == 1 && sendPageObj.find(".scCtrl").length == 0){
				clickStatus = false;
				var clickObj = sendPageObj.find("#wx_bottom_menu").find("li:last .btn_g");
				clickObj.click();
				clickObj.off();
			}
		}
	};

	return sendPage;
});