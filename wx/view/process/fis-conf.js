require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'process');
fis.match('*', {
	release:'/process/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});