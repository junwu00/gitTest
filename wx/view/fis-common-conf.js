fis.set('product-cdn-domain', 'https://private.wx.easywork365.com/view_master');

fis.set('project.ignore', ['fis3-smarty/**','fis-conf.js','compile.bat', '**/ew_cp_*']);

//线上配置
fis.media('prod')
    .match('*.{js,css}', {
        domain : fis.get('product-cdn-domain')
    })
    .match('::image', {
        useMap: true,
        domain : fis.get('product-cdn-domain')
    });

//其他配置
fis.unhook('amd');
fis.unhook('commonjs');