wxJsdkConfig.debug = false;

require([
    'jquery',
    'common:widget/lib/tools',
    'common:widget/lib/UiFramework',
    "common:widget/lib/plugins/jweixin-1.1.0",

], function($,t,UiFramework,wx){

    var verify_sign_url = '/wx/index.php?model=recognize_demo&a=verify_sign';

    try {
        wxJsdkConfig.jsApiList = ['chooseImage', 'getSupportSoter', 'requireSoterBiometricAuthentication'];
        wx.config(wxJsdkConfig);

        wx.error(function(res){
            alert('error:' + JSON.stringify(res));
        });

        var evalWXjsApi = function(jsApiFun) {
            if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
                jsApiFun();
            } else {
                document.attachEvent && document.attachEvent("WeixinJSBridgeReady", jsApiFun);
                document.addEventListener && document.addEventListener("WeixinJSBridgeReady", jsApiFun);
            }
        }

        document.querySelector('#test-btn').onclick = function() {
            evalWXjsApi(function() {
                WeixinJSBridge.invoke("getSupportSoter", {}, function(res){
                    alert(JSON.stringify(res));
                });
            });
        }

        document.querySelector('#test-btn-2').onclick = function() {
            evalWXjsApi(function() {
                WeixinJSBridge.invoke("requireSoterBiometricAuthentication", {
                    "auth_mode": '0x1',
                    "challenge": "sample_challenge",
                    "auth_content": "请将使用指纹识别"

                },function(res) {
                    alert(JSON.stringify(res));
                    if (res.err_code == 0) {
                        alert('verify sign');
                        t.ajaxJson({
                            url:verify_sign_url,
                            data:{
                                "data": {'json': res.result_json, 'sign': res.result_json_signature}
                            },
                            callback:function(result, status){
                                alert(JSON.stringify(result));
                            }
                        });

                    } else {

                    }
                });
            });
        }

        wx.ready(function() {
            console.log('ready');
        });


    } catch (e) {
        alert(JSON.stringify(e));
    }

});