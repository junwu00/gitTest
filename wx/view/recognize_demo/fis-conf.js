require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'recognize_demo');
fis.match('*', {
	release:'/recognize_demo/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});