export default {
  // 部门跟树
  getDeptTop: "/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept",
  // 超过二级的部门树
  getDept: "/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept",
  // 根据部门id获取人员
  getUserByDeptId: "/wx/index.php?model=legwork&m=ajax&a=contact_list_user",
  // 分组列表:该项目不使用
  // getGroupList: "/wx/index.php?model=summary&m=contact&a=get_group_tree",
  // 根据分组id获取人员:该项目不使用
  // getUesrByGroupId: "/wx/index.php?model=summary&m=contact&a=get_user_by_group",
  // 获取初始换数据
  getInitData: "/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids",
  // 查询接口
  getSearchData: "/wx/index.php?model=legwork&m=ajax&a=contact_search_dept",
  // 最近使用数据接口
  getRecentData: "/wx/index.php?model=index&m=index&a=get_recent_user_depts",
  // 获取我的部门信息
  getMyDeptData: "/wx/index.php?model=legwork&m=ajax&a=load_leader_list"
};
