import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VConsole from 'vconsole';

// import fastclick from 'fastclick' // 消除移动端点击延迟
import gallery from 'img-vuer';
import Vant from 'vant';
import vueEsign from 'vue-esign';
import ElementUI from 'element-ui';

import common from '@/assets/js/common.js';

import url from '@/assets/js/url.js';

import 'vant/lib/index.css';
import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/less/index.less';

Vue.prototype.$common = common;
Vue.prototype.$url = url;

Vue.config.productionTip = false;

// fastclick.attach(document.body)

Vue.use(gallery, {
  swipeThreshold: 150, // default 100 ,new in 0.12.0
});
Vue.use(vueEsign);
Vue.use(Vant);
Vue.use(ElementUI);

// 移动端调试
if (location.href.includes('isLjxTest')) {
  new VConsole();
}

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});
