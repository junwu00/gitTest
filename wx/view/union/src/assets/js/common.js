import Vue from 'vue';
import axios from 'axios';
import {Toast, Dialog} from 'vant';
import WX from 'weixin-js-sdk';
import { sm2 } from 'sm-crypto'

// 处理数据加密
const base64JsonData = msg => {
  // let msg = res.data || ''
  // if (typeof msg === 'object') {
  //   msg = JSON.stringify(msg)
  // }
  const cipher = sm2.doEncrypt(msg, `04d33d64abb8aa3a2d130240afb29ace65d8cfa50cf2fc58d5e62b70dd6bd734884ba1c1bae2957fd52e3905cfa3cac161ea5aa8575a50748808b20e8f1c1a4f07`, 1);
  return cipher
}
export default class Common extends Vue {
  static wx = WX;
  static test = process.env.NODE_ENV !== 'production' ? '&test=9136342' : '';
  // [9136345,9136347,9136342,9136343,9136344,9136350]
  // static test = ''

  static isIos() {
    return (
      window.navigator.userAgent.indexOf('iPhone') !== -1 ||
      window.navigator.userAgent.indexOf('iPad') !== -1
    );
  }

  static http = ({
    url,
    method = 'post',
    data = {},
    params = {},
    otherParams = {},
    lock = true,
    error = true,
    header = {
      'content-type': 'application/x-www-form-urlencoded',
    },
  }) =>
    new Promise((resolve, reject) => {
      if (lock) {
        this.lock();
      }

      otherParams['data'] = data;

      const formData = new FormData();
      for (let key in otherParams) {
        const keyVal = otherParams[key];
        url.includes('model')
        let value = null
        if (keyVal instanceof Object && !(keyVal instanceof File)) {
          let v = JSON.stringify(keyVal)
          value = url.includes('model') && !url.includes('cmd=101') ? base64JsonData(v) : v
        } else {
          value = keyVal
        }
        formData.append(key, value);
      }
      axios({
        url: url + this.test,
        method,
        header,
        data: formData,
        params,
      })
        .then((res) => {
          this.unlock();
          if (Number(res.data.errcode) === 0) {
            resolve(res);
          } else if (Number(res.data.errcode) === 996) {
            Common.redirectFn(res.data.errmsg);
          } else {
            reject(res);
            if (error) {
              Toast.fail({
                message: res.data.errmsg,
              });
            }
          }
        })
        .catch((res) => {
          Toast.clear();
          console.log('请求失败');
          reject(res);
        });
    });

  static postFile(url, files, callback) {
    let test = this.test;
    let formData = new FormData();
    formData.append('Filedata', files);
    axios
      .post(url + test, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((response) => {
        callback(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  // tips
  // type: danger, success, warn
  // msg: string

  static tips(msg, duration) {
    Toast({
      message: msg,
      duration: duration || 1500,
    });
  }

  static error(msg, duration) {
    Toast.fail({
      message: msg,
      duration: duration || 1500,
    });
  }
  static alert(msg, title) {
    Dialog.alert({
      title: title,
      message: msg,
      confirmButtonText: '我知道了',
    });
  }
  static success(msg, callback) {
    Toast.success({
      message: msg || '操作成功',
      duration: 1000,
    });
    if (callback) {
      setTimeout(() => {
        callback();
      }, 1000);
    }
  }
  static confirm = (title, msg) =>
    Dialog.confirm({
      title: title,
      message: msg,
    });

  static lock() {
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
      duration: 0,
    });
  }
  static unlock() {
    Toast.clear();
  }
  static initJssdk() {
    Common.http({
      url: '/wx/index.php?model=union&m=ajax&a=assignJssdkSign',
      method: 'post',
      data: {
        url: encodeURIComponent(window.location.href.split('#')[0]),
      },
    }).then((res) => {
      Common.wx.config({
        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: res.data.data.app_id, // 必填，企业号的唯一标识，此处填写企业号corpid
        timestamp: res.data.data.time, // 必填，生成签名的时间戳
        nonceStr: res.data.data.nonce_str, // 必填，生成签名的随机串
        signature: res.data.data.sign, // 必填，签名，见附录1
        jsApiList: [
          'getLocation',
          'closeWindow',
          'chooseImage',
          'uploadImage',
          'scanQRCode',
          'previewImage',
          'hideOptionMenu'
        ], // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
      });
      Common.wx.ready(() => {
        Common.wx.hideOptionMenu();
      })
    });
  }
  static preview(obj) {
    if (!obj) {
      this.tips({
        type: 'danger',
        msg: '没有可以预览的对象!',
      });
      return false;
    }
    if (this.isIMG(obj.ext)) {
      this.previewImage(obj);
    } else {
      this.previewFile(obj);
    }
  }
  static isIMG(ext) {
    let arr = ['jpg', 'png', 'gif', 'jpeg'];
    if (ext && arr.indexOf(ext.toLowerCase()) > -1) {
      return true;
    }
    return false;
  }
  static previewImage(obj) {
    this.wx.ready(() => {
      console.log('ready');
      this.wx.previewImage({
        current: obj.url,
        urls: [obj.url],
      });
    });
    console.log('preview img');
  }
  static previewImages(obj) {
    this.wx.ready(() => {
      console.log('ready');
      this.wx.previewImage({
        current: obj.current,
        urls: obj.urls,
      });
    });
    console.log('preview img');
  }
  static closeWindow() {
    this.wx.ready(() => {
      this.wx.closeWindow();
    });
  }

  static getIconByExt(ext) {
    if (ext === undefined) return 'video';
    ext = ext.toLowerCase();
    if (['doc', 'docx'].indexOf(ext) > -1) {
      return 'doc';
    }
    if (['xls', 'xlsx'].indexOf(ext) > -1) {
      return 'xls';
    }
    if (['ppt', 'pptx'].indexOf(ext) > -1) {
      return 'ppt';
    }
    if (['pdf'].indexOf(ext) > -1) {
      return 'pdf';
    }
    if (['jpg', 'jpeg', 'png', 'gif'].indexOf(ext) > -1) {
      return 'img';
    }
    if (['zip', 'rar'].indexOf(ext) > -1) {
      return 'zip';
    }
    if (['mp3', 'wma', 'wav', 'amr'].indexOf(ext) > -1) {
      return 'mp3';
    }
    if (['mp4'].indexOf(ext) > -1) {
      return 'mp4';
    }
    return 'others';
  }

  static formatTime(time, fmt = 'YYYY-MM-DD HH:mm', empty = '') {
    let dateObj;
    // 如果传入的是时间对象
    if (typeof time === 'object') {
      if (notDate(time)) {
        return empty;
      } else {
        dateObj = time;
      }
    } else {
      // 当传入这个是非数字
      if (isNaN(time)) {
        if (notDate(new Date(time))) {
          return empty;
        } else {
          dateObj = new Date(time);
        }
        // 当传入的是数字
      } else if (!Number(time)) {
        return empty;
      } else {
        dateObj = new Date(Number(time) * 1000);
      }
    }
    // 格式化时间开始
    if (/(Y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (dateObj.getFullYear() + '').substr(4 - RegExp.$1.length)
      );
    }
    let o = {
      'M+': dateObj.getMonth() + 1 + '',
      'D+': dateObj.getDate() + '',
      'H+': dateObj.getHours() + '',
      'm+': dateObj.getMinutes() + '',
      's+': dateObj.getSeconds() + '',
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length === 1 ? o[k] : addZero(o[k])
        );
      }
    }
    // 添加0
    function addZero(str) {
      return ('00' + str).substr(str.length);
    }
    // 判断是否时间对象
    function notDate(obj) {
      return obj.toString() === 'Invalid Date';
    }
    return fmt;
  }

  // 当996时的重定向方法
  static redirectFn(errmsg) {
    let url = decodeURIComponent(
      location.href.replace(
        '/#/',
        '/index.html?debug=' + new Date().getTime() + '/#/'
      )
    );
    location.replace(errmsg + '&' + '&redirect=' + encodeURIComponent(url));
  }

  //判断网页终端,是企业微信还是微信,[2]移动设备 [3]电脑
  static terminal() {
    // if (process.env.NODE_ENV === 'development') {
    //   return 3
    // }
    var userAgentInfo = navigator.userAgent.toLowerCase();
    var Agents = new Array(
      'android',
      'iphone',
      'symbianOS',
      'windows phone',
      'ipad',
      'ipod'
    );
    for (var v = 0; v < Agents.length; v++) {
      if (userAgentInfo.indexOf(Agents[v]) > 0) {
        return 2;
      }
    }
    return 3;
  }
  // 去掉空格和换行
  static trim(str) {
    if (typeof str === 'string') {
      return str.replace(/ /g, '').replace(/\n/g, '');
    }
    return str;
  }

  static copyUrl(url) {
    const input = document.createElement('input');
    input.setAttribute('readonly', 'readonly');
    input.setAttribute('value', url);
    document.body.appendChild(input);
    if (navigator.userAgent.indexOf('Android') !== -1) {
      // 安卓的时候focus
      input.focus();
    }
    input.setSelectionRange(0, 9999);
    if (document.execCommand('copy')) {
      document.execCommand('copy');
      this.success('已复制到剪贴板');
    }
    document.body.removeChild(input);
  }

  /**
   * async方法异常捕获
   * @param {Function} asnycFunc 要捕获的async 方法
   */
  static async errorCaptured(asnycFunc) {
    try {
      let res = await asnycFunc();
      return [null, res];
    } catch (e) {
      return [e, null];
    }
  }

  /**
   * 将回调函数转为 promise 的辅助函数
   * @param {*} fn 要转的回调函数
   */
  static promisify(fn) {
    return function () {
      const args = Array.prototype.slice.call(arguments);
      return new Promise(function (resolve) {
        args.push(function (result) {
          resolve(result);
        });
        fn.apply(null, args);
      });
    };
  }

  static debounce = (cb, ms = 300) => {
    let timer = null;
    return function () {
      var args = Array.from(arguments);
      clearTimeout(timer);
      timer = setTimeout(() => {
        cb.apply(this, args);
      }, ms);
    };
  };
}

/**
 * 获取图片旋转90度后的base64
 * @param {object}
 * @param {string} data 图片路径
 * @param {string} direction 旋转方向
 * @returns
 */
export function getRotateImg({data, direction = 'left'}) {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.src = data;

    //如果你不加croossOrigin这个头，在canvas.toDataURL('image/jpg')时会报这个canvas被污染的错误
    //如果你这张图片被页面缓存过了，即使你加了crossOrigin的请求头也会报错
    image.setAttribute('crossOrigin', 'Anonymous');
    image.onload = () => {
      var width = image.width;
      var height = image.height;
      var img = image;

      var canvas = document.createElement('canvas');
      var degree = ((direction == 'right' ? 1 : 3) * 90 * Math.PI) / 180;
      var ctx = canvas.getContext('2d');
      canvas.width = height;
      canvas.height = width;
      ctx.rotate(degree);
      if (direction == 'right') {
        ctx.drawImage(img, 0, -height);
      } else {
        ctx.drawImage(img, -width, 0);
      }
      try {
        const dataURL = canvas.toDataURL('image/jpg');
        resolve(dataURL);
      } catch (err) {
        console.error(err);
        reject();
      }
    };
    image.onerror = function () {
      console.log('Error: image error!');
    };
  });
}

/**
 * base64转File
 *
 * @param {base64} base64
 * @param {string} filename 转换后的文件名
 * @returns {File}
 */
export const base64ToFile = (base64, filename) => {
  let arr = base64.split(',');
  let mime = arr[0].match(/:(.*?);/)[1];
  let suffix = mime.split('/')[1]; // 图片后缀
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], `${filename}.${suffix}`, {
    type: mime,
  });
};
