// jssdk相关的使用统一写在此文件，现有方法禁止修改,有bug除外，可添加其他方法，或者追加jsApiList对应的参数，该工具类暂时未兼容企业微信openData的初始化
import common from "./common.js";
import { _getTerminalMsg } from "./util.js";

const wx = window.wx;
const jsApiList = ["selectExternalContact"];
const jsApiListAg = [];
const initUrl = "/index.php?model=mass_message&m=ajax&a=getJssdk";
const initOpenDataUrl = "/index.php?model=mass_message&m=ajax&a=getAgantJssdk";

// 获取初始换参数
export const getJSSDK = async (
  url1, // 初始化jssdkurl
  list1, // 初始換的功能
  isInitAgentCfg = false, //是否注入的是企业的身份与权限
  url2, // 初始化企業微信jssdkurl
  list2 // 初始化的功能
) => {
  const urlNoHash = window.location.href.split("#")[0];
  try {
    const cfg = await common.http({
      url: url1 || initUrl,
      data: { data: JSON.stringify({ url: urlNoHash }) },
      noLoading: true
    });
    await initJSSDK(cfg.data.data, list1 || jsApiList);
    const { isWorkWX } = _getTerminalMsg();
    if (isWorkWX && isInitAgentCfg) {
      const cfgAg = await common.http({
        url: url2 || initOpenDataUrl,
        data: { data: JSON.stringify({ url: urlNoHash }) },
        noLoading: true
      });
      await initJSSDKAgentCfg(cfgAg.data.data, list2 || jsApiListAg);
    }
  } catch (e) {
    console.log("cfgerror: " + e);
  }
};

// 执行初始换（同步）
export const initJSSDK = (cfg, list) => {
  console.log(cfg, "cfg");
  return new Promise(resolve => {
    wx.config({
      beta: true, // 必须这么写，否则wx.invoke调用形式的jsapi会有问题
      debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
      appId: cfg.appid, // 必填，企业微信的corpID
      timestamp: cfg.timestamp, // 必填，生成签名的时间戳
      nonceStr: cfg.noncestr, // 必填，生成签名的随机串
      signature: cfg.signature, // 必填，签名，见 附录-JS-SDK使用权限签名算法
      jsApiList: list // 必填，需要使用的JS接口列表，凡是要调用的接口都需要传进来
    });
    wx.ready(() => {
      checkJsApi(list).then(() => {
        console.log("jssdk ok");
        resolve("jssdk ok");
      });
    });
    wx.error(res => {
      console.error("wx.error:" + JSON.stringify(res));
      alert("wx.error" + JSON.stringify(res));
      resolve("jssdk error");
    });
  });
};

// 檢查wx接口
export const checkJsApi = list => {
  return new Promise(resolve => {
    wx.checkJsApi({
      jsApiList: list,
      success: function(res) {
        console.log("checkJsApi", res);
        resolve("ok");
      }
    });
  });
};

// 企业微信openData
export const initJSSDKAgentCfg = (cfgAg, list) => {
  return new Promise(resolve => {
    console.log(
      cfgAg.corpid,
      cfgAg.agentid,
      cfgAg.timestamp,
      cfgAg.noncestr,
      cfgAg.signature
    );
    wx.agentConfig({
      corpid: cfgAg.corpid, // 必填，企业微信的corpid，必须与当前登录的企业一致
      agentid: cfgAg.agentid, // 必填，企业微信的应用id
      timestamp: cfgAg.timestamp, // 必填，生成签名的时间戳
      nonceStr: cfgAg.noncestr, // 必填，生成签名的随机串
      signature: cfgAg.signature, // 必填，签名，见附录1
      jsApiListAg: list, // 必填
      success: () => {
        console.log("agentConfig jssdk ok");
        resolve("agentConfig jssdk ok");
      },
      fail: function(res) {
        console.error("agentConfig wx.error:" + JSON.stringify(res));
        // alert("agentConfig wx.error:" + JSON.stringify(res));
        if (res.errMsg.indexOf("function not exist") > -1) {
          alert("企业微信版本过低请升级");
        }
        resolve("agentConfig jssdk error");
      }
    });
  });
};

// 打开地图
export const openLocation = param => {
  wx.openLocation({
    latitude: parseFloat(Number(param.latitude)), // 纬度，浮点数，范围为90 ~ -90
    longitude: parseFloat(Number(param.longitude)), // 经度，浮点数，范围为180 ~ -180。
    name: param.clientName, // 位置名
    address: param.clientAddress, // 地址详情说明
    scale: 16 // 地图缩放级别,整形值,范围从1~28。默认为16
  });
};

// 分享
export const share = (param, successF, cancelF) => {
  console.warn("分享配置", JSON.stringify(param));
  //获取“转发”按钮点击状态及自定义分享内容接口
  try {
    wx.onMenuShareAppMessage({
      title: param.title, // 分享标题
      desc: param.desc, // 分享描述
      link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
      imgUrl: param.imgUrl, // 分享图标
      success: function() {
        // 用户确认分享后执行的回调函数
        successF();
      },
      cancel: function() {
        // 用户取消分享后执行的回调函数
        cancelF();
      }
    });
  } catch (error) {
    console.error(error);
  }

  //获取“微信”按钮点击状态及自定义分享内容接口
  try {
    wx.onMenuShareWechat({
      title: param.title, // 分享标题
      desc: param.desc, // 分享描述
      link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
      imgUrl: param.imgUrl, // 分享图标
      success: function() {
        // 用户确认分享后执行的回调函数
        successF();
      },
      cancel: function() {
        // 用户取消分享后执行的回调函数
        cancelF();
      }
    });
  } catch (error) {
    console.error(error);
  }

  //获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
  try {
    wx.onMenuShareTimeline({
      title: param.title, // 分享标题
      desc: param.desc, // 分享描述
      link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
      imgUrl: param.imgUrl, // 分享图标
      success: function() {
        // 用户确认分享后执行的回调函数
        successF();
      },
      cancel: function() {
        // 用户取消分享后执行的回调函数
        cancelF();
      }
    });
  } catch (error) {
    console.error(error);
  }

  // 分享,自定义转发到会话
  try {
    wx.invoke(
      "shareAppMessage",
      {
        title: param.title, // 分享标题
        desc: param.desc, // 分享描述
        link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
        imgUrl: param.imgUrl // 分享图标
      },
      function(res) {
        if (res.err_msg == "shareAppMessage:ok") {
          successF();
        } else {
          cancelF;
        }
      }
    );
  } catch (error) {
    console.error(error);
  }

  // 分享,自定义转发到会话
  try {
    wx.invoke(
      "shareAppMessage",
      {
        title: param.title, // 分享标题
        desc: param.desc, // 分享描述
        link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
        imgUrl: param.imgUrl // 分享图标
      },
      function(res) {
        if (res.err_msg == "shareWechatMessage:ok") {
          successF();
        } else {
          cancelF;
        }
      }
    );
  } catch (error) {
    console.error(error);
  }
};

// 分享,自定义转发到会话
export const shareInvokeShareAppMessage = (param, successF, cancelF) => {
  wx.invoke(
    "shareAppMessage",
    {
      title: param.title, // 分享标题
      desc: param.desc, // 分享描述
      link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
      imgUrl: param.imgUrl // 分享图标
    },
    function(res) {
      if (res.err_msg == "shareAppMessage:ok") {
        successF();
      } else {
        cancelF;
      }
    }
  );
};

// 分享,自定义转发到会话
export const shareInvokeShareWechatMessage = (param, successF, cancelF) => {
  wx.invoke(
    "shareAppMessage",
    {
      title: param.title, // 分享标题
      desc: param.desc, // 分享描述
      link: param.link, // 分享链接；在微信上分享时，该链接的域名必须与企业某个应用的可信域名一致
      imgUrl: param.imgUrl // 分享图标
    },
    function(res) {
      if (res.err_msg == "shareWechatMessage:ok") {
        successF();
      } else {
        cancelF;
      }
    }
  );
};

// 隐藏的菜单项
export const hideMenuItems = menuList => {
  wx.hideMenuItems({
    menuList // 要隐藏的菜单项
  });
};

// 显示菜单项
export const showMenuItems = menuList => {
  wx.showMenuItems({
    menuList // 要隐藏的菜单项
  });
};

// 增加其他方法
// 拍照或从手机相册中选图接口
export const chooseImage = (sourceType, successF) => {
  wx.chooseImage({
    count: 9, // 默认9
    sizeType: ["original", "compressed"], // 可以指定是原图还是压缩图，默认二者都有
    sourceType: sourceType, //["album", "camera"] 可以指定来源是相册还是相机，默认二者都有
    defaultCameraMode: "normal", //表示进入拍照界面的默认模式，目前有normal与batch两种选择，normal表示普通单拍模式，batch表示连拍模式，不传该参数则为normal模式。从3.0.26版本开始支持front和batch_front两种值，其中front表示默认为前置摄像头单拍模式，batch_front表示默认为前置摄像头连拍模式。（注：用户进入拍照界面仍然可自由切换两种模式）
    isSaveToAlbum: 1, //整型值，0表示拍照时不保存到系统相册，1表示自动保存，默认值是1
    success: function(res) {
      console.log(res, "res1");
      successF(res);
      // var localIds = res.localIds; // 返回选定照片的本地ID列表，
      // andriod中localId可以作为img标签的src属性显示图片；
      // iOS应当使用 getLocalImgData 获取图片base64数据，从而用于img标签的显示（在img标签内使用 wx.chooseImage 的 localid 显示可能会不成功）
    }
  });
};

// 上传图片接口
export const uploadImage = localId => {
  // console.warn(localId, "upload");
  return new Promise((resolve, reject) => {
    wx.uploadImage({
      localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
      isShowProgressTips: 1, // 默认为1，显示进度提示
      success: function(res) {
        // var serverId = res.serverId; // 返回图片的服务器端ID
        console.warn(res, "upload");
        // successF(res);
        resolve(res);
      },
      fail: function(res) {
        console.warn(res, "reject");
        reject(res);
      }
    });
  });
};

// 预览图片接口
export const previewImage = (current, urls) => {
  wx.previewImage({
    current: current, // 当前显示图片的http链接
    urls: urls // 需要预览的图片http链接列表
  });
};
