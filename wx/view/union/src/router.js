import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/apply',
    },
    {
      path:'/couponChoose',
      name:'couponChoose',
      component: () => import('./views/couponChoose.vue'),
      meta: {
        title: '领券申请',
        keepAlive: true,
      }
    },
    {
      path: '/apply',
      name: 'apply',
      component: () => import('./views/apply.vue'),
      meta: {
        title: '入会申请单',
        keepAlive: true,
      },
    },
    {
      path: '/subform',
      name: 'subform',
      component: () => import('./views/subform.vue'),
      meta: {
        title: '添加主要成员',
        keepAlive: false,
      },
    },
    {
      path: '/esign',
      name: 'esign',
      component: () => import('./views/esign.vue'),
      meta: {
        title: '签名',
        keepAlive: false,
      },
    },
    {
      path: '/dept/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?',
      name: 'dept',
      component: () => import('@/components/dept/index.vue'),
      props: {
        visible: true,
        type: 2,
        max: 1,
        elkey: 'uniqueK',
      },
      meta: {
        title: '部门选择',
        keepAlive: false,
      },
    },
    {
      path: '/success',
      name: '/success',
      component: () => import('./views/success.vue'),
      meta: {
        title: '提交成功',
        keepAlive: false
      }
    },
    {
      path: '/debug/:id',
      name: '/debug',
      component: () => import('./views/debug.vue'),
      meta: {
        title: '调试',
        keepAlive: false,
      },
      props: (route) => {
        console.log(route);
        return {
          id: route.query.id,
          test: route,
        };
      },
    },
    {
      path: '*',
      name: '404',
      component: () => import('./views/404.vue'),
      meta: {
        title: '404',
        keepAlive: false,
      },
    },
    {
      path: '/coupon',
      name: '/coupon',
      component: () => import('./views/coupon.vue'),
      meta: {
        title: '我的卡券',
        keepAlive: false,
      },
    },
    {
      path: '/couponApply',
      name: '/couponApply',
      component: () => import('./views/couponApply.vue'),
      meta: {
        title: '领券申请',
        keepAlive: false,
      },
    },
    {
      path: '/couponDetail',
      name: '/couponDetail',
      component: () => import('./views/couponDetail.vue'),
      meta: {
        title: '卡券详情',
        keepAlive: false,
      },
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0,
      };
    }
  },
});
