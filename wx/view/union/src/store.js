import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userList: [],
    esign: null,
    departmentList: [],
    windowInfo: {
      width: 0,
      height: 0,
    },
    shopConfig: {
      1: {
        id: 1,
        logo: require('@/assets/img/coupon/logo2.png'),
        bg: require('@/assets/img/coupon/bg2.png'),
        name: '太阳之手',
        bottom: '0px',
      },
      2: {
        id: 2,
        logo: require('@/assets/img/coupon/logo1.png'),
        bg: require('@/assets/img/coupon/bg1.png'),
        name: '向阳坊',
        bottom: '0px',
      },
      3: {
        id: 3,
        logo: require('@/assets/img/coupon/logo3.png'),
        bg: require('@/assets/img/coupon/bg3.png'),
        name: '华丰贺氏',
        bottom: '0px',
      },
      4: {
        id: 3,
        logo: require('@/assets/img/coupon/logo3.png'),
        bg: require('@/assets/img/coupon/bg4.png'),
        name: '电子卡券',
        bottom: '0px',
      },
    },
  },
  getters: {
    getUserList: (state) => state.userList,
    getEsign: (state) => state.esign,
    getShopConfig: (state) => state.shopConfig,
  },
  mutations: {
    setUserList(state, data) {
      state.userList = data;
    },
    setEsign(state, data) {
      state.esign = data;
    },
    setDepartmentList(state, data) {
      state.departmentList = data;
    },
    setWindowInfo(state, data) {
      state.windowInfo = data;
    },
  },
  actions: {
    setUserList({commit}, data) {
      return new Promise((resolve) => {
        commit('setUserList', data);
        resolve();
      });
    },
  },
});
