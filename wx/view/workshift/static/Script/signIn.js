require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/text!'+buildView+'/workshift/Templates/signInTpl.html',
    'common:widget/lib/plugins/gdmap/lbs.amap',
	'common:widget/lib/GenericFramework',
	"workshift:static/Script/commonData",
	"workshift:static/Script/commonFuc"
], function(t,UiFramework,tpl,signInTpl,gdmap,GenericFramework,commonData,commonFuc){
	var layerPackage = UiFramework.layerPackage();
	layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;

	var target = $("#main-container");
	var type = t.getHrefParamVal("type") || 1;

	var signIn = {
		init:function(target,type){
			var addTime = 0;
			this.getSignInInfo(type,function(result){
				this.initMap(target,result.addr_info,type);

				//打卡
				if(type == 1){
					tpl.register('getColorNumber', commonFuc.getColorNumber);
					var dom = tpl(signInTpl,{
						getRota:1,
						data:result
					});
					tpl.unregister('getColorNumber');
					
					target.html($($.trim(dom)));

					if(result.clock_record.length > 0){
						this.getRecordList(target,result.clock_record);	
					}

					this.bindCommonDom(target);
					this.bindRotaDom(target);
				}else if(type == 2){
					tpl.register('getRotaTime', commonFuc.getRotaTime);
					tpl.register('getDiffTime', commonFuc.getDiffTime);
					tpl.register('getShortTime', t.getShortTime);
					tpl.register('getLocationTime', commonFuc.getLocationTime);
					var dom = tpl(signInTpl,{
						getLocation:1,
						data:result
					});
					tpl.unregister('getRotaTime');
					tpl.unregister('getDiffTime');
					tpl.unregister('getShortTime');
					tpl.unregister('getLocationTime');
					
					target.html($($.trim(dom)));

					GenericFramework.init(wxJsdkConfig);

					if(result.clock_list.length > 0){
						this.getClockList(target,result.clock_list);	
					}

					this.bindCommonDom(target);
					this.bindLocationDom(target);
				}

				result.time = commonFuc.checkTime(result.time || new Date().getTime());
				var i = 0;
				setInterval(function(){
					i++;
					target.find(".clickTime").text(commonFuc.getRotaTime((result.time + i*1000),1));
				},1000);
			});
		},
		getSignInInfo:function(type,callback){
			var pThis = this;
			var url = type == 1 ? ajaxUrl.getRotaInfoUrl : ajaxUrl.getLocationInfoUrl;

			t.ajaxJson({
				url:url,
				data:{
					data:[]
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);

						setTimeout(function(){
							window.history.go(-1);
						},2000);

						return;
					}

					pThis.type = type;
					
					pThis.data = result;

					callback.apply(pThis,[result]);

					layerPackage.unlock_screen();
				}
			});
		},
		getRecordList:function(target,data){
			tpl.register('getRotaTime', commonFuc.getRotaTime);
			tpl.register('getDiffTime', commonFuc.getDiffTime);
			target.find(".clockRecord").html("").append($($.trim(tpl(signInTpl,{
				getClockRecord:1,
				data:data
			}))));
			tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');
		},
		restTimeList:function(target,data){
			target.find('.attendanceList p .time').html(data.curr_shift.time);
			var htmlArray = [];
			for(var i in data.clock_time) {
				var statCalss = '';
				var typeDesc = '';
				if(data.clock_time[i].state == 1){
					statCalss = 's1';
				}else if(data.clock_time[i].state == 0){
					statCalss = 's2';
				}else if(data.clock_time[i].state == 2){
					statCalss = 's3';
				}
				
				if(data.clock_time[i].type == 1){
					typeDesc = '上';
				}else if(data.clock_time[i].type == 2){
					typeDesc = '下';
				}else if(data.clock_time[i].type == 3){
					typeDesc = '请';
				}else if(data.clock_time[i].type == 4){
					typeDesc = '外';
				}
				if(i == 0){
					htmlArray.push('<span class="'+statCalss+'">');
					htmlArray.push('<span class="desc fs12">'+typeDesc+'</span>');
					htmlArray.push('<span class="time fs12">'+ data.clock_time[i].time+'</span>');
					htmlArray.push('</span>');
				}else{
					htmlArray.push('<span class="'+statCalss+'">');
					htmlArray.push('<span class="timel fs12">……</span>');
					htmlArray.push('<span class="desc fs12">'+typeDesc+'</span>');
					htmlArray.push('<span class="time fs12">'+ data.clock_time[i].time+'</span>');
					htmlArray.push('</span>');
				}
			}
			var html = htmlArray.join('');
			target.find('.attendanceList div').html(html);
		},
		setRecordApi:function(callback,cover){
			var pThis = this;
			var position = this.position;

			if(!position){
				layerPackage.unlock_screen();
				layerPackage.fail_screen("请等地图加载完整!");
				return;
			}

			var data = this.data.curr_shift;
			var params = {
				url:ajaxUrl.setRecordUrl,
				data:{
					data:{
						"id":data.id,
						"time":data.time,
						"lng":position.lng, 
						"lat":position.lat
					}
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						//layerPackage.fail_screen(result.errmsg);

						//layerPackage.success_screen(result.errmsg);
						layerPackage.ew_alert({
							title:pThis.getErrorHtml(result.errmsg),
							action_desc:"我知道了"
						});
						return;
					}

					if(result.type == 1 || result.type == 3){
						layerPackage.ew_confirm({
							title:result.title,
							desc:result.desc,
							ok_callback:function(){
								pThis.setRecordApi(callback,1);
							}
						});
						return;
					}else if(result.type == 2){
						layerPackage.ew_alert({
							title:"温馨提示",
							desc:result.title,
							action_desc:"我知道了"
						});
						return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen("打卡成功");

					if(callback && t.isFunction(callback)){
						//result.time = 1481195532763;
						callback(result);
					}
				}
			};

			if(cover){
				params.data.data.cover = cover;
			}

			t.ajaxJson(params);
		},
		initMap:function(target,rotadData,type){
			var point = null;
			var pThis = this;
			require(['amap'], function() {
				window.init = function() {
					gdmap.init(target.find("section.fullPic#gdmap"), {
						zoom:16, //PC [3-18] 移动 [3-19]
						//center: [113.320703, 23.162334], //测试用  113.320703, 23.165386

						autoMarkCenter: 0, //默认定位中心点
						autoFixedCenter: 0, //默认缩放地图时保持中心点不变

						searchEnable:0,

						callback: {
							onAfterSetCenter:function(position,map){
								console.log(position);
								point = position;
								pThis.position = position;
								var marker = null;
								var addr = "&nbsp;";
								
								if(rotadData && rotadData.length > 0){
									var g = map.setting.amap;
									for (var i = 0; i < rotadData.length; i++) {
										var r = rotadData[i];
										var cirle = new AMap.Circle({
											map:g,
											zIndex:16,
											center:[r.lng,r.lat],
											radius:r.scope,
											strokeColor:"#DDF1D7",
											fillColor:"#DDF1D7",
											strokeOpacity:0.2
										});

										var addr_name = r.addr_name;
										var x_l = 106 - (addr_name.length * 12 + 28 )/2 + 11;

										map.addMarker({
											position:[r.lng,r.lat],
											icon:gdmapMarkIcon3,
											iconSize:[24,33]
										},addr_name,"<div class='placeName hideTxt'>"+addr_name+"</div>",{x:x_l,y:44});
									}
								}

								map.getInfoByPosition(position,function(result){
									var address =  result.regeocode.formattedAddress;

									var time = t.getNowDate();

									marker = this.addMarker({
										position:[position.lng, position.lat],
										icon:gdmapMarkIcon2,
										iconSize:[24,33]
									},address,map.getMarkerLabel(userInfo.pic,address,time),{x:18,y:19});

									//g.setFitView();
								});

								if(type == 1){
									var testObj = null;

									try{
										function test(){
											return setTimeout(function(){
												map.getLocal(function(lng,lat){
													pThis.position.lng = lng;
													pThis.position.lat = lat;

													var newPosition = [lng,lat];

													if(marker){
														map.getInfoByPosition(newPosition,function(res){
															try{
																addr = res.regeocode.formattedAddress;
															}catch(e){

															}

															marker.setPosition(newPosition);
															map.panTo(newPosition);

															target.find(".amap-marker-label .mTitle").text(addr);

															//target.find(".bottom-link").append("<span class='block'>lng:"+lng+"---lat:"+lat+"----"+addr+"</span>");

															clearTimeout(testObj);

															testObj = test();
														});
													}
												});
											},10000);
										}

										testObj = test();
									}catch(e){
										alert(e);
									}
								}
							},
							onAfterMapLoaded: function(map) {
								console.log("地图加载完成");
							}
						}
					});
				}
			});
		},
		bindCommonDom:function(target){
			//我的考勤记录
			target.find(".bottom-link span").on("click",function(){
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=timeTag";
			});
		},
		bindRotaDom:function(target){
			var pThis = this;

			//查看排班表
			target.find(".linkToRota").on("click",function(){
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=index";
			});

			//重新打卡
			target.on("click",".restartRecord",function(){
				var o = $(this),
					p_o = o.parents("section"),
					id = p_o.attr("data-id");

				pThis.setRecordApi(function(result){
					result.time = commonFuc.checkTime(result.time || new Date().getTime());
					p_o.find("time").text(commonFuc.getRotaTime(result.time,1));
				},1);
			});

			commonFuc.bindCommonRoatDom(target);

			/*
			//查看补录
			target.on("click",".checkRecord",function(){
				var o = $(this),
					p_o = o.parents("section"),
					id = p_o.attr("data-id");

				alert("查看补录");
			});

			//申请补录
			target.on("click",".setRecord",function(){
				var o = $(this),
					p_o = o.parents("section"),
					id = p_o.attr("data-id");

				alert("申请补录");
			});
			*/

			//打卡按钮
			target.find(".attendanceBtn").on("click",function(){
				pThis.setRecordApi(function(result){		
					if(result.clock_record.length > 0){
						pThis.getRecordList(target,result.clock_record);
						pThis.restTimeList(target,result);
					}
				});
			});
		},
		bindLocationDom:function(target){
			var pThis = this;
			//定位按钮
			target.find(".locationBtn").on("click",function(){
				pThis.locationContent();
			});

			target.on("click",".locationContent",function(){
				$(this).remove();
			});

			target.on("click",".locationPart .oneLocation > section",function(){
				var id = $(this).attr("data-id");

				window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=2&id="+id+"&activekey=0&isIndex=1&menuType=2#pMineList";
			});

			GenericFramework.previewImage({
				target:target.find(".img_div")
			});
		},
		getErrorHtml:function(errmsg){
			var html = new Array();

			html.push('<i class="errorIcon"></i>');
			html.push('<b class="fs20 lhn fsn block">打卡失败喔</b>');
			html.push('<span class="c-red fs12 block mt5">'+errmsg+'</span>');
			html.push('<span class="c-gray fs12 block" style="margin-top:21px;">位置不精准？可以截图发给管理员进行补录</span>');

			return html.join('');
		},
		locationContent:function(){
			var dom = tpl(signInTpl,{
				locationContent:1
			});

			var o = target.find(".locationContent");

			if(o.length > 0){
				o.remove();
			}

			target.append($($.trim(dom)));

			this.locationContentEvent(target);

			setTimeout(function(){
				target.find(".locationContent .content .address").click();
			},0);
		},
		localInfo:{
			address:null,
			longt:null,
			lat:null,
			image_list:[]
		},
		locationContentEvent:function(target){
			var pThis = this;
			target.find(".locationContent .content").on("click",function(e){
				e.preventDefault();
				e.stopPropagation();
			});

			target.find(".locationContent .content .addComment").on("click",function(e){
				$(this).addClass('hide');
				$(this).next().removeClass('hide');
			});

			//刷新获取地理位置
			var freshBtn = target.find(".locationContent .content .address");
			GenericFramework.location({
				target:freshBtn,
				callback:{
					beforeGetLocation:function(){
						freshBtn.find(".add").addClass('hide');
						freshBtn.find(".search-address").removeClass('hide');
					},
					afterGetLocation:function(res,lng,lat){
						var address = res.address;
						pThis.localInfo.address = address;
						pThis.localInfo.longt = lng;
						pThis.localInfo.lat = lat;

						freshBtn.find(".add").removeClass('hide');
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span.add").text(address);
					},
					error:function(msg){
						alert(msg);
						freshBtn.find(".search-address").addClass('hide');
						freshBtn.find("span").text("请重新点击获取");
					}
				}
			});

			var cameraBtn = target.find(".locationContent .content .cameraBtn");
			var picObj = target.find(".camera-pic");
			GenericFramework.uploadFile({
				target:cameraBtn,
				type:"wx",
				sourceType:2, //拍照
				callback:{
					afterUpload:function(imgInfo){
						//console.log(imgInfo);
						picObj.removeClass('hide');
						picObj.find("img").attr("src",imgInfo.image_path);

						//alert(JSON.stringify(imgInfo));

						pThis.localInfo.image_list.push({
							hash:imgInfo.hash,
							size:imgInfo.filesize,
							url:imgInfo.path,
							name:imgInfo.name,
							ext:imgInfo.ext,
							image_path:imgInfo.image_path
						});

						cameraBtn.addClass('hide');
					}
				},
				wx:{
					uploadUrl:ajaxUrl.newUrlForPic,
					uploadData:{
						data:{
							addr:pThis.localInfo.address
						}
					},
					beforeStartCallback:function(_setting){
						if(!pThis.localInfo.address){
							layerPackage.fail_screen("请获取地址后再拍照!");
							return false;
						}

						_setting.wx.uploadData = {
							data:{
								addr:pThis.localInfo.address
							}
						}

						return true;
					}
				}
			});
	
			GenericFramework.previewImage({
				target:picObj
			});
			
			picObj.on('click','i',function(event){
                event.stopPropagation();
                
                pThis.localInfo.image_list.pop();

                picObj.find("img").attr("src","");

                cameraBtn.removeClass('hide');
                picObj.addClass('hide');
            });

            target.find(".locationContent .content .submitBtn button").on("click",function(e){
            	var data = pThis.data;
            	var localInfo = pThis.localInfo;

            	if(!localInfo.address){
            		layerPackage.fail_screen("请重新获取地址!");
            		return;
            	}

            	if(localInfo.image_list.length == 0 && data.is_photo == 1){
            		layerPackage.fail_screen("请拍照!");
            		return;
            	}

            	var textarea = target.find(".locationContent .content textarea");

            	pThis.setLocationApi({
            		"id":data.legwork_record.record_id,
            		"addr":localInfo.address,
            		"lng":localInfo.longt,
            		"lat":localInfo.lat,
					"file_list":localInfo.image_list,
					"mark":textarea.val()
            	},function(result){
					pThis.getClockList(target, [{
						"addr_name": localInfo.address || "",
						"clock_time": commonFuc.checkTime(result.time || new Date().getTime()),
						"mark": textarea.val() || "",
						"file_path": localInfo.image_list.length == 0 ? [] : [localInfo.image_list[0].image_path]
					}],1);

            		pThis.localInfo.address =null;
					pThis.localInfo.longt =null;
					pThis.localInfo.lat =null;
					pThis.localInfo.image_list = [];
					textarea.val("");

					target.find(".locationContent").click();
            	});
			});
		},
		setLocationApi:function(data,callback){
			var pThis = this;

			t.ajaxJson({
				url:ajaxUrl.setLocationUrl,
				data:{
					data:data
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					if(callback && t.isFunction(callback)){
						callback(result);
					}
				}
			});
		},
		getClockList:function(target,data,append){
			tpl.register('getRotaTime', commonFuc.getRotaTime);
			tpl.register('getDiffTime', commonFuc.getDiffTime);
			tpl.register('getShortTime', t.getShortTime);
			var dom = $($.trim(tpl(signInTpl,{
				getClockList:1,
				data:data,
				append:append
			})));

			GenericFramework.previewImage({
				target:dom.find(".img_div")
			});
			
			tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');
			tpl.unregister('getShortTime');

			if(!append){
				target.find(".clockList").append(dom);
			}else{
				target.find(".clockList").prepend(dom);
			}
			
		}
	};


	signIn.init(target,type);

	layerPackage.unlock_screen();
},function(e){
	requireErrBack(e);
});