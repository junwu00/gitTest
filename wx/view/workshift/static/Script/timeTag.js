require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
     'common:widget/lib/text!'+buildView+'/workshift/Templates/signInTpl.html',
    'common:widget/lib/plugins/juicer',
    "workshift:static/Script/commonData",
    "workshift:static/Script/commonFuc",
    "workshift:static/Script/datetimepicker/bootstrap.min",
    "workshift:static/Script/datetimepicker/bootstrap-datetimepicker.min",
    "workshift:static/Script/datetimepicker/bootstrap-datetimepicker.zh-CN",
], function($,t,UiFramework,signInTpl,tpl,commonData,commonFuc){
	var ajaxUrl = commonData.ajaxUrl;
	//getAttendanceMonthUrl:"",
	//getAttendanceDayUrl:"",

	var layerPackage = UiFramework.layerPackage();
	var target = $("#main-container");

	var timeTag = {
		getAttendanceMonth:function(year,month,callback,notShow){
			var pThis = this;
			layerPackage.lock_screen();//遮罩start
			t.ajaxJson({
				url:ajaxUrl.getAttendanceMonthUrl,
				data:{
					"data":{
						"year":year,
						"month":month
					}
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);

						return;
					}

					if(!year && !month){
						tpl.register('getLocationTime', commonFuc.getLocationTime);
					 	var dom = tpl(signInTpl,{
					 		attendance:1,
					 		data:result,
					 		userInfo:userInfo,
					 		defaultFace:defaultFace
					 	});
					 	tpl.unregister('getLocationTime');

		 				target.html($($.trim(dom)));
					}

					if(!notShow){
						pThis.getMyAttendance(target,result);
					}

	 				if(callback && t.isFunction(callback)){
	 					callback.apply(pThis,[result]);
	 				}

					layerPackage.unlock_screen();
				}
			});
		},
		getMyAttendance:function(target,data,timeTag){
			tpl.register('getLocationTime', commonFuc.getLocationTime);
		 	var dom = tpl(signInTpl,{
		 		myAttendance:1,
		 		data:data,
		 		timeTag:timeTag,
		 		userInfo:userInfo,
		 		defaultFace:defaultFace
		 	});
		 	tpl.unregister('getLocationTime');

		 	if(timeTag){
		 		return dom;
		 	}

		 	target.find(".dayDetail").addClass('hide').html("");
		 	target.find(".myAttendance").removeClass('hide').html($($.trim(dom)));
		},
        init:function(params){
        	var pThis = this;
            var nowDate = t.getNowDate("",1);
            this.target = params.target;

        	this.getAttendanceMonth(null,null,function(result){
        		var timestamp = result.date_info;
	            pThis.nowDate = nowDate;

	            var obj = params.target.find("#timeTag");

	            obj.datetimepicker({
	                format: "yyyymmdd",
	                language:  'zh-CN',
	                maxView: 2,
	                minView: 2,
	                linkField:"time_val",
	                forceParse:false,
	                todayHighlight:false,
	                /*weekStart: 1,
	                autoclose: 1,
	                todayHighlight: 1,
	                startView: 1,
	                minView: 0,
	                maxView: 1,
	                forceParse: 0*/
	            })
	            .datetimepicker().on('changeDate', function(ev){
	                //console.log(ev.date.valueOf());
	                //console.log($("#time_val").val());

	                var clickDate = t.getNowDate(ev.date.valueOf(),1);
	                var year = clickDate.year;
	                var month = clickDate.month;
	                var day = clickDate.days;
	                var week = t.getWeek(clickDate.week);

	                titleObj.attr("month",month);
	                titleObj.attr("year",year);

	                //pThis.nowDate.year = year;
                	//pThis.nowDate.month = month;

	                setTimeout(function(){
	                    obj.find("td").removeClass('c-white bt');
	                    obj.find("i.state").remove();

	                    var r = pThis.getCacheTimestamp(year,month);

	                    if(r){
							//pThis.getMyAttendance(pThis.target,r);
                    		pThis.getDaysInMonth(year,month,r.date_info);
                    		obj.find("td.active").addClass('bt');
                    		pThis.getDayDetail(year+"-"+month+"-"+day,week);
	                    }else{
	                    	pThis.getAttendanceMonth(year,month,function(result){
	                    		pThis.saveTimestamp(year,month,result);
	                    		pThis.getDaysInMonth(year,month,result.date_info);
	                    		obj.find("td.active").addClass('bt');
	                    		pThis.getDayDetail(year+"-"+month+"-"+day,week);
	                    	},1);
	                    }

	                    /*if(year == nowDate.year && month == nowDate.month){
	                    	pThis.getDaysInMonth(year,month,timestamp);
	                    	obj.find("td.active").addClass('bt');
	                    }else{
	                    	pThis.getAttendanceMonth(year,month,function(result){
	                    		pThis.getDaysInMonth(year,month,result.date_info);
	                    		obj.find("td.active").addClass('bt');
	                    	});
	                    }*/
	                    ////pThis.titleFormatter(params.formatter,nowDate,titleObj);
	                },0);
	            });

	            var titleObj = obj.find(".datetimepicker-days table .switch");
	            this.titleObj = titleObj;

	            obj.find("td.active").removeClass('active');

	            //this.titleFormatter(params.formatter,nowDate,titleObj);

	            titleObj.attr("month",nowDate.month);
	            titleObj.attr("year",nowDate.year);

				obj.find(".datetimepicker-days table .prev").find("span")
				.removeClass('glyphicon-arrow-left')
				.addClass('rulerIcon rulericon-angle_left');

				obj.find(".datetimepicker-days table .next").find("span")
				.removeClass('glyphicon-arrow-right')
				.addClass('rulerIcon rulericon-angle_right');

	            setTimeout(function(){
	                //if(timestamp.length > 0){
	                	pThis.saveTimestamp(nowDate.year,nowDate.month,result);
	                    pThis.getDaysInMonth(nowDate.year,nowDate.month,timestamp);
	                    pThis.bindDom(obj,params);
	                    pThis.bindCommonDom(params.target);
	                //}
	            },0);
        	});
        },
        titleFormatter:function(f,nowDate,titleObj){
            if(!f){return;}

            titleObj.attr("month",nowDate.month);
            titleObj.attr("year",nowDate.year);

            f(titleObj,nowDate);
        },
        bindCommonDom:function(target){
        	var pThis = this;
        	target.on("click",".userPic",function(){
        		var o = target.find(".datetimepicker-days .switch");

        		var r = pThis.getCacheTimestamp(o.attr("year"),o.attr("month"));
        		pThis.getMyAttendance(target,r);

        		target.find("#timeTag td.active").removeClass('active');
        	});

        	target.on("click",".detailDom section",function(){
        		var id = $(this).attr("data-id");

				if(!id || $(this).hasClass('record')){return;}

				window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=2&id="+id+"&activekey=0&isIndex=1&menuType=2#pMineList";
        	});
        },
        bindDom:function(obj,params){
            var pThis = this;
            var table = obj.find(".datetimepicker-days table");

            obj.find(".prev,.next").on("click",function(e){
                var className = e.currentTarget.className;
                var titleObj = pThis.titleObj;

                var nowDate = pThis.nowDate;
                var month = parseInt(titleObj.attr("month"));
                var year = parseInt(titleObj.attr("year"));

                if(className == "prev" || className == "day old"){
                    month = month == 1 ? 12 : month-1;

                    if(month == 12){
                        year--;
                    }
                }else if(className == "next" || className == "day new"){
                    month = month == 12 ? 1 : month+1;

                    if(month == 1){
                        year++;
                    }
                }

                titleObj.attr("year",year);
                titleObj.attr("month",month);

                nowDate.year = year;
                nowDate.month = month;

                setTimeout(function(){
                    obj.find("td.active").removeClass('active');
                    //pThis.titleFormatter(params.formatter,nowDate,titleObj);

                    var r = pThis.getCacheTimestamp(year,month);

                    if(r){
                    	pThis.getMyAttendance(pThis.target,r);
                    	pThis.getDaysInMonth(year,month,r.date_info);
                    }else{
                    	pThis.getAttendanceMonth(year,month,function(result){
                    		pThis.saveTimestamp(year,month,result);
	                    	pThis.getDaysInMonth(year,month,result.date_info);
	                    });
                    }
                    //pThis.bindDom(obj,params);
                },0);
            });
        },
        getNewTimestamp:function(timestamp){
            for(var i = 0;i < timestamp.length;i++){
            	timestamp[i].date = commonFuc.checkTime(timestamp[i].date);
                var nowDate = t.getNowDate(timestamp[i].date,1);

                timestamp[i].nowDate = nowDate;
            }

            return timestamp;
        },
        getDaysInMonth:function(year,month,timestamp){
            /*
            no_clock //未打卡
            late_early //早退/迟到
            rest //请假
            legwork //外出
            clock //正常打卡
            */
            var pThis = this;
            timestamp = this.getNewTimestamp(timestamp); 

            console.log(timestamp);

            this.target.find("td.day:not(.old):not(.new)").each(function(){
                var o = $(this);
                for (var i = 0; i < timestamp.length; i++) {
                    var ts = timestamp[i];
                    if(ts.nowDate && year == ts.nowDate.year && parseInt(month) == parseInt(ts.nowDate.month) &&  parseInt(ts.nowDate.days) == o.text()){
                        var s1 = ts.no_clock,
                            s2 = ts.late_early,
                            s3 = ts.rest,
                            s4 = ts.legwork,
                            s5 = ts.clock;

                        var count = 0;

                        //未打卡
                        if(s1){
                            count++;
                            o.append('<i class="spSpan state1" date-val="'+s1+'"></i>');
                        }
                        //早退/迟到
                        if(s2){
                            count++;
                            o.append('<i class="spSpan state2" date-val="'+s2+'"></i>');
                        }
                        //请假
                        if(s3){
                            count++;
                            o.append('<i class="spSpan state3" date-val="'+s3+'"></i>');
                        }
                        //外出
                        if(s4){
                            count++;
                            o.append('<i class="spSpan state4" date-val="'+s4+'"></i>');
                        }
                        //正常打卡
                        if(s5){
                            count++;
                            o.append('<i class="spSpan state5" date-val="'+s5+'"></i>');
                        }

                        o.find("i").addClass('s-'+(count > 0 ? count : ""));

                        if(count > 0){
                             o.addClass('c-white');
                        }
                    }
                };
            });
        },
        cacheTimestamp:[],
        saveTimestamp:function(year,month,result){
        	var flag = false;
        	for (var i = 0; i < this.cacheTimestamp.length; i++) {
        		if(this.cacheTimestamp[i].year == year && this.cacheTimestamp[i].year == month){
        			flag = true;
        			break;
        		}
        	};

        	if(!flag){
        		this.cacheTimestamp.push({
        			year:year,
        			month:month,
        			result:result
        		});
        	}
        },
        getCacheTimestamp:function(year,month){
        	for (var i = 0; i < this.cacheTimestamp.length; i++) {
        		if(this.cacheTimestamp[i].year == year && this.cacheTimestamp[i].month == month){
        			return this.cacheTimestamp[i].result;
        		}
        	};

        	return null;
        },
        getDayDetail:function(date,week){
        	var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getAttendanceDayUrl,
				data:{
					"data":{
						"date":date //2016-12-06
					}
				},
				callback:function(result,status){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);

						return;
					}

					pThis.getDayDetailHtml(pThis.target,result,date,week);
				}
			});
        },
        getDayDetailHtml:function(target,data,date,week){
		 	target.find(".myAttendance").addClass('hide');
		 	var o = target.find(".dayDetail");
		 	
		 	o.removeClass('hide')
		 	.html('<section class="autoH clickDate pb0"><b class="fs14 lhn fsn">'+date+'（周'+week+'）</b></section>');
		 	
		 	//打卡记录
		 	tpl.register('getRotaTime', commonFuc.getRotaTime);
			tpl.register('getDiffTime', commonFuc.getDiffTime);
		 	var dom = tpl(signInTpl,{
		 		getClockRecord:1,
		 		data:data.clock_info,
		 		timeTag:1
		 	});
		 	tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');

			o.append($($.trim(dom)));
			commonFuc.bindCommonRoatDom(o);

			if(data.clock_info.length > 0){
				o.append('<div class="wxLine rightLine bg-white"></div>');
			}
			
			//请假外出记录
			var dom2 = this.getMyAttendance(target,data,1);
	 		o.append($($.trim(dom2)));

	 		if(data.clock_info == 0 && data.rest_record == 0 && data.legwork_record == 0){
	 			o.append('<section class="autoH pb0"><b class="fs14 lhn fsn c-9b">无考勤记录</b></section>');
	 		}
		}
    };

    timeTag.init({
        target:target,
        //timestamp:timestamp,
        /*formatter:function(titleObj,nowDate){
            titleObj.text("乌拉拉的"+nowDate.month+"月考勤");
        },*/
        prevNextEvent:function(){

        }
    });

},function(e){
	requireErrBack(e);
});