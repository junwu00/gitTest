define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
],function($,t,UiFramework) {
	var layerPackage = UiFramework.layerPackage();
	var commonFuc = {
		//处理数据返回信息
		error:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.fail_screen(result.errmsg);
		},
		success:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.success_screen(result.errmsg);
		},
		checkTime:function(time){
			if(time.toString().length == 10){
				time = parseInt(time) * 1000;
			}

			return time;
		},
		getRotaTime:function(time,showSecond){
			time = commonFuc.checkTime(time);

			var nowTime = t.getNowDate(time,1);

			return nowTime.hours + ":" + nowTime.minutes + (showSecond ? ":" + nowTime.seconds : "");
		},
		getDiffTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;
			var json = t.getDiffTime(firstTime,secondTime,1);

			return (json.days ? json.days + "天" : "") + (json.hours ? json.hours + "小时" : "") 
			+(json.minutes ? json.minutes + "分钟" : "")+(json.seconds ? json.seconds + "秒" : "");
		},
		getColorNumber:function(num){
			//从0开始算
			return num > 10 ? num%11 : num;
		},
		getLocationTime:function(firstTime,secondTime){
			firstTime = firstTime ? parseInt(firstTime) : firstTime;
			secondTime = secondTime ? parseInt(secondTime) : secondTime;

			var str = "";

			var flag = false;
			var m = commonFuc.checkTime(firstTime || secondTime);
			var time = t.getNowDate(m,1);

			if(parseInt(time.month) != parseInt(t.getNowDate(null,1).month)){
				str += parseInt(time.month) + "月" + (time.days ?  time.days + "日" : "");
				flag = true;
			}

			if(!flag){
				str += time.days+"日";
			}

			return str + " " + time.hours + ":" + time.minutes;
		},
		bindCommonRoatDom:function(target){
			var pThis = this;
			//查看补录
			target.on("click",".checkRecord",function(e){
				e.stopPropagation();
				e.preventDefault();
				var id = $(this).attr("data-rid");

				window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=2&id="+id+"&activekey=0&isIndex=1&menuType=2#pMineList";
			});

			//申请补录
			target.on("click",".setRecord",function(e){
				e.stopPropagation();
				e.preventDefault();
				var time = $(this).parents("section").attr("data-time");

				t.ajaxJson({
					url:"/wx/index.php?model=workshift&m=ajax&cmd=110",
					callback:function(result,state){
						if(result.errcode !=0){
							pThis.error(result);
							return;
						}

						window.location.href = "/wx/index.php?model=process&a=create&debug=1&id="+result.form_id+"&rotaTime="+time+"&isIndex=1&menuType=2&activekey=2#pMineList"
					}
				})
			});
		}
	};

	return commonFuc;	
});