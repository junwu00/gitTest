require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/text!'+buildView+'/workshift/Templates/signInTpl.html',
    'common:widget/lib/plugins/gdmap/lbs.amap',
	'common:widget/lib/GenericFramework',
	"workshift:static/Script/commonData",
	"workshift:static/Script/commonFuc"
], function(t,UiFramework,tpl,signInTpl,gdmap,GenericFramework,commonData,commonFuc){
	var layerPackage = UiFramework.layerPackage();
	layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;

	var target = $("#main-container");
	var type = t.getHrefParamVal("type") || 1;

	var myLocation = {
		init:function(target,id){
			var addTime = 0;
			this.getSignInInfo(id,function(result){
				tpl.register('getRotaTime', commonFuc.getRotaTime);
				tpl.register('getDiffTime', commonFuc.getDiffTime);
				tpl.register('getShortTime', t.getShortTime);
				tpl.register('getLocationTime', commonFuc.getLocationTime);
				var dom = tpl(signInTpl,{
					getLocation:1,
					data:result,
					hideBtn:true,
					hideLink:true
				});
				tpl.unregister('getRotaTime');
				tpl.unregister('getDiffTime');
				tpl.unregister('getShortTime');
				tpl.unregister('getLocationTime');
				
				target.html($($.trim(dom)));

				GenericFramework.init(wxJsdkConfig);

				if(result.clock_list.length > 0){
					this.initMap(target,result.clock_list[0].lng,result.clock_list[0].lat);

					this.getClockList(target,result.clock_list);	
				}

				this.bindLocationDom(target);
			});
		},
		getSignInInfo:function(id,callback){
			var pThis = this;
			var url = ajaxUrl.getLocationInfoUrl;

			if(!id){
				return;
			}

			t.ajaxJson({
				url:url,
				data:{
					data:{
						record_id:id
					}
				},
				callback:function(result,state){
					console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);

						return;
					}

					pThis.data = result;

					callback.apply(pThis,[result]);

					layerPackage.unlock_screen();
				}
			});
		},
		initMap:function(target,lng,lat){
			if(!lng || !lat){
				return;
			}

			var point = null;
			var pThis = this;
			require(['amap'], function() {
				window.init = function() {
					gdmap.init(target.find("section.fullPic#gdmap"), {
						zoom:16, //PC [3-18] 移动 [3-19]
						center: [lng, lat], //测试用  113.320703, 23.165386

						autoMarkCenter: 0, //默认定位中心点
						autoFixedCenter: 0, //默认缩放地图时保持中心点不变

						searchEnable:0,

						callback: {
							onAfterSetCenter:function(position,map){
								console.log(position);
								point = position;
								pThis.position = position;
								var marker = null;
								var addr = "&nbsp;";

								pThis.map = map;

								map.getInfoByPosition(position,function(result){
									var address =  result.regeocode.formattedAddress;

									var time = t.getNowDate();

									marker = this.addMarker({
										position:[position.lng, position.lat],
										icon:gdmapMarkIcon3,
										iconSize:[24,33]
									},address,map.getMarkerLabel(pThis.data.user_info.pic_url,address,time),{x:18,y:19});

									//g.setFitView();
									pThis.marker = marker;
								});
							},
							onAfterMapLoaded: function(map) {
								console.log("地图加载完成");
							}
						}
					});
				}
			});
		},
		bindLocationDom:function(target){
			var pThis = this;

			target.on("click",".locationPart .oneLocation > section",function(){
				var id = $(this).attr("data-id");
				var type = t.getUrlParam("type") || 1;
					
				//代表
				if(type == 1){
					window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=1&id="+id+"&activekey=0&isIndex=1&menuType=2#pDealList";
				}else{
					window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType=3&id="+id+"&activekey=0&isIndex=1&menuType=2#pNotifyList";
				}
			});

			target.on("click",".clockList > section",function(){
				var o = $(this);
				var lng = o.attr("data-lng");
				var lat = o.attr("data-lat");
				
				if(!lng || !lat){
					layerPackage.fail_screen("获取地址错误");
					return;
				}
				if(!pThis.map || !pThis.marker){
					layerPackage.fail_screen("地图加载完未成！");
					return;
				}

				var newPosition = [lng,lat];
				
				pThis.marker.setPosition(newPosition);
				pThis.map.panTo(newPosition);

				target.find(".amap-marker-label .mTitle").text(o.find(".lPart_div > span").text());
			});

			GenericFramework.previewImage({
				target:target.find(".img_div")
			});
		},
		getClockList:function(target,data,append){
			tpl.register('getRotaTime', commonFuc.getRotaTime);
			tpl.register('getDiffTime', commonFuc.getDiffTime);
			tpl.register('getShortTime', t.getShortTime);
			var dom = $($.trim(tpl(signInTpl,{
				getClockList:1,
				data:data,
				append:append
			})));

			GenericFramework.previewImage({
				target:dom.find(".img_div")
			});
			
			tpl.unregister('getRotaTime');
			tpl.unregister('getDiffTime');
			tpl.unregister('getShortTime');

			if(!append){
				target.find(".clockList").append(dom);
			}else{
				target.find(".clockList").prepend(dom);
			}
			
		}
	};

	var id = t.getUrlParam("id");
	if(!id){return;}

	myLocation.init(target,id);

	layerPackage.unlock_screen();
},function(e){
	requireErrBack(e);
});