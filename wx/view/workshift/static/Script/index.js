require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/chartHelper',
    "common:widget/lib/scFormTable/scFormTable-v2.0",
    'common:widget/lib/plugins/juicer',
    "workshift:static/Script/commonData",
    "workshift:static/Script/commonFuc"
], function($,t,UiFramework,chartHelper,ScFormTable,tpl,commonData,commonFuc){
	var ajaxUrl = commonData.ajaxUrl;
	var layerPackage = UiFramework.layerPackage();

 	var target = $("#formTable");
 	var header = $(".rotaHeader");
 	var footer = $(".rotaFooter");
 	var rotaIcon = $(".rotaIcon");

	var schedule = {
		getTimeForThead:function(timestamp){
			var arr = new Array();

			this.timeIndex = null;
			var serverTime = this.serverTime ? (this.serverTime - 60*60*24)*1000 : new Date().getTime();

			for (var i = 0; i < timestamp.length; i++) {
				timestamp[i] = commonFuc.checkTime(timestamp[i]);
				var time = t.getNowDate(timestamp[i],1);

				var params = {
					value: time,
					formatter:function(val){
						var str = ""
						if(val.days == "01"){
							str += time.month+"月";
						}

						// + "<br />" + "(周"+t.getWeek(val.week)+")"

						return '<span class="block mt5 hSpan">'+str + parseInt(val.days)+'日</span><span class="block mt5 hSpan">(周'+t.getWeek(val.week)+')</span>'
					}
				};

				if(timestamp[i] < serverTime){
					this.timeIndex =  i;
					params.thClass = "bg-gray";
				}

				arr.push(params);
			};

			return arr;
		},
		getDataForTbody:function(data){
			var pThis = this;
			var arr = new Array();

			for (var i = 0; i < data.length; i++) {
				var params = {
					value:data[i] ,
					formatter : function(val){
						var arr = val.split("^");
						var str = "";
						for (var k = 0; k < arr.length; k++) {
							var v = arr[k];
							var color = v != "&nbsp;" ? "color"+pThis.shiftInfo[v].colorNumber : "c-9b";
							str += "<span class='ma0 block sch tac c-white "+(k == arr.length-1 ? "mb0" : "")+" "+color+"'>"+(v != "&nbsp;" ? pThis.shiftInfo[v].name : "&nbsp;")+"</span>";
						};

						return str;
					}
				};

				/*test*/
				if(this.timeIndex && i <= this.timeIndex){
					params.divClass = "bg-gray";
				}else{
					params.divClass = "bg-white";
				}

				arr.push(params);
			};

			return arr;
		},
		getClass:function(str){
			switch(str){
				case "早班":
					return "bg-blue";
					break;
				case "晚班":
					return "bg-yellow";
					break;
				case "补班":
					return "bg-s3";
					break;
			}
		},
		//竖
		getTbody:function(timestamp,a2){
			var pThis = this;
			var arr = new Array();
			var serverTime = this.serverTime ? (this.serverTime - 60*60*24)*1000 : new Date().getTime();

			for (var i = 0; i < timestamp.length; i++) {
				var time = t.getNowDate(timestamp[i],1);
				var str = "";

				if(i == 0){
					str += time.month+"月";
				}

				var params = {
					time:{
						divClass:"lh16 week",
						value:str + parseInt(time.days)+"日" + "<br />" + "(周"+t.getWeek(time.week)+")"
					},
					schedule:{
						value:a2[i] ,
						formatter : function(val){
							var arr = val.split("^");
							var s = "";
							for (var k = 0; k < arr.length; k++) {
								var v = arr[k];
								var color = v != "&nbsp;" ? "color"+pThis.shiftInfo[v].colorNumber : "c-9b";
								s += "<span class='ma0 block fl sch sch2 tac c-white "+color+"'>"+(v != "&nbsp;" ? pThis.shiftInfo[v].name : "&nbsp;")+"</span>";
							};

							return s;
						}
					}
				};

				if(timestamp[i] < serverTime){
					params.time.tdClass = "bg-gray";
					params.schedule.tdClass = "bg-gray";
				}

				arr.push(params);
			};

			return arr;
		},
		getPortraitParams:function(params){
			var data = this.data;

			params.isHideThead = 1;
		    params.fixFirst = 0;
		    params.thead = [{value:"时间"},{value:"排班"}];
		    params.tbody = schedule.getTbody(data.title,data.info);
		    params.resizeHeight = 1;
		    params.tbodyTdClass = "";
		    params.tbodyTdDivClass = "divPadding";
		    params.noOddEvenClass = 1;

		    return params;
		},
		getSchedule:function(year,month){
			var pThis = this;
			layerPackage.lock_screen();//遮罩start
			t.ajaxJson({
				url:ajaxUrl.getScheduleUrl,
				data:{
					"data":{
						"year":year,
						"month":month
					}
				},
				callback:function(result,state){
					//console.log(result);
					if(result.errcode !=0){
						layerPackage.unlock_screen();
						layerPackage.fail_screen(result.errmsg);

						return;
					}

					pThis.data = result;

					$(window).off("resize");
					target.html("");

					var noRota = $(".noRota");
					if(result.shift_info.length == 0){
						noRota.removeClass('hide');
					}else{
						noRota.addClass('hide');
					}

					noRota.addClass('hide');

					pThis.serverTime = result.time;
					
					var nowDate = t.getNowDate(null,1);

	 				header.find(".rotaTitle").text(result.user_name + "的" + (month || parseInt(nowDate.month)) + "月排班表");
	 				header.attr("data-year",year || nowDate.year);
	 				header.attr("data-month",month || parseInt(nowDate.month));

	 				if(!pThis.firstBind){
	 					pThis.bindBaseDom();
	 					pThis.firstBind = 1;
	 				}

	 				pThis.bindDom(result);

					layerPackage.unlock_screen();
				}
			});
		},
		bindBaseDom:function(){
			var pThis = this;
			header.find(".prevMonth,.nextMonth").on("click",function(e){
				var className = e.currentTarget.className;

				var month = parseInt(header.attr("data-month"));
                var year = parseInt(header.attr("data-year"));

				if(className.indexOf("prevMonth") > 0){
					month = month == 1 ? 12 : month-1;

                    if(month == 12){
                        year--;
                    }
				}else if(className.indexOf("nextMonth") > 0){
					month = month == 12 ? 1 : month+1;

                    if(month == 1){
                        year++;
                    }
				}

                pThis.getSchedule(year,month);
			});
		},
		bindDom:function(result){
			var pThis = this;
		 	var thead = schedule.getTimeForThead(result.title);
 			var tbody = schedule.getDataForTbody(result.info);

 			$(window).resize(function(event) {
				/*var orientation = screen.orientation || screen.mozOrientation || screen.msOrientation;

				if (orientation.type === "landscape-primary") {
				  console.log("That looks good.");
				} else if (orientation.type === "landscape-secondary") {
				  console.log("Mmmh... the screen is upside down!");
				} else if (orientation.type === "portrait-secondary" || orientation.type === "portrait-primary") {
				  console.log("Mmmh... you should rotate your device to landscape");
				}
				*/

				//portrait 竖屏 landscape 横屏
				var width = document.documentElement.clientWidth;
				var height =  document.documentElement.clientHeight;

				target.removeAttr('style').html("");

				var isApp = 1;
			 	//横屏
				var params = {
			        target:target,
			        thead:thead,
			        tbody:[tbody],
			        //tfoot:tfoot,
			        fixFirst:0,
			        isApp:isApp,
			        resizeHeight:"full",
			        theadEvent:0,
			        theadThDivClass:"headDivPadding lh16",
			        tbodyTdClass:"vt",
			        tbodyTdDivClass:"divPadding h_100",
			        hasBorder:1,
			        fadeScrollbars:1
			    };
			    
			    setTimeout(function(){
			    	if(width < height){
			    		params = schedule.getPortraitParams(params);

			    		pThis.getFooterHtml(1);

			    		target.addClass('portraitHeight').removeClass('landscapeHeight');
						footer.addClass('portraitHeight').removeClass('landscapeHeight');
						rotaIcon.addClass('portraitHeight').removeClass('landscapeHeight');
						header.addClass('portraitHeight').removeClass('landscapeHeight');
			    	}else{
			    		pThis.getFooterHtml();

			    		target.addClass('landscapeHeight').removeClass('portraitHeight');
						footer.addClass('landscapeHeight').removeClass('portraitHeight');
						rotaIcon.addClass('landscapeHeight').removeClass('portraitHeight');
						header.addClass('landscapeHeight').removeClass('portraitHeight');
			    	}

			    	try{
			    		if(platid == 4 || platid == 5){
			    			rotaIcon.addClass('hide');
			    		}else{
			    			rotaIcon.removeClass('hide');
			    		}
			    	}catch(e){
			    		console.log(e);
			    	}

			    	var s = new ScFormTable();
			    	s.init(params);
			    },0);
			});

			$(window).resize();
		},
		getTime:function(timestamp){
			timestamp = commonFuc.checkTime(timestamp);
			var time = t.getNowDate(timestamp,1);

			return time.year + "-" + time.month + "-" + time.days
		},
		getFooterHtml:function(type){
			var html = new Array();
			var data = this.data;

			var startTime = this.getTime(data.title[0]);
			var endTime = this.getTime(data.title[data.title.length - 1]);
			var arr = new Array();

			//横屏
			if(type){
				html.push('<div class="fs12 lhn c-9b rotaFooterPart">');
				html.push('<span>排版时间：</span>');
				html.push('<span>'+startTime+'～'+endTime+'</span>');
				html.push('</div>');
				html.push('<div class="fs12 lhn c-9b rotaFooterPart">');
				if(data.shift_info.length > 0){
					html.push('<span class="fl">班次说明：</span>');
				}
				html.push('<div class="fl rotaTimeList">');
				for (var i = 0; i < data.shift_info.length; i++) {
					var d = data.shift_info[i];
					var colorNumber = commonFuc.getColorNumber(i);
					html.push('<span class="rSpan tac c-white color'+colorNumber+'">'+d.name+'：'+d.time+'</span>');

					arr[d.id] = {
						colorNumber:colorNumber,
						name:d.name,
						id:d.id
					};
				};
				html.push('</div>');
				html.push('</div>');
			}else{
				html.push('<div class="fs12 lhn c-9b rotaFooterPart">');
				html.push('<span>排版时间：</span>');
				html.push('<span>'+startTime+'～'+endTime+'</span>');
				if(data.shift_info.length > 0){
					html.push('<span class="ml54">班次说明：</span>');
				}
				for (var i = 0; i < data.shift_info.length; i++) {
					var d = data.shift_info[i];
					var colorNumber = commonFuc.getColorNumber(i);
					html.push('<span class="rSpan tac c-white color'+colorNumber+'">'+d.name+'：'+d.time+'</span>');

					arr[d.id] = {
						colorNumber:colorNumber,
						name:d.name,
						id:d.id
					};
				};
				html.push('</div>');
			}

			this.shiftInfo = arr;

			footer.html(html.join(''));
		}
	};

	var year = t.getUrlParam("year") || undefined;
	var month = t.getUrlParam("month") || undefined;

	schedule.getSchedule(year,month);
},function(e){
	requireErrBack(e);
});