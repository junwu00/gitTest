require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'workshift');
fis.match('*', {
	release:'/workshift/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});