require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'legwork');
fis.match('*', {
	release:'/legwork/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});