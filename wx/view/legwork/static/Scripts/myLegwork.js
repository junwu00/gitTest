require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'legwork:static/Scripts/list'
],function(UiFramework,t,l) {
//*
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();

	var listUrl = "/wx/index.php?model=legwork&m=ajax&cmd=112";
	var listObj = $('.advice_list');

	var listMenu = UiFramework.listMenuPackage();
	var pageNum = 1,pageCount = 1; //当前页数
	var akey = 0;

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'我的','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'收到的','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:1
		},
		callback: {
			onMenuClick: function(data, obj){
				layerPackage.lock_screen();
				//console.log(data);
				//list.getList(data.activekey);
				pageNum = 1;
				pageCount = 1;
                listObj.find("div.line").remove();
                listObj.find("section").remove();

                akey = data.activekey;

				//滚动条
				UiFramework.scrollLoadingPackage().init({
					target:listObj,
					scroll_target:listObj,
					fun:function(self){
						if(pageCount < pageNum){self.remove_scroll_waiting($('.advice_list'));return;}

						if(pageNum != 1){
							self.add_scroll_waiting($("#powerby"),1);
						}

						l.getList({
							"type":akey,
							"listUrl":listUrl,
							"clickUrl":"/wx/index.php?debug=1&app=legwork&a=detail&id=",
							"listObj":listObj,
							"listType":"legwork",
							"page":pageNum,
							callback:{
								bindDom:function(res){
									var listType = res.listType,dType = res.dType,type = res.type,id = res.id;

									window.location.href = "/wx/index.php?debug=1&app=legwork&a=detail&id="+id+"&type="+type+"&listType="+listType;
								}
							}
							//"key_work":"",
				            //"asc":""
						},function(data,pCount){
							UiFramework.bottomInfo.remove();
							pageCount = pCount;
							layerPackage.unlock_screen();
							pageNum ++;
							listObj.attr("is_scroll_loading",false);

							if(data.length < 10){
								listObj.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting($('.advice_list'));

							UiFramework.bottomInfo.init(listObj);
						});
					}
				});

				listObj.scroll();
			}
		},
		noRecord : {
			target:listObj,
			imgSrc : wxOutworkingNothingImage,//图片地址
			title:'暂时没有外勤记录',//内容标题
			desc:['作为外勤某分类的接收人或抄送人','将会收到该外出人员的计划'] //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
},function(e){
	requireErrBack(e);
});