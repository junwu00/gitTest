require([
	'common:widget/lib/plugins/list_common/list_common',
	'common:widget/lib/UiFramework'
],function(List,UiFramework) {
	var listMenu = UiFramework.listMenuPackage();

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'我的','activekey':3,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'收到的','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:2
		},
		sort: { //配置头部插件排序
			status: true, //状态 false 取消 true 启用
			contents: [
				{'name':'时间','sort_name':'time'},
				{'name':'时间1','sort_name':'time1'},
				{'name':'时间3','sort_name':'time2'}
			]
		},
		search: {
			status: true
		},
		callback: {
			onMenuClick: function(data, obj){
				console.log(data);
				console.log(obj);
			},
			onSortClick: function(key){
				console.log(key);
			},
			onSearchClick: function(key, obj){
				console.log(key);
				console.log(obj);
			}
		},
		noRecord : {
			target:$('.advice_list'),
			imgSrc : '',//图片地址
			title:'111',//内容标题
			desc:11 //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
});