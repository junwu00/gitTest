require(['amap'], function() {
    window.init = function() {
		require([
			'common:widget/lib/plugins/gdmap/lbs.amap',
			'common:widget/lib/tools',
			'common:widget/lib/UiFramework'
		], function(gdmap,t,UiFramework) {
			var layerPackage = UiFramework.layerPackage();
			var getDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=108";

			var id = t.getHrefParamVal("id") || 1,
			dType = t.getHrefParamVal("t") || 1;

			t.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":{
						"id":id
					}
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						layerPackage.unlock_screen();//遮罩end
                		layerPackage.fail_screen(result.errmsg);
						return;
					}

					var data = result.data;

					//console.log(data);

					if((!data.longt || !data.lat) && data.sign_list){
						data.longt = data.sign_list[0].longt;
						data.lat = data.sign_list[0].lat;
					}

					if(!data.longt || !data.lat){return;}

					data.create_time = t.dateFormat(data.create_time);

					data.type = dType;
					
					//alert(JSON.stringify(data));

					//地图处理
					gdmap.init($("#map"), {
						//zoom:14,
						zoom:18,
						center: [data.longt, data.lat],

						autoMarkCenter: 0, //默认定位中心点
						autoFixedCenter: 0, //默认缩放地图时保持中心点不变

						searchEnable:0,

						callback: {
							onAfterMapLoaded: function(map) {
								try{
									//console.log("地图加载完成");
									var moveMarker = null;
									if(data && data.type == 2 && data.plan_id && data.plan_id == 0){
										map.addMarker({
											position:[data.longt, data.lat],
										},data.address,map.getMarkerLabel(data.creater.pic_url,data.address,data.create_time));
									}

									if(data.type == 1){
										var d = new Array();

										for (var i = 0; i < data.sign_list.length; i++) {
											var l = data.sign_list[i];
											d.push({
												lng:l.longt,
												lat:l.lat,
												info:[""]
											});

											map.addMarker({
												position:[l.longt, l.lat],
											},l.address,map.getMarkerLabel(
												data.creater.pic_url,
												l.address,
												t.dateFormat(l.create_time)
											));
										};
										//map.setInfoDlgData(d,{});
										if(d.length > 1){
											moveMarker = map.drawTrail(d,1,1,{});
										}
									}
								}
								catch(e){
									console.log(e);
								}
								
								var html = new Array();
								html.push('<div class="info hide">');		
								html.push('<img src="'+data.creater.pic_url+'" alt=""/>');
								html.push('<span class="mTitle">'+data.creater.name+'行动轨迹</span>');
								if(data.sign_list && data.sign_list.length > 1){
									html.push("<i>|</i><span class='mTitle c-green' id='seeTrail'>动态轨迹</span>");
								}
								html.push('</div>');

								
								$("#main-container").append(html.join(''));

								var infoObj = $("body").find(".info");
								infoObj.removeClass('hide');
								infoObj.attr("style","left:calc(50% - "+infoObj.innerWidth()+"px/2)");

								if(!moveMarker){return;}
								infoObj.off().on('click','#seeTrail',function(){
									AMap.event.trigger(moveMarker,"click");
								});
							}
						}
					});
				}
			});
		});
    }
});

