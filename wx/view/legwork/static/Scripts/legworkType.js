define([
	'jquery',
	'common:widget/lib/tools'
],function($,t){
	var legworkType = {
		data:null,
		jqObj:null,
		init:function(jqObj,data,finish_callback,bindDom_callback){
			if(!t.checkJqObj(jqObj)){t.console("jqObj error",1);return;}

			this.data = data;
			this.jqObj = jqObj;

			jqObj.html("").append(this.getHtml(data));

			if(!t.isFunction(finish_callback)){return;}
			finish_callback.apply(this);

			if(!t.isFunction(bindDom_callback)){return;}
			this.bindDom(bindDom_callback);
		},
		bindDom:function(callback){
			var pThis = this;
			this.jqObj.on("change",function(){
				var selectObj = $(this).find("option:selected"),
				id = selectObj.attr("id");

				callback.apply(pThis,[selectObj,id]);
			});
		},
		getHtml:function(data){
			var html = new Array();

			html.push('<option id="0">选择外出类型</option>');
			data.forEach(function(d){
				html.push('<option id="'+d.id+'">'+d.name+'</option>');
			});

			return html.join('');
		},
		getData:function(id){
			var data = this.data,res = null;

			data.forEach(function(d){
				if(d.id == id){
					res =  d;
					return;
				}
			});

			return res;
		}
	};

	return legworkType;
});