require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
	'legwork:static/Scripts/legworkType',
	'common:widget/lib/UiFramework'
],function($,t,depts_users_select,lwt,UiFramework) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	//请求地址
	var typeUrl = "/wx/index.php?model=legwork&m=ajax&cmd=111",
	createUrl = "/wx/index.php?model=legwork&m=ajax&cmd=101",
	editUrl = "/wx/index.php?model=legwork&m=ajax&cmd=102",
	getDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=108",
	editDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=102",
	base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
	dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
	user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
	user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
	init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
	removeDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=103";

	//jqObj对象
	var container = $("#main-container"),
	typeObj = container.find("#legworkType").find("select"),
	receiveObj = container.find("#receive"),
	notifyObj = container.find("#notify"),
	titleObj = container.find("#title"),
	markerObj = container.find("#mark"),
	startTime = container.find("#startTime"), //开始时间
	endTime = container.find("#endTime"); //结束时间

	function initUserDept(target,select,canEdit){
		var opt = {};
		//opt.target = target;
		//opt.select = select;
		opt.target = target;
		opt.select={'user':select} || {}; //{'user':['80237','80225']};
		opt.type = 2;
		opt.max_select = 0;
		opt.base_dept_url = base_dept_url;
		opt.dept_url = dept_url;
		opt.user_url = user_url;
		opt.user_detail = user_detail;
		opt.init_selected_url = init_selected_url;
		opt.canEdit = canEdit;
		//ob.get_depts_users_ids()
		//ob.get_depts_users()
		var dept = new depts_users_select().init(opt)

		//if(!canEdit){dept.target.off('click');}

		return dept;
	}
	//上传信息
	var classify_id = null,recevicer_list = null,notify_list = null,rObj = null,nObj = null;
	//编辑用到
	var id = t.getHrefParamVal("id"),is_publish = null,
	edit_recevicer_list = null,edit_notify_list = null,edit_classify_id;

	var legwork = {
		checkLegwork:function(params){
			layerPackage.lock_screen();//遮罩start
			if(!params.is_publish){params.is_publish = 0;}

			if(!rObj){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("请选择外出类型!");
				return;
			}

			var recevicer_list = rObj.get_depts_users_ids(),
			notify_list = nObj.get_depts_users_ids(),
			title = $.trim(titleObj.val()),mark = $.trim(markerObj.val()),
			start_time = startTime.val(),end_time = endTime.val();

			params.classify_id = classify_id;

			if(!start_time || !end_time){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("请选择时间!");
				return;
			}

			if(new Date(end_time).getTime() <= new Date(start_time).getTime()){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("结束时间不能比开始时间小!");
				return;
			}

			start_time = start_time.split("T").join(" ");
			end_time = end_time.split("T").join(" ");

			if(!title || !params.classify_id || !recevicer_list){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("信息不完整!");
				return;
			}

			var obj = {
				"title":title,
				"mark":mark,
				"classify_id":params.classify_id,
				"recevicer_list":recevicer_list,
				"notify_list":notify_list,
				"start_time":start_time,
				"end_time":end_time,
				"is_publish":params.is_publish //0：草稿，1:发布
			};

			if(params.id){obj.id = id;}

			t.ajaxJson({
				url:params.url,
				data:{
					"data":obj
				},
				callback:function(result,status){
					//console.log(result);

					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					layerPackage.success_screen(result.errmsg);

					if(!params.clickUrl){params.clickUrl = "/wx/index.php?debug=1&app=legwork&a=myList";}
					if(!params.clickUrlParam){params.clickUrlParam = "";}
					window.location.href = params.clickUrl + params.clickUrlParam;
				}
			});
		},
		//删除
		delLegwork:function(id) {
			t.ajaxJson({
				url: removeDetailUrl,
				data: {
					"data": {
						"id": id
					}
				},
				callback: function(result, status) {
					if (result.errcode != 0) {
						layerPackage.ew_alert({
							desc: result.errmsg
						});
						return;
					}

					var src = "";
					if (is_publish != "1") {
						src = "&activekey=3";
					}

					window.location.href = "index.php?debug=1&app=legwork&a=myList" + src;
					//调用遮罩层
				}
			});
		},
		getDetail:function(id){
			var pThis = this;
			t.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":{
						"id":id
					}
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					var data = result.data;
					//console.log(data);

					is_publish = data.is_publish;

					edit_classify_id = data.classify_id;
					edit_recevicer_list = data.recevicer_list;
					edit_notify_list = data.notify_list;
					
					typeObj.find('option[id="'+data.classify_id+'"]').prop("selected",true).change();
					titleObj.val(data.title);
					markerObj.val(data.desc);

					startTime.val(t.dateFormat(data.start_time).split(" ").join("T"));
					endTime.val(t.dateFormat(data.end_time).split(" ").join("T"));

					var menu = [{
						width: 30,
						name: "删除",
						iconClass: "icon-trash",
						btnColor: "r",
						click: function(self, obj, index) {
							layerPackage.ew_confirm({
								desc: "是否删除",
								ok_callback: function() {
									pThis.delLegwork(id);
								}
							});
						}
					}, {
						width: 70,
						name: "保存",
						iconClass: "icon-save",
						btnColor: "g",
						click: function(self, obj, index) {
							legwork.checkLegwork({
								"id": id,
								"is_publish": is_publish,
								"url": editUrl,
								"clickUrlParam": is_publish == 1 ? "" : "&activekey=3"
							});
						}
					}];

					if(is_publish == 0){
						menu[1].width = 30;

						menu.push({
							width: 40,
							name: "提交",
							btnColor: "g",
							iconClass: "icon-plus-sign",
							click: function(self, obj, index) {
								legwork.checkLegwork({
									"id": id,
									"is_publish": 1,
									"url": editUrl
								});
							}
						});
					}

					UiFramework.wxBottomMenu.init({
						target:container,
						menu:menu
					});

					layerPackage.unlock_screen();//遮罩end
				}
			});	
		}
	};

	t.ajaxJson({
		url:typeUrl,
		callback:function(result,status){
			if(status == false && result == null){return;}

			if(result.errcode != 0){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen(result.errmsg);
				return;
			}

			var data = result.data

			lwt.init(typeObj,data,function(){
				if(!id){
					layerPackage.unlock_screen();//遮罩end
				}

				if(id){
					legwork.getDetail(id);
				}else{
					var time = new Date().getTime();
					startTime.val(t.getNowDate(time).split(" ").join("T"));
					time  = time + 60*1000;
					endTime.val(t.getNowDate(time).split(" ").join("T"));
					UiFramework.wxBottomMenu.init({
						target:container,
						menu:[
							{
								width:30,
								name:"保存",
								iconClass:"icon-save",
								btnColor:"w",
								click:function(self,obj,index){
									legwork.checkLegwork({
										"is_publish":0,
										"url":createUrl,
										"clickUrlParam" :"&activekey=3"
									});
								}
							},
							{
								width:70,
								name:"创建",
								btnColor:"g",
								iconClass:"icon-plus-sign",
								click:function(self,obj,index){
									legwork.checkLegwork({
										"is_publish":1,
										"url":createUrl
									});
								}
							}
						]
					});
				}
			},function(selectObj,id){
				var recNameObj = receiveObj.find('.recName'),
				selectData = this.getData(id),
				r_type = null,r_list = null,r_edit = null,
				n_type = null,n_list = null,n_edit = null;

				//console.log(selectData);
				
				if(selectData){
					r_type = selectData.recevicer_type;
					r_list = selectData.recevicer_list;
					r_edit = selectData.recevicer_edit;

					n_type = selectData.notify_type;
					n_list = selectData.notify_list;
					n_edit = selectData.notify_edit;

					receiveObj.removeClass('hide');
					notifyObj.removeClass('hide');
				}else{
					receiveObj.addClass('hide');
					notifyObj.addClass('hide');
				}

				if(edit_classify_id == id && edit_recevicer_list){
					var r_arr = new Array();
					for (var i = 0; i < edit_recevicer_list.length; i++) {
						r_arr.push(edit_recevicer_list[i].id);
					};
					r_list = r_arr;
				}
				if(edit_classify_id == id && edit_notify_list){
					var n_arr = new Array();
					for (var i = 0; i < edit_notify_list.length; i++) {
						n_arr.push(edit_notify_list[i].id);
					};
					n_list = n_arr;
				}

				classify_id = id;recevicer_list = r_list;notify_list = n_list;

				//recNameObj.attr("edit",edit).html(list ? list.join('、') : "");
				/*console.group("===");
				console.log(r_edit + " " + JSON.stringify(r_list));
				console.log(n_edit + " " + JSON.stringify(n_list));
				console.groupEnd();*/

				if(r_edit == "0"){receiveObj.find("i.i-icon").addClass('hide');}else{receiveObj.find("i.i-icon").removeClass('hide');}
				if(n_edit == "0"){notifyObj.find("i.i-icon").addClass('hide');}else{notifyObj.find("i.i-icon").removeClass('hide');}

				//抄送人
				rObj = initUserDept(container.find("#recevicer_user"),r_list,r_edit);
				//接收人
				nObj = initUserDept(container.find("#notify_user"),n_list,n_edit);
			});
		}
	});

	UiFramework.bottomInfo.init(container,77);
});