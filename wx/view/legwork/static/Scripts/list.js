//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/list.html',
],function(t,tpl,listTemplate){

    var list = {
        listObj:null,
        dom:"",
        $dom:null,
        type:null,
        pageCount:null,
        getList:function(params,callback){
            if(!params.keyworks){params.keyworks = "";}
            if(!params.asc){params.asc = 0;}//0：按时间倒序排，1:按时间顺序排
            if(!params.type){params.type = 1;}//1：我的计划，2：我收到的，3：草稿计划
            if(!params.listType){params.listType = "plan";}
            if(!params.listUrl){return;}
            if(!params.clickUrl){return;}
            if(!t.checkJqObj(params.listObj)){return;}

            var pThis = this,listObj = params.listObj;
            pThis.params = params;

            t.ajaxJson({
                url:params.listUrl,
                data:{
                    "data":{
                        "type":params.type,
                        "key_work":params.keyworks,
                        "asc":params.asc,
                        "page":params.page
                    }
                },
                callback:function(result,status){
                    if(status == false && result == null){return;}

                    if(result.errcode != 0){t.console(result.errmsg,1);return;}

                    if(pThis.type != params.type){
                        pThis.type = params.type;
                        pThis.dom = "";
                        pThis.$dom = "";
                        pThis.pageCount = null;
                    }
                    var data = result.data.data;
                    pThis.pageCount = result.data.pageinfo.total_page;

                    listObj.find("section").remove();
                    listObj.find(".line1").remove();
                    
                    //console.dir(data);

                    if(data.length > 0){
                        listObj.find("div").addClass('hide');
                        //listObj.prepend(pThis.getHtml(data));

                        tpl.register('dateFormat', t.dateFormat);
                        var dom = tpl(listTemplate,{
                            listData:data,
                            listType:params.listType,
                            type:params.type,
                            defaultFace:defaultFace
                           	});
                        tpl.unregister('dateFormat');

                        pThis.dom = pThis.dom + dom;
                        pThis.$dom = $($.trim(pThis.dom));

                        listObj.append(pThis.$dom);
                    }else{
                        listObj.find("div").removeClass('hide');
                    }
                    
                    pThis.listObj = listObj;

                    pThis.bindDom();

                    if(!t.isFunction(callback)){return;}
                    callback(data,pThis.pageCount);
                }
            });
        },
        getHtml:function(data){
            var html = new Array(),bgColor = "",name = "",pThis = this;

            data.forEach(function(d){
                html.push('<div class="line1"></div>');
                html.push('<section class="autoH" data-id="'+d.id+'">');
                html.push('<span class="title fullW">'+d.title+'</span>');
                html.push('<p>')
                html.push('<time>'+t.dateFormat(d.create_time)+'</time>')
                html.push('<span class="message">'+d.classify_name+'</span>')
                if(d.state){
                    bgColor = "bg-green";name="已定位";
                }else{
                    bgColor = "bg-yellow";name="未定位";
                }
                html.push('<span class="fr status '+bgColor+'">'+name+'</span>')
                html.push('</p>')
                html.push('</section>');
            });
            html.push('<div class="line1"></div>');

            return html.join('');
        },
        bindDom:function(){
            var pThis = this,type = pThis.params.type,recevice = "",listType = pThis.params.listType,detailType="";

            this.listObj.find('section').on('click',function(){
                var id = $(this).attr("data-id"),
                    r = $(this).attr("data-recevicerType"),
                    dType = $(this).attr("data-type"),
                    planId = $(this).attr("data-planId");


                if(pThis.params.callback && t.isFunction(pThis.params.callback.bindDom)){
                    pThis.params.callback.bindDom.apply(pThis,[{
                        obj:pThis,listType:listType,r:r,dType:dType,planId:planId,id:id,type:type
                    }])
                    return;
                }

                return;
                //处理草稿跳转
                if(type == 3){
                    window.location.href = "/wx/index.php?debug=1&app=legwork&a=create&id="+id;
                    return;
                }

                if(r){
                    recevice = "&r="+r;
                }

                if(listType == "legwork" && dType && dType == 2){
                    detailType = "&t="+dType;
                }

                if(listType == "legwork" && dType && dType == 1  && planId && planId != 0){
                    id = planId;
                }

                window.location.href = pThis.params.clickUrl+id+"&type="+type+recevice+"&listType="+listType+detailType;
            });
        }
    };

	return list;
});