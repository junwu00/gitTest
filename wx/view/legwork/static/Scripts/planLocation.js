require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/localTypeAndList.html',
    'common:widget/lib/GenericFramework',
    'common:widget/lib/plugins/depts_users_select/js/depts_users_select'
],function(UiFramework,t,tpl,localTemplate,GenericFramework,depts_users_select) {
    var layerPackage = UiFramework.layerPackage(); 
    layerPackage.lock_screen(); //start

	var getUrl = "/wx/index.php?model=legwork&m=ajax&cmd=114",
        sendLocationUrl = "/wx/index.php?model=legwork&m=ajax&cmd=105",
        base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
        dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
        user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
        user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
        init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids';

	var container = $("#main-container"),
		localPageObj = container.find(".localPage"),
		localTypeAndListObj = container.find(".localTypeAndList");

	var firstTitleObj = container.find(".localList:eq(0)");

	//更新时间
	container.find("time").text(t.getNowDate());

    UiFramework.bottomInfo.init(container,77);

    //获取计划 实时定位列表
	t.ajaxJson({
        url:getUrl,
        callback:function(result,status){
            if(status == false && result == null){return;}

            if(result.errcode != 0){
                layerPackage.unlock_screen();//遮罩end
                layerPackage.fail_screen(result.errmsg);
                return;
            }

            var data = result.data;

            //console.log(data);
            
           	var dom = tpl(localTemplate,{
				data:data
			});

			$dom = $.trim(dom);

			//默认插入到开头位置
			localTypeAndListObj.html("").append($dom);

			//firstTitleObj.find(".title").text(localTypeAndListObj.find(".localList:first .title").text());

			bindDom.init();

            localTypeAndListObj.find(".localList:first").click();

            setTimeout(function(){
                localPageObj.find("#fresh-location").click();
            },2000);

            layerPackage.unlock_screen();//遮罩end
        }
    });

    var bindDom = {
        localInfo:{
            longt:null,
            lat:null,
            address:null,
            image_list:[],
            mark:null,
        },
        dType:null,
        id:null,
        notify_list:null,
        notify_edit:0,
        recevicer_list:null,
        recevicer_edit:0,
        classify_id:null,
        sourceType:null,
        picIndex:0,
    	init:function(){
            var pThis = this;
    		firstTitleObj.on("click",function(){
    			pThis.showTypeList();

                window.location.hash = "#planLocation";
    		});

    		localTypeAndListObj.on("click",".localList",function(){
                var o = $(this);
                o.find("i").addClass('icon-plane');
                o.siblings().find("i").removeClass('icon-plane');

    			firstTitleObj.find(".title").text(o.find(".title").text());
    			firstTitleObj.find("p").text(o.find("p").text());

    			localTypeAndListObj.addClass('hide');
    			localPageObj.removeClass('hide');

                pThis.dType = $(this).attr("data-type");
                pThis.id = $(this).attr("data-id");
                pThis.classify_id = $(this).find(".classify_id").val() || null;
                pThis.notify_list = $(this).find(".notify_list").val();
                pThis.notify_edit = $(this).find(".notify_edit").val() || "0";
                pThis.recevicer_list = $(this).find(".recevicer_list").val();
                pThis.recevicer_edit = $(this).find(".recevicer_edit").val() || "0";
                pThis.sourceType = $(this).find(".sourceType").val() || null;

                //console.log(pThis.recevicer_edit);
                //console.log(pThis.notify_edit);

                if(pThis.recevicer_edit == "0"){
                    localPageObj.find("#receive i.i-icon").addClass('hide');
                }else{
                    localPageObj.find("#receive i.i-icon").removeClass('hide');
                }
                if(pThis.notify_edit == "0"){
                    localPageObj.find("#notify i.i-icon").addClass('hide');
                }else{
                    localPageObj.find("#notify i.i-icon").removeClass('hide');
                }

                var rArr = pThis.recevicer_list.split(","),
                    nArr = pThis.notify_list.split(",");

                pThis.rObj = pThis.initUserDept(container.find("#recevicer_user"),rArr,pThis.recevicer_edit);
                pThis.nObj = pThis.initUserDept(container.find("#notify_user"),nArr,pThis.notify_edit);

                pThis.upLoadFile(pThis.sourceType);

                if(window.location.hash){window.history.go(-1);}
    		});

            GenericFramework.init(wxJsdkConfig);

            //刷新获取地理位置
            var freshBtn = localPageObj.find("#fresh-location");
            GenericFramework.location({
                target:freshBtn,
                callback:{
                    beforeGetLocation:function(){
                        localPageObj.find(".search-address").removeClass('hide');
                    },
                    afterGetLocation:function(res,lng,lat){
                        var address = res.address;
                        pThis.localInfo.address = address;
                        pThis.localInfo.longt = lng;
                        pThis.localInfo.lat = lat;

                        localPageObj.find(".search-address").addClass('hide');
                        localPageObj.find(".now-address").text(address);
                        container.find(".findLocation .address").text(address);
                    },
                    error:function(msg){
                        alert(msg);
                    }
                }
            });

            localPageObj.find("#send-location-info").on("click",function(){
                var localInfo = pThis.localInfo,longt = localInfo.longt,lat = localInfo.lat,address = localInfo.address,
                    dType = pThis.dType,id = pThis.id;
                
                pThis.recevicer_list = pThis.rObj.get_depts_users_ids();
                pThis.notify_list = pThis.nObj.get_depts_users_ids();

                if(!longt || !lat || !address || !dType || !id){alert("请刷新地址!");return;}

                if(dType == 2 && pThis.recevicer_list.length == 0){alert("请选择接收人!");return;}

                if(pThis.sourceType != 0 && localInfo.image_list.length == 0){
                    alert("至少上传一张图片!");return;
                }

                var mark = $.trim(localPageObj.find("#mark").val()),image_list = pThis.localInfo.image_list;

                var parmas = {
                    "type":dType,
                    "id":id,
                    "longt":longt,
                    "lat":lat,
                    "address":address,
                    "mark":mark,
                    "image_list":image_list,
                    "recevicer_list":pThis.recevicer_list,
                    "notify_list":pThis.notify_list
                    //"create_time":new Date().getTime() / 1000
                };

                t.ajaxJson({
                    url:sendLocationUrl,
                    data:{
                        "data":parmas
                    },
                    callback:function(result,status){
                        if(status == false && result == null){return;}

                        if(result.errcode != 0){t.console(result.errmsg,1);
                            layerPackage.unlock_screen();//遮罩end
                            layerPackage.fail_screen(result.errmsg);
                            localPageObj.find(".searchLocation").slideUp('fast');
                            return;
                        }

                        container.find("#location-info").show().animate({
                            "top": "0",
                        },"normal");

                        container.find(".findLocation").show().animate({
                            "top": "0",
                        },"normal");

                        try{
                            container.find(".localHref").attr("href","/wx/index.php?debug=1&app=legwork&a=detail&id="+result.data.id+"&type=1&listType=legwork");
                        }catch(e){
                            console.log(e);
                        }
                    }
                });
            });

            $(window).on('hashchange', function (e) {
            	 var hash = window.location.hash;
                 if(hash == '#planLocation'){
                     pThis.showTypeList();
                 }else{
                	 pThis.hideTypeList();
                 }
            });
            
    	},
        initUserDept:function(target,select,canEdit){
            var opt = {};
            //opt.target = target;
            //opt.select = select;
            opt.target = target;
            opt.select={'user':select} || {}; //{'user':['80237','80225']};
            opt.type = 2;
            opt.max_select = 0;
            opt.base_dept_url = base_dept_url;
            opt.dept_url = dept_url;
            opt.user_url = user_url;
            opt.user_detail = user_detail;
            opt.init_selected_url = init_selected_url;
            opt.canEdit = canEdit;
            //ob.get_depts_users_ids()
            //ob.get_depts_users()
            var dept = new depts_users_select().init(opt)

            return dept;
        },
        upLoadFile:function(sourceType){
            var st = 3;

            if(sourceType == 0){ //不需要
                st = 3;
            }else if(sourceType == 1){ //拍照
                st = 2;
            }else if(sourceType == 2){ //拍照或相册
                st = 3;
            }

            //上传图片
            var picList = localPageObj.find('.picList'),pThis = this;
            GenericFramework.uploadFile({
                target:picList.find('.addPhotos'),
                type:"wx",
                sourceType:st,//1 相册 2 相机 3 相册和相机
                callback:{
                    afterUpload:function(imgInfo){
                        //console.log(imgInfo);

                        var html = new Array();
                        html.push('<div class="upload-pic fl" data-picIndex="'+pThis.picIndex+'">');
                        html.push('<i class="icon-remove">');
                        html.push('</i>');
                        html.push('<img src="'+imgInfo.image_path+'" alt=""/>');
                        html.push('</div>');

                        picList.prepend(html.join(''));

                        pThis.localInfo.image_list.push({
                            hash:imgInfo.hash,
                            size:imgInfo.filesize,
                            url:imgInfo.path,
                            name:imgInfo.name,
                            ext:imgInfo.ext,
                            index:pThis.picIndex,
                            image_path:imgInfo.image_path
                        });

                        pThis.picIndex ++;

                        //预览图片
                        var picObj = picList.find('.upload-pic');
                        GenericFramework.previewImage({
                            target:picObj
                        });

                        //删除图片
                        picObj.on('click','i',function(event){
                            event.stopPropagation();
                            var pic = $(this).parent(),pIndex = pic.attr("data-picIndex");

                            for (var i = 0; i < pThis.localInfo.image_list.length; i++) {
                                if(pThis.localInfo.image_list[i].index == pIndex){
                                    pThis.localInfo.image_list.splice(i,1);
                                    break;
                                }
                            };

                            pic.remove();
                        });
                    }
                }
            });
        },
        hideTypeList:function(){
            localPageObj.removeClass('hide');
            localTypeAndListObj.addClass('hide');
        },
        showTypeList:function(){
            localPageObj.addClass('hide');
            localTypeAndListObj.removeClass('hide');
        }
    };
});