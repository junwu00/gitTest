require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'legwork:static/Scripts/list'
],function(UiFramework,t,l) {
//*
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();

	var listUrl = "/wx/index.php?model=legwork&m=ajax&cmd=106";
	var listObj = $('.advice_list');

	var activekey = t.getHrefParamVal("activekey") || 1;

	var listMenu = UiFramework.listMenuPackage();
	var pageNum = 1,pageCount = 1; //当前页数
	var akey = 0;

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'我的','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'收到的','activekey':2,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'草稿','activekey':3,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:activekey
		},
		callback: {
			onMenuClick: function(data, obj){
				layerPackage.lock_screen();
				//console.log(data);
				//list.getList(data.activekey);
				pageNum = 1;
				pageCount = 1;
				//listObj.scrollTop(0);
                listObj.find("div.line").remove();
                listObj.find("section").remove();

                akey = data.activekey;
  
				UiFramework.scrollLoadingPackage().init({
					target:listObj,
					scroll_target:listObj,
					fun:function(self){
						if(pageCount < pageNum){self.remove_scroll_waiting($('.advice_list'));return;}

						if(pageNum != 1){
							self.add_scroll_waiting($("#powerby"),1);
						}
						
						l.getList({
							"type":akey,
							"listUrl":listUrl,
							"clickUrl":"/wx/index.php?debug=1&app=legwork&a=detail&id=",
							"listObj":listObj,
							"page":pageNum,
							callback:{
								bindDom:function(res){
									var recevice = "",type = res.type,id = res.id,listType = res.listType;
									//草稿
									if(type == 3){
										window.location.href = "/wx/index.php?debug=1&app=legwork&a=create&id="+id;
                    					return;
									}

									if(type == 2){
										recevice = "&r="+res.r;
									}

									window.location.href = "/wx/index.php?debug=1&app=legwork&a=detail&id="+id+"&type="+type+recevice+"&listType="+listType;
								}
							}
							//"keyworks":keyworks,
							//"asc":asc
						},function(data,pCount){
							UiFramework.bottomInfo.remove();

							pageCount = pCount;
							layerPackage.unlock_screen();
							pageNum ++;
							listObj.attr("is_scroll_loading",false);

							if(data.length < 10){
								listObj.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting($('.advice_list'));

							UiFramework.bottomInfo.init(listObj);
						});
					}
				});

				listObj.scroll();
			}
		},
		noRecord : {
			target:listObj,
			imgSrc : wxOutworkingNothingImage,//图片地址
			title:'暂时没有外出计划',//内容标题
			desc:['外勤计划只针对外勤分类配置中需要创','建计划的外勤分类类型'] //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
},function(e){
	requireErrBack(e);
});