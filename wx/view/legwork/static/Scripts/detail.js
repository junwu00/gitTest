require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/detail.html',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/dTitleTpl.html',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/dMarkTpl.html',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/dListTpl.html',
	'common:widget/lib/text!'+buildView+'/legwork/Templates/dBtnTpl.html',
	'common:widget/lib/plugins/gdmap/lbs.amap',
	'common:widget/lib/GenericFramework'
],function(t,UiFramework,tpl,detailTemplate,dTitleTpl,dMarkTpl,dListTpl,dBtnTpl,gdmap,GenericFramework) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen(); //start

	var getDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=108",
		removeDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=103",
		sendLocationUrl = "/wx/index.php?model=legwork&m=ajax&cmd=105",
		finishLocationUrl = "/wx/index.php?model=legwork&m=ajax&cmd=115",
		container = $("#main-container");

	var id = t.getHrefParamVal("id") || 1,type = t.getHrefParamVal("type") || 1,
	r = t.getHrefParamVal("r") || ""/*接收或者抄送*/,dType = t.getHrefParamVal("t") || 1/*详情定位类型*/,
	listType = t.getHrefParamVal("listType") || "plan",gdMapInit = false;

	if(listType =="legwork"/* && dType && dType==2*/){
		getDetailUrl = "/wx/index.php?model=legwork&m=ajax&cmd=113";
		gdMapInit = true;
		container.addClass('bg-white');
	}
	
	var detail = {
		dom:null,
		$dom:null,
		jqObj:null,
		localW:0,
		localH:0,
		localInfo:{
			longt:null,
			lat:null,
			address:null,
			image_list:[],
			mark:null
		},
		cache:[],
		getDetail:function(jqObj,id,gdMapInit){
			var pThis = this;
			this.jqObj = jqObj;
			t.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":{
						"id":id
					}
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					var data = result.data;
					pThis.sourceType = data.sign_pic_type;
					pThis.cacheData = data;

					var sign_list = data.sign_list,count_time = "";

					if(sign_list && sign_list.length >0){
						for (var i = 0; i < sign_list.length; i++) {
							var image_list = sign_list[i].image_list;
							for (var j = 0; j < image_list.length; j++) {
								image_list[j] = media_domain + "/" + image_list[j];
							};

						};
					}

					if(data.image_list && data.image_list.length>0){
						for (var k = 0; k < data.image_list.length; k++) {
							data.image_list[k] = media_domain + "/" + data.image_list[k];
						};
					}

					pThis.cache = t.clone(sign_list);

					if(sign_list && sign_list.length > 0 && sign_list.length != 1){
						var sign_list_len = sign_list.length;
						//获取全程时间
						count_time = pThis.getTime(sign_list[sign_list_len - 1].create_time , sign_list[0].create_time);

						//改版顺序
						var signList = new Array();
						for (var i = sign_list_len - 1; i >= 0; i--) {
							signList.push(sign_list[i]);
						};

						data.sign_list = signList;
					}

					data.count_time = count_time;
					data.recevie_type = r;
					data.type = dType;

					//console.log(data);

					var dom = null;
					
					tpl.register('dateFormat', t.dateFormat);
					dom = tpl(detailTemplate,{
						dTitleTpl:dTitleTpl,
						dMarkTpl:dMarkTpl,
						dListTpl:dListTpl,
						dBtnTpl:dBtnTpl,
						detailData:{
							data:data,
							type:type,
							cdn_domain:cdn_domain,
							defaultFace:defaultFace
						},
						listType:listType
					});
					tpl.unregister('dateFormat');

					pThis.dom = $.trim(dom);
					//console.log(pThis.dom);
					pThis.$dom = $(pThis.dom);

					//默认插入到开头位置
					pThis.jqObj.html("").append(pThis.$dom);

					pThis.locationObj = pThis.jqObj.find("#location-info");
					pThis.localListObj = pThis.jqObj.find("#localList");

					pThis.getLocalListHeight();
					pThis.getContentHeight();

					pThis.bindDom();	

					layerPackage.unlock_screen();

					//地图处理
					if(gdMapInit){
						pThis.gdMapInit({
							mapObj:pThis.jqObj.find("section.fullPic#gdmap"),
							address:data.address,
							longt:data.longt,
							lat:data.lat,
							pic_url:data.creater.pic_url,
							time:t.dateFormat(data.create_time)
						});
					}
				}
			});
		},
		bindDom:function(){
			var pThis = this;
			this.jqObj.find('.showMore').on("click",function(){
				var o = $(this).next();
				if($(this).hasClass('isClick')){
					o.addClass('hide');
					$(this).text("显示更多").removeClass('isClick');
				}else{
					o.removeClass('hide');
					$(this).text("隐藏更多").addClass('isClick');
				}
			});

			//删除
			this.jqObj.find("#dBtn").on("click",".removeLegwork",function(){
				layerPackage.ew_confirm({
					title:"确定删除该计划",
					ok_callback:function(){
						layerPackage.lock_screen();//遮罩start
						t.ajaxJson({
							url:removeDetailUrl,
							data:{
								"data":{
									"id":id
								}
							},
							callback:function(result,status){
								if(result.errcode != 0){
									layerPackage.ew_alert({title:result.errmsg});
									return;
								}

								window.location.href = "/wx/index.php?debug=1&app=legwork&a=myList";
							}
						});
					}
				});
			});

			//修改
			this.jqObj.find("#dBtn").on("click",".editLegwork",function(){
				window.location.href = "/wx/index.php?debug=1&app=legwork&a=create&id="+id;
			});

			//完成
			this.jqObj.find("#dBtn").on("click","#finish-location-info",function(){
				t.ajaxJson({
					url:finishLocationUrl,
					data:{
						"data":{
							"id":id,
						}
					},
					callback:function(result,status){
						if(status == false && result == null){return;}

						if(result.errcode != 0){t.console(result.errmsg,1);return;}

						layerPackage.success_screen(result.errmsg);
						window.location.href = "/wx/index.php?debug=1&app=legwork&a=myList";
					}
				});
			});

			//查看轨迹
			this.jqObj.find(".checkContrail").on("click",function(){
				window.location.href = "/wx/index.php?debug=1&app=legwork&a=map&id="+id+"&listType="+listType+"&t="+dType;	
			});

			//查看计划
			this.jqObj.find(".checkPlan").on("click",function(){
				window.location.href = "/wx/index.php?debug=1&app=legwork&a=detail&id="+pThis.cacheData.plan_id+"&listType=plan&type="+type;
			});

			if(!this.locationObj){return;}

			//实时定位
			//弹出页面
			this.jqObj.find("#dBtn").on("click","#getLocation",function(){
				layerPackage.lock_screen({bgColor:'rgba(0,0,0,0.3)',showLoading:false});

				pThis.locationObj.find("time").text(t.getNowDate());

				pThis.locationObj.slideDown('fast',function() {
					pThis.localW = $(this).width();
					pThis.localH = $(this).height();

					//$(".searchLocation").width(localW);
					//$(".searchLocation").height(localH);

					//$("#fresh-location").click();

					pThis.locationObj.find("#fresh-location").click();
				});
			});
			//关闭页面
			this.locationObj.on("click","#close-location-info",function(){
				pThis.locationObj.slideUp('fast',function() {
					pThis.localW = 0;
					pThis.localH = 0;

					layerPackage.unlock_screen();
				});
			});

			//提交
			this.locationObj.on("click","#send-location-info",function(){
				var localInfo = pThis.localInfo,longt = localInfo.longt,lat = localInfo.lat,address = localInfo.address;

				if(!longt || !lat || !address || !dType || !id){alert("请刷新地址!");return;}

				if(pThis.sourceType != 0 && localInfo.image_list.length == 0){
                    alert("至少上传一张图片!");return;
                }

				var mark = $.trim(pThis.locationObj.find("#mark").val()),image_list = pThis.localInfo.image_list;

				pThis.locationObj.find(".searchLocation").slideDown('fast');

				//alert(JSON.stringify({"type":dType,"id":id,"longt":longt,"lat":lat,"address":address,"mark":mark,"image_list":image_list}));
				var parmas = {
					"type":dType,
					"id":id,
					"longt":longt,
					"lat":lat,
					"address":address,
					"mark":mark,
					"image_list":image_list,
					"create_time":new Date().getTime() / 1000
				};

				t.ajaxJson({
					url:sendLocationUrl,
					data:{
						"data":parmas
					},
					callback:function(result,status){
						if(status == false && result == null){return;}

						if(result.errcode != 0){t.console(result.errmsg,1);
							alert(result.errmsg);
							pThis.locationObj.find(".searchLocation").slideUp('fast');
							return;
						}

						pThis.locationObj.find(".searchLocation").slideUp('fast',function(){
							pThis.locationObj.find(".findLocation").slideDown('fast');
						});

						pThis.refreshDetail(parmas);
					}
				});
			});

			GenericFramework.init(wxJsdkConfig);
			
			//刷新获取地理位置
			var freshBtn = this.locationObj.find("#fresh-location");
			GenericFramework.location({
				target:freshBtn,
				callback:{
					beforeGetLocation:function(){
						pThis.locationObj.find(".search-address").removeClass('hide');
					},
					afterGetLocation:function(res,lng,lat){
						var address = res.address;
						pThis.localInfo.address = address;
						pThis.localInfo.longt = lng;
						pThis.localInfo.lat = lat;

						pThis.locationObj.find(".search-address").addClass('hide');
						pThis.locationObj.find(".now-address").text(address);
						pThis.locationObj.find(".findLocation .address").text(address);
					},
					error:function(msg){
						alert(msg);
					}
				}
			});
			
			//上传图片
			var picList = this.locationObj.find('.picList'),picIndex = 0;

			var st = 3;
            if(this.sourceType == 0){ //不需要
                st = 3;
            }else if(this.sourceType == 1){ //拍照
                st = 2;
            }else if(this.sourceType == 2){ //拍照或相册
                st = 3;
            }
			GenericFramework.uploadFile({
				target:picList.find('.addPhotos'),
				type:"wx",
				sourceType:st,
				callback:{
					afterUpload:function(imgInfo){
						//console.log(imgInfo);

						var html = new Array();
						html.push('<div class="upload-pic fl" data-picIndex="'+picIndex+'">');
						html.push('<i class="icon-remove">');
						html.push('</i>');
						html.push('<img src="'+imgInfo.image_path+'" alt=""/>');
						html.push('</div>');

						picList.prepend(html.join(''));

						pThis.localInfo.image_list.push({
							hash:imgInfo.hash,
							size:imgInfo.filesize,
							url:imgInfo.path,
							name:imgInfo.name,
							ext:imgInfo.ext,
							index:picIndex,
							image_path:imgInfo.image_path
						});

						picIndex ++;

						//预览图片
						var picObj = picList.find('.upload-pic');
						GenericFramework.previewImage({
							target:picObj
						});

						//删除图片
						picObj.on('click','i',function(event){
                            event.stopPropagation();
                            var pic = $(this).parent(),pIndex = pic.attr("data-picIndex");

                            for (var i = 0; i < pThis.localInfo.image_list.length; i++) {
                                if(pThis.localInfo.image_list[i].index == pIndex){
                                    pThis.localInfo.image_list.splice(i,1);
                                    break;
                                }
                            };

                            pic.remove();
                        });
					}
				}
			});

			this.locationObj.on('click',".closeInfo", function(event) {
				pThis.locationObj.find(".findLocation").slideUp('fast');
				pThis.clearData();
				pThis.locationObj.find("#close-location-info").click();
				layerPackage.unlock_screen();
				//pThis.locationObj.find(".now-address").html(pThis.locationObj.find(".address").html());
			});

			//列表图片点击
			this.jqObj.find("#localList").on("click","li img",function(){
				var len = $(this).parents("ul").find("li").length,
					index = $(this).parents("li").index();

				var data = pThis.cache[len - 1 - index];

				GenericFramework.previewImage({
					target:$(this),
					current:$(this).attr("src"),
					urls:data.image_list
				});
			});

			//会话
			GenericFramework.chat({
				target:this.jqObj.find("#chat"),
				userIds:this.cacheData.creater.acct
			});

			this.jqObj.find(".viewList img").on("click",function(){
				var arr = new Array();

				$(this).parent().find("img").each(function(){
					arr.push($(this).attr("src"));
				});

				GenericFramework.previewImage({
					target:$(this),
					current:$(this).attr("src"),
					urls:arr
				});
			});
		},
		gdMapInit: function(params) {
			require(['amap'], function() {
				window.init = function() {
					gdmap.init(params.mapObj, {
						zoom:18, //PC [3-18] 移动 [3-19]
						center: [params.longt, params.lat],

						autoMarkCenter: 0, //默认定位中心点
						autoFixedCenter: 0, //默认缩放地图时保持中心点不变

						searchEnable:0,

						callback: {
							onAfterMapLoaded: function(map) {
								//console.log("地图加载完成");
								map.addMarker({
									position:[params.longt, params.lat],
								},params.address,map.getMarkerLabel(params.pic_url,params.address,params.time));
							}
						}
					});
				}
			});
		},
		//计算时间差
		getTime:function(firstTime,lastTime){
			var days = 0,minutes = 0,hours = 0;
			try{
				var time = Math.abs(lastTime - firstTime);

				days = Math.floor(time / (24 * 3600));
				leave1 = time % (24 * 3600);
				hours = Math.floor(leave1 / (3600));
				leave2 = leave1 % (3600);
				minutes = Math.floor(leave2 / (60));
			}catch(e){
				alert(e);						
			}

			return {
				days: days,
				hours: hours,
				minutes: minutes
			};
		},
		//数据转换
		changeData:function(data){
			var image_list = data.image_list,arr = new Array();
			for (var i = 0; i < image_list.length; i++) {
				image_list[i].image_path = image_list[i].image_path;

				arr.push(image_list[i].image_path);
			};

			data.image_list = arr;

			return data;
		},
		//添加图片列表
		refreshDetail:function(data){
			try {
				//保存起来，方便后面定位完成添加列表
				this.cache.push(this.changeData(data));

				var sign_list = this.cache;

				var obj = {
					state: 1,
					sign_list: sign_list
				};

				var count_time = "";
				if (sign_list && sign_list.length > 0 && sign_list.length != 1) {
					var sign_list_len = sign_list.length;
					//获取全程时间

					count_time = this.getTime(sign_list[sign_list_len - 1].create_time , sign_list[0].create_time);

					//改版顺序
					var signList = new Array();
					for (var i = sign_list_len - 1; i >= 0; i--) {
						signList.push(sign_list[i]);
					};

					obj.sign_list = signList;
				}

				obj.count_time = count_time;

				tpl.register('dateFormat', t.dateFormat);
				if(this.cacheData.state == "0"){
					this.cacheData.state = 1;
					var dTitle = tpl(dTitleTpl, {
						data:this.cacheData,
						defaultFace:defaultFace
						
					});
					this.jqObj.find("#dTitle").html($($.trim(dTitle)).html());

					var dBtn = tpl(dBtnTpl, {
						data:this.cacheData,
						type:type
					});
					this.jqObj.find("#dBtn").html($($.trim(dBtn)).html());
				}
				var dList = tpl(dListTpl, {
					data:obj
				});
				this.jqObj.find("#localList").html($($.trim(dList)).html());
				this.localListObj = this.jqObj.find("#localList");
				tpl.unregister('dateFormat');

				this.locationObj.find(".searchLocation").slideUp('fast');
			} catch (e) {
				alert(e);
			}
		},
		clearData:function(){
			picIndex = 0;
			this.localInfo = {
				longt:null,
				lat:null,
				address:null,
				image_list:[],
				mark:null
			};
			this.locationObj.find('.picList .upload-pic').remove();
			this.locationObj.find(".now-address").text("");
			this.locationObj.find("#mark").val("");
		},
		//调整页面内容的高度
		getContentHeight:function(){
			var h = this.jqObj.find("#dBtn").outerHeight() || 0;
			//this.jqObj.css("height","calc(100% - "+h+"px)");
			//this.jqObj.css("height","-webkit-calc(100% - "+h+"px)");
			this.jqObj.attr("style","height:calc(100% - "+h+"px);height:-webkit-calc(100% - "+h+"px);");
		},
		//调整列表内容高度
		getLocalListHeight:function(){
			var h = 0;
			$("#main-container > section:not(#localList)").each(function(){
				h += $(this).outerHeight();
			});

			h = h + 15 + 28 - (this.jqObj.find("#dBtn").outerHeight() || 0);//margin-top 15 padding:14 0 

			if(this.localListObj.length==0){return;}
			//this.localListObj.css("min-height","calc(100% - "+h+"px)");
			//this.localListObj.css("min-height","-webkit-calc(100% - "+h+"px)");
			this.localListObj.attr("style","min-height:calc(100% - "+h+"px);min-height:-webkit-calc(100% - "+h+"px);");
		}
	};

	detail.getDetail(container,id,gdMapInit);
},function(e){
	requireErrBack(e);
});



