requirejs.config({
	baseUrl: '/',
	paths: requirejs_path_conf,
	map: {
        '*': {
            'css': 'common:widget/lib/require_css',
        }
    }
});

require(['common:widget/lib/lbs.amap'], function(gdmap) {
	var map = gdmap.init($("#main-container"),{
		width:"100%",
		height:"500px",

		//zoom:14,
		center:[113,23],

		autoMarkCenter:1,//默认定位中心点
		autoFixedCenter:1,//默认缩放地图时保持中心点不变

		callback:{
			onMapClick:function(event,mapId){
				//alert(mapId);
			},
			onAfterSetCenter:function(position){
				console.group("中心坐标:");
				console.log(position);
				map.getInfoByPosition(position,function(result){
					console.log(result.regeocode.formattedAddress);
				});
				console.groupEnd();
			},
			onAfterMapLoaded:function(){
				console.log("地图加载完成");
			},
			onMoveEnd:function(position){
				console.group("地图移动后中心坐标:");
				console.log(position);
				map.getInfoByPosition(position,function(result){
					//console.log(result);
					console.log(result.regeocode.formattedAddress);
				});
				console.groupEnd();
			}
		}
	});

	setTimeout(function(){
		//map.setMapCss({css:"color:red;"});
		//map.panTo([115,23]);.
		var data = [
			{lng:113.1,lat:23,info:["11111","11111"]},
			{lng:113.2,lat:23,info:["22222","22222"]},
			{lng:113.3,lat:23,info:["33333","33333"]},
			{lng:113.1,lat:22.9,info:["44444","44444"]},
			{lng:113.2,lat:22.9,info:["55555","55555"]},
			{lng:113.3,lat:22.9,info:["66666","66666"]},
		];
		/*map.setInfoDlgData(data,{
		});*/

		//map.drawTrail(data,1,1,{});
	},2000);
});