requirejs.config({
	baseUrl: '/',
	paths: requirejs_path_conf,
	map: {
        '*': {
            'css': 'common:widget/lib/require_css',
        }
    }
});

require(['common:widget/lib/UiFramework','jquery'], function(UiFramework,$) {
	var btn = UiFramework.btnPackage();

	btn.checkBoxBtn.init({
		status:true,
		target:$('.btn1'),
		callback:{
			on:function(target,obj){
				if(!target.hasClass('off')){
					alert('打开了1');
				}
			},off:function(target,obj){
				if(target.hasClass('off')){
					alert('关闭了1');
				}
			}
		}
	});

	btn.checkBoxBtn.init({
		status:true,
		target:$('.btn2'),
		type:1,
		callback:{
			on:function(target,obj){
				if(!target.hasClass('off')){
					alert('打开了2');
				}
			},off:function(target,obj){
				if(target.hasClass('off')){
					alert('关闭了2');
				}
			}
		}
	});

	btn.checkBoxBtn.init({
		target:$('.btn3'),
		type:1,
		callback:{
			on:function(target,obj){
				if(!target.hasClass('off')){
					alert('打开了3');
				}
			},off:function(target,obj){
				if(target.hasClass('off')){
					alert('关闭了3');
				}
			}
		}
	});

	btn.checkBoxBtn.init({
		status:true,
		target:$('.btn4'),
		type:2,
		callback:{
			on:function(target,obj){
				if(!target.hasClass('off')){
					alert('打开了4');
				}
			},off:function(target,obj){
				if(target.hasClass('off')){
					alert('关闭了4');
				}
			}
		}
	});

	btn.checkBoxBtn.init({
		target:$('.btn5'),
		type:2,
		callback:{
			on:function(target,obj){
				if(!target.hasClass('off')){
					alert('打开了5');
				}
			},off:function(target,obj){
				if(target.hasClass('off')){
					alert('关闭了5');
				}
			}
		}
	});

	//上传图片
	$('.picList').on('click','.addPic',function(){
		var self = $(this),pDom = self.parent();

		var html = new Array();
		html.push('<div class="upload-pic fl">');
		html.push('<i class="icon-remove">');
		html.push('</i>');
		html.push('<img src="" alt=""/>');
		html.push('</div>');

		$(this).before(html.join(''));
	});
	//删除图片
	$('.picList').on('click','.upload-pic i',function(){
		$(this).parent().remove();
	});

	//搜索框输入文本
	$('.search').on('click','i',function(){
		$(this).prev().val('');
	});

	//Tab切换
	$("section").on('click','.tab li',function(){
		var li = $(this),index = li.index();

		if(li.hasClass('active')){return;}

		li.addClass('active').siblings('li').removeClass('active');

		li.parents('section')
		.find('div:eq('+index+')')
		.removeClass('hide')
		.siblings('div').addClass('hide');
	});
});