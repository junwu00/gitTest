require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'expense');
fis.match('*', {
	release:'/expense/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});



