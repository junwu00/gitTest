requirejs.config({
    shim: {
        "jquery":[
            "css!common:widget/ui/font-3ruler/font-3ruler.css"
        ]
    }
});

require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
    'common:widget/lib/GenericFramework',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/text!'+buildView+'/expense/Templates/expenseTpl.html',
    'expense:static/Scripts/commonData',
    'common:widget/lib/chartHelper',
    "common:widget/lib/scFormTable/scFormTable-v3.0",
    'common:widget/lib/plugins/depts_users_select/js/depts_users_select'
],function($,t,UiFramework,GenericFramework,tpl,expenseTpl,commonData,chartHelper,ScFormTable,depts_users_select) {
    var ajaxUrl = commonData.ajaxUrl,
        layerPackage = UiFramework.layerPackage();

    var id = t.getUrlParam("id");

    var expense = {
        init:function (jqObj) {
            this.jqObj = jqObj;
            this.isApp = 1;
            this.getMainHtml(jqObj);
        },
        getMainHtml:function(jqObj,month,cacheData,cover,callback){
            var pThis = this;

            function commonFuc(result){
                pThis.getChartData(commonData.chartData,null,result);

                if(callback){
                    callback(result);
                }
            }

            if(cacheData){
                commonFuc(cacheData);
                return;
            }

            if(!month){
                month = "2017-03";
            }
            //layerPackage.lock_screen();
           
            t.ajaxJson({
                url:ajaxUrl.getExpenseCountUrl,
                data:{
                    "data":{
                        month:month, //2017-01
                        report_id:id
                    }
                },
                callback:function (result,sttatus) {
                    if(result.errcode !=0){
                        layerPackage.unlock_screen();//遮罩end
                        
                        layerPackage.ew_alert({
                            title:result.errmsg,
                            action_desc:'ok,我知道了',
                            top:40,
                            callback:function(){
                                var corpurl = t.getHrefParamVal('corpurl') || "";

                                if(corpurl){
                                    try{
                                        require(['common:widget/lib/GenericFramework-v2.0'],function(GenericFramework){
                                            GenericFramework.wxCloseWindow();
                                        });
                                    }catch(e){
                                        console.log(e);
                                    }

                                    return;
                                }else{
                                    window.history.go(-1);
                                }
                            }
                        });
                        return;
                    }

                    if(!cover){
                        var timeJson = t.getNowDate(result.month*1000,1);
                        pThis.year = timeJson.year;
                        pThis.month = timeJson.month;

                        result.title = parseInt(timeJson.month) + "月报销总金额";

                        //先生成主体模板
                        var dom = tpl(expenseTpl,{
                            indexHtml:1,
                            timeJson:timeJson,
                            data:result
                        });

                        jqObj.html($($.trim(dom)));

                        pThis.bindDom(jqObj);

                        pThis.saveTimestamp(pThis.year, pThis.month,result);
                    }

                    
                    pThis.cacheData = result;

                    commonFuc(result);
                }
            });
        },
        bindDom:function (jqObj) {
            var pThis = this;
            jqObj.find(".prev,.next").on("click",function (e) {
                var className = e.currentTarget.className;
                var titleObj = jqObj.find("#expenseTitle");

                var month = parseInt(pThis.month);
                var year = parseInt(pThis.year);

                if(className.indexOf("prev") >= 0){
                    month = month == 1 ? 12 : month-1;

                    if(month == 12){
                        year--;
                    }
                }else if(className.indexOf("next") >= 0){
                    month = month == 12 ? 1 : month+1;

                    if(month == 1){
                        year++;
                    }
                }

                titleObj.find(".year").text(year);
                titleObj.find(".month").text(month < 10 ? "0"+month : month);
                console.log(year,month);

                pThis.year = year;
                pThis.month = month;

                var r = pThis.getCacheTimestamp(year,month);

                var ajaxTime = year + "-" + (month < 10 ? "0"+month : month);

                pThis.getMainHtml(jqObj,ajaxTime,r,1,function (result) {
                    pThis.saveTimestamp(year, month,result);
                });

                pThis.cacheChart.splice(1,1);
                pThis.cacheFormTable = new Array();
            });

            jqObj.find("#expenseLink > span").on("click",function(){
                window.location.hash = "#expenseSecond";

                var index = $(this).index();

                pThis.getExpenseSecond(pThis.year,pThis.month,index,null,{
                    range_time:pThis.range_time
                });
            });

            //jqObj.find("#expenseLink > span:last").click(); //测试用

            var timer = 0;
            $(window).resize(function(event) {
                clearTimeout(timer);

                timer = setTimeout(function(){
                    var cacheChart = pThis.cacheChart;
                    for (var i = 0; i < cacheChart.length; i++) {
                        var c = cacheChart[i];
                        var opt = c.opt;

                        var chart = chartHelper.init(document.getElementById(c.id));

                        chartHelper.isLoading(chart,1);
                        chartHelper.setOption(chart,opt,1,1);
                        chartHelper.isLoading(chart);
                    };

                    var cacheFormTable = pThis.cacheFormTable;
                    for (var j = 0; j < cacheFormTable.length; j++) {
                        var obj = cacheFormTable[j].obj;

                        var jqObj = pThis.jqObj;
                        var h = jqObj.innerHeight() - jqObj.find("#expenseSecondTop").innerHeight()  - 15 - 44;
                        //h = h - 33 - 30 - 1;//33

                        obj.innerHeight(h);

                        cacheFormTable[j].s.init(cacheFormTable[j].params);
                    };
                },200);
            });

            $(window).on("hashchange",function(){
                var hash = window.location.hash;

                if(hash == "" && jqObj.find("#expenseThird").hasClass('slideInRight')){
                    jqObj.find("#expenseThird").removeClass('slideInRight').addClass('slideOutRight');
                    jqObj.find("#expenseLink > span:last").click();
                }else if(hash == ""){
                    jqObj.find("#expenseThird").removeClass('slideInRight').addClass('slideOutRight');
                    jqObj.find("#expenseSecond").removeClass('slideInRight').addClass('slideOutRight');
                }
            });
        },
        cacheChart:[],
        getChartData:function(chartData,callback,data){
            if(!chartData) {return;}

            //1 饼 2 柱形 3 线形
            var type = chartData.chart_type,
                id = chartData.id,
                name = chartData.name,
                pThis = this;

            var obj = type == 1 ? this.jqObj.find("#echartData") : this.jqObj.find("#lineData");
            var chart = chartHelper.init(obj[0]);

            chartHelper.isLoading(chart,1,{
                text: '加载中...',
                color: '#c23531',
                textColor: '#000000',
                maskColor: 'rgba(255, 255, 255,0.8)',
                zlevel: 0
            });

            function commonChartFuc(result){
                if(result && result.data && result.data.length == 0){
                    obj.addClass('noData');
                    return;
                }

                if(type == 1){
                    pThis.getPie(obj,chart,chartData,result,id);
                }else if(type == 2){
                    pThis.getBarOrLine(obj,chart,chartData,result,id,type);
                }

                chartHelper.isLoading(chart);

                if(callback && t.isFunction(callback)){
                    callback.apply(pThis,[result]);
                }
            }

            if(data){
                commonChartFuc(data);
                return;
            }

            t.ajaxJson({
                url:pThis.getChartDataUrl + "?time="+new Date().getTime(),
                data:{
                    "data":{
                        chart_id:id
                    }
                },
                callback:function(result,status){
                    if(result.errcode != 0){
                        obj.addClass('noData');
                        console.log("error");
                        return;
                    }

                    commonChartFuc(result);
                }
            });
        },
        /*饼图*/
        getPie:function(obj,chart,chartData,result,id){
            var pThis = this;
            //var obj = $("#chart_"+id);
            //var chart = chartHelper.init(obj[0]);

            var data = this.getPieData(chartData.row_list,result.data);

            if(!data){return;}

            var title = chartData.name || chartData.row_list[0].name;

            var params = this.getPieOption(title,data,function(params){
                var showPart = obj.next(".showPart");

                showPart.find(".txtString").text(params.name+ "(" +params.seriesName+ ")");
                showPart.find(".value").text(params.value);
                showPart.find(".percent").text("("+params.percent+"%"+")");
            },function(name,titleObj){
                chartHelper.bindPieClick(chart,name,titleObj);

                obj.parent().find(".pieTitle").append($(titleObj));
            },chartData,result);

            var opt = chartHelper.myOption("pie",params.series,1,1);

            opt = chartHelper.getOption(params,opt);
            opt = chartHelper.donutChartOption(opt,"￥"+t.formatMoney(result.sum),result.title);
            opt = chartHelper.pieAppOption(opt);

            chartHelper.setOption(chart,opt);

            this.cacheChart[0] = {
                id:obj.attr("id"),
                chart:chart,
                opt:opt
            };
        },
        getPieOption:function(name,data,tooltipCallback,legendTitleCallback,chartData,result){
            var pThis = this;
            var titleArr = new Array();
            var params =  {
                series: [{
                    name: name,
                    type: 'pie',
                    selectedOffset:0,
                    selectedMode:'single',
                    label: {
                        normal: {
                            show: true,
                            position: 'outside',
                            formatter:function(params){
                                titleArr.push({
                                    val:params.value,
                                    percent:params.percent,
                                    name:params.name,
                                    color:chartHelper.color[titleArr.length%chartHelper.color.length]
                                });

                                if(titleArr.length == data.length){
                                    pThis.getChartInfo(chartData,data,result,titleArr);
                                }

                                return params.percent+'%';
                            }
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    data: data
                }],
                tooltip:function(params,ticket,callback){
                    if(tooltipCallback && t.isFunction(tooltipCallback)){
                        tooltipCallback(params);
                    }
                }
            };

            if(this.isApp){
                params.legendTitle = function(name,obj){
                    if(legendTitleCallback && t.isFunction(legendTitleCallback)){
                        legendTitleCallback(name,obj);
                    }
                };
            }

            return params;
        },
        getPieData:function(rowList,data){
            if(!data){return;}
            var arr = new Array();
            
            for (var i = 0; i < data.length; i++) {
                arr.push({
                    name:data[i][0] || "其他",
                    value:data[i][1]
                });
            };

            return arr;
        },
        getChartInfo:function(chartData,data,result,titleArr,index){
            if(index != 1){
                var color = chartHelper.color;
                var type = chartData.chart_type;
                var id = chartData.id;

                var legend = data.legend;
                var xAxis = data.xAxis;
                var newData = new Array();
                var count = 0;

                var cacheChart = this.cacheChart;

                newData = titleArr;

                for (var j in newData) {
                    count += parseInt(newData[j].val);
                };

                var dom  = tpl(expenseTpl,{
                    data:newData,
                    count:count,
                    chartInfo:1
                });

                this.jqObj.find("#chartInfo").html($.trim(dom));
            }else{
                tpl.register('formatMoney', t.formatMoney);
                tpl.register('addNumber', this.addNumber);
                var dom  = tpl(expenseTpl,{
                    lineDetail:1,
                    data:result.data
                });
                tpl.unregister('formatMoney');
                tpl.unregister('addNumber');

                this.jqObj.find("#lineDetail").html($.trim(dom));
            }
        },
        minNum:14,
        /*柱形OR折线*/
        getBarOrLine:function(obj,chart,chartData,result,id,type){
            //var obj = $("#chart_"+id);
            //var chart = chartHelper.init(obj[0]);
            var cacheResult = t.clone(result);

            var data = this.getBarOrLineData(chartData.row_list,result.data,type,id);

            if(!data){return;}

            var legend = data.legend,xAxis = data.xAxis,series = data.series;

            for (var i = 0; i < xAxis.length; i++) {
                if(i%2 != 0){
                    xAxis[i] = "";
                }else{
                    xAxis[i] += "月";
                }
            };

            var params = this.getBarOrLineOption(legend,xAxis,series);

            var opt = chartHelper.myOption("line",params.series,1,1);

            //PC 显示
            if(!this.isApp){
                 if(chartData.name.length > 0){
                     opt.title.text = chartData.name;
                 }
                 chartHelper.setDownLoadBtn(opt);
                 //opt.toolbox.right = 300;
            }

            var len = data.sCount;
            opt.sCount = len;

            if(len > this.minNum){
                alert(1);
                opt = chartHelper.setDataZoom(opt,this.minNum,xAxis.length,len);
            }

            opt = chartHelper.getOption(params,opt);
            if(type == 2){
                opt = chartHelper.xAxisBoundaryGap(opt);
            }
            opt = chartHelper.xAxisLabel(opt);

            var indexArr = new Array();

            opt = chartHelper.showNumber(opt,type == 2 ? "all" : "noShow");
            opt = this.specialLineOption(opt);
            chartHelper.setOption(chart,opt);

            this.cacheChart[1] = {
                id:obj.attr("id"),
                chart:chart,
                opt:opt,
                isBind:1,
                data:cacheResult
            };

            this.getChartInfo(chartData,data,cacheResult,[],1);
        },  
        getBarOrLineData:function(rowList,data,type,id){
            var xAxis = new Array();
            var legend = new Array();
            var series = new Array();
            var sCount = 0;

            for (var i = 0; i < rowList.length; i++) {
                var name = rowList[i].name;
                legend.push(name);
                series[i] = {data:[],name:name,type:"line"};
            };

            for (var j = 0; j < data.length; j++) {
                var d = data[j];

                xAxis.push(d[0] || "其他");
                d.splice(0,1);

                for (var k = 0; k < d.length; k++) {
                     series[k].data.push(d[k]);
                };

                sCount += d.length;
            };

            var obj = {
                xAxis:xAxis,
                legend:legend,
                series:series,
                sCount:sCount
            };

            return obj;
        },
        getBarOrLineOption:function(legend,xAxis,series,tooltipCallback){
            var params = {
                legend:legend,
                xAxis:[{
                    data:xAxis
                }],
                series: series,
                tooltip:function(params){
                    if(!t.isFunction(tooltipCallback)){return;}
                    tooltipCallback(params);
                }
            };

            return params;
        },
        specialLineOption:function(option){
            option.legend.show = false;

            return option;
        },
        /*表格*/
        cacheFormTable:[],
        getFormTable:function(data,first_data,callback){
            var id = data.id,
                type = data.type,
                name = data.name;

            var pThis = this;
            var isUseCount = type == 2 ? 1 : 0;
            var isApp = this.isApp;

            var pageSize = 20;

            function commonTableFuc(result){
                var obj = $("#formTable");

                var jqObj = pThis.jqObj;
                var h = jqObj.innerHeight() - jqObj.find("#expenseSecondTop").innerHeight() - 15 - 44;
                //h = h - 33 - 30 - 1;//33

                obj.innerHeight(h);

                var s = new ScFormTable();
                var sortKey = "",order = 0;

                var f_x = 0,f_y = 0;
                //配置固定列
                var fixColNum = data.col_key_count;

                //总数据量
                var sum = result.sum;
                var page_length = result.page_length;
                var endLoad = 1;

                var thead = result.data.thead;
                var tbody = result.data.tbody;
                var tfoot = result.data.tfoot;

                var params = {
                    target:obj,
                    data:{
                        thead:thead,
                        tbody:tbody,
                        tfoot:tfoot
                    },
                    base:{
                        isApp:isApp,
                        fixColNum:fixColNum,
                        //hideThead:1,
                        //maxTdWidth:150,
                        //minTdWidth:120,
                        showScrollbars:pThis.showScrollbars,
                        checkBox:{
                            //state:1,
                            className:"aaa11"
                        },
                        //limitTdWidth:0
                        //theadEvent:0
                        //autoResize:"no",
                        //mergeTable:0
                    },
                    css:{
                        hasBorder:1,
                        /*
                        theadThClass:"t1", //仅局限于thead 全部th 的样式
                        tfootThClass:"t2", //仅局限于thead 全部th 的样式
                        tbodyTdClass:"t3",

                        tbodyTrClass:"tbodyTrClass",
                        */

                        //noOddEvenClass:1
                    },
                    callback:{
                        theadClick:function(o,name,sort,key,self){
                            //console.log(name,sort,key,self);
                            //添加新数据
                            //self.addTbodyData($("#test"),res.sc3.tbody2);

                            pThis.setFormTableData(id,isUseCount,0,pageSize,function(res){
                                if(res.data.length == 0){
                                    return;
                                }

                                s.restartTbody(obj,res.data);

                                s.restartPageTools({
                                    index:1
                                });

                                //自适应用。可去掉
                                for (var i = 0; i < pThis.cacheFormTable.length; i++) {
                                    var ft = pThis.cacheFormTable[i];
                                    if(ft.id == "formTable_"+id){
                                        ft.data = res;
                                        break;
                                    }
                                };
                            },sortKey,order);
                        },
                        getScrollxy:function(x,y,target,self){
                               
                            var mTable = target.find(".mTable:first");
                            var wrapper = target.find("#wrapper");
                            var trLen = mTable.find("tbody tr").length;

                           // console.log(trLen);
                            var c = mTable.height() - wrapper.height();
                            //滚动到底部
                            if(y == c && page_length < sum && endLoad){
                                //console.log("====");
                                endLoad = 0;
                                pThis.setFormTableData(id,isUseCount,trLen,pageSize,function(res){
                                    var data = res.data;
                                    data = pThis.tableDataFormat(data);
                                    self.addTbodyData(obj,data.tbody);

                                    //自适应数据保存
                                    for (var i = 0; i < pThis.cacheFormTable.length; i++) {
                                        var ft = pThis.cacheFormTable[i];
                                        if(ft.id == "formTable"){
                                            for (var j = 0; j < data.tbody.length; j++) {
                                                ft.data.data.tbody.push(data.tbody[j]);
                                            };

                                            page_length +=  res.page_length;
                                            ft.data.page_length = page_length;
                                            
                                            break;
                                        }
                                    };

                                    pThis.setFormTablePageMsg(obj,page_length,sum);

                                    endLoad = 1;
                                });
                            }
                        },
                        initCallBack:function(){
                            var pageLengthT = result.page_length;
                            if(pageLengthT < page_length){
                                pageLengthT = page_length;
                            }
                            pThis.setFormTablePageMsg(obj,pageLengthT,sum);
                        },
                        checkBoxClick:function(data,theadHeight){
                            console.log(data,theadHeight);  
                        }
                    }
                };

                //自适应用。可去掉
                pThis.cacheFormTable[0] = {
                    id:obj.attr("id"),
                    obj:obj,
                    data:result,
                    s:s,
                    params:params,
                    page_length : result.page_length
                };

                s.init(params);
            }

            if(first_data){
                commonTableFuc(first_data);
                return;
            }

            this.setFormTableData(id,isUseCount,null,pageSize,function(result){
                commonTableFuc(result);
            });
        },
        setFormTableData:function(id,count,page,pageSize,callback,sortKey,order){
            var pThis = this;
            if(pageSize === undefined || pageSize == null){pageSize = 15;}
            if(page === undefined || page == null){page = 0;}
            var pThis = this;
            if(this.isApp == 1){
                layerPackage.lock_screen(); 
            }else{
                $('.showPageInfo').children('p:eq(0)').addClass('d-n');
                $('.showPageInfo').children('p:eq(1)').removeClass('d-n');
            }
            t.ajaxJson({
                url:ajaxUrl.getFormTableUrl,
                data:{
                    "data":{
                        report_id:id,
                        count:count,
                        start:page,
                        page_size:pageSize,
                        key:sortKey,
                        order:order,
                        dept_id:pThis.cacheDept[0],
                        range_time:pThis.range_time
                    }
                },
                callback:function(result,status){
                    if(pThis.isApp == 1){
                        layerPackage.unlock_screen();   
                    }else{
                        $('.showPageInfo').children('p:eq(1)').addClass('d-n');
                        $('.showPageInfo').children('p:eq(0)').removeClass('d-n');
                    }
                    if(result.errcode != 0){
                        console.log("error");
                        return;
                    }

                    callback(result);
                }
            });
        },
        //分页记录处理
        setFormTablePageMsg:function(obj,pageLength,sum){
            var hSpan = obj.next(".showPageInfo").children('span');
            obj.next(".showPageInfo").html("<p>第1条~第<span>"+pageLength+"</span>记录,总共"+sum+"条</p><p class='loading-notice d-n'>加载中，请稍候...</p>");
        },
        //数据格式换
        tableDataFormat:function(data){
            var tmpData = $.extend(true,{},data);
            //找出需要标识千分位的下标head
            var changeIndexs = [];
            
            var lastHead = tmpData.thead[tmpData.thead.length -1].data;
            for ( var i in lastHead) {
                if(lastHead[i].type && lastHead[i].type == 'number'){
                    changeIndexs.push(i)
                }
            }
            
            //修改千分位body,foot
            for ( var i in tmpData.tbody) {
                var row = tmpData.tbody[i].data;
                for ( var j in changeIndexs) {
                    var v = row[changeIndexs[j]].val;
                    v = t.formatMoney(v);
                    row[changeIndexs[j]].val = v;
                }
            }
            
            for ( var i in tmpData.tfoot) {
                var row = tmpData.tfoot[i].data;
                for ( var j in changeIndexs) {
                    var v = row[changeIndexs[j]].val;
                    v = t.formatMoney(v);
                    row[changeIndexs[j]].val = v;
                }
            }
            
            return tmpData;
        },
        cacheTimestamp:[],
        saveTimestamp:function(year,month,result){
            var flag = false;
            for (var i = 0; i < this.cacheTimestamp.length; i++) {
                if(this.cacheTimestamp[i].year == year && this.cacheTimestamp[i].year == month){
                    flag = true;
                    break;
                }
            };

            if(!flag){
                this.cacheTimestamp.push({
                    year:year,
                    month:month,
                    result:result
                });
            }
        },
        getCacheTimestamp:function(year,month){
            for (var i = 0; i < this.cacheTimestamp.length; i++) {
                if(this.cacheTimestamp[i].year == year && this.cacheTimestamp[i].month == month){
                    return this.cacheTimestamp[i].result;
                }
            };

            return null;
        },
        getExpenseSecond:function(year,month,index,noAnimate,otherData,callback){
            var pThis = this;
            var expenseSecond = this.jqObj.find("#expenseSecond");

            function commonFuc(index,result,deptId){
                if(!noAnimate){
                    var dom = tpl(expenseTpl,{
                        expenseSecond:1,
                        index:index,
                        data:result
                    });

                    expenseSecond.html($($.trim(dom)));
                    pThis.bindExpenseSecondDom(expenseSecond,index,deptId);
                }

                if(index == 0){
                    try{
                        var count = 0;
                        for (var i = 0; i < result.data.length; i++) {
                            count += parseInt(result.data[i][1]);
                        };
                        pThis.jqObj.find(".lineDataCount").text(count);
                    }catch(e){
                        console.log(e);
                    }

                    pThis.getChartData(commonData.lineData,null,result);
                }else if(index ==  1){
                    pThis.getFormTable({
                        col_key_count: 1,
                        id: "3",
                        name: "汇总表",
                        type: "2",
                    }, result);
                }

                if(!noAnimate){
                    expenseSecond.removeClass('slideOutRight').addClass('slideInRight');
                }

                if(callback && t.isFunction(callback)){
                    callback();
                }
            }

            if(index == 0 && pThis.cacheChart[1]){
                commonFuc(index,pThis.cacheChart[1].data);
                return;
            }else if(index == 1 && pThis.cacheFormTable[0]){
                commonFuc(index,pThis.cacheFormTable[0].data,pThis.cacheDept[0]);
                return;
            }

            var params = {
                year:year,
                month:month,
                report_id:id
            };

            if(index == 1){
                var h = this.jqObj.innerHeight() - 158 - 15 - 44;
                var pageSize = Math.ceil(h / 33);

                params.page_size = pageSize;
                params.start = 0;
            }

            var url = index == 0 ? ajaxUrl.getLineCountUrl : ajaxUrl.getFormTableUrl;

            if(otherData){
                otherData.year = year;
                otherData.month = month;
                otherData.report_id = id;

                if(params.page_size){
                    otherData.page_size = params.page_size;
                    otherData.start = params.start;
                }

                params = otherData;
            }

            t.ajaxJson({
                url:url,
                data:{
                    "data":params
                },
                callback:function(result,status){
                    if(result.errcode !=0){
                        layerPackage.unlock_screen();//遮罩end
                        layerPackage.fail_screen(result.errmsg);
                        return;
                    }

                    if(index == 0){
                        commonFuc(index,result);
                    }else if(index ==  1){
                        var dept_id = result.info.dept_id;
                        pThis.cacheDept.push(dept_id);
                        commonFuc(index,result.info.table,dept_id);
                    }

                }
            });
        },
        range_time:1,
        bindExpenseSecondDom:function(obj,index,deptId){
            var pThis = this;
            if(index == 0){
                obj.find("select").on("change",function(){
                    var year = $(this).val();
                    pThis.cacheChart.splice(1,1);
                    pThis.getExpenseSecond(year,pThis.month,index,1);
                });
            }else if(index == 1){
                var dept = {"dept":[]};

                if(deptId){
                    dept.dept.push(deptId);
                }

                if(this.cacheDept.length > 0){
                    dept.dept = this.cacheDept;
                }
                this.initUserDept(obj.find(".selectPartCover"),dept,1,1,function(result){
                    if(result.length == 0){
                        return;
                    }
                    pThis.cacheFormTable = new Array();
                    pThis.getExpenseSecond(pThis.year,pThis.month,index,1,{
                        dept_id:pThis.cacheDept[0],
                        range_time :  pThis.range_time
                    },function(){
                        obj.find(".selectPart .deptName").text(result[0].name);
                    });
                },function(result){
                    obj.find(".selectPart .deptName").text(result[0].name);
                });

                obj.find(".selectTime > span").off().on("click",function(){
                    var o = $(this);
                    var id = o.attr("data-id");
                    var range_time = "";

                    if(o.hasClass('current')){
                       /* o.removeClass('current');
                        range_time = 0;*/
                        return;
                    }else{
                        o.addClass('current');
                        o.siblings().removeClass('current');
                        range_time = id;
                    }

                    pThis.range_time = range_time;

                    pThis.cacheFormTable = new Array();
                    pThis.getExpenseSecond(pThis.year,pThis.month,index,1,{
                        dept_id:pThis.cacheDept[0],
                        range_time :  range_time
                    });
                });
                
                //跳转第三层
               /* obj.find(".selectPart").on("click",function(){
                    console.log(1);
                    //window.location.hash = "#expenseThird";
                    var expenseThird = pThis.jqObj.find("#expenseThird");

                    var dom = tpl(expenseTpl,{
                        expenseThird:1
                    });

                    expenseThird.html($($.trim(dom)));
                    pThis.bindExpenseThirdDom(expenseThird);

                    expenseThird.removeClass('slideOutRight').addClass('slideInRight');
                });*/
            }
        },
        cacheDept:[],
        bindExpenseThirdDom:function(obj){
            var dept = {"dept":[]},pThis = this;

            if(this.cacheDept.length > 0){
                dept.dept = this.cacheDept;
            }

            var selectObj = this.initUserDept(obj.find(".initDept"),dept,1);

            obj.off().on("click",".selectObj .dept",function(){
                var id = $(this).attr("data-id");
                for (var i = 0; i < pThis.cacheDept.length; i++) {
                    if(pThis.cacheDept[i] == id){
                        pThis.cacheDept.splice(i,1);
                        $(this).remove();

                        pThis.bindExpenseThirdDom(obj);
                        break;
                    }
                };
            });

            obj.off().on("click","button#getNewFromTable",function(){
                var startTime = obj.find("#startTime");
                var endTime = obj.find("#endTime");

                pThis.cacheFormTable = new Array();
                pThis.jqObj.find("#expenseThird").removeClass('slideInRight').addClass('slideOutRight');
                pThis.getExpenseSecond(pThis.year,pThis.month,1,1,{
                    startTime:startTime,
                    endTime:endTime,
                    dept:pThis.cacheDept
                });
            });
        },
        initUserDept:function (target,select,canEdit,max_select,callback,init_callback){
            if(!t.checkJqObj(target)){alert("error");return;}

            var pThis = this,
                base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
                dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
                user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
                user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
                init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
                get_common_url = "/wx/index.php?model=index&m=index&a=get_recent_user_depts",
                save_common_url = "/wx/index.php?model=index&m=index&a=save_recent_user_depts";

            var opt = {};
            //opt.target = target;
            //opt.select = select;
            opt.target = target;
            opt.select=select;//{'user':select} || {}; //{'user':['80237','80225']};
            if(select.user){
                opt.type = 2;
            }else if(select.dept){
                opt.type = 1;
            }
            opt.max_select = max_select || 0;
            opt.base_dept_url = base_dept_url;
            opt.dept_url = dept_url;
            opt.user_url = user_url;
            opt.user_detail = user_detail;
            opt.init_selected_url = init_selected_url;
            opt.get_common_url = get_common_url;
            opt.save_common_url = save_common_url;
            opt.canEdit = canEdit;
            //ob.get_depts_users_ids()
            //ob.get_depts_users()

            opt.init_call_back = function(that){
                if(init_callback && t.isFunction(init_callback)){
                    init_callback(that.get_depts_users());
                }
            };

            opt.ret_fun = function(that){
                var result = that.get_depts_users();
                pThis.cacheDept = that.get_depts_users_ids();
                console.log(result);

                if(callback && t.isFunction(callback)){
                    callback(result);
                }

                /*target.parent().find(".dept").remove();

                for (var i = 0; i < result.length; i++) {
                    var r =  result[i];
                    target.before("<span class='dept fs12 fsn mr10 mb5 d-ib' data-id='"+r.id+"' data-name='"+r.name+"'>"+r.name+"<i class='rulericon-cancel_border_bold c-9b9 ml5'></i></span>");
                };*/
            };

            var dept = new depts_users_select().init(opt);

            //if(!canEdit){dept.target.off('click');}

            return dept;
        },
        addNumber:function(num,addNum){
            if(!addNum){
                addNum = 1;
            }

            return parseInt(num) + addNum;
        }
    };

    expense.init($("#main-container"));
});