require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'index');
fis.match('*', {
	release:'/index/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});