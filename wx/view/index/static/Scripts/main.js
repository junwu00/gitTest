require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/pageManager',
	'index:static/Scripts/list',
	//"index:static/Scripts/lib/swiper.jquery.min",
	"index:static/Scripts/lib/idangerous.swiper.min"
],function($,t,UiFramework,tpl,commonTpl,commonData,pageManager,l,Swiper) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;//专门处理请求URL

	//jqObj对象
	var container = $("#main-container"),
		view = container.find(".view-container"),
		vLis = container.find(".view-container > li"),//view.find("li"),
		footer = container.find("footer"),
		fLis = footer.find("ul.baseBtn li"),
		nFirstDom = view.find("li.nFirst"),
		nSecondDom = view.find("li.nSecond"),
		mFirstDom = view.find("li.mFirst"),
		mSecondDom = view.find("li.mSecond"),
		pFirstDom = view.find("li.pFirst"),
		pSecondDom = view.find("li.pSecond");

	var menuType = t.getUrlParam("menuType") || 1;
	var menuNum = menuType - 1;
	vLis.removeClass('pageOld pageInt pageNew');
	if(menuType == 1){
		vLis.eq(menuNum).addClass('pageInt').siblings().addClass('pageNew');
	}else if (menuType > 1 && menuType < vLis.length){
		vLis.eq(menuNum).addClass('pageInt');
		view.find("li:lt("+menuNum+")").addClass('pageOld');
		view.find("li:gt("+menuNum+")").addClass('pageNew');
		//vLis.eq(0).addClass('pageOld');vLis.eq(1).addClass('pageInt');vLis.eq(2).addClass('pageNew');
	}else if(menuType == vLis.length){
		vLis.eq(menuType - 1).addClass('pageInt').siblings().addClass('pageOld');
	}

	fLis.eq(menuType - 1 ).addClass('current').siblings().removeClass('current');

	container.removeClass('hide');

    //.slideSlow 慢速滑动，一定是左边的页面
    //.slideFast 快速滑动，一定是右边的页面
    //.slideSlowBack 返回时，慢速滑动，一定是左边的页面
    //.slideFastBack 返回时，快速滑动，一定是右边的页面
    //.pageInt  初始页面位置
    //.pageOld  正向划过页面位置 -100%
    //.pageNew  正向未滑页面位置 +100%

    //记录滑动过的页面
    var currentNum = menuNum;//0

    fLis.on("click",function(){   	
    	var index = $(this).index(),num = index - currentNum;

    	//当前页面
    	if(num == 0){return;}

    	var fCurrent = 0;
    	if(index > 2){
    		if(index == 3 || index == 4){
    			fCurrent = 0;
    		}else if(index == 5 || index == 6){
    			fCurrent = 2;
    		}else if(index == 7 || index == 8){
    			fCurrent = 1;
    		}
    	}else{
    		fCurrent = index;
    	}

    	if(fCurrent == 1 && index == 1){
    		processList.geDealNumber();
    	}

    	if(index < 3){
    		commonFuc.changeWindowTitle(commonData.version);
    	}
    	
    	var pageStack = pageManager.getPageStack();
 		//console.log(pageStack);
    	if(index < 3 && pageStack.length > 1 && !vLis.eq(fCurrent).hasClass('pageInt')){
    		pageManager.clearOtherPageStack();

    		if(footer.find("li.current").index() == index){
    			//window.history.go(-1);
    			//return;
    		}else{
    			if(pageStack.length > 1){
    				pageStack.pop();
    			}
    			
    		}

    		var activekey =  t.getUrlParam("activekey");

    		if(activekey === undefined || activekey == null){
    			//alert(JSON.stringify(window.history));

    			try{
    				if(WeixinJSBridge){
    					window.history.go(-2);
    				}
    			}catch(e){
    				console.log(e);
			    	console.log(document.title);
    				console.log(window.location.hash);
    				window.history.go(-1);
    			}

				/*window.addEventListener("popstate", function(e) { 
    				window.history.pushState({
    					title:"流程审批2.0",
    					url:"#"
    				}, "流程审批2.0", "#"); 
				}, false); */
    		}
    	}

    	fLis.eq(fCurrent).addClass('current').siblings().removeClass('current');

		var currentPage = view.find("li.page-container:eq("+currentNum+")");
		var clickPage = view.find("li.page-container:eq("+index+")");

		//currentPage.removeClass('slideSlow slideFast slideSlowBack slideFastBack');
		//clickPage.removeClass('slideSlow slideFast slideSlowBack slideFastBack');
		vLis.removeClass('slideSlow slideFast slideSlowBack slideFastBack');

    	//向前
    	if(num > 0){
    		currentPage.removeClass("pageInt pageNew").addClass('pageOld slideSlow');
    		clickPage.addClass('pageInt slideFast').removeClass("pageOld pageNew");
    	}
    	//后退
    	else if(num < 0){
    		currentPage.removeClass("pageInt pageOld").addClass('pageNew slideFastBack');
    		clickPage.addClass('pageInt slideSlowBack').removeClass("pageOld pageNew");
    	}

    	if(num > 1 || num < -1){
    		vLis.each(function(i, el) {
				if(i < index && i > currentNum){
					$(this).addClass('pageOld').removeClass('pageNew');
				}else if(i > index && i < currentNum){
					$(this).addClass('pageNew').removeClass('pageOld');
				}
			});
    	}
		
    	currentNum = index;
    	/*if(currentNum != 3){
    		view.find("li.nFirst")
	    	.removeClass("pageInt pageNew pageOld")
	    	.removeClass('slideSlow slideFast slideSlowBack slideFastBack')
	    	.addClass('pageNew');

	    	vLis.eq(0).find("section.clickBg").removeClass('clickBg');
    	}*/
    	vLis.eq(index).find("section.clickBg").removeClass('clickBg');

    	if(index < 3){
    		window.location.hash = "";

			/*document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
				WeixinJSBridge.call('hideToolbar');
				WeixinJSBridge.call('hideOptionMenu');
			});*/
    	}else{
    		/*document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
				WeixinJSBridge.call('showToolbar');
				WeixinJSBridge.call('showOptionMenu');
			});*/
    	}
    });

	//处理数据返回信息
	var returnMsg = {
		error:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.fail_screen(result.errmsg);
		},
		success:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.success_screen(result.errmsg);
		}
	};


	var has_new_msg = false;				//是否有消息提醒
				
	var commonFuc = {
		addClickBg:function(jqObj){
			jqObj.addClass('clickBg');
		},
		removeClickBg:function(jqObj){
			jqObj.removeClass('clickBg');
		},
		changeWindowTitle:function(name){
			document.title = name;

			/*const mobile = navigator.userAgent.toLowerCase();
			const length = document.querySelectorAll('iframe').length;
			if (/iphone|ipad|ipod/.test(mobile) && !length) {
			  const iframe = document.createElement('iframe');
			  iframe.style.cssText = 'display: none; width: 0; height: 0;';
			  iframe.setAttribute('src', 'about:blank');
			  iframe.addEventListener('load', () => {
			    setTimeout(() => {
			      iframe.removeEventListener('load', false);
			      document.body.removeChild(iframe);
			    }, 0);
			  });
			  document.body.appendChild(iframe);
			}*/
		},
		polling_url:"",//获取消息的请求地址
		getPollingUrl:function(callback){
			//if(!getConsumeUrl){return;}

			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getPollingUrl,
				callback:function(resp,status){
					if (resp.errcode == 0 && resp.url) {
						pThis.polling_url = resp.url;

						if (callback && t.isFunction(callback)) {
							callback(pThis.polling_url);
						}			
					} else {	//失败，5秒后重试
						setTimeout(function() {
							pThis.getPollingUrl(callback);
						}, 5000);
					}

					return;
					if (resp.data.length > 0) {
						pThis.polling_url = resp.data.url;

						if (callback && t.isFunction(callback)) {
							callback(pThis.polling_url);
						}			
					} else {	//失败，5秒后重试
						setTimeout(function() {
							pThis.getPollingUrl(callback);
						}, 5000);
					}
				}
			});
		},
		//接收消息通知
		/*
		array(
		   'status' 	=> 1,				//状态码
		   'desc' 		=> '',				//状态描述
		   'data' 		=> array(			//数据体
		       'msg_id' 	=> '',			//消息的唯一标识
		       'title' 	=> '',				//消息标题
		       'url'	=> ''				//目标地址
		       'detail' 	=> '',			//消息详述
		       'ajax_url' 	=> '',			//需要发送异步请求的ajax链接
		       'type'		=> 1,			//消息状态：1-正常、2-删除，后端默认为1
		       'time' 		=> 1448880640,	//消息发送时间戳
			   'ident' => '',				//自定义标识：默认为空 1-扫描成功，2-微信端确认登录, 3-退出登录（后到后得），4-权限变更
		   ),
		);

		状态码：
		0 - 默认错误				//终止轮询
		1 - 成功					//继续轮询
		-2 - 未知错误			//终止轮询
		-3 - 签名无效			//终止轮询
		-4 - 参数错误			//终止轮询
		-5 - 令牌过期			//重新获取链接后轮询
		-6 - 没有超级权限		//终止轮询
		-7 - 没有找到指定标识	//终止轮询
		-8 - 指定标识已有订阅者	//终止轮询
		*/
		system_msg_listener:null, //监听消息变更
		system_msg_responsed:false, //是否响应了消息
		background_msg_queue:[], //所有后台待处理的消息（无序，需要全部取回后按时间升序排列后再处理）
		system_get_new_msg:function (is_first) {
			var pThis = this;

			is_first = typeof(is_first) === 'undefined' ? true : is_first; //true：第一次取；false：第一次取不完之后的取操作
			if (is_first) this.background_msg_queue = []; //第一次取，清空上次保存的内容

			//title_warn(true);

			var queue_length = 100; //每次取消息的上限数量
			var wait_time = 5000; //若当前数量刚好等于queue_length，下一页则为空，取数据会超时，该变量限定该情况下的等待时长（5秒）
			this.system_msg_responsed = false;
			

			clearTimeout(this.system_msg_listener);
			if (!is_first) {
				this.system_msg_listener = setTimeout(function(is_first) {
					if (!pThis.system_msg_responsed) { //超过时间还未响应,展示已接收到的消息，并清空队列
						pThis.system_get_new_msg_complete();
						pThis.background_msg_queue = [];
					}
				}, wait_time);
			}

			var pollingOpt = {
				cycle: false, //不自动查询
				//		timeout			: is_first ? 30000 : wait_time,				//非第一次查询，超时时间缩短为5秒
				statusTimeout: function(url, time, opts) { //超时，展示已取到的数据，并重新发起轮询
					pThis.system_get_new_msg_complete();
					pThis.system_get_new_msg(true);
				},
				success: function(data) {
					pThis.system_msg_responsed = true;

					try {
						data = $.parseJSON(data);
					} catch (e) { //解析失败
						pThis.system_get_new_msg_complete(); //展示已取到的数据，终止轮询
						setTimeout(function() {
							pThis.system_get_new_msg(true);
						}, 2000);
						return false;
					}

					//解析消息数据
					var lists = data.data || [];

					for (var i in lists) {
						try {
							var detail = $.parseJSON(lists[i]);
							var msg_exists = $('.newList > div[data-msgid="' + detail.msg_id + '"]').length;
							if (msg_exists > 0) {
								console.log('[main]消息已显示，跳过：' + detail.msg_id);
								console.log(data);
							} else {
								pThis.background_msg_queue.push(detail); //加入到待处理队列
							}
						} catch (e) {
							continue; //解析失败，跳过
						}
					}

					data.status = Number(data.status);
					switch (data.status) {
						case 1: //成功
							if (lists.length >= queue_length) { //未取完，取下一页
								pThis.system_get_new_msg(false);

							} else { //取完，处理队列中数据，同时继续轮询
								pThis.system_get_new_msg_complete();
								pThis.system_get_new_msg(true);
							}
							break;

						case -5: //令牌过期
							pThis.system_get_new_msg_complete(); //展示已取到的数据
							pThis.getPollingUrl(function(polling_url) { //重新获取请求链接后再轮询
								pThis.system_get_new_msg(true);
							});
							break;

						case -8: //被订阅
							pThis.system_get_new_msg_complete(); //展示已取到的数据
							setTimeout(function() { //2秒后再发起轮询
								pThis.system_get_new_msg(true);
							}, 2000);
							break;

						default:
							pThis.system_get_new_msg_complete(); //展示已取到的数据，终止轮询
					}
				}
			}

			this.polling(this.polling_url, null, pollingOpt);
		},
		//轮询
		polling :function(url, time, opts) {
			opts = opts || {};
			time = time || (new Date()).toGMTString();
			var cycle = typeof(opts.cycle) === 'undefined' ? true : opts.cycle;	//成功后是否继续轮询，默认为true
			
			var pThis = this;
			var opt = {
				wait	: false,
				url		: url,
				timeout	: opts.timeout || 30000,
				success	: function(resp, status, e) {
					if (e.status == 200) {										//有新消息：显示消息；更新最后一次获取的时间（避免获取重复数据），立即发起请求
						var newTime = (new Date()).toGMTString();
						if (typeof(opts.success) === 'function') {
							newTime = opts.success(resp, status, e);
						}
						if (cycle) {
							pThis.polling(url, newTime, opts);		
						}
						
					} else if (e.status == 304) {								//服务端超时，不更新最后一次获取时间，重新发起请求
						pThis.polling(url, time, opts);
						
					} else {													//其他情况，延迟5s再请求
						setTimeout(function() {									
							pThis.polling(url, time, opts);
						}, 5000);
					}
				},
				error	: function(data, status, e) {
					//处理超时
					if (status == 'timeout' || e == 'timeout') {				//超时，再执行最后一次请求内容
						if (typeof(opts.statusTimeout) == 'function') {
							opts.statusTimeout(url, time, opts);
							
						} else {
							pThis.polling(url, time, opts);
						}
						
					} else if (data && data.status == 403) {					//已被抢占，延迟5s再请求
						if (typeof(opts.status403) == 'function') {
							opts.status403(url, time, opts);
							
						} else {
							setTimeout(function() {									
								pThis.polling(url, time, opts);
							}, 5000);
						}
						
					} else {													//其他情况，延迟5s再请求
						setTimeout(function() {									
							pThis.polling(url, time, opts);
						}, 5000);
					}
				},
				exp		: function(e) {
					setTimeout(function() {										//其他异常，延迟5s再请求
						pThis.polling(url, time, opts);
					}, 5000);
				}
			};
			

			this.get(opt);
		},
		//get请求
		get:function(opt) {
			var pThis = this;

			try {
				t.ajaxJson({
					url:opt.url,
					type : 'get',
					dataType : 'text',
					timeout	: opt.timeout || 30000,	//默认30s超时

					callback:function(data,finish,status,e){
						if(finish){
							if (typeof(opt.success) === 'function') {
								opt.success(data, status, e);
							}
						}else{
							if (typeof(opt.error) === 'function') {
								opt.error(data, status, e);
								return;
							}
							if (status != 'abort') {
								console.log('系统繁忙');
								return;
							}
						}
					}	
				}); 
			}catch (e) {
				if (typeof(opt.exp) === 'function') {
					opt.exp(e);
				} else {
					console.log('系统繁忙.');
				}
			}
		},
		system_get_new_msg_complete:function(){
			if(this.background_msg_queue.length > 0){
				//console.log(this.background_msg_queue);
				console.log(this.background_msg_queue);
				var msg_ids = new Array();
				var data = this.background_msg_queue;
				for (var i = 0; i < data.length; i++) {
					newsEvent.addNewMsgData(data[i]);
					msg_ids.push(data[i].msg_id);
				};

				t.ajaxJson({
					url:ajaxUrl.saveNewsUrl,
					data:{
						data:{
							msg_ids:msg_ids
						}
					},
					callback:function(result,status){
						if(result.errcode !=0){
							returnMsg.error(result);
							return;
						}

						//returnMsg.success(result);
					}
				});
			}
			
		},
		countNum:function(o,count){
			o.removeClass('hide second more');
			if(count == 0){
				o.addClass('hide');
			}else if(count >= 10 && count <=99){
				o.addClass('second');
			}else if(count > 99){
				o.addClass('more');
			}
			o.text(count);
		},
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
 		checkPlatform: function() { 
			if (/android/i.test(navigator.userAgent)) {  
				alert("This is Android‘browser."); //这是Android平台下浏览器 
			} 
			if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {           
				alert("This is iOS‘browser."); //这是iOS平台下浏览器 
			} 
			if (/Linux/i.test(navigator.userAgent)) {  
				alert("This is Linux‘browser."); //这是Linux平台下浏览器 
			} 
			if (/Linux/i.test(navigator.platform)) {  
				alert("This is Linux operating system."); //这是Linux操作系统平台 
			} 
			if (/MicroMessenger/i.test(navigator.userAgent)) {  
				alert("This is MicroMessenger‘browser."); //这是微信平台下浏览器 
			}

			alert(navigator.userAgent);
			alert(navigator.platform);
		}
	};

	//commonFuc.checkPlatform();
	
	//搜索相关
	var searchEvent = {
		type:0,
		init:function(container){
			this.searchPart = container.find("#searchPart"),
			this.search = container.find("li.news .search"),
			this.searchForm = this.search.find("form"),
			this.searchInput = this.searchForm.find("input"),
			this.searchRemove = this.searchForm.find(".searchRemove"),
			this.searchCancel = this.searchForm.find(".searchCancel");

			this.searchInput.val("");

			this.bindDom();

			this.listPart = container.find("#searchOneList");
			this.listSearch = this.listPart.find(".search");
			this.listSearchInput = this.listPart.find("input");
			this.listSearchRemove = this.listPart.find(".searchRemove");
			this.listSearchCancel = this.listPart.find(".searchCancel");
			this.bindOneListDom();
		},
		bindDom:function(){
			var pThis = this;
			this.searchInput.on("focus",function(){
				pThis.searchPart.removeClass('hide');
				pThis.search.addClass('ing');
				pThis.searchCancel.removeClass('hide');

				container.addClass('noBottom');
				footer.addClass("hide");
			});

			this.searchInput.on("keyup",function(){
				var key = $.trim($(this).val());

				if(key == ""){
					pThis.searchRemove.addClass('hide');
					pThis.searching();
				}else{
					pThis.searchRemove.removeClass('hide');
				}
			});

			this.searchRemove.on("click",function(){
				pThis.searchInput.val("");
				$(this).addClass('hide');
				pThis.searching();
			});

			this.searchCancel.on("click",function(){
				pThis.searchPart.addClass('hide')
				pThis.search.removeClass('ing');
				$(this).addClass('hide');
				pThis.searchRemove.addClass('hide');
				pThis.searchInput.val("");
				pThis.searching();

				container.removeClass('noBottom');
				footer.removeClass("hide");
			});

			this.searchForm.on('submit',function(e) {
				 e.preventDefault();

				var key = $.trim(pThis.searchInput.val());
				if(key == ""){
					pThis.searching();
					return;
				}

				pThis.searchPart.find(".f1").addClass('hide');
				pThis.searchPart.find(".f2").remove();

				pThis.searchAjax({
					key_word:key
				},function(data){
					if(data.length == 0){
						pThis.searchPart.find(".f1:first").removeClass('hide').text("暂时没有内容"); 
						return;
					}

					pThis.searchPart.addClass('bg-gray');
					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.searchPart.append(o);
					pThis.bindSearchDom();
				});
			});	
		},
		searching:function(){
			this.searchPart.removeClass('bg-gray');
			this.searchPart.find(".f1").removeClass('hide');
			this.searchPart.find(".f2").remove();
			this.searchPart.find(".f1:first").removeClass('hide').text("搜索更多内容");
		},
		//搜索标记
		searchMark:function(title){
			var key = $.trim(searchEvent.searchInput.val());
			if(key == ""){return;}

			var reg = new RegExp(key,"g");

			return title.replace(reg, '<span class="searchMark">'+key+'</span>');
		},
		searchAjax:function(params,callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.searchNewsUrl,
				data:{
					data:params
				},
				callback:function(result,status){
					console.log(result);
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}

					var data = result.data;

					if(callback && t.isFunction(callback)){
						callback(data);
					}
				}
			});
		},
		bindSearchDom:function(){
			var pThis = this;
			this.searchPart.find(".findMore").on('click',function(){
				var p = $(this).parent();
				var type = p.attr("data-type"),
					appId = p.attr("data-appid");

				var key = $.trim(pThis.searchInput.val());
				if(key == ""){
					pThis.searching();
					return;
				}
				
				pThis.searchParam = {
					key : key,
					page : 1,
					type : type,
					appId : appId,
					pageSize:20,
					nextPage : true
				};

				pThis.searchAjax({
					key_word:key,
					type:type,
					app_id:appId,
					page:1,
					page_size:pThis.searchParam.pageSize
				},function(data){
					console.log(data);

					pThis.listSearch.addClass('ing');
					if($.trim(pThis.searchParam.key) != ""){
						pThis.listSearchRemove.removeClass('hide');
					}
					pThis.listSearchCancel.removeClass('hide');

					pThis.listSearchInput.val(pThis.searchParam.key);
					pThis.listSearchInput.keyup();

					pThis.listPart.removeClass("hide");
					pThis.listPart.find(".f1:first").addClass('hide'); 
					if(data.length == 0){
						pThis.listPart.find(".f1:first").removeClass('hide'); 
						pThis.listPart.find(".dataList").html("");
						return;
					}

					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						hideBar:true
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.listPart.find(".dataList").html(o);

					if(data[0].list && data[0].list.length == pThis.searchParam.pageSize){
						pThis.searchParam.page ++;
						pThis.searchParam.nextPage = true;
					}else{
						pThis.searchParam.nextPage = false;
					}
					
					//pThis.searchOneData = data;
				})
			});
		},
		bindOneListDom:function(){
			var pThis = this;
			var dataList = this.listPart.find(".dataList");

			this.listSearchInput.on("focus",function(){
				pThis.listSearch.addClass('ing');
				if($.trim($(this).val()) != ""){
					pThis.listSearchRemove.removeClass('hide');
				}
				pThis.listSearchCancel.removeClass('hide');
			});

			this.listSearchInput.on("keyup",function(){
				var key = $.trim($(this).val());

				if(key == ""){
					pThis.listSearchRemove.addClass('hide');
					pThis.listPart.find(".dataList").html("");
				}else{
					pThis.listSearchRemove.removeClass('hide');
				}
			});

			this.listSearchRemove.on("click",function(){
				pThis.listSearchInput.val("");
				$(this).addClass('hide');
				pThis.listPart.find(".dataList").html("");
			});

			this.listSearchCancel.on("click",function(){
				pThis.listSearch.removeClass('ing');
				$(this).addClass('hide');
				pThis.listSearchRemove.addClass('hide');
				pThis.listSearchInput.val("");
				pThis.listPart.addClass('hide');
				pThis.listPart.find(".dataList").html("");

				dataList.attr("is_scroll_loading",false);
				dataList.attr("is_scroll_end",false);

				pThis.searchCancel.click();
			});

			this.listPart.find("form").on('submit',function(e) {
				 e.preventDefault();

				var key = $.trim(pThis.listSearchInput.val());
				if(key == ""){
					pThis.listPart.find(".dataList").html("");
					return;
				}

				pThis.listPart.find(".f1").addClass('hide');

				pThis.searchAjax({
					key_word:key,
					type:pThis.searchParam.type,
					app_id:pThis.searchParam.appId,
					page:1,
					page_size:pThis.searchParam.pageSize
				},function(data){
					if(data.length == 0){
						pThis.listPart.find(".f1:first").removeClass('hide'); 
						pThis.listPart.find(".dataList").html("");
						return;
					}

					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						hideBar:true
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.listPart.find(".dataList").html(o);
				});
			});

			
			UiFramework.scrollLoadingPackage().init({
				target:dataList,
				scroll_target:dataList,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}
					if(!pThis.searchParam.nextPage){
						dataList.attr("is_scroll_loading",false);
						dataList.attr("is_scroll_end",true);
						return;
					}

					if(pThis.searchParam.page != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}

					pThis.searchAjax({
						key_word:pThis.searchParam.key,
						type:pThis.searchParam.type,
						app_id:pThis.searchParam.appId,
						page:pThis.searchParam.page,
						page_size:pThis.searchParam.pageSize
					},function(data){
						if(data.length == 0){
							dataList.attr("is_scroll_loading",false);
							dataList.attr("is_scroll_end",true);
							pThis.searchParam.nextPage = false;
							return;
						}

						tpl.register('searchMark', pThis.searchMark);
						var dom = tpl(commonTpl, {
							searchPartList:data,
							defaultFace:defaultFace,
							hideBar:true,
							getList:true
						});
						tpl.unregister('searchMark');

						var o = $($.trim(dom));

						pThis.listPart.find(".dataList > section").append(o);
						dataList.attr("is_scroll_loading",false);

						self.remove_scroll_waiting(dataList);

						pThis.searchParam.page ++;
					});
				}
			});
		}
	}
	searchEvent.init(container);

	//消息
	var newsEvent = {
		newsData:[],
		init:function(container){
			this.newList = container.find(".news .newList");

			this.getNewList();
		},
		getNewList:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getNewListUrl,
				callback:function(result,status){
					console.log(result);
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}
					var data = result.msg;
					pThis.msgData = data;

					tpl.register('getShortTime', t.getShortTime);
					var dom = tpl(commonTpl, {
						msg:data,
						defaultFace:defaultFace
					});
					tpl.unregister('getShortTime');

					var o = $($.trim(dom));

					pThis.newList.append(o);
					pThis.bindDom();

					pThis.getAllCount();

					pThis.getNewsNumber();
				}
			});
		},
		getAllCount:function(){
			var data = this.msgData,count = 0;
			for (var i = 0; i < data.length; i++) {
				count += parseInt(data[i].count);
			};

			var o = fLis.eq(0).find(".countNum");
			o.removeClass('hide');
			if(count == 0){
				o.addClass('hide');
			}else if(count >= 10 && count <=99){
				o.addClass('second');
			}else if(count > 99){
				o.addClass('more');
			}

			o.text(count);
		},
		getNewsNumber:function(){
			commonFuc.getPollingUrl(function(polling_url){
				commonFuc.system_get_new_msg(true);
			});
		},
		bindDom:function(){
			var pThis = this;
			this.newList.on("click",'section',function(e){
    			//vLis.removeClass('slideSlow slideFast slideSlowBack slideFastBack');
    			e.preventDefault();
    			commonFuc.addClickBg($(this));

				var type = $(this).attr("data-type"),
					appId = $(this).attr("data-appid");

				pThis.type = type;pThis.appId = appId;
				pThis.saveOneNewData(type,appId);

				pThis.goPage(type,appId);
			});
		},
		testLink:function(){
			window.location.href = "http://www.baidu.com";
		},
		goPage:function(type,appId){
			//移步小助手
			if(type == 0 && appId == 0){
				pageManager.go("nHelperOne");
			}
			//待办信息
			else if(type == 1 && appId == 23){
				pageManager.go("nWaitOne");
			}
			//知会信息
			else if(type == 2 && appId == 23){
				pageManager.go("nNotifyOne");
			}
			//考勤提醒
			else if(type == 0 && appId == 3){
				pageManager.go("nAttendanceOne");
			}
		},
		//信息保存
		saveOneNewData:function(type,appId){
			var flag = true;
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId){
					flag = false;
					break;
				}
			};

			if(flag){
				this.newsData.push({type:type,appId:appId,page:0,pageSize:10,data:[]});
			}
		},
		updateOneNewData:function(type,appId,data,position){
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId && data.length >0){
					this.newsData[i].page ++;
					//this.newsData[i].data.concat(data);
					//this.newsData[i].data = this.newsData[i].data.concat(data);
					for (var j = 0; j < data.length; j++) {
						if(!position){
							this.newsData[i].data.push(data[j]);
						}else{
							this.newsData[i].data.unshift(data[j]);
						}
					};
					
					return this.newsData[i];
				}
			};
		},
		getOneNewData:function(type,appId){
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId){
					return this.newsData[i];
				}
			};
		},
		getHtml: function(oneNewData, data, finish,scrollFuc) {
			function returnFinish(params){
				if(finish && t.isFunction(finish)){
					finish.apply(pThis,params);
				}
			}

			var pThis = this;
			var noReadTime = null;
			if (data.length > 0) {
				if(!scrollFuc){
					for (var i = 0; i < data.length; i++) {
						if (data[i].state == 1) {
							noReadTime = data[i].time;
							break;
						}
					};
				}else{
					for (var k = data.length - 1; k >=0; k--) {
						if (data[k].state == 1) {
							noReadTime = data[k].time;
							break;
						}
					};
				}
			}

			var dom = "",arr = new Array();

			var rt = noReadTime;

			for (var j = 0; j < data.length; j++) {
				if(j == 0 && !scrollFuc){
					rt = noReadTime;
				}else if(j == data.length - 1 && scrollFuc){
					rt = noReadTime;
				}else{
					rt = null;
				}

				tpl.register('getShortTime', t.getShortTime);
				var html = tpl(commonTpl, {
					oneNewMsg: data[j],
					oneNewData: oneNewData,
					defaultFace: defaultFace,
					noReadTime: rt,
					scrollFuc: 1
				});
				tpl.unregister('getShortTime');

				arr.push($.trim(html));
			};

			var o = $($.trim(dom));

			if (scrollFuc && t.isFunction(scrollFuc)) {
				scrollFuc.apply(pThis, [arr]);
				return;
			}

			nFirstDom.find(".swiper-wrapper").removeAttr('style');
			nFirstDom.find(".swiper-wrapper").html(arr.join(''));

			var height = nFirstDom.find(".swiper-slide > section").outerHeight() /*+ 6 + 12*/;

			if(oneNewData.type == 0 && oneNewData.appId == 0){
				/*height += 21 ;*/
			}

			//if(data.length != 0 && nFirstDom.attr("data-type") != type && nFirstDom.attr("data-appid") != appId){
			pThis.bindMsgDom(height,oneNewData);
			//}

			nFirstDom.attr("data-type", oneNewData.type);
			nFirstDom.attr("data-appid", oneNewData.appId);

			returnFinish([o]);
			return;
		},
		getOneNewMsg:function(type,appId,finish,scrollFuc){
			
			var oneNewData = this.getOneNewData(type,appId),pThis = this;

			if(oneNewData.data.length > 0 && !scrollFuc){
				this.getHtml(oneNewData,oneNewData.data,finish,scrollFuc);
				return;
			}

			this.topMsgId = oneNewData.data.length >0 ? oneNewData.data[0].id : 0;
			
			t.ajaxJson({
				url:ajaxUrl.getOneNewsUrl,
				data:{
					data:{
						type:type,
						app_id:appId,
						start_id:pThis.topMsgId,
						page_size:oneNewData.pageSize//oneNewData.pageSize
					}
				},
				callback:function(result,status){
					console.log(result);
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}

					var data = result.data;

					if(data.length != 0){
						oneNewData = pThis.updateOneNewData(type,appId,data,1);
					}

					var d = t.clone(oneNewData.data);

					if(scrollFuc && t.isFunction(scrollFuc)){
						d = data;
					}

					pThis.getHtml(oneNewData,d,finish,scrollFuc);
				}
			});
		},
		bindMsgDom:function(height,oneNewData){
			var pThis = this;

			if(this.mySwiper){
				this.mySwiper.destroy(false);
			}

			var h = nFirstDom.height();
			var len = Math.floor((h - 40) / height);
			/*alert(height);
			alert(h);
			alert(len);*/

			var type = oneNewData.type,appId = oneNewData.appId;
			var listLength = oneNewData.data.length;
			var initialSlide = listLength > 1 ? (listLength - 1) : listLength;

			var params = {
				direction: 'vertical',
				//height: height,
				freeMode:true,
				freeModeMomentumRatio:1,
				freeModeSticky:true,
				resistance:true,
				preloadImages:false,
				slidesOffsetBefore:20,
				slidesOffsetAfter:20,
				initialSlide : initialSlide,
				//autoHeight: true,
				onTouchStart: function(swiper) {
					//if(num < Math.abs(swiper.translate)){return;}
					holdPosition = 0;

					swiper.translate > 100 ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onTouchMove:function(swiper){
					swiper.translate > 100 ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onResistanceBefore: function(s, pos) {
					holdPosition = pos;
				},
				onTouchEnd: function(swiper) {
					if (swiper.translate > 100) {
						mySwiper.setWrapperTranslate(0, 100, 0)
						mySwiper.params.onlyExternal = true;
						//$('.preloader').addClass('visible');
						$('.preloader').removeClass('visible');
						pThis.getOneNewMsg(pThis.type, pThis.appId, null,function(o,position){
							//nFirstDom.find(".swiper-wrapper").prepend(o)
							mySwiper.prependSlide(o);
							
							mySwiper.slideTo(0,0,false);
							//Release interactions and set wrapper
							mySwiper.setWrapperTranslate(0, 0, 0);
							mySwiper.params.onlyExternal = false;
							//Update active slide
							mySwiper.update();
							//Hide loader
							$('.preloader').removeClass('visible');

							if(o.length == 0 && pThis.mySwiper){
								pThis.mySwiper.params.resistanceRatio = 0;
								//pThis.mySwiper.destroy(false);
								//$(".swiper-container").addClass('scrollY');
								return;
							}
						});
					}
				},
				onTransitionEnd:function(swiper){
					//if(swiper.isEnd){console.log(1);return;}
					//console.log(swiper.activeIndex);
				}
			};

			/*if(type == 0 && appId == 0 && len == 1){
				params.height = height;
				params.spaceBetween = 20;
				params.slidesOffsetAfter = 0;
			}else{
				if(height > h / len){
					params.spaceBetween = 10;
				}
				params.slidesPerView = len;
				params.slidesPerGroup = len;
			}*/

			params.height = height;
			params.spaceBetween = 20;
			params.slidesOffsetAfter = 0;

			var holdPosition = 0;
			var isLast = false;

			var params2 = {
				slidesPerView:'auto',
			    mode:'vertical',
			    //freeMode:true,
			    freeModeFluid:true,
			    momentumRatio:100,
			    watchActiveIndex: true,
			    calculateHeight:true,
			    initialSlide:initialSlide,
			    offsetPxBefore : 10,
			    offsetPxAfter : 10,
				//autoHeight: true,
				onTouchStart: function(swiper) {
					holdPosition = 0;

					holdPosition > 100 && !isLast ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onTouchMove:function(swiper){
					holdPosition > 100 && !isLast ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onResistanceBefore: function(s, pos) {
					holdPosition = pos;
				},
				onTouchEnd: function(swiper) {
					if (holdPosition > 100 && !isLast) {
						mySwiper.setWrapperTranslate(0, 100, 0)
						mySwiper.params.onlyExternal = true;
						//$('.preloader').addClass('visible');
						$('.preloader').removeClass('visible');
						pThis.getOneNewMsg(pThis.type, pThis.appId, null,function(o,position){
							//nFirstDom.find(".swiper-wrapper").prepend(o)

							for (var i = 0; i < o.length; i++) {
								mySwiper.prependSlide($($.trim(o[i])).html());
								mySwiper.updateActiveSlide(i);
							};
							
							//Release interactions and set wrapper
							mySwiper.setWrapperTranslate(0, 0, 0);
							mySwiper.params.onlyExternal = false;
							//Update active slide
							//mySwiper.updateActiveSlide(0);
							//Hide loader
							$('.preloader').removeClass('visible');

							swiper.swipeTo(o.length,0,false);

							if(o.length == 0 && pThis.mySwiper){
								swiper.params.resistance = "100%";
								isLast = true;
								//pThis.mySwiper.destroy(false);
								//$(".swiper-container").addClass('scrollY');
								return;
							}
						});
					}
				}
			};

			var mySwiper = new Swiper('.swiper-container', params2);

			this.mySwiper = mySwiper;
		},
		getHelperSystemMsg:function(data){
			function getSystemHtml(){
				var o = vLis.eq(3).find(".np2");var pThis = this;
				var new_o = o.clone();
				new_o.each(function(index, el) {
					var time = $(this).find(".time");
					time.removeClass("mt5");
					$(this).find(".title").addClass('mt7');
					$(this).find("img").addClass('mt7').removeClass('mt15');
					$(this).prepend(time);
				});

				new_o.addClass('w96p')

				vLis.eq(4).html(new_o);
			}
			
			if(nFirstDom.find(".swiper-wrapper").html() ==""){
				this.saveOneNewData(data.type,data.appId);
				this.getOneNewMsg(data.type,data.appId,function(){
					getSystemHtml();
				});
				return;
			}

			getSystemHtml();
		},
		goToFirstFloor:function(){
			fLis.eq(3).click();
		},
		backToIndex:function(index){
			this.type = null;
			this.appId = null;
			//nFirstDom.removeAttr('data-type').removeAttr('data-appid');

			if(index === undefined){index = 0;}
			fLis.eq(index).click();
			commonFuc.removeClickBg(this.newList.find("section.clickBg"));
			this.removeHelperBottom();

			commonFuc.changeWindowTitle(commonData.version);
		},
		//移步小助手
		getHelperBottom:function(type,appId){
			if(!(type == 0 && appId == 0) || nFirstDom.find(".nBottomBtns").length !=0){return;}

			var html = new Array();
			html.push('<ul class="nBottomBtns tac">');
			html.push('<li class="fl col2 pt0">');
			html.push('<i class="icon-align-justify fs12 c-gray"></i>');
			html.push('<span class="fs17">系统公告</span>');
			html.push('<span></span>');
			html.push('</li>');
			html.push('<li class="fl col2 pt0">');
			html.push('<i class="icon-align-justify fs12 c-gray"></i>');
			html.push('<span class="fs17">操作指南</span>');
			html.push('<span></span>');
			html.push('</li>');
			html.push('</ul>');

			var o = $(html.join(''));

			var pThis = this;
			o.on('click',"li:first",function(){
				pageManager.go("nHelperTwo");
			});

			o.on('click',"li:last",function(){
				//pThis.testLink();
				//personalInfo.form = "news"
				pageManager.go("nUseHelper");
			});

			footer.prepend(o);
		},
		removeHelperBottom:function(){
			footer.find("ul.nBottomBtns").remove();
		},
		goToHelperOne:function(data){
			
			var pThis = this,type,appId;
			if(this.type && this.appId){
				type = this.type;
				appId = this.appId;
			}else{
				type = data.type;
				appId = data.appId;
				this.type = type;
				this.appId = appId;
				this.saveOneNewData(type,appId);
			}

			var o = nFirstDom.find(".swiper-wrapper");

			if(o.html() == "" || (nFirstDom.attr("data-type") != type || nFirstDom.attr("data-appid") != appId)){
				o.html("");
				this.getHelperBottom(type,appId);
				this.goToFirstFloor();
				this.getOneNewMsg(type,appId,function(){
					pThis.readNewMsg();
				});
			}else{
				this.getHelperBottom(type,appId);
				this.goToFirstFloor();

				pThis.readNewMsg();
			}

			commonFuc.changeWindowTitle(data.name);
		},
		goToHelperTwo:function(data){
			footer.addClass('none');
			this.removeHelperBottom();
			container.addClass('noBottom');

			this.getHelperSystemMsg(data);
			fLis.eq(4).click();
		},
		backToHelperOne:function(){
			footer.removeClass('none');
			container.removeClass('noBottom');
			pageManager.go("nHelperOne",1);
		},
		//待办信息
		goToWaitOne:function(data){
			this.goToHelperOne(data);
		},
		//知会信息
		goToNotifyOne:function(data){
			this.goToHelperOne(data);
		},
		//考勤提醒
		goToAttendanceOne:function(data){
			this.goToHelperOne(data);
		},
		readNewMsg:function(){
			var o = this.newList.find("section[data-type="+this.type+"][data-appid="+this.appId+"]");

			var sc = o.find(".countNum"),scn = sc.text();
			var fc = fLis.eq(0).find(".countNum"),fcn = fc.text();
			var minus = fcn - scn;

			if(minus == 0){
				fc.addClass('hide');
			}
			fc.text(minus);
			sc.addClass('hide');
			sc.text(0);
		},
		addNewMsgData:function(data){
			var type = data.type,appId =data.app_id,
			d = [{
				content:data.content,
				id:data.id,
				pic:data.pic,
				state:0,
				time:data.time,
				title:data.title,
				url:data.url,
				user_id:data.user_id,
				user_pic:data.user_pic
			}];
			var oneNewData = this.updateOneNewData(type,appId,d);

			var o = this.newList.find("section[data-type="+type+"][data-appid="+appId+"]");

			var sc = o.find(".countNum"),scn = sc.text();
			scn = this.checkNumber(scn) + 1;

			this.countNum(sc,scn);

			var fc = fLis.eq(0).find(".countNum"),fcn = fc.text();
			fcn = this.checkNumber(fcn) + 1;

			this.countNum(fc,fcn);

			this.newList.prepend(o);

			o.find("time").text(t.getShortTime(data.time));
			o.find("p").text(data.title);

			if(nFirstDom.attr("data-type") == type && nFirstDom.attr("data-appid") == appId){
				//var oneNewData = this.getOneNewData(this.type,this.appId);
				this.getHtml(oneNewData, d, null,function(o){
					if(this.mySwiper){
						//this.mySwiper.appendSlide(obj);
						for (var i = 0; i < o.length; i++) {
							this.mySwiper.appendSlide($($.trim(o[i])).html());
						};
						//this.mySwiper.slideTo(nFirstDom.find(".swiper-slide").length, 1000, false);//切换到第一个slide，速度为1秒
						this.mySwiper.swipeTo(this.mySwiper.slides.length - 1, 1000, false);//切换到第一个slide，速度为1秒
					}
				}, 1);
			}
		},
		countNum:function(o,count){
			o.removeClass('hide second more');
			if(count == 0){
				o.addClass('hide');
			}else if(count >= 10 && count <=99){
				o.addClass('second');
			}else if(count > 99){
				o.addClass('more');
			}
			o.text(count);
		},
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
		goToUseHelper:function(data){
			footer.addClass('none');
			this.removeHelperBottom();
			container.addClass('noBottom');

			commonFuc.changeWindowTitle(data.name);

			var sId = data.sId;

			nSecondDom.html(personalInfo.getServiceHtml(sId));

			fLis.eq(4).click();
		}
	};
	newsEvent.init(container);

	//应用
	var processList = {
		isIndex:"&isIndex=1",
		init:function(container){
			this.o = container;

			this.getHtml();
		},
		getHtml:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getUserProcessListUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}

					var dom = tpl(commonTpl,{
						processList:result.info.data
					});

					pThis.o.find("#processList").html($($.trim(dom)));
					pThis.bindDom();
				}
			});
		},
		bindDom:function(){
			var pThis = this;
			this.o.find("#processType > a").on("click",function(){
				var id = $(this).attr("data-id");

				pThis.pId = id;

				var listObj = pFirstDom.find(".advice_list");
				listObj.find("div.wxLine").remove();
	            listObj.find("section").remove();

				if(id == "1"){
					pageManager.go("pDealList");
				}else if(id == "2"){
					pageManager.go("pMineList");
				}else if(id == "3"){
					pageManager.go("pNotifyList");
				}
			});

			this.o.find("#processList > span").on("click",function(){
				var id = $(this).attr("data-id");

				window.location.href = "/wx/index.php?model=process&a=create&debug=1&id="+ id + pThis.isIndex;
			});
		},
		getProcessHtml:function(pId){
			l.finit();
			var pThis = this;
			var listObj = $('.advice_list');
			var listUrl = "",contents = null,sortArr = null;
			var activekey = t.getHrefParamVal("activekey") || 0;

			if(pId == 1){
				listUrl = ajaxUrl.getDealListUrl;
				contents = [
					{'name':'待办','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已办','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'申请人','sort_name':'applyName','sort_id':1},
					{'name':'申请时间','sort_name':'applyTime','sort_id':2},
					{'name':'流程名称','sort_name':'proName','sort_id':3},
					{'name':'状态','sort_name':'status','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}else if(pId == 2){
				listUrl = ajaxUrl.getMineListUrl;
				contents = [
					{'name':'运行中','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已结束','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'草稿','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'流程实例名','sort_name':'proName','sort_id':1},
					{'name':'表单名','sort_name':'formName','sort_id':2}
				];
			}else if(pId == 3){
				listUrl = ajaxUrl.getNotifyListUrl;
				contents = [
					{'name':'未阅读','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已阅读','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'申请时间','sort_name':'applyTime','sort_id':1},
					{'name':'发送人','sort_name':'applyName','sort_id':2},
					{'name':'流程名','sort_name':'proName','sort_id':3},
					{'name':'表单名','sort_name':'formName','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keyworks = "",sort = 0,order = 1,listMenuObj = null;

			var getPageData = {
				init:function(activekey,keyworks,sort,order){
					if(sort === undefined){sort = 0;}
					if(order === undefined){order = 1;}
					if(!keyworks){keyworks="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}
							
							l.getList({
								"listUrl":listUrl,
								"listObj":listObj,
								"page":pThis.pageNum,
								"type":activekey,
								"sort":sort,
								"order":order,
								"pro_name":keyworks,
								"listType":pId,
								callback:{
									bindDom:function(res){
										var listType = res.listType,id = res.id;

										window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType="
										+pId+"&id="+id+"&activekey="+activekey+ pThis.isIndex;
									}
								}
							},function(data,pCount){
								layerPackage.unlock_screen();
								//数据空时显示提示
								if(!data || data.length == 0){
									listMenuObj.showRecord();
									return;
								}

								//pageCount = pCount;
								pThis.pageCount += data.length;
								//if(pageCount == pCount){UiFramework.bottomInfo.init(listObj);return;}
								
								pThis.pageNum ++;
								listObj.attr("is_scroll_loading",false);

								if(data.length < 10 || data.length == pCount || pThis.pageCount == pCount){
									listObj.attr("is_scroll_end",true);
								}

								self.remove_scroll_waiting(listObj);

								UiFramework.bottomInfo.init(listObj);
							});
						}
					});

					listObj.scroll();
				}
			};

			var setting = {
				//列表菜单配置
				target: $('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: contents,
					activekey:activekey
				},
				sort:{ //配置头部插件排序
					status:true, 
					contents:sortArr
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onMenuClick: function(data, obj,self){
						listMenuObj = self;
						akey = data.activekey;

						if(akey == 1 && pId != 2){
			            	var newSortArr = t.clone(sortArr);
			            	newSortArr.push({'name':'处理时间','sort_name':'dealTime','sort_id':5});

			            	listMenuObj.resetSortData(newSortArr);
			            }else{
			        		listMenuObj.resetSortData(sortArr);
			            }

		            	listMenuObj.setSearchValue("");

			            listMenuObj.showRecord();

						getPageData.init(akey);
					},
					onSortClick:function(data){
						sort = data.sort_id;
						if(data.sort_type === undefined || data.sort_type == "asc"){
							order = 0;
						}else if(data.sort_type == "desc"){
							order = 1;
						}
						getPageData.init(akey,keyworks,sort,order);
					},
					onSearchClick:function(key){
						keyworks = key;
						getPageData.init(akey,key,sort,order);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/process/static/Contents/images/logo_empty.png',//图片地址
					title:'暂时没有内容',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		},
		goToFirstFloor:function(){
			footer.removeClass('none');
			container.removeClass('noBottom');
			fLis.eq(7).click();
		},
		backToIndex:function(){
			//this.geDealNumber();
			fLis.eq(1).click();
		},
		goToDealList:function(data){
			commonFuc.changeWindowTitle(data.name);
			var pId = this.pId || data.pId;

			this.getProcessHtml(pId);

			this.goToFirstFloor();
		},
		goToMineList:function(data){
			this.goToDealList(data);
		},
		goToNotifyList:function(data){
			this.goToDealList(data);
		},
		geDealNumber:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getDealCountUrl,
				callback:function(result,status){
					var jqObj = pThis.o.find("#processType").find("a:eq(0)").find("i.countNum");
					if(result.errcode !=0){
						commonFuc.countNum(jqObj,0);
					}else{
						commonFuc.countNum(jqObj,result.info.count);
					}
				}
			});
		}
	};
	processList.init(vLis.eq(1));
	
	//个人信息
	var personalInfo = {
		init:function(container){
			this.o = container.find("li.personalInfo");

			this.getInfoHtml();
		},
		getInfoHtml:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getMyInfoUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}

					var info = result.info;
					pThis.info = info;
					var dom = tpl(commonTpl, {
						personalInfo:info,
						defaultFace:defaultFace
					});

					var html = $($.trim(dom));

					pThis.o.html(html);
					pThis.bindDom();
				}
			});
			
		},
		bindDom:function(){
			var pThis = this;
			this.o.on("click",".module section",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				$(this).addClass('clickBg');
				pThis.mId = id;
				
				pThis.goPage(id);
			});

			this.o.on("click",".editInfo",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				pThis.mId = id;

				pThis.goPage(id);	
			});
		},
		goPage:function(mId){
			/*产品服务支持*/
			if(mId == 1){
				pageManager.go("mProduct");
			}
			else if(mId == 2){
				pageManager.go("mVip");
			}
			else if(mId == 3){
				pageManager.go("mAdminService");
			}
			else if(mId == 4){
				pageManager.go("mEditInfo");
			}
		},
		backToIndex:function(){
			fLis.eq(2).click();

			this.o.find("section.clickBg").removeClass('clickBg');
		},
		/*产品服务支持*/
		bindProductServiceDom:function(){
			var pThis = this;
			//常见问题
			mFirstDom.find(".questions section").off().on("click",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				$(this).addClass('clickBg');

				pThis.qId = id;
				pThis.getQuestionDetail(id);
				pageManager.go('mQuestionDetail');
			});

			mFirstDom.find(".pServices section").off().on("click",function(){
				var id = $(this).attr("data-id");
				pThis.sId = id;
				//角色体验
				if(id == 1){
					pageManager.go("mRole");
				}
				/*产品案例 直接跳转*/
				else if(id == 2){
					window.location.href = "http://www.baidu.com";
					return;
				}
				//使用指引
				else if(id == 3){
					pageManager.go("mUseHelper");
				}
			});
		},
		/*常见问题*/
		getQuestionHtml:function(data){
			var html = new Array();
			html.push("<div class='questionDetail bg-white fs13'>");
			html.push('<span class="title fs18">'+data.title+'</span>');
			html.push('<p class="mt15">'+data.content+'</p>');
			html.push("</div>");
			return html.join("");
		},
		getQuestionData:function(id){
			var questions = commonData.questions;
			for (var i = 0; i < questions.length; i++) {
				if(questions[i].id == id){
					return questions[i];
				}
			};
		},
		getQuestionDetail:function(id){
			var questionsPart = mFirstDom.find(".questions").clone();
			questionsPart.find(".clickBg").removeClass('clickBg');
			questionsPart.find(".sTitle").text("你可能需要");//相关文章
			var selfPart = questionsPart.find("section[data-id="+id+"]");
			selfPart.addClass('hide');
			selfPart.next("div.wxLine").addClass('hide');

			mSecondDom
			.html(this.getQuestionHtml(this.getQuestionData(id)))
			.append(questionsPart);

			UiFramework.bottomInfo.init(mSecondDom);
		},
		bindQuestionDetailDom:function(){
			var pThis = this;
			mSecondDom.find(".questions section").off().on("click",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				var data = pThis.getQuestionData(id);
				mSecondDom.find(".questions .hide").removeClass('hide');
				$(this).addClass('hide');
				$(this).next("div.wxLine").addClass('hide');

				mSecondDom.find(".questionDetail .title").text(data.title);
				mSecondDom.find(".questionDetail p").text(data.content);
			});
		},
		/*产品服务*/
		getServiceHtml:function(sId){
			var dom = tpl(commonTpl,{
				pService:{
					id:sId
				},
				commonData:commonData
			});

			var o = $($.trim(dom));
			return o;
		},
		/*VIP*/
		bindVipDom:function(){
			var pThis = this;
			mFirstDom.find("section.aboutVip").off().on("click",function(){
				$(this).addClass('clickBg');
				pThis.vId = $(this).attr("data-id");
				pageManager.go("mVipInfo");
			});
		},
		getVipHtml:function(vId){
			var dom = tpl(commonTpl,{
				vip:{
					id:vId
				},
				commonData:commonData
			});

			var o = $($.trim(dom));
			return o;
		},
		/*个人编辑*/
		bindEditInfoDom:function(){
			var o = mFirstDom.find(".editInfoPart"),pThis = this;

			o.on("click",".editThis",function(){
				var id = $(this).attr("id");
				pThis.ePart = id;

				if(id == "gender"){
					pageManager.go("mEditGender");
				}else if(id == "phone"){
					pageManager.go("mEditPhone");
				}else if(id == "mail"){
					pageManager.go("mEditEmail");
				}
				//pageManager.go("mSecond");
			});
		},
		getEditInfoHtml:function(ePart){
			var dom = tpl(commonTpl,{
				editInfo:{
					ePart:ePart
				},
				commonData:commonData,
				info:this.info
			});

			var o = $($.trim(dom));
			return o;
		},
		bindEditDom:function(){
			var pThis = this;
			mSecondDom.find(".search input").on("keyup",function(){
				if($.trim($(this).val()) == ""){
					$(this).next("i").addClass('hide');
				}else{
					$(this).next("i").removeClass('hide');
				}
			});

			mSecondDom.find(".search i").on("click",function(){
				$(this).prev("input").val("");
				$(this).addClass("hide");
			});

			mSecondDom.find(".genderEdit").on("click","section:not(:first)",function(){
				var id = $(this).attr("data-id"),
					text = $(this).text();
				if(pThis.info.gender == id){return;}
				mSecondDom.find(".genderEdit").find("i").addClass('hide');
				$(this).find("i").removeClass('hide');
				$(this).addClass('clickBg');

				pThis.saveEditInfo({
					gender:id
				},function(result){
					returnMsg.success(result);
					pThis.info.gender = id;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#gender").find("em").text(text);
				});
			});

			mSecondDom.find(".phoneEdit").on("click",".save",function(){
				var val = mSecondDom.find(".phoneEdit").find("input").val();

				if(!t.regCheck(val,"mobile")){
					layerPackage.fail_screen("请填写正确的手机号码！");
					return;
				}

				pThis.saveEditInfo({
					mobile:val
				},function(result){
					returnMsg.success(result);
					pThis.info.mobile = val;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#phone").find("em").text($.trim(val));
				});
				//pageManager.go("mEditPhone",1);
			});

			mSecondDom.find(".mailEdit").on("click",".save",function(){
				var val = mSecondDom.find(".mailEdit").find("input").val();
				if(!t.regCheck(val,"email")){
					layerPackage.fail_screen("请填写正确的邮箱地址！");
					return;
				}
				//pageManager.go("mEditEmail",1);
				pThis.saveEditInfo({
					email:val
				},function(result){
					returnMsg.success(result);
					pThis.info.email = val;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#mail").find("em").text($.trim(val));
				});
			});
		},
		//----------
		getMineHtml:function(id){	
			if(mFirstDom.html() != ""){
				//return;
			}

			if(id === undefined){id = 1;}

			var params = {
				module:{id:id}
			};
			if(id == 1){
				params.data = commonData.questions;
			}
			if(id == 4){
				params.info = this.info;
				params.defaultFace = defaultFace;

				if(!params.info){return;}
			}

			var dom = tpl(commonTpl,params);

			var o = $($.trim(dom));

			mFirstDom.html(o);

			if(id == 1){
				this.bindProductServiceDom();
			}else if(id == 2){
				this.bindVipDom();
			}else if(id == 3){
				UiFramework.bottomInfo.init(mFirstDom);
			}else if(id == 4){
				this.bindEditInfoDom();
			}
		},
		goToFirstFloor:function(){
			footer.removeClass('none');
			container.removeClass('noBottom');
			fLis.eq(5).click();
		},
		goToSecondFloor:function(){
			footer.addClass('none');
			container.addClass('noBottom');
			fLis.eq(6).click();
		},
		//产品服务与支持
		goToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getMineHtml(mId);
			this.bindProductServiceDom();
			this.goToFirstFloor();
		},
		backToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			//this.goToFirstFloor();
			pageManager.go("mProduct",1);
		},
		//角色体验
		goToRole:function(data){
			commonFuc.changeWindowTitle(data.name);
			this.getMineHtml();

			var sId = this.sId || data.sId;

			mSecondDom.html(this.getServiceHtml(sId));
			this.goToSecondFloor();
		},
		//使用指引
		goToUseHelper:function(data){
			this.goToRole(data);
		},
		//常见问题详情
		goToQuestionDetail:function(data){
			commonFuc.changeWindowTitle(data.name);
			this.getMineHtml();

			var qId = this.qId || 1;
			this.getQuestionDetail(qId);
			this.bindQuestionDetailDom();

			this.goToSecondFloor();
		},
		//VIP
		goToVip:function(data){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getMineHtml(mId);
			
			this.goToFirstFloor();
		},
		backToVip:function(){
			pageManager.go("mVip",1);
		},
		goToVipInfo:function(data){
			commonFuc.changeWindowTitle(data.name);
			var vId = this.vId || data.vId;
			mSecondDom.html(this.getVipHtml(vId));
			this.goToSecondFloor();
		},
		//管理员服务中心
		goToAdminService:function(data){
			this.goToVip(data);
		},
		//编辑个人信息第一层
		goToEditInfo:function(data){
			this.goToVip(data);
		},
		backToEditInfo:function(){
			pageManager.go("mEditInfo",1);
		},
		//编辑性别
		goToEditGender:function(data){
			commonFuc.changeWindowTitle(data.name);

			var ePart = this.ePart || data.ePart;
			mSecondDom.html(this.getEditInfoHtml(ePart));
			this.bindEditDom();
			this.goToSecondFloor();
		},
		//编辑手机号码
		goToEditPhone:function(data){
			this.goToEditGender(data);
		},
		//编辑邮箱
		goToEditEmail:function(data){
			this.goToEditGender(data);	
		},
		saveEditInfo:function(params,callback){
			t.ajaxJson({
				url:ajaxUrl.editMyInfoUrl,
				data:{
					data:params
				},
				callback:function(result,status){
					if(result.errcode !=0){
						returnMsg.error(result);
						return;
					}

					if(callback && t.isFunction(callback)){
						callback(result);
					}
				}
			});
		}
	};
	personalInfo.init(view);

	
	//主页
	var home = {
        name: 'home',
        url: '#',
        events: {
        }
    };
    //移步小助手
    var nHelperOne = {
		name: 'nHelperOne',
        url: '#nHelperOne',
        type:1,
        floor:1,
        events: {  
        },
        data:{
        	type:0,
        	appId:0,
        	name:"移步小助手"
        },
        go: function(){
        	console.log("移步小助手第1层 go");
        	newsEvent.goToHelperOne(this.data);
        },
        back:function(){
        	console.log("移步小助手第1层 back");
        	newsEvent.backToIndex();
        }
    };
    var nHelperTwo = {
		name: 'nHelperTwo',
        url: '#nHelperTwo',
        type:1,
        floor:2,
        events: {  
        },
        data:{
        	type:0,
        	appId:0,
        	name:"移步小助手"
        },
        go: function(){
        	console.log("移步小助手第2层 go");
        	newsEvent.goToHelperTwo(this.data);
        },
        back:function(){
        	console.log("移步小助手第2层 back");
        	newsEvent.backToHelperOne();
        }
    };
    //操作指南
    var nUseHelper = {
    	name: 'nUseHelper',
        url: '#nUseHelper',
        type:1,
        floor:2,
        events: {  
        },
        data:{
        	type:0,
        	appId:0,
        	mId:1,
        	sId:3,
        	name:"操作指南"
        },
        go: function(){
        	console.log("操作指南第1层 go");
        	newsEvent.goToUseHelper(this.data);
        },
        back:function(){
        	console.log("操作指南第1层 back");
        	newsEvent.backToHelperOne(this.data);
        }
    };
    //待办信息
    var nWaitOne = {
    	name: 'nWaitOne',
        url: '#nWaitOne',
        type:2,
        floor:1,
        events: {  
        },
        data:{
        	type:1,
        	appId:23,
        	name:"待办信息"
        },
        go: function(){
        	console.log("待办信息第1层 go");
        	newsEvent.goToWaitOne(this.data);
        },
        back:function(){
        	console.log("待办信息第1层 back");
        	newsEvent.backToIndex();
        }
    };
    //知会信息
    var nNotifyOne = {
    	name: 'nNotifyOne',
        url: '#nNotifyOne',
        type:3,
        floor:1,
        events: {  
        },
        data:{
        	type:2,
        	appId:23,
        	name:"知会信息"
        },
        go: function(){
        	console.log("知会信息第1层 go");
        	newsEvent.goToNotifyOne(this.data);
        },
        back:function(){
        	console.log("知会信息第1层 back");
        	newsEvent.backToIndex();
        }
    };
    //考勤提醒
    var nAttendanceOne = {
    	name: 'nAttendanceOne',
        url: '#nAttendanceOne',
        type:4,
        floor:1,
        events: {  
        },
        data:{
        	type:0,
        	appId:3,
        	name:"考勤提醒"
        },
        go: function(){
        	console.log("考勤提醒第1层 go");
        	newsEvent.goToAttendanceOne(this.data);
        },
        back:function(){
        	console.log("考勤提醒第1层 back");
        	newsEvent.backToIndex();
        }
    };
    //产品服务与支持
    var mProduct = {
    	name: 'mProduct',
        url: '#mProduct',
        type:5,
        floor:1,
        events: {  
        },
        data:{
        	mId:1,
        	name:"产品服务与支持"
        },
        go: function(){
        	console.log("产品服务与支持第1层 go");
        	personalInfo.goToProduct(this.data);
        },
        back:function(){
        	console.log("产品服务与支持第1层 back");
        	personalInfo.backToIndex();
        }
    };
    //角色体验
    var mRole = {
    	name: 'mRole',
        url: '#mRole',
        type:5,
        floor:2,
        events: {  
        },
        data:{
        	mId:1,
        	sId:1,
        	name:"角色体验"
        },
        go: function(){
        	console.log("角色体验第1层 go");
        	personalInfo.goToRole(this.data);
        },
        back:function(){
        	console.log("角色体验第1层 back");
        	personalInfo.backToProduct(this.data);
        }
    };
	//使用指引
    var mUseHelper = {
    	name: 'mUseHelper',
        url: '#mUseHelper',
        type:5,
        floor:2,
        events: {  
        },
        data:{
        	mId:1,
        	sId:3,
        	name:"使用指引"
        },
        go: function(){
        	console.log("使用指引第1层 go");
        	personalInfo.goToUseHelper(this.data);
        },
        back:function(){
        	console.log("使用指引第1层 back");
        	personalInfo.backToProduct(this.data);
        }
    };
    //常见问题
    var mQuestionDetail = {
    	name: 'mQuestionDetail',
        url: '#mQuestionDetail',
        type:5,
        floor:3,
        events: {  
        },
        data:{
        	mId:1,
        	name:"常见问题"
        },
        go: function(){
        	console.log("常见问题详情 go");
        	personalInfo.goToQuestionDetail(this.data);
        },
        back:function(){
        	console.log("常见问题详情 back");
        	personalInfo.backToProduct(this.data);
        }
    };
    //VIP
    var mVip = {
    	name: 'mVip',
        url: '#mVip',
        type:6,
        floor:1,
        events: {  
        },
        data:{
        	mId:2,
        	name:"VIP专享服务"
        },
        go: function(){
        	console.log("VIP第1层 go");
        	personalInfo.goToVip(this.data);
        },
        back:function(){
        	console.log("VIP第1层 back");
        	personalInfo.backToIndex();
        }
    };
    //关于vip会员
    var mVipInfo = {
    	name: 'mVipInfo',
        url: '#mVipInfo',
        type:6,
        floor:2,
        events: {  
        },
        data:{
        	mId:2,
        	vId:100,
        	name:"关于vip会员"
        },
        go: function(){
        	console.log("关于vip会员 go");
        	personalInfo.goToVipInfo(this.data);
        },
        back:function(){
        	console.log("关于vip会员 back");
        	personalInfo.backToVip(this.data);
        }
    };
    //管理员服务中心
    var mAdminService = {
    	name: 'mAdminService',
        url: '#mAdminService',
        type:7,
        floor:1,
        events: {  
        },
        data:{
        	mId:3,
        	name:"管理员服务中心"
        },
        go: function(){
        	console.log("管理员服务中心第1层 go");
        	personalInfo.goToAdminService(this.data);
        },
        back:function(){
        	console.log("管理员服务中心第1层 back");
        	personalInfo.backToIndex();
        }
    };
    //编辑信息第一层
    var mEditInfo = {
    	name: 'mEditInfo',
        url: '#mEditInfo',
        type:8,
        floor:1,
        events: {  
        },
        data:{
        	mId:4,
        	name:"编辑个人信息"
        },
        go: function(){
        	console.log("编辑信息第1层 go");
        	personalInfo.goToEditInfo(this.data);
        },
        back:function(){
        	console.log("编辑信息第1层 back");
        	personalInfo.backToIndex();
        }
    };
    //编辑性别
    var mEditGender = {
    	name: 'mEditGender',
        url: '#mEditGender',
        type:8,
        floor:2,
        events: {  
        },
        data:{
        	mId:4,
        	ePart:"gender",
        	name:"编辑性别"
        },
        go: function(){
        	console.log("编辑性别 go");
        	personalInfo.goToEditGender(this.data);
        },
        back:function(){
        	console.log("编辑性别 back");
        	personalInfo.backToEditInfo();
        }
    };
    //编辑手机号码
    var mEditPhone = {
    	name: 'mEditPhone',
        url: '#mEditPhone',
        type:8,
        floor:2,
        events: {  
        },
        data:{
        	mId:4,
        	ePart:"phone",
        	name:"编辑手机号码"
        },
        go: function(){
        	console.log("编辑手机号码 go");
        	personalInfo.goToEditPhone(this.data);
        },
        back:function(){
        	console.log("编辑手机号码 back");
        	personalInfo.backToEditInfo();
        }
    };
    //编辑邮箱
    var mEditEmail = {
    	name: 'mEditEmail',
        url: '#mEditEmail',
        type:8,
        floor:2,
        events: {  
        },
        data:{
        	mId:4,
        	ePart:"mail",
        	name:"编辑邮箱"
        },
        go: function(){
        	console.log("编辑邮箱 go");
        	personalInfo.goToEditEmail(this.data);
        },
        back:function(){
        	console.log("编辑邮箱 back");
        	personalInfo.backToEditInfo();
        }
    };
    //待办/已办
    var pDealList = {
    	name: 'pDealList',
        url: '#pDealList',
        type:9,
        floor:1,
        events: {  
        },
        data:{
        	pId:1,
        	name:"待办/已办"
        },
        go: function(){
        	console.log("待办/已办 go");
        	processList.goToDealList(this.data);
        },
        back:function(){
        	console.log("待办/已办 back");
        	processList.backToIndex();
        }
    };
    //我的发起
    var pMineList = {
    	name: 'pMineList',
        url: '#pMineList',
        type:9,
        floor:1,
        events: {  
        },
        data:{
        	pId:2,
        	name:"我的发起"
        },
        go: function(){
        	console.log("我的发起 go");
        	processList.goToMineList(this.data);
        },
        back:function(){
        	console.log("我的发起 back");
        	processList.backToIndex();
        }
    };
    //我的知会
    var pNotifyList = {
    	name: 'pNotifyList',
        url: '#pNotifyList',
        type:9,
        floor:1,
        events: {  
        },
        data:{
        	pId:3,
        	name:"我的知会"
        },
        go: function(){
        	console.log("我的知会 go");
        	processList.goToNotifyList(this.data);
        },
        back:function(){
        	console.log("我的知会 back");
        	processList.backToIndex();
        }
    };


	pageManager
	.push(home)
	.push(nHelperOne)
	.push(nHelperTwo)
	.push(nUseHelper)
	.push(nWaitOne)
	.push(nNotifyOne)
	.push(nAttendanceOne)
	.push(mProduct)
	.push(mQuestionDetail)
	.push(mRole)
	.push(mUseHelper)
	.push(mVip)
	.push(mVipInfo)
	.push(mAdminService)
	.push(mEditInfo)
	.push(mEditGender)
	.push(mEditPhone)
	.push(mEditEmail)
	.push(pDealList)
	.push(pMineList)
	.push(pNotifyList)
	.default('home')
	.init();

	layerPackage.unlock_screen();
});