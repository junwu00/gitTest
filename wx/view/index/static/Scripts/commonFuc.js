define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'index:static/Scripts/commonData',
],function($,t,UiFramework,commonData) {
	var ajaxUrl = commonData.ajaxUrl,
		layerPackage = UiFramework.layerPackage();
	var commonFuc = {
		addClickBg:function(jqObj){
			jqObj.addClass('clickBg');
		},
		removeClickBg:function(jqObj){
			jqObj.removeClass('clickBg');
		},
		changeWindowTitle:function(name){
			document.title = name;

			/*const mobile = navigator.userAgent.toLowerCase();
			const length = document.querySelectorAll('iframe').length;
			if (/iphone|ipad|ipod/.test(mobile) && !length) {
			  const iframe = document.createElement('iframe');
			  iframe.style.cssText = 'display: none; width: 0; height: 0;';
			  iframe.setAttribute('src', 'about:blank');
			  iframe.addEventListener('load', () => {
			    setTimeout(() => {
			      iframe.removeEventListener('load', false);
			      document.body.removeChild(iframe);
			    }, 0);
			  });
			  document.body.appendChild(iframe);
			}*/
		},
		polling_url:"",//获取消息的请求地址
		getPollingUrl:function(callback){
			//if(!getConsumeUrl){return;}

			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getPollingUrl,
				callback:function(resp,status){
					if (resp.errcode == 0 && resp.url) {
						pThis.polling_url = resp.url;

						if (callback && t.isFunction(callback)) {
							callback(pThis.polling_url);
						}			
					} else {	//失败，5秒后重试
						setTimeout(function() {
							pThis.getPollingUrl(callback);
						}, 5000);
					}

					return;
					if (resp.data.length > 0) {
						pThis.polling_url = resp.data.url;

						if (callback && t.isFunction(callback)) {
							callback(pThis.polling_url);
						}			
					} else {	//失败，5秒后重试
						setTimeout(function() {
							pThis.getPollingUrl(callback);
						}, 5000);
					}
				}
			});
		},
		//接收消息通知
		/*
		array(
		   'status' 	=> 1,				//状态码
		   'desc' 		=> '',				//状态描述
		   'data' 		=> array(			//数据体
		       'msg_id' 	=> '',			//消息的唯一标识
		       'title' 	=> '',				//消息标题
		       'url'	=> ''				//目标地址
		       'detail' 	=> '',			//消息详述
		       'ajax_url' 	=> '',			//需要发送异步请求的ajax链接
		       'type'		=> 1,			//消息状态：1-正常、2-删除，后端默认为1
		       'time' 		=> 1448880640,	//消息发送时间戳
			   'ident' => '',				//自定义标识：默认为空 1-扫描成功，2-微信端确认登录, 3-退出登录（后到后得），4-权限变更
		   ),
		);

		状态码：
		0 - 默认错误				//终止轮询
		1 - 成功					//继续轮询
		-2 - 未知错误			//终止轮询
		-3 - 签名无效			//终止轮询
		-4 - 参数错误			//终止轮询
		-5 - 令牌过期			//重新获取链接后轮询
		-6 - 没有超级权限		//终止轮询
		-7 - 没有找到指定标识	//终止轮询
		-8 - 指定标识已有订阅者	//终止轮询
		*/
		system_msg_listener:null, //监听消息变更
		system_msg_responsed:false, //是否响应了消息
		background_msg_queue:[], //所有后台待处理的消息（无序，需要全部取回后按时间升序排列后再处理）
		system_get_new_msg:function (is_first,finish) {
			var pThis = this;

			is_first = typeof(is_first) === 'undefined' ? true : is_first; //true：第一次取；false：第一次取不完之后的取操作
			if (is_first) this.background_msg_queue = []; //第一次取，清空上次保存的内容

			//title_warn(true);

			var queue_length = 100; //每次取消息的上限数量
			var wait_time = 5000; //若当前数量刚好等于queue_length，下一页则为空，取数据会超时，该变量限定该情况下的等待时长（5秒）
			this.system_msg_responsed = false;
			

			clearTimeout(this.system_msg_listener);
			if (!is_first) {
				this.system_msg_listener = setTimeout(function(is_first) {
					if (!pThis.system_msg_responsed) { //超过时间还未响应,展示已接收到的消息，并清空队列
						pThis.system_get_new_msg_complete(finish);
						pThis.background_msg_queue = [];
					}
				}, wait_time);
			}

			var pollingOpt = {
				cycle: false, //不自动查询
				//		timeout			: is_first ? 30000 : wait_time,				//非第一次查询，超时时间缩短为5秒
				statusTimeout: function(url, time, opts) { //超时，展示已取到的数据，并重新发起轮询
					pThis.system_get_new_msg_complete(finish);
					pThis.system_get_new_msg(true,finish);
				},
				success: function(data) {
					pThis.system_msg_responsed = true;

					try {
						data = $.parseJSON(data);
					} catch (e) { //解析失败
						pThis.system_get_new_msg_complete(finish); //展示已取到的数据，终止轮询
						setTimeout(function() {
							pThis.system_get_new_msg(true,finish);
						}, 2000);
						return false;
					}

					//解析消息数据
					var lists = data.data || [];

					for (var i in lists) {
						try {
							var detail = $.parseJSON(lists[i]);
							var msg_exists = $('.newList > div[data-msgid="' + detail.msg_id + '"]').length;
							if (msg_exists > 0) {
								console.log('[commonFunc]消息已显示，跳过：' + detail.msg_id);
								console.log(data);
							} else {
								pThis.background_msg_queue.push(detail); //加入到待处理队列
							}
						} catch (e) {
							continue; //解析失败，跳过
						}
					}

					data.status = Number(data.status);
					switch (data.status) {
						case 1: //成功
							if (lists.length >= queue_length) { //未取完，取下一页
								pThis.system_get_new_msg(false);

							} else { //取完，处理队列中数据，同时继续轮询
								pThis.system_get_new_msg_complete(finish);
								pThis.system_get_new_msg(true,finish);
							}
							break;

						case -5: //令牌过期
							pThis.system_get_new_msg_complete(finish); //展示已取到的数据
							pThis.getPollingUrl(function(polling_url) { //重新获取请求链接后再轮询
								pThis.system_get_new_msg(true,finish);
							});
							break;

						case -8: //被订阅
							pThis.system_get_new_msg_complete(finish); //展示已取到的数据
							setTimeout(function() { //2秒后再发起轮询
								pThis.system_get_new_msg(true,finish);
							}, 2000);
							break;

						default:
							pThis.system_get_new_msg_complete(finish); //展示已取到的数据，终止轮询
					}
				}
			}

			this.polling(this.polling_url, null, pollingOpt);
		},
		//轮询
		polling :function(url, time, opts) {
			opts = opts || {};
			time = time || (new Date()).toGMTString();
			var cycle = typeof(opts.cycle) === 'undefined' ? true : opts.cycle;	//成功后是否继续轮询，默认为true
			
			var pThis = this;
			var opt = {
				wait	: false,
				url		: url,
				timeout	: opts.timeout || 30000,
				success	: function(resp, status, e) {
					if (e.status == 200) {										//有新消息：显示消息；更新最后一次获取的时间（避免获取重复数据），立即发起请求
						var newTime = (new Date()).toGMTString();
						if (typeof(opts.success) === 'function') {
							newTime = opts.success(resp, status, e);
						}
						if (cycle) {
							pThis.polling(url, newTime, opts);		
						}
						
					} else if (e.status == 304) {								//服务端超时，不更新最后一次获取时间，重新发起请求
						pThis.polling(url, time, opts);
						
					} else {													//其他情况，延迟5s再请求
						setTimeout(function() {									
							pThis.polling(url, time, opts);
						}, 5000);
					}
				},
				error	: function(data, status, e) {
					//处理超时
					if (status == 'timeout' || e == 'timeout') {				//超时，再执行最后一次请求内容
						if (typeof(opts.statusTimeout) == 'function') {
							opts.statusTimeout(url, time, opts);
							
						} else {
							pThis.polling(url, time, opts);
						}
						
					} else if (data && data.status == 403) {					//已被抢占，延迟5s再请求
						if (typeof(opts.status403) == 'function') {
							opts.status403(url, time, opts);
							
						} else {
							setTimeout(function() {									
								pThis.polling(url, time, opts);
							}, 5000);
						}
						
					} else {													//其他情况，延迟5s再请求
						setTimeout(function() {									
							pThis.polling(url, time, opts);
						}, 5000);
					}
				},
				exp		: function(e) {
					setTimeout(function() {										//其他异常，延迟5s再请求
						pThis.polling(url, time, opts);
					}, 5000);
				}
			};
			

			this.get(opt);
		},
		//get请求
		get:function(opt) {
			var pThis = this;

			try {
				t.ajaxJson({
					url:opt.url,
					type : 'get',
					dataType : 'text',
					timeout	: opt.timeout || 30000,	//默认30s超时

					callback:function(data,finish,status,e){
						if(finish){
							if (typeof(opt.success) === 'function') {
								opt.success(data, status, e);
							}
						}else{
							if (typeof(opt.error) === 'function') {
								opt.error(data, status, e);
								return;
							}
							if (status != 'abort') {
								console.log('系统繁忙');
								return;
							}
						}
					}	
				}); 
			}catch (e) {
				if (typeof(opt.exp) === 'function') {
					opt.exp(e);
				} else {
					console.log('系统繁忙.');
				}
			}
		},
		system_get_new_msg_complete:function(finish){
			if(this.background_msg_queue.length > 0){
				//console.log(this.background_msg_queue);
				console.log(this.background_msg_queue);
				var msg_ids = new Array();
				var data = this.background_msg_queue;

				finish(data);
				for (var i = 0; i < data.length; i++) {
					//newsObj.addNewMsgData(data[i]);
					msg_ids.push(data[i].msg_id);
				};

				t.ajaxJson({
					url:ajaxUrl.saveNewsUrl,
					data:{
						data:{
							msg_ids:msg_ids
						}
					},
					callback:function(result,status){
						if(result.errcode !=0){
							returnMsg.error(result);
							return;
						}

						//returnMsg.success(result);
					}
				});
			}
			
		},
		countNum:function(o,count){
			o.removeClass('hide second more');
			if(count < 0 ){count = 0;}
			if(count == 0){
				o.addClass('hide');
			}else if(count >= 10 && count <=99){
				o.addClass('second');
			}else if(count > 99){
				o.addClass('more');
			}
			o.text(count);
		},
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
		checkPlatform: function(){
			if (/android/i.test(navigator.userAgent)) {
				alert("This is Android‘browser."); //这是Android平台下浏览器 
			}
			if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
				alert("This is iOS‘browser."); //这是iOS平台下浏览器 
			}
			if (/Linux/i.test(navigator.userAgent)) {
				alert("This is Linux‘browser."); //这是Linux平台下浏览器 
			}
			if (/Linux/i.test(navigator.platform)) {
				alert("This is Linux operating system."); //这是Linux操作系统平台 
			}
			if (/MicroMessenger/i.test(navigator.userAgent)) {
				alert("This is MicroMessenger‘browser."); //这是微信平台下浏览器 
			}
		},
		//处理数据返回信息
		error:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.fail_screen(result.errmsg);
		},
		success:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.success_screen(result.errmsg);
		},
		onetimeMsgClick:function(key,obj){
			t.ajaxJson({
				url:ajaxUrl.updateOneTimeMsgUrl,
				data:{
					data:{
						msg_key:key
					}
				},
				callback:function(result,status){
					if(result.errcode == 0){
						obj.find("em.c").removeClass("c");
						obj.find(".newPic").addClass("hide");
					}
				}
			});
		}
	};

	return commonFuc;	
});