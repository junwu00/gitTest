define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	//"index:static/Scripts/lib/swiper.jquery.min",
	"index:static/Scripts/lib/idangerous.swiper.min",
	'index:static/Scripts/commonFuc'
],function($,t,UiFramework,tpl,commonTpl,commonData,l,Swiper,commonFuc) {
	var ajaxUrl = commonData.ajaxUrl;//专门处理请求URL
	//消息
	var newsObj2 = {
		mediator:null,
		newsData:[],
		init:function(mediator){
			this.mediator =  mediator;

			this.newList = this.mediator.container.find(".news .newList");

			this.getNewList();
		},
		getNewList:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getNewListUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}
					var data = result.msg;
					pThis.msgData = data;

					tpl.register('getShortTime', t.getShortTime);
					var dom = tpl(commonTpl, {
						msg:data,
						defaultFace:defaultFace
					});
					tpl.unregister('getShortTime');

					var o = $($.trim(dom));

					pThis.newList.append(o);
					pThis.bindDom();

					pThis.getAllCount();

					pThis.getNewsNumber();
				}
			});
		},
		getAllCount:function(){
			var data = this.msgData,count = 0;
			for (var i = 0; i < data.length; i++) {
				if(!data[i].hasOwnProperty("hide")){
					count += parseInt(data[i].count);
				}
			};

			var o = this.mediator.fLis.eq(0).find(".countNum");

			commonFuc.countNum(o,count);
		},
		getNewsNumber:function(){
			var pThis = this;
			commonFuc.getPollingUrl(function(polling_url){
				commonFuc.system_get_new_msg(true,function(data){
					for (var i = 0; i < data.length; i++) {
						var msg_exists = $('.newList > div[data-msgid="' + data[i].msg_id + '"]').length;
						if (msg_exists > 0) {
							console.log('[newsObj]消息已显示，跳过：' + data[i].msg_id);
							console.log(data);
						} else {
							pThis.addNewMsgData(data[i]);
						}
					};
				});
			});
		},
		bindDom:function(){
			var pThis = this;
			this.newList.on("click",'section',function(e){
    			//vLis.removeClass('slideSlow slideFast slideSlowBack slideFastBack');
    			e.preventDefault();
    			commonFuc.addClickBg($(this));

				var type = $(this).attr("data-type"),
					appId = $(this).attr("data-appid");

				pThis.type = type;pThis.appId = appId;
				pThis.saveOneNewData(type,appId);

				pThis.goPage(type,appId);
			});

			var startX = 0,startY = 0,moveEndX = 0,moveEndY = 0;

            this.newList.find('section').on("touchstart",function(e){
                //e.preventDefault();
                var touch = e.originalEvent.targetTouches[0]; 
                var y = touch.pageY;

                startX = e.originalEvent.changedTouches[0].pageX,
                startY = e.originalEvent.changedTouches[0].pageY;
            });

            this.newList.find('section').on("touchmove",function(e){
                e.preventDefault();
                var touch = e.originalEvent.targetTouches[0]; 
                var y = touch.pageY;
                
                moveEndX = e.originalEvent.changedTouches[0].pageX,
                moveEndY = e.originalEvent.changedTouches[0].pageY,
                X = moveEndX - startX,
                Y = moveEndY - startY;
                 
                if ( Math.abs(X) > Math.abs(Y) && X > 0 ) {
                    alert("left 2 right");
                }
                else if ( Math.abs(X) > Math.abs(Y) && X < 0 ) {
                    alert("right 2 left");
                }
                else if ( Math.abs(Y) > Math.abs(X) && Y > 0) {
                    //alert("top 2 bottom");
                }
                else if ( Math.abs(Y) > Math.abs(X) && Y < 0 ) {
                    //alert("bottom 2 top");
                }
                else{
                    alert("just touch");
                }
            });
		},
		goPage:function(type,appId){
			var pageManager = this.mediator.pageManager;
			//移步小助手
			if(type == 0 && appId == 0){
				pageManager.go("nHelperOne");
			}
			//待办信息
			else if(type == 1 && appId == 23){
				pageManager.go("nWaitOne");
			}
			//知会提醒
			else if(type == 3 && appId == 23){
				pageManager.go("nNotifyOne");
			}
			//通过提醒
			else if(type == 2 && appId == 23){
				pageManager.go("nPassOne");
			}
			//润衡提醒
			else if(type == 1 && appId == 0){
				pageManager.go("nRH");
			}
			//报表提醒
			else if(type == 2 && appId == 0){
				pageManager.go("nChart");
			}
			//打卡
			else if(type == 1 && appId == 25){
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=1");
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=signIn&type=1";
			}
			//定位
			else if(type == 2 && appId == 25){
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=1");
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=signIn&type=2";
			}
			//定位提醒
			else if(type == 3 && appId == 25){
				pageManager.go("nLocation");
			}
			//排班提醒
			else if(type == 4 && appId == 25){
				pageManager.go("nRota");
			}
		},
		//信息保存
		saveOneNewData:function(type,appId){
			var flag = true;
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId){
					flag = false;
					break;
				}
			};

			if(flag){
				this.newsData.push({type:type,appId:appId,page:0,pageSize:10,data:[]});
			}
		},
		updateOneNewData:function(type,appId,data,position){
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId && data.length >0){
					this.newsData[i].page ++;
					//this.newsData[i].data.concat(data);
					//this.newsData[i].data = this.newsData[i].data.concat(data);
					for (var j = 0; j < data.length; j++) {
						if(!position){
							this.newsData[i].data.push(data[j]);
						}else{
							this.newsData[i].data.unshift(data[j]);
						}
					};
					
					return this.newsData[i];
				}
			};
		},
		getOneNewData:function(type,appId){
			for (var i = 0; i < this.newsData.length; i++) {
				if(this.newsData[i].type == type && this.newsData[i].appId == appId){
					return this.newsData[i];
				}
			};
		},
		getHtml: function(oneNewData, data, finish,scrollFuc) {
			var nFirstDom = this.mediator.nFirstDom,
				sc = nFirstDom.find(".swiper-container");

			sc.find(".swiper-wrapper").removeAttr('style');

			if(data.length == 0 && !scrollFuc){
				sc.addClass('advice_list');
				sc.find(".has-no-record").removeClass('hide');

				return;
			}else{
				sc.removeClass('advice_list');
				sc.find(".has-no-record").addClass("hide");
			}

			var pThis = this;
			function returnFinish(params){
				if(finish && t.isFunction(finish)){
					finish.apply(pThis,params);
				}
			}

			var noReadTime = null;
			if (data.length > 0) {
				if(!scrollFuc){
					for (var i = 0; i < data.length; i++) {
						if (data[i].state == 1) {
							noReadTime = data[i].time;
							break;
						}
					};
				}else{
					for (var k = data.length - 1; k >=0; k--) {
						if (data[k].state == 1) {
							noReadTime = data[k].time;
							break;
						}
					};
				}
			}

			var dom = "",arr = new Array();

			var rt = noReadTime;

			for (var j = 0; j < data.length; j++) {
				if(j == 0 && !scrollFuc){
					rt = noReadTime;
				}else if(j == data.length - 1 && scrollFuc){
					rt = noReadTime;
				}else{
					rt = null;
				}

				tpl.register('getShortTime', t.getShortTime);
				tpl.register('addMark', pThis.addMark);
				tpl.register('checkUrl', pThis.checkUrl);
				var html = tpl(commonTpl, {
					oneNewMsg: data[j],
					oneNewData: oneNewData,
					defaultFace: defaultFace,
					noReadTime: rt,
					scrollFuc: 1,
					isIndex:"&isIndex=1",
					menuType:"&menuType=1",
					hash:window.location.hash
				});
				tpl.unregister('getShortTime');
				tpl.unregister('addMark');
				tpl.register('checkUrl');

				arr.push($.trim(html));
			};

			var o = $($.trim(dom));

			if (scrollFuc && t.isFunction(scrollFuc)) {
				scrollFuc.apply(pThis, [arr]);
				return;
			}

			//var nFirstDom = this.mediator.nFirstDom;
			nFirstDom.find(".swiper-wrapper").removeAttr('style');
			nFirstDom.find(".swiper-wrapper").html(arr.join(''));

			var height = nFirstDom.find(".swiper-slide > section").outerHeight() /*+ 6 + 12*/;

			if(oneNewData.type == 0 && oneNewData.appId == 0){
				/*height += 21 ;*/
			}

			//if(data.length != 0 && nFirstDom.attr("data-type") != type && nFirstDom.attr("data-appid") != appId){
			pThis.bindMsgDom(height,oneNewData);
			//}

			nFirstDom.attr("data-type", oneNewData.type);
			nFirstDom.attr("data-appid", oneNewData.appId);

			returnFinish([o]);
			return;
		},
		getOneNewMsg:function(type,appId,finish,scrollFuc){
			
			var oneNewData = this.getOneNewData(type,appId),pThis = this;

			if(oneNewData.data.length > 0 && !scrollFuc){
				this.getHtml(oneNewData,oneNewData.data,finish,scrollFuc);
				return;
			}

			this.topMsgId = oneNewData.data.length >0 ? oneNewData.data[0].id : 0;
			
			t.ajaxJson({
				url:ajaxUrl.getOneNewsUrl,
				data:{
					data:{
						type:type,
						app_id:appId,
						start_id:pThis.topMsgId,
						page_size:oneNewData.pageSize//oneNewData.pageSize
					}
				},
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					var data = result.data;

					if(data.length != 0){
						oneNewData = pThis.updateOneNewData(type,appId,data,1);
					}

					var d = t.clone(oneNewData.data);

					if(scrollFuc && t.isFunction(scrollFuc)){
						d = data;
					}

					pThis.getHtml(oneNewData,d,finish,scrollFuc);
				}
			});
		},
		bindMsgDom:function(height,oneNewData){
			var pThis = this;

			if(this.mySwiper){
				this.mySwiper.destroy(false);
			}

			var h = this.mediator.nFirstDom.height();
			var len = Math.floor((h - 40) / height);

			var type = oneNewData.type,appId = oneNewData.appId;
			var listLength = oneNewData.data.length;
			var initialSlide = listLength > 1 ? (listLength - 1) : listLength;

			var holdPosition = 0;
			var isLast = false;

			var params = {
				slidesPerView:'auto',
			    mode:'vertical',
			    //freeMode:true,
			    freeModeFluid:true,
			    momentumRatio:100,
			    watchActiveIndex: true,
			    calculateHeight:true,
			    initialSlide:initialSlide,
			    offsetPxBefore : 10,
			    offsetPxAfter : 10,
				//autoHeight: true,
				onTouchStart: function(swiper) {
					holdPosition = 0;

					holdPosition > 100 && !isLast ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onTouchMove:function(swiper){
					holdPosition > 100 && !isLast ? $('.preloader').addClass('visible') : $('.preloader').removeClass('visible');
				},
				onResistanceBefore: function(s, pos) {
					holdPosition = pos;
				},
				onTouchEnd: function(swiper) {
					if (holdPosition > 100 && !isLast) {
						mySwiper.setWrapperTranslate(0, 100, 0)
						mySwiper.params.onlyExternal = true;
						//$('.preloader').addClass('visible');
						$('.preloader').removeClass('visible');
						pThis.getOneNewMsg(pThis.type, pThis.appId, null,function(o,position){
							for (var i = 0; i < o.length; i++) {
								mySwiper.prependSlide($($.trim(o[i])).html());
								mySwiper.updateActiveSlide(i);
							};
							
							//Release interactions and set wrapper
							mySwiper.setWrapperTranslate(0, 0, 0);
							mySwiper.params.onlyExternal = false;
							//Update active slide
							//mySwiper.updateActiveSlide(0);
							//Hide loader
							$('.preloader').removeClass('visible');

							swiper.swipeTo(o.length,0,false);

							if(o.length == 0 && pThis.mySwiper){
								swiper.params.resistance = "100%";
								isLast = true;
								//pThis.mySwiper.destroy(false);
								//$(".swiper-container").addClass('scrollY');
								return;
							}
						});
					}
				}
			};

			try{
				if(platid == 4 || platid == 5){
					var mySwiper = null;
					var nFirstDom = this.mediator.nFirstDom;
					nFirstDom.find(".swiper-container").removeClass('swiper-free-mode');
					nFirstDom.find(".swiper-scrollbar").html("");
					require(["index:static/Scripts/lib/idangerous.swiper.scrollbar-2.4.1.min"],function(){
						params.scrollContainer = true;
						params.mousewheelControl = true;
						params.calculateHeight = false;
						params.scrollbar = {
						    container: '.swiper-scrollbar',
						    draggable : true,
						    hide: false,
						    snapOnRelease: false
					    };

					    mySwiper = new Swiper('.swiper-container', params);

						pThis.mySwiper = mySwiper;
					});
				}else{
					var mySwiper = new Swiper('.swiper-container', params);

					this.mySwiper = mySwiper;
				}
			}catch(e){
				var mySwiper = new Swiper('.swiper-container', params);

				this.mySwiper = mySwiper;
			}
		},
		getHelperSystemMsg:function(data){
			var mediator = this.mediator,
				vLis = mediator.vLis;
			function getSystemHtml(){
				var o = vLis.eq(3).find(".np2");var pThis = this;
				var new_o = o.clone();
				new_o.each(function(index, el) {
					var time = $(this).find(".time");
					time.removeClass("mt5");
					$(this).find(".title").addClass('mt7');
					$(this).find("img").addClass('mt7').removeClass('mt15');
					$(this).prepend(time);
				});

				new_o.addClass('w96p')

				vLis.eq(4).html(new_o);
			}
			
			if(mediator.nFirstDom.find(".swiper-wrapper").html() ==""){
				this.saveOneNewData(data.type,data.appId);
				this.getOneNewMsg(data.type,data.appId,function(){
					getSystemHtml();
				});
				return;
			}

			getSystemHtml();
		},
		goToFirstFloor:function(){
			this.mediator.fLis.eq(3).click();
		},
		backToIndex:function(index){
			this.type = null;
			this.appId = null;
			//nFirstDom.removeAttr('data-type').removeAttr('data-appid');

			if(index === undefined){index = 0;}
			this.mediator.fLis.eq(index).click();
			commonFuc.removeClickBg(this.newList.find("section.clickBg"));
			this.removeHelperBottom();

			commonFuc.changeWindowTitle(commonData.version);
		},
		//移步小助手
		getHelperBottom:function(type,appId){
			var mediator = this.mediator;
			if(!(type == 0 && appId == 0) || mediator.nFirstDom.find(".nBottomBtns").length !=0){return;}

			var html = new Array();
			html.push('<ul class="nBottomBtns tac">');
			html.push('<li class="fl col2 pt0">');
			html.push('<i class="icon-align-justify fs12 c-gray"></i>');
			html.push('<span class="fs17">操作指南</span>');
			html.push('<span></span>');
			html.push('</li>');
			html.push('<li class="fl col2 pt0">');
			html.push('<i class="icon-align-justify fs12 c-gray"></i>');
			html.push('<span class="fs17">常见问题</span>');
			html.push('<span></span>');
			html.push('</li>');
			html.push('</ul>');

			var o = $(html.join(''));

			var pThis = this;
			o.on('click',"li:first",function(){
				//操作指南
				mediator.pageManager.go("nUseHelper");
			});

			o.on('click',"li:last",function(){
				//常见问题
				mediator.pageManager.go("nQuestions");
				//系统公告
				//mediator.pageManager.go("nHelperTwo");
			});

			mediator.nFirstDom.off().on("click",".clickUrl",function(){
				var url = $(this).find("input.hideUrl").val();

				if(!url){return;}

				var pageUrl = url.substring(url.lastIndexOf("#") + 1);

				if(pageUrl != "nVip"){
					window.location.href = url;
				}else{
					mediator.pageManager.go(pageUrl);
				}
			});

			mediator.footer.prepend(o);
		},
		removeHelperBottom:function(){
			this.mediator.footer.find("ul.nBottomBtns").remove();
		},
		goToHelperOne:function(data){
			var mediator = this.mediator,
				nFirstDom =  mediator.nFirstDom,
				fLis = mediator.fLis;

			var pThis = this,type,appId;
			if(this.type && this.appId){
				type = this.type;
				appId = this.appId;
			}else{
				type = data.type;
				appId = data.appId;
				this.type = type;
				this.appId = appId;
				this.saveOneNewData(type,appId);
			}

			var o = nFirstDom.find(".swiper-wrapper");

			if(o.html() == "" || (nFirstDom.attr("data-type") != type || nFirstDom.attr("data-appid") != appId)){
				o.html("");
				this.getHelperBottom(type,appId);
				this.goToFirstFloor();
				this.getOneNewMsg(type,appId,function(){
					pThis.readNewMsg();
				});
			}else{
				this.getHelperBottom(type,appId);
				this.goToFirstFloor();

				pThis.readNewMsg();
			}

			commonFuc.changeWindowTitle(data.name);
		},
		goToHelperTwo:function(data){
			var mediator = this.mediator;
			mediator.footer.addClass('none');
			this.removeHelperBottom();
			mediator.container.addClass('noBottom');

			this.getHelperSystemMsg(data);
			mediator.fLis.eq(4).click();
		},
		backToHelperOne:function(){
			var mediator = this.mediator;
			mediator.footer.removeClass('none');
			mediator.container.removeClass('noBottom');
			mediator.pageManager.go("nHelperOne",1);
		},
		//待办信息
		goToWaitOne:function(data){
			this.goToHelperOne(data);
		},
		//知会信息
		goToNotifyOne:function(data){
			this.goToHelperOne(data);
		},
		//考勤提醒
		goToPassOne:function(data){
			this.goToHelperOne(data);
		},
		//报表提醒
		goToChart:function(data){
			this.goToHelperOne(data);
		},
		//定位提醒
		goToLocation:function(data){
			this.goToHelperOne(data);
		},
		//排班提醒
		goToRota:function(data){
			this.goToHelperOne(data);
		},
		readNewMsg:function(){
			var o = this.newList.find("section[data-type="+this.type+"][data-appid="+this.appId+"]");

			var sc = o.find(".countNum"),scn = sc.text();
			var fc = this.mediator.fLis.eq(0).find(".countNum"),fcn = fc.text();
			var minus = fcn - scn;

			if(minus == 0){
				fc.addClass('hide');
			}
			fc.text(minus);
			sc.addClass('hide');
			sc.text(0);
		},
		addNewMsgData:function(data){
			var type = data.type,appId =data.app_id,
			d = [{
				content:data.content,
				id:data.id,
				pic:data.pic,
				state:0,
				time:data.time,
				title:data.title,
				url:data.url,
				user_id:data.user_id,
				user_pic:data.user_pic
			}];
			var oneNewData = this.updateOneNewData(type,appId,d);

			var o = this.newList.find("section[data-type="+type+"][data-appid="+appId+"]");

			o.removeClass('hide');

			var sc = o.find(".countNum"),scn = sc.text();
			scn = this.checkNumber(scn) + 1;

			commonFuc.countNum(sc,scn);

			var fc = this.mediator.fLis.eq(0).find(".countNum"),fcn = fc.text();
			fcn = this.checkNumber(fcn) + 1;

			commonFuc.countNum(fc,fcn);

			var sp1 = this.newList.find("section[data-type=2][data-appid=25]");//定位
			var sp2 = this.newList.find("section[data-type=1][data-appid=25]");//打卡
			var appendObj = sp2.length > 0 ? sp2 : sp1;

			if(appendObj.length > 0){
				appendObj.after(o);
			}else{
				this.newList.prepend(o);
			}

			o.find("time").text(t.getShortTime(data.time));
			o.find("p").text(data.content);

			var nFirstDom = this.mediator.nFirstDom;
			if(nFirstDom.attr("data-type") == type && nFirstDom.attr("data-appid") == appId){
				//var oneNewData = this.getOneNewData(this.type,this.appId);
				this.getHtml(oneNewData, d, null,function(o){
					if(this.mySwiper){
						//this.mySwiper.appendSlide(obj);
						for (var i = 0; i < o.length; i++) {
							this.mySwiper.appendSlide($($.trim(o[i])).html());
						};
						//this.mySwiper.slideTo(nFirstDom.find(".swiper-slide").length, 1000, false);//切换到第一个slide，速度为1秒
						this.mySwiper.swipeTo(this.mySwiper.slides.length - 1, 1000, false);//切换到第一个slide，速度为1秒
					}
				}, 1);
			}

			//待办提醒
			if(type == 1 && appId == 23){
				var redIcon = this.mediator.fLis.eq(1).find(".countNum");
				
				if(!redIcon.hasClass('hide')){
					this.mediator.getDealNumber();
				}
			}
		},
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
		goToUseHelper:function(data){
			var mediator = this.mediator;
			mediator.footer.addClass('none');
			this.removeHelperBottom();
			mediator.container.addClass('noBottom');

			commonFuc.changeWindowTitle(data.name);

			var sId = data.sId;

			mediator.nSecondDom.html(mediator.mGetServiceHtml(sId));

			mediator.fLis.eq(4).click();
		},
		goToQuestions:function(data){
			var mediator = this.mediator;
			mediator.footer.addClass('none');
			this.removeHelperBottom();
			mediator.container.addClass('noBottom');

			commonFuc.changeWindowTitle(data.name);

			var sId = data.sId;

			mediator.nSecondDom.html(mediator.mGetQuestionsHtmlToNews());

			mediator.fLis.eq(4).click();
		},
		goToQuestionDetail:function(data){
			this.mediator.mGoToQuestionDetail(data);
		},
		backToQuestions:function(){
			this.mediator.fLis.eq(4).click();
		},
		//标记
		addMark:function(str){
			if(str == ""){return;}

			var reg = new RegExp("(《)[^《》\n\r]*(》)","g");

			var arr = str.match(reg);

			if(arr && arr.length > 0){
				for (var i = 0; i < arr.length; i++) {
					str = str.replace(reg, '<b>'+arr[i]+'</b>');
				};
			}

			return str;
		},
		checkUrl:function(url){
			if(url.indexOf("?") >= 0){
				return url;
			}

			return url + "?";
		},
		/*Vip*/
		goToVip:function(data){
			this.mediator.mGoToVip(data);

			this.removeHelperBottom();
		},
		/*------------第三方新增--------------------*/
		//润衡提醒
		goToRH:function(data){
			this.goToHelperOne(data);
		}
	};

	var newsObj = {
		mediator:null,
		init:function(mediator){
			this.mediator =  mediator;

			this.newList = this.mediator.container.find(".news .newList");

			this.bindDom();
			this.bindLoadPageDom(this.newList);
		},
		lastMsgId : 0,
		bindLoadPageDom:function(target){
			var pThis = this;

			UiFramework.scrollLoadingPackage().init({
				target:target,
				scroll_target:target,
				fun:function(self,e){
					pThis.getNewMsg(pThis.lastMsgId,function(data){
						pThis.getNoReadCount(data.unsee_count);

						if(!pThis.firstLoad){
							pThis.getNewsNumber();
							pThis.firstLoad =true;
						}

						var o = pThis.getNewMsgHtml(data);

						pThis.newList.append(o);

						if(data.list.length == 0){
							target.attr("is_scroll_loading",false);
							target.attr("is_scroll_end",true);
							return;
						}

						target.attr("is_scroll_loading",false);

						self.remove_scroll_waiting(target);

						if(data.list.length > 0){
							pThis.lastMsgId = data.list[data.list.length - 1].id;
						}

						if(data.list.length < 20){
							target.attr("is_scroll_loading",false);
							target.attr("is_scroll_end",true);
						}
					});
				}
			});

			target.scroll();
		},
		getNewMsgHtml:function(data){
			tpl.register('getShortTime', t.getShortTime);
			var dom = tpl(commonTpl, {
				newMsg:1,
				data:data,
				defaultFace:defaultFace
			});
			tpl.unregister('getShortTime');

			return $($.trim(dom));
		},
		getNewsNumber:function(){
			var pThis = this;
			commonFuc.getPollingUrl(function(polling_url){
				commonFuc.system_get_new_msg(true,function(data){
					for (var i = 0; i < data.length; i++) {
						pThis.addNewMsgData(data[i]);
					};
				});
			});
		},
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
		addNewMsgData:function(data){
			console.log(data);

			var o = this.getNewMsgHtml({
				list:[data]
			});

			var sp1 = this.newList.find("section[data-type=2][data-appid=25]");//定位
			var sp2 = this.newList.find("section[data-type=1][data-appid=25]");//打卡
			var appendObj = sp2.length > 0 ? sp2 : sp1;

			o.addClass("animated bounceInRight");

			if(appendObj.length > 0){
				appendObj.after(o);
			}else{
				this.newList.prepend(o);
			}

			var fc = this.mediator.fLis.eq(0).find(".countNum"),fcn = fc.text();
			fcn = this.checkNumber(fcn) + 1;

			commonFuc.countNum(fc,fcn);

			//待办提醒
			if(data.type == 1 && data.appId == 23){
				var redIcon = this.mediator.fLis.eq(1).find(".countNum");
				
				if(!redIcon.hasClass('hide')){
					this.mediator.getDealNumber();
				}
			}
		},
		//获取消息接口
		getNewMsg:function(lastMsgId,callback){
			if(lastMsgId === undefined || lastMsgId == null){
				lastMsgId = 0;
			}

			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getNewMsgUrl,
				data:{
					data:{
						start_id:lastMsgId
					}
				},
				callback:function(result,status){
					//alert(JSON.stringify(result));
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}
					var data = result.data;
					pThis.msgData = data;

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[data]);
					}
				}
			});
		},
		//消息阅读接口 && 删除消息接口
		readOrDelMsg:function(msgId,callback,del){
			var pThis = this;
			if(msgId === undefined || msgId == null){
				if(callback && t.isFunction(callback)){
					callback.apply(pThis,[]);
				}
				return;
			}

			var url = del ? ajaxUrl.delMsgUrl : ajaxUrl.readMsgUrl;
			
			t.ajaxJson({
				url:url,
				data:{
					data:{
						msg_id:msgId
					}
				},
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[]);
					}
				}
			});
		},
		bindDom:function(){
			var pThis = this;

			this.newList.on("click","section",function(e){
    			e.preventDefault();
    			var o = $(this);
    			commonFuc.addClickBg(o);

    			var type = o.attr("data-type"),
					appId = o.attr("data-appid"),
					state = o.attr("data-state"),
					msgId = o.attr("data-msgid"),
					url = o.find("input.partUrl").val();

				//打卡 定位按钮
				if((type == 1 || type == 2) && appId == 25){
					history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=1");
					window.location.href = "/wx/index.php?debug=1&app=workshift&a=signIn&type="+type;
					return;
				}
			});

			this.newList.on("click",'.msg .partContent',function(e){
				var o = $(this).parent();
				
				/*o.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				});*/
				
				//console.log("click");
				if($(this).is(":animated")){
					return;
				}

    			e.preventDefault();
    			commonFuc.addClickBg($(this));

    			var type = o.attr("data-type"),
					appId = o.attr("data-appid"),
					state = o.attr("data-state"),
					msgId = o.attr("data-msgid"),
					url = o.find("input.partUrl").val();

				//链接不为空时跳转,否则打开详情
				if(url){
					url = t.checkUrl(url);
					var hash = "";

					if(url.lastIndexOf("#") > 0){
						hash = url.substring(url.lastIndexOf("#"),url.length);
						url = url.substring(0,url.lastIndexOf("#"));
					}

					url = url + "&isIndex=1&menuType=1" + hash || window.location.hash;

					history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=1");

					var pageUrl = url.substring(url.lastIndexOf("#") + 1);
					if(pageUrl != "nVip"){
						setTimeout(function(){
							window.location.href = url;
						},500);
					}else{
						pThis.mediator.pageManager.go(pageUrl);
					}
					
					if(state != "1"){
						pThis.readOrDelMsg(msgId,function(){
						});
					}
				}else{
					pThis.viewData = {
						title:o.find(".title i").text(),
						content:o.find("p").text(),
						user_pic:o.find("img").attr("src"),
						time:o.find("time").text(),
						pic:o.find("input[name=pic]").val()
					};

					pThis.mediator.pageManager.go("nMsgInfo");

					//pThis.viewInfo(pThis.viewData);
					if(state != "1"){
						o.attr("data-state",1);
						o.find("i.countNum").addClass('hide');
						pThis.readOrDelMsg(msgId,function(){
							var fc = pThis.mediator.fLis.eq(0).find(".countNum"),fcn = fc.text();
							fcn = pThis.checkNumber(fcn) - 1;

							commonFuc.countNum(fc,fcn);
						});
					}
				}
			});

			this.newList.on("click",'.msg .delMsg',function(e){
				e.preventDefault();
				var o = $(this).parents(".msg");
				var msgId = o.attr("data-msgid");
				var state = o.attr("data-state");

				o.removeClass('animated bounceInRight').addClass('animated bounceOutLeft');
				setTimeout(function(){
					o.remove();

					//未阅读处理
					if(state == "0" || state == ""){
						var fc = pThis.mediator.fLis.eq(0).find(".countNum"),fcn = fc.text();
						fcn = pThis.checkNumber(fcn) - 1;

						commonFuc.countNum(fc,fcn);
					}
				},500);

				pThis.readOrDelMsg(msgId,function(){
				},1);
			});

			var startX = 0,startY = 0,moveEndX = 0,moveEndY = 0;

            this.newList.on("touchstart",".msg .partContent",function(e){
                //e.preventDefault();
                var touch = e.originalEvent.targetTouches[0]; 
                var y = touch.pageY;

                startX = e.originalEvent.changedTouches[0].pageX,
                startY = e.originalEvent.changedTouches[0].pageY;
            });

            this.newList.on("touchmove",".msg .partContent",function(e){
                //e.preventDefault();
                var touch = e.originalEvent.targetTouches[0]; 
                var y = touch.pageY;
                
                moveEndX = e.originalEvent.changedTouches[0].pageX,
                moveEndY = e.originalEvent.changedTouches[0].pageY,
                X = moveEndX - startX,
                Y = moveEndY - startY;

                var o =  $(this);

    			var partBtns = o.next(".partBtns");
    			var btns_w = 0;
    			partBtns.find("span").each(function(index){
    				btns_w += $(this).outerWidth();
				});
                 
                if ( Math.abs(X) > Math.abs(Y) && X > 0 ) {
                    //console.log("left 2 right");
                    
                    //o.css("left",0);
                    pThis.slideObj(o,0);
                }
                else if ( Math.abs(X) > Math.abs(Y) && X < 0 ) {
                    //console.log("right 2 left");

                    if(X <= -btns_w){
                    	X = -btns_w;
                    }

                    //o.css("left",X);
                    pThis.slideObj(o,X);
                }
                else if ( Math.abs(Y) > Math.abs(X) && Y > 0) {
                    //alert("top 2 bottom");
                }
                else if ( Math.abs(Y) > Math.abs(X) && Y < 0 ) {
                    //alert("bottom 2 top");
                }
                else{
                    //alert("just touch");
                }
            });

 			this.newList.on("touchend",".msg .partContent",function(e){
 				var o = $(this);
                //var left = o.css("left");
                //left = left ? parseInt(left.substring(-2)) : 0;

                var left = o[0].style.webkitTransform;
                left = pThis.getTransformVal(left);

                console.log(left);

                if(left == 0){
					return;
				}
 				//e.preventDefault();

 				moveEndX = e.originalEvent.changedTouches[0].pageX,
                moveEndY = e.originalEvent.changedTouches[0].pageY,
                X = moveEndX - startX,
                Y = moveEndY - startY;
  
                var partBtns = o.next(".partBtns");
    			var btns_w = 0,fBtn_w = 0;
    			partBtns.find("span").each(function(index){
    				if(index == 0){
    					fBtn_w = $(this).outerWidth();
    				}

    				btns_w += $(this).outerWidth();
				});
				var silde_w = btns_w - fBtn_w / 2;

				console.log("left:"+left);

                if ( Math.abs(X) > Math.abs(Y) && X < 0 && X <= -silde_w) {
                    X = -btns_w;

                    //o.css("left",X);
                    pThis.slideObj(o,X);
                }else{
                    //o.css("left",0);
                    pThis.slideObj(o,0);
                    e.preventDefault();
                }
 			});
		},
		slideObj:function(obj,x,cancel){
			if(!cancel){
				//obj.css("left",x);

				obj.attr("style","-webkit-transform: translateX("+x+"px);");
				return;
			}

			obj.animate({"left":x+"px"}, 50);
		},
		//获取未阅读数量
		getNoReadCount:function(count){
			var o = this.mediator.fLis.eq(0).find(".countNum");

			commonFuc.countNum(o,count);
		},
		getTransformVal:function(val){
			return parseInt(val.substring(val.indexOf("(") + 1,val.lastIndexOf("px"))) || 0;
		},
		goToMsgInfo:function(data){
			commonFuc.changeWindowTitle(data.name);

			this.viewInfo();

			this.goToFloor(4);
		},
		goToFloor:function(floorNum,showBottom){
			var mediator = this.mediator;
			if(!showBottom){
				mediator.footer.addClass('none');
				mediator.container.addClass('noBottom');
			}
			mediator.fLis.eq(floorNum).click();
		},
		backToIndex:function(index){
			if(index === undefined){index = 0;}

			var mediator = this.mediator;
			mediator.fLis.eq(index).click();
			mediator.footer.removeClass('none');
			mediator.container.removeClass('noBottom');

			commonFuc.removeClickBg(this.newList.find("div.clickBg"));

			commonFuc.changeWindowTitle(commonData.version);

			if(this.searching){
				this.searching = 0;
				mediator.container.addClass('noBottom');
				var pThis = this;
				setTimeout(function(){
					pThis.mediator.showSeachObj();
				},300);
			}
		},
		viewInfo:function(){
			var data = this.viewData;
			if(!data){
				window.history.go(-1);
				return;
			}
			
			data.msgView = 1;

			var dom = tpl(commonTpl,data);

			this.mediator.nSecondDom.html($($.trim(dom)));
		},
		/*Vip*/
		goToVip:function(data){
			this.mediator.mGoToVip(data);

			this.removeHelperBottom();
		},
		removeHelperBottom:function(){
			this.mediator.footer.find("ul.nBottomBtns").remove();
		},
		backToNewMsg:function(){
			this.backToIndex(0);
		}
	};

	return newsObj;
});