define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'index:static/Scripts/commonData',
],function($,t,UiFramework,commonData) {
	var ajaxUrl = commonData.ajaxUrl,
		layerPackage = UiFramework.layerPackage();
	var commonFuc = {
		//添加背景色
		addClickBg:function(jqObj){
			jqObj.addClass('clickBg');
		},
		//去掉背景色
		removeClickBg:function(jqObj){
			jqObj.removeClass('clickBg');
		},
		//改变网页标题名 IOS不生效
		changeWindowTitle:function(name){
			document.title = name;

			/*const mobile = navigator.userAgent.toLowerCase();
			const length = document.querySelectorAll('iframe').length;
			if (/iphone|ipad|ipod/.test(mobile) && !length) {
			  const iframe = document.createElement('iframe');
			  iframe.style.cssText = 'display: none; width: 0; height: 0;';
			  iframe.setAttribute('src', 'about:blank');
			  iframe.addEventListener('load', () => {
			    setTimeout(() => {
			      iframe.removeEventListener('load', false);
			      document.body.removeChild(iframe);
			    }, 0);
			  });
			  document.body.appendChild(iframe);
			}*/
		},
		countNum:function(o,count){
			o.removeClass('hide second more');
			if(count < 0 ){count = 0;}
			if(count == 0){
				o.addClass('hide');
			}else if(count >= 10 && count <=99){
				o.addClass('second');
			}else if(count > 99){
				o.addClass('more');
			}
			o.text(count);
		},
		//校验是否数字 INT
		checkNumber:function(num){
			if(!num){num = 0;}else{num = parseInt(num);}

			return num;
		},
		//处理数据返回信息
		//错误信息返回
		error:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.fail_screen(result.errmsg);
		},
		//成功信息返回
		success:function(result){
			layerPackage.unlock_screen();//遮罩end
			layerPackage.success_screen(result.errmsg);
		},
		//一次性消息点击处理
		onetimeMsgClick:function(key,obj){
			t.ajaxJson({
				url:ajaxUrl.updateOneTimeMsgUrl,
				data:{
					data:{
						msg_key:key
					}
				},
				callback:function(result,status){
					if(result.errcode == 0){
						obj.find("em.c").removeClass("c");
						obj.find(".newPic").addClass("hide");
					}
				}
			});
		},
		goFloor:function(floor){
			floor.removeClass('hide slideOutRight').addClass('slideInRight');
		},
		backFloor:function(floor){
			floor.removeClass('slideInRight').addClass('slideOutRight');
			setTimeout(function(){
				floor.addClass('hide');
			},500);
		}
	};

	return commonFuc;	
});