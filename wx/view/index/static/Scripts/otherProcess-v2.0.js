define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/otherProcess.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc-v2.0'
],function($,t,UiFramework,tpl,otherProcessTpl,commonData,l,commonFuc) {
	var layerPackage = UiFramework.layerPackage();
	var otherProcess = {
		processObj:null,
		init:function(processObj){
			this.processObj = processObj;
			this.mediator = processObj.mediator;
		},
		backToRHList:function(){
			commonFuc.backFloor(this.mediator.secondFloor);
		},
		//润恒列表
		goToRHList:function(data){
			layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			this.getHtmlForRH(data);
		},
		//润恒分类列表
		goToRHTypeList:function(data,oneData){
			layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			//说明不是单条数据，应该跳转到第二层
			if($.trim(this.mediator.firstFloor.html()) == "" && !oneData){
				this.getHtmlForRH(data,function(rhData){
					var rhId = this.mediator.getSaveKey("rhId");

					this.rhId = rhId || rhData[0].id;

					this.getHtmlForRHType(data);

					commonFuc.goFloor(this.mediator.secondFloor);
				});

				return;
			}

			this.getHtmlForRHType(data,oneData);

			commonFuc.goFloor(this.mediator.firstFloor);
		},
		//获取润恒列表模板
		getHtmlForRH:function(data,callback){
			var pThis = this,
				mediator = this.mediator,
				firstFloor = mediator.firstFloor;

			t.ajaxJson({
				url:commonData.ajaxUrl.getRHListUrl,
				callback:function(result,status){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();

					var rhData = result.data;

					//只有单条数据的时候做特殊处理
					if(rhData.length == 1){
						pThis.rhId = rhData[0].id;
						pThis.goToRHTypeList(data,1);
						return;
					}

					tpl.register('dateFormat', t.dateFormat);
					var dom  = tpl(otherProcessTpl,{
						rhList:true,
						list:rhData
					});
					tpl.unregister('dateFormat');

					commonFuc.goFloor(firstFloor.html(dom));

					firstFloor.find(".rhClickObj").off().on("click",function(){
						var id = $(this).attr("data-id");
						pThis.rhId = id;

						mediator.setSaveKey("rhId",id);
						mediator.pageManager.go("pRHTypeList");
					});

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[rhData]);
					}
				}
			});
		},
		//获取润恒分类列表模板
		getHtmlForRHType:function(data,oneData){
			var pThis = this,
				secondFloor = this.mediator.secondFloor,
				firstFloor = this.mediator.firstFloor;

			var floorObj = oneData ? firstFloor : secondFloor;

			floorObj.html("");

			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			UiFramework.scrollLoadingPackage().init({
				target:floorObj,
				scroll_target:floorObj,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

					if(pThis.pageNum != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}
					
					t.ajaxJson({
						url:commonData.ajaxUrl.getRHTypeListUrl,///wx/index.php?model=index&m=msg&cmd=101
						data:{
							"data":{
								id:pThis.rhId || data.rhId,
								page:pThis.pageNum
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								commonFuc.error(result);
								return;
							}

							layerPackage.unlock_screen();

							var listData = result.data;

							tpl.register('dateFormat', t.dateFormat);
							var dom  = tpl(otherProcessTpl,{
								rhTypeList:true,
								listData:listData,
								type:data.type
							});
							tpl.unregister('dateFormat');

							if($.trim(floorObj.html()) != ""){
								for (var i = 0; i < listData.length; i++) {
									var o = floorObj.find("div.rhTypeList[data-typename="+listData[i].report_name+"]");

									if(o.length == 0){
										tpl.register('dateFormat', t.dateFormat);
										var dom  = tpl(otherProcessTpl,{
											rhTypeList:true,
											listData:[listData[i]],
											type:data.type
										});
										tpl.unregister('dateFormat');

										floorObj.append(dom);
									}else{
										tpl.register('dateFormat', t.dateFormat);
										var dom  = tpl(otherProcessTpl,{
											rhTypeList:true,
											listData:[listData[i]],
											type:data.type,
											noShow:1
										});
										tpl.unregister('dateFormat');

										o.last().after($.trim(dom));
									}
								};
							}else{
								commonFuc.goFloor(floorObj.html(dom));
							}
							
							pThis.pageCount += listData.length;
							
							pThis.pageNum ++;
							floorObj.attr("is_scroll_loading",false);

							if(listData.length < 10 
								|| listData.length == result.count 
								|| pThis.pageCount == result.count){
								floorObj.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting(floorObj);
						}
					});
				}
			});

			floorObj.scroll();
		},
		/*我的报表*/
		goToMyChart:function(data){
			//layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			this.getHtmlForChart(data);

			commonFuc.goFloor(this.mediator.firstFloor);
			//this.goToSecondFloor(1);
		},
		//获取我的报表模板
		getHtmlForChart:function(data){
			var pThis = this,
				mediator = this.mediator,
				firstFloor = mediator.firstFloor;

			firstFloor.html('<div id="listMenu"></div><div class="wxLine"></div><div class="advice_list scrollY" data-page="0" style="-webkit-height:calc(100% - 45px);height:calc(100% - 45px);"></div>');

			l.finit();
			var pThis = this;
			var listObj = firstFloor.find('.advice_list');
			var listUrl = "",contents = null;

			listUrl = commonData.ajaxUrl.getChartList;

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keywords = "";

			var getPageData = {
				init:function(keywords){
					if(!keywords){keywords="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					pThis.pageNum = 1,pThis.pageCount = 0; //当前页数

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}
							
							t.ajaxJson({
								url:listUrl,///wx/index.php?model=index&m=msg&cmd=101
								data:{
									"data":{
										page:pThis.pageNum,
										keywords:keywords,
										page_size:20
									}
								},
								callback:function(result,status){
									if(result.errcode !=0){
										commonFuc.error(result);
										return;
									}

									layerPackage.unlock_screen();

									var listData = result.data;
									console.log(listData);

									tpl.register('dateFormat', t.dateFormat);
									var dom  = tpl(otherProcessTpl,{
										myChart:true,
										listData:listData
									});
									tpl.unregister('dateFormat');

									if(listObj.find("section").length == 0 && listData.length == 0){
										listObj.find(".has-no-record").removeClass('hide');
										listObj.css("backgroundColor","#ffffff");
									}else if(listData.length != 0){
										listObj.find(".has-no-record").addClass('hide');
										listObj.css("backgroundColor","#F6F6F6");
										listObj.append(dom);
									}
									
									pThis.pageCount += listData.length;
									
									pThis.pageNum ++;
									listObj.attr("is_scroll_loading",false);

									if(listData.length < 20 
										|| listData.length == result.count 
										|| pThis.pageCount == result.count){
										listObj.attr("is_scroll_end",true);
									}

									self.remove_scroll_waiting(listObj);
								}
							});
						}
					});

					listObj.scroll();

					this.bindDom(firstFloor);
				},
				bindDom:function(obj){
					obj.off().on("click",".advice_list section",function(){
						var type = $(this).attr("data-type"),
							id = $(this).attr("data-id"); 

						if(type == 1){
							window.location.href = "/wx/index.php?debug=1&app=finstat&a=chart&id=" + id;
						}else if(type == 2){
							window.location.href = "/wx/index.php?app=finstat&m=ajax&a=report_detail&id="+id;
						}else if(type == 3){
							window.location.href = "/wx/index.php?app=expense&a=index&menuType=1&id="+id;
						}
					});
				}
			};

			getPageData.init("");

			//return;
			var setting = {
				//列表菜单配置
				target: firstFloor.find('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: []
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onSearchClick:function(key){
						keywords = key;
						getPageData.init(key);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/index/static/Contents/images/weixin_application_nothing01.jpg',//图片地址
					title:'暂时没有内容',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		},
		/*我的报销*/
		goToMyExaccount:function(data){
			commonFuc.changeWindowTitle(data.name);

			this.getHtmlForExaccount(data);

			commonFuc.goFloor(this.mediator.firstFloor);return;
			this.goToSecondFloor();
		},
		getHtmlForExaccount:function(data){
			var pThis = this,
				mediator = this.mediator,
				firstFloor = mediator.firstFloor;

				firstFloor.html('<div class="exaccountList"></div>');

			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			UiFramework.scrollLoadingPackage().init({
				target:firstFloor,
				scroll_target:firstFloor,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

					if(pThis.pageNum != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}
					
					t.ajaxJson({
						url:commonData.ajaxUrl.getExpenseListUrl,///wx/index.php?model=index&m=msg&cmd=101
						data:{
							"data":{
								id:pThis.rhId || data.rhId,
								page:pThis.pageNum
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								commonFuc.error(result);
								return;
							}

							layerPackage.unlock_screen();

							var exaccountData = result.info.data || [];

							tpl.register('dateFormat', t.dateFormat);
							var dom  = tpl(otherProcessTpl,{
								exaccountList:true,
								list:exaccountData
							});
							tpl.unregister('dateFormat');

							for (var i = 0; i < exaccountData.length; i++) {
								var d = exaccountData[i];
								
								pThis.getExaccountListHtml(firstFloor,d);
							};

							pThis.pageCount += exaccountData.length;

							if(pThis.pageCount == 0){
								firstFloor.find(".exaccountList").html(pThis.exaccountNoData());
							}
							
							pThis.pageNum ++;
							firstFloor.attr("is_scroll_loading",false);

							if(exaccountData.length < 10 
								|| exaccountData.length == result.info.count 
								|| pThis.pageCount == result.info.count){
								firstFloor.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting(firstFloor);
						}
					});
				}
			});

			firstFloor.scroll();

			firstFloor.on("click",".ecClick",function(){
				var id = $(this).attr("data-id");

				commonFuc.addClickBg($(this));

				window.location.href = "/wx/index.php?app=process&a=detail&listType=2&id="+id+"&activekey=0&isIndex=1&menuType=2#pMineList";
			});
		},
		getExaccountListHtml:function (obj,data) {
			var timeJson = t.getNowDate(parseInt(data.create_time)*1000,1);
			var year = timeJson.year,month = timeJson.month;
			var exaccountList = obj.find(".exaccountList");

			//分类
			var o = obj.find("section[data-year='"+year+"'][data-month='"+month+"']");

			if(o.length == 0){
				var html = new Array();
				html.push('<div class="wxLine"></div>');
				html.push('<section data-year="'+year+'" data-month="'+month+'" class="autoH bg-gray pt5 pb5">');
				html.push('<p class="ma0 c-black">'+year+'年'+month+'月</p>');
				html.push('</section>');
				//html.push('<div class="wxLine" data-year="'+year+'" data-month="'+month+'"></div>');
				exaccountList.append(html.join(''));
				exaccountList.append(this.getExaccountDetailHtml(data,timeJson));
			}else{
				o.eq(o.length - 1).after(this.getExaccountDetailHtml(data,timeJson));
			}

		},
		getExaccountDetailHtml:function (data,timeJson) {
			var status = data.pay_state,statusType = data.pay_type;
			var str = status == "1" ? "已支付":"未支付";

			if(status == "1"){
				/*if(statusType == 1){
					str += "（现金）";
				}else if(statusType == 2){
					str += "（转账）";
				}*/

				str += "（"+statusType+"）";
			}

			var html = new Array();

			html.push('<div class="wxLine"></div>');
			html.push('<section class="autoH ecClick" data-id="'+data.id+'" data-year="'+timeJson.year+'" data-month="'+timeJson.month+'">');
			html.push('<p class="c-black fs15 ma0">');
			html.push('¥'+t.formatMoney(data.sum_money)+' ');
			html.push('<i class="'+(status == "1" ? "bg-green" : "bg-red")+' c-white status fsn fs12 autoW">'+str+'</i>');
			html.push('<i class="c-4a fsn fs12 fr">'+data.expense_type+'</i>');
			html.push('</p>');
			html.push('<p class="mt5">');
			html.push('<i class="fsn hideTxt ec1">'+data.formsetinst_name+'</i>');
			html.push('<i class="fsn fr">'+timeJson.month+'-'+timeJson.days+' '+timeJson.hours+':'+timeJson.minutes+'</i>');
			html.push('</p>');
			html.push('</section>');

			return html.join('');
		},
		exaccountNoData:function(){
			var html = new Array();
			html.push('<div class="noRota tac">');
			html.push('<p class="noRotaIcon">');
			html.push('<span class="fs14 c-9b block tal">暂无报销记录喔</span>');
			//html.push('<span class="fs12 block tal">只有报销申请审批通过的表单</span>');
			//html.push('<span class="fs12 block tal">才回现显示在这里</span>');
			html.push('</p>');
			html.push('</div>');
			return html.join('');
		}
	};

	return otherProcess;	
});