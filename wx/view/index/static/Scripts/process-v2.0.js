define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl-v2.0.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc-v2.0',
	'index:static/Scripts/otherProcess-v2.0',
],function($,t,UiFramework,tpl,commonTpl,commonData,l,commonFuc,otherProcess) {
	var ajaxUrl = commonData.ajaxUrl;
	var layerPackage = UiFramework.layerPackage();
	//应用
	var process = {
		mediator:null,
		isIndex:"&isIndex=1",
		init:function(mediator){
			this.mediator = mediator;

			this.o = mediator.container;

			this.getHtml();

			otherProcess.init(this);

			this.getDealNumber();
		},
		//获取流程表单列表
		getHtml:function(){
			var pThis = this;

			$.ajax({
				url:ajaxUrl.getUserProcessUrl,// getUserProcessListUrl
				type:"post"
			}).done(function(r1){
				try{
					var r1Json = $.parseJSON(r1);

					if(r1Json.errcode != 0){layerPackage.fail_screen(r1Json.errmsg);return;}

					//流程表单
					var dom = tpl(commonTpl,{
						processList:r1Json.info.data,
						isNew:r1Json.info.is_new
					});

					pThis.o.find("#processList").removeClass('hide').html($($.trim(dom)));
					pThis.bindDom();

					//其他表单
					if(r1Json.info.server_list.length != 0){
						var arr = new Array();

						//公用定制模块
						var dom2 = tpl(commonTpl,{
							processList:r1Json.info.server_list,
							otherProcess:"otherProcess",
							isNew:r1Json.info.is_new
						});

						dom2 = $($.trim(dom2));

						pThis.bindOtherProcessDom(dom2);

						pThis.o.find("#otherProcessList").removeClass('hide').html(dom2);
					}else{
						pThis.o.find("#otherProcessList").addClass('hide').html('');
					}

					layerPackage.unlock_screen();
				}catch(e){
					layerPackage.fail_screen(e);
				}
			}).fail(function(){
				layerPackage.fail_screen("请求错误");
			});
		},
		//流程表单相关事件
		bindDom:function(){
			var pThis = this,
				mediator = this.mediator,
				pageManager = mediator.pageManager;

			this.o.find(".fixMain").on("scroll",function(e){
				var o = $(this);
				var top = o.scrollTop();
				//pThis.o.find(".fixTop").attr("style","-webkit-transform: translateY("+top+"px);");

				if(top == 0){
					pThis.o.find(".fixTop").removeClass('bgBottom');
				}else{
					pThis.o.find(".fixTop").addClass('bgBottom');
				}

			});

			//表单类型
			this.o.find("#listBtn > span").on("click",function(){
				var o = $(this);
				commonFuc.addClickBg(o);
				var id = o.attr("data-id");

				pThis.pId = id;

				if(id == "1"){
					pageManager.go("pDealList");
				}else if(id == "2"){
					pageManager.go("pMineList");
				}else if(id == "3"){
					pageManager.go("pNotifyList");
				}
			});

			//除了全部按钮外的表单事件
			this.o.find("#processList").on("click","span:not(.showAllProcess)",function(){
				var o = $(this);
				commonFuc.addClickBg(o);
				var id = o.attr("data-id");

				pThis.linkToProcess(id);
			});

			//全部按钮事件
			this.o.find("#processList > span.showAllProcess").on("click",function(){
				pageManager.go("pAllProcess");
			});
		},
		//获取表单类型列表模板
		getProcessHtml:function(pId){
			l.finit();
			var pThis = this;

			var firstFloor = this.mediator.firstFloor;
			var html = new Array();
			html.push('<div id="listMenu"></div>');
			html.push('<div class="wxLine"></div>');
			html.push('<div class="advice_list scrollY" data-page="0"></div>');
			html.push('<span class="backToIndex fs14 c-4a tac bg-white">返回主页型</span>');
			firstFloor.html(html.join(''));

			firstFloor.on("click",".backToIndex",function(){
				commonFuc.addClickBg($(this));
				pThis.mediator.pageManager.go('home',1);
			});

			//return;

			var listObj = firstFloor.find('.advice_list');
			var listUrl = "",contents = null,sortArr = null;
			var activekey = t.getHrefParamVal("activekey") || 0;

			if(this.pId !== undefined){
				activekey = 0;
			}

			try{
				var saveKey = this.mediator.getSaveKey("activekey");
				if(saveKey && (!activekey  || activekey !=0)){
					activekey = saveKey;
				}
			}catch(e){

			}

			if(pId == 1){
				listUrl = ajaxUrl.getDealListUrl;
				contents = [
					{'name':'未审批','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已审批','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'申请人','sort_name':'applyName','sort_id':1},
					/*{'name':'申请时间','sort_name':'applyTime','sort_id':2},*/
					{'name':'表单名称','sort_name':'proName','sort_id':3},
					{'name':'状态','sort_name':'status','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}else if(pId == 2){
				listUrl = ajaxUrl.getMineListUrl;
				contents = [
					{'name':'审批中','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已通过','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'草稿','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					/*{'name':'流程实例名','sort_name':'proName','sort_id':1},*/
					{'name':'表单名称','sort_name':'formName','sort_id':2}
				];
			}else if(pId == 3){
				listUrl = ajaxUrl.getNotifyListUrl;
				contents = [
					{'name':'未阅读','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已阅读','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					/*{'name':'申请时间','sort_name':'applyTime','sort_id':1},*/
					{'name':'发送人','sort_name':'applyName','sort_id':2},
					/*{'name':'流程名','sort_name':'proName','sort_id':3},*/
					{'name':'表单名称','sort_name':'formName','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keyworks = "",sort = 0,order = 1,listMenuObj = null;

			var getPageData = {
				init:function(activekey,keyworks,sort,order){
					if(sort === undefined){sort = 0;}
					if(order === undefined){order = 1;}
					if(!keyworks){keyworks="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}

							l.getList({
								"listUrl":listUrl,
								"listObj":listObj,
								"page":pThis.pageNum,
								"type":activekey,
								"sort":sort,
								"order":order,
								"pro_name":keyworks,
								"listType":pId,
								callback:{
									bindDom:function(res){
										var listType = res.listType,id = res.id;

										window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType="
										+pId+"&id="+id+"&activekey="+activekey+ pThis.isIndex+window.location.hash;
									}
								}
							},function(data,pCount){
								layerPackage.unlock_screen();
								//数据空时显示提示
								if(!data || data.length == 0){
									listMenuObj.showRecord();
									return;
								}

								//pageCount = pCount;
								pThis.pageCount += data.length;
								//if(pageCount == pCount){UiFramework.bottomInfo.init(listObj);return;}

								pThis.pageNum ++;
								listObj.attr("is_scroll_loading",false);

								if(data.length < 10 || data.length == pCount || pThis.pageCount == pCount){
									listObj.attr("is_scroll_end",true);
								}

								self.remove_scroll_waiting(listObj);

								UiFramework.bottomInfo.init(listObj);
							});
						}
					});

					listObj.scroll();
				}
			};

			var setting = {
				//列表菜单配置
				target: firstFloor.find('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: contents,
					activekey:activekey
				},
				sort:{ //配置头部插件排序
					status:true,
					contents:sortArr
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onMenuClick: function(data, obj,self){
						listMenuObj = self;
						akey = data.activekey;

						keyworks = "";

						if(akey == 1 && pId != 2){
			            	var newSortArr = t.clone(sortArr);
			            	/*newSortArr.push({'name':'处理时间','sort_name':'dealTime','sort_id':5});*/

			            	listMenuObj.resetSortData(newSortArr);
			            }else{
			        		listMenuObj.resetSortData(sortArr);
			            }

		            	listMenuObj.setSearchValue("");

			            listMenuObj.showRecord();

						getPageData.init(akey);

						if(akey == 0){
							pThis.mediator.removeSaveKey("activekey");
						}else{
							pThis.mediator.setSaveKey("activekey",akey);
							console.log(pThis.mediator.getSaveKey("activekey"));
						}
					},
					onSortClick:function(data){
						sort = data.sort_id;
						if(data.sort_type === undefined || data.sort_type == "asc"){
							order = 0;
						}else if(data.sort_type == "desc"){
							order = 1;
						}
						getPageData.init(akey,keyworks,sort,order);
					},
					onSearchClick:function(key){
						keyworks = key;
						getPageData.init(akey,key,sort,order);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/index/static/Contents/images/weixin_application_nothing01.jpg',//图片地址
					title:'暂时没有记录',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		},
		backToIndex:function(clearClickBg){
			commonFuc.changeWindowTitle(commonData.version);
			//this.geDealNumber();
			var mediator = this.mediator;
			mediator.removeSaveKey("rhId");//清除润恒保存的key值

			if(clearClickBg){
				commonFuc.removeClickBg($("span.clickBg"));
			}

			commonFuc.backFloor(mediator.firstFloor);

			//隐藏排序层
			$('#head—sort-screen').addClass('hide');
		},
		//待办/已办
		goToDealList:function(data){
			commonFuc.changeWindowTitle(data.name);
			var pId = this.pId || data.pId;

			this.getProcessHtml(pId);

			commonFuc.goFloor(this.mediator.firstFloor);
		},
		//我的发起
		goToMineList:function(data){
			this.goToDealList(data);
		},
		//我的知会
		goToNotifyList:function(data){
			this.goToDealList(data);
		},
		getDealNumber:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getDealCountUrl,
				callback:function(result,status){
					var jqObj = pThis.o.find("#listBtn").find("i.count");
					if(result.errcode !=0){
						jqObj.eq(0).text(0);
						jqObj.eq(1).text(0);
						jqObj.eq(2).text(0);
					}else{
						jqObj.eq(0).text(result.info.handler);
						jqObj.eq(1).text(result.info.apply);
						jqObj.eq(2).text(result.info.notify);
					}
				}
			});
		},
		//其他流程表单
		bindOtherProcessDom:function(dom){
			var pThis = this;

			this.o.on("click","span.otherProcess",function(){
				var o = $(this);
				var type = o.attr("data-type");

				commonFuc.addClickBg(o);

				/*润恒*/
				if(type == "runheng"){
					pThis.mediator.pageManager.go("pRHList");
				}
				/*我的报表*/
				else if(type == "report"){
					pThis.mediator.pageManager.go("pMyChart");
				}
				/*我的考勤*/
				else if(type == "checkwork"){
					history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index");
					window.location.href = "/wx/index.php?debug=1&app=workshift&a=timeTag";
				}
				/*我的报销*/
				else if(type == "exaccount"){
					pThis.mediator.pageManager.go("pMyExaccount");
				}
			});
		},
		//润恒
		goToRHList:function(data){
			otherProcess.goToRHList(data);
		},
		//润恒分类列表
		goToRHTypeList:function(data){
			otherProcess.goToRHTypeList(data);
		},
		backToRHList:function(){
			otherProcess.backToRHList();
		},
		//我的报表
		goToMyChart:function(data){
			otherProcess.goToMyChart(data);
		},
		//----------全部应用--------------
		//获取全部表单数据
		getAllProcess:function(callback){
			var pThis = this;
			layerPackage.lock_screen();

			t.ajaxJson({
				url:ajaxUrl.getAllProcessUrl,
				callback:function(result,state){
					if(result.errcode != 0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();//遮罩end
					if(callback && t.isFunction(callback)){
						pThis.allProcessData = result.info;
						callback.apply(pThis,[result.info]);
					}
				}
			});
		},
		//跳转到全部应用
		goToAllProcess:function(data){
			commonFuc.changeWindowTitle(data.name);

			this.getAllProcess(function(info){
				var dom = tpl(commonTpl,{
					allProcess:1,
					data:info
				});

				var thirdFloor = this.mediator.thirdFloor;

				//PC作另外一种处理
				if(platid == 4 || platid == 5){
					thirdFloor.addClass('bg-white').removeClass("hide animated slideOutRight").html($($.trim(dom)));
				}else{
					commonFuc.goFloor(thirdFloor.addClass('bg-white').html($($.trim(dom))));
				}

				this.bindAllProcess(thirdFloor);
			});
		},
		canClick:false,
		//全部应用页面 事件
		bindAllProcess:function(dom){
			var pThis = this;
			/*dom.on("scroll",function(e){
				var o = $(this);
				var top = o.scrollTop();
				o.find(".apTop").attr("style","-webkit-transform: translateY("+top+"px);");
			});*/

			//编辑
			dom.find(".eidtUserProcess").on("click",function(){
				var o = $(this),
					p = o.parents(".allProcess"),
					click = o.attr("data-click");

				if(click == "0"){
					o.text("完成");
					var span = p.find("span");
					span.removeClass('noBorder').find("i.rp").removeClass('hide');
					o.attr("data-click","1");
					pThis.canClick = true;
					//隐藏new图片
					span.find("b.isnewIcon").addClass('hide');
				}else if(click == "1"){
					var div_u = dom.find(".userProcess");
					var arr = new Array();

					div_u.find("span:not(.noUserProcess span)").each(function(){
						var id = $(this).attr("data-id");

						if(id){
							arr.push(id);
						}
					});

					function changeTxt(){
						//当前页面的处理
						o.text("管理");
						var span = p.find("span");
						span.addClass('noBorder').find("i.rp").addClass('hide');
						o.attr("data-click","0");
						pThis.canClick = false;
						//显示new图片
						span.find("b.isnewIcon").removeClass('hide');
					}

					if(arr.join(",") == pThis.allProcessData.c_ids.join(",")){
						changeTxt();
					}else{
						pThis.saveUserProcess(arr,function(){
							pThis.allProcessData.c_ids = arr;

							//表单页面的处理
							var container = pThis.mediator.container;
							var allSpan = container.find(".showAllProcess");
							console.log(allSpan.index());
							container.find("#processList span:lt("+allSpan.index()+")").remove();

							var clone_span = div_u.find("span:not(.noUserProcess span):lt(8)").clone();
							clone_span.find("i.rp").remove();

							container.find("#processList").prepend(clone_span);

							changeTxt();
						});
					}
				}
			});

			//已选择表单事件
			dom.find(".userProcess").on("click","span:not(.noUserProcess span)",function(){
				var o = $(this),
					div = o.parent();

				commonFuc.addClickBg(o);

				if(!pThis.canClick){
					pThis.linkToProcess(o.attr("data-id"));
					return;
				}

				o.find("i.rp").removeClass('rulericon-cancel_border bg-e4').addClass('rulericon-add_border bg-green');

				commonFuc.removeClickBg(o);

				dom.find(".otherProcessList").prepend(o);

				var len = div.find("span:not(.noUserProcess span)").length;

				if(len == 0){
					div.addClass('noData').find(".noUserProcess").removeClass('hide');
				}else if(len <= 8){
					dom.find(".noticeMsg").addClass('hide');
				}
			});

			//未选择表单事件
			dom.find(".otherProcessList").on("click","span",function(){
				var o = $(this),
					div = o.parent();

				commonFuc.addClickBg(o);

				if(!pThis.canClick){
					pThis.linkToProcess(o.attr("data-id"));
					return;
				}

				var div_u = dom.find(".userProcess");
				var len = div_u.find("span:not(.noUserProcess span)").length;

				if(len == 0){
					div_u.removeClass('noData').find(".noUserProcess").addClass('hide');
				}else if(len > 7){
					dom.find(".noticeMsg").removeClass('hide');
				}

				o.find("i.rp").removeClass('rulericon-add_border bg-green').addClass('rulericon-cancel_border bg-e4');

				commonFuc.removeClickBg(o);

				div_u.append(o);
			});
		},
		//保存表单设置
		saveUserProcess:function(form_ids,callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.saveUserProcessUrl,
				data:{
					data:{
						form_ids:form_ids
					}
				},
				callback:function(result,state){
					if(result.errcode != 0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();//遮罩end
					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[]);
					}

					commonFuc.success(result);
				}
			})
		},
		//跳转到流程表单模块
		linkToProcess:function(id){
			var hash = window.location.hash || "";

			// if(!hash){
			// 	history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index");
			// }

			window.location.href = "/wx/index.php?model=process&a=create&debug=1&id="+ id + this.isIndex+"&activekey=2#pMineList";
		},
		//全部应用返回首页
		allProcessBackToIndex:function(){
			commonFuc.changeWindowTitle(commonData.version);

			var mediator = this.mediator,
				thirdFloor = mediator.thirdFloor;
			thirdFloor.find(".eidtUserProcess").attr("data-click","0");
			this.canClick = false;

			//去除new标签
			mediator.container.find("b.isnewIcon").remove();

			var hash = window.location.hash || "";
			//history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2"+hash);

			commonFuc.backFloor(thirdFloor);
		},
		/*我的报销*/
		goToMyExaccount:function(data){
			otherProcess.goToMyExaccount(data);
		}
	};

	return process;
});