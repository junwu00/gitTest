define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/commonFuc'
],function($,t,UiFramework,tpl,commonTpl,commonData,commonFuc) {
	var ajaxUrl = commonData.ajaxUrl;
	//搜索相关
	var searchObj = {
		mediator:null,
		type:0,
		init:function(mediator){
			this.mediator = mediator;
			var container = mediator.container;

			this.searchPart = container.find("#searchPart"),
			this.search = container.find("li.news .search"),
			this.searchForm = this.search.find("form"),
			this.searchInput = this.searchForm.find("input"),
			this.searchRemove = this.searchForm.find(".searchRemove"),
			this.searchCancel = this.searchForm.find(".searchCancel");

			this.searchInput.val("");

			this.bindDom();

			this.listPart = container.find("#searchOneList");
			this.listSearch = this.listPart.find(".search");
			this.listSearchInput = this.listPart.find("input");
			this.listSearchRemove = this.listPart.find(".searchRemove");
			this.listSearchCancel = this.listPart.find(".searchCancel");
			this.bindOneListDom();


			this.bindCommonEvent(this.searchPart);
			this.bindCommonEvent(this.listPart);
		},
		getMsgTypeHtml:function(callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getMsgTypeUrl,
				callback:function(result,state){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[result.list]);
					}
				}
			});
		},
		getMsgTypeClass:function(type,appId){
			//未阅读
			if(type == -1 && appId == -1){
				return "rulericon-eye";
			}
			//小助手
			else if(type == 0 && appId == 0){
				return "rulericon-rabort";
			}
			//润恒
			else if(type == 1 && appId == 0){
				return "rulericon-runheng";
			}
			//报表
			else if(type == 2 && appId == 0){
				return "rulericon-formdata";
			}
			//待办
			else if(type == 1 && appId == 23){
				return "rulericon-signature";
			}
			//通过
			else if(type == 2 && appId == 23){
				return "rulericon-request";
			}
			//知会
			else if(type == 3 && appId == 23){
				return "rulericon-get";
			}
			//定位
			else if(type == 3 && appId == 25){
				return "rulericon-plain";
			}
			//排班
			else if(type == 4 && appId == 25){
				return "rulericon-calendar";
			}
			else{
				return "rulericon-feekback";
			}
		},
		bindMsgTypeDom:function(dom){
			var pThis = this;
			dom.on("click","li",function(e){
				e.preventDefault();

				var o = $(this),type = o.attr("data-type"),appId = o.attr("data-appid");

				pThis.searchParam = {
					key : "",
					page : 1,
					type : type,
					appId : appId,
					pageSize:20,
					nextPage : true
				};

				pThis.searchAjax({
					key_word:"",
					type:type,
					app_id:appId,
					page:1,
					page_size:pThis.searchParam.pageSize
				},function(data){
					pThis.listSearch.addClass('ing');
					if($.trim(pThis.searchParam.key) != ""){
						pThis.listSearchRemove.removeClass('hide');
					}
					pThis.listSearchCancel.removeClass('hide');

					pThis.listSearchInput.val(pThis.searchParam.key);
					pThis.listSearchInput.keyup();

					pThis.listPart.removeClass("hide");
					pThis.listPart.find(".f1:first").addClass('hide'); 
					if(data.length == 0){
						pThis.listPart.find(".f1:first").removeClass('hide'); 
						pThis.listPart.find(".dataList").html("");
						return;
					}

					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						hideBar:true,
						isIndex:"&isIndex=1",
						menuType:"&menuType=1",
						hash:window.location.hash
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.listPart.find(".dataList").html(o);

					if(data[0].list && data[0].list.length == pThis.searchParam.pageSize){
						pThis.searchParam.page ++;
						pThis.searchParam.nextPage = true;
					}else{
						pThis.searchParam.nextPage = false;
					}
					
					//pThis.searchOneData = data;
				});
			});
		},
		bindDom:function(){
			var pThis = this;
			this.searchInput.on("focus",function(){
				pThis.searchPart.removeClass('hide');
				pThis.search.addClass('ing');
				pThis.searchCancel.removeClass('hide');

				if(pThis.searchPart.find("section").length == 0){
					pThis.searchPart.find("p.f1").removeClass('hide');
				}

				if(!pThis.firstLoadType){
					pThis.getMsgTypeHtml(function(data){
						tpl.register('getMsgTypeClass',this.getMsgTypeClass);
						var dom = $($.trim(tpl(commonTpl,{
							data:data,
							msgType:1
						})));
						tpl.unregister('getMsgTypeClass');

						var o = this.searchPart.find("ul");
						o.html(dom);
						this.bindMsgTypeDom(o);
					});

					pThis.firstLoadType = true;
				}

				pThis.mediator.container.addClass('noBottom');
				pThis.mediator.footer.addClass("hide");
			});

			this.searchInput.on("keyup",function(){
				var key = $.trim($(this).val());

				if(key == ""){
					pThis.searchRemove.addClass('hide');
					pThis.searching();
				}else{
					pThis.searchRemove.removeClass('hide');
				}
			});

			this.searchRemove.on("click",function(){
				pThis.searchInput.val("");
				$(this).addClass('hide');
				pThis.searching();
			});

			this.searchCancel.on("click",function(){
				pThis.searchPart.addClass('hide')
				pThis.search.removeClass('ing');
				$(this).addClass('hide');
				pThis.searchRemove.addClass('hide');
				pThis.searchInput.val("");
				pThis.searching();

				pThis.mediator.container.removeClass('noBottom');
				pThis.mediator.footer.removeClass("hide");
			});

			this.searchForm.on('submit',function(e) {
				 e.preventDefault();

				var key = $.trim(pThis.searchInput.val());
				if(key == ""){
					pThis.searching();
					return;
				}

				pThis.searchPart.find(".f1").addClass('hide');
				pThis.searchPart.find(".f2").remove();
				pThis.searchPart.find("ul").addClass('hide');

				pThis.searchAjax({
					key_word:key
				},function(data){
					if(data.length == 0){
						pThis.searchPart.removeClass('bg-gray');
						pThis.searchPart.find(".f1:first").removeClass('hide').text("暂时没有内容"); 
						pThis.searchPart.find("ul").removeClass('hide');
						return;
					}

					pThis.searchPart.find(".f1").addClass('hide');

					pThis.searchPart.addClass('bg-gray');
					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						isIndex:"&isIndex=1",
						menuType:"&menuType=1",
						hash:window.location.hash
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.searchPart.find("section").remove();
					pThis.searchPart.find(".wxLine").remove();
					pThis.searchPart.append(o);
					pThis.bindSearchDom();
				});
			});	
		},
		searching:function(){
			this.searchPart.removeClass('bg-gray');
			this.searchPart.find(".f1").removeClass('hide');
			this.searchPart.find(".f2").remove();
			this.searchPart.find(".f1:first").removeClass('hide').html("搜索内容");
			this.searchPart.find("ul").removeClass('hide');
			this.searchParam = {};
		},
		//搜索标记
		searchMark:function(title){
			var key = $.trim(searchObj.searchInput.val());
			if(key == ""){return title;}

			var reg = new RegExp(key,"g");

			return title.replace(reg, '<span class="searchMark">'+key+'</span>');
		},
		searchAjax:function(params,callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.searchNewsUrl,
				data:{
					data:params
				},
				callback:function(result,status){
					console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					var data = result.data;

					if(callback && t.isFunction(callback)){
						callback(data);
					}
				}
			});
		},
		bindSearchDom:function(){
			var pThis = this;
			this.searchPart.find(".findMore").on('click',function(){
				var p = $(this).parent();
				var type = p.attr("data-type"),
					appId = p.attr("data-appid");

				var key = $.trim(pThis.searchInput.val());
				if(key == ""){
					pThis.searching();
					return;
				}
				
				pThis.searchParam = {
					key : key,
					page : 1,
					type : type,
					appId : appId,
					pageSize:20,
					nextPage : true
				};

				pThis.searchAjax({
					key_word:key,
					type:type,
					app_id:appId,
					page:1,
					page_size:pThis.searchParam.pageSize
				},function(data){
					pThis.listSearch.addClass('ing');
					if($.trim(pThis.searchParam.key) != ""){
						pThis.listSearchRemove.removeClass('hide');
					}
					pThis.listSearchCancel.removeClass('hide');

					pThis.listSearchInput.val(pThis.searchParam.key);
					pThis.listSearchInput.keyup();

					pThis.listPart.removeClass("hide");
					pThis.listPart.find(".f1:first").addClass('hide'); 
					if(data.length == 0){
						pThis.listPart.find(".f1:first").removeClass('hide'); 
						pThis.listPart.find(".dataList").html("");
						return;
					}

					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						hideBar:true,
						isIndex:"&isIndex=1",
						menuType:"&menuType=1",
						hash:window.location.hash
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.listPart.find(".dataList").html(o);

					if(data[0].list && data[0].list.length == pThis.searchParam.pageSize){
						pThis.searchParam.page ++;
						pThis.searchParam.nextPage = true;
					}else{
						pThis.searchParam.nextPage = false;
					}
					
					//pThis.searchOneData = data;
				})
			});
		},
		bindOneListDom:function(){
			var pThis = this;
			var dataList = this.listPart.find(".dataList");

			this.listSearchInput.on("focus",function(){
				pThis.listSearch.addClass('ing');
				if($.trim($(this).val()) != ""){
					pThis.listSearchRemove.removeClass('hide');
				}
				pThis.listSearchCancel.removeClass('hide');
			});

			this.listSearchInput.on("keyup",function(){
				var key = $.trim($(this).val());

				if(key == ""){
					pThis.listSearchRemove.addClass('hide');
					pThis.listPart.find(".dataList").html("");
					pThis.listPart.find("form").submit();
				}else{
					pThis.listSearchRemove.removeClass('hide');
				}
			});

			this.listSearchRemove.on("click",function(){
				pThis.listSearchInput.val("");
				$(this).addClass('hide');
				pThis.listPart.find(".dataList").html("");
				pThis.listPart.find("form").submit();
			});

			this.listSearchCancel.on("click",function(){
				pThis.listSearch.removeClass('ing');
				$(this).addClass('hide');
				pThis.listSearchRemove.addClass('hide');
				pThis.listSearchInput.val("");
				pThis.listPart.addClass('hide');
				pThis.listPart.find(".dataList").html("");

				dataList.attr("is_scroll_loading",false);
				dataList.attr("is_scroll_end",false);

				pThis.searchCancel.click();
			});

			this.listPart.find("form").on('submit',function(e) {
				 e.preventDefault();

				var key = $.trim(pThis.listSearchInput.val());
				if(key == ""){
					//pThis.listPart.find(".dataList").html("");
					//return;
				}

				searchObj.searchInput.val(key);

				pThis.listPart.find(".f1").addClass('hide');
				dataList.scrollTop(0);
				dataList.attr("is_scroll_loading",false);
				dataList.attr("is_scroll_end",false);
				pThis.searchParam.key = key;
				pThis.searchParam.nextPage = true;
				pThis.searchParam.page = 2;

				pThis.searchAjax({
					key_word:key,
					type:pThis.searchParam.type,
					app_id:pThis.searchParam.appId,
					page:1,
					page_size:pThis.searchParam.pageSize
				},function(data){
					if(data.length == 0){
						pThis.listPart.find(".f1:first").removeClass('hide'); 
						pThis.listPart.find(".dataList").html("");
						return;
					}

					tpl.register('searchMark', pThis.searchMark);
					var dom = tpl(commonTpl, {
						searchPartList:data,
						defaultFace:defaultFace,
						hideBar:true,
						isIndex:"&isIndex=1",
						menuType:"&menuType=1",
						hash:window.location.hash
					});
					tpl.unregister('searchMark');

					var o = $($.trim(dom));

					pThis.listPart.find(".dataList").html(o);
				});
			});

			
			UiFramework.scrollLoadingPackage().init({
				target:dataList,
				scroll_target:dataList,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}
					if(!pThis.searchParam.nextPage){
						dataList.attr("is_scroll_loading",false);
						dataList.attr("is_scroll_end",true);
						return;
					}

					if(pThis.searchParam.page != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}

					pThis.searchAjax({
						key_word:pThis.searchParam.key,
						type:pThis.searchParam.type,
						app_id:pThis.searchParam.appId,
						page:pThis.searchParam.page,
						page_size:pThis.searchParam.pageSize
					},function(data){
						if(data.length == 0){
							dataList.attr("is_scroll_loading",false);
							dataList.attr("is_scroll_end",true);
							pThis.searchParam.nextPage = false;
							return;
						}

						tpl.register('searchMark', pThis.searchMark);
						var dom = tpl(commonTpl, {
							searchPartList:data,
							defaultFace:defaultFace,
							hideBar:true,
							getList:true,
							isIndex:"&isIndex=1",
							menuType:"&menuType=1",
							hash:window.location.hash
						});
						tpl.unregister('searchMark');

						var o = $($.trim(dom));

						pThis.listPart.find(".dataList > section").append(o);
						dataList.attr("is_scroll_loading",false);

						self.remove_scroll_waiting(dataList);

						pThis.searchParam.page ++;
					});
				}
			});
		},
		bindCommonEvent:function(dom){
			var pThis = this;
			dom.on("click","section.sUrl",function(){
				console.log(1);
				var o = $(this);
				var url = o.find("input[name=linkUrl]").val();

				//链接不为空时跳转,否则打开详情
				if(url){
					url = t.checkUrl(url);
					var hash = "";

					if(url.lastIndexOf("#") > 0){
						hash = url.substring(url.lastIndexOf("#"),url.length);
						url = url.substring(0,url.lastIndexOf("#"));
					}

					url = url + "&isIndex=1&menuType=1" + hash || window.location.hash;

					history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=1");

					var pageUrl = url.substring(url.lastIndexOf("#") + 1);
					if(pageUrl != "nVip"){
						window.location.href = url;
					}else{
						pThis.hideSeachObj();
						pThis.mediator.pageManager.go(pageUrl);
					}
				}else{
					pThis.hideSeachObj();

					pThis.mediator.newsObj.viewData = {
						title:o.find(".searchTitle").text(),
						content:o.find(".searchContent").text(),
						user_pic:o.find("img").attr("src"),
						time:t.getShortTime(o.find("input[name=time]").val(),1),
						pic:o.find("input[name=pic]").val()
					};

					pThis.mediator.pageManager.go("nMsgInfo");
				}
			});
		},
		hideSeachObj:function(){
			this.mediator.newsObj.searching = 1;
			var listPart = this.listPart;
			var searchPart = this.searchPart;

			if(!searchPart.hasClass('hide') && listPart.hasClass('hide')){
				searchPart.attr("data-hide",1);
				searchPart.addClass('hide');
				return;
			}

			searchPart.addClass('hide');
			listPart.addClass('hide');
		},
		showSeachObj:function(){
			var listPart = this.listPart;
			var searchPart = this.searchPart;

			if(searchPart.attr("data-hide") == 1){
				searchPart.removeClass('hide');
				searchPart.removeAttr('data-hide');
			}else{
				listPart.removeClass('hide');
				searchPart.removeClass('hide');
			}
		}
	};

	return searchObj;
});