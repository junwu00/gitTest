define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc',
	'index:static/Scripts/vipCtrl',
	'common:widget/lib/GenericFramework'
],function($,t,UiFramework,tpl,commonTpl,commonData,l,commonFuc,vipCtrl,GenericFramework) {
	var ajaxUrl = commonData.ajaxUrl,
		layerPackage = UiFramework.layerPackage();
	//个人信息
	var personalObj = {
		init:function(mediator){
			this.mediator = mediator;

			this.o = mediator.container.find("li.personalInfo");

			this.getInfoHtml();

			vipCtrl.init(this);

		},
		getInfoHtml:function(callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getMyInfoUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					var info = result.info;
					if(callback && t.isFunction(callback)){
						callback(info);
						return;
					}
					pThis.info = info;
					var dom = tpl(commonTpl, {
						personalInfo:info,
						defaultFace:defaultFace,
						userInfo:userInfo,
						platid:platid
					});

					var html = $($.trim(dom));

					pThis.o.html(html);
					pThis.bindDom();

					//移动端才显示
					
					pThis.getCount(info);
				}
			});
			
		},
		getCount:function(info){
			var count = 0
			for(var i in info.onetime_msg){
				count ++ ;
			}

			var o = this.mediator.fLis.eq(2).find("i.countNum");

			try{
				if(userInfo.isAdmin != 1){
					var arr = new Array();
					for (var i = 0; i < commonData.onetimeMsg.length; i++) {
						if(commonData.onetimeMsg[i] != "admin_service"){
							arr.push(commonData.onetimeMsg[i]);
						}
					};

					commonData.onetimeMsg = arr;
				}
			}catch(e){
				console.log(e);
			}

			if((platid == 1 || platid == 2) && count != commonData.onetimeMsg.length){
				try{
					o.removeClass('hide');
				}catch(e){
					console.log(e);
				}
			}else{
				o.addClass('hide');
			}
		},
		bindDom:function(){
			var pThis = this;
			this.o.on("click",".module section",function(){
				var id = $(this).attr("data-id"),key = $(this).attr("data-key");
				if(!id){return;}
				$(this).addClass('clickBg');
				pThis.mId = id;
				
				pThis.goPage(id,key,$(this));
			});

			this.o.on("click",".editInfo",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				pThis.mId = id;

				pThis.goPage(id);	
			});
			
			var ua= window.navigator.userAgent.toLowerCase();
			// 如果是企业微信  隐藏指纹按钮
			if (ua.indexOf('micromessenger') !== -1 && ua.indexOf('wxwork') !== -1) {
				this.o.find("#soterService").addClass('hide')
			}
		},
		goPage:function(mId,key,obj){
			if(key){
				commonFuc.onetimeMsgClick(key,obj);
				this.info.onetime_msg[key] = 1;
				this.getCount(this.info);
			}

			var pageManager = this.mediator.pageManager;
			/*产品服务支持*/
			if(mId == 1){
				pageManager.go("mProduct");
			}
			else if(mId == 2){
				pageManager.go("mVip");
			}
			else if(mId == 3){
				pageManager.go("mAdminService");
			}
			else if(mId == 4){
				pageManager.go("mEditInfo");
			}
			//推荐企业购买
			else if(mId == 5){
				//pageManager.go("mVipService3");
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=3");
			}
			//指纹识别设置
			else if(mId == 6){
				try{
					if(this.canSupport){
						pageManager.go("mSoterPage1");
					}else{
						var pThis = this;
						GenericFramework.init(wxJsdkConfig,1);
						GenericFramework.getSupportSoter(function(canSupport){
							pThis.canSupport = canSupport;
							
							pageManager.go("mSoterPage1");
						});
					}
				}catch(e){
					console.log(e);
				}
			}
		},
		backToIndex:function(){
			this.mediator.fLis.eq(2).click();

			this.o.find("section.clickBg").removeClass('clickBg');
		},
		/*产品服务支持*/
		bindProductServiceDom:function(){
			var pThis = this,
				mediator = this.mediator,
				mFirstDom = mediator.mFirstDom,
				pageManager = mediator.pageManager;
			//常见问题
			mFirstDom.find(".questions section").off().on("click",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				$(this).addClass('clickBg');

				return;

				pThis.qId = id;
				pThis.getQuestionDetail(id);

				setTimeout(function(){
					pageManager.go('mQuestionDetail');
				},100);
			});

			mFirstDom.find(".pServices section").off().on("click",function(){
				var id = $(this).attr("data-id");
				commonFuc.addClickBg($(this));

				pThis.sId = id;

				setTimeout(function(){
					//角色体验
					if(id == 1){
						pageManager.go("mRole");
					}
					/*产品案例 直接跳转*/
					else if(id == 2){
						//window.location.href = "http://www.baidu.com";
						//mFirstDom.find("section.clickBg").removeClass("clickBg");
						pageManager.go("mProductDemo");
					}
					//使用指引
					else if(id == 3){
						pageManager.go("mUseHelper");
					}
				},100);
			});
		},
		/*常见问题*/
		getQuestionHtml:function(data){
			var html = new Array();
			html.push("<div class='questionDetail bg-white fs13'>");
			html.push('<span class="title fs18">'+data.title+'</span>');
			html.push('<p class="mt15">'+data.content+'</p>');
			html.push("</div>");
			return html.join("");
		},
		getQuestionData:function(id){
			var questions = commonData.questions;
			for (var i = 0; i < questions.length; i++) {
				if(questions[i].id == id){
					return questions[i];
				}
			};
		},
		getQuestionDetail:function(id){
			var mediator = this.mediator;

			var questionsPart = mediator.mFirstDom.find(".questions").clone();
			questionsPart.find(".clickBg").removeClass('clickBg');
			questionsPart.find(".sTitle").text("你可能需要");//相关文章
			var selfPart = questionsPart.find("section[data-id="+id+"]");
			selfPart.addClass('hide');
			selfPart.next("div.wxLine").addClass('hide');

			mediator.mSecondDom
			.html(this.getQuestionHtml(this.getQuestionData(id)))
			.append(questionsPart);

			UiFramework.bottomInfo.init(mediator.mSecondDom);
		},
		bindQuestionDetailDom:function(){
			var pThis = this,
				mSecondDom = this.mediator.mSecondDom;
			mSecondDom.find(".questions section").off().on("click",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				var data = pThis.getQuestionData(id);
				mSecondDom.find(".questions .hide").removeClass('hide');
				$(this).addClass('hide');
				$(this).next("div.wxLine").addClass('hide');

				mSecondDom.find(".questionDetail .title").text(data.title);
				mSecondDom.find(".questionDetail p").text(data.content);
			});
		},
		/*产品服务*/
		getServiceHtml:function(sId){
			var dom = tpl(commonTpl,{
				pService:{
					id:sId
				},
				commonData:commonData
			});

			var o = $($.trim(dom));
			return o;
		},
		/*个人编辑*/
		bindEditInfoDom:function(){
			var mediator = this.mediator,pageManager = mediator.pageManager;
			var o = mediator.mFirstDom.find(".editInfoPart"),pThis = this;

			o.on("click",".editThis",function(){
				var id = $(this).attr("id");
				pThis.ePart = id;

				commonFuc.addClickBg($(this));

				if(id == "gender"){
					pageManager.go("mEditGender");
				}else if(id == "phone"){
					pageManager.go("mEditPhone");
				}else if(id == "mail"){
					pageManager.go("mEditEmail");
				}
			});
		},
		getEditInfoHtml:function(ePart){
			var dom = tpl(commonTpl,{
				editInfo:{
					ePart:ePart
				},
				commonData:commonData,
				info:this.info
			});

			var o = $($.trim(dom));
			return o;
		},
		bindEditDom:function(){
			var pThis = this,
				mediator = this.mediator,
				mSecondDom = mediator.mSecondDom,
				mFirstDom = mediator.mFirstDom;
			mSecondDom.find(".search input").on("keyup",function(){
				if($.trim($(this).val()) == ""){
					$(this).next("i").addClass('hide');
				}else{
					$(this).next("i").removeClass('hide');
				}
			});

			mSecondDom.find(".search i").on("click",function(){
				$(this).prev("input").val("");
				$(this).addClass("hide");
			});

			mSecondDom.find(".genderEdit").on("click","section:not(:first)",function(){
				var id = $(this).attr("data-id"),
					text = $(this).text();
				$(this).addClass('clickBg');
				if(pThis.info.gender == id){
					window.history.go(-1);
					return;
				}
				mSecondDom.find(".genderEdit").find("i").addClass('hide');
				$(this).find("i").removeClass('hide');
				

				pThis.saveEditInfo({
					gender:id
				},function(result){
					commonFuc.success(result);
					pThis.info.gender = id;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#gender").find("em").text(text);
				});
			});

			mSecondDom.find(".phoneEdit").on("click",".save",function(){
				var val = mSecondDom.find(".phoneEdit").find("input").val();

				if(!t.regCheck(val,"mobile")){
					layerPackage.fail_screen("请填写正确的手机号码！");
					return;
				}

				pThis.saveEditInfo({
					mobile:val
				},function(result){
					commonFuc.success(result);
					pThis.info.mobile = val;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#phone").find("em").text($.trim(val));
				});
				//pageManager.go("mEditPhone",1);
			});

			mSecondDom.find(".mailEdit").on("click",".save",function(){
				var val = mSecondDom.find(".mailEdit").find("input").val();
				if(!t.regCheck(val,"email")){
					layerPackage.fail_screen("请填写正确的邮箱地址！");
					return;
				}
				//pageManager.go("mEditEmail",1);
				pThis.saveEditInfo({
					email:val
				},function(result){
					commonFuc.success(result);
					pThis.info.email = val;
					//pageManager.go("mEditInfo",1);
					window.history.go(-1);
					mFirstDom.find("#mail").find("em").text($.trim(val));
				});
			});
		},
		//----------
		getMineHtml:function(id,changeUrl){
			var mediator = this.mediator;
			if(mediator.mFirstDom.html() != ""){
				//return;
			}

			if(id === undefined){id = 1;}

			var params = {
				module:{id:id}
			};
			if(id == 1){
				params.data = commonData.questions;
			}
			else if(id == 4 || id == 2){
				params.info = this.info;
				params.defaultFace = defaultFace;
				params.userInfo = userInfo;

				if(!params.info && id == 4){
					window.history.go(-1);
					return;
				}
			}

			var dom = null;

			if(id == 2){
				dom = vipCtrl.getHtml(params,changeUrl);
			}else{
				dom = tpl(commonTpl,params);
			}

			var o = $($.trim(dom));

			mediator.mFirstDom.html(o);

			if(id == 1){
				this.bindProductServiceDom();
				this.pcSolved();
			}else if(id == 2){
				//vipCtrl.bindDom(mediator.mFirstDom,changeUrl);
			}else if(id == 3){
				UiFramework.bottomInfo.init(mediator.mFirstDom);
			}else if(id == 4){
				this.bindEditInfoDom();
			}
		},
		goToFirstFloor:function(flag){
			var mediator = this.mediator;
			
			this.noShowBottom(flag);

			mediator.fLis.eq(5).click();
		},
		goToSecondFloor:function(){
			var mediator = this.mediator;
			mediator.footer.addClass('none');
			mediator.container.addClass('noBottom');
			mediator.fLis.eq(6).click();
		},
		//产品服务与支持
		goToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getMineHtml(mId);
			this.bindProductServiceDom();
			this.goToFirstFloor();
		},
		backToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			//this.goToFirstFloor();
			this.mediator.pageManager.go("mProduct",1);
		},
		//角色体验
		goToRole:function(data){
			commonFuc.changeWindowTitle(data.name);
			this.getMineHtml();

			var sId = this.sId || data.sId;

			this.mediator.mSecondDom.html(this.getServiceHtml(sId));
			this.goToSecondFloor();
		},
		//使用指引
		goToUseHelper:function(data){
			this.goToRole(data);
		},
		goToProductDemo:function(data){
			this.goToRole(data);
		},
		//常见问题详情
		goToQuestionDetail:function(data){
			commonFuc.changeWindowTitle(data.name);
			this.getMineHtml();

			var qId = this.qId || 1;
			this.getQuestionDetail(qId);
			this.bindQuestionDetailDom();

			this.goToSecondFloor();
		},
		//VIP
		goToVip:function(data,changeUrl){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getMineHtml(mId,changeUrl);
			
			this.goToFirstFloor();
		},
		backToVip:function(data,changeUrl){
			if(changeUrl){
				this.mediator.pageManager.go("nVip",1);
				return;
			}

			this.mediator.pageManager.go("mVip",1);
		},
		//管理员服务中心
		goToAdminService:function(data){
			this.goToVip(data);
		},
		//编辑个人信息第一层
		goToEditInfo:function(data){
			this.goToVip(data);
		},
		backToEditInfo:function(){
			this.mediator.pageManager.go("mEditInfo",1);
		},
		//编辑性别
		goToEditGender:function(data){
			commonFuc.changeWindowTitle(data.name);

			var ePart = this.ePart || data.ePart;
			this.mediator.mSecondDom.html(this.getEditInfoHtml(ePart));
			this.bindEditDom();
			this.goToSecondFloor();
		},
		//编辑手机号码
		goToEditPhone:function(data){
			this.goToEditGender(data);
		},
		//编辑邮箱
		goToEditEmail:function(data){
			this.goToEditGender(data);	
		},
		saveEditInfo:function(params,callback){
			t.ajaxJson({
				url:ajaxUrl.editMyInfoUrl,
				data:{
					data:params
				},
				callback:function(result,status){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					result.errmsg = "保存成功";

					if(callback && t.isFunction(callback)){
						callback(result);
					}
				}
			});
		},
		//------处理常见问题-------
		getQuestionsHtmlToNews:function(){
			var dom = tpl(commonTpl,{
				module:{id:1},
				data:commonData.questions,
				noUse:1
			});

			var o = $($.trim(dom));

			this.qDetailClick(o);

			return o;
		},
		qDetailClick:function(dom){
			var pThis = this,
				pageManager = this.mediator.pageManager;
			//常见问题
			dom.find("section").off().on("click",function(){
				var id = $(this).attr("data-id");
				if(!id){return;}
				$(this).addClass('clickBg');

				return;

				pThis.qId = id;
				pThis.getQuestionDetail(id);

				setTimeout(function(){
					pageManager.go('nQuestionDetail');
				},100);
			});
		},
		//PC处理
		pcSolved:function(){
			var mediator = this.mediator,
				mFirstDom = mediator.mFirstDom;
			try{
				if(platid == 4 || platid == 5){
					var dom = mFirstDom.find(".qqList");
					dom.find("a").attr("href","javascript:void(0)");

					dom.find("a").on("click",function(){
						var index = $(this).parent().index();

						var json = {};

						if(index == 0){
							json.title = "客服热线：4006339592";
						}else if(index == 1){
							json.title = "QQ客服：2850871225";
						}else if(index == 2){
							json.title = "代理商合作：4006339592";
						}

						layerPackage.ew_alert(json);
					});
				}
			}catch(e){
				console.log(e);
			}
		},
		noShowBottom:function(flag){
			var mediator = this.mediator;
			if(flag){
				mediator.footer.addClass('none');
				mediator.container.addClass('noBottom');
			}else{
				mediator.footer.removeClass('none');
				mediator.container.removeClass('noBottom');
			}
		},
		//VIP会员特权
		goToVipPrivilege:function(data){
			vipCtrl.goToVipPrivilege(data,this.info);
		},
		backToIndexForVs1:function(){
			this.noShowBottom(false);

			this.backToIndex();
		},
		goToVipService1:function(data){
			this.goToVip(data);
		},
		//如何成为vip会员？
		goToVipService:function(data){
			vipCtrl.goToVipService(data);
		},
		goToFloor:function(num,flag){
			var mediator = this.mediator;
			
			this.noShowBottom(flag);

			mediator.fLis.eq(num).click();
		},
		//----------指纹识别设置-----------
		getErrorHtml:function(errmsg){
			var html = new Array();

			html.push('<i class="errorIcon"></i>');
			html.push('<b class="fs20 lhn fsn block">指纹认证失败！</b>');
			html.push('<span class="c-red fs12 block mt5">暂时无法获取指纹信息，请重新尝试，<i class="fsn c-green restartSoter">重新录入</i></span>');

			return html.join('');
		},
		showErrorMsg:function(){
			var pThis = this;
			layerPackage.ew_alert({
				title:pThis.getErrorHtml(),
				action_desc:'<span class="c-black">我知道了</span>',
				top:27,
				callback:function(){
					pThis.mediator.pageManager.go("mSoterPage1");
				}
			});
			
			var alert = $(".alert_screen");
			alert.on("click",".restartSoter",function(){
				pThis.requireSoter(1);
			});
		},
		requireSoter:function(flag){
			var pThis = this,
				mediator = pThis.mediator,
				pageManager = mediator.pageManager;

			if(flag){
				GenericFramework.init(wxJsdkConfig,1);
			}
				
			GenericFramework.requireSoter(function(res,status){
				//alert(JSON.stringify(res));
				if(status){
					t.ajaxJson({
						url:ajaxUrl.saveSoterUrl,
						data:{
							"data":{
								"pwd":pThis.password,
								"fingerprint":JSON.stringify(res.result_json)
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								//commonFuc.error(result);
								pThis.showErrorMsg();
								return;
							}

							$(".alert_screen").remove();

							if(userInfo.fpState == "0"){
								pThis.openOrCloseSoter("",1);
							}
							if(userInfo.fpState == "0" && userInfo.hasFp == "0"){
								mediator.fLis.eq(2).find("i.countNum").addClass('hide');
								mediator.view.find("#soterService i.newPic").addClass('hide');
							}
							pThis.password = null; //清除之前密码
							userInfo.fpState = 1;
							userInfo.hasFp = 1;

							if(pThis.soterEdit){
								commonFuc.success({
									errmsg:"指纹采集成功!"
								});
								pThis.soterEdit = false;
							}

							pageManager.go("mSoterPage1");
						}
					});
				}else{
					//pThis.showErrorMsg();
				}
			});
		},
		//保存关闭或者开启状态
		openOrCloseSoter:function(pwd,state){
			t.ajaxJson({
				url:ajaxUrl.openOrCloseSoterUrl,
				data:{
					"data":{
						"pwd":pwd,
						"state":state
					}
				},
				callback:function(result,status){
					//alert(ajaxUrl.openOrCloseSoterUrl);
					//alert(pwd+" "+state);
					//alert(JSON.stringify(result));
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}
				}
			});
		},
		getSoterPage:function(data){
			var pThis = this;
			var canSupport = this.canSupport;
			if(canSupport === undefined || canSupport === null){
				setTimeout(function(){
					window.history.go(-1);
				},1000);
				return;
			}

			commonFuc.changeWindowTitle(data.name);
			var id = this.mId || data.mId;
			var pageNum = data.pageNum;
		
			var mediator = this.mediator;

			var params = {
				module:{id:id},
				pageNum:pageNum,
				fpState:userInfo.fpState
			};

			if(id == 6){
				params.canSupport = canSupport;
			}

			var dom = tpl(commonTpl,params);

			var o = $($.trim(dom));

			if(pageNum == 1){
				mediator.mFirstDom.html(o);
			}else if(pageNum == 2){
				mediator.mSecondDom.html(o);
			}else if(pageNum == 3){
				mediator.mThirdDom.html(o);
			}

			this.bindSoterDom(pageNum);
		},
		goToSoterPage:function(data){
			this.getSoterPage(data);

			this.goToFirstFloor(true);
		},
		backToSoterPage:function(){
			$(".alert_screen").remove();
			this.mediator.mSecondDom.find("input").blur();
			this.mediator.pageManager.go("mSoterPage1",1);
		},
		goToSoterPage2:function(data){
			this.getSoterPage(data);

			this.goToSecondFloor();
		},
		bindSoterDom:function(pageNum){
			var pThis = this,
				mediator = this.mediator,
				pageManager = mediator.pageManager;

			var fpState = userInfo.fpState,hasFp = userInfo.hasFp;

			if(pageNum == 1){
				var mFirstDom = mediator.mFirstDom;
				var editSoter = mFirstDom.find("#editSoter");

				UiFramework.btnPackage().checkBoxBtn.init({
					status : fpState,
					type:0,
					target:mFirstDom.find("#soterButton .rPart"),
					cancelAnimate:1,
					callback: {
						on:function(){
							pThis.soterEdit = false;
							if(fpState == "0" && hasFp == "0"){
								pageManager.go("mSoterPage2");
							}else if(hasFp == "1" && fpState == "0"){
								pThis.openOrCloseSoter("",1);
								userInfo.fpState = "1";
								editSoter.removeClass('hide');								
							}
						},
						off:function(){
							pThis.soterEdit = false;
							pageManager.go("mSoterPage2");
							//editSoter.addClass('hide');
						}
					},
					cancelEvent:false
				});

				editSoter.off().on("click",function(){
					pThis.soterEdit = true;
					pageManager.go("mSoterPage2");
				});
			}else if(pageNum == 2){
				var mSecondDom = mediator.mSecondDom;
				var soterPassword = mSecondDom.find("#soterPassword");
				var span = soterPassword.find("span");
				var input = soterPassword.find("#password");

				soterPassword.on("click",function(){
					input.focus();
				});

				input.on("input",function(){
					var o = $(this);
					var val = o.val();
					var len = val.length;

					if(len > 6){
						o.val(val.substring(0,6));
						return;
					}

					var spanVal = val.substring(len - 1,len);

					if(len > 0){
						var s = span.eq(len - 1);
						if(s.find("i").length == 0){
							s.attr("data-val",spanVal).text(spanVal);

							setTimeout(function(){
								s.html("<i class='passwordRadius'></i>");
							},300);
						}
						soterPassword.find("span:gt("+(len - 1)+")").attr("data-val","").text("");

						if(len == 6){
							pThis.password = val;
							console.log(val);
							o.blur();

							if(fpState == "0"){
								//调接口保存密码
								/*UiFramework.soterAlert(27,function(){
									pThis.password = null;
									pageManager.go("mSoterPage1");
								});*/

								setTimeout(function(){
									pThis.requireSoter(1);
								});
							}else{
								//校验密码
								t.ajaxJson({
									url:ajaxUrl.checkSoterPasswordUrl,
									data:{
										"data":{
											"pwd":val
										}
									},
									callback:function(result,state){
										if(result.errcode !=0){
											commonFuc.error(result);
											o.val('');
											setTimeout(function(){
												span.removeAttr('data-val').text("");
											},300);
											return;
										}

										//只有编辑的时候校验指纹
										if(pThis.soterEdit){
											pThis.requireSoter(1);
										}else{
											pThis.openOrCloseSoter(val,0);
											userInfo.fpState = 0;
											pageManager.go("mSoterPage1");
										}
									}
								});
							}
						}
					}
					else{
						span.eq(0).attr("data-val","").text("");
					}
				});

				try{
					/*setTimeout(function(){
						input.focus();
						input.click();
					},2000);*/

					input.focus();
				}catch(e){
					console.log(e);
				}
			}
		}
	};
	
	return personalObj;
});