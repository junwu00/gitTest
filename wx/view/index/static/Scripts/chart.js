require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'index:static/Scripts/commonData',
	'index:static/Scripts/pageManager',
	/*----*/
	'index:static/Scripts/commonFuc',
	'common:widget/lib/chartHelper',
    "common:widget/lib/scFormTable/scFormTable-v2.0"
],function($,t,UiFramework,commonData,pageManager,commonFuc,chartHelper,ScFormTable) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	//layerPackage.lock_screen();//遮罩start

    // 基于准备好的dom，初始化echarts实例
    var chart1 = chartHelper.init(document.getElementById('line'));
    var chart2 = chartHelper.init(document.getElementById('bar'));
    var chart3 = chartHelper.init(document.getElementById('pie'));

    // 指定图表的配置项和数据
    /*var option = {
        title: {
            text: 'ECharts 入门示例'
        },
        tooltDNF ip: {},
        legend: {
            data:['销量']
        },
        xAxis: {
            data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        }]
    };*/
    var time = 0;
    var click = true;

    /*var params = {
        title:"ECharts 入门示例",
        //color: ['#3398DB'],
        legend:['销量','test'],
        xAxis:[{
            data:["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子","衬衫","羊毛衫","雪纺衫","裤子","高跟鞋"]
        }],
        //yAxis:[],
        series: [{
            name: '销量',
            type: 'bar',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            data: [5, 20, 36, 10, 10, 20,5, 20, 36, 10, 10]
        },{
            name: 'test',
            type: 'line',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            areaStyle:{normal: {}},//面积
            data: [10, 10, 20,5, 20, 36]
        }],
        tooltip:function(params){
            try{
                if(t.isArray(params)){
                    var arr = new Array();

                    arr.push(params[0].name  + " ");
                    for (var i = 0; i < params.length; i++) {
                        if(params[i].value){
                            arr.push(params[i].seriesName + ":" + params[i].value + " ");
                        }
                    };

                    $("p").text(arr.join(''));
                }
            }catch(e){
                console.log(e);
            }
        },
        //新增按钮
        myTool:function(){
            //chart.clear();
            option = chartHelper.getBaseOption(params,!click);
            chartHelper.setOption(chart,option,true);

            click = !click;
        }
    };*/

    var params = {
        title:"ECharts 入门示例",
        //color: ['#3398DB'],
        legend:['销量','test'],
        xAxis:[{
            data:[
               "衬衫衬衫衬衫衬衫",
                "羊毛衫","雪纺衫","裤子","高跟鞋","袜子","衬衫","羊毛衫","雪纺衫","裤子",
               "衬衫衬衫"
            ]
        }],
        //yAxis:[],
        series: [{
            name: '销量',

            type: 'bar',
            /*label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },*/
            data: [5, 20, 36, 10, 10, 20,5, 20, 36, 10, 10]
        },{
            name: 'test',
            type: 'line',
            /*label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },*/
            areaStyle:{normal: {}},//面积
            data: [10, 10, 20,5, 20, 36]
        }],
        //*
        tooltip:function(params){
            try{
                if(t.isArray(params)){
                    var arr = new Array();

                    arr.push(params[0].name  + " ");
                    for (var i = 0; i < params.length; i++) {
                        if(params[i].value){
                            arr.push(params[i].seriesName + ":" + params[i].value + " ");
                        }
                    };

                    $("p").text(arr.join(''));
                }
            }catch(e){
                console.log(e);
            }
        }
        //*/
    };


    //var option = chartHelper.getBaseOption(params,1);
    //*
    var opt1 = chartHelper.myOption("bar",params.series,1,1);
    //opt1 = chartHelper.useMyTool(opt1,1,1,chart1);
    opt1 = chartHelper.setDataZoom(opt1);
    opt1 = chartHelper.getOption(params,opt1);
    //opt1 = chartHelper.xAxisBoundaryGap(opt1);
    opt1 = chartHelper.xAxisLabel(opt1);
    opt1 = chartHelper.showNumber(opt1,[0]);

    console.log(opt1);

    var params2 = t.clone(params);
    if(true){
        //面积
        //chartHelper.addMarkLine(params2.series[0]).addMarkPoint(params2.series[0]);

        params2.series[0].type = "line";
        //params2.series[0].areaStyle = {normal: {}};
        params2.series[0] = chartHelper.lineToArea(params2.series[0]);

        params2.color = null;
    }
    var opt2 = chartHelper.myOption("line",params2.series,1,1);
    opt2 = chartHelper.setDataZoom(opt2);
    opt2 = chartHelper.getOption(params2,opt2);
    opt2 = chartHelper.xAxisBoundaryGap(opt2);
    opt2 = chartHelper.xAxisLabel(opt2);
    opt2 = chartHelper.showNumber(opt2,"noShow");

    console.log(opt2);
    //*/

    var params3 =  {
        title:"ECharts 入门示例",
        //legend:["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子","衬衫","羊毛衫","雪纺衫","裤子","高跟鞋"],
        //color: ['#3398DB'],
        //yAxis:[],
        series: [{
            name: '销量',
            type: 'pie',
            /*label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },*/
            selectedOffset:0,
            selectedMode:'single',
            label: {
                normal: {
                    show: true,
                    position: 'outside'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            data: [
                {
                    value: 335,
                    name: '直接访问直接访问直接访问'
                }, {
                    value: 310,
                    name: '邮件营销'
                    
                }, {
                    value: 234,
                    name: '联盟广告'
                }, {
                    value: 135,
                    name: '视频广告'
                },
                {
                    value:1548, 
                    name:'搜索引擎',
                    selected:true
                }
            ]
        }],
        tooltip:function(params,ticket,callback){
            console.log(params);
            try{
                $("p").text(params.seriesName + ":" + params.value + " " + "百分比:"+params.percent+"%");
            }catch(e){
                console.log(e);
            }
        },
        legendTitle:function(name,obj){
            console.log(obj);

            chartHelper.bindPieClick(chart3,name,obj);

            $("#pieTitle").append($(obj));
        }
    };
    var opt3 = chartHelper.myOption("pie",params3.series,1,1);
    opt3 = chartHelper.getOption(params3,opt3);
    opt3 = chartHelper.pieAppOption(opt3);

    console.log(opt3);
    //console.log(opt2);
    //chartHelper.isLoading(chart,true);
    t.ajaxJson({
        "url":"",
        callback:function(result,status){
            if(!status){
                setTimeout(function(){
                    //chartHelper.isLoading(chart,false);

                    // 使用刚指定的配置项和数据显示图表。
                    chartHelper.setOption(chart1,opt1);
                    chartHelper.setOption(chart2,opt2);
                    chartHelper.setOption(chart3,opt3);
                },time);
            }

            /*chartHelper.bindClick(chart,function(type,data){
                console.group("====");
                console.log(type);
                console.log(data);
                console.groupEnd("====");
            });*/

            $("button").on("click",function(){
                click = !click;
                opt1 = chartHelper.useMyTool(opt1,click);
                chartHelper.setOption(chart1,opt1,true);
            });

            $(window).resize(function(event) {
                var chart1 = chartHelper.init(document.getElementById('line'));
                var chart2 = chartHelper.init(document.getElementById('bar'));
                var chart3 = chartHelper.init(document.getElementById('pie'));
                chartHelper.setOption(chart1,opt1,1,1);
                chartHelper.setOption(chart2,opt2,1,1);
                chartHelper.setOption(chart3,opt3,1,1);

                $("button").off().on("click",function(){
                    click = !click;
                    opt1 = chartHelper.useMyTool(opt1,click);
                    chartHelper.setOption(chart1,opt1,true);
                });
            });
        }
    });

    //--------------表格--------------------------------
    
    var testData = {
        "errcode": "0",
        "errmsg": "成功",
        "data": [
            {
                "col_key": "2016-10-09",
                "input0": "陈意浩",
                "input1": "2016-10-09",
                "input2": "2016-10-10",
                "input3": [
                    "111111",
                    "111111"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            },
            {
                "col_key": "2016-10-10",
                "input0": "陈意浩",
                "input1": "2016-10-10",
                "input2": "2016-10-11",
                "input3": [
                    "五",
                    "七",
                    "九"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            },
            {
                "col_key": "2016-10-09",
                "input0": "陈意浩",
                "input1": "2016-10-09",
                "input2": "2016-10-10",
                "input3": [
                    "111111",
                    "111111"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            },
            {
                "col_key": "2016-10-10",
                "input0": "陈意浩",
                "input1": "2016-10-10",
                "input2": "2016-10-11",
                "input3": [
                    "五",
                    "七",
                    "九"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            },
            {
                "col_key": "2016-10-09",
                "input0": "陈意浩",
                "input1": "2016-10-09",
                "input2": "2016-10-10",
                "input3": [
                    "111111",
                    "111111"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            },
            {
                "col_key": "2016-10-10",
                "input0": "陈意浩",
                "input1": "2016-10-10",
                "input2": "2016-10-11",
                "input3": [
                    "五",
                    "七",
                    "九"
                ],
                "input11": "2016-10-09",
                "input21": "2016-10-10"
            }
        ]
    };
    //return;

    console.log(ScFormTable);
    var a = new ScFormTable();
    var b = new ScFormTable();
   
    a.init({
        target:$(".test"),
        tbody:testData.data,
        fixFirst:0,
        theadClick:function(obj,name,sort){
            console.log(name + "  " + sort);
        },
        pageTools:{
            target:$(".testPageTools"),
            index:3,
            count:10,
            buttonClick:function(pageNum,pageTools){
                console.log(pageNum);

                //*
                setTimeout(function(){
                    testData.data.splice(0,1);
                    testData.data.splice(1,1);
                    testData.data[1].input0 = "陈意浩陈意浩陈\t意浩陈意浩陈意浩";
                    a.restartTbody($(".test"),testData.data);

                    a.restartPageTools({
                        target:$(".testPageTools"),
                        index:4,
                        count:10
                    });

                    console.log(a.getFirstWidth());
                },1000);
                //*/
            }
        },
        getScrollxy:function(x,y){
            console.log(x,y);
        }
    });
    console.log(a);

    b.init({
        target:$(".test2"),
        thead:[
            {
                value:"time1"
            },
            {
                value:"name"
            },
            {
                value:"time2"
            },
            {
                value:"time3"
            },
            {
                value:"group"
            },
            {
                value:"time4"
            },
            {
                value:"time5"
            }
        ],
        tbody:testData.data,
        tfoot:[
            {
                value:"time1"
            },
            {
                value:"name"
            },
            {
                value:"time2"
            },
            {
                value:"time3"
            },
            {
                value:"group"
            },
            {
                value:"time4"
            },
            {
                value:"time5"
            }
        ],
        theadClick:function(obj,name,sort){
            console.log(name + "  " + sort);
        },
        pageTools:{
            target:$(".testPageTools2"),
            index:3,
            count:10,
            buttonClick:function(pageNum,pageTools){
                console.log(pageNum);

                //*
                setTimeout(function(){
                    testData.data.splice(0,1);
                    testData.data[1].input0 = "李林华李林华陈\t意浩李林华李林华";
                    b.restartTbody($(".test2"),testData.data);

                    b.restartPageTools({
                        target:$(".testPageTools2"),
                        index:3,
                        count:10
                    });

                    console.log(b.getFirstWidth());
                    console.log(b);
                },1000);
                //*/
            }
        }
    });
    console.log(b);


	//layerPackage.unlock_screen();
});