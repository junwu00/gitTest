define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl-v2.0.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc-v2.0',
	'index:static/Scripts/vipCtrl-v2.0',
	'common:widget/lib/GenericFramework'
],function($,t,UiFramework,tpl,commonTpl,commonData,l,commonFuc,vipCtrl,GenericFramework) {
	var ajaxUrl = commonData.ajaxUrl,
		layerPackage = UiFramework.layerPackage();
	//个人信息
	var mySetting = {
		init:function(mediator){
			this.mediator = mediator;

			this.o = $(".mySetting");

			this.getInfoHtml();

			vipCtrl.init(this);
		},
		getInfoHtml:function(callback){
			var pThis = this;

			if(pThis.info){return;}

			layerPackage.lock_screen();
			t.ajaxJson({
				url:ajaxUrl.getMyInfoUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					var info = result.info;
					if(callback && t.isFunction(callback)){
						callback(info);
						return;
					}
					pThis.info = info;
					var dom = tpl(commonTpl, {
						personalInfo:info,
						defaultFace:defaultFace,
						platid:platid
					});

					var html = $($.trim(dom));

					pThis.o.html(html);

					layerPackage.unlock_screen();
					
					//事件集合
					pThis.bindDom(pThis.o);

					//移动端才显示
					
					pThis.getCount(info);
				}
			});
		},
		getCount:function(info){
			var count = 0
			for(var i in info.onetime_msg){
				count ++ ;
			}

			var o = this.mediator.container.find("#userPage i.redCir");

			try{
				if(info.isAdmin != 1){
					var arr = new Array();
					for (var i = 0; i < commonData.onetimeMsg.length; i++) {
						if(commonData.onetimeMsg[i] != "admin_service" 
							&& commonData.onetimeMsg[i] != "suggest_buy" 
							&& commonData.onetimeMsg[i] != "invite_proc" 
							&& commonData.onetimeMsg[i] != "employee_share"){
							arr.push(commonData.onetimeMsg[i]);
						}
					};

					commonData.onetimeMsg = arr;
				}
			}catch(e){
				console.log(e);
			}

			if((platid == 1 || platid == 2) && count != commonData.onetimeMsg.length){
				try{
					o.removeClass('hide');
				}catch(e){
					console.log(e);
				}
			}else{
				o.addClass('hide');
			}
		},
		//事件集合
		bindDom:function(container){
			var pThis = this;
			//点击选项事件
			container.find("section").on("click",function(){
				var o = $(this);
				commonFuc.addClickBg(o);

				var id = o.attr("data-id"),key = o.attr("data-key");
				if(!id){return;}

				pThis.goPage(id,key,$(this));
			});
		},
		goPage:function(mId,key,obj){
			if(key){
				commonFuc.onetimeMsgClick(key,obj);
				this.info.onetime_msg[key] = 1;
				this.getCount(this.info);
			}

			var pageManager = this.mediator.pageManager;
			/*产品服务支持*/
			if(mId == 1){
				pageManager.go("mProduct");
			}
			else if(mId == 2){
				pageManager.go("mVip");
			}
			else if(mId == 3){
				pageManager.go("mAdminService");
			}
			//指纹识别设置
			else if(mId == 6){
				try{
					//this.canSupport = 1;
					if(this.canSupport){
						pageManager.go("mSoterPage1");
					}else{
						var pThis = this;
						GenericFramework.init(wxJsdkConfig,1);
						GenericFramework.getSupportSoter(function(canSupport){
							pThis.canSupport = canSupport;
							
							pageManager.go("mSoterPage1");
						});
					}
				}catch(e){
					alert(e);
					console.log(e);
				}
			}
		},
		//获取页面模板
		getTplHtml:function(id,changeUrl){
			var mediator = this.mediator;

			if(id === undefined){id = 1;}

			var params = {
				module:{id:id}
			};

			//产品服务与支持
			if(id == 1){
				//常见问题数据
				params.data = commonData.questions;
			}

			var dom = null;

			//VIP服务模板特殊处理
			if(id == 2){
				params.info = this.info;
				dom = vipCtrl.getHtml(params,changeUrl);
			}
			//其他走常用公用模板
			else{
				dom = tpl(commonTpl,params);
			}

			var o = $($.trim(dom));

			commonFuc.goFloor(mediator.firstFloor.html(o));

			if(id == 1){
				//绑定事件
				this.bindProductServiceDom();
				//PC微信处理
				this.pcSolved();
			}
			//管理中心
			else if(id == 3){
				UiFramework.bottomInfo.init(mediator.firstFloor);
			}
		},
		//返回我的设置
		backToMySetting:function(){
			var mediator = this.mediator,
				firstFloor = mediator.firstFloor;
			commonFuc.backFloor(firstFloor);

			//去除点击项背景色
			commonFuc.removeClickBg(this.o.find("section.clickBg"));

			if(!mediator.container.hasClass('closeMainContent')){
				mediator.pageManager.go("openMySetting",1);
			}
		},
		//PC处理
		pcSolved:function(){
			var mediator = this.mediator,
				firstFloor = mediator.firstFloor;
			try{
				if(platid == 4 || platid == 5){
					var dom = firstFloor.find(".qqList");
					dom.find("a").attr("href","javascript:void(0)");

					dom.find("a").on("click",function(){
						var index = $(this).parent().index();

						var json = {};

						if(index == 0){
							json.title = "客服热线：4006339592";
						}else if(index == 1){
							json.title = "QQ客服：2850871225";
						}else if(index == 2){
							json.title = "代理商合作：4006339592";
						}

						layerPackage.ew_alert(json);
					});
				}
			}catch(e){
				console.log(e);
			}
		},
		//产品服务与支持
		goToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getTplHtml(mId);
			//this.bindProductServiceDom();
		},
		/*产品服务支持*/
		bindProductServiceDom:function(){
			var pThis = this,
				mediator = this.mediator,
				firstFloor = mediator.firstFloor,
				pageManager = mediator.pageManager;

			//常见问题
			firstFloor.find(".questions section").off().on("click",function(){
				commonFuc.addClickBg($(this));
			});

			firstFloor.find(".pServices section").off().on("click",function(){
				var id = $(this).attr("data-id");
				commonFuc.addClickBg($(this));

				pThis.sId = id;

				setTimeout(function(){
					//角色体验
					if(id == 1){
						pageManager.go("mRole");
					}
					/*产品案例 直接跳转*/
					else if(id == 2){
						pageManager.go("mProductDemo");
					}
					//使用指引
					else if(id == 3){
						pageManager.go("mUseHelper");
					}
				},100);
			});
		},
		/*产品服务支持第二层模板*/
		getServiceHtml:function(sId){
			var dom = tpl(commonTpl,{
				pService:{
					id:sId
				},
				commonData:commonData
			});

			var o = $($.trim(dom));
			return o;
		},
		//回退到产品服务支持
		backToProduct:function(data){
			commonFuc.changeWindowTitle(data.name);
			var mediator = this.mediator,
				secondFloor = mediator.secondFloor;
			commonFuc.backFloor(secondFloor);
			//this.goToFirstFloor();
			this.mediator.pageManager.go("mProduct",1);
		},
		//角色体验
		goToRole:function(data){
			commonFuc.changeWindowTitle(data.name);
			//this.getTplHtml();

			var sId = this.sId || data.sId;

			commonFuc.goFloor(this.mediator.secondFloor.html(this.getServiceHtml(sId)));
		},
		//使用指引
		goToUseHelper:function(data){
			this.goToRole(data);
		},
		//产品案例
		goToProductDemo:function(data){
			this.goToRole(data);
		},
		//VIP
		goToVip:function(data,changeUrl){
			commonFuc.changeWindowTitle(data.name);
			var mId = this.mId || data.mId;

			this.getTplHtml(mId,changeUrl);
		},
		//退回到VIP层
		backToVip:function(data,changeUrl){
			var mediator = this.mediator;
			var pageManager = mediator.pageManager;
			secondFloor = mediator.secondFloor;
			commonFuc.backFloor(secondFloor);
			if(changeUrl){
				pageManager.go("nVip",1);
				return;
			}

			pageManager.go("mVip",1);
		},
		//vip会员特权
		goToVipPrivilege:function(data){
			vipCtrl.goToVipPrivilege(data,this.info);
		},
		//管理员服务中心
		goToAdminService:function(data){
			this.goToVip(data);
		},
		//----------指纹识别设置-----------
		//错误信息模板
		getErrorHtml:function(errmsg){
			var html = new Array();

			html.push('<i class="errorIcon"></i>');
			html.push('<b class="fs20 lhn fsn block">指纹认证失败！</b>');
			html.push('<span class="c-red fs12 block mt5">暂时无法获取指纹信息，请重新尝试，<i class="fsn c-green restartSoter">重新录入</i></span>');

			return html.join('');
		},
		//显示错误信息
		showErrorMsg:function(){
			var pThis = this;
			layerPackage.ew_alert({
				title:pThis.getErrorHtml(),
				action_desc:'<span class="c-black">我知道了</span>',
				top:27,
				callback:function(){
					pThis.mediator.pageManager.go("mSoterPage1");
				}
			});
			
			var alert = $(".alert_screen");
			alert.on("click",".restartSoter",function(){
				pThis.requireSoter(1);
			});
		},
		//请求指纹接口
		requireSoter:function(flag){
			var pThis = this,
				mediator = pThis.mediator,
				pageManager = mediator.pageManager;

			if(flag){
				GenericFramework.init(wxJsdkConfig,1);
			}

			GenericFramework.requireSoter(function(res,status){
				//alert(JSON.stringify(res));
				//alert(status);
				if(status){
					t.ajaxJson({
						url:ajaxUrl.saveSoterUrl,
						data:{
							"data":{
								"pwd":pThis.password,
								"fingerprint":JSON.stringify(res.result_json)
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								//commonFuc.error(result);
								pThis.showErrorMsg();
								return;
							}

							$(".alert_screen").remove();

							if(pThis.info.fpState == "0"){
								pThis.openOrCloseSoter("",1);
							}

							if(pThis.info.fpState == "0" && pThis.info.hasFp == "0"){
								//mediator.fLis.eq(2).find("i.countNum").addClass('hide');
								//mediator.view.find("#soterService i.newPic").addClass('hide');
							}

							pThis.password = null; //清除之前密码
							pThis.info.fpState = 1;
							pThis.info.hasFp = 1;

							if(pThis.soterEdit){
								commonFuc.success({
									errmsg:"指纹采集成功!"
								});
								pThis.soterEdit = false;
							}

							//pageManager.go("mSoterPage1",1);
							window.history.go(-1);
						}
					});
				}else{
					//pThis.showErrorMsg();
				}
			});
		},
		//保存关闭或者开启状态
		openOrCloseSoter:function(pwd,state){
			t.ajaxJson({
				url:ajaxUrl.openOrCloseSoterUrl,
				data:{
					"data":{
						"pwd":pwd,
						"state":state
					}
				},
				callback:function(result,status){
					//alert(ajaxUrl.openOrCloseSoterUrl);
					//alert(pwd+" "+state);
					//alert(JSON.stringify(result));
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}
				}
			});
		},
		//获取指纹模板
		getSoterPage:function(data){
			var pThis = this;
			var canSupport = this.canSupport;
			if(canSupport === undefined || canSupport === null){
				setTimeout(function(){
					window.history.go(-1);
				},1000);
				return;
			}

			commonFuc.changeWindowTitle(data.name);
			var id = this.mId || data.mId;
			var pageNum = data.pageNum;
		
			var mediator = this.mediator;

			var params = {
				module:{id:id},
				pageNum:pageNum,
				fpState:pThis.info.fpState
			};

			if(id == 6){
				params.canSupport = canSupport;
			}

			var dom = tpl(commonTpl,params);

			var o = $($.trim(dom));

			if(pageNum == 1){
				commonFuc.goFloor(mediator.firstFloor.html(o));
			}else if(pageNum == 2){
				commonFuc.goFloor(mediator.secondFloor.html(o));
			}else if(pageNum == 3){
				//mediator.thirdFloor.html(o);
			}

			this.bindSoterDom(pageNum);
		},
		//跳转到指纹设置页面
		goToSoterPage:function(data){
			this.getSoterPage(data);
		},
		//退回到指纹设置页面
		backToSoterPage:function(){
			$(".alert_screen").remove();
			var mediator = this.mediator;
			mediator.secondFloor.find("input").blur();
			commonFuc.backFloor(mediator.secondFloor);
			mediator.pageManager.go("mSoterPage1",1);
		},
		//跳转密码页面
		goToSoterPage2:function(data){
			this.getSoterPage(data);
		},
		//绑定指纹事件
		bindSoterDom:function(pageNum){
			var pThis = this,
				mediator = this.mediator,
				pageManager = mediator.pageManager;

			var fpState = this.info.fpState,hasFp = this.info.hasFp;

			if(pageNum == 1){
				var firstFloor = mediator.firstFloor;
				var editSoter = firstFloor.find("#editSoter");

				UiFramework.btnPackage().checkBoxBtn.init({
					status : fpState,
					type:0,
					target:firstFloor.find("#soterButton .rPart"),
					cancelAnimate:1,
					callback: {
						on:function(){
							pThis.soterEdit = false;
							if(fpState == "0" && hasFp == "0"){
								pageManager.go("mSoterPage2");
							}else if(hasFp == "1" && fpState == "0"){
								pThis.openOrCloseSoter("",1);
								pThis.info.fpState = "1";
								editSoter.removeClass('hide');								
							}
						},
						off:function(){
							pThis.soterEdit = false;
							pageManager.go("mSoterPage2");
							//editSoter.addClass('hide');
						}
					},
					cancelEvent:false
				});

				editSoter.off().on("click",function(){
					pThis.soterEdit = true;
					pageManager.go("mSoterPage2");
				});
			}else if(pageNum == 2){
				var secondFloor = mediator.secondFloor;
				var soterPassword = secondFloor.find("#soterPassword");
				var span = soterPassword.find("span");
				var input = soterPassword.find("#password");

				soterPassword.on("click",function(){
					input.focus();
				});

				input.on("input",function(){
					var o = $(this);
					var val = o.val();
					var len = val.length;

					if(len > 6){
						o.val(val.substring(0,6));
						return;
					}

					var spanVal = val.substring(len - 1,len);

					if(len > 0){
						var s = span.eq(len - 1);
						if(s.find("i").length == 0){
							s.attr("data-val",spanVal).text(spanVal);

							setTimeout(function(){
								s.html("<i class='passwordRadius'></i>");
							},300);
						}
						soterPassword.find("span:gt("+(len - 1)+")").attr("data-val","").text("");

						if(len == 6){
							pThis.password = val;
							console.log(val);
							o.blur();

							if(fpState == "0"){
								//调接口保存密码
								/*UiFramework.soterAlert(27,function(){
									pThis.password = null;
									pageManager.go("mSoterPage1");
								});*/

								setTimeout(function(){
									pThis.requireSoter(1);
								});
							}else{
								//校验密码
								t.ajaxJson({
									url:ajaxUrl.checkSoterPasswordUrl,
									data:{
										"data":{
											"pwd":val
										}
									},
									callback:function(result,state){
										if(result.errcode !=0){
											commonFuc.error(result);
											o.val('');
											setTimeout(function(){
												span.removeAttr('data-val').text("");
											},300);
											return;
										}

										//只有编辑的时候校验指纹
										if(pThis.soterEdit){
											pThis.requireSoter(1);
										}else{
											pThis.openOrCloseSoter(val,0);
											pThis.info.fpState = 0;
											//pageManager.go("mSoterPage1",1);
											window.history.go(-1);
										}
									}
								});
							}
						}
					}
					else{
						span.eq(0).attr("data-val","").text("");
					}
				});

				try{
					/*setTimeout(function(){
						input.focus();
						input.click();
					},2000);*/

					input.focus();
				}catch(e){
					console.log(e);
				}
			}
		}
	};
	
	return mySetting;
});