define({
	company:"移步到微",
	version:"流程专家",
	questions:[
		{
			id:1,
			title:"怎么样【删除】表单",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=3&sn=ee186b52fc8371b440562ed8013e7814&scene=0#wechat_redirect"
		},
		{
			id:2,
			title:"审批人能不能【退回】申请",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=4&sn=b5efbf87ad90d64fc4a7728e77a3d845&scene=0#wechat_redirect"
		},
		{
			id:3,
			title:"知会信息哪里查看",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=5&sn=50118a31d3a71800983dc3204dc960b0&scene=0#wechat_redirect"
		},
		{
			id:4,
			title:"审批完的【申请单】在哪里查看",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=6&sn=d8e3c24a50e13f6c843ff48cbcdf26e6&scene=0#wechat_redirect"
		}
	],
	productDemos:[
		{
			title:"佛山禅城医院",
			sTitle:"",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=1&sn=382991a84c4b5564d45c1a9c582bccf9&scene=0#wechat_redirect",
			content:"佛山市禅城中心医院是佛山市首家综合性三甲民营医院，在中国近万家非公立医院排行榜第三，集医疗、急救、预防、保健、科研、教学为一体。1958年建院，历经发展，业务量、品牌度和影响力在全佛山已位居前列。现有医职员工1600名；床位1200张，年门诊量超230万人次，年住院超4万人次。"
		},
		{
			title:"光明同创集团",
			sTitle:"",
			url:"http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=2651173178&idx=2&sn=8293cbd341b8754830fb5bc37ae534d4&scene=0#wechat_redirect",
			content:"光明同创集团是从事餐饮、酒店、娱乐、房地产的多元化大中型企业，下属三十多家公司和酒店，现有员工1200多人。拥有希迩顿、咏乐汇、华商汇等多个知名品牌。<br/>光明同创集团重点打造核心竞争力，斥巨资整合行业模式并深度挖掘经营优势，为广大客户和合作伙伴完美诠释了高端服务和诚信品质，获得良好的商业信誉和广泛好评。"
		}
	],
	ajaxUrl:{
		//消息
		/*获取我的消息项列表 101*/
		"getNewListUrl":"/wx/index.php?model=index&m=msg&cmd=101",
		/*搜索消息 101*/
		"searchNewsUrl":"/wx/index.php?model=index&m=msg&cmd=102",
		/*获取指定消息项的消息 103*/
		"getOneNewsUrl":"/wx/index.php?model=index&m=msg&cmd=103",
		/*将消息项内的所有消息置为已读 104*/
		//"readNewsUrl":"/wx/index.php?model=index&m=msg&cmd=104",
		/*获取轮询地址*/
		"getPollingUrl":"/wx/index.php?model=index&m=msg&cmd=104",
		/*保存消息 105*/
		"saveNewsUrl":"/wx/index.php?model=index&m=msg&cmd=105",
		//我的
		//获取我的个人信息
		"getMyInfoUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=101",
		//修改我的个人信息
		"editMyInfoUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=102",
		//应用
		//获取用户可使用的表单 101 
		"getUserProcessListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=101",
		//获取待办流程总数 102
		"getDealCountUrl":"/wx/index.php?model=index&m=app&a=index&cmd=102",
		//获取我待办/已办的流程列表 103
		"getDealListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=103",
		//获取我待办/已办的流程列表 103
		"getMineListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=104",
		//获取我待办/已办的流程列表 103
		"getNotifyListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=105",
		//获取套帐列表 102
		"getRHListUrl":"/wx/index.php?model=finstat&m=ajax&cmd=102",
		//返回套帐内报表列表 101
		"getRHTypeListUrl":"/wx/index.php?model=finstat&m=ajax&cmd=101",
		//检查是否开报表功能103
		"getOpenListUrl":"/wx/index.php?model=finstat&m=ajax&cmd=103",
		//获取报表列表数据
		"getChartList":"/wx/index.php?model=finstat&m=ajax&cmd=104",
		//获取VIP基本信息
		"getVipStatus":"/wx/index.php?model=vip&m=ajax&a=index&cmd=101",
		//--------------全部应用---------------
		//获取自定义常用应用表单
		"getUserProcessUrl":"/wx/index.php?model=index&m=app&a=index&cmd=109",
		//获取所有表单
		"getAllProcessUrl":"/wx/index.php?model=index&m=app&a=index&cmd=110",
		//保存自定义常用应用表单
		"saveUserProcessUrl":"/wx/index.php?model=index&m=app&a=index&cmd=111",
		//------------------新的消息接口---------------------
		//获取消息接口
		"getNewMsgUrl":"/wx/index.php?model=index&m=msg&cmd=106",
		//消息阅读接口
		"readMsgUrl":"/wx/index.php?model=index&m=msg&cmd=107",
		//消息删除接口
		"delMsgUrl":"/wx/index.php?model=index&m=msg&cmd=108",
		//获取消息分类接口
		"getMsgTypeUrl":"/wx/index.php?model=index&m=msg&cmd=109",
		//-----------------------指纹识别设置接口-------------------------------
		//保存密码和指纹
		"saveSoterUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=103",
		//开启/关闭指纹
		"openOrCloseSoterUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=104",
		//获取指纹开关设置信息 
		"getSoterStatusUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=105",
		//指纹验证 
		"checkSoterUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=106",
		//验证密码
		"checkSoterPasswordUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=107",
		//已读消息接口
		"updateOneTimeMsgUrl":"/wx/index.php?model=index&m=mine&a=update_onetime_msg"
	},
	onetimeMsg:[
		'fingerprint_setting',		//指纹识别设置
		//'service_and_support',		//产品服务与支持
		'invite_proc',				//邀请同事发起流程
		'employee_share',			//员工分享
		'suggest_buy',				//推荐企业购买
		'admin_service'			//管理员服务中心
	]
});