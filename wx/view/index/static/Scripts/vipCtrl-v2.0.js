define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/vipTpl-v2.0.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/commonFuc-v2.0'
],function($,t,UiFramework,tpl,vipTpl,commonData,commonFuc) {
	var ajaxUrl = commonData.ajaxUrl,
		layerPackage = UiFramework.layerPackage();
	//搜索相关
	var vipCtrl = {
		mySetting:null,
		init:function(mySetting){
			this.mySetting = mySetting;
			this.mediator = mySetting.mediator;
		},
		getHtml:function(params,changeUrl){
			params.vip = 1;

			var pThis = this;
			layerPackage.lock_screen();

			function getDom(params){
				tpl.register('dateFormat', t.dateFormat);
				var dom = tpl(vipTpl,params);
				tpl.unregister('dateFormat');

				var o = $($.trim(dom));

				var firstFloor = pThis.mediator.firstFloor;
				firstFloor.html(o);
				pThis.bindDom(firstFloor,changeUrl);

				layerPackage.unlock_screen();
			}

			if(!params.info){
				this.mySetting.getInfoHtml(function(info){
					params.info = info;
					//params.userInfo = pThis.updateUserInfo(info);
					params.userInfo = params.info;

					getDom(params);
				});
			}else{
				this.getVipStatus(function(info){
					//params.userInfo = pThis.updateUserInfo(info);
					params.userInfo = params.info;

					getDom(params);
				});
			}
		},
		getVipStatus:function(callback){
			t.ajaxJson({
				url:ajaxUrl.getVipStatus,
				callback:function(result,state){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					if(callback && t.isFunction(callback)){
						callback(result.info);
					}
				}
			});
		},
		updateUserInfo:function(data){
			try{
				userInfo.isVip = data.is_vip;
				userInfo.vipPastDate = data.past_date;
				userInfo.vipPastTime = data.past_time;
				userInfo.vipState = data.vip_state;
				userInfo.onetime_msg = data.onetime_msg;

				return userInfo;
			}catch(e){

			}
		},
		getVipActivity:function(){
			t.ajaxJson({
				url:"",
				data:{

				},
				callback:function(result,state){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}
				}
			})
		},
		bindDom:function(jqObj,changeUrl){
			var pThis = this;
			//续费
			jqObj.find(".vipInfo").on("click",".renew",function(){
				pThis.linkToSomeoneQQ();
			});

			//重新开通VIP
			jqObj.find(".vipInfo").on("click",".overdueVip",function(){
				pThis.linkToSomeoneQQ();
			});

			//立即开通VIP
			jqObj.find(".vipInfo").on("click",".openVip",function(){
				pThis.linkToSomeoneQQ();
			});

			jqObj.find(".vipPrivilege").on("click",".morePrivilege",function(){
				pThis.mediator.pageManager.go(changeUrl ? "nVipPrivilege" : "mVipPrivilege");
			});

			jqObj.find(".vipServices").on("click",function(){
				var o = $(this);
				var id = o.attr("data-id"),
					key = o.attr("data-key"),
				pageManager = pThis.mediator.pageManager;

				commonFuc.addClickBg(o);

				if(key){
					commonFuc.onetimeMsgClick(key,$(this));
				}
			});
		},
		//VIP会员特权
		goToVipPrivilege:function(data,info){
			commonFuc.changeWindowTitle(data.name);

			this.getVipPrivilegeHtml(info);
		},
		getVipPrivilegeHtml:function(info){
			var dom = tpl(vipTpl,{
				info:info,
				userInfo:userInfo,
				vipPrivilege:1
			});

			var secondFloor = this.mediator.secondFloor;
			commonFuc.goFloor(secondFloor.html($.trim(dom)));
			this.bindVipPrivilegeDom(secondFloor);
		},
		bindVipPrivilegeDom:function(jqObj){
			var pThis = this;
			//重新开通VIP
			jqObj.on("click",".overdueVip",function(){
				pThis.linkToSomeoneQQ();
			});

			//立即开通VIP
			jqObj.on("click",".openVip",function(){
				pThis.linkToSomeoneQQ();
			});
		},
		linkToSomeoneQQ:function(){
			//移步到微-技术支持-梓健2850871225
			try{
				if(platid == 4 || platid == 5){
					layerPackage.ew_alert({
						title:"请咨询 移步到微-技术支持-梓健 QQ:2850871225"
					});
				}else{
					window.location.href = "http://wpa.qq.com/msgrd?v=3&uin=2850871225&site=qq&menu=yes";
				}
			}catch(e){
				console.log(e);
			}
		},
		//如何成为vip会员？
		goToVipService:function(data){
			commonFuc.changeWindowTitle(data.name);

			this.getVipServiceHtml(data);
		},
		getVipServiceHtml:function(data){
			var dom = tpl(vipTpl,{
				vipService:1,
				id:data.id,
				name:data.name
			});

			var secondFloor = this.mediator.secondFloor;
			secondFloor.html($.trim(dom));

			this.goToSecondFloor();
		}
	};

	return vipCtrl;
});