define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/commonTpl.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc',
	'index:static/Scripts/otherProcess',
],function($,t,UiFramework,tpl,commonTpl,commonData,l,commonFuc,otherProcess) {
	var ajaxUrl = commonData.ajaxUrl;
	var layerPackage = UiFramework.layerPackage();
	//应用
	var processObj = {
		mediator:null,
		isIndex:"&isIndex=1",
		menuType:"&menuType=2",
		init:function(mediator){
			this.mediator = mediator;

			this.o = mediator.vLis.eq(1);

			this.getHtml();

			otherProcess.init(this);
		},
		getHtml:function(){
			var pThis = this;
			/*t.ajaxJson({
				url:ajaxUrl.getUserProcessListUrl,
				callback:function(result,status){
					//console.log(result);
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					var dom = tpl(commonTpl,{
						processList:result.info.data
					});

					pThis.o.find("#processList").html($($.trim(dom)));
					pThis.bindDom();
				}
			});*/

			$.when($.ajax({
				url:ajaxUrl.getUserProcessUrl,// getUserProcessListUrl
				type:"post"
			}),$.ajax({
				url:ajaxUrl.getOpenListUrl,
				type:"post"
			})).done(function(r1,r2){
				try{
					//if(r1[1] != "success" || r2[1] != "success"){return;}

					var r1Json = $.parseJSON(r1[0]),
					r2Json = $.parseJSON(r2[0]);

					if(r1Json.errcode != 0){layerPackage.fail_screen(r1Json.errmsg);return;}
					if(r2Json.errcode != 0){layerPackage.fail_screen(r2Json.errmsg);return;}

					//流程表单
					var dom = tpl(commonTpl,{
						processList:r1Json.info.data
					});

					pThis.o.find("#processList").html($($.trim(dom)));
					pThis.bindDom();

					if(r2Json.server_list.length != 0){
						var arr = new Array();

						//公用定制模块
						var dom2 = tpl(commonTpl,{
							processList:r2Json.server_list,
							otherProcess:"otherProcess"
						});

						dom2 = $($.trim(dom2));

						pThis.bindOtherProcessDom(dom2);

						pThis.o.find("#processList").append(dom2);
					}
					
					layerPackage.unlock_screen();
				}catch(e){
					layerPackage.fail_screen(e); 
				}
			}).fail(function(){
				layerPackage.fail_screen("请求错误");
			});
		},
		bindDom:function(){
			var pThis = this,mediator = this.mediator,pageManager = mediator.pageManager;
			this.o.find("#processType > a").on("click",function(){
				var id = $(this).attr("data-id");

				pThis.pId = id;

				var listObj = mediator.pFirstDom.find(".advice_list");
				listObj.find("div.wxLine").remove();
	            listObj.find("section").remove();

				if(id == "1"){
					pageManager.go("pDealList");
				}else if(id == "2"){
					pageManager.go("pMineList");
				}else if(id == "3"){
					pageManager.go("pNotifyList");
				}
			});

			this.o.find("#processList").on("click","span:not(.showAllProcess):not(.otherProcess)",function(){
				var o = $(this);
				commonFuc.addClickBg(o);
				var id = o.attr("data-id");

				pThis.linkToProcess(id);
			});

			this.o.find("#processList > span.showAllProcess").on("click",function(){
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2");
				pageManager.go("pAllProcess");
			});
		},
		getProcessHtml:function(pId){
			l.finit();
			var pThis = this;
			var listObj = this.mediator.pFirstDom.find('.advice_list');
			var listUrl = "",contents = null,sortArr = null;
			var activekey = t.getHrefParamVal("activekey") || 0;

			if(this.pId !== undefined){
				activekey = 0;
			}

			try{
				var saveKey = this.mediator.getSaveKey("activekey");
				if(saveKey && (!activekey  || activekey !=0)){
					activekey = saveKey;
				}
			}catch(e){

			}

			if(pId == 1){
				listUrl = ajaxUrl.getDealListUrl;
				contents = [
					{'name':'待办','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已办','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'申请人','sort_name':'applyName','sort_id':1},
					{'name':'申请时间','sort_name':'applyTime','sort_id':2},
					{'name':'流程名称','sort_name':'proName','sort_id':3},
					{'name':'状态','sort_name':'status','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}else if(pId == 2){
				listUrl = ajaxUrl.getMineListUrl;
				contents = [
					{'name':'运行中','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已结束','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'草稿','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'流程实例名','sort_name':'proName','sort_id':1},
					{'name':'表单名','sort_name':'formName','sort_id':2}
				];
			}else if(pId == 3){
				listUrl = ajaxUrl.getNotifyListUrl;
				contents = [
					{'name':'未阅读','activekey':0,'other_data':{'name1':'12123','name2':'aa'}},
					{'name':'已阅读','activekey':1,'other_data':{'name1':'12123','name2':'aa'}}
				];
				sortArr = [
					{'name':'默认','sort_name':'auto','sort_id':0},
					{'name':'申请时间','sort_name':'applyTime','sort_id':1},
					{'name':'发送人','sort_name':'applyName','sort_id':2},
					{'name':'流程名','sort_name':'proName','sort_id':3},
					{'name':'表单名','sort_name':'formName','sort_id':4}
				];
				if(activekey > 1){activekey = 0;}
			}

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keyworks = "",sort = 0,order = 1,listMenuObj = null;

			var getPageData = {
				init:function(activekey,keyworks,sort,order){
					if(sort === undefined){sort = 0;}
					if(order === undefined){order = 1;}
					if(!keyworks){keyworks="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}
							
							l.getList({
								"listUrl":listUrl,
								"listObj":listObj,
								"page":pThis.pageNum,
								"type":activekey,
								"sort":sort,
								"order":order,
								"pro_name":keyworks,
								"listType":pId,
								callback:{
									bindDom:function(res){
										var listType = res.listType,id = res.id;

										window.location.href = "/wx/index.php?debug=1&app=process&a=detail&listType="
										+pId+"&id="+id+"&activekey="+activekey+ pThis.isIndex+pThis.menuType+window.location.hash;
									}
								}
							},function(data,pCount){
								layerPackage.unlock_screen();
								//数据空时显示提示
								if(!data || data.length == 0){
									listMenuObj.showRecord();
									return;
								}

								//pageCount = pCount;
								pThis.pageCount += data.length;
								//if(pageCount == pCount){UiFramework.bottomInfo.init(listObj);return;}
								
								pThis.pageNum ++;
								listObj.attr("is_scroll_loading",false);

								if(data.length < 10 || data.length == pCount || pThis.pageCount == pCount){
									listObj.attr("is_scroll_end",true);
								}

								self.remove_scroll_waiting(listObj);

								UiFramework.bottomInfo.init(listObj);
							});
						}
					});

					listObj.scroll();
				}
			};

			var setting = {
				//列表菜单配置
				target: $('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: contents,
					activekey:activekey
				},
				sort:{ //配置头部插件排序
					status:true, 
					contents:sortArr
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onMenuClick: function(data, obj,self){
						listMenuObj = self;
						akey = data.activekey;

						keyworks = "";

						if(akey == 1 && pId != 2){
			            	var newSortArr = t.clone(sortArr);
			            	newSortArr.push({'name':'处理时间','sort_name':'dealTime','sort_id':5});

			            	listMenuObj.resetSortData(newSortArr);
			            }else{
			        		listMenuObj.resetSortData(sortArr);
			            }

		            	listMenuObj.setSearchValue("");

			            listMenuObj.showRecord();

						getPageData.init(akey);

						if(akey == 0){
							pThis.mediator.removeSaveKey("activekey");
						}else{
							pThis.mediator.setSaveKey("activekey",akey);
							console.log(pThis.mediator.getSaveKey("activekey"));
						}
					},
					onSortClick:function(data){
						sort = data.sort_id;
						if(data.sort_type === undefined || data.sort_type == "asc"){
							order = 0;
						}else if(data.sort_type == "desc"){
							order = 1;
						}
						getPageData.init(akey,keyworks,sort,order);
					},
					onSearchClick:function(key){
						keyworks = key;
						getPageData.init(akey,key,sort,order);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/index/static/Contents/images/weixin_application_nothing01.jpg',//图片地址
					title:'暂时没有内容',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		},
		goToFirstFloor:function(){
			var mediator = this.mediator;
			mediator.footer.removeClass('none hide');
			mediator.container.removeClass('noBottom');
			mediator.fLis.eq(7).click();
		},
		backToIndex:function(){
			//this.geDealNumber();
			var mediator = this.mediator;
			mediator.removeSaveKey("rhId");//清除润恒保存的key值

			mediator.footer.removeClass('none hide');
			mediator.container.removeClass('noBottom');
			this.mediator.fLis.eq(1).click();
		},
		goToDealList:function(data){
			commonFuc.changeWindowTitle(data.name);
			var pId = this.pId || data.pId;

			this.getProcessHtml(pId);

			this.goToFirstFloor();
		},
		goToMineList:function(data){
			this.goToDealList(data);
		},
		goToNotifyList:function(data){
			this.goToDealList(data);
		},
		geDealNumber:function(){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.getDealCountUrl,
				callback:function(result,status){
					var jqObj = pThis.o.find("#processType").find("a:eq(0)").find("i.countNum");
					var o = pThis.mediator.fLis.eq(1).find("i.countNum");
					if(result.errcode !=0){
						commonFuc.countNum(jqObj,0);
						o.addClass('hide');
					}else{
						commonFuc.countNum(jqObj,result.info.count);
						if(result.info.count > 0){
							o.removeClass('hide');
						}
					}
				}
			});
		},
		//其他流程表单
		bindOtherProcessDom:function(dom){
			var pThis = this;

			this.o.on("click","span.otherProcess",function(){
				var o = $(this);
				var type = o.attr("data-type");

				/*润恒*/
				if(type == "runheng"){
					pThis.mediator.pageManager.go("pRHList");
				}
				/*我的报表*/
				else if(type == "report"){
					pThis.mediator.pageManager.go("pMyChart");
				}
				// mrc add
				else if(type == "material"){
					pThis.mediator.pageManager.go("pMyTpl");
				}
				// mrc add end 
				/*我的考勤*/
				else if(type == "checkwork"){
					history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2");
					window.location.href = "/wx/index.php?debug=1&app=workshift&a=timeTag";
				}
			});
			/*
			this.o.on("click","span.otherProcess[data-type=runheng]",function(){
				pThis.mediator.pageManager.go("pRHList");
			});

			this.o.on("click","span.otherProcess[data-type=report]",function(){
				pThis.mediator.pageManager.go("pMyChart");
			});

			this.o.on("click","span.otherProcess[data-type=checkwork]",function(){
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2");
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=timeTag";
			});
			*/
		},
		//润恒
		goToRHList:function(data){
			otherProcess.goToRHList(data);
		},
		//润恒分类列表
		goToRHTypeList:function(data){
			otherProcess.goToRHTypeList(data);
		},
		backToRHList:function(){
			otherProcess.backToRHList();
		},
		goToMyChart:function(data){
			otherProcess.goToMyChart(data);
		},
		goToMyTpl:function(data){
			otherProcess.goToMyTpl(data);
		},
		//----------全部应用--------------
		getAllProcess:function(callback){
			var pThis = this;
			layerPackage.lock_screen();

			t.ajaxJson({
				url:ajaxUrl.getAllProcessUrl,
				callback:function(result,state){
					if(result.errcode != 0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();//遮罩end
					if(callback && t.isFunction(callback)){
						pThis.allProcessData = result.info;
						callback.apply(pThis,[result.info]);
					}
				}
			});
		},
		goToAllProcess:function(data){
			commonFuc.changeWindowTitle(data.name);

			this.getAllProcess(function(info){
				var dom = tpl(commonTpl,{
					allProcess:1,
					data:info
				});

				var pThirdDom = this.mediator.pThirdDom;

				pThirdDom.addClass('bg-white').html($($.trim(dom)));

				this.bindAllProcess(pThirdDom);

				otherProcess.goToThirdFloor();
			});
		},
		canClick:false,
		bindAllProcess:function(dom){
			var pThis = this;
			dom.on("scroll",function(e){
				var o = $(this);
				var top = o.scrollTop();
				o.find(".apTop").attr("style","-webkit-transform: translateY("+top+"px);");
			});

			dom.find(".eidtUserProcess").on("click",function(){
				var o = $(this),
					p = o.parents(".allProcess"),
					click = o.attr("data-click");

				if(click == "0"){
					o.text("完成");
					var span = p.find("span");
					span.removeClass('noBorder').find("i.rp").removeClass('hide');
					o.attr("data-click","1");
					pThis.canClick = true;
				}else if(click == "1"){
					var div_u = dom.find(".userProcess");
					var arr = new Array();

					div_u.find("span:not(.noUserProcess span)").each(function(){
						var id = $(this).attr("data-id");

						if(id){
							arr.push(id);
						}
					});

					function changeTxt(){
						//当前页面的处理
						o.text("管理");
						var span = p.find("span");
						span.addClass('noBorder').find("i.rp").addClass('hide');
						o.attr("data-click","0");
						pThis.canClick = false;
					}

					if(arr.join(",") == pThis.allProcessData.c_ids.join(",")){
						changeTxt();
					}else{
						pThis.saveUserProcess(arr,function(){
							pThis.allProcessData.c_ids = arr;

							//表单页面的处理
							var vLis = pThis.mediator.vLis.eq(1);
							var allSpan = vLis.find(".showAllProcess");
							console.log(allSpan.index());
							vLis.find("#processList span:lt("+allSpan.index()+")").remove();

							var clone_span = div_u.find("span:not(.noUserProcess span):lt(8)").clone();
							clone_span.find("i.rp").remove();

							vLis.find("#processList").prepend(clone_span);

							changeTxt();
						});
					}
				}
			});

			dom.find(".userProcess").on("click","span:not(.noUserProcess span)",function(){
				var o = $(this),
					div = o.parent();

				commonFuc.addClickBg(o);
					
				if(!pThis.canClick){
					pThis.linkToProcess(o.attr("data-id"));
					return;
				}

				o.find("i.rp").removeClass('rulericon-cancel_border bg-e4').addClass('rulericon-add_border bg-green');

				commonFuc.removeClickBg(o);

				dom.find(".otherProcessList").prepend(o);

				var len = div.find("span:not(.noUserProcess span)").length;

				if(len == 0){
					div.addClass('noData').find(".noUserProcess").removeClass('hide');
				}else if(len <= 8){
					dom.find(".noticeMsg").addClass('hide');
				}
			});

			dom.find(".otherProcessList").on("click","span",function(){
				var o = $(this),
					div = o.parent();

				commonFuc.addClickBg(o);
					
				if(!pThis.canClick){
					pThis.linkToProcess(o.attr("data-id"));
					return;
				}

				var div_u = dom.find(".userProcess");
				var len = div_u.find("span:not(.noUserProcess span)").length;

				if(len == 0){
					div_u.removeClass('noData').find(".noUserProcess").addClass('hide');
				}else if(len > 7){
					dom.find(".noticeMsg").removeClass('hide');
				}

				o.find("i.rp").removeClass('rulericon-add_border bg-green').addClass('rulericon-cancel_border bg-e4');

				commonFuc.removeClickBg(o);

				div_u.append(o);
			});
		},
		saveUserProcess:function(form_ids,callback){
			var pThis = this;
			t.ajaxJson({
				url:ajaxUrl.saveUserProcessUrl,
				data:{
					data:{
						form_ids:form_ids
					}
				},
				callback:function(result,state){
					if(result.errcode != 0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();//遮罩end
					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[]);
					}

					commonFuc.success(result);
				}
			})
		},
		linkToProcess:function(id){
			var hash = window.location.hash || "";

			if(!hash){
				history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2");
			}

			window.location.href = "/wx/index.php?model=process&a=create&debug=1&id="+ id + this.isIndex+this.menuType+"&activekey=2#pMineList";
		},
		allProcessBackToIndex:function(){
			this.mediator.pThirdDom.find(".eidtUserProcess").attr("data-click","0");
			this.canClick = false;

			var hash = window.location.hash || "";
			//history.replaceState(null, null,"/wx/index.php?debug=1&app=index&a=index&menuType=2"+hash);

			this.backToIndex();
		}
	};

	return processObj;	
});