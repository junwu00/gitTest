define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/otherProcess.html',
	'index:static/Scripts/commonData',
	'index:static/Scripts/list',
	'index:static/Scripts/commonFuc'
],function($,t,UiFramework,tpl,otherProcessTpl,commonData,l,commonFuc) {
	var layerPackage = UiFramework.layerPackage();
	var otherProcess = {
		processObj:null,
		init:function(processObj){
			this.processObj = processObj;
			this.mediator = processObj.mediator;
		},
		backToRHList:function(){
			this.mediator.fLis.eq(8).click();
		},
		goToSecondFloor:function(showBottom){
			var mediator = this.mediator;
			if(!showBottom){
				mediator.footer.addClass('none');
				mediator.container.addClass('noBottom');
			}
			mediator.fLis.eq(8).click();
		},
		goToThirdFloor:function(){
			var mediator = this.mediator;
			mediator.footer.addClass('none');
			mediator.container.addClass('noBottom');
			mediator.fLis.eq(9).click();
		},
		//润恒
		goToRHList:function(data){
			layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			this.getHtmlForRH(data);

			this.goToSecondFloor();
		},
		goToRHTypeList:function(data,oneData){
			layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			if($.trim(this.mediator.pSecondDom.html()) == "" && !oneData){
				this.getHtmlForRH(data,function(rhData){
					var rhId = this.mediator.getSaveKey("rhId");

					this.rhId = rhId || rhData[0].id;

					this.getHtmlForRHType(data);

					this.goToThirdFloor();
				});

				return;
			}

			this.getHtmlForRHType(data);

			this.goToThirdFloor();
		},
		getHtmlForRH:function(data,callback){
			var pThis = this,
				mediator = this.mediator,
				pSecondDom = mediator.pSecondDom;

			t.ajaxJson({
				url:commonData.ajaxUrl.getRHListUrl,///wx/index.php?model=index&m=msg&cmd=101
				callback:function(result,status){
					if(result.errcode !=0){
						commonFuc.error(result);
						return;
					}

					layerPackage.unlock_screen();

					var rhData = result.data;

					if(rhData.length == 1){
						pThis.rhId = rhData[0].id;
						pThis.goToRHTypeList(data,1);
						return;
					}

					tpl.register('dateFormat', t.dateFormat);
					var dom  = tpl(otherProcessTpl,{
						rhList:true,
						list:rhData
					});
					tpl.unregister('dateFormat');

					pSecondDom.html(dom);

					pSecondDom.find(".rhClickObj").off().on("click",function(){
						var id = $(this).attr("data-id");
						pThis.rhId = id;

						mediator.setSaveKey("rhId",id);
						mediator.pageManager.go("pRHTypeList");
					});

					if(callback && t.isFunction(callback)){
						callback.apply(pThis,[rhData]);
					}
				}
			});
		},
		getHtmlForRHType:function(data){
			var pThis = this,
				pThirdDom = this.mediator.pThirdDom;

			pThirdDom.html("");

			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			UiFramework.scrollLoadingPackage().init({
				target:pThirdDom,
				scroll_target:pThirdDom,
				fun:function(self){
					//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

					if(pThis.pageNum != 1){
						self.add_scroll_waiting($("#powerby"),1);
					}
					
					t.ajaxJson({
						url:commonData.ajaxUrl.getRHTypeListUrl,///wx/index.php?model=index&m=msg&cmd=101
						data:{
							"data":{
								id:pThis.rhId || data.rhId,
								page:pThis.pageNum
							}
						},
						callback:function(result,status){
							if(result.errcode !=0){
								commonFuc.error(result);
								return;
							}

							layerPackage.unlock_screen();

							var listData = result.data;

							tpl.register('dateFormat', t.dateFormat);
							var dom  = tpl(otherProcessTpl,{
								rhTypeList:true,
								listData:listData,
								type:data.type
							});
							tpl.unregister('dateFormat');

							if($.trim(pThirdDom.html()) != ""){
								for (var i = 0; i < listData.length; i++) {
									var o = pThirdDom.find("div.rhTypeList[data-typename="+listData[i].report_name+"]");

									if(o.length == 0){
										tpl.register('dateFormat', t.dateFormat);
										var dom  = tpl(otherProcessTpl,{
											rhTypeList:true,
											listData:[listData[i]],
											type:data.type
										});
										tpl.unregister('dateFormat');

										pThirdDom.append(dom);
									}else{
										tpl.register('dateFormat', t.dateFormat);
										var dom  = tpl(otherProcessTpl,{
											rhTypeList:true,
											listData:[listData[i]],
											type:data.type,
											noShow:1
										});
										tpl.unregister('dateFormat');

										o.last().after($.trim(dom));
									}
								};
							}else{
								pThirdDom.html(dom);
							}
							
							pThis.pageCount += listData.length;
							
							pThis.pageNum ++;
							pThirdDom.attr("is_scroll_loading",false);

							if(listData.length < 10 
								|| listData.length == result.count 
								|| pThis.pageCount == result.count){
								pThirdDom.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting(pThirdDom);
						}
					});
				}
			});

			pThirdDom.scroll();
		},
		/*我的报表*/
		goToMyChart:function(data){
			//layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			this.getHtmlForChart(data);

			this.goToSecondFloor(1);
		},
		// mrc add
		goToMyTpl:function(data){
			//layerPackage.lock_screen();
			commonFuc.changeWindowTitle(data.name);
			//var pId = this.pId || data.pId;

			this.getHtmlForTpl(data);

			this.goToSecondFloor(1);
		},
		getHtmlForTpl:function(data){
			var pThis = this,
				mediator = this.mediator,
				pSecondDom = mediator.pSecondDom;

			pSecondDom.html('<div id="listMenu"></div><div class="wxLine"></div><div class="advice_list scrollY" data-page="0" style="-webkit-height:calc(100% - 45px);height:calc(100% - 45px);"></div>');

			l.finit();
			var pThis = this;
			var listObj = pSecondDom.find('.advice_list');
			var listUrl = "",contents = null;

			listUrl = commonData.ajaxUrl.getChartList;

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keywords = "";

			var getPageData = {
				init:function(keywords){
					if(!keywords){keywords="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					pThis.pageNum = 1,pThis.pageCount = 0; //当前页数

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}
							
							t.ajaxJson({
								url: './wx/index.php?model=process&m=material&cmd=102',///wx/index.php?model=index&m=msg&cmd=101
								data:{
									"data":{
										type: 2,
										page:pThis.pageNum,
										keyword:keywords,
										page_size:20
									}
								},
								callback:function(result,status){
									if(result.errcode !=0){
										commonFuc.error(result);
										return;
									}

									layerPackage.unlock_screen();

									var listData = result.data.data;
									console.log(listData);

									tpl.register('dateFormat', t.dateFormat);
									var dom  = tpl(otherProcessTpl,{
										myTpl:true,
										listData:listData
									});
									tpl.unregister('dateFormat');

									if(listObj.find("section").length == 0 && listData.length == 0){
										listObj.find(".has-no-record").removeClass('hide');
										listObj.css("backgroundColor","#ffffff");
									}else if(listData.length != 0){
										listObj.find(".has-no-record").addClass('hide');
										listObj.css("backgroundColor","#F6F6F6");
										listObj.append(dom);
									}
									
									pThis.pageCount += listData.length;
									
									pThis.pageNum ++;
									listObj.attr("is_scroll_loading",false);

									if(listData.length < 20 
										|| listData.length == result.count 
										|| pThis.pageCount == result.count){
										listObj.attr("is_scroll_end",true);
									}

									self.remove_scroll_waiting(listObj);
								}
							});
						}
					});

					listObj.scroll();

					this.bindDom(pSecondDom);
				},
				bindDom:function(obj){
					console.log(obj)
					obj.off().on("click",".advice_list section",function(){
						id = $(this).attr("data-id"); 
						window.location.href = "/wx/index.php?model=process&a=importMaterial#/"+id;
					});
				}
			};

			getPageData.init("");

			//return;
			var setting = {
				//列表菜单配置
				target: pSecondDom.find('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: []
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onSearchClick:function(key){
						keywords = key;
						getPageData.init(key);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/index/static/Contents/images/weixin_application_nothing01.jpg',//图片地址
					title:'暂时没有内容',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		},
		// mrc add end
		getHtmlForChart:function(data){
			var pThis = this,
				mediator = this.mediator,
				pSecondDom = mediator.pSecondDom;

			pSecondDom.html('<div id="listMenu"></div><div class="wxLine"></div><div class="advice_list scrollY" data-page="0" style="-webkit-height:calc(100% - 45px);height:calc(100% - 45px);"></div>');

			l.finit();
			var pThis = this;
			var listObj = pSecondDom.find('.advice_list');
			var listUrl = "",contents = null;

			listUrl = commonData.ajaxUrl.getChartList;

			var listMenu = UiFramework.listMenuPackage();
			pThis.pageNum = 1,pThis.pageCount = 0; //当前页数
			pThis.akey = 0;
			var keywords = "";

			var getPageData = {
				init:function(keywords){
					if(!keywords){keywords="";}

					layerPackage.lock_screen();
					pThis.pageNum = 1;
					pThis.pageCount = 0;

		            listObj.find("div.wxLine").remove();
		            listObj.find("section").remove();
		            UiFramework.bottomInfo.remove();

					pThis.pageNum = 1,pThis.pageCount = 0; //当前页数

					UiFramework.scrollLoadingPackage().init({
						target:listObj,
						scroll_target:listObj,
						fun:function(self){
							//if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

							if(pThis.pageNum != 1){
								self.add_scroll_waiting($("#powerby"),1);
							}
							
							t.ajaxJson({
								url:listUrl,///wx/index.php?model=index&m=msg&cmd=101
								data:{
									"data":{
										page:pThis.pageNum,
										keywords:keywords,
										page_size:20
									}
								},
								callback:function(result,status){
									if(result.errcode !=0){
										commonFuc.error(result);
										return;
									}

									layerPackage.unlock_screen();

									var listData = result.data;
									console.log(listData);

									tpl.register('dateFormat', t.dateFormat);
									var dom  = tpl(otherProcessTpl,{
										myChart:true,
										listData:listData
									});
									tpl.unregister('dateFormat');

									if(listObj.find("section").length == 0 && listData.length == 0){
										listObj.find(".has-no-record").removeClass('hide');
										listObj.css("backgroundColor","#ffffff");
									}else if(listData.length != 0){
										listObj.find(".has-no-record").addClass('hide');
										listObj.css("backgroundColor","#F6F6F6");
										listObj.append(dom);
									}
									
									pThis.pageCount += listData.length;
									
									pThis.pageNum ++;
									listObj.attr("is_scroll_loading",false);

									if(listData.length < 20 
										|| listData.length == result.count 
										|| pThis.pageCount == result.count){
										listObj.attr("is_scroll_end",true);
									}

									self.remove_scroll_waiting(listObj);
								}
							});
						}
					});

					listObj.scroll();

					this.bindDom(pSecondDom);
				},
				bindDom:function(obj){
					obj.off().on("click",".advice_list section",function(){
						var type = $(this).attr("data-type"),
							id = $(this).attr("data-id"); 

						if(type == 1){
							window.location.href = "/wx/index.php?debug=1&app=finstat&a=chart&id=" + id;
						}else if(type == 2){
							window.location.href = "/wx/index.php?app=finstat&m=ajax&a=report_detail&id="+id;
						}
					});
				}
			};

			getPageData.init("");

			//return;
			var setting = {
				//列表菜单配置
				target: pSecondDom.find('#listMenu'), //添加的jq dom对象
				menu: { //配置头部插件菜单
					contents: []
				},
				search:{ //搜索项配置
					status:true //状态 false 取消 true 启用
				},
				callback: {
					onSearchClick:function(key){
						keywords = key;
						getPageData.init(key);
					}
				},
				noRecord : {
					target:listObj,
					imgSrc : '../view/index/static/Contents/images/weixin_application_nothing01.jpg',//图片地址
					title:'暂时没有内容',//内容标题
					desc:'' //内容说明，数组形式 或者 单个字串
				}
			};

			listMenu.init(setting);
		}
	};

	return otherProcess;	
});