require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'index:static/Scripts/commonData',
	'index:static/Scripts/pageManager-v2.0',
	/*----*/
	'index:static/Scripts/process-v2.0',
	'index:static/Scripts/mySetting',
	'index:static/Scripts/commonFuc-v2.0'
],function($,t,UiFramework,commonData,pageManager,process,mySetting,commonFuc) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	//layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;//专门处理请求URL

	var mediator = {
		process:null, /*应用对象*/
		mySetting:null, /*我的对象*/
		pageManager:null, /*路由对象*/
		init:function(params){
			this.params = params;

			this.getDom(params.container);

			//打卡相关信息
			this.getSignUpInfo();

			this.process = params.process;
			params.process.init(this);

			this.mySetting = params.mySetting;
			params.mySetting.init(this);

			this.pageManager = params.pageManager;
			
			this.bindDom();

			this.pageManagerInit();
		},
		fini:function(){
			this.process = null;
			this.mySetting = null;
			this.pageManager = null;
		},
		getDom:function(container){
			//jqObj对象
			this.container = container;
			this.firstFloor = $(".firstFloor");
			this.secondFloor = $(".secondFloor");
			this.thirdFloor = $(".thirdFloor");
		},
		bindDom:function(){
			var pThis = this,
				container = pThis.container,
				pageManager = pThis.pageManager;

			//缩小时点击还原
			container.on("click",".coverDiv",function(){
				if(!container.hasClass('closeMainContent')){return;}

				window.history.go(-1);
			})

			//点击进入我的设置
			container.find("#userPage").click(function(){
				pageManager.go('openMySetting');
			});

			//打卡外出
			container.find("#signUp").click(function(){
				var type = $(this).attr("data-type") || 1;
				window.location.href = "/wx/index.php?debug=1&app=workshift&a=signIn&type="+type;
			});
		},
		pageManagerInit:function(){
			var pThis = this,
				container = pThis.container,
				mySetting = this.mySetting;

			var home = {
		        name: 'home',
		        url: '#',
		        events: {
		        }
		    };

		    //我的设置
		    var openMySetting = {
		    	name: 'openMySetting',
		        url: '#openMySetting',
		        id:1,
		        floor:1,
		        pId:0,
		        events: {  
		        },
		        data:{
		        	name:"我的设置"
		        },
		        go: function(){
		        	console.log("go 我的设置");
		        	//pThis.mySetting.getInfoHtml();
		        	container.removeClass('openMainContent').addClass('animated closeMainContent');
		        	container.find(".coverDiv").removeClass('hide');
		        },
		        back:function(){
		        	console.log("back 我的设置");
		        	container.removeClass('closeMainContent').addClass('openMainContent');
		        	container.find(".coverDiv").addClass('hide');
		        }
		    };

		    //指纹识别设置
		    var mSoterPage1 = {
		    	name: 'mSoterPage1',
		        url: '#mSoterPage1',
		        id:2,
		        floor:2,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:6,
		        	pageNum:1,
		        	name:"指纹识别设置"
		        },
		        go: function(){
		        	console.log("指纹识别设置 go");
		        	mySetting.goToSoterPage(this.data);
		        },
		        back:function(){
		        	console.log("指纹识别设置 back");
		        	mySetting.backToMySetting();
		        }
		    };

		    //指纹识别设置
		    var mSoterPage2 = {
		    	name: 'mSoterPage2',
		        url: '#mSoterPage2',
		        id:2,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:6,
		        	pageNum:2,
		        	name:"指纹识别设置"
		        },
		        go: function(){
		        	console.log("指纹识别设置2 go");
		        	mySetting.goToSoterPage2(this.data);
		        },
		        back:function(){
		        	console.log("指纹识别设置2 back");
		        	mySetting.backToSoterPage();
		        }
		    };

		    //产品服务与支持
		    var mProduct = {
		    	name: 'mProduct',
		        url: '#mProduct',
		        id:3,
		        floor:2,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	name:"产品服务与支持"
		        },
		        go: function(){
		        	console.log("产品服务与支持第1层 go");
		        	mySetting.goToProduct(this.data);
		        },
		        back:function(){
		        	console.log("产品服务与支持第1层 back");
		        	mySetting.backToMySetting();
		        }
		    };

		    //角色体验
		    var mRole = {
		    	name: 'mRole',
		        url: '#mRole',
		        id:3,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:1,
		        	name:"角色体验"
		        },
		        go: function(){
		        	console.log("角色体验第1层 go");
		        	mySetting.goToRole(this.data);
		        },
		        back:function(){
		        	console.log("角色体验第1层 back");
		        	mySetting.backToProduct(this.data);
		        }
		    };
		    //产品案例
		    var mProductDemo = {
		    	name: 'mProductDemo',
		        url: '#mProductDemo',
		        id:3,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:2,
		        	name:"产品案例"
		        },
		        go: function(){
		        	console.log("产品案例第1层 go");
		        	mySetting.goToProductDemo(this.data);
		        },
		        back:function(){
		        	console.log("产品案例第1层 back");
		        	mySetting.backToProduct(this.data);
		        }
		    };
			//使用指引
		    var mUseHelper = {
		    	name: 'mUseHelper',
		        url: '#mUseHelper',
		        id:3,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:3,
		        	name:"使用指引"
		        },
		        go: function(){
		        	console.log("使用指引第1层 go");
		        	mySetting.goToUseHelper(this.data);
		        },
		        back:function(){
		        	console.log("使用指引第1层 back");
		        	mySetting.backToProduct(this.data);
		        }
		    };

		    //VIP
		    var mVip = {
		    	name: 'mVip',
		        url: '#mVip',
		        id:4,
		        floor:2,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:2,
		        	name:"VIP专享会员"
		        },
		        go: function(){
		        	console.log("VIP第1层 go");
		        	mySetting.goToVip(this.data);
		        },
		        back:function(){
		        	console.log("VIP第1层 back");
		        	mySetting.backToMySetting();
		        }
		    };
		    //vip会员特权
		    var mVipPrivilege = {
		    	name: 'mVipPrivilege',
		        url: '#mVipPrivilege',
		        id:4,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:100,
		        	vId:100,
		        	name:"vip会员特权"
		        },
		        go: function(){
		        	console.log("vip会员特权 go");
		        	mySetting.goToVipPrivilege(this.data);
		        },
		        back:function(){
		        	console.log("vip会员特权 back");
		        	mySetting.backToVip(this.data);
		        }
		    };
		    //邀请同事发起流程
		    var mVipService1 = {
		    	name: 'mVipService1',
		        url: '#mVipService1',
		        id:4,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	id:1,
		        	name:"邀请同事发起流程"
		        },
		        go: function(){
		        	console.log("邀请同事发起流程 go");
		        	mySetting.goToVipService(this.data);
		        },
		        back:function(){
		        	console.log("邀请同事发起流程 back");
		        	mySetting.backToVip(this.data);
		        }
		    };
		    //员工分享有礼
		    var mVipService2 = {
		    	name: 'mVipService2',
		        url: '#mVipService2',
		        id:4,
		        floor:3,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	id:2,
		        	name:"员工分享有礼"
		        },
		        go: function(){
		        	console.log("员工分享有礼 go");
		        	mySetting.goToVipService(this.data);
		        },
		        back:function(){
		        	console.log("员工分享有礼 back");
		        	mySetting.backToVip(this.data);
		        }
		    };

		    //管理员服务中心
		    var mAdminService = {
		    	name: 'mAdminService',
		        url: '#mAdminService',
		        id:5,
		        floor:2,
		        pId:1,
		        events: {  
		        },
		        data:{
		        	mId:3,
		        	name:"管理员服务中心"
		        },
		        go: function(){
		        	console.log("管理员服务中心第1层 go");
		        	mySetting.goToAdminService(this.data);
		        },
		        back:function(){
		        	console.log("管理员服务中心第1层 back");
		        	mySetting.backToMySetting();
		        }
		    };

		    //全部应用
	    	var pAllProcess = {
		    	name: 'pAllProcess',
		        url: '#pAllProcess',
		        id:6,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:5,
		        	name:"全部应用"
		        },
		        go: function(){
		        	console.log("全部应用 go");
		        	process.goToAllProcess(this.data);
		        },
		        back:function(){
		        	console.log("全部应用 back");
		        	process.allProcessBackToIndex();
		        }
		    };

		    //待办/已办
		    var pDealList = {
		    	name: 'pDealList',
		        url: '#pDealList',
		        id:7,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:1,
		        	name:"待审批/审批"
		        },
		        go: function(){
		        	console.log("待办/已办 go");
		        	process.goToDealList(this.data);
		        },
		        back:function(){
		        	console.log("待办/已办 back");
		        	process.backToIndex(1);
		        }
		    };
		    //我的发起
		    var pMineList = {
		    	name: 'pMineList',
		        url: '#pMineList',
		        id:8,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:2,
		        	name:"我的发起"
		        },
		        go: function(){
		        	console.log("我的发起 go");
		        	process.goToMineList(this.data);
		        },
		        back:function(){
		        	console.log("我的发起 back");
		        	process.backToIndex(1);
		        }
		    };
		    //我的知会
		    var pNotifyList = {
		    	name: 'pNotifyList',
		        url: '#pNotifyList',
		        id:9,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:3,
		        	name:"我的知会"
		        },
		        go: function(){
		        	console.log("我的知会 go");
		        	process.goToNotifyList(this.data);
		        },
		        back:function(){
		        	console.log("我的知会 back");
		        	process.backToIndex(1);
		        }
		    };

		    //润衡
		    var pRHList = {
		    	name: 'pRHList',
		        url: '#pRHList',
		        id:10,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"润衡"
		        },
		        go: function(){
		        	console.log("润衡 go");
		        	process.goToRHList(this.data);
		        },
		        back:function(){
		        	console.log("润衡 back");
		        	process.backToIndex(1);
		        }
		    };

		    //润衡分类列表
		    var pRHTypeList = {
		    	name: 'pRHTypeList',
		        url: '#pRHTypeList',
		        id:10,
		        floor:2,
		        pId:10,
		        events: {  
		        },
		        data:{
		        	rhId:7,
		        	type:1,//0 显示年月
		        	name:"润衡"
		        },
		        go: function(){
		        	console.log("润衡分类列表 go");
		        	process.goToRHTypeList(this.data);
		        },
		        back:function(){
		        	console.log("润衡分类列表 back");
		        	process.backToRHList();
		        }
		    };

		    //我的报表
		    var pMyChart = {
		    	name: 'pMyChart',
		        url: '#pMyChart',
		        id:11,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"我的报表"
		        },
		        go: function(){
		        	console.log("我的报表 go");
		        	process.goToMyChart(this.data);
		        },
		        back:function(){
		        	console.log("我的报表 back");
		        	process.backToIndex(1);
		        }
		    };

		    //我的报销
		    var pMyExaccount = {
		    	name: 'pMyExaccount',
		        url: '#pMyExaccount',
		        id:12,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"我的报销"
		        },
		        go: function(){
		        	console.log("我的报销 go");
		        	process.goToMyExaccount(this.data);
		        },
		        back:function(){
		        	console.log("我的报销 back");
		        	process.backToIndex(1);
		        }
		    };

    	   	this.pageManager
				.push(home)
				.push(openMySetting)
				.push(mSoterPage1)
				.push(mSoterPage2)
				.push(mProduct)
				.push(mRole)
				.push(mProductDemo)
				.push(mUseHelper)
				.push(mVip)
				.push(mVipPrivilege)
				.push(mVipService1)
				.push(mVipService2)
				.push(mAdminService)
				.push(pAllProcess)
				.push(pDealList)
				.push(pMineList)
				.push(pNotifyList)
				.push(pRHList)
				.push(pRHTypeList)
				.push(pMyChart)
				.push(pMyExaccount)
				.default('home')
				.init();
		},
		//交互性操作
		setSaveKey:function(key,val){
			try{
				//window.sessionStorage[key] = val;
				window.sessionStorage.setItem(key,val);
			}catch(e){
				console.log(e);
			}
		},
		getSaveKey:function(key){
			try{
				return window.sessionStorage.getItem(key);
			}catch(e){
				console.log(e);
			}
		},
		removeSaveKey:function(key){
			console.log(1);
			try{
				if(key){
					window.sessionStorage.removeItem(key);
				}else{
					window.sessionStorage.clear();
				}
			}catch(e){
				console.log(e);
			}
		},
		//获取用户打卡信息
		getSignUpInfo:function(){
			var pThis = this,container = pThis.container;
			t.ajaxJson({
				url:ajaxUrl.getUserSignUpUrl,
				callback:function(result,status){
					if(result.errcode != 0){
						commonFuc.error(result);
						return;
					}

					console.log(result);
					var info = result.info;
					//显示用户名
					//container.find("#titleBar .userName").text(info.user_name+"的主页型");

					//打卡相关显示
					var signUpObj = container.find("#signUp");

					if(info.clock_time.length == 0){
						return;
					}

					if(info.type == 2){
						signUpObj.find(".signUpTitle").text("定位");
						signUpObj.find(".signUpIcon i ").removeClass('rulericon-checked').addClass('rulericon-plain');
					}

					signUpObj.removeClass('hide');
					signUpObj.attr("data-type",info.type); //1 打卡 2外出
					signUpObj.find(".signUpInfo").text(info.clock_time.join(";"));
				}
			});
		}
	};

	mediator.init({
		container:$(".mainContent"),
		process:process,
		mySetting:mySetting,
		pageManager:pageManager
	});

	//layerPackage.unlock_screen();
});