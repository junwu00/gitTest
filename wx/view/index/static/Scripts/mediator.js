require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'index:static/Scripts/commonData',
	'index:static/Scripts/pageManager',
	/*----*/
	'index:static/Scripts/newsObj',
	'index:static/Scripts/searchObj',
	'index:static/Scripts/processObj',
	'index:static/Scripts/personalObj',
	'index:static/Scripts/commonFuc'
],function($,t,UiFramework,commonData,pageManager,newsObj,searchObj,processObj,personalObj,commonFuc) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;//专门处理请求URL

	var menuType = t.getUrlParam("menuType") || 1;
	var menuNum = menuType - 1;

	var mediator = {
		newsObj: null, /*消息对象*/
		searchObj:null, /*搜索对象*/
		processObj:null, /*应用对象*/
		personalObj:null, /*我的对象*/
		pageManager:null,
		init:function(params){
			this.params = params;

			this.getDom(params.container);

			this.newsObj = params.newsObj;
			params.newsObj.init(this);

			this.searchObj = params.searchObj;
			params.searchObj.init(this);

			this.processObj = params.processObj;
			params.processObj.init(this);

			this.personalObj = params.personalObj;
			params.personalObj.init(this);

			this.pageManager = params.pageManager;
			
			this.bindDom();

			this.pageManagerInit();
			
		},
		fini:function(){
			this.newsObj = null;
			this.searchObj = null;
			this.processObj = null;
			this.personalObj = null;
			this.pageManager = null;
		},
		getDom:function(container){
			//jqObj对象
			this.container   =   container;
			this.view        =   this.container.find(".view-container");
			this.vLis        =   this.container.find(".view-container > li");//view.find("li");
			this.footer      =   this.container.find("footer");
			this.fLis        =   this.footer.find("ul.baseBtn li");
			this.nFirstDom   =   this.view.find("li.nFirst");
			this.nSecondDom  =   this.view.find("li.nSecond");
			this.mFirstDom   =   this.view.find("li.mFirst");
			this.mSecondDom  =   this.view.find("li.mSecond");
			this.pFirstDom   =   this.view.find("li.pFirst");
			this.pSecondDom  =   this.view.find("li.pSecond");
			this.pThirdDom  =   this.view.find("li.pThird");
			this.mThirdDom  =   this.view.find("li.mThird");
		},
		bindDom:function(){
			var fLis = this.fLis,
				vLis = this.vLis,
				container = this.container,
				view = this.view,
				footer = this.footer,
				pageManager = this.pageManager,
				processObj = this.processObj,
				pThis = this;

			var menuType = t.getUrlParam("menuType") || 1;
			var menuNum = menuType - 1;
			vLis.removeClass('pageOld pageInt pageNew');
			if (menuType == 1) {
				vLis.eq(menuNum).addClass('pageInt').siblings().addClass('pageNew');
			} else if (menuType > 1 && menuType < vLis.length) {
				vLis.eq(menuNum).addClass('pageInt');
				this.container.find(".view-container > li:lt(" + menuNum + ")").addClass('pageOld');
				this.container.find(".view-container > li:gt(" + menuNum + ")").addClass('pageNew');
			} else if (menuType == vLis.length) {
				vLis.eq(menuType - 1).addClass('pageInt').siblings().addClass('pageOld');
			}

			fLis.eq(menuType - 1).addClass('current').siblings().removeClass('current');

			if(menuType == 2){
				//processObj.geDealNumber();
			}
			processObj.geDealNumber();

			container.removeClass('hide');

			//.slideSlow 慢速滑动，一定是左边的页面
			//.slideFast 快速滑动，一定是右边的页面
			//.slideSlowBack 返回时，慢速滑动，一定是左边的页面
			//.slideFastBack 返回时，快速滑动，一定是右边的页面
			//.pageInt  初始页面位置
			//.pageOld  正向划过页面位置 -100%
			//.pageNew  正向未滑页面位置 +100%

			//记录滑动过的页面
			var currentNum = menuNum; //0

			fLis.on("click", function() {
				var index = $(this).index(),
					num = index - currentNum;

				//当前页面
				if (num == 0) {
					return;
				}

				var fCurrent = 0;
				if (index > 2) {
					if (index == 3 || index == 4) {
						fCurrent = 0;
					} else if (index == 5 || index == 6) {
						fCurrent = 2;
					} else if (index == 7 || index == 8) {
						fCurrent = 1;
					}
				} else {
					fCurrent = index;
				}

				if (fCurrent == 1 && index == 1) {
					processObj.geDealNumber();
				}

				if (index < 3) {
					commonFuc.changeWindowTitle(commonData.version);
					pThis.removeSaveKey();
				}

				var pageStack = pageManager.getPageStack();
				//console.log(pageStack);
				if (index < 3 && pageStack.length > 1 && !vLis.eq(fCurrent).hasClass('pageInt')) {
					pageManager.clearOtherPageStack();

					if (footer.find("li.current").index() == index) {
						//window.history.go(-1);
						//return;
					} else {
						if (pageStack.length > 1) {
							pageStack.pop();
						}

					}

					var activekey = t.getUrlParam("activekey");

					if ((activekey === undefined || activekey == null) && menuType == 2) {
						try {
							if (WeixinJSBridge) {
								window.history.go(-2);
							}
						} catch (e) {
							console.log(e);
							console.log(document.title);
							console.log(window.location.hash);
							window.history.go(-1);
						}
					}
				}

				fLis.eq(fCurrent).addClass('current').siblings().removeClass('current');

				var currentPage = view.find("li.page-container:eq(" + currentNum + ")");
				var clickPage = view.find("li.page-container:eq(" + index + ")");

				vLis.removeClass('slideSlow slideFast slideSlowBack slideFastBack');

				//向前
				if (num > 0) {
					currentPage.removeClass("pageInt pageNew").addClass('pageOld slideSlow');
					clickPage.addClass('pageInt slideFast').removeClass("pageOld pageNew");
				}
				//后退
				else if (num < 0) {
					currentPage.removeClass("pageInt pageOld").addClass('pageNew slideFastBack');
					clickPage.addClass('pageInt slideSlowBack').removeClass("pageOld pageNew");
				}

				if (num > 1 || num < -1) {
					vLis.each(function(i, el) {
						if (i < index && i > currentNum) {
							$(this).addClass('pageOld').removeClass('pageNew');
						} else if (i > index && i < currentNum) {
							$(this).addClass('pageNew').removeClass('pageOld');
						}
					});
				}

				currentNum = index;

				vLis.eq(index).find("section.clickBg").removeClass('clickBg');

				if (index < 3) {
					while(pageStack.length > 1){
						pageStack.pop();
					};

					window.location.hash = "";

					/*document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
						WeixinJSBridge.call('hideToolbar');
						WeixinJSBridge.call('hideOptionMenu');
					});*/
				} else {
					/*document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
						WeixinJSBridge.call('showToolbar');
						WeixinJSBridge.call('showOptionMenu');
					});*/
				}
			});
		},
		pageManagerInit:function(){
			var newsObj = this.newsObj,
				processObj = this.processObj,
				personalInfo = this.personalInfo;
			//主页
			var home = {
		        name: 'home',
		        url: '#',
		        events: {
		        }
		    };

		    //消息详情
		    var nMsgInfo = {
		    	name: 'nMsgInfo',
		        url: '#nMsgInfo',
		        type:1,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	name:"消息详情"
		        },
		        go: function(){
		        	console.log("消息详情第1层 go");
		        	newsObj.goToMsgInfo(this.data);
		        },
		        back:function(){
		        	console.log("消息详情第1层 back");
		        	newsObj.backToIndex();
		        }
		    };

		    //产品服务与支持
		    var mProduct = {
		    	name: 'mProduct',
		        url: '#mProduct',
		        type:2,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	name:"产品服务与支持"
		        },
		        go: function(){
		        	console.log("产品服务与支持第1层 go");
		        	personalObj.goToProduct(this.data);
		        },
		        back:function(){
		        	console.log("产品服务与支持第1层 back");
		        	personalObj.backToIndex();
		        }
		    };
		    //角色体验
		    var mRole = {
		    	name: 'mRole',
		        url: '#mRole',
		        type:2,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:1,
		        	name:"角色体验"
		        },
		        go: function(){
		        	console.log("角色体验第1层 go");
		        	personalObj.goToRole(this.data);
		        },
		        back:function(){
		        	console.log("角色体验第1层 back");
		        	personalObj.backToProduct(this.data);
		        }
		    };
		    //产品案例
		    var mProductDemo = {
		    	name: 'mProductDemo',
		        url: '#mProductDemo',
		        type:2,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:2,
		        	name:"产品案例"
		        },
		        go: function(){
		        	console.log("产品案例第1层 go");
		        	personalObj.goToProductDemo(this.data);
		        },
		        back:function(){
		        	console.log("产品案例第1层 back");
		        	personalObj.backToProduct(this.data);
		        }
		    };
			//操作指引
		    var mUseHelper = {
		    	name: 'mUseHelper',
		        url: '#mUseHelper',
		        type:2,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	sId:3,
		        	name:"使用指引"
		        },
		        go: function(){
		        	console.log("使用指引第1层 go");
		        	personalObj.goToUseHelper(this.data);
		        },
		        back:function(){
		        	console.log("使用指引第1层 back");
		        	personalObj.backToProduct(this.data);
		        }
		    };
		    //常见问题
		    var mQuestionDetail = {
		    	name: 'mQuestionDetail',
		        url: '#mQuestionDetail',
		        type:2,
		        floor:3,
		        events: {  
		        },
		        data:{
		        	mId:1,
		        	name:"常见问题"
		        },
		        go: function(){
		        	console.log("常见问题详情 go");
		        	personalObj.goToQuestionDetail(this.data);
		        },
		        back:function(){
		        	console.log("常见问题详情 back");
		        	personalObj.backToProduct(this.data);
		        }
		    };
		    //VIP
		    var mVip = {
		    	name: 'mVip',
		        url: '#mVip',
		        type:3,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:2,
		        	name:"VIP专享会员"
		        },
		        go: function(){
		        	console.log("VIP第1层 go");
		        	personalObj.goToVip(this.data);
		        },
		        back:function(){
		        	console.log("VIP第1层 back");
		        	personalObj.backToIndex();
		        }
		    };
		    //VIP专享会员
		    var mVipPrivilege = {
		    	name: 'mVipPrivilege',
		        url: '#mVipPrivilege',
		        type:3,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:100,
		        	vId:100,
		        	name:"VIP专享会员"
		        },
		        go: function(){
		        	console.log("vip会员特权 go");
		        	personalObj.goToVipPrivilege(this.data);
		        },
		        back:function(){
		        	console.log("vip会员特权 back");
		        	personalObj.backToVip(this.data);
		        }
		    };
		    //邀请同事发起流程
		    var mVipService1 = {
		    	name: 'mVipService1',
		        url: '#mVipService1',
		        type:3,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	id:1,
		        	name:"邀请同事发起流程"
		        },
		        go: function(){
		        	console.log("邀请同事发起流程 go");
		        	personalObj.goToVipService(this.data);
		        },
		        back:function(){
		        	console.log("邀请同事发起流程 back");
		        	personalObj.backToVip(this.data);
		        }
		    };
		    //员工分享有礼
		    var mVipService2 = {
		    	name: 'mVipService2',
		        url: '#mVipService2',
		        type:3,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	id:2,
		        	name:"员工分享有礼"
		        },
		        go: function(){
		        	console.log("员工分享有礼 go");
		        	personalObj.goToVipService(this.data);
		        },
		        back:function(){
		        	console.log("员工分享有礼 back");
		        	personalObj.backToVip(this.data);
		        }
		    };
		    //推荐企业购买
		    var mVipService3 = {
		    	name: 'mVipService3',
		        url: '#mVipService3',
		        type:4,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:5,
		        	name:"推荐企业购买"
		        },
		        go: function(){
		        	console.log("推荐企业购买 go");
		        	personalObj.goToVipService1(this.data);
		        },
		        back:function(){
		        	console.log("推荐企业购买 back");
		        	personalObj.backToIndexForVs1()(this.data);
		        }
		    };
		    //管理员服务中心
		    var mAdminService = {
		    	name: 'mAdminService',
		        url: '#mAdminService',
		        type:5,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:3,
		        	name:"管理员服务中心"
		        },
		        go: function(){
		        	console.log("管理员服务中心第1层 go");
		        	personalObj.goToAdminService(this.data);
		        },
		        back:function(){
		        	console.log("管理员服务中心第1层 back");
		        	personalObj.backToIndex();
		        }
		    };
		    //编辑信息第一层
		    var mEditInfo = {
		    	name: 'mEditInfo',
		        url: '#mEditInfo',
		        type:6,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:4,
		        	name:"编辑个人信息"
		        },
		        go: function(){
		        	console.log("编辑信息第1层 go");
		        	personalObj.goToEditInfo(this.data);
		        },
		        back:function(){
		        	console.log("编辑信息第1层 back");
		        	personalObj.backToIndex();
		        }
		    };
		    //编辑性别
		    var mEditGender = {
		    	name: 'mEditGender',
		        url: '#mEditGender',
		        type:6,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:4,
		        	ePart:"gender",
		        	name:"编辑性别"
		        },
		        go: function(){
		        	console.log("编辑性别 go");
		        	personalObj.goToEditGender(this.data);
		        },
		        back:function(){
		        	console.log("编辑性别 back");
		        	personalObj.backToEditInfo();
		        }
		    };
		    //编辑手机号码
		    var mEditPhone = {
		    	name: 'mEditPhone',
		        url: '#mEditPhone',
		        type:6,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:4,
		        	ePart:"phone",
		        	name:"编辑手机号码"
		        },
		        go: function(){
		        	console.log("编辑手机号码 go");
		        	personalObj.goToEditPhone(this.data);
		        },
		        back:function(){
		        	console.log("编辑手机号码 back");
		        	personalObj.backToEditInfo();
		        }
		    };
		    //编辑邮箱
		    var mEditEmail = {
		    	name: 'mEditEmail',
		        url: '#mEditEmail',
		        type:6,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:4,
		        	ePart:"mail",
		        	name:"编辑邮箱"
		        },
		        go: function(){
		        	console.log("编辑邮箱 go");
		        	personalObj.goToEditEmail(this.data);
		        },
		        back:function(){
		        	console.log("编辑邮箱 back");
		        	personalObj.backToEditInfo();
		        }
		    };
		    //待办/已办
		    var pDealList = {
		    	name: 'pDealList',
		        url: '#pDealList',
		        type:7,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:1,
		        	name:"待办/已办"
		        },
		        go: function(){
		        	console.log("待办/已办 go");
		        	processObj.goToDealList(this.data);
		        },
		        back:function(){
		        	console.log("待办/已办 back");
		        	processObj.backToIndex();
		        }
		    };
		    //我的发起
		    var pMineList = {
		    	name: 'pMineList',
		        url: '#pMineList',
		        type:8,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:2,
		        	name:"我的发起"
		        },
		        go: function(){
		        	console.log("我的发起 go");
		        	processObj.goToMineList(this.data);
		        },
		        back:function(){
		        	console.log("我的发起 back");
		        	processObj.backToIndex();
		        }
		    };
		    //我的知会
		    var pNotifyList = {
		    	name: 'pNotifyList',
		        url: '#pNotifyList',
		        type:9,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:3,
		        	name:"我的知会"
		        },
		        go: function(){
		        	console.log("我的知会 go");
		        	processObj.goToNotifyList(this.data);
		        },
		        back:function(){
		        	console.log("我的知会 back");
		        	processObj.backToIndex();
		        }
		    };

		    //润衡
		    var pRHList = {
		    	name: 'pRHList',
		        url: '#pRHList',
		        type:10,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"润衡"
		        },
		        go: function(){
		        	console.log("润衡 go");
		        	processObj.goToRHList(this.data);
		        },
		        back:function(){
		        	console.log("润衡 back");
		        	processObj.backToIndex();
		        }
		    };

		    //润衡分类列表
		    var pRHTypeList = {
		    	name: 'pRHTypeList',
		        url: '#pRHTypeList',
		        type:10,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	rhId:7,
		        	type:1,//0 显示年月
		        	name:"润衡"
		        },
		        go: function(){
		        	console.log("润衡分类列表 go");
		        	processObj.goToRHTypeList(this.data);
		        },
		        back:function(){
		        	console.log("润衡分类列表 back");
		        	processObj.backToRHList();
		        }
		    };

		    //我的报表
		    var pMyChart = {
		    	name: 'pMyChart',
		        url: '#pMyChart',
		        type:11,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"我的报表"
		        },
		        go: function(){
		        	console.log("我的报表 go");
		        	processObj.goToMyChart(this.data);
		        },
		        back:function(){
		        	console.log("我的报表 back");
		        	processObj.backToIndex();
		        }
		    };

		    //我的moban
		    var pMyTpl = {
		    	name: 'pMyTpl',
		        url: '#pMyTpl',
		        type:11,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:4,
		        	type:1,//0 显示年月
		        	name:"我的模板"
		        },
		        go: function(){
		        	console.log("我的模板 go");
		        	processObj.goToMyTpl(this.data);
		        },
		        back:function(){
		        	console.log("我的模板 back");
		        	processObj.backToIndex();
		        }
		    };

		    //全部应用
	    	var pAllProcess = {
		    	name: 'pAllProcess',
		        url: '#pAllProcess',
		        type:12,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	pId:5,
		        	name:"全部应用"
		        },
		        go: function(){
		        	console.log("全部应用 go");
		        	processObj.goToAllProcess(this.data);
		        },
		        back:function(){
		        	console.log("全部应用 back");
		        	processObj.allProcessBackToIndex();
		        }
		    };

		    //VIP
		    var nVip = {
		    	name: 'nVip',
		        url: '#nVip',
		        type:13,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:2,
		        	name:"VIP专享会员"
		        },
		        go: function(){
		        	console.log("VIP第1层 go");
		        	newsObj.goToVip(this.data);
		        },
		        back:function(){
		        	console.log("VIP第1层 back");
		        	newsObj.backToNewMsg(this.data);
		        }
		    };

		    //VIP专享会员
		    var nVipPrivilege = {
		    	name: 'nVipPrivilege',
		        url: '#nVipPrivilege',
		        type:13,
		        floor:3,
		        events: {  
		        },
		        data:{
		        	mId:100,
		        	vId:100,
		        	name:"VIP专享会员"
		        },
		        go: function(){
		        	console.log("vip会员特权 go");
		        	personalObj.goToVipPrivilege(this.data);
		        },
		        back:function(){
		        	console.log("vip会员特权 back");
		        	personalObj.backToVip(this.data,1);
		        }
		    };

		    //指纹识别设置
		    var mSoterPage1 = {
		    	name: 'mSoterPage1',
		        url: '#mSoterPage1',
		        type:14,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	mId:6,
		        	pageNum:1,
		        	name:"指纹识别设置"
		        },
		        go: function(){
		        	console.log("指纹识别设置 go");
		        	personalObj.goToSoterPage(this.data);
		        },
		        back:function(){
		        	console.log("指纹识别设置 back");
		        	personalObj.backToIndexForVs1();
		        }
		    };

		    //指纹识别设置
		    var mSoterPage2 = {
		    	name: 'mSoterPage2',
		        url: '#mSoterPage2',
		        type:14,
		        floor:2,
		        events: {  
		        },
		        data:{
		        	mId:6,
		        	pageNum:2,
		        	name:"指纹识别设置"
		        },
		        go: function(){
		        	console.log("指纹识别设置2 go");
		        	personalObj.goToSoterPage2(this.data);
		        },
		        back:function(){
		        	console.log("指纹识别设置2 back");
		        	personalObj.backToSoterPage();
		        }
		    };

		    //15

			this.pageManager
			.push(home)
			.push(nMsgInfo)
			.push(mProduct)
			.push(mQuestionDetail)
			.push(mRole)
			.push(mProductDemo)
			.push(mUseHelper)
			.push(mVip)
			.push(mVipPrivilege)
			.push(mAdminService)
			.push(mEditInfo)
			.push(mEditGender)
			.push(mEditPhone)
			.push(mEditEmail)
			.push(pDealList)
			.push(pMineList)
			.push(pNotifyList)
			.push(pRHList) //润衡
			.push(pRHTypeList) //润衡分类列表
			.push(pAllProcess) //全部应用
			.push(pMyChart) //我的报表
			.push(pMyTpl) //我的模板
			.push(nVip) //vip
			.push(nVipPrivilege) //VIP专享会员
			.push(mSoterPage1) //指纹识别设置
			.push(mSoterPage2) //指纹识别设置2
			.default('home')
			.init();
		},
		//交互性操作
		mGetServiceHtml:function(sId){
			return this.personalObj.getServiceHtml(sId);
		},
		mGetQuestionsHtmlToNews:function(){
			return this.personalObj.getQuestionsHtmlToNews();
		},
		mGoToQuestionDetail:function(data){
			this.personalObj.goToQuestionDetail(data);
		},
		mGoToVip:function(data){
			this.personalObj.goToVip(data,1);
		},
		setSaveKey:function(key,val){
			try{
				//window.sessionStorage[key] = val;
				window.sessionStorage.setItem(key,val);
			}catch(e){
				console.log(e);
			}
		},
		getSaveKey:function(key){
			try{
				return window.sessionStorage.getItem(key);
			}catch(e){
				console.log(e);
			}
		},
		removeSaveKey:function(key){
			console.log(1);
			try{
				if(key){
					window.sessionStorage.removeItem(key);
				}else{
					window.sessionStorage.clear();
				}
			}catch(e){
				console.log(e);
			}
		},
		getDealNumber:function(){
			processObj.geDealNumber();
		},
		changeUrl:function(){
			var state = {
				title:"流程专家",
				url:"/wx/index.php?debug=1&app=index&a=index"
			}
			window.history.pushState(state, state.title, state.url);
		},
		showSeachObj:function(){
			this.searchObj.showSeachObj();
		}
	};

	mediator.init({
		container:$("#main-container"),
		newsObj:newsObj,
		searchObj:searchObj,
		processObj:processObj,
		personalObj:personalObj,
		pageManager:pageManager
	});

	layerPackage.unlock_screen();
});