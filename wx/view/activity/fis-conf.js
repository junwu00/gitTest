require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'activity');
fis.match('*', {
	release:'/activity/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
}); 