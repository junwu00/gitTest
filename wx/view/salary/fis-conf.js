require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'salary');
fis.match('*', {
	release:'/salary/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});