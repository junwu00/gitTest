require([
  'common:widget/lib/UiFramework',
  'common:widget/lib/tools',
  'common:widget/lib/GenericFramework',
],function(UiFramework,t,GenericFramework) {
  var layerPackage = UiFramework.layerPackage();
  var container = $('#main-container'),pageNum = 2;
  $(document).ready(function (){
     _get_data(1,function(res){
      _init_html(res.info);
      if(res.info.count > 20){
        _init_scroll();
      }
     });
     $('#bottom-menu').css('display','none');
  });
  function _get_data(page,cb){
    layerPackage.lock_screen()
    //container.html('')
    $.post('/wx/index.php?model=salary&m=ajax&cmd=101', {"data":{"page":page,"page_size":20}}, function(res) {
      try {
        var resp = JSON.parse(res);
        if (resp.errcode == 0) {
          cb(resp);
        }else{
          layerPackage.fail_screen(resp.errmsg)
        }
        layerPackage.unlock_screen()
      } catch(e) {
        console.log(e)
      }
    })
  }

  function _init_html (data) {
    _init_title(data)
    _init_list(data)
    _init_click()
  }

  function _init_title (data) {
    var name = data.name
    var dept = data.dept
    var html_name = '姓名: '+name;
    var html = '<div id="title">'
        html += '<div class="name">'+html_name+'</div>'
        html += '<div class="dept">部门: '+dept+'</div>'
        html += '</div>'
    if($('#title').length === 0){
      container.before(html)
    }
  }

  function _init_list (dataTemp) {
    var data = dataTemp.data;
    var html = '<div class="content">'; 
    if(data.length == 0){
      html += '<div class="empty"></div><p class="tac fs18 mt15 c-gray">目前没有您的工资条信息！</p>';
      $('#container').css('background','#FFF');
      $('#screen').css('background','#FFF');
    }else{
      for (var i in data) {
          if($('.banner').text().indexOf(i) === -1){
            html += '<div class="banner">您在'+ i +'收到的工资</div>';
          }
        
        for (var j in data[i]) {
          html += '<div class="item" data-id="'+data[i][j]['id']+'" data-year="'+data[i][j]['year']+'" data-month="'+data[i][j]['month']+'" data-person_number="'+data[i][j]['person_number']+'">'
          html += '<div class="month">' + j +'</div>'
          html += '<div class="date">' + data[i][j]['value'] +'</div>'
          html += '</div>'
        }
      }
    }
    html += '</div>';
    container.append(html);
  }

  function _init_click () {
    $('.content .item').click(function(){
      layerPackage.lock_screen()
      window.location.href = "./wx/index.php?model=salary&m=index&a=detail&id=" + $(this).attr('data-id')+"&year=" + $(this).attr('data-year') + "&month=" +  $(this).attr('data-month') + "&person_number=" + $(this).attr('data-person_number')
    })
  }
  //hdh add 2017/10/13
  function _init_scroll(){
    var listObj = $('#main-container');
    layerPackage.lock_screen();
      pageCount = 1;
      UiFramework.scrollLoadingPackage().init({
        target:listObj,
        scroll_target:listObj,
        fun:function(self){
          //if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}
          if(pageNum != 1){
            self.add_scroll_waiting($("#powerby"),1);
          }
          _get_data(pageNum,function(res){
            _init_html(res.info);
            var data = res.info.data;
            var len = [];
              for(var i = 0 in data){
                  var key = Object.keys(data[i]);
                  for(var j = 0 in key){
                    len.push(key[j]);
                  }
              }
              pageCount += len.length;
              pageNum++;
              listObj.attr("is_scroll_loading",false);
              if(len.length < 20){
                listObj.attr("is_scroll_end",true);
                //$('#scroll_loading').remove();
              }
              self.remove_scroll_waiting(listObj);
              layerPackage.unlock_screen()
          });
          }
      });
    //container.scroll();
  }
},function(e){
  requireErrBack(e);
});