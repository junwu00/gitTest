require([
  'common:widget/lib/UiFramework',
  'common:widget/lib/tools',
  'common:widget/lib/GenericFramework',
],function(UiFramework,t,GenericFramework) {
  var layerPackage = UiFramework.layerPackage();
  var container = $('#main-container')
  var id = t.getUrlParam('id'),year = t.getUrlParam('year'),month = t.getUrlParam('month'),person_number = t.getUrlParam('person_number');

  var dict_1 = {
    // "total_deduct": "应扣合计",
    // "actual_total": "实发合计",
    // "print_date": "打印日期",
    "job_wage": "职务工资",
    "wage_level": "级别工资",
    "base_pay": "基础工资",
    "seniority_wage": "工龄工资",
    "technical_grade_wage": "技术等级工资",
    "probationary_wages": "见习工资",
    "living_wage": "活工资",
    "wage_pay": "薪级工资",
    "margin_difference": "套转差额",
    "special_allowance": "特区津贴",
    "retention_allowance": "保留津贴",
    "post_allowance": "岗位津贴",
    "other_additions": "其他增补",
    "working_subsidy": "工作性补贴(奖金)",
    "grassroots_subsidies": "基层补贴",
    "incentive_performance_allowance": "奖励性绩效津贴",
    "temporary_subsidy": "临时补贴",
    // mrc add 20190218
    "normative_subsidy": "规范性补贴",
    "career_post_allowance": "事业岗位津贴",
    "living_allowances": "生活补贴",
    // mrc add 20190218
    "reform_subsidy": "改革性补贴(房补)",
    "living_subsidies": "生活性补贴(物补)",
    "temporary_post_allowance": "临时岗位津贴",
    "special_post_allowance": "特殊岗位津贴",
    "pay_only_child": "独生子女费",
    "national_pension_increase": "离退休费全国增资",
    "retirement_fee_increase": "离退休费特区增资",
    "annual_assessment_bonus": "年度考核奖金",
    "other": "其他",
    "pre_tax_income": "税前收入",
    "after_tax_income": "税后收入",
    "exemption": "免税额 "
  }

  var dict_2 = {
    "income_tax": "所得税",
    "endowment_insurance": "养老保险(个人)",
    "medical_insurance": "医疗保险(个人)",
    "annuity": "年金(个人)",
    "pay": "代缴",
    "accumulation_fund": "公积金(个人)",
    "other_deduction": "其他应扣"
  }

  var dict_1_new = {
    "job_wage": "职务工资",
    "wage_level": "级别工资",
    "incentive_performance_allowance": "绩效工资",
    "post_wage": "岗位工资",
    "wage_pay": "事业薪级工资",
    "base_pay": "基础工资",
    "seniority_wage": "工龄工资",
    "living_wage": "奖金（活工资）",
    "probationary_wages": "见习工资",
    "other_fees": "其他费用",
    "living_allowances": "生活补贴",
    "comprehensive_benefits": "综合补助",
    "retention_allowance": "保留补助",
    "life_allowance": "物补/生活津贴",
    "retirement_subsidy": "离退休补贴费",
    "retirement_fee_increase": "离退休费特区增资",
    "national_pension_increase": "离退休费全国增资",
    "reform_subsidy": "改革性补贴",
    "reform_allowance": "改革性津贴",
    "technical_grade_wage": "技术等级工资",
    "staff_scale_salary": "薪级制人员薪级工资",
    "margin_difference": "薪级制人员套转差额",
    "festival_bonus": "过节费",
    "living_subsidies": "生活性补贴",
    "special_post_allowance": "特岗津贴",
    "temporary_post_allowance": "临岗津贴",
    "normative_subsidy": "规范津补贴",
    "special_allowance": "特区津贴",
    "post_allowance": "岗位津贴",
    "career_post_allowance": "事业岗位津贴",
    "grassroots_subsidies": "基础津贴",
    "pay_only_child": "独生子女费",
    "other_additions": "其他增补",
    "other_income_1": "其他收入1",
    "other_income_2": "其他收入2",
    "other_income_3": "其他收入3",
    "other_income_4": "其他收入4",
    "other_more_less": "其他增减"
  }

  var dict_2_new = {
    "accumulation_fund": "个人缴纳公积金",
    "endowment_insurance": "个人缴纳养老保险",
    "medical_insurance": "个人缴纳医疗保险",
    "unemployment_insurance": "个人缴纳失业保险",
    "occupational_pension": "个人缴纳职业年金",
    "whole_medical": "个人统筹医疗",
    "supplement_accumulation_fund": "个人补缴公积金",
    "supplement_endowment_insurance": "个人补缴养老保险",
    "supplement_medical_insurance": "个人补缴医疗保险",
    "supplement_unemployment_insurance": "个人补缴失业保险",
    "supplement_occupational_pension": "个人补缴职业年金",
    "income_tax": "个人所得税",
    "other_deduction": "其他代扣项"
  }

  var dict_3_new = {
    "unit_accumulation_fund": "单位缴纳公积金",
    "unit_occupational_pension": "单位缴纳职业年金",
    "unit_endowment_insurance": "单位缴纳养老保险",
    "unit_medical_insurance": "单位缴纳医疗保险",
    "unit_unemployment_insurance": "单位缴纳失业保险",
    "unit_injury_insurance": "单位缴纳工伤保险",
    "unit_supplement_accumulation_fund": "单位补缴公积金",
    "unit_supplement_occupational_pension": "单位补缴职业年金",
    "unit_supplement_endowment_insurance": "单位补缴养老保险",
    "unit_supplement_medical_insurance": "单位补缴医疗保险",
    "unit_supplement_unemployment_insurance": "单位补缴失业保险",
    "unit_supplement_injury_insurance": "单位补缴工伤保险"
  }

  var dict_4_new = {
    "housing_subsidies": "房改住房补贴"
  }

  if (!id) {
    //layerPackage.fail_screen('不合法的id')
    return false
  }

  _get_data()

  function _get_data (){
    layerPackage.lock_screen()
    container.html('')
    $.post('/wx/index.php?model=salary&m=ajax&cmd=102', {
      data : {
        salary_id: id,
        year:year,
        month:month,
        person_number:person_number
      }
    }, function(res) {
      try {
        var resp = JSON.parse(res)
        if (resp.errcode == 0) {
          _init_html(resp.info)
        } else{
          layerPackage.fail_screen(resp.errmsg)
        }
        layerPackage.unlock_screen()
      } catch(e) {
        console.log(e)
      }
    })
  }

  function _init_html(data) {
    if (!data.type || data.type === 'person_number') {
      _init_title(data);
      _init_info(data);
      _init_content(data);
    }
    // zhp add 新版工资条
    if (data.type === 'id_card') {
      _init_title_new(data);
      _init_info_new(data);
      _init_content_new(data);
    }
  }

  function _init_title (data) {
    var name = data.name
    var dept = data.dept

    var html = '<div id="title">'
        html += '<div class="name">姓名: '+name+'</div>'
        html += '<div class="dept">部门: '+dept+'</div>'
        html += '</div>'
    container.append(html)
  }

  function _init_title_new (data) {
    var name = data.name
    var dept = data.dept

    var html = '<div id="title">'
        html += '<div class="name">姓名: '+name+'</div>'
        html += '<div class="dept">部门: '+dept+'</div>'
        html += '</div>'
    container.append(html)
  }

  function _init_info (data) {
    var html = '<div id="info">'
        html += '<div class="person_number">人员编号: '+data.person_number+'</div>'
        html += '<div class="when">'+data.year+'年'+data.month+'月'+'</div>'
        html += '</div>'
    container.append(html)
  }

  function _init_info_new (data) {
    var html = '<div id="info">'
        html += '<div class="person_number">身份证号: '+data.id_card+'</div>'
        html += '<div class="when">'+data.year+'年'+data.month+'月'+'</div>'
        html += '</div>'
    container.append(html)
  }

  function _init_content (data) {
    var html = '<div id="content">'

        html += '<div class="table">'
          html += '<div class="title">合计</div>'
          html += '<div class="item "><span class="left">应发合计</span><span class="right">'+data.total_payable+'</span></div>'
          html += '<div class="item "><span class="left">应扣合计</span><span class="right">'+data.total_deduct+'</span></div>'
          html += '<div class="item "><span class="left">实发合计</span><span class="right red">'+data.actual_total+'</span></div>'
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">工资标准</div>'
        for(var key in dict_1){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_1[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">应扣部分</div>'
        for(var key in dict_2){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_2[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'


        html += '</div>'
    container.append(html)
  }

  function _init_content_new (data) {
    var html = '<div id="content">'

        html += '<div class="table">'
          html += '<div class="title">合计</div>'
          html += '<div class="item "><span class="left">应发工资</span><span class="right">'+data.total_payable+'</span></div>'
          html += '<div class="item "><span class="left">个人扣缴合计</span><span class="right">'+data.total_deduct+'</span></div>'
          html += '<div class="item "><span class="left">实发工资</span><span class="right red">'+data.actual_total+'</span></div>'
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">工资标准</div>'
        for(var key in dict_1_new){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_1_new[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">个人扣缴部分</div>'
        for(var key in dict_2_new){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_2_new[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">单位缴纳部分</div>'
        for(var key in dict_3_new){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_3_new[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'

        html += '<div class="table">'
        html += '<div class="title">其他</div>'
        for(var key in dict_4_new){
          if(!_is_equal_0(data[key])){
            html += '<div class="item "><span class="left">'+dict_4_new[key]+'</span><span class="right">'+data[key]+'</span></div>'
          }
        }
        html += '</div>'


        html += '</div>'
    container.append(html)
  }

  function _is_equal_0 (string){
    if (parseFloat(string) === 0) {
      return true
    } else {
      return false
    }
  }
},function(e){
  requireErrBack(e);
});