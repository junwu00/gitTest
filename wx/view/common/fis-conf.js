require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'common');
fis.match('*', {
	release:'/common/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});