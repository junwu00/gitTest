<{html framework="common:widget/lib/require.js"}>
	<{head}>
		<meta charset='utf-8'>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="format-detection" content="telephone=no">
    	<title><{$title}></title>
    	<{require name="common:widget/ui/easyworkBase.css"}>
   		<{block name="block_head_static"}><{/block}>
 	<{/head}>

 	<{body id="screen"}>
 		<{widget name="common:widget/header/header.tpl"}>
    	<div id="container">
      		<div id='main' class="main">
       			<{block name="main"}><{/block}>
      		</div>
    	</div>
		<{widget name="common:widget/staticConfig/requireConfig.tpl"}>
		<{widget name="common:widget/footer/footer.tpl"}>
    	<{widget name="common:widget/menu/menu.tpl"}>
 	<{/body}>
   
	<{block name="block_foot_static"}><{/block}>
<{/html}>