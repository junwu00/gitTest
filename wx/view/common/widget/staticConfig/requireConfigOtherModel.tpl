<script type="text/javascript">	
	//同一处理整合之前其他模块的依赖，后期其他的模块的依赖在使用再进行添加，不在此文件添加
	var requireOtherModelConfig = {
	    shim:{
			"process:static/Scripts/mv_pic/GooFunc":{
				deps:['jquery']
			},
			"process:static/Scripts/mv_pic/json2":{
				deps:['process:static/Scripts/mv_pic/GooFunc']
			},
			"process:static/Scripts/mv_pic/GooFlow":{
				deps:['process:static/Scripts/mv_pic/json2']
			},
			"process:static/Scripts/mv_pic/GooFunc":["css!process:static/Contents/mv_pic/default.css"],
			"process:static/Scripts/mv_pic/GooFlow":["css!process:static/Contents/mv_pic/GooFlow.css"],

			"index:static/Scripts/lib/idangerous.swiper.min":{
				deps:['jquery']
			},
			"index:static/Scripts/lib/idangerous.swiper.min":["css!index:static/Scripts/lib/idangerous.swiper.css"],

			"index:static/Scripts/lib/swiper.jquery.min":{
				deps:['jquery']
			},
			"index:static/Scripts/lib/swiper.jquery.min":["css!index:static/Scripts/lib/swiper.min.css"],

			"index:static/Scripts/lib/idangerous.swiper.scrollbar-2.4.1.min":{
				deps:['index:static/Scripts/lib/idangerous.swiper.min']
			},
			"index:static/Scripts/lib/idangerous.swiper.scrollbar-2.4.1.min":["css!index:static/Scripts/lib/idangerous.swiper.scrollbar-2.4.1.css"],
	        "workshift:static/Script/datetimepicker/bootstrap.min":{
				deps:['jquery']
			},
			"workshift:static/Script/datetimepicker/bootstrap-datetimepicker.min":{
				deps:['workshift:static/Script/datetimepicker/bootstrap.min']
			},
			"workshift:static/Script/datetimepicker/bootstrap-datetimepicker.zh-CN":{
				deps:['workshift:static/Script/datetimepicker/bootstrap-datetimepicker.min']
			},
			"process:static/Scripts/otherProcess":['css!process:static/Contents/otherProcess.css'],
			"process:static/Scripts/mv_pic_new/GooFunc":{
				deps:['jquery']
			},
			"process:static/Scripts/mv_pic_new/json2":{
				deps:['process:static/Scripts/mv_pic_new/GooFunc']
			},
			"process:static/Scripts/mv_pic_new/GooFlow":{
				deps:['process:static/Scripts/mv_pic_new/json2']
			},
			"process:static/Scripts/mv_pic_new/GooFunc":["css!process:static/Contents/mv_pic_new/default.css"],
			"process:static/Scripts/mv_pic_new/GooFlow":["css!process:static/Contents/mv_pic_new/GooFlow.css"],
	    }
	};
</script>