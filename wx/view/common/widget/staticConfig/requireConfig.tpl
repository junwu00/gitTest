<script type="text/javascript">
	var requirejs_path_conf = {
	    <{foreach from=$static_res key=k item=res}>
	    	'<{$k}>' : '<{$res}>',
	    <{/foreach}>
	};
	
	//判断不同平台设备
	var deviceKey = 0;
	
	//加载地图API配置
	var u = navigator.userAgent,script = "",plarforms  = 0;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
	    script = "http://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad";
	} else if (u.indexOf('iPhone') > -1) {//苹果手机
		deviceKey = 1;
	    script = "https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad";
	} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
	    script = "https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad";
	} else {
	    plarforms  = 1;
	    script = "https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad";
	};
	requirejs_path_conf.amap = script + "&callback=init";
	//console.log(requirejs_path_conf.amap);
	//加载地图API配置
	
	var require = {
		baseUrl: '/',
		paths: requirejs_path_conf,
		map: {
	        '*': {
	            'css': 'common:widget/lib/require_css',
	        }
	    },
	    shim:{
	    	//"jquery":['css!common:widget/ui/common.css'],
	    	//"common:widget/ui/common":['css!common:widget/ui/base.css','css!common:widget/ui/font-awesome/css/font-awesome.min.css'],
	        "common:widget/lib/plugins/juicer":{
	　　　　    	exports: 'juicer'
	　　    	},
	        "common:widget/lib/plugins/uploadifive/uploadifive":{
	         	deps:['jquery'],
	　　　　    	exports: 'jQuery.fn.uploadifive'
	　　    	},
	        "common:widget/lib/plugins/uploadifive/uploadifive": ['css!common:widget/lib/plugins/uploadifive/uploadifive_c.css'],
	        "common:widget/lib/plugins/jweixin-1.1.0":{
	　　　　    	exports: 'jweixin'
	　　    	},
			"common:widget/lib/UiFramework": ["css!common:widget/ui/UIFramework.css"],
			"common:widget/lib/GenericFramework": ["css!common:widget/ui/GenericFramework.css"],
			'common:widget/lib/plugins/gdmap/lbs.amap': ['css!common:widget/lib/plugins/gdmap/lbs.amap_c.css'],
			'common:widget/lib/plugins/depts_users_select/js/depts_users_select': ['css!common:widget/lib/plugins/depts_users_select/css/depts_users_select.css'],

			"common:widget/lib/plugins/process_form_analyse/js/process_form_analyse" : ["css!common:widget/lib/plugins/process_form_analyse/css/process_form_analyse.css"],

	        "common:widget/lib/scFormTable/scFormTable": ["common:widget/lib/plugins/iscroll/build/iscroll-probe","css!common:widget/lib/scFormTable/scFormTable.css"],
	        "common:widget/lib/scFormTable/scFormTable-v2.0": ["common:widget/lib/plugins/iscroll/build/iscroll-probe","css!common:widget/lib/scFormTable/scFormTable.css"],
	        "common:widget/lib/scFormTable/scFormTable-v3.0": ["common:widget/lib/plugins/iscroll/build/iscroll-probe","css!common:widget/lib/scFormTable/scFormTable-v3.0_css.css"],
	        /*报表*/
	        "common:widget/lib/plugins/swiper/swiper-3.4.1.jquery.min":["css!common:widget/lib/plugins/swiper/swiper-3.4.1.min"],
	        "common:widget/serviceWidget/chartTable/chartTable":["css!common:widget/serviceWidget/chartTable/chartTable_css.css"]
	        /*报表*/
	    },
	    <{if $VIEW_PUBLISH == 'view'}>urlArgs:'ver=<{$VERSION_VER}>'<{/if}>
	};
	
	//处理其他模块的关联依赖，合并
	try {
		if(requireOtherModelConfig != undefined){
			 for (var i in requireOtherModelConfig.shim){
			 	require.shim[i] = requireOtherModelConfig.shim[i];
		    }
		}
	} catch (e) {}
	
  	//错误处理
  	var reload = null;
  	function requireErrBack(err){
  		try{
			document.body.innerHTML = "";
			var divNode = document.createElement("div");
			divNode.setAttribute("id","errorClick");
			divNode.className = "c-gray bg-gray errorClick";

			var iNode = document.createElement("i");
			iNode.style.display = "block";
			iNode.className = "iconRefresh ma0 mb15";
			divNode.appendChild(iNode);

			var spanNode = document.createElement("span");
			spanNode.innerHTML = "网络出错，轻触屏幕重新加载";
			divNode.appendChild(spanNode);

		    document.body.appendChild(divNode);

		    document.getElementById("errorClick").addEventListener("click",function(){
		    	window.location.reload();
		    });
		    /*
			//含有类别信息的字串值，如“timeout”，“nodefine”， “scripterror”
			var type = err.requireType;
			if(type == "timeout"){
				if(!reload){
					alert("请求超时,3秒后自动刷新!");
					reload = setTimeout(function(){
						clearTimeout(reload);
						reload = null;
						window.location.reload();
					},3000);
				}
			}
			else if (type == "nodefine"){
				alert("未能找到相应的文件!");
			}
			else if(type == "scripterror"){
				alert("加载的脚本含有错误!");
			}else{
				alert(type);
			}
			*/

			//requireModules: 超时的模块名/URL数组。
			console.log(err.requireModules);
  		}catch(e){
  			console.log(e);
  		}
  	}
	
</script>


<script type="text/javascript">

	var http_domain = "<{$HTTP_DOMAIN}>";
  	var cdn_domain = "<{$CDN_DOMAIN}>";
  	var media_domain = "<{$MEDIA_DOMAIN}>";
  	var media_domain_download = '';
  	var powerby_val = "<{$POWERBY}>";
  	var ver="<{$VERSION_VER}>";
  	var userInfo = {
  		userId:"<{$USER_ID}>",
  		userName:"<{$USER_NAME}>",
  		pic:"<{$USER_PIC}>",
  		deptId:"<{$USER_DEPT_ID}>",
  		deptName:"<{$USER_DEPT_NAME}>",
  		isAdmin:"<{$IS_ADMIN}>",
  		isVip:"<{$IS_VIP}>",
  		vipState:"<{$VIP_STATE}>",
  		vipPastTime:"<{$PAST_TIME}>",
  		vipPastDate:"<{$PAST_DATE}>",
  		fpState:"<{$FP_STATE}>",
  		hasFp:"<{$HAS_FP}>"
  	};
  	
  	var wxJsdkConfig={
		debug: false,
      	appId: '<{if isset($app_id)}><{$app_id}><{/if}>',
      	timestamp: '<{if isset($time)}><{$time}><{/if}>',
      	nonceStr: '<{if isset($nonce_str)}><{$nonce_str}><{/if}>',
      	signature: '<{if isset($sign)}><{$sign}><{/if}>'
  	};

	//编译之后的前段根目录
  	var buildView = '<{$VIEW_PUBLISH}>';
  	
  	//默认人头像图片
  	var defaultFace = "<{uri name='common:static/images/face.png'}>";
  	
  	//人员选择器相关图片配置
  	var deptsUsersSelectIconDept = "<{uri name='common:widget/lib/plugins/depts_users_select/image/icon-dept.png'}>";
  	//人员选择器相关图片配置
  	
  	//gdmap相关图片配置
  	var gdmapMarkIcon = "<{uri name='common:widget/lib/plugins/gdmap/mark_icon.png'}>";
  	var gdmapMarkIcon2 = "<{uri name='common:widget/lib/plugins/gdmap/location_icon.svg'}>";
  	var gdmapMarkIcon3 = "<{uri name='common:widget/lib/plugins/gdmap/locationred_icon.svg'}>";
  	//gdmap相关图片配置

  	//长轮询链接
  	var getConsumeUrl = "<{$CONSUME_URL }>";

  	//平台类型
  	var platid = "<{$platid}>";
</script>