define([
        'jquery',
        'common:widget/lib/tools',
        'common:widget/lib/UiFramework',
        'common:widget/lib/plugins/juicer',
        'common:widget/lib/text!'+buildView+'/common/widget/lib/plugins/depts_users_select/tmp/tmp.html',
        'common:widget/lib/text!'+buildView+'/common/widget/lib/plugins/depts_users_select/tmp/deptListTmp.html',
        'common:widget/lib/text!'+buildView+'/common/widget/lib/plugins/depts_users_select/tmp/deptChildListTmp.html',
        'common:widget/lib/text!'+buildView+'/common/widget/lib/plugins/depts_users_select/tmp/userListTmp.html',
        'common:widget/lib/text!'+buildView+'/common/widget/lib/plugins/depts_users_select/tmp/userDetailTmp.html'
        ], function($,tools,UiFramework,tmpTool,tmp,deptListTmp,deptChildListTmp,userListtmp,userDetailTml) {
	
	function depts_users_select(){
		this.comStTml={//映射静态资源，方便后期修改 
				fileIcon : deptsUsersSelectIconDept,
				userDefaultFace : defaultFace,
				};
		this.target = null;
		this.type = 0;// 可选类型：0表示部门人员可选 （默认），1表示部门可选，2表示人员可选
		this.max_select = 0;// 可选个数：0表示无限制（默认），1表示最多选一个，2表示最多选2个，以此类推
		this.base_dept_url = '';//查找最顶级的部门url
		this.dept_url = '';//查找自部门部门url
		this.user_url = '';// 加载部门对应的人员url
		this.user_detail = '';//人员详情url
		this.init_selected_url = '';// 根据Id数组精确查找部门以及人员的url
		this.targetClick = 1;//判断target是否可点击，1为可点击，0为不可点击
		this.hashChageCallBack = null;//回退件外部回调函数；
		this.clickHashChage=null,
		this.ret_fun=null;
		this.init_call_back = null;	
		this.layerPackage = null;
	}
	
	depts_users_select.prototype.init= function(data){
		this.target = data.target;
		this.type = data.type;
		this.max_select = data.max_select;
		this.base_dept_url = data.base_dept_url;
		this.dept_url = data.dept_url;
		this.user_url = data.user_url;
		this.user_detail = data.user_detail;
		this.init_selected_url = data.init_selected_url;
		this.init_call_back = data.init_call_back;
		this.hashChageCallBack = data.hashChageCallBack;
		this.ret_fun= data.ret_fun;
		this.layerPackage = UiFramework.layerPackage();
		this.initSelectData(data);
		if(data.canEdit != undefined && data.canEdit.toString() == "0"){
			this.targetClick = 0;
		}

		this.initHash = window.location.hash;

		return this;
	}

	depts_users_select.prototype.initSelectData=function(data){	
		var that = this;
		that.target.addClass('deptUserTarget');
		var selectData = data.select;
		that.layerPackage.lock_screen();
		if((selectData.hasOwnProperty('user') || selectData.hasOwnProperty('dept'))){
			$.ajax({
				url:that.init_selected_url+'&time='+(new Date()).getTime(),
				type:'post',
				data_type:'json',
				data:{ajax_act:'load',data:selectData},
				success:function(res){
					that.layerPackage.unlock_screen();
					try {
						res = $.parseJSON(res);
					} catch (e) {
						console.log(e);
						console.log('返回数据转json出错');
						that.layerPackage.ew_alert({'desc':'加载数据异常'});
					}
					if(res.errcode != 0){
						that.layerPackage.ew_alert({desc:'初始化人员选择器数据失败'});
						return;
					}
					that.target.html('');
					var html = that.getSelecHtml(res.info);
					that.target.append(html);
					that.target.attr('data-init','true');
					if (typeof(that.init_call_back) === 'function') {
						that.init_call_back(that);
					}
					that.initBindDom(data);
					that.setTargetClick();
					that.initHashChange();
				},
				error:function(){
					console.log('系统繁忙:初始化人员选择器数据失败');
				}
			});
		}else{
			that.initBindDom(data);
			that.setTargetClick();
			that.initHashChange();
		}
	}
	
	
	depts_users_select.prototype.initBindDom = function(data){
		var that = this;
		that.target.off().on('click',function(e){
			that.clickHashChage = true;
			$('body').find('#depts_users_select').remove();
			var tmpData = {};
			tmpData.select = that.getSelectData();
			tmpData.fileIcon = that.comStTml.fileIcon;
			tmpData.type = data.type;
			var $dom = $(tmpTool(tmp,tmpData));
			window.location.hash='1';
			$('body').append($dom);
			that.layerPackage.lock_screen();
			$.ajax({
				url:that.base_dept_url+'&time='+(new Date()).getTime(),
				type:'post',
				data_type:'json',
				success:function(res){
					try {
						res = $.parseJSON(res);
					} catch (e) {
						console.log(e);
						console.log('返回数据转json出错');
						that.layerPackage.ew_alert({'desc':'加载数据异常'});
					}
					if(res.errcode != 0){
						that.layerPackage.fail_screen(res.errmsg);
						return;
					}
					var deptData = {};
					var arr = [res.data.dept_list[0],res.data.dept_list[0]];						
					deptData.dept_list = res.data.dept_list;
					deptData.setting = data;
					var $deptDom = $(tmpTool(deptListTmp,deptData));
					that.bindLiDom($deptDom,data)
					that.checkList($deptDom,'true');
					that.inputSelect($deptDom,'true');
					$('#depts_users_select').find('.treeBody .depttree').remove();
					$('#depts_users_select').find('.treeBody').append($deptDom);
					that.bindBottomBar(data);
					that.bindSearch(data);
					that.bindMydept(data,res);

					if (res.data.dept_list.length == 0 && res.data.group_list.length == 0 && res.data.user_dept.length == 0) {
						$('#depts_users_select').append('<div class="no_pri">您没有权限查看通讯录!</div')
						$('#depts_users_select .myDept').hide()
					}
					that.layerPackage.unlock_screen();
				},
				error:function(){
					console.log('系统繁忙:查找顶级部门失败');
				}
			});
		})
	}
	
	depts_users_select.prototype.bindMydept = function(data,res){
		var that = this;
		$('#depts_users_select .myDept').click(function(){
			var dept = res.data.user_dept.name;
			var deptId = res.data.user_dept.id
			that.createUserList(deptId,dept);
			that.loadUserListData(data);
		});
	}
	
	depts_users_select.prototype.bindLiDom = function($dom,data){
		var that = this;
		$($dom).find("span.openDept").on("click",function(event){
			event.stopPropagation();
			var li = $(this).parent().parent();
			if(li.attr('data-haschild')=="true"){
				if(li.find('ul').length > 0){
					if(li.attr('class').indexOf('open') > -1){
						li.removeClass('open');
						li.find('ul').hide('fast');
					}else{
						li.addClass('open');
						li.find('ul').show('fast');
					}
				}else{
					//加载
					var d_id = li.attr('data-id');
					that.layerPackage.lock_screen();
					$.ajax({
						url:that.dept_url+'&time='+(new Date()).getTime(),
						type:'post',
						data_type:'json',
						data:{data:{d_id:d_id}},
						success:function(res){
							that.layerPackage.unlock_screen();
							try {
								res = $.parseJSON(res);
							} catch (e) {
								console.log(e);
								console.log('返回数据转json出错');
								that.layerPackage.ew_alert({'desc':'加载数据异常'});
							}
							if(res.errcode != 0){
								that.layerPackage.fail_screen(res.errmsg);
								return;
							}
							var deptData = {};
							deptData.dept_list = res.info;
							deptData.setting = data;
							var $deptDom = $(tmpTool(deptChildListTmp,deptData));
							that.bindLiDom($deptDom,data);
							that.checkList($deptDom,'true');
							that.inputSelect($deptDom,'true');
							li.append($deptDom);
							li.addClass('open');
						},
						error:function(){
							console.log('系统繁忙:查找部门失败');
						}
					});
				}
			}
		});
		
		$($dom).find("span.intoDept").on("click",function(event){
			event.stopPropagation()
			if(that.type == 1){
				return;
			}
			var li = $(this).parent().parent();
			that.createUserList(li.attr('data-id'),li.attr('data-name'));
			that.loadUserListData(data);
		});
		
		$($dom).find("input").on("click",function(event){
			event.stopPropagation()
		});
	}
	
	depts_users_select.prototype.loadUserListData = function(data){
		var that = this;
		var $userListtDom = $('#depts_users_select').find('.treeBody').find('.userTree');
		var opt = {};
		opt.name = $('#c-search-title').val();
		opt.dept = $userListtDom.attr('data-deptid');
		opt.offset = Number($userListtDom.attr('data-limit')) * Number($userListtDom.attr('data-index'));
		opt.limit = $userListtDom.attr('data-limit');
		$userListtDom.attr('is_scroll_loading','true');
		that.layerPackage.lock_screen();
		$.ajax({
			url:that.user_url+'&time='+(new Date()).getTime(),
			type:'post',
			data_type:'json',
			data:{data:opt},
			success:function(res){
				that.layerPackage.unlock_screen();
				try {
					res = $.parseJSON(res);
				} catch (e) {
					console.log(e);
					console.log('返回数据转json出错');
					that.layerPackage.ew_alert({'desc':'加载数据异常'});
				}
				if(res.errcode != 0){
					that.layerPackage.fail_screen(res.errmsg);
					return;
				}
				$userListtDom.attr('is_scroll_loading','false');
				$userListtDom.attr('data-index',Number($userListtDom.attr('data-index'))+1)
				if(res.errcode != 0){
					that.layerPackage.ew_alert({desc:res.errmsg});
					return;
				}
				if(res.info.data < 100){
					$userListtDom.attr('is_scroll_end','true');
				}
				$userListtDom.attr('data-offset',opt.limit);
				var html = '';
				var treeTab= '';
				var treeTabs = $userListtDom.find('p.treeTab');
				if(res.info.data.length > 0){
					if(treeTabs.length>0){
						treeTab=$(treeTabs[treeTabs.length - 1]).html().toUpperCase();
					}else{
						if(res.info.data[0].first_py == ''){
							return;
						}
						treeTab = res.info.data[0].first_py.split('')[0].toUpperCase();
					}
					for (var i = 0; i < res.info.data.length; i++) {
						var user = res.info.data[i];
						var treeTabTmp = user.first_py.split('')[0].toUpperCase();
						if(treeTab != treeTabTmp && i != 0){
							treeTab = treeTabTmp;
							html += '<p class="treeTab">'+treeTab+'</p>';
						}
						if(i == 0 && Number($userListtDom.attr('data-index')) == 1){
							treeTab = treeTabTmp;
							html += '<p class="treeTab">'+treeTab+'</p>';
						}
						html += '<p class="user" data-id="'+user.id+'" data-pic_url="'+user.pic_url+'" data-name="'+user.name+'">';
						html += '<input type="checkbox" class="ck2">';
						html += '<img alt="" src="'+user.pic_url+'" onerror="this.src=\''+ that.comStTml.userDefaultFace +'\'">';
						html += '<span>'+user.name+'</span>';
						html += '</p>';
					}
				}
				if(Number($userListtDom.attr('data-index')) == 1 && html==''){
					html = '<p class="emptyUser">暂无成员</p>';
				}
				var $html= $(html);
				that.checkList($html,'false');
				that.inputSelect($html,'false');
				$userListtDom.append($html);

				
			},
			error:function(){
				console.log('系统繁忙:查找部门人员失败');
			}
		});
	}
	
	depts_users_select.prototype.createUserList = function(deptId,deptName){
		var that = this;
		$('#depts_users_select').find('.treeBody').find('.userTree').remove();
		var deptData = {};
		deptData.dept = {};
		deptData.dept.name = deptName;
		deptData.dept.id = deptId;
		var $userListtDom = $(tmpTool(userListtmp,deptData));
		$userListtDom.attr('data-dept',deptName);
		$userListtDom.attr('data-offset',0);
		$userListtDom.attr('data-limit',100);
		$userListtDom.attr('data-index',0);
		$userListtDom.attr('is_scroll_end','fasle');
		$userListtDom.attr('is_scroll_loading','fasle');
		that.srollLoadUser($userListtDom);
		that.bindUserList($userListtDom);
		$('#depts_users_select').find('.treeBody .depttree ul').hide();
		$('#depts_users_select').find('.treeBody').append($userListtDom);
		window.location.hash='1_2';
		that.bindPysearch();
		that.showBackButton();
	}
	
	depts_users_select.prototype.bindPysearch = function(){
		var that = this;
		$('#depts_users_select .userTree .search-py>span').click(function(event){
			event.stopPropagation();
			$(this).parent().children('span').children('span').add('hide');
			$(this).children('span').removeClass('hide');
		});
		
		$('#depts_users_select .userTree .search-py>span>span>span').click(function(event){
			event.stopPropagation();
			$(this).parent().addClass('hide');
			var treeTab = $(this).html();
			that.srollToTreeTab(treeTab);
		});

		$('#depts_users_select .userTree').on('click',function(){
			$('#depts_users_select .userTree .search-py>span>span').addClass('hide');
		})

		//hdh 2018-03-14 add 
		$('.treeDesc').on('click','input',function(){
			var checkedInput = $(this)[0].checked; 
			$('.userTree').find('.user input').each(function(){
				// $(this)[0].attr('checked',true);
				if(checkedInput != $(this)[0].checked){
					$(this).trigger('click');
				}
			});
		})
	}
	
	depts_users_select.prototype.srollToTreeTab = function(treeTab){
		var that = this;
		var treeTabs = $('#depts_users_select .userTree >p.treeTab');
		var notKey = true;
		var notKeyIndex = 0;
		for (var i = 0; i < treeTabs.length; i++) {
			if($(treeTabs[i]).html() == treeTab){
				notKey = false;
				notKeyIndex = i;
				break;
			}
		}
		if(notKey){
			that.layerPackage.ew_alert({desc:'没有'+treeTab+'开头的成员'});
		}else{
			var treeTab = treeTabs[i];
			$('#depts_users_select .userTree').scrollTop(treeTab.offsetTop);
		}
	}
	
	depts_users_select.prototype.srollLoadUser = function(target,data){
		var that = this;
		var sTimer;
		$(target).off('scroll').scroll(function scrollHandler(){
			clearTimeout(sTimer);
			 sTimer = setTimeout(function() {
				 var c = $(target)[0].scrollHeight;
				 var t = $(target).scrollTop();
				 var h = $(target)[0].clientHeight;
				 var is_scroll_end = $(target).attr('is_scroll_end') == 'true' ? true : false;
				 var is_scroll_loading = $(target).attr('is_scroll_loading') == 'true' ? true : false;
				 if(t + h +200 > c && !is_scroll_end && !is_scroll_loading){
					 that.loadUserListData(data);
				 }
			 },100);
		});
	}
	
	depts_users_select.prototype.showBackButton = function(){
		$('#depts_users_select .depts_users_bottom-bar .back').removeClass('hide');
	}
	
	
	depts_users_select.prototype.hideBackButton = function(){
		$('#depts_users_select .depts_users_bottom-bar .back').addClass('hide');
	}
	
	
	depts_users_select.prototype.getSelectData = function(){
		var that = this;
		var seleceDom = that.target.children('span');
		var arr = [];
		for (var i = 0; i < seleceDom.length; i++) {
			var ob = {};
			 ob.id = $.trim($(seleceDom[i]).attr('data-id'));
			 ob.isDept = $.trim($(seleceDom[i]).attr('data-isDept'));
			 ob.pic_url = $.trim($(seleceDom[i]).attr('data-pic_url'));
			 ob.name = $.trim($(seleceDom[i]).attr('data-name'));
			 arr.push(ob);
		}
		return arr;
	}
	
	depts_users_select.prototype.getInnerselec = function(data){
		var that = this;
		var arr = []
		var selectDoms = $('#depts_users_select .treeBody .selectOb>span');
		for (var i = 0; i < selectDoms.length; i++) {
			var select = $(selectDoms[i]);
			var ob = {};
			ob.isDept = select.attr('data-isdept');
			ob.id = select.attr('data-id');
			ob.name=select.attr('data-name');
			ob.pic_url = select.attr('data-pic_url');
			arr.push(ob);
		}
		return arr;
		
	}
	
	depts_users_select.prototype.bindSearch = function(data){
		var that = this;
		$('#depts_users_select form').submit(function(){
			var keyWorld = $.trim($(this).find('input').val());

			if(keyWorld.length == 0){
				if(that.isSearch){
					$("#depts_users_select").find(".back").click();
				}
				that.isSearch = false;
				return false;
			}

			that.isSearch = true;
			var deptname = '';
			var deptId = '';
			if($('#depts_users_select').find('.treeBody').find('.userTree').length>0 && $('#depts_users_select').find('.treeBody').find('.userTree').attr('data-deptid') != 0){
				deptname = '在'+$('#depts_users_select').find('.treeBody').find('.userTree').attr('data-dept')+'查找 \''+keyWorld+'\'';
				deptId = $('#depts_users_select').find('.treeBody').find('.userTree').attr('data-deptid');
			}else{
				deptname = '在顶级部门查找 \''+keyWorld+'\'';
				deptId = 0
			}
			that.createUserList(deptId,deptname);
			that.loadUserListData(data);
			return false;
		});
	}
	
	depts_users_select.prototype.removeUserListData=function(data){
		$('#depts_users_select .userTree .user').remove();
		$('#depts_users_select .userTree .userTab').remove();
	}
	
	depts_users_select.prototype.bindBottomBar = function(data){
		var that = this;
		$('#depts_users_select .depts_users_bottom-bar .back').click(function(){
			history.back();
		})
		
		$('#depts_users_select .depts_users_bottom-bar .select_count').click(function(){
			if($('#depts_users_select .treeBody .selectOb').hasClass('hide')){
				$('#depts_users_select .treeBody .selectOb').removeClass('hide');
			}else{
				$('#depts_users_select .treeBody .selectOb').addClass('hide');
			}
		})
		
		$('#depts_users_select .depts_users_bottom-bar .ok').click(function(){
			var arr = that.getInnerselec();
			var html = that.getSelecHtml(arr);
			that.target.html(html);
			//$('#depts_users_select .depts_users_bottom-bar .back').click();

			var hash = window.location.hash;
			if(hash){
				var len = hash.split('_').length;
				var num = 0 - len;
				history.go(num);
			}
			
			$('#depts_users_select').remove();
			that.clickHashChage = false;
			if (typeof(that.ret_fun) === 'function') {
				that.ret_fun(that);
			}
		})
		
		$('#depts_users_select .treeBody .selectOb').on('click','span',function(){
			var id = $(this).attr('data-id');
			var idDept = $(this).attr('data-isdept');
			if(idDept == 'true'){
				var inputs = $('#depts_users_select .depttree input.ck2');
				for (var i = 0; i < inputs.length; i++) {
					if($(inputs[i]).parent().parent().parent().parent().parent().attr('data-id') == id){
						inputs[i].checked = false;
						break;
					}
				}
			}else{
				var inputs = $('#depts_users_select .userTree input.ck2');
				for (var i = 0; i < inputs.length; i++) {
					if($(inputs[i]).parent().attr('data-id') == id){
						inputs[i].checked = false;
						break;
					}
				}
			}
			$(this).remove();
			var countDom = $('#depts_users_select .depts_users_bottom-bar .select_count').children('span');
			var count = Number(countDom.html())
			count--;
			countDom.html(count);
		})
		
	}
	
	depts_users_select.prototype.checkList=function($dom,isDept){
		var that = this;
		var select = that.getInnerselec();
		$($dom).find('input').each(function(){
			var id = ''
			if(isDept == 'true'){
				id = $(this).parent().parent().parent().parent().parent().attr('data-id');
			}else{
				id = $(this).parent().attr('data-id')
			}
			for (var i = 0; i < select.length; i++) {
				if(isDept == select[i].isDept && id == select[i].id){
					$(this).attr('checked','checked');
					break;
				}
			}

			// hdh 2018-03-14
			if($(this)[0].checked == false){
				$('.treeDesc input').attr('checked',false);
			}else{
				$('.treeDesc input').attr('checked',true);
			}
			// hdh 2018-03-14 end
		});
	}
	
	depts_users_select.prototype.inputSelect=function($dom,isDept){
		var that = this;
		var select = that.getInnerselec();
		$($dom).find('input').click(function(event){
			event.stopPropagation();
			var ob = null
			if(isDept == 'true'){
				ob = $(this).parent().parent().parent().parent().parent();
			}else{
				ob = $(this).parent();
			}
			var s_ob = {};
			s_ob.isDept = isDept;
			s_ob.id = ob.attr('data-id');
			s_ob.name=ob.attr('data-name');
			s_ob.pic_url = ob.attr('data-pic_url');

			if(this.checked){
				if(that.max_select != 0 && $('#depts_users_select .treeBody .selectOb>span').length + 1 > that.max_select){
					this.checked = false;
					that.layerPackage.ew_alert({desc:'最多只能选择' + that.max_select + '个'});
					return;
				}
				var html = '';
				if(isDept == 'true'){
					html += '<span class="item" data-id="'+s_ob.id+'" data-isDept="'+s_ob.isDept+'" data-name="'+s_ob.name+'" data-pic_url="'+s_ob.pic_url+'">';
					html += '<img alt="" src="' + that.comStTml.fileIcon + '">';
					html += '<span>'+s_ob.name+'</span>';
					html += '</span>';
				}else{
					// hdh 2018-03-14
					var check = [];
					$('.user input').each(function(){
						if(!$(this)[0].checked){
							check.push(this.checked);
						}
					})
					if(check.length == 0){
						$('.treeDesc input')[0].checked = true;
					}
					// hdh 2018-03-14 end
					html += '<span class="item" data-id="'+s_ob.id+'" data-isDept="'+s_ob.isDept+'" data-name="'+s_ob.name+'" data-pic_url="'+s_ob.pic_url+'">';
					html += '<img alt="" src="'+s_ob.pic_url+'" onerror="this.src=\''+ that.comStTml.userDefaultFace +'\'">';
					html += '<span>'+s_ob.name+'</span>';
					html += '</span>';
				}
				$('#depts_users_select .treeBody .selectOb').append(html);
			}else{
				$('.user input').each(function(){
					if(!$(this)[0].checked){
						$('.treeDesc input')[0].checked = false;
					}
				})
				var innerChecks = $('#depts_users_select .treeBody .selectOb>span');
				for (var i = 0; i < innerChecks.length; i++) {
					if(s_ob.isDept == $(innerChecks[i]).attr('data-isDept') && s_ob.id == $(innerChecks[i]).attr('data-id')){
						$(innerChecks[i]).remove();
						break;
					}
				}
			}
			that.changeInnerSelectCount();
		});
	}
	
	depts_users_select.prototype.changeInnerSelectCount = function(){
		var count = $('#depts_users_select .treeBody .selectOb').children().length;
		$('#depts_users_select .depts_users_bottom-bar .select_count').children('span').html(count);
	}
	
	depts_users_select.prototype.get_depts_users=function(){
		var that = this;
		var arr = that.getSelectData();
		var returnArr = [];
		for (var i = 0; i < arr.length; i++) {
			var ob = {};
			ob.isDept = arr[i].isDept;
			ob.id = arr[i].id;
			ob.name = arr[i].name;
			ob.pic_url = arr[i].pic_url;
			returnArr.push(ob);
		}
		return returnArr;
	}

	depts_users_select.prototype.get_depts_users_names = function(){
		var arr = this.get_depts_users();
		var html = new Array();

		for (var i = 0; i < arr.length; i++) {
			html.push(arr[i].name);
		};

		return html.join(";");
	}
	
	depts_users_select.prototype.get_depts_users_ids = function(){
		var that = this;
		var arr = that.getSelectData();
		var returnArr = [];
		for (var i = 0; i < arr.length; i++) {
			returnArr.push(arr[i].id);
		}
		return returnArr;
	}
	
	
	depts_users_select.prototype.getSelecHtml=function(objs){
		var html = '';
		for (var i = 0; i < objs.length; i++) {
			if(i == objs.length -1){
				html += '<span data-name="'+$.trim(objs[i].name)+'" data-pic_url="'+objs[i].pic_url+'" data-id="'+$.trim(objs[i].id)+'" data-isDept="'+$.trim(objs[i].isDept)+'">'+$.trim(objs[i].name)+'</span>';
			}else{
				html += '<span data-name="'+$.trim(objs[i].name)+'" data-pic_url="'+objs[i].pic_url+'" data-id="'+$.trim(objs[i].id)+'" data-isDept="'+$.trim(objs[i].isDept)+'">'+$.trim(objs[i].name)+';</span>';
			}
		}
		return html;
	}
	
	depts_users_select.prototype.setTargetClick=function(){
		if(!(this.targetClick == 1)){
			this.target.off();
		}
	}
	
	depts_users_select.prototype.bindUserList=function($userListtDom){
		var that = this;
		$userListtDom.on('click','p.user',function(){
			 that.showUserDetail($(this));
		});
	} 
	
	depts_users_select.prototype.showUserDetail=function($dom){
		var that = this;
		var opt = {};
		opt.user = $dom.attr('data-id');
		that.layerPackage.lock_screen();
		$.ajax({
			url:that.user_detail+'&time='+(new Date()).getTime(),
			type:'post',
			data_type:'json',
			data:{data:opt},
			success:function(res){
				that.layerPackage.unlock_screen();
				try {
					res = $.parseJSON(res);
				} catch (e) {
					console.log(e);
					console.log('返回数据转json出错');
					that.layerPackage.ew_alert({'desc':'加载数据异常'});
				}
				var data = {};
				data.name = res.info.name;
				data.id = res.info.id;
				data.position = res.info.position;
				data.pic_url = res.info.pic_url + '64';
				var deptStr = ''
				for (var i = 0; i < res.info.dept_list.length; i++) {
					deptStr += res.info.dept_list[i].name + '; '
				}
				deptStr=deptStr.substring(0,deptStr.length-2);
				data.deptStr = deptStr;
				data.userDefaultFace = that.comStTml.userDefaultFace;
				var $userDetailDom = $(tmpTool(userDetailTml,data)); 
				$('#depts_users_select .userDetailMsg').remove();
				$('#depts_users_select').append($userDetailDom);
				window.location.hash='1_2_3';
			}
		});
	}
	
	depts_users_select.prototype.initHashChange=function(){
		var that = this;
		$(window).on('hashchange', function (e) {
			var hash = window.location.hash;
			if(that.clickHashChage && $('#depts_users_select').length > 0){
				if(hash == '#1'){
					$('#depts_users_select').find('.treeBody').find('.userTree').remove();
					$('#depts_users_select').find('.treeBody .depttree ul').show();
					$('#depts_users_select .depts_users_bottom-bar .back').addClass('hide');
				}
				if(hash == '#1_2'){
					$('#depts_users_select .userDetailMsg').remove();
				}
				if(hash == '' || hash == that.initHash){
					$('#depts_users_select').remove();
					that.clickHashChage = false;
				}
			}
        });
	}
	
	
	
	return depts_users_select;
	

});
