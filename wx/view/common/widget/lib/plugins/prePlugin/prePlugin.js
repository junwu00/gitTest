define([ 'jquery',
],function($) {
	var pre = {
		init_img_url:function(opt){
			var target = opt.target;
			var imgList = opt.imgList;
			var html = '';
			html += '<div class="imgpre" style="position: fixed; width: 100%; height: 100%; top: 0; left: 0;background: rgba(0,0,0,0.5);z-index:200;">';
			html += '<div id="removeImgPre" style="position: absolute;right: 5px;top: 5px;z-index: 255;cursor: pointer;"><span style="display: inline-block;font-size: 50px;color: red;cursor: pointer;" class="rulericon-cancel_border"></span></div>';
			html += '<div class="left fl h w100 pr" style="z-index:250;">';
			html += '<span class="rulericon-angle_left c-white f50 hand pa pa-vertical-align" style=""></span>';
			html += '</div>';
			html += '<div data-srolling="false" class="center fl h pt10 pb30" style="width: 100%;height:100%;position:absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);-webkit-transform: translate(-50%, -50%);">';
			html += '<div class="h pr" style="left:0px;">';
			for (var i = 0; i < imgList.length; i++) {
				html += '<div class="h tc" style="position:absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);width:100%;height:100%;">';
				html += '<img style="width:100%;height:100%;position:absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);object-fit:contain" alt="" src="' + imgList[i] + '">';	
				html += '</div>';
			}
			html += '<span class="clear"></span>';
			html += '</div>';
			html += '</div>';
			html += '<div class="right fl h w100 pr" style="z-index:250;">';
			html += '<span class="rulericon-angle_right c-white f50 hand pa pa-vertical-align"></span>';
			html += '</div>';
			html += '</div>';
			$html = $(html);
			$html = this.bindImgPre($html,opt);
			$('.imgpre').remove();
			$(target).append($html);
		},
		bindImgPre:function($html,opt){
			if(opt.imgList.length == 1){
				$html.find('.right span').addClass('hide');
				$html.find('.left span').addClass('hide');
			}
			$html.click(function(event){
				event.stopPropagation();
			});
			$html.find('#removeImgPre').click(function(event){
				event.stopPropagation();
				$(this).parent().remove();
			});
			$html.find('.right span').click(function(){
				var left = Number($('.imgpre').find('.center').children().css('left').replace('px',''));
				var itemWidth = $($('.imgpre').find('.center').children().children()[0]).width();
				var allChildrenWidth = itemWidth * ($('.imgpre').find('.center').children().children('div').length - 1);
				if($('.imgpre').find('.center').attr('data-srolling') == 'false' && (-left) < allChildrenWidth){
					$('.imgpre').find('.center').attr('data-srolling','true');
					left = left - itemWidth;
					$('.imgpre').find('.center').children().animate({left:left+'px'},'fast',function(){
						$('.imgpre').find('.center').attr('data-srolling','false');
					});
				}
			});
			$html.find('.left span').click(function(){
				var left = Number($('.imgpre').find('.center').children().css('left').replace('px',''));
				var itemWidth = $($('.imgpre').find('.center').children().children()[0]).width();
				if($('.imgpre').find('.center').attr('data-srolling') == 'false' && left != 0){
					$('.imgpre').find('.center').attr('data-srolling','true');
					left = left + itemWidth;
					$('.imgpre').find('.center').children().animate({left:left+'px'},'fast',function(){
						$('.imgpre').find('.center').attr('data-srolling','false');
					});
				}
			});
			return $html;
		},
	};
	return pre;
});