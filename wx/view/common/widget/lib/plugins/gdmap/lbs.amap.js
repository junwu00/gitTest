define(['jquery'], function($) {
	/*
	//加载地图API
	var u = navigator.userAgent,script = "",plarforms  = 0;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
		script = "http://webapi.amap.com/maps?v=1.3&key=2061c0bccd8286419797484a43fe6d73";
	} else if (u.indexOf('iPhone') > -1) {//苹果手机
		script = "https://webapi.amap.com/maps?v=1.3&key=2061c0bccd8286419797484a43fe6d73";
	} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
		script = "https://webapi.amap.com/maps?v=1.3&key=2061c0bccd8286419797484a43fe6d73";
	} else {
		plarforms  = 1;
		script = "https://webapi.amap.com/maps?v=1.3&key=2061c0bccd8286419797484a43fe6d73";
	}
	*/

	(function($){
		var settings = {},
		_setting = {
			mapId: "",
			mapObj:null,
			width:0, //地图宽度
			height:0, //地图高度
			css:"",//其他样式

			zoom:10, //默认地图图层
			center:"",//[113,23],
			resizeEnable: true,
			autoMarkCenter:1,//默认定位中心点
			autoFixedCenter:1,//默认缩放地图时保持中心点不变
			controller:0,//默认带缩放等控件
			showLocationBtn:0,//是否显示定位按钮

			searchEnable:1,

			callback: {
				onMapClick:null,
				onAfterSetCenter:null,
				onAfterMapLoaded:null,
				onMoveEnd:null,

				getLocalError:null
			}
		},
		_consts = {
			event: {
				MAP_CLICK: "lbs_click"
			}
		},
		_bindEvent = function(setting){
			var o = setting.mapObj,
			c = consts.event;

			//-------------地图功能-------------
			/*
			 * mapId : 地图id
			 * mapObj : 地图jquery obj
			 */
			o.on(c.MAP_CLICK, function (event, mapId, mapObj) {
				tools.apply(setting.callback.onMapClick, [event,mapId, mapObj]);
			});
		},
		_unbindEvent = function(setting){
			var o = setting.mapObj,
			c = consts.event;
			o.off(c.MAP_CLICK);
		},
		_initCache = function(setting){
			var t = consts.tag;
			this.setCache(setting,{});
			caches[t] = [];
		},
		_init = {
			bind: [_bindEvent],
			unbind: [_unbindEvent],
			caches: [_initCache],
			lbsMaps: []
		},
		data = {
			setCache:function(setting,cache){
				caches[setting.mapId] = cache;
			},
			getCache:function(setting){
				return caches[setting.mapId];
			},
			initCache:function(setting){
				for (var i=0, j=_init.caches.length; i<j; i++) {
					_init.caches[i].apply(this, arguments);
				}
			},exSetting: function(s) {
				$.extend(true, _setting, s);
			},
			addInitBind: function(bindEvent) {
				_init.bind.push(bindEvent);
			},
			addInitUnBind: function(unbindEvent) {
				_init.unbind.push(unbindEvent);
			},
			addInitCache: function(initCache) {
				_init.caches.push(initCache);
			},
			addLbsMaps: function(bitMap) {
			_init.lbsMaps.push(bitMap);
			},
			getSetting: function(treeId) {
				return settings[treeId];
			},
			getSettings: function() {
				return settings;
			},
			setLbsMaps: function(setting, bitMap) {
				for (var i=0, j=_init.lbsMaps.length; i<j; i++) {
					_init.lbsMaps[i].apply(this, arguments);
				}
			}
		},
		//method of event proxy
		event = {
			bindEvent: function(setting) {
				for (var i=0, j=_init.bind.length; i<j; i++) {
					_init.bind[i].apply(this, arguments);
				}
			},
			unbindEvent: function(setting) {
				for (var i=0, j=_init.unbind.length; i<j; i++) {
					_init.unbind[i].apply(this, arguments);
				}
			},
			bindMap:function(setting){
				var eventParam = {
					mapId: setting.mapId
				},
				o = setting.mapObj,
				c = consts.event;

				o.on('click', function () {
					$(this).trigger(c.MAP_CLICK,[eventParam.mapId,o]);
				});
			},
			unbindMap:function(setting){
				var o = setting.mapObj;
				o.off('click');
			}
		},
		tools = {
			apply: function(fun, param, defaultValue) {
				if ((typeof fun) == "function") {
					return fun.apply(lbs, param?param:[]);
				}
				return defaultValue;
			},
			clone: function (obj){
				if (obj === null) return null;
				var o = tools.isArray(obj) ? [] : {};
				for(var i in obj){
					o[i] = (obj[i] instanceof Date) ? new Date(obj[i].getTime()) : (typeof obj[i] === "object" ? arguments.callee(obj[i]) : obj[i]);
				}
				return o;
			},
			isArray: function(arr) {
				return Object.prototype.toString.apply(arr) === "[object Array]";
			},
			$: function(node, exp, setting) {
				if (!!exp && typeof exp != "string") {
					setting = exp;
					exp = "";
				}
				if (typeof node == "string") {
					return $(node, setting ? setting.treeObj.get(0).ownerDocument : null);
				} else {
					return $("#" + node.tId + exp, setting ? setting.treeObj : null);
				}
			},
	    	toBeArr:function(json){
	    		if(this.isArray(json)){
	    			return json;
	    		}

	    		return [json];
	    	}
		},
		view = {
			_initMap:function(setting){
				this._setMapWH(setting);
				this._setMapCss(setting);
				//console.log(setting);
				this._initSearch(setting);
			},
			_setMapWH:function(setting){
				var o = setting.mapObj,
				width = setting.width,height = setting.height;
				if(width == 0 || height == 0){return;}
				o.width(width);
				o.height(height);
			},
			_setMapCss:function(setting){
				var o = setting.mapObj,css = setting.css;

				try{
					o.attr("style",o.attr("style") + css);
				}catch(e){
					console.log("添加额外样式错误！");
				}
			},
			_resetMapCss:function(setting){
				setting.mapObj.removeAttr('style');
			},
			_initSearch:function(setting){
				if(setting.searchEnable != 1){
					return;
				}
				//搜索输入框
				setting.searchInput = $(this._createSearchInputHtml());
				//搜索结果显示页面
				setting.searchListPanel = $(this._createSearchListPanelHtml());

				setting.mapObj.parent()
				.append(setting.searchInput)
				.append(setting.searchListPanel);

				this._searchBindDom(setting);

				gdMap._autoSearch(setting);

				//gdMap._mapClick(setting);
			},
			//创建搜索框
			_createSearchInputHtml:function(){
				var html = new Array();
				html.push('<div class="gdMap-search">');
				html.push('<i class="icon-chevron-left fl" id="gdMap-back"></i>');
				html.push('<input type="text" class="gdMap-search-input" id="gdMap-search-input" placeholder="输入搜索地点" />');
				html.push('<i class="icon-search fl" id="addr-search"></i>');
				html.push('</div>');
				return html.join('');
			},
			//搜索列表
			_createSearchListPanelHtml:function(){
				var html = new Array();
				html.push('<div id="search_list" style="display: block;">');
				html.push('</div>');
				return html.join('');
			},
			_searchBindDom:function(setting){
				var pThis = this,
				searchInput = setting.searchInput;
				
				searchInput.find("#addr-search").on('click', function(event) {
					gdMap._clickSearch(setting);
				});
			},
			_getMarkerLabel:function(pic,address,time,desc){
				var html = new Array(),errorImg = "";
				
				try{
					if(defaultFace){
						errorImg = 'onerror="this.src=\''+defaultFace+'\'"';
					}
				}catch(e){
					console.log(e);
				}

				html.push('<section class="autoH mp0">');
				html.push('<img src="'+pic+'" ' + errorImg + ' alt="" class="fl pic25 cir mr6"/>');
				html.push('<div class="fl w-25">');
				html.push('<span class="mTitle hideTxt fullW" title="'+address+'">'+address+'</span>');
				html.push('<p><time>'+time+'</time></p>');
				if(desc){
					html.push('<span class="mTitle hideTxt fullW">'+desc+'</span>');
				}
				html.push('</div>');
				html.push('</section>');

				return html.join('');
			}
		},
		//高德地图相关
		gdMap = {
			init:function(setting){
				var amap = setting.amap;
				this.setting = setting;
				this._setZoom(setting);

				if (setting.controller == 1) {
					 amap.plugin(["AMap.ToolBar"], function () {
						 amap.addControl(new AMap.ToolBar());
					 });
				}
				
				var pThis = this;
				AMap.event.addListener(amap, 'complete', function() {
					if(setting.center){
						pThis._setCenter(setting);
						pThis._afterInit(setting);
					}else{
						pThis._getLocal(setting,function(lng, lat) {
							setting.center = {lng: lng, lat: lat};
							pThis._setCenter(setting);
							pThis._afterInit(setting);
						});
					}
				});
			},
			//点击地图，将点击的位置设置为中心点
			_mapClick:function(setting){
				var pThis = this;
				AMap.event.addListener(setting.amap, 'click', function(ret) {
					pThis._setCenter(setting,ret.lnglat);
				});
			},
			//设置图层
			_setZoom:function(setting){
				var amap = setting.amap,zoom = setting.zoom;
				amap.setZoom(zoom);
			},
			//设置缩放
			_setResize:function(setting){

			},
			//设置中心点
			_setCenter:function(setting,position) {
				position = this._trans2Position(setting,position);

	        	setting.amap.setCenter(position);
	        	
	        	tools.apply(setting.callback.onAfterSetCenter,[position,gdMap]);
			},
			//获取点心点位置
			_getCenter: function(setting) {
				return setting.amap.getCenter();
			},
			//转换成地图坐标对象
			_trans2Position:function(setting,position) {
				//var position = setting.center;
				if(!position){
					position = setting.center;
				}

				if(tools.isArray(position)){
					return new AMap.LngLat(position[0],position[1]);
				}
				else if(position instanceof AMap.LngLat){
					return position;
				}
				else if(typeof(position) === 'object' && position.lng && position.lat){
					return new AMap.LngLat(position.lng, position.lat);
				}
				else{
					return this._getCenter(setting);
				}
			},
			//获取当前位置
			_getLocal: function(setting,callback) {
				//frame_obj.lock_screen('定位中...', true);
				setting.amap.plugin('AMap.Geolocation', function() {
					geolocation = new AMap.Geolocation({
						enableHighAccuracy: true, //是否使用高精度定位，默认:true
						timeout: 10000, //超过10秒后停止定位，默认：无穷大
						buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
						zoomToAccuracy: true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
						buttonPosition: 'RB',
						showMarker: false,        //定位成功后在定位到的位置显示点标记，默认：true
				        showCircle: false,        //定位成功后用圆圈表示定位精度范围，默认：true
				        panToLocation: false,     //定位成功后将定位到的位置作为地图中心点，默认：true
					});
					if(setting.showLocationBtn){
						setting.amap.addControl(geolocation);
					}
					geolocation.getCurrentPosition();
					//返回定位信息
					AMap.event.addListener(geolocation, 'complete', function(data) {
						//frame_obj.unlock_screen();
						callback(data.position.getLng(), data.position.getLat());
					});
					AMap.event.addListener(geolocation, 'error', function() { //返回定位出错信息
						if(setting.callback.getLocalError && typeof setting.callback.getLocalError === "function"){
							setting.callback.getLocalError();
						}else{
							//frame_obj.unlock_screen();
							alert('获取您当前位置信息失败，请刷新页面后重试');
						}
					});
				});
			},
			//初始化完毕后调用
			_afterInit: function(setting) {
				if (setting.autoMarkCenter == 1) {
					this._markerCenter(setting);
					this._autoMarkCenter(setting);
				}

				if (setting.autoFixedCenter == 1) {
					this._autoFixedCenter(setting);
				}

				//地图加载完毕后回调

				tools.apply(setting.callback.onAfterMapLoaded,[gdMap]);
			},
			//标识中心位置
			_markerCenter:function(setting,position) {
				setting.centerMarker = this._addMarker(setting,{
					position:position || this._getCenter(setting),
					icon:setting.centerIcon,
					iconSize:setting.centerIconSize
				});
			},
			//自动标识中心点（移点/缩放地图时也会）
			_autoMarkCenter:function(setting) {
				var pThis = this;
				AMap.event.addListener(setting.amap, 'moveend', function() {
					var position = pThis._getCenter(setting);

					if(!setting.centerMarker){
						pThis._markerCenter(setting,position);
					}else{
						setting.centerMarker.setPosition(position);
					}
					
					tools.apply(setting.callback.onMoveEnd,[position,setting.centerMarker]);
				});
			},
			//地图缩放后，中心位置不变
			_autoFixedCenter:function(setting) {
				/*
				var pThis = this;
				AMap.event.addListener(setting.amap, 'zoomstart', function() {
					pThis.zoomstart_center = pThis._getCenter(setting);
				});
				AMap.event.addListener(setting.amap, 'zoomend', function() {
					setting.center = pThis.zoomstart_center;
					pThis._setCenter(setting);
				});
				*/
			},
			//删除地图上所有的覆盖物
			_cleanMarker:function(setting) {
				setting.amap.clearMap();
			},
			//增加标注点
			_addMarker:function(setting,opt,setTitle,setLabel,setLabelOffset) {
				/*
				opt = opt || {};
				opt.position = this._trans2Position(opt.position);
				opt.icon = opt.icon || this.opt.defMarkerIcon || "https://webapi.amap.com/theme/v1.3/markers/n/mark_bs.png";
				opt.iconSize = opt.iconSize || this.opt.defMarkerIconSize || [19,33];
				*/
				var obj = {
					position:null,
					icon:gdmapMarkIcon,//"https://webapi.amap.com/theme/v1.3/markers/n/mark_bs.png",
					iconSize:[19,33],
					zIndex:100 //默认100
				}

				opt = $.extend(true, obj, opt);
				
				var marker = new AMap.Marker({
					icon		: opt.icon,
		            position 	: opt.position,
		            offset 		: new AMap.Pixel(-opt.iconSize[0]/2, -opt.iconSize[1]/2),
		            map 		: setting.amap,
		            zIndex      : opt.zIndex
				});

				if(setTitle){
					marker.setTitle(setTitle);
				}

				if(setLabel){
					var x = 0,y = 0;

					if(setLabelOffset){
						x = setLabelOffset.x,y = setLabelOffset.y;
					}

					marker.setLabel({
						offset: new AMap.Pixel(-106+x, -70+y),//修改label相对于maker的位置
						content: setLabel
					});
				}

				return marker;
			},
			//地图中心点平移至指定点位置
			_panTo:function(setting,position) {
				position = this._trans2Position(setting,position);
				
		        setTimeout(function() {
		        	setting.amap.panTo(position);
		        }, 10);
			},
			//设置信息窗口内容（标注点+信息窗口）
			_setInfoDlgData:function(setting,data, opt) {
				opt = opt || {}
				var pThis = this;
				var len = data.length;
				for (var i=0; i<len; i++) {
					var item = data[i],
					obj = {
						position:this._trans2Position(setting,{lng:item.lng, lat: item.lat})
					};

					if(opt.icon){obj.icon = opt.icon;}
					if(opt.iconSize){obj.iconSize = opt.iconSize;}

					var marker = this._addMarker(setting,obj);
					marker.setExtData(item);

					var eventName = "mouseover";
					if(plarforms == 0){
						eventName = "touchstart";
					}
					
					marker.on(eventName,function(e){
						pThis._drawInfoDlg(
							setting,
							this.getExtData().info, 
							this.getPosition()
						);
					});

					/*
					if (item.handler) {
						$(item.handler).off('click').on('click', function() {
							var idx = $(this).attr('data-idx');
							var positionConf = $(this).attr('data-position');
							positionConf = positionConf.split(',');
							var position = pThis._trans2Position({lng:positionConf[0], lat: positionConf[1]});
							pThis._drawInfoDlg(data[idx].info, position);
						});
					}*/
				}
			},
			//生成信息窗口
			_drawInfoDlg:function(setting,info, position) {
				infoWindow = new AMap.InfoWindow({
		            content: info.join("<br/>")  //使用默认信息窗体框样式，显示信息内容
		        });
		        infoWindow.open(setting.amap, position);
	        	this._panTo(setting,position);
			},
			//获取指定位置信息
			_getInfoByPosition:function(setting,position, callback) {
				var pThis = this;
				var MGeocoder;
		        //加载地理编码插件
		        AMap.service(["AMap.Geocoder"], function() {
		            MGeocoder = new AMap.Geocoder({
		                radius: 1000,
		                extensions: "all"
		            });
		            //逆地理编码
					position = pThis._trans2Position(setting,position);
					MGeocoder.getAddress(position, function(status, result) {
		                if (status === 'complete' && result.info === 'OK') {
		                	if (typeof(callback) === 'function') {
		                		//callback(result);
		                		callback.apply(pThis,[result])
		                	}
		                }
		            });
		        });
			},
			//绘制轨迹(无标注点)
			_drawTrail:function(setting,data, startHandler, stopHandler, opt) {
				var dist = 0;
				opt = opt || {};

				var len = data.length,lngX = data[0].lng,latY = data[0].lat;

				//gdMap._cleanMarker(setting);
				
				var marker = this._addMarker(setting,{
					position:[lngX, latY],
					icon:opt.icon,
					offset: typeof(opt.iconSize) === 'undefined' ? new AMap.Pixel(-26, -13) : new AMap.Pixel(-opt.iconSize[0]/2, -opt.iconSize[1]/2),
					autoRotation: true,
					zIndex:1
				});

				var lineArr = new Array();
				lineArr.push([lngX, latY]);

				for (var i = 1; i < len; i++) {
					dist += this._getDistance(data[0], data[i]);
			        lineArr.push([data[i].lng, data[i].lat]);
				}

				// 绘制轨迹
				var polyline = new AMap.Polyline({
					map: setting.amap,
			        path: lineArr,
			        strokeColor: "#00A",  //线颜色
			        strokeOpacity: 1,     //线透明度
			        strokeWeight: 3,      //线宽
			        strokeStyle: "solid"  //线样式
				});

				//高德地图 安卓手机，重复点会报错，不能调用此方法进行地图自适应显示到合适的范围内,点标记已全部显示在视野中
				//setting.amap.setFitView();

				/*
				if (startHandler) {
					var showTime = 5;	//大概5秒显示完
					$(startHandler).off('click').on('click', function() {
						marker.moveAlong(lineArr, dist / showTime * 10);
					});

					marker.on("click",function(){
						this.moveAlong(lineArr, dist / showTime * 10);
					});
				}

				if (stopHandler) {
					$(stopHandler).off('click').on('click', function() {
						marker.stopMove();
					});
				}
				//*/
				marker.on("click",function(){
					this.moveAlong(lineArr, dist / 5 * 10);
				});

				return marker;
			},
			//获取两点间距离
			_getDistance:function(point1, point2) {
				var radLat1 = this._deg2rad(point1.lat); 
				var radLat2 = this._deg2rad(point2.lat); 
				
				var a = radLat1 - radLat2; 
				var b = this._deg2rad(point1.lng) - this._deg2rad(point2.lng); 

				var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b/2),2))); 
				var EARTH_RADIUS = 6378137.0; //单位M 
				s = s * EARTH_RADIUS; 
				s = Math.round(s * 10000) / 10000.0; 
				return s; 
			},
			//角度转为狐度
			_deg2rad:function(d) {
				return d * Math.PI / 180.0; 
			},
			//---------------搜索-------------------
		 	//输入提示\自动查找
			_autoSearch:function(setting) {
				var input = setting.searchInput.find("input"),
				id = input.attr("id"),
				pThis = this;

				AMap.plugin(['AMap.Autocomplete','AMap.PlaceSearch'],function(){
					var placeSearch = new AMap.PlaceSearch({
					    map: setting.amap
					});

					var autocomplete = new AMap.Autocomplete(autoOptions = {
						city: "", //城市，默认全国
						input: id//使用联想输入的input的id
					});

					AMap.event.addListener(autocomplete, "select", function(e){
						setting.centerMarker = null;
						pThis._cleanMarker(setting);

						//TODO 针对选中的poi实现自己的功能
						placeSearch.search(e.poi.name,function(status, result){
						});
					});
				});
			},
			_clickSearch:function(setting){
				var input = setting.searchInput.find("input"),
				keywords = $.trim(input.val()),pThis = this;

				this._cleanMarker(setting);
				setting.centerMarker = null;

				AMap.plugin(['AMap.PlaceSearch'],function(){
					var placeSearch = new AMap.PlaceSearch({
					    map: setting.amap
					});
					placeSearch.search(keywords, function(status, result) {
					});
				});
			},
			//---------------------地图相关---------------------
			addMarker:function(opt,setTitle,setLabel,setLabelOffset){
				return this._addMarker(this.setting,opt,setTitle,setLabel,setLabelOffset);
			},
			getMarkerLabel:function(pic,address,time,desc){
				return view._getMarkerLabel(pic,address,time,desc);
			},
			drawTrail:function(data, startHandler, stopHandler, opt) {
				return this._drawTrail(this.setting,data, startHandler, stopHandler, opt);
			},
			getInfoByPosition:function(position,callback){
				this._getInfoByPosition(this.setting,position, callback);
			},
			getLocal:function(callback){
				if(callback && typeof callback === "function"){
					this._getLocal(this.setting,callback);
				}
			},
			panTo:function(position){
				this._panTo(this.setting,position);
			},
			cleanMarker:function(){
				this._cleanMarker(this.setting);
			}
		};

		$.fn.lbs = {
			consts : _consts,
			_m:{
				tools:tools,
				view:view,
				event: event,
				data: data
			},
			init:function(obj,mSetting){
				var setting = tools.clone(_setting);
				$.extend(true, setting, mSetting);
				setting.mapId = obj.attr("id");
				setting.mapObj = obj;
				setting.mapObj.empty();
				settings[setting.mapId] = setting;

				setting.mapObj.addClass('lbsMap');

				/*$.ajaxSetup({
					cache:true
				});

				$.ajax({
					url:script,
					dataType:"script",
					cache:true
				}).done(function(){
					try{
						setting.amap = new AMap.Map(setting.mapId,{});
						gdMap.init(setting);

						view._initMap(setting);
					}catch(e){
						alert("地图加载失败");
					}
				}).fail(function(){
					
					console.log("加载失败");
				});*/

				setting.amap = new AMap.Map(setting.mapId,{});
				gdMap.init(setting);

				view._initMap(setting);

				data.initCache(setting);
				event.unbindMap(setting);
				event.bindMap(setting);
				event.unbindEvent(setting);
				event.bindEvent(setting);
				
				var lbsMap = {
					setting : setting,
					//设置地图高宽  set:{width:width,height:height}
					setMapWH:function(set){
						$.extend(true, this.setting, set);
						view._setMapWH(this.setting);
					},
					//设置地图样式  set:{css:css}
					setMapCss:function(set){
						$.extend(true, this.setting, set);
						view._setMapCss(this.setting);
					},
					//清楚地图样式
					resetMapCss:function(){
						view._resetMapCss(this.setting);
					},
					//---------------地图相关-------------------
					//添加标注
					addMarker:function(opt,setTitle,setLabel,setLabelOffset){
						gdMap._addMarker(this.setting,opt,setTitle,setLabel,setLabelOffset);
					},
					//删除地图上所有的覆盖物
					cleanMarker:function(){
						gdMap._cleanMarker(this.setting);
					},
					//地图中心点平移至指定点位置
					panTo:function(position){
						gdMap._panTo(this.setting,position);
					},
					//设置信息窗口内容（标注点+信息窗口）
					setInfoDlgData:function(data, opt){
						gdMap._setInfoDlgData(this.setting,data, opt);
					},
					//获取指定位置信息
					getInfoByPosition:function(position,callback){
						gdMap._getInfoByPosition(this.setting,position,callback);
					},
					//绘制轨迹(无标注点)
					drawTrail:function(data, startHandler, stopHandler, opt) {
						return gdMap._drawTrail(this.setting,data, startHandler, stopHandler, opt);
					},
					getMarkerLabel:function(pic,address,time,desc){
						return view._getMarkerLabel(pic,address,time,desc);
					},
					getLocal:function(callback){
						if(callback && typeof callback === "function"){
							gdMap._getLocal(this.setting,callback);
						}
					}
				};

				data.setLbsMaps(setting, lbsMap);

				return lbsMap;
			}
		};

		var lbs = $.fn.lbs,
		$$ = tools.$,
		consts = lbs.consts;
	})(jQuery);

	return $.fn.lbs;
});