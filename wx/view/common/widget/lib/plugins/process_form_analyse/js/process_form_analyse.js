/* 解析流程表单内容 */
define(['jquery'], function ($) {

	var def_opt = {
		cls_th: 'pm_th',
		cls_td: 'pm_td',
		cls_sub_div: 'pm_sub_div',
		cls_sub_tb: 'pm_sub_tb',
		cls_sign_pic: 'pm_sign_pic',
		download_url: '',					//文件下载链接前缀
		file_download: false,				//附件可以下载，默认false
	};

	var entity = {

		//获取object或array的元素个数
		_len: function (data) {
			if (data instanceof Array) {
				return data.length;
			}
			if (data instanceof Object) {
				return Object.keys(data).length;
			}
			return 0;
		},

		//表单数据转表格html
		to_form_html: function (vals, opt) {
			let newVals = {}
			for (let key in vals) {
				if (vals[key].needcheck != 0) {
					newVals[key] = vals[key]
				}
			}
			opt = $.extend(def_opt, opt || {});
			var html = '';
			var html_arr = [];
			var img_index = 0;
			// 打印页面不需要显示的控件类型
			var unShowInoutType = ['relation_report'];
			for (var i in newVals) {
				var form_val = newVals[i];
				if (!form_val['type'] || form_val['type'].indexOf(unShowInoutType) > -1) continue;

				temp_html_arr = [];
				temp_html = '';
				temp_type = 0;

				if (form_val['type'] == 'textarea') {
					temp_html = '<th class="' + opt.cls_th + '">' + form_val['name'] + '</th>';
					temp_html += '<td colspan="3" class="' + opt.cls_td + '"><pre>' + form_val['val'] + '</pre></td>';
					temp_type = 1;
				}
				else if (form_val['type'] == 'file' && Number(form_val['file_type']) == 0) { // 文件
					temp_html = '<th class="' + opt.cls_th + '">' + form_val['name'] + '</th>';
					temp_html += '<td colspan="3" class="' + opt.cls_td + '">';
					if (form_val.val && Array.isArray(form_val.val)) {
						form_val.val.forEach(f => {
							temp_html += '<div class="pb5"><span>' + f['file_name'] + '</span>';
							if (opt.file_download) {
								temp_html += '<a href="' + opt.download_url + '&hash=' + f['file_hash'] + '" target="_target" class="pm_download">下载</a></div>';
							}
						})
					}
					temp_html += '</td>';
					temp_type = 1;
				}
				else if (form_val['type'] == 'table') {		//子表
					var recs = form_val['rec'] || [];

					temp_html = '<td colspan="4">' +
						'<div style="overflow-x:auto" class="' + opt.cls_sub_div + '">' +
						'<table class="' + opt.cls_sub_tb + '">' +
						'<tr style="page-break-after: auto;">' +
						'<th colspan="' + entity._len(recs[0]) + '" class="tc ' + opt.cls_th + '">' + form_val['name'] + '</th></tr>';

					if (entity._len(recs) <= 0) {	//等于0，直接显示无
						temp_html += '<tr><th colspan="' + entity._len(recs[0]) + '" class="tc fb">暂无</th></tr>';
					}
					else {
						//表头
						temp_html += '<tr style="page-break-after: auto;">';
						for (var j in recs[0]) {
							if (recs[0][j]['type'].indexOf(unShowInoutType) > -1) {
								temp_html += ''
							}
							else {
								temp_html += '<th>' + recs[0][j]["name"] + '</th>';
							}
						}
						temp_html += '</tr>';
						for (var j in recs) {
							var rec = recs[j];
							temp_html += '<tr style="page-break-after: auto;">';

							for (var k in rec) {
								var item = rec[k];
								temp_item_val = '';

								if (item['val'] && item['val'] != '') {
									temp_item_val = item['val'];
								}
								if (item['type'] == 'money' && item['val'] && item['val'].indexOf('.') > -1) {
									money_type = item['money_type'];
									temp_item_val = Number(temp_item_val).toFixed(2);
									var unit = '¥';
									switch (Number(money_type)) {
										case 1: unit = '$'; break;
										case 2: unit = '€'; break;
										case 3: unit = 'HK$'; break;
									}
									temp_item_val = unit + temp_item_val;

									if (Number(item['capitalized']) == 1) {
										temp_item_val += '<br>(' + item['capitalized_val'] + ')';
									}
								}
								else if (item['type'] == 'money' && item['val']) {
									money_type = item['money_type'];
									temp_item_val = Number(temp_item_val).toFixed(0);
									var unit = '¥';
									switch (Number(money_type)) {
										case 1: unit = '$'; break;
										case 2: unit = '€'; break;
										case 3: unit = 'HK$'; break;
									}
									temp_item_val = unit + temp_item_val;

									if (Number(item['capitalized']) == 1) {
										temp_item_val += '<br>(' + item['capitalized_val'] + ')';
									}
								}

								else if (item['type'] == 'file' && Number(item['file_type']) == 0) { // 文件
									temp_item_val = ''
									if (item.val && Array.isArray(item.val)) {
										item.val.forEach(f => {
											temp_item_val += '<div class="pb5"><span>' + f['file_name'] + '</span>';
											if (opt.file_download) {
												temp_item_val += '<a href="' + opt.download_url + '&hash=' + f['file_hash'] + '" target="_target" class="pm_download">下载</a></div>';
											}
										})
									}

								}
								else if (item['type'] == 'file' && Number(item['file_type']) != 0) { // 文件图片
									temp_item_val = ''
									if (item.val && Array.isArray(item.val)) {
										item.val.forEach(f => {
											temp_item_val += '<img data-index="' + img_index + '" class="viewerImg" data-url="' + f['image_path'] + '" style="object-fit: cover;width:28px;height:50px;margin: 0 5px 5px 0;" src="' + f['image_path'] + '" alt="' + f['file_name'] + '"></img>'
											img_index++
										})
									}
								}
								else if (item['type'] == 'dangerous') { // 隐患控件
									temp_item_val = ''
									item['snake_list'].forEach((snakeItem, aindex) => {
										let snakeStatue = ''
										if (Number(item['val'][aindex]) == 1) {
											snakeStatue = '<span style="color: #7CD168;">（√）</span>'
										}
										if (Number(item['val'][aindex]) == 2) {
											snakeStatue = '<span style="color: red;">（×）</span>'
										}
										if (Number(item['val'][aindex]) == 3) {
											snakeStatue = '<span class="pm_orange">（无）</span>'
										}

										temp_item_val += '<p><span>' + snakeItem + '</span>' + snakeStatue + '</p>'
									})
								}
								if (item['type'].indexOf(unShowInoutType) > -1) {
									temp_html += ''
								} else {
									temp_html += '<td><div>' + temp_item_val + '</div></td>';

								}

							}
							temp_html += '</tr>';
						}
					}

					temp_html += '</table></div></td>';
					temp_type = 1;
				}
				else {
					val = '';
					if (form_val['val'] && form_val['val'] != '') {
						val = form_val['val'];
					}

					if (form_val['type'] == 'money' && form_val['val'] && form_val['val'].indexOf('.') > -1) {
						money_type = form_val['money_type'];
						val = Number(val).toFixed(2);
						var unit = '¥';
						switch (Number(money_type)) {
							case 1: unit = '$'; break;
							case 2: unit = '€'; break;
							case 3: unit = 'HK$'; break;
						}
						val = unit + val;

						if (Number(form_val['capitalized']) == 1) {
							val += '<br>(' + form_val['capitalized_val'] + ')';
						}
					}
					else if (form_val['type'] == 'money' && form_val['val']) {
						money_type = form_val['money_type'];
						val = Number(val).toFixed(0);
						var unit = '¥';
						switch (Number(money_type)) {
							case 1: unit = '$'; break;
							case 2: unit = '€'; break;
							case 3: unit = 'HK$'; break;
						}
						val = unit + val;

						if (Number(form_val['capitalized']) == 1) {
							val += '<br>(' + form_val['capitalized_val'] + ')';
						}
					}
					else if (form_val['type'] == 'file' && Number(form_val['file_type']) != 0) { // 文件图片
						val = ''
						if (form_val.val && Array.isArray(form_val.val)) {
							form_val.val.forEach(f => {
								val += '<img data-index="' + img_index + '" class="viewerImg" data-url="' + f['image_path'] + '" style="object-fit: cover;width:57px;height:100px;margin: 0 5px 5px 0;" src="' + f['image_path'] + '" alt="' + f['file_name'] + '"></img>'
								img_index++
							})
						}
					}
					else if (form_val['type'] == 'dangerous') { // 隐患控件
						val = ''
						if (!form_val.danger_list) {
							form_val['snake_list'].forEach((snakeItem, aindex) => {
								let snakeStatue = ''
								if (Number(form_val['val'][aindex]) == 1) {
									snakeStatue = '<span style="color: #7CD168;">（√）</span>'
								}
								if (Number(form_val['val'][aindex]) == 2) {
									snakeStatue = '<span style="color: red;">（×）</span>'
								}
								if (form_val['val'] && Number(form_val['val'][aindex]) == 3) {
									snakeStatue = '<span style="color: red;">（无）</span>'
								}
								val += '<p><span>' + snakeItem + '</span>' + snakeStatue + '</p>'
							})
						} else {
							let dangerous_list = [];
							for (let i = 0; i < form_val.danger_list.length; i++) {
								dangerous_list[i] = {
									...form_val.danger_list[i],
									name: form_val['snake_list'][i],
									color: form_val.danger_list[i].val == 1 ? '#7CD168' : form_val.danger_list[i].val == 2 ? 'red' : form_val.danger_list[i].val == 3 ? '#f59d2b' : '' ,
                                	icon: form_val.danger_list[i].val == 1 ? '（√）' : form_val.danger_list[i].val == 2 ? '（×）' : form_val.danger_list[i].val == 3 ? '（无）' : '',
								}
							}
							for (let item of dangerous_list) {
								let snakeStatue = `<span style="color: ${item.color};">${item.icon}</span>`
								val += '<p><span>' + item.name + '</span>' + snakeStatue + '</p>'
								if (item.val == 2) {
									let beforeImg = ''
									item.before_img.map(e => {
										beforeImg += `<img class="previewImage" style="margin: 5px 5px 5px 0;cursor: pointer;" width="100" height="60" src="${e.image_path}" alt="" />`
									})
									let afterImg = ''
									item.after_img.map(e => {
										afterImg += `<img class="previewImage" style="margin: 5px 5px 5px 0;cursor: pointer;" width="100" height="60" src="${e.image_path}" alt="" />`
									})
									if (beforeImg.length > 0) {
										val += '<div><p style="margin: 0;">隐患图片</p>' + beforeImg + '</div>'
									}
									if (afterImg.length > 0) {
										val += '<div><p style="margin: 0;">整改后图片</p>' + afterImg + '</div>'
									}
								}
							}
						}

					}
					temp_html = '<td class="' + opt.cls_th + '" width="15%">' + form_val['name'] + '</td>';
					temp_html += '<td class="' + opt.cls_td + '" width="35%">' + val + '</td>';
				}
				temp_html_arr['temp_html'] = temp_html;
				temp_html_arr['temp_type'] = temp_type;
				html_arr.push(temp_html_arr);
			}

			row_num = 0;
			other_html = '<td class="' + opt.cls_th + '"></td><td class="' + opt.cls_td + '"></td>';
			var len = entity._len(html_arr);
			for (var i = 0; i < len; i++) {
				temp_html_arr = html_arr[i];
				if (row_num == 0) {
					html += '<tr style="page-break-after: auto;">';
					is_close_tr = false;
				}
				if (Number(temp_html_arr['temp_type']) == 1) {
					if (row_num == 1) {
						html += other_html + '</tr><tr style="page-break-after: auto;">';
					}
					row_num = 1;
				}
				html += temp_html_arr['temp_html'];
				row_num++;
				if (row_num == 2) {
					html += '</tr>';
					row_num = 0;
				}
			}

			if (row_num != 0) {
				html += other_html + '</tr>';
			}

			return html;
		},

		//审批记录转为表单
		to_deal_html: function (workitems, opt) {
			opt = $.extend({}, def_opt, opt || {});

			var html = '<tr style="page-break-after: auto;">' +
				'<td colspan="4">' +
				'<table style="width: 100%" class="' + opt.cls_sub_tb + '">' +
				'<tr>' +
				'<th colspan="7" class="tc ' + opt.cls_th + '">处理信息</th>' +
				'</tr>' +
				'<tr>' +
				'<th class="' + opt.cls_th + '">步骤名称</th>' +
				'<th class="' + opt.cls_th + '">步骤属性</th>' +
				'<th class="' + opt.cls_th + '">处理人</th>' +
				'<th class="' + opt.cls_th + '">处理状态</th>' +
				'<th class="' + opt.cls_th + '">处理时间</th>' +
				'<th class="' + opt.cls_th + '">处理意见</th>' +
				'<th class="' + opt.cls_th + '">目标步骤</th>' +
				'</tr>';

			for (var i in workitems) {
				var l = workitems[i];
				l['state'] = Number(l['state']);
				if (l['state'] == 6 || l['state'] == 7 || l['state'] == 8 || l['state'] == 9 || l['state'] == 14) {
					//步骤属性
					l['work_type'] = Number(l['work_type']);
					l['is_linksign'] = Number(l['is_linksign']);
					var step = '';
					if (l['work_type'] == 1) {
						step = '承办';
					} else if (l['work_type'] == 0 && l['is_linksign'] == 1 && l['state'] == 7) {
						step = '审批(签字)';
					} else {
						if (l['is_start'] == 1) {
							step = '申请';
						} else {
							step = '审批';
						}
					}
					//处理状态
					var state = '';
					if (l['state'] == 7) {
						if (l['work_type'] == 1) {
							state = '已确认';
						} else {
							if (l['is_start'] == 1) {
								state = '已发送';
							} else {
								state = '已同意';
							}
						}
					} else if (l['state'] == 8) {
						state = '已退回';
					} else if (l['state'] == 6) {
						state = '已驳回';
					} else if (l['state'] == 14) {
						state = '通过已撤销';
					}
					//处理意见
					var advice = '';
					if (l['work_type'] == 0 && l['is_linksign'] == 1 && l['state'] == 7) {
						advice = '<img src="' + opt.linksign_url + l['judgement'] + '" class="' + opt.cls_sign_pic + '">'; //TODO
					} else {
						advice = l['judgement'];
					}


					html += '<tr>' +
						'<td class="' + opt.cls_td + '">' + l['workitem_name'] + '</td>' +
						'<td class="' + opt.cls_td + '">' + step + '</td>' +
						'<td class="' + opt.cls_td + '">' + l['handler_name'] + '</td>' +
						'<td class="' + opt.cls_td + '">' + state + '</td>' +
						'<td class="' + opt.cls_td + '">' + entity._dateFormat(l['complete_time']) + '</td>' +
						'<td class="' + opt.cls_td + '">' + advice + '</td>' +
						'<td class="' + opt.cls_td + '">' + (l['target_node'] || '') + '</td>' +
						'</tr>';

				}
			}

			html += '</table></td></tr>';

			return html;
		},

		//附件
		to_file_html: function (files, opt) {
			opt = $.extend(def_opt, opt || {});

			if (entity._len(files) <= 0) return '';
			var html = '<tr>' +
				'<td colspan="4">' +
				'<table style="width: 100%" class="' + opt.cls_sub_tb + '">' +
				'<tr>' +
				'<th colspan="4" class="tc ' + opt.cls_th + '">附件信息</th>' +
				'</tr>' +
				'<tr>' +
				'<th class="' + opt.cls_th + '">上传节点</th>' +
				'<th class="' + opt.cls_th + '">文件名称</th>' +
				'<th class="' + opt.cls_th + '">上传者</th>' +
				'<th class="' + opt.cls_th + '">上传时间</th>' +
				'</tr>';

			for (var i in files) {
				var f = files[i];
				var download_html = '';
				if (opt.file_download) {
					download_html = '<a href="' + opt.download_url + '&hash=' + f['file_key'] + '" target="_target" class="pm_download">下载</a>';
				}
				html += '<tr>' +
					'<td class="' + opt.cls_td + '">' + f['work_node_name'] + '</td>' +
					'<td class="' + opt.cls_td + '">' + f['file_name'] + download_html + '</td>' +
					'<td class="' + opt.cls_td + '">' + f['create_name'] + '</td>' +
					'<td class="' + opt.cls_td + '">' + entity._dateFormat(f['create_time']) + '</td>' +
					'</tr>';
			}
			html += '</table></td></tr>';

			return html;
		},

		_dateFormat: function (time, style) {
			if (!Date.Format) {
				Date.prototype.Format = function (fmt) {
					var o = {
						"M+": this.getMonth() + 1,
						"d+": this.getDate(),
						"h+": this.getHours(),
						"m+": this.getMinutes(),
						"s+": this.getSeconds(),
						"q+": Math.floor((this.getMonth() + 3) / 3),
						"S": this.getMilliseconds()
					};
					if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
					for (var k in o)
						if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
					return fmt;
				};
			}

			if (time == null || time == '' || time.length == 0) {
				return '';
			}
			if (style == undefined || style == null || style == '') {
				style = 'yyyy-MM-dd hh:mm';
			}
			var date = null;
			if (time.length == 10) {
				date = new Date(time * 1000);
			} else if (time.length == 13) {
				date = new Date(time);
			} else {
				date = new Date();
			}
			return date.Format(style);
		},

	};

	return entity;

});
