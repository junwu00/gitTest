define(['jquery'],function($){
	var tools = {
		/*
		 * 重定向this
		 */
		apply: function(fun,callback, param, defaultValue) {
			if ((typeof fun) == "function") {
				return fun.apply(callback, param?param:[]);
			}
			return defaultValue;
		},
		/*
		 * 克隆
		 */
		clone: function (obj){
			if (obj === null) return null;
			var o = tools.isArray(obj) ? [] : {};
			for(var i in obj){
				o[i] = (obj[i] instanceof Date) ? new Date(obj[i].getTime()) : (typeof obj[i] === "object" ? arguments.callee(obj[i]) : obj[i]);
			}
			return o;
		},
		/*
		 * 判断相等
		 */
		eqs: function(str1, str2) {
			return str1.toLowerCase() === str2.toLowerCase();
		},
		/*
		 * 判断是否数组
		 */
		isArray: function(arr) {
			return Object.prototype.toString.apply(arr) === "[object Array]";
		},
		/*
		 * 图片加载
		 */
		imgLoad:function(img, callback) {
            var timer = setInterval(function() {
                if (img.complete) {
                    callback(img);
                    clearInterval(timer);
                }
            }, 50);
    	},
    	/*
    	 * json转数组
    	 */
    	toBeArr:function(json){
    		if(this.isArray(json)){
    			return json;
    		}
    		return [json];
    	},
    	/*
    	 * 判断是否整形
    	 */
    	isInteger: function (obj) {
		    return typeof obj === 'number' && obj%1 === 0
		},
		/*
		 * 判断是否jquery对象
		 */
		checkJqObj:function(jqObj){
			if(jqObj == null || !(jqObj instanceof jQuery)){
				return false;
			}
			return true;
		},
		/*
		 * 获取地址参数
		 */
		getUrlParam:function(key){
			var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
		    var r = window.location.search.substr(1).match(reg);
			if (r!=null) return unescape(r[2]); return null;
		},
		/*
		 * 判断是否函数
		 */
		isFunction:function(func){
			return typeof func === "function";
		},
		/*
		 * 时间格式化
		 */
		dateFormat:function(time,style){
			// 对Date的扩展，将 Date 转化为指定格式的String
			// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
			// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
			// 例子： 
			// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
			// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
			if(!Date.Format){
				Date.prototype.Format = function (fmt) { //author: meizz 
				    var o = {
				        "M+": this.getMonth() + 1, //月份 
				        "d+": this.getDate(), //日 
				        "h+": this.getHours(), //小时 
				        "m+": this.getMinutes(), //分 
				        "s+": this.getSeconds(), //秒 
				        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
				        "S": this.getMilliseconds() //毫秒 
				    };
				    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
				    for (var k in o)
				        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
				    return fmt;
				};
			}

			if (time == null || time == '' || time.length == 0) {
				return '';
			}
			if (style == undefined || style == null || style == '') {
				style = 'yyyy-MM-dd hh:mm';
			}
			var date = null;
			if (time.toString().length == 10) {		//php中time()的长度为10位，秒级
				date = new Date(time * 1000);
			} else if (time.toString().length == 13) {	//js中，毫秒级
				if(typeof(time) == "string"){
					time = parseInt(time);
				}
				date = new Date(time);
			} else {
				date = new Date();
			}
			return date.Format(style);
		},
		/*
		 * console重写
		 */
		console:function(msg,type){
			if(!type || !this.isInteger(type)){type = 0;}

			if(!msg){return;}

			switch(type){
				case 0:
				console.log(msg);
				break;
				case 1:
				console.error(msg);
				break;
				case 2:
				console.warn(msg);
				break;
				case 3:
				console.dir(msg);
				break;
			}
		},
		//获取地址参数
		getHrefParamVal:function(key){
			var linkParams = window.location.search;
		    linkParams = linkParams.substring(1).split("&");

		    var val = null;

		    linkParams.forEach(function(p){
		    	var arr = p.split("=");
				if(arr[0] == key){
					val =  arr[1];
				} 
		    });

		    return val;
		},
		//获取现在时间
		getNowDate:function(time,isJson){
			var date = time ? new Date(time) : new Date();

			var month = date.getMonth()+1;
			month = month >= 10 ? month :"0" +  month;

			var days = date.getDate();
			days = days >= 10 ? days : "0" + days;

			var hours = date.getHours();
			hours = hours >= 10 ? hours : "0" + hours;

			var minutes = date.getMinutes();
			minutes = minutes >= 10 ? minutes: "0" + minutes;

			var seconds = date.getSeconds();
			seconds = seconds >= 10 ? seconds: "0" + seconds;

			var week = date.getDay();

			if(isJson){
				return {
					year:date.getFullYear(),
					month:month,
					days:days,
					hours:hours,
					minutes:minutes,
					seconds:seconds,
					week:week
				}
			}

			return date.getFullYear()+"-"+month+"-"+days+" "+hours+":"+minutes;
		},
		/*
		 * 时间差转换 刚刚，2分钟前，4天前
		 */
		getDiffTime:function(firstTime,lastTime,type){
			var days = 0,minutes = 0,hours = 0;
			try{
				if(!firstTime){firstTime = new Date().getTime()/1000;}
				if(!lastTime){lastTime = new Date().getTime()/1000;}

				var time = Math.abs(lastTime - firstTime);

				days = Math.floor(time / (24 * 3600));
				leave1 = time % (24 * 3600);
				hours = Math.floor(leave1 / (3600));
				leave2 = leave1 % (3600);
				minutes = Math.floor(leave2 / (60));
				leave3 = leave2 % (60);
				seconds = Math.floor(leave3);
			}catch(e){
				alert(e);						
			}

			if(!type){
				if(days == 0){days = "";}else{days = days + "天"}
				if(hours == 0){hours = "";}else{hours = hours + "小时"}
				if(minutes == 0){minutes = "";}else{minutes = minutes + "分钟"}

				var str = days + hours + minutes;

				if(str == ""){
					str = "刚刚";
				}else{
					str += "前";
				}

				return str;
			}else{
				return {
					days: days,
					hours: hours,
					minutes: minutes,
					seconds:seconds
				};
			}
		},
		/*
		 * 文件大小转换
		 */
		bytesToSize:function(bytes) {
			if (bytes === 0) return '0 B';
			var k = 1024, // or 1024
				sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
				i = Math.floor(Math.log(bytes) / Math.log(k));
			return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
		},
		/*
		 *txt,xml,pdf,zip,doc,ppt,xls,docx,pptx,xlsx,rar,jpeg,jpg,png,rm,rmvb,wmv,avi,mpg,mpeg,mp4,mp3,wma,wav,amr
		 *文件图标统一管理 
		 */
		getFileTypeExtIcon:function(fileTypeExt){
			if(!fileTypeExt){return;}
			fileTypeExt = fileTypeExt.toLowerCase();

			switch(fileTypeExt){
				case "txt":
					return "txt";
				case "pdf":
					return "pdf";
				case "zip":
					return "zip";
				case "rar":
					return "zip";
				case "doc":
					return "doc";
				case "docx":
					return "doc";
				case "ppt":
					return "ppt";
				case "pptx":
					return "ppt";
				case "xls":
					return "xls";
				case "xlsx":
					return "xls";
				case "jpeg":
					return "img";
				case "jpg" :
					return "img";
				case "png":
					return "img";
				case "gif":
					return "img";
				case "bmp":
					return "img";
				case "rm":
					return "video";
				case "rmvb":
					return "video";
				case "wmv":
					return "video";
				case "avi":
					return "video";
				case "mpg":
					return "video";
				case "mpeg":
					return "video";
				case "mp4":
					return "video";
				case "mp3":
					return "music";
				case "wma":
					return "music";
				case "wav":
					return "music";
				case "ram":
					return "music";
				case "amr":
					return "music";
				default://xml
					return "no"
			}
		},
		//输入控制
		checkVal:function(val,replaceVal){
			if(val === undefined || val === ""){
				if(replaceVal){return replaceVal;}

			 	return "";
			}

			return val;
		},
		//替换字符
		replaceStr:function(val,str,replace){
			if(!val){return "";}
			var r = new RegExp(str,"g");
			return val.replace(r,replace);
		},
		//获取HTML5时间格式
		getDateTimeLocal:function(timeStr){
			if(!timeStr){return "";}
			return timeStr.replace(' ',"T");
		},
		/*
		 * 金额大小写转换
		 */
		DX: function(num) {
			var strOutput = "";
			var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
			num += "00";
			var intPos = num.indexOf('.');
			if (intPos >= 0) {
				num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
			}

			strUnit = strUnit.substr(strUnit.length - num.length);
			for (var i = 0; i < num.length; i++) {
				strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i, 1), 1) + strUnit.substr(i, 1);
			}
			return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");
		},
		/*
		 * 正则表达式校验
		 */
		regCheck:function(str, st){
			if (st == undefined || st == '' || st == null)	st = 'notnull';
			
			var reg = '';
		    switch(st){
		        case 'sname'    :{ reg = /^[\u4e00-\u9fa5a-zA-Z]{1}[\u4e00-\u9fa5_a-zA-Z0-9]*$/; break;}
		    	case 'notnull'  :{ reg = /^[\s\t\n\r]*[\S]+[\s\t\n\r\S]*$/; break;}
		    	case 'pwd'  	:{ reg = /^[\w\d_]{6,16}$/; break;}
		    	case 'number'  	:{ reg = /^[\d]+$/; break;}
		        case 'email'	:{ reg = /^(\w+[-+.]*\w+)*@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/; break; }
		        case 'http'     :{ reg = /^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/; break; }
		        case 'qq'       :{ reg = /^[1-9]\d{4,11}$/; break; }
		        case 'english'  :{ reg = /^[A-Za-z]+$/; break; }
		        case 'mobile'   :{ reg = /^1[34578]\d{9}$/; break; }
		        case 'mobile_s' :{ reg = /^\d{11}$/; break; }
		        case 'phone'    :{ reg = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/; break; }
		        case 'en_num'   :{ reg = /^[A-Za-z0-9]+$/; break; }
		        case 'ip'       :{ reg = /^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/; break; }
		        case 'mac'      :{ reg = /^[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}$/; break; };
		        case 'float'	:{ reg = /^(([1-9]\d*)|\d)(\.\d{1,2})?$/;	break;}
		        case 'money'	:{ reg = /^(([1-9]{1}[0-9]{0,11}(\.[0-9]{1,2})?)|(0{1}\.[0-9][1-9])|(0{1}\.[1-9]{1,2}))$/; break;}
		        case 'double'	:{ reg = /^(([1-9]\d*)|\d)(\.\d+)?$/; break; }
		    }
		   	if(reg == '') return false;
		   	return reg.test(str);
		},
		/*
		 * 数字小于10时前面补0
		 */
		returnFullNumber:function(number){
			if(number < 10){
				return "0"+number;
			}

			return number;
		},
		/*
		 * 时间差转换 天级别
		 */
		getShortTime:function(time,hideTime){
			var n = new Date(),
				nNum = n.getTime()/1000,
				nYear = n.getFullYear(),
				nHour = n.getHours(),
				nMinute = n.getMinutes(),
				nSecond = n.getSeconds(),
				nCount = nHour*60*60+nMinute*60+nSecond;

			if(typeof time === "string"){
				time = parseInt(time);
			}
			if(time.toString().length == 10){
				time = time*1000;
			}
			var t = new Date(time),
				tNum = t.getTime()/1000,
				tYear = t.getFullYear(),
				tMonth = tools.returnFullNumber(t.getMonth() + 1),
				tDay = tools.returnFullNumber(t.getDate()),
				tTime = tools.returnFullNumber(t.getHours()) + ":" + tools.returnFullNumber(t.getMinutes());

			var d = tools.getDiffTime(tNum,nNum,1);
			num = d.hours*60*60 + d.minutes*60 +d.seconds;
			if(d.days == 0 && num < nCount){
				return tTime;
			}
			else if((d.days == 0 && num > nCount) || (d.days == 1 && num < nCount)){
				return "昨天" + tTime;
			}
			else if((d.days == 1 && num > nCount) || (d.days == 2 && num < nCount)){
				return "前天" + tTime;
			}
			else if(tYear == nYear){
				return tMonth+"月"+tDay+"日"+" "+ (hideTime ? "" : tTime);
			}else{
				return tYear + "年" + tMonth+"月"+tDay+"日"+" "+(hideTime ? "" : tTime);
			}
		},
		/*
		 * 获取当天格式化时间
		 */
		getNowFormatDate:function (format) {
		    var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    var hour = date.getHours();
		    var minute = date.getMinutes();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    if(hour>=0 && hour <= 9){
		    	hour = "0" + hour;
		    }
		    if(minute>=0 && minute <= 9){
		    	minute = "0" + minute;
		    }
		    var currentdate = "";
		    if(format=="date"){
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
		    }
		    else if(format=="time"){
		    	currentdate = hour + seperator2 + minute;
		    }
		    else{
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		        + "T" + hour + seperator2 + minute;
		    }
		    return currentdate;
		},
		//检查URL
		checkUrl:function(url){
			if(url.indexOf("?") >= 0){
				return url;
			}

			return url + "?";
		},
		//换行
		checkNewline:function(str){
			return str.replace(/\n/g,'<br/>');
		},
		//星期转换
		getWeek:function(week){
			switch (week) {
				case 1:
					return "一";
					break;
				case 2:
					return "二";
					break;
				case 3:
					return "三";
					break;
				case 4:
					return "四";
					break;
				case 5:
					return "五";
					break;
				case 6:
					return "六";
					break;
				case 0:
					return "日";
					break;
			}
		},
		/*
		 * 正则校验
		 */
		checkData:function(str, st){
			if (st == undefined || st == '' || st == null)	st = 'notnull';
			var reg = '';
		    switch(st){
		    	case 'notnull'  :{ reg = /^[\s\t\n\r]*[\S]+[\s\t\n\r\S]*$/; break;}
		    	case 'pwd'  	:{ reg = /^[\w\d_]{6,16}$/; break;}
		    	case 'number'  	:{ reg = /^[\d]+$/; break;}
		        case 'email'	:{ reg = /^(\w+[-+.]*\w+)*@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/; break; }
		        case 'http'     :{ reg = /^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/; break; }
		        case 'qq'       :{ reg = /^[1-9]\d{4,11}$/; break; }
		        case 'english'  :{ reg = /^[A-Za-z]+$/; break; }
		        case 'mobile'   :{ reg = /^((\(\d{3}\))|(\d{3}\-))?((13)|(15)|(18)|(14)|(17)){1}\d{9}$/; break; }
		        case 'phone'    :{ reg = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/; break; }
		        case 'en_num' :{ reg = /^[A-Za-z0-9]+$/; break; }
		        case 'ip'       :{ reg = /^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/; break; }
		        case 'mac'      :{ reg = /^[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}$/; break; };
		        case 'ext'      :{ reg = /gif|png|jpg|jpeg|bmp/i; break; }
		        case 'double'	:{ reg = /^(([1-9]\d*)|\d)(\.\d+)?$/; break; }
		        case 'money'	:{ reg = /^(([1-9]{1}[0-9]{0,11}(\.[0-9]{1,2})?)|(0{1}\.[0-9][1-9])|(0{1}\.[1-9]{1,2}))$/; break;}
		    }
		   	if(reg == '') return false;
			return reg.test(str);
		},
		/*
		 * iframe判断是否跨域
		 */
		isCrossDomain:function(){
			try {
				window.top.document.domain;
				if(window.top === window || window.document.domain == window.top.document.domain){	
					return false;
				}else{
					return true;
				}
			} catch (e) {
				return true;
			}
		},
		//是否在iframe中
		isIframe:function() {
			try {
				return (top.location != self.location);
				
			} catch (e) {
				return false;
			}
		},
		/*
		 * 判断是否Safari浏览器
		 */
		isSafari:function(){
			if(navigator.userAgent.toLowerCase().match(/version\/([\d.]+).*safari/) != null){
				return true;
			}else{
				return false;
			}
			
		},
		/**
		 * 根据指定的时间，获取增加/减少指定时长后的时间
		 * @param srcTime
		 * @param opt
		 * opt.h 增减的小时数
		 */
		getTime:function(srcTime, opt) {
			var hour_reg = /^[\d]{2}:[\d]{2}$/;
			if (hour_reg.test(srcTime)) {
				var time = new Date('2016-01-01 ' + srcTime).getTime();
				if (opt.h !== 'undefined') {
					time += opt.h * 3600 * 1000;
				}
				var target_time = new Date(time);
				var target_hour = target_time.getHours();
				target_hour = target_hour >= 10 ? target_hour : '0'+target_hour;
				var target_min = target_time.getMinutes();
				target_min = target_min >= 10 ? target_min : '0'+target_min;
				return target_hour + ':' + target_min;
			}
			return srcTime;
		},
		/**
		 * 获取随机字串
		 */
		getUuid: function(){
		    var s = [];
		    var hexDigits = "0123456789abcdef";
		    for (var i = 0; i < 36; i++) {
		        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
		    }
		    s[14] = "4";
		    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
		    s[8] = s[13] = s[18] = s[23];
		    var uuid = s.join("");
		    return uuid;
		},
		//ajax 二次封装
		ajaxJson : function (obj,noJSon) {
			//触发控件disable
			if(obj.btn &&　obj.btn.length > 0){
				opt.btn.attr('disabled', 'disabled');
			}
			if(noJSon){
				if(obj.data && typeof(obj.data.data) == 'object'){
					obj.data.data = JSON.parse(JSON.stringify(obj.data.data));
				}
			} else {
				//统一字符串提交
				if(obj.data && typeof(obj.data.data) == 'object'){
				obj.data.data = JSON.stringify(obj.data.data);
			}
			}
			
			
			
    		var j = $.extend({},{
    			url:"",
    			data:"",
    			callback:function(){},
    			context:"",
    			type:"POST",
    			dataType:"json",
    			async:true
    		},obj);

		    $.ajax(j).done(function (result, status, e){
		    	//触发控件禁用disable
		    	if(obj.btn &&　obj.btn.length > 0){
		    		obj.btn.removeAttr('disabled');
				}
		    	
		        if (typeof j.callback === "function") {
		            j.callback(result, true, status, e, obj);
		            return true;
		        }
		    }).fail(function (jqXHR, status, e) {
		    	//触发控件禁用disable
		    	if(obj.btn &&　obj.btn.length > 0){
		    		obj.btn.removeAttr('disabled');
				}
		    	var failResult = {};
		    	failResult.jqXHR = jqXHR;
		    	failResult.errcode = '-99999';
		    	failResult.errmsg = '请求失败';
		    	
		    	if (typeof j.callback === "function") {
		            j.callback(failResult,false, status, e, obj);
		            return false;
		        }
		    });
		},
		//金额千分位分割
		formatMoney:function(num){
			
		    var f = num.toString().split('.');
		    var l = '',r = '';
		    if(f.length > 1){
		        r = f[1];
		    }
		    var f_str = f[0];
		    var l = (f_str || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');

		    if(f.length > 1){
		        l = l + '.' + r;
		    }

		    return l;  
		},
		//还原金额千分位分割
		restoreFormatMoney:function(s){   
			 return Number(s.replace(/[^\d\.-]/g, ""));
		}
	}
	
	return tools;
});