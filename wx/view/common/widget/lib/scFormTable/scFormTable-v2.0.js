/*
 * author:Numver
 * ver:3.0
 * 3.0 新增样式调整和参数配置，更加实用与多种情况下的表格
*/
//UMD 通用式
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery', 'common:widget/lib/tools','common:widget/lib/plugins/iscroll/build/iscroll-probe'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS之类的
        module.exports = factory(require('jquery'), require('tools'), require('common:widget/lib/plugins/iscroll/build/iscroll-probe'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory(root.jQuery, root.tools,root.IScroll);
    }
}(this, function ($, t , IScroll) {
    function ScFormTable(){
        this.option = {
            target:null,
            thead:[],
            tfoot:[],       
            tbody:[],
            pageTools:{
                target:null,
                count:0, //总数
                index:1,//当前页
                showNum:3,// 一次性显示多少个
                pageSize:15,//一页显示条数
                showPageInfo:1,//是否显示分页信息
                pageInfoTarget:null//分页信息所在obj
            },
            fixFirst:1,//固定第一行
            isApp:0, //1 是app  
            getScrollxy:null ,
            isHideThead:0, //是否隐藏头部 (生成之后执行)
            resizeTarget:0,
            scrollEvent:{
            	target:null,
                status:0,
                scrollTop:100 //app默认是该项的1/10
            },
            minTdWidth:0,
            resizeHeight:1, //自动调整高度 0 1 full
            theadEvent:1, //是否有排序事件
            theadThClass:"", //仅局限于thead th 的样式
            theadThDivClass:"",//仅局限于thead th 中div的样式
            tbodyTdClass:"", //仅局限于tbody td 的样式
            tbodyTdDivClass:"", //仅局限于tbody td div 中的样式
            hasBorder:0, //是否有边框
            fadeScrollbars : 0, //滚动石头隐藏滚动条
            noOddEvenClass:0, //表格间隔样式去掉
            callback:{
                finishCallBack:null //表格完成回调
            }
        };

        this.firstWidth = [];
    };

    ScFormTable.prototype.init = function(params){
        if(!params || !t.checkJqObj(params.target)){
            console.error("请检查配置!!!");
            return;
        }

        if(!t.isArray(params.tbody)){
            console.error("请检查表格数据!!!");
            return;
        }

        params = $.extend(true, this.option, params);

        this._addTable(params);

        if(params.callback && params.callback.finishCallBack && t.isFunction(params.callback.finishCallBack)){
            params.callback.finishCallBack.apply(this,[this,params]);
        }

        return this;
    };

    ScFormTable.prototype._addTable = function(option){
        var target = option.target,
        thead = option.thead,
        tfoot = option.tfoot,
        tbody = option.tbody;

        target.addClass('scFormTable');
        if(option.isApp){
            target.addClass('scFormTable app');
        }
        target.html("");

        this.fixFirst = option.fixFirst;
        this.isApp = option.isApp;
        this.isHideThead = option.isHideThead;
        this.resizeTarget = option.resizeTarget;
        this.scrollEvent = option.scrollEvent;
        this.minTdWidth = option.minTdWidth;
        this.resizeHeight = option.resizeHeight;

        //-----
        this.theadThClass = option.theadThClass;
        this.theadThDivClass = option.theadThDivClass;
        this.tbodyTdClass = option.tbodyTdClass;
        this.tbodyTdDivClass = option.tbodyTdDivClass;
        this.hasBorder = option.hasBorder;
        this.fadeScrollbars = option.fadeScrollbars;
        this.noOddEvenClass = option.noOddEvenClass;

        //表头
        if(t.isArray(option.thead) && option.thead.length > 0){
            target.prepend(this._getTheadHtml(thead));
            this.thead = thead;
        }

        //表内容
        target.append(this._getTbodyHtml(tbody));

        //底部
        if(t.isArray(option.tfoot) && option.tfoot.length > 0){
            target.append(this._getTfootHtml(tfoot));
        }

        //分页菜单
        var pageTools = option.pageTools;
        if(pageTools && pageTools.target && parseInt(pageTools.index) >= 1 && parseInt(pageTools.count) >= 1 && parseInt(pageTools.index) <= parseInt(pageTools.count)){
            this._getPageToolsHtml(pageTools);
            this._bindPageTools(pageTools);
        }
        
        this._resizeTable(target);
        this.bindDom(target,option);
    };

    ScFormTable.prototype._getTheadHtml = function(thead){
        var boder = this.hasBorder ? "hasBorder" : "";

        var html = new Array();
        html.push("<table class='scTable common-table cth tTable "+boder+"'>");

        this._getColHtml(html,thead.length);

        html.push("<thead>");
        html.push("<tr>");
        for (var i = 0; i < thead.length; i++) {
            var d = thead[i];
            if(i == 0){
                this.clt = d;
            }

            html.push("<th data-disable='"+(d.disable || "")+"' data-name='"+d.value+"' data-sort='0' data-key='"+(d.key || "")+"' class='"+(d.thClass || "") +" " +this.theadThClass+"'>");
            html.push(this._getValHtml(d.value,d.formatter,0,(d.divClass || "") +" " + this.theadThDivClass,d));
            html.push("</th>");
        };
        html.push("</tr>");
        html.push("</thead>");
        html.push("</table>");

        if(this.fixFirst){
            //*/左上角
            html.push("<table class='scTable common-list clt "+boder+"'>");

            this._getColHtml(html,1);

            html.push("<thead>");
            html.push("<th data-disable='"+(this.clt.disable || "")+"' data-name='"+this.clt.value+"' data-sort='0' data-key='"+(this.clt.key || "")+"' class='"+(this.clt.thClass || "") +" " +this.theadThClass+"'>");
            html.push(this._getValHtml(this.clt.value,this.clt.formatter,0,(this.clt.divClass || "") +" " + this.theadThDivClass,d));
            html.push("</th>");
            html.push("</thead>");
            html.push("</table>");
            //*/
        }

        return html.join('');
    };

    ScFormTable.prototype._getTfootHtml = function(tfoot){
        var html = new Array();
        html.push("<table class='scTable common-table ctf tTable'>");

        this._getColHtml(html,tfoot.length);

        html.push("<tfoot>");
        html.push("<tr>");
        for (var i = 0; i < tfoot.length; i++) {
            if(i == 0){
                this.clb = tfoot[i].value;
            }

            html.push("<th>");
            html.push(this._getValHtml(tfoot[i].value));
            html.push("</th>");
        };
        html.push("</tr>");
        html.push("</tfoot>");
        html.push("</table>");

        if(this.fixFirst){
            //*左下角
            html.push("<table class='scTable common-list clb'>");

            this._getColHtml(html,1);

            html.push("<tfoot>");
            html.push("<th>");
            html.push(this._getValHtml(this.clb));
            html.push("</th>");
            html.push("</tfoot>");
            html.push("</table>");
            //*/
        }

        return html.join('');
    };

    ScFormTable.prototype._getTbodyHtml = function(tbody){
        var html = new Array();
        var tleft = new Array();

        var hasData = true;var newTbody = new Array();

        this.tbody = tbody;

        if(t.isArray(tbody) && tbody.length == 0){
            var noData = {};

            for (var z = 0; z < this.thead.length; z++) {
                noData[z] = "&nbsp;"
            };

            newTbody.push(noData);
            hasData = false;
        }

        html.push('<div class="wrapper" id="wrapper">');
        if(!hasData){
            html.push('<div class="tac fs12 c-4a lhn noData">暂无数据</div>');
        }
        html.push('<div id="scroller">');

        var boder = this.hasBorder ? "hasBorder" : "";
        var noOddEvenClass = this.noOddEvenClass ? "noOddEven" : "";

        html.push("<table class='scTable mTable "+boder+" "+noOddEvenClass+"'>");

        this._getColHtml(html,this.thead.length);

        html.push("<tbody>");

        var tb = hasData ? tbody : newTbody;

        for (var i = 0; i < tb.length; i++) {
            html.push("<tr>");
            var k = 0;
            for (var key in tb[i]){
                var d = tb[i][key];

                d = this._checkObject(d);

                if(k == 0){
                    tleft.push(d);
                }

                var v = d.value;
                var f = d.formatter;

                if(t.isArray(v)) {
                    html.push("<td class='p0 "+(d.tdClass || "") +" " + this.tbodyTdClass+"'>");
                    for(var a in v){
                        html.push(this._getValHtml(v[a] || "&nbsp;",f,0,(d.divClass || "") +" " + this.tbodyTdDivClass,d));
                    }
                }else{
                    html.push("<td class='"+(d.tdClass || "") +" " + this.tbodyTdClass+"'>");
                    html.push(this._getValHtml(v || "&nbsp;",f,0,(d.divClass || "") +" " + this.tbodyTdDivClass,d));
                }
                html.push("</td>");
                k++;
            };
            html.push("</tr>");
        };

        html.push("</tbody>");
        html.push("</table>");
        html.push("</div>");

        if(this.fixFirst){
            //*
            html.push("<table class='scTable table-left lTable "+boder+" "+noOddEvenClass+"'>");

            this._getColHtml(html,1);

            html.push("<tbody>");
            for (var j = 0; j < tleft.length; j++) {
                var ld = tleft[j];

                html.push("<tr>");

                ld = this._checkObject(ld);

                var v = ld.value;
                var f = ld.formatter;

                if(t.isArray(v)){
                    html.push("<td class='p0 "+(ld.tdClass || "") +" " +this.tbodyTdClass+"'>");
                    for(var b in v){
                        html.push(this._getValHtml(v[b] || "&nbsp;",f,0,(ld.divClass || "") +" " + this.tbodyTdDivClass,d));
                    }
                }else{
                    html.push("<td class='"+(ld.tdClass || "") +" "+this.tbodyTdClass+"'>");
                    html.push(this._getValHtml(v || "&nbsp;",f,0,(ld.divClass || "") +" " + this.tbodyTdDivClass,d));
                }
                html.push("</td>");
                html.push("</tr>");
            }
            html.push("</tbody>");
            html.push("</table>");
            html.push("</div>");
            //*/
        }

        return html.join('');
    };

    ScFormTable.prototype._getPageToolsHtml = function(pageTools){
        if(!pageTools.showNum){
            pageTools.showNum = this.option.pageTools.showNum;
        }

        if(!pageTools.pageSize){
            pageTools.pageSize = this.option.pageTools.pageSize;
        }

        if(!pageTools.target){
            pageTools.target = this.option.pageTools.target;
        }

        if(!pageTools.showPageInfo){
            pageTools.showPageInfo = this.option.pageTools.showPageInfo;
        }

        if(!pageTools.pageInfoTarget){
            pageTools.pageInfoTarget = this.option.pageTools.pageInfoTarget;
        }

        if(!pageTools.count){
            pageTools.count = this.option.pageTools.count;
        }

        if(pageTools.showNum <= 0){
            pageTools.showNum = 1;
        }

        var target = pageTools.target;
        if(!t.checkJqObj(target)){
            console.error("请检查配置!!!");
            return;
        }

        var html = new Array(),
        count = parseInt(pageTools.count), //总数
        index = parseInt(pageTools.index), //当前页数
        showNum = parseInt(pageTools.showNum), //显示个数
        pageCount = Math.ceil(count/pageTools.pageSize); //总页数

        var tbody = this.tbody;
        if(pageTools.pageInfoTarget && pageTools.showPageInfo && t.isArray(tbody) && tbody.length > 0){
            var pageSize = pageTools.pageSize;
            var n1 = pageSize*(index-1) + 1,
                n2 = pageSize*index > count ? count : pageSize*index;
            pageTools.pageInfoTarget.html('<p class="pageNumInfo tac fs12 c-4a">第<span>'+n1+'</span>~<span>'+n2+'</span>条记录,共<span>'+count+'</span>条记录</p>');
        }

        //刚好就只有一页
        if(pageTools.pageSize >= count){
            return;
        }

        //当前显示个数
        var num = showNum > pageCount ? pageCount : showNum;

        html.push('<div class="pageTools">');
        html.push('<span class="firstPage '+(index == 1 ? "lock" : "")+'" data-page="1"><<</span>');
        html.push('<span class="prePage '+(index == 1 ? "lock" : "")+'" data-page="'+(index - 1 < 1  ? 1 : index - 1)+'"><</span>');

        num = num - 1;
        var l_num =  Math.ceil(num/2),
        r_num = num - l_num;

        //倒数第一页
        if(index > 1 && pageCount - index == 1 ){
            l_num = num - 1;
        }
        //最后一页
        else if(pageCount - index == 0){
            l_num = num;
        }

        for (var j = l_num; j >= 1; j--) {
            var prev = index - j;
            if(prev > 0){
                html.push('<span data-page="'+prev+'">'+prev+'</span>');
            }else{
                console.log("===" + j);
                r_num = num - j + 1;
            }
        };

        html.push('<span data-page="'+index+'" class="current">'+index+'</span>');

        for (var k = 1; k <= r_num; k++) {
            var next = index + k;
            if(next <= pageCount){
                html.push('<span data-page="'+next+'">'+next+'</span>');
            }
        };

        html.push('<span class="nextPage '+(index == pageCount ? "lock" : "")+'" data-page="'+(index + 1 >= pageCount ? pageCount : index +1)+'">></span>');
        html.push('<span class="lastPage '+(index == pageCount ? "lock" : "")+'" data-page="'+pageCount+'">>></span>');
        html.push('</div>');

        target.html(html.join(''));
    };

    //数据外面的包裹标签
    ScFormTable.prototype._getValHtml = function(val,formatter,notUse,className,data){
        if(notUse){return val;}

        if(formatter && t.isFunction(formatter)){
            val = formatter(val,data);
        }

        className = className ? "class='"+className+"'" : "";

        return "<div "+className+">"+val+"</div>"
    };

    ScFormTable.prototype._checkObject = function(val){
        if(!val || !(typeof val === "object" && !t.isArray(val))){
            return {
                value:val
            };
        }

        return val;
    };

    ScFormTable.prototype._getColHtml = function(arr,length){
        for (var i = 0; i < length; i++) {
            arr.push("<col width='0'/>");
        };
    };

    ScFormTable.prototype._resizeTable =function(target){

        this._newResizeTable(target);

        return;
        var c_table = target.find(".common-table");
        var t_table = target.find(".cth");
        var f_table = target.find(".ctf");
        var scroller = target.find("#wrapper #scroller");
        var mTable = scroller.find(".mTable");

        var thHeight = this.isHideThead ? 0 : t_table.height();
        var tfHeight = f_table.height();

        var targetWidth = target.width();

        var pThis = this;

        /*重置表格状态*/
        scroller.css({
            "transform": "translate(0px, 0px) translateZ(0px)",
            "width":"auto"
        });
        mTable.width("auto");
        c_table.css({
            "left":0,
            "width":"auto"
        });
        target.find(".table-left").removeClass('shadow');
        target.find(".common-list").removeClass('shadow');
        target.removeAttr("style");
        /*重置表格状态*/

        this.firstWidth = [];

        target.find("#wrapper").css({
            "top":thHeight,
            "bottom":tfHeight
        });

        /*处理子表单内容高度*/
        var spanArr = {};
        mTable.find('tbody tr td.p0').each(function(){
            var o = $(this);
            var tr = o.parent();
            var tr_index = tr.index();
            var td_index = o.index;
            var span = o.find("span");
            var len = span.length;
            
            if(!spanArr[tr_index]){
                 spanArr[tr_index] = {};
            }

            if(!spanArr[tr_index][len]){
                 spanArr[tr_index][len] = {};
            }

            span.each(function(){
                var s = $(this);
                var span_index = s.index();
                var span_height = s.height();

                var save_height = spanArr[tr_index][len][span_index];

                if(!save_height){
                    spanArr[tr_index][len][span_index] = span_height;
                }else{
                    if(save_height < span_height){
                        spanArr[tr_index][len][span_index] = span_height;
                    }
                }
            });
        });

        //console.log(spanArr);
        for(var tr_key in spanArr){
            var tr_p = spanArr[tr_key];
            var tr_obj = mTable.find('tbody tr:eq('+tr_key+')');

            tr_obj.find("td.p0").each(function(){
                var td_obj = $(this);
                for(var td_key in tr_p){
                    var td_p = tr_p[td_key];
                    if(td_obj.find("span").length == td_key){
                        for(var span_key in td_p){
                             td_obj.find("span:eq("+span_key+")").height(td_p[span_key]);
                        }
                    }
                };
            });
        }

        mTable.find("tbody tr:first td").each(function(){
            var index = $(this).index();
            var w = 0;
            var width = $(this).outerWidth();
            var c_width = t_table.find("tr th:eq("+index+")").outerWidth();
            var f_width = f_table.find("tr th:eq("+index+")").outerWidth();

            w = width >= c_width ? width : c_width;

            w = w >= f_width ? w : f_width;

            if(pThis.isApp && w > 84){
                w = 84;
            }else if(!pThis.isApp && w > 120){
                w = 120;
            }

             //头部 底部
            c_table.find("tr th:eq("+index+")")
            .outerWidth(w);
            $(this).outerWidth(w);

            pThis.firstWidth.push(w);
        });

        var count = 0;
        for (var i in this.firstWidth) {
            count += this.firstWidth[i];
        };

        var trueWidth = (this.isApp && targetWidth > count) ? "100%" : count;

        scroller.width(trueWidth);
        mTable.width(trueWidth)
        c_table.width(trueWidth);

        if(this.resizeTarget){
            if(target.outerWidth() > trueWidth){
                target.outerWidth(trueWidth);
            }
        }

        var scrollerHeight = scroller.height();
        var wrapper = scroller.parent();
        if(scrollerHeight < wrapper.height()){
        	wrapper.height(scrollerHeight);
            target.height(thHeight + tfHeight + scrollerHeight);
        }else{
        	//wrapper.height(scrollerHeight);
        	target.height(wrapper.height());
        }

        if(pThis.fixFirst){
            var trueFirstWidth = mTable.find("tr:first td").outerWidth();
            target.find(".common-list").outerWidth(trueFirstWidth);
            target.find(".table-left").outerWidth(trueFirstWidth);

            mTable.find("tbody tr").each(function(){
                var index = $(this).index();

                //左侧
                target.find(".table-left").find("tr:eq("+index+")")
                .height($(this).height());
            });
        }
        
        if(this.isHideThead){
        	t_table.addClass("hide");
        	target.find(".clt").addClass("hide");
        }
    };

    ScFormTable.prototype._newResizeTable = function(target){
        target.removeClass('autoWp');
        target.find("col").removeAttr('width');

        var c_table = target.find(".common-table");
        var t_table = target.find(".cth");
        var f_table = target.find(".ctf");
        var scroller = target.find("#wrapper #scroller");
        var mTable = scroller.find(".mTable");
        var c_list = target.find(".common-list");
        var l_table = target.find(".table-left");

        var targetWidth = target.width();

        var pThis = this;

        /*重置表格状态*/
        scroller.css({
            "transform": "translate(0px, 0px) translateZ(0px)",
            "width":"auto"
        });
        mTable.width("auto");
        l_table.width("auto");
        c_list.width("auto");
        c_table.css({
            "left":0,
            "width":"auto"
        });
        
        l_table.removeClass('shadow');
        c_list.removeClass('shadow');
        target.removeAttr("style");
        /*重置表格状态*/

        this.firstWidth = [];

        var minWidth = this.minTdWidth;

        if(!t.isInteger(minWidth) || minWidth == 0){
            minWidth = this.isApp ? 120 : 180;
        }

        mTable.find("tbody tr:first td").each(function(){
            var index = $(this).index();
            var w = 0;
            var width = $(this).outerWidth();
            var c_width = t_table.find("tr th:eq("+index+")").outerWidth();
            var f_width = f_table.find("tr th:eq("+index+")").outerWidth();

            if(c_width){
                w = width >= c_width ? width : c_width;
            }

            if(f_width){
                w = w >= f_width ? w : f_width;
            }

            w = w > minWidth ? minWidth : w;

             //头部 底部
            c_table.find("col:eq("+index+")").attr("width",w);
            mTable.find("col:eq("+index+")").attr("width",w);

            if(index == 0){
                c_list.find("col:eq("+index+")").attr("width",w);
                l_table.find("col:eq("+index+")").attr("width",w);
            }

            pThis.firstWidth.push(w);
        });

        //return;

        var count = 0;
        for (var i in this.firstWidth) {
            count += this.firstWidth[i];
        };

        var trueWidth = (!this.resizeTarget && targetWidth > count) ? "100%" : count;

        scroller.outerWidth(trueWidth);
        mTable.outerWidth(trueWidth)
        c_table.outerWidth(trueWidth);

        //自动调整表格后重新获取数据
        if(targetWidth > count){
            this.firstWidth = [];
            mTable.find("tbody tr:first td").each(function(){
                pThis.firstWidth.push($(this).outerWidth());
            });
        }

        target.addClass('autoWp');

        //*
        //处理子表单内容高度
        var oArr = {};
        mTable.find('tbody tr td.p0').each(function(){
            var o = $(this);
            var tr = o.parent();
            var tr_index = tr.index();
            var td_index = o.index();
            var d = o.find("div");
            var len = d.length;
            
            if(!oArr[tr_index]){
                 oArr[tr_index] = {};
            }

            if(!oArr[tr_index][len]){
                 oArr[tr_index][len] = {};
            }

            d.each(function(){
                var s = $(this);
                var d_index = s.index();
                var d_height = s.outerHeight();

                var save_height = oArr[tr_index][len][d_index];

                if(!save_height){
                    oArr[tr_index][len][d_index] = d_height;
                }else{
                    if(save_height < d_height){
                        oArr[tr_index][len][d_index] = d_height;
                    }
                }
            });
        });

        //console.log(oArr);
        //*
        for(var tr_key in oArr){
            var tr_p = oArr[tr_key];
            var tr_obj = mTable.find('tbody tr:eq('+tr_key+')');

            tr_obj.find("td.p0").each(function(){
                var td_obj = $(this);
                for(var td_key in tr_p){
                    var td_p = tr_p[td_key];
                    if(td_obj.find("div").length == td_key){
                        for(var d_key in td_p){
                             td_obj.find("div:eq("+d_key+")").outerHeight(td_p[d_key]);
                        }
                    }
                };
            });
        }
        //*/

        var thHeight = this.isHideThead ? 0 : t_table.height();
        var tfHeight = f_table.height();

        target.find("#wrapper").css({
            "top":thHeight,
            "bottom":tfHeight
        });

        if(this.resizeTarget){
            if(target.outerWidth() > trueWidth){
                target.outerWidth(trueWidth);
            }
        }

        var scrollerHeight = scroller.height();
        var wrapper = scroller.parent();

        if(this.resizeHeight == 1){
            if(scrollerHeight < wrapper.height()){
                wrapper.height(scrollerHeight);
                target.height(thHeight + tfHeight + scrollerHeight);
            }else{
                //wrapper.height(scrollerHeight);
                target.height(wrapper.height());
            }
        }else if(this.resizeHeight == "full"){
            if(mTable.height() < wrapper.height()){
                wrapper.outerHeight("calc(100% - "+(thHeight+2)+"px)");
                scroller.outerHeight("100%")
                mTable.outerHeight("100%");
            }else{
                mTable.find("tr div").height(mTable.height());
            }

            if(this.fixFirst){
                l_table.find("tr div").outerHeight(mTable.height());
            }
        }

        if(this.fixFirst){
            var trueFirstWidth = mTable.find("tr:first td").outerWidth();
            c_list.outerWidth(trueFirstWidth);
            l_table.outerWidth(trueFirstWidth);

            target.find(".clt").find("tr").height(thHeight + 1);
            target.find(".clb").find("tr").height(tfHeight);

            mTable.find("tbody tr").each(function(){
                var index = $(this).index();

                //左侧
                l_table.find("tr:eq("+index+")")
                .height($(this).height());
            });
        }
        
        if(this.isHideThead){
            t_table.addClass("hide");
            target.find(".clt").addClass("hide");
        }
    };

    ScFormTable.prototype.bindDom = function(target,option){
        var pThis = this;

        this.left = 0,this.top = 0;

        if(option.theadEvent){
            target.find(".cth,.clt").on("click","thead tr th",function(){
                var o = $(this);
                var name = o.attr("data-name");
                var sort = o.attr("data-sort");
                var key = o.attr("data-key");
                var disable = o.attr("data-disable");

                if(disable){return;}

                if(option.theadClick && t.isFunction(option.theadClick)){
                    option.theadClick(o,name,sort,key);
                }

                target.find(".cth").find("th:not(:eq("+o.index()+"))").attr("data-sort",0);
                if(o.index() > 0){
                    target.find(".clt th").attr("data-sort",0);
                }

                target.find(".cth th i").remove();
                target.find(".clt th i").remove();

                var i_obj = o.find("div").find("i");

                o.find("div").append("<i class='"+(sort == 0 ? "rulericon-triangle_up":"rulericon-triangle_down")+"'></i>");
                
                o.attr("data-sort",sort == 0 ? 1 : 0);
            });
        }

        this._bindIScroll(target,option);
    };

    ScFormTable.prototype._bindIScroll = function(target,option){
        if( this.myScroll){
             this.myScroll.destroy();
        }
        this.myScroll = null;

        var wrapper = target.find('#wrapper')[0];
        var scroll = target.find("#wrapper #scroller");

        var isApp = this.isApp,
        scrollEvent = this.scrollEvent,
        status = scrollEvent.status,
        scrollTop = scrollEvent.scrollTop,
        scrollTarget = scrollEvent.target || $(window),
        pThis = this;

        var fadeScrollbars = this.fadeScrollbars;

        var params = { 
            probeType: 3,
            bounce:false,
            scrollX: true, 
            //scrollbars: 'custom',

            scrollbars: true,
            shrinkScrollbars:"scale",
            mouseWheel: true,
            fadeScrollbars:fadeScrollbars
        };

        if(!isApp){
            params.interactiveScrollbars = true; //滚动条反向(默认是app模式)
        }

        this.myScroll = new IScroll(wrapper, params);

        var max = scroll.height() - scroll.parent().height();
        var w = scrollTarget;
        var p_w = scrollEvent.target ? w : $("body");

        this.myScroll.on("scroll",function(){
            var x = this.x,y = this.y;

            target.find(".lTable").css("top",y);

            target.find(".common-table").css("left",x);

            if(x < 0){
                target.find(".table-left").addClass('shadow');
                target.find(".common-list").addClass('shadow');
            }else{
                target.find(".table-left").removeClass('shadow');
                target.find(".common-list").removeClass('shadow');
            }

            if(option && t.isFunction(option.getScrollxy)){
                option.getScrollxy(x,y,target,max);
            }
        });

        var s_x = 0,s_y = 0;
        this.myScroll.on("scrollEnd",function(){
            var x = this.x,y = this.y;

            if(status == 1 && scrollTop > 0 && max > 0 && s_x == x){
                var w_t = w.scrollTop();
                if(y + max == 0){
                    //w.scrollTop(w.scrollTop() + scrollTop);

                	p_w.clearQueue().animate({
                        "scrollTop": w_t + scrollTop + "px"
                    },100);

                }else if(y  == 0 ){
                    //w.scrollTop(w.scrollTop() - scrollTop);

                	p_w.clearQueue().animate({
                        "scrollTop": w_t - scrollTop + "px"
                    },100);
                }
            }

            s_x = x;
            s_y = y;
        });

        var c = target.width() - scroll.width();
        if(c > 0){
            target.find(".iScrollVerticalScrollbar ")
            .css("right",c);
        }

        if(max > 0){
            wrapper.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        }else if(status == 1 && scrollTop > 0 && max <=0){
            function scrollFunc(e) {
                deltaY = e.deltaY;

                if(deltaY > 0){
                    //w.scrollTop(w.scrollTop() + 100);
                	p_w.clearQueue().animate({scrollTop:w.scrollTop() + scrollTop},200);
                }else if(deltaY < 0){
                    //w.scrollTop(w.scrollTop() - 100);
                	p_w.clearQueue().animate({scrollTop:w.scrollTop() - scrollTop},200);
                }
            }
            
            if(!isApp){
                 /*注册事件*/
                if(document.addEventListener){
                    wrapper.addEventListener('DOMMouseScroll',scrollFunc,false);
                }//W3C
                target[0].onmousewheel=scrollFunc;//IE/Opera/Chrome/Safari
            }else{
                var startX = 0,startY = 0,moveEndX = 0,moveEndY = 0;

                target.find('#wrapper').on("touchstart",function(e){
                    e.preventDefault();
                    var touch = e.originalEvent.targetTouches[0]; 
                    var y = touch.pageY;

                    startX = e.originalEvent.changedTouches[0].pageX,
                    startY = e.originalEvent.changedTouches[0].pageY;
                });

                target.find('#wrapper').on("touchmove",function(e){
                    e.preventDefault();
                    var touch = e.originalEvent.targetTouches[0]; 
                    var y = touch.pageY;
                    
                    moveEndX = e.originalEvent.changedTouches[0].pageX,
                    moveEndY = e.originalEvent.changedTouches[0].pageY,
                    X = moveEndX - startX,
                    Y = moveEndY - startY;
                     
                    if ( Math.abs(X) > Math.abs(Y) && X > 0 ) {
                        //alert("left 2 right");
                    }
                    else if ( Math.abs(X) > Math.abs(Y) && X < 0 ) {
                        //alert("right 2 left");
                    }
                    else if ( Math.abs(Y) > Math.abs(X) && Y > 0) {
                        //alert("top 2 bottom");
                        w.scrollTop(w.scrollTop() - scrollTop/10);
                    }
                    else if ( Math.abs(Y) > Math.abs(X) && Y < 0 ) {
                        //alert("bottom 2 top");
                        w.scrollTop(w.scrollTop() + scrollTop/10);
                    }
                    else{
                        //alert("just touch");
                    }
                });
            } 
        }
    };

    ScFormTable.prototype.restartTbody = function(target,tbody,option){
        var html = new Array();
        var tbody =  this._getTbodyHtml(tbody);

        target.find("#wrapper").remove();
        target.append(tbody);
        this._resizeTable(target);
        this._bindIScroll(target,option);
    };

    ScFormTable.prototype.restartPageTools = function(pageTools){
        this._getPageToolsHtml(pageTools);
        //this._bindPageTools(pageTools);
    };

    ScFormTable.prototype._bindPageTools = function(pageTools){
        var pThis = this;
        pageTools.target.on("click","span",function(){
            var num = $(this).attr("data-page");

            if($(this).hasClass("lock")){return;}

            $(this).addClass('current').siblings().removeClass('current');

            if(!t.isFunction(pageTools.buttonClick)){return;}
            pageTools.buttonClick(num,pageTools);

            //pageTools.index = parseInt(num); 
            //pThis.restartPageTools(pageTools);
        });
    };

    ScFormTable.prototype.getFirstWidth = function(){
        return this.firstWidth;
    };
 
    return ScFormTable;
}));