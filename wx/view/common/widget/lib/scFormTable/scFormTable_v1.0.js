//UMD 通用式
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery', 'common:widget/lib/tools','common:widget/lib/plugins/iscroll/build/iscroll-probe'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS之类的
        module.exports = factory(require('jquery'), require('tools'), require('common:widget/lib/plugins/iscroll/build/iscroll-probe'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory(root.jQuery, root.tools,root.IScroll);
    }
}(this, function ($, t , IScroll) {
    var scFormTable = {
    	data:null,
    	option:{
            target:null,
            thead:[],
            tfoot:[],		
            tbody:[],
            pageTools:{
            	target:null,
            	count:0, //总数
            	index:1,//当前页
            	showNum:3// 一次性显示多少个
            },
            isApp:1 //1 是app	
        },
        init:function(params){
            if(!params || !t.checkJqObj(params.target)){
                console.error("请检查配置!!!");
                return;
            }

            if(!t.isArray(params.thead) || params.thead.length == 0){
                console.error("请检查表头数据!!!");
                return;
            }

            if(!t.isArray(params.tbody) || params.tbody.length == 0){
                console.error("请检查表格数据!!!");
                return;
            }

            params = $.extend(true, this.option, params);

            this._addTable(params);

            return this;
        },
        //添加表格内容
        _addTable:function(option){
        	var target = option.target,
        	thead = option.thead,
        	tfoot = option.tfoot,
        	tbody = option.tbody;

        	target.addClass('scFormTable');
            if(option.isApp){
                target.addClass('scFormTable app');
            }
            target.html("");

            //表头
            target.prepend(this._getTheadHtml(thead));

            //表内容
        	target.append(this._getTbodyHtml(tbody));

            //底部
            if(t.isArray(option.tfoot) && option.tfoot.length > 0){
                target.append(this._getTfootHtml(tfoot));
            }

            //分页菜单
            var pageTools = option.pageTools;
            if(pageTools && pageTools.target && parseInt(pageTools.index) >= 1 && parseInt(pageTools.count) >= 1 && parseInt(pageTools.index) <= parseInt(pageTools.count)){
                this._getPageToolsHtml(pageTools);
                this._bindPageTools(pageTools);
            }
            
        	this._resizeTable(target);
        	this.bindDom(target,option);
        },
        _getTheadHtml:function(thead){
            var html = new Array();
            html.push("<table class='scTable common-table cth tTable'>");
            html.push("<thead>");
            html.push("<tr>");
            for (var i = 0; i < thead.length; i++) {
                if(i == 0){
                    this.clt = thead[i].value;
                }

                html.push("<th data-name='"+thead[i].value+"' data-sort='0'>");
                html.push(thead[i].value);
                html.push("</th>");
            };
            html.push("</tr>");
            html.push("</thead>");
            html.push("</table>");

            //*/左上角
            html.push("<table class='scTable common-list clt'>");
            html.push("<thead>");
            html.push("<th data-name='"+this.clt+"' data-sort='0'>");
            html.push(this.clt);
            html.push("</th>");
            html.push("</thead>");
            html.push("</table>");
            //*/

            return html.join('');
        },
        _getTfootHtml:function(tfoot){
            var html = new Array();
            html.push("<table class='scTable common-table ctf tTable'>");
            html.push("<tfoot>");
            html.push("<tr>");
            for (var i = 0; i < tfoot.length; i++) {
                if(i == 0){
                    this.clb = tfoot[i].value;
                }

                html.push("<th>");
                html.push(tfoot[i].value);
                html.push("</th>");
            };
            html.push("</tr>");
            html.push("</tfoot>");
            html.push("</table>");

            //*左下角
            html.push("<table class='scTable common-list clb'>");
            html.push("<tfoot>");
            html.push("<th>");
            html.push(this.clb);
            html.push("</th>");
            html.push("</tfoot>");
            html.push("</table>");
            //*/

            return html.join('');
        },
        _getTbodyHtml:function(tbody){
            var html = new Array();
            var tleft = new Array();

            html.push('<div id="wrapper">');
            html.push('<div id="scroller">');
            html.push("<table class='scTable mTable'>");
            html.push("<tbody>");

            for (var i = 0; i < tbody.length; i++) {
                html.push("<tr>");
                var k = 0;
                for (var key in tbody[i]){
                    var d = tbody[i][key];
                    if(k == 0){
                        tleft.push(d);
                    }

                    if(typeof d === "object"){
                        html.push("<td class='p0'>");
                        for(var a in d){
                            html.push("<span>"+d[a]+"</span>");
                        }
                    }else{
                        html.push("<td>");
                        html.push(d);
                    }
                    html.push("</td>");
                    k++;
                };
                html.push("</tr>");
            };

            html.push("</tbody>");
            html.push("</table>");
            html.push("</div>");
            //*
            html.push("<table class='scTable table-left lTable'>");
            html.push("<tbody>");
            for (var j = 0; j < tleft.length; j++) {
                var ld = tleft[j];

                html.push("<tr>");
                if(typeof ld === "object"){
                    html.push("<td class='p0'>");
                    for(var b in ld){
                        html.push("<span>"+ld[b]+"</span>");
                    }
                }else{
                    html.push("<td>");
                    html.push(ld);
                }
                html.push("</td>");
                html.push("</tr>");
            }
            html.push("</tbody>");
            html.push("</table>");
            html.push("</div>");
            //*/

            return html.join('');
        },
        _getPageToolsHtml:function(pageTools){
            var target = pageTools.target;
            if(!t.checkJqObj(target)){
                console.error("请检查配置!!!");
                return;
            }

            if(!pageTools.showNum){
                pageTools.showNum = this.option.pageTools.showNum;
            }

            var html = new Array(),count = pageTools.count,index = pageTools.index,showNum = pageTools.showNum;

            var num = showNum > count ? count : showNum;

            html.push('<div class="pageTools">');
            html.push('<span class="firstPage '+(index == 1 ? "lock" : "")+'" data-page="1"><<</span>');
            html.push('<span class="prePage '+(index == 1 ? "lock" : "")+'" data-page="'+(index - 1 < 1  ? 1 : index - 1)+'"><</span>');

            if(index == 1){
                html.push('<span data-page="1" class="current">1</span>');

                for (var i = 1; i < showNum; i++) {
                    html.push('<span data-page="'+(index + i)+'">'+(index + i)+'</span>');
                };
            }

            if(index > 1 && index < count){
                var l_num =  Math.ceil(num/2),
                r_num = num - l_num;

                console.log(l_num);

                for (var i = l_num - 1; i >= 1; i--) {
                    if(index - i > 0){
                        html.push('<span data-page="'+(index - i)+'">'+(index - i)+'</span>');
                    }
                };

                html.push('<span data-page="'+index+'" class="current">'+index+'</span>');

                for (var i = 1; i <= r_num; i++) {
                    if(index + i <= count){
                        html.push('<span data-page="'+(index + i)+'">'+(index + i)+'</span>');
                    }
                };
            }

            if(index == count){
                for (var i = showNum - 1; i >= 1 ; i--) {
                    html.push('<span data-page="'+(count - i)+'">'+(count - i)+'</span>');
                };

                html.push('<span data-page="'+count+'" class="current">'+count+'</span>');
            }

            html.push('<span class="nextPage '+(index == count ? "lock" : "")+'" data-page="'+(index + 1 >= count ? count : index +1)+'">></span>');
            html.push('<span class="lastPage '+(index == count ? "lock" : "")+'" data-page="'+count+'">>></span>');
            html.push('</div>');

            target.html(html.join(''));
        },
        _resizeTable:function(target){
            var thHeight = target.find(".cth").height();
            var tfHeight = target.find(".ctf").height();
            var tbWidth = target.find("#wrapper #scroller").width();

            target.find(".common-table").width(tbWidth);

            target.find("#wrapper").css({
                "top":thHeight,
                "bottom":tfHeight
            });

            target.find("#wrapper #scroller").css("width",tbWidth);

            target.find(".mTable tbody").find("tr:first td").each(function(){
                var index = $(this).index();

                if(index == 0){
                    target.find(".common-list")
                    .outerWidth($(this).outerWidth());
                }

                //头部 底部
                target.find(".common-table").find("tr th:eq("+index+")")
                .outerWidth($(this).outerWidth());
            });

            target.find(".mTable tbody").find("tr").each(function(){
                var index = $(this).index();

                //左侧
                target.find(".table-left").find("tr:eq("+index+")").height($(this).height());
            });
        },
        bindDom:function(target,option){
            var pThis = this;

            this.left = 0,this.top = 0;

            target.find(".cth,.clt").on("click","thead tr th",function(){
                var name = $(this).attr("data-name");
                var sort = $(this).attr("data-sort");

                if(option.theadClick /*&& t.isFunction(option.theadClick)*/){
                    option.theadClick($(this),name,sort);
                }

                $(this).attr("data-sort",sort == 0 ? 1 : 0);
            });

            this._bindIScroll(target);
        },
        _bindIScroll:function(target){
            this.myScroll = null;

            this.myScroll = new IScroll('#wrapper', { 
                probeType: 3,
                bounce:false,
                scrollX: true, 
                //scrollbars: 'custom',

                scrollbars: true,
                shrinkScrollbars:"scale",
                mouseWheel: true,
                fadeScrollbars:true
             });

            this.myScroll.on("scroll",function(){
                target.find(".lTable").css("top",this.y);

                target.find(".common-table").css("left",this.x);

                if(this.x < 0){
                    target.find(".table-left").addClass('shadow');
                    target.find(".common-list").addClass('shadow');
                }else{
                    target.find(".table-left").removeClass('shadow');
                    target.find(".common-list").removeClass('shadow');
                }
            });

            var c = target.width() - target.find("#wrapper #scroller").width();
            if(c > 0){
                target.find(".iScrollVerticalScrollbar ")
                .css("right",c);
            }

            target[0].addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
        },
        restartTbody:function(target,tbody){
            var html = new Array();
            var tbody =  this._getTbodyHtml(tbody);

            target.find("#wrapper").remove();
            target.append(tbody);
            this._resizeTable(target);
            this._bindIScroll(target);
        },
        restartPageTools:function(pageTools){
            this._getPageToolsHtml(pageTools);
            //this._bindPageTools(pageTools);
        },
        _bindPageTools:function(pageTools){
            var pThis = this;
            pageTools.target.on("click","span",function(){
                var num = $(this).attr("data-page");

                if($(this).hasClass("lock")){return;}

                $(this).addClass('current').siblings().removeClass('current');

                if(!t.isFunction(pageTools.buttonClick)){return;}
                pageTools.buttonClick(num,pageTools);

                pageTools.index = parseInt(num); 

                pThis.restartPageTools(pageTools);
            });
        }
    };
 
    return scFormTable;
}));