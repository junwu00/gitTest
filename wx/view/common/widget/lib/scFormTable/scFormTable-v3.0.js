/*
 * author:Numver
 * ver:3.0
 * 3.0 新增样式调整和参数配置，更加实用与多种情况下的表格
*/
//UMD 通用式
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery', 'common:widget/lib/tools','common:widget/lib/plugins/iscroll/build/iscroll-probe','common:widget/lib/plugins/juicer', 'common:widget/lib/text!' + buildView + '/common/widget/lib/scFormTable/form-dailog.html','common:widget/lib/plugins/prePlugin/prePlugin'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS之类的
        module.exports = factory(require('jquery'), require('tools'), require('common:widget/lib/plugins/iscroll/build/iscroll-probe'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory(root.jQuery, root.tools,root.IScroll);
    }
}(this, function ($, t , IScroll, tpl, formDialogTmp,prePlugin) {
    function ScFormTable(){
        var pThis = this;
        this.maxTdWidth = 180;
        this.minTdWidth = 128;
        this.columnWidth = []; //每一列宽度
        //预留选项
        this.scrollEvent = {
            target:null,
            status:0,
            scrollTop:100 //app默认是该项的1/10
        };
        this.option = {
            target:null,
            //数据层
            data:{
                thead:[],
                tfoot:[],       
                tbody:[]
            },
            //基础配置
            base:{
                fixRowNum:1,//固定行
                fixColNum:1,//固定列数
                isApp:0, //1 是app  
                hideThead:0, //是否隐藏头部 (生成之后执行)
                maxTdWidth:pThis.maxTdWidth, //最大ＴＤ宽度
                minTdWidth:pThis.minTdWidth, //最小ＴＤ宽度
                autoResize:"auto", //自动调整 auto 宽度高度调整 w 宽度 h高度 no 不调整
                theadEvent:1, //是否有排序事件
                mergeTable:1, //是否合并表格

                fadeScrollbars:0, //是否自动隐藏滚动条 （影响插件）
                showScrollbars:1, //是否显示滚动条
                checkBox:{//是否有复选框
                    state:0,
                    width:"54",
                    className:""
                },
                limitTdWidth:1, //是否限制最大TD宽度
                noDataVal:"--"
            },
            //样式
            css:{
                theadTrClass:"", //仅局限于thead 全部tr 的样式
                tfootTrClass:"", //仅局限于thead 全部tr 的样式
                tbodyTrClass:"", //仅局限于tbody 全部tr 的样式

                theadThClass:"", //仅局限于thead 全部th 的样式
                tfootThClass:"", //仅局限于thead 全部th 的样式
                tbodyTdClass:"", //仅局限于tbody 全部td 的样式

                hasBorder:0, //是否有边框
                noOddEvenClass:0, //表格间隔样式去掉
            },
            //分页
            pageTools:{
                target:null,
                count:0, //总数
                index:1,//当前页
                showNum:3,// 一次性显示多少个
                pageSize:15,//一页显示条数
                showPageInfo:1,//是否显示分页信息
                pageInfoTarget:null//分页信息所在obj
            },
            //回调函数
            callback:{
                getScrollxy:null , // 获取滚动条数值
                initCallBack:null, //表格完成回调
                theadClick:null, //表头点击回调
                checkBoxClick:null //复选框回调
            }
        };
        
        this.hasRowMerge = 0; //是否存在列合并
    };

    //初始化
    ScFormTable.prototype.init = function(params){
        var target = params.target;
        var data = params.data;
        var thead = data.thead;
        var tbody = data.tbody;
        var tfoot = data.tfoot;

        if(!params || !t.checkJqObj(target)){
            console.error("请检查配置!!!");
            return;
        }

        if(!t.isArray(tbody)){
            console.error("请检查表格数据!!!");
            return;
        }

        params = $.extend(true, this.option, params);
        this.params = params;
        this.tbodyData = tbody;
        this.theadData = thead;
        this.tfootData = tfoot;

        var base = params.base;
        var callback = params.callback;

        this._addTable(params);

        //自动调整表单
        this._resizeTable(target,base);

        //是否合并
        if(base.mergeTable){
            this._mergeTable(target.find(".mTable"));
        }

        var flag = false;

        if(t.isArray(tbody) && tbody.length > 0){
            flag = true;
        }

        //需要在合并之前隐藏
        if(base.hideThead){
            target.find("thead").addClass('hide');
        }
        
        //是否固定列 行不固定默认全部不固定
        if(flag && base.fixRowNum){
            this._fixTable(target,base);
        }

        //分页菜单
        var pageTools = params.pageTools;
        if(pageTools && pageTools.target && parseInt(pageTools.index) >= 1 && parseInt(pageTools.count) >= 1 && parseInt(pageTools.index) <= parseInt(pageTools.count)){
            this._getPageToolsHtml(pageTools);
            this._bindPageTools(pageTools);
        }

        if(flag){
            this._bindDom(target,base,callback);
        }

        this._removeHideSpan(target);

        //自动调节高度
        if(!flag){
            this._autoResizeHeight(target);
        }

        if(callback && callback.initCallBack && t.isFunction(callback.initCallBack)){
            callback.initCallBack.apply(this,[this,params]);
        }

        //重新刷新滚动条
        if(this.myScroll){
            this.myScroll._execEvent('scrollEnd')
        }
        return this;
    };

    //初始化生成表单
    ScFormTable.prototype._addTable = function(option){
        var target = option.target,
            base = option.base,
            css = option.css,
            thead = option.data.thead,
            tfoot = option.data.tfoot,
            tbody = option.data.tbody;

        target.addClass('scFormTable');
        if(base.isApp){
            target.addClass('app');
        }
        if(!base.limitTdWidth){
            target.addClass('limitTdWidth')
        }
        target.html("");

        var checkBox = base.checkBox,
            state = checkBox.state;
        if(state && base.fixColNum !=0){
            base.fixColNum ++;
        }

        //表内容
        target.append(this._getTableHtml(thead,tbody,tfoot,css));
    };

    //数据formatter处理
    ScFormTable.prototype._getFormatter = function(val,formatter,data){
        if(formatter && t.isFunction(formatter)){
            val = formatter(data);
        }else{
            val = val ? val : this.params.base.noDataVal;
        }

        // 换行转换
        val = val.replace(/\n/g,'<br/>')

        return val;
    };

    //根据表格标签类型获取HTML
    ScFormTable.prototype._getTablePartHtml = function(data,css,type,newAdd){
        var way = "",span = "",wayClass = "",trClass = "";

        if(type == "thead" || type == "tfoot"){
            way = type;
            span = "th";

            if(type == "thead"){
                wayClass = css.theadThClass;
                trClass = css.theadTrClass;
             }else{
                wayClass = css.tfootThClass;
                trClass = css.tfootTrClass;
             }
        }else{
            way = "tbody";
            span = "td";
            wayClass = css.tbodyTdClass;
            trClass = css.tbodyTrClass;
        }

        var html = new Array();

        var base = this.params.base,
            checkBox = base.checkBox;

        var theadData = this.theadData;

        html.push("<"+type+">");
        for (var i = 0; i < data.length; i++) {
            var tr = data[i];
            var tr_data = tr.data;

            html.push("<tr class='"+(tr.className || "")+" "+trClass+"' "+(newAdd ? "data-new='1'": "")+">");
            for (var j = 0; j < tr_data.length; j++) {
                var d = tr_data[j];
                var val = d.val;
                var key = d.key;
                var sort = d.sort;
                var d_type = d.type;
                var f = d.formatter;

                if(theadData && theadData.length > 0){
                    try{
                        var theadType = this.theadData[theadData.length - 1].data[j].type;

                        d_type = theadType || d_type;
                    }catch(e){
                        console.log(e);
                    }
                }else{
                    d_type = "";
                }

                if(checkBox.state && j == 0){
                    if(type != "tfoot"){
                        html.push("<"+span+" data-key='"+key+"_cb' class='hasCb h-center "+(checkBox.className || "") +" " + wayClass+"'>");
                        //html.push('<input type="checkbox" name="'+key+'_cb" value="111"/>');
                        html.push('<span class="scCheckBox"></span>');
                        html.push("</"+span+">");
                    }else{
                        html.push("<"+span+" data-key='"+key+"' class='"+(d.className || "") +" " + wayClass+"'>");
                        html.push(this._getFormatter(val,f,d));
                        html.push("</"+span+">");
                    }
                }

                html.push("<"+span+" "+(sort != undefined ? "data-click='1' data-sort='"+sort+"' data-val='"+val+"'" : "")+" data-key='"+key+"' class='"+(d.className || "") +" " + wayClass+" "+ (d_type == 'number' ? 'h-right' : '') + "'>");
                html.push(this._getFormatter(val,f,d));
                html.push("</"+span+">");
            };
            html.push("</tr>");
        };
        html.push("</"+type+">");

        return html.join('');
    };

    //数据表内容
    ScFormTable.prototype._getTableHtml = function(thead,tbody,tfoot,css){
        var html = new Array();

        html.push('<div class="wrapper" id="wrapper">');
        html.push('<div id="scroller">');
        var boder = css.hasBorder ? "hasBorder" : "";
        var noOddEvenClass = css.noOddEvenClass ? "noOddEven" : "";

        html.push("<table class='scTable mTable "+boder+" "+noOddEvenClass+"'>");

        if(t.isArray(thead) && thead.length > 0){
            html.push(this._getTablePartHtml(thead,css,"thead"));
        }

        if(t.isArray(tbody) && tbody.length == 0){
            tbody = this._getNoData(thead,tbody);
        }

        html.push(this._getTablePartHtml(tbody,css,"tbody"));

        if(t.isArray(tfoot) && tfoot.length > 0){
            html.push(this._getTablePartHtml(tfoot,css,"tfoot"));
        }

        html.push("</table>");

        html.push("</div>");
        html.push("</div>");

        return $(html.join(''));
    };

    //自动调整表单
    ScFormTable.prototype._resizeTable = function(target,base){
        var isApp = base.isApp,
            checkBox = base.checkBox,
            maxTdWidth = base.maxTdWidth || this.maxTdWidth,
            minTdWidth = base.minTdWidth || this.minTdWidth,
            limitTdWidth = base.limitTdWidth;

        if(limitTdWidth && maxTdWidth <= minTdWidth){
            console.error("最小单元宽度要比最大单元宽度小");
            maxTdWidth = this.maxTdWidth;
            minTdWidth = this.minTdWidth;
        }

        var targetWidth = target.width();

        var mTable = target.find(".scTable.mTable"),
            mTableWidth = mTable.width(), //不包括边框
            tbodyTdLen = mTable.find("tbody td").length,
            tbodyTrLen = mTable.find("tbody tr").length,
            tbodyNum = tbodyTdLen / tbodyTrLen; //列数

        //保存表格列数
        this.tbodyNum = tbodyNum;

        var columnWidth = new Array();

        var countWidth = 0;

        mTable.find("tbody tr:first td").each(function(index) {
            var w = 0;
            var o = $(this),index = o.index(),colspan = parseInt(o.attr("colspan"));
            var tbodyTdWidth = o.innerWidth();
            var theadTh = mTable.find("thead tr:first th:eq("+index+")");
            var theadThWidth = theadTh.innerWidth();
            var tfootTh = mTable.find("tfoot tr:first th:eq("+index+")");
            var tfootThWidth = tfootTh.innerWidth();

            if(theadThWidth){
                w = tbodyTdWidth >= theadThWidth ? tbodyTdWidth : theadThWidth;
            }

            if(tfootThWidth){
                w = w >= tfootThWidth ? w : tfootThWidth;
            }

            //对比最小值
            if(w < minTdWidth){
                w = minTdWidth;
            }
            //对比最大值
            else if(w > maxTdWidth){
                w = maxTdWidth;
            }

            if(checkBox.state && index == 0){
                w = parseInt(checkBox.width);
            }

            //columnWidth.push(w);

            countWidth += w;
            mTable.find("tbody tr").each(function() {
                $(this).find("td:eq("+index+")").css("width",w);
            });
            //o.css("width",w);
            //theadTh.css("width",w);
            //表头以后可能会出现多行情况
            mTable.find("thead tr").each(function(){
                $(this).find("th:eq("+index+")").css("width",w);
            });
            tfootTh.css("width",w);

            
        });
        
        //表格内容小于外层宽度
        if(tbodyNum * maxTdWidth <= mTableWidth){

        }else{
            if(countWidth < mTableWidth){
                countWidth = mTableWidth;
            }

            target.find("#scroller").css("width",countWidth);
        }

        mTable.find("tbody tr:first td").each(function(){
             columnWidth.push($(this).innerWidth());
        });

        if(base.autoResize != "no"  && mTable.innerHeight() < target.innerHeight()){
            this._autoResizeHeight(target);
        }

        this.columnWidth = columnWidth;
    };

    //表单事件
    ScFormTable.prototype._bindDom = function(target,base,callback){
        var theadEvent = base.theadEvent;
        var fixColNum = base.fixColNum;
        var pThis = this;

        var mTable = target.find(".scTable.mTable"),
            thead = mTable.find("thead"),
            tfoot = mTable.find("tfoot"),
            tbody = mTable.find("tbody");

        if(theadEvent){
            thead.on("click","th[data-click=1]",function(){
                var o = $(this),
                    sort = o.attr("data-sort"),
                    click = o.attr("data-click"),
                    name = o.attr("data-val"),
                    key = o.attr("data-key");

                if(!click){return;}

                if(callback.theadClick && t.isFunction(callback.theadClick)){
                    callback.theadClick.apply(pThis,[o,name,sort,key,pThis]);
                }

                thead.find("i[class^=rulericon-]").remove();

                o.append("<i class='"+(sort == 0 ? "rulericon-triangle_up":"rulericon-triangle_down")+"'></i>");
                
                o.attr("data-sort",sort == 0 ? 1 : 0);
            });
        }
        // ljx add:增加tbody和tfoot每行的点击事件
        tbody.on('click', 'tr', function () {
            var bodyerData = pThis.tbodyData[$(this).index()].data
            pThis.viewCurRow(bodyerData)
        })
        tfoot.on('click', 'tr', function () {
            var tfootData = pThis.tfootData[$(this).index()].data
            pThis.viewCurRow(tfootData)
        })

        var checkBox = base.checkBox;
        if(checkBox.state){
            target.on("click",".hasCb span.scCheckBox",function(e){
                e.preventDefault();
                e.stopPropagation();

                var o = $(this),
                    p = o.parent(),
                    rowspan = p.attr("rowspan"),
                    index = p.parent().index();

                var checked  = o.hasClass('checked');
                var allCheckBox = target.find('tbody .hasCb span.scCheckBox');
                
                var nodeName = p[0].nodeName;
                var obj = nodeName == "TH" ? allCheckBox : o;

                if(checked){
                    obj.removeClass('checked');
                    o.removeClass('checked');
                }else{
                    obj.addClass('checked');
                    o.addClass('checked');
                }

                var arr = new Array();
                try{
                    target.find('.mTable'+(fixColNum > 1 ? ".lTable" : ":first")+' tbody .hasCb span.scCheckBox.checked').each(function() {
                        arr.push(pThis.tbodyData[$(this).parent().parent().index()].data[0]);
                    });

                    if(arr.length == allCheckBox.length || arr.length == allCheckBox.length / 2){
                        target.find('thead .hasCb span.scCheckBox').addClass('checked');
                    }else{
                        target.find('thead .hasCb span.scCheckBox').removeClass('checked');
                    }
                }catch(e){
                    console.log(e);
                }
                
                if(callback.checkBoxClick && t.isFunction(callback.checkBoxClick)){
                    callback.checkBoxClick.apply(pThis,[arr,thead.innerHeight()]);
                }
            });
        }
        this._bindIScroll(target,base,callback);
    };
    ScFormTable.prototype.viewCurRow = function (data) {
        var pThis = this
        var headerData = pThis.theadData[0].data
        var fields = new Array()
        for (var i = 0; i < headerData.length; i++) {
            fields.push({
                label: headerData[i].val,
                value: data[i].val || '--'
            })
        }
        console.log(JSON.stringify(fields, null, 2))
        var html = $(tpl(formDialogTmp, {
            title: '数据详情',
            fields: fields
        }))
        var formId = pThis.option.target.attr('id')
        html.attr('data-id', formId)
        $('body').append(html)

        var dialogDom = $('.form-dialog[data-id="' + formId + '"]')

        dialogDom.fadeIn(300, function () {
        })
        dialogDom.find('.dialog-close').unbind().bind('click', function () {
            dialogDom.fadeOut(300, function () {
                $(this).remove()
            })
        })
        console.log(dialogDom);
        dialogDom.find('.previewImage').on('click', function() {
            console.log('点击图片');
            var preOpt = {};
            preOpt.target = $('.form-dialog');
            preOpt.imgList = [];
            preOpt.imgList.push($(this).attr('src'));
            prePlugin.init_img_url(preOpt);
            $('.imgpre').css({"display":"flex","align-items":"center"})
        })
    }


    ScFormTable.prototype._bindIScroll = function(target,base,callback){

        // this.myScroll = null;

        var wrapper = target.find('#wrapper')[0];
        var scroll = target.find("#wrapper #scroller");

        var isApp = base.isApp,
        scrollEvent = this.scrollEvent,
        status = scrollEvent.status,
        scrollTop = scrollEvent.scrollTop,
        scrollTarget = scrollEvent.target || $(window),
        pThis = this;

        var fadeScrollbars = base.fadeScrollbars;

        //scrollbars: 'custom',
        //click: true, 
        //tap: true, //tap: 'myCustomTapEvent'
        //preventDefault:false,
        var params = { 
            probeType: 3,
            bounce:false,
            scrollX: true, 
            scrollbars: base.showScrollbars ? true : false,
            shrinkScrollbars:"scale",
            mouseWheel: true,
            fadeScrollbars:fadeScrollbars,
            useTransition:true,
            useTransform:true,
            scrollY: true
        };

        if(!isApp){
            params.interactiveScrollbars = true; //滚动条反向(默认是app模式)
        }else{
            params.click = true;
        }
        if( this.myScroll){
            params.startX = this.myScroll.x;
            params.startY = this.myScroll.y;
        }       
        this.myScroll = new IScroll(wrapper, params);

        var mTable = target.find(".scTable.mTable:first"),
            thead = mTable.find("thead"),
            tfoot = mTable.find("tfoot"),
            tbody = mTable.find("tbody");
        var ltTable = target.find(".ltTable"),
            lTable = target.find(".lTable"),
            lbTable = target.find(".lbTable"),
            tTable = target.find('.tTable'),
            bTable = target.find('.bTable');

        this.myScroll.on("scroll",function(){
            var x = this.x,y = this.y;

            if(x < 0){
                ltTable.addClass('shadow');
                lTable.addClass('shadow');
                lbTable.addClass('shadow');
            }else{
                ltTable.removeClass('shadow');
                lTable.removeClass('shadow');
                lbTable.removeClass('shadow');
            }

            //ltTable.attr("style","-webkit-transform: translateX("+x+"px);");
            //lTable.attr("style","-webkit-transform: translateX("+x+"px);top:"+thead.innerHeight()+"px");
            //lbTable.attr("style","-webkit-transform: translateX("+x+"px);");

            /*ltTable.css("top",-y);
            tTable.css("top",-y);

            ltTable.css("left",-x);
            lTable.css("left",-x);
            lbTable.css("left",-x);*/
            x = -x;
            y = -y;

            if(thead.length > 0){
                // ltTable.css({
                //     "-webkit-transform":"translate("+x+"px,"+y+"px)"
                // });
                tTable.css({
                    "-webkit-transform":"translateY("+y+"px)"
                });
            }

            lTable.css({
                "-webkit-transform":"translateX("+x+"px)"
            });

            if(tfoot.length > 0){
                // lbTable.css({
                //     "-webkit-transform":"translate("+x+"px,"+y+"px)"
                // });
                bTable.css({
                    "-webkit-transform":"translateY("+y+"px)"
                });
            }

            if(callback && t.isFunction(callback.getScrollxy)){
                callback.getScrollxy.apply(pThis,[x,y,target,pThis]);
            }
        });
        
        this.myScroll.on("scrollEnd",function(){
            console.log(this.x, this.y)
            var x = this.x,y = this.y;

            x = -x;
            y = -y;

            if(thead.length > 0){
                // ltTable.css({
                //     "-webkit-transform":"translate("+x+"px,"+y+"px)"
                // });
                tTable.css({
                    "-webkit-transform":"translateY("+y+"px)"
                });
            }

            lTable.css({
                "-webkit-transform":"translateX("+x+"px)"
            });

            if(tfoot.length > 0){
                // lbTable.css({
                //     "-webkit-transform":"translate("+x+"px,"+y+"px)"
                // });
                bTable.css({
                    "-webkit-transform":"translateY("+y+"px)"
                });
            }

            //重新刷新滚动条
            if(pThis.myScroll){
                pThis.myScroll.refresh();
            }
        });

        wrapper.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
    };

    //固定列
    ScFormTable.prototype._fixTable = function(target,base){
        var fixColNum = base.fixColNum;

        var mTable = target.find(".scTable.mTable"),
            thead = mTable.find("thead"),
            tfoot = mTable.find("tfoot"),
            tbody = mTable.find("tbody"),
            tbodyNum = this.tbodyNum,
            mTableWidth = mTable.innerWidth();

        //是否需要左边固定 现已头部底部固定
        var canFixCol = 1;
        if(!t.isInteger(fixColNum) || fixColNum < 0 || fixColNum > tbodyNum || fixColNum == 0){
            canFixCol = 1; // 不再需要固定列
            //console.error("固定列配置错误!!");return;
        }
        var pThis = this;
        function fixColTable(newTable,way,span,fixTablePart,notUse){
            newTable.find(way+" tr").each(function() {
                var lt_tr = $(this);
                lt_tr.find(span+":gt("+(fixColNum - 1)+")").remove();

                lt_tr.find(span).each(function(index) {
                    var o = fixTablePart.find("tr:eq("+lt_tr.index()+") "+span+":eq("+index+")");

                    var minus = 0;

                    if(o.attr("rowspan") > 1){
                        minus = 0;
                    }

                    $(this).innerHeight(o.innerHeight() + minus + 1);  
                    $(this).innerWidth(o.innerWidth() + 1);
                });
            });
        }

        if(thead.length > 0){


            //头部固定表格
            var tTable = mTable.clone();
            tTable.addClass('fixTable tTable');
            tTable.find("tbody,tfoot").remove();
            //tTable.css({"top":"75px"});
            if(!base.limitTdWidth){
                mTable.find("thead tr").each(function(){
                    var o = $(this);
                    var index = o.index();

                    o.find("th").each(function() {
                        var th_index = $(this).index();
                        var w = $(this).innerWidth();

                        console.log(w);

                        //tTable.find("thead tr:eq("+index+") th:eq("+th_index+")").css("width",w);
                        tTable.find("thead tr:eq("+index+") th:eq("+th_index+")").each(function(index, el) {
                            $(this).html("<div style='width:"+(w-15*2)+"px'>"+$(this).html()+"</div>");
                        });
                    });
                });
            }
        }

        if(canFixCol){
            //左边固定表格
            // var lTable = mTable.clone();
            // lTable.addClass('fixTable lTable');
            // lTable.find("thead,tfoot").remove();
            // var tbodyClone = tbody.clone();
            // fixColTable(lTable,"tbody","td",tbody);
            //lTable.css("top",thead.innerHeight());
        }

        if(tfoot.length > 0){
           //底部固定表格
            var bTable = mTable.clone();
            bTable.addClass('fixTable bTable');
            bTable.find("thead,tbody").remove();

            mTable.find("tfoot tr").each(function(){
                var o = $(this);
                var index = o.index();

                if(o.attr("data-colmerge") == 1){
                    o.find("th").each(function(th_index) {
                        var w = $(this).innerWidth();

                        bTable.find("tfoot tr:eq("+index+") th:eq("+th_index+")").each(function() {
                            $(this).html("<div style='width:"+(w-15*2)+"px'>"+$(this).html()+"</div>");
                        });
                    });
                }
            });

            if(canFixCol){
                //左下角固定表格
                var lbTable = mTable.clone();
                lbTable.addClass('fixTable lbTable');
                lbTable.find("thead,tbody").remove();
                fixColTable(lbTable,"tfoot","th",tfoot);
            }
        }

        var scroller = target.find("#scroller");
       
        scroller.append(tTable);
        scroller.append(bTable);
        if(canFixCol){
            // scroller.append(ltTable);
            // scroller.append(lTable);
            scroller.append(lbTable);

            // lTable.css({
            //     top:thead.innerHeight() ? thead.innerHeight() + 1 : 0
            // });
        }

        //lTable.innerWidth(ltTable.innerWidth());
        if(tfoot.length > 0){
            var top = 0;
            if(mTable.innerHeight() > target.innerHeight() && canFixCol){
                top = target.innerHeight() - lbTable.innerHeight();
            }else{
                top = mTable.innerHeight() - tfoot.innerHeight() - 1;
            }
        
            if(canFixCol){
                bTable.css({
                    "top":top//
                });

                // lbTable.css({
                //     "top":top
                // });
                // lbTable.innerWidth(lTable.innerWidth());
            }
        }
    };

    ScFormTable.prototype._mergeObj = function(target,way,span,justTbody){
        var newAdd = justTbody ? " tr[data-new=1]" : "";
        var pSpanLen = target.find(way + newAdd + " " + span).length,
            wayNum = this.tbodyNum; //列数

        /* //暂时不处理横向
        var eachArr = target.find(way+newAdd+" "+span);
        eachArr.each(function(index) {
            var o = $(this);
            var tr = o.parent();
            var key = o.attr("data-key");
            var first_o = target.find(way+newAdd+" "+span+"[data-key="+key+"]:first");

            //横向处理
            tr.find(span).each(function(col_o_index) {
                var col_o = $(this);

                var o_use_index = tr.find(o).index();

                if(!col_o.hasClass('hide') && key == col_o.attr("data-key") && col_o_index - o_use_index == 1){
                    var first_col_o = tr.find(span+"[data-key="+key+"]:first");
                    first_col_o.attr("colspan",(parseInt(first_col_o.attr("colspan")) || 1) + 1);
                    col_o.addClass('hide');
                }
            });

            //纵向处理
            // for (var i = index + wayNum; i <= pSpanLen - 1; i+wayNum) {
            //     var row_o = target.find(way+" "+span+":eq("+i+")");
            //     if(!row_o.hasClass('hide') && row_o.attr("data-key") == key){
            //         first_o.attr("rowspan",(parseInt(first_o.attr("rowspan")) || 1) + 1);
            //         row_o.addClass('hide');
            //     }else{
            //         break;
            //     }
            // };
        });
        //*/
        //console.time("横向处理时间:");
        var pThis = this;
        var newTr = justTbody ? " tr[data-new=1]" : " tr";

        var trArr = target.find(way+newTr);

        if(pThis.hasRowMerge){
            var lastMergeTrNum = target.find(way+" tr[data-rowmerge=1]:last").index() - 1 || 0;
            trArr = target.find(way+" tr:gt("+lastMergeTrNum+")");

            this.hasRowMerge = 0;
        }

        //目前只处理tfoot 横向合并
        if(way == "tfoot" || (way == "tbody" && this.params.data.tbody.length == 0)){
            trArr.each(function() {
                var o = $(this);
                var spanArr = o.find(span);
                var spanArrLen = spanArr.length;
                var mergeStatus = 0;

                spanArr.each(function(index) {
                    var span_o = $(this),
                        span_o_key = span_o.attr("data-key");

                    //横向处理
                    for (var z = index + 1; z <= spanArrLen - 1; z++) {
                        var col_o = spanArr.eq(z);
                        if(!col_o.hasClass('hide') && col_o.attr("data-key") == span_o_key){
                            span_o.attr("colspan",(parseInt(span_o.attr("colspan")) || 1) + 1);
                            col_o.addClass('hide');
                            span_o.css({
                                height:span_o.innerHeight() + 1
                            });

                            mergeStatus = 1;

                            pThis.hasColMerge = 1;
                        }else{
                            break;
                        }
                    };
                });

                if(mergeStatus){
                    o.attr("data-colmerge",1);
                }
            });
        }
        //console.timeEnd("横向处理时间:");
        //console.time("纵向处理时间：");

        var trArrLen = trArr.length;
        // 纵向处理
        for (var j = 0; j < wayNum; j++) {
            var spanArr = trArr.find(span+":eq("+j+")"),
                spanArrLen = spanArr.length;

            spanArr.each(function(index) {
                var span_o = $(this);
                var span_o_key = span_o.attr("data-key");
                var mergeStatus = 0;

                //纵向处理
                for (var i = index + 1; i <= spanArrLen - 1; i++) {
                    var row_o = spanArr.eq(i);
                    if(!row_o.hasClass('hide') && row_o.attr("data-key") == span_o_key){
                        span_o.attr("rowspan",(parseInt(span_o.attr("rowspan")) || 1) + 1);
                        row_o.addClass('hide');
                        span_o.css({
                            height:span_o.innerHeight() + 1
                        });

                        mergeStatus = 1;

                        pThis.hasRowMerge = 1;
                    }else{
                        break;
                    }
                };

                if(mergeStatus){
                    span_o.parent().attr("data-rowmerge",1);
                }
            });
        };
        //console.timeEnd("纵向处理时间：");
    };

    //整合表格
    ScFormTable.prototype._mergeTable = function(mTable,justTbody){
        var pThis = this;
        var keysArr = pThis.keysArr;

        var tbodyTrLen = mTable.find("tbody tr").length,
            tbodyNum = this.tbodyNum;

        if(!justTbody){
            this._mergeObj(mTable,"thead","th");
            this._mergeObj(mTable,"tfoot","th");
        }
        
        this._mergeObj(mTable,"tbody","td",justTbody);
        
        //特殊合并样式处理
        //背景色
        mTable.find("tbody td").each(function(index) {
            var o = $(this),rowspan = o.attr("rowspan"),colspan = o.attr("colspan");

            //背景色
            /*if((colspan && colspan < tbodyNum - 1 && tbodyNum > 1) || 
               (rowspan && rowspan < tbodyTrLen - 1 && rowspan > 1 && index%tbodyNum != 0)){
                o.addClass('c-bg');
            }*/
            if(rowspan > 1 || colspan > 1){
                o.addClass('c-bg');
            }
            //底部线
            /*if(!o.hasClass('r-bottom') && rowspan){
                var key_td = mTable.find("tbody td[data-key="+o.attr("data-key")+"]");
                
                o.addClass('r-bottom');
                key_td.eq(key_td.length - 1).parent().addClass('r-bottom');

                if(rowspan == tbodyTrLen - 1){
                    o.addClass('r-bottom');
                    key_td.eq(key_td.length - 1).parent().addClass('r-bottom');
                }else if(key_td.parent().index()+1 >= tbodyTrLen - 1){
                    o.addClass('r-bottom');
                }
            }*/
           // o.addClass('r-bottom');
        });

        //底部线
        mTable.find("tbody tr").each(function() {
            var max_row = 0;
            var max_row_key = "";
            var count = 0;
            var o = $(this);

            var td = o.find("td");

            td.each(function() {
                var rowspan = $(this).attr("rowspan") || 0;

                if(!$(this).hasClass('hide') && rowspan > max_row){
                    max_row = rowspan;
                    max_row_key = $(this).attr("data-key");
                }

                if($(this).hasClass('hide') && $(this).hasClass('r-bottom')){
                    count ++;
                }
            });

            if(max_row != 0){
                var maxObj = mTable.find("tbody td[data-key='"+max_row_key+"'][rowspan='"+max_row+"']");
                maxObj.addClass('r-bottom');
                mTable.find("tbody td[data-key="+max_row_key+"]:last").parent().find("td").each(function() {
                    $(this).addClass('r-bottom');

                    if($(this).hasClass('hide')){
                        mTable.find("tbody td[data-key="+$(this).attr("data-key")+"]").addClass('r-bottom');
                    }
                });
            }else if(count == 0){
                td.addClass('r-bottom');
            }
        });
    };

    //获取无数据的tbody
    ScFormTable.prototype._getNoData = function(thead,tbody){
        tbody = new Array();
        for (var i = 0; i < thead.length; i++) {
            var d = thead[i].data;
            var arr = new Array();

            for (var j = 0; j < d.length; j++) {
                arr.push({
                    key:"noData",
                    val:"暂无数据"
                })
            };

            tbody.push({
                data:arr
            })
        };

        return tbody;
    };

    ScFormTable.prototype._removeHideSpan = function(target){
        target.find("td.hide,th.hide").remove();
    };

    ScFormTable.prototype._autoResizeHeight = function(target){
        target.innerHeight(target.find(".scTable.mTable").innerHeight());
    };

    //重新生成数据
    ScFormTable.prototype.addTbodyData = function(target,tbody){
        var wrapper = target.find("#wrapper");
        var mTable = wrapper.find(".mTable:first");
        var lTable = wrapper.find(".lTable");

        var params = this.params;
        var pThis = this;

        var dom = $($.trim(this._getTablePartHtml(tbody,params.css,"tbody",1)));
        var html = dom.html();

        mTable.find("tbody").append(html);

        if(params.base.mergeTable){
            this._mergeTable(target.find(".mTable:first"),1);
        }

        //重新计算插入高度
        mTable.find("tr[data-new=1]").each(function() {
            //$(this).find("td").outerHeight($(this).outerHeight());
            var o = $(this);
            //o.find("td").innerWidth(o.find("td").innerWidth());
            o.find("td").each(function(index){
                $(this).innerWidth(pThis.columnWidth[index]);
            });
        });

        // if(lTable.length > 0){
        //     var clone = mTable.find("tr[data-new=1]").clone();
        //     clone.each(function() {
        //         var lt_tr = $(this);
        //         lt_tr.find("td:gt("+(params.base.fixColNum - 1)+")").remove();
        //     });
        //     lTable.find("tbody").append(clone);
        // }

        //lTable.find("tr[data-new=1] td") //??局部
        // lTable.find("tbody tr").each(function() {
        //     var index = $(this).index();
        //     var l_td = $(this).find("td:eq(0)");
        //     var m_tr = mTable.find("tbody tr:eq("+parseInt(index)+")");

        //     if(m_tr.length >  0 && l_td.length > 0){
        //         var m_td = m_tr.find("td");
        //         if(l_td.attr("rowspan") > 1){
        //             l_td.attr("rowspan",m_td.attr("rowspan")); 
        //         }

        //         if(l_td.attr("colspan") > 1){
        //             l_td.attr("colspan",m_td.attr("colspan")); 
        //         }

        //         l_td.innerHeight(m_td.innerHeight());
        //     }
        // });

        // target.find("tr[data-new=1]").removeAttr('data-new');

        //重新刷新滚动条
        if(this.myScroll){
            this.myScroll.refresh();
        }

        this._removeHideSpan(target);
    };

    ScFormTable.prototype.getColumnWidth = function(){
        return this.columnWidth;
    };

    //分页相关
    ScFormTable.prototype._getPageToolsHtml = function(pageTools){
        if(!pageTools.showNum){
            pageTools.showNum = this.params.pageTools.showNum;
        }

        if(!pageTools.pageSize){
            pageTools.pageSize = this.params.pageTools.pageSize;
        }

        if(!pageTools.target){
            pageTools.target = this.params.pageTools.target;
        }

        if(!pageTools.showPageInfo){
            pageTools.showPageInfo = this.params.pageTools.showPageInfo;
        }

        if(!pageTools.pageInfoTarget){
            pageTools.pageInfoTarget = this.params.pageTools.pageInfoTarget;
        }

        if(!pageTools.count){
            pageTools.count = this.params.pageTools.count;
        }

        if(pageTools.showNum <= 0){
            pageTools.showNum = 1;
        }

        var target = pageTools.target;
        if(!t.checkJqObj(target)){
            console.error("请检查配置!!!");
            return;
        }

        var html = new Array(),
        count = parseInt(pageTools.count), //总数
        index = parseInt(pageTools.index), //当前页数
        showNum = parseInt(pageTools.showNum), //显示个数
        pageCount = Math.ceil(count/pageTools.pageSize); //总页数

        var tbody = this.tbody;
        if(pageTools.pageInfoTarget && pageTools.showPageInfo && t.isArray(tbody) && tbody.length > 0){
            var pageSize = pageTools.pageSize;
            var n1 = pageSize*(index-1) + 1,
                n2 = pageSize*index > count ? count : pageSize*index;
            pageTools.pageInfoTarget.html('<p class="pageNumInfo tac fs12 c-4a">第<span>'+n1+'</span>~<span>'+n2+'</span>条记录,共<span>'+count+'</span>条记录</p>');
        }

        //刚好就只有一页
        if(pageTools.pageSize >= count){
            return;
        }

        //当前显示个数
        var num = showNum > pageCount ? pageCount : showNum;

        html.push('<div class="pageTools">');
        html.push('<span class="firstPage '+(index == 1 ? "lock" : "")+'" data-page="1"><<</span>');
        html.push('<span class="prePage '+(index == 1 ? "lock" : "")+'" data-page="'+(index - 1 < 1  ? 1 : index - 1)+'"><</span>');

        num = num - 1;
        var l_num =  Math.ceil(num/2),
        r_num = num - l_num;

        //倒数第一页
        if(index > 1 && pageCount - index == 1 ){
            l_num = num - 1;
        }
        //最后一页
        else if(pageCount - index == 0){
            l_num = num;
        }

        for (var j = l_num; j >= 1; j--) {
            var prev = index - j;
            if(prev > 0){
                html.push('<span data-page="'+prev+'">'+prev+'</span>');
            }else{
                console.log("===" + j);
                r_num = num - j + 1;
            }
        };

        html.push('<span data-page="'+index+'" class="current">'+index+'</span>');

        for (var k = 1; k <= r_num; k++) {
            var next = index + k;
            if(next <= pageCount){
                html.push('<span data-page="'+next+'">'+next+'</span>');
            }
        };

        html.push('<span class="nextPage '+(index == pageCount ? "lock" : "")+'" data-page="'+(index + 1 >= pageCount ? pageCount : index +1)+'">></span>');
        html.push('<span class="lastPage '+(index == pageCount ? "lock" : "")+'" data-page="'+pageCount+'">>></span>');
        html.push('</div>');

        target.html(html.join(''));
    };

    ScFormTable.prototype._bindPageTools = function(pageTools){
        var pThis = this;
        pageTools.target.on("click","span",function(){
            var num = $(this).attr("data-page");

            if($(this).hasClass("lock")){return;}

            $(this).addClass('current').siblings().removeClass('current');

            if(!t.isFunction(pageTools.buttonClick)){return;}
            pageTools.buttonClick(num,pageTools);

            //pageTools.index = parseInt(num); 
            //pThis.restartPageTools(pageTools);
        });
    };

    ScFormTable.prototype.restartPageTools = function(pageTools){
        this._getPageToolsHtml(pageTools);
        //this._bindPageTools(pageTools);
    };
 
    return ScFormTable;
}));