define([
	'jquery',
	'common:widget/lib/tools',
	"common:widget/lib/plugins/jweixin-1.2.0",
	"common:widget/lib/plugins/uploadifive/uploadifive",
	"common:widget/lib/plugins/spark-md5.min",
	'common:widget/lib/UiFramework',
], function($, tools, wx, uploadifive, SparkMD5,UiFramework) {
	function GenericFramework() {
		this.wxJsdkConfig = null;
	}

	GenericFramework.prototype.init = function(wxJsdkConfig,beta) {
		this.wxJsdkConfig = wxJsdkConfig;

		var params = {
			debug: false,
			appId: wxJsdkConfig.appId,
			timestamp: wxJsdkConfig.timestamp,
			nonceStr: wxJsdkConfig.nonceStr,
			signature: wxJsdkConfig.signature,
			jsApiList: [
				'chooseImage',
				'previewImage',
				'uploadImage',
				'downloadImage',
				'openLocation',
				'getLocation',
				'openEnterpriseChat',
				'getNetworkType'
			]
		};

		if(beta){
			params.beta = true;
			params.jsApiList.push('getSupportSoter');
			params.jsApiList.push('requireSoterBiometricAuthentication');
			params.jsApiList.push('chooseInvoice');
		}
		
		this.newWxJsdkConfig = params;

		wx.config(params);

		wx.error(function(res) {
			alert(res.errMsg);
		});
	};

	//上传文件
	GenericFramework.prototype.uploadFile = function(_setting) {
		var pThis = this;
		var uploadFile = {
			uploadUrl: "/wx/index.php?model=upload&cmd=101&utype=2",
			setting: null,
			_setting: {
				target: null, //添加的jq dom对象
				type: "files",
				buttonClass: "file-btnClass",

				fileSizeLimit: "20MB",
				uploadScript: "/wx/index.php?model=upload&cmd=101",
				//txt,xml,pdf,zip,doc,ppt,xls,docx,pptx,xlsx,rar,jpeg,jpg,png,rm,rmvb,wmv,avi,mpg,mpeg,mp4,mp3,wma,wav,amr"
				//'*.gif; *.jpeg; *.jpg; *.png; *.ppt; *.pptx; *.doc; *.docx'
				fileTypeExts: '*.txt;*.xml;*.pdf;*.zip;*.doc;*.ppt;*.xls;*.docx;*.pptx;*.xlsx;*.rar;*.jpeg;*.jpg;*.gif;*.bmp;*.png;*.rm;*.rmvb;*.wmv;*.avi;*.mpg;*.mpeg;*.mp4;*.mp3;*.wma;*.wav;*.ram;*.amr',
				sourceType: 3, //1 相册 2 相机 3 相册和相机
				callback: {
					filesInitCallback:null, //附件回调
					filesUpload:null, //附件上传中回调
					uploadCompleteCallback:null, //图片上传完成回调
					delFileCallback:null, //删除图片回调

					afterUpload: null,
					//附件 点击
					fileClick:null
				},
				wx:{
					uploadUrl:null,
					uploadData:null,
					beforeStartCallback:null
				}	
			},
			cacheSetting:null,
			//微信图片保存
			images: {
				localId: [],
				serverId: []
			},
			//附件保存
			files:[],
			//是否有文件上传中
			hasFileUploading:false,
			//检查配置
			checkParams: function(setting) {
				if (setting.target == null || !(setting.target instanceof jQuery)) {
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting;

				this.cacheSetting = _setting;

				return _setting;
			},
			init: function(_setting) {
				_setting = this.checkParams(_setting);

				//特殊配置
				if(_setting.wx.uploadUrl){
					this.uploadUrl = _setting.wx.uploadUrl;
				}

				//doc docx image ppt pptx execl pdf 
				if (_setting.type == "files") {
					this.bindDomForFiles(_setting);
				}

				if (_setting.type == "wx") {

					if (!pThis.wxJsdkConfig) {
						tools.console("请先初始化微信配置！", 1);
						return;
					}

					this.bindDomForWx(_setting);
				}

				return this;
			},
			bindDomForFiles: function(_setting) {
				var obj = _setting.target,pThis = this;

				obj.uploadifive({
					'auto': true,
					'buttonText': '',//上传附件
					'buttonClass': _setting.buttonClass,
					'width':"59",
					'height': "59",
					'fileSizeLimit': _setting.fileSizeLimit,
					'method': _setting.method,
					'uploadScript': _setting.uploadScript,
					'fileTypeExts': _setting.fileTypeExts,
					'itemTemplate': pThis.getQueueHtml(),
					//'formData': { 'areaTreeId': 1 },
					'onAddQueueItem': function(file) {
						var fileSizeLimit = _setting.fileSizeLimit,
							limitNumber = 0,unit = "KB";

						var layerPackage = UiFramework.layerPackage();

						var ext = pThis.getFileTypeExt(_setting,file),
						extClass = "file-"+tools.getFileTypeExtIcon(ext);

						file.queueItem.addClass('hide')
						file.queueItem.find(".fileTypeExt").addClass(extClass);
						file.queueItem.attr("data-ext",ext);
						file.queueItem.attr("data-name",file.name);

						if(fileSizeLimit && fileSizeLimit !=0){
							try{
								if(_setting.fileSizeLimit.lastIndexOf("GB") > 0){
									limitNumber = 1024*1024*1024*fileSizeLimit.split("GB")[0];
									unit = "GB";
								}else if(_setting.fileSizeLimit.lastIndexOf("MB") > 0){
									limitNumber = 1024*1024*fileSizeLimit.split("MB")[0];
									unit = "MB";
								}else{
									limitNumber = 1024*fileSizeLimit.split("KB")[0];
								}
							}catch(e){
								console.log("计算文件大小出错");
							}
							
							if(limitNumber !=0){

								if(extClass == "file-img" && file.size > 5*1024*1024){
									layerPackage.ew_alert({title:"图片文件大小不得超过5MB！"});
									obj.uploadifive('cancel', file);
									return;
								}else if(extClass == "file-music" && file.size > 2*1024*1024){
									layerPackage.ew_alert({title:"音频文件大小不得超过2MB！"});
									obj.uploadifive('cancel', file);
									return;
								}else if(extClass == "file-video" && file.size > 10*1024*1024){
									layerPackage.ew_alert({title:"视频文件大小不得超过10MB！"});
									obj.uploadifive('cancel', file);
									return;
								}else if(file.size > limitNumber){
									layerPackage.ew_alert({title:"文件大小不得超过"+fileSizeLimit+unit+"！"});
									obj.uploadifive('cancel', file);
									return;
								}
								//UiFramework.layerPackage().ew_alert("文件大小不得超过2MB！");
								//alert("文件大小不得超过2MB！");
								//obj.uploadifive('cancel', file);
								//return;
							}
						}

						if (!ext) {
							layerPackage.ew_alert({title:"上传文件类型只能是 txt,xml,pdf,zip,doc,ppt,xls,docx,pptx,xlsx,rar,jpeg,jpg,png,rm,rmvb,wmv,avi,mpg,mpeg,mp4,mp3,wma,wav,amr 的一种"});
							//alert("上传文件类型只能是 txt,xml,pdf,zip,doc,ppt,xls,docx,pptx,xlsx,rar,jpeg,jpg,png,rm,rmvb,wmv,avi,mpg,mpeg,mp4,mp3,wma,wav,amr 的一种");
							//obj.uploadifive('cancel', file.queueItem.last().data('file'));
							obj.uploadifive('cancel', file);
							//file.queueItem.last().remove();
							return;
						}

						pThis.getFileMd5(file,function(md5){
							var flag = pThis.hasFiles(md5,ext);
							file.queueItem.attr("data-hash",md5);

							if (flag == 2) {
								alert("重复文件上传");
								obj.uploadifive('cancel', file);
								//file.queueItem.last().remove();
								return;
							}
							else if(flag == 1){
								alert("该文件正在上传");
								pThis.bindFileClickDom(file.queueItem.last(),md5,ext);
								//return;
							}
							else if(flag == 0){
								//复制显示在制定位置
								var clone = file.queueItem.clone();
								//clone.addClass('cloneFileItem');
								obj.parent().before(clone.removeClass('hide'));
								pThis.bindFileClickDom(clone.last(),md5,ext);

								//绑定事件
								pThis.bindFileClickDom(file.queueItem.last(),md5,ext);
								//保存数据
								pThis.saveUploadFiles({
									file_name:file.name,
									md5:md5,
									file_path:"",
									file_ext:ext,
									file_size:file.size,
									file_hash:"",
									isDownload:0 //是否下载完成
								});

								pThis.hasFileUploading = true;
							}

							if (tools.isFunction(_setting.callback.filesUpload)) {
								_setting.callback.filesUpload.apply(pThis, [pThis,pThis.files]);
							}
						});
					},
					'onQueueComplete' : function(uploads) {
			            //console.log(uploads.successful + ' files were uploaded successfully.');
			        },
					//显示上传的文件数量
					'onUpload': function(file) {
					},
					'onProgress'   : function(file, e) {
			            if (e.lengthComputable) {
			                var percent = Math.round((e.loaded / e.total) * 100);
			            }
			            file.queueItem.find('.fileinfo').html(percent + '%');
			            file.queueItem.find('.progress-bar i').css('width', percent + '%');

			            var msg = percent + '%' + '<br/> (上传中)';
			            var o = $(".uploadifive-queue-item[data-hash="+file.queueItem.attr("data-hash")+"]");
			            o.find('.fileinfo').html(msg);
			            o.find('.progress-bar i').css('width', percent + '%');

			            if(percent == 100){
			            	setTimeout(function(){
			            		o.find('.progress-bar i').remove();
			            		o.find('.fileinfo').addClass('fileDel').html("删除");
			            	},500);
			            }
			        },
					//文件上传完成触发事件， 文件是否上传成功插件不做判断
					'onUploadComplete': function(file, data) {
						try{
							data = $.parseJSON(data);
							var fileInfo = file.queueItem.find(".fileInfo");

							if(data.errcode != 0){
								alert(data.errmsg);
								fileInfo.text("上传失败");

								pThis.delFile(file.queueItem.attr("data-hash"),pThis.getFileTypeExt(_setting,file));
								
								setTimeout(function(){
									file.queueItem.fadeOut('slow', function() {
										file.queueItem.remove();
										console.log(pThis.files);
									});

								},2000);
								//删除错误文件
								$(".cloneFileItem[data-hash="+file.queueItem.attr("data-hash")+"]").remove();
								
								return;
							}else{
								var ext = pThis.getFileTypeExt(_setting,file);
								//var extClass = "file-"+tools.getFileTypeExtIcon(ext);
								//file.queueItem.find(".fileTypeExt").addClass(extClass);
								if(tools.getFileTypeExtIcon(ext) == "img"){
									file.queueItem.find(".fileTypeExt").html('<img src="'+data.image_path+'" alt="" style="background-color:#fff;"/>');
									$(".uploadifive-queue-item[data-hash="+file.queueItem.attr("data-hash")+"]").find(".fileTypeExt").html('<img src="'+data.image_path+'" alt="" style="background-color:#fff;"/>');
								}
								fileInfo.text(tools.bytesToSize(file.size));

								pThis.saveData(data.file_hash,data.path,data.hash,ext);

								pThis.hasFileUploading = false;

								if (tools.isFunction(_setting.callback.uploadCompleteCallback)) {
									_setting.callback.uploadCompleteCallback.apply(pThis, [pThis,pThis.files]);
								}
							}
						}catch(e){
							console.log(e);
						}
					},
					'onError': function(errorType,file) {
						console.log("errorType");
						console.log(errorType);
						obj.uploadifive('cancel', file);
					},
					'onCancel': function(file) {
					},
					'onInit' : function() {
						if (tools.isFunction(_setting.callback.filesInitCallback)) {
							_setting.callback.filesInitCallback.apply(pThis, [pThis]);
						}
					}
				});
			},
			bindDomForWx: function(_setting) {
				var obj = _setting.target,
					pThis = this,
					sourceType = _setting.sourceType;

				if (sourceType == 1) {
					sourceType = ['album'];
				} else if (sourceType == 2) {
					sourceType = ['camera'];
				} else {
					sourceType = ['album', 'camera'];
				}

				wx.ready(function() {
					obj.off().on("click", function() {
						var flag = true;
						if(_setting.wx.beforeStartCallback && tools.isFunction(_setting.wx.beforeStartCallback)){
							flag = _setting.wx.beforeStartCallback.apply(pThis,[_setting]);
						}

						if(!flag){
							return;
						}

						wx.chooseImage({
							sourceType: sourceType,
							success: function(res) {
								//alert("----res.localIds-----" + res.localIds);
								pThis.images.localId = res.localIds;
								//alert('已选择 ' + res.localIds.length + ' 张图片');

								pThis.wxUpload(_setting);
							}
						});
					});
				});
			},
			wxUpload: function(_setting) {
				var images = this.images,
					pThis = this;
				//alert("----images.localId-----" + images.localId);

				if (images.localId.length == 0) {
					alert('请先选择图片');
					return;
				}
				var i = 0,
					length = images.localId.length;
				images.serverId = [];
				//alert("----images.localId[i]-----" + images.localId[i]);
				function upload() {
					wx.uploadImage({
						localId: images.localId[i],
						success: function(res) {
							//alert(res);
							//alert(JSON.stringify(res));
							i++;
							//alert('已上传：' + i + '/' + length);
							var flag = false;
							//alert("----res.serverId-----" + res.serverId);

							var params = _setting.wx.uploadData;

							if(!params){
								params = {
									media_id : res.serverId
								};
							}else{
								params.data.media_id =  res.serverId;
							}

							//alert(JSON.stringify(params));

							tools.ajaxJson({
								url: pThis.uploadUrl,
								data: params,
								async: false,
								callback: function(result, status) {
									if (status == false && result == null) {
										return;
									}
									if (result.errcode != 0) {
										tools.console(result.errmsg, 1);
										return;
									}
									//alert(result.errmsg);
									//alert(JSON.stringify(result));

									flag = true;

									if (tools.isFunction(_setting.callback.afterUpload)) {
										_setting.callback.afterUpload.apply(pThis, [result]);
									}
								}
							});

							if (flag) {
								images.serverId.push(res.serverId);

								if (i < length) {
									upload();
								}
							}
						},
						fail: function(res) {
							alert(JSON.stringify(res));
						}
					});
				}
				upload();
			},
			getQueueHtml: function(fileData) {
				if(!fileData){
					fileData = {
						file_name:"",
						file_hash:"",
						file_path:"",
						file_ext:"",
						file_size:0
					}
				}

				var dataIdHtml = '';
				if (fileData.id) {
					dataIdHtml = ' data-id="' + fileData.id + '" ';
				}

				var dataWorkNodeIdHtml = '';
				if (fileData.work_node_id) {
					dataWorkNodeIdHtml = ' data-worknodeid="' + fileData.work_node_id + '" ';
				}

				var dataCreatIdHtml = '';
				if (fileData.create_id) {
					dataCreatIdHtml = ' data-createid="' + fileData.create_id + '" ';
				}

				var dataPathHtml = '';
				if (fileData.file_path) {
					dataPathHtml = ' data-path="'+ (media_domain || "") + fileData.file_path + '" ';
				}

				var dataFileSizeHtml = '';
				if (fileData.file_size) {
					dataFileSizeHtml = ' data-filesize="'+ fileData.file_size + '" ';
				}

				var html = new Array();

				html.push('<div class="uploadifive-queue-item queue-file ma0 cloneFileItem" ' + dataIdHtml + dataWorkNodeIdHtml +  dataCreatIdHtml + dataPathHtml + dataFileSizeHtml + ' data-hash="'+fileData.file_hash+'" data-ext="'+fileData.file_ext+'" data-name="'+(fileData.file_name || "")+'">');
				html.push('<span class="fileTypeExt fl file-'+tools.getFileTypeExtIcon(fileData.file_ext)+'">');
				if(tools.getFileTypeExtIcon(fileData.file_ext) == "img"){
					html.push('<img src="'+media_domain + fileData.file_path+'" alt="" style="background-color:#fff;"/>');
				}
				html.push('</span>');
				html.push('<span class="fl filename fs17 c-4a hideTxt"><i class="fs-n hideTxt">'+(fileData.file_name || "")+'</i></span>');
				
				var size = "";
				if(fileData.file_size){
					size = "删除";
				}
				html.push('<span class="fileinfo fr c-gray mr6 '+(size ? "fileDel":"")+'">'+size+'</span>');
				html.push('<span class="progress-bar block"><i></i></span>'); //进度条
				
				html.push('<div class="close hide"></div>');
				html.push('</div>');

				return html.join('');
			},
			getFileTypeExt: function(_setting,file) {
				var fileName = file.name;
				var ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length);

				ext = ext.toLowerCase();

				var fileTypeExts = _setting.fileTypeExts.split(";");

				for (var i = 0; i < fileTypeExts.length; i++) {
					if (fileTypeExts[i] == "*." + ext) {
						return ext;
					}
				};

				if(fileName.indexOf('image%') >= 0){
					return "jpg";
				}

				return null;
			},
			saveUploadFiles:function(data){
				var flag = false;

				for (var i = 0; i < this.files.length; i++) {
					if(this.files[i].md5 == data.md5 && this.files[i].file_ext == data.file_ext){
						flag = true;
						break;
					}
				};

				if(!flag){
					this.files.push(data);
				}

				console.log(this.files);
			},
			saveData:function(md5_hash,path,file_hash,ext){
				for (var i = 0; i < this.files.length; i++) {
					if(this.files[i].md5 == md5_hash && this.files[i].file_ext == ext){
						this.files[i].file_path = path;
						this.files[i].file_hash = file_hash;
						this.files[i].isDownload = 1;
						break;
					}
				};
			},
			hasFiles:function(md5_hash,ext){
				for (var i = 0; i < this.files.length; i++) {
					if(this.files[i].md5 == md5_hash && this.files[i].file_ext == ext){
						if(this.files[i].isDownload == 0){
							return 1;
						}else{
							return 2;
						}
					}
				};

				return 0;
			},
			delFile:function(md5_hash,ext){
				for (var i = 0; i < this.files.length; i++) {
					if((this.files[i].file_hash == md5_hash && this.files[i].file_ext == ext) || 
						(this.files[i].md5 == md5_hash && this.files[i].file_ext == ext)){
						this.files.splice(i,1);
						break;
					}
				};

				console.log(ext);
				console.log(md5_hash);
				console.log(this.files);
			},
			getFileMd5:function(file,callback){
				var blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
					file = file,
					chunkSize = 2097152, // Read in chunks of 2MB
					chunks = Math.ceil(file.size / chunkSize),
					currentChunk = 0,
					spark = new SparkMD5.ArrayBuffer(),
					fileReader = new FileReader();

				fileReader.onload = function(e) {
					//console.log('read chunk nr', currentChunk + 1, 'of', chunks);
					spark.append(e.target.result); // Append array buffer
					currentChunk++;

					if (currentChunk < chunks) {
						loadNext();
					} else {
						//console.log('finished loading');
						var md5 =  spark.end();
						//console.info('computed hash', md5); // Compute hash

						callback(md5);
					}
				};

				fileReader.onerror = function() {
					console.warn('oops, something went wrong.');
				};

				function loadNext() {
					var start = currentChunk * chunkSize,
						end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize;

					fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
				}

				loadNext();
			},
			setUploadFiles:function(filesData){
				for (var i = 0; i < filesData.length; i++) {
					var file_path = filesData[i].file_path;
					filesData[i].md5 = file_path.substring(file_path.lastIndexOf('/') + 1,file_path.lastIndexOf('.'));
				};

				this.files = filesData;
			},
			getUploadFiles:function(){
				var files = this.files;
				var f = new Array();
				for (var i = 0; i < files.length; i++) {
					if(files[i].file_path && files[i].file_hash){
						f.push(files[i]);
					}else{
						alert(files[i].file_name + " 上传失败！");
						$(".queue-file[data-hash="+files[i].md5+"]").remove();
					}
				};

				return f;
			},
			addFiles:function(filesData){
				if(!filesData){filesData = new Array();}
				var o = $(".uploadifive-queue"),pThis = this;

				if(this.setting.pTarget){
					o = this.setting.pTarget.find(".uploadifive-queue.scCtrlUploadFiles");
				}

				o.html('');

				if(filesData.length == 0){return;}

				var html = new Array();

				for (var i = 0; i < filesData.length; i++) {
					html += this.getQueueHtml(filesData[i]);
				};

				o.append(html);

				o.find(".uploadifive-queue-item").each(function(){
					$(this).addClass('complete').attr("id","uploadifive-sign-upload-file-"+(parseInt($(this).index())+1));
					pThis.bindFileClickDom($(this),$(this).attr("data-hash"),$(this).attr("data-ext"));
				});

				//this.bindCloseDom(o);
				this.setUploadFiles(filesData);
			},
			bindCloseDom:function(o){
				var pThis = this;
				o.on("click",".close",function(){
					$(this).parent().fadeOut(function() {
						pThis.delFile($(this).attr("data-hash"),$(this).attr("data-ext"));
						$(this).remove();
					});
				});
			},
			bindFileClickDom:function(target,md5,ext){
				var pThis = this;

				//删除点击
				target.on('click','.fileDel',function(event){
					event.preventDefault();
					event.stopPropagation();
					UiFramework.layerPackage().ew_confirm({
						title:"是否确认删除？",
						ok_callback:function(){
							target.remove();
							pThis.delFile(md5,ext);

							if (tools.isFunction(_setting.callback.delFileCallback)) {
								_setting.callback.delFileCallback.apply(pThis, [pThis,pThis.files]);
							}
						}
					});
				});
			},
			//检查文件上传状态
			checkUploadStatus:function(){
				var hasFileUploading = false;

				for (var i = 0; i < this.files.length; i++) {
					if(this.files[i].isDownload == 0){
						hasFileUploading = true;
						break;
					}
				};

				return hasFileUploading;
				//return this.hasFileUploading;
			}
		};

		//return uploadFile;
		return uploadFile.init(_setting);
	};

	//获取地理位置
	GenericFramework.prototype.location = function(_setting) {
		if (!this.wxJsdkConfig) {
			tools.console("请先初始化微信配置！", 1);
			return;
		}

		var location = {
			localtionUrl: "/wx/index.php?model=legwork&m=ajax&a=trans2address",
			setting: null,
			_setting: {
				target: null, //添加的jq dom对象
				callback: {
					beforeGetLocation: null,
					afterGetLocation: null,
					errorMsg: null
				}
			},
			isChecking: false,
			//检查配置
			checkParams: function(setting) {
				if (setting.target == null || !(setting.target instanceof jQuery)) {
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting;

				return _setting;
			},
			init: function(_setting) {
				_setting = this.checkParams(_setting);

				this.bindDom(_setting);
			},
			bindDom: function(_setting) {
				var obj = _setting.target,
					pThis = this;

				wx.ready(function() {
					obj.off().on("click", function() {
						if (pThis.isChecking) {
							_setting.callback.errorMsg.apply(pThis, ["正在搜索当中！"]);
							return;
						}

						pThis.isChecking = true;
						if (tools.isFunction(_setting.callback.beforeGetLocation)) {
							_setting.callback.beforeGetLocation.apply(pThis);
						}

						wx.getLocation({
							type: 'wgs84',
							success: function(res) {
								//var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
								//var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
								//var speed = res.speed; // 速度，以米/每秒计
								//var accuracy = res.accuracy; // 位置精度

								tools.ajaxJson({
									url: pThis.localtionUrl,
									data: {
										"data": {
											"lng": res.longitude,
											"lat": res.latitude
										}
									},
									//async:false,
									callback: function(result, status) {
										if (status == false && result == null) {
											return;
										}
										if (result.errcode != 0) {
											tools.console(result.errmsg, 1);
											alert(result.errmsg);
											_setting.callback.errorMsg.apply(pThis, [result]);
											return;
										}

										//alert(JSON.stringify(result));
										pThis.isChecking = false;

										if (tools.isFunction(_setting.callback.afterGetLocation)) {
											_setting.callback.afterGetLocation.apply(pThis, [result, res.longitude, res.latitude]);
										}
										return true;
									}
								});
							},
							cancel: function(res) {
								alert('用户拒绝授权获取地理位置');
							}
						});
					});
				});
			}
		};

		location.init(_setting);
	};

	//预览图片微信
	GenericFramework.prototype.previewImage = function(_setting) {
		if (!this.wxJsdkConfig) {
			tools.console("请先初始化微信配置！", 1);
			return;
		}

		var previewImage = {
			setting: null,
			_setting: {
				target: null,
				current: null,
				urls: null
			},
			//检查配置
			checkParams: function(setting) {
				if (setting.target == null || !(setting.target instanceof jQuery)) {
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting;

				return _setting;
			},
			init: function(_setting) {
				_setting = this.checkParams(_setting);

				this.bindDom(_setting);
			},
			bindDom: function(_setting) {
				var obj = _setting.target,
					pThis = this;
				try {
					wx.ready(function() {
						if (_setting.current && _setting.urls) {
							pThis.showImg(_setting.current, _setting.urls);
							return;
						}

						obj.off().on("click", function() {
							var selfUrl = $(this).find("img").attr("src"),
								arr = new Array();

							obj.each(function() {
								$(this).find("img").each(function() {
									arr.push($(this).attr("src"));
								});
							});

							/*wx.previewImage({
						      current: selfUrl,
						      urls: arr
						    });*/
							pThis.showImg(selfUrl, arr);
						});
					});
				} catch (e) {
					alert(e);
				}
			},
			showImg: function(current, urls) {
				wx.previewImage({
					current: current,
					urls: urls
				});
			}
		};

		previewImage.init(_setting);
	};

	//会话
	GenericFramework.prototype.chat = function(_setting) {
		if (!this.wxJsdkConfig) {
			tools.console("请先初始化微信配置！", 1);
			return;
		}

		var chat = {
			setting: null,
			_setting: {
				target: null,
				userIds: null,
				groupName: "" //会话名称。单聊时该参数不需要传。
			},
			//检查配置
			checkParams: function(setting) {
				if (!tools.checkJqObj(setting.target)) {
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting;

				return _setting;
			},
			init: function(_setting) {
				_setting = this.checkParams(_setting);

				this.bindDom(_setting);
			},
			bindDom: function(_setting) {
				var obj = _setting.target,
					pThis = this;
				try {
					wx.ready(function() {
						obj.off().on("click", function() {
							wx.openEnterpriseChat({
								userIds: _setting.userIds,
								groupName: _setting.groupName,
								success: function(res) {},
								error: function(res) {
									if (res.errMsg.indexOf('function not exist') > 0) {
										alert('微信版本过低，请升级后重试');
									} else {
										switch (res.errCode) {
											case '10001':
												msg = 'appid无效';
												break;
											case '10002':
												msg = '用户未关注企业号';
												break;
											case '10003':
												msg = '消息服务未开启';
												break;
											case '10004':
												msg = '用户不在消息服务可见范围';
												break;
											case '10005':
												msg = '存在无效的消息会话成员';
												break;
											case '10006':
												msg = '消息会话成员数不合法';
												break;
										}
										alert(msg);
									}
								}
							});
						});
					});
				} catch (e) {
					alert(e);
				}
			}
		};

		chat.init(_setting);
	};

	//非图片资源预览
	GenericFramework.prototype.viewFile = function(ext,hash){
		var viewFile = {
			init:function(ext,hash){
				tools.ajaxJson({
					url : http_domain + 'index.php?m=root_ajax&cmd=103&hash=' + hash + '&ext=' + ext,
					callback:function(result,status){
						try{
							//console.log(result.data.preview_url);
							window.location.href =  result.data.preview_url;
						}catch(e){
							UiFramework.layerPackage().fail_screen(result.errmsg);
						}
					}
				});
			}
		};

		viewFile.init(ext,hash);
	};
	
	//资源下载
	GenericFramework.prototype.fileDownload = function(url,params,ext){
		var fileDownload = {
			init:function(url,params){
				/*if(deviceKey == 1){
					url += '&is_push=1';
				}*/
				tools.ajaxJson({
					url : url,
					type:"GET",
					data:params,
					callback:function(result,status){
						UiFramework.layerPackage().unlock_screen();

						if(ext == "txt"){
							window.location.href = result.url;
							return;
						}

						if(status){
							if(result.errcode != 0){
								UiFramework.layerPackage().ew_alert({title:result.errmsg});
								return;
							}
							UiFramework.layerPackage().ew_alert({title:'附件已成功推送到信息框!'});
						}else{
							window.location.href = url;
						}
					}
				});
			}
		};

		fileDownload.init(url,params);
	};

	GenericFramework.prototype.evalWXjsApi = function(weixinJSBridge,jsApiFun){
		var evalWXjsApi = function(jsApiFun) {
            if (typeof weixinJSBridge == "object" && typeof weixinJSBridge.invoke == "function") {
                jsApiFun();
            } else {
                document.attachEvent && document.attachEvent("WeixinJSBridgeReady", jsApiFun);
                document.addEventListener && document.addEventListener("WeixinJSBridgeReady", jsApiFun);
            }
        }
	};

	//检查设备是否支持指纹识别
    GenericFramework.prototype.getSupportSoter = function(callback){
        var soter = {
            init: function() {
                wx.ready(function () {
                    wx.invoke("getSupportSoter", {},function(res){
                        var support_mode = res.support_mode;

                        if(callback && tools.isFunction(callback)){
                        	callback(support_mode == 1 || support_mode == 3 ? true : false,support_mode);
                        }
                    });
                });
            }
        };
        soter.init();
    };

    //指纹识别认证
    GenericFramework.prototype.requireSoter = function(callback){
    	var requireSoter = {
    		init: function() {
                wx.ready(function () {
					wx.invoke("requireSoterBiometricAuthentication", {
						"auth_mode": "0x1",
						"challenge": "sample_challenge",
						"auth_content": "请验证已有的指纹"
					}, function(res) {
						//alert(JSON.stringify(res));
						if (res.err_code == 0) {
							//检查use_mode
							//使用result_json和result_son_signature本地验签是否合法
							//使用所提供的后台接口将result_json和result_son_signature发送到微信企业号后台进行验签
							//处理验签结果
							callback(res,true);
						} else {
							/*var ret = res.err_msg;
							ret += " errCode: " + res.resultCode;
							alert(ret);*/
							callback(res,false);
						}
					});
				});
            }
    	};
		
		requireSoter.init();
    };

    //获取设备网络状态
    GenericFramework.prototype.getNetworkType = function(callback){
    	var getNetworkType = {
    		init:function(){
    			wx.getNetworkType({
				    success: function (res) {
				        var networkType = res.networkType; // 返回网络类型2g，3g，4g，wifi

				        callback(networkType);
				    }
				});
    		}
    	};

    	getNetworkType.init();
    };

    //拉取适用卡券列表并获取用户选择信息
    GenericFramework.prototype.chooseInvoice = function(timestamp,nonceStr,cardSign,callback){
    	var pThis = this;
    	var chooseInvoice = {
    		init:function(){
    			wx.ready(function(){
    				wx.invoke('chooseInvoice',{
    					timestamp: timestamp, // 卡券签名时间戳
					    nonceStr: nonceStr, // 卡券签名随机串
					    cardSign: cardSign, // 卡券签名
					    signType:'SHA1'
    				},function(res){
    					alert('已选择卡券：' + JSON.stringify(res));
				       	
				       	if(res.err_msg == "choose_invoice:ok"){
				       		var cardList = $.parseJSON(res.choose_invoice_info);
				       		var cardInfo = cardList[0];
				       		alert(cardInfo.card_id);
				       		alert(cardInfo.encrypt_code);

				       		 pThis.getCardInfo(cardInfo.card_id,cardInfo.encrypt_code,function(info){
					        	callback(info,res);
					        });
				       	}
    				});
    			});

				/*wx.chooseInvoice({
				    timestamp: timestamp, // 卡券签名时间戳
				    nonceStr: nonceStr, // 卡券签名随机串
				    cardSign: cardSign, // 卡券签名
				    signType:'SHA1',
				    success: function (res) {
        				alert('已选择卡券：' + JSON.stringify(res.cardList));
				        var cardList= res.cardList; // 用户选中的卡券列表信息
				        var cardInfo = cardList[0];

				        pThis.getCardInfo(cardInfo.card_id,card_id.encrypt_code,function(info){
				        	callback(info,res);
				        });

				         //[{'card_id':"",'encrypt_code':"",'app_id':''}]
				        // res.cardList = JSON.parse(res.cardList);
        				// encrypt_code = res.cardList[0]['encrypt_code'];
				    }
				});*/
    		}
    	};

    	chooseInvoice.init();
    };

    GenericFramework.prototype.getCardInfo = function(card_id,encrypt_code,callback){
    	tools.ajaxJson({
    		url:"/wx/index.php?model=expense&m=ajax&a=index&cmd=101",
    		data:{
    			"data":{
    				card_id:card_id,
    				encrypt_code:encrypt_code
    			}
    		},
    		callback:function(result,status){
    			alert(JSON.stringify(result));
    			/*if(result.errcode != 0){
    				return;
    			}*/

    			/*
    			var testTime = new Date().getTime();
				var result = {
					"errcode": 0,
					"errmsg": "ok",
					"card_id": "pjZ8Yt5crPbAouhFqFf6JFgZv4Lc",
					"begin_time": 1476068114,
					"end_time": 1476168114,
					"user_card_status": "EXPIRE",
					"openid": "obLatjnG4vRXJvSO8p914rSK8-Vo",
					"type": "广东省增值税普通发票",
					"payee": "测试-收款方",
					"detail": "测试-detail",
					"user_info": {
						"fee": 1100,
						"title": "灌哥发票",
						"billing_time": 1468322401,
						"billing_no": "hello",
						"billing_code": "world",
						"info": [{
							"name": "绿巨人",
							"num": 10,
							"unit": "吨",
							"fee": 5,
							"price": 4
						}],
						"accept": true,
						"fee_without_tax": 2345,
						"tax": 123,
						"detail": "项目",
						"pdf_url": "pdf_url",
						" reimburse_status": "INVOICE_REIMBURSE_INIT",
					},
					pdf_file:{
						file_name:"测试用"+testTime,
						md5:"asd41ad213a1d"+(testTime+10),
						file_path:"../asd41ad213a1d"+(testTime+10)+".jpg",
						file_ext:"jpg",
						file_size:"20000",
						file_hash:"asdadasdad"+testTime,
						isDownload:1 //是否下载完成
					}
				};
				*/

				result.pdf_file.isDownload = 1;
				result.pdf_file.md5 = result.pdf_file.file_path.substring(result.pdf_file.file_path.lastIndexOf("/")+1,result.pdf_file.file_path.lastIndexOf("."));

    			if(callback && tools.isFunction(callback)){
    				callback(result);
    			}
    		}
    	});
    };

    GenericFramework.prototype.wxCloseWindow = function(){
    	try{
			wx.closeWindow();
		}catch(e){
			console.log(e);
		}
    };

	return new GenericFramework();
});