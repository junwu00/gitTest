/*define([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/echarts',
], function($,tools,echarts) {
});*/

//UMD 通用式
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery', 'common:widget/lib/tools','common:widget/lib/echarts'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS之类的
        module.exports = factory(require('jquery'), require('tools'),require('echarts'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory(root.jQuery, root._);
    }
}(this, function ($, t, echarts) {
    var chartHelper = {
    	color:['#76BFE0','#FED819','#90BF70','#D25A5A','#4F8096','#655A89','#969694','#AFBFA7','#B2DCDA','#B1B9EE','#EEB1BA'],
		init:function(dom){
			var chart = echarts.init(dom);
			this.chart = chart;
			return chart;
		},
		setOption:function(chart,option,notMerge,lazyUpdate){
			if(!chart || !option){return;}

			chart.setOption(option,notMerge,lazyUpdate);
		},
		isLoading:function(chart,status,opt){
			status ? chart.showLoading("default",opt) : chart.hideLoading();
		},
		clear:function(chart){
			chart.clear();
		},
		getBaseOption:function(params,dataZoom,type){
			if(!params){return;}
			this.params = params;
			var pThis =  this;
			this.click = true;

			var option = {
			    title: {
			        text: ''
			    },
			    tooltip: {
			    	trigger: 'axis'/*,
			        axisPointer : {            // 坐标轴指示器,坐标轴触发有效
			            type : 'shadow'        // 默认为直线,可选为：'line' | 'shadow'
			        }*/
			    },
			    toolbox:{
			    	show: true,
			        feature: {
			            /*magicType: {
			            	type: ['line', 'bar']
			            },
			            restore: {},
			            saveAsImage: {},*/
			            myTool: {
			                show: true,
			                title: dataZoom ? '显示全部' : '显示部分',
			                icon: 'image://http://echarts.baidu.com/images/favicon.png',
			                onclick: function (){
			                	params.myTool(this);
			                	//var option = pThis.getBaseOption(pThis.params,!pThis.click);
                				//pThis.setOption(pThis.chart,option);
			                }
			            }
			        },
			        right:"20"
			    },
			    legend: {
			        data:[]
			    },
			    xAxis: [{
			    	boundaryGap : false,
			        data: []
			    }],
			    yAxis: [{}],
			    series: null
			};

			if(params.title){
				option.title.text = params.title;
			}
			if(params.legend){
				option.legend.data = params.legend;
			}
			if(params.xAxis){
				for (var i = 0; i < option.xAxis.length; i++) {
					option.xAxis[i].data = params.xAxis[i].data
				};
			}
			if(params.yAxis){
				for (var i = 0; i < option.yAxis.length; i++) {
					option.yAxis[i].data = params.yAxis[i].data
				};
			}
			if(params.series){
				option.series = params.series;
			}
			if(params.color){
				option.color = params.color;
			}

			if(params.tooltip && t.isFunction(params.tooltip)){
				option.tooltip.formatter =  function(data,ticket,callback) {
		            params.tooltip(data);
		        }
			}

			try{
				if(dataZoom){
					option.dataZoom = [
						{
				            id: 'dataZoomX',
				            type: 'slider',
				            xAxisIndex: [0],
				            filterMode: 'filter',
				            start:0,
				            end: 20,
				            show: false
				        },
			            {
			                type: 'inside',
			                start: 0,
			                end: 10
			            }
					];
				}
			}catch(e){
				alert(e);
			}

			//console.log(option);

			return option;
		},
		addMarkLine:function(obj,position){
			if(position === undefined || position === null) {
				position = "end";
			}

			obj.markLine = {
				label:{
					normal:{
						position:position
					}
				},
	            data: [
	                {type: 'average', name: '平均值'}
	            ]
	        };

	        return this;
		},
		addMarkPoint:function(obj){
			obj.markPoint = {
	            data: [
	                {type: 'max', name: '最大值'},
	                {type: 'min', name: '最小值'}
	            ]
	        };

	        return this;
		},
		bindClick:function(chart,callback){
            chart.on('click', function (params) {
                if (params.componentType === 'markPoint') {
                	callback('markPoint',params);
                    //console.log(params);
                    // 点击到了 markPoint 上
                    if (params.seriesIndex === 5) {
                        // 点击到了 index 为 5 的 series 的 markPoint 上。
                    }
                }
                else if (params.componentType === 'series') {
                    callback('series',params);
                    if (params.seriesType === 'graph') {
                        if (params.dataType === 'edge') {
                            // 点击到了 graph 的 edge（边）上。
                            callback('edge',params);
                        }
                        else {
                            // 点击到了 graph 的 node（节点）上。
                            callback('node',params);
                        }
                    }
                }
            });
		},
		lineToArea:function(obj){
			if(obj.type && obj.type == "line"){
				 obj.areaStyle = {normal: {}};
			}

			return obj;
		},
		areaToLine:function(obj){
			if(obj.type && obj.type == "line"){
				 obj.areaStyle = null;
			}

			return obj;
		},
		//基础配置框架
		baseOption:function(){
			return option = {
				title: {
			        text: '',
			        show:false,
			        textStyle:{
			        	fontSize:13
			        }
			    },
			    tooltip:{
			    },
			    legend: {
			        data:[],
			        left:0
			    },
			    series: null,
			    color:this.color
			};
		},
		//设置缩放配置
		setDataZoom:function(option,minNum,xLen,sCount){
			var percent = 70;

			if(minNum === undefined || minNum === null || sCount === undefined || sCount === null){
				return option;
			}

			try{
				percent = 100 * Math.floor(minNum / (sCount/xLen) ) / xLen;
			}catch(e){
				console.log(e);
			}

			option.dataZoom = [
				{
		            id: 'dataZoomX',
		            type: 'slider',
		            xAxisIndex: [0],
		            filterMode: 'filter',
		            start:0,
		            end: percent,
		            show: false,
		            zoomLock:true
		        },
	            {
	                type: 'inside',
	                start: 0,
	                end: percent,
	                zoomLock:true
	            }
			];

			return option;
		},
		useMyTool:function(option,use,status,chart,minNum,xLen,sCount){
			var pThis = this;
			if(!use){
				try{
					delete option.dataZoom;
					delete option.toolbox.myTool;
				}catch(e){}
			}else{
				option.status = status;
				option = this.setDataZoom(option,minNum,xLen,sCount);

				if(false){
					option.toolbox = {
				    	show: true,
				        feature: {
				            /*magicType: {
				            	type: ['line', 'bar']
				            },
				            restore: {},
				            saveAsImage: {},*/
				            myTool: {
				                show: (chart || false),
				                title: option.status ? '显示全部' : '显示部分',
				                icon: 'image://http://echarts.baidu.com/images/favicon.png',
				                onclick: function (){
				                	//params.myTool(this);
				                	
				                	if(option.status){
				                		delete option.dataZoom;
				                	}else{
				                		option = pThis.setDataZoom(option);
				                	}

				                	option.status = !option.status;

				                	pThis.setOption(chart,option,true);
				                }
				            }
				        },
				        right:"20"
				    };
				}
			}

			return option;
		},
		//图标撑满坐标
		xAxisBoundaryGap:function(option){
			if(option.xAxis && t.isArray(option.xAxis) && option.xAxis.length > 0){
				for (var i = 0; i < option.xAxis.length; i++) {
					option.xAxis[i].boundaryGap = false;
				};
			}else if(option.xAxis){
				option.xAxis.boundaryGap = false;
			}

			return option;
		},
		//横坐标竖列显示
		xAxisLabel:function(option){
			var obj = {
	            interval:0 ,
	            formatter:function(value, index){
	                var v = value.split('');
	                var arr = new Array();
	                var l = 1;

	                for (var i = 0; i < v.length; i++) {
	                	
	                    arr.push(v[i]);

	                    if(l % 4 == 0){
	                		arr.push("\n");
	                		l = 1;
	                	}else{
	                		l ++;
	                	}
	                };
	                return arr.join('');

	                /*for (var i = 0; i < v.length; i++) {
	                	if(i >= 3){
	                		arr.push("..");
	                		break;
	                	}
	                    arr.push(v[i]);
	                };
	                return arr.join('\n');*/
	            }
	        };
			if(option.xAxis && t.isArray(option.xAxis) && option.xAxis.length > 0){
				for (var i = 0; i < option.xAxis.length; i++) {
					option.xAxis[i].axisLabel = obj;
				};
			}else if(option.xAxis){
				option.xAxis.axisLabel = obj;
			}

			return option;
		},
		myOption:function(type,series,useBase,isApp){
			if(!series){return;}

			/* data
			[
                {value:335, name:'直接访问'},
                {value:310, name:'邮件营销'},
                {value:234, name:'联盟广告'},
                {value:135, name:'视频广告'},
                {value:1548, name:'搜索引擎'}
            ]
		
			*/

			var option = this.baseOption();
			option.series = series;

			if((type == "line" || type == "bar") && useBase){
				option = this.LineOrBarOptionChnage(option,isApp);
			}else if(type == "pie" && useBase){
				option = this.pieOptionChange(option,isApp);	
			}

			return option;
		},
		pieOptionChange:function(option,isApp){
			var arr = new Array();
			option.title.x = 'center'

			option.tooltip = {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    };

		    option.legend.orient = 'horizontal';
		    option.legend.top = '-100%';
		    //option.legend.show = false;
		    option.legend.left = 'center';
		    option.legend.textStyle  = {
		    	fontSize:12
		    };

		    if(option.series && t.isArray(option.series) && option.series.length > 0){
		    	for (var i = 0; i < option.series.length; i++) {
		    		var s = option.series[i];
		            if(s.type == "pie"){
			    		s.itemStyle = {
			                emphasis: {
			                    shadowBlur: 10,
			                    shadowOffsetX: 0,
			                    shadowColor: 'rgba(0, 0, 0, 0.5)'
			                },
			                normal:{ 
                                label:{ 
                                   show: true, 
                                   formatter: '{d}%' 
                                }, 
                                labelLine :{show:true}
                            } 
			            }
			            s.radius = '70%';
		            	s.center = ['50%', '55%'];

			            var label_forrmatter = s.label.normal.formatter;
			            
		            	s.label =  {
			                normal: {
			                    show: true,
			                    position: "outside"
			                }
			            };

			            if(label_forrmatter){
			            	s.label.normal.formatter = label_forrmatter;
			            }

		            	try{
			            	if(i == 0){
			            		for (var j = 0; j < s.data.length; j++) {
			            			if(s.data[j].name){
			            				arr.push(s.data[j].name);
			            			}
			            		};

			            		option.legend.data = arr;
			            	}
		            	}catch(e){
		            		
		            	}
		    		}
		    	};
		    }

		    option.grid = {
				top:0,
				bottom:0
				//containLabel:true //强制显示坐标轴
			};

			option.legendHoverLink = false;

			if(isApp){
				
			}

		    return option;
		},
		LineOrBarOptionChnage:function(option,isApp){
			//缩放配置
			option.tooltip = {
				formatter :  function(data,ticket,callback) {
	            	//params.tooltip(data);
	        	},
	        	trigger: 'axis'/*,
		        axisPointer : {            // 坐标轴指示器,坐标轴触发有效
		            type : 'shadow'        // 默认为直线,可选为：'line' | 'shadow'
		        }*/
			};

			option.xAxis = [{}];
			option.yAxis = {
    			boundaryGap:false,
                splitLine:{
                    lineStyle:{
                        color:"#E4E4E4"
                    }
                }
			};

			if(isApp){
				option.yAxis.offset = 21;
				option.grid = {
					left:20,
					right:20,
					//containLabel:true //强制显示坐标轴
				}
			}

		    return option;
		},
		getOption:function(params,option){
			var pThis = this;
			if(params.title){
				option.title.text = params.title;
			}
			if(params.legend){
				option.legend.data = params.legend;
			}
			if(params.xAxis){
				option.xAxis = params.xAxis;
			}
			if(params.yAxis){
				option.yAxis = params.yAxis;
			}
			if(params.color){
				option.color = params.color;
			}
			if(params.tooltip && t.isFunction(params.tooltip)){
				option.tooltip.formatter =  function(data,ticket,callback) {
		            params.tooltip(data,ticket,callback);
		        }
			}
			if(params.legendTitle && t.isFunction(params.legendTitle)){
				option.titleCount = 0;
				option.legend.formatter =  function(name) {
					var data = option.legend.data;

					if(option.titleCount == data.length){return;}
		            params.legendTitle(name,pThis.getLegendTitleHtml(name,option));
		        }
			}

			return option;
		},
		//图标上显示数值
		showNumber:function(option,indexArr,params){
			var obj = {
                normal: {
                    show: true,
                    position: 'top'
                }
            };

            if(params){
            	obj = params;
            }

			if(indexArr && t.isArray(indexArr)){
				for (var i = 0; i < indexArr.length; i++) {
					option.series[indexArr[i]].label = obj;
				};
			}

			if(indexArr == "noShow"){
				for (var j = 0; j < option.series.length; j++) {
					delete option.series[j].label;
				};
			}

			if(indexArr == "all"){
				for (var z = 0; z < option.series.length; z++) {
					option.series[z].label = obj;
				};
			}

			return option;
		},
		getLegendTitleHtml:function(name,option){
			var color = this.color;
			var data = option.legend.data;
			var html = new Array();

			html.push("<span class='legendTitle' data-click='0'>");
			html.push("<i style='background-color:"+color[option.titleCount%color.length]+"'></i>");
			html.push(name);
			html.push("</span>");

			if(option.titleCount == data.length){
				//option.titleCount = 0;
			}else{
				option.titleCount ++;
			}

			var dom = $($.trim(html.join('')));
			
			return dom;
		},
		bindPieClick:function(chart,name,dom){
			var pThis = this;
			dom.off().on('click',function(){
				var flag = $(this).attr("data-click");

				if(!parseInt(flag)){
					chart.dispatchAction({
					    type: 'legendUnSelect',
					    // 图例名称
					    name: name
					});
					$(this).addClass('unSelect');
					$(this).attr("data-click","1");
				}else{
					chart.dispatchAction({
					    type: 'legendSelect',
					    // 图例名称
					    name: name
					});
					$(this).removeClass('unSelect');
					$(this).attr("data-click","0");
				}
			});
		},
		pieAppOption:function(option){
			option.calculable = true;
	        option = {
	            baseOption:option,
	            media: [
	                {
	                    option: {
	                        series: [
	                            {
	                                radius: '50%',
	                                center: ['50%', '50%']
	                            }
	                        ]
	                    }
	                },
	                {
	                    query: {
	                        minAspectRatio: 1
	                    },
	                    option: {
	                        series: [
	                            {
	                                radius: '50%',
	                                center: ['50%', '50%']
	                            }
	                        ]
	                    }
	                },
	                {
	                    query: {
	                        maxAspectRatio: 1
	                    },
	                    option: {
	                        series: [
	                            {
	                                radius: '50%',
	                                center: ['50%', '50%']
	                            }
	                        ]
	                    }
	                },
	                {
	                    query: {
	                        maxWidth: 500
	                    },
	                    option: {
	                        series: [
	                            {
	                                radius: '60%',
	                                center: ['50%', '50%']
	                            }
	                        ]
	                    }
	                }
	            ]
	        };

	        return option;
		},
		//添加下载
		setDownLoadBtn:function(option,title,type){
			var saveAsImage = {};

			if(title){saveAsImage.title = title;}
			if(type){saveAsImage.type = type;}

			if(option.toolbox){
				option.toolbox.feature.saveAsImage = saveAsImage;
			}else{
				option.toolbox = {
					feature:{
						saveAsImage:saveAsImage
					}
				}
			}

			option.toolbox.right = 20;

			return option;
		}
	}
 
    return chartHelper;
}));