(function(window){
	
	/**
	 * 缓存类型 使用LocalStorage和SessionStorage （不使用cookie，数据保存在cookie会造成请求时传输效率降低）
	 */
	var s = {};
	
	//local存储对象
	s.localStorage = window.localStorage;
	//session存储对象
	s.sessionStorage = window.sessionStorage;
	//保存数据KEY
	s.key = '';
	//保存数据child-key数组
	s.ckey =[];
	//自动保存时间周期
	s.cycle_time = 5000;
	//自动保存状态,1：运行，0：停止
	s.cycle_state = 1; 
	//定时任务
	s.t = 0;
	/**
	 * 初始化方法 KEY 确保每个用户每个页面的KEY唯一
	 */
	s.init = function(opt){
		if( opt == 'undefined' || !opt ) return;
		if(typeof(opt.key) !== 'undefined' && opt.key){
			this.key = opt.key;
		}
		if(typeof(opt.ckey) !== 'undefined' && opt.ckey){
			this.ckey = opt.ckey;
		}
		if(typeof(opt.time) != 'undefined' && Number(opt.time)!=0){
			this.cycle_time = opt.time;
		}

		//设置初始化方法
		if(typeof(opt.init_data) == 'function'){
			this.fun_init_data = opt.init_data;
		}
		//设置保存方法
		if(typeof(opt.save_data) == 'function'){
			this.fun_save_data = opt.save_data;
		}
		//初始化数据
		var json_val = this.local_get(this.key);
		this.fun_init_data(json_val);
		this.keep_save();
	};
	
	/** 
	 * 默认的初始化数据方法
	 **/
	s.fun_init_data = function(json_val){
		if(json_val != 'undefined'){
			var val = JSON.parse(json_val);
			for(var i in val){
				$(i).val(val[i]);
			}
		}
	}

	/** 
	 * 默认的保存方法
	 **/
	s.fun_save_data = function(){
		var that = this;
		var val = {};
		for(var i=0; i< that.ckey.length;i++){
			val[that.ckey[i]] = $(that.ckey[i]).val();
		}
		return val;
	}

	/**
	 * 周期性保存方法
	 */
	s.keep_save = function(){
		var that = this;
		this.t = setTimeout(function(){
			var val = that.fun_save_data();
			if(that.cycle_state == 1){
				that.local_set(that.key, JSON.stringify(val));
				that.keep_save();
			}
		},that.cycle_time);
	}
	
	/**
	 * 根据KEY清除localStorage数据
	 */
	s.clear = function(key){
		clearTimeout(this.t);
		this.cycle_state = 0;
		if(key != undefined){
			this.localStorage.removeItem(key);
		}else{
			this.localStorage.removeItem(this.key);
			this.key = undefined;
		}
	}
	
	/**
	 * 清除同一个域名下所有的localStorage数据
	 */
	s.clearAll = function(){
		this.localStorage.clear();
	}
	
	/**
	 * 根据KEY获取localStorage缓存数据
	 * 获取空值时，不同浏览器有些差异，统一返回 null 
	 */
	s.local_get = function(key){
		var val = this.localStorage.getItem(key);
		if( val !== null && val !== undefined && val !== "" ){
			return val;
		}
		return null;
	};
	
	/**
	 * 根据KEY缓存localStorage数据
	 */
	s.local_set = function(key,val){
		if( key === undefined  || key === ""){
			return;
		}
		this.localStorage.removeItem(key);
		this.localStorage.setItem(key,val);
	};
	
	/**
	 * 根据KEY获取sessionStorage缓存数据
	 * 获取空值时，不同浏览器有些差异，统一返回 null 
	 */
	s.session_get = function (key){
		var val = this.sessionStorage.getItem(key);
		if( val !== null && val !== undefined && val !== "" ){
			return val;
		}
		return null;
	}
	
	/**
	 * 根据KEY缓存sessionStorage数据
	 */
	s.session_set = function (key,val){
		this.sessionStorage.removeItem(key);
		this.sessionStorage.setItem(key,val);
	}
	
	/**
	 * 根据KEY缓存sessionStorage数据
	 */
	s.session_clear = function(key){
		if(key != undefined){
			this.sessionStorage.removeItem(key);
		}else{
			this.sessionStorage.clear();
		}
	}
	
	window.$storage = s;
	
})(window);