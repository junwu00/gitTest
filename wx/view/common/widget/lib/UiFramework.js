define(['jquery','common:widget/lib/tools'], function($,tools) {
	function UIFramework() {

	}

	//按钮包
	UIFramework.prototype.btnPackage = function() {
		//checkBox zu
		var checkBoxBtn = {
			setting: null,
			_setting : {
				status : false, //初始状态 false 未选中 true 选中
				type:0, //0 滑动按钮 1 圆形按钮 2 方形按钮
				target:null, //添加的jq dom对象
				callback: {
					on:null, //打开的函数操作
					off:null //关闭的函数操作
				},
				cancelEvent:false,
				cancelAnimate:0 //取消动画
			},
			cacheSetting:null,
			//检查配置
			checkParams: function(setting) {
				if(setting.target == null || !(setting.target instanceof jQuery)){
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting;

				return _setting;
			},
			//添加checkBox Dom
			init: function(_setting) {
				_setting = this.checkParams(_setting);
				this.cacheSetting = _setting;
				_setting.target.append(this.getCheckBoxHtml(_setting));

				if(!_setting.cancelEvent){
					this.bindDom(_setting);
				}
			},
			//checkBox 相关HTML
			getCheckBoxHtml:function(_setting){
				var type = _setting.type;

				if(!tools.isInteger(type)){
					console.log('type 字段不是正整数！！！');
					return;
				}

				var html = new Array(),
				btnName = '',handleClass = '';

				switch(type){
					case 1: 
						btnName = 'circleBtn';
						handleClass = 'icon-ok';
						break;
					case 2: 
						btnName = 'squareBtn';
						handleClass = 'icon-ok';
						break;
					default:
						btnName = 'slideBtn';
				}

				html.push('<div class="'+ btnName + ' ' + (_setting.status == false ? 'off' : '') + '">');
				html.push('<i class="handle '+ handleClass +'">');
				html.push('</i>');
				html.push('</div>');

				return html.join('');
			},
			//绑定事件
			bindDom: function(_setting) {

				switch(_setting.type){
					case 0:
						this.bindSlideBtnDom(_setting);
						break;
					default:
						this.bidnCircleOrSquareBtnDom(_setting);
				}
				
			},
			//滑动事件
			bindSlideBtnDom:function(_setting){
				var self = this;

				_setting.target.on('click', '.slideBtn .handle', function() {
					var handle = $(this),
						btn = handle.parent();

					if(handle.is(':animated')){return;}

					if (btn.hasClass('off')) {
						if(_setting.cancelAnimate == 1){
							handle.css("left","21px");
							btn.removeClass('off');
							tools.apply(_setting.callback.on, self.setting.callback.on, [btn, self]);
							return;
						}
						
						handle.animate({
							'left': '21px',
						}, '100', function() {
							btn.removeClass('off');

							//self.setting.on.apply(self.on, btn, self);
							tools.apply(_setting.callback.on, self.setting.callback.on, [btn, self]);
						});
					} else {
						if(_setting.cancelAnimate == 1){
							handle.css("left","1px");
							btn.addClass('off');
							tools.apply(_setting.callback.off, self.setting.callback.on, [btn, self]);
							return;
						}

						handle.animate({
							'left': '1px'
						}, '100', function() {
							btn.addClass('off');

							//self.setting.off.apply(self.off, btn, self);
							tools.apply(_setting.callback.off, self.setting.callback.on, [btn, self]);
						});
					}
				});
			},
			//圆形和方形的事件
			bidnCircleOrSquareBtnDom:function(_setting){
				var self = this;

				_setting.target.on('click', '.circleBtn .handle,.squareBtn .handle', function() {
					var handle = $(this),btn = handle.parent();
					if (btn.hasClass('off')) {
						btn.removeClass('off');

						tools.apply(_setting.callback.on, self.setting.callback.on, [btn, self]);
					} else {
						btn.addClass('off');

						tools.apply(_setting.callback.off, self.setting.callback.on, [btn, self]);
					}
				});
			},
			changeStatus:function(){
				if(!this.cacheSetting){return;}

				var target = this.cacheSetting.target,
				type = this.cacheSetting.type,
				handle = target.find(".handle"),
				btn = handle.parent();

				if (btn.hasClass('off')){
					if(type == 0){handle.css("left","21px");}
					btn.removeClass('off');
				}else{
					if(type == 0){handle.css("left","1px");}
					btn.addClass('off');
				}
			},
			click:function(){
				if(!this.cacheSetting){return;}

				this.cacheSetting.target.find(".handle").click();
			},
			isClick:function(){
				if(!this.cacheSetting){return;}

				return !this.cacheSetting.target.find(".handle").parent().hasClass('off');
			}
		};

		return {
			checkBoxBtn : checkBoxBtn
		};
	};

	//列表菜单组件
	UIFramework.prototype.listMenuPackage = function(){
		var listCommon = {
			setting: null,
			_setting : {
				//列表菜单配置
				target:null, //添加的jq dom对象
				menu:{ //配置头部插件菜单
					status:true, //状态 false 取消 true 启用
					contents:null, //数组形式 
					//数组说明：name:菜单名称，activekey：当前菜单激活的标志，other_data：其他配置项
					//{'name':'运行中','activekey':3,'other_data':{'name1':'12123','name2':'aa'}}
					activekey:null,
					autoClick:true //初始化的时候自动点击 false 取消 true 启用
				},
				sort:{ //配置头部插件排序
					status:false, //状态 false 取消 true 启用
					contents:null //数组形式 或者 单个字串 （最少一个）
					//name:排序中文名，sort_name：排序名 
					//{'name':'时间','sort_name':'time'}
				},
				search:{ //搜索项配置
					status:false //状态 false 取消 true 启用
				},
				callback: {
					onMenuClick:null, //点击菜单时回调函数
					onSortClick:null, //点击排序时回调函数
					onSearchClick:null //搜索按钮回调函数
				},
				//列表空内容 参数
				noRecord : {
					target:null,
					imgSrc : null,//图片地址
					title:'',//内容标题
					desc:null //内容说明，数组形式 或者 单个字串
				}
			},
			//数据
			target : null,
			menuObj : null,
			menu_contents : null,
			activekey : null,
			sortObj : null,
			sortContentsObj : null,
			sort_contents : null,
			searchObj : null,

			//检查配置
			_checkParams: function(setting) {
				if(setting.target == null || !(setting.target instanceof jQuery)){
					console.log('错误的jq对象');
					return;
				}

				var _setting = tools.clone(this._setting);
				$.extend(true, _setting, setting);

				this.setting = setting; //保存原有配置

				return _setting;
			},
			init:function(_setting){
				_setting = this._checkParams(_setting);

				this.cacheSetting = _setting;

				_setting.target.append(this._getLitMenuHtml(_setting));

				//初始化没有数据列表的情况
				this.noRecord(_setting);

				if(_setting.menu.autoClick){
					this.activeKeyClick();
				}
			},
			//组装菜单HTML
			_getLitMenuHtml:function(_setting){
				this._targetHtml(_setting);
			},
			_targetHtml:function(_setting){
				var menu = _setting.menu, menu_contents = menu.contents, activekey = menu.activekey,
				sort = _setting.sort, sort_contents = sort.contents,
				search = _setting.search;

				var html = new Array();

				html.push('<div class="head">');

				try{
					//头部菜单组装
					if(menu.status && tools.isArray(menu_contents) && menu_contents.length > 0){
						html.push('<div class="head-menu">');

						html.push(this._init_menu(menu_contents, activekey));

						//排序组装
						if(sort.status && sort_contents && sort_contents.length > 0){
							html.push(this._init_sort(sort_contents));
						}

						html.push('</div>');
					}
				}catch(e){
					console.log('menu 或者 sort 配置出错');
				}

				//搜索组装
				if(search.status){ html.push(this._init_search()); }

				html.push('</div>');

				_setting.target.html('').append(html.join(''));

				_setting.target.css({'padding-top':_setting.target.find('.head').height() + 'px'});

				//保存数据
				this.target = _setting.target;
				this.menuObj = _setting.target.find('.head-menu');
				this.menu_contents = menu_contents;
				this.activekey = activekey;

				//绑定菜单事件
				this._bindMenuDom(_setting);

				if(sort.status){
					this.sortObj = _setting.target.find('.head—sort');
					this.sortContentsObj = $('#head—sort-screen');
					this.sort_contents = sort_contents;

					this.sort_select = {
						sort_name:sort_contents[0].sort_name,
						sort_type:sort_contents[0].sort_type,
						sort_id:sort_contents[0].sort_id
					};
					//绑定排序事件
					this._bindSortDom(_setting);
				}

				if(search.status){
					this.searchObj = _setting.target.find(".search-div");

					//绑定搜索事件
					this._bindSearchDom(_setting);
				}
			},
			//初始化菜单menu menu:obj配置对象
			_init_menu:function(menu_contents, activekey){
				var contents = menu_contents,menuHtml = new Array(),active = '';

				menuHtml.push('<ul>');

				for (var i = 0; i < contents.length; i++) {
					var ele = contents[i],data_str = '';

					if (typeof ele.other_data === "object") {
						for (var j in ele.other_data) {
							data_str += ' data-' + j + '="' + ele.other_data[j] + '"';
						}
					}

					//初始化选中标签
					if((!activekey && i == 0) || (activekey && ele.activekey == activekey)){
						active = 'class = "active"';
						//当前激活菜单数据
						this.activeMenuData = ele;
					}else{
						active = '';
					}

					menuHtml.push('<li data-activekey="' + ele.activekey + '" data-name="' + ele.name + '"' 
						+ data_str + ' ' + active + '>');
					menuHtml.push('<a href="javascript:void(0);">' + ele.name + '</a>');
					menuHtml.push('</li>');
				}

				menuHtml.push('</ul>');

				//输出MenuHtml字串
				return menuHtml.join('');
			},
			//初始化排序
			_init_sort: function(sort_contents){
				this._init_sort_contents(sort_contents);

				var sortHtml = new Array();

				sortHtml.push('<span class="head—sort">');
				sortHtml.push('<i is-sort-icon="true" class="asc icon-arrow-up hide"></i>');
				sortHtml.push('<i is-sort-icon="true" class="desc icon-arrow-down hide"></i>');
				sortHtml.push('<i class="nosort icon-align-justify"></i>');
				sortHtml.push('</span>');

				return sortHtml.join('');
			},
			_init_sort_contents: function(sort_contents) {
				var sortContentsHtml = new Array();
				sortContentsHtml.push('<div id="head—sort-screen" class="hide">');
				sortContentsHtml.push('<span><i class="icon-caret-up"></i></span>');
				sortContentsHtml.push('<div>');
				sortContentsHtml.push('<ul>');

				if(!tools.isArray(sort_contents)){
					sort_contents = [sort_contents];
				}

				for (var i = 0; i < sort_contents.length; i++) {
					sortContentsHtml.push('<li sort_name="' + sort_contents[i].sort_name + '" sort_id="'+sort_contents[i].sort_id+'"><span>' + sort_contents[i].name + '</span>');
					sortContentsHtml.push('<span sort-type="asc" class="hide sort-icon sort-icon-asc"><i class="icon-arrow-up"></i></span>');
					sortContentsHtml.push('<span sort-type="desc" class="hide sort-icon sort-icon-desc"><i class="icon-arrow-down"></i></span></li>');
				}

				sortContentsHtml.push('</ul>');sortContentsHtml.push('</div>');sortContentsHtml.push('</div>');

				$('body').append(sortContentsHtml.join(''));
			},
			//初始化搜索
			_init_search: function(){
				var searchHtml = new Array();

				searchHtml.push('<div class="search-div">');
				searchHtml.push('<form>');
				searchHtml.push('<input AUTOCOMPLETE ="off" type="search"  placeholder="搜索">');
				searchHtml.push('<span class="hide"><i class="icon-remove-sign"></i></span>');
				searchHtml.push('</form>');
				searchHtml.push('</div>');

				return searchHtml.join('');
			},
			_bindMenuDom:function(_setting){
				var self = this;
				this.menuObj.on('click','ul li',function(){
					if($(this).hasClass('active')){ return; }

					$(this).addClass('active').siblings().removeClass('active');
					var index = $(this).index(),menuData = self.menu_contents[index];
					self.activeMenuData = menuData;

					/*tools.apply(
						_setting.callback.onMenuClick,
						self.setting.callback.onMenuClick, 
						[menuData, $(this)]
					);*/
					_setting.callback.onMenuClick.apply(self,[menuData, $(this),self]);
				});
			},
			_bindSortDom:function(_setting){
				var self = this, sortObj = this.sortObj, sortContentsObj = this.sortContentsObj;

				sortObj.off().on('click',function(){
					sortContentsObj.removeClass('hide');
				});

				sortContentsObj.on("click",function(e){
					sortContentsObj.addClass('hide');
				});

				sortContentsObj.on('click','div ul li',function(){
					var pThis = $(this),siblingsDom = pThis.addClass('sortactive').siblings();

					siblingsDom.removeClass('sortactive');

					var sort_id = pThis.attr("sort_id");

					if(sort_id !=0){
						if(pThis.find('.sort-icon:not(.hide)').length == 0){
							pThis.find('.sort-icon:first').removeClass('hide');
						}else{
							pThis.find('.sort-icon.hide').removeClass('hide')
							.siblings('.sort-icon').addClass('hide');
						}
					
						siblingsDom.find('.sort-icon').addClass('hide');
					}else{
						siblingsDom.find('.sort-icon').addClass('hide');
						self.resetSort();
					}	

					var sort_name = pThis.attr('sort_name');
					var sort_type = pThis.find('.sort-icon:not(.hide)').attr('sort-type');

					sortContentsObj.addClass('hide');

					sortObj.find('i[is-sort-icon="true"].'+ sort_type +'').removeClass('hide')
					.siblings('i[is-sort-icon="true"]').addClass('hide');

					self.sort_select = {sort_name:sort_name,sort_type:sort_type,sort_id:sort_id};

					tools.apply(
						_setting.callback.onSortClick,
						self.setting.callback.onSortClick, 
						[{sort_name:sort_name,sort_type:sort_type,sort_id:sort_id}]
					);
				});
			},
			_bindSearchDom:function(_setting){
				var self = this, 
				searchObj = this.searchObj, formDom = searchObj.find('form'),
				closeDom = formDom.find('span'), inputDom = formDom.find('input');

				inputDom.on('keyup',function(){
					if ($.trim($(this).val()).length == 0) {
						closeDom.addClass('hide');
					} else {
						closeDom.removeClass('hide');
					}
				});

				closeDom.on('click',function() {
					$(this).addClass('hide');
					inputDom.val('');
				});

				formDom.on('submit',function(e) {
					 e.preventDefault();

					var key = $.trim(inputDom.val());

					tools.apply(
						_setting.callback.onSearchClick,
						self.setting.callback.onSearchClick, 
						[key, $(this)]
					);
				});
			},
			resetSort:function(){
				this.sortObj.find('i[is-sort-icon="true"]').addClass('hide');
			},
			//重置sort排序
			resetSortData:function(data){
				this.resetSort();
				var sortContentsObj = this.sortContentsObj;
				sortContentsObj.remove();
				this._init_sort_contents(data);
				this.sortContentsObj = $("body").find("#head—sort-screen");
				this.sort_contents = data;
				this.sort_select = {
					sort_name:data[0].sort_name,
					sort_type:data[0].sort_type,
					sort_id:data[0].sort_id
				}
				this._bindSortDom(this.cacheSetting);
			},
			getSelectSort:function(){
				return this.sort_select;
			},
			//没有列表数据方法
			noRecord : function(_setting){
				var noRecord = _setting.noRecord,
				target = noRecord.target,imgSrc = noRecord.imgSrc,
				title = noRecord.title,desc = noRecord.desc

				if(noRecord.target == null || !(noRecord.target instanceof jQuery)){
					console.log('错误的jq对象');
					return;
				}

				var html = new Array();

				html.push('<div class="has-no-record">');
				if (imgSrc) { html.push('<img alt="" src="' + imgSrc + '">'); }
				html.push('<p class="p1">' + ( title ? title : '暂时没有内容') + '</p>');

				//如果desc是个数组就循环添加 否则 单条添加
				if(!tools.isArray(desc)){ desc = [desc]; }

				for (var i = 0; i < desc.length; i++) {
					html.push('<p class="p2">' + desc[i] + '</p>');
				}
				
				html.push('</div>');

				target.html(html.join(''));
			},
			//对外提供的方法
			//获取menu菜单数据
			getMenuData:function(){
				return this.menu_contents;
			},
			//获取激活菜单信息,返回节点信息
			getActiveMenuData:function(){
				return this.activeMenuData;
			},
			//获取排序数据
			getSortData:function(){
				return this.sort_contents;
			},
			//获取搜索信息
			getSearchValue:function(){
				return $.trim(this.searchObj.find('input').val());
			},
			//设置搜索信息
			setSearchValue:function(key){
				this.searchObj.find('input').val(key);
				if($.trim(key) == ""){
					this.searchObj.find("span").addClass('hide');
				}else{
					this.searchObj.find("span").removeClass('hide');
				}
			},
			//设置排序信息
			setSort: function(sort_obj) {
				var sort_name = sort_obj.sort_name;
				var sort_type = sort_obj.sort_type;

				this.sortObj.find('i[is-sort-icon="true"].'+ sort_type +'').removeClass('hide')
					.siblings('i[is-sort-icon="true"]').addClass('hide');

				var li = this.sortContentsObj.find('div ul li');
				li.find('.sort-icon').addClass('hide');
				li.removeClass('sortactive');

				li.each(function(index, val) {
					if($(this).attr('sort_name') == sort_name){
						$(this).addClass('sortactive');

						$(this).find('.sort-icon').each(function() {
							if (sort_type == $(this).attr('sort-type')) {
								$(this).removeClass('hide');
							}
						});

						return;
					}
				});
			},
			//选中项点击
			activeKeyClick:function(){
				this.menuObj.find("ul li.active").removeClass('active').click();
			},
			showRecord:function(){
				$(".has-no-record").removeClass('hide');
			}
		}

		return listCommon;
	}

	//按钮菜单操作组件
	UIFramework.prototype.btnMenuPackage=function(){
		//底部弹出操作
		/*
		 * @opt参数对象{menus:[{id:1,name:'',menu_data:{},fun:aa,icon_html:'<span></span>'}],data:{}]}
		 * @opt.menus操作项数组
		 * @opt.data:这个底部弹出操作的相关信息
		 * @opt.menus[i].id：操作项id,唯一性
		 * @opt.menus[i].name:操作项名字
		 * @opt.menus[i].icon_html:操作项icon:如<span class="icon icon-eye-open"></span>,参考Font Awesome字体图标
		 * @opt.menus[i].menu_data:当前操作的其他数据
		 * @opt.menus[i].fun:点击时该操作项时的回调函数function(el,menu,data){}
		 * 
		 */
		var bottom_menu_action = {
				init:function(opt){
					var data_html = '';
					if(opt.data){
						for(var k in opt.data){
							data_html += ' data-' + k +'="' + opt.data[k] +'"';
						}
					}
					var colorThml = ''
					if(opt.color){
						colorThml = 'style="color: ' + opt.color + ';"'
					}
					var html = '';
					html += '<div id="bottom_menu_action_wrap">';
					html += '<div id="bottom_menu_action" '+ data_html +'>';
					for(var i = 0; i < opt.menus.length; i++){
						var menu = opt.menus[i];
						var menu_data_html = '';
						if(menu.menu_data){
							for(var k in menu.menu_data){
								menu_data_html += ' data-' + k +'="' + menu.menu_data[k] +'"';
							}
						}
						if(menu.icon_html){
							html += '<div ' + colorThml + ' data-id="'+ menu.id +'" data-name="'+ menu.name +'" '+ menu_data_html +'">'+menu.icon_html+menu.name+'</div>';
						}else{
							html += '<div ' + colorThml + ' data-id="'+ menu.id +'" data-name="'+ menu.name +'" '+ menu_data_html +'">'+menu.name+'</div>';
						}
					}
					html += '<div ' + colorThml + ' class="bottom_menu_action_del" data-del="true"><div>取消</div></div>'
						html += '</div></div>';
					if($('body').children('#bottom_menu_action_wrap').length > 0){
						$('body').children('#bottom_menu_action_wrap').remove();
					}
					$('body').append(html);
					bottom_menu_action.init_action(opt);
					bottom_menu_action.init_hide();
				},
				init_hide:function(){
					$('#bottom_menu_action_wrap').click(function(){
						$(this).remove();
					})
				},
				init_action:function(opt){
					$('#bottom_menu_action').children().click(function(e){
						if($(this).attr("data-del") == undefined || !$(this).attr("data-del") == "true"){
							e.stopPropagation();
							var id = $(this).attr('data-id');
							for(var i = 0; i < opt.menus.length; i++){
								var menu = opt.menus[i];
								if(id == menu.id){
									if (typeof(menu.fun) === 'function') {
										menu.fun(this,menu,opt.data);
									}
									break;
								}
							}
						}
					});
				},
				hide:function(){
					$('#bottom_menu_action_wrap').remove();
				}
		};
		
		return bottom_menu_action;
	};
	
	//滚动加载操作组件
	UIFramework.prototype.scrollLoadingPackage=function(){
		//滚动加载??需改造，y向滚动最好采用最外层的滚动,浏览器自带隐藏滚动条效果
		/*
		 * @opt参数对象{scroll_target:$(),target:$(),fun:function(){}}
		 * @opt.scroll_target:滚动对象
		 * @opt.target:操作对象
		 * @opt.fun:滚动到底部回调函数
		 * 
		 */
		var scroll_load_obj = {
			init:function(opt){
				var pThis = this;
				$(opt.scroll_target).attr('is_scroll_end','false');
				$(opt.scroll_target).attr('is_scroll_loading','false');
				var sTimer;
				$(opt.scroll_target).unbind('scroll').scroll(function scrollHandler(e){
					clearTimeout(sTimer);
					 //sTimer = setTimeout(function() {
						 var c = $(opt.scroll_target)[0].scrollHeight;
						 var t = $(opt.scroll_target).scrollTop();
						 var h = $(opt.scroll_target)[0].clientHeight;
						 var is_scroll_end = $(opt.scroll_target).attr('is_scroll_end') == 'true' ? true : false;
						 var is_scroll_loading = $(opt.scroll_target).attr('is_scroll_loading') == 'true' ? true : false;
						 if(t + h +200 > c && !is_scroll_end && !is_scroll_loading){
							 //scroll_load_obj.add_scroll_waiting(opt.target);
							 $(opt.scroll_target).attr('is_scroll_loading','true');
							 if (typeof(opt.fun) === 'function') {
								 opt.fun(pThis,e);
							 }
						 }
					 //},100);
				});
			},
			scroll_load_end:function(scroll_target){
				$(scroll_target).attr('is_scroll_end','true');
			},
			add_scroll_waiting:function(target,before){
				var html = '<div id="scroll_loading"><div><span class="an-1"></span><span class="an-2"></span><span class="an-3"></span></div></div>';
				if($(target).children('#scroll_loading').length == 0){
					if(before){
						$(target).before(html);
						return;
					}
					$(target).append(html);
				}
			},
			remove_scroll_waiting:function(target){
				if($(target).children('#scroll_loading').length > 0){
					$(target).children('#scroll_loading').remove();
				}
			},
			set_is_scroll_loading_false:function(scroll_target){
				 $(scroll_target).attr('is_scroll_loading','false');
			}
		};
		
		return scroll_load_obj;
	};
	
	//遮罩相关载操作组件
	UIFramework.prototype.layerPackage=function(){
		//隐藏页面loading遮罩
		var hide_mask_screen_head = function(){
			$('#mask_screen_head').addClass('hide');
		};
		//显示页面loading遮罩
		var show_mask_screen_head = function(){
			$('#mask_screen_head').removeClass('hide');
		};
		//锁屏遮罩
		/*
		 * @opt:遮罩是显示的信息参数
		 * @opt.bg_color:背景颜色值
		 * @opt.showLoading:是否显示loading
		 * @opt.msg:文案
		 */
		var lock_screen = function(opt){
			var bgColor = null;
			var showLoading = true;
			var msg = '';
			if(opt && opt.bgColor != null && opt.bgColor != undefined){
				bgColor = opt.bgColor;
			}
			if(opt && opt.showLoading != null && opt.showLoading != undefined){
				showLoading = opt.showLoading;
			}
			if(opt && opt.msg){
				msg = opt.msg;
			}
			this.unlock_screen();
			var inner_html = '';
			if(bgColor){
				inner_html += '<div id="lock-screen" style="background:'+bgColor+';">';
			}else{
				inner_html += '<div id="lock-screen">';
			}
			var colorThml = ''
			if(opt && opt.color){
				colorThml = 'style="background: ' + opt.color + ';"'
			}
			if(showLoading){
				inner_html += '<div><span ' + colorThml + ' class="an-1"></span><span ' + colorThml + ' class="an-2"></span><span ' + colorThml + ' class="an-3"></span></div>';
			}
			if(msg){
				inner_html += '<p>' + msg + '</p>';
			}
			inner_html += '</div>';
			$('body').append(inner_html);
		};
		//取消锁屏遮罩
		var unlock_screen = function(){
			if ($('#lock-screen').length > 0) {
				$('#lock-screen').remove();
			}
		};
		//成功提示窗口
		/*
		 * @msg:成功提示的信息,默认成功
		 * @hideTime:显示多长时间隐藏，默认1000毫秒
		 */
		var success_screen = function(msg,hideTime){
			msg = msg || '成功';
			var inner_html = '<div id="success_screen"><div><i class="icon icon-ok"></i><p>' + msg + '</p></div></div>';
			$('body').append(inner_html);
			hideTime = hideTime == undefined ? 1000 : hideTime;
			setTimeout(function(){
				$('#success_screen').remove();
			},hideTime);
		};
		//失败提示窗口
		/*
		 * @msg:成功提示的信息,默认成功
		 * @hideTime:显示多长时间隐藏，默认1000毫秒
		 */
		var fail_screen = function(msg,hideTime){
			msg = msg || '失败';
			var inner_html = '<div id="fail_screen"><div><i class="icon icon-remove"></i><p>' + msg + '</p></div></div>';
			$('body').append(inner_html);
			hideTime = hideTime == undefined ? 1500 : hideTime;
			setTimeout(function(){
				$('#fail_screen').remove();
			},hideTime);
		};
		
		//alert弹窗
		/*
		 * @opt:参数对象{title:"",desc:"",action_desc:"",callback:function(){}}
		 * @opt.title:alert的显示标题，默认"网页显示"
		 * @opt.desc:alert的内容，默认""
		 * @opt.action_desc:操作按钮显示文本，默认"确定"
		 * @opt.callback:确定按钮回调函数
		 * @opt.initCallback:出事回调函数
		 */
		var ew_alert = function(opt){
			$('body').find('div.alert_screen').remove();
			opt = opt || {};
			var title = opt.title || '网页显示';
			var desc = opt.desc || '';
			var action_desc = opt.action_desc || '确定';
			var top = opt.top ? "style='top:"+ opt.top + "%'" : "";
			var inner_html = '<div class="alert_screen"><div '+top+'><p class="alert_title">'+ title +'</p><p class="alert_desc">'+ desc +'</p><p class="alert_action">'+ action_desc +'</p></div></div>';
			$('body').append(inner_html);
			$('body').find('div.alert_screen').find('p.alert_action').click(function(){
				$(this).parent().parent().remove();
				if (typeof(opt.callback) === 'function') {
					opt.callback();
				}
			});
			
			if (typeof(opt.initCallback) === 'function') {
				opt.initCallback();
			}
		};
		
		//confirm确认弹窗
		/*
		 * @opt:参数对象{title:"",desc:"",ok_text:"",cancel_text:"",cancel_callback:function(){},ok_callback:function(){}}
		 * @opt.title:confirm确认弹窗标题，默认"确定"
		 * @opt.desc:confirm确认弹窗标题内容，默认""
		 * @opt.ok_text:确定操作按钮文本，默认"确定"
		 * @opt.cancel_text:取消操作按钮文本，默认"取消"
		 * @opt.cancel_callback：确认回调函数
		 * @opt.ok_callback：取消回调函数
		 * @opt.ok_remove：1 删除 0 不删除
		 */
		ew_confirm = function(opt){
			$('body').find('div.confirm_screen').remove();
			opt = opt || {};
			var title = opt.title || '';
			var desc = opt.desc || '';
			var ok_text = opt.ok_text || '确定';
			var cancel_text = opt.cancel_text || '取消';
			var ok_remove = (opt.ok_remove === undefined || opt.ok_remove === null) ? 1 :opt.ok_remove;
			var inner_html = '<div class="confirm_screen"><div><p class="confirm_title">'+ title +'</p><div class="confirm_desc">'+ desc +'</div><div class="confirm_action"><p class="confirm_cancle">'+ cancel_text +'</p><p class="confirm_ok">'+ ok_text +'</p></div></div></div>';
			$('body').append(inner_html);
			$('body').find('div.confirm_screen').find('p.confirm_cancle').click(function(){
				$(this).parent().parent().parent().remove();
				if (typeof(opt.cancel_callback) === 'function') {
					opt.cancel_callback();
				}
			});
			$('body').find('div.confirm_screen').find('p.confirm_ok').click(function(){
				var selfObj = $(this).parent().parent().parent();
				if(ok_remove){
					selfObj.remove();
				}
				if (typeof(opt.ok_callback) === 'function') {
					opt.ok_callback($('body').find('div.confirm_screen'),selfObj);
				}
			});

			if(!opt.callback){return;}
			opt.callback($('body').find('div.confirm_screen'));
		};
		
		//输入信息确认提交插件
		/*
		 * @opt:参数对象{default_t:'',placeholder:'',must_input:,ok_text:"",cancel_text:"",ok_callback :function(value){},cancel_callback:function(){}}
		 * @opt.default_t:默认填充内容
		 * @opt.placeholder:文本输入框的placeholder值
		 * @opt.must_input:是否必须输入true/false
		 * @opt.ok_text:确定操作按钮的文本展示，默认"确定"
		 * @opt.cancel_text：取消操作按钮的文本展示，默认"取消"
		 * @opt.ok_callback：确认回调函数functon(value){};value为文本框内容
		 * @opt.cancel_callback：取消回调函数
		 * 还需解决js生成节点focus()弹出浮动键盘问题。
		 * 使用页面需要加上已下代码,body的子节点
		 * <div id="comfirm_input" class="comfirm_input hide">
		 * <div class="content">
		 * <textarea id="comfirm_input_textarea" placeholder=""></textarea>
		 * </div>
		 * <div class="action">
		 * <button id="comfirm_input_cancel" class="nb-btn nb-btn-green cancel_b">取消</button><button id="comfirm_input_ok" class="nb-btn nb-btn-green ok_b">确定</button>
		 * </div></div>
		 */
		var ew_confirm_input = {
			init:function(opt){
				$('#comfirm_input').removeClass('hide');
				var default_t = opt.default_t || '';
				var placeholder = opt.placeholder || '';
				var ok_text = opt.ok_text;
				var cancel_text = opt.cancel_text;
				$('#comfirm_input_cancel').html(cancel_text);
				$('#comfirm_input_ok').html(ok_text);
				$("#comfirm_input_textarea").attr('placeholder',opt.placeholder);
				$("#comfirm_input_textarea").focus().val(default_t);
				ew_confirm_input.must_input(opt);
				ew_confirm_input.init_action(opt);
			},
			must_input:function(opt){
				console.log(opt);
				var must_input = opt.must_input || false;
				if(must_input){
					var textarea_value = $.trim($('#comfirm_input_textarea').val());
					if(textarea_value.length == 0){
						$('#comfirm_input_ok').attr('disabled','disabled');
						$('#comfirm_input_ok').addClass('nb-btn-green-dis');
						$('#comfirm_input_ok').removeClass('nb-btn-green');
					}
					$('#comfirm_input_textarea').off('keyup').on('keyup',function(){
						if($.trim($(this).val()).length > 0){
							$('#comfirm_input_ok').removeAttr('disabled');
							$('#comfirm_input_ok').addClass('nb-btn-green');
							$('#comfirm_input_ok').removeClass('nb-btn-green-dis');
						}else{
							$('#comfirm_input_ok').attr('disabled','disabled');
							$('#comfirm_input_ok').addClass('nb-btn-green-dis');
							$('#comfirm_input_ok').removeClass('nb-btn-green');
						}
					});
				}
			},
			init_action:function(opt){
				$('#comfirm_input_cancel').off('click').on('click',function(){
					ew_confirm_input.hide();
					if(typeof(opt.cancel_callback) === 'function'){
						opt.cancel_callback();
					}
				});
				$('#comfirm_input_ok').off('click').on('click',function(){
					if(typeof(opt.ok_callback) === 'function'){
						var textarea_value = $('#comfirm_input_textarea').val();
						opt.ok_callback(textarea_value);
					}
				});
			},
			hide:function(){
				$("#comfirm_input").addClass('hide');
			}
		};
		
		return{
			hide_mask_screen_head:hide_mask_screen_head,//隐藏loadding遮罩
			show_mask_screen_head:show_mask_screen_head,//显示loadding遮罩
			lock_screen:lock_screen,//数据交互时屏幕遮罩
			unlock_screen:unlock_screen,//隐藏屏幕遮罩
			success_screen:success_screen,//成功提示
			fail_screen:fail_screen,//失败提示
			ew_alert:ew_alert,//alert弹窗
			ew_confirm:ew_confirm,//confirm弹出
			ew_confirm_input:ew_confirm_input//confirm inout弹出
		}
	};
		
	//菜单初始化
	UIFramework.prototype.menuPackage={
		init:function(){
			if($('#bottom-menu').length == 0){
				return;
			}
			$('#bottom-menu').find('a.dropdown-toggle').click(function(event){
				event.stopPropagation();
				var ul = $(this).parent().children('ul');
				var height = 0-ul.height();
				if($(this).hasClass('open')){
					$('#bottom-menu').find('a.dropdown-toggle').removeClass('open');
					$('#bottom-menu').find('a.dropdown-toggle').parent().children('ul').css({'display':'none'});
					ul.css({'top':'0px','display':'none'});
					$(this).removeClass('open');
				}else{
					$('#bottom-menu').find('a.dropdown-toggle').removeClass('open');
					$('#bottom-menu').find('a.dropdown-toggle').parent().children('ul').css({'display':'none'});
					ul.css({'top':height+'px','display':'inline-block'});
					$(this).addClass('open');
				}
			})
			$('body').on('click',function(){
				$('#bottom-menu').find('a.dropdown-toggle').removeClass('open');
				$('#bottom-menu').find('a.dropdown-toggle').parent().children('ul').css({'display':'none'});
			})
		}	
	};

	//微信底部菜单按钮
	UIFramework.prototype.wxBottomMenu = {
		setting: null,
		_setting : {
			target:null, //添加的jq dom对象
			menu:[
				/*
				{
					width:0,//百分比宽度 数值
					name:"",//名字
					btnColor:"",//按钮颜色 w 白色 r 红色 g 绿色
					className:"",//样式名称
					iconClass:""//图标样式
					click:null//回调
				}
				*/
			],//按钮所占据百分比 [30%,70%] 2个按钮 37分 [30%,30%,40%] 3个按钮334分
			className:"",//添加样式class
			css:""//手动改变样式
		},
		//检查配置
		_checkParams: function(setting) {
			if(!tools.checkJqObj(setting.target)){
				console.log('错误的jq对象');
				return;
			}

			var _setting = tools.clone(this._setting);
			$.extend(true, _setting, setting);

			this.setting = setting;

			return _setting;
		},
		init: function(_setting) {
			_setting = this._checkParams(_setting);

			_setting.target.append(this._getHtml(_setting)).addClass(_setting.className).css(_setting.css);
			this._bindDom(_setting);
		},
		_getHtml:function(_setting){
			var html = new Array(),menu = _setting.menu;

			html.push('<div id="wx_bottom_menu">');
			html.push('<ul>');
			for (var i = 0; i < menu.length; i++) {
				var m = menu[i];
				if(!this._checkObj(m)){alert("参数有错误!");return;}
				var className = m.className || "",width = m.width;

				if(className){className = 'class="'+className+'"';}
				if(tools.isInteger(width)){width = 'style="width:'+width+'%"';}

				html.push('<li '+ width + ' ' +className+'>');
				html.push('<div class="btn_'+m.btnColor+'">');
				html.push('<i class="'+m.iconClass+'"></i>&nbsp;' + m.name);
				html.push('</div>');
				html.push('</li>');
			};
			html.push('</ul>');
			html.push('</div>');

			return html.join('');
		},
		_checkObj:function(obj){
			if(typeof obj === "object" && 
				tools.isInteger(obj.width) && obj.name && 
				tools.isFunction(obj.click)){
				return true;
			}

			return false;
		},
		_bindDom:function(_setting){
			var menu = _setting.menu,pThis = this;

			_setting.target.find("li > div").on("click",function(){
				var index = $(this).parent().index();

				menu[index].click.apply(pThis,[pThis,$(this),index]);
			});
		},
		addClass:function(className){
			setting.target.addClass(className);
		},
		removeClass:function(className){
			setting.target.removeClass(className);
		},
		addCss:function(css){
			setting.target.css(css);
		}
	};

	//初始化powerby,默认初始化在.main的最底部；target为jq对象，传入target侧初始化powerdy在target的底部
	UIFramework.prototype.bottomInfo = {
		init: function(target,bottomMargin){
			this.remove();
			try{
				target = target || $('.main'),info = powerby_val || "FQ工会管理后台";
				target.append('<p id="powerby" '+(bottomMargin ? 'style="margin-bottom:'+bottomMargin+'px;"' : "") +'>' + info + '</p>');
			}catch(e){
				console.log(e);
			}
		},
		remove:function(){
			$("#powerby").remove();
		}
	}

	//指纹统一弹出框
	UIFramework.prototype.soterAlert = function(top,callback){
		var layerPackage = this.layerPackage();

		var html = new Array();
		html.push('<div>');
		html.push('<i class="block rulericon-fingerprint tac c-yellow" style="font-size:40px;"></i>');
		html.push('<p class="mt7">“微信”的Touch ID</p>');
		html.push('<p class="fs12 c-4a mt5">请输入按住手机主键录入指纹</p>');
		html.push('</div>');

		layerPackage.ew_alert({
			title: html.join(''),
			desc: "",
			action_desc: "<span class='fs14 c-black'>取消</span>",
			top:top,
			callback: function() {
				callback();
			}
		});
	};
	
	return new UIFramework();
});