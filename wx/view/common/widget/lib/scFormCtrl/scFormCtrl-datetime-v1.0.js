define([
    'common:widget/lib/scFormCtrl/scFormCtrl-v1.0',
    'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-datetime.html',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/plugins/mobiscroll/js/mobiscroll.custom-3.0.0-beta6.min'
], function(scFormCtrl,datetimeTpl,tpl,mobiscroll) {
	(function($){
		var ctrl = $.fn.scFormCtrl._init();
		var _consts = ctrl._consts;
		var scFormCtrl = ctrl._scFormCtrl;
		var tools = ctrl._tools;
		var _event = ctrl._event;

		_consts.datetimeClassName = "scCtrlDatetime";

		//时间选择器事件
		scFormCtrl.checkDatetime = function(target,data,afterEvent){
			var pThis = this;
			var ctrlErrorMsg = target.find(".ctrlErrorMsg");
			function common(e){
				var o = $(this),val = data.val || "";

				//验证是否为空
				if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val, 'notnull')){
					pThis.errorMsg(ctrlErrorMsg,'请填写'+data.name,target,data,function(){
						//data.ids = "";
						//data.imgs = "";
					});
					return;
				}

				ctrlErrorMsg.addClass('hide');
				target.attr("data-checked",1);
				target.data("cacheData",data);
				target.data("cacheAfterEvent",afterEvent);

				scFormCtrl.afterEvent(data,afterEvent,target);
			}

			target.off("check").on("check",common);

			//隐藏错误信息
			target.off("hideMsg").on("hideMsg",function(){
				ctrlErrorMsg.addClass('hide');
			});
		};

		//额外事件
		scFormCtrl.datetimeBindDom = function(target){
			var pThis = this;
			/*改变控件值方法*/
			target.off("changeData").on("changeData",function(){
				var o = $(this);
				var instance = o.data("instance");
				var data =  o.data("cacheData");
				var val = data.val;

				if(!val){return;}
				var timeVal = val;

				if(data.format == "time"){
					timeVal = timeVal.split(":");
					instance.setVal(new Date(new Date().setHours(parseInt(timeVal[0]),parseInt(timeVal[1]),0,0)),1);
				}else{
					instance.setVal(new Date(tools.timeSolve(timeVal)),1);
				}
				//跳过验证并且强制验证成功
				if(data.noCheck){delete data.noCheck;target.attr("data-checked",1); return;}
				_event.check(o);
			});

			//改版控件类型方法
			target.off("changeType").on("changeType",function(){
				var dom = $(this);
				var instance = dom.data("instance");

				instance.clear();
				instance.destroy();

				pThis.initDatetime(dom.data("cacheData"),dom.data("cacheAfterEvent"),dom);
			});

			//改版控件最小值时间
			target.off("changeMinTime").on("changeMinTime",function(){
				var o = $(this);
				var instance = o.data("instance");
				var data =  o.data("cacheData");
				var minVal = data.minVal;

				if(!minVal){return;}

				var minTime = minVal;

				minTime = tools.timeSolve(minTime);

				if(data.format == "time" || data.format == "datetime"){
					//加1分钟
					minTime = new Date(minTime).getTime() + 1000*60;
				}else if(data.format == "date"){
					//加1天
					minTime = new Date(minTime).getTime() /*+ 1000*60*60*24*/;
				}

				instance.option({
				    min:new Date(minTime)
				});

				delete data.minVal;
				if(data.clearVal){
					instance.clear();
					data.val = "";
					delete data.clearVal;
					_event.check(o);
				}

				var val = data.val;

				if(new Date(minTime).getTime() > new Date(tools.timeSolve(val)).getTime()){
					data.val = tools.timeSolve(tools.getNowDate(new Date(minTime)),1);
					o.trigger('changeData');
				}
				//data.val = tools.getNowDate(new Date(minTime));
				//o.trigger('changeData');
			});

			//跳过验证执行事后事件
			target.off("afterEvent").on("afterEvent",function(){
				var dom = $(this);
				scFormCtrl.afterEvent(dom.data("cacheData"),dom.data("cacheAfterEvent"),dom);
			});
		};

		scFormCtrl.getNowFormatDate = function(format,time) {
		    var date = time ? new Date(tools.timeSolve(time)) : new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    var hour = date.getHours();
		    var minute = date.getMinutes();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    if(hour>=0 && hour <= 9){
		    	hour = "0" + hour;
		    }
		    if(minute>=0 && minute <= 9){
		    	minute = "0" + minute;
		    }
		    var currentdate = "";
		    if(format=="date"){
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
		    }
		    else if(format=="time"){
		    	currentdate = hour + seperator2 + minute;
		    }
		    else{
		    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		        + " " + hour + seperator2 + minute;
		    }
		    return currentdate;
		};

		//初始化时间控件
		scFormCtrl.initDatetime = function(data,afterEvent,dom){
			var pThis = this;
			var now = new Date(),
				maxDate = new Date(now.getFullYear() + 100, now.getMonth(), now.getDate());

			var params = {
		        theme: 'ios',
		        lang: 'zh',
		        display: 'bottom',
		        
		        //min: minDate,
		        onInit:function(event, inst){
		        	//_event.check(dom);
		        },
		        onSet: function (event, inst) {
		            // Your custom event handler goes here
		            var val = inst.getVal();
		            //苹果手机问题
		            val = tools.timeSolve(val);
		            var datetime = tools.getNowDate(new Date(val),1);

		            if(data.format == "date"){
		            	datetime = datetime.year + "-" + datetime.month + "-" + datetime.days;
		            }else if(data.format == "datetime"){
		            	datetime = datetime.year + "-" + datetime.month + "-" + datetime.days + " " + datetime.hours + ":" + datetime.minutes;
		            }else if(data.format == "time"){
		            	datetime = datetime.hours + ":" + datetime.minutes;
		            }

		            data.val = datetime;
		            //console.log(data);
		            //pThis.checkDatetime(dom,data,afterEvent);
		            _event.check(dom);
		        }
		    };

			var instance = "";

			if(data.format == 'date'){
				params.dateFormat = 'yyyy年mm月dd日';
				params.max = maxDate;
				instance = mobiscroll.date(dom.find("."+_consts.datetimeClassName), params);
			}else if(data.format == 'time'){
				params.steps = {
				    minute: 0,
				    second: 0,
				    zeroBased: true
				};
				instance = mobiscroll.time(dom.find("."+_consts.datetimeClassName), params);
			}else if(data.format == 'datetime'){
				params.dateFormat = 'yyyy年mm月dd日';
				params.max = maxDate;
				instance = mobiscroll.datetime(dom.find("."+_consts.datetimeClassName), params);
			}

			var val = "";

			if(data.val && data.val != ""){
				val = data.val;
			}else{
				if(data.default_val == 1){
					val = this.getNowFormatDate(data.format,data.val);
				}
			}

			if(val){
				var timeVal = val;
				if(data.format == "time"){
					timeVal = timeVal.split(":");
					instance.setVal(new Date(new Date().setHours(parseInt(timeVal[0]),parseInt(timeVal[1]),0,0)),1);
				}else{
					timeVal = tools.timeSolve(timeVal);
					instance.setVal(new Date(timeVal),1);
				}

				data.val = val;
			}

			//this.instance = instance;
			dom.data("instance",instance);
		};

		//获取时间选择器事件
		$.fn.scFormCtrl.getCtrlExtendDatetime = function(data,params){
			//this._init();

			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				detail = params.detail;

			var type = data.type;
			var dom =  scFormCtrl.getCtrlInfoDom(data,datetimeTpl,afterEvent,detail);

			if(!(detail && data.edit_input != "1")){
				//初始化选择器
				scFormCtrl.initDatetime(data,afterEvent,dom);

				scFormCtrl.checkDatetime(dom,data,afterEvent);
				scFormCtrl.datetimeBindDom(dom);
				if(check || data.val){_event.check(dom);}
			}

			return dom;
		};
	})(jQuery);
});