/*
 * 控件公式计算 
 */
define([
    'common:widget/lib/scFormCtrl/scFormCtrl-v1.0',
    'common:widget/lib/scFormCtrl/scFormCtrl-sublist-v1.0'
], function(scFormCtrl) {
	(function($){
		var ctrl = $.fn.scFormCtrl._init();
		var _consts = ctrl._consts;
		var scFormCtrl = ctrl._scFormCtrl;
		var tools = ctrl._tools;
		var _event = ctrl._event;

		//获取能计算或者公式关联的控件
		scFormCtrl.getCalcCtrl = function(format) {
			var sctrlObj = $("body").find("."+_consts.ctrlClassName);

			var arr = {},dataArr = new Array(),keyObjArr = new Array(),objArr = new Array();

			sctrlObj.each(function(index, el) {
				var o = $(this);
				var data = 	o.data("cacheData");
				var format = data.format,type = data.type,key = data.input_key;

				//筛选
				if((data.type == "text" && (format == "number" || format == "function")) 
					|| data.type == "money" 
					|| data.type == "table"){
					arr[key] = data;
					dataArr.push(data);
					keyObjArr[key] = o;
					objArr.push(o);
				}


			});

			//保存数据结构（和已有数据做对比）
			this.cws = arr;
			//保存对应数据
			this.cacheCalcCtrlData = dataArr;
			//保存对应jq对象
			this.cacheCalcCtrlObj = objArr;
			//保存对应jq对象(KEY)
			this.cacheCalcKeyCtrlObj = keyObjArr;

			return dataArr;
		};

		scFormCtrl.calc = {
			init:function(cws){
				this.cws = cws;
				this.kws = this.getKeyWords();

				this.getKwArrAndReg();
				this.getCwArrAndReg();

				//没有公式
				if(!this.formulaArrStr){return;}

				this.bindDom();
			},
			//获取函数方法关键字
			getKeyWords : function(){
			  	var keywords = function(){
				    function kw(type) {return {type: type, style: "keyword"};}
				    var A = kw("keyword a"), B = kw("keyword b"), C = kw("keyword c");
				    var operator = kw("operator"), atom = {type: "atom", style: "atom"};

				    var jsKeywords = {
				      "SUM" : kw("SUM"),"MAX" : kw("MAX"),"MIN" : kw("MIN"),"AVG" : kw("AVG")
				    };
				    return jsKeywords;
			  	}();

			  	return keywords;
			},
			//数据处理
			cloneData : function(data){
				var bu = tools.clone(data);
				
				$.each(bu,function(key){
					var data = bu[key];

					if(data.type == "table"){
						delete bu[key];

						$.each(data.table_arr,function(k){
							bu[k] = data.table_arr[k];

							bu[k].name = bu[k].name;

							bu[k].pKey = key;
						});

						bu[key] = data;
					}
				});

				return bu;
			},
			getKwArrAndReg : function(){
				var kwArr = new Array(),keywords = this.kws;
				$.each(keywords,function(key){
					kwArr.push(key);
				});
				this.kwArrStr = kwArr.join('|');
				//替换函数
				this.kwStr = '('+this.kwArrStr+')';
				this.kwStrReg = new RegExp(this.kwStr,"g");
			},
			getCwArrAndReg : function(){
				var cwsArr = new Array(),cws = this.cloneData(this.cws);
				var formulaArr = new Array();
				var tableFormulaArr = new Array(); //子表相关公式
				$.each(cws,function(key){
					var data = cws[key];

					if(data.type == "table"){
						$.each(data.table_arr,function(k){
							cwsArr.push(k);

							var f = data.table_arr[k].formula;
							if(f){formulaArr.push(f);tableFormulaArr.push(f)}
						});
					}

					cwsArr.push(key);
					if(data.formula){ formulaArr.push(data.formula);}
				});
				this.cwsArrStr = cwsArr.join('|');
				//替换组件
				this.cwStr = '('+this.cwsArrStr+')';
				this.cwStrReg = new RegExp(this.cwStr,"g");
				//获取所有公式的字符串
				this.formulaArrStr = formulaArr.join('|');//
				//子表相关公式 字串
				this.tableFormulaArrStr = tableFormulaArr.join('|');
			},
			bindDom : function(){
				var pThis = this;
				//input 输入框
				$(".mContent ."+_consts.ctrlClassName).on('input',"."+_consts.textClassName,function(){
					pThis.clickEvent($(this));
				});

				/*$("body").on("input","."+_consts.sublistDialogClassName + " ."+_consts.sublistPartClassName+" ."+_consts.ctrlClassName+" ."+_consts.textClassName,function(){
					pThis.clickEvent($(this));
				});*/
			},
			bindObjInputClick:function(obj,isDialog){
				var pThis = this;
				obj.on('input',function(){
					pThis.clickEvent($(this),isDialog);
				});
			},
			clickEvent:function(obj,isDialog){
				//console.time("===");
				this.handle(obj,isDialog);//}catch(e){$(".formInfo p").text(e);}
				//console.timeEnd("===");
			},
			//判断KEY是否有相关公式关联
			checkIsHasFormulaContact:function(key){
				var formulaArrStr = this.formulaArrStr;

				formulaArrStr = formulaArrStr.replace(/\s+/g, '');

				var str = '('+key+')';
				var reg = new RegExp(str,"g");
				var res = formulaArrStr.match(reg);

				if(res == null || res.length == 0){
					//console.log("匹配不上");
					return false;
				}else{
					return true;
				}
			},
			//验证该key是否有公式
			checkIsHasFormula: function(key,obj){
				if(!obj){obj = this.cws;}

				var data = obj[key];
				return (obj.propertyIsEnumerable(key) && data && data.formula);
			},
			//验证该公式是否含有某个KEY
			checkFormulaHasKey:function(formula,key){
				formula = formula.replace(/\s+/g, '');

				var str = '('+key+')';
				var reg = new RegExp(str,"g");
				var res = formula.match(reg);

				if(res == null || res.length == 0){
					//console.log("匹配不上");
					return false;
				}else{
					return true;
				}
			},
			checkHasStr:function(val){
				if(!isNaN(Math.abs(val))){
					val = Math.abs(val);
				}
				return /[^(\d+(.\d+)*)]/g.test(val);
			},
			checkHasNumAndOpt:function(formula){
				var res = formula.match(/[^((\d+(.\d+)*)|+|\-|*|\/|'|'|\(|\)]/g);

				if(res == null || res.length == 0){
					//console.log("匹配不上");
					formula = formula.replace(/('')/g,0);
					return formula;
				}else{
					return formula;
				}
			},
			//判断并且验证
			judgeAndGetVal:function(val){
				if(val == ""){
					val = "''";
				}
				else if(this.checkHasStr(val)){
					val = "'" + val +"'";
				}

				return val;
			},
			getJqObjVal:function(jqObj, cal){
				cal = typeof(cal) === 'undefined' ? true : cal;
				var arr = new Array(),pThis = this;

				$.each(jqObj,function(){
					arr.push(pThis.judgeAndGetVal($(this).val()));
				});

				if (!cal) {
					return arr.join(',');
				}
				
				var str = arr.join('+');

				str = this.checkHasNumAndOpt(str);

				var val = eval(str);

				return val;
			},
			//根据key值获取jqObj对象
			getCalcCtrlObj:function(key,formulaNotTable,index){
				/*var cacheCalcCtrlObj = scFormCtrl.cacheCalcCtrlObj;

				for (var i = 0; i < cacheCalcCtrlObj.length; i++) {
					if(cacheCalcCtrlObj[i].attr("data-key") == key){
						return cacheCalcCtrlObj[i];
					}
				};*/

				var o = null;
				//寻找字表内容

				if(formulaNotTable == 1){
					o = $(".mContent").find("."+_consts.ctrlClassName+" input[data-key="+key+"]");
				}else{
					o = $("."+_consts.sublistDialogClassName).find("."+_consts.sublistPartClassName+":eq("+index+") ."+_consts.ctrlClassName+"[data-key="+key+"] ."+_consts.textClassName);
				}

				if(o && o.length > 0){
					return o;
				}

				return null;
			},
			//替换
			replaceStr:function(regStr,replaceStr,str){
				regStr = '('+regStr+')',reg = new RegExp(regStr,"g");

				if(!replaceStr){replaceStr = 0;}

				str = str.replace(reg,replaceStr);

				return str;
			},
			//当状态改变的时候执行的函数
			//obj:所在key的jq对象,isDialog:是否在列表中
			handle:function (obj,isDialog) {
				var key = obj.attr("data-key");

				var cws_1 = this.cloneData(this.cws),
				val = obj.val();

				var keyInfo = {
					val : val,
					key : key, //当前点击的输入框key值
					obj :obj,
					isDialog:isDialog //当前点击的输入框是否在弹出框中
				};

				//如果没有公式关联则退出
				if(!this.checkIsHasFormulaContact(key)){return;}

				var pThis = this;

				$.each(cws_1,function(k){
					//获取拥有该KEY的公式
					var formula = cws_1[k].formula;

					if(pThis.checkIsHasFormula(k,cws_1) && 
						pThis.checkFormulaHasKey(formula,key)){
						//console.log(formula,k);

						pThis.calcFormula(formula,k,keyInfo);
					}
				});
			},
			//公式处理
			//公式,公式所在的KEY,该key输入框的信息,本身是否有公式
			calcFormula:function(formula,formulaKey,keyInfo){
				//输入框信息
				var key = keyInfo.key,isDialog = keyInfo.isDialog,keyObj = keyInfo.obj;

				//控件对象
				var ctrlObj =  scFormCtrl.cacheCalcKeyCtrlObj[formulaKey];

				//公式所在对象
				var formulaObj =  null;
				var dialog = null;

				//判断公式所在KEY是不是在表格当中
				var formulaNotTable = 0;

				if(ctrlObj && ctrlObj.length > 0){
					formulaNotTable = 1;
					formulaObj = ctrlObj.find("."+_consts.textClassName);
				}else{
					dialog = $("."+_consts.sublistDialogClassName);
					formulaObj = keyObj.parents("."+_consts.sublistPartClassName).find("input."+_consts.textClassName+"[data-key="+formulaKey+"]");
				}

				//去除公式空格
				formula = formula.replace(/\s+/g, '');

				var reg = new RegExp(this.cwStrReg);
				var res = formula.match(reg);

				if(res == null || res.length == 0){return;}
				
				//获取控件
				for (var i = 0; i < res.length; i++) {
					var k = res[i];

					var input = null;

					//触发不是在弹出层里
					if(!isDialog){
						input = $('.mContent').find("input[data-key="+k+"]");
					}else{
						input = keyObj.parents("."+_consts.sublistPartClassName).find("input."+_consts.textClassName+"[data-key="+k+"]");
					}

					var val = 0;

					if(input.length == 1){
						val = this.judgeAndGetVal(input.val());
					}else{
						val = this.getJqObjVal(input);
					}

					formula = this.replaceStr(k,val,formula);
				}


				//替换公用函数
				formula = this.calcFormulaFuc(formula);
				//检查公式还有没有没替换的组件名
				if(this.checkFormulaHasCW(formula)){return;}

				formula = this.checkHasNumAndOpt(formula);

				try{
					formula = eval(formula);
					var str = formula.toString();
					
					if(str.split(".")[1] && str.split(".")[1].length>0 && str.split(".")[1].length >= 5){
						formula = formula.toFixed(2);
					}
					
				}catch(e){
					console.log("error");
					//console.groupEnd();
					return;
				}

				//如果在弹出框触发计算并且 公式计算在外面的 则外面的公式不付值和触发事件
				if(isDialog && formulaNotTable){return;}

				formulaObj.val(formula).trigger("input");
			},
			isInteger: function (obj) {
			    //return typeof obj === 'number' && obj%1 === 0
				//console.log(obj);
				if(obj.toString().match(/^(\d+(.\d+)*)$/g)){
					return true;
				}
				
				return false;
			},
			//计算公式里的函数方法
			calcFormulaFuc:function(formula){
				//console.log(formula);
				var keywords = this.kws,pThis = this;
				//替换函数关键字
				$.each(keywords,function(key){
					key = key + '\\[[^'+pThis.kwStr+']*\\]';
					var newReg = new RegExp(key,"g");
					var arr = formula.match(newReg);

					if(!arr)return;

					for (var i = 0; i < arr.length; i++) {
						var val = arr[i];
						var index = val.indexOf('[');
						var kw = val.substr(0,index);
						var data = val.substring(index + 1,val.length-1).split(',');

						switch(kw){
							//相加
							case "SUM":
								var count = new Array();
								
								for (var j = 0; j < data.length; j++) {
									//count += parseFloat(data[j]);
									data[j] = pThis.isInteger(data[j]) ? data[j] : 0;
									count.push(data[j]);
								};
								count = eval(count.join('+'));

								count = pThis.judgeAndGetVal(count);

								formula = formula.replace(arr[i],count);
							break;
							//平均值
							case "AVG":
								var count = 0,num = 0;
								
								for (var k = 0; k < data.length; k++) {
									var v = parseFloat(data[k]);
									if(isNaN(v)){
										v = 0;
									}
									count += v;
									num += 1;
								};
								count = pThis.isInteger(count) ? count : 0;
								formula = formula.replace(arr[i],(count/num));
								
							break;
							//取最大值
							case "MAX":
								var max_str = new Array();
								
								for (var j = 0; j < data.length; j++) {
									var v = parseFloat(data[j]);
									if(isNaN(v)){
										v = 0;
									}
									max_str.push(v);
								};

								var max = Math.max.apply(null, max_str);
								//max = pThis.isInteger(max) ? max : 0;
								formula = formula.replace(arr[i],max);
							break;
							//取最小值
							case "MIN":
								var min_str = new Array();
								
								for (var l = 0; l < data.length; l++) {
									var v = parseFloat(data[l]);
									if(isNaN(v)){
										v = 0;
									}
									min_str.push(parseFloat(v));
								};

								var min = Math.min.apply(null, min_str);
								//min = pThis.isInteger(min) ? min : 0;
								formula = formula.replace(arr[i],min);
							break;
						}
					};
				});

				return formula;
			},
			//验证是否有组件key
			checkFormulaHasCW:function(formula){
				formula = formula.replace(/\s+/g, '');

				var res = formula.match(this.cwStrReg);

				if(res == null || res.length == 0){
					//console.log("匹配不上");
					return false;
				}else{
					return true;
				}
			},
			checkTableFormulaHasCW:function(){
				var formula = this.tableFormulaArrStr.replace(/\s+/g, '');

				var res = formula.match(this.cwStrReg);

				if(res == null || res.length == 0){
					//console.log("匹配不上");
					return false;
				}else{
					return res;
				}
			}
		};

		//公式计算初始方法
		$.fn.scFormCtrl.calc = function(){
			//this._init();
			var data = scFormCtrl.getCalcCtrl();

			//没有公式相关控件
			if(data.length == 0){return;}

			scFormCtrl.calc.init(scFormCtrl.cws);

			//console.log(scFormCtrl.cacheCalcCtrlData);
			//console.log(scFormCtrl.calc.kwStrReg);
			//console.log(scFormCtrl.calc.cwStrReg);
			//console.log(scFormCtrl.calc.formulaArrStr);
		};
	})(jQuery);
});