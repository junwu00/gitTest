define([
	'common:widget/lib/UiFramework',
    'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-sublist.html',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/scFormCtrl/scFormCtrl-v1.0',
    'common:widget/lib/scFormCtrl/scFormCtrl-datetime-v1.0',
    'common:widget/lib/scFormCtrl/scFormCtrl-userDept-v1.0',
    'common:widget/lib/scFormCtrl/scFormCtrl-filesPics-v1.0',
    'common:widget/lib/scFormCtrl/scFormCtrl-calc-v1.0',
], function(UiFramework,sublistTpl,tpl,scFormCtrl) {
	var layerPackage = UiFramework.layerPackage(); 

	(function($){
		var ctrl = $.fn.scFormCtrl._init();
		var _consts = ctrl._consts;
		var scFormCtrl = ctrl._scFormCtrl;
		var tools = ctrl._tools;
		var _event = ctrl._event;

		_consts.sublistClassName = "scCtrlSublist";
		_consts.sublistAddClassName = "scCtrlSublistAdd";
		_consts.sublistEditClassName = "scCtrlSublistEdit";
		_consts.sublistPartClassName = "scCtrlSublistPart";
		_consts.sublistPartDelClassName = "scCtrlSublistPartDel";
		_consts.sublistDialogClassName = "scCtrlSublistDialog";

		//子表单验证
		scFormCtrl.checkSublist = function(target,data,afterEvent){
			var pThis = this;
			var ctrlErrorMsg = target.find(".ctrlErrorMsg");
			function common(e){
				var o = $(this);
				var rec = data.rec;

				//验证是否为空
				if(data.edit_input==1 && data.must == 1 && (rec===undefined||rec.length==0)){
					pThis.errorMsg(ctrlErrorMsg,"请添加"+data.name+"明细",target,data,function(){
					},1);
					return;
				}

				if(rec!==undefined){
					var table_arr = data.table_arr;
					var flag = false;

					for(var i = 0;i < rec.length;i++){
						if(flag){break;}

						var items = rec[i];
						$.each(items,function(key,item){
							var table_obj = table_arr[key];
							if (data.visit_input==1 && table_obj.visit_input==1 && table_obj.edit_input==1 && item.must == 1 && item.type != 'table' && !tools.regCheck(item.val, 'notnull')) {
								pThis.errorMsg(ctrlErrorMsg,'请填写'+data.name+"详情"+(i+1)+"的" + item.name,target,data,function(){
								},1);

								flag = true;
								return false;
							}
						});
					}

					if(flag){return;}
				}

				ctrlErrorMsg.addClass('hide');
				target.attr("data-checked",1);

				scFormCtrl.afterEvent(data,afterEvent,target);

				//data.val = val;
				//o.data("cacheData",data);
			}

			target.on("check",common);

			//隐藏错误信息
			target.on("hideMsg",function(){
				ctrlErrorMsg.addClass('hide');
			});
		};

		//子表单相关事件
		scFormCtrl.sublistEvent = function(target,data,params){
			//子表单可编辑情况下
			var pThis = this;
			//添加
			target.on('click','.'+_consts.sublistAddClassName,function(e){
				window.location.hash = "sublistDialog";
				//alert("添加");
				pThis.sublistAddDialog(target,data,params);
			});

			//删除
			target.on('click','.'+_consts.sublistPartClassName+" ."+_consts.sublistPartDelClassName,function(e){
				e.stopPropagation();
				e.preventDefault();
				var o = $(this),
					p_o = o.parents('.'+_consts.sublistPartClassName),
					index = p_o.index();

				layerPackage.ew_confirm({
					title:"确定要删除明细？",
					ok_callback:function(){
						o.fadeOut('fast', function() {
							p_o.remove();
							data.rec.splice(index,1);

							if(data.rec.length == 0){
								_event.check(target);
							}else if(data.rec.length == 1){
								//隐藏批量编辑
								target.find("."+_consts.sublistEditClassName).addClass('hide');
							}

							if(data.rec.length != 0){
								target.find("."+_consts.sublistClassName).find("input").trigger('input');
							}else{
								var res = scFormCtrl.calc.checkTableFormulaHasCW();

								//console.log(res);

								/*var input_count = 0;
								for (var i = 0; i < res.length; i++) {
									var input = $("input[data-key="+res[i]+"]");

									if(input.length > 0){
										input.trigger('input');
										input_count ++;
									}
								};

								if(input_count == 0){

								}*/
							}
						});
						//otherProcess.delEvent(pThis); 报销相关
					}
				});
			});

			//明细操作
			if(params.table_detail_control){
				//批量编辑
				target.on('click','.'+_consts.sublistEditClassName,function(e){
					if(!data.rec || data.rec.length == 0){return;}
					window.location.hash = "sublistDialog";
					//alert("批量编辑");
					pThis.sublistPartAllEdit(target,data);
				});

				//单项编辑
				target.on('click','.'+_consts.sublistPartClassName,function(e){
					window.location.hash = "sublistDialog";

					var o = $(this),index = o.index();
					var d = data.rec[index];

					pThis.sublistPartEditDialog(target,data,index);
				});
			}
		};

		//子表单公用事件
		scFormCtrl.sublistBindDom = {
        	flag:false,
        	init:function(){
        		if(!this.flag){
        			this.flag = true;
        			this.bindDom();
        		}
        	},
        	bindDom:function(){
        		$(window).on('hashchange', function() {
        			var hash = window.location.hash;

        			if(hash != "#sublistDialog" && hash !="#1" && hash !="#1_2" && hash !="#1_2_3" && hash !="#1_6" && hash !="#1_2_6" && hash !="#1_2_3_6" && hash != '#1_4'){
        				$("body").find("."+_consts.sublistDialogClassName).remove();
        			}
        		});
        	}
        };

        //显示详情部分
        scFormCtrl._getSublistDetail = function(data){
        	//子表特殊权限控制修改对应明细的控件权限
        	var table_arr = data.table_arr,
        		rec = data.rec || [];

    		for (var i = 0; i < rec.length; i++) {
    			var r_d = rec[i];

    			for(var k in r_d){
    				r_d[k].edit_input = table_arr[k].edit_input;
    				r_d[k].visit_input = table_arr[k].visit_input;
    			}
    		};

        	tpl.register('getNowDate', tools.getNowDate);
        	tpl.register('formatMoney', tools.formatMoney);
        	var dom = tpl(sublistTpl,{
        		sublistDetail:1,
        		data:data,
        		consts:_consts,
        		defaultFace:defaultFace || ''
        	});
        	tpl.unregister('getNowDate');
        	tpl.unregister('formatMoney');

        	return $.trim(dom);
        };

        //添加弹出框
        scFormCtrl._getSublistDialogCreateHtml = function(data,noTop){
        	var dom = tpl(sublistTpl,{
        		createDialog:1,
        		data:data,
        		consts:_consts,
        		noTop:noTop //是否显示头部信息
        	});

        	return $($.trim(dom));
        };

        //创建详情模块模板
        scFormCtrl._getSublistPartHtml = function(data,index,isDetail){
        	var dom = tpl(sublistTpl,{
        		sublistPart:1,
        		data:data,
        		consts:_consts,
        		index:index,
        		isDetail:isDetail //是否是详情
        	});

        	return $.trim(dom);
        };

        //创建详情模块
        scFormCtrl._addPartToCreateDialog = function(dialog,data,index){
			var new_table_arr = $.extend(true, {}, data.table_arr);

			dialog.find(".dialogMain").append(this._getSublistPartHtml(data,index));

			try{
				for(var d in new_table_arr){
					var type = new_table_arr[d].type;
					var dom = "";

					if(type == "people" || type == "dept"){
						dom = $.fn.scFormCtrl.getCtrlExtendUserDept(new_table_arr[d],userInfo);
					}else if(type == "date"){
						dom = $.fn.scFormCtrl.getCtrlExtendDatetime(new_table_arr[d],{
							check:new_table_arr[d].default_val
						});
					}else if(type == "picture"){
						dom = $.fn.scFormCtrl.getCtrlExtendPics(new_table_arr[d],{
							newConfig:1
						});
					}else{
						dom = $.fn.scFormCtrl(new_table_arr[d]);

						if(type == "radio" || type == "checkbox"){
							var input = dom.find("input[type="+type+"]");
							input.attr("name",input.attr("name")+"_"+index);
						}

						if(type == "text" || type == "money"){
							scFormCtrl.calc.bindObjInputClick(dom,1);
						}
					}

					dialog.find("."+_consts.sublistPartClassName+":last").append(dom);
				}
			}catch(e){
			}
        };

        //预览图片
        scFormCtrl._viewPic = function(dom){
        	var pic = dom.find('.upload-pic');
			if(pic.length > 0){
				try{
    				require(['common:widget/lib/GenericFramework-v2.0'],function(GenericFramework){
						GenericFramework.init(wxJsdkConfig);
			            GenericFramework.previewImage({
			                target:pic
			            });
					});
				}catch(e){}
			}
        };

		//子表单单条添加弹出页面
		scFormCtrl.sublistAddDialog = function(target,data,params){
			var pThis = this;
			var dialog = this._getSublistDialogCreateHtml(data);

			$("body").append(dialog);

			var index = 1;

			if(data.rec && data.rec.length > 0){
				index = data.rec.length + 1;
			}

			this._addPartToCreateDialog(dialog,data,index);

			//添加按钮确认
			var menu = [{
				width: 100,
				name: "确定",
				iconClass: "",
				btnColor: "g",
				id: null,
				click: function() {
					var noCheckObj = dialog.find("."+_consts.ctrlClassName+"[data-checked=0]");
					if(noCheckObj.length !=0){
						noCheckObj.trigger('check');

						//定位到当前第一个错误的控件
						var top = noCheckObj.eq(0)[0].offsetTop;
						dialog.find(".dialogMain").animate({
						    scrollTop: top
						});
						return;
					}

					var scCtrlSublistPart = dialog.find("."+_consts.sublistPartClassName);

					scCtrlSublistPart.each(function() {
						var part = $(this);

						var o = part.find("."+_consts.ctrlClassName+"[data-checked=1]");
						var obj = {}; 
						o.each(function() {
							var  d = $(this).data("cacheData");

							obj[d.th_key] = d;
						});

						if(!data.rec || data.rec.length == 0){
							data.rec = new Array();
						}

						data.rec.push(obj);
					});

					target.data("cacheData",data);

					target.find("."+_consts.sublistClassName).html(pThis._getSublistDetail(data));

					scFormCtrl.calc.bindObjInputClick(target);
					target.find("."+_consts.sublistClassName).find("input").trigger('input');

					//显示批量编辑
					if(params.table_detail_control && data.rec && data.rec.length > 1){
						target.find("."+_consts.sublistEditClassName).removeClass('hide');
					}

					//校验
					if(target.attr("data-checked") == 0){
						_event.check(target);
					}

					window.history.go(-1);
				}
			}];

			UiFramework.wxBottomMenu.init({
				target:dialog,
				menu: menu
			});

			//添加详情事件
			dialog.on("click",'.partAdd',function(){
				var o = $(this);
				o.addClass('clickBg');

				setTimeout(function(){
					o.removeClass('clickBg');
				},100);

				var len = dialog.find("."+_consts.sublistPartClassName).length;
				index = len + 1;

				pThis._addPartToCreateDialog(dialog,data,index);
			});

			//删除详情事件
			dialog.on("click",'.partDel',function(){
				$(this).parents("."+_consts.sublistPartClassName).remove();
				//重新定义名字
				dialog.find("."+_consts.sublistPartClassName).each(function(index) {
					var o = $(this);
					o.find("p").text("子表单详情"+(index+1));
					o.attr("data-index",index+1);
				});
			});
		};

		//详情模块
		scFormCtrl._addPartToDetailDialog = function(target,dialog,data,index){
			var rec = data.rec;

			try{
				for (var i = 0; i < rec.length; i++) {
					//index 单项用，不是当前选择详情则跳过
					if(index !== undefined && i !=index){
						continue;
					}

					var r = $.extend(true, {} , rec[i]);

					dialog.find(".dialogMain").append(this._getSublistPartHtml(data,index ? index +1 : i + 1,1));

					var params = {
						check:1
					};

					for(var k in r){
						var type = r[k].type;
						var dom = "";
						if(type == "people" || type == "dept"){
							dom = $.fn.scFormCtrl.getCtrlExtendUserDept(r[k],userInfo,params);
						}else if(type == "date"){
							dom = $.fn.scFormCtrl.getCtrlExtendDatetime(r[k],params);
						}else if(type == "picture"){
							params.newConfig = 1;
							dom = $.fn.scFormCtrl.getCtrlExtendPics(r[k],params);
						}else{
							dom = $.fn.scFormCtrl(r[k],params);

							if(type == "radio" || type == "checkbox"){
								var input = dom.find("input[type="+type+"]");
								input.attr("name",input.attr("name")+"_"+i);
							}

							if(type == "text" || type == "money"){
								scFormCtrl.calc.bindObjInputClick(dom,1);
							}
						}

						dialog.find("."+_consts.sublistPartClassName+":last").append(dom);
					}
				};
			}catch(e){
			}
		};

		//子表单单条修改弹出页面
		scFormCtrl.sublistPartEditDialog = function(target,data,index){
			var pThis = this;
			var dialog = this._getSublistDialogCreateHtml(data,1);

			$("body").append(dialog);

			this._addPartToDetailDialog(target,dialog,data,index);

			//添加按钮确认
			var menu = [{
				width: 100,
				name: "确定",
				iconClass: "",
				btnColor: "g",
				id: null,
				click: function() {
					var noCheckObj = dialog.find("."+_consts.ctrlClassName+"[data-checked=0]");
					if(noCheckObj.length !=0){
						noCheckObj.trigger('check');

						//定位到当前第一个错误的控件
						var top = noCheckObj.eq(0)[0].offsetTop;
						dialog.find(".dialogMain").animate({
						    scrollTop: top
						});
						return;
					}

					var scCtrlSublistPart = dialog.find("."+_consts.sublistPartClassName);

					scCtrlSublistPart.each(function() {
						var part = $(this);
						var pIndex = index ? index : part.index();

						var o = part.find("."+_consts.ctrlClassName+"[data-checked=1]");

						o.each(function() {
							var  d = $(this).data("cacheData");
							var key = d.th_key;

							data.rec[pIndex][key] = d;

							/*var o = target.find("."+_consts.sublistPartClassName+":eq("+pIndex+") span[data-key="+key+"]");
								o.find(".scCtrlVal").text(d.val);
								o.find("input[type=hidden]").val(d.val);*/
						});
					});

					target.data("cacheData",data);

					target.find("."+_consts.sublistClassName).html(pThis._getSublistDetail(data));

					scFormCtrl.calc.bindObjInputClick(target);
					target.find("."+_consts.sublistClassName).find("input").trigger('input');

					//校验
					if(target.attr("data-checked") == 0){
						target.trigger('check');
					}

					window.history.go(-1);
				}
			}];

			UiFramework.wxBottomMenu.init({
				target:dialog,
				menu: menu
			});
		};

		//子表单批量编辑
		scFormCtrl.sublistPartAllEdit = function(target,data){
			this.sublistPartEditDialog(target,data);
		};

		//获取子表
		$.fn.scFormCtrl.getCtrlSublist = function(data,params){
			//this._init();
			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				detail = params.detail;

			//对特定控件数据做特殊处理
			var newData = scFormCtrl.dataChange($.extend({},data));

			var rec = newData.rec || [];

			//子表里数据 特定控件数据做特殊处理
			for (var i = 0; i < rec.length; i++) {
				for(var k in rec[i]){
					rec[i][k] = scFormCtrl.dataChange($.extend({},rec[i][k]));
				}
			};

			var dom = scFormCtrl.getCtrlInfoDom(newData,sublistTpl,afterEvent,detail);

			//数据部分
			if(rec.length > 0){
				dom.find("."+_consts.sublistClassName).html(scFormCtrl._getSublistDetail(newData));
			}

			var table_arr = newData.table_arr;
			var table_detail_control = 0; //明细操作权限

			for (var k in table_arr) {
				var t_d = table_arr[k];
				if(t_d.visit_input && t_d.edit_input){
					table_detail_control = 1;
					break;
				}
			};

			params.table_detail_control = table_detail_control;

			var editBtn = dom.find("."+_consts.sublistEditClassName);

			if(!table_detail_control){
				//editBtn.addClass('hide');
				editBtn.remove();// 不需要批量编辑按钮
			}else{
				if(rec.length > 1){
					editBtn.removeClass('hide');
				}else{
					editBtn.addClass('hide');
				}
			}

			scFormCtrl.checkSublist(dom,data,afterEvent);
			scFormCtrl.sublistEvent(dom,data,params);
			scFormCtrl.sublistBindDom.init();

			//数据存在的时候进一步验证明细数据
			if(rec.length > 0){
				var flag = false;

				for(var i = 0;i < rec.length;i++){
					if(flag){
						break;
					}

					var items = rec[i];
					$.each(items,function(key,item){
						var table_obj = table_arr[key];
						if (data.visit_input==1 && table_obj.visit_input==1 && table_obj.edit_input==1 && item.must == 1 && item.type != 'table' && !tools.regCheck(item.val, 'notnull')) {
							flag = true;
							dom.attr("data-checked",0);
							return false;
						}
					});
				}
			}

			if(check || rec.length > 0){_event.check(dom);}

			scFormCtrl._viewPic(dom);

			return dom;
		};
	})(jQuery);
});