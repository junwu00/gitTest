/*
* 基础控件
*/
define([
	'jquery',
	'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-v2.0.html',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/UiFramework'
], function($,scFormCtrlTpl,tpl,UiFramework) {
	var layerPackage = UiFramework.layerPackage();

	(function($){
		//常量
		var _consts = {
			money_type : ["人民币","美元","欧元","港币"],
			money_type_icon : ["¥","$","€","HK$"],
			//default默认 email邮箱地址 phone电话 telephone手机
			default_text_format :  "default",
			number_text_format :  "number",
			email_text_format :  "email",
			phone_text_format :  "phone",
			telephone_text_format :  "telephone",

			ctrlClassName:"scCtrl",

			textClassName:"scCtrlText",
			inputValLimit:168,
			textareaValLimit:1000, //限制字数

			rcClassName:"scCtrlRC",
			rcOtherClassName:"scCtrlRC_other", //单选 复选 其他选项 输入框className
			rcOtherInputClassName:"scCtrlRC_other_input", //单选 复选 其他选项 输入框className

			selectClassName:"scCtrlSelect",

			moneyClassName:"scCtrlMoney",
			maxMoneyVal:999999999999.99,//金额控件最大值
			moneyCapitalizedClassName:"scCtrlMoney-capitalized"
		};
		//工具类
		var tools = {
			//是否数组
			isArray: function(arr) {
				return Object.prototype.toString.apply(arr) === "[object Array]";
			},
			//复制
			clone: function (obj){
				if (obj === null) return null;
				var o = tools.isArray(obj) ? [] : {};
				for(var i in obj){
					o[i] = (obj[i] instanceof Date) ? new Date(obj[i].getTime()) : (typeof obj[i] === "object" ? arguments.callee(obj[i]) : obj[i]);
				}
				return o;
			},
			//输入控制
			checkVal:function(val,replaceVal){
				if(val === undefined || val === ""){
					if(replaceVal){return replaceVal;}

				 	return "";
				}

				return val;
			},
			//替换字符
			replaceStr:function(val,str,replace){
				if(!val){return "";}
				var r = new RegExp(str,"g");
				return val.replace(r,replace);
			},
			/*
			 * 金额大小写转换
			 */
			DX: function(num) {
				var strOutput = "";
				var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
				num += "00";
				var intPos = num.indexOf('.');
				if (intPos >= 0) {
					num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
				}

				strUnit = strUnit.substr(strUnit.length - num.length);
				for (var i = 0; i < num.length; i++) {
					strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i, 1), 1) + strUnit.substr(i, 1);
				}
				return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");
			},
			/*
			 * 正则表达式校验
			 */
			regCheck:function(str, st){
				if (st == undefined || st == '' || st == null)	st = 'notnull';
				
				var reg = ''; //^(\-?)\d+(\.\d+)?$
			    switch(st){
			        case 'sname'    :{ reg = /^[\u4e00-\u9fa5a-zA-Z]{1}[\u4e00-\u9fa5_a-zA-Z0-9]*$/; break;}
			    	case 'notnull'  :{ reg = /^[\s\t\n\r]*[\S]+[\s\t\n\r\S]*$/; break;}
			    	case 'pwd'  	:{ reg = /^[\w\d_]{6,16}$/; break;}
			    	case 'number'  	:{ reg = /^[\d]+$/; break;}
			        case 'email'	:{ reg = /^(\w+([-+.]*\w+)*)+@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/; break; }
			        case 'http'     :{ reg = /^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/; break; }
			        case 'qq'       :{ reg = /^[1-9]\d{4,11}$/; break; }
			        case 'english'  :{ reg = /^[A-Za-z]+$/; break; }
			        case 'mobile'   :{ reg = /^1[34578]\d{9}$/; break; }
			        case 'mobile_s' :{ reg = /^\d{11}$/; break; }
			        case 'phone'    :{ reg = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/; break; }
			        case 'en_num'   :{ reg = /^[A-Za-z0-9]+$/; break; }
			        case 'ip'       :{ reg = /^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/; break; }
			        case 'mac'      :{ reg = /^[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}$/; break; };
			        case 'float'	:{ reg = /^(([1-9]\d*)|\d)(\.\d{1,2})?$/;	break;}
			        case 'money'	:{ reg = /^(\-?)(([1-9]{1}[0-9]{0,11}(\.[0-9]{1,2})?)|(0{1}\.[0-9][1-9])|(0{1}\.[1-9]{1,2}))$/; break;}
			        case 'double'	:{ reg = /^(\-?)(([1-9]\d*)|\d)(\.\d+)?$/; break; }
			    }
			   	if(reg == '') return false;
			   	return reg.test(str);
			},
			//金额千分位分割
			formatMoney:function(num){
				
			    var f = num.toString().split('.');
			    var l = '',r = '';
			    if(f.length > 1){
			        r = f[1];
			    }
			    var f_str = f[0];
			    var l = (f_str || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');

			    if(f.length > 1){
			        l = l + '.' + r;
			    }

			    return l;  
			},
			isFunction:function(func){
				return typeof func === "function";
			},
			getNowDate:function(time,isJson,format){
				if(!time){return "";}

				//针对苹果时间的处理
				time = time.toString().replace("-","/").replace("-","/");

				var date = "";
				if(format == "time"){
					time = time.split(":");
					date = new Date(new Date().setHours(time[0],time[1]));
				}else{
					date = new Date(time);
				}

				var year = date.getFullYear();

				var month = date.getMonth()+1;
				month = month >= 10 ? month :"0" +  month;

				var days = date.getDate();
				days = days >= 10 ? days : "0" + days;

				var hours = date.getHours();
				hours = hours >= 10 ? hours : "0" + hours;

				var minutes = date.getMinutes();
				minutes = minutes >= 10 ? minutes: "0" + minutes;

				var seconds = date.getSeconds();
				seconds = seconds >= 10 ? seconds: "0" + seconds;

				var week = date.getDay();

				if(isJson){
					return {
						year:date.getFullYear(),
						month:month,
						days:days,
						hours:hours,
						minutes:minutes,
						seconds:seconds,
						week:week
					}
				}

				if(format == "time"){
					return hours+":"+minutes;
				}else if(format == "date"){
					return year+"年"+month+"月"+days+"日";
				}else if(format == "datetime"){
					return year+"年"+month+"月"+days+"日 "+hours+":"+minutes;
				}else{
					return year+"-"+month+"-"+days+" "+hours+":"+minutes;
				}
			},
			//金额千分位分割
			formatMoney:function(num){
				if(!num){num = "";}
				
			    var f = num.toString().split('.');
			    var l = '',r = '';
			    if(f.length > 1){
			        r = f[1];
			    }
			    var f_str = f[0];
			    var l = f_str.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');

			    if(f.length > 1){
			        l = l + '.' + r;
			    }

			    return l;  
			},
			//change time suit as apple
			//主要针对苹果时间处理
			timeSolve:function(val,change){
				if(change){
					return val.toString().replace("/","-").replace("/","-");
				}

				return val.toString().replace("-","/").replace("-","/");
			},
			//电话号码处理
			phoneSolve:function(val){
				if(!val){return val;}

				var t0 = val.substring(0,1);
				var t1 = val.substring(0,2);

				if(t0 == "0"){
					if(t1 == "01" || t1 == "02"){
						return val.substring(0,3)+"-"+val.substring(3)
					}else{
						return val.substring(0,4)+"-"+val.substring(4)
					}
				}else{
					return val;
				}
			},
			//手机号码处理
			telephoneSolve:function(val){
				if(!val){return val;}
				
				return val.substring(0,3)+"-"+val.substring(3,7)+"-"+val.substring(7);
			}
		};
		//事件类
		var _event = {
			//回车事件(目前只支持输入框)
			keydown:function(target){
				target.on('keydown',"."+_consts.textClassName,function(e){
					if(e.keyCode==13){
						$(this).trigger('input');

						target.nextAll('.'+_consts.ctrlClassName+':eq(0)').find("."+_consts.textClassName).focus();
					}
				});
			},
			blur:function(target){
				target.on('blur','input[type=text]',function(e){
					//alert(1);
				});
			},
			input:function(target){
				//target.on("input",'input[]')
			},
			//控件检验
			check:function(target,tagName){
				if(tagName){
					target.find(tagName).trigger('check');
				}else{
					target.trigger('check');
				}
			},
			scCtrlErrMsg:function(target,errMsg,clear){
				target.find(".scCtrlErrMsg").remove();
				var mContent = target.find(".sc-content");

				if(clear){mContent.removeClass('hasErrMsg');return;}

				if(!errMsg){return;}

				//显示错误信息
				mContent.before('<div class="scCtrlErrMsg tac fs15 w_100">'+errMsg+'</div>');
				mContent.addClass('hasErrMsg');
			}
		};
		//处理事件类
		var scFormCtrl = {
			//获取基础控件dom节点
			getCtrlInfoDom : function(data,tplHtml,afterEvent,scCtrlState){
				tpl.register('checkVal', tools.checkVal);
				tpl.register('replaceStr', tools.replaceStr);
				tpl.register('getNowDate', tools.getNowDate);
				tpl.register('formatMoney', tools.formatMoney);
				tpl.register('telephoneSolve', tools.telephoneSolve);
				var dom = tpl(tplHtml,{
					getDom:1,
					data:data,
					consts:_consts,
					defaultFace:defaultFace || '',
					scCtrlState:scCtrlState
				});
				tpl.unregister('checkVal');
				tpl.unregister('replaceStr');
				tpl.unregister('getNowDate');
				tpl.unregister('formatMoney');
				tpl.unregister('telephoneSolve');

				dom = $($.trim(dom)); 
				//缓存数据到DOM节点上
				dom.data("cacheData",data);

				if(afterEvent && tools.isFunction(afterEvent)){
					dom.data("cacheAfterEvent",afterEvent);
				}

				return dom;
			},
			//关联事件
			afterEvent:function(data,afterEvent,dom){
				if(afterEvent && tools.isFunction(afterEvent)){
					afterEvent.apply(this,[data,dom]);
				}
			},
			//错误提示
			errorMsg:function(errorObj,text,target,data,callback,noVal){
				errorObj.removeClass('hide').html(text);
				target.attr("data-checked",0);
				target.attr("data-empty",1);

				target.data("errorMsg",text);

				//if(!noVal){data.val = "";}

				if(callback && tools.isFunction(callback)){
					callback();
				}
			},
			//检查字数长度
			checkValLimit:function(valLength,valLimit,o,target){
		        if (valLimit > 0 && valLength > valLimit) {
		        	if(valLength > valLimit){
		        		valLength = valLimit;
		        	}

		        	var tagName = o[0].tagName;

		        	//覆盖值
	        		var val = o.val().substring(0,valLimit);
	        		target.data("cacheVal",val);
	        		o.val(val);

		            var html = new Array();
		            html.push("<span class='fl'>文本应该限制在"+valLimit+"字内</span>");
		            html.push("<span class='fr'>"+valLength+"/"+valLimit+"</span>");

		            return html.join('');
		        } 

		        return '';
			},
			// text 基础验证和事件 (包括基础input)
			checkText:function(target,data,afterEvent){
				var reg = "",errorMsg = "",format = data.format;
				var pThis = this;
				var ctrlErrorMsg = target.find(".ctrlErrorMsg");

				if(format == _consts.number_text_format){
					reg = "double";
					errorMsg = "请输入正确的数字";
				}else if(format == _consts.email_text_format){
					reg = "email";
					errorMsg = "请输入正确的邮箱";
				}else if(format == _consts.phone_text_format){
					reg = "phone";
					errorMsg = "请输入正确的电话";
				}else if(format == _consts.telephone_text_format){
					reg = "mobile";
					errorMsg = "请输入正确的手机";
				}

				function common(e){
					var eventType = e.type;

					var o = $(this);

					//是不是触发检查事件
					if(eventType == "check"){
						o = o.find('.'+_consts.textClassName);
					}

					var tagName = o[0].tagName; //INPUT TEXTAREA
					val = $.trim(o.val());

					if(o.length == 0){return;}

					var textNumberObj = target.find(".textNumber");

					if(tagName != "input"){
						o.css("height","auto");
					}

					//验证是否为空
					if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val, 'notnull')){
						pThis.errorMsg(ctrlErrorMsg,data.errorMsg || '请填写'+data.name,target,data);
						textNumberObj.text("");
						return;
					}

					//校验输入框是否为空
					target.attr("data-empty",0);

					//针对Input不同类型作单独验证处理
					if(tagName == "INPUT"){
						if(data.edit_input == 1 && val !="" && !tools.regCheck(val, reg)){
							pThis.errorMsg(ctrlErrorMsg,errorMsg,target,data);
							return;
						}
					}

					//是否限制长度
					var valLength = tagName == "INPUT" ? val.length : val.replace(/\s/g,'').length,
						valLimit = tagName == "INPUT" ? _consts.inputValLimit : _consts.textareaValLimit;

					var valLimitHtml = pThis.checkValLimit(valLength,valLimit,o,target);

			        if(valLimitHtml == ''){
		            	target.data("cacheVal",val);
			        }else{
			        	val = target.data("cacheVal");
			        }

					ctrlErrorMsg.addClass('hide');
					target.attr("data-checked",1);

					data.val = val;
					target.data("cacheData",data);

					if(tagName == "INPUT"){
						o.attr("value",val); //防止页面val没变影响获取值
					}else{
						var valLen = valLength <= _consts.textareaValLimit ? valLength : _consts.textareaValLimit;
						textNumberObj.text(valLen+"/"+_consts.textareaValLimit);

						if(valLen == _consts.textareaValLimit){
							textNumberObj.addClass('c-red');
						}else{
							textNumberObj.removeClass('c-red');
						}

						var scrollHeight = o[0].scrollHeight;
						var textareaHeight = o.outerHeight();

						if(scrollHeight > 0){
							if(scrollHeight > textareaHeight){
								data.scrollHeight = scrollHeight;
							}else{
								delete data.scrollHeight;
							}

							o.scrollTop(0);
            				o.outerHeight(o[0].scrollHeight);
						}else{
							if(data.scrollHeight){
								o.outerHeight(data.scrollHeight);
							}
						}

            			//o.scrollTop(0);
            			//o.outerHeight(o[0].scrollHeight);
					}

					scFormCtrl.afterEvent(data,afterEvent,target);
				}

				target.on("input",'.'+_consts.textClassName,common);
				target.on("check",common);

				//改变数值
				target.on("changeData",function(){
					var o = $(this);
					var data =  o.data("cacheData");

					o.find('.'+_consts.textClassName).val(data.val);
				});

				//隐藏错误信息
				target.on("hideMsg",function(){
					ctrlErrorMsg.addClass('hide');
				});
			},
			// 单选 复选控件
			checkRadioOrCheckBox:function(target,data,afterEvent){
				var pThis = this;
				var ctrlErrorMsg = target.find(".ctrlErrorMsg");

				function common(e){
					var eventType = e.type;

					var o = $(this);

					//是不是触发检查事件
					if(eventType == "check"){
						o = o.find('input:not(".'+_consts.rcOtherInputClassName+'")');
					}

					if(!o || o.length == 0){return;}

					val = new Array(),val_id = new Array();

					//其他选项的处理
					if(e.currentTarget.type == "radio"){
						target.find("."+_consts.rcOtherInputClassName).val('');
					}/*else if(e.currentTarget.type == "checkbox" && e.currentTarget.className.indexOf(_consts.rcOtherClassName) >= 0){
						if(o.is(':checked')){
							target.find("."+_consts.rcOtherInputClassName).css("display", "");
						}else{
							target.find("."+_consts.rcOtherInputClassName).css("display", "none");
						}
					}*/

					target.find("input:checked").each(function(index,event) {
						val_id.push($(this).attr("data-id"));

						if(event.className.indexOf(_consts.rcOtherClassName) >= 0){
							var other_val = $.trim(target.find("."+_consts.rcOtherInputClassName).val());

							if(other_val == ""){
								val = new Array();
							}else{
								val.push(other_val);
							}
						}else{
							val.push($.trim($(this).val()));
						}
					});

					if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val.join(","), 'notnull')){
						pThis.errorMsg(ctrlErrorMsg,'请选择'+data.name,target,data,function(){
							data.val_id = "";
						});
						return;
					}

					target.attr("data-empty",0);

					ctrlErrorMsg.addClass('hide');
					target.attr("data-checked",1);

					data.val = val.join(",");
					data.val_id = val_id.join(",");
					target.data("cacheData",data);

					scFormCtrl.afterEvent(data,afterEvent,target);
				};

				target.on("click",'input:not(".'+_consts.rcOtherInputClassName+'")',common);
				target.on("check",common);

				target.on("input",'.'+_consts.rcOtherInputClassName,function(e){
					e.preventDefault();
					e.stopPropagation();

					//v2.0 增加单独其他输入框的方法处理
					var o = $(this);

					if(o.hasClass('autoCheck')){
						var rcInput = target.find("."+_consts.rcOtherClassName);
						if(rcInput.attr("type") == "radio"){
							target.find("."+_consts.rcOtherClassName).prop("checked",1);
						}else if(rcInput.attr("type") == "checkbox"){
							if($.trim(o.val()) == ""){
								target.find("."+_consts.rcOtherClassName).prop("checked",0);
							}else{
								target.find("."+_consts.rcOtherClassName).prop("checked",1);
							}
						}
					}

					_event.check(target);
				});

				//隐藏错误信息
				target.on("hideMsg",function(){
					ctrlErrorMsg.addClass('hide');
				});
			},
			// 下拉控件
			checkSelect:function(target,data,afterEvent){
				var pThis = this;
				var ctrlErrorMsg = target.find(".ctrlErrorMsg");

				function common(e){
					var eventType = e.type;

					var o = $(this);

					//是不是触发检查事件
					if(eventType == "check"){
						o = o.find('.'+_consts.selectClassName);
					}

					if(!o || o.length == 0){return;}

					var selectObj = o.find("option:selected"),
						val = selectObj.text(),
						val_id = selectObj.val();

					o.addClass('c-9b');

					if(val_id == ""){ val = "";}

					var ctrlErrorMsg = target.find(".ctrlErrorMsg");

					if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val, 'notnull')){
						pThis.errorMsg(ctrlErrorMsg,'请选择'+data.name,target,data,function(){
							data.val_id = "";
						});
						return;
					}

					//选中有效选项改变颜色
					if(selectObj.index() != 0){
						o.removeClass('c-9b');
					}

					target.attr("data-empty",0);

					ctrlErrorMsg.addClass('hide');
					target.attr("data-checked",1);

					data.val = val;
					data.val_id = val_id;
					target.data("cacheData",data);

					scFormCtrl.afterEvent(data,afterEvent,target);
				}

				target.on("change",'.'+_consts.selectClassName,common);
				target.on("check",common);

				//隐藏错误信息
				target.on("hideMsg",function(){
					ctrlErrorMsg.addClass('hide');
				});
			},
			//金额控件
			checkMoney:function(target,data,afterEvent){
				var pThis = this;
				var ctrlErrorMsg = target.find(".ctrlErrorMsg");
				var setTime = null;

				function common(e){
					var eventType = e.type;

					var o = $(this);

					//是不是触发检查事件
					if(eventType == "check"){
						o = o.find('.'+_consts.moneyClassName);
					}

					if(!o || o.length == 0){return;}

					val = o.val();

					var input_self = this;

	                if(target.data("chinaInput")){
	                	val = target.data("chinaInput");
	                	target.data("chinaInput","");
	                }

					var capitalizedObj = target.find("."+_consts.moneyCapitalizedClassName);

					//验证是否为空
					if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val === undefined ? "" : val, 'notnull')){
						//因为苹果机可以输入不合法的金额
						pThis.errorMsg(ctrlErrorMsg,'请输入正确的金额格式',target,data,function(){
							data.capitalized_val = "";
							capitalizedObj.html("");
						});

						var cacheVal = target.data("cacheVal");
						if(cacheVal){
							this.value = cacheVal.length == 1 ? "" : cacheVal;
							val = this.value;
							target.data("cacheVal",val);
						}

						//清除多余状态
						var moneyDetail = target.find(".moneyDetail");
						moneyDetail.addClass('hide');
						moneyDetail.attr("data-moneyDetail","");
						moneyDetail.html("");
						clearTimeout(setTime);
						return;
					}

					target.attr("data-empty",0);

					if(val != ""){
						target.data("cacheVal",val);

						if(val.toString().length >=13 && val > 999999999999){
							this.value = _consts.maxMoneyVal;
							val = _consts.maxMoneyVal;
						}
					}else{
						var cacheVal = target.data("cacheVal");
						if(cacheVal){
							this.value = Math.abs(cacheVal).toString().length == 1 ? "" : cacheVal;
							val = this.value;
							target.data("cacheVal",val);
						}
					}

					if(val > _consts.maxMoneyVal){
						pThis.errorMsg(ctrlErrorMsg,'超出数值最大范围',target,data,function(){
							data.capitalized_val = "";
							capitalizedObj.html("");
						});
						return;
					}
					
					var capitalized_val = "";
					if(capitalizedObj.length > 0){
						capitalized_val = (val == 0 || val == "") ? "" : tools.DX(Math.abs(val));
						capitalizedObj.html(capitalized_val == "" ? "" : "("+capitalized_val+")");
					}

					ctrlErrorMsg.addClass('hide');
					target.attr("data-checked",1);

					data.val = val;
					data.capitalized_val = capitalized_val;
					target.data("cacheData",data);

					scFormCtrl.afterEvent(data,afterEvent,target);
					/*
					//金额显示处理
					if(setTime){clearTimeout(setTime);}

					setTime = setTimeout(function(){
						if(o.val() == ""){
							var moneyDetail = target.find(".moneyDetail");
							moneyDetail.addClass('hide');
							moneyDetail.attr("data-moneyDetail","");
							moneyDetail.html("");
							return;
						}

						var moneyVal = _consts.money_type_icon[data.money_type] + val;
						target.find(".moneyDetail").removeClass('hide').attr("data-moneyDetail",moneyVal).text(moneyVal);
						o.val(val);
					},1000);
					*/
				}

				target.on("input",'.'+_consts.moneyClassName,common);
				target.on("check",common);

				//隐藏错误信息
				target.on("hideMsg",function(){
					ctrlErrorMsg.addClass('hide');
				});

				//点击金额详情
				target.on("click",".moneyDetail",function(){
					$(this).addClass('hide');
					var o = target.find('.'+_consts.moneyClassName);
					var val = o.val();
					o.val('').focus().val(val);
				});

				//输入框焦点消失事件
				target.on("blur",'.'+_consts.moneyClassName,function(){
					var moneyDetail = target.find(".moneyDetail");
					val = target.data("cacheVal");

					if(moneyDetail.html() != ""){
						moneyDetail.removeClass('hide');					
					}

					if(val == undefined || val == ""){
						var moneyDetail = target.find(".moneyDetail");
						moneyDetail.addClass('hide');
						moneyDetail.attr("data-moneyDetail","");
						moneyDetail.html("");
						return;
					}

					if(val == "NaN"){$(this).val("");return;}

					val = Number(val).toFixed(2);
					target.data("cacheData").val = val;
					target.data("cacheVal",val);
					var moneyVal = _consts.money_type_icon[data.money_type] + val;
					moneyDetail.removeClass('hide').attr("data-moneyDetail",moneyVal).text(moneyVal);
					$(this).val(val);
				});

				//针对中文输入法的状态而定
				target.find("."+_consts.moneyClassName).off('compositionstart').off('compositionend').off('oevent').on({
                    oevent:function(e){
 
                    },
                    compositionstart:function(){
                        val = target.data("cacheVal");

                        if(val == undefined || val == null){
                        	val = "";
                        }

                        this.value = val;

                        target.data("chinaInput",val);
                    },
                    compositionupdate:function () {
                    	val = target.data("cacheVal");

                    	if(val == undefined || val == null){
                        	val = "";
                        }

                        this.value = val;

                        target.data("chinaInput",val);
                    },
                    compositionend:function(){
                        val = target.data("cacheVal");

                        if(val == undefined || val == null){
                        	val = "";
                        }

                        this.value = val;
                        target.data("chinaInput",val);
                    }
                });
			},
			//数据转换
			dataChange:function(data){
				var type = data.type;

				try{
					if(type == "radio" || type == "checkbox" || type == "select"){
						data.opts = data.opts ? data.opts.split(',') : data.opts;
						data.opts_nums = data.opts_nums ? data.opts_nums.split(',') : data.opts_nums;
						data.val = data.val ? data.val.split(',') : data.val;
						data.val_id = data.val_id ? data.val_id.split(',') : data.val_id;
						//data.val_id = "1"; 测试用

						if(data.default_val){
							data.default_val = data.default_val.split(',');
							//没有选中值
							if(!data.val || data.val.length == 0){
								data.val_id = data.default_val;

								var arr = new Array();
								for (var i = 0; i < data.default_val.length; i++) {
									for (var j = 0; j < data.opts_nums.length; j++) {
										if(data.opts_nums[j] == data.default_val[i]){
											arr.push(data.opts[j]);
										}
									};
								};

								data.val = arr;
							}
						}
					}

					return data;
				}catch(e){
					return data;
				}
			}
		};

		$.fn.scFormCtrl = function(data,params){
			return $.fn.scFormCtrl.getCtrlBaseInfo(data,params);
		};

		//初始化方法，避免扩展插件引用不到资源
		$.fn.scFormCtrl._init = function(){
			if(!this._consts){
				this._consts = _consts;
				this._scFormCtrl = scFormCtrl;
				this._tools = tools;
				this._event = _event;
			}

			return this;
		};

		//获取基础控件
		$.fn.scFormCtrl.getCtrlBaseInfo = function(data,params){
			this._init();

			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				scCtrlState = params.scCtrlState; //1 详情 非1创建

			var type = data.type;

			//对特定控件数据做特殊处理
			var newData = scFormCtrl.dataChange($.extend({},data));

			var dom = scFormCtrl.getCtrlInfoDom(newData,scFormCtrlTpl,afterEvent,scCtrlState);

			//添加样式
			if(data.className){dom.addClass(data.className);}

			//可见可编辑绑定事件
			if(scCtrlState != "1" && data.visit_input == "1"){
				if(type == "text" || type == "textarea"){
					scFormCtrl.checkText(dom,data,afterEvent);

					//_event.keydown(dom);
				}else if(type == "radio" || type == "checkbox"){
					scFormCtrl.checkRadioOrCheckBox(dom,data,afterEvent);
				}else if(type == "select"){
					scFormCtrl.checkSelect(dom,data,afterEvent);
				}else if(type == "money"){
					scFormCtrl.checkMoney(dom,data,afterEvent);

					//_event.keydown(dom);
				}

				if(check || newData.val){_event.check(dom);}
			}

			return dom;
		};

	})(jQuery);

	return $.fn.scFormCtrl;
});