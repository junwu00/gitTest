define([
    'common:widget/lib/scFormCtrl/scFormCtrl-v2.0',
    'common:widget/lib/plugins/depts_users_select/js/depts_users_select',
    'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-userDept-v2.0.html',
    'common:widget/lib/plugins/juicer'
], function(scFormCtrl,depts_users_select,userDeptTpl,tpl) {
	(function($){
		var ctrl = $.fn.scFormCtrl._init();
		var _consts = ctrl._consts;
		var scFormCtrl = ctrl._scFormCtrl;
		var tools = ctrl._tools;
		var _event = ctrl._event;

		_consts.userDeptClassName = "scCtrlUserDept";
		_consts.userDeptHideNumber = 8;

		//初始化人员选择器
		scFormCtrl.initUserDept = function (target,select,canEdit,max_select,callback,init_callback){
            var base_dept_url = '/wx/index.php?model=legwork&m=ajax&a=assign_contact_dept',
                dept_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_sub_dept',
                user_url = '/wx/index.php?model=legwork&m=ajax&a=contact_list_user',
                user_detail = '/wx/index.php?model=legwork&m=ajax&a=contact_load_user_detail',
                init_selected_url = '/wx/index.php?model=legwork&m=ajax&a=contact_load_by_ids',
                get_common_url = "/wx/index.php?model=index&m=index&a=get_recent_user_depts",
				save_common_url = "/wx/index.php?model=index&m=index&a=save_recent_user_depts";

            var opt = {};
            //opt.target = target;
            //opt.select = select;
            opt.target = target;
            opt.select=select;//{'user':select} || {}; //{'user':['80237','80225']};
            if(select.user){
                opt.type = 2;
            }else if(select.dept){
                opt.type = 1;
            }
            opt.max_select = max_select || 0;
            opt.base_dept_url = base_dept_url;
            opt.dept_url = dept_url;
            opt.user_url = user_url;
            opt.user_detail = user_detail;
            opt.init_selected_url = init_selected_url;
            opt.get_common_url = get_common_url;
            opt.save_common_url = save_common_url;
            opt.canEdit = canEdit;
            //ob.get_depts_users_ids()
            //ob.get_depts_users()

            opt.init_call_back = function(that){
                if(init_callback && tools.isFunction(init_callback)){
                    init_callback(that.get_depts_users());
                }
            };

            opt.ret_fun = function(that){
                var result = that.get_depts_users();
                console.log(result);

                if(callback && tools.isFunction(callback)){
                    callback(result);
                }
            };

            var dept = new depts_users_select().init(opt);

            //if(!canEdit){dept.target.off('click');}

            return dept;
        };

        scFormCtrl.getUserDeptHtml = function(dom,type,data){
        	var scUserDeptContent = dom.find('.scUserDeptContent');

        	var html = tpl(userDeptTpl,{
        		getUserDeptContent:1,
        		type:type,
        		data:data,
        		defaultFace:defaultFace || ''
        	});

        	html = $($.trim(html));

        	scUserDeptContent.html(html);

        	//人员选择器查看更多
        	if(data.length > 0){
        		var str = type == "people" ? "人员" : "部门";
        		var className = type == "people" ? "userInfo" : "deptInfo";

	        	scUserDeptContent.find(".getAll").off().on('click',function() {
	        		var hideObj = scUserDeptContent.find("."+className+".hide");

	        		if(hideObj.length > 0){
	        			hideObj.removeClass('hide');
	        			$(this).html("隐藏更多");
	        		}else{
	        			var otherObj = scUserDeptContent.find("."+className+":gt("+(_consts.userDeptHideNumber - 1)+")");
	        			otherObj.addClass('hide');
	        			$(this).html("查看更多"+str+"("+otherObj.length+")");
	        		}
	        	});	
        	}
        };

        //公用部分，防止重复绑定事件
        scFormCtrl.userDeptBindEvent = {
        	flag:false,
        	init:function(){
        		if(!this.flag){
        			this.flag = true;
        			this.bindDom();
        		}
        	},
        	bindDom:function(){
        		$(window).on('hashchange', function() {
        			var hash = window.location.hash;

        			if(hash != "#allUser"){
        				$("body").find(".scUserDeptContent.all").remove();
        			}
        		});
        	}
        };

        //人员选择器数据处理
        scFormCtrl.userDeptEvent = function(target,data,callback,init_callback,userInfo){
			var select = null,ids = [];
			
			var default_type = data.default_type,
				default_val = data.default_val || data.default_value,
				type = data.type;

			try{
				if(data.ids === undefined || data.ids === null){
					if(default_type == 2 && default_val.length > 0){
						for (var i = 0; i < default_val.length; i++) {
							ids.push(default_val[i].id);
						};
					}else if(default_type == 1 && type == "people"){
						ids.push(userInfo.userId);
					}else if(default_type == 1 && type == "dept"){
						ids.push(userInfo.deptId);
					}
				}
			}catch(e){
				console.error("get userInfo error!");
			}

			if(data.ids && data.ids.length > 0){
				if(typeof data.ids == "string"){
					var arr = data.ids.split(",");
					for (var i = 0; i < arr.length; i++) {
						ids.push(arr[i]);
					};
				}
				
				//ids = data.ids;
			}

			if(data.type == "people"){
				select = {"user":ids};
			}else if(data.type == "dept"){
				select = {"dept":ids};
			}

			var canEdit = null;
			if(data.edit_input === undefined){
				canEdit = 1;
			}else{
			 	canEdit = data.edit_input;
			}

			var userDeptDom = target.find("."+_consts.userDeptClassName);
			var userDeptObj = this.initUserDept(userDeptDom,select,canEdit,data.select_max,callback,init_callback);

			target.data("userDeptObj",userDeptObj);
		};

		//人员部门选择器事件
		scFormCtrl.checkUserDept = function(target,data,afterEvent){
			var pThis = this;
			var ctrlErrorMsg = target.find(".ctrlErrorMsg");
			function common(e){
				var o = $(this),val = data.val;

				if(val == []){val = "";}

				//验证是否为空
				if(data.edit_input == 1 && data.must == 1 && !tools.regCheck(val, 'notnull')){
					pThis.errorMsg(ctrlErrorMsg,'请选择'+data.name,target,data,function(){
						//data.ids = "";
						//data.imgs = "";
					});
					return;
				}

				target.attr("data-empty",0);

				ctrlErrorMsg.addClass('hide');
				target.attr("data-checked",1);
				target.data("cacheData",data);

				scFormCtrl.afterEvent(data,afterEvent,target);

				//data.val = val;
				//o.data("cacheData",data);
			}

			target.off().on("check",common);

			//隐藏错误信息
			target.on("hideMsg",function(){
				ctrlErrorMsg.addClass('hide');
			});
		};

		//获取人员部门选择器事件
		$.fn.scFormCtrl.getCtrlExtendUserDept = function(data,userInfo,params){
			//this._init();

			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				scCtrlState = params.scCtrlState;

			var type = data.type;
			var dom = scFormCtrl.getCtrlInfoDom(data,userDeptTpl,afterEvent,scCtrlState);


			if(scCtrlState != "1"){
				//数据处理
				function dataChange(result,target,data){
					//重要
					data = $.extend(true,{},data);

					var aId = new Array(),
						aName = new Array(),
						aPic = new Array();
					for (var i = 0; i < result.length; i++) {
						aId.push(result[i].id);
						aName.push(result[i].name);
						if(result[i].pic_url != "undefined" || !result[i].pic_url){
							aPic.push(result[i].pic_url);
						}
					};
					data.val = aName.join(",");
					data.ids = aId.join(",");
					data.imgs = aPic.join(",");

					return data;
				}
				//初始化选择器
				scFormCtrl.userDeptEvent(dom,data,function(result){
					scFormCtrl.getUserDeptHtml(dom,type,result);

					data = dataChange(result,dom,data);
					scFormCtrl.checkUserDept(dom,data,afterEvent);
					//每次操作完进行检查
					_event.check(dom);
				},function(result){
					if(result.length > 0){
						scFormCtrl.getUserDeptHtml(dom,type,result);
					}

					scFormCtrl.userDeptBindEvent.init();

					data = dataChange(result,dom,data);
					dom.data("cacheData",data);
					scFormCtrl.checkUserDept(dom,data,afterEvent);

					if(check || result.length > 0){_event.check(dom);}
				},userInfo);
			}

			if(scCtrlState != "1" && data.visit_input == "1" && data.edit_input == "1"){
				//模拟事件触发人员部门选择器
				dom.find(".clickObj").on("click",function(){
					var o = $(this);

					o.addClass('current');

					setTimeout(function(){
						o.removeClass('current');
					},1000);

					dom.find("."+_consts.userDeptClassName).click();
				});
			}

			return dom;
		};
	})(jQuery);
});