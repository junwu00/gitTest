define([
    'common:widget/lib/scFormCtrl/scFormCtrl-v1.0',
    'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-files.html',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/GenericFramework-v2.0',
    'common:widget/lib/text!'+buildView+'/common//widget/lib/scFormCtrl/scFormCtrl-pics.html',
], function(scFormCtrl,filesTpl,tpl,GenericFramework,picsTpl) {
	(function($){
		var ctrl = $.fn.scFormCtrl._init();
		var _consts = ctrl._consts;
		var scFormCtrl = ctrl._scFormCtrl;
		var tools = ctrl._tools;
		var _event = ctrl._event;

		_consts.filesClassName = "scCtrlFiles";
		_consts.picsClassName = "scCtrlPics";

		//附件事件
		scFormCtrl.checkFiles = function(target,data,afterEvent){
			var pThis = this;
			var ctrlErrorMsg = target.find(".ctrlErrorMsg");
			function common(e){
				var o = $(this);

				var files = o.data("scCtrlFiles").files

				//验证是否为空
				if(data.file_type == 2 && files.length == 0){
					pThis.errorMsg(ctrlErrorMsg,'请上传附件',target,data,function(){
						//data.ids = "";
						//data.imgs = "";
					});
					return;
				}

				//是否有正在上传的文件
				if(o.data("scCtrlFiles").checkUploadStatus()){
					pThis.errorMsg(ctrlErrorMsg,'存在文件正在上传',target,data,function(){
					});
					return;
				}

				ctrlErrorMsg.addClass('hide');
				target.attr("data-checked",1);
				target.data("cacheData",data);

				scFormCtrl.afterEvent(data,afterEvent,target);
			}

			target.off().on("check",common);

			//隐藏错误信息
			target.off("hideMsg").on("hideMsg",function(){
				ctrlErrorMsg.addClass('hide');
			});
		};

		//附件
		$.fn.scFormCtrl.getCtrlExtendFiles = function(data,params){
			//this._init();

			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				detail = params.detail; //存粹显示数据，不能添加附件

			var dom = scFormCtrl.getCtrlInfoDom(data,filesTpl,afterEvent,detail);
			var pThis = this;

			//初始化
			var uploadFileObj = GenericFramework.uploadFile({
				buttonClass: detail ? "hide" : "file-btnClass",
				target:dom.find("."+_consts.filesClassName),
				pTarget:dom, //控件jq对象
				callback:{
					filesInitCallback:function(self){
						dom.data("scCtrlFiles",self);

						//数据处理
						if(data && data.files && data.files.length > 0){
							self.addFiles(data.files);

							dom.find(".filesCount").removeClass('hide').text("("+data.files.length+")");
						}
					},
					//文件正在上传
					filesUpload:function(){
						dom.attr("data-checked",0);
					},
					//图片上传完回调
					uploadCompleteCallback:function(self,files){
						//改变文件数量
						dom.find(".filesCount").removeClass('hide').text("("+files.length+")");

						if(dom.attr("data-checked") == 0){
							_event.check(dom);
						}
					},
					//删除图片回调
					delFileCallback:function(self,files){
						if(files.length == 0){
							dom.find(".filesCount").addClass('hide');
						}else{
							dom.find(".filesCount").removeClass('hide').text("("+files.length+")");
						}

						_event.check(dom);
					}
				}
			});

			scFormCtrl.checkFiles(dom,data,afterEvent);

			if(check){_event.check(dom);}

			return dom;
		};

		//图片控件事件
		scFormCtrl.checkPics = function(target,data,afterEvent){
			var pThis = this;
			var ctrlErrorMsg = target.find(".ctrlErrorMsg");
			function common(e){
				var o = $(this),val = data.val;

				if(!val){val = [];}

				//验证是否为空
				if(data.edit_input == 1 && data.must == 1 && val.length == 0){
					pThis.errorMsg(ctrlErrorMsg,'请上传图片',target,data,function(){
						//data.ids = "";
						//data.imgs = "";
					});
					return;
				}

				ctrlErrorMsg.addClass('hide');
				target.attr("data-checked",1);
				target.data("cacheData",data);

				scFormCtrl.afterEvent(data,afterEvent,target);
			}

			target.on("check",common);
		};

		//图片控件
		$.fn.scFormCtrl.getCtrlExtendPics = function(data,params){
			if(!params){params = {};}
			var afterEvent = params.afterEvent,
				check = params.check,
				detail = params.detail;
			
			var dom = scFormCtrl.getCtrlInfoDom(data,picsTpl,afterEvent,detail);

			//初始化
			var picList = dom.find('.'+_consts.picsClassName),
				picBtn = picList.find(".addPhotos");

			if(!(detail && data.edit_input != "1")){
				var pThis = this;

				var localInfo = {
		            image_list:data.val ? data.val : []
		        };

		        var picIndex = data.val ? data.val.length : 0;

				//判断选取范围
				var st = 3; //拍照或相册

				if(data.is_camera){
					st = 2;  //拍照
					picBtn.addClass('cameraIcon');
				}

				if(data.edit_input == 1){
					/*var newConfig = params.newConfig;
					var config = null;

					if(newConfig){
						$.ajax({
							url:"/wx/index.php?model=index&m=index&a=assign_jssdk_sign",
							async:false, //同步
							data:{
								url:encodeURIComponent(window.location.href.split("#")[0])
							},
							success:function(res){
								res = $.parseJSON(res);

								if(res.errcode == "0"){
									var info = res.info;
									config = {
										appId: info.app_id,
										debug: false,
										nonceStr: info.nonce_str,
										signature: info.sign,
										timestamp:info.time.toString()
									}
								}else{
									config = wxJsdkConfig;
								}
							},
							error:function(){
								config = wxJsdkConfig;
							}
						});
					}else{
						config = wxJsdkConfig;
					}*/

					config = wxJsdkConfig;

					GenericFramework.init(config);

		            GenericFramework.uploadFile({
		                target:picBtn,
		                type:"wx",
		                sourceType:st,//1 相册 2 相机 3 相册和相机
		                callback:{
		                    afterUpload:function(imgInfo){
		                        //console.log(imgInfo);
		                        var flag = false;
		                        for (var i = 0; i < localInfo.image_list.length; i++) {
		                        	if(localInfo.image_list[i].file_hash == imgInfo.hash){
		                        		flag = true;
		                        		break;
		                        	}
		                        };

		                        //文件重复上传处理
		                        if(flag){
		                        	var ctrlErrorMsg = dom.find(".ctrlErrorMsg");
		                        	ctrlErrorMsg.removeClass('hide').text("图片重复上传");

		                        	setTimeout(function(){
		                        		ctrlErrorMsg.addClass('hide');
		                        	},1000);
		                        	return;
		                        }

		                        var html = new Array();
		                        html.push('<div class="upload-pic fl" data-picIndex="'+picIndex+'">');
		                        html.push('<i class="rulericon-cancel_border_bold"></i>');
		                        html.push('<img src="'+imgInfo.image_path+'" alt=""/>');
		                        html.push('</div>');

		                        picList.prepend(html.join(''));

		                        localInfo.image_list.push({
		                            file_hash:imgInfo.hash,
		                            file_size:imgInfo.filesize,
		                            file_path:imgInfo.path,
		                            file_name:imgInfo.name,
		                            file_ext:imgInfo.ext,
		                            index:picIndex,
		                            image_path:imgInfo.image_path
		                        });

		                        picIndex ++;

		                        data.val = localInfo.image_list;

		                        if(dom.attr("data-checked") == 0){
		                        	dom.trigger('check');
		                        }

		                        //预览图片
		                        /*var picObj = picList.find('.upload-pic');
		                        GenericFramework.previewImage({
		                            target:picObj
		                        });*/

		                        //删除图片
		                       /* picObj.on('click','i',function(event){
		                            event.stopPropagation();
		                            var pic = $(this).parent(),pIndex = pic.attr("data-picIndex");

		                            for (var i = 0; i < localInfo.image_list.length; i++) {
		                                if(localInfo.image_list[i].index == pIndex){
		                                    localInfo.image_list.splice(i,1);
		                                    break;
		                                }
		                            };

		                            pic.remove();
		                        });*/
		                    }
		                }
		            });
				}

				//if(data.val && tools.isArray(data.val) && data.val.length > 0){
				//localInfo.image_list = data.val;
	            	//删除图片
					dom.on("click",'.upload-pic i',function(event){
						event.preventDefault();
						event.stopPropagation();
		                var pic = $(this).parent(),pIndex = pic.attr("data-picIndex");

		                for (var i = 0; i < localInfo.image_list.length; i++) {
		                    if(localInfo.image_list[i].index == pIndex){
		                        localInfo.image_list.splice(i,1);
		                        break;
		                    }
		                };

		                data.val = localInfo.image_list;

		                pic.off();
		                pic.remove();

		                _event.check(dom);
					});
				//}

				scFormCtrl.checkPics(dom,data,afterEvent);

				if(check || data.val){_event.check(dom);}
			}

			//预览图片
			if(detail && data.edit_input != "1"){
				GenericFramework.init(wxJsdkConfig);
			}
            dom.on("click",'.upload-pic',function(event){
            	event.preventDefault();
				event.stopPropagation();
            	var o = $(this),img = o.find("img"),
            		url = img.attr("src");

        		GenericFramework.previewImage({
        			target:img,
        			current:url,
        			urls:[url]
           		});
            });

			return dom;
		};
	})(jQuery);
});