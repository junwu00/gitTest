define([
	'jquery',
	'common:widget/lib/tools'
],function($,t){
	var selectType = {
		data:null,
		setting: null,
		_setting : {
			target:null, //添加的jq dom对象
			text:"请选择",
			data:null,
			callback: {
				finish_callback:null,
				change_callback:null
			},
		},
		checkParams: function(setting) {
			if(setting.target == null || !(setting.target instanceof jQuery)){
				console.log('错误的jq对象');
				return;
			}

			var _setting = t.clone(this._setting);
			$.extend(true, _setting, setting);

			this.setting = setting;

			return _setting;
		},
		//jqObj,data,finish_callback,bindDom_callback
		init:function(_setting){
			_setting = this.checkParams(_setting);

			if(!_setting.data){alert("数据为空");return;}

			this.data = _setting.data;

			_setting.target.html("").append(this.getHtml(_setting.data));

			if(!t.isFunction(_setting.callback.finish_callback)){return;}
			_setting.callback.finish_callback.apply(this);

			if(!t.isFunction(_setting.callback.change_callback)){return;}
			this.bindDom(_setting,_setting.callback.change_callback);
		},
		bindDom:function(_setting,callback){
			var pThis = this;
			_setting.target.on("change",function(){
				var selectObj = $(this).find("option:selected"),
				selectId = selectObj.attr("id"),
				selectData = pThis.getData(selectId);

				callback.apply(pThis,[selectId,selectData,selectObj]);
			});
		},
		getHtml:function(data){
			var html = new Array();

			html.push('<option id="0">请选择</option>');
			data.forEach(function(d){
				html.push('<option id="'+d.id+'">'+d.name+'</option>');
			});

			return html.join('');
		},
		getData:function(id){
			var data = this.data,res = null;

			data.forEach(function(d){
				if(d.id == id){
					res =  d;
					return;
				}
			});

			return res;
		}
	};

	return selectType;
});