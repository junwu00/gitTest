/*
 * author:Numver
 * ver:3.0
 * 3.0 新增样式调整和参数配置，更加实用与多种情况下的表格
*/
//UMD 通用式
define([
    'jquery',
    'common:widget/lib/tools',
    'common:widget/lib/UiFramework',
    'common:widget/lib/chartHelper',
    "common:widget/lib/scFormTable/scFormTable-v3.0",
    'common:widget/lib/text!'+buildView+'/common//widget/serviceWidget/chartTable/chartTable.html',
    'common:widget/lib/plugins/juicer',
    'common:widget/lib/plugins/swiper/swiper-3.4.1.jquery.min',
], function($,t,UiFramework,chartHelper,ScFormTable,chartTpl,tpl) {    
    // widget start
    function Widget(){}
    Widget.prototype.key = '';
    Widget.prototype.name = '';
    Widget.prototype.type = '';
    Widget.prototype.format = '';
    Widget.prototype.value = '';
    Widget.prototype.is_child = 0;

    Widget.prototype.setData = function(obj,searchData){
        if(obj && obj.type != 'dangerous'){
            // this.key = obj.input_key?obj.input_key:'';
            this.key = obj.row_key?obj.row_key:'';
            this.row_key = obj.row_key?obj.row_key:'';
            this.name = obj.name?obj.name:'';
            this.type = obj.type?obj.type:'';
            this.format = obj.format?obj.format:'';
            var key = this.getKey();
            if(searchData){
                searchData = JSON.parse(searchData);
                this.value = searchData[key]?searchData[key]:'';
            }else{
                this.value = '';
            }
        }
    }
    Widget.prototype.init_html = function(){
        return '';
    }
    Widget.prototype.init = function(){
        // do something
    }
    Widget.prototype.getValue = function(){
        var val = $('#input_'+this.key.replace('.','')).val();
        return val;
    }
    Widget.prototype.getInputValue = function(){
        var val = $('#input_'+this.key.replace('.','')).val();
        return val;
    }
    Widget.prototype.getKey = function(){
        return 'input_'+this.key.replace('.','');
    }
    //单行文本
    function Text(){
        Widget.call(this);
    }
    Text.prototype = Object.create(Widget.prototype);
    Text.prototype.init_html = function(){
        var html = '<div class="input_item"><div class="input_label">'+this.name+'</div>';
            html += '<div class="input_box"><input type="text" value="'+this.value+'" placeholder="请输入'+this.name+'" id="input_'+this.key.replace('.','')+'"></div></div>'
        return html;    
    }
    Text.prototype.getValue = function(){
        var val = $('#input_'+this.key.replace('.','')).val();
        var obj = this;
        if(this.type == "checkbox" || this.type == "radio" || this.type == "select" || this.type == "dept" || this.type == "people"){
            var arr = [];
                arr.push(val);
                obj.value = arr;
            return obj;
        }
        obj.value = val;
        return obj;
    }
    //人员选择器
    function User(){
        Widget.call(this);
    }
    User.prototype = Object.create(Widget.prototype);

    User.prototype.init_html = function(){
        var html = '<div class="input_item"><div class="input_label">'+this.name+'</div>';
            html += '<div class="input_box"><input type="text" value="'+this.value+'" placeholder="请输入'+this.name+'" id="input_'+this.key.replace('.','')+'"></div></div>'
        return html;    
    }
    User.prototype.getValue = function(){
        var val = $('#input_'+this.key.replace('.','')).val() || '';
        var obj = this;
        obj.value = val;
        return obj;
    }
    //时间区间选择器
    function Time(){
        Widget.call(this);
    }
    Time.prototype = Object.create(Widget.prototype);
    Time.prototype.init_html = function(){
        var html = '<div class="input_item"><div class="input_label">'+this.name+'</div>';
            var start_time = '',
                end_time = '';
            if(this.value.split(' - ').length == 2){
                start_time = this.value.split(' - ')[0].replace(' ','T');
                end_time = this.value.split(' - ')[1].replace(' ','T');
            }
            html += '<div class="input_box"><input type="datetime-local" value="'+start_time+'" placeholder="请输入开始时间" class="js_input_date input_'+this.key.replace('.','')+'" format="'+this.format+'" value=""></div><div class="input_box"><input type="datetime-local" placeholder="请输入结束时间" class="js_input_date input_'+this.key.replace('.','')+'" format="'+this.format+'" value="'+end_time+'"></div></div>';
        return html;    
    }
    Time.prototype.getValue = function(){
        var inputs = $('.input_'+this.key.replace('.',''));
        if($(inputs[0]).val() && $(inputs[1]).val()){
            var val =  $(inputs[0]).val().replace('T',' ') + ' - ' + $(inputs[1]).val().replace('T',' ');
        }else{
            var val = '';
        }
        var obj = this;
        obj.value = val;
        return obj;
    }
    Time.prototype.getInputValue = function(){
        var inputs = $('.input_'+this.key.replace('.',''));
        if($(inputs[0]).val() && $(inputs[1]).val()){
            return $(inputs[0]).val().replace('T',' ') + ' - ' + $(inputs[1]).val().replace('T',' ');
        }else{
            return '';
        }
    }
    //时间区间选择器
    function Create_time(){
        Widget.call(this);
    }
    Create_time.prototype = Object.create(Widget.prototype);
    Create_time.prototype.init_html = function(){
        var html = '<div class="input_item"><div class="input_label">'+this.name+'</div>';
            var start_time = '',
                end_time = '';
            if(this.value && this.value.value.split(' - ').length == 2){
                start_time = this.value.value.split(' - ')[0].replace(' ','T');
                end_time = this.value.value.split(' - ')[1].replace(' ','T');
            }
            html += '<div class="input_box"><input type="datetime-local" value="'+start_time+'" placeholder="请输入开始时间" class="js_input_date input_'+this.key.replace('.','')+'" format="'+this.format+'" value=""></div><div class="input_box"><input type="datetime-local" placeholder="请输入结束时间" class="js_input_date input_'+this.key.replace('.','')+'" format="'+this.format+'" value="'+end_time+'"></div></div>';
        return html;    
    }
    Create_time.prototype.getValue = function(){
        var inputs = $('.input_'+this.key.replace('.',''));
        var val = ''
        if($(inputs[0]).val() && $(inputs[1]).val()){
            val =  $(inputs[0]).val().replace('T',' ') + ' - ' + $(inputs[1]).val().replace('T',' ');
        }else{
            val = '';
        }
        var obj = this;
        obj.value = val;
        return obj;
    }
    Create_time.prototype.getInputValue = function(){
        return this.getValue();
    }
    //区间选择
    function Range(){
        Widget.call(this);
    }
    Range.prototype = Object.create(Widget.prototype);
    Range.prototype.init_html = function(){
        var html = '<div class="input_item"><div class="input_label">'+this.name+'</div>';
            var start_number = '',
                end_number = '';

            if(this.value.split(' - ').length == 2){
                start_number = this.value.split(' - ')[0];
                end_number = this.value.split(' - ')[1];
            }

            html += '<div class="input_box"><input type="number" value="'+start_number+'" placeholder="开始范围" class="input_'+this.key.replace('.','')+'" format="'+this.format+'" value=""></div><div class="input_box"><input type="number" placeholder="结束范围" class="input_'+this.key.replace('.','')+'" format="'+this.format+'" value="'+end_number+'"></div></div>';
        return html;    
    }
    Range.prototype.getValue = function(){
        var inputs = $('.input_'+this.key.replace('.',''));
        if($(inputs[0]).val() && $(inputs[1]).val()){
             var val = $(inputs[0]).val() + ' - ' + $(inputs[1]).val();
        }else{
             var val = '';
        }
        var obj = this;
        obj.value = val;
        return obj;
    }
    Range.prototype.getInputValue = function(){
        var inputs = $('.input_'+this.key.replace('.',''));
        if($(inputs[0]).val() && $(inputs[1]).val()){
             var val = $(inputs[0]).val() + ' - ' + $(inputs[1]).val();
        }else{
             var val = '';
        }
        return val;
    }

	var layerPackage = UiFramework.layerPackage(); 
    // mrc add
    var the_table_list = [];
    var items = [];
    var g_id = '',
        g_code = '',
        g_jqObj = '';
    var g_search_data = {};

    var chartTable = {
        init:function(option){
            this.id = option.id || 0;
            this.code = option.code;
            this.jqObj = option.jqObj;
            this.getChartTableUrl = option.getChartTableUrl;
            this.getChartDataUrl = option.getChartDataUrl;
            this.getFormTableDataUrl = option.getFormTableDataUrl;
            this.isApp = option.isApp != undefined ? option.isApp : 1;
            this.showScrollbars = option.showScrollbars;
            this.callback = option.callback;

            this.getChartTable(this.id,this.jqObj,this.isApp,this.code,{});
            // mrc add
            this.init_click();

            g_id = option.id;
            g_code = option.code;
            g_jqObj = option.jqObj;
            g_isApp = option.isApp;
        },
        // mrc add
        getItemsDataParams(items) {
            var data = {};
            // 固定的
            data['report_id'] = $('.chartTableTitle.current').attr('data-id') ? $('.chartTableTitle.current').attr('data-id') : $('#chartTableFirst').attr('data-id');
            data['code'] = null;
            data['page_size'] = 20;
            data['cond_list'] = [];

            if ($('#search_box .input_item').length > 0) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].type == "creater_id") {
                        var creater_id_arr = [];
                        creater_id_arr.push(items[i].getValue())
                        data['creater_id'] = creater_id_arr;

                        data['cond_list'].push(items[i].getValue());
                    } else if (items[i].type == "create_time") {
                        data['create_time'] = items[i].getValue();

                        data['cond_list'].push(items[i].getValue());
                    } else {
                        data['cond_list'].push(items[i].getValue());
                    }
                }
            }

            return data;
        },
        init_click: function(){
            localStorage.setItem('searchData', '');
            var that = this;
            $('body').on('click','#search_btn',function(e){
                var search_box = $('#search_box');
                    search_box.css('height',$(window).height()-44);
                    search_box.css('overflowY','scroll');

                e.stopPropagation();
                that.init_select_box();
                // 显示, 隐藏
                if(search_box.hasClass('d-n')){
                    search_box.removeClass('d-n');
                }else{
                    search_box.addClass('d-n');
                }
            });

            $('body').on('click','#search_commit',function(){
                layerPackage.lock_screen({color: '#2F7DCD'}); 
                // that.getChartTable(g_id,g_jqObj,g_isApp,g_code,getItemsData(items));
                $('#search_box').addClass('d-n').attr('data_active',1);
                setSearchData();
                let e_data = getItemsData(items)
                that.getFormTable({
                    id: e_data.report_id,
                    type: 2
                }, undefined, undefined);
            });

            $('body').on('click','#search_reset',function(){
                $(this).parent().find('input').val('')
            });

            function getItemsData(items){
                return that.getItemsDataParams(items)
            }

            function setSearchData(){
                var data = {};
                for(var i=0;i<items.length;i++){
                    var key = items[i].getKey();
                    data[key] = items[i].getInputValue();
                }
                localStorage.setItem('searchData', JSON.stringify(data));
                return data;
            }

        },  
        init_select_box:function(){
            var index = $('.chartTableTitle.current').index()==-1?0:$('.chartTableTitle.current').index();
            var search_key = the_table_list[index].row_list;
            var main_item = $('#search_box');
            var that = this;
            var searchData = localStorage.getItem('searchData');
            items = [];
            main_item.html(''); 

            // if(main_item.find('button').length == 0){
                for(input in search_key){
                    var obj = search_key[input];
                    var item = new Object();

                    if(obj.type == 'creater_id'){ // 创建人
                        item = new User();
                        item.init();
                    }else if(obj.type == 'create_time'){ // 创建时间
                        item = new Create_time();
                    }else{
                        if(obj.type == 'date'){
                            item = new Time();
                        }else if(obj.type == 'money'){
                            item = new Range();
                        }
                         else if(obj.type == 'dangerous'){
                             item = ''
                        } else {
                            item = new Text();
                        }
                    }
                    if(obj.type == 'muselect'){
                        obj.type = 'select';
                    }
                    // 报表控件筛选条件需屏蔽
                    if(item != ''){
                        item.setData(obj,searchData);
                        main_item.append(item.init_html());
                        item.init();
                        items.push(item);
                    }
                }
                main_item.append('<button id="search_commit">搜索</button>');
                main_item.append('<button id="search_reset">清除</button>');
            // }

            // function init_widgets(widgets_data){
            //     main_item.html('');
            //     for(var i=0;i<widgets_data.length;i++){
            //         var item = new Object();
            //         switch(widgets_data[i].type){
            //             case 'text':
            //                 item = new Text();
            //                 break; 
            //             case 'time':
            //                 item = new Time();
            //                 break;  
            //             case 'select':
            //                 // item = new Select();
            //                 break;           
            //             default:
            //                 // do something
            //                 console.log('something left?');
            //         }
            //         console.log(item);
            //         item.setData(widgets_data[i]);
            //         widgets.push(item);
            //         main_item.append(item.init_html());
            //     }
            //     for(var i=0;i<widgets.length;i++){
            //         widgets[i].init();
            //     }
            // }
            // init_widgets(search_key);
        }, // mrc add end
        getChartTable:function(id,jqObj,isApp,code,e_data){
            var pThis = this;

            var h = jqObj.innerHeight() - 121 - 43 - 2 - 44;
            var pageSize = Math.ceil(h / 33);

            // mrc add
            e_data.report_id = e_data.report_id?e_data.report_id:id;
            e_data.code = code;
            e_data.page_size = pageSize < 20 ? 20 : pageSize;
            // e_data.page_size = 10;

            console.log(e_data);

            t.ajaxJson({
                url:pThis.getChartTableUrl,
                data:{
                    // "data":{
                    //     report_id:id,
                    //     code:code,
                    //     page_size:pageSize < 20 ? 20 : pageSize
                    // }
                    "data": e_data
                },
                callback:function(result,status){
                    if(result.errcode != 0){
                        layerPackage.unlock_screen();//遮罩end
                        if(isApp == 1){
                        	layerPackage.fail_screen(result.errmsg);
                        	setTimeout(function(){
                        		window.history.go(-1);
                        	},2000);
                        }else{
                        	layerPackage.fail_screen(result.errmsg,2000);
                        }
                        
                        return;
                    }

                    // console.log(result);
                    the_table_list = result.data.table_list; // mrc add

                    var data = result.data;
                    data.first_data.table_data.data = pThis.tableDataFormat(data.first_data.table_data.data);
                    data.first_data.table_data.data = pThis.hiddenDataFormat(data.first_data.table_data.data);
                    pThis.cacheData = data;

                    //tpl.register('dateFormat', t.dateFormat);
                    var dom  = tpl(chartTpl,{
                        isApp:isApp,
                        data:data,
                        chartTable:1
                    });
                    //tpl.unregister('dateFormat');

                    pThis.jqObj.html($.trim(dom));

                    //var table_list = data.table_list;
                    //pThis.getTableList(table_list);
                    pThis.getFirstData(data.table_list,data.first_data);

                    pThis.bindDom(jqObj,isApp);

                    layerPackage.unlock_screen();
                    
                    //加载完报表配置后回调函数
                    if(pThis.callback && pThis.callback.initTablDataCallBack && t.isFunction(pThis.callback.initTablDataCallBack)){
                            pThis.callback.initTablDataCallBack.apply(pThis,[result]);  
                    }
                }
            });
        },
        cahceSwiper:[],
        hash:"chartDetail",
        currentIndex:0,
        tableIndex:0,
        bindSwiper:function(jqObj,index,callback){
            var pThis = this;
            var cacheData = pThis.cacheData;

            var mySwiper = new Swiper ('#swiper-container'+index, {
                // 如果需要分页器
                initialSlide :0,
                pagination: '#swiper-pagination'+index,
                preventLinksPropagation : false,
                paginationClickable :true,
                grabCursor : true,
                autoHeight: true,
                setWrapperSize :true,
                onInit: function(swiper){
                    //Swiper初始化了
                    pThis.cahceSwiper[index] = swiper;
                    //alert(swiper.activeIndex);提示Swiper的当前索引
                    jqObj.find('.ctList #swiper-container'+index).find(".swiper-slide").on("click",function(){
                        if(!jqObj.find("#chartTableSecond").hasClass('d-n')){
                            jqObj.find("#chartTableFirst").click();
                        }

                        var o = $(this);
                        //console.log(o.index());
                        var chartList = jqObj.find(".ctList[data-id="+index+"]");
                        var chartTableTop = jqObj.find("#chartTableTop");
                        var detail = o.find(".chartMain");

                        if(o.hasClass('loading')){
                            return;
                        }else{
                            o.addClass('loading');
                            window.location.hash = pThis.hash;
                            pThis.currentIndex = index;
                        }

                        swiper.lockSwipes();

                        jqObj.find("#swiper-pagination"+index).addClass('d-n');
                        chartList.find(".formTable").addClass("d-n");
                        //jqObj.find("#chartTableTop").addClass('downToTop');
                        swiper.onResize();
                        chartList.animate({"top": 0,"left":0},'fast',function(){
                            $(this).addClass('cover');
                            var chartDetail = detail.find(".chartDetail");
                            chartDetail.removeClass('limit');
                            detail.find(".pieTitle").removeClass('d-n');
                            chartDetail.find(".blTitle i").removeClass('d-n');

                            var chartInfo = detail.find(".chartInfo");
                            chartInfo.show();

                            swiper.update();

                            chartInfo.height(chartInfo.height);
                            var h =  detail.find(".chartInfo").innerHeight();

                            detail.find(".chartInfo").css("height",0);
                            detail.find(".chartInfo").animate({
                                "height":  h
                            }, 2000,function(){
                                //swiper.update();
                            });
                        });
                        chartTableTop.animate({"top":"-100%"});
                    });

                    if(callback && t.isFunction(callback)){
                        callback.apply(pThis,[swiper]);
                    }
                },
                onSlideChangeStart: function(swiper){
                    //alert(swiper.activeIndex);
                    var o = jqObj.find('.ctList #swiper-container'+index).find(".swiper-slide:eq("+swiper.activeIndex+")");
                    var id = o.attr("data-id"),name = o.attr("data-name"),type = o.attr("data-type"),
                        load = o.attr("data-load");

                    if(load == 1){return;}

                    var t_d = cacheData.table_list[index];

                    for (var j = 0; j < t_d.chart_list.length; j++) {
                        var c_d = t_d.chart_list[j];

                        if(c_d.id == id){
                            o.attr("data-load",1);
                            pThis.getChartData(c_d,function(){
                                swiper.update()
                            });
                            return;
                        }
                    };
                }
            });
        },
        bindDom:function(jqObj,isApp){
            var pThis = this;
            var chartTableFirst = jqObj.find("#chartTableFirst");
            var chartTableSecond = jqObj.find("#chartTableSecond");
            jqObj.find("#chartTableFirst").on("click",function(){
                if(chartTableSecond.hasClass('d-n')){
                    chartTableSecond.removeClass('d-n');
                }else{
                    chartTableSecond.addClass('d-n');
                }
            });

            jqObj.find("#chartTableSecond > span.chartTableTitle").on("click",function(e){
                // mrc add
                $('#search_box').html('').addClass('d-n').attr('data_active',0);

                e.preventDefault();
                e.stopPropagation();
                var o = $(this),id = o.attr("data-id"),index = o.index(),load = o.attr("data-load");

                var f_o =  chartTableFirst.find("span.chartTableTitle:first");

                pThis.tableIndex = index;

                if(f_o.attr("data-id") == id){
                    chartTableSecond.addClass('d-n');
                }else{
                    f_o.attr("data-id",id);
                    f_o.find("em").text(o.text());

                    o.addClass('current').siblings('.chartTableTitle').removeClass('current');

                    //do something
                    chartTableSecond.addClass('d-n');

                    jqObj.find(".ctList:eq("+index+")").addClass('current').siblings('.ctList').removeClass('current');

                    if(load == 0){
                        var table_data = pThis.cacheData.table_list[index];

                        if(isApp){
                            pThis.bindSwiper(jqObj,index,function(swiper){
                                this.getChartData(table_data.chart_list[0],null);
                                //swiper.slideTo(0, 1000, false);
                            });
                        }else{
                            var chart_list = table_data.chart_list;
                            for (var i = 0; i < chart_list.length; i++) {
                                pThis.getChartData(chart_list[i],null);
                            };
                        }

                        o.attr("data-load",1);

                        pThis.getFormTable(table_data);
                    }
                }
            });

            if(isApp){
                this.bindSwiper(jqObj,0);

                $(window).on('hashchange', function (e) {
                     var hash = window.location.hash;
                     if(hash == '#'+pThis.hash){
                         //pThis.showTypeList();
                         //console.log(1);
                     }else{
                         //pThis.hideTypeList();
                         //console.log(2);
                         pThis.hideChartDetail();
                     }
                });
            }else{
                //导出表格
                // jqObj.find(".formTable .tableDownload").on("click",function(e){
                //     var id = $(this).attr("data-id");

                //     if(pThis.callback && pThis.callback.tableDownload 
                //         && t.isFunction(pThis.callback.tableDownload)){
                //         pThis.callback.tableDownload.apply(pThis,[id]);  
                //     }
                // });

                jqObj.find(".formTable #export-drap-menu-btn").on("click", function (e) {
                    let id = $(this).attr("data-id");
                    layerPackage.lock_screen({
                        color: '#2F7DCD'
                    })
                    let dUrl = '/wx/index.php?model=process&m=report&a=export_table&table_id=' + id + '&export_type=2';
                    var filterParams = pThis.getItemsDataParams(items);
                    var condition = [];
                    for (var filterI in filterParams.cond_list) {
                        let field_key = filterParams.cond_list[filterI].getValue().row_key
                        let field_val = filterParams.cond_list[filterI].getValue().value || ''
                        if (Array.isArray(field_val)) {
                            field_val = field_val[0]
                        }
                        condition.push({
                            field_key: field_key,
                            field_val: field_val,
                        })
                    }
                    t.ajaxJson({
                        url: dUrl,
                        type: 'get',
                        data: {
                            "data": {
                                table_id: id,
                                condition: condition,
                                init_return_all: 1,
                                export_type: 2
                            }
                        },
                        callback: function (result, status) {
                            console.log(result);
                            layerPackage.unlock_screen()
                            if (result.errcode == 0) {
                                // btnMenuPackage.hide();
                                layerPackage.success_screen('系统生成文件中，请稍后到企业微信客户端查看文件！', 2000)
                            } else {
                                layerPackage.fail_screen(result.errmsg, 3000)
                            }
                        }
                    })
                })

                // jqObj.find(".formTable #export-drap-menu-btn").on("click", function (e) {
                //     var opt = {};
                //     opt.data = {};
                //     opt.data.id = $(this).attr("data-id");
                //     opt.color = "#2F7DCD";
                //     opt.menus = [];
                //     var btnMenuPackage = UiFramework.btnMenuPackage();
                //     var layerPackage = UiFramework.layerPackage();
                //     var m1 = {};
                //     m1.id = 1;
                //     m1.name = "导出PDF";
                //     m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
                //     m1.fun = function (el, m, o) {
                //         layerPackage.lock_screen({
                //             color: '#2F7DCD'
                //         })
                //         let dUrl = '/wx/index.php?model=process&m=report&a=export_table&table_id=' + o.id + '&export_type=1';
                //         var filterParams = pThis.getItemsDataParams(items);
                //         var condition = [];
                //         for (var filterI in filterParams.cond_list) {
                //             let field_key = filterParams.cond_list[filterI].getValue().row_key
                //             let field_val = filterParams.cond_list[filterI].getValue().value || ''
                //             if (Array.isArray(field_val)) {
                //                 field_val = field_val[0]
                //             }
                //             condition.push({
                //                 field_key: field_key,
                //                 field_val: field_val,
                //             })
                //         }
                //         t.ajaxJson({
                //             url: dUrl,
                //             type: 'get',
                //             data: {
                //                 "data": {
                //                     table_id: o.id,
                //                     condition: condition,
                //                     init_return_all: 1,
                //                     export_type: 1
                //                 }
                //             },
                //             callback: function (result, status) {
                //                 console.log(result);
                //                 layerPackage.unlock_screen()
                //                 if (result.errcode == 0) {
                //                     btnMenuPackage.hide();
                //                     layerPackage.success_screen('系统生成文件中，请稍后到企业微信客户端查看文件！', 2000)
                //                 } else {
                //                     layerPackage.fail_screen(result.errmsg, 3000)
                //                 }
                //             }
                //         })
                //     };
                //     // opt.menus.push(m1);
                //     var m2 = {};
                //     m2.id = 2;
                //     m2.name = "导出Excel";
                //     m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
                //     m2.fun = function (el, m, o) {
                //         layerPackage.lock_screen({
                //             color: '#2F7DCD'
                //         })
                //         let dUrl = '/wx/index.php?model=process&m=report&a=export_table&table_id=' + o.id + '&export_type=2';
                //         var filterParams = pThis.getItemsDataParams(items);
                //         var condition = [];
                //         for (var filterI in filterParams.cond_list) {
                //             let field_key = filterParams.cond_list[filterI].getValue().row_key
                //             let field_val = filterParams.cond_list[filterI].getValue().value || ''
                //             if (Array.isArray(field_val)) {
                //                 field_val = field_val[0]
                //             }
                //             condition.push({
                //                 field_key: field_key,
                //                 field_val: field_val,
                //             })
                //         }
                //         t.ajaxJson({
                //             url: dUrl,
                //             type: 'get',
                //             data: {
                //                 "data": {
                //                     table_id: o.id,
                //                     condition: condition,
                //                     init_return_all: 1,
                //                     export_type: 2
                //                 }
                //             },
                //             callback: function (result, status) {
                //                 console.log(result);
                //                 layerPackage.unlock_screen()
                //                 if (result.errcode == 0) {
                //                     btnMenuPackage.hide();
                //                     layerPackage.success_screen('系统生成文件中，请稍后到企业微信客户端查看文件！', 2000)
                //                 } else {
                //                     layerPackage.fail_screen(result.errmsg, 3000)
                //                 }
                //             }
                //         })
                //     };
                //     opt.menus.push(m2);
                //     btnMenuPackage.init(opt);
                // });
            }

            var pThis = this;
            var timer = 0;
            $(window).resize(function(event) {
               
                    var swiper = pThis.cahceSwiper[pThis.tableIndex];
                    if(swiper){
                        swiper.onResize();
                    }
                    var cacheChart = pThis.cacheChart;
                    for (var i = 0; i < cacheChart.length; i++) {
                        var c = cacheChart[i];
                        var opt = c.opt;

                        var chart = chartHelper.init(document.getElementById(c.id));

                        chartHelper.isLoading(chart,1);
                        chartHelper.setOption(chart,opt,1,1);
                        chartHelper.isLoading(chart);

                        if(c.isBind){
                            pThis.bindBarOrLineDom($("#"+c.id),chart,opt);
                        }else{
                            $("#"+c.id).parent().find(".pieTitle .legendTitle").each(function(){
                                chartHelper.bindPieClick(chart,$(this).text(),$(this));
                            });
                        }
                    };

                    var cacheFormTable = pThis.cacheFormTable;
                    for (var j = 0; j < cacheFormTable.length; j++) {
                        var obj = cacheFormTable[j].obj;

                        var jqObj = pThis.jqObj;
                        var h = jqObj.innerHeight() - jqObj.find("#chartTableTop").innerHeight()  - 43 - 2 - 44;
                        //h = h - 33 - 30 - 1;//33

                        obj.innerHeight(h);

                        cacheFormTable[j].s.init(cacheFormTable[j].params);
                    };

            });


            
            //点击表格区域滑动处理
            if(this.isApp == 1){
            	if(platid == 4 || platid == 5){
            		jqObj.on('click','.formTable',function(){
            			var o = $(pThis.jqObj.find('.ctList')[pThis.tableIndex]);
            			var chart_list = pThis.cacheData.table_list[pThis.tableIndex].chart_list;
            			var scrollHeight = o[0].scrollHeight;
            			var clientHeight = o[0].clientHeight;
            			var scrollTop = o.scrollTop();
            			
            			if(o.is(":animated") || chart_list == 0 || (clientHeight + scrollTop) == scrollHeight){
            				return;
            			}
            			o.animate({scrollTop: (scrollHeight - clientHeight) + 'px'},300);
            		});
            	}else{
            		jqObj.on('touchstart','.formTable',function(){
            			var o = $(pThis.jqObj.find('.ctList')[pThis.tableIndex]);
            			var chart_list = pThis.cacheData.table_list[pThis.tableIndex].chart_list;
            			var scrollHeight = o[0].scrollHeight;
            			var clientHeight = o[0].clientHeight;
            			var scrollTop = o.scrollTop();
            			
            			if(o.is(":animated") || chart_list == 0 || (clientHeight + scrollTop) == scrollHeight){
            				return;
            			}
            			o.animate({scrollTop: (scrollHeight - clientHeight) + 'px'},300);
            		});
            	}
            }
            
        },
        hideChartDetail:function(){
            try{
                var index = this.currentIndex;
                var swiper = this.cahceSwiper[index];
                var jqObj = this.jqObj;

                jqObj.find("#swiper-pagination"+index).removeClass('d-n');

                var sp = jqObj.find('.ctList #swiper-container'+index);

                var slide = sp.find(".swiper-slide");
                var chartDetail = slide.find(".chartDetail");
                chartDetail.addClass('limit');
                slide.find(".pieTitle").addClass('d-n');
                chartDetail.find(".blTitle i").addClass('d-n');

                var chartTableTop = jqObj.find("#chartTableTop");
                var chartList = jqObj.find(".ctList[data-id="+index+"]");

                chartTableTop.animate({"top":"0"},'fast');  
                chartList.animate({"top": chartTableTop.innerHeight()},'fast',function(){
                    $(this).removeClass('cover');

                    slide.find(".chartInfo").slideUp(function(){
                        $(this).removeAttr('style').css("display","none");

                        slide.removeClass('loading');

                        chartList.find(".formTable").removeClass("d-n");

                        swiper.update();
                        swiper.unlockSwipes();

                        if(swiper){
                            swiper.onResize();
                        }
                    }); 
                });
            }catch(e){
                console.log(e);
            }
        },
        getFirstData:function(data,first_data){
            var d_f = data[0];
            var d = d_f.chart_list;

            if(this.isApp){
                this.getChartData(d[0],null,first_data.chart_data[0]);
            }else{
                for (var i = 0; i < d.length; i++) {
                    this.getChartData(d[i],null,first_data.chart_data[i]);
                };
            }
            
            this.getFormTable(d_f,first_data.table_data);
        },
        getTableList:function(data){
            for (var i = 0; i < data.length; i++) {
                var d = data[i];

                this.getChartList(d.chart_list);
                //this.getFormTable(d);
            };
        },
        getChartList:function(chartList){
            for (var i = 0; i < chartList.length; i++) {
                var c = chartList[i];

                this.getChartData(c);
            };
        },
        cacheChart:[],
        getChartData:function(chartData,callback,data){
            if(!chartData) {return;}

            //1 饼 2 柱形 3 线形
            var type = chartData.chart_type,
                id = chartData.id,
                name = chartData.name,
                pThis = this;

            var obj = $("#chart_"+id);
            var chart = chartHelper.init(obj[0]);

            chartHelper.isLoading(chart,1,{
                text: '加载中...',
                color: '#c23531',
                textColor: '#000000',
                maskColor: 'rgba(255, 255, 255,0.8)',
                zlevel: 0
            });

            function commonChartFuc(result){
                if(result && result.data && result.data.length == 0){
                    obj.addClass('noData');
                    return;
                }

                if(type == 1){
                    pThis.getPie(obj,chart,chartData,result,id);
                }else if(type == 2 || type == 3){
                    pThis.getBarOrLine(obj,chart,chartData,result,id,type);
                }

                chartHelper.isLoading(chart);

                if(callback && t.isFunction(callback)){
                    callback.apply(pThis,[result]);
                }
            }

            if(data){
                commonChartFuc(data);
                return;
            }

            t.ajaxJson({
                url:pThis.getChartDataUrl + "?time="+new Date().getTime(),
                data:{
                    "data":{
                        chart_id:id
                    }
                },
                callback:function(result,status){
                    if(result.errcode != 0){
                        obj.addClass('noData');
                        console.log("error");
                        return;
                    }

                    commonChartFuc(result);
                    return;

                    if(type == 1){
                        pThis.getPie(obj,chart,chartData,result,id);
                    }else if(type == 2 || type == 3){
                        pThis.getBarOrLine(obj,chart,chartData,result,id,type);
                    }

                    //pThis.bindDom();

                    chartHelper.isLoading(chart);

                    if(callback && t.isFunction(callback)){
                        callback.apply(pThis,[result]);
                    }
                }
            });
        },
        /*饼图*/
        getPie:function(obj,chart,chartData,result,id){
        	var pThis = this;
            //var obj = $("#chart_"+id);
            //var chart = chartHelper.init(obj[0]);

            var data = this.getPieData(chartData.row_list,result.data);

            if(!data){return;}

            var title = chartData.name || chartData.row_list[0].name;

            var params = this.getPieOption(title,data,function(params){
                var showPart = obj.next(".showPart");

                showPart.find(".txtString").text(params.name+ "(" +params.seriesName+ ")");
                showPart.find(".value").text(params.value);
                showPart.find(".percent").text("("+params.percent+"%"+")");
            },function(name,titleObj){
                chartHelper.bindPieClick(chart,name,titleObj);

                obj.parent().find(".pieTitle").append($(titleObj));
            },chartData,result);

            var opt = chartHelper.myOption("pie",params.series,1,1);

            //PC 显示
            if(!this.isApp){
                opt.legend.orient =  'vertical';
                opt.legend.left = 'left';
                opt.legend.top = 0;
                if(chartData.name.length > 0){
           		 	opt.title.text = chartData.name;
           	 	}
                chartHelper.setDownLoadBtn(opt);
                //opt.toolbox.right = 300;
            }

            opt = chartHelper.getOption(params,opt);
            opt = chartHelper.pieAppOption(opt);

            chartHelper.setOption(chart,opt);

            this.cacheChart.push({
                id:obj.attr("id"),
                chart:chart,
                opt:opt
            }); 
        },
        getPieOption:function(name,data,tooltipCallback,legendTitleCallback,chartData,result){
            var pThis = this;
            var titleArr = new Array();
            var params =  {
                series: [{
                    name: name,
                    type: 'pie',
                    selectedOffset:0,
                    selectedMode:'single',
                    label: {
                        normal: {
                            show: true,
                            position: 'outside',
                            formatter:function(params){
                                titleArr.push({
                                    val:params.value,
                                    percent:params.percent,
                                    name:params.name,
                                    color:chartHelper.color[titleArr.length]
                                });

                                if(titleArr.length == data.length){
                                    pThis.getChartInfo(chartData,data,result,titleArr);
                                }

                                return params.percent+'%';
                            }
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    data: data
                }],
                tooltip:function(params,ticket,callback){
                    if(tooltipCallback && t.isFunction(tooltipCallback)){
                        tooltipCallback(params);
                    }
                }
            };

            if(this.isApp){
                params.legendTitle = function(name,obj){
                    if(legendTitleCallback && t.isFunction(legendTitleCallback)){
                        legendTitleCallback(name,obj);
                    }
                };
            }

            return params;
        },
        getPieData:function(rowList,data){
            if(!data){return;}
            var arr = new Array();
            
            for (var i = 0; i < data.length; i++) {
                arr.push({
                    name:data[i][0] || "其他",
                    value:data[i][1]
                });
            };

            return arr;
        },
        minNum:14,
        /*柱形OR折线*/
        getBarOrLine:function(obj,chart,chartData,result,id,type){
            //var obj = $("#chart_"+id);
            //var chart = chartHelper.init(obj[0]);

            var data = this.getBarOrLineData(chartData.row_list,result.data,type,id);

            if(!data){return;}

            var legend = data.legend,xAxis = data.xAxis,series = data.series;
            var params = this.getBarOrLineOption(legend,xAxis,series,function(params){
                var showPart = obj.next(".showPart");
                try{
                    if(t.isArray(params)){
                        var arr = new Array();

                        for (var i = 0; i < params.length; i++) {
                            if(params[i].value){
                                arr.push('<span class="mr20">');
                                arr.push('<i class="c-9b mr6 fsn">'+params[i].seriesName+'</i>');
                                arr.push("<i style='color:"+params[i].color+"' class='fs24 lhn fsn'>"+params[i].value+"</i>");
                                arr.push('</span>');
                            }
                        };

                        showPart.find(".seriesName").text(params[0].name);
                        showPart.find(".txtString").html(arr.join(''));
                    }
                }catch(e){
                    console.log(e);
                }
            });

            var opt = chartHelper.myOption(type == 2 ? "bar" : "line",params.series,1,1);

            //PC 显示
            if(!this.isApp){
            	 if(chartData.name.length > 0){
            		 opt.title.text = chartData.name;
            	 }
                 chartHelper.setDownLoadBtn(opt);
                 //opt.toolbox.right = 300;
            }

            var len = data.sCount;
            opt.sCount = len;

            if(len > this.minNum){
                opt = chartHelper.setDataZoom(opt,this.minNum,xAxis.length,len);
            }

            opt = chartHelper.getOption(params,opt);
            if(type == 3){
                opt = chartHelper.xAxisBoundaryGap(opt);
            }
            opt = chartHelper.xAxisLabel(opt);

            var indexArr = new Array();

            if(type == 3){
                for (var z = 0; z < opt.series.length; z++) {
                   //chartHelper.addMarkLine(opt.series[z],"middle").addMarkPoint(opt.series[z]);
                };
            }

            opt = chartHelper.showNumber(opt,type == 2 ? "all" : "noShow");
            chartHelper.setOption(chart,opt);

            this.cacheChart.push({
                id:obj.attr("id"),
                chart:chart,
                opt:opt,
                isBind:1
            });

            this.getChartInfo(chartData,data,result);

            this.bindBarOrLineDom(obj,chart,opt);
        },  
        getBarOrLineData:function(rowList,data,type,id){
            var t = type == 2 ? "bar" : "line";

            var xAxis = new Array();
            var legend = new Array();
            var series = new Array();
            var sCount = 0;

            for (var i = 0; i < rowList.length; i++) {
                var name = rowList[i].name;
                legend.push(name);
                series[i] = {data:[],name:name,type:t};
            };

            for (var j = 0; j < data.length; j++) {
                var d = data[j];

                xAxis.push(d[0] || "其他");
                d.splice(0,1);

                for (var k = 0; k < d.length; k++) {
                     series[k].data.push(d[k]);
                };

                sCount += d.length;
            };

            var obj = {
                xAxis:xAxis,
                legend:legend,
                series:series,
                sCount:sCount
            };

            return obj;
        },
        getBarOrLineOption:function(legend,xAxis,series,tooltipCallback){
            var params = {
                legend:legend,
                xAxis:[{
                    data:xAxis
                }],
                series: series,
                tooltip:function(params){
                    if(!t.isFunction(tooltipCallback)){return;}
                    tooltipCallback(params);
                }
            };

            return params;
        },
        bindBarOrLineDom:function(obj,chart,opt){
            var pThis = this;
            obj.data("click",false);

            obj.parent().find(".blTitle i").off().on("click",function(){
                var click = obj.data("click");
                //click = !click;
                obj.data("click",!click);

                if(click){
                    $(this).addClass('rulericon-enlarge').removeClass('rulericon-shrink');
                }else{
                    $(this).addClass('rulericon-shrink').removeClass('rulericon-enlarge');
                }

                if(opt.sCount > pThis.minNum){
                    opt = chartHelper.useMyTool(opt,click,null,null,pThis.minNum,opt.xAxis[0].data.length,opt.sCount);
                }
                chartHelper.setOption(chart,opt,true);
            });    
        },
        getChartInfo:function(chartData,data,result,titleArr){
            var color = chartHelper.color;
            var type = chartData.chart_type;
            var id = chartData.id;

            var legend = data.legend;
            var xAxis = data.xAxis;
            var newData = new Array();
            var count = 0;

            if(type == 1){
                var cacheChart = this.cacheChart;

                newData = titleArr;

                for (var j in newData) {
                    count += parseInt(newData[j].val);
                };
            }else if(type == 2 || type == 3){
                for (var i = 0; i < result.data.length; i++) {
                    var arr = new Array();
                    var r = result.data[i];
                    for (var j = 0; j < r.length; j++) {
                        arr.push({
                            val:r[j],
                            name:legend[j],
                            color:color[j]
                        });
                    };
                   
                    newData[xAxis[i]] = arr;
                };
            }

            tpl.register('formatMoney', t.formatMoney);
            var dom  = tpl(chartTpl,{
                data:newData,
                count:count,
                chartInfo:1,
                type:type
            });
            tpl.unregister('formatMoney');

            var chartInfo = $("#chartInfo_"+id);

            chartInfo.append($.trim(dom));
        },
        /*表格*/
        cacheFormTable:[],
        getFormTable:function(data,first_data,callback){
            var id = data.id,
                type = data.type,
                name = data.name;

            var pThis = this;
            var isUseCount = type == 2 ? 1 : 0;
            var isApp = this.isApp;

            var pageSize = 20;

            function commonTableFuc(result){
                var obj = $("#formTable_"+id);

                var jqObj = pThis.jqObj;
                var h = jqObj.innerHeight() - jqObj.find("#chartTableTop").innerHeight() - 43 - 2 - 44;
                //h = h - 33 - 30 - 1;//33

                obj.innerHeight(h);

                var s = new ScFormTable();
                var sortKey = "",order = 0;

                var f_x = 0,f_y = 0;
                //配置固定列
                var fixColNum = 0;
                // var fixColNum = 0;  //取消固定列


                //总数据量
                var sum = result.sum;
                var page_length = result.page_length;
                var endLoad = 1;

                var thead = result.data.thead;
                var tbody = result.data.tbody;
                var tfoot = result.data.tfoot;

                var params = {
                    target:obj,
                    data:{
                        thead:thead,
                        tbody:tbody,
                        tfoot:tfoot
                    },
                    base:{
                        isApp:isApp,
                        fixColNum:fixColNum,
                        //hideThead:1,
                        //maxTdWidth:150,
                        //minTdWidth:120,
                        showScrollbars:pThis.showScrollbars,
                        checkBox:{
                            //state:1,
                            className:"aaa11"
                        },
                        //limitTdWidth:0
                        //theadEvent:0
                        //autoResize:"no",
                        //mergeTable:0
                    },
                    css:{
                        hasBorder:1,
                        /*
                        theadThClass:"t1", //仅局限于thead 全部th 的样式
                        tfootThClass:"t2", //仅局限于thead 全部th 的样式
                        tbodyTdClass:"t3",

                        tbodyTrClass:"tbodyTrClass",
                        */

                        //noOddEvenClass:1
                    },
                    callback:{
                        theadClick:function(o,name,sort,key,self){
                            //console.log(name,sort,key,self);
                            //添加新数据
                            //self.addTbodyData($("#test"),res.sc3.tbody2);

                            pThis.setFormTableData(id,isUseCount,0,pageSize,function(res){
                                if(res.data.length == 0){
                                    return;
                                }

                                s.restartTbody(obj,res.data);

                                s.restartPageTools({
                                    index:1
                                });

                                //自适应用。可去掉
                                for (var i = 0; i < pThis.cacheFormTable.length; i++) {
                                    var ft = pThis.cacheFormTable[i];
                                    if(ft.id == "formTable_"+id){
                                        ft.data = res;
                                        break;
                                    }
                                };
                            },sortKey,order);
                        },
                        getScrollxy:function(x,y,target,self){
                        	
                        	var o = $(pThis.jqObj.find('.ctList')[pThis.tableIndex]);
                        	var chart_list = pThis.cacheData.table_list[pThis.tableIndex].chart_list;
                        	var scrollHeight = o[0].scrollHeight;
                        	var clientHeight = o[0].clientHeight;
                        	var scrollTop = o.scrollTop();
                        	if(this._y == undefined){
                        		this._y = 0;
                        	}
                        	
                        	if(y != 0){
                        		this._y = y;
                        	}
                        	
                            if(this._y != 0 && y == 0 && isApp == 1 && chart_list.length > 0 && scrollTop > 0 && scrollHeight == (clientHeight + scrollTop)){
                            	if(o.is(":animated")){
                            		return;
                            	}
                            	o.animate({scrollTop: '0px'},300);
                            	this._y = 0;
                            	return;
                            }
                            

                            var mTable = target.find(".mTable:first");
                            var wrapper = target.find("#wrapper");
                            var trLen = page_length;

                           // console.log(trLen);
                            var c = mTable.height() - wrapper.height();
                            //滚动到底部
                            if(y == c && page_length < sum && endLoad){
                                //console.log("====");
                                endLoad = 0;
                                pThis.setFormTableData(id,isUseCount,trLen,pageSize,function(res){
                                    var data = res.data;
                                    
                                    data = pThis.tableDataFormat(data);
                                    data = pThis.hiddenDataFormat(data);
                                    self.addTbodyData(obj,data.tbody);

                                    //自适应数据保存
                                    for (var i = 0; i < pThis.cacheFormTable.length; i++) {
                                        var ft = pThis.cacheFormTable[i];
                                        if(ft.id == "formTable_"+id){
                                            for (var j = 0; j < data.tbody.length; j++) {
                                                ft.data.data.tbody.push(data.tbody[j]);
                                            };

                                            page_length +=  res.page_length;
                                            ft.page_length = page_length;
                                            
                                            break;
                                        }
                                    };

                                    pThis.setFormTablePageMsg(obj,page_length,sum);

                                    endLoad = 1;
                                    $(window).trigger('resize')
                                });
                            }
                        },
                        initCallBack:function(){
                        	var pageLengthT = result.page_length;
                            if(pageLengthT < page_length){
                                pageLengthT = page_length;
                            }
                        	pThis.setFormTablePageMsg(obj,pageLengthT,sum);
                        },
                        checkBoxClick:function(data,theadHeight){
                            console.log(data,theadHeight);  
                        }
                    }
                };

                //自适应用。可去掉
                pThis.cacheFormTable.push({
                    id:obj.attr("id"),
                    obj:obj,
                    data:result,
                    s:s,
                    params:params,
                    page_length : result.page_length
                });

                s.init(params);
            }

            if(first_data){
                commonTableFuc(first_data);
                return;
            }

            this.setFormTableData(id,isUseCount,null,pageSize,function(result){
                let second_table = pThis.hiddenDataFormat(result.data)
                commonTableFuc({...result,data:second_table});
            },undefined,undefined);
        },
        setFormTableData:function(id,count,page,pageSize,callback,sortKey,order){
        	var pThis = this;
        	var filterParams = pThis.getItemsDataParams(items);
        	var condition = [];
        	for (var filterI in filterParams.cond_list) {
        	    let field_key = filterParams.cond_list[filterI].getValue().row_key
        	    let field_val = filterParams.cond_list[filterI].getValue().value || ''
        	    if (Array.isArray(field_val)) {
        	        field_val = field_val[0]
        	    }
        	    condition.push({
        	        field_key: field_key,
        	        field_val: field_val,
        	    })
        	}
            if(pageSize === undefined || pageSize == null){pageSize = 15;}
            if(page === undefined || page == null){page = 0;}
            var pThis = this;
            if(this.isApp == 1){
            	layerPackage.lock_screen({color: '#2F7DCD'});	
            }else{
            	$('.showPageInfo').children('p:eq(0)').addClass('d-n');
            	$('.showPageInfo').children('p:eq(1)').removeClass('d-n');
            }
            t.ajaxJson({
                url:pThis.getFormTableDataUrl,
                data:{
                    "data":{
                        table_id:id,
                        count:count,
                        start:page,
                        page_size:pageSize,
                        // page_size:10,
                        key:sortKey,
                        order:order,
                        condition:condition,
                        init_return_all:1
                    }
                },
                callback:function(result,status){
                    layerPackage.unlock_screen();	
                	if(pThis.isApp == 1){
                		layerPackage.unlock_screen();	
                	}else{
                		$('.showPageInfo').children('p:eq(1)').addClass('d-n');
                    	$('.showPageInfo').children('p:eq(0)').removeClass('d-n');
                	}
                    callback(result);
                    var data = result.data;
                    data.first_data = {}
                    var tbody = result.data.tbody;
                    var table_list = result.table_list[pThis.tableIndex];
                    var chart_data = [];
                    for(let chart of table_list.chart_list){
                        console.log(chart)
                        var chart_data_dict = {};
                        chart_data_dict['data'] = [];
                        for(let body of tbody){
                            var bodyresult = []
                            for(let key of chart.col_key){
                                bodyresult.push(body.data.filter(index => index.key.split('_')[0] == key)[0].val)
                            }
                            for(let key of chart.value_key){
                                bodyresult.push(body.data.filter(index => index.key.split('_')[0] == key)[0].val)
                            }
                            chart_data_dict['data'].push(bodyresult)
                        }
                        chart_data.push(chart_data_dict)
                    }
                    console.log(chart_data)
                    data.table_list = result.table_list;
                    data.first_data.chart_data = chart_data;
                    var d_f = data.table_list[pThis.tableIndex];
                    var d = d_f.chart_list;
                    if(pThis.isApp){
                        pThis.getChartData(d[0],null,data.first_data.chart_data[0]);
                    }else{
                        for (var i = 0; i < d.length; i++) {
                            pThis.getChartData(d[i],null,data.first_data.chart_data[i]);
                        };
                    }
                    
                }
            });
        },
        //分页记录处理
        setFormTablePageMsg:function(obj,pageLength,sum){
        	var hSpan = obj.next(".showPageInfo").children('span');
            obj.next(".showPageInfo").html("<p class='ta-c'>第1条~第<span>"+pageLength+"</span>记录,总共"+sum+"条</p><p class='loading-notice d-n'>加载中，请稍候...</p>");
        },
        //数据格式换
        tableDataFormat:function(data){
        	var tmpData = $.extend(true,{},data);
        	//找出需要标识千分位的下标head
        	var changeIndexs = [];
        	
        	var lastHead = tmpData.thead[tmpData.thead.length -1].data;
        	for ( var i in lastHead) {
				if(lastHead[i].type && lastHead[i].type == 'number'){
					changeIndexs.push(i)
				}
			}
        	
        	//修改千分位body,foot
        	for ( var i in tmpData.tbody) {
				var row = tmpData.tbody[i].data;
				for ( var j in changeIndexs) {
					var v = row[changeIndexs[j]].val;
					// v = t.formatMoney(v);
					row[changeIndexs[j]].val = v;
				}
			}
        	
        	for ( var i in tmpData.tfoot) {
        		var row = tmpData.tfoot[i].data;
				for ( var j in changeIndexs) {
					var v = row[changeIndexs[j]].val;
					// v = t.formatMoney(v);
					row[changeIndexs[j]].val = v;
				}
			}
        	
        	return tmpData;
        },
        // 隐患控件显示处理
        hiddenDataFormat:function(data){
            var tmpData = $.extend(true,{},data);
        	var changeIndexs = [];
        	
        	var lastHead = tmpData.thead[tmpData.thead.length -1].data;
        	for ( var i in lastHead) {
				if(lastHead[i].type && lastHead[i].type == 'dangerous'){
					changeIndexs.push(i)
				}
			}
            for(var i in tmpData.tbody){
                var row = tmpData.tbody[i].data
                for ( var j in changeIndexs) {
                    let form_val = row[changeIndexs[j]].val
                    form_val = JSON.parse(form_val || '{}')
                    val_html = ''
                    if (!form_val.danger_list) {
                        form_val['snake_list'] = form_val['snake_list'] || []
                        form_val['snake_list'].forEach((snakeItem, aindex) => {
                            let snakeStatue = ''
                            if (form_val['val'] && Number(form_val['val'][aindex]) == 1) {
                                snakeStatue = '<span style="color: #7CD168;">（√）</span>'
                            }
                            if (form_val['val'] && Number(form_val['val'][aindex]) == 2) {
                                snakeStatue = '<span style="color: red;">（×）</span>'
                            }
                            if (form_val['val'] && Number(form_val['val'][aindex]) == 3) {
                                snakeStatue = '<span style="color: red;">（无）</span>'
                            }
                            console.log(Number(form_val['val']));
                            val_html += `<p><span>${!form_val['val'] ? '--' : form_val['val'][aindex] == 0 ? '--' : snakeItem}</span>${snakeStatue}</p>`
                        })
                    } else {
                        let dangerous_list = [];
                        for (let i = 0; i < form_val.danger_list.length; i++) {
                            dangerous_list[i] = {
                                ...form_val.danger_list[i],
                                name: form_val['snake_list'][i],
                                color: form_val.danger_list[i].val == 1 ? '#7CD168' : form_val.danger_list[i].val == 2 ? 'red' : form_val.danger_list[i].val == 3 ? '#f59d2b' : '' ,
                                icon: form_val.danger_list[i].val == 1 ? '（√）' : form_val.danger_list[i].val == 2 ? '（×）' : form_val.danger_list[i].val == 3 ? '（无）' : '',
                                red: form_val.danger_list[i].val == 2 ? 'red' :'',
                            }
                        }
                        for (let item of dangerous_list) {
                            let snakeStatue = `<span style="color: ${item.color};">${item.icon}</span>`
                            val_html += `<p style="color: ${item.val == 2 ? 'red' : '#000000'}"><span>${!item.val ? '--' : item.name}</span>${snakeStatue}</p>`

                            if (item.val == 2) {
                                let beforeImg = ''
                                item.before_img.map(e => {
                                    beforeImg += `<img class="previewImage" style="margin: 5px 5px 5px 0;cursor: pointer;" width="48" height="48" src="${e.image_path}" alt="" />`
                                })
                                let afterImg = ''
                                item.after_img.map(e => {
                                    afterImg += `<img class="previewImage" style="margin: 5px 5px 5px 0;cursor: pointer;" width="48" height="48" src="${e.image_path}" alt="" />`
                                })
                                if (beforeImg.length > 0) {
                                    val_html += '<div><p style="margin: 0;">隐患图片</p>' + beforeImg + '</div>'
                                }
                                if (afterImg.length > 0) {
                                    val_html += '<div><p style="margin: 0;">整改后图片</p>' + afterImg + '</div>'
                                }
                            }
                        }
                    }
                    row[changeIndexs[j]].val = val_html;

				}
            }
            console.log(tmpData);
            return tmpData
        }
    };

    return chartTable;
});