<div id="bottom-menu">
	<ul class="nav nav-pills" role="tablist">
		<{foreach from=$APP_MENU item=m}>
			<{if !isset($m['childs'])}>
				<li style="width:<{(100 / ($APP_MENU|count))|string_format:"%.1f"}>%">
					<a href="<{$m['url']}>">
						<{if !empty($m['icon'])}>
							<i class="<{$m['icon']}>"></i> 
						<{/if}>
						<{$m['name']}>
					</a>
				</li>
			<{else}>
				<li class="dropdown" style="width:<{(100 / ($APP_MENU|count))|string_format:"%.1f"}>%">
		        	<a id="<{$m['id']}>" href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          		<{$m['name']}>
		          		<span class="has-submenu"></span>
		        	</a>
		        	<ul class="dropdown-menu" role="menu" aria-labelledby="<{$m['id']}>" style="top: -<{45 * (($m['childs']|count))}>px;">
			        	<{foreach from=$m['childs'] item=s}>
			          		<li role="presentation">
			          			<a id="<{$s['id']}>" role="menuitem" tabindex="-1" href="<{$s['url']}>">
									<{if !empty($s['icon'])}>
										<i class="<{$s['icon']}>"></i> 
									<{/if}>
			          				<{$s['name']}>
			          			</a>
			          		</li>
		        		<{/foreach}>
		        	</ul>
		      	</li>
			<{/if}>
      	<{/foreach}>
    </ul>
</div>