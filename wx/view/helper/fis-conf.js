require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'helper');
fis.match('*', {
	release:'/helper/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});