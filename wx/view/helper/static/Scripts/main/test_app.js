define([
	'jquery',
	'common:widget/lib/underscore',
	'common:widget/lib/backbone',
	'common:widget/lib/text!'+buildView+'/helper/static/Templates/test_template.html'
],function($,_,Backbone,statsTemplate){

	var AppView = Backbone.View.extend({
		// tagName : "li",
		// className:"testUl",
		el:"#main-container",

		template: _.template( statsTemplate ),

		events:{
			"click .f1" : "testClick"
		},

		//初始化函数
		initialize: function() {
			console.log("init?!");
			this.$el.html( this.template());

			//this.listenTo(this.model, 'visible', this.render);
		},

		render:function(){
			this.$el.html( this.template(this.model.attributes));

			console.log(this.$el.find(".f1").text());

			return this;
		},

		testClick:function(e){
			alert("haha");

			console.log(e);
		}
	});

	return AppView;
});