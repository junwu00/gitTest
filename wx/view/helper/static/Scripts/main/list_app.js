define([
	'jquery',
	'common:widget/lib/underscore',
	'common:widget/lib/backbone'
],function($,_,Backbone){

	var AppView = Backbone.View.extend({
		// tagName : "li",
		// className:"testUl",
		el:"#main-container",

		//template: _.template( statsTemplate ),

		events:{
			"click li" : "testClick",
			'click table' : 'tableClick'
		},

		//初始化函数
		initialize: function() {
			this.$table = this.$el.find('table');
			this.$input = 

			this.render();
		},

		render:function(){
			this.listData = this.getListData({});

			return this;
		},

		getListData:function(obj){
			var url = obj.url == null ? 'http://wx.ew.test/wx/index.php?app=advice&m=ajax&cmd=103' : obj.url;

			$.ajax({
				url : url,
				type:'post',
				data:{ajax_act:'',data:{"page":1,"state":"1"}},
				dataType : 'json',
				context:this,
				success:function(res){
					console.log(res);

					this.$table.html("列表数据: " + JSON.stringify(res.data));

					//this.showTable(res.data.data);

				},error:function(){
					console.log('get list data error!!!');
				}
			});
		},
		showTable:function(data){
			//console.log(data);
			this.$table.bootstrapTable({
				//url: 'data1.json',
			    columns: [{
			        field: 'id',
			        title: 'Item ID'
			    }, {
			        field: 'title',
			        title: 'Title'
			    }, {
			        field: 'detail',
			        title: 'Detail'
			    }],
			    data: data
			});
		},

		testClick:function(e){
			console.log(e);
		},
		tableClick:function(){
			alert(1);
		}
	});

	return AppView;
});