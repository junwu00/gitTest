define([
	'common:widget/lib/UiFramework'
],function(UiFramework){

	var btn = UiFramework.btnPackage;

	btn.slideBtn.addSlideBtnDom({
		target:$('.slide-button'),
		on:function(target,obj){
			if(!target.hasClass('off')){
				alert('打开了');
			}
		},off:function(target,obj){
			if(target.hasClass('off')){
				alert('关闭了');
			}
		}
	});

});