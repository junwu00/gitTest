requirejs.config({
	baseUrl: '/',
	paths: requirejs_path_conf,
	map: {
        '*': {
            'css': 'common:widget/lib/require_css',
        }
    },
	shim: {
		"common:widget/lib/underscore": {
			exports: '_'
		},
		"common:widget/lib/backbone": {
			deps: [
				'common:widget/lib/underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		'common:widget/lib/bootstrap/dist/js/bootstrap.min': ['css!view/common/widget/lib/bootstrap/dist/css/bootstrap.min.css'],
		'common:widget/lib/bootstrap-table/bootstrap-table.min':['css!view/common/widget/lib/bootstrap-table/bootstrap-table.min.css']
	}
});

require(['helper:static/Scripts/main/list_app'], function(ListAppView) {
	console.log($);
	//console.log(_);
	//console.log(Backbone);

	new ListAppView();
});
