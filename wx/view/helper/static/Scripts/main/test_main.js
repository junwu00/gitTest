requirejs.config({
	baseUrl: '/',
	paths: requirejs_path_conf,
	map: {
        '*': {
            'css': 'common:widget/lib/require_css',
        }
    },
	shim: {
		"common:widget/lib/underscore": {
			exports: '_'
		},
		"common:widget/lib/backbone": {
			deps: [
				'common:widget/lib/underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	}
});

require(['helper:static/Scripts/main/test_app'], function(TestAppView) {
	console.log($);
	//console.log(_);
	//console.log(Backbone);

	new TestAppView();
});
