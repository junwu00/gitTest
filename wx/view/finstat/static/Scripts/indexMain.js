require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework'
],function($,t,UiFramework) {
	t.ajaxJson({
		url:'index.php?model=finstat&m=ajax&cmd=101',
		data:{},
		callback:function(result,status){
			if(result.errcode != 0){
				alert(result.errmsg);
				return;
			}
			var data = result.info.data;
			var html = '';
			for (var i = 0; i < data.length; i++) {
				var file = data[i];
				html += '<div data-href="'+file.url+'">';
				html += '<p><span class="fileIcon"></span><span class="name hideTxt">'+file.file_name+'</span></p>';
				html += '</div>';
			}
			$('#main-container').append(html);
			$('#main-container').on('click','div',function(){
				location.href = $(this).attr('data-href');
			})
		}
	});
	
});