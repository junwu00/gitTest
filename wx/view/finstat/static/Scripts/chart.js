requirejs.config({
    shim: {
        "jquery":[
            "css!common:widget/ui/base.css",
            "css!common:widget/ui/common.css",
            "css!common:widget/ui/font-3ruler/font-3ruler.css"
        ]
    }
});

require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/serviceWidget/chartTable/chartTable',
    'finstat:static/Scripts/commonData',
],function($,t,UiFramework,chartTable,commonData) {
	UiFramework.menuPackage.init();
    var layerPackage = UiFramework.layerPackage(); 
    layerPackage.lock_screen({color: '#2F7DCD'});//遮罩start

    var id = t.getUrlParam("id"),ajaxUrl = commonData.ajaxUrl,
        code = t.getUrlParam("preview_code");

    chartTable.init({
        id:id,
        code:code,
        jqObj:$("#main-container"),
        getChartTableUrl:ajaxUrl.getChartDetail,
        getChartDataUrl:ajaxUrl.getChartData,
        getFormTableDataUrl:ajaxUrl.getFormTableData,
        getFilterDataUrl:ajaxUrl.getFilterData,
        isApp:0,
        callback:{
            tableDownload:function(id){
                console.log(id);
            }   
        }
    });
});