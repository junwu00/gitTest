define({
	ajaxUrl:{
		//获取报表详情105
		getChartDetail:"/wx/index.php?model=finstat&m=ajax&cmd=105",
		//获取表格数据106
		// getFormTableData:"index.php?model=finstat&m=ajax&cmd=106",
		// getFormTableData:"/wx/index.php?model=finstat&m=ajax&cmd=201",
		getFormTableData:"/wx/index.php?model=finstat&m=ajax&cmd=203",

		//获取图表数据107
		getChartData:"/wx/index.php?model=finstat&m=ajax&cmd=107",

		//过滤报表关联字段
		getFilterData:"/wx/index.php?model=finstat&m=ajax&cmd=202"
	}
});