require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'finstat');
fis.match('*', {
	release:'/finstat/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});