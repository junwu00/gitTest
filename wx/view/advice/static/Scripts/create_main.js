require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/selectType',
	'common:widget/lib/GenericFramework'
],function($,t,UiFramework,selectType,GenericFramework) {
	var checkBoxBtn = UiFramework.btnPackage().checkBoxBtn;
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start
	layerPackage.unlock_screen();

	//请求地址
	var typeUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=101",
	saveOrEditUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=102",
	getDetailUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=112",
	removeDetailUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=114",
	draftSendUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=107",
	replyUrl = "index.php?model=advice&m=ajax&a=index&cmd=113";

	var id = t.getHrefParamVal("id") || null;

	//jqObj对象
	var container = $("#main-container"),
	typeObj = container.find("#adviceType").find("select"),
	title = container.find("#title"),
	mark = container.find("#mark"),
	picList = container.find(".picList"),
	msg = container.find("#msg"),
	ctrl = container.find("#ctrl"),
	info = container.find("#info"),
	page = $("#finishPage"),
	fileCtrl = container.find(".fileUpload #sign-upload");

	info.find("img").attr("src",(userInfo.pic || ""));
	info.find(".username").text((userInfo.userName || ""));
	info.find(".dept").text((userInfo.deptName || ""));

	var ays = 0,classify_id = 0; //创建用
	var class_id = 0,edit_ays = 0;//编辑用

	//底部菜单
	var menu = [],
	save_menu = {
		width:30,
		name:"保存",
		iconClass:"icon-save",
		btnColor:"w",
		id:null,
		click:function(self,obj,index){
			var obj = {
				url:saveOrEditUrl,
				clickUrlParam:"&activekey=0",
				state:0 //草稿
			};

			if(save_menu.id){obj.id = save_menu.id;}

			commonCtrl.link(obj);
		}
	},send_menu = {
		width:70,
		name:"提交",
		btnColor:"g",
		iconClass:"icon-plus-sign",
		draftSendUrl:null,
		id:null,
		click:function(self,obj,index){
			var obj = {
				url:saveOrEditUrl,
				state:1 //实时
			};

			if(send_menu.draftSendUrl){obj.url = send_menu.draftSendUrl;}
			if(send_menu.id){obj.id = send_menu.id;}

			commonCtrl.link(obj,true);
		}
	};

	//上传附件操作
	var uploadFileObj = GenericFramework.uploadFile({
		target:fileCtrl
	});

	//uploadFileObj.addFiles();

	//公用操作
	var commonCtrl = {
		link:function(params,showPage){
			layerPackage.lock_screen();//遮罩start

			var titleTxt = $.trim(title.val()),markTxt = $.trim(mark.val());

			if(classify_id == 0){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("请选择分类");
				return;
			}
			if(!titleTxt){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("请填写标题");
				return;
			}
			if(!markTxt){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen("请填写内容");
				return;
			}

			params.classify_id = classify_id;
			params.title = titleTxt;
			params.content = markTxt;
			params.ays = ays;

			var files = uploadFileObj.getUploadFiles();
			if(files.length > 0){
				params.file_hash = files;
			}

			if(!params.clickUrl){params.clickUrl = "/wx/index.php?debug=1&app=advice&a=mineList";}
			if(!params.clickUrlParam){params.clickUrlParam = "";}

			t.ajaxJson({
				url:params.url,
				data:{
					"data":params
				},
				callback:function(result,status){
					if(result.errcode !="0"){
						layerPackage.unlock_screen();//遮罩end
						layerPackage.fail_screen(result.errmsg);
						return;
					}

					layerPackage.unlock_screen();
					layerPackage.success_screen(result.errmsg);

					if(showPage){
						var newId = null;

						if(result.info && result.info.id){
							newId = result.info.id;
						}else{
							newId = params.id;
						}

						page.removeClass('hide').animate({
							top: 0,
						},"normal", function() {
							page.find("#page_title").text(titleTxt);
							page.find("#page_detail").text(markTxt);
							page.find("#page_detail_link").attr("href","/wx/index.php?debug=1&app=advice&a=detail&type=1&activekey=1&id="+newId);
						});

						page.on("click","#reply",function(){
							layerPackage.lock_screen();
							layerPackage.ew_confirm({
								title:"确定撤回该计划",
								ok_callback:function(){
									t.ajaxJson({
										url: replyUrl,
										data: {
											"data": {
												"id":newId
											}
										},
										callback: function(result, status) {
											if (status == false && result == null) {return;}

											if (result.errcode != 0) {
												layerPackage.unlock_screen();//遮罩end
												layerPackage.fail_screen(result.errmsg);
												return;
											}

											layerPackage.unlock_screen();
											layerPackage.success_screen(result.errmsg);
											
											window.location.href = params.clickUrl;
										}
									});
								}
							});
						});
						
						return;
					}

					window.location.href = params.clickUrl + params.clickUrlParam;
				}
			});
		},
		addBottomBtn:function(){
			menu.push(save_menu);
			menu.push(send_menu);

			UiFramework.wxBottomMenu.init({
				target:container,
				menu:menu
			});
		},
		delLink:function(id){
			layerPackage.lock_screen();
			t.ajaxJson({
				url: removeDetailUrl,
				data: {
					"data": {
						"id": id
					}
				},
				callback: function(result, status) {
					if (result.errcode != 0) {
						layerPackage.ew_alert({
							desc: result.errmsg
						});
						return;
					}

					layerPackage.success_screen(result.errmsg);

					window.location.href = "index.php?debug=1&app=advice&a=mineList&activekey=0";
				}
			});
		}
	};

	//ctrl
	checkBoxBtn.init({
		status:false,
		target:ctrl.find(".btn1"),
		callback: {
			on:function(){
				ays = 1;
				info.find("div:last").removeClass('hide').siblings('div').addClass('hide');
			},
			off:function(){
				ays = 0;
				info.find("div:first").removeClass('hide').siblings('div').addClass('hide');
			}
		}
	});

	t.ajaxJson({
		url:typeUrl,
		callback:function(result,status){
			if(status == false && result == null){return;}

			if(result.errcode != 0){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen(result.errmsg);
				return;
			}

			var data = result.info.data;

			selectType.init({
				target:typeObj,
				data:data,
				callback:{
					finish_callback:function(){
						if(id){
							t.ajaxJson({
								url:getDetailUrl,
								data:{
									"data":{
										"id":id,
										"type":1
									}
								},
								callback:function(result,status){
									if(status == false && result == null){return;}

									if(result.errcode != 0){
										layerPackage.unlock_screen();
										layerPackage.fail_screen(result.errmsg);
										return;
									}

									var data = result.info;
									
									class_id = data.class_id;
									edit_ays = data.ays;

									typeObj.find('option[id='+class_id+']').prop("selected",true).change();
									title.val(data.title);
									mark.val(data.detail);
									
									uploadFileObj.addFiles(data.file);
									
									menu.push({
										width: 30,
										name: "删除",
										iconClass: "icon-trash",
										btnColor: "r",
										click: function(self, obj, index) {
											layerPackage.ew_confirm({
												desc: "是否删除",
												ok_callback: function() {
													commonCtrl.delLink(id);
												}
											});
										}
									});

									save_menu.id = id;
									send_menu.width = 40;
									send_menu.draftSendUrl = draftSendUrl;
									send_menu.id = id;

									commonCtrl.addBottomBtn();
								}
							});
						}
					},
					change_callback:function(selectId,selectData,selectObj){
						if(selectId == 0 || !selectData){
							msg.addClass('hide');
							ctrl.addClass('hide');
							info.addClass('hide');
							return;
						}

						classify_id = selectId;

						//是否匿名操作
						var anonymous = selectData.anonymous;
						console.log(selectData);

						msg.removeClass('hide');
						if(anonymous == 0){
							msg.find("p:first").removeClass('hide').siblings('p').addClass('hide');
							ctrl.addClass('hide');
						}else{
							msg.find("p:last").removeClass('hide').siblings('p').addClass('hide');
							ctrl.removeClass('hide');
						}

						info.find("div:first").removeClass('hide').siblings('div').addClass('hide');
						info.removeClass('hide');

						if(id && edit_ays == 1 && classify_id == class_id){
							info.find("div:last").removeClass('hide').siblings('div').addClass('hide');
							checkBoxBtn.changeStatus();
							return;
						}

						if(checkBoxBtn.isClick()){
							checkBoxBtn.changeStatus();
						}
					}
				}
			});
		}
	});

	if(!id){
		commonCtrl.addBottomBtn();
	}

	UiFramework.bottomInfo.init(container,77);
});