//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/advice/Templates/list.html',
],function(t,tpl,listTemplate){

    var list = {
        listObj:null,
        dom:"",
        $dom:null,
        state:null,
        pageCount:null,
        type:null,
        getList:function(params,callback){
            if(!params.keyworks){params.keyworks = "";}
            if(!params.asc){params.asc = 0;}//0：按时间倒序排，1:按时间顺序排
            if(!params.listUrl){return;}
            if(!params.type){return;}
            if(!t.checkJqObj(params.listObj)){return;}

            var pThis = this,listObj = params.listObj;
            pThis.params = params;

            t.ajaxJson({
                url:params.listUrl,
                data:{
                    "data":{
                        "state":params.state,
                        "key_work":params.keyworks,
                        "asc":params.asc,
                        "page":params.page
                    }
                },
                callback:function(result,status){
                    if(status == false && result == null){return;}

                    if(result.errcode != 0){t.console(result.errmsg,1);return;}

                    if(pThis.state != params.state){
                        pThis.state = params.state;
                        pThis.dom = "";
                        pThis.$dom = "";
                        pThis.pageCount = null;
                    }
                    var data = result.info.data;
                    pThis.pageCount = result.info.count;

                    listObj.find("section").remove();
                    listObj.find(".line1").remove();
                    
                    //console.dir(data);

                    if(data.length > 0){
                        listObj.find("div").addClass('hide');

                        tpl.register('dateFormat', t.dateFormat);
                        var dom = tpl(listTemplate,{
                            listData:data,
                            type:params.type,
                            activekey : params.state
                        });
                        tpl.unregister('dateFormat');

                        pThis.dom = pThis.dom + dom;
                        pThis.$dom = $($.trim(pThis.dom));

                        listObj.append(pThis.$dom);
                    }else{
                        listObj.find("div").removeClass('hide');
                    }
                    
                    pThis.listObj = listObj;

                    pThis.bindDom();

                    if(!t.isFunction(callback)){return;}
                    callback(data,pThis.pageCount);
                }
            });
        },
        bindDom:function(){
            var pThis = this,state = pThis.params.state;

            this.listObj.find('section').on('click',function(){
                var id = $(this).attr("data-id");

                if(pThis.params.callback && t.isFunction(pThis.params.callback.bindDom)){
                    pThis.params.callback.bindDom.apply(pThis,[{
                        obj:pThis,id:id,state:state
                    }])
                    return;
                }
            });
        }
    };

	return list;
});