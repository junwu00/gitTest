require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'advice:static/Scripts/list'
],function(UiFramework,t,l) {
//*
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	//layerPackage.lock_screen();

	var listUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=105";
	var listObj = $('.advice_list');

	var activekey = t.getHrefParamVal("activekey") || 1;

	var listMenu = UiFramework.listMenuPackage();
	var pageNum = 1,pageCount = 1; //当前页数
	var akey = 0;

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'未完成','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'已完成','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:activekey
		},
		callback: {
			onMenuClick: function(data, obj,listMenuObj){
				layerPackage.lock_screen();

				pageNum = 1;
				pageCount = 1;

                listObj.find("div.line1").remove();
                listObj.find("section").remove();

                akey = data.activekey;

                UiFramework.bottomInfo.remove();

                listMenuObj.showRecord();
  
				UiFramework.scrollLoadingPackage().init({
					target:listObj,
					scroll_target:listObj,
					fun:function(self){
						if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

						if(pageNum != 1){
							self.add_scroll_waiting($("#powerby"),1);
						}
						
						l.getList({
							"state":akey,
							"listUrl":listUrl,
							"listObj":listObj,
							"page":pageNum,
							"type":2,
							callback:{
								bindDom:function(res){
									var state = res.state,id = res.id;

									window.location.href = "/wx/index.php?debug=1&app=advice&a=detail&type=2&id="+id+"&activekey="+akey;
								}
							}
							//"keyworks":keyworks,
							//"asc":asc
						},function(data,pCount){
							layerPackage.unlock_screen();
							//数据空时显示提示
							if(!data || data.length == 0){
								listMenuObj.showRecord();
								return;
							}

							pageCount = pCount;
							
							pageNum ++;
							listObj.attr("is_scroll_loading",false);

							if(data.length < 10){
								listObj.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting(listObj);

							UiFramework.bottomInfo.init(listObj);
						});
					}
				});

				listObj.scroll();
			}
		},
		noRecord : {
			target:listObj,
			imgSrc : feekbackDeal,//图片地址
			title:'暂无可处理的反馈信息',//内容标题
			desc:['目前你的处理列表中没有需要处理的反馈信息，','若出现新反馈信息你将会有消息提醒！'] //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
},function(e){
	requireErrBack(e);
});