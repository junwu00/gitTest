require([
	'common:widget/lib/UiFramework',
	'common:widget/lib/tools',
	'advice:static/Scripts/list'
],function(UiFramework,t,l) {
//*
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	//layerPackage.lock_screen();

	var listUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=103";
	var listObj = $('.advice_list');

	var activekey = t.getHrefParamVal("activekey") || 1;

	var listMenu = UiFramework.listMenuPackage();
	var pageNum = 1,pageCount = 1; //当前页数
	var akey = 0;

	var setting = {
		//列表菜单配置
		target: $('#listMenu'), //添加的jq dom对象
		menu: { //配置头部插件菜单
			contents: [
				{'name':'未完成','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'已完成','activekey':2,'other_data':{'name1':'12123','name2':'aa'}},
				{'name':'草稿','activekey':0,'other_data':{'name1':'12123','name2':'aa'}}
			],
			activekey:activekey
		},
		callback: {
			onMenuClick: function(data, obj,listMenuObj){
				layerPackage.lock_screen();

				pageNum = 1;
				pageCount = 1;

                listObj.find("div.line1").remove();
                listObj.find("section").remove();

                akey = data.activekey;

                UiFramework.bottomInfo.remove();

                listMenuObj.showRecord();
  
				UiFramework.scrollLoadingPackage().init({
					target:listObj,
					scroll_target:listObj,
					fun:function(self){
						if(pageCount < pageNum){self.remove_scroll_waiting(listObj);return;}

						if(pageNum != 1){
							self.add_scroll_waiting($("#powerby"),1);
						}

						l.getList({
							"state":akey,
							"listUrl":listUrl,
							"listObj":listObj,
							"page":pageNum,
							"type":1,
							callback:{
								bindDom:function(res){
									var state = res.state,id = res.id;
									//草稿
									if(state == 0){
										window.location.href = "/wx/index.php?debug=1&app=advice&a=create&id="+id;
										return;
									}

									window.location.href = "/wx/index.php?debug=1&app=advice&a=detail&type=1&id="+id+"&activekey="+akey;
								}
							}
							//"keyworks":keyworks,
							//"asc":asc
						},function(data,pCount){
							layerPackage.unlock_screen();
							//数据空时显示提示
							if(!data || data.length == 0){
								listMenuObj.showRecord();
								return;
							}

							pageCount = pCount;
							
							pageNum ++;
							listObj.attr("is_scroll_loading",false);

							if(data.length < 10){
								listObj.attr("is_scroll_end",true);
							}

							self.remove_scroll_waiting(listObj);

							UiFramework.bottomInfo.init(listObj);
						});
					}
				});

				listObj.scroll();
			}
		},
		noRecord : {
			target:listObj,
			imgSrc : feekbackMine,//图片地址
			title:'暂无反馈信息',//内容标题
			desc:['你的反馈信息暂时没有在处理中的，如需查看','发起过的反馈请进入已完成进行查看'] //内容说明，数组形式 或者 单个字串
		}
	};

	listMenu.init(setting);
},function(e){
	requireErrBack(e);
});