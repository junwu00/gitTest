//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/advice/Templates/detail.html',
	'common:widget/lib/GenericFramework'
],function(tools,UiFramework,tpl,detailTpl,GenericFramework){
	var layerPackage = UiFramework.layerPackage();

	var getDetailUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=112",
	addCommentUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=104",
	replyUrl = "index.php?model=advice&m=ajax&a=index&cmd=113",
	timeUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=116",
	activateUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=115",
	dealMySelfUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=109",
	cancelMySelfUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=119",
	completeUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=108",
	closeUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=118",
	getCommentListUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=111",
	getCtrlUrl = "/wx/index.php?model=advice&m=ajax&a=index&cmd=110";

	var detail = {
		_setting:{
			id:null,
			type:null,
			activekey:null,
			target:null,
			callback:{
				error_callback:null,
				success_callback:null,
				finish_callback:null
			}
		},
		dom:null,
		$dom:null,
		cacheSetting:null,
		cacheData:null,
		commentPage:1,
		dealPage:1,
		checkParams: function(setting) {
			if(setting.target == null || !(setting.target instanceof jQuery)){
				console.log('错误的jq对象');
				return;
			}

			var _setting = tools.clone(this._setting);
			$.extend(true, _setting, setting);

			this.setting = setting;

			return _setting;
		},
		init: function(_setting) {
			_setting = this.checkParams(_setting);

			if(!_setting.id || !_setting.type || !_setting.activekey){console.log("配置错误");return;}

			this.cacheSetting = _setting;

			this.getDetail(_setting);
		},
		getDetail:function(_setting){
			var pThis = this;

			tools.ajaxJson({
				url:getDetailUrl,
				data:{
					"data":{
						"id":_setting.id,
						"type":_setting.type
					}
				},
				callback:function(result,status){
					if(status == false && result == null){return;}

					if(result.errcode != 0){
						if(!_setting.callback || !tools.isFunction(_setting.callback.error_callback)){return;}
						_setting.callback.error_callback.apply(pThis,[result]);
						return;
					}

					var data = result.info;
					pThis.cacheData = data;
					console.log(data);
					pThis.appendHtml(_setting,data);

					_setting.data = data;
					_setting.id = data.id;
					pThis.bindDom(_setting);	

					pThis.finish_callback(_setting);
				}
			});
		},
		bindDom:function(_setting){
			var pThis = this;
			//Tab切换
			_setting.target.on("click",".tab li",function(){
				var li = $(this),index = li.index();

				if(li.hasClass('active')){return;}

				li.addClass('active').siblings('li').removeClass('active');

				li.parents('section')
				.find('.tab-part:eq('+index+')')
				.removeClass('hide')
				.siblings('.tab-part').addClass('hide');
			});

			//评论
			_setting.target.on("click","button.button-comment",function(){
				_setting.target.find("section.nothing").addClass('hide');
				var input = _setting.target.find("input.input-comment"),comment = $.trim(input.val());

				if(!comment){
					if(!_setting.callback || !tools.isFunction(_setting.callback.error_callback)){return;}
					var result = {};result.errmsg = "评论内容不能为空";
					_setting.callback.error_callback.apply(pThis,[result]);
					return;
				}

				pThis.btnAjax(_setting,addCommentUrl,{
					"comment":comment,
					"ad_id":_setting.id,
					//"reply_id":""
				},"comment",function(){
					//_setting.target.find(".commentList").find("section:first").after(pThis.getCommentHtml(comment));
					input.val("");
					pThis.commentPage = 1;

					pThis.getMoreList(_setting,{
						target:_setting.target.find(".commentList").find("section:first"),
						url:getCommentListUrl
					});
				});
			});

			//撤回
			_setting.target.on("click","button#btn-reply",function(){
				layerPackage.ew_confirm({
					title:"确定撤回该计划",
					ok_callback:function(){
						pThis.btnAjax(_setting,replyUrl,{
							"id": _setting.id
						},"reply");
					}
				});
			});

			//催办
			_setting.target.on("click","button#btn-time",function(){
				pThis.btnAjax(_setting,timeUrl,{
					"id": _setting.id
				},"",function(){
					pThis.dealPage = 1;
					pThis.getMoreList(_setting,{
						target:_setting.target.find(".dealList").find("section:first"),
						url:getCtrlUrl
					});
				});
			});

			//激活
			_setting.target.on("click","button#btn-activate",function(){
				pThis.btnAjax(_setting,activateUrl,{
					"id": _setting.id
				},"",function(){
					var data = pThis.cacheData;
					data.state = 2;//类型重置为处理中
					_setting.activekey = 1;//状态重置为完成
					pThis.appendHtml(_setting,data);

					pThis.dealPage = 1;
					pThis.getMoreList(_setting,{
						target:_setting.target.find(".dealList").find("section:first"),
						url:getCtrlUrl
					});
				});
			});

			//我要处理
			_setting.target.on("click","button#dealMySelf",function(){
				layerPackage.ew_confirm({
					title:"确定主导处理？",
					desc:"主导处理后，反馈将被锁定，其他处理人将无法对反馈进行处理操作！",
					ok_callback:function(){
						layerPackage.lock_screen();
						pThis.btnAjax(_setting,dealMySelfUrl,{
							"id": _setting.id
						},"dealMySelf",function(){
							var data = pThis.cacheData;
							data.primary_id = userInfo.userId;
							pThis.appendHtml(_setting,data);
							pThis.finish_callback(_setting);

							pThis.dealPage = 1;
							pThis.getMoreList(_setting,{
								target:_setting.target.find(".dealList").find("section:first"),
								url:getCtrlUrl
							});
						});
					}
				});	
			});

			//取消处理
			_setting.target.on("click","button#cancelMySelf",function(){
				layerPackage.lock_screen();
				pThis.btnAjax(_setting,cancelMySelfUrl,{
					"id": _setting.id
				},"cancelMySelf",function(){
					var data = pThis.cacheData;
					data.primary_id = "0";
					pThis.appendHtml(_setting,data);
					pThis.finish_callback(_setting);

					pThis.dealPage = 1;
					pThis.getMoreList(_setting,{
						target:_setting.target.find(".dealList").find("section:first"),
						url:getCtrlUrl
					});
				});
			});

			//完成
			_setting.target.on("click","button#btn-complete",function(){
				var c_opt = {};
				c_opt.placeholder = "备注一下吧";
				c_opt.ok_callback = function(value) {
					layerPackage.lock_screen("数据提交中");

					pThis.btnAjax(_setting, completeUrl, {
						"id": _setting.id,
						"comment": value,
					}, "complete");
				};

				layerPackage.ew_confirm_input.init(c_opt);

				/*var res = window.prompt("备注一下吧");
				if(res == null){return;}

				pThis.btnAjax(_setting,completeUrl,{
					"id": _setting.id,
					"comment":res,
				},"complete");*/
			});

			//关闭
			_setting.target.on("click","button#btn-close",function(){
				var c_opt = {};
				c_opt.placeholder = "备注一下吧";
				c_opt.ok_callback = function(value) {
					layerPackage.lock_screen("数据提交中");
					
					pThis.btnAjax(_setting, closeUrl, {
						"id": _setting.id,
						"comment": value,
					}, "close");
				};

				layerPackage.ew_confirm_input.init(c_opt);

				/*var res = window.prompt("备注一下吧");
				if(res == null){return;}

				pThis.btnAjax(_setting,closeUrl,{
					"id": _setting.id,
					"comment":res,
				},"close");*/
			});

			//获取更多列表内容
			_setting.target.on("click",".commentList .getMore",function(){
				pThis.commentPage += 1;
				pThis.getMoreList(_setting,{
					target:$(this),
					url:getCommentListUrl,
					page:pThis.commentPage,
					notDelete:true
				});
			});

			//获取更多动态内容
			_setting.target.on("click",".dealList .getMore",function(){
				pThis.dealPage += 1;
				pThis.getMoreList(_setting,{
					target:$(this),
					url:getCtrlUrl,
					page:pThis.dealPage,
					notDelete:true
				});
			});

			//附件预览
			_setting.target.on("click",".queue-file",function(){
				var md5_hash = $(this).attr("data-hash"),
				ext = $(this).attr("data-ext"),
				path = $(this).find("input").val(),
				filename = $(this).find(".filename").text(),
				id = $(this).attr("data-id");

				var btnMenuPackage = UiFramework.btnMenuPackage();

				var opt = {};
				opt.data = {};
				opt.data.hash = md5_hash;
				opt.data.ext = ext;
				opt.data.path = path;
				opt.data.name = filename;
				opt.data.id = id;
				opt.menus = [];

				var extClass = tools.getFileTypeExtIcon(ext);

				var url = "/wx/index.php?model=advice&m=ajax&a=index&cmd=117&advice_id="+_setting.id+"&cf_id="+id;

				if(extClass == "doc" || extClass == "ppt" 
					|| extClass == "xls" || extClass == "txt" 
					|| extClass == "pdf"){

					var m1 = {};
					m1.id = 1;
					m1.name = "预览";
					m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
					m1.fun = function(el, m, o) {
						GenericFramework.viewFile(ext,md5_hash);
					};
					opt.menus.push(m1);
					var m2 = {};
					m2.id = 2;
					m2.name = "下载";
					m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
					m2.fun = function(el, m, o) {
						//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
					};
					opt.menus.push(m2);

					btnMenuPackage.init(opt);
				}else if(extClass == "img"){
					GenericFramework.init(wxJsdkConfig);

					GenericFramework.previewImage({
						target:$(this),
						current:path,
						urls:[path]
					});
				}else{
				    var m2 = {};
				    m2.id = 1;
			        m2.name = "下载";
			        m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			        m2.fun = function(el, m, o) {
			        	//var url = media_domain_download + '&id='+o.id+'&advice_id=' + _setting.id;
						GenericFramework.fileDownload(url);
			        };
			        opt.menus.push(m2);
			        btnMenuPackage.init(opt);
				}
			});
		},
		clearData:function(){
		},
		getCommentHtml:function(comment){
			var html = new Array();

			html.push('<section class="autoH bt1">');
			html.push('<img src="'+userInfo.pic+'" alt="" class="fl pic35 cir" onerror="this.src=\''+defaultFace+'\'"/>');
			html.push('<div>');
			html.push('<span class="fs14">'+userInfo.userName+'<time class="fr fs12 c-gray">'+tools.getNowDate()+'</time></span>');
			html.push('<p class="fs15 c-black">'+comment+'</p>');
			html.push('</div>');
			html.push('</section>');

			return html.join('');
		},
		btnAjax:function(_setting,url,params,ctrlName,callback){
			var pThis = this;
			tools.ajaxJson({
				url: url,
				data: {
					"data": params
				},
				callback: function(result, status) {
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						if (!_setting.callback || !tools.isFunction(_setting.callback.error_callback)) {return;}
						_setting.callback.error_callback.apply(pThis, [result]);
						return;
					}

					if (!_setting.callback || !tools.isFunction(_setting.callback.success_callback)) {return;}
					_setting.callback.success_callback.apply(pThis, [result, ctrlName]);

					if(!tools.isFunction(callback)){return;}
					callback();
				}
			});
		},
		appendHtml:function(_setting,data){
			var dom = null;
			tpl.register('dateFormat', tools.dateFormat);
			tpl.register('getDiffTime', tools.getDiffTime);
			tpl.register('bytesToSize', tools.bytesToSize);
			tpl.register('getFileTypeExtIcon', tools.getFileTypeExtIcon);
			dom = tpl(detailTpl,{
				data:data,
				type:_setting.type,
				activekey:_setting.activekey,
				userInfo:userInfo,
				cdn_domain:cdn_domain,
				media_domain:media_domain,
				defaultFace:defaultFace
			});
			tpl.unregister('dateFormat');
			tpl.unregister('getDiffTime');
			tpl.unregister('bytesToSize');
			tpl.unregister('getFileTypeExtIcon');

			this.dom = $.trim(dom);
			this.$dom = $(this.dom);

			//默认插入到开头位置
			_setting.target.html("").append(this.$dom);
		},
		finish_callback:function(_setting){
			if(!_setting.callback || !tools.isFunction(_setting.callback.finish_callback)){return;}
			_setting.callback.finish_callback.apply(this);
		},
		getMoreList:function(_setting,params){
			var pThis = this;
			tools.ajaxJson({
				url: params.url,
				data: {
					"data": {
						"id":_setting.id,
						"page":params.page || 1
					}
				},
				callback: function(result, status) {
					if (status == false && result == null) {return;}

					if (result.errcode != 0) {
						if (!_setting.callback || !tools.isFunction(_setting.callback.error_callback)) {return;}
						_setting.callback.error_callback.apply(pThis, [result]);
						return;
					}

					try{
						var html = "",data = result.info.data;

						if(data.length == 0){
							params.target.addClass("hide");
							layerPackage.ew_alert({
								title: "已经是最后一页"
							});
							return;
						}

						if(!params.notDelete){
							params.target.siblings('section').remove();
						}

						for (var i = 0; i < data.length; i++) {
							if(params.url == getCommentListUrl){
								html += $.trim(pThis._getCommentHtml(data[i]));
							}else if(params.url == getCtrlUrl){
								html += $.trim(pThis._getDealHtml(data[i]));
							}
						};

						params.target.after(html);

						if(data.length >= 10){
							pThis.addScroll(params.target.parent());
						}

						if(params.notDelete){
							params.target.parent().append(params.target);
						}
					}catch(e){

					}	
				}
			});
		},
		_getCommentHtml:function(data){
			var html = new Array();

			if(!data.user_pic){
				data.user_pic = defaultFace;
			}

			html.push('<section class="autoH bt1">');
			html.push('<img src="'+data.user_pic+'" alt="" class="fl pic35 cir" onerror="this.src=\''+defaultFace+'\'"/>');
			html.push('<div>');
			html.push('<span class="fs14">'+data.user_name+'<time class="fr fs12 c-gray">'+tools.getDiffTime(data.create_time)+'</time></span>');
			html.push('<p class="fs15 c-black">'+data.text+'</p>');
			html.push('</div>');
			html.push('</section>');

			return html.join('');
		},
		_getDealHtml:function(data){
			var html = new Array();

			if(!data.user_pic){
				data.user_pic = defaultFace;
			}

			html.push('<section class="autoH pb5 bt1">');
			html.push('<img src="'+data.user_pic+'" alt="" class="fl pic35 cir" onerror="this.src=\''+defaultFace+'\'"/>');
			html.push('<div>');
			html.push('<span class="fs14">('+data.user_name+')'+data.desc+'</span>');
			html.push('<p class="mt5"><time>'+tools.getDiffTime(data.create_time)+'</time></p>');
			html.push('</div>');
			html.push('</section>');

			return html.join('');
		},
		addScroll:function(jqObj){
			if(jqObj.find(".getMore").length >0){jqObj.find(".getMore").removeClass("hide");return;}

			jqObj.append("<p class='tac c-gray getMore'>更多评论内容</p>");
		}
	};

	return detail;
});