require([
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'common:widget/lib/GenericFramework',
	'advice:static/Scripts/detail'
],function(t,UiFramework,GenericFramework,detail) {
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen(); //start

	var container = $("#main-container"),
	id = t.getHrefParamVal("id") || 1,type = t.getHrefParamVal("type") || 1,activekey = t.getHrefParamVal("activekey") || 1;

	detail.init({
		target:container,
		id:id,
		type:type,
		activekey:activekey,
		callback:{
			error_callback:function(result){
				layerPackage.unlock_screen();//遮罩end
				layerPackage.fail_screen(result.errmsg);
			},
			success_callback:function(result,ctrlName){
				if(ctrlName == "dealMySelf"){
					layerPackage.unlock_screen();
					result.errmsg = "主导成功";
				}

				if(ctrlName == "cancelMySelf"){
					layerPackage.unlock_screen();
					result.errmsg = "取消主导成功";
				}

				if(ctrlName == "comment"){
					result.errmsg = "评论成功";
				}

				layerPackage.success_screen(result.errmsg);

				//撤回
				if(ctrlName == "reply"){
					window.location.href = "/wx/index.php?app=advice&a=mineList&debug=1";
					return;
				}

				if(ctrlName == "complete" || ctrlName == "close"){
					window.location.href = "/wx/index.php?app=advice&a=dealList&debug=1";
					return;
				}
			},
			finish_callback:function(result){
				layerPackage.unlock_screen();

				UiFramework.bottomInfo.init(container,77);
			}
		}
	});
},function(e){
	requireErrBack(e);
});



