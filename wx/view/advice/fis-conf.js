require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'advice');
fis.match('*', {
	release:'/advice/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});