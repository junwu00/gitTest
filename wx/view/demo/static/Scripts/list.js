//列表页面
define([
	'common:widget/lib/tools',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/index/Templates/list.html',
],function(t,tpl,listTemplate){

    var list = {
        listObj:null,
        dom:"",
        $dom:null,
        type:null,
        sort:null,
        order:null,
        keywords:null,
        pageCount:null,
        type:null,
        init:function(){

        },
        finit:function(){
            this.listObj = null;
            this.dom = "";
            this.$dom = null;
            this.type = null;
            this.sort = null;
            this.order = null;
            this.keywords = null;
            this.pageCount = null;
            this.type = null;
        },
        getList:function(params,callback){
            if(!params.pro_name){params.pro_name = "";}
            if(params.sort === undefined){params.sort = 0;}
            if(params.order === undefined){params.order = 1;}
            if(!params.keywords){params.keywords = "";}
            if(!params.listUrl){return;}
            if(params.type === undefined){return;}
            if(!t.checkJqObj(params.listObj)){return;}

            var pThis = this,listObj = params.listObj;
            pThis.params = params;

            t.ajaxJson({
                url:params.listUrl,
                data:{
                    "data":{
                        "type":params.type,
                        "pro_name":params.pro_name,
                        "sort":params.sort,
                        "order":params.order,
                        "page":params.page,
                    }
                },
                callback:function(result,status){
                    if(status == false && result == null){return;}

                    if(result.errcode != 0){t.console(result.errmsg,1);return;}

                    if(pThis.type != params.type || pThis.sort !== params.sort || 
                        pThis.order !== params.order || pThis.keywords !== params.pro_name){
                        pThis.type = params.type;
                        pThis.sort = params.sort;
                        pThis.order = params.order;
                        pThis.keywords = params.pro_name;
                        pThis.dom = "";
                        pThis.$dom = "";
                        pThis.pageCount = null;
                    }
                    var data = result.info.data;
                    pThis.pageCount = result.info.count;

                    listObj.find("section").remove();
                    listObj.find(".line1").remove();
                    
                    console.dir(data);

                    if(data.length > 0){
                        listObj.find("div").addClass('hide');

                        tpl.register('dateFormat', t.dateFormat);
                        var dom = tpl(listTemplate,{
                            listData:data,
                            listType:params.listType,
                            activekey : params.type,
                            defaultFace:defaultFace
                        });
                        tpl.unregister('dateFormat');

                        pThis.dom = pThis.dom + dom;
                        pThis.$dom = $($.trim(pThis.dom));

                        listObj.append(pThis.$dom);
                    }else{
                        listObj.find("div").removeClass('hide');
                    }
                    
                    pThis.listObj = listObj;

                    pThis.bindDom();

                    if(!t.isFunction(callback)){return;}
                    callback(data,pThis.pageCount);
                }
            });
        },
        bindDom:function(){
            var pThis = this,listType = pThis.params.listType;

            this.listObj.find('section').on('click',function(){
                var id = $(this).attr("data-id");

                if(pThis.params.callback && t.isFunction(pThis.params.callback.bindDom)){
                    pThis.params.callback.bindDom.apply(pThis,[{
                        obj:pThis,id:id,listType:listType
                    }])
                    return;
                }
            });
        }
    };

	return list;
});