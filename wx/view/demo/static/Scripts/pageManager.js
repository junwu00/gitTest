define(['jquery','common:widget/lib/tools'],function($,t){
	//hash event
	var pageManager = {
		_pageStack: [],
		_configs:[],
		_defaultPage: null,
		_isGo:false,
		_firstUrl:"",
		default: function (defaultPage) {
            this._defaultPage = defaultPage;
            return this;
        },
		init:function(){
			var pThis = this;
			$(window).on('hashchange', function(e) {
				var _isBack = !pThis._isGo;
				pThis._isGo = false;

				var pageStack = pThis._getPageStack();
				if(pThis._firstUrl != "" && (!pageStack || pageStack.config.name == pThis._defaultPage)){
					backDefault(pThis._firstUrl);
					pThis._firstUrl = "";
					//_isBack = true;
					return;
				}

				if(!_isBack){
					return;
				}

				var found = null;
				var url = location.hash.indexOf('#') === 0 ? location.hash : '#';
                for(var i = 0, len = pThis._pageStack.length; i < len; i++){
                    var stack = pThis._pageStack[i];
                    if (stack.config.url === url) {
                        found = stack;
                        break;
                    }
                }

                if (found) {
                    pThis.back();
                }else {
                    goDefault();
                }
			});

			function goDefault(){
                var url = location.hash.indexOf('#') === 0 ? location.hash : '#';
                var page = pThis._find('url', url) || pThis._find('name', pThis._defaultPage);

                var name = page.name;
                if(name != pThis._defaultPage){
                	pThis._firstUrl = url;//说明第一次加载的不是Home默认页面
                	if(pThis._pageStack.length ==0 || (pThis._pageStack.length > 0 && pThis._pageStack[0].config.name != pThis._defaultPage)){
                		pThis._pageStack.unshift({
				            config: pThis._configs[0]
			            });
                	}
                }

                pThis.go(name);
            }

            function backDefault(url){
            	//要把默认页面加入到栈开始位置
                /*var page = pThis._find('name', pThis._defaultPage);
                pThis._firstUrl = "";
            	pThis._pageStack.unshift({
	                config: page
	            });*/
				if(pThis._pageStack.length == 0){return;}
				if(pThis._pageStack.length > 1){
					pThis._pageStack.shift();
				}
				
	            var p = pThis._pageStack[0].config,index = 0;

	            var configs = pThis._configs;
	            for (var j = 0; j < configs.length; j++) {
	            	if(configs[j] == p){
	            		index = j;
	            		break;
	            	}
	            };

            	for (var i = j - 1; i >= 0; i--) {
            		if(configs[i] != p 
            			&& (!configs[i].type || (configs[i].type == p.type && configs[i].floor < p.floor))){
            			pThis._pageStack.unshift({
			                config: configs[i]
			            });
            		}
            	};

            	
            	//console.log(pThis._pageStack);return;
                pThis.back();
            }

            goDefault();

			return this;
		},
		push: function (config) {
            this._configs.push(config);
            return this;
        },
        go: function (to,isBack) {
            var config = this._find('name', to);
            if (!config) {
                return;
            }
            // var html = $(config.template).html();
            // var $html = $(html).addClass('slideIn').addClass(config.name);
            // this.$container.append($html);
            var flag = true;
            for (var i = 0; i < this._pageStack.length; i++) {
            	if(this._pageStack[i].config == config){
            		flag = false;
            		break;
            	}
            };
            if(flag){
            	this._pageStack.push({
	                config: config,
	                //dom: $html
	            });
            }

            if(!isBack){
            	this._isGo = true;
            }else{
            	this._isGo = false;
            }
          
            location.hash = config.url;

            if (!config.isBind) {
                this._bind(config);
            }

            if(config.go && t.isFunction(config.go)){
            	config.go();
            }

            return this;
        },
        back: function () {
        	if(this._pageStack.length <= 1){return;}
            var stack = this._pageStack.pop();

            if (!stack) {
                return;
            }

            if(stack.config.back && t.isFunction(stack.config.back)){
           		stack.config.back();
       		}

            return this;
        },
        _find: function (key, value) {
            var page = null;
            for (var i = 0, len = this._configs.length; i < len; i++) {
                if (this._configs[i][key] === value) {
                    page = this._configs[i];
                    break;
                }
            }
            return page;
        },
        _bind: function (page) {
            var events = page.events || {};
            for (var t in events) {
                for (var type in events[t]) {
                    this.$container.on(type, t, events[t][type]);
                }
            }
            page.isBind = true;
        },
        _getPageStack:function(){
        	var found = null;
			var url = location.hash.indexOf('#') === 0 ? location.hash : '#';
            for(var i = 0, len = this._pageStack.length; i < len; i++){
                var stack = this._pageStack[i];
                if (stack.config.url === url) {
                    found = stack;
                    break;
                }
            }

            return found;
        },
        getPageStack:function(){
        	return this._pageStack;
        },
        isDefaultPage:function(){
        	if(this._firstUrl){return false;}
        	return true;
        },
        clearOtherPageStack:function(){
        	var arr = new Array();
        	for (var i = 1; i < this._pageStack.length - 1; i++) {
        		this._pageStack.splice(i,1);
        	};
        }
	};

	return pageManager;
});