require([
	'jquery',
	'common:widget/lib/tools',
	'common:widget/lib/UiFramework',
	'demo:static/Scripts/commonData',
	'demo:static/Scripts/pageManager',
	/*----*/
	'demo:static/Scripts/commonFuc',
	'common:widget/lib/plugins/juicer',
	'common:widget/lib/text!'+buildView+'/demo/Templates/detail.html',
],function($,t,UiFramework,commonData,pageManager,commonFuc,tpl,detailTpl) {
	UiFramework.menuPackage.init();
	var layerPackage = UiFramework.layerPackage(); 
	layerPackage.lock_screen();//遮罩start

	var ajaxUrl = commonData.ajaxUrl;//专门处理请求URL

	var menuType = t.getUrlParam("menuType") || 1;
	var menuNum = menuType - 1;

	var mediator = {
		newsObj: null, /*消息对象*/
		searchObj:null, /*搜索对象*/
		processObj:null, /*应用对象*/
		personalObj:null, /*我的对象*/
		pageManager:null,
		init:function(params){
			this.params = params;

			this.getDom(params.container);

			this.pageManager = params.pageManager;
			
			this.bindDom();

			this.pageManagerInit();

			this.testBindDom();

			UiFramework.bottomInfo.init($("#processList"),20); 
		},
		fini:function(){
			this.newsObj = null;
			this.searchObj = null;
			this.processObj = null;
			this.personalObj = null;
			this.pageManager = null;
		},
		getDom:function(container){
			//jqObj对象
			this.container   =   container;
			this.view        =   this.container.find(".view-container");
			this.vLis        =   this.container.find(".view-container > li");//view.find("li");
			this.footer      =   this.container.find("footer");
			this.fLis        =   this.footer.find("ul.baseBtn li");
			this.nFirstDom   =   this.view.find("li.nFirst");
			this.nSecondDom  =   this.view.find("li.nSecond");
			this.mFirstDom   =   this.view.find("li.mFirst");
			this.mSecondDom  =   this.view.find("li.mSecond");
			this.pFirstDom   =   this.view.find("li.pFirst");
			this.pSecondDom  =   this.view.find("li.pSecond");
		},
		bindDom:function(){
			var fLis = this.fLis,
				vLis = this.vLis,
				container = this.container,
				view = this.view,
				footer = this.footer,
				pageManager = this.pageManager,
				processObj = this.processObj;

			var menuType = t.getUrlParam("menuType") || 1;
			var menuNum = menuType - 1;
			vLis.removeClass('pageOld pageInt pageNew');
			if (menuType == 1) {
				vLis.eq(menuNum).addClass('pageInt').siblings().addClass('pageNew');
			} else if (menuType > 1 && menuType < vLis.length) {
				vLis.eq(menuNum).addClass('pageInt');
				view.find("li:lt(" + menuNum + ")").addClass('pageOld');
				view.find("li:gt(" + menuNum + ")").addClass('pageNew');
			} else if (menuType == vLis.length) {
				vLis.eq(menuType - 1).addClass('pageInt').siblings().addClass('pageOld');
			}

			fLis.eq(menuType - 1).addClass('current').siblings().removeClass('current');

			container.removeClass('hide');

			//.slideSlow 慢速滑动，一定是左边的页面
			//.slideFast 快速滑动，一定是右边的页面
			//.slideSlowBack 返回时，慢速滑动，一定是左边的页面
			//.slideFastBack 返回时，快速滑动，一定是右边的页面
			//.pageInt  初始页面位置
			//.pageOld  正向划过页面位置 -100%
			//.pageNew  正向未滑页面位置 +100%

			//记录滑动过的页面
			var currentNum = menuNum; //0

			fLis.on("click", function() {
				var index = $(this).index(),
					num = index - currentNum;

				//当前页面
				if (num == 0) {
					return;
				}
				//禁止点击我的
				if(index == 3){return;}

				var fCurrent = 0;
				if (index > 2) {
					if (index == 3 || index == 4) {
						fCurrent = 0;
					} else if (index == 5 || index == 6) {
						fCurrent = 2;
					} else if (index == 7 || index == 8) {
						fCurrent = 1;
					}
				} else {
					fCurrent = index;
				}

				if (index < 3) {
					commonFuc.changeWindowTitle(commonData.version);
				}

				fLis.eq(fCurrent).addClass('current').siblings().removeClass('current');

				var currentPage = view.find("li.page-container:eq(" + currentNum + ")");
				var clickPage = view.find("li.page-container:eq(" + index + ")");

				vLis.removeClass('slideSlow slideFast slideSlowBack slideFastBack');

				//向前
				if (num > 0) {
					currentPage.removeClass("pageInt pageNew").addClass('pageOld slideSlow');
					clickPage.addClass('pageInt slideFast').removeClass("pageOld pageNew");
				}
				//后退
				else if (num < 0) {
					currentPage.removeClass("pageInt pageOld").addClass('pageNew slideFastBack');
					clickPage.addClass('pageInt slideSlowBack').removeClass("pageOld pageNew");
				}

				if (num > 1 || num < -1) {
					vLis.each(function(i, el) {
						if (i < index && i > currentNum) {
							$(this).addClass('pageOld').removeClass('pageNew');
						} else if (i > index && i < currentNum) {
							$(this).addClass('pageNew').removeClass('pageOld');
						}
					});
				}

				currentNum = index;

				vLis.eq(index).find("section.clickBg").removeClass('clickBg');
			});
		},
		pageManagerInit:function(){
			var newsObj = this.newsObj,
				processObj = this.processObj,
				personalInfo = this.personalInfo;
			//主页
			var home = {
		        name: 'home',
		        url: '#',
		        events: {
		        }
		    };
		    //test页面
		    var tDetail = {
		    	name: 'tDetail',
		        url: '#tDetail',
		        type:1,
		        floor:1,
		        events: {  
		        },
		        data:{
		        	type:0,
		        	appId:0,
		        	name:"详情"
		        },
		        go: function(){
		        	console.log("go");
		        	mediator.goToDetail(this.data);
		        },
		        back:function(){
		        	console.log("back");
		        	mediator.backToIndex();
		        }
		    };

			this.pageManager
			.push(home)
			.push(tDetail)
			.default('home')
			.init();
		},
		testBindDom:function(){
			var pThis = this;
			$("#page0").click(function(){
				pThis.container.removeClass('noBottom');
				pThis.fLis.eq(2).click();
				setTimeout(function(){
					pThis.footer.removeClass('hide');
				},250);
			});

			$(".newList").find("section:first").on("click",function(){
				commonFuc.addClickBg($(this));

				pThis.pageManager.go("tDetail");
			});

			$("#processList").click(function(){
				pThis.fLis.eq(1).find(".countNum").removeClass('hide').addClass('animated wobble2');

				pThis.fLis.eq(1).click();

				setTimeout(function(){
					pThis.vLis.eq(1).find(".countNum").removeClass('hide').addClass('animated wobble2');
				},500);
			});

			//Tab切换
			$(".tDetail").on("click",".tab li",function(){
				var li = $(this),index = li.index();

				if(li.hasClass('active')){return;}

				li.addClass('active').siblings('li').removeClass('active');

				li.parents('section')
				.find('.tab-part:eq('+index+')')
				.removeClass('hide')
				.siblings('.tab-part').addClass('hide');

				$(".jitem > section").removeClass('animated zoomInUp');
			});
		},
		goToDetail:function(data){
			console.log(this.pageManager);

			var pThis = this;
			this.footer.addClass('hide');
			this.container.addClass('noBottom');

			tpl.register('getTime', this.getTime);
			var dom = tpl(detailTpl,{
				info:1
			});
			tpl.unregister('getTime');

			var o = $(".tDetail");
			o.html($.trim(dom));
			//sign_url = "11";
			//image_url = "";
			if(sign_url){
				UiFramework.wxBottomMenu.init({
					target:o.find("#info"),
					menu:[
						{
							width:100,
							name:"签名",
							btnColor:"g",
							iconClass:"rulericon-sign11 baseIconSize",
							click:function(){
								window.location.href = sign_url;
							}
						}
					]
				});
			}
				
			this.fLis.eq(4).click();

			if(image_url){
				pThis.appendSign(image_url);
			}
		},
		backToIndex:function(){
			var pThis = this;
			this.fLis.eq(1).click();

			setTimeout(function(){
				pThis.container.removeClass('noBottom');
				pThis.footer.removeClass('hide');
			},250);
		},
		appendSign:function(image_path){
			tpl.register('getTime', this.getTime);
			var dom = tpl(detailTpl,{
				sign:1,
				image_path:image_path
			});
			tpl.unregister('getTime');

			this.container.addClass('noBottom');
			this.container.find("#info").addClass('noBottom');

			var o = $(".tDetail");
			o.find(".noData").addClass('hide');
			o.find("#dealList")
			.addClass('dealList')
			.append($.trim(dom))
			.find(".jitem:last > section")
			.addClass('animated zoomInUp');

			o.find("#notifyList")
			.addClass('notifyList')
			.find(".nitem")
			.removeClass('hide');

			o.find("#processList")
			.addClass('processList')
			.find(".witem")
			.removeClass('hide');

			var li = $("#ctrlList").find("ul.tab li");
			li.eq(0).find("span").text(o.find("#dealList").find(".jitem").length);
			li.eq(1).find("span").text(o.find("#notifyList").find(".nitem").length);
			li.eq(2).find("span").text(o.find("#processList").find(".witem").length);

			this.container.find("#info .changeStatus")
			.removeClass("bg-green").addClass('complete').text("已结束");
		},
		getTime:function(minus){
			var time = "";
			if(minus){
				time = new Date().getTime() - minus;
			}	

			return t.getNowDate(time);
		}
	};

	mediator.init({
		container:$("#main-container"),
		pageManager:pageManager
	});

	layerPackage.unlock_screen();
});