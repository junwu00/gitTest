define({
	company:"移步到微",
	version:"流程专家",
	questions:[
		{
			id:1,
			title:"移步到微平台怎么收费？",
			content:"这是一个很严肃的问题，具体为什么严肃，那就要来说说下一个问题了，哇，好冷的感觉，世界上最小的湖是哪个湖，世界上最大的山是那一座，南极游北极熊，北极有极光？哇。"
		},
		{
			id:2,
			title:"为什么选择移步到微平台？",
			content:"这是一个很严肃的问题，具体为什么严肃，那就要来说说下一个问题了，哇，好冷的感觉，世界上最小的湖是哪个湖，世界上最大的山是那一座，南极游北极熊，北极有极光？哇。"
		},
		{
			id:3,
			title:"移步到微平台有哪些增值服务？",
			content:"这是一个很严肃的问题，具体为什么严肃，那就要来说说下一个问题了，哇，好冷的感觉，世界上最小的湖是哪个湖，世界上最大的山是那一座，南极游北极熊，北极有极光？哇。"
		},
		{
			id:4,
			title:"平台是怎么保障用户信息安全？",
			content:"这是一个很严肃的问题，具体为什么严肃，那就要来说说下一个问题了，哇，好冷的感觉，世界上最小的湖是哪个湖，世界上最大的山是那一座，南极游北极熊，北极有极光？哇。"
		},
		{
			id:5,
			title:"流程专家是做什么的？",
			content:"这是一个很严肃的问题，具体为什么严肃，那就要来说说下一个问题了，哇，好冷的感觉，世界上最小的湖是哪个湖，世界上最大的山是那一座，南极游北极熊，北极有极光？哇。"
		}
	],
	ajaxUrl:{
		//消息
		/*获取我的消息项列表 101*/
		"getNewListUrl":"/wx/index.php?model=index&m=msg&cmd=101",
		/*搜索消息 101*/
		"searchNewsUrl":"/wx/index.php?model=index&m=msg&cmd=102",
		/*获取指定消息项的消息 103*/
		"getOneNewsUrl":"/wx/index.php?model=index&m=msg&cmd=103",
		/*将消息项内的所有消息置为已读 104*/
		//"readNewsUrl":"/wx/index.php?model=index&m=msg&cmd=104",
		/*获取轮询地址*/
		"getPollingUrl":"/wx/index.php?model=index&m=msg&cmd=104",
		/*保存消息 105*/
		"saveNewsUrl":"/wx/index.php?model=index&m=msg&cmd=105",
		//我的
		//获取我的个人信息
		"getMyInfoUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=101",
		//修改我的个人信息
		"editMyInfoUrl":"/wx/index.php?model=index&m=mine&a=index&cmd=102",
		//应用
		//获取用户可使用的表单 101 
		"getUserProcessListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=101",
		//获取待办流程总数 102
		"getDealCountUrl":"/wx/index.php?model=index&m=app&a=index&cmd=102",
		//获取我待办/已办的流程列表 103
		"getDealListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=103",
		//获取我待办/已办的流程列表 103
		"getMineListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=104",
		//获取我待办/已办的流程列表 103
		"getNotifyListUrl":"/wx/index.php?model=index&m=app&a=index&cmd=105"
	}
});