require('fis3-smarty')(fis);
require('../fis-common-conf.js');

fis.set('namespace', 'demo');
fis.match('*', {
	release:'/demo/$0'
});
fis.match('*-map.json', {
	release: '/config/$0'
});