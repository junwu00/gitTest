const proxy = {
  open: process.platform === "darwin",
  host: "0.0.0.0",
  port: 8981,
  https: false,
  hot: true,
  proxy: {
    "/wx/index.php": {
      // target 反向代理到的地址
      target: "https://dev.wx.luohu.order.3ruler.com/wx/index.php",
      // target: "http://wx.luohu.3ruler.com/wx/index.php",
      // target: "http://uat.wx.nh.order.3ruler.com/wx/index.php",
      // target: "https://wx.nanhu.3ruler.com/wx/index.php",
      changeOrigin: true,
      pathRewrite: {
        "^/wx/index.php": "/wx/index.php"
      },
      cookieDomainRewrite: {
        "*": ""
      },
      ws: true
    },
    "/jeecg-boot": {
      // target 反向代理到的地址
      target: "https://dev.wx.luohu.order.3ruler.com/jeecg-boot",
      changeOrigin: true,
      pathRewrite: {
        "^/jeecg-boot": ""
      },
      cookieDomainRewrite: {
        "*": ""
      },
      ws: true
    }
  }
};
module.exports = proxy;
