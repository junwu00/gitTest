import Vue from "vue";
import Vuex from "vuex";
import adminInfo from "../../../store/nh/adminInfo";
import wxprocessApprovalData from "../../../store/nh/wxprocessApprovalData";
import form from "../../../store/rh/form";
import condition from "../../../store/rh/condition";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: { adminInfo, wxprocessApprovalData, form, condition }
});
