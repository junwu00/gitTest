import Vue from "vue";
import App from "./App.vue";
import "../../../registerServiceWorker";
import router from "../../../router";
import store from "./store";
import "babel-polyfill";

import Vant from "vant";
import "vant/lib/index.css";
import "vant/lib/index.less";
import "@vant/touch-emulator";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

// vue3 composition-api
import VueCompositionApi from "@vue/composition-api";

// echarts
import echarts from "echarts";
import { _initVConsole } from "../../../assets/js/util";
import Common from "../../../assets/js/common";
import "./common.less";

import "xe-utils";
import VXETable from "vxe-table";
import "vxe-table/lib/style.css";

Vue.use(VXETable);

// 开发环境mockjs
if (process.env.NODE_ENV == "development") {
  let Mock = require("../../../mock");
  Mock.default.init();
  console.log("init Mock");
}

Vue.config.productionTip = false;
Vue.use(Vant);
Vue.use(ElementUI);
Vue.use(VueCompositionApi);

Vue.prototype.$common = Common;

Vue.prototype.$echarts = echarts;

// 设置标题
router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
  document.body.scrollTop = 0;
  document.getElementById("app").scrollTop = 0;
  next();
});

router.afterEach(() => {
  let bodySrcollTop = document.body.scrollTop;
  if (bodySrcollTop !== 0) {
    document.body.scrollTop = 0;
    return;
  }
  let docSrcollTop = document.documentElement.scrollTop;
  if (docSrcollTop !== 0) {
    document.documentElement.scrollTop = 0;
  }
  if (document.getElementById("app") !== null) {
    document.getElementById("app").scrollTop = 0;
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

_initVConsole();
