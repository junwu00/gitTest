// 项目开发修改该文件配置，其他文件尽量保持统一，如其他文件需要修改，先想对应对应技术负责人确认后执行修改
// 不同项目尽量格式相同，部分内容需要和后端确定
// import { _getQuery } from "../../assets/js/common";
// console.log(_getQuery)
const _getQuery = (name, _url) => {
  let url = _url || window.location.href;
  if (!name) {
    return "";
  }
  url = url || window.location.search;
  let _name = name.replace(/(?=[\\^$*+?.():|{}])/, "\\");
  let reg = new RegExp("(?:[?&]|^)" + _name + "=([^?&#]*)", "i");
  let match = url.match(reg);
  return !match ? "" : match[1];
};
const config = {
  // test 大法的token
  _getTestToken: () => {
    return "";
  },
  // 地址参数添加userID
  _getTestUserId: () => {
    return "";
    // return "9135891";
    // return "9136249";
    // return "9136259";
    // return "9135953";
    // return "9135612";
    // return "9135638";
    // return "9135641";
  },
  // 获取当前环境
  _getEvn: () => {
    return process.env.NODE_ENV;
  },
  // 获取当前项目成功状态码
  _getSuccessCode: () => {
    return 0;
  },
  // 获取当前项目登录超时状态码
  _getLoginOutCode: () => {
    return 400100103;
  },
  // 获取当前项目无权限状态码
  _getNoPowerCode: () => {
    return 400100001;
  },
  // 获取当前项目无权限状态码，特定页面
  _getNoPowerCodeComSelect: () => {
    return 400100011;
  },
  // 获取登录验证码
  _getCaptchaCode: () => {
    return 400100009;
  },
  // 常规错误页面
  _getCommErrUrl: () => {
    return "/view/index/dist/index.html#/comerr";
  },
  // 无权限页面路径
  _getNoPowerUrl: () => {
    return "/rhwx/index.html#/noPower";
  },
  // 无权限页面路径，特定页面
  _getNoPowerComSelectUrl: () => {
    return "/rhwx/index.html#/rhwx/noPower";
  },
  // 获取统一数据结构的code值和errmsg值
  _getResCodeAndErrMsg: res => {
    // console.log(res.data);
    return { code: res.data.errcode, errmsg: res.data.errmsg };
  },
  // 获取登录路径
  _getLoginAuthorizeUrl: () => {
    let comId = _getQuery("com_id");
    let appId = _getQuery("app_id");
    window.localStorage.setItem("comId", comId);
    window.localStorage.setItem("appId", appId);
    if (comId.length == 0 && appId.length == 0) {
      comId = window.localStorage.getItem("comId");
      appId = window.localStorage.getItem("appId");
    }
    const redirectUrl = encodeURIComponent(window.location.href);
    const loginUrl = `/api/authorize/qywx_login?com_id=${comId}&app_id=${appId}&redirect_url=${redirectUrl}`;
    console.log(loginUrl);
    return loginUrl;
  }
};
module.exports = config;
