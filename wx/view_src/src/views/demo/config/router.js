const routes = [
  {
    path: "/demoPage1",
    name: "demoPage1",
    component: () => import("../page1.vue")
  },
  {
    path: "/demoPage2",
    name: "demoPage2",
    component: () => import("../page2.vue")
  },
  {
    path: "/demoPage3",
    name: "demoPage3",
    component: () => import("../page3.vue")
  }
];
export default routes;
