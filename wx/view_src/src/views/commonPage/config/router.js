const routes = [
  {
    path: "/404Page",
    name: "404page",
    component: () => import("../404Page.vue")
  },
  {
    path: "/noPower",
    name: "noPower",
    component: () => import("../noPower.vue")
  },
  {
    path: "/empty",
    name: "empty",
    component: () => import("../Empty.vue")
  },
  {
    path: "/comerr",
    name: "comerr",
    component: () => import("../comerr.vue")
  }
];

export default routes;
