// import Vue from "vue";
// import VueRouter from "vue-router";

// Vue.use(VueRouter);

const routes = [
  // 无权限页面
  // {
  //   path: "/nhwx/noPower",
  //   component: () => import("../noPower.vue"),
  //   meta: {
  //     title: "无权操作"
  //   }
  // }
  // 默认页面企业微信
  {
    path: "/nhwx",
    redirect: "/nhwxIndex"
  },
  // 跳转中间页面
  {
    path: "/nhwxJump",
    component: () => import("../util/jumpPage/Index.vue"),
    meta: {
      title: "跳转中"
    }
  },
  // 确认扫码登录页面
  {
    path: "/nhwx/checkPcEqCodeLogin",
    component: () => import("../checkEqCode/Index.vue"),
    meta: {
      title: "确认登录"
    }
  },
  // 流程专家首页
  {
    path: "/nhwxIndex",
    name: "nhwxIndex",
    component: () => import("../Index/homePage/Index.vue"),
    meta: {
      title: "流程专家"
    }
  },
  // 流程专家全部应用页面
  {
    path: "/nhwxAllProcess",
    name: "nhwxAllProcess",
    component: () => import("../Index/homePage/AllProcess.vue"),
    meta: {
      title: "流程专家"
    }
  },
  // 流程专家待办list
  {
    path: "/nhwxAgencyList",
    name: "nhwxAgencyList",
    component: () => import("../Index/agencyList/Index.vue"),
    meta: {
      title: "我的待办"
    }
  },
  // 流程专家发起list
  {
    path: "/nhwxLaunchList",
    name: "nhwxLaunchList",
    component: () => import("../Index/launchList/Index.vue"),
    meta: {
      title: "我的发起"
    }
  },
  // 流程专家知会list
  {
    path: "/nhwxInformList",
    name: "nhwxInformList",
    component: () => import("../Index/InformList/Index.vue"),
    meta: {
      title: "我的知会"
    }
  },
  // 流程专家报表list
  {
    path: "/nhwxReportList",
    name: "nhwxReportList",
    component: () => import("../Index/reportList/Index.vue"),
    meta: {
      title: "报表"
    }
  },
  // 模板列表
  {
    path: "/nhwxMaterialTemplate",
    name: "nhwxMaterialTemplate",
    component: () => import("../Index/materialTemplate/Index.vue"),
    meta: {
      title: "选择分类"
    }
  },
  // 批量入库
  {
    path:
      "/nhwxWarehousing/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?",
    name: "nhwxWarehousing",
    component: () => import("../Index/warehousing/Index.vue"),
    meta: {
      title: "批量入库"
    }
  },
  // 物资告警
  {
    path: "/nhwxMaterialWarn",
    name: "nhwxMaterialWarn",
    component: () => import("../Index/materialWarn/index.vue"),
    meta: {
      title: "物资预警"
    }
  },
  // 流程专家创建页面，草稿重新编辑
  // :pageLeve2?(子表单)/:pageLeve3?(人员部门选择器)/:key1(组件唯一ID)/:key2?/:key3?/:key4?
  {
    path: "/processCreate/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?",
    component: () => import("../process/create/index.vue"),
    meta: {
      title: "新建表单",
      keepAlive: true
    }
  },
  // 流程专家审批页面
  // :approval?(编辑表单)/:pageLeve2?(子表单)/:pageLeve3?(人员部门选择器)/:key1(组件唯一ID)/:key2?/:key3?/:key4?
  {
    path:
      "/processApproval/:approval?/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?",
    component: () => import("../process/approval/index.vue"),
    meta: {
      title: "流程审批",
      keepAlive: true
    }
  },
  // 流程专家详情页面
  {
    path: "/processDetail/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?",
    component: () => import("../process/detail/index.vue"),
    meta: {
      title: "表单详情"
    }
  },
  // 流程专家发送页面
  {
    path: "/prcessSend/:pageLeve2?/:pageLeve3?/:key1?/:key2?/:key3?/:key4?",
    component: () => import("../process/send/index.vue"),
    meta: {
      title: "发送"
    }
  },
  // 历史记录页面
  // {
  //   path: "/history",
  //   name: "history",
  //   component: () => import("../process/history/index.vue"),
  //   meta: {
  //     title: "历史申请记录"
  //   }
  // },
  // 南湖微信-问卷调查-列表
  {
    path: "/nhwxQuestionSurveyList",
    name: "nhwxQuestionSurveyList",
    component: () => import("../survey/list/Index.vue"),
    meta: {
      title: "问卷调查列表"
    }
  },
  // 南湖微信-问卷内容
  {
    path: "/nhwxQuestionSurveyDetail",
    name: "nhwxQuestionSurveyDetail",
    component: () => import("../survey/question/Index.vue"),
    meta: {
      title: "问卷调查"
    }
  },
  // 南湖微信-问卷详情
  {
    path: "/nhwxQuestionDetail",
    name: "nhwxQuestionDetail",
    component: () => import("../survey/question/questionDetail.vue"),
    meta: {
      title: "问卷详情"
    }
  },
  // 南湖微信-提交成功
  {
    path: "/nhwxQuestionSuccPage",
    name: "nhwxQuestionSuccPage",
    component: () => import("../survey/question/succPage.vue"),
    meta: {
      title: "提交成功"
    }
  },
  // 南湖微信-错误页面
  {
    path: "/nhwxQuestionErrorPage",
    name: "nhwxQuestionErrorPage",
    component: () => import("../survey/errorPage/errorPage.vue"),
    meta: {
      title: "错误页面"
    }
  },
  {
    path: "/pullReport",
    name: "pullReport",
    component: () => import("../pushMessage/bureauReport.vue"),
    meta: {
      title: "推送报告"
    }
  }
  // test路由-head
  // {
  //   path: "/test1",
  //   name: "test1",
  //   component: () => import("../process/components/head.vue"),
  //   meta: {
  //     title: "test路由-head"
  //   }
  // },
  // test路由-notifyMsg
  // {
  //   path: "/notifyMsgTest",
  //   name: "notifyMsgTest",
  //   component: () => import("../process/components/notifyMsg.vue"),
  //   meta: {
  //     title: "test路由-notifyMsg"
  //   }
  // },
  // test路由-ctrlProcess
  // {
  //   path: "/ctrlProcessTest",
  //   name: "ctrlProcessTest",
  //   component: () => import("../process/components/ctrlProcess.vue"),
  //   meta: {
  //     title: "test路由-ctrlProcess"
  //   }
  // },
  // test路由-handOpinion
  // {
  //   path: "/handOpinionTest",
  //   name: "handOpinionTest",
  //   component: () => import("../process/components/handOpinion.vue"),
  //   meta: {
  //     title: "test路由-handOpinion"
  //   }
  // },
  // test路由-组件测试
  // {
  //   path: "/compoTest",
  //   name: "compoTest",
  //   component: () => import("../process/test.vue"),
  //   meta: {
  //     title: "test路由"
  //   }
  // },
  // // test路由-选择人员组件
  // {
  //   path: "/choosePeople",
  //   name: "choosePeople",
  //   component: () => import("../process/components/choosePeople.vue"),
  //   meta: {
  //     title: "选择人员"
  //   }
  // },
  // // test路由-回退人员组件
  // {
  //   path: "/backPeople",
  //   name: "backPeople",
  //   component: () => import("../process/components/backPeople.vue"),
  //   meta: {
  //     title: "回退人员"
  //   }
  // }
];

export default routes;
