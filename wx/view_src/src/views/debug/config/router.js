const routes = [
  {
    path: "/showVConsole",
    name: "showVConsole",
    component: () => import("../showVConsole.vue")
  },
  {
    path: "/hideVConsole",
    name: "hideVConsole",
    component: () => import("../hideVConsole.vue")
  }
];

export default routes;
