export default {
  data() {
    return {};
  },
  mounted() {},
  methods: {
    // 数组交换
    swapArray(arr, index1, index2) {
      arr[index1] = arr.splice(index2, 1, arr[index1])[0];
      return arr;
    },
    // 改变队列
    reSetListBySocket(_action, _data, _list) {
      // debugger;
      console.log(_list.length);
      // 兼容大屏幕数据
      if (!_data.info.response_time) {
        _data.info.response_time = _data.response_time;
      }
      // 兼容id和queue_id字段不一致
      if (!_data.info.id) {
        _data.info.id = _data.info.queue_id;
      }

      // 兼容大屏幕数据
      console.log("Socket 变化前数组", _list, _data);
      //置顶
      if (_action == "ACTION_SET_TOP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          // 删除原来的位置
          _list.splice(index, 1);
          // 插入最新位置
          _list.unshift(_data.info);
        }
      }
      //置低
      if (_action == "ACTION_SET_BOTTOM") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          // 删除原来的位置
          _list.splice(index, 1);
          // 插入最新位置
          _list.push(_data.info);
        }
      }
      //顶部插入
      if (_action == "ACTION_INPUT_TOP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (element.id == _data.queue_id) {
            index = i;
            break;
          }
        }
        // 插入最新位置
        if (index == -1) {
          _list.unshift(_data.info);
        }
      }
      //底部插入
      if (_action == "ACTION_INPUT_BOTTOM") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (element.id == _data.queue_id) {
            index = i;
            break;
          }
        }
        // 插入最新位置
        if (index == -1) {
          _list.push(_data.info);
        }
      }
      //上移一位
      if (_action == "ACTION_MOVE_UP") {
        let index = 0;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != 0) {
          _list = this.swapArray(_list, index, index - 1);
        }
      }
      //移除
      if (_action == "ACTION_DELETE") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          // debugger;
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list.splice(index, 1);
        }
      }
      //位置不变，装车状态可能变，结合data->state
      if (_action == "ACTION_LOADING_KEEP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list[index] = _data.info;
        }
      }
      //排队状态变化，排队->作业  结合产品线id判断，如果是同产品线则不处理
      //排队状态变化，作业->排队 结合产品线id判断，如果是同产品线则不处理
      if (_action == "ACTION_QUEUE_WORK" || _action == "ACTION_QUEUE_WORK") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            element.business_id == _data.business_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list[index] = _data.info;
        }
      }
      console.log("Socket 变化后数组", _list);
      return _list;
    },
    // 返回改变队列的数据
    reSetListBySocketNew(_action, _data, _list) {
      console.log(_list.length);
      // debugger;
      // 兼容大屏幕数据
      if (!_data.info.response_time) {
        _data.info.response_time = _data.response_time;
      }
      // 兼容id和queue_id字段不一致
      if (!_data.info.id) {
        _data.info.id = _data.info.queue_id;
      }

      // 兼容大屏幕数据
      console.log("Socket 变化前数组", _list, _data);
      //置顶
      if (_action == "ACTION_SET_TOP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          // 删除原来的位置
          _list.splice(index, 1);
          // 插入最新位置
          _list.unshift(_data.info);
        }
      }
      //顶部插入
      if (_action == "ACTION_INPUT_TOP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (element.id == _data.queue_id) {
            index = i;
            break;
          }
        }
        // 插入最新位置
        if (index == -1) {
          _list.unshift(_data.info);
        }
      }
      //底部插入
      if (_action == "ACTION_INPUT_BOTTOM") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (element.id == _data.queue_id) {
            index = i;
            break;
          }
        }
        // 插入最新位置
        if (index == -1) {
          _list.push(_data.info);
        }
      }
      //上移一位
      if (_action == "ACTION_MOVE_UP") {
        let index = 0;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != 0) {
          _list = this.swapArray(_list, index, index - 1);
        }
      }
      //移除
      if (_action == "ACTION_DELETE") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          // debugger;
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list.splice(index, 1);
        }
      }
      //位置不变，装车状态可能变，结合data->state
      if (_action == "ACTION_LOADING_KEEP") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list[index] = _data.info;
        }
      }
      //排队状态变化，排队->作业  结合产品线id判断，如果是同产品线则不处理
      //排队状态变化，作业->排队 结合产品线id判断，如果是同产品线则不处理
      if (_action == "ACTION_QUEUE_WORK" || _action == "ACTION_QUEUE_WORK") {
        let index = -1;
        for (let i = 0; i < _list.length; i++) {
          let element = _list[i];
          if (
            element.id == _data.queue_id &&
            element.business_id == _data.business_id &&
            Number(element.response_time) < Number(_data.info.response_time)
          ) {
            index = i;
            break;
          }
        }
        if (index != -1) {
          _list[index] = _data.info;
        }
      }
      console.log("Socket 变化后数组", _list);
      // return _list;
    }
  },
  computed: {},
  watch: {},
  async created() {}
};
