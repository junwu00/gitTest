export default {
  data() {
    return {};
  },
  beforeRouteLeave(_to, _form, next) {
    if (
      _to.path != this.toKeepAlivePath &&
      _to.path.indexOf(this.toKeepAlivePath) < 0 &&
      this.$vnode &&
      this.$vnode.data.keepAlive
    ) {
      if (
        this.$vnode.parent &&
        this.$vnode.parent.componentInstance &&
        this.$vnode.parent.componentInstance.cache
      ) {
        if (this.$vnode.componentOptions) {
          const cache = this.$vnode.parent.componentInstance.cache;
          const keys = this.$vnode.parent.componentInstance.keys;
          console.log("keys,cache", keys, cache);
          // 销毁缓存
          keys.forEach(wk => {
            delete cache[wk];
          });
        }
      }
    }
    next();
  },
  mounted() {},
  methods: {},
  computed: {},
  watch: {},
  async created() {}
};
