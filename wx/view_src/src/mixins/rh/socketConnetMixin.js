import { mapActions, mapMutations, mapGetters } from "vuex";
import moment from "moment";
export default {
  data() {
    return {
      evenQueueList: {},
      evenDriverQueueCall: {},
      isJoinRoomAndMonitorEven: false,
      checkIsOnTheTourInterval: null,
      isOnTheTour: false,
      isOnTheTourTime: null,
      nowTime: null,
      timeInterval: null,
      isTenMinute: false,
      isTenMinuteTime: null,
      version: ""
    };
  },
  mounted() {},
  methods: {
    ...mapMutations(["closeSocket"]),
    ...mapActions(["initSocket", "joinRoom"]),
    // 添加当前页面业务内容的socket监听
    monitorEvenQueue() {
      this.socketObj.on("even_queue", data => {
        this.evenQueueList = data;
      });
    },
    monitorEvenDriverQueueCall() {
      this.socketObj.on("even_driver_queue_call", data => {
        console.log("even_driver_queue_call", data);
        this.evenDriverQueueCall = data;
      });
    },
    //定期纠正数据
    socketConnetMixinRestData() {
      this.closeSocket();
      setTimeout(() => {
        this.initSocket();
      }, 2000);
    },
    // 判断是否整点
    checkIsOnTheTour() {
      let now = moment();
      // let dura = now.diff(this.isOnTheTourTime, "seconds"); //minutes
      let dura = now.diff(this.isOnTheTourTime, "hours");
      // 间隔一小时
      console.log("dura", dura);
      if (Number(dura) >= 1) {
        this.isOnTheTour = true;
        this.isOnTheTourTime = now;
      } else {
        this.isOnTheTour = false;
      }
      // 间隔5分钟检测
      console.log("duraminutes", duraminutes);
      let duraminutes = now.diff(this.isTenMinuteTime, "minutes");
      if (Number(duraminutes) >= 5) {
        this.isTenMinute = true;
        this.isTenMinuteTime = now;
      } else {
        this.isTenMinute = false;
      }
    },
    getPcenterdetail() {
      return this.$common.http({
        url: "/api/wx/pcenter/detail",
        noLoading: true
      });
    },
    getAutoReloadType() {
      if (this.autoReloadPage) {
        this.$common
          .http({
            url: "/api/wx/queue/summary/board_version",
            method: "get",
            noLoading: true
          })
          .then(res => {
            let version = res.data.data.version;
            if (this.version != "" && this.version != version) {
              this.version = version;
              window.location.reload();
            } else {
              this.version = version;
            }
          });
      }
    }
  },
  computed: {
    ...mapGetters(["socketObj", "socketIsDisconnect", "socketBsStatu"])
  },
  watch: {
    async socketIsDisconnect(val) {
      if (val && !this.socketBsStatu) {
        console.log("开始加入房间成功");
        await this.joinRoom(this.socketConnetMixinRoom);
        console.log("加入房间成功");
        this.monitorEvenQueue();
        this.monitorEvenDriverQueueCall();
        this.isJoinRoomAndMonitorEven = true;
      } else {
        this.isJoinRoomAndMonitorEven = false;
      }
    },
    async socketBsStatu(val) {
      if (!val && this.socketIsDisconnect && !this.isJoinRoomAndMonitorEven) {
        console.log("socket断开已重新连接，当前业务已经处理完,重新加入房间");
        await this.joinRoom(this.socketConnetMixinRoom);
        console.log("加入房间成功");
        this.monitorEvenQueue();
        this.monitorEvenDriverQueueCall();
        this.isJoinRoomAndMonitorEven = true;
      }
    },
    isOnTheTour(val) {
      if (val) {
        this.socketConnetMixinRestData();
      }
    },
    isTenMinute(val) {
      if (val) {
        this.getAutoReloadType();
      }
    }
  },
  beforeRouteLeave(_to, _form, next) {
    this.closeSocket();
    window.clearInterval(this.checkIsOnTheTourInterval);
    window.clearInterval(this.timeInterval);
    next();
  },
  async created() {
    // 获取个人信息，用于自动登录
    if (!this.socketConnetMixinUnGetToken) {
      await this.getPcenterdetail();
    }
    await this.initSocket();
    console.log("initSocket 初始换成功");
    let now = moment();
    this.isOnTheTourTime = now;
    this.isTenMinuteTime = now;
    this.getAutoReloadType();
    this.checkIsOnTheTourInterval = window.setInterval(() => {
      this.checkIsOnTheTour();
    }, 60 * 1000);

    this.timeInterval = window.setInterval(() => {
      this.nowTime = moment().format("YYYY-MM-DD HH:mm:ss");
    }, 1 * 1000);

    // // 模拟socket数据
    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_TOP"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"}],"data":{"queue_id":"88d1a692b9da4755b3c8b4ccf94362a8","warehouse_id":"4106a10e90d24c1da7e68d6d4573ef12","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"88d1a692b9da4755b3c8b4ccf94362a8","queue_code":"FP020","license_plate_type":"","license_plate":"\u7518A75934","vehicle_type":1,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"8\u6210\u54c1\u51fa\u5382-\u5b50\u83ab\u52a8","queue_state":2,"loading_state":"","into_factory_time":1614828170,"parking_space_id":"4106a10e90d24c1da7e68d6d4573ef12","parking_space_name":"HTYA01C11","warehouse_id":"2f766b8ef16445b993b1914f7b530d43","warehouse_name":"2\u533aBM2\u8054\u5408\u5382\u623f","queue_begin_time":1614828175,"warehouseman_id":"19713759bc7041ab8fa80a77bbb178af","com_id":"k71kercjqsa44d5f14tyrhbjy3kcotua","begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":[]},"response_time":1616136036},"time":1616136036}';
    // }, 5000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dd","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dd","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 6000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 7000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc1","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc1","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 8000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc2","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc2","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 9000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc3","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc3","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 10000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc4","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc4","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 11000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 12000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc6","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc6","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 13000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc7","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc7","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 14000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc8","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc8","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 15000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc9","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc9","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 16000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc10","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc10","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 17000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc11","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc11","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 18000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc12","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc12","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 19000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc13","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc13","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 20000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc14","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc14","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 21000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc15","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc15","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 22000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc16","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc16","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 23000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc17","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc17","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 30000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc118","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc118","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 30000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc19","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc19","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    // }, 30000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_INPUT_BOTTOM"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc20","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc20","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":1608693677},"time":1608693677}';
    //   console.error("增加数据完毕");
    // }, 30000);

    // // 移除数据
    // setTimeout(() => {
    //   this.evenQueueList = console.error("开始移除数据");
    // }, 55000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97ff","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97ff","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dd","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dd","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc1","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc1","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc2","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc2","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc3","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc3","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc4","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc4","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc5","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.evenQueueList =
    //     '{"id":1697,"actions":[{"queue":"ACTION_REFRESH_NUMBER","action":"ACTION_REFRESH_NUMBER"},{"queue":"QUEUE_QUEUE","action":"ACTION_DELETE"},{"queue":"QUEUE_CALLING","action":"ACTION_DELETE"},{"queue":"ACTION_OCCUPY_WAREHOUSE","action":"ACTION_OCCUPY_WAREHOUSE"},{"queue":"VOICE","action":"ACTION_PLAY_VOICE"}],"data":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc6","warehouse_id":"b9df7e1f232347ceb48dfbc2238b5df8","business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","info":{"queue_id":"8acc0fa369fd412dab9227a5432a97dc6","queue_code":"FP416","license_plate_type":1,"license_plate":"\\u8d35675242","vehicle_type":3,"business_id":"4cf24a5d6fcf40c2a0693e59a4035dec","business_name":"7\\u6210\\u54c1\\u51fa\\u5382-\\u7981\\u7528","queue_state":2,"loading_state":"","into_factory_time":1608525833,"parking_space_id":"b9df7e1f232347ceb48dfbc2238b5df8","parking_space_name":"\\u5e93\\u4f4d8-2","warehouse_id":"c65bd63c9fe648d790e27adf60ea1175","warehouse_name":"\\u534e\\u4e30\\u516b\\u53f7\\u4ed3","queue_begin_time":1608646055,"begin_loading_time":"","unit_id":"","unit_full_name":"","unit_short_name":"","call_number_time":""},"more":{"calling_audio":["http:\\/\\/dev.renheng.order.3ruler.com\\/tts\\/b85697c9e182d083b9859048337f4705.wav"]},"response_time":16086936770},"time":16086936770}';
    // }, 60000);

    // setTimeout(() => {
    //   this.socketConnetMixinRestData();
    // }, 12000);
  }
};
