import moment from "moment";
export default {
  data() {
    return {
      calculationTime: null
    };
  },
  mounted() {},
  methods: {},
  computed: {
    minNumTotext() {
      return dura => {
        dura = parseInt(dura / 60);
        if (dura >= 60 * 24) {
          return (
            parseInt(dura / (60 * 24)) +
            "天" +
            parseInt((dura - parseInt(dura / (60 * 24)) * (60 * 24)) / 60) +
            "小时"
          );
        }
        if (dura >= 60) {
          return parseInt(dura / 60) + "小时" + parseInt(dura % 60) + "分钟";
        }
        if (dura < 60) {
          return dura + "分钟";
        }
      };
    },
    timeCaclFormat() {
      return time => {
        let t = moment.unix(time);
        let dura = this.calculationTime.diff(t, "minute");
        if (dura >= 60 * 24) {
          return (
            parseInt(dura / (60 * 24)) +
            "天" +
            parseInt((dura - parseInt(dura / (60 * 24)) * (60 * 24)) / 60) +
            "小时"
          );
        }
        if (dura >= 60) {
          return parseInt(dura / 60) + "小时" + (dura % 60) + "分钟";
        }
        if (dura < 60) {
          return dura + "分钟";
        }
        return "";
      };
    },
    timeCacl() {
      return time => {
        let t = moment.unix(time);
        let dura = this.calculationTime.diff(t, "minute");
        return dura + "分钟";
      };
    },
    timeCaclNum() {
      return time => {
        let t = moment.unix(time);
        let dura = this.calculationTime.diff(t, "minute");
        return dura;
      };
    },
    indexText() {
      return index => {
        if (Number(index) < 10) {
          return "0" + (index + 1).toString();
        } else {
          return index;
        }
      };
    },
    formatStatus() {
      return (into, appoint) => {
        let str1 = String(appoint.booking_day);
        let start_date = moment(
          `${str1.substring(0, 4)}-${str1.substring(4, 6)}-${str1.substring(
            6,
            8
          )} ${appoint.begin_time}`
        ).unix();
        let end_date = moment(
          `${str1.substring(0, 4)}-${str1.substring(4, 6)}-${str1.substring(
            6,
            8
          )} ${appoint.end_time}`
        ).unix();
        if (into < start_date) {
          return 0;
        } else if (into >= start_date && into <= end_date) {
          return 1;
        } else if (into > end_date) {
          return 2;
        }
      };
    },
    formatTime() {
      return str => {
        let colYear = Number(moment(Number(str) * 1000).format("YYYY"));
        if (moment().year() === colYear) {
          if (
            moment().format("MM月DD日") ===
            moment(Number(str) * 1000).format("MM月DD日")
          ) {
            return "今日" + moment(Number(str) * 1000).format("HH:mm");
          } else {
            return moment(Number(str) * 1000).format("MM月DD日 HH:mm");
          }
        } else {
          return moment(Number(str) * 1000).format("YYYY年MM月DD日 HH:mm");
        }
      };
    },
    queueStateText() {
      return state => {
        if (state == 1) {
          return "排队中";
        }
        if (state == 2) {
          return "叫号中";
        }
        if (state == 3) {
          return "已过号";
        }
        if (state == 4) {
          return "已失效";
        }
        if (state == 5) {
          return "已完成";
        }
        if (state == 6) {
          return "已终止";
        }
        return "--";
      };
    },
    loadingStateText() {
      return state => {
        if (state == 0) {
          return "未开始";
        }
        if (state == 1) {
          return "等待装货";
        }
        if (state == 2) {
          return "拒绝装货";
        }
        if (state == 3) {
          return "装货中";
        }
        if (state == 4) {
          return "装货完成";
        }
        if (state == 5) {
          return "已完成";
        }
        if (state == 6) {
          return "已终止";
        }
        return "--";
      };
    },
    vehicleTypeText() {
      return vehicleType => {
        if (vehicleType == 1) {
          return "平板车";
        } else if (vehicleType == 2) {
          return "货柜车";
        } else if (vehicleType == 3) {
          return "槽罐车";
        } else if (vehicleType == 5) {
          return "高栏车";
        } else {
          return "其他";
        }
      };
    }
  },
  watch: {},
  async created() {
    this.calculationTime = moment();
    this.calculationTimeInterval = window.setInterval(() => {
      this.calculationTime = moment();
    }, 60000);
  }
};
