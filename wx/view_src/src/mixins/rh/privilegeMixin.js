import { mapGetters } from "vuex";
export default {
  data() {
    return {
      calculationTime: null
    };
  },
  mounted() {},
  methods: {},
  computed: {
    ...mapGetters(["privilegeConfig", "dirverOrCoopUnit"])
  },
  watch: {},
  async created() {}
};
