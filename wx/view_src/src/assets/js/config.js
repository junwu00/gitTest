// 项目开发修改对应的envConfig/项目名.js文件，其他文件尽量保持统一，如其他文件需要修改，先想对应对应技术负责人确认后执行修改
const pConfig = require("../../projectConfig/envConfig/" +
  process.env.VUE_APP_PRJ_NAME +
  ".js");

// test 大法的token
export const _getTestToken = () => {
  return pConfig._getTestToken();
};
// 地址参数添加userID
export const _getTestUserId = () => {
  return pConfig._getTestUserId();
};
// 获取当前环境
export const _getEvn = () => {
  return process.env.NODE_ENV;
};

// 获取当前项目成功状态码
export const _getSuccessCode = () => {
  return pConfig._getSuccessCode();
};

// 获取当前项目登录超时状态码
export const _getLoginOutCode = () => {
  return pConfig._getLoginOutCode();
};

// 获取当前项目无权限状态码
export const _getNoPowerCode = () => {
  return pConfig._getNoPowerCode();
};

// 获取当前项目无权限状态码特殊页面
export const _getNoPowerCodeComSelect = () => {
  return pConfig._getNoPowerCodeComSelect();
};

// 常规错误页面
export const _getCommErrUrl = () => {
  return pConfig._getCommErrUrl();
};

// 无权限页面路径
export const _getNoPowerUrl = () => {
  return pConfig._getNoPowerUrl();
};

// 无权限页面路径，特定页面
export const _getNoPowerComSelectUrl = () => {
  return pConfig._getNoPowerComSelectUrl();
};

// 获取统一数据结构的code值和errmsg值
export const _getResCodeAndErrMsg = res => {
  return pConfig._getResCodeAndErrMsg(res);
};

// 获取登录路径
export const _getLoginAuthorizeUrl = res => {
  return pConfig._getLoginAuthorizeUrl(res);
};

// 获取验证码状态
export const _getCaptchaCode = res => {
  return pConfig._getCaptchaCode(res);
};
