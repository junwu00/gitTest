// 该文件仅封装请求，get，post，upLoadFile，登录token的维护，请求中涉及的遮罩，独立文件引入，以便兼容各个平台
import axios from "axios";
import qs from "qs";
import common from "./common";
import {
  _getEvn,
  _getSuccessCode,
  _getLoginOutCode,
  _getNoPowerCode,
  _getNoPowerCodeComSelect,
  _getNoPowerUrl,
  _getNoPowerComSelectUrl,
  _getTestUserId,
  _getLoginAuthorizeUrl,
  _getResCodeAndErrMsg,
  _getCaptchaCode,
  _getCommErrUrl
} from "./config.js";

// 跳转到登录页面
const redirectLogin = res => {
  // 授权登录url，部分項目返回的数据结构不一致，通过方法获取，在config.js里面配置
  const finallUrl = _getLoginAuthorizeUrl(res);
  console.log("登录地址", finallUrl);
  if (_getEvn() !== "development") {
    window.location.replace(finallUrl);
  }
};

// 跳转到无常规错误页面
// eslint-disable-next-line no-unused-vars
const redirectCommErrUrl = res => {
  console.log("常规错误页面", _getCommErrUrl());
  if (_getEvn() !== "development") {
    window.location.href = _getCommErrUrl() + "?err=" + res;
  }
};

// 跳转到无权限页面
// eslint-disable-next-line no-unused-vars
const redirectNoPower = res => {
  console.log("无权限页面", _getNoPowerUrl());
  if (_getEvn() !== "development") {
    window.location.href = _getNoPowerUrl();
  }
};

// 跳转到无权限页面,特定页面
// eslint-disable-next-line no-unused-vars
const redirectNoPowerComSelect = res => {
  console.log("无权限页面", _getNoPowerComSelectUrl());
  if (_getEvn() !== "development") {
    window.location.href = _getNoPowerComSelectUrl();
  }
};
let pending = []; //声明一个数组用于存储每个ajax请求的取消函数和ajax标识
let cancelToken = axios.CancelToken;
let removePending = (ever) => {
    for(let p in pending){
        if(pending[p].u === ever.url + '&' + ever.method) { //当当前请求在数组中存在时执行函数体
            pending[p].f(); //执行取消操作
            pending.splice(p, 1); //把这条记录从数组中移除
        }
    }
}
    //http request 拦截器
    axios.interceptors.request.use(
      config => {
        // config.data = JSON.stringify(config.data);
        // config.headers = {
        //   'Content-Type':'application/x-www-form-urlencoded'
        // }
        // ------------------------------------------------------------------------------------
        removePending(config); //在一个ajax发送前执行一下取消操作
        config.cancelToken = new cancelToken((c)=>{
          let blackList = ["model=finstat&m=ajax&cmd=201"]
          for (let black of blackList){
            if(config.url.indexOf(black)!=-1){
              pending.push({ u: config.url + '&' + config.method, f: c }); 
             }
          }
           // 这里的ajax标识我是用请求地址&请求方式拼接的字符串，当然你可以选择其他的一些方式
           
            
        });
        // -----------------------------------------------------------------------------------------
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      response => {
        // ------------------------------------------------------------------------------------------
        // removePending(response.config);  //在一个ajax响应后再执行一下取消操作，把已经完成的请求从pending中移除
        // -------------------------------------------------------------------------------------------
        return response;
      },
      error => {
        return Promise.reject(error)
      }
    )
// 兼容token或者session模式
export default ({
  url,
  data = {},
  noLoading = false,
  file = null,
  method = "POST",
  retuenReject = false
}) => {
  // 部分项目test调试
  if (_getEvn() === "development" && _getTestUserId().length > 0) {
    url += "&test=" + _getTestUserId();
  }
  return new Promise((resolve, reject) => {
    // console.log(common._getTerminalMsg());
    // token 值
    let projectNameArray = window.location.pathname.split("/");
    let projectName = (projectNameArray[1] || "") + "token";
    let { isWX, isMobile, isPC } = common._getTerminalMsg();
    if (!noLoading) {
      if (isMobile || isWX) {
        common.mbToastLoading();
      } else if (isPC) {
        common.pcToastLoading();
      } else {
        console.log("XXX组件加载中");
      }
    }
    axios({
      method: method,
      url: url,
      data: file || qs.stringify(data),
      headers: {
        Authorization: common.getToken(),
        "Content-Type": file
          ? "multipart/form-data"
          : "application/x-www-form-urlencoded"
      }
    })
      .then(
        res => {
          if (!noLoading) {
            if (isMobile || isWX) {
              common.mbToastClose();
            } else if (isPC) {
              common.pcToastClose();
            } else {
              console.log("XXX组件取消加载");
            }
          }
          let { code, errmsg } = _getResCodeAndErrMsg(res);
          // console.log(code, _getCaptchaCode(res));
          if (code === _getSuccessCode()) {
            resolve(res);
          } else if (code === _getLoginOutCode(res)) {
            // window.localStorage.removeItem("token");
            window.localStorage.removeItem(projectName);
            redirectLogin(res);
            reject(res);
          } else if (code === _getNoPowerCodeComSelect(res)) {
            // 记录当前页面地址
            let noPowerReturnUrl = window.location.href;
            window.localStorage.setItem("noPowerReturnUrl", noPowerReturnUrl);
            redirectNoPowerComSelect(res);
            reject(res);
          } else if (code === _getNoPowerCode(res)) {
            // 记录当前页面地址
            // let noPowerReturnUrl = window.location.href;
            // window.localStorage.setItem("noPowerReturnUrl", noPowerReturnUrl);
            redirectNoPower(res);
            reject(res);
          } else if (code === _getCaptchaCode(res)) {
            if (isMobile || isWX) {
              common.mbToastClose();
              common.mbToastFail(errmsg);
            } else if (isPC) {
              common.pcToastClose();
              common.pcToastFail(errmsg);
            } else {
              alert(errmsg);
            }
            resolve(res);
          } else {
            if (!noLoading) {
              if (isMobile || isWX) {
                common.mbToastFail(errmsg);
                // redirectCommErrUrl(errmsg);
              } else if (isPC) {
                common.pcToastFail(errmsg);
              } else {
                alert(errmsg);
              }
            }
            if (retuenReject) {
              resolve(res);
            }
            reject(res);
          }
        },
        res => {
          console.error("axios default reject error", res);
          try {
            if (isMobile || isWX) {
              common.mbToastClose();
            }
            if (isPC) {
              common.pcToastClose();
            }
          } catch (error) {
            console.error(error);
          }
          reject(res);
        }
      )
      .catch(e => {
        console.error("axios resolve catch error", e);
        try {
          if (isMobile || isWX) {
            common.mbToastClose();
            // common.mbToastFail(e.toString());
          } else if (isPC) {
            common.pcToastClose();
            // common.pcToastFail(e.toString());
          } else {
            alert(e);
          }
        } catch (error) {
          console.error(error);
        }
        // 上传错误日志
        // logUpload
        reject(e);
      })
      .finally(() => {});
  });
};
