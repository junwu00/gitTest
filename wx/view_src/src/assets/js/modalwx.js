// 移动端基于Vant开发
import { Toast } from "vant";

export const mbToastLoading = (msg = "") => {
  Toast.loading({
    message: msg,
    forbidClick: true,
    duration: 0
  });
};

export const mbToastClose = () => {
  Toast.clear();
};

export const mbToastFail = msg => {
  let msgNew = msg.replace(/<br\/>/g, "\n");
  Toast.fail({
    message: msgNew,
    className: "comToast"
  });
};

export const mbToastSuccess = msg => {
  let msgNew = msg.replace(/<br\/>/g, "\n");
  Toast.success({
    message: msgNew
  });
};
