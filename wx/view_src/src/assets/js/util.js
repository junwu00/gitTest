// 该文件仅能有纯js可运行方式
import vConsole from "vconsole";
import moment from "moment";
// import { _getEvn, _getTestToken } from "./config.js";

// 获取地址参数
export const _getQuery = (name, _url) => {
  let url = _url || window.location.href;
  if (!name) {
    return "";
  }
  url = url || window.location.search;
  let _name = name.replace(/(?=[\\^$*+?.():|{}])/, "\\");
  let reg = new RegExp("(?:[?&]|^)" + _name + "=([^?&#]*)", "i");
  let match = url.match(reg);
  return !match ? "" : match[1];
};

// vconsole 封装，默認访问了showVConsole之后30分钟有效，用于快速定位移动端的问题，支持客户的手机调起以及到期自动关闭
export const _initVConsole = () => {
  // new vConsole();
  try {
    // 判断是否过期
    const vccf = JSON.parse(window.localStorage.getItem("showVConsole"));
    if (vccf && moment().unix() - Number(vccf.time) > 120) {
      window.localStorage.removeItem("showVConsole");
    }
    // 重新取值判断是否初始化vConsole
    const vccf1 = JSON.parse(window.localStorage.getItem("showVConsole"));
    if (vccf1 && Number(vccf1.show) === 1) {
      new vConsole();
    }
  } catch (error) {
    window.localStorage.removeItem("showVConsole");
  }
};

// 获取终端信息
export const _getTerminalMsg = () => {
  const userAgent = window.navigator.userAgent;
  return {
    // 是否移动端
    isMobile:
      userAgent.indexOf("Mobile") >= 0 || userAgent.indexOf("mobile") >= 0
        ? true
        : false,
    // 是否安卓
    isAndroid:
      userAgent.indexOf("Android") >= 0 || userAgent.indexOf("android") >= 0
        ? true
        : false,
    // 是否IOS
    isIOS:
      userAgent.indexOf("iPhone") >= 0 || userAgent.indexOf("iphone") >= 0
        ? true
        : false,
    // 是否PC
    isPC:
      userAgent.indexOf("Mobile") < 0 && userAgent.indexOf("mobile") < 0
        ? true
        : false,
    // 是否微信
    isWX: userAgent.indexOf("MicroMessenger") >= 0 ? true : false,
    // 是否企业微信
    isWorkWX:
      userAgent.indexOf("MicroMessenger") >= 0 &&
      userAgent.indexOf("wxwork") >= 0
        ? true
        : false,
    // 是否微信PC版本
    isWinWX:
      userAgent.indexOf("MicroMessenger") >= 0 &&
      userAgent.indexOf("WindowsWechat") >= 0
        ? true
        : false,
    // 是否PC版本企业微信
    isWinWorkWX:
      userAgent.indexOf("MicroMessenger") >= 0 &&
      userAgent.indexOf("WindowsWechat") >= 0 &&
      userAgent.indexOf("wxwork") >= 0
        ? true
        : false
  };
};

export const getCookie = name => {
  let value = "";
  let cookies = document.cookie.split(";");
  for (let i = 0; i < cookies.length; i++) {
    let cookie = cookies[i].trim();
    if (cookie.indexOf(name + "=") !== -1) {
      value = decodeURIComponent(cookie.substring(name.length + 1));
      break;
    }
  }
  return value;
};

export const removeCookie = cookieName => {
  var cookies = document.cookie.split(";");
  for (var i = 0; i < cookies.length; i++) {
    // 有些cookie键值对前面会莫名其妙产生一个空格，将空格去掉
    if (cookies[i].indexOf(" ") === 0) {
      cookies[i] = cookies[i].substring(1);
    }

    if (cookies[i].indexOf(cookieName) === 0) {
      var exp = new Date();
      exp.setTime(exp.getTime() - 60 * 1000);
      document.cookie =
        cookies[i] + ";expires=" + exp.toUTCString() + ";path=/";
      break;
    }
  }
};

export const setCookie = opt => {
  let exp = new Date();
  let timeStamp = exp.getTime() + opt.expires;
  exp.setTime(timeStamp);
  document.cookie = `${opt.key}=${escape(
    opt.value
  )};expires=${exp.toGMTString()};path=/`;
};

// 获取token
export const getToken = () => {
  // token 值
  let projectNameArray = window.location.pathname.split("/");
  let projectName = (projectNameArray[1] || "") + "token";
  // 取地址token
  const urlToken = _getQuery("token");

  // 1、强制获取地址的url，不更新localStorage，强制返回
  if (_getQuery("token_type") == "urlonly") {
    return urlToken;
  }
  // 2、获取地址或者缓存地址，地址优先
  if (urlToken.length > 0) {
    // 更新token
    // window.localStorage.setItem("token", urlToken);
    window.localStorage.setItem(projectName, urlToken);
    return urlToken;
  }
  // 取缓存token
  // const cacheToken = window.localStorage.getItem("token") || "";
  const cacheToken = window.localStorage.getItem(projectName) || "";
  if (cacheToken.length == 0) {
    console.error("token不存在");
    return cacheToken;
  }
  return cacheToken;
};

export const deep = obj => {
  var objClone = Array.isArray(obj) ? [] : {};
  if (obj && typeof obj === "object") {
    for (let key in obj) {
      if (obj[key] && typeof obj[key] === "object") {
        objClone[key] = deep(obj[key]);
      } else {
        objClone[key] = obj[key];
      }
    }
  }
  return objClone;
};

export const objValToJson = obj => {
  let newObj = {};
  if (obj && typeof obj === "object") {
    for (let key in obj) {
      newObj[key] = JSON.stringify(obj[key]);
    }
    return newObj;
  } else {
    return obj;
  }
};

//将base64转换为blob
export const dataURLtoBlob = (dataurl, fileName) => {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], fileName, { type: mime });
};

//将blob转换为file
export const blobToFile = (theBlob, fileName) => {
  theBlob.lastModifiedDate = new Date();
  theBlob.name = fileName;
  return theBlob;
};

// 校验车牌号码
export const isLicensePlate = str => {
  return /^(([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]))$/.test(
    str
  );
};

//
// 根据权限信息过滤菜单
export const filterByPrivilege = (menuData, configs) => {
  let newData = [];
  if (Number(configs.is_super) == "is_super") {
    newData = menuData;
  } else {
    menuData.forEach(item => {
      if (item.children) {
        let tmpItem = JSON.parse(JSON.stringify(item));
        let temChildren = [];
        item.children.forEach(itemC => {
          if (!itemC.privilege) {
            temChildren.push(itemC);
          } else {
            let privileges = itemC.privilege.split("&");
            // 菜单无权限控制
            if (privileges.length == 0) {
              temChildren.push(itemC);
            } else {
              let hasOnce = false;
              privileges.forEach(privilegeItem => {
                if (configs.keys.indexOf(privilegeItem) >= 0) {
                  hasOnce = true;
                }
              });
              if (hasOnce) {
                temChildren.push(itemC);
              }
            }
          }
        });
        tmpItem.children = temChildren;
        if (tmpItem.children.length > 0) {
          newData.push(tmpItem);
        }
      } else {
        if (!item.privilege) {
          newData.push(item);
        } else {
          let privileges = item.privilege.split("&");
          // 菜单无权限控制
          if (privileges.length == 0) {
            newData.push(item);
          } else {
            let hasOnce = false;
            privileges.forEach(privilegeItem => {
              if (configs.keys.indexOf(privilegeItem) >= 0) {
                hasOnce = true;
              }
            });
            if (hasOnce) {
              newData.push(item);
            }
          }
        }
      }
    });
  }
  return newData;
};
