// 该文件仅封装请求，get，post，upLoadFile，登录token的维护，请求中涉及的遮罩，独立文件引入，以便兼容各个平台
import axios from "axios";
import qs from "qs";
import common from "./common";
import { _getEvn, _getTestUserId } from "./config.js";

// 兼容token或者session模式
export default ({
  url,
  data = {},
  noLoading = true,
  file = null,
  downloadName = ""
}) => {
  // 部分项目test调试
  if (_getEvn() === "development" && _getTestUserId().length > 0) {
    url += "&test=" + _getTestUserId();
  }
  return new Promise((resolve, reject) => {
    // console.log(common._getTerminalMsg());
    let { isWX, isMobile, isPC } = common._getTerminalMsg();
    if (!noLoading) {
      if (isMobile || isWX) {
        common.mbToastLoading();
      } else if (isPC) {
        common.pcToastLoading();
      } else {
        console.log("XXX组件加载中");
      }
    }

    axios({
      method: "get",
      url: url,
      data: file || qs.stringify(data),
      headers: {
        Authorization: common.getToken(),
        "Content-Type": file
          ? "multipart/form-data"
          : "application/x-www-form-urlencoded"
      },
      responseType: "blob"
    })
      .then(res => {
        let blob = new Blob([res.data]);
        //下载后文件名
        let filename = res.headers;
        // 文件服务提供的文件时，某些情况没有该属性
        if (filename["content-disposition"] == undefined) {
          filename = url.split("/").pop();
        } else {
          filename = filename["content-disposition"];
          filename = filename.split(";")[1].split("filename=")[1];
        }

        let fileExt = filename.split(".").pop();
        if (downloadName.length == 0) {
          downloadName = filename;
        } else {
          downloadName = downloadName + "." + fileExt;
        }
        // ie下载
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveBlob(blob, downloadName);
        } else {
          //火狐谷歌下载
          const elink = document.createElement("a");
          elink.download = downloadName;
          elink.style.display = "none";
          elink.href = URL.createObjectURL(blob);
          document.body.appendChild(elink);
          elink.click();
          URL.revokeObjectURL(elink.href); // 释放URL 对象
          document.body.removeChild(elink);
        }
        resolve();
      })
      .catch(e => {
        console.log("axios error", e);
        if (isMobile) {
          common.mbToastClose();
          common.mbToastFail(e);
        } else if (isPC) {
          common.pcToastClose();
          common.pcToastFail(e);
        } else {
          alert(e);
        }
        reject();
        // 上传错误日志
      })
      .finally(() => {});
  });
};
