// PC端基于elementUI开发
import { Loading, Message } from "element-ui";
import MessagePlus from "../../components/elementPlus/packages/message/index.js";

let loadingInstance = null;

export const pcToastLoading = () => {
  loadingInstance = Loading.service({
    lock: true,
    text: "",
    customClass: "loading"
  });
};

export const pcToastClose = () => {
  loadingInstance.close();
};

export const pcToastFail = (msg, duration = 3000) => {
  // Message({
  //   customClass: "err",
  //   type: "error",
  //   message: msg,
  //   duration: duration,
  //   dangerouslyUseHTMLString: true,
  //   showClose: true
  // });
  MessagePlus({
    customClass: "err",
    type: "error",
    message: msg,
    duration: duration,
    dangerouslyUseHTMLString: true,
    showClose: true
  });
};

export const pcToastSuccess = (msg, duration = 3000) => {
  Message({
    customClass: "err",
    type: "success",
    message: msg,
    duration: duration,
    dangerouslyUseHTMLString: true,
    showClose: true
  });
};

export const pcError = (msg, duration = 3000) => {
  Message({
    customClass: "err",
    type: "error",
    message: msg,
    duration: duration,
    showClose: true
  });
};

export const pcSuccess = (msg, duration = 3000) => {
  Message({
    customClass: "success",
    type: "success",
    message: msg,
    duration: duration,
    showClose: true
  });
};
