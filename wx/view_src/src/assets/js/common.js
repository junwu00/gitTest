import http from "./http";
import httpLoadFile from "./httpLoadFile";
import {
  _getQuery,
  _getTerminalMsg,
  getToken,
  getCookie,
  setCookie,
  removeCookie,
  deep,
  isLicensePlate,
  objValToJson,
  filterByPrivilege,
  dataURLtoBlob,
  blobToFile
} from "./util.js";
import {
  pcToastLoading,
  pcToastClose,
  pcToastFail,
  pcToastSuccess,
  pcError,
  pcSuccess
} from "./modalqy.js";
import {
  mbToastLoading,
  mbToastClose,
  mbToastFail,
  mbToastSuccess
} from "./modalwx.js";
export default class Common {
  static http = http;
  static httpLoadFile = httpLoadFile;
  static _getQuery = _getQuery;
  static _getTerminalMsg = _getTerminalMsg;
  static getToken = getToken;
  static pcToastLoading = pcToastLoading;
  static pcToastClose = pcToastClose;
  static pcToastFail = pcToastFail;
  static pcToastSuccess = pcToastSuccess;
  static mbToastLoading = mbToastLoading;
  static mbToastClose = mbToastClose;
  static mbToastFail = mbToastFail;
  static mbToastSuccess = mbToastSuccess;
  static pcError = pcError;
  static pcSuccess = pcSuccess;
  static getCookie = getCookie;
  static setCookie = setCookie;
  static removeCookie = removeCookie;
  static deep = deep;
  static isLicensePlate = isLicensePlate;
  static objValToJson = objValToJson;
  static filterByPrivilege = filterByPrivilege;
  static dataURLtoBlob = dataURLtoBlob;
  static blobToFile = blobToFile;
}
