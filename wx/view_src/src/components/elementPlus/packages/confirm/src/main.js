import Vue from "vue";
import Main from "./main.vue";
// import { PopupManager } from "../../src/utils/popup";
// import { isVNode } from "../../src/utils/vdom";
let ConfirmConstructor = Vue.extend(Main);

let instance;
// let instances = [];
let seed = 1;

const Confirm = function(options) {
  if (Vue.prototype.$isServer) return;
  options = options || {};
  console.log(options);
  // 默认值
  options = {
    // 标题
    title: options.title || "确认提示",
    // 宽度
    width: options.width || "380px",
    // 控件样式
    confirmDialogClass: options.confirmDialogClass || "confirmClass",
    // 确认按钮文案
    confirmAcText: options.confirmAcText || "确 定",
    // 确认按钮样式
    confirmAcClass: options.confirmAcClass || "confirmAcClass",
    // 确认按钮Style样式
    confirmAcStyle: options.confirmAcStyle || "",
    // 取消按钮文案
    cancelAcText: options.cancelAcText || "取 消",
    // 取消按钮样式
    cancelAcClass: options.cancelAcClass || "cancelAcClass",
    // 取消按钮Style样式
    cancelAcStyle: options.cancelAcStyle || "",
    // 提示文案，支持html
    message: options.message || "确认是否删除？删除后数据无法恢复。",
    // 点击外层是否关闭
    closeOnClickModal: options.closeOnClickModal || false,
    // 取消回调
    cancelAc: options.cancelAc,
    // 确认回调
    confirmAc: options.confirmAc
  };
  console.log(options);

  // let userOnClose = options.onClose;
  let id = "element_plus_confirm_" + seed++;

  // options.onClose = function() {
  //   Message.close(id, userOnClose);
  // };
  instance = new ConfirmConstructor({
    propsData: {
      visible: true,
      id: id,
      title: options.title,
      confirmDialogClass: options.confirmDialogClass,
      confirmAcText: options.confirmAcText,
      confirmAcClass: options.confirmAcClass,
      confirmAcStyle: options.confirmAcStyle,
      cancelAcText: options.cancelAcText,
      cancelAcClass: options.cancelAcClass,
      cancelAcStyle: options.cancelAcStyle,
      message: options.message,
      width: options.width,
      closeOnClickModal: options.closeOnClickModal,
      cancelAc: options.cancelAc,
      confirmAc: options.confirmAc
    }
  });
  instance.id = id;
  // if (isVNode(instance.message)) {
  //   instance.$slots.default = [instance.message];
  //   instance.message = null;
  // }
  instance.$mount();
  // document.body.appendChild(instance.$el);
  // let verticalOffset = options.offset || 20;
  // instances.forEach(item => {
  //   verticalOffset += item.$el.offsetHeight + 16;
  // });
  // instance.verticalOffset = verticalOffset;
  // instance.visible = true;
  // instance.$el.style.zIndex = PopupManager.nextZIndex();
  // instances.push(instance);
  return instance;
};
export default Confirm;
