export default function MapLoader() {
  // <-- 原作者这里使用的是module.exports
  return new Promise((resolve, reject) => {
    if (window.AMap || window.AMapUI) {
      resolve(window.AMap, window.AMapUI);
    } else {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.async = true;
      script.src =
        "//webapi.amap.com/maps?v=2.0&key=3f8e5812328614d729354f34b0f6a375&callback=initAMap";
      script.onerror = reject;
      document.head.appendChild(script);
    }
    window.initAMap = () => {
      // window.initAMapUI = () => {
      resolve(window.AMap, window.AMapUI);
      // }
    };
  });
}
