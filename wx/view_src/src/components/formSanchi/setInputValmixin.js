import { v4 as uuidv4 } from "uuid";
export default {
  props: {},

  data() {
    return {};
  },

  computed: {},

  watch: {},

  components: {},

  mixins: [],

  beforeCreate() {},

  created() {},

  beforeMount() {},

  mounted() {},

  activated() {},

  deactivated() {},

  methods: {
    setInputEditInitData(inputs) {
      Object.keys(inputs).forEach(key => {
        // inputs[key].is_line = true;
        // inputs[key].show_type = 2;
        inputs[key].is_line = false;
        inputs[key].show_type = 1;
        inputs[key].uuid = uuidv4().replace(/-/g, "");

        // eslint-disable-next-line no-prototype-builtins
        if (!inputs[key].hasOwnProperty("val")) {
          inputs[key].val = "";
        }

        // eslint-disable-next-line no-prototype-builtins
        // if (!inputs[key].hasOwnProperty("uuid")) {
        // }

        if (inputs[key].type == "text") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "textarea") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "radio") {
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("c_other")) {
            inputs[key].c_other = 0;
          }
        }

        if (inputs[key].type == "checkbox") {
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("c_other")) {
            inputs[key].c_other = 0;
          }
        }

        if (inputs[key].type == "select") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "date") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "money") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "school") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "muselect") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "conflict") {
          // inputs[key].is_line = true;
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("conflict_type")) {
            inputs[key].conflict_type = "";
          }
          console.log("数据清洗", inputs[key].val);
          if (inputs[key].val == " - ") {
            inputs[key].val = "";
          }
        }

        if (inputs[key].type == "wuzhi_chose") {
          inputs[key].is_line = true;
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("value")) {
            inputs[key].value = {};
          }
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("cat")) {
            inputs[key].cat = {};
          }
        }

        if (inputs[key].type == "wuzhi_attr") {
          inputs[key].is_line = true;
        }

        if (inputs[key].type == "file") {
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("val")) {
            inputs[key].val = [];
          }
        }

        if (inputs[key].type == "car") {
          inputs[key].is_line = false;
          inputs[key].car_list.forEach(car => {
            // eslint-disable-next-line no-prototype-builtins
            if (!car.hasOwnProperty("uuid")) {
              car.uuid = uuidv4().replace(/-/g, "");
            }
            // eslint-disable-next-line no-prototype-builtins
            if (!car.hasOwnProperty("driver")) {
              car.driver = { id: "", name: "", pic: "" };
            }
            if (Array.isArray(car.driver)) {
              car.driver = { id: "", name: "", pic: "" };
            }
          });
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("val_list")) {
            inputs[key].val_list = [];
          }
        }

        if (inputs[key].type == "table") {
          // eslint-disable-next-line no-prototype-builtins
          if (!inputs[key].hasOwnProperty("rec")) {
            inputs[key].rec = [];
          }
          // 可见可编辑状态取table_arr里面的数据
          let table_arr = inputs[key].table_arr;
          inputs[key].rec.forEach(itemArr => {
            for (const key in itemArr) {
              itemArr[key].edit_input = table_arr[key].edit_input;
              itemArr[key].visit_input = table_arr[key].visit_input;
            }
          });
          delete inputs[key].val;
        }
      });
      return inputs;
    },
    setInputShowInitData(inputs) {
      Object.keys(inputs).forEach(key => {
        inputs[key].is_line = true;
        inputs[key].show_type = 2;
        inputs[key].uuid = uuidv4().replace(/-/g, "");
      });
      return inputs;
    },
    setInputTableShowState(inputs) {
      Object.keys(inputs).forEach(key => {
        inputs[key].is_line = true;
        inputs[key].show_type = 2;
      });
      return inputs;
    }
  }
};
