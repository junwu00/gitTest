export default {
  props: {
    // 样式控制
    styleControl: {
      type: Object,
      default: () => {
        return {
          // 可见
          visit_input: 1,
          // 可编辑
          edit_input: 1,
          // 当前组件状态：1编辑，2可见，3可见不可编辑
          show_type: 1,
          // 是否一行显示
          is_line: true,
          //表单项 label 宽度，默认单位为px
          label_width: "6.2em",
          // 表单项 label 对齐方式，可选值为left  center right
          label_align: "left",
          // 输入框对齐方式，可选值为 left center right
          input_align: "left",
          // 字体大小,15->15px、16->16px,17->17px,18->18px
          text_size: 15
        };
      }
    },
    // 接收当前表单所有的值,
    fromListResult: {
      type: Array,
      default: () => {
        return [];
      }
    },
    // rule 整个表单配置的规则
    fromListRule: {
      type: Array,
      default: () => {
        return [];
      }
    }
  },

  data() {
    return {};
  },

  computed: {
    // 样式控制
    classText() {
      let cl = "";
      if (!this.styleControl.is_line) {
        cl += " notLine";
      }
      if (!this.styleControl.text_size) {
        cl += " fs15";
      }
      if (Number(this.styleControl.text_size) === 15) {
        cl += " fs15";
      }
      if (Number(this.styleControl.text_size) === 16) {
        cl += " fs16";
      }
      if (Number(this.styleControl.text_size) === 17) {
        cl += " fs17";
      }
      if (Number(this.styleControl.text_size) === 18) {
        cl += " fs18";
      }
      if (Number(this.styleControl.text_size) === 20) {
        cl += " fs20";
      }
      return cl;
    },
    // 标题控制
    labelWidth() {
      let l = this.styleControl.label_width || "7.2em";
      if (!this.styleControl.is_line) {
        l = "100%";
      }
      return l;
    },
    // 冲突控件对象字段宽度
    labelWidthdoubleDatetime() {
      let l = "7.2em";
      return l;
    },
    // 标题对齐控制
    labelAlign() {
      let l = this.styleControl.label_align || "left";
      if (!this.styleControl.is_line) {
        l = "left";
      }
      return l;
    },
    // 标题颜色样式
    titlestyle() {
      return {
        color: this.styleControl.titlecolor
          ? "#" + this.styleControl.titlecolor
          : "#646566"
      };
    },
    // 描述颜色样式
    descstyle() {
      return {
        color: this.styleControl.desccolor
          ? "#" + this.styleControl.desccolor
          : "#646566"
      };
    }
  },

  watch: {},

  components: {},

  mixins: [],

  beforeCreate() {},

  created() {},

  beforeMount() {},

  mounted() {},

  activated() {},

  deactivated() {},

  methods: {}
};
