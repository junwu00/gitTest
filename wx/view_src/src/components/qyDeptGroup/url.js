// 角色
export default {
  // 部门跟树
  getDeptTop: "/api/wx/tag/list_top_departments",
  // 超过二级的部门树
  getDept: "/api/wx/tag/list_sub_departments",
  // 根据部门id获取人员
  getUserByDeptId: "/api/wx/contact/search",
  // 顶级查询人员id获取人员,查询分组id
  getTopUserByDeptId: "/api/wx/tag/search_user_by_page",
  // 角色不使用
  getGroupList: "",
  // 角色不使用
  getUesrByGroupId: "",
  // 获取初始换数据
  getInitData: "/api/wx/contact/id_to_name"
};
