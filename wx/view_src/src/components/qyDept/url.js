// 作废，使用mixinOrgUrl.js获取当前系统管理员使用的路径

// 跨厂区共
// export default {
//   // 部门跟树
//   getDeptTop: "/api/wx/contact_union/list_top_departments",
//   // 超过二级的部门树
//   getDept: "/api/wx/contact_union/list_sub_departments",
//   // 根据部门id获取人员
//   getUserByDeptId: "/api/wx/contact_union/search",
//   // 跨厂区不使用
//   getGroupList: "",
//   // 跨厂区不使用
//   getUesrByGroupId: "",
//   // 获取初始换数据
//   getInitData: "/api/wx/contact_union/id_to_name"
// };

// 厂区内
export default {
  // 部门跟树
  getDeptTop: "/api/wx/contact/list_top_departments",
  // 超过二级的部门树
  getDept: "/api/wx/contact/list_sub_departments",
  // 根据部门id获取人员
  getUserByDeptId: "/api/wx/contact/search",
  // 分组列表
  getGroupList: "/api/wx/tag/list_all",
  // 根据分组id获取人员
  getUesrByGroupId: "/api/wx/tag/search_user_by_page",
  // 获取初始换数据
  getInitData: "/api/wx/contact/id_to_name"
};

// 角色
// export default {
//   // 部门跟树
//   getDeptTop:
//     "/api/wx/tag/list_top_departments?id=7cb623a1ed3d4e699837f32876aeac3d",
//   // 超过二级的部门树
//   getDept: "/api/wx/tag/list_sub_departments?id=7cb623a1ed3d4e699837f32876aeac3d",
//   // 根据部门id获取人员
//   getUserByDeptId: "/api/wx/contact/search",
//   // 顶级查询人员id获取人员
//   getTopUserByDeptId: "/api/wx/tag/search_user_by_page",
//   // 角色区不使用
//   getGroupList: "",
//   // 角色区不使用
//   getUesrByGroupId: "",
//   // 获取初始换数据
//   getInitData: ""
// };
