// 仅有一行查询条件时，混入计算table的最大高度
export default {
  data() {
    return {};
  },
  mounted() {},
  methods: {
    mixinOrgUrlSet(type) {
      return new Promise(resolve => {
        let sysAdminOrgUrl = {
          // 部门跟树
          getDeptTop: "/api/wx/contact_union/list_top_departments",
          // 超过二级的部门树
          getDept: "/api/wx/contact_union/list_sub_departments",
          // 根据部门id获取人员
          getUserByDeptId: "/api/wx/contact_union/search",
          // 不使用
          getGroupList: "",
          // 不使用
          getUesrByGroupId: "",
          // 初始换数据
          getInitData: "/api/wx/contact_union/id_to_name"
        };
        let bsAdminOrgUrl = {
          // 部门跟树
          getDeptTop: "/api/wx/contact/list_top_departments",
          // 超过二级的部门树
          getDept: "/api/wx/contact/list_sub_departments",
          // 根据部门id获取人员
          getUserByDeptId: "/api/wx/contact/search",
          // 分组列表
          getGroupList: "/api/wx/tag/list_all",
          // 根据分组id获取人员
          getUesrByGroupId: "/api/wx/tag/search_user_by_page",
          // 初始换数据
          getInitData: "/api/wx/contact/id_to_name"
        };
        if (type == "sysAdmin") {
          // 系统管理员
          resolve(sysAdminOrgUrl);
        } else if (type == "bsAdmin") {
          // 业务管理员
          resolve(bsAdminOrgUrl);
        } else {
          //登录身份判断
          this.$common
            .http({
              method: "get",
              url: "/api/wx/permission/list_com"
            })
            .then(res => {
              let comList = res.data.data;
              let isMainCom = "";
              let orgUrl = {};
              comList.forEach(element => {
                if (Number(element.com_is_current) == 1) {
                  isMainCom = element.is_main_com;
                }
              });
              if (Number(isMainCom) == 1) {
                orgUrl = sysAdminOrgUrl;
              } else {
                orgUrl = bsAdminOrgUrl;
              }
              resolve(orgUrl);
            });
        }
      });
    }
  },
  computed: {},
  async created() {}
};
