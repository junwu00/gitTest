var axios = require("axios");
var MockAdapter = require("axios-mock-adapter");
/*
1、安装开发依赖包
yarn add mockjs axios-mock-adapter -D

2、地址规则,如下，左边是后端定义好的API路径，mock的模拟路径仅需要在前面添加mock即可，待后端接口完成后，批量替换mockapi为api即可
/api/users => /mockapi/users
/api/gets => /mockapi/gets
/api/authorize/check_login => /mockapi/authorize/check_login

3、模拟请求的业务数据在./data 中自行模拟

4、mockjs官网：http://mockjs.com/
*/
import { users } from "./data/user";
export default {
  init() {
    var mock = new MockAdapter(axios);
    // post请求
    mock.onPost("/mockapi/users").reply(200, {
      errcode: 0,
      errmsg: "成功",
      users
    });
    // get请求
    mock.onGet("/mockapi/gets").reply(200, {
      errcode: 0,
      errmsg: "成功",
      users
    });
    // 已对接对接部分API过滤
    mock.onAny().passThrough();
  }
};
