import Vue from "vue";
import VueRouter from "vue-router";

// 根据配置加载指定模块
const THIS_APP_MODULES = JSON.parse(process.env.VUE_APP_MODULES);
// 公共路由
THIS_APP_MODULES.push("commonPage");
THIS_APP_MODULES.push("debug");
let serviceRoutes = [];
THIS_APP_MODULES.forEach(element => {
  // console.log(require("../views/" + element + "/config/router.js"));
  serviceRoutes = serviceRoutes.concat(
    require("../views/" + element + "/config/router.js").default
  );
});

const prjIndexpath = "/" + process.env.VUE_APP_PRJ_NAME;

Vue.use(VueRouter);

const originalPush = VueRouter.prototype.push;
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

let routes = [
  {
    path: "/",
    redirect: prjIndexpath
  },
  // test路由-head
  {
    path: "/test",
    component: () => import("../views/nhwx/process/components/head.vue"),
    meta: {
      title: "test路由-head"
    }
  }
];
routes = routes.concat(serviceRoutes);
console.log("当前配置路由:", routes);
const router = new VueRouter({
  routes
});

export default router;
