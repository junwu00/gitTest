import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "babel-polyfill";

import Vant from "vant";
import "vant/lib/index.css";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

import { _initVConsole } from "./assets/js/util";
import Common from "./assets/js/common";
import EXIF from 'exif-js'
Vue.use(EXIF)
Vue.prototype.EXIF = EXIF

// import echarts from "echarts";


console.log(
  "该项目没有配置对应的/interConfig/main.js和/interConfig/index.html文件，采用公共配置"
);

Vue.config.productionTip = false;
Vue.use(Vant);
Vue.use(ElementUI);
Vue.prototype.$common = Common;
// Vue.prototype.$echarts = echarts;

// 设置标题
router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

_initVConsole();
