// 左菜单缩放
const leftMenuZoomX = {
  state: () => ({
    leftMenuZoom: false
  }),
  getters: {
    leftMenuZoom(state) {
      return state.leftMenuZoom;
    }
  },
  mutations: {
    setLeftMenuZoom(state, data) {
      state.leftMenuZoom = data;
    }
  },
  actions: {}
};
export default leftMenuZoomX;
