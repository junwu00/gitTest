const rightMianWidthX = {
  state: () => ({
    // 右边宽度计算
    rightMianWidth: 0
  }),
  getters: {
    rightMianWidth(state) {
      return state.rightMianWidth;
    }
  },
  mutations: {
    setRightMianWidth(state, data) {
      state.rightMianWidth = data;
    }
  },
  actions: {}
};
export default rightMianWidthX;
