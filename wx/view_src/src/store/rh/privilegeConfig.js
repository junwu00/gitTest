import common from "../../assets/js/common";
const privilegeConfigX = {
  state: () => ({
    privilegeConfig: {}
  }),
  getters: {
    privilegeConfig(state) {
      return state.privilegeConfig;
    }
  },
  mutations: {
    setPrivilegeConfig(state, data) {
      state.privilegeConfig = data;
    }
  },
  actions: {
    // 获取权限
    fetchPrivilege({ commit }) {
      return new Promise(resolve => {
        common
          .http({
            method: "get",
            url: "/api/wx/permission/list_mine",
            noLoading: true
          })
          .then(res => {
            // 缓存权限
            commit("setPrivilegeConfig", res.data.data);
            // 缓存司机和合作单位权限
            let keys = res.data.data.keys;
            let dirverOrCoopUnit = false;
            if (
              keys.indexOf("H-dirver-all") >= 0 ||
              keys.indexOf("H-coop-unit-all") >= 0
            ) {
              dirverOrCoopUnit = true;
            }
            commit("setDirverOrCoopUnit", dirverOrCoopUnit);
            // 计算第一个访问的菜单（待完善，结合路由）
            // commit("setFirstPath", "/rhqy/home");
            resolve();
          });
      });
    }
  }
};

export default privilegeConfigX;
