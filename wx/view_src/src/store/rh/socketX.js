import io from "socket.io-client";
import { getToken } from "../../assets/js/util";

const socketX = {
  state: () => ({
    // socket消息队列,废弃，不同业务采用独立队列维护，样例socket*对应的数组
    socketMsgList: {},
    // socket 對象
    socketObj: undefined,
    // 业务DebugA socket队列
    socketDebugA: [],
    // 业务DebugB socket队列
    socketDebugB: [],
    // socket队列改变
    queueList: [],
    // 当前连接的socketId
    socketId: null,
    // 计入的房间队列
    joinRoomList: [],
    // socket connect 状态
    socketIsDisconnect: false,
    // socket 工作状态，标识部分情况完成后才能重新连接
    socketBsStatu: false
  }),
  getters: {
    socketMsgList(state) {
      return state.socketMsgList;
    },
    socketObj(state) {
      return state.socketObj;
    },
    socketDebugA(state) {
      return state.socketDebugA;
    },
    socketDebugB(state) {
      return state.socketDebugB;
    },
    queueList(state) {
      return state.queueList;
    },
    socketId(state) {
      return state.socketId;
    },
    joinRoomList(state) {
      return state.joinRoomList;
    },
    socketIsDisconnect(state) {
      return state.socketIsDisconnect;
    },
    socketBsStatu(state) {
      return state.socketBsStatu;
    }
  },
  mutations: {
    pushSocketMsgList(state, data) {
      state.socketMsgList.push(data);
    },
    // Socket发送数据
    sendSocket(state, sendData) {
      state.socketObj.emit(sendData.method, sendData.data);
    },
    // 设置SocketObj
    setSocketObj(state, socket) {
      state.socketObj = socket;
    },
    // 设置SocketOid
    setSocketId(state, id) {
      state.socketId = id;
    },
    // 设置房间列表
    setJoinRoomList(state, data) {
      state.joinRoomList.push(data);
    },
    // 关闭socket
    closeSocket(state) {
      state.socketObj.close();
    },
    // 设置状态
    setSocketIsDisconnect(state, data) {
      state.socketIsDisconnect = data;
    },
    // 设置socket 工作状态
    setSocketBsStatu(state, data) {
      state.socketBsStatu = data;
    }
  },
  actions: {
    initSocket({ commit }) {
      return new Promise(resolve => {
        const socket = io("", {
          path: "/sio",
          reconnectionDelayMax: 10000,
          transports: ["websocket"],
          query: {
            auth: getToken()
          }
        });
        socket.on("connect", () => {
          console.log("wb connect", socket.id, socket.connected);
          commit("setSocketId", socket.id);
          commit("setSocketIsDisconnect", true);
          resolve();
        });
        socket.on("disconnect", data => {
          // 应用页面监听该变量，变化时重新获取数据
          commit("setSocketIsDisconnect", false);
          console.error("connect，客户端断网", data);
        });
        socket.on("connect_error", data => {
          console.error("connect_error", data);
        });
        socket.on("agentStateChanged", data => {
          console.log("agentStateChanged", data);
        });
        commit("setSocketObj", socket);
      });
    },
    // 加入房间
    joinRoom({ commit, state }, roomName) {
      return new Promise(resolve => {
        state.socketObj.on("join-room", data => {
          commit("setJoinRoomList", data);
          if (data == roomName) {
            resolve();
          }
        });
        state.socketObj.emit("join-room", roomName);
      });
    },
    // 离开房间
    leaveRoom({ state }, roomName) {
      return new Promise(resolve => {
        state.socketObj.on("leave-room", data => {
          if (data == roomName) {
            resolve();
          }
        });
        state.socketObj.emit("leave-room", roomName);
      });
    }
  }
};

export default socketX;
