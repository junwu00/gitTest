const form = {
  namespaced: true, // 增加命名空间控制
  state: { formsetinst_ids: "" }, // 模块内的状态已经是嵌套的了，使用 `namespaced` 属性不会对其产生影响
  mutations: {
    setForm(state, id) {
      state.formsetinst_ids = id;
      console.log('缓存id',id)
    }
  },
  getters: {
    getFormInfo(state) {
      return state.formsetinst_ids;
    }
  }
};
export default form;
