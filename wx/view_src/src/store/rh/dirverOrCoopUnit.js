// 司机或者合作单位
const dirverOrCoopUnitX = {
  state: () => ({
    dirverOrCoopUnit: false
  }),
  getters: {
    dirverOrCoopUnit(state) {
      return state.dirverOrCoopUnit;
    }
  },
  mutations: {
    // 设置合作单位和司机权限
    setDirverOrCoopUnit(state, data) {
      state.dirverOrCoopUnit = data;
    }
  },
  actions: {}
};

export default dirverOrCoopUnitX;
