const condition = {
  namespaced: true, // 增加命名空间控制
  state: { condition: [], tableId: "" }, // 模块内的状态已经是嵌套的了，使用 `namespaced` 属性不会对其产生影响
  mutations: {
    setCon(state, data) {
      state.condition = data;
    },
    setTableid(state, id) {
      state.tableId = id;
    }
  }
};
export default condition;
