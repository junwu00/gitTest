// 登录后访问的第一个路由
const firstPathX = {
  state: () => ({
    firstPath: ""
  }),
  getters: {
    firstPath(state) {
      return state.firstPath;
    }
  },
  mutations: {
    setFirstPath(state, data) {
      state.firstPath = data;
    }
  },
  actions: {},
  modules: {}
};

export default firstPathX;
