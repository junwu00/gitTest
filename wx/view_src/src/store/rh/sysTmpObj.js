const sysTmpObjX = {
  state: () => ({
    // 系统临时变量,对象行使保存，设置值->获取值->清除值，支撑某些连贯操作存储数据
    sysTmpObj: {}
  }),
  getters: {
    sysTmpObj(state) {
      return state.sysTmpObj;
    }
  },
  mutations: {
    setSysTmpObj(state, data) {
      if (Object.keys(state.sysTmpObj).length > 0) {
        console.error(
          "vuex sysTmpObj 对象某次使用未释放，请在前一次使用后调用cleanSysTmpObj方法释放，当前不能调用"
        );
      } else {
        state.sysTmpObj = data;
      }
    },
    cleanSysTmpObj(state) {
      state.sysTmpObj = {};
    }
  },
  actions: {}
};

export default sysTmpObjX;
