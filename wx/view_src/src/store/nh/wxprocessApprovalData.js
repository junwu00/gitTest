// 管理员信息
const wxprocessApprovalData = {
  state: () => ({
    approvaFromData: ""
  }),
  getters: {
    approvaFromData(state) {
      return state.approvaFromData;
    }
  },
  mutations: {
    setApprovaFromData(state, data) {
      state.approvaFromData = data;
    }
  },
  actions: {}
};

export default wxprocessApprovalData;
