// 管理员信息
const adminInfoX = {
  state: () => ({
    adminInfo: {}
  }),
  getters: {
    adminInfo(state) {
      return state.adminInfo;
    }
  },
  mutations: {
    setAdminInfo(state, data) {
      state.adminInfo = data;
    }
  },
  actions: {}
};

export default adminInfoX;
