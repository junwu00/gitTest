const path = require("path");
const fs = require("fs");
console.log("编译项目名：", process.env.VUE_APP_PRJ_NAME);
const VUE_APP_PRJ_NAME = process.env.VUE_APP_PRJ_NAME;
const devProxyBase = "./src/projectConfig/devProxy/";
const enterPageBase = "./src/projectConfig/interConfig/";
// vant UI 样式文件路径
const vantmodifyVars =
  enterPageBase + VUE_APP_PRJ_NAME + "/vantThemeIndex.less";

// require(enterPageBase + VUE_APP_PRJ_NAME + "/main.js");

// 同步查找文件是否存在
const fileFile = dir => {
  try {
    fs.accessSync(dir, fs.constants.R_OK);
    return true;
  } catch (error) {
    return false;
  }
};

let entryVal = "./src/main.js";
if (fileFile(enterPageBase + VUE_APP_PRJ_NAME + "/main.js")) {
  entryVal = enterPageBase + VUE_APP_PRJ_NAME + "/main.js";
}
let templateVal = "./public/index.html";
if (fileFile(enterPageBase + VUE_APP_PRJ_NAME + "/index.html")) {
  templateVal = enterPageBase + VUE_APP_PRJ_NAME + "/index.html";
}

console.log("入口配置：" + entryVal, "模板配置：" + templateVal);

module.exports = {
  // 部署应用包时的基本 URL
  publicPath: process.env.VUE_APP_PUBLICPATH,
  // 生产环境构建文件的目录
  outputDir: process.env.VUE_APP_OUTPUTDIR,
  // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录
  assetsDir: "static",
  // 指定生成的 index.html 的输出路径 (相对于 outputDir)。也可以是一个绝对路径。
  indexPath: "index.html",
  // hash缓存
  filenameHashing: true,
  // 编译异常强制显示，减少生产错误
  lintOnSave: "default",
  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: false,
  productionSourceMap: false,
  pwa: {
    iconPaths: {
      favicon32: "favicon.ico",
      favicon16: "favicon.ico",
      appleTouchIcon: "favicon.ico",
      maskIcon: "favicon.ico",
      msTileImage: "favicon.ico"
    }
  },
  // 多页面入口配置,绝大多数项目使用单页面,参考https://cli.vuejs.org/zh/config/#pages
  pages: {
    index: {
      entry: entryVal,
      template: templateVal,
      filename: "index.html",
      chunks: ["chunk-vendors", "chunk-common", "index"],
      title: ""
    }
  },
  css: {
    loaderOptions: {
      less: fileFile(path.join(__dirname, vantmodifyVars))
        ? {
            // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
            modifyVars: {
              // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
              hack: `true; @import "${path.join(__dirname, vantmodifyVars)}";`
            }
          }
        : {}
    }
  },
  configureWebpack: config => {
    config.optimization = {};
    if(process.env.NODE_ENV == "development"){
      config.devtool= '#eval-source-map';
    }
    
  },
  devServer: require(devProxyBase + VUE_APP_PRJ_NAME + ".js")
};
