const p1 = new Promise(resolve => {
  setTimeout(() => {
    console.log(1);
    resolve(1);
  }, 5000);
});

const p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log(2);
    reject(2);
  }, 2000);
});

Promise.allSettled([p1, p2]).then(values => {
  console.log(values);
});
