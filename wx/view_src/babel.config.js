const plugins = [];
// plugins.push("transform-remove-console");
// plugins.push("transform-remove-debugger");
if (process.env.NODE_ENV === "production") {
  plugins.push(["transform-remove-console", { exclude: ["error", "warn"] }]);
  plugins.push("transform-remove-debugger");
}
module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: plugins
};
