<?php
/**
 * DES加密类
 *
 * @author JianMing Liang
 * @create 2016-01-28 11:33:45
 */
namespace package\crypto;

class DesPlus extends Des {
    // 密钥偏移量：0-15，一般更新密钥仅需调整该值
    public $DES_OFFSET = 0;

    /**
     * 初始化加密密钥偏移值
     *
     * @access public
     * @return void
     */
    public function __construct() {
        $conf = load_config($this -> _default_config_path);
        if (!is_array($conf) or empty($conf)) {
            to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
            return;
        }

        isset($conf['DES_KEY']) and $this -> DES_KEY = $conf['DES_KEY'];
        isset($conf['DES_IV']) and $this -> DES_IV = $conf['DES_IV'];
        isset($conf['DES_OFFSET']) and $this -> DES_OFFSET = $conf['DES_OFFSET'];
    }

    /**
     * 字符串加密
     *
     * @access public
     * @param string $str 需要加密的字符串
     * @param int $offset 偏移值，如果不设置，则使用全局指定的默认值
     * @return string
     */
    public function encode($str, $offset=null) {
        //return $str;
        $str = parent::encode($str);
        $str = $this -> key_offset($str, 'add', $offset);

        return $str;
    }

    /**
     * 字符串解密
     *
     * @access public
     * @param string $str 需要解密的字符串
     * @param int $offset 偏移值，如果不设置，则使用全局指定的默认值
     * @return string
     */
    public function decode($str, $offset=null) {
        //return $str;
        $ret = $this -> key_offset($str, $offset, 'back');
        $ret = parent::decode($ret);

        return $ret;
    }

    /**
     * 16进制字符串偏移
     *
     * @access private
     * @param string $hex_str 大写16进制字符串
     * @param string $type 偏移类型：add-增加偏移，back-恢复偏移
     * @param int $offset 偏移值，如果不设置，则使用全局指定的默认值
     * @return boolean
     */
    private function key_offset($hex_str, $type='add', $offset=null) {
        $offset === null and $offset = $this -> DES_OFFSET;
        $offset = intval($offset);
        $offset > 15 and $offset = $offset % 15;

        $count = 0;
        $length = strlen($hex_str);
        while($count < $length) {
            $dec = hexdec($hex_str[$count]);
            if ($type == 'add') {
                ($dec += $this -> DES_OFFSET) > 15 and $dec = $dec - 16;
            }else {
                ($dec -= $this -> DES_OFFSET) < 0 and $dec = $dec + 16;
            }
            $hex_str[$count] = strtoupper(dechex($dec));
            $count += 6;
        }

        return $hex_str;
    }
}

/* End of file cls_des.php */ 