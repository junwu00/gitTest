<?php
/**
 * DES加密类
 *
 * @author JianMing Liang
 * @create 2016-01-28 11:33:45
 */
namespace package\crypto;

class Des {
    // 默认配置名称（使用load_config加载）
    protected $_default_config_path = 'package/crypto/des';

    // des加密的两组密钥
    public $DES_KEY = null;
    public $DES_IV  = null;

    /**
     * 初始化
     *
     * @access public
     * @return void
     */
    public function __construct() {
        $conf = load_config($this -> _default_config_path);
        if (!is_array($conf) or empty($conf)) {
            to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
            return;
        }

        isset($conf['DES_KEY']) and $this -> DES_KEY = $conf['DES_KEY'];
        isset($conf['DES_IV']) and $this -> DES_IV = $conf['DES_IV'];
    }

    /** 
     * 字符串加密
     * 
     * @access public
     * @param string $name 变量名
     * @param string $str 需要加密的字符串
     * @return string
     */
    public function encode($str) {
        $ret = $this -> encrypt($str, $this -> DES_KEY, $this -> DES_IV);

        return $ret;
    }
    
    /** 
     * 字符串解密
     * 
     * @access public
     * @param string $str 需要解密的字符串
     * @return string
     */
    public function decode($str) {
        $ret = $this -> decrypt($str, $this -> DES_KEY, $this -> DES_IV);

        return $ret;
    }


    /** 
     * 加密，返回大写十六进制字符串
     * 
     * @access private
     * @param string $str
     * @param string $key
     * @param string $iv
     * @return string
     */
    private function encrypt($str, $key, $iv) {
        $key = $this -> bytes2str($key);
        $iv = $this -> bytes2str($iv);

        $size = mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_CBC);
        $str = $this -> pkcs5_pad($str, $size);
        $str = mcrypt_cbc(MCRYPT_DES, $key, $str, MCRYPT_ENCRYPT, $iv);
        
        $ret = strtoupper(bin2hex($str));

        return $ret;
    }

    /** 
     * 解密
     * 
     * @access private
     * @param string $str
     * @param string $key
     * @param string $iv
     * @return string
     */
    private function decrypt($str, $key, $iv) {
        $key = $this -> bytes2str($key);
        $iv = $this -> bytes2str($iv);

        $str_bin = @pack("H*", $str);

        $str = mcrypt_cbc(MCRYPT_DES, $key, $str_bin, MCRYPT_DECRYPT, $iv);

        $ret = $this -> pkcs5_unpad($str);
        return $ret;
    }
    
    private function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        $ret = $text . str_repeat(chr($pad), $pad);
        
        return $ret;
    }

    private function pkcs5_unpad($text) {
        $pad = ord($text{strlen($text) - 1});

        if($pad > strlen($text)) return false;

        if(strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
            return false;
        }

        $ret = substr($text, 0, - 1 * $pad);
        return $ret;
    }

    /**
     * 将字节型数组转换成字符串
     * 
     * @access private
     * @param array $bytes 字节型数组
     * @return string
     */
    private function bytes2str($bytes) {
        $ret = '';
        foreach ($bytes as $ch) {
            $ret .= chr($ch);
        }

        return $ret;
    }
}

/* End of file cls_des.php */ 