<?php
/**
 * 封装curl的http客户端操作类
 *
 * @author JianMing Liang
 * @create 2016-01-27 16:04:29
 */
namespace package\net\http;

class CurlClient {
    /**
     * 执行get请求
     *
     * @access public
     * @param string $url 远程地址
     * @param array $options curl配置
     * @param integer $timeout 读取数据超时时间
     * @param integer $con_timeout 连接超时时间
     * @return array
     */
    public function get($url, array $options=array(), $time_out=30, $con_timeout=20) {
        $ret = array(
            //返回码：0 - client主动断开
            'httpcode' => 0,
            //返回内容
            'content'  => '',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $time_out);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        foreach ($options as $key => $val) {
            curl_setopt($ch, $key, $val);
        }
        unset($val);

        // 执行远程请求，并清除utf8的bom输出
        $ret['content'] = $this -> remove_utf8_bom(curl_exec($ch));
        // 执行之后才能获取状态码
        $ret['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $ret;
    }

    /**
     * 执行post请求
     *
     * @access public
     * @param string $url 远程地址
     * @param mixed $data 数据集合
     * @param array $options curl配置
     * @param integer $timeout 读取数据超时时间
     * @param integer $con_timeout 连接超时时间
     * @return array
     */
    public function post($url, $data=null, array $options=array(), $timeout=30, $con_timeout=20) {
        $ret = array(
            //返回码：0 - client主动断开
            'httpcode' => 0, 
            //返回内容
            'content'  => '', 
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        !empty($data) and curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        foreach ($options as $key => $val) {
            curl_setopt($ch, $key, $val);
        }
        unset($val);

        // 执行远程请求，并清除utf8的bom输出
        $ret['content']  = $this -> remove_utf8_bom(curl_exec($ch));
        //执行之后才能获取状态码
        $ret['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $ret;
    }

    /**
     * 清除utf-8的bom头
     *
     * @access public
     * @param string $text 字符串  
     * @return string
     */
    public function remove_utf8_bom($text) {
        $bom = pack('H*', 'EFBBBF');
        $text = preg_replace("/^{$bom}+?/", '', $text);

        return $text;
    }
}

/* End of this file */