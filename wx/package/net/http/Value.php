<?php
/**
 * $_GET、$_POST参数的统一操作类
 *
 * @author JianMing Liang
 * @create 2016-01-27 16:04:29
 */
namespace package\net\http;

class Value {
    // 使用XSS过滤
    const XSS_FILTER = 0b1;

    // get参数容器，默认值 $_GET
    private $_gets = null;
    // post参数容器，默认值 $_POST
    private $_posts = null;

    /**
     * 构造函数
     *
     * @access public
     * @return void
     */
    public function __construct() {
        $this -> _gets = $_GET;
        $this -> _posts = $_POST;
    }

    /**
     * 设置get参数容器
     *
     * @access public
     * @param array $arr 包含get参数的数组
     * @return boolean
     */
    public function set_gets(array $arr) {
        if (!is_array($arr)) return false;

        $this -> _gets = $arr;
        return true;
    }

    /**
     * 设置post参数容器
     *
     * @access public
     * @param array $arr 包含post参数的数组
     * @return boolean
     */
    public function set_posts(array $arr) {
        if (!is_array($arr)) return false;

        $this -> _posts = $arr;
        return true;
    }

    /**
     * 获取全部参数，包括 GET、POST参数
     *
     * @access public
     * @param bool $post_first 是否post参数优先
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @return array
     */
    public function get_all($post_first=true, $use_trim=true) {
        $ret = array();

        foreach ($this -> _gets as $key => $val) {
            $ret[$key] = $use_trim ? trim($val) : $val;
        }

        foreach ($this -> _posts as $key => $val) {
            $ret[$key] = $use_trim ? trim($val) : $val;
        }

        return $ret;
    }

    /**
     * 获取单个GET参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @return mixed 不存在返回false，否则返回字符串
     */
    public function get_get($name, $use_trim=true) {
        $ret = false;
        $name = strval($name);
        
        if ($name === '') {
            return $ret;
        }

        if (!is_array($this -> _gets) or !isset($this -> _gets[$name])) {
            return $ret;
        }

        $ret = $this -> _gets[$name];
        $use_trim and $ret = trim($ret);

        return $ret;
    }

    /**
     * 获取单个POST参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @return mixed 不存在返回false，否则返回字符串
     */
    public function get_post($name, $use_trim=true) {
        $ret = false;
        $name = strval($name);
        
        if ($name === '') {
            return $ret;
        }

        if (!is_array($this -> _posts) or !isset($this -> _posts[$name])) {
            return $ret;
        }

        $ret = $this -> _posts[$name];
        $use_trim and $ret = trim($ret);

        return $ret;
    }

    /**
     * 获取单个参数，不论其是GET还是POST参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @param bool $post_first 是否post参数优先
     * @return mixed 不存在返回false，否则返回字符串
     */
    public function get_value($name, $use_trim=true, $post_first=true) {
        $ret = false;

        $func_1 = 'get_post';
        $func_2 = 'get_get';
        if (!$post_first) {
            // 交换两个变量
            $func_1 = $func_1 ^ $func_2;
            $func_2 = $func_2 ^ $func_1;
            $func_1 = $func_1 ^ $func_2;
        }

        $ret = $this -> $func_1($name);
        $ret === false and $ret = $this -> $func_2($name);
        $ret !== false and $use_trim and $ret = trim($ret);
        
        return $ret;
    }

    /**
     * 安全地获取单个GET参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @param int $filter 过滤器集合
     * @return mixed 如果该值不存在，则返回false
     */
    public function safe_get_get($name, $use_trim=true, $filter=self::XSS_FILTER) {
        $val = $this -> get_get($name, $use_trim);
        $funcs = $this -> prase_filter($filter);

        if (!empty($funcs)) {
            foreach ($funcs as $func) {
                $val = $this -> $func($val);
            }
        }

        return $val;
    }

    /**
     * 安全地获取单个POST参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @param int $filter 过滤器集合
     * @return mixed 如果该值不存在，则返回false
     */
    public function safe_get_post($name, $use_trim=true, $filter=self::XSS_FILTER) {
        $val = $this -> get_post($name, $use_trim);
        $funcs = $this -> prase_filter($filter);

        if (!empty($funcs)) {
            foreach ($funcs as $func) {
                $val = $this -> $func($val);
            }
        }

        return $val;
    }

    /**
     * 安全地获取单个参数，不论其是GET、POST参数
     *
     * @access public
     * @param string $name 参数名
     * @param bool $use_trim 是否使用trim清除两边空白字符
     * @param int $filter 过滤器集合
     * @param bool $post_first 是否post参数优先
     * @return mixed 如果该值不存在，则返回false
     */
    public function safe_get_value($name, $use_trim=true, $filter=self::XSS_FILTER, $post_first=true) {
        $val = $this -> get_value($name, $use_trim, $post_first);
        $funcs = $this -> prase_filter($filter);

        if (!empty($funcs)) {
            foreach ($funcs as $func) {
                $val = $this -> $func($val);
            }
        }

        return $val;
    }

    /**
     * 进行字符串的xss跨站攻击过滤
     *
     * @access public
     * @param string $str 需要过滤的字符串
     * @return string
     */
    public function xss_filter($str) {
        try {
            $conf = \HTMLPurifier_Config::createDefault();
            $ret = g('\\HTMLPurifier', array($conf)) -> purify($str);
        }catch(Exception $e) {
            to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
            // 过滤失败返回原值
            return $str;
        }

        return $ret;
    }

    /**
     * 解析指定的filter参数包含哪些过滤器
     *
     * @access private
     * @param int $filter 过滤器集合
     * @return array
     */
    private function prase_filter($filter) {
        $ret = array();

        // 如果 $filter 包含 self::XSS_FILTER，则与运算结果必定为 self::XSS_FILTER
        $tmp_xss = self::XSS_FILTER & $filter;
        if ($tmp_xss === self::XSS_FILTER) {
            $ret[] = 'xss_filter';
        }

        return $ret;
    }
}

/* End of this file */