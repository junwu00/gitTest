<?php
/** 
 * 签名类
 * 
 * @author LiangJianMing
 * @create 2014-07-04 
 * @version 1.0
 */
namespace package\net\http;
class Sign {
	/**
	 * 生成签名
	 *
	 * @access public
	 * @param array $data 参数集合
	 * @param string $su_key 加密后缀
	 * @return string
	 */
	public static function get_sign($data, $su_key){
		if (!is_array($data)) return '';
		ksort($data);
		$sign = '';
		foreach ($data as $key => $val) {
			$sign .= $key.$val;
		}
		unset($val);
		$sign .= $su_key;
		$sign = md5($sign);
		return $sign;
	}

	/**
	 * 检验签名合法性
	 *
	 * @access public
	 * @param array $data 参数集合
	 * @param string $sign 签名
	 * @return boolean
	 */
	public static function check_sign($data, $sign, $su_key){
		$this_sign = self::get_sign($data, $su_key);
		return $this_sign === $sign ? TRUE : FALSE;
	}
}

/* End of this file */