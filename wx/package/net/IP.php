<?php
/**
 * IP信息操作类
 *
 * @author yangpz
 * @create 2016-02-19
 */
namespace package\net;

class IP {

	/** 
	 * 将长整型转换成ip地址
	 * 
	 * @param integer $ip_long 需要转换的整型数
	 * @return string
	 */
	public function long_ip($ip_long){
	    return long2ip($ip_long);
	}

	/** 
	 * 将ip地址转换成长整型
	 * 
	 * @param string $ip_string 需要转换的ip地址
	 * @return integer
	 */
	public function ip_long($ip_string){
	    $return = 0;
	    $tmp_array = explode('.', $ip_string);
	    foreach($tmp_array as $key => $val) {
	    	$return += intval($val) * pow(256, abs($key-3));
	    }
	    return $return;
	}
	
	/** 
	 * 获取有效的客户端IP地址
	 * @return string
	 */
	public function get_ip(){
	    static $ip = NULL;
	    if ($ip !== NULL) return $ip;
	
	    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	        is_array($ip) and $ip = array_shift($ip);
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    if (!empty($_SERVER['REMOTE_ADDR'])) {
	        $ip = $_SERVER['REMOTE_ADDR'];
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    $ip = '';
		return $ip;
	}
	
	/** 
	 * 获取请求者的ip地址，
	 * 如果使用代理服务，这里将是代理地址
	 * 
	 * @return mixed 成功返回string, 否则返回FALSE
	 */
	public function get_remote_ip(){
	    $ip = FALSE;
	
	    !empty($_SERVER['REMOTE_ADDR']) and $ip = $_SERVER['REMOTE_ADDR'];
	    !check_data($ip, 'ip') && $ip = FALSE;
	
	    return $ip;
	}
}

/* End of this file */