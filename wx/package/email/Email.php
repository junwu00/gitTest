<?php

namespace package\email;

class Email {
	/**
	 * 发送邮件
	 * @param unknown_type $service	        发件邮箱服务器信息
	 * @param unknown_type $from_user		发件人信息
	 * @param unknown_type $cc_user		    抄送人信息
	 * @param unknown_type $bcc_user		密送人信息
	 * @param unknown_type $subject	     	主题
	 * @param unknown_type $content	     	内容
	 */
	public function send($service, $from_user, $to_user,$cc_user,$bcc_user,$subject,$content,$anexfiles=false,$messageID=false){
		
		if(!$service||!$from_user||!$content)
		   return false;

		$mail = new PHPMailer(); 
		
		$mail -> IsHTML(TRUE); 	 									            //是否支持HTML邮件
		$mail->SMTPAuth = false;
		// $mail->SMTPDebug  = 1;													//是否显示调试信息
												
		$mail -> WordWrap 	= 50; 												// set word wrap 
		$mail -> CharSet 	= 'UTF-8';											//字符设置
		$mail -> Encoding 	= 'base64';											//编码方式
		$mail -> Port       = $service['port'];
		$mail -> SMTPSecure = $service['ssl']==1?'ssl':'';
		$mail -> IsSMTP(); 														
		$mail -> Host 		= $service['send_service']; 
		$mail -> SMTPAuth 	= TRUE; 											// turn on SMTP authentication 
		$mail -> Username 	= $from_user['email']; 			
		$mail -> Password 	= $from_user['password'];	
		$mail -> From 		= $from_user['email'];
		$mail -> FromName 	= $from_user['name'];
		if($to_user)															//添加接收人
		foreach($to_user as $t_user){
		   $mail -> AddAddress($t_user['email'], $t_user['name']); 
		}
		
		if($cc_user)															//添加抄送人
	    foreach($cc_user as $c_user){
		   $mail -> addCC($c_user['email'], $c_user['name']); 
		}
		
		if($bcc_user)															//添加密送人
	    foreach($bcc_user as $b_user){
		   $mail -> addBCC($b_user['email'], $b_user['name']); 
		}
		
		if($anexfiles)															//添加附件
     	foreach($anexfiles as $anex){
		   $mail->AddAttachment($anex['path'],$anex['name']);
		}
		if(!$messageID)
			$messageID = time();
		$mail -> setMessageID($messageID);
		
		$mail -> Subject 	= $subject; 
		$mail -> Body 		= $content; 
		try{
			$mail -> Send();
			return true;		
		}catch(Exception $e){
		    return false;
		}
	}
	/*
	 * 验证邮箱账号密码
	 *  @param unknown_type $service 服务器信息
	 *  @param unknown_type $from_user 需要验证的账号信息
	 *  @return true/false
	 */
	public function test($service,$from_user){
	      try{
	        $mail = new PHPMailer(); 
	      	$mail -> IsHTML(TRUE); 	 									            //是否支持HTML邮件
			$mail->SMTPAuth = false;
													
			$mail -> WordWrap 	= 50; 												// set word wrap 
			$mail -> CharSet 	= 'UTF-8';											//字符设置
			$mail -> Encoding 	= 'base64';											//编码方式
			
			$mail -> IsSMTP(); 														
			$mail -> Host 		= $service['send_service']; 
			$mail -> SMTPAuth 	= TRUE; 											// turn on SMTP authentication 
			$mail -> Username 	= $from_user['email']; 			
			$mail -> Password 	= $from_user['password'];	
			$mail -> From 		= $from_user['email'];
			$mail -> FromName 	= $from_user['email'];
			return $mail -> smtpConnect();
	      }catch(Exception $e){
	      	return false;
	      }
	}
	
}  