<?php
/**
 * 二维码生成类
 * 
 * @author luja
 * @date 2017-02-13
 *
 */
namespace package\qrcode;

class Qrcode {
	
	/**
	 * 生成url二维码
	 * 
	 * @param unknown_type $url
	 * @param unknown_type $size	尺寸，默认8
	 * @param unknown_type $level	容错量，默认30%，L（QR_ECLEVEL_L，7%），M（QR_ECLEVEL_M，15%），Q（QR_ECLEVEL_Q，25%），H（QR_ECLEVEL_H，30%）
	 * QR_ECLEVEL_L = 0
	 * QR_ECLEVEL_M = 1
	 * QR_ECLEVEL_Q = 2
	 * QR_ECLEVEL_H = 3
	 */
	public function encode_url($url, $size=8, $level=3) {
		return \PHPQRCode\QRcode::png($url, false, $level, $size);
	}
	
}

// end of file