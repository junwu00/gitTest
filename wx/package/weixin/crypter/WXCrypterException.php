<?php

/**
 * 异常类
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\crypter;

class WXCrypterException extends \Exception {
	
	public function __construct($err_code=99999) {
		$msg = WXQYErrorCode::get_desc($err_code);
		$msg = $err_code.', '.$msg;
		
		to_log($GLOBALS['levels']['MAIN_LOG_WARN'], '', '异常: code='.$err_code.', msg='.$msg);
		to_log($GLOBALS['levels']['MAIN_LOG_WARN'], '', '异常明细: '.parent::getTraceAsString());
		
		parent::__construct($msg, $err_code);
	}
	
}

// end of file