<?php
/**
 * SHA1 class
 *
 * 计算公众平台的消息签名接口.
 */
namespace package\weixin\crypter;

class Sha1 {
	/**
	 * 用SHA1算法生成安全签名
	 * @param string $token 票据
	 * @param string $timestamp 时间戳
	 * @param string $nonce 随机字符串
	 * @param string $encrypt 密文消息
	 */
	public function get_sha1($token, $timestamp, $nonce, $encrypt_msg) {
		//排序
		try {
			$array = array($encrypt_msg, $token, $timestamp, $nonce);
			sort($array, SORT_STRING);
			$str = implode($array);
			return array(WXCrypterErrorCode::$OK, sha1($str));
			
		} catch (\Exception $e) {
			throw new WXCrypterException(WXCrypterErrorCode::$ComputeSignatureError);
		}
	}

}

// end of file