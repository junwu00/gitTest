<?php
namespace package\weixin\crypter;

/**
 * 微信消息加解密_错误码及描述
 * @author yangpz
 * @date 2016-02-15
 *
 */
class WXCrypterErrorCode {
	/** 验证Signature异常 */
	public static $ValidateSignatureError 	= -40001;		
	/** XML转换异常 */
	public static $ParseXmlError 			= -40002;		
	/** 计算Signature异常 */
	public static $ComputeSignatureError 	= -40003;		
	/** 无效的AES键 */
	public static $IllegalAesKey			= -40004;		
	/** 验证企业号Corpid异常 */
	public static $ValidateCorpidError 		= -40005;		
	/** AES加密异常 */
	public static $EncryptAESError 			= -40006;		
	/** AES解密异常 */
	public static $DecryptAESError 			= -40007;		
	/** 缓存异常 */
	public static $IllegalBuffer 			= -40008;		
	/** Base64加密异常 */
	public static $EncodeBase64Error 		= -40009;		
	/** Base64解密异常 */
	public static $DecodeBase64Error 		= -40010;		
	/** 解析XML异常 */
	public static $GenReturnXmlError 		= -40011;
			
	/** 成功 */
	public static $OK						= 0;			
	
	/** 自定义错误码 */
	private static $SelfErrorCode = array(
		0			=> '成功',
		-40001		=> '验证Signature异常',
		-40002		=> 'XML转换异常',
		-40003		=> '计算Signature异常',
		-40004		=> '无效的AES键',
		-40005		=> '验证企业号Corpid异常',
		-40006		=> 'AES加密异常',
		-40007		=> 'AES解密异常',
		-40008		=> '缓存异常',
		-40009		=> 'Base64加密异常',
		-40010		=> 'Base64解密异常',
		-40011		=> '解析XML异常',
	);

	/**
	 * 获取自定义错误码描述
	 * @param unknown_type $err_code	自定义错误码
	 */
	public static function get_desc($err_code) {
		if (isset(self::$SelfErrorCode[$err_code])) {
			return self::$SelfErrorCode[$err_code];
			
		} else {
			throw new \Exception('未找到错误码描述：err_code='.$err_code);
		}
	}
}

//end