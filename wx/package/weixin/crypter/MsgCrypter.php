<?php

/**
 * 1.第三方回复加密消息给公众平台；
 * 2.第三方收到公众平台发送的消息，验证消息的安全性，并对消息进行解密。
 */
namespace package\weixin\crypter;

class MsgCrypter {
	private $token;
	private $encoding_aes_key;
	private $corpid;

	/**
	 * 构造函数
	 * @param $token string 公众平台上，开发者设置的token
	 * @param $encoding_aes_key string 公众平台上，开发者设置的EncodingAESKey
	 * @param $corpid string 公众平台的Corpid
	 */
	public function __construct($token, $encoding_aes_key, $corpid) {
		$this -> token = $token;
		$this -> encoding_aes_key = $encoding_aes_key;
		$this -> corpid = $corpid;
	}
	
    /*
	*验证URL
    *@param $msg_signature: 签名串，对应URL参数的msg_signature
    *@param $timestamp: 	时间戳，对应URL参数的timestamp
    *@param $nonce: 		随机串，对应URL参数的nonce
    *@param $echo_str: 		随机串，对应URL参数的echostr
    *@param $reply_str: 	解密之后的echostr，当return返回0时有效
    *@return：成功0，失败返回对应的错误码
	*/
	public function verify_url($msg_signature, $timestamp, $nonce, $echo_str, &$reply_str) {
		if (strlen($this -> encoding_aes_key) != 43) {
			throw new WXCrypterException(WXCrypterErrorCode::$IllegalAesKey);
		}

		$pc = new Prpcrypt($this -> encoding_aes_key);
		//verify msg_signature
		$sha1 = new Sha1();
		$array = $sha1 -> get_sha1($this -> token, $timestamp, $nonce, $echo_str);

		$signature = $array[1];
		if ($signature != $msg_signature) {
			throw new WXCrypterException(WXCrypterErrorCode::$ValidateSignatureError);
		}

		$result = $pc -> decrypt($echo_str, $this -> corpid);
		$reply_str = $result[1];

		return WXCrypterErrorCode::$OK;
	}
	
	/**
	 * 将公众平台回复用户的消息加密打包.
	 * <ol>
	 *    <li>对要发送的消息进行AES-CBC加密</li>
	 *    <li>生成安全签名</li>
	 *    <li>将消息密文和安全签名打包成xml格式</li>
	 * </ol>
	 *
	 * @param $reply_msg string 公众平台待回复用户的消息，xml格式的字符串
	 * @param $timestamp string 时间戳，可以自己生成，也可以用URL参数的timestamp
	 * @param $nonce string 随机串，可以自己生成，也可以用URL参数的nonce
	 * @param &$encrypt_msg string 加密后的可以直接回复用户的密文，包括msg_signature, timestamp, nonce, encrypt的xml格式的字符串,
	 *                      当return返回0时有效
	 *
	 * @return int 成功0，失败返回对应的错误码
	 */
	public function encrypt_msg($reply_msg, $timestamp, $nonce, &$encrypt_msg) {
		$pc = new Prpcrypt($this -> encoding_aes_key);

		//加密
		$array = $pc -> encrypt($reply_msg, $this -> corpid);

		if ($timestamp == null) {
			$timestamp = time();
		}
		$encrypt = $array[1];

		//生成安全签名
		$sha1 = new Sha1();
		$array = $sha1 -> get_sha1($this -> token, $timestamp, $nonce, $encrypt);
		$signature = $array[1];

		//生成发送的xml
		$xmlparse = new XmlParse();
		$encrypt_msg = $xmlparse -> generate($encrypt, $signature, $timestamp, $nonce);
		return WXCrypterErrorCode::$OK;
	}


	/**
	 * 检验消息的真实性，并且获取解密后的明文.
	 * <ol>
	 *    <li>利用收到的密文生成安全签名，进行签名验证</li>
	 *    <li>若验证通过，则提取xml中的加密消息</li>
	 *    <li>对消息进行解密</li>
	 * </ol>
	 *
	 * @param decrypt_msg string 签名串，对应URL参数的msg_signature
	 * @param $timestamp string 时间戳 对应URL参数的timestamp
	 * @param $nonce string 随机串，对应URL参数的nonce
	 * @param $post_data string 密文，对应POST请求的数据
	 * @param &$msg string 解密后的原文，当return返回0时有效
	 *
	 * @return int 成功0，失败返回对应的错误码
	 */
	public function decrypt_msg($msg_signature, $timestamp = null, $nonce, $post_data, &$msg) {
		if (strlen($this -> encoding_aes_key) != 43) {
			throw new WXCrypterException(WXCrypterErrorCode::$IllegalAesKey);
		}

		$pc = new Prpcrypt($this -> encoding_aes_key);

		//提取密文
		$xmlparse = new XmlParse();
		$array = $xmlparse -> extract($post_data);

		if ($timestamp == null) {
			$timestamp = time();
		}

		$encrypt = $array[1];
		$touser_name = $array[2];

		//验证安全签名
		$sha1 = new Sha1();
		$array = $sha1 -> get_sha1($this -> token, $timestamp, $nonce, $encrypt);

		$signature = $array[1];
		if ($signature != $msg_signature) {
			throw new WXCrypterException(WXCrypterErrorCode::$ValidateSignatureError);
		}

		$result = $pc -> decrypt($encrypt, $this -> corpid);
		$msg = $result[1];

		return WXCrypterErrorCode::$OK;
	}

}

// end of file