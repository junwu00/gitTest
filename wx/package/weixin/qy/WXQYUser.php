<?php
/**
 * 微信企业号官方接口调用_成员管理
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYUser extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 创建成员，mobile,weixin_id, email三者不能同时为空<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id			对应管理端的账号，企业内必须唯一
	 * @param unknown_type $name
	 * @param unknown_type $department		如：array(1, 2);每个部门直属员工上限1000
	 * @param unknown_type $position
	 * @param unknown_type $mobile			企业内必须唯一
	 * @param unknown_type $gender			0男1女
	 * @param unknown_type $tel				
	 * @param unknown_type $email			企业内必须唯一
	 * @param unknown_type $weixin_id		企业内必须唯一
	 * @param unknown_type $ext_attr
	 */
	public function create_user($access_token, $user_id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr=array()) {
		parent::log_info("开始创建成员: id={$user_id}, dept=".json_encode($department));
		$depts = array_values($department);
		$depts = intval_array($depts);
		$ext = array('attrs' => $ext_attr);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={$access_token}";
		
		$data = array(
			'userid' 		=> $user_id,
			'name' 			=> $name,
			'department' 	=> $depts,
			'position' 		=> $position,
			'mobile' 		=> $mobile,
			'gender' 		=> isset($gender) ? $gender : 0,
			'tel' 			=> $tel,
			'email' 		=> $email,
			'weixinid' 		=> $weixin_id,
			'extattr' 		=> json_encode($ext),
		);
		try {
			$ret = parent::post($url, $data);
			parent::log_info("创建成员成功: id={$user_id}, dept=".json_encode($department));
			return $user_id;
			
		} catch (\Exception $e) {
	        parent::log_info('创建成员失败：'.$e -> getMessage());
	        parent::log_error('创建成员失败：'.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 更新成员信息<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门、成员的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 * @param unknown_type $name
	 * @param unknown_type $department
	 * @param unknown_type $position
	 * @param unknown_type $mobile
	 * @param unknown_type $gender
	 * @param unknown_type $tel
	 * @param unknown_type $email
	 * @param unknown_type $weixin_id
	 * @param unknown_type $ext_attr
	 * @param unknown_type $enable			0禁用1启用
	 */
	public function update_user($access_token, $user_id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr=array(), $enable=1) {
		parent::log_info("开始更新成员: id={$user_id}, dept=".json_encode($department));
		$depts = array_values($department);
		$depts = intval_array($depts);
		$ext = array('attrs' => $ext_attr);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={$access_token}";
		
		$data = array(
			'userid' 		=> $user_id,
			'name' 			=> $name,
			'department' 	=> $depts,
			'position' 		=> $position,
			'mobile' 		=> $mobile,
			'gender' 		=> isset($gender) ? $gender : 0,
			'tel' 			=> $tel,
			'email' 		=> $email,
			'weixinid' 		=> $weixin_id,
			'enable' 		=> $enable,
			'extattr' 		=> json_encode($ext),
		);
		try {
			$ret = parent::post($url, $data);
			parent::log_info("更新成员成功: id={$user_id}, dept=".json_encode($department));
			return $user_id;
			
		} catch (\Exception $e) {
			parent::log_info("更新成员失败: ".$e -> getMessage());
			parent::log_error("更新成员失败: ".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 删除成员<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门、成员的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 */
	public function delete_user($access_token, $user_id) {
		parent::log_info("开始删除成员：id={$user_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={$access_token}&userid={$user_id}";
		try {
			$ret = parent::get($url);
			parent::log_info("删除成员成功：id={$user_id}");
			return $user_id;
			
		} catch (\Exception $e) {
			parent::log_info("删除成员失败: ".$e -> getMessage());
			parent::log_error("删除成员失败: ".$e -> getMessage());
	        throw $e;
		}
	}
	
	/**
	 * 批量删除成员<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门、成员的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 */
	public function batch_delete_user($access_token, array $user_id_array) {
		parent::log_info("开始批量删除成员：id=".json_encode($user_id_array));
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token={$access_token}";
		$data = array(
			'useridlist' => $user_id_array
		);
		try {
			$ret = parent::post($url, $data);
			parent::log_info("批量删除成员成功：id=".json_encode($user_id_array));
			return $user_id_array;
			
		} catch (\Exception $e) {
			parent::log_info("批量删除成员失败: ".$e -> getMessage());
			parent::log_error("批量删除成员失败: ".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 获取单个成员详细信息<br>
	 * 权限说明：管理员须拥有’获取成员’的接口权限，以及成员的查看权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 * @return 成员信息
	 */
	public function get_user($access_token, $user_id) {
		parent::log_info("开始获取成员信息：id={$user_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={$access_token}&userid={$user_id}";
		try {
			$data = parent::get($url);
			parent::log_info("获取成员信息成功：id={$user_id}");
			parent::log_debug($data);
			return $data;
			
		} catch (\Exception $e) {
			parent::log_info("获取成员信息失败：".$e -> getMessage());
			parent::log_error("获取成员信息失败：".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 获取部门下成员信息<br>
	 * 权限说明：管理员须拥有’获取部门成员’的接口权限，以及指定部门的查看权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $department_id	
	 * @param unknown_type $fetch_child		1/0：是否递归获取子部门下面的成员
	 * @param unknown_type $status			0获取全部员工，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加
	 * @return 部门成员信息(只有userid, name)
	 */
	public function list_user_by_dept($access_token, $department_id, $fetch_child=0, $status=0) {
		parent::log_info("开始获取部门成员列表：dept_id={$department_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={$access_token}&department_id={$department_id}&fetch_child={$fetch_child}&status={$status}";
		try {
			$data = parent::get($url);
			parent::log_info("获取部门成员列表成功：dept_id={$department_id}");
			parent::log_debug("成员数量：".count($data['userlist']));
			parent::log_debug($data['userlist']);
			return $data['userlist'];
			
		} catch (\Exception $e) {
			parent::log_info("获取部门成员列表失败：".$e -> getMessage());
			parent::log_error("获取部门成员列表失败：".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 获取部门下成员信息(详细)
	 * 权限说明：管理员须拥有’获取部门成员’的接口权限，以及指定部门的查看权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $department_id	
	 * @param unknown_type $fetch_child		1/0：是否递归获取子部门下面的成员
	 * @param unknown_type $status			0获取全部员工，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加
	 * @return 部门成员信息(所有信息)
	 */
	public function list_user_message_by_dept($access_token, $department_id, $fetch_child=0, $status=0) {
		parent::log_info("开始获取部门成员列表[详细]：dept_id={$department_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={$access_token}&department_id={$department_id}&fetch_child={$fetch_child}&status={$status}";
		try {
			$data = parent::get($url);
			parent::log_info("获取部门成员列表[详细]成功：dept_id={$department_id}");
			parent::log_debug("成员数量：".count($data['userlist']));
			parent::log_debug($data['userlist']);
			return $data['userlist'];
			
		} catch (\Exception $e) {
			parent::log_info("获取部门成员列表[详细]失败：".$e -> getMessage());
			parent::log_error("获取部门成员列表[详细]失败：".$e -> getMessage());
			throw $e;
		}
	}	
}

//end