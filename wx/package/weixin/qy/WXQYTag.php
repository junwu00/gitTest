<?php
/**
 * 微信企业号官方接口调用_标签
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYTag extends WXQYBase {

	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 创建标签<br>
	 * 权限说明：无限定。
	 * @param unknown_type $access_token
	 * @param unknown_type $name			标签名称，不可与其他同组的标签重名，也不可与全局标签重名
	 */
	public function create_tag($access_token, $name) {
		parent::log_info('开始创建标签: tag_name='.$name);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token={$access_token}";
		$data = array(
			'tagname' => $name,
		);
		try {
			$result = parent::post($url, $data);
			parent::log_info('创建标签成功: tag_id='.$result['tagid']);
			return $result['tagid'];
			
		} catch (\Exception $e) {
			parent::log_info('创建标签失败: '.$e -> getMessage());
			parent::log_error('创建标签失败: '.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 更新标签名称<br>
	 * 权限说明：管理员必须是指定标签的创建者。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id			标签ID
	 * @param unknown_type $name			标签名称，不可与其他同组的标签重名，也不可与全局标签重名
	 */
	public function update_tag($access_token, $tag_id, $name) {
		parent::log_info('开始更新标签名称: tag_id='.$tag_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'tagname' => $name,
		);
		try {
			parent::post($url, $data);
			parent::log_info('更新标签名称成功: tag_id='.$tag_id);
			return $tag_id;
			
		} catch (\Exception $e) {
			parent::log_info('更新标签名称失败: '.$e -> getMessage());
			parent::log_error('更新标签名称失败: '.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 删除标签<br>
	 * 权限说明：管理员必须是指定标签的创建者，并且标签的成员列表为空。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 */
	public function delete_tag($access_token, $tag_id) {
		parent::log_info('开始删除标签：tag_id='.$tag_id);		
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token={$access_token}$tagid={$tag_id}";
		try {
			parent::get($url);
			parent::log_info('删除标签成功：tag_id='.$tag_id);		
			return $tag_id;
			
		} catch (\Exception $e) {
			parent::log_info('删除标签失败：'.$e -> getMessage());	
			parent::log_error('删除标签失败：'.$e -> getMessage());	
			throw $e;
		}
	}
	
	/**
	 * 获取标签成员<br>
	 * 权限说明：管理员须拥有“获取标签成员”的接口权限，标签须对管理员可见；返回列表仅包含管理员管辖范围的成员。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id		
	 * @return 成员列表
	 */
	public function list_user_by_tag($access_token, $tag_id) {
		parent::log_info('开始获取标签成员列表：tag_id='.$tag_id);		
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token={$access_token}$tagid={$tag_id}";
		try {
			$data = parent::get($url);
			parent::log_info('获取标签成员列表成功：tag_id='.$tag_id);	
			parent::log_debug('成员总数：'.count($data['userlist']));	
			parent::log_debug($data['userlist']);	
			return $data['userlist'];
			
		} catch (\Exception $e) {
			parent::log_info('获取标签成员列表失败：'.$e -> getMessage());	
			parent::log_error('获取标签成员列表失败：'.$e -> getMessage());	
			throw $e;
		}
	}
	
	/**
	 * 增加标签成员（列表）<br>
	 * 权限说明：标签对管理员可见且未加锁，成员属于管理员管辖范围。<br>
	 * a)完全正确<br>
	 * b)部分user_id非法<br>
	 * c)全部user_id非法
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 * @param unknown_type $user_list		如：array('user1_id', 'user2_id');
	 * @return 非法user_id字符串
	 */
	public function add_tag_user_list($access_token, $tag_id, $user_list) {
		parent::log_info('开始增加标签成员：tag_id='.$tag_id);
		$users = array_values($user_list);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'userlist' => json_encode($users),
		);
		try {
			$result = parent::post($url, $data);
			if (isset($result['invalidlist'])) {
				parent::log_info('部分user_id非法：'.$result['invalidlist']);
				return $result['invalidlist'];
				
			} else {
				parent::log_info('全部增加成功：'.$result['invalidlist']);
				return $user_list;
			}
			
		} catch (\Exception $e) {
			parent::log_info('增加标签成员失败：'.$e -> getMessage());
			parent::log_error('增加标签成员失败：'.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 删除标签成员（列表）
	 * 权限说明：标签对管理员可见且未加锁，成员属于管理员管辖范围。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 * @param unknown_type $user_list		如：array('user1_id', 'user2_id');
	 * @return 非法user_id字符串
	 */
	public function del_tag_user($access_token, $tag_id, $user_list) {
		parent::log_info('开始删除标签成员：tag_id='.$tag_id);
		$users = array_values($user_list);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'userlist' => json_encode($users),
		);
		try {
			$result = parent::post($url, $data);
			if (isset($result['invalidlist'])) {
				parent::log_info('部分user_id非法：'.$result['invalidlist']);
				return $result['invalidlist'];
				
			} else {
				parent::log_info('全部删除成功');
				return $user_list;
			}
			
		} catch (\Exception $e) {
			parent::log_info('删除标签成员失败：'.$e -> getMessage());
			parent::log_error('删除标签成员失败：'.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 获取标签列表
	 * 权限说明：标签对管理员可见
	 * @param unknown_type $access_token
	 * @return 标签列表，含tagid, tagname
	 */
	public function list_tag($access_token) {
		parent::log_info('开始获取标签列表');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token={$access_token}";
		try {
			$data = parent::get($url);
			parent::log_info('获取标签列表成功');
			parent::log_debug('标签总数量： '.count($data['taglist']));
			parent::log_debug($data['taglist']);
			return $data['taglist'];
			
		} catch (\Exception $e) {
			parent::log_info('获取标签列表失败: '.$e -> getMessage());
			parent::log_error('获取标签列表失败: '.$e -> getMessage());
			throw $e;
		}
	}
		
}

//end