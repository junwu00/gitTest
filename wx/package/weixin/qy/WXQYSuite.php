<?php
/**
 * 微信企业号官方接口调用_套件/应用管理
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYSuite extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}

	/**
	 * 更新套件令牌
	 * @param unknown_type $suite_id     套件ID
	 * @param unknown_type $suite_secret 套件secret
	 * @param unknown_type $suite_ticket 套件ticket
	 */
	public function update_suite_token($suite_id, $suite_secret, $suite_ticket) {
		parent::log_info("开始更新套件令牌: suite_id={$suite_id}");
		try {
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token';
			$data = array(
				'suite_id' 		=> $suite_id,
				'suite_secret'	=> $suite_secret,
				'suite_ticket'	=> $suite_ticket
			);
			$result = parent::post($url, $data);
			parent::log_info("更新套件令牌成功: suite_id={$suite_id}, token={$result['suite_access_token']}");
			return $result['suite_access_token'];
			
		} catch(\Exception $e) {
			parent::log_info('更新套件令牌失败: '.$e -> getMessage());
			parent::log_error('更新套件令牌失败: '.$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 获取预授权码
	 * @param unknown_type $suite_id	套件ID
	 * @param unknown_type $suite_token	套件令牌
	 * @param unknown_type $appid		套件需要授权的应用的ID数组，默认为所有（本参数已废弃）
	 */
	public function get_pre_auth_code($suite_id, $suite_token, $appid=array()) {
		parent::log_info('开始获取预授权码: app_ids='.json_encode($appid));
		try {	
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token='.$suite_token;
			$data = array(
				'suite_id' 	=> $suite_id,
//				'appid'		=> $appid
			);
			$result = parent::post($url, $data);
			parent::log_info('获取预授权码成功: app_ids='.json_encode($appid).', pre_auth_code='.$result['pre_auth_code']);
			return $result['pre_auth_code'];
			
		} catch(\Exception $e) {
			parent::log_info('获取预授权码失败: '.$e -> getMessage());
			parent::log_error('获取预授权码失败: '.$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 开始设置安装应用
	 * @param unknown_type $suite_id	套件ID
	 * @param unknown_type $suite_token	套件令牌
	 * @param unknown_type $appid		套件需要授权的应用的ID数组，默认为所有（本参数已废弃）
	 * @throws SCException
	 */
	public function set_intalling_app($pre_auth_code, $suite_token, $appid=array()) {
		parent::log_info('开始设置正在安装的应用: app_ids='.json_encode($appid));
		try {	
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token='.$suite_token;
			$session_info = array(
				'appid' => $appid
			);
			$data = array(
				'pre_auth_code' => $pre_auth_code,
				'session_info'	=> $session_info,
			);
			parent::post($url, $data);
			parent::log_info('设置正在安装的应用成功: app_ids='.json_encode($appid));
			return $appid;
			
		} catch(\Exception $e) {
			parent::log_info('设置正在安装的应用失败: '.$e -> getMessage());
			parent::log_error('设置正在安装的应用失败: '.$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 获取企业号永久授权码（包含企业信息，企业授权应用信息）
	 * @param unknown_type $suite_id	套件ID
	 * @param unknown_type $suite_token	套件令牌说
	 * @param unknown_type $auth_token	临时授权码（由企业确认授权之后，回调获得）
	 * @throws SCException
	 */
	public function get_permanent_code($suite_id, $suite_token, $auth_token) {
		parent::log_info("开始获取企业永久授权码及企业信息: suite_id={$suite_id}");
		try {	
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token='.$suite_token;
			$data = array(
				'suite_id' 	=> $suite_id,
				'auth_code'	=> $auth_token
			);
			$result = parent::post($url, $data);
			parent::log_info('获取永久授权码及企业信息成功');
			return $result;
			
		} catch(\Exception $e) {
			parent::log_info('获取永久授权码及企业信息失败: '.$e -> getMessage());
			parent::log_error('获取永久授权码及企业信息失败: '.$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 获取企业信息
	 * @param unknown_type $suite_id		套件ID
	 * @param unknown_type $suite_token		套件令牌
	 * @param unknown_type $auth_corpid		企业ID
	 * @param unknown_type $permanent_code	企业永久授权码
	 * @throws SCException
	 */
	public function get_auth_corp_info($suite_id, $suite_token, $auth_corpid, $permanent_code) { 
		parent::log_info("开始获取企业信息: suite_id={$suite_id}");
		try {	
			$url ="https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token={$suite_token}";
			$data = array(
				'suite_id' 			=> $suite_id,
				'auth_corpid'		=> $auth_corpid,
				'permanent_code'	=> $permanent_code
			);
			$result = parent::post($url, $data);
			parent::log_info('获取企业信息成功');
			parent::log_info($result);
			return $result;
			
		} catch(\Exception $e) {
			parent::log_info('开始获取企业信息失败: '.$e -> getMessage());
			parent::log_error('开始获取企业信息失败: '.$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 获取企业应用信息
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_token			套件令牌
	 * @param unknown_type $auth_corpid			企业ID
	 * @param unknown_type $permanent_code		企业永久授权码
	 * @param unknown_type $agentid				企业内应用ID
	 * @throws SCException
	 */
	public function get_auth_corp_agent($suite_id, $suite_token, $auth_corpid, $permanent_code, $agentid) {
		parent::log_info("开始获取企业应用信息: suite_id={$suite_id}");
		try {	
			$url = "https://qyapi.weixin.qq.com/cgi-bin/service/get_agent?suite_access_token={$suite_token}";
			$data = array(
				'suite_id' 			=> $suite_id,
				'auth_corpid'		=> $auth_corpid,
				'permanent_code'	=> $permanent_code,
				'agentid' 			=> $agentid
			);
			$result = parent::post($url, $data);
			parent::log_info("获取企业应用信息成功");
			parent::log_debug($result);
			return $result;
			
		} catch(\Exception $e) {
			parent::log_info("获取企业应用信息失败: ".$e -> getMessage());
			parent::log_error("获取企业应用信息失败: ".$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 设置企业应用信息
	 * @param unknown_type $suite_id		套件ID
	 * @param unknown_type $suite_token		套件令牌
	 * @param unknown_type $auth_corpid		企业ID
	 * @param unknown_type $permanent_code	企业永久授权码
	 * @param unknown_type $agent			企业内应用信息
	 * @throws SCException
	 */
	public function set_auth_corp_agent($suite_id, $suite_token, $auth_corpid, $permanent_code, $agent) {
		parent::log_info("设置企业应用信息: suite_id={$suite_id}");
		try {	
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/set_agent?suite_access_token='.$suite_token;
			$data = array(
				'suite_id' 			=> $suite_id, 
				'auth_corpid'		=> $auth_corpid,
				'permanent_code'	=> $permanent_code,
				'agent' 			=> $agent
			);
			$result = parent::post($url, $data);
			parent::log_info("设置企业应用信息成功");
			return TRUE;
			
		} catch(\Exception $e) {
			parent::log_info("设置企业应用信息失败: ".$e -> getMessage());
			parent::log_error("设置企业应用信息失败: ".$e -> getMessage());
			throw $e;
		}
	}

	/**
	 * 更新企业号access_token
	 * @param unknown_type $suite_id		套件ID
	 * @param unknown_type $suite_token		套件令牌
	 * @param unknown_type $auth_corpid		企业ID
	 * @param unknown_type $permanent_code	企业永久授权码
	 * @throws SCException
	 */
	public function update_access_token($suite_id, $suite_token, $auth_corpid, $permanent_code) {
		parent::log_info("更新企业号access_token: suite_id={$suite_id}");
		try {	
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token='.$suite_token;
			$data = array(
				'suite_id' 			=> $suite_id,
				'auth_corpid'		=> $auth_corpid,
				'permanent_code'	=> $permanent_code
			);
			$result = parent::post($url, $data);
			parent::log_info("更新企业号access_token成功: suite_id={$suite_id}, token={$result['access_token']}");
			return $result['access_token'];
			
		} catch(\Exception $e) {
			parent::log_info("更新企业号access_token失败: ".$e -> getMessage());
			parent::log_error("更新企业号access_token失败: ".$e -> getMessage());
			throw $e;
		}
	}
}

//end