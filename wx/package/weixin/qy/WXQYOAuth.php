<?php
/**
 * 微信企业号官方接口调用_身份验证
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYOAuth extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 获取二次验证授权的url，页面将跳转至 redirect_uri/?code=CODE&state=STATE，企业可根据code参数获得员工的userid。
	 * @param unknown_type $corpid			企业号管理员组的corp id
	 * @param unknown_type $redirect_url	目标页面（用于获取code）
	 */
	public function get_oauth_url($corpid, $redirect_url) {
		$redirect_url = urlencode(urlencode($redirect_url));
		$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$corpid}&redirect_uri={$redirect_url}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
		return $url;
	}

	/**
	 * 获取授权用户的user_id
	 * 权限说明：管理员须拥有agent的使用权限；agentid必须和跳转链接时所在的企业应用ID相同。
	 * @param unknown_type $access_token
	 * @param unknown_type $code
	 * @param unknown_type $agent_id		跳转链接时所在的企业应用ID，默认为0：企业小助手
	 * @return user_id
	 */
	public function get_oauth_user_id($access_token, $code, $agent_id=0) {
		if (empty($code)) {
			throw new WXQYException(WXQYErrorCode::$OauthCodeNotFound);
		}
		
		parent::log_info("开始获取授权用户的user_id: agent_id={$agent_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$access_token}&code={$code}&agentid={$agent_id}";
		
		try {
			$data = parent::get($url);
			$user_id = '';
			if(isset($data['UserId'])){
				$user_id = $data['UserId'];
			}
			parent::log_info("获取授权用户的user_id成功: agent_id={$agent_id}");
			parent::log_debug("user_id={$user_id}");
			return $user_id;
			
		} catch (\Exception $e) {
			parent::log_info("获取授权用户的user_id失败: ".$e -> getMessage());
			parent::log_error("获取授权用户的user_id失败: ".$e -> getMessage());
			throw $e;			
		}
	}
	
	/**
	 * 完成二次验证，成员关注成功
	 * 权限说明：管理员须拥有userid对应员工的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 */
	public function oauth_user($access_token, $user_id) {
		parent::log_info('开始完成二次验证');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token={$access_token}&userid={$user_id}";
		try {
			parent::get($url);
			parent::log_info('完成二次验证成功');
			return $user_id;
			
		} catch (\Exception $e) {
			parent::log_info('完成二次验证失败: '.$e -> getMessage());
			parent::log_error('完成二次验证失败: '.$e -> getMessage());
			throw $e;
		}
	}


    /**
     * userid转openid
     * @param $access_token
     * @param $user_id          成员账号
     * @param null $agent_id    整型，需要发送红包的应用ID，若只是使用微信支付和企业转账，则无需该参数
     * @return array
     * @throws \Exception
     *
     * @return
     * openid 企业号成员userid对应的openid，若有传参agentid，则是针对该agentid的openid。否则是针对企业号corpid的openid
     * appid 应用的appid，若请求包中不包含agentid则不返回appid。该appid在使用微信红包时会用到
     */
    public function convert_to_openid($access_token, $user_id, $agent_id=NULL) {
        parent::log_debug('开始userid转openid');
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token={$access_token}";
        try {
            $data = array(
                'userid'    => $user_id,
            );
            !is_null($agent_id) && $data['agentid'] = $agent_id;
            $ret = $this -> post($url, $data);
            parent::log_debug('userid转openid成功');
            return $ret;

        } catch (\Exception $e) {
            parent::log_debug('userid转openid失败: '.$e -> getMessage());
            parent::log_error('userid转openid失败: '.$e -> getMessage());
            throw $e;
        }
    }

    /**
     * openid转userid
     * @param $access_token
     * @param $openid           在使用微信支付、微信红包和企业转账之后，返回结果的openid
     * @throws \Exception
     *
     * @return
     * userid   该openid在企业号中对应的成员userid
     */
    public function convert_to_userid($access_token, $openid) {
        parent::log_debug('开始openid转userid');
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token={$access_token}";
        try {
            $data = array(
                'openid'    => $openid
            );
            $ret = $this -> post($url, $data);
            parent::log_debug('openid转userid成功');
            return $ret;

        } catch (\Exception $e) {
            parent::log_debug('openid转userid失败: '.$e -> getMessage());
            parent::log_error('openid转userid失败: '.$e -> getMessage());
            throw $e;
        }
    }

}

//end