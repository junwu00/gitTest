<?php
/**
 * 微信企业号官方接口调用_部门管理
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYDept extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 创建部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及父部门的管理权限。
	 * @param unknown_type $access_token		
	 * @param unknown_type $name			部门名称，1~64字符
	 * @param unknown_type $parent_id		上级部门ID，根部门ID为1
	 * @param unknown_type $order			在上级部门中的次序，从1开始，小号在前
	 */
	public function create_dept($access_token, $name, $parent_id, $order=1) {
		parent::log_info('开始创建部门：name='.$name.', wx_pid='.$parent_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$access_token}";
		$data = array(
			'name' 		=> $name,
			'parentid' 	=> $parent_id,
			'order'		=> $order,
		);
		try {
			$result = parent::post($url, $data);
			parent::log_info("创建部门成功: wx_id={$result['id']}, name={$name}, wx_pid={$parent_id}, order={$order}");
			return $result['id'];
			
		} catch (\Exception $e) {
			parent::log_info('创建部门失败: '.$e -> getMessage());
			parent::log_error('创建部门失败: '.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 更新部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及该部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $id				部门ID
	 * @param unknown_type $name
	 * @param unknown_type $parent_id
	 * @param unknown_type $order
	 */
	public function update_dept($access_token, $id, $name, $parent_id, $order) {
		parent::log_info('开始更新部门：name='.$name.', wx_id='.$id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={$access_token}";
		$data = array(
			'id' => $id,
			'name' => $name,
			'parentid' => $parent_id,
			'order' => $order,
		);
		try {
			parent::post($url, $data);
			parent::log_info("更新部门成功: wx_id={$id}, name={$name}, wx_pid={$parent_id}, order={$order}");
			return $id;
			
		} catch (\Exception $e) {
	        parent::log_info('更新部门失败: '.$e -> getMessage());
	        parent::log_error('更新部门失败: '.$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 删除部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及该部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $id			部门ID
	 */
	public function delete_dept($access_token, $id) {
		parent::log_info('开始删除部门：id='.$id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token={$access_token}&id={$id}";
		try {
			parent::get($url);
			parent::log_info("删除部门成功: wx_id={$id}");
			return $id;
			
		} catch (\Exception $e) {
	        parent::log_info("删除部门失败: wx_id={$id}");
	        parent::log_error("删除部门失败: wx_id={$id}");
			throw $e;
		}
	}
	
	/**
	 * 获取部门列表<br>
	 * 权限说明：管理员须拥有’获取部门列表’的接口权限，以及对部门的查看权限。
	 * @param unknown_type $access_token
	 * @return 部门列表
	 */
	public function list_dept($access_token) {
		parent::log_info('开始获取企业部门列表');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={$access_token}";
		try {
			$result = parent::get($url);
			parent::log_info('获取企业部门列表成功');
			parent::log_debug("部门总数: ".count($result['department']));
			parent::log_debug($result['department']);
			
			return $result['department'];
					
		} catch (\Exception $e) {
			parent::log_info('获取企业部门列表失败');
			parent::log_error('获取企业部门列表失败');
			throw $e;
		}
	}
	
}

//end