<?php
/**
 * 微信企业号官方接口调用_多媒体文件管理
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

use CURLFile;

class WXQYMedia extends WXQYBase {

	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 上传媒体文件<br>
	 * 
	 * 图片（image）:1MB，支持JPG格式
	 * 语音（voice）：2MB，播放长度不超过60s，支持AMR格式
	 * 视频（video）：10MB，支持MP4格式
	 * 普通文件（file）：10MB
	 * 
	 * @param unknown_type $access_token
	 * @param unknown_type|string $type			媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件(file)
	 * @param unknown_type|string $file_path		要上传的文件的路径
	 * @param unknown_type $file_path		原文件名
	 */
	public function upload_media($access_token, $type, $file_path, $file_name) {
		$url = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={$access_token}&type={$type}";
		try {
			return $this -> _post_media($url, $file_path, $file_name);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}

    /**
     * 拉取图片
     *
     * @param $access_token
     * @param $media_id
     * @return array
     * @throws \Exception
     */
	public function get_media($access_token, $media_id) {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token={$access_token}&media_id={$media_id}";
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $file = curl_exec($ch);
            $ret_info = curl_getinfo($ch);
            curl_close($ch);

            if ($ret_info['http_code'] != 200) {
                throw new \Exception('获取上传素材失败');
            }

            return array(
                'file'  => $file,
                'info'  => $ret_info,
            );

        } catch (\Exception $e) {
            throw $e;
        }
    }
	
	/**
	 * 上传文件到微信服务器<br>
	 * 为确认用户收到的文件名与上传时一致，采用以下方法<br>
	 * 1.先从本地复制文件，以用户上传时的文件名命名<br>
	 * 2.上传该临时文件到微信服务器<br>
	 * 3.删除该临时文件<br>
	 * @param unknown_type $url			POST请求的目标URL
	 * @param unknown_type $file_path	文件路径
	 * @param unknown_type $file_name	用户上传时自己命名的文件名
	 * @return 数组形式的结果
	 */
	private function _post_media($url, $file_path, $file_name) {
		$dirs = explode('/', $file_path);
		$ori_file_name = $dirs[count($dirs) - 1];
		$file_name = iconv('utf-8', 'gbk', $file_name);
		// $tmp_dir = get_rand_string(3);	//存储复制的文件的临时目录
		// $new_file_path = substr($file_path, 0, strlen($file_path) - strlen($ori_file_name)) . $tmp_dir . '/';
		$new_file_path = substr($file_path, 0, strlen($file_path) - strlen($ori_file_name));
		if (!file_exists($new_file_path)) {
			mkdir($new_file_path, 0777, TRUE);
		}
		$new_file_path .= $file_name;
		copy($file_path, $new_file_path);
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		if(defined('CURL_SSLVERSION_TLSv1')){
	    	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST,true);

		// curl_setopt($ch, CURLOPT_POSTFIELDS,
	    //     //1.文件路径之前必须要加@
	    //     //2.文件路径带中文就会失败，例如'img_1'=>'@C:\Documents and Settings\Administrator\桌面\Android壁纸\androids.gif'
	    //     array("media"=> "@".$new_file_path)
	    // );
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            array("media"=> new \CURLFile($new_file_path, '', $file_name))
        );
		$info = curl_exec($ch);
		
		file_exists($new_file_path) && file($new_file_path) && unlink($new_file_path);
		
		if (curl_errno($ch)) {
			parent::log_error('POST MEDIA请求失败:' .curl_error($ch));
			curl_close($ch);
			throw new WXQYException(WXQYErrorCode::$PostExp);
		}
		
		curl_close($ch);
		$info = json_decode($info, TRUE);
		if (isset($info['errcode']) && $info['errcode'] != 0) {
			throw new WXQYException($info['errcode']);
			
		} else {
			return $info;
		}
	}
	
}

//end