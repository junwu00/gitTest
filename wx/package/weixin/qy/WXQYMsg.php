<?php
/**
 * 微信企业号官方接口调用_消息回复/发送
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYMsg extends WXQYBase {
	private $app_token;			//应用token
	private $corpid;			
	private $aes_key;			//应用密钥
	
	/** 
	 * 文字消息模板: 1.ToUserName, 2.FromUserName, 3.CreateTime, 4.Content
	 */
	private static $TextTemp = '<xml>
	   <ToUserName><![CDATA[%s]]></ToUserName>
	   <FromUserName><![CDATA[%s]]></FromUserName> 
	   <CreateTime>%s</CreateTime>
	   <MsgType><![CDATA[text]]></MsgType>
	   <Content><![CDATA[%s]]></Content>
	</xml>';
	
	/** 
	 * 图文消息模板 : 1.ToUserName, 2.FromUserName, 3.CreateTime, 4.ArticleCount, 5.Articles
	 */
	private static $NewsTemp = '<xml>
	   <ToUserName><![CDATA[%s]]></ToUserName>
	   <FromUserName><![CDATA[%s]]></FromUserName>
	   <CreateTime>%s</CreateTime>
	   <MsgType><![CDATA[news]]></MsgType>
	   <ArticleCount>%d</ArticleCount>
	   <Articles>%s</Articles>
	</xml>';
	
	/** 
	 * 图文消息的消息项模板: 1.Title, 2.Description, 3.PicUrl, 4.Url
	 */
	private static $NewsItemTemp = '<item>
		<Title><![CDATA[%s]]></Title> 
		<Description><![CDATA[%s]]></Description>
		<PicUrl><![CDATA[%s]]></PicUrl>
		<Url><![CDATA[%s]]></Url>
	</item>';
	
	/** 消息加密/解密器 */
	private $msg_crypter;
	
	public function __construct(array $conf = array(), $log_pre_desc='') {
		if(!empty($conf)){
			$this -> corpid 	= $conf['corpid'];
			$this -> app_token 	= $conf['app_token'];
			$this -> aes_key 	= $conf['aes_key'];
			$this -> msg_crypter = new \package\weixin\crypter\MsgCrypter($this -> app_token, $this -> aes_key, $this -> corpid);
		}
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 解密收到的消息
	 * @param unknown_type $post_data		收到的消息（密文）
	 * @param unknown_type $msg_signature	
	 * @param unknown_type $timestamp	
	 * @param unknown_type $nonce
	 */
	public function decrypt_msg($post_data, $msg_signature, $timestamp, $nonce) {
		parent::log_debug("开始解密消息");
		parent::log_debug($this -> app_token.'--'.$this -> aes_key.'--'.$this -> corpid);
		$msg = '';
		try {
			$this -> msg_crypter -> decrypt_msg($msg_signature, $timestamp, $nonce, $post_data, $msg);
			parent::log_debug('解密消息结果:'.$msg);
			return $msg;
			
		} catch (\Exception $e) {
			parent::log_debug("解密消息失败: ".$e -> getMessage());
			parent::log_error("解密消息失败: ".$e -> getMessage());
			throw $e;			
		}
	}
	
	/**
	 * 加密要响应的消息
	 * @param unknown_type $reply_msg	要响应的消息（明文）
	 * @param unknown_type $timestamp
	 * @param unknown_type $nonce
	 */
	public function encrypt_msg($reply_msg, $timestamp=NULL, $nonce=NULL) {
		parent::log_debug('开始加密消息');
		empty($timestamp) && $timestamp = time();
		empty($nonce) && $nonce = get_rand_str(6);
		
		$msg = '';
		try {
			$result = $this -> msg_crypter -> encrypt_msg($reply_msg, $timestamp, $nonce, $msg);
			parent::log_debug('加密消息成功');
			return $msg;
			
		} catch (\Exception $e) {
			parent::log_debug('加密消息失败: '.$e -> getMessage());
			parent::log_error('加密消息失败: '.$e -> getMessage());
			throw $e;			
		}
	}

	/**
	 * 验证接入请求
	 * @param unknown_type $msg_signature
	 * @param unknown_type $timestamp
	 * @param unknown_type $nonce
	 * @param unknown_type $echo_str
	 */
	public function verify_url($msg_signature, $timestamp, $nonce, $echo_str) {
		parent::log_info('开始验证接入请求');
		$reply_str = '';
		$this -> msg_crypter -> verify_url($msg_signature, $timestamp, $nonce, $echo_str, $reply_str);
		parent::log_info('验证接入请求成功');
		return $reply_str;
	}
	
	//---------------------------------------------被动响应消息---------------------------------------

	/**
	 * 获取文本回复
	 * @param unknown_type $post_data	请求内容数组（明文）
	 * @param unknown_type $content		文本内容
	 */
	public function get_text_reply(array $post_data, $content) {
		$reply_msg = sprintf(self::$TextTemp, $post_data['FromUserName'], $post_data['ToUserName'], time(), $content);
		try {
			$msg = $this -> encrypt_msg($reply_msg);
			return $msg;
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取（单/多）图文回复
	 * @param unknown_type $post_data		请求内容数组（明文）
	 * @param unknown_type $article_list	图文数组，含title, desc, pic_url, url
	 */
	public function get_news_reply(array $post_data, $article_list) {
		$arts = '';
		foreach ($article_list as $art) {
			$art_item = sprintf(self::$NewsItemTemp, $art['title'], $art['desc'], $art['pic_url'], $art['url']);
			$arts .= $art_item;
		}unset($art);
		
		$reply_msg = sprintf(self::$NewsTemp, $post_data['FromUserName'], $post_data['ToUserName'], time(), count($article_list), $arts);
		try {
			$msg = $this -> encrypt_msg($reply_msg);
			return $msg;
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	//------------------------------------------发送消息-----------------------------------------

	/**
	 * 发送文本消息
	 * @param unknown_type $access_token
	 * @param unknown_type $text		要发送的文本信息
	 * @param unknown_type $user_list	UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
	 * @param unknown_type $agent_id	企业应用的id，整型
	 * @param unknown_type $tag_list	TagID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
	 * @param unknown_type $party_list	PartyID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
	 * @param unknown_type $is_safe		表示是否是保密消息，0表示否，1表示是，默认0
	 */
	public function send_text($access_token, $user_list, $agent_id, $text, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
				"touser" 	=> $users,
			   	"toparty" 	=> $partys,
			   	"totag" 	=> $tags,
			   	"msgtype" 	=> "text",
			   	"agentid" 	=> $agent_id,
			   	"text" 		=> array(
			    	"content" => $text,
				),
			   	"safe" 		=> $is_safe,
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;			
		}
	}
	
	/**
	 * 发送图片
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_img($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   	"touser" 	=> $users,
			   	"toparty" 	=> $partys,
			   	"totag" 	=> $tags,
			   	"msgtype" 	=> "image",
			   	"agentid" 	=> $agent_id,
			   	"image" 	=> array(
			       "media_id" => $media_id,
				),
			   	"safe" 		=> $is_safe,
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送音频
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_voice($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   	"touser" 	=> $users,
			   	"toparty" 	=> $partys,
			   	"totag" 	=> $tags,
			   	"msgtype" 	=> "voice",
			   	"agentid" 	=> $agent_id,
			  	"voice" 	=> array(
			       "media_id" => $media_id,
				),
			   	"safe" 		=> $is_safe,
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送视频
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $title
	 * @param unknown_type $desc
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_video($access_token, $user_list, $agent_id, $media_id, $title, $desc, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   	"touser" 	=> $users,
			   	"toparty" 	=> $partys,
			   	"totag" 	=> $tags,
			   	"msgtype" 	=> "video",
			   	"agentid" 	=> $agent_id,
			   	"video" 	=> array(
			  		"media_id" 		=> $media_id,
					"title" 		=> $title,
					"description" 	=> $desc,
				),
			   	"safe" 		=> $is_safe,
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送文件
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_file($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   	"touser" 	=> $users,
			   	"toparty" 	=> $partys,
			   	"totag" 	=> $tags,
			   	"msgtype" 	=> "file",
			   	"agentid" 	=> $agent_id,
			   	"file" 		=> array(
			       "media_id" => $media_id,
				),
			   	"safe" 		=> $is_safe,
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送图文消息
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $art_list
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 */
	public function send_news($access_token, $user_list, $agent_id, $art_list, $tag_list=array(), $party_list=array()) {
		try {
			$users = $this -> _verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$arts = array();
			foreach ($art_list as $art) {
				$item = array(
					'title' 		=> $art['title'],
					'description' 	=> $art['description'],
					'url' 			=> $art['url'],
					'picurl' 		=> $art['picurl'],
				);
				$arts[] = $item;
			}unset($art);
			
			$data = array(
		 		"touser" 	=> $users,
				"toparty" 	=> $partys,
			  	"totag" 	=> $tags,
			   	"msgtype" 	=> "news",
			   	"agentid" 	=> $agent_id,
			   	"news" 		=> array(
					"articles" => $arts,
				),
			);
			$this -> _send_msg($access_token, $data);
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	//内部实现-------------------------------------------------------------------------------
	
	/**
	 * 是否为有效的消息发送请求
	 * @param unknown_type $user_list	UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
	 * @param unknown_type $agent_id	企业应用的id，整型
	 */
	private function _verify_send_req($user_list, $agent_id) {
		if (is_null($agent_id) || !is_int(intval($agent_id))) {
			parent::log_error('应用ID找不到: '.$agent_id);
			throw new \Exception('应用ID找不到: '.$agent_id);
		}
		
		$users = implode('|', array_values($user_list));
		return $users;
	}

	/**
	 * 发送消息
	 * @param unknown_type $access_token
	 * @param unknown_type $data		要发送的消息
	 */
	private function _send_msg($access_token, $data) {
		parent::log_info('开始发送消息');
		parent::log_debug($data);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={$access_token}";
		try {
			parent::post($url, $data);
			parent::log_info('发送消息成功');
			return $data;
			
		} catch(\Exception $e) {
			parent::log_info('发送消息失败:'.$e -> getMessage());
			parent::log_error('发送消息失败:'.$e -> getMessage());
			throw $e;
		}
	}
	
}

//end