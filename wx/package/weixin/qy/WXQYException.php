<?php

/**
 * 异常类
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYException extends \Exception {
	
	public function __construct($err_code=99999) {
		$err_type = substr($err_code, 0, 1);
		if ($err_type == '4' || $err_type == '5' || $err_type == '6' || $err_type == '8' || $err_code == -1) {	//微信官方错误码
			$msg = WXQYErrorCode::get_wx_desc($err_code);
		} else {														//自定义错误码
			$msg = WXQYErrorCode::get_desc($err_code);
		}
		
		$msg = $err_code.', '.$msg;
		
		to_log($GLOBALS['levels']['MAIN_LOG_WARN'], '', '异常: code='.$err_code.', msg='.$msg);
		to_log($GLOBALS['levels']['MAIN_LOG_WARN'], '', '异常明细: '.parent::getTraceAsString());
		
		parent::__construct($msg, $err_code);
	}
	
}

// end of file