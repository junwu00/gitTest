<?php
/**
 * 微信企业号官方接口调用_卡卷管理（包含电子发票）
 * @author chenyh
 * @date 2017-03-02
 *
 */
namespace package\weixin\qy;

class WXQYCard extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}

	/**
	 * 获取企业ticket
	 * @param   $access_token 
	 * @return                
	 */
	public function get_ticket($access_token){
		parent::log_info("开始获取企业ticket");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token={$access_token}&type=wx_card";
		try{
			$data = parent::get($url);
			parent::log_info("获取ticket成功：".$data['ticket']);
			return $data;
		}catch(\Exception $e){
			parent::log_info("获取ticket失败");
			parent::log_error("获取ticket失败");
			throw $e;
		}
	}

	/**
	 * 获取电子发票信息
	 * @param   $access_token  企业access_token
	 * @param   $card_id       发票id
	 * @param   $encrypt_code  发票加密code
	 * @return                
	 */
	public function get_invoice_info($access_token, $card_id, $encrypt_code){
		$url = "https://qyapi.weixin.qq.com/cgi-bin/card/invoice/reimburse/getinvoiceinfo?access_token={$access_token}";
		try{
			$data = array(
					"card_id" 		=> $card_id,
					"encrypt_code" 	=> $encrypt_code
				);
			$result = parent::post($url, $data);
			parent::log_info("获取电子在发票信息成功");
			return $result;
		}catch(\Exception $e){
			parent::log_info("获取电子发票信息失败");
			parent::log_error("获取电子发票信息失败");
			throw $e;
		}
	}

	/**
	 * 更新电子发票状态
	 * @param   $access_token     企业access_token
	 * @param   $card_id          发票ID (由jssdk拉取发票获取)
	 * @param   $encrypt_code     发票加密code (由jssdk拉取发票获取)
	 * @param   $reimburse_status 发报销状态 INVOICE_REIMBURSE_INIT: 发票初始状态，未锁定; 
	 *                                     INVOICE_REIMBURSE_LOCK:  发票已锁定; 
	 *                                     INVOICE_REIMBURSE_CLOSURE: 发票已核销
	 * @return                    
	 */
	public function update_invoice_info($access_token, $card_id, $encrypt_code, $reimburse_status){
		$url = "https://qyapi.weixin.qq.com/cgi-bin/card/invoice/reimburse/updateinvoicestatus?access_token={$access_token}";
		try{
			$data = array(
					"card_id" 			=> $card_id,
					"encrypt_code" 		=> $encrypt_code,
					"reimburse_status" 	=> $reimburse_status
				);
			$result = parent::post($url, $data);
			parent::log_info("更新电子发票状态成功");
			return true;
		}catch(\Exception $e){
			parent::log_error("更新电子发票状态失败");
			throw $e;
		}
	}

}

//end