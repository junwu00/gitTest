<?php
/**
 * 微信企业号官方接口调用_指纹识别
 * @author yangpz
 * @date 2016-12-09
 *
 */
namespace package\weixin\qy;

class WXQYSoter extends WXQYBase {

    public function __construct($log_pre_desc='') {
        parent::__construct($log_pre_desc);
    }

    /**
     * 验证指纹签名
     * @param $access_token
     * @param array $data
     *      data.open_id
     *      data.json
     *      data.sign
     * @return array
     * @throws \Exception
     */
    public function verify_sign($access_token, array $data) {
        parent::log_info("开始验证指纹签名: data=".json_encode($data));
        $url = "https://qyapi.weixin.qq.com/cgi-bin/soter/verify_signature?access_token={$access_token}";

        !is_string($data['json']) && $data['json'] = json_encode($data['json']);
        $data['json'] = str_replace('"', '\"', $data['json']);
        $post_data = array(
            "openid"        => $data['open_id'],
            "json_string"   => $data['json'],
            "json_signature" => $data['sign'],
        );

//        parent::log_info("post_data=".json_encode($post_data));
        try {
            $ret = parent::post($url, $post_data);
            parent::log_info("验证指纹签名成功");
            return $ret;

        } catch (\Exception $e) {
            parent::log_info('验证指纹签名失败：'.$e -> getMessage());
            parent::log_error('验证指纹签名失败：'.$e -> getMessage());
            throw $e;
        }
    }

}

//end