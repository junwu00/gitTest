<?php

/**
 * 微信企业号官方接口调用_基础类，提供各模块公用方法
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

abstract class WXQYBase {	
    /** 默认配置名称（使用load_config加载） */
    private $_default_config_path = 'package/weixin/qy';
	/** 额外的日志内容，字符串格式，多个用[]分隔。 如：[com:123][admin:456] */
	private $log_pre_desc = '';
	/** 是否为调试模式，默认为FALSE */
	private $debug = FALSE;
	
	public function __construct($log_pre_desc = '') {
		$this -> log_pre_desc = $log_pre_desc;
		
		$conf = load_config($this -> _default_config_path);
		isset($conf['debug']) && $this -> debug = $conf['debug'];
	}

	/**
	 * 记录调试日志
	 * @param unknown_type $str	日志内容
	 */
	protected function log_debug($str) {
		if ($this -> debug) {
			is_array($str) && $str = json_encode($str);
			to_log($GLOBALS['levels']['MAIN_LOG_DEBUG'], '', $this -> log_pre_desc . $str);
		}
	}

	/**
	 * 记录普通日志
	 * @param unknown_type $str	日志内容
	 */
	protected function log_info($str) {
		is_array($str) && $str = json_encode($str);
		to_log($GLOBALS['levels']['MAIN_LOG_INFO'], '', $this -> log_pre_desc . $str);
	}
	
	/**
	 * 记录错误日志
	 * @param unknown_type $str	日志内容
	 */
	protected function log_error($str) {
		is_array($str) && $str = json_encode($str);
		to_log($GLOBALS['levels']['MAIN_LOG_ERROR'], '', $this -> log_pre_desc . $str);
	}
	
 	/**
     * 执行get请求
     *
     * @access public
     * @param string 	$url 远程地址
     * @param array 	$options curl配置
     * @param integer 	$timeout 读取数据超时时间
     * @param integer 	$con_timeout 连接超时时间
     * @return array
     */
    protected function get($url, array $options=array(), $time_out=30, $con_timeout=20) {
        $ret = array(
            //返回码：0 - client主动断开
            'httpcode' => 0,
            //返回内容
            'content'  => '',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $time_out);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        foreach ($options as $key => $val) {
            curl_setopt($ch, $key, $val);
        }
        unset($val);

        // 执行远程请求，并清除utf8的bom输出
        $ret['content'] = $this -> _remove_utf8_bom(curl_exec($ch));
        // 执行之后才能获取状态码
        $ret['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $content = json_decode($ret['content'], TRUE);
        if (empty($content)) {
        	throw new WXQYException(WXQYErrorCode::$GetExp);
        }
        
		if (isset($content['errcode']) && $content['errcode'] != 0) {
			throw new WXQYException($content['errcode']);
		}
        
        return $content;
    }

    /**
     * 执行post请求
     *
     * @access public
     * @param string 	$url 远程地址
     * @param mixed 	$data 数据集合
     * @param string 	$need_urlencode 是否需要转码，默认为TRUE
     * @param array 	$options curl配置
     * @param integer 	$timeout 读取数据超时时间
     * @param integer 	$con_timeout 连接超时时间
     * @return array
     */
    protected function post($url, $data=null, $need_urlencode=TRUE, array $options=array(), $timeout=30, $con_timeout=20) {
    	if ($need_urlencode) {
			$data = urlencode_array($data);
			$data = urldecode(json_encode($data));	
    	}
			
        $ret = array(
            //返回码：0 - client主动断开
            'httpcode' => 0, 
            //返回内容
            'content'  => '', 
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        !empty($data) and curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        foreach ($options as $key => $val) {
            curl_setopt($ch, $key, $val);
        }
        unset($val);

        // 执行远程请求，并清除utf8的bom输出
        $ret['content']  = $this -> _remove_utf8_bom(curl_exec($ch));
        //执行之后才能获取状态码
        $ret['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $content = json_decode($ret['content'], TRUE);
        if (empty($content)) {
        	throw new WXQYException(WXQYErrorCode::$GetExp);
        }
        
		if (isset($content['errcode']) && $content['errcode'] != 0) {
			throw new WXQYException($content['errcode']);
		}

        return $content;
    }
    
    //内部实现----------------------------------------------------------------------------------

    /**
     * 清除utf-8的bom头
     *
     * @access public
     * @param string $text 字符串  
     * @return string
     */
    private function _remove_utf8_bom($text) {
        $bom = pack('H*', 'EFBBBF');
        $text = preg_replace("/^{$bom}+?/", '', $text);

        return $text;
    }
	
}

//end