<?php

/**
 * 微信企业号官方接口调用_Access Token
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYAccessToken extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 更新并获取access token
	 * @param unknown_type $corpid	企业Id
	 * @param unknown_type $secret	管理组的凭证密钥
	 * @return 获取到的access token，获取到的时间
	 */
	public function update_access_token($corpid, $secret) {
		parent::log_info("开始更新access token");

		try {
			$result = $this -> _check_corpid_and_secret($corpid, $secret);
			$url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={$corpid}&corpsecret={$secret}";
			$data = parent::get($url);
			!isset($data['access_token']) && parent::log_info('更新access token失败：重试 1') && $data = parent::get($url);
			!isset($data['access_token']) && parent::log_info('更新access token失败：重试 2') && $data = parent::get($url);
			
			if (isset($data['access_token'])) {
				parent::log_info('更新access token成功: token='.$data['access_token']);
				return array(
					'token' => $data['access_token'], 
					'time'	=> time(),
				);
				
			} elseif (isset($data['errcode'])) {
				throw new WXQYException($data['errcode']);
				
			} else {
				throw new WXQYException(WXQYErrorCode::$GetAccessTokenExp);
			}
		} catch(Exception $e) {
			throw new WXQYException(WXQYErrorCode::$GetAccessTokenExp);
		}
	}
	
	//内部实现----------------------------------------------------------------------------
	
	/**
	 * 验证$corpid和$secret是否为空
	 * @param unknown_type $corpid	企业Id
	 * @param unknown_type $secret	管理组的凭证密钥
	 * @return 
	 */
	private function _check_corpid_and_secret($corpid, $secret) {
		if (is_null($corpid) || empty($corpid))	{
			parent::log_error('找不到corp id');
			throw new WXQYException(WXQYErrorCode::$CorpIdNotFound);
		}
		if (is_null($secret) || empty($secret)) {
			log_write('找不到corp secret');
			throw new WXQYException(WXQYErrorCode::$SecretNotFound);
		}
	}
}

//end