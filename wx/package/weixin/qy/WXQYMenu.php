<?php
/**
 * 微信企业号官方接口调用_菜单管理
 * @author yangpz
 * @date 2016-02-15
 *
 */
namespace package\weixin\qy;

class WXQYMenu extends WXQYBase {
	
	public function __construct($log_pre_desc='') {
		parent::__construct($log_pre_desc);
	}
	
	/**
	 * 创建菜单
	 * 权限说明：管理员须拥有应用的管理权限，并且应用必须设置在回调模式。
	 * @param unknown_type $access_token
	 * @param unknown_type $agent_id		企业应用的id，整型
	 * @param unknown_type $data			菜单内容（数组格式，只含一、二级菜单内容，中文需进行urlencode）
	 */
	public function create_menu($access_token, $agent_id, array $data) {
		parent::log_info("开始创建菜单：agent_id={$agent_id}");
		parent::log_debug($data);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token={$access_token}&agentid={$agent_id}";
		$data = array('button' => $data);
		
		try {
			parent::post($url, $data);
			parent::log_info("创建菜单成功：agent_id={$agent_id}");
			return $agent_id;
			
		} catch (\Exception $e) {
			parent::log_info("创建菜单失败：".$e -> getMessage());
			parent::log_error("创建菜单失败：".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 获取菜单
	 * 权限说明：管理员须拥有应用的管理权限，并且应用必须设置在回调模式。
	 * @param unknown_type $access_token
	 * @param unknown_type $agent_id		企业应用的id，整型
	 */
	public function get_menu($access_token, $agent_id) {
		parent::log_info("开始获取菜单：agent_id={$agent_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/menu/get?access_token={$access_token}&agentid={$agent_id}";
		
		try {
			$data = parent::get($url);
			parent::log_info("获取菜单成功：agent_id={$agent_id}");
			parent::log_debug($data);
			return $data;
			
		} catch (\Exception $e) {
			parent::log_info("获取菜单失败：".$e -> getMessage());
			parent::log_error("获取菜单失败：".$e -> getMessage());
			throw $e;
		}
	}
	
	/**
	 * 删除菜单
	 * 权限说明：管理员须拥有应用的管理权限，并且应用必须设置在回调模式。
	 * @param unknown_type $access_token
	 * @param unknown_type $agent_id		企业应用的id，整型
	 */
	public function delete_menu($access_token, $agent_id) {
		parent::log_info("开始删除菜单：agent_id={$agent_id}");
		$url = "https://qyapi.weixin.qq.com/cgi-bin/menu/delete?access_token={$access_token}&agentid={$agent_id}";
		
		try {
			$data = parent::get($url);
			parent::log_info("删除菜单成功：agent_id={$agent_id}");
			return $agent_id;
			
		} catch (\Exception $e) {
			parent::log_info("删除菜单失败：".$e -> getMessage());
			parent::log_error("删除菜单失败：".$e -> getMessage());
			throw $e;
		}
	}
	
}

//end