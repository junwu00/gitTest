<?php
/**
 * 当前项目错误日志提醒
 *
 * @author yangpz
 * @create 2016-11-18
 */
namespace package\notice;

class ErrorFileNotice {

    /**
     * 遍历目录下所有文件，找出其中的异常文件
     * @param array $dir_list   要遍历的目录
     * @param $log_path         判断后后记录的文件路径（只记录最后一次）
     * @param $only_today       是否只查询当天日志
     * @param null $reg         用于匹配的正则表达式，不填则使用默认值
     */
    public function lists_and_log(array $dir_list=array(), $log_path, $only_today=TRUE, $reg=NULL) {
        empty($reg) && $reg = "/^(.*)(error|ERROR|exp|EXP)(.*)\\.(txt|log)$/";
        $now = date('Y-m-d H:i:s');
        file_put_contents($log_path, "\n" . "log_time: {$now}\n", FILE_APPEND);

        $today = date('Ymd');
        $content = '';
        foreach ($dir_list as $dir) {
            if (!is_dir($dir))      continue;

            $files = scandir($dir);
            if (empty($files))      continue;

            foreach ($files as $file) {
                if ($file == '.' || $file == '..')  continue;

                if (preg_match($reg, $file)) {
                    $file_path = rtrim($dir, '\\/') . DS . $file;
                    if ($file_path != $log_path) {
                        /*
                        $create_time = filectime($file_path);
                        $create_time = date('Y-m-d H:i:s', $create_time);
                        */

                        $last_time = filemtime($file_path);
                        $last_date = date('Ymd', $last_time);
                        $last_time = date('Y-m-d H:i:s', $last_time);

                        if ($only_today && $today != $last_date) {
                            //仅查当天，且非当天的文件，不记录
                        } else {
                            $content .= "update_time: {$last_time}, file_path: {$file_path} \n";
                        }
                    }
                }
            }unset($file);
        }unset($dir);

        if (empty($content)) {
            file_put_contents($log_path, "no error or exception\n", FILE_APPEND);
        } else {
            file_put_contents($log_path, $content, FILE_APPEND);
        }

        return $content;
    }

}

/* End of this file */