<?php
/**
 * 为图片增加水印
 * User: yangpz
 * Date: 2016/12/7
 * Time: 19:19
 */

namespace package\file\image;

class WaterMark {

    /** 临时文件存放目录 */
    private $tmp_dir;

    public function __construct() {
        $this -> tmp_dir = MAIN_DATA . 'tmp' . DS;
        if (!is_dir($this -> tmp_dir)) {
            mkdir($this -> tmp_dir);
        }
    }

    /**
     * 生成水印
     * @param array $conf
     * @return string
     */
    public function mark(array $conf) {
        $background	= $this -> _get_img_data($conf['bg_path']);     //要加水印的图片
        imagesavealpha($background, true);
        $bg_w       = imagesx($background) - 4;
        $bg_h       = imagesy($background) - 4;

        $pic_list 	= $conf['text_list'];                           //要加的水印
        !is_array($pic_list) && $pic_list = array();

        $bottom = $bg_h - 15;
        $pic_list = array_reverse($pic_list);           //倒序，从下向上加水印
        foreach ($pic_list as &$p) {
            //字体最小大小
            $font_min       = (isset($p['font_min'])) ? $p['font_min'] : 14;
            //字体大小
            if (isset($p['font_size'])) {               //指定大小，不受最小大小限制
                $font_size = $p['font_size'];

            } else {
                $font_size      = ceil($bg_h / 40);
                $font_size      = max($font_size, $font_min);
            }
            $font_size = (int)($font_size * (isset($p['ratio']) ? $p['ratio'] : 1));

            $line_height    = ceil($font_size * 1.8);

            $p['font_size'] = $font_size;
            $bottom -= $line_height;                    //计算/累计文字与图片顶部的距离
            $p['y'] = $bottom;
            $p['img_path'] = $this -> _create_text_img($p['val'], $p);
        }unset($p);

        foreach ($pic_list as $p) {
            $pic    = $this -> _get_img_data($p['img_path']);
            $angle  = isset($p['angle']) ? intval($p['angle']) : 0;
            $pic_w  = imagesx($pic) - 4;
            $pic_h  = imagesy($pic) - 4;

            if(IS_PRIVATE == 0 && isset($p['logo'])){   //非私有化，可支持移步logo配置
                $pic_logo = $this -> _get_img_data($p['logo']);
                $pic_logo_w  = imagesx($pic_logo) - 4;
                $pic_logo_h  = imagesy($pic_logo) - 4;
                $logo_left = $bg_w-$pic_w-$pic_h-2;

                imagecopyresampled($background, $pic_logo, $logo_left, $p['y']-2, 3, 3, $pic_h, $pic_h, $pic_logo_w, $pic_logo_h);
            }

            $left = $bg_w-$pic_w;       //右对齐

		    imagecopyresampled($background, $pic, $left, $p['y'], 2, 2, $pic_w, $pic_h, $pic_w, $pic_h);
//		    imagecopymerge($background, $pic, $left, $p['y'], 2, 2, $pic_w, $pic_h, $pic_w, $pic_h, 80);
        }
        foreach ($pic_list as $p) {
            unlink($p['img_path']);
        }unset($p);

        return $this -> _save_mark($conf, $background);
    }

    /**
     * 根据源文件类型生成对应水印图
     * @param $conf     生成配置
     * @param $image    水印图
     *
     * @return string   结果路径
     */
    private function _save_mark($conf, $image) {
        $file_path = $this -> tmp_dir . md5(json_encode($conf)) . '_' . time();
        if (isset($conf['bg_ext'])) {
            $bg_ext = $conf['bg_ext'];
        } else {
            $path_info	= pathinfo($conf['bg_path']);
            $bg_ext = strtolower($path_info['extension']);
        }
        switch ($bg_ext) {
            case 'jpg':     $im_fun	= 'imagejpeg';      $ext = 'jpg';      break;
            case 'jpeg':    $im_fun	= 'imagejpeg';      $ext = 'jpeg';     break;
            case 'png':     $im_fun	= 'imagepng';       $ext = 'png';      break;
            case 'gif';     $im_fun	= 'imagegif';       $ext = 'gif';      break;
            case 'bmp';     $im_fun	= 'imagewbmp';      $ext = 'bmp';      break;
            case 'wbmp';    $im_fun	= 'imagewbmp';      $ext = 'bmp';      break;
            default:        $im_fun	= 'imagejpeg';      $ext = 'jpg';
        }
        $file_path .= '.' . $ext;
        if ($ext == 'jpg' || $ext == 'jpeg') {
            $im_fun($image, $file_path, 90);
        } else if ($ext == 'png') {
            $im_fun($image, $file_path);
        } else {
            $im_fun($image, $file_path);
        }
        return $file_path;
    }

    /**
     * 获取文件实例
     * @param $pic_path 图片绝对路径
     *
     * @return mixed    图片实例
     */
    private function _get_img_data($pic_path) {
        $path_info	= pathinfo($pic_path);
        switch (strtolower($path_info['extension']) ) {
            case 'jpg':     $im_fun	= 'imagecreatefromjpeg';    break;
            case 'jpeg':    $im_fun	= 'imagecreatefromjpeg';    break;
            case 'png':     $im_fun	= 'imagecreatefrompng';     break;
            case 'gif':     $im_fun	= 'imagecreatefromgif';     break;
            case 'bmp':     $im_fun	= 'imagecreatefromwbmp';    break;
            case 'wbmp':    $im_fun	= 'imagecreatefromwbmp';    break;
            default:        $im_fun	= 'imagecreatefromstring';  $pic_path = file_get_contents($pic_path);   break;
        }
        return $im_fun($pic_path);
    }

    /**
     * 生成文字图片
     *
     * @param $str          要生成图片的文字
     * @param array $conf   生成配置
     *
     * @return string       生成的图片的绝对路径
     */
    private function _create_text_img($str, array $conf=array()) {
        $str = urldecode($str);

        $font       = isset($conf['font']) ? $conf['font'] : 'SIMHEI.TTF';                          //字体类型
        $font       = dirname(__DIR__) . DS . "fonts" . DS . $font;
        $font_size  = isset($conf['font_size']) ? intval($conf['font_size']) : 14;                  //字体大小
        //字体颜色，默认为白色
        if (isset($conf['font_front'])) {
            $font_front = $conf['font_front'];
            is_string($font_front) && $font_front = $this -> _hex2rgb($font_front);

        } else {
            $font_front = array(255, 255, 255);
        }

        //背景颜色，默认为灰色
        if (isset($conf['font_bg'])) {
            $font_bg = $conf['font_bg'];
            is_string($font_bg) && $font_bg = $this -> _hex2rgb($font_bg);
        } else {
            $font_bg = array(128, 128, 128);
        }

        //计算生成的图片的大小
        $box = imagettfbbox($font_size, 0, $font, $str);
        $scale = 1.2;
        $w = ($box[4] - $box[0]) + $font_size;			    //右上角X - 左下角X
        $h = ceil(($box[3] - $box[7]) * $scale * 1.1) + 10;	//右下角Y - 左上角Y

        //设置字体图片
        $im_white = imagecreatetruecolor($w, $h);
        $trans_colour = imagecolorallocatealpha($im_white, 255, 255, 255, 127);
        imagefill($im_white, 0, 0, $trans_colour);
        imagesavealpha($im_white, true);
        $front = imagecolorallocate($im_white, $font_front[0], $font_front[1], $font_front[2]);
        imagefttext($im_white, $font_size, 0, intval($font_size / 2), 	ceil($font_size * $scale) + 1, $front, $font, $str);

        //设置字体阴影
        $im_black = imagecreatetruecolor($w, $h);
        $trans_colour = imagecolorallocatealpha($im_black, 255, 255, 255, 127);
        imagefill($im_black, 0, 0, $trans_colour);
        imagesavealpha($im_black, true);
        $bg = imagecolorallocate($im_black, $font_bg[0], $font_bg[1], $font_bg[2]);
        imagefttext($im_black, $font_size, 0, intval($font_size / 2)+1, ceil($font_size * $scale) + 2, $bg, $font, $str);

        //合成有字体阴影的图片
        imagecopyresampled($im_black, $im_white, 0, 0, 0, 0, $w, $h, $w, $h);
        $file_path = $this -> tmp_dir . md5($str . json_encode($conf)) . '_' . time() . '.png';         //生成临时图片，用于后续合成
        imagepng($im_black, $file_path);

        /*
        header("Content-type: image/png");
        imagepng($im_black);
        //*/
        imagedestroy($im_white);
        imagedestroy($im_black);

        return $file_path;
    }

    /**
     * 16进制颜色值转rgb
     * @param $hexColor
     * @return array
     */
    private function _hex2rgb($hexColor) {
        $color = str_replace('#', '', $hexColor);
        if (strlen($color) > 3) {
            $rgb = array(
                'r' => hexdec(substr($color, 0, 2)),
                'g' => hexdec(substr($color, 2, 2)),
                'b' => hexdec(substr($color, 4, 2))
            );
        } else {
            $color = $hexColor;
            $r = substr($color, 0, 1) . substr($color, 0, 1);
            $g = substr($color, 1, 1) . substr($color, 1, 1);
            $b = substr($color, 2, 1) . substr($color, 2, 1);
            $rgb = array(
                'r' => hexdec($r),
                'g' => hexdec($g),
                'b' => hexdec($b)
            );
        }
        return array_values($rgb);
    }

}

//end