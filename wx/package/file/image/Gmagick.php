<?php
/**
 * gmagick图片处理类
 *
 * @require php-gmagick扩展
 * @author JianMing Liang
 * @create 2016-01-27 16:24:40
 */
namespace package\file\image;

class Gmagick {
    // 缩放处理后的图片质量比例（%）
    public $resize_quality = 90;

    // 默认配置名称（使用load_config加载）
    private $_default_config_path = 'package/file/image/gmagick';
    // 图片对象
    private $_imager = null;

    /**
     * 构造函数
     *
     * @access public
     * @return void
     */
    public function __construct() {
        if (!class_exists('\Gmagick')) {
            to_log(MAIN_LOG_FATAL, '', '\Gmagick类不存在，可能是未安装\gmagick扩展');
            exit;
        }

        $conf = load_config($this -> _default_config_path);
        if (!is_array($conf) or empty($conf)) {
            to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
            return;
        }

        isset($conf['resize_quality']) and $this -> resize_quality = $conf['resize_quality'];
    }
    
    /** 
     * 析构函数
     * 
     * @access public
     * @return viod
     */
    public function __destruct(){
        $this -> destroy();
    }
    
    /** 
     * 缩小或放大图片
     * 
     * @access public
     * @param string $from_file 原图片全路径
     * @param string $to_file 输出全路径
     * @param string $width 将图片调整到此宽度
     * @param string $height 将图片调整到此高度
     * @return boolean
     */
    public function resize($file, $to_file, $width, $height) {
        //初始化图片对象
        if(!$this -> load_image($file)) return false;
        //参数严格验证
        if(!is_string($to_file) || is_empty($to_file)) return false;
        if(is_empty($width) || is_empty($height)) return false;
        if(preg_match('/[^\.%\d]+/', $width) || preg_match('/[^\.%\d]+/', $height)) return false;
        
        //获取图片的宽度与高度
        try{
            $ori_width = $this -> _imager -> getimagewidth();
            $ori_height = $this -> _imager -> getimageheight();
        }catch(\GmagickException $e){
            to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
            return false;
        }
        
        //支持按百分比参数
        preg_match('/%$/', $width) && $width = intval($ori_width * intval(preg_replace('/%$/', '', $width)) / 100);
        preg_match('/%$/', $height) && $height = intval($ori_height * intval(preg_replace('/%$/', '', $height)) / 100);
        
        $width = intval($width);
        $height = intval($height);
        
        if($width == 0 || $height == 0) return false;
        
        $x = 0;
        $y = 0;
        
        //根据缩放比例裁剪中央区域的图片算法
        if($width >= $ori_width && $height >= $ori_height){
            if($ori_width * $height / $width <= $ori_height){
                $tmp_height = intval($ori_width * $height / $width);
                $y = intval(($ori_height - $tmp_height) / 2);
                $ori_height = $tmp_height;
            }else{
                $tmp_width = intval($ori_height * $width / $height);
                $x = intval(($ori_width - $tmp_width) / 2);
                $ori_width = $tmp_width;
            }
            
        }elseif($width < $ori_width && $height < $ori_height){
            if($ori_width * $height / $width <= $ori_height){
                $tmp_height = intval($ori_width * $height / $width);
                $y = intval(($ori_height - $tmp_height) / 2);
                $ori_height = $tmp_height;
            }else{
                $tmp_width = intval($ori_height * $width / $height);
                $x = intval(($ori_width - $tmp_width) / 2);
                $ori_width = $tmp_width;
            }
        }else{
            if($width > $ori_width){
                $tmp_height = intval($ori_width * $height / $width);
                $y = intval(($ori_height - $tmp_height) / 2);
                $ori_height = $tmp_height;
            }else{
                $tmp_width = intval($ori_height * $width / $height);
                $x = intval(($ori_width - $tmp_width) / 2);
                $ori_width = $tmp_width;
            }
        }
        
        //执行图片裁剪及缩放处理
        try{
            $this -> _imager -> cropimage($ori_width, $ori_height, $x, $y)
                           -> thumbnailImage($width, $height)
                           -> setCompressionQuality($this -> resize_quality)
                           -> write($to_file);
        }catch(\GmagickException $e){
            to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
            return false;
        }

        return true;
    }
    
    /** 
     * 销毁图片对象，清空缓存
     * 
     * @access private
     * @return boolean
     */
    private function destroy() {
        if(is_object($this -> _imager)) {
            try{
                $this -> _imager -> destroy();
            }catch(\GmagickException $e){
                to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
                return false;
            }
        }

        return true;
    }

    /** 
     * 加载图片创建图片对象
     * 
     * @access private
     * @param string $file 图片绝对路径
     * @return mixed 成功返回图片对象，否则返回false
     */
    private function load_image($file='') {
        $this -> destroy();

        $file = strval($file);
        if(!file_exists($file)) return false;

        try {
            $this -> _imager = new \Gmagick($file);
        }catch(\GmagickException $e){
            to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
            return false;
        }

        return $this -> _imager;
    }
}

/* End of file cls_gmagick.php */ 