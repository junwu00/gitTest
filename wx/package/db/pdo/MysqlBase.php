<?php
/**
 * pdo_mysql数据库基类
 *
 * @author LiangJianMing
 * @create 2016-01-06 17:42:04
 */
namespace package\db\pdo;

class MysqlBase implements \package\db\pdo\INF {
    // 数据库连接句柄
    private $_db;
    // 数据库配置数据
    private $_conf;
    
    // pdo数据库连接串
    private $_pdo_dsn;
    // pdo连接参数
    private $_pdo_driver;
    
    /** 
     * 构造函数
     * 
     * @access public
     * @param array $conf 数据库配置数组，如果该数组为空，会尝试使用预定义配置；
     * 数组包含以下元素：
     *              必须 string "host" 服务地址
     *              必须 string "port" 服务端口
     *              必须 string "user" 服务帐号
     *              必须 string "pass" 服务密码
     *              必须 string "name" 库名称
     *              必须 string "charset" 连接字符集
     *              可选 string "persistent" 是否长连接，默认为false
     * @return void
     */
    public function __construct(array $conf=array()) {
        if (!class_exists('\PDO', false)) {
            to_log(MAIN_LOG_FATAL, '', '\PDO类不存在，可能未安装\PDO扩展');
            return;
        }

        $this -> set_conf($conf);
    }

    /**
     * 改变当前的数据库配置，但未重新进行连接
     *
     * @access public
     * @param array $conf 意义请参考构造函数
     * @return array
     */
    public function set_conf(array $conf=array()) {
        !is_array($conf) and $conf = array();
        
        // 先将当前配置清空未默认值
        $this -> _conf = $now_conf = array(
            'name' => '',
            'host' => '',
            'user' => '',
            'pass' => '',
            'port' => '3306',
            'charset' => 'utf8',
            'persistent' => false,
        );

        // 使用新的配置
        isset($conf['name']) and $now_conf['name'] = strval($conf['name']);
        isset($conf['host']) and $now_conf['host'] = strval($conf['host']);
        isset($conf['user']) and $now_conf['user'] = strval($conf['user']);
        isset($conf['pass']) and $now_conf['pass'] = strval($conf['pass']);
        isset($conf['port']) and $now_conf['port'] = strval($conf['port']);
        isset($conf['charset']) and $now_conf['charset'] = strval($conf['charset']);
        isset($conf['persistent']) and $now_conf['persistent'] = $conf['persistent'] ? true : false;

        $this -> _conf = $now_conf;
        $this -> _pdo_dsn = 'mysql' . ':host=' . $this -> _conf['host'] . ';dbname=' . $this -> _conf['name'];

        if(!empty($this->_conf['port'])){
            $this -> _pdo_dsn = 'mysql' . ':host=' . $this -> _conf['host'] . ';dbname=' . $this -> _conf['name'].';port='.$this->_conf['port'];
        }

        $this -> _pdo_driver = array(
            // 是否长连接
            \PDO::ATTR_PERSISTENT => $this -> _conf['persistent'],
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'' . $this -> _conf['charset'] . '\'',
        );
    }

    /**
     * 获取当前的数据库配置
     *
     * @access public 
     * @return mixed
     */
    public function get_conf() {
        return $this -> _conf;
    }
    
    /** 
     * 析构函数
     * 
     * @access public  
     * @return void
     */
    public function __destruct() {
        $this -> close();
    }
    
    /** 
     * 判断是否连接，若未连接则尝试连接
     * 
     * @access public
     * @param boolean $force_new 是否强制进行新的连接，而不是复用现有连接
     * @return void
     */
    public function reconnect($force_new=false) {
        if ($force_new) {
            $this -> _connect();
            return;
        }

        $check = $this -> _is_connected();
        !$check and $this -> _connect();
    }
    
    /** 
     * 关闭数据库连接
     * 
     * @access public
     * @return void
     */
    public function close() {
        $this -> _db = NULL;
    }

    /** 
     * 更换数据库
     * 
     * @access public
     * @param string $name 数据库名称
     * @return boolean
     */
    public function change_db($name){
        $name = strval($name);
        if ($name == '') {
            return false;
        }

        $db = $this -> _db;
        $ret = $db -> exec('USE ' . $name);
        
        return $ret;
    }
    
    /** 
     * 更换数据库编码
     * 
     * @access public
     * @param string $charset 编码
     * @return boolean
     */
    public function change_charset($charset){
        $charset = strval($charset);
        if ($charset == '') {
            return false;
        }
        
        $db = $this -> _db;
        $ret = $db -> exec("SET NAMES '{$charset}'");

        return $ret ? true : false;
    }

    /** 
     * 获取pdo连接句柄
     * 
     * @access public
     * @param boolean $force_new 是否强制进行新的连接，而不是复用现有连接
     * @return mixed 成功返回object，否则返回false
     */
    public function get_pdo($force_new=false){
        $this -> reconnect($force_new);

        return $this -> _db;
    }

    // ----------------以下是私有实现----------------------------
    /** 
     * 连接数据库
     * 
     * @access private
     * @return void
     */
    private function _connect() {
        try {
            $_db = new \PDO($this -> _pdo_dsn, $this -> _conf['user'], $this -> _conf['pass'], $this -> _pdo_driver);
            // 防止：Cannot execute queries while other unbuffered queries are active
            $_db -> setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        }catch(\PDOException $e) {
            $_db = NULL;
        }

        if(!$_db or empty($_db)) {
            \to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ': pdo_mysql连接失败');
        }

        $this -> _db = $_db;
    }
    
    /** 
     * 是否连接数据库
     * 
     * @access private
     * @param boolean $is_real 是否真实连接
     * @return boolean
     */
    private function _is_connected($is_real=false){
        $_db = $this -> _db;

        if (empty($_db)) {
            return false;
        }

        if (!$is_real and is_object($_db)) {
            return true;
        }
        
        $check = $this -> query('show tables');
        return $check ? true : false;
    }
}

/* End of this file */