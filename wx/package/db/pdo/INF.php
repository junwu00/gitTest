<?php
/**
 * 约定数据库基类的标准接口
 *
 * @author LiangJianMing
 * @create 2016-01-06 17:32:48
 */
namespace package\db\pdo;

interface INF {
    // 设定数据库配置
    public function set_conf(array $conf);
    // 获得当前数据库配置
    public function get_conf();
    // 重新连接数据库
    public function reconnect($force_new);
    // 关闭数据库连接
    public function close();
    // 更换数据库
    public function change_db($name);
    // 更换数据库编码
    public function change_charset($charset);
    // 获取pdo连接句柄
    public function get_pdo($force_new);
}