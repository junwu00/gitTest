<?php
/**
 * 提供一致性的pdo二次封装类
 *
 * @author LiangJianMing
 * @create 2016-01-07 14:01:25
 */
namespace package\db\pdo;

class Mysql extends \package\db\pdo\MysqlBase {
    // 日志文件路径
    private $_log_path = '';

    /** 
     * 构造函数
     * 
     * @access public
     * @param string $driver 驱动类型，如：mysql、SQLite
     * @param array $conf 数据库配置数组，具体参数请查看具体基类
     * @return void
     */
    public function __construct(array $conf=array()) {
        empty($conf) and isset($GLOBALS['MAIN_DB_CONFIG']) and $conf = $GLOBALS['MAIN_DB_CONFIG'];
        isset($conf['log_path']) and $this -> _log_path = $conf['log_path'];

        parent::__construct($conf);
    }

    /**
     * 访问不可读取的属性时，调用该方法，实现按需连接
     *
     * @access public
     * @param string $name 属性名
     * @return mixed 成功返回object，否则返回false
     */
    public function __get($name) {
        if ($name != '_db') return false;

        return $this -> get_pdo();
    }

    /** 
     * 如果使用对象调用无权访问或不存在的函数时，触发该函数
     * 
     * @access public
     * @param string $name 函数名
     * @param array $args 参数数组
     * @return void
     */
    public function __call($name, $args) {
        to_log(MAIN_LOG_WARN, $this -> _log_path, __CLASS__ . ": 未找到名为\"{$name}\"的方法");
    }

    /**
     * 获取pdo连接句柄
     *
     * @access public
     * @return void
     */
    public function get_handler() {
        return $this -> _db;
    }

    /** 
     * 单条记录入库
     * 
     * @access public
     * @param string $table 数据表名称
     * @param array $data 数据集合数组
     * @return mixed 写入且成功返回integer, 否则返回false
     */
    public function insert($table, array $data) {
        $table = strval($table);

        if (empty($table) or empty($data)) {
            return false;
        }

        $set_prepare = $this -> compose_prepare_set($data);

        $pre_sql = "INSERT INTO {$table} SET " . $set_prepare['sql'];
        $ret = $this -> prepare_exec($pre_sql, false, $set_prepare['param']);
        $ret and $ret = $this -> _db -> lastInsertId();
        
        return $ret ? $ret : false;
    }
    
    /** 
     * 批量写入数据库
     * 
     * @access public
     * @param string $table 数据库表名
     * @param midex $fields 写入的对应字段字符串
     * @param array $data 需要批量写入的数据集合，子元素必须与$fields一一对应
     * @param integer $per_num 每次写入多少条记录
     * @return boolean 全部成功返回true，否则返回false
     */
    public function batch_insert($table, $fields, array $data, $per_num=500) {
        $ret = true;
        
        $data = array_values($data);
        $len = count($data);
        $per_num > $len and $per_num = $len;
        
        $count = 0;
        $tmp_arr = array();
        for ($i = 0; $i < $len; $i++) {
            $tmp_arr[] = $data[$i];
            $count++;

            if ($count == $per_num or $i == $len - 1) {
                $prepare = $this -> compose_prepare_batch_insert($table, $fields, $tmp_arr);
                $result = $this -> prepare_exec($prepare['sql'], false, $prepare['param']);
                !$result and $ret = false;
                $count = 0;
                $tmp_arr = array();
            }
        }
        
        return $ret;
    }
    
    /** 
     * 更新一条数据库记录
     * 
     * @access public
     * @param string $table 数据表名
     * @param array $condition 条件数组
     * @param array $data 数据集合
     * @param boolean $ret_count 是否返回受影响行数
     * @return mixed
     */
    public function update($table, array $condition, array $data, $ret_count=false) {
        $table = strval($table);

        if (empty($table) or empty($condition) or empty($data)) {
            return false;
        }

        $set_pre = $this -> compose_prepare_set($data);
        $where_pre = $this -> compose_prepare_where($condition);

        $pre_param = $set_pre['param'];
        foreach ($where_pre['param'] as $val) {
            $pre_param[] = $val;
        }

        $pre_sql = "UPDATE {$table} SET {$set_pre['sql']}";
        $where_pre['sql'] !== '' and $pre_sql .= " WHERE {$where_pre['sql']}";

        $ret = $this -> prepare_exec($pre_sql, $ret_count, $pre_param);
        
        return $ret;
    }
    
    /** 
     * 根据条件删除数据库记录
     * 
     * @access public
     * @param string $table 数据表名
     * @param array $condition 条件集合
     * @param boolean $ret_count 是否返回受影响行数
     * @return mixed
     */
    public function delete($table, array $condition, $ret_count=false) {
        $table = strval($table);

        if (empty($table) or empty($condition)) {
            return false;
        }

        $where_pre = $this -> compose_prepare_where($condition);

        $pre_sql = "DELETE FROM {$table} WHERE {$where_pre['sql']}";
        $ret = $this -> prepare_exec($pre_sql, $ret_count, $where_pre['param']);

        return $ret;
    }
    
    /** 
     * 常用的查询方法
     * 
     * @access public
     * @param string $table 数据表名称
     * @param midex $fields 要查询的字段，可以是string，也可以是array
     * @param array $condition 条件集合
     * @param integer $page 页码
     * @param integer $page_size 每页最大记录数  
     * @param string $groub_By 分组方式, 自动补全 " GROUP BY "
     * @param string $order_by 排序方式, 自动补全 " ORDER BY "
     * @param boolean $with_count 是否查询limit前的总量
     * @return mixed 成功返回array，否则返回false
     */
    public function select($table, $fields, array $condition=array(), $page=0, $page_size=0, $group_by='', $order_by='', $with_count=FALSE) {
        $table = strval($table);
        if (empty($table) or empty($fields)) {
            return false;
        }

        is_array($fields) and $fields = implode(',', $fields);

        $pre_sql = "SELECT {$fields} FROM {$table}";
        $where_pre = $this -> compose_prepare_where($condition);
        $where_pre['sql'] !== '' and $pre_sql .= " WHERE {$where_pre['sql']}";

        $group_by = preg_replace('/group[ ]+by/i', '', $group_by);
        $order_by = preg_replace('/order[ ]+by/i', '', $order_by);
        !empty($group_by) and $pre_sql .= " GROUP BY {$group_by}";
        !empty($order_by) and $pre_sql .= " ORDER BY {$order_by}";

        $limit_sql = $pre_sql;
        if ($page > 0 and $page_size > 0) {
            $begin = ($page - 1) * $page_size;
            $limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
            $limit_sql .= $limit;
        }
        $ret = $this -> prepare_query($limit_sql, 1, $where_pre['param']);
        // $test = $this -> prepare_to_full($limit_sql, $where_pre['param']);
        // to_log('Info', '', $test);

        // if (empty($ret)) {
        //     return false;
        // }

        if ($with_count) {
            $tmp_table_name = 'select_get_count';
            $tmp_sql = "SELECT COUNT(1) AS total FROM ({$pre_sql}) AS {$tmp_table_name}";
            $count = $this -> prepare_query($tmp_sql, 3, $where_pre['param']);
            $ret = array(
                'data' => $ret,
                'count' => $count ? intval($count) : 0,
            );
        }

        return $ret;
    }

    /** 
     * 常用的单条记录查询方法
     * 
     * @access public
     * @param string $table 数据表名称
     * @param midex $fields 要查询的字段，可以是string，也可以是array
     * @param array $condition 条件集合
     * @return mixed 成功返回array，否则返回false
     */
    public function select_one($table, $fields, array $condition=array()) {
        $table = strval($table);
        if (empty($table) or empty($fields)) {
            return false;
        }

        is_array($fields) and $fields = implode(',', $fields);

        $pre_sql = "SELECT {$fields} FROM {$table}";
        $where_pre = $this -> compose_prepare_where($condition);
        $where_pre['sql'] !== '' and $pre_sql .= " WHERE {$where_pre['sql']}";
        $pre_sql .= " LIMIT 1";

        $ret = $this -> prepare_query($pre_sql, 2, $where_pre['param']);
        return $ret;
    }

    /** 
     * 常用的第一行第一列查询方法
     * 
     * @access public
     * @param string $table 数据表名称
     * @param mixed $fields 要查询的字段
     * @param array $condition 条件集合
     * @return mixed 成功返回string，否则返回false
     */
    public function select_first_val($table, $fields, array $condition=array()) {
        $table = strval($table);
        if (empty($table) or empty($fields)) {
            return false;
        }

        is_array($fields) and $fields = implode(',', $fields);

        $pre_sql = "SELECT {$fields} FROM {$table}";
        $where_pre = $this -> compose_prepare_where($condition);
        $where_pre['sql'] !== '' and $pre_sql .= " WHERE {$where_pre['sql']}";
        $pre_sql .= " LIMIT 1";

        $ret = $this -> prepare_query($pre_sql, 3, $where_pre['param']);

        return $ret;
    }

    /** 
     * 检查一条记录是否存在
     * 
     * @param string $table 数据表名
     * @param array $condition 条件数组，如：array('id!=' => 0)
     * @return boolean
     */
    public function check_exists($table, array $condition) {
        if (empty($condition)) {
            return false;
        }
        
        $ret = $this -> select_one($table, '1', $condition);

        return $ret ? true : false;
    }

    /** 
     * 开始事务
     * 
     * @access public
     * @return void
     */
    public function begin_trans() {
        $db = $this -> _db;
        $db -> beginTransaction();
    }

    /** 
     * 提交事务
     * 
     * @access public
     * @return void
     */
    public function commit() {
        $db = $this -> _db;
        $db -> commit();
    }

    /** 
     * 回滚事务
     * 
     * @access public
     * @return void
     */
    public function rollback() {
        $db = $this -> _db;
        $db -> rollback();
    }
    
    /** 
     * 切换数据库
     * 
     * @access public
     * @param string $name 数据库名
     * @return boolean 成功返回true，失败返回false
     */
    public function change_db($name) {
        $name = strval($name);
        if ($name === '') {
            return false;
        }

        $ret = $this -> change_db($name);

        return $ret;
    }
    
    /** 
     * 切换数据库字符集
     * 
     * @access public
     * @param string $charset 字符格式
     * @return boolean 成功返回true，失败返回false
     */
    public function change_charset($charset) {
        $charset = strval($charset);
        if ($charset === '') {
            return false;
        }

        $ret = $this -> change_charset($charset);

        return $ret;
    }

    // ---begin----------------组合系列方法--------------------------------
    /**
     * 组合批量insert预处理语句和其对应的绑定值数组
     *
     * @access public
     * @param string $table 数据表名称
     * @param array $fields 与 $data 一一对应的字段名称
     * @param array $data 数据结合
     * @return array 返回如下格式：
     *                array(
     *                    'sql' => '',
     *                    'param' => array(
     *                         唯一随机数 => 对应的值,
     *                         ...
     *                    ),
     *                )
     */
    public function compose_prepare_batch_insert($table, array $fields, array $data) {
        $table = strval($table);
        !is_array($fields) and $fields = array();
        !is_array($data) and $data = array();

        $ret = array(
            'sql' => '',
            'param' => array(),
        );

        if (empty($table) or empty($fields) or empty($data)) {
            return $ret;
        }

        $fields_str = implode(',', $fields);
        $ret['sql'] = "INSERT INTO {$table}($fields_str) VALUES";
        
        $values = '';
        foreach ($data as $val) {
            if (!is_array($val)) {
                continue;
            }

            $tmp_values = '';
            foreach ($val as $cval) {
                $tmp_values .= "?, ";
                $ret['param'][] = $cval;
            }
            $tmp_values !== '' and $tmp_values = '(' . substr($tmp_values, 0, -2) . '), ';
            $values .= $tmp_values;
        }
        $values !== '' and $values = substr($values, 0, -2);
        $ret['sql'] .= $values;

        return $ret;
    }

    /**
     * 组合set预处理语句和其对应的绑定值数组
     *
     * @access public
     * @param array $data 数据结合
     * @return array 返回如下格式：
     *                array(
     *                    'sql' => '',
     *                    'param' => array(
     *                         唯一随机数 => 对应的值,
     *                         ...
     *                    ),
     *                )
     */
    public function compose_prepare_set(array $data) {
        !is_array($data) and $data = array();

        $ret = array(
            'sql' => '',
            'param' => array(),
        );

        if (empty($data)) {
            return $ret;
        }

        $set = '';
        foreach($data as $key => $val) {
            $set .= "{$key} = ?, ";
            $ret['param'][] = $val;
        }
        $set = substr($set, 0, -2);
        $ret['sql'] = $set;

        return $ret;
    }

    /**
     * 组合where条件并获取其预处理语句和其对应的bind数组
     *
     * @access public
     * @param array $condition 条件集合
     * @return array 返回如下格式：
     *                array(
     *                    'sql' => '',
     *                    'param' => array(
     *                         唯一随机数 => 对应的值,
     *                         ...
     *                    ),
     *                )
     */
    public function compose_prepare_where(array $condition) {
        $ret = array(
            'sql' => '',
            'param' => array(),
        );

        $where = '';
        !is_array($condition) and $condition = array();
        foreach ($condition as $key => $val) {
            if (preg_match('/ IN$/', $key)) {
                $in_str = "";
                foreach ($val as $cval) {
                    $in_str .= "?, ";
                    $ret['param'][] = $cval;
                }

                if ($in_str != "") {
                    $in_str = substr($in_str, 0, -2);
                    $where .= "{$key} ($in_str) AND ";
                }

            }else if (strpos($key, '^') === 0) {
                // condition数组中，key在开头使用"^"号直接字符串连接即可
                $tmp_key = preg_replace('/^[\^]/', '', $key);
                $where .= "{$tmp_key} {$val} AND ";
            
            // OR条件，以 '__OR' 开头作为数组元素索引的元素被视为OR条件，该元素必定是数组，方便多个OR或单个OR
            }elseif (strpos($key, '__OR') === 0) {
                $tmp_or = '';
                foreach ($val as $ckey => $cval) {
                    // or条件中还允许有and子条件
                    if (is_array($cval)) {
                        $tmp_ret = $this -> compose_prepare_where($cval);
                        $ret['param'] = array_merge($ret['param'], $tmp_ret['param']);

                        $tmp_or .= '(' . $tmp_ret['sql'] . ') OR ';
                        continue;
                    }
                    //允许多个同样字段的or条件
                    $tmp_ckey = preg_replace('/^__[\d]+__/', '', $ckey);
                    $tmp_or .= "{$tmp_ckey} ? OR ";
                    $ret['param'][] = $cval;
                }

                $tmp_or !== '' and $tmp_or = substr($tmp_or, 0, -4);
                $where .= "($tmp_or) AND ";
            }else {
                $where .= "{$key} ? AND ";
                $ret['param'][] = $val;
            }
        }
        $where !== '' and $where = substr($where, 0, -5);

        $ret['sql'] = $where;

        return $ret;
    }
    // ---end------------------组合系列方法--------------------------------   

    // ---begin----------------底层方法-----------------------------------
    /** 
     * 执行select型语句并返回数据，可避免注入攻击
     * 
     * @access public
     * @param string $pre_sql pdo预处理语句
     * @param integer $scale 返回的数据规模，1 - 全部行、2 - 第一行、其他值 - 第一行第一列
     * @param array $param 预处理语句对应的变量值
     * @param integer $fetch 返回的数组索引类型，1-字段索引、2-数字索引、其他-两种索引并存
     * @return mixed 如果失败返回false，否则返回对应的结果
     */
    public function prepare_query($pre_sql, $scale=1, array $param=array(), $fetch=1) {
        $ret = false;

        $func = $this -> _get_scale_func($scale);
        $fetch_style = $this -> _get_fetch_style($fetch);
        // 如果是返回第一列，无需指定fetch类型
        $func === 'fetchColumn' and $fetch_style = 0;

        try {
            $stmt = $this -> _db -> prepare($pre_sql);
            if ($stmt !== false) {
                if (!empty($param)) {
                    $result = $stmt -> execute($param);
                }else {
                    // 如果$param为空，直接执行语句
                    $result = $stmt -> execute();
                }
            }

            if ($stmt !== false and $result !== false) {
                // 获取结果集
                $ret = $stmt -> $func($fetch_style);
            }
        }catch(\PDOException $e) {
            to_log(MAIN_LOG_ERROR, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
        }
        // 关闭游标
        $stmt !== false and $stmt -> closeCursor();

        return $ret;
    }

    /** 
     * 执行非select型语句，可避免注入攻击
     * 
     * @access public
     * @param string $pre_sql pdo预处理语句
     * @param array $ret_count 是否返回受影响的记录数
     * @param array $param 预处理语句对应的变量值
     * @return mixed 如果失败返回false
     */
    public function prepare_exec($pre_sql, $ret_count=false, array $param=array()) {
        $ret = false;
        try {
            $stmt = $this -> _db -> prepare($pre_sql);
            if ($stmt !== false) {
                if (!empty($param)) {
                    $result = $stmt -> execute($param);
                }else {
                    // 如果$param为空，直接执行语句
                    $result = $stmt -> execute();
                }
            }

            if ($stmt !== false and $result !== false) {
                $ret = true;
                // 获取受影响的记录数
                $ret_count and $ret = $stmt -> rowCount();
            }
        }catch(\PDOException $e) {
            to_log(MAIN_LOG_ERROR, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
        }
        // 关闭游标
        $stmt !== false and $stmt -> closeCursor();

        if ($ret === false) {
            to_log(MAIN_LOG_WARN, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": 执行SQL语句失败", "sql: " . $this -> prepare_to_full($pre_sql, $param));
        }

        return $ret;
    }

    /** 
     * 非prepare型查询方法，使用该方法容易被注入攻击，
     * 如不能保证SQL语句无注入漏洞，请使用 prepare_query
     * 
     * @access public
     * @param string $sql 完整sql
     * @param integer $scale 返回的数据规模，1 - 全部行、2 - 第一行、其他值 - 第一行第一列
     * @param integer $fetch 返回的数组索引类型，1-字段索引、2-数字索引、其他-两种索引并存
     * @return mixed 如果失败返回false，否则返回对应的结果
     */
    public function query($sql, $scale=1, $fetch=1) {
        $ret = false;

        $func = $this -> _get_scale_func($scale);
        $fetch_style = $this -> _get_fetch_style($fetch);
        // 如果是返回第一列，无需指定fetch类型
        $func === 'fetchColumn' and $fetch_style = 0;

        try {
            $stmt = $this -> _db -> query($sql);
            $stmt !== false and $ret = $stmt -> $func($fetch_style);
        }catch(\PDOException $e) {
            to_log(MAIN_LOG_ERROR, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
        }
        // 关闭游标
        $stmt !== false and $stmt -> closeCursor();

        return $ret;
    }

    /** 
     * 非prepare型执行方法，使用该方法容易被注入攻击，
     * 如不能保证SQL语句无注入漏洞，请使用 prepare_query
     * 
     * @access public
     * @param string $sql 完整语句
     * @param array $ret_count 是否返回受影响的记录数
     * @return mixed 如果失败返回false，否则根据 $ret_count 返回对应值
     */
    public function exec($sql, $ret_count=false) {
        $ret = false;

        try {
            $ret = $this -> _db -> exec($sql);
        }catch(\PDOException $e) {
            to_log(MAIN_LOG_ERROR, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
        }

        if ($ret === false) {
            to_log(MAIN_LOG_WARN, $this -> _log_path, __CLASS__ . " -> " . __FUNCTION__ . ": 执行SQL语句失败", "sql: {$sql}");
        }

        !$ret_count and $ret = true;

        return $ret;
    }
    // ---end------------------底层方法-----------------------------------

    // ------------------------私有实现--------------------------------
    /**
     * 获得从数据集中获取结果规模的函数名称
     *
     * @access private
     * @param integer $scale 返回的数据规模，1 - 全部行、2 - 第一行、其他值 - 第一行第一列
     * @return string
     */
    private function _get_scale_func($scale=1) {
        $func = '';
        switch ($scale) {
            case 1 : {
                $func = 'fetchAll';
                break;
            }
            case 2 : {
                $func = 'fetch';
                break;
            }
            default : {
                $func = 'fetchColumn';
                break;
            }
        }

        return $func;
    }

    /**
     * 获取pdo的fetch类型
     *
     * @access private
     * @param integer $fetch 返回的数组索引类型，1-字段索引、2-数字索引、其他-两种索引并存
     * @return string
     */
    private function _get_fetch_style($fetch=1) {
        $fetch_style = '';
        switch($fetch) {
            case 1 : {
                $fetch_style = \PDO::FETCH_ASSOC;
                break;
            }
            case 2 : {
                $fetch_style = \PDO::FETCH_NUM;
                break;
            }
            default : {
                $fetch_style = \PDO::FETCH_BOTH;
                break;
            }
        }

        return $fetch_style;
    }

    /**
     * 预处理语句转换为完整语句，方便记录日志
     *
     * @access public
     * @param string $pre_sql 预处理语句
     * @param array $param 绑定值数组，仅支持string类型
     * @return string
     */
    public function prepare_to_full($pre_sql, array $param) {
        $ret = $pre_sql;
        !is_array($param) and $param = array();

        foreach ($param as $val) {
            $ret = preg_replace('/\?/', $val, $ret, 1);
        }

        return $ret;
    }
}

/* End of this file */