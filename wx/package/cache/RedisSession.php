<?php
/**
 * session机制的redis实现
 *
 * @author JianMing Liang
 * @create 2016-01-26 10:45:39
 */
namespace package\cache;

class RedisSession {
    // 默认配置名称（使用load_config加载）
    private $_default_config_path = 'package/cache/redis_session';

    // sessions 最大生存时间，单位：秒
    public $max_life_time = 0;
    // 用作生成 session_id 的标识值
    public $key_preffix = '';

    // \package\cache\Redis 类的一个实例
    public $redis = null;
    
    /** 
     * 构造函数，自动赋予默认值
     * 
     * @access public
     * @param objcet $redis 指定一个特定的 \package\cache\Redis 实例
     * @return void
     */
    public function __construct($redis=null) {
        $conf = load_config($this -> _default_config_path);
        if (!is_array($conf) or empty($conf)) {
            to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
            return;
        }

        isset($conf['max_life_time']) and $this -> max_life_time = $conf['max_life_time'];
        isset($conf['key_preffix']) and $this -> key_preffix = $conf['key_preffix'];

        empty($redis) and $redis = g('\\package\\cache\\Redis');
        $this -> redis = $redis;
    }

    /**
     * 调用这个方法代替session_start
     *
     * @access public
     * @return void
     */
    public function start() {
        session_set_save_handler(
            array(&$this, 'open'),
            array(&$this, 'close'),
            array(&$this, 'read'),
            array(&$this, 'write'),
            array(&$this, 'destroy'),
            array(&$this, 'gc'),
            array(&$this, 'create_id')
        );

        session_start();
    }

    /**
     * 获取redis缓存变量名
     *
     * @access public
     * @param string $str 使用该字符串生成key
     * @return string
     */
    public function get_cache_key($str) {
        $key = md5($str);
        return $key;
    }

    /**
     * 自定义session_id
     *
     * @access public 
     * @return string
     */
    public function create_id() {
		$time = time();
		$ip = $this -> _get_ip();
		$uid = uniqid($this -> key_preffix, TRUE);
		$rand = get_rand_str(13);
		$key_str = 'session_id:ip:' . $ip . ':uid:' . $uid . ':time:' . $time . ':rand:' . $rand;
		$key = sha1($key_str);
		return $key;
    }

    /** 
     * session_open 将提交到该函数
     * 
     * @access public
     * @return boolean
     */
    public function open($sess_id) {
        $cache_key = $this -> get_cache_key(session_id());
        $ret = $this -> redis -> expire($cache_key, $this -> max_life_time);
        
        return $ret;
    }
    
    /** 
     * session_close将提交到该函数
     * 
     * @access public
     * @return boolean
     */
    public function close() {
        return true;
    }
    
    /** 
     * session_read将提交到该函数
     * 
     * @access public
     * @param string $sess_id session_id
     * @return mixed
     */
    public function read($sess_id) {
        $cache_key = $this -> get_cache_key($sess_id);
        $ret = $this -> redis -> get($cache_key);
        return $ret;
    }
    
    /** 
     * session_write将提交到该函数
     * 
     * @access public
     * @param string $sess_id session_id
     * @param string $sess_data session数据
     * @return boolean
     */
    public function write($sess_id, $sess_data) {
        $cache_key = $this -> get_cache_key($sess_id);
        $ret = $this -> redis -> set($cache_key, $sess_data, $this -> max_life_time);
        return $ret;
    }

    /**
     * session_destroy将提交到该函数
     * 
     * @access public
     * @param string $sess_id session_id
     * @return boolean
     */
    public function destroy($sess_id) {
        session_unset();
        $cache_key = $this -> get_cache_key($sess_id);
        $ret = $this -> redis -> delete($cache_key);

        return $ret;
    }

    /** 
     * session的gc回收将托管到该方法
     * 
     * @access public
     * @return void
     */
    public function gc() {
    }
    

	private function _get_ip(){
	    static $ip = NULL;
	    if ($ip !== NULL) return $ip;
	
	    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	        is_array($ip) and $ip = array_shift($ip);
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    if (!empty($_SERVER['REMOTE_ADDR'])) {
	        $ip = $_SERVER['REMOTE_ADDR'];
	        if (check_data($ip, 'ip')) return $ip;
	    }
	
	    $ip = '';
	    return $ip;
	}
}

/* End of this file */