<?php
/**
 * redis操作类
 *
 * @author LiangJianMing
 * @create 2016-01-05 22:10:01
 */
namespace package\cache;

class Redis {
	// 默认配置名称（使用load_config加载）
    private $_default_config_path = 'package/cache/redis';

	// redis实例对象
	private $_redis;

	// redis服务器地址
	private $_host = '';
	// redis服务器端口
	private $_port = 6379;

	private $_pass = "";
	
	/** 
	 * 构造函数
	 * 
	 * @access public
	 * @param array $conf 配置文件集合, 包含参数：
	 *				string $host 服务器地址
	 *				string $port 服务器端口
	 * @return void
	 */
	public function __construct(array $conf=array()) {
		$this -> set_conf($conf);
		$this -> reconnect(true);
	}

	/**
	 * 设置redis配置
	 * 执行前，配置会被重置为[host='', port='6379']
	 *
	 * @access public
	 * @param array $conf 配置文件集合, 包含参数：
	 *				string $host 服务器地址
	 *				string $port 服务器端口
	 * @return void
	 */
	public function set_conf(array $conf=array()) {
		if (empty($conf)) {
			$conf = load_config($this -> _default_config_path);
            if (!is_array($conf) or empty($conf)) {
                to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
                return;
            }
		}

		$this -> _host = '';
		$this -> _port = 6379;

		isset($conf['host']) and $this -> _host = $conf['host'];
        isset($conf['port']) and $this -> _port = intval($conf['port']);
        isset($conf['pass']) and $this -> _pass = $conf['pass'];
	}
	
	/** 
	 * 重新连接redis
	 * 
	 * @access public
	 * @param boolean $is_new 是否必须重新连接
	 * @return boolean
	 */
	public function reconnect($is_new=false) {
		$ret = false;
		if ($is_new) {
			$ret = $this -> _connect();
			return $ret;
		}

		$check = $this -> _is_connected();
		if (!$check) {
			$ret = $this -> _connect();
		}

		return $ret;
	}
	
	/** 
	 * 设置缓存数据, 仅支持字符串
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $value 缓存变量值
	 * @param string $ttl 缓存生存时间，单位:秒
	 * @return boolean
	 */
	public function set($key, $value, $ttl=3600) {
		$key = strval($key);
		$value = strval($value);
		$ttl = intval($ttl);

		$ttl < 0 and $ttl = 0;

		if ($key === '' or $value === '') {
			return false;
		}
		
		try {
			$result = $this -> _redis -> setex($key, $ttl, $value);
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}

	/** 
	 * 重新设置缓存变量的生存时间
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $ttl 缓存生存时间，单位:秒
	 * @return boolean
	 */
	public function expire($key, $ttl=3600) {
		$key = strval($key);
		$ttl = intval($ttl);

		$ttl < 0 and $ttl = 0;

		if ($key === '') {
			return false;
		}
		
		try {
			$result = $this -> _redis -> expire($key, $ttl);
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}

	/** 
	 * 获取缓存变量值
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return mixed 成功返回变量值，失败返回false
	 */
	public function get($key) {
		$key = strval($key);
		if ($key === '') {
			return false;
		}

		try {
			$result = $this -> _redis -> get($key);
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}
		return $result;
	}
	
	/** 
	 * 批量删除缓存变量
	 * 
	 * @access public
	 * @param mixed $key [string|array] 当为string时，自动转换为array
	 * @return boolean
	 */
	public function delete($key) {
		!is_array($key) and $key = array($key);

		$tmp_arr = array();
		foreach ($key as $val) {
			$tmp_str = strval($val);
			$tmp_str !== '' and $tmp_arr[$tmp_str] = 1;
		}
		$key = array_keys($tmp_arr);

		try {
			$ret = true;
			foreach ($key as $val) {
				$result = $this -> _redis -> delete($val);
				!$result and $ret = false;
			}
		}catch(\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}
		
		return  $ret;
	}
	
	/** 
	 * 清空redis中的所有数据
	 * 
	 * @access public
	 * @return boolean
	 */
	public function clear() {
		try {
			$result = $this -> _redis -> flushAll();
		}catch(\RedisException $e){
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}
	
	/** 
	 * 将缓存变量放入redis队列，仅支持字符串及整型
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $value 缓存变量值
	 * @param boolean $to_right 是否从右边入列
	 * @return boolean
	 */
	public function push($key, $value, $to_right=true) {
		$key = strval($key);
		$value = strval($value);
		
		if ($key === '' or $value === '') {
			return false;
		}
		
		$func = 'rPush';
		!$to_right and $func = 'lPush';

		try {
			$result = $this -> _redis -> $func($key, $value);
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}

	/** 
	 * 缓存变量出列
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param boolean $from_left 是否从左边出列
	 * @return boolean 成功返回缓存变量值，失败返回false
	 */
	public function pop($key , $from_left=true) {
		$key = strval($key);
		if ($key === '') {
			return false;
		}

		$func = 'lPop';
		!$from_left and $func = 'rPop';
		
		try {
			$result = $this -> _redis -> $func($key);
		}catch(\RedisException $e){
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result;
	}
	
	/** 
	 * 缓存变量自增
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean
	 */
	public function increase($key) {
		$key = strval($key);
		if ($key === '') {
			return false;
		}

		try {
			$result = $this -> _redis -> incr($key);
		}catch(\RedisException $e){
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}

	/** 
	 * 缓存变量自减
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function decrease($key) {
		$key = strval($key);
		if ($key === '') {
			return false;
		}

		try {
			$result = $this -> _redis -> decr($key);
		}catch(\RedisException $e){
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}
	
	/** 
	 * 判断缓存变量是否已经存在
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean 存在返回TRUE，否则返回FALSE
	 */
	public function exists($key) {
		$key = strval($key);
		if ($key === '') {
			return false;
		}

		try {
			$result = $this -> _redis -> exists($key);
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return $result ? true : false;
	}
	
	/** 
	 * 返回redis源对象
	 * 
	 * @access public
	 * @return object
	 */
	public function get_handler() {
		return $this -> _redis;
	}

	// ---------私有实现---------------------------------------------
	/**
	 * 检验并连接redis服务器
	 *
	 * @access private
	 * @return boolean
	 */
	private function _connect() {
		if (!class_exists('\Redis', false)) {
			to_log(MAIN_LOG_ERROR, '', 'Redis类不存在，可能是没有安装php_redis扩展');
			return false;
		}

		try {
			$this -> _redis = new \Redis();
            $this -> _redis -> connect($this -> _host, $this -> _port);
            $this -> _pass && $this -> _redis -> auth($this -> _pass);

		}catch(\RedisException $e){
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return true;
	}

	/** 
	 * 判断是否已连接到服务器
	 * 
	 * @access public
	 * @return boolean
	 */	
	public function _is_connected() {
		if(!is_object($this -> _redis)) return false;

		try {
			$this -> _redis -> ping();
		}catch (\RedisException $e) {
			to_log(MAIN_LOG_WARN, '', __CLASS__ . " -> " . __FUNCTION__ . ": " . $e -> getMessage());
			return false;
		}

		return true;
	}
}

/* End of this file */