<?php

/** 
 * excel工具在
 * 
 * @author yangpz
 * @create 2014-06-28
 * @version 1.0
 */

namespace package\excel;
class ExcelQy {
	// gch add
	private $file_type = '2007';

	public function set_file_type($file_type) {
		if ($file_type == 'xls') {
			$this -> file_type = '5';
		}
	}

    public function export_xlsx_well($export_data,$model,$fileName){
        $rawChar = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        $objPHPExcel = new \PHPExcel();
        $length = $rawChar[0]."1:".$rawChar[count($model)]."1";
        $objPHPExcel->getActiveSheet()->getStyle($length)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);
        //$objPHPExcel -> getActiveSheet() -> getColumnDimension() -> setAutoSize(true);
        $j = 0;
        foreach ($model as $key=>$item){
            //设置单元格标题
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($rawChar[$j].'1', $item);
            //设置单元格左对齐
            $objPHPExcel->getActiveSheet()->getStyle($rawChar[$j])->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
            $j++;
        }

        $i=1;
        foreach ($export_data as $data){
            $i++;
            $j=0;
            foreach ($model as $key=>$item){
                $objPHPExcel->getActiveSheet()->getStyle($rawChar[$j].$i)->getAlignment()->setWrapText(true);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($rawChar[$j].$i, $data[$key]);
                $j++;
            }
        }

        $fileName = $fileName?$fileName:'Export_'.date('YmdHi',time());
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xlsx"');
        header("Content-Disposition:attachment;filename=$fileName.xlsx");//attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
	/**
	 * 导出自由排版模板
	 */
	public function export_xlsx($cells, $data, $remind, $work_str, $item_list, $filename='sc_workshift_export_sample') {
		// Create new PHPExcel object
		$objPHPExcel = new \PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SanChi")
									 ->setLastModifiedBy("SanChi")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("Test result file");

		$objPHPExcel -> setActiveSheetIndex(0);
		//获取当前活动sheet
		$objSheet = $objPHPExcel -> getActiveSheet();

		//设置默认字体大小和格式
		$objSheet -> getDefaultStyle() -> getFont() -> setSize(12) -> setName("新宋体");
		//设置默认列宽
		$objSheet -> getDefaultColumnDimension() -> setWidth(13);
		$objSheet -> getStyle("A3:A".(count($data)+2))-> getNumberFormat() -> setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		
		//设置第一、二行行高
		$objSheet -> getRowDimension(1) -> setRowHeight(36);
		//设置第二行行高
		$objSheet -> getRowDimension(2) -> setRowHeight(36);

		//设置第一、二行字体大小和加粗
		$styleArray = array(
			'font' => array(
				'size' => 14,
				'bold' => true,
				'color' => array(
					'rgb' => 'ff0000'
					)
				),
			'alignment' => array(
				// 'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical'  => \PHPExcel_Style_Alignment::VERTICAL_CENTER
				),
			'borders' => array(
				'top'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'bottom'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					)
				),
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'rotation' => 90,
				'startcolor' => array(
					'rgb' => 'd9e4bc',
					),
				),
			);

		$objSheet -> getStyle("A1:".$cells[count($cells)-1].'2') -> applyFromArray($styleArray);
		$objSheet -> getStyle("A1:".$cells[count($cells)-1].'1') -> getBorders() -> getBottom() -> setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
		$objSheet -> getStyle("A2:".$cells[count($cells)-1].'2') -> getFont() -> getColor() -> setRGB('963734');

		//合并第一、二行
		$objSheet -> mergeCells("A1:".$cells[count($cells)-1].'1');
		$objSheet -> mergeCells("A2:".$cells[count($cells)-1].'2');

		//设置第三行样式（表头）
		$t_styleArray = array(
			'font' => array(
				'bold' => TRUE,
				'color' => array(
					'rgb' => '35615d'
					)
				),
			'alignment' => array(
				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => \PHPExcel_Style_Alignment::VERTICAL_CENTER
				),
			'borders' => array(
				'inside'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'top' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'bottom' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					)
				),
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'rotation' => 90,
				'startcolor' => array(
					'rgb' => '8db5e2',
					),
				),
			);
		$objSheet -> getStyle('A3:'.$cells[count($cells)-1].'3') -> applyFromArray($t_styleArray);

		//设置提醒行
		$objSheet -> setCellValue('A1', $remind) -> setCellValue('A2', $work_str);
		//冻结A、B列和前三行
		$objSheet -> freezePane("C4");
		
		$r = 3;
		foreach($data as $d){
			$i = 1;
			foreach ($d as $cell => $val) {
				$objSheet -> setCellValue($cell, $val);
				if ($i > 2 && $r > 3) {
					//设置下拉框
					$this -> set_Formula1($objSheet, $cell, $item_list);
				}
				
				$i++;
			}
			//设置行高
			if ($r == 3) {
				$objSheet -> getRowDimension($r) -> setRowHeight(30);
			} else {
				$objSheet -> getRowDimension($r) -> setRowHeight(20);
			}
			$r++;

		}unset($d);

		// 生成excel文件
		$objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);

		//浏览器数据excel07文件
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//输出文件的名称
		header("Content-Disposition: attachment;filename={$filename}.xlsx");
		//禁止缓存
		header('Cache-Control: max-age=0');

		$objWriter -> save("php://output");
	}


	/**
	 * 导出年假模板
	 */
	public function export_year_xlsx($data, $remind, $filename='sc_annual_leave_sample') {
		// Create new PHPExcel object
		$objPHPExcel = new \PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SanChi")
									 ->setLastModifiedBy("SanChi")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("Test result file");

		$objPHPExcel -> setActiveSheetIndex(0);
		//获取当前活动sheet
		$objSheet = $objPHPExcel -> getActiveSheet();

		//设置默认字体大小和格式
		$objSheet -> getDefaultStyle() -> getFont() -> setSize(12) -> setName("新宋体");
		$objSheet -> getDefaultStyle() -> getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		//设置默认列宽
		$objSheet -> getDefaultColumnDimension() -> setWidth(13);
		$objSheet -> getColumnDimension('C') -> setWidth(26);
		$objSheet -> getStyle("A3:A".(count($data)+1))-> getNumberFormat() -> setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		
		//设置第一、二行高
		$objSheet -> getRowDimension(1) -> setRowHeight(62);
		$objSheet -> getRowDimension(2) -> setRowHeight(30);

		//设置第一行字体大小和加粗
		$styleArray = array(
			'font' => array(
				'size' => 14,
				'bold' => true,
				'color' => array(
					'rgb' => 'ff0000'
					)
				),
			'alignment' => array(
				'vertical'  => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap' => TRUE
				),
			'borders' => array(
				'top'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'bottom'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					)
				),
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'rotation' => 90,
				'startcolor' => array(
					'rgb' => 'd9e4bc',
					),
				),
			);

		$objSheet -> getStyle("A1:Z1") -> applyFromArray($styleArray);
		//合并第一行
		$objSheet -> mergeCells("A1:Z1");

		//设置第二行样式（表头）
		$t_styleArray = array(
			'font' => array(
				'bold' => TRUE,
				'color' => array(
					'rgb' => '35615d'
					)
				),
			'alignment' => array(
				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
				),
			'borders' => array(
				'inside'  => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'top' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					),
				'bottom' => array(
					'style' => \PHPExcel_Style_Border::BORDER_THIN,
					)
				),
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'rotation' => 90,
				'startcolor' => array(
					'rgb' => '8db5e2',
					),
				),
			);
		$objSheet -> getStyle('A2:Z2') -> applyFromArray($t_styleArray);

		//设置提醒行
		$objSheet -> setCellValue('A1', $remind);
		
		//冻结前两行
		$objSheet -> freezePane("A3");

		$r = 2;
		foreach($data as $d){
			$j = 1;
			foreach ($d as $cell => $val) {
				$objSheet -> setCellValue($cell, $val);
				$r > 2 && $j == 4 && $objSheet -> getStyle($cell) -> getAlignment() -> setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$j++;
			}
			//设置行高
			$r > 2 && $objSheet -> getRowDimension($r) -> setRowHeight(20);
			$r++;
		}unset($d);

		// 生成excel文件
		$objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);

		//浏览器数据excel07文件
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//输出文件的名称
		header("Content-Disposition: attachment;filename={$filename}.xlsx");
		//禁止缓存
		header('Cache-Control: max-age=0');

		$objWriter -> save("php://output");
	}

	/**
	 * 插入错误批注输出excel表
	 * @param  [type] $file_path 
	 * @param  array  $err_data  
	 * @return [type]            
	 */
	public function export_err_ws_plan($file_path, $err_data=array(), $filename=''){
		$objReader =new \PHPExcel_Reader_Excel2007();//实例化一个读取对象
		$objPHPExcel = $objReader -> load($file_path);
		$objSheet = $objPHPExcel -> getSheet(0);

		foreach ($err_data as $cell => $val) {
			$this -> set_TextRun($objSheet, $cell, $val);
		}unset($val);

		// 生成excel文件
		$objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);

		$file_path = explode('/', $file_path);
		unset($file_path[count($file_path)- 1]);
		$file_path = $save_file = implode('/', $file_path)."/{$filename}";
		// windows下中文文件名要转编码
		// $file_path = iconv('utf-8', 'gbk', $save_file);
		$objWriter -> save($file_path);

		return $save_file;

		// //浏览器数据excel07文件
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// //输出文件的名称
		// header("Content-Disposition: attachment;filename={$filename}.xlsx");
		// //禁止缓存
		// header('Cache-Control: max-age=0');

		// $objWriter -> save("php://output");
	}
	
	/**
	 * 添加批注
	 * @param [type] $sheet 
	 * @param [type] $cell  
	 */
	private function set_TextRun(&$sheet, $cell, $text_run='') {
		if (empty($text_run)) return;
		
		$sheet -> getStyle($cell) -> getFill() -> setFillType(\PHPExcel_Style_Fill::FILL_SOLID) -> getStartColor() -> setRGB('ffff00');
		$sheet -> getComment($cell) -> setAuthor('');
		$sheet -> getComment($cell) -> getText() -> createTextRun('') -> getFont() -> setBold(TRUE);
		$sheet -> getComment($cell) -> getText() -> createTextRun("\r\n");
		$sheet -> getComment($cell) -> getText() -> createTextRun($text_run);
	}

	/**
	 * 添加数据验证项
	 * @param [type] &$sheet 
	 * @param [type] $cell   
	 * @param [type] $item   
	 */
	private function set_Formula1(&$sheet, $cell, $item) {
		$objValidation = $sheet -> getCell($cell) -> getDataValidation();
		$objValidation -> setType(\PHPExcel_Cell_DataValidation::TYPE_LIST)
						-> setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP)
						-> setAllowBlank(TRUE)
						-> setShowInputMessage(TRUE)
						-> setShowErrorMessage(TRUE)
						-> setShowDropDown(TRUE)
						// -> setErrorTitle('pick a value')
						// -> setError('pick a value')
						// -> setPromptTitle('pick a value')
						// -> setPrompt('Please pick a value from the drop-down list.')
						-> setFormula1('"'.$item.'"');	

	}


	public function export_html($file_name){
		$objReader =new \PHPExcel_Reader_Excel5();//实例化一个读取对象
		$objPHPExcel = $objReader -> load($file_name);
		$sheet = $objPHPExcel -> getSheet(0);
		$sheet -> mergeCells("A2:H2");

		for($i = 1; $i <= 4; $i++) {
			$row_data = $sheet -> rangeToArray("A{$i}:Z{$i}");
			if (!is_array($row_data) || empty(implode('', $row_data[0]))) {
				continue;
			}
			$row = 'A';
			foreach ($row_data[0] as &$item) {
				if(!empty($item)){
					$item = trim($item);
					$sheet -> setCellValue("{$row}{$i}", $item);
				}
				$row++;
			}
		}

		$objWriteHTML =new \PHPExcel_Writer_HTML($objPHPExcel); //读取excel文件，并将它实例化为PHPExcel_Writer_HTML对象
		//在页面上打印（这里会直接打印，没有返回值。需要返回值的童鞋请根据save()方法自行改写）
		$objWriteHTML->save("php://output");
	}

	/**
	 * 导出数据到EXCEL 2003
	 * @param unknown_type $data			要导出的数据，二维数组形式，如：array(array('name' => 'yanghd', 'acct' => 'sc_yanghd'), array('name' => 'yangpz', 'acct' => 'sc_yangpz'),);
	 * @param unknown_type $fields_key		数据中各记录的KEY值，如：array('name', 'acct'), array('姓名', '账号')
	 * @param unknown_type $fields_name		表头（中文），如：array('姓名', '账号)
	 */
	public function export($data, $fields_key, $fields_name) {
		$this -> check_data_count(count($data));
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel 	-> getProperties() 
						-> setCreator("Maarten Balliauw")
						->setLastModifiedBy("Maarten Balliauw")
						->setTitle("Office 2003 XLS Test Document")
						->setSubject("Office 2003 XLS Test Document")
						->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
						->setKeywords("office 2003 openxml php")
						->setCategory("Test result file");
	
		$rows_name = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		for ($i=0; $i<count($fields_key); $i++) {	//设置文件头
			$cell_style = $objPHPExcel -> getActiveSheet() -> getStyleByColumnAndRow($i, 1);
			$cell_style -> getFont() -> setBold(true);
			$cell_style -> getFill() -> setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$cell_style -> getFill() -> getStartColor() -> setARGB('EE62C462');
			$objPHPExcel -> getActiveSheet() -> setCellValue($rows_name[$i].'1', $fields_name[$i]);
			$this -> set_center($objPHPExcel, $rows_name[$i].'1');
		}
		
		for ($i=0; $i<count($data); $i++) {
			for ($j=0; $j<count($fields_key); $j++) {
				$objPHPExcel -> getActiveSheet() -> setCellValue($rows_name[$j].($i+2), $data[$i][$fields_key[$j]]);
			}
		}
		
		
		$file_name = date('y-m-d h_i_s', time()).'.xlsx';
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Pragma: no-cache");
		
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');  
		header('content-disposition:attachment;filename="' .$file_name . '"');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    iconv('utf-8', 'gb2312', $file_name);
	    ob_flush();
		ob_clean();
		$objWriter->save('php://output');
	}
	
	/**
	 * 简单的excel 2007导出(先生成csv，再转换成excel，速度较快，耗内存较少，缺点：无法设置边框等所有样式)
	 * @param array $data				数据源
	 * @param array $model				导出模板（如：表头数据）
	 * @param unknown_type $file_name	文件名(不需带扩展名)，默认为: export
	 * @param unknown_type $download	生成文件后是否直接下载，默认为TRUE
	 */
	public function simple_export_xlsx(array $data, array $model, $file_name='export', $download=TRUE) {
		$this -> check_data_count(count($data));
		
		$tmp_filename = uniqstr().'_'.time().'.csv';	//临时文件名
		$tmp_file = SYSTEM_DATA.$tmp_filename;			//临时文件路径
		
		$fp = fopen($tmp_file, 'x');
		fputcsv($fp, $model['title']);					//插入表头
		foreach ($data as $a) {
			$a = is_array($a) ? $a : array();
			fputcsv($fp, $a);
		}
		unset($a);
		fclose($fp);
		
		$objReader = \PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader -> load($tmp_file);
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
		
		if ($download) {
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
			header("Content-Type:application/force-download");
			header("Content-Type:application/vnd.ms-execl");
			header("Content-Type:application/octet-stream");
			header("Content-Type:application/download");;
			header('Content-Disposition:attachment;filename="'.$file_name.'.xlsx"');
			header("Content-Transfer-Encoding:binary");
			
			unlink($tmp_file);
			$objWriter -> save('php://output');
			
		} else {
			$file_path = str_replace('.csv', '.xlsx', $tmp_file);
			$objWriter -> save($file_path);
			return $file_path;
		}
		
	}

	/** 利用XML导出(支持批注) */
	public function XML_export_xlsx($file_name, $title=array(), $data=array(), $modal_title=array(),$is_down=true){
		$content = '';
		//填充标题
		if(!empty($title)){
			foreach ($title as $row) {
				$content .='<Row ss:AutoFitHeight="0" ss:Height="25">';
				foreach ($row as $til) {
					$index = isset($til['index']) ? 'ss:Index="'.$til['index'].'"' : "";
					$col = isset($til['col']) ? 'ss:MergeAcross="'.$til['col'].'"' : '';
					$row = isset($til['row']) ? 'ss:MergeDown="'.$til['row'].'"' : '';
					$content .= '<Cell ss:StyleID="s24" '.$index.' '.$col.' '.$row.'><Data ss:Type="String">'.$til['name'].'</Data></Cell>';
				}
				$content .="</Row>";
			}unset($row);
		}
		$file = MAIN_DATA.'tmp/'.$file_name;
		file_put_contents($file, self::$XML_HEADER.$content);
		//填充数据
		foreach ($data as &$row) {
			$content = '<Row ss:AutoFitHeight="0" ss:Height="20">';
			$other_row = array();
			foreach ($modal_title as $key => $val) {
				if(preg_match('/^[\^]{1}/', $key)){
					$key = preg_replace('/^[\^]/', '', $key);
					$krow = array_shift($row[$key]);
					foreach ($val as $k => $v) {
						$index = isset($v['index']) ? 'ss:Index="'.$v['index'].'"' : "";
						$col = isset($v['col'])  && $v['col'] =='n' ? 'ss:MergeAcross="'.$row['col'].'"' : '';
						$cell_row = isset($v['row']) && $v['row'] =='n' ? 'ss:MergeDown="'.$row['row'].'"' : '';

						if (is_array($krow[$k]) && isset($krow[$k]['val']) && isset($krow[$k]['textRun'])) {
							$textRun = $this->set_xml_TextRun($krow[$k]['textRun']);
							$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$krow[$k]['val'].'</Data>'.$textRun.'</Cell>';
						
						} else {
							$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$krow[$k].'</Data></Cell>';
						}

					}
					if(!empty($row[$key])){
						array_push($other_row, $key);
					}
				}else{
					$index = isset($val['index']) ? 'ss:Index="'.$val['index'].'"' : "";
					$col = isset($val['col'])  && $val['col'] =='n' ? 'ss:MergeAcross="'.$row['col'].'"' : '';
					$cell_row = isset($val['row']) && $val['row'] =='n' ? 'ss:MergeDown="'.$row['row'].'"' : '';

					if (is_array($row[$key]) && isset($row[$key]['val']) && isset($row[$key]['textRun'])) {
						$textRun = $this->set_xml_TextRun($row[$key]['textRun']);
						$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$row[$key]['val'].'</Data>'.$textRun.'</Cell>';

					} else {
						$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$row[$key].'</Data></Cell>';
					}
				}
			}
			$content .="</Row>";

			if(!empty($other_row)){
				$this -> set_other_row($content, $row, $other_row, $modal_title);
			}
			file_put_contents($file, $content, FILE_APPEND);
		}unset($row);

		file_put_contents($file, self::$XML_FOOTER, FILE_APPEND);
		$fp = fopen($file, "r"); 
		$file_size = filesize($file); 

		if($is_down){
            header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
            header("Content-Disposition: inline; filename=".$file_name);
            header("Content-Transfer-Encoding: binary");
            header("Pragma: public");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            $buffer=1024;
            $file_count=0;
            //向浏览器返回数据
            while(!feof($fp) && $file_count<$file_size){
                $file_con=fread($fp,$buffer);
                $file_count+=$buffer;
                echo $file_con;
            }
            fclose($fp);
            unlink($file);
        }

		return $file;
	}

	private function set_other_row(&$content, &$row, $other_row, $modal_title){
		$other_row_tmp = array();
		$content .= '<Row  ss:AutoFitHeight="0" ss:Height="20">';
		foreach ($other_row as $key) {
			$krow = array_shift($row[$key]);
			$val = $modal_title['^'.$key];
			foreach ($val as $k => $v) {
				$index = isset($v['index']) ? 'ss:Index="'.$v['index'].'"' : "";
				$col = isset($v['col'])  && $v['col'] =='n' ? 'ss:MergeAcross="'.$row['col'].'"' : '';
				$cell_row = isset($v['row']) && $v['row'] =='n' ? 'ss:MergeDown="'.$row['row'].'"' : '';

				if (is_array($krow[$k]) && isset($krow[$k]['val']) && isset($krow[$k]['textRun'])) {
					$textRun = $this->set_xml_TextRun($krow[$k]['textRun']);
					$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$krow[$k]['val'].'</Data>'.$textRun.'</Cell>';
				} else {
					$content .= '<Cell ss:StyleID="s21" '.$index.'  '.$col.' '.$cell_row.'><Data ss:Type="String">'.$krow[$k].'</Data></Cell>';
				}

			}unset($v);
			if(!empty($row[$key])){
				array_push($other_row_tmp, $key);
			}
		}unset($key);
		$content .="</Row>";
		if(!empty($other_row_tmp)){
			$this -> set_other_row($content, $row, $other_row, $modal_title);
		}
	}

	/**
	 * 返回xml表格的批注信息
	 * @param array $textRun 数组里的每个值为批注的一行
	 */
	private function set_xml_TextRun($textRun) {
		if (empty($textRun)) return '';
		!is_array($textRun) && $textRun = array($textRun);
		$comment = '<Comment ss:Author=""><ss:Data xmlns="http://www.w3.org/TR/REC-html40"><B><Font
         html:Size="9" html:Color="#000000"></Font></B><Font html:Size="9"
        html:Color="#000000">&#10;';

        foreach ($textRun as $text) {
        	$comment .= $text .'&#10;';
        }unset($text);

        $comment = substr_replace($comment, '', -1, 5);
        $comment .= '</Font></ss:Data></Comment>';
        return $comment;
	}

	/**
	 * 分段excel 2007导出（耗时合格耗内存，多用户系统下不使用该方法）
	 * @param array $data				数据源
	 * @param array $model				导出模板（如：表头数据）
	 * 
	 * @param array $conf				配置项，具体如下
	 * per_count			每次记录的数量，默认为：100
	 * offset				数据集起始下载（从0开始），默认为：0
	 * offset_row			数据记录起始行数（从1开始），若有表头，则需要加上表头所占行数，默认为：1 (即无表头)
	 * is_start				是否第一批数据（用于判断是否录入表头），默认为：FALSE
	 * is_end				是否最后一批数据，默认为：TRUE
	 * tmp_file_name		临时文件的名称(带扩展名)，需要唯一，默认自动生成
	 * expoprt_file_name	最终导出的文件的名称(带扩展名)，默认与临时文件名相同
	 */
	public function part_export_xlsx(array $data, array $model, array &$conf=array()) {
		$this -> check_data_count(count($data));
		
		$per_max = 100;					//每次记录的记录数的上限
		$per_count			= isset($conf['per_count']) ? $conf['per_count'] : $per_max;		
		$offset 			= isset($conf['offset']) ? $conf['offset'] : 0;
		$offset_row 		= isset($conf['offset_row']) ? $conf['offset_row'] : 1;
		$is_start 			= isset($conf['is_start']) ? $conf['is_start'] : FALSE;
		$is_end 			= isset($conf['is_end']) ? $conf['is_end'] : TRUE;
		$tmp_file_name 		= (isset($conf['tmp_file_name']) && !empty($conf['tmp_file_name'])) ? $conf['tmp_file_name'] : uniqstr().'_'.time().'.xlsx';	//临时文件名
		$expoprt_file_name 	= isset($conf['expoprt_file_name']) ? $conf['expoprt_file_name'] : $tmp_file_name;
		
		$conf['tmp_file_name'] = $tmp_file_name;
		$conf['expoprt_file_name'] = $expoprt_file_name;
		
		$path_perfix = SYSTEM_DATA;								//临时文件根目录
		$root_dir = dirname(dirname(SYSTEM_ROOT));
		$data_dir = substr(SYSTEM_DATA, strlen($root_dir));
		$data_dir = ltrim($data_dir, '/');
		
		$url_perfix = SYSTEM_HTTP_DOMAIN.$data_dir;
		$tmp_file_path = $path_perfix.$tmp_file_name;			//临时文件路径
		$tmp_file_url = $url_perfix.$tmp_file_name;				//临时文件下载路径
	
		if ($per_count > $per_max) {
			throw new \Exception('每次录入文件的记录数不能超过'.$per_max.'条');
		}
		
		PHPExcel_Settings::setCacheStorageMethod(PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip);
		if ($is_start) {
			$objPHPExcel = new PHPExcel();
			$objPHPExcel 	-> getProperties() 
							-> setCreator("Maarten Balliauw")
							-> setLastModifiedBy("Maarten Balliauw")
							-> setTitle("Office 2007 XLS Document")
							-> setSubject("Office 2007 XLS Document")
							-> setDescription("Document for Office 2007 XLS, generated using PHP classes")
							-> setKeywords("Office 2007 openxml php")
							-> setCategory("Excel export file");
							
		} else if (!file_exists($tmp_file_path)) {
			throw new \Exception('文件不存在');
			
		} else {
			$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
			$objPHPExcel = $objReader -> load($tmp_file_path);
		}
		
		$sheet = $objPHPExcel -> getActiveSheet();
		
		if ($is_start) {
			//填充表头
			foreach ($model['title'] as $key => $title) {
				$key_array = explode(':', $key);
				$sheet -> setCellValue($key_array[0], $title);
				if (count($key_array) == 2) {
					$sheet-> mergeCells($key);
					$this -> set_border($sheet, $key);
				} else {
					$this -> set_border($sheet, $key.':'.$key);
				}
				$this -> set_center($sheet,$key);
			}
			unset($key); unset($title);
		}
		
		//填写记录
		$curr_row = $offset_row-1;
		$curr_col = $model['start_col']-1;
		$target = count($data);		//最后一条记录的下标
		for ($i=0; $i<$target; $i++) {
			$recode = $data[$i];
			$this -> set_row($model['row'], $recode, $sheet, ++$curr_row, ++$curr_col);
		}
		unset($recode);
		
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
	    ob_flush();
		ob_clean();
		$objWriter -> save($tmp_file_path);
		
		if ($is_end) {
			return array('file_name' => $tmp_file_name, 'file_url' => $tmp_file_url);
			
		} else {
			return array('file_name' => $tmp_file_name, 'file_url' => 'creating');
		}
	}
	
	/**
	 * 获取数据总行数
	 * @param unknown_type $file_path	文件路径
	 */
	public function get_count_from_xlsx($file_path) {
		if (!file_exists($file_path)) {
			throw new \Exception('找不到文件');
		}
		
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($file_path);				//读取文件
		$sheet = $objPHPExcel -> getSheet(0);
		$ret = $sheet -> getHighestDataRow();
		return $ret;
	}
	
	/**
	 * 分段读取excel数据
	 * @param unknown_type $file		文件本地路径
	 * @param unknown_type $offset_row	起始行
	 * @param unknown_type $start_col	起始列: 如：'A'
	 * @param unknown_type $end_col		终止列: 如：'E'
	 * @param unknown_type $is_end		是否执行本次后就终止
	 * @param unknown_type $limit		每次读取量，默认为：50，上限也为50
	 * @param unknown_type $delete		读取完成后是否删除该文件，默认为：TRUE
	 * @param unknown_type $trim		是否去掉数据的前后空白字符，默认为：TRUE
	 * @param unknown_type $remove 		读取之后是否删除已读取的数据，默认是false
 	 */
	public function part_read_xlsx($file, $offset_row, $start_col, $end_col, $is_end, $limit=50, $delete=TRUE, $trim=TRUE) {
		if ($limit > 50) {
			throw new \Exception('每次最多读取50条记录');
		}
		
		if (!file_exists($file)) {
			throw new \Exception('找不到文件');
		}
		
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($file);				//读取文件
		$sheet = $objPHPExcel -> getSheet(0);
		$xls_data = array();
		$end_row = $offset_row + $limit;
		for ($i=$offset_row; $i<$end_row; $i++) {
			$row_data = $sheet -> rangeToArray("{$start_col}{$i}:{$end_col}{$i}");
			if (!is_array($row_data) || empty(implode('', $row_data[0]))) {
				continue;
			}
			//去掉空白字符
			if ($trim) {
				foreach ($row_data[0] as &$item) {
					$item = trim($item);
				}
			}
			$xls_data[] = $row_data[0];
		}

		if ($is_end && $delete) {
			unlink($file);
		}
		return $xls_data;
	}
	

	/**
	 * 导出报销数据
	 * @param unknown_type $data			要导出的数据，二维数组形式，如：array(array('name' => 'yanghd', 'acct' => 'sc_yanghd'), array('name' => 'yangpz', 'acct' => 'sc_yangpz'),);
	 * @param unknown_type $model			模板
	 */
	public function model_export($data, $model,$file_name="") {
		$this -> check_data_count(count($data));
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel 	-> getProperties() 
						-> setCreator("Maarten Balliauw")
						-> setLastModifiedBy("Maarten Balliauw")
						-> setTitle("Office 2007 XLS Test Document")
						-> setSubject("Office 2007 XLS Test Document")
						-> setDescription("Test document for Office 2007 XLS, generated using PHP classes.")
						-> setKeywords("office 2007 openxml php")
						-> setCategory("Test result file");
		// -------------------开始填充数据------------------
		
		$sheet = $objPHPExcel -> getActiveSheet();
		
		//填充表头
		foreach( $model['title'] as $key => $title){
			$key_array = explode(':', $key);
			$sheet -> setCellValue($key_array[0],$title);
			if(count($key_array) == 2){
				$sheet-> mergeCells($key);
				$this -> set_border($sheet,$key);
			}else{
				$this -> set_border($sheet,$key.':'.$key);
			}
			$this -> set_center($sheet,$key);
		}
		unset($title);
		
		//填写记录
		$curr_row = $model['start_row'];
		$curr_col = $model['start_col'];
		$recode_row = 1;
		foreach( $data as $recode){
			$this -> set_row($model['row']['once'], $recode, $sheet, $curr_row, $curr_col, $recode['row'],$recode_row);
			foreach($model['row']['loop'] as $key => $value){
				$data = $recode[$value['value']];
				$row = $value['r'] + $curr_row;
				$col = $value['c'];
				$order = 1;
				foreach($data as $d){
					$this -> set_row($value['row'], $d, $sheet, $row, $col, $recode['row'],$order);
					$rule_array = explode('.', $value['rule']);
					$col = $this -> char_add($col,$rule_array[0]);
					$row += $rule_array[1];
					$order++;
				}
				unset($d);
			}
			unset($value);
			$curr_row +=$recode['row'];
			$recode_row++;
		}
		unset($recode);
		
		empty($file_name) && $file_name = date('y-m-d h_i_s', time()).'.xlsx';
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Pragma: no-cache");
		
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');  
		header('content-disposition:attachment;filename="' .$file_name . '"');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
	    iconv('utf-8', 'gb2312', $file_name);
	    ob_flush();
		ob_clean();
		$objWriter->save('php://output');
	}
	
	/**
	 * 按模板配置填充数据
	 * @param unknown_type $model		模板配置
	 * @param unknown_type $recode		行数据（要填充的数据）
	 * @param unknown_type $sheet		要填充到的sheet
	 * @param unknown_type $curr_row	当前行
	 * @param unknown_type $curr_col	当前列
	 * @param unknown_type $r			
	 * @param unknown_type $order		
	 */
	private function set_row($model, $recode, $sheet, $curr_row, $curr_col, $r=1, $order=0){
		foreach($model as $key => $val) {
			$key_array = explode('.', $key);
			$key_array[0] = ($key_array[0] =='$' ? $this -> char_add($curr_col,$r-1) : ( is_numeric($key_array[0]) ? $this -> char_add($curr_col,$key_array[0]) : $key_array[0]));
			$key_array[1] = ($key_array[1] =='$' ? ($curr_row + $r-1) : ( $key_array[1] + $curr_row));
			$key_array[2] = ($key_array[2] =='$' ? $this -> char_add($curr_col,$r-1) : ( is_numeric($key_array[2]) ? $this -> char_add($curr_col,$key_array[2]) : $key_array[2]));
			$key_array[3] = ($key_array[3] =='$' ? ($curr_row + $r-1) : ( $key_array[3] + $curr_row));
			$xy = $key_array[0].$key_array[1].':'.$key_array[2].$key_array[3];
			$sheet-> mergeCells($xy);
			if($val == '$'){
				$sheet -> setCellValue($key_array[0].$key_array[1],$order);
			}else{
				$sheet -> setCellValue($key_array[0].$key_array[1],$recode[$val]);
			}
			$this -> set_center($sheet,$xy);
			$this -> set_border($sheet,$xy);
		}
		unset($key); unset($val); unset($key_array);
	}
	
	/**
	 * 计算字符相加的方法
	 */
	private function char_add($char,$num){
		return chr(ord($char) + $num);
	}
	
	
	/**
	 * 按照模板导出数据到EXCEL 2007
	 * @param unknown_type $data			要导出的数据，二维数组形式，如：array(array('name' => 'yanghd', 'acct' => 'sc_yanghd'), array('name' => 'yangpz', 'acct' => 'sc_yangpz'),);
	 * @param unknown_type $model			模板
	 */
	public function new_export($data, $modal) {
		$this -> check_data_count(count($data));
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel 	-> getProperties() 
						-> setCreator("Maarten Balliauw")
						->setLastModifiedBy("Maarten Balliauw")
						->setTitle("Office 2007 XLS Test Document")
						->setSubject("Office 2007 XLS Test Document")
						->setDescription("Test document for Office 2007 XLS, generated using PHP classes.")
						->setKeywords("office 2007 openxml php")
						->setCategory("Test result file");
		// -------------------开始填充数据------------------
		
		//当前行数
		$curr_row = 1;
		$curr_col = 'A';
		$sheet = $objPHPExcel -> getActiveSheet();
		foreach( $data as $recode){
			foreach($modal as $row){
				if($row['type'] == 'row'){
					$this -> set_row_val($recode, $row['model'], $sheet, $curr_row, $curr_col);
				}else if($row['type'] == 'option'){
					foreach($recode[$row['value']] as $val){
						$this -> set_row_val($val,$row['model'], $sheet, $curr_row, $curr_col);
						$curr_row++;
						$curr_col = 'A';
					}
					unset($val);
					$curr_row--;
				}else if($row['type'] == 'blank'){
				}
				$curr_row++;
				$curr_col = 'A';
			}
			unset($row);
			$curr_row++;
		}
		unset($recode);
		
		$file_name = date('y-m-d h_i_s', time()).'.xlsx';
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Pragma: no-cache");
		
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');  
		header('content-disposition:attachment;filename="' .$file_name . '"');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
	    iconv('utf-8', 'gb2312', $file_name);
	    ob_flush();
		ob_clean();
		$objWriter->save('php://output');
	}
	
	//填写一行数据(根据模板填充数据)
	private function set_row_val($recode,$row_modal,&$sheet,&$curr_row,&$curr_col){
		foreach($row_modal as $cell){
			if(!isset($cell['val'])){
				$sheet -> setCellValue($curr_col.$curr_row,$cell['title']);
			}else{
				$sheet -> setCellValue($curr_col.$curr_row,$recode[$cell['val']]);
			}
			$this -> set_center($sheet,$curr_col.$curr_row);
			$this -> set_border($sheet,$curr_col.$curr_row.':'.chr(ord($curr_col) + $cell['col'] - 1).$curr_row);
			
			if($cell['col'] != 1){
				$sheet-> mergeCells($curr_col.$curr_row.':'.chr(ord($curr_col) + $cell['col'] - 1).$curr_row);
			}
			$curr_col = chr(ord($curr_col)+$cell['col']);
		}
		unset($cell);
	}
	
	private function set_border(&$sheet,$cell){
		if(!strpos($cell,':'))return;
		$styleThinBlackBorderOutline = array( 
			'borders' => array (
				'outline' => array ( 
					'style' => PHPExcel_Style_Border::BORDER_THIN, //设置border样式 'color' => array ('argb' => 'FF000000'), //设置border颜色 
				), 
			),
		);
		$sheet -> getStyle($cell) -> applyFromArray($styleThinBlackBorderOutline);
	}
	
	/**
	 * 导入数据到EXCEL 2007
	 * @param unknown_type $data			要导入的文件对象 $_FILES['filename']
	 * @param unknown_type $indata		           返回文件内容二维数组，若文件为空或者文件格式不对，返回false
	 */
	public function input($data) {
        //判断文件类型
	    if($data['type']!= "application/vnd.ms-excel" && 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'!=$data['type'])
		   return false;
		   
		$PHPExcel = new PHPExcel();
		$PHPReader = new PHPExcel_Reader_Excel2007();
		$PHPExcel = $PHPReader->load($data['tmp_name']);//读取文件

		$indata = $PHPExcel -> getSheet(0) -> toArray();//转为array
		
		if($indata)
		   return $indata;
		else 
		   return false;
	}
	 
	/**
	  * 读取csv文件
	  * @param unknown_type $filename	文件路径
	  * @param unknown_type $start		起始记录行号
	  * @param unknown_type $size		读取的行的数量，0为不限制
	  * @param unknown_type $trim		是否去掉前后空格，默认为TRUE
	  * @param unknown_type $is_gbk		源文件是否为GBK格式，默认为TRUE
	  * @param unknown_type $trim_mark	是否过滤前后引号后再过滤空白字符，默认为FALSE
	  * @return 数据数组
	  */
	public function read_from_csv($filename, $start=0, $size=0, $trim=TRUE, $is_gbk=TRUE, $trim_mark=FALSE) {
	 	$data = array();
	 	try {
			$spl_object = new \SplFileObject($filename, 'r+');
			$spl_object -> seek($start);
			if ($size == 0) {	//读完文件
				while (!$spl_object -> eof()) {
					$line_data = $this -> _csv_get_line_data($spl_object -> current(), $trim, $is_gbk);
					!$this -> empty_line($line_data) && array_push($data, $line_data);
					$spl_object -> next();
				}
			 	
			} else {			//只读size行
					$line_data = $this -> _csv_get_line_data($spl_object -> current(), $trim, $is_gbk);
				!$this -> empty_line($line_data) && array_push($data, $line_data);
				
				while (--$size && !$spl_object -> eof()) {
					$spl_object -> next();
					$line_data = $this -> _csv_get_line_data($spl_object -> current(), $trim, $is_gbk);
					!$this -> empty_line($line_data) && array_push($data, $line_data);
				}
				
			}
			if ($trim_mark) {
				foreach ($data as &$item) {
					foreach ($item as &$val) {
						if (substr($val, 0, 1) == '"')				$val = substr($val, 1);
						if (substr($val, strlen($val)-1, 1) == '"')	$val = substr($val, 0, strlen($val) - 1);
					}
				}
			}
			// if ($trim) {
			// 	$data = trim_array($data);
			// }
			return count($data) > 0 ? $data : FALSE;
			
	 	} catch (\Exception $e) {
	 		throw new \Exception($e -> getMessage());
	 	}
		
		/* old
		$file = fopen($filename, "r");
		$result = array();
		while(! feof($file)) {
			$data = fgetcsv($file);
			$data = eval('return '.iconv('gbk','utf-8',var_export($data, TRUE)).';');
			is_array($data) && array_push($result, $data);
		}
		
		fclose($file);
		return count($result) > 0 ? $result : FALSE;
		*/
	}

	/**
	  * 追加内容到文件前
	  * @param unknown_type $filename			文件绝对路径
	  * @param array $data						数据
	  */
	public function prepend_to_xlsx($filename, array $data) {
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($filename);				//读取文件
		$sheet = $objPHPExcel -> getSheet(0);
		$sheet -> insertNewRowBefore(1, count($data));	//在前面先插入N行
		foreach ($data as $line) {
			foreach ($line as $rc => $val) {	//$rc为 A1:C1 这样的区域
				$cell = explode(':', $rc);
				$sheet -> setCellValue($cell[0], $val);
				$sheet -> mergeCells($rc);
				
				$style = array( 
					'font' => array(
						'bold'	=> TRUE,
					),
					'borders' => array (
						'outline' => array ( 
							'style' => \PHPExcel_Style_Border::BORDER_THIN, 
						), 
					),
				);
				$sheet -> getStyle($rc) -> applyFromArray($style);
			}unset($val);
		}unset($line);
		
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
	    ob_flush();
		ob_clean();
		$objWriter -> save($filename);
	}
	 
	 /**
	  * 写入csv文件
	  * @param unknown_type $filename	文件绝对路径
	  * @param unknown_type $data		数据
	  * @param unknown_type $append		true在文件尾追加, false清空后写入
	  * @param unknown_type $is_gbk		是否转码为GBK
	  */
	public function write_to_csv($filename, $data, $append=FALSE, $is_gbk=TRUE) {
		$this -> check_data_count(count($data));
		
 		$method = 'w+';
	 	if ($append) {
	 		$method = 'a+';
	 	}
	 	
		$fp = fopen($filename, $method);
	 	$data = is_array($data) ? $data : array();
	 	foreach ($data as $line) {
	 		$line = is_array($line) ? $line : array();
			$line = array_values($line);
	 		if ($is_gbk) {
		 		foreach ($line as &$item) {
		 			$item = iconv('utf-8', 'gbk', strval($item));
		 		}
	 		}
			fputcsv($fp, $line);
	 	}
	 	unset($line);
		fclose($fp);
	 }
	 
	/**
	  * 分段导出/追加内容到csv文件
	  * @param unknown_type $file_path	文件路径
	  * @param unknown_type $data		数据(二维数组)
	  * @param unknown_type $is_new		是否新建（删除原有文件），默认为TRUE
	  * @param unknown_type $trans_gbk	是否转成gbk内容，默认为TRUE
	  */
	public function part_export_csv($file_path, array $data, $is_new=TRUE, $trans_gbk=TRUE) {
	 	if ($is_new && file_exists($file_path)) {
	 		unlink($file_path);
	 	}
	 	
	 	$fp = fopen($file_path, 'a');
		foreach ($data as $a) {
			$a = is_array($a) ? $a : array();
			fputcsv($fp, $a);
		}
		unset($a);
		fclose($fp);
	 }
	 
	/**
	  * csv文件转excel 2007
	  * @param unknown_type $file_path			CSV文件路径
	  * @param unknown_type $target_file_path	转换后的文件路径
	  * @param unknown_type $target_file_name	下载时显示的文件名(需要带扩展名)
	  * @param unknown_type $delete				格式转换完成后，是删除CSV文件，默认为FALSE
	  * @param unknown_type $download			格式转换完成后，是否直接下载，默认为TRUE
	  */
	public function csv_to_xlsx($file_path, $target_file_path, $target_file_name, $delete=FALSE, $download=TRUE) {
	 	if (!file_exists($file_path)) {
	 		throw new \Exception('CSV文件不存在');
	 	}
	 	
		$objReader = \PHPExcel_IOFactory::createReader('CSV');
		$objPHPExcel = $objReader -> load($file_path);
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
		
		$delete && unlink($file_path);
		if ($download) {
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
			header("Content-Type:application/force-download");
			header("Content-Type:application/vnd.ms-execl");
			header("Content-Type:application/octet-stream");
			header("Content-Type:application/download");;
			header('Content-Disposition:attachment;filename="'.$target_file_name.'"');
	    	iconv('utf-8', 'gb2312', $target_file_name);
			header("Content-Transfer-Encoding:binary");
			
			$objWriter -> save('php://output');
			
		} else {
			$objWriter -> save($target_file_path);
		}
	}
	 
	public function mergeCells($file_path,$cells,$title_cells,$download=false,$file_name="",$delete=false){
	 	$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($file_path);				//读取文件
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);	
		$sheet = $objPHPExcel -> getSheet(0);

		$delete && unlink($file_path);
		$styleThinBlackBorderOutline = array( 
			'borders' => array (
				'outline' => array ( 
					'style' => \PHPExcel_Style_Border::BORDER_THIN, //设置border样式 'color' => array ('argb' => 'FF000000'), //设置border颜色 
				), 
			),
		);

		foreach ($cells as $val) {
			$sheet -> mergeCells($val);
		}
		unset($val);
		foreach ($title_cells as $val) {
			$sheet -> getStyle($val) -> applyFromArray($styleThinBlackBorderOutline);
		}
		unset($val);
		if($download){
			empty($file_name) && $file_name = time().'.xlsx';
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
			header("Content-Type:application/force-download");
			header("Content-Type:application/vnd.ms-execl");
			header("Content-Type:application/octet-stream");
			header("Content-Type:application/download");
			header('Content-Disposition:attachment;filename="'.$file_name.'"');
	    	iconv('utf-8', 'gb2312', $file_name);
			header("Content-Transfer-Encoding:binary");
			$objWriter -> save('php://output');
		} else {
			$objWriter -> save($file_path);
		}
	 }

	/**
	  * 下载xlsx文件
	  * @param unknown_type $file_path 			文件绝对路径
	  * @param unknown_type $target_file_name 	下载时用的文件名
	  * @throws \Exception
	  */
	public function download_xlsx($file_path, $target_file_name) {
	 	if (!file_exists($file_path)) {
	 		throw new \Exception('CSV文件不存在');
	 	}
	 	
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($file_path);
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");;
		header('Content-Disposition:attachment;filename="'.$target_file_name.'"');
    	iconv('utf-8', 'gb2312', $target_file_name);
		header("Content-Transfer-Encoding:binary");
		$objWriter -> save('php://output');
	 }
	 
	/**
	  * xlsx文件转csv文件
	  * @param unknown_type $file_path			XLSX文件路径
	  * @param unknown_type $target_file_path	转换后的文件路径
	  */
	public function xlsx_to_csv($file_path, $target_file_path, $delete=TRUE) {
	 	if (!file_exists($file_path)) {
	 		throw new \Exception('XLSX文件不存在');
	 	}
	 	
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($file_path);
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		
		$delete && unlink($file_path);
		$objWriter -> save($target_file_path);
	 }
	 
	/**
	  * 导出csv文件
	  * @param unknown_type $filename
	  * @param unknown_type $data		二维数组
	  */
	public function export_csv($filename, $data) {
		$this -> check_data_count(count($data));
		
	 	$ret = '';
	 	$data = is_array($data) ? $data : array();
		foreach ($data as $a) {
			$a = is_array($a) ? $a : array();
			$row = '';
			foreach ($a as $item) {
				$row .= ',' . '"'.iconv('utf-8', 'gbk', strval($item)).'"';
			} 
			unset($item);
			$ret .= substr($row, 1) . "\n";
		}
		unset($a);
	 	
	 	header("Content-type:text/csv"); 
	    header("Content-Disposition:attachment;filename=".iconv('utf-8', 'gbk', strval($filename))); 
	    header('Cache-Control:must-revalidate,post-check=0,pre-check=0'); 
	    header('Expires:0'); 
	    header('Pragma:public'); 
	    echo $ret; 
	 }
	 
	/**
	  * 写入xlsx文件（支持格式）
	  * @param unknown_type $filename		文件绝对路径
	  * @param unknown_type $data			二维数组，支持格式
	  * @param unknown_type $sheet_idx		sheet序号
	  * @param unknown_type $sheet_name		sheet名称
	  */
	public function write_to_xlsx($filename, array $data, $sheet_idx=0, $sheet_name='sheet') {
		
		$objReader = \PHPExcel_IOFactory::createReader('Excel' . $this -> file_type);
		$objPHPExcel = $objReader -> load($filename);	//读取文件
		
		$sheet_count = $objPHPExcel -> getSheetCount();
		if ($sheet_idx >= $sheet_count) {
			$objPHPExcel -> createSheet($sheet_idx);
		}
		$sheet = $objPHPExcel -> getSheet($sheet_idx);
		if ($sheet_idx >= $sheet_count) {
			$sheet -> setTitle($sheet_name);
		}
		foreach ($data as $rc => $val) {			//$rc为 A1:C1 这样的区域
			$cell = explode(':', $rc);
			$sheet -> setCellValue($cell[0], $val['data']);
			$sheet -> mergeCells($rc);
			$sheet -> getStyle($rc) -> applyFromArray($val['style']);
			if (isset($val['width'])) {
				$column = substr($rc, 0, 1);
				$sheet -> getColumnDimension($column) -> setWidth($val['width']);
			}
			if (isset($val['height'])) {
				$row = substr($cell[0], 1);
				$sheet -> getRowDimension($row) -> setRowHeight($val['height']);
			}
			if (isset($val['auto_height'])) {
				$sheet -> getStyle($rc) -> getAlignment() -> setWrapText($val['auto_height']);
			}
			
		}unset($val);
		
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel' . $this -> file_type);
	    ob_flush();
		ob_clean();
		$objWriter -> save($filename);
	}	 

	/**
	 * 单元格内容居中
	 * @param unknown_type $objPHPExcel	EXCEL对象
	 * @param unknown_type $cell		单元格对象
	 */
	private function set_center($sheet, $cell) {
		$objStyleA1 = $sheet -> getStyle($cell);
	    $objAlignA1 = $objStyleA1 -> getAlignment();  
	    $objAlignA1 -> setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    //左右居中  
	    $objAlignA1 -> setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  		//上下居中  
	}
	
	/**
	 * 判断是否空行
	 * @param unknown_type $data	行数组，数组格式
	 */
	private function empty_line($data) {
		if (!is_array($data)) {
			return TRUE;
		}
		
		$flag = TRUE;
		foreach ($data as $d) {
			if (!empty($d)) {
				$flag = FALSE;
				break;
			}
		}
		unset($d);
		
		return $flag;
	}
	
	/**
	 * 检查每次导出的数据量是否超限
	 * @param unknown_type $count	当前要导出的数量量（行数）
	 */
	private function check_data_count($count) {
		$max = 30000;
		if ($count > $max) {
			cls_resp::show_err_page('每次最多导出'.$max.'条数据');
			exit();
		}
	}
	
	/**
	 * 获取CSV文件的行数据
	 * @param unknown_type $line_data	行数据（字符串）
	 * @param unknown_type $trim		是否删除前后空格等符号
	 * @param unknown_type $is_gbk		文件是否为GBK编码，默认为TRUE
	 */
	private function _csv_get_line_data($line_data, $trim=TRUE, $is_gbk=TRUE) {
		if ($trim)		$line_data = trim($line_data);
		if ($is_gbk)	$line_data = iconv('gbk', 'utf-8', $line_data);
		$line_data = explode(',', $line_data);
		return $line_data;
	}
	

	private static $XML_HEADER = <<<EOF
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Title>Office 2007 XLS Test Document</Title>
  <Subject>Office 2007 XLS Test Document</Subject>
  <Author>Office 2007 XLS Test Document</Author>
  <Keywords>office 2007 openxml php</Keywords>
  <Description>Test document for Office 2007 XLS, generated using PHP classes.</Description>
  <LastAuthor>Administrator</LastAuthor>
  <Created>2016-04-28T15:09:00Z</Created>
  <LastSaved>2016-05-06T04:09:20Z</LastSaved>
  <Category>Test result file</Category>
  <Company>Microsoft Corporation</Company>
  <Version>14.00</Version>
 </DocumentProperties>
 <CustomDocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <KSOProductBuildVer dt:dt="string">2052-9.1.0.5218</KSOProductBuildVer>
 </CustomDocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
  <DoNotRelyOnCSS/>
  <PixelsPerInch>120</PixelsPerInch>
  <TargetScreenSize>1920x1200</TargetScreenSize>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8370</WindowHeight>
  <WindowWidth>20400</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
   <Interior/>
   <Protection/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Worksheet">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="47.25" ss:DefaultRowHeight="15">
   <Column ss:Index="2" ss:AutoFitWidth="0" ss:Width="72.75"/>
   <Column ss:Index="10" ss:AutoFitWidth="0" ss:Width="129"/>
EOF;

	private static $XML_FOOTER = <<<EOF
</Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.69930555555555596"
     x:Right="0.69930555555555596" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>4</ActiveRow>
     <ActiveCol>10</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
   <AllowFormatCells/>
   <AllowSizeCols/>
   <AllowSizeRows/>
   <AllowInsertCols/>
   <AllowInsertRows/>
   <AllowInsertHyperlinks/>
   <AllowDeleteCols/>
   <AllowDeleteRows/>
   <AllowSort/>
   <AllowFilter/>
   <AllowUsePivotTables/>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
EOF;


}

/* End of file cls_excel.php */ 