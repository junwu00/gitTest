<?php

/** 
 * excel工具在
 * 
 * @author yangpz
 * @create 2014-06-28
 * @version 1.0
 */

namespace package\excel;
class ExcelServer {
    private $objPHPExcel;

    // 下拉框数据工作表
    private $comboBoxSheet;
    private $comboBoxIndex = 'A';
    private $comboBoxKey = 'a';

    public function __construct()
    {
        $this->objPHPExcel = new \PHPExcel();
    }
    public function getComboBoxSheet()
    {
        if (!$this->comboBoxSheet) {
            $this->comboBoxSheet = new \PHPExcel_Worksheet($this->objPHPExcel, 'comboBox'); //创建一个工作表
            $this->objPHPExcel->addSheet($this->comboBoxSheet); //插入工作表
        }
        return $this->objPHPExcel->getSheetByName('comboBox');
    }
    /**
     * 设置下拉框
     * @param array $data 示例：['江夏区','洪山区','青山区','武昌区','汉口']
     * @param string $targetIndex 设置下拉框目标列  示例：A
     * @param string $link_key 设置关联key值，与link_index配对使用， 示例："广东省"
     * @param string $link_index 设置关联所在列，示例： A
     * @param int $offset 设置下拉框起始位置
     * @param int $limit 设置下拉框长度
     * @param string $objValidFunc 下拉框设置函数
     */
    public function setComboBoxData($data,$targetIndex,$offset=1,$limit=100,$link_key=null,$link_index=null,$objValidFunc="")
    {
        // 关联值和关联列
        if (!$link_key || !$link_index) {
            $link_key = $this->comboBoxKey;
            $link_index = "={$link_key}";
            // 默认key值
            $this->comboBoxKey++;
        } else {
            $link_index = '=INDIRECT($'.$link_index.'$'.'%d)';
        }
        // 默认存放位置
        $index = $this->comboBoxIndex;
        $this->comboBoxIndex++;
        //移花接木
        foreach ($data as $var=>$content){
            $this->getComboBoxSheet()->setCellValue($index.($var+1),$content);
        }
        $total = count($data);
        $this->objPHPExcel->addNamedRange(
            new \PHPExcel_NamedRange(
                $link_key,
                $this->getComboBoxSheet(),
                "{$index}1:{$index}{$total}"
            )
        );
        //数据验证
        for ($i = $offset;$i < $limit;$i++) {

            $tmp_index = sprintf($link_index,$i);

            $objValidation = $this->objPHPExcel->getActiveSheet()->getCell($targetIndex . $i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('输入错误');
            $objValidation->setError('不在列表中的值');
            $objValidation->setPromptTitle('请选择');
            $objValidation->setPrompt('请从列表中选择一个值.');
            $objValidation->setFormula1($tmp_index);
            // 下拉框设置
            if ($objValidFunc){
                $objValidFunc($objValidation);
            }
        }
    }
    /**
     * 浏览器下载
     */
    public function output($filename=null)
    {
        !$filename && $filename = time();
        //输出表格
        ob_end_clean();
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Content-Type:application/force-download');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Type:application/octet-stream');
        header('Content-Type:application/download');
        header('Content-Transfer-Encoding:binary');
        header('Content-Disposition: attachment;filename='.$filename.'.xlsx');
        $objWriter = new \PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->save('php://output');
        exit;
    }
    /**
     * 文件保存
     * @param string $filename 文件名
     * @return string
     */
    public function save($filename=null)
    {
        !$filename && $filename = time().'.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter->save($filename);
        return $filename;
    }
    /**
     * 写入数据
     * @param array $list
     * 示例： 以下 A1:E1:100 的 100是高度设置
    $list = array(
        array(
            'A1:E1:100' => "填写说明：
        ),
        array(
            'A2' => "物品大类",
            'B2' => "物品名称",
        ),
    );
     */
    public function writeData($list)
    {
        //接下来就是写数据到表格里面去
        $objActSheet = $this->objPHPExcel->getActiveSheet();
        foreach ($list as $row) {
            foreach ($row as $key => $value){
                $cells = explode(":",$key);
                if (count($cells)>1 && $cells[0] != $cells[1]) {
                    //合并单元格
                    $objActSheet->setCellValue($cells[0],$value);
                    $objActSheet->getStyle($cells[0])->getFont()->setColor(new \PHPExcel_Style_Color(\PHPExcel_Style_Color::COLOR_RED));
                    $_key = "{$cells[0]}:{$cells[1]}";
                    $objActSheet->mergeCells($_key);
                } else {
                    //这里是设置单元格的内容
                    $objActSheet->setCellValue($cells[0],$value);
                    $_key = $cells[0];
                }
                // 设置高度
                if (isset($cells[2]) && $cells[2]) {
                    $this->objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight($cells[2]);
                    $objActSheet->getStyle($_key)->getAlignment()->setWrapText(TRUE);
                }
            }
        }
    }
    /** 设置列宽度 */
    public function setWidth($pColumn,$width){
        $this->objPHPExcel->getActiveSheet()->getColumnDimension($pColumn)->setWidth($width);
    }
}

/* End of file cls_excel.php */ 