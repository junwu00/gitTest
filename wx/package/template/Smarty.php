<?php
/**
 * smarty二次封装类
 *
 * @author JianMing Liang
 * @create 2016-01-26 12:11:16
 */
namespace package\template;

class Smarty {
    // 默认配置名称（使用load_config加载）
    private $_default_config_path = 'package/template/smarty';

    // smarty配置 [array()]
    private $_conf = array();
    // smarty实例 [null]
    private $_smarty = null;
    
    /**
     * 构造函数
     *
     * @access public
     * @param array $conf 配置数组
     * @return boolean
     */
    public function __construct(array $conf=array()) {
        $this -> set_conf($conf);
    }

    /**
     * 设置smarty配置参数
     *
     * @access public
     * @param array $conf 配置文件集合, 可包含参数请看 /config/package/smarty.config.php
     * @return void
     */
    public function set_conf(array $conf=array()) {
        if (empty($conf)) {
            $conf = load_config($this -> _default_config_path);
            if (!is_array($conf) or empty($conf)) {
                to_log(MAIN_LOG_ERROR, '', __CLASS__ . ':' . __FUNCTION__ . ': 默认配置为空');
                return;
            }
        }

        $this -> _smarty === null and $this -> _smarty = new \Smarty();
        $sm = $this -> _smarty;

        isset($conf['debugging']) and $sm -> debugging = $conf['debugging'];
        isset($conf['error_unassigned']) and $sm -> error_unassigned = $conf['error_unassigned'];
        isset($conf['use_sub_dirs']) and $sm -> use_sub_dirs = $conf['use_sub_dirs'];
        isset($conf['compile_check']) and $sm -> compile_check = $conf['compile_check'];
        isset($conf['left_delimiter']) and $sm -> left_delimiter = $conf['left_delimiter'];
        isset($conf['right_delimiter']) and $sm -> right_delimiter = $conf['right_delimiter'];
        isset($conf['error_reporting']) and $sm -> error_reporting = $conf['error_reporting'];

        isset($conf['cache_dir']) and $sm -> setCacheDir($conf['cache_dir']);
        isset($conf['compile_dir']) and $sm -> setCompileDir($conf['compile_dir']);
        isset($conf['template_dir']) and $sm -> setTemplateDir($conf['template_dir']);

        $this -> _conf = $conf;
    }
    
    /**
     * 获取源smarty实例句柄
     *
     * @access public
     * @return mixed
     */
    public function get_handler() {
        return $this -> _smarty;
    }
    
    /**
     * 使用smarty输出模板页面
     *
     * @access public
     * @param string $tpl_file 模板文件路径，既可是绝对路径，也可是相对路径
     * @param boolean $to_exit 输出后是否终止脚本执行
     * @return void
     */
    public function show($tpl_file, $to_exit=true){
        try {
            $this -> _smarty -> display($tpl_file);
        }catch(\Exception $e) {
            to_log(MAIN_LOG_WARN, '', __CLASS__ . ':' . __FUNCTION__ . ': ' . $e -> getMessage());
        }

        $to_exit and exit;
    }
    
    /**
     * 进行smarty变量赋值
     *
     * @access public
     * @param string $var_name 变量名
     * @param mixed $var_value 变量值
     * @return boolean
     */
    public function assign($var_name, $var_value=NULL) {
        try {
            $this -> _smarty -> assign($var_name, $var_value);
        }catch(\Exception $e) {
            to_log(MAIN_LOG_WARN, '', __CLASS__ . ':' . __FUNCTION__ . ': ' . $e -> getMessage());
        }
    }
    
    /**
     * 获得由smarty渲染后的输出流
     *
     * @access public
     * @param string $tpl_file 模板文件路径，既可是绝对路径，也可是相对路径
     * @return mixed 失败返回(bool)false，否则返回(string)
     */
    public function fetch($tpl_file) {
        try {
            $ret = $this -> _smarty -> fetch($tpl_file);
        }catch(\SmartyException $e) {
            to_log(MAIN_LOG_WARN, '', __CLASS__ . ':' . __FUNCTION__ . ': ' . $e -> getMessage());
            return false;
        }

        return $ret;
    }
}