#!/bin/bash

#----------------------------------------------------------------------
# 全局定时脚本触发器，自定执行所有model下的定时脚本
#----------------------------------------------------------------------
#定义该项目的唯一关键字
pid_key="";
#脚本的进程id保存路径
pid_file="/var/run/${pid_key}.pid";

#当前绝对路径
cur_dir=$(cd "$(dirname "$0")"; pwd);
#当前文件名称
cur_file_name=${0##*/};
#执行具体功能所用的脚本
do_file="${cur_dir}/done.sh";

#模型所在的根目录
model_dir="$(dirname "${cur_dir}")"/model;

#脚本上一次的进程id
prev_pid="";
if [ -f "${pid_file}" ]; then 
    prev_pid=`/bin/cat ${pid_file}`;
fi

#echo "last_pid:"${prev_pid};
if [ "${prev_pid}" != "" ]; then 
    /bin/kill -9 "${prev_pid}";
    
    #echo "kill finshed";
    
    if [ $? -ne 0 ]; then 
        #当前脚本的进程数
        count=`/bin/ps -ef|grep '${cur_file_name}'|grep '/bin/bash'|grep -v 'grep'|wc -l`;
        if [ ${count} -ne 0 ]; then 
            exit 1;
            #echo "alerm";
            #触发报警
        fi
    fi
fi

#写入进程id
/bin/echo $$ > ${pid_file};
#echo 'pid:'$$;

while true; do 
    #找不到进程id文件，则退出并报出异常吗
    if [ ! -f "${pid_file}" ]; then 
        exit 1;
    fi

    #循环遍历模型中的所有定时脚本（仅支持.php后缀），全部以后台进程的方式执行
    for dir_name in `/bin/ls ${model_dir}`; do 
        tmp_dir="${model_dir}/${dir_name}";
        tmp_cron_dir="${tmp_dir}/cron";
        tmp_cron_file="${tmp_cron_dir}/config";
        if [ ! -d "${tmp_dir}" -o ! -d "${tmp_cron_dir}" -o ! -f "${tmp_cron_file}" ]; then 
            continue;
        fi

        dos2unix ${tmp_cron_file};

        while read line; do 
            file_name=`echo ${line} |awk '{print $1}'`;
            full_name="${tmp_cron_dir}/${file_name}";
            interval=`echo ${line} |awk '{print $2}'`;

            if [ ! -f "${full_name}" ]; then 
                continue;
            fi

            # 同一时间只能有一个等待任务
            check=`ps -ef|grep "${full_name}"|grep "${do_file}"|grep -v grep|wc -l`;
            if [ "${check}" != "0" ]; then 
                #echo "exit";
                #exit 0;
                continue;
            fi

            ${do_file} ${full_name} ${interval} &
            #echo ${full_name};
            #exit 0;
            #echo "${do_file} ${full_name} ${interval} &" > /data1/a.test;
        done < ${tmp_cron_file}
    done
    #每秒执行一次遍历
    sleep "2s";
done

exit 0;
