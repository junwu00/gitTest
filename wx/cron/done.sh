#!/bin/sh

#----------------------------------------------------------------------
# 全局执行脚本，由定时器注入参数，该脚本执行实际功能
#----------------------------------------------------------------------
help_tip="Usage: done.sh [PHP FILE] [interval(unit:second)]\n";

if [ $# -eq 0 ]; then 
    echo $help_tip;
    exit 1;
fi

if [ -z "$1" -o -z "$2" ]; then 
    echo $help_tip;
    exit 1;
fi

php_cmd="/user/bin/php";
php_file=$1;
interval="${2}s";
#时间间隔至少5秒，默认为1分钟
if [ $2 -lt 5 ]; then 
    interval="60s";
fi

if [ ! -f "${php_file}" ]; then 
    echo "no such file : {$php_file}";
    exit 2;
fi

sleep ${interval};
${php_cmd} ${php_file} & 
exit 0;
