<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    '5255c38a0faeba867671b61dfda6d864' => $vendorDir . '/paragonie/random_compat/lib/random.php',
    '2cffec82183ee1cea088009cef9a6fc3' => $vendorDir . '/ezyang/htmlpurifier/library/HTMLPurifier.composer.php',
    'e634b85a2fb38aee255cf72749a83143' => $vendorDir . '/sanchi/guomi/src/overwrite.php',
    '048b46910cf4efb3923a0c6ff80f73ea' => $baseDir . '/util/init.php',
    '64212085055d4b8916a3cc08dded09e7' => $baseDir . '/config/init.php',
    '88e288634bf0cdd760f015d7c087accb' => $baseDir . '/model/init.php',
);
