<?php
/**
 * 全局对外错误码前缀配置（2位数字长度）
 * 各模块不同（模块名大写，驼峰式，如A_B_C）
 * 
 * @author yangpz
 * @date 2016-04-08
 */

$GLOBAL_MODEL_ERRCODE_PREFIX = array(
	'API'		=> '01',
	'legwork'	=> '02',
);

!isset($GLOBALS['MODEL_ERRCODE_PREFIX']) && $GLOBALS['MODEL_ERRCODE_PREFIX'] = $GLOBAL_MODEL_ERRCODE_PREFIX;

return $GLOBAL_MODEL_ERRCODE_PREFIX;
//end