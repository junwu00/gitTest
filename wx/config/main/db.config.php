<?php
/**
 * 数据库配置定义
 *
 * @author LiangJianMing
 * @create 2015-12-15
 */

$db_file = dirname(MAIN_ROOT) . '/install_conf/db.config.json';
$MAIN_DB_CONFIG = json_decode(file_get_contents($db_file), true);
/*
$MAIN_DB_CONFIG = array(
	'host' 		=> 'localhost',
	'port' 		=> '3306',
	'user' 		=> 'admin',
	'pass' 		=> 'adminpass',
	'name' 		=> 'd_ew365_private',
	'charset' 	=> 'utf8',
);
*/
!isset($GLOBALS['MAIN_DB_CONFIG']) and $GLOBALS['MAIN_DB_CONFIG'] = $MAIN_DB_CONFIG;

return $MAIN_DB_CONFIG;

/* End of file db.config.php */ 