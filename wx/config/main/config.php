<?php
/**
 * 框架主要配置文件
 *
 * 只允许定义框架必须配置项
 *
 * @author LiangJianMing
 * @create 2015-12-15
 */

// define('DS', DIRECTORY_SEPARATOR);

/*--begin-----主框架常量-------------*/
$MAIN_CONFIG = load_config('env/env.now');
//定义上述常量
batch_defined($MAIN_CONFIG);
/*--end-------主框架常量-------------*/

/*--begin-------安全配置--------------------------------------------------*/
// 指定session的作用域名
ini_set('session.cookie_domain', MAIN_COOKIE_DOMAIN);
// 只允许使用cookie来记录session_id
ini_set('session.use_only_cookies', true);
// session的cookie只允许使用http方式进行访问
ini_set('session.cookie_httponly', true);
/*--end---------安全配置--------------------------------------------------*/

// 指定session的序列化处理器
ini_set('session.serialize_handler', 'php_serialize');
ini_set('session.save_handler', 'user');

// 指定页面编码
header('Content-Type: text/html; charset=' . MAIN_ENCODING);
// 指定默认时区
date_default_timezone_set(MAIN_TIMEZONE);
// 设定报错类型
error_reporting(-1);
ini_set('display_errors', true);

/* End of file main.config.php */