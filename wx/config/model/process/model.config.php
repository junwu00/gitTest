<?php
/**
 * 模块基本信息配置
 *
 * @author luja
 * @date 2016-05-17
 */

$model_root = dirname(dirname(__FILE__));

$com_id = isset($_SESSION[SESSION_VISIT_COM_ID])?$_SESSION[SESSION_VISIT_COM_ID]:0;
$dao_com_form = new \model\api\dao\ComForm();
$material_apply_form = $dao_com_form->getByComId($com_id,\model\api\dao\ComForm::TYPE_MATERIAL_APPLY);

return array(
	//基础配置
	'name'				=> 'process',											//模块名称
	'cn_name'			=> '流程审批',												//模块中文名称
	'dir'				=> $model_root . DS,									//模块根路径
	'view_dir'			=> MAIN_VIEW . 'process' . DS,							//模块视图根路径
	'log_dir'			=> MAIN_DATA . 'process' . DS . 'logs' . DS,				//模块视图根路径
	'upload_dir'		=> MAIN_DATA . 'process' . DS . 'upload' . DS,			//本地上传/下载根路径
	'smarty_plugin'		=> MAIN_PACKAGE . 'template' . DS . 'smarty_plugin',
	'smarty_conf_dir'	=> MAIN_VIEW . 'process' . DS,
	'suite_id' 			=> g('dao_com_app') -> get_sie_id(11),					//套件ID，根据应用id获取
	'app_id'			=> 11,													//应用ID
	'log_lev'			=> 0,													//自定义日志的输出级别，从0~5共6个级别，0 => 所有日志，1 => 仅正常及更高级日志，2 => 仅突出通知及更高级，
																				//3 => 仅警告及更高级，4 => 仅错误及更高级，5 => 仅致使错误
	
	//其他自定义配置...

    //物品明细库管理配置
    "material_management"=>[
        'item_claim_form' => isset($material_apply_form['form_history_id'])?$material_apply_form['form_history_id']:'',                                    // 物资管理-物品领用表单根源版本ID（历史版本）
        'goods_table'  =>  'input1487687030000',                        // 商品表对应input_key
        "goods_input_key"    => 'input1487687030000_1520856402000',     // 商品对应input_key
        "number_input_key"    => 'input1487687030000_1487687065000',    // 数量对应input_key
    ],
);