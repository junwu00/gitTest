<?php
/**
 * 推送的操作指引的配置
 * @author luja
 * @date 2016-05-17
 */

return array(
	'title' => '『意见反馈』操作指引',
	'desc' => '欢迎使用意见反馈应用，在这里你可以匿名发建议给公司，说不定就能升职加薪迎娶白富美，给领导发一下你的心声吧！',
	'pic_url' => '',
	'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971786&idx=3&sn=d3d02fafa5dfff8def52820f5f9dc12a&scene=0#rd'
);

//end