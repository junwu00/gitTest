<?php
/**
 * 错误码配置
 * 
 * @author yangpz
 * @date 2016-04-08
 */

//获取当前模块名（即目录名）
$model_dir = dirname(__FILE__);
$model_dir = explode(DS, $model_dir);
$model_dir_name = $model_dir[count($model_dir) - 1];
$model_name = strtoupper($model_dir_name);					

if (!isset($GLOBALS['MODEL_ERRCODE_PREFIX'][$model_name])) {
	throw new \Exception('未配置模块错误码前缀：' . $model_name);
}

//前缀要求为：02
//（与\config\main\errcode.config.php中配置一致）
final class ERRCODE_LEGWORK {
	/** 02000 未知异常 */
	public static $UNKNOW_EXP					= array('code' => '02000', 'desc' => '未知异常');
	/** 05001 参数错误 */
	public static $ERR_PARAM_EXP				= array('code' => '02001', 'desc' => '参数错误');
	
}

//end