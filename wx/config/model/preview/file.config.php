<?php

/**
 * 预览文件配置信息
 */

return array(
	'1' => array(
		'name' => '发起人催办.pdf',
		'hash' => '2078ba527cd01c3f6d451accf2605d5d'
		),
	'2' => array(
		'name' => '发起人删除表单.pdf',
		'hash' => 'd9a21afc13afd960b30311f29b311815'
		),
	'3' => array(
		'name' => '发起人知会.pdf',
		'hash' => '37628e0b8a79a9b08bf9283e3b44fd46'
		),
	'4' => array(
		'name' => '发起申请.pdf',
		'hash' => '8cddd5961d98e0f245561f38ae648d35'
		),
	'5' => array(
		'name' => '审批人催办.pdf',
		'hash' => 'f7d08a07966d6f685123da776c655775'
		),
	'6' => array(
		'name' => '审批人知会.pdf',
		'hash' => '4a8a8f0ab6cd8df577318d3863d8da26'
		),
	'7' => array(
		'name' => '审批通过.pdf',
		'hash' => '5908b1d940adfcbb9ffd5ddea0862aab'
		),
	'8' => array(
		'name' => '审批人退回表单.pdf',
		'hash' => 'f7d08a07966d6f685123da776c655775'
		),


	);


//end