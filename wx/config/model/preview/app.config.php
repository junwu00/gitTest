<?php
/**
 * 应用的配置文件
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 		=> 0,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	// 'combo' 	=> g('dao_com_app') -> get_sie_id(0),
	'combo' 	=> 0,
	'name' 		=> 'preview',
	'cn_name' 	=> 'PDF预览',
	// 'icon' 		=> SYSTEM_HTTP_APPS_ICON.'outsidework.png',
	'menu' => array(
		
	),
);

// end of file