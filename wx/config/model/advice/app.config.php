<?php
/**
 * 应用的配置文件_意见反馈
 * 
 * @author luja
 * @date 2016-05-17
 * 
 */

return array(
	'id' 		=> 18,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('dao_com_app') -> get_sie_id(18),
	'name' 		=> 'advice',
	'cn_name' 	=> '意见反馈',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'freeback.png',
		
	'menu' => array(
		array('id' => 'bmenu-1', 'name' => '反馈信息', 
			'childs' => array(
				array('id' => 'bmenu-1-1', 'name' => '我发起的', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?app=advice&a=mineList&debug=1'),
				array('id' => 'bmenu-1-2', 'name' => '我处理的', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?app=advice&a=dealList&debug=1'),
				array('id' => 'bmenu-1-3', 'name' => '我监督的', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?app=advice&a=superviseList&debug=1'),
			)	
		),
		array('id' => 'bmenu-2', 'name' => '发起反馈', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?app=advice&a=create&debug=1', 'icon' => ''),
    ),
        
);

// end of file