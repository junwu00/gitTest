<?php
/**
 * 推送的操作指引的配置
 * @author luja
 * @date 2016-05-23
 */

return array(
	'title' => '『组织活动』操作指引',
	'desc' => '欢迎使用组织活动应用，在这里你可以发起活动，组织同事一起娱乐，一起运动，给无趣的生活增添一丝有趣的经历！',
	'pic_url' => '',
	'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=6&sn=7289e587de51c2cf1141a7756c6db3ba#rd'
);

//end