<?php
/**
 * 应用的配置文件_掌上考勤
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id'				=> 13,									//对应sc_app表的id
    'combo' 			=> g('dao_com_app') -> get_sie_id(13),		//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'name' 				=> 'legwork',
	'cn_name' 			=> '移动外勤',
	'icon'				=> SYSTEM_HTTP_APPS_ICON.'Helper.png',
);

// end of file