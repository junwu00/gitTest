<?php
/**
 * 推送的操作指引的配置
 * @author yangpz
 * @date 2016-02-18
 */

return array(
	'title' 	=> '『通知公告』操作指引',
	'desc' 		=> '欢迎使用通知公告应用，在这里你可以随时随地查看公司的新闻、动态、通知等，不再错过任何与你有关的权益，快速查看下吧！',
	'pic_url' 	=> '',
	'url' 		=> 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=4&sn=74d64156ea71da9109d363aa2d51eadb&scene=0#rd'
);

//end