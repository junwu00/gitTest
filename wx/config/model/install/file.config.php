<?php
/**
 * 常规定义项
 *
 * @author JianMing Liang
 * @create 2016-03-29 18:13:53
 */
$conf_root_dir = dirname(dirname(dirname(dirname(__DIR__)))) . DS . 'install_conf';
return array(
    'advance.config' => $conf_root_dir . DS . 'advance.config.json',
    'company.config' => $conf_root_dir . DS . 'company.config.json',
    'powerby.config' => $conf_root_dir . DS . 'powerby.config.json',
    'db.config' => $conf_root_dir . DS . 'db.config.json',
    'domain.config' => $conf_root_dir . DS . 'domain.config.json',
    'redis.config' => $conf_root_dir . DS . 'redis.config.json',

    'sdk.server' => $conf_root_dir . DS . 'sdk.server.ini',
    'file.server' => $conf_root_dir . DS . 'file.server.ini',
    'msg.server' => $conf_root_dir . DS . 'msg.server.ini',

    'database.sql' => $conf_root_dir . DS . 'database.sql',
);