<?php
/**
 * 类命名空间与缩写的映射关系
 * 
 * 注意：
 * 1.模块（model）内所有映射都必须、且只能在该文件配置（即模块内其他所有地方不允许再出现g_map或g_maps方法）
 * 2.必须保证值的唯一性
 * 
 * 目的：
 * 1.模块内所有类映射关系统一管理，避免命名重复导致的系统异常
 */
$maps = array(
	//当前模块的配置
	'model\\index\\dao\\DaoIndexMine'		=> 'dao_index_mine', 
	'model\\index\\dao\\DaoIndexNews'		=> 'dao_index_news', 
	'model\\index\\dao\\DaoIndexNewsUser'	=> 'dao_index_news_user', 

	'model\\index\\server\\ServerIndexMsg'	=> 'ser_index_msg', 
	'model\\index\\server\\ServerIndexApp'	=> 'ser_index_app', 
	'model\\index\\server\\ServerIndexMine'	=> 'ser_index_mine', 

	'model\\index\\view\\IndexNotice'		=> 'view_notice', 
	'model\\index\\view\\IndexIndex'		=> 'view_index', 
	'model\\index\\view\\IndexApp'			=> 'view_app', 
	'model\\index\\view\\IndexMine'			=> 'view_mine', 
	'model\\index\\view\\IndexMsg'			=> 'view_msg', 
);
g_maps($maps);

//END