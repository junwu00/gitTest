<?php
/**
 * 模块消息提醒应用配置
 */

return array(
	'0'  => array(
			'0' => array(
					'name' => '移步小助手',
					'simple_name' => '小助手',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'YibuHelper.png',
					'app_id' => 0,
					'type' => 0
				),
			'1' => array(
					'name' => '润衡提醒',
					'simple_name' => '润衡',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Runheng.png',
					'app_id' => 0,
					'type' => 1
				),
			'2' => array(
					'name' => '报表提醒',
					'simple_name' => '报表',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Report.png',
					'app_id' => 0,
					'type' => 2
				),
		), 
	'23' => array(
			'1' => array(
					'name' => '待办提醒',
					'simple_name' => '待办',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Do.png',
					'app_id' => 23,
					'type' => 1
				),
			'2' => array(
					'name' => '发起提醒',
					'simple_name' => '发起',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Pass.png',
					'app_id' => 23,
					'type' => 2
				),
			'3' => array(
					'name' => '知会提醒',
					'simple_name' => '知会',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Notify.png',
					'app_id' => 23,
					'type' => 3
				),
		),
	'25' => array(
			'1' => array(
					'name' => '打卡',
					'simple_name' => '打卡',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Checkwork.png',
					'app_id' => 25,
					'type' => 1
				),
			'2' => array(
					'name' => '定位',
					'simple_name' => '定位',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Legwork.png',
					'app_id' => 25,
					'type' => 2
				),
			'3' => array(
					'name' => '定位提醒',
					'simple_name' => '定位',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Legwork.png',
					'app_id' => 25,
					'type' => 3
				),
			'4' => array(
					'name' => '排班提醒',
					'simple_name' => '排班',
					'icon' => SYSTEM_MSG_INFO_ICON_PATH. 'Workshift.png',
					'app_id' => 25,
					'type' => 4
				),
		)
);