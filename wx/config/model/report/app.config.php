<?php
/**
 * 应用的配置文件_综合报表
 * 
 * @author luja
 * @date 2016-05-17
 * 
 */

return array(
	'id' 		=> 27,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	// 'combo' 	=> g('dao_com_app') -> get_sie_id(27),
	'name' 		=> 'report',
	'cn_name' 	=> '综合报表',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'Process.png',
		
	'menu' => array(),
        
);

// end of file