<?php
/**
 * 应用信息配置
 *
 * 
 * 
 */
return array(
	'bpm' => array(
		'id' => 23, 
		'name' => '流程专家', 
		'icon' => SYSTEM_APP_INFO_ICON_PATH.'ProcessM.png',
		'md5' => '5075140835d0bc504791c76b04c33d2c',
		'default_url' => '',
		'is_child' => 0,
	),
	'process' => array(
		'id' => 24, 
		'name' => '流程审批', 
		'icon' => SYSTEM_APP_INFO_ICON_PATH.'ProcessM.png',
		'md5' => '5075140835d0bc504791c76b04c33d2c',
		'default_url' => '',
		'is_child' => 1,
	),
	'workshift' => array(
		'id' => 25, 
		'name' => '考勤管理', 
		'icon' => SYSTEM_APP_INFO_ICON_PATH.'ProcessM.png',
		'md5' => '5075140835d0bc504791c76b04c33d2c',
		'default_url' => '',
		'is_child' => 1,
	),
);


/* End of this file */