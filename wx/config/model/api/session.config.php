<?php
/**
 * SESSION及COOKIE相关配置
 */

/** 当前访问的ip */
define('SESSION_VISIT_IP',						'session_visit_ip');
/** 当前访问的企业号的corp url */
define('SESSION_VISIT_CORP_URL',				'session_visit_corp_url');
/** 当前访问的企业号的com id */
define('SESSION_VISIT_COM_ID',					'session_visit_com_id');
/** 当前访问的企业号根部门的id */
define('SESSION_VISIT_DEPT_ID',					'session_visit_dept_id');
/** 当前访问的企业号的corp id */
define('SESSION_VISIT_CORP_ID',					'session_visit_corp_id');
/** 当前访问的企业号的corp secret */
define('SESSION_VISIT_CORP_SECRET',				'session_visit_corp_secret');
/** 当前访问的员工的id */
define('SESSION_VISIT_USER_ID',					'session_visit_user_id');
/** 当前访问的员工的姓名 */
define('SESSION_VISIT_USER_NAME',				'session_visit_user_name');
/** 当前访问的员工的企业号账号 */
define('SESSION_VISIT_USER_WXACCT',				'session_visit_user_wxacct');
/** 当前访问的员工的授权信息 */
define('SESSION_VISIT_USER_WXINFO',				'session_visit_user_wxinfo');
/** 当前访问的员工的微信号 */
define('SESSION_VISIT_USER_WXID',				'session_visit_user_wxid');
/** 当前访问的员工的主部门的id */
define('SESSION_VISIT_USER_DEPT',				'session_visit_user_dept_id');
/** 技术支持的文本内容 */
define('SESSION_VISIT_POWERBY',					'session_visit_powerby');
/** 技术支持的链接地址 */
define('SESSION_VISIT_POWERBY_URL',				'session_visit_powerby_url');
/** 企业的永久授权码 */
define('SESSION_VISIT_AUTH_CODE',				'session_visit_auth_code');
/** 当前访问的员工的直属及上级部门 */
define('SESSION_VISIT_USER_DEPT_PARENT_LIST',	'session_visit_user_dept_parent_list');

/** 最后一次访问的企业号的corp url */
define('COOKIE_VISIT_LAST_CORP_URL','session_visit_last_url');
//end