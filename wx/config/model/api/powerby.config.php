<?php
/**
 * 技术支持信息配置
 * @author yangpz
 * @date 2016-02-19
 */

if (defined('IS_PRIVATE') && IS_PRIVATE == 1) {
    return array(
        'desc'	=> MAIN_COPYRIGHT,
        'url'	=> '',
    );

} else {
    return array(
        'desc'	=> '移步到微技术支持',
        'url'	=> 'http://www.easywork365.com',
    );
}

//end