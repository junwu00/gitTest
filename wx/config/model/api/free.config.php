<?php
/**
 * 请求白名单配置
 * @author yangpz
 * @date 2016-02-18
 */

/**
 *	可绕过check_user方法的url参数配置
 *	需要url中带free为参数名的参数，如：http://host/index.php?app=xxx&free。或不带free，默认进行用户身份验证
 *	如（以下四项不能都为空）：
 *	array(
 *		'app' => 'process',	//可为空，如：'app' => '',
 *		'm' => 'apply',		//可为空，如：'m' => '',
 *		'a' => 'apply',		//可为空，如：'a' => '',
 *		'match' => '',		//可为空，如：'match' => ''。正则表达式，用于检测url是否与该表达式匹配
 *		'lev' => 1,			//放行等级：1=>不检测企业和员工；2=>检测企业和员工，检测失败继续放行，
 *	),
 */
return array(
	//招聘上传头像	//TODO
	array(
		'app'	=> '',
		'm'		=> 'root_ajax',
		'a'		=> '',
		'match' => '/cmd=101/',
		'lev'	=> 2,
	),
	//快速体验
	array(
		'app'	=> '',
		'm'		=> 'quick_try',
		'a'		=> '',
		'match' => '',
		'lev'	=> 1,
	),
	//分享客户名片
	array(
		'app' 	=> 'card',
		'm'		=> 'info',
		'a'		=> 'show',
		'match' => '',
		'lev'	=> 2,
	),
	//下载客户名片到通讯录
	array(
		'app' 	=> 'card',
		'm'		=> 'ajax',
		'a'		=> '',
		'match' => '/cmd=113/',
		'lev'	=> 2,
	),
	//参加调研
	array(
		'app' 	=> 'survey',
		'm'		=> 'detail',
		'a'		=> '',
		'match' => '',
		'lev'	=> 2,
	),
	//提交调研问卷
	array(
		'app' 	=> 'survey',
		'm'		=> 'ajax',
		'a'		=> '',
		'match' => '/cmd=101/',
		'lev'	=> 2,
	),
	//推荐有赏主页
	array(
		'app' 	=> 'recruit',
		'm'		=> 'recommend',
		'a'		=> '',
		'match' => '',
		'lev'	=> 2,
	),
	//职位详情
	array(
		'app' 	=> 'recruit',
		'm'		=> 're_job',
		'a'		=> '',
		'match' => '',
		'lev'	=> 2,
	),
	//推荐有赏主页
	array(
		'app' 	=> 'recruit',
		'm'		=> 'ajax',
		'a'		=> '',
		//'match' => 'cmd=!(103|104|105|106|110|111|112|113|114|116|117)',
		'lev'	=> 2,
	),
);

//end