<?php
/**
 * 企业号Access Token相关配置
 * @author yangpz
 * @date 2016-02-19
 */

return array(
	//缓存时长，单位：秒
	'alive_time'			=> array(
		'atoken'		=> 60,
		'stoken'		=> 60,
	),
	
	//缓存key，需保证多个端一致
	'cache_key_prefix'		=> array(	
		'atoken'			=> 'redis:qy:ew365:access:token',		
		'atoken_time'		=> 'redis:qy:ew365:access:token:time',
	
		'stoken'			=> 'redis:qy:ew365:suite:access:token',
		'stoken_time'		=> 'redis:qy:ew365:suite:access:token:time',
	),
);

//end