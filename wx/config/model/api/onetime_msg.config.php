<?php
/**
 * 一次性消息key值映射表，请保持key唯一
 * 每次更新需保持pc、wx端同步更新（目前pc不需要）
 * 删除已失效的消息时把相关key注释掉即可，数据库会自动更新
 */

return array(
	
	// 'one_time_msg_test',		//一次性消息测试

	'fingerprint_setting',		//指纹识别设置
	'service_and_support',		//产品服务与支持
	
	'invite_proc',				//邀请同事发起流程
	'employee_share',			//员工分享
	'suggest_buy',				//推荐企业购买
	'admin_service',			//管理员服务中心

	);

//end