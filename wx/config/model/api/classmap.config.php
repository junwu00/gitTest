<?php
/**
 * 类命名空间与缩写的映射关系
 * 
 * 注意：
 * 1.模块（model）内所有映射都必须、且只能在该文件配置（即模块内其他所有地方不允许再出现g_map或g_maps方法）
 * 2.必须保证值的唯一性
 * 
 * 目的：
 * 1.模块内所有类映射关系统一管理，避免命名重复导致的系统异常
 */ 
$maps = array(
	//公用的扩展、模块的配置------------------------------------------------
	'package\\db\\pdo\\Mysql' 				=> 'pkg_db',
	'package\\cache\\Redis' 				=> 'pkg_redis',
	'package\\cache\\RedisCookie'	 		=> 'pkg_rcookie',
	'package\\cache\\RedisSession'			=> 'pkg_rsession',
	'package\\template\\Smarty' 			=> 'pkg_smarty',
	'package\\net\\http\\Value' 			=> 'pkg_val',
	'package\\net\\IP' 						=> 'pkg_ip',
    'package\\net\\http\\Http' 				=> 'pkg_http',
    'package\\net\\http\\CurlClient' 	    => 'pkg_curl',
    'package\\crypto\\DesPlus'		 		=> 'pkg_des_plus',
	'package\\net\\http\\Sign' 				=> 'pkg_sign',			//接口签名验证
	'package\\bdmap\\Bdmap' 				=> 'pkg_map',			//地图插件
	'package\\excel\\Excel'		 			=> 'pkg_excel',
    'package\\email\\Email'		 			=> 'pkg_email',
    'package\\notice\\ErrorFileNotice'		=> 'pkg_err_file_notice',
    'package\\file\\image\\WaterMark'		=> 'pkg_watermark',		//图片水印
    'package\\qrcode\\Qrcode'				=> 'pkg_qrcode',		//二维码

	//微信企业号
	'package\\weixin\\qy\\WXQYDept'			=> 'wxqy_dept',
	'package\\weixin\\qy\\WXQYUser'			=> 'wxqy_user',
	'package\\weixin\\qy\\WXQYTag'			=> 'wxqy_tag',
	'package\\weixin\\qy\\WXQYAccessToken'	=> 'wxqy_token',
	'package\\weixin\\qy\\WXQYMsg'			=> 'wxqy_msg',
	'package\\weixin\\qy\\WXQYMenu'			=> 'wxqy_menu',
	'package\\weixin\\qy\\WXQYOAuth'		=> 'wxqy_auth',
	'package\\weixin\\qy\\WXQYSuite'		=> 'wxqy_suite',
    'package\\weixin\\qy\\WXQYMedia'		=> 'wxqy_media',
    'package\\weixin\\qy\\WXQYSoter'		=> 'wxqy_soter',

	//共同服务--------------------------------------------------------
	'model\\api\\server\\Login'				=> 'api_login', 
	'model\\api\\server\\Notice'			=> 'api_notice',
	'model\\api\\server\\Response'			=> 'api_resp',
	'model\\api\\server\\Access'			=> 'api_access',
	'model\\api\\server\\AToken'			=> 'api_atoken',
	'model\\api\\server\\Messager'			=> 'api_messager',		//PC推送
	'model\\api\\server\\Nmsg'				=> 'api_nmsg',			//WX推送
	'model\\api\\server\\Media'				=> 'api_media',			//多媒体
	'model\\api\\server\\Char'				=> 'api_char',			//拼音转换
	'model\\api\\server\\Format'			=> 'api_format',		//格式校验
	'model\\api\\server\\Upload'			=> 'api_upload',		//上传
	'model\\api\\server\\Download'			=> 'api_download',		//下载
	'model\\api\\server\\Jssdk'				=> 'api_jssdk',			//JSSDK
	'model\\api\\server\\SignApi'			=> 'api_sign',			//领签统一调用
	'model\\api\\server\\FingerPrint'		=> 'api_fingerprint',	//指纹相关调用

	'model\\api\\server\\TriggerForm'		=> 'api_triggerform',	//触发表单

	//数据库操作类
	'model\\api\\dao\\Group'				=> 'dao_group',
	'model\\api\\dao\\Admin'				=> 'dao_admin',
	'model\\api\\dao\\Com'					=> 'dao_com',
	'model\\api\\dao\\Struct'				=> 'dao_struct',
	'model\\api\\dao\\Contact'				=> 'dao_contact',
	'model\\api\\dao\\Dept'					=> 'dao_dept',
	'model\\api\\dao\\User'					=> 'dao_user',
	'model\\api\\dao\\LeaderRelationship'	=> 'dao_leader',		//上下级关系
	'model\\api\\dao\\App'					=> 'dao_app',
	'model\\api\\dao\\ComApp'				=> 'dao_com_app',
	'model\\api\\dao\\ComChildApp'			=> 'dao_com_child_app',
	'model\\api\\dao\\Suite'				=> 'dao_suite',
	'model\\api\\dao\\AgencyInfo'			=> 'dao_agency_info',
	'model\\api\\dao\\Capacity'				=> 'dao_capacity',		//容量计算
	'model\\api\\dao\\User_general'			=> 'dao_user_general',	//常用联系人
	'model\\api\\dao\\ShareStruct'			=> 'dao_share_struct',	//虚拟架构
	'model\\api\\dao\\News'					=> 'dao_news',
	'model\\api\\dao\\Vip'					=> 'dao_vip',
	'model\\api\\dao\\Service'				=> 'dao_service',
	'model\\api\\dao\\Salary'				=> 'dao_salary',
	'model\\api\\dao\\Material'				=> 'dao_material',
	'model\\api\\dao\\Tag'					=> 'dao_tag',
	'model\\api\\dao\\ContactConfig'		=> 'dao_contact_config',
    'model\\api\\dao\\RecentChoose'			=> 'dao_recent_choose',

	//报表相关服务类
	'model\\api\\server\\report\\Report'				=> 'api_report', 

		//考勤、外勤、请假应用相关
	'model\\api\\server\\workshift\\Workshift'		=> 'ws_workshift',	// 排班
	'model\\api\\server\\workshift\\Shift'			=> 'ws_shift',		// 班次
	'model\\api\\server\\workshift\\Core'			=> 'ws_core',		// 核心
	'model\\api\\server\\workshift\\Checkwork'		=> 'ws_checkwork',	// 考勤
	'model\\api\\server\\workshift\\Rest'			=> 'ws_rest',		// 请假
	'model\\api\\server\\workshift\\Legwork'		=> 'ws_legwork',	// 外勤
	'model\\api\\server\\workshift\\Config'			=> 'ws_config',		// 配置

	//流程相关服务类
	'model\\api\\server\\mv_proc\\Mv_File'				=> 'mv_file', 
	'model\\api\\server\\mv_proc\\Mv_Form'				=> 'mv_form', 
	'model\\api\\server\\mv_proc\\Mv_Form_User'			=> 'mv_form_user',
    'model\\api\\server\\mv_proc\\Mv_Form_Rule'	        => 'mv_form_rule',
	'model\\api\\server\\mv_proc\\Mv_Formsetinst'		=> 'mv_formsetinst', 
	'model\\api\\server\\mv_proc\\Mv_Formsetinst_Name'	=> 'mv_formsetinst_name', 
	'model\\api\\server\\mv_proc\\Mv_Linksign'			=> 'mv_linksign', 
	'model\\api\\server\\mv_proc\\Mv_Notify'			=> 'mv_notify', 
	'model\\api\\server\\mv_proc\\Mv_Reference'			=> 'mv_reference', 
	'model\\api\\server\\mv_proc\\Mv_Work'				=> 'mv_work', 
	'model\\api\\server\\mv_proc\\Mv_Work_Node'			=> 'mv_work_node', 
	'model\\api\\server\\mv_proc\\Mv_Workinst'			=> 'mv_workinst', 
	'model\\api\\server\\mv_proc\\Mv_Report'			=> 'mv_report', 

);
g_maps($maps);

//END