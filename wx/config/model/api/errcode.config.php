<?php
/**
 * 错误码配置
 * 
 * @author yangpz
 * @date 2016-04-08
 */

//获取当前模块名（即目录名）
$model_dir = dirname(__FILE__);
$model_dir = explode(DS, $model_dir);
$model_dir_name = $model_dir[count($model_dir) - 1];
$model_name = strtoupper($model_dir_name);					

if (!isset($GLOBALS['MODEL_ERRCODE_PREFIX'][$model_name])) {
	throw new \Exception('未配置模块错误码前缀：' . $model_name);
}

//前缀要求为：01 
//（与\config\main\errcode.config.php中配置一致）
final class ERRCODE_API {
	/** 01000 未知异常 */
	public static $UNKNOW_EXP 					= array('code' => '01000', 'desc' => '未知异常');
	/** 01001 不合法的请求 */
	public static $REQUEST_NOT_ALLOW 			= array('code' => '01001', 'desc' => '不合法的请求');
	/** 01002 请求参数不完整 */
	public static $REQUEST_PARAMS_NOT_COMPLETE 	= array('code' => '01002', 'desc' => '请求参数不完整');
	
}

//end