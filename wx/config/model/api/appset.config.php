<?php
/**
 * 配置可关闭的应用ID
 * 对应qy端privilege模块下的appset.config.php文件（需保持一致）
 */

//配置可关闭的应用ID
return array(
	"25" => array(
			"name" => "综合考勤", 
			"key" => "workshift_key",
			'id' => 25,							//应用ID
			'open_time' => 4070880000,			//开放时间时间戳 2099-01-01
			'is_vip' => 0 						//是否VIP功能
		),
);