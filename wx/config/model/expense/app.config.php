<?php
/**
 * 应用的配置文件_流程审批
 * 
 * @author luja
 * @date 2016-05-17
 * 
 */

return array(
	'id' 		=> 26,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('dao_com_app') -> get_sie_id(26),
	'name' 		=> 'expense',
	'cn_name' 	=> '费用报销',
	// 'icon' 		=> SYSTEM_HTTP_APPS_ICON.'Process.png',
        
);

// end of file