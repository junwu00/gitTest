<?php
/**
 * 类命名空间与缩写的映射关系
 * 
 * 注意：
 * 1.模块（model）内所有映射都必须、且只能在该文件配置（即模块内其他所有地方不允许再出现g_map或g_maps方法）
 * 2.必须保证值的唯一性
 * 
 * 目的：
 * 1.模块内所有类映射关系统一管理，避免命名重复导致的系统异常
 */
$maps = array(
	//当前模块的配置
	'model\\finstat\\dao\\DaoFinstat'				=> 'dao_finstat', 
	'model\\finstat\\dao\\DaoAccount'				=> 'dao_account', 
	'model\\finstat\\dao\\DaoAcct'					=> 'dao_acct', 
	'model\\finstat\\dao\\DaoExcel'					=> 'dao_excel', 

	'model\\finstat\\server\\ServerFinstat'			=> 'ser_finstat', 

	'model\\finstat\\view\\FinstatNotice'			=> 'view_notice', 
	'model\\finstat\\view\\FinstatIndex'			=> 'view_index', 
	'model\\finstat\\view\\FinstatAjax'				=> 'view_ajax', 
);
g_maps($maps);

//END