<?php
/**
 * 模块基本信息配置
 */

$model_root = dirname(dirname(__FILE__));
return array(
	//基础配置
	'name'				=> 'demo',											//模块名称
	'cn_name'			=> '演示',												//模块中文名称
	'dir'				=> $model_root . DS,									//模块根路径
	'view_dir'			=> MAIN_VIEW . 'demo' . DS,							//模块视图根路径
	'log_dir'			=> MAIN_DATA . 'demo' . DS . 'logs' . DS,				//模块视图根路径
	'upload_dir'		=> MAIN_DATA . 'demo' . DS . 'upload' . DS,			//本地上传/下载根路径
	'smarty_plugin'		=> MAIN_PACKAGE . 'template' . DS . 'smarty_plugin',
	'smarty_conf_dir'	=> MAIN_VIEW . 'demo' . DS,
	'app_id'			=> 13,													//应用ID
	
	'log_lev'			=> 0,													//自定义日志的输出级别，从0~5共6个级别，0 => 所有日志，1 => 仅正常及更高级日志，2 => 仅突出通知及更高级，
																				//3 => 仅警告及更高级，4 => 仅错误及更高级，5 => 仅致使错误
	
	//其他自定义配置...
);