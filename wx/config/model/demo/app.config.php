<?php
/**
 * 应用的配置文件_移动外勤
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 		=> 13,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('dao_com_app') -> get_sie_id(13),
	'name' 		=> 'index',
	'cn_name' 	=> '主页型应用',
	// 'icon' 		=> SYSTEM_HTTP_APPS_ICON.'outsidework.png',
	'menu' => array(
		array('id' => 'bmenu-1', 'name' => '外勤', 'icon' => '','childs' => array(
				array('id' => 'bmenu-2-1', 'name' => '外勤计划', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?&app=legwork&a=mylist', 'icon' => ''),
				array('id' => 'bmenu-2-2', 'name' => '外勤记录', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?&app=legwork&a=myLegwork', 'icon' => ''),
			),
		),
		array('id' => 'bmenu-2', 'name' => '定位', 'url' => MAIN_DYNAMIC_DOMAIN.'index.php?&app=legwork&a=planLocation', 'icon' => ''),
		array('id' => 'bmenu-3', 'name' => '新建计划', 	'url' => MAIN_DYNAMIC_DOMAIN.'index.php?&app=legwork&a=create', 'icon' => ''),
	),
);

// end of file