<?php
/**
 * 数据库配置定义
 *
 * @author yangpz
 * @create 2015-12-15
 */
require (__DIR__.'/env.define.config.php');

/*--begin-------数据库链接信息配置-------------*/
$ENV = include (__DIR__ . '/env.now.config.php');
$CONF_BASE_DIR = WEBPATH . "/install_conf/{$ENV}/";
$DB_CONF = json_decode(file_get_contents($CONF_BASE_DIR . 'db.config.json'), true);
$MAIN_DB_CONFIG = array(
    'host' 		=> $DB_CONF['host'],
    'port' 		=> $DB_CONF['port'],
    'user' 		=> $DB_CONF['user'],
    'pass' 		=> $DB_CONF['pass'],
    'name' 		=> $DB_CONF['name'],
    'charset' 	=> $DB_CONF['charset'],
    'debug'     => $DB_CONF['debug'],
);
!isset($GLOBALS['MAIN_DB_CONFIG']) and $GLOBALS['MAIN_DB_CONFIG'] = $MAIN_DB_CONFIG;
/*--end-------数据库链接信息配置-------------*/

return $MAIN_DB_CONFIG;

/* End of file  db.config.php */
