<?php
/**
 * 运行环境配置
 * User: yangpz
 * Date: 2017/1/5
 * Time: 11:57
 */
!defined('DS') && define('DS', DIRECTORY_SEPARATOR);
$MAIN_ROOT = dirname(dirname(dirname(__FILE__))) . DS;
$NOW_YEAR = @date('Y');

$CONF_FILES = include ($MAIN_ROOT . 'config/model/install/file.config.php');
$DOMAIN_CONF = json_decode(file_get_contents($CONF_FILES['domain.config']), true);
$DOMAIN_CONF_THIS = $DOMAIN_CONF['wx'];

$ADVANCE_CONF = json_decode(file_get_contents($CONF_FILES['advance.config']), true);
$COM_CONF = json_decode(file_get_contents($CONF_FILES['company.config']), true);
$POWERBY_CONF = json_decode(file_get_contents($CONF_FILES['powerby.config']), true);

// 当前访问域名
$G_HOST = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
empty($G_HOST) && $G_HOST = $DOMAIN_CONF_THIS['host'];

// 当前协议
$G_SCHEME = $DOMAIN_CONF_THIS["scheme"];
$QY_SIDE = "qy";
$WX_SIDE = "wx";

// 使用的前端视图
$G_VIEW = 'view';   //todo

$ENV_CONF = array(
    'MAIN_NAME'             => $COM_CONF[0]['name'],                            // 定义项目名称
    'MAIN_VERSION'          => 'V6.2.2',                                        // 定义项目版本号
    'MAIN_COPYRIGHT'        => "{$POWERBY_CONF[0]['name']} 技术支持", // 默认版权信息
    'MAIN_IDENT'            => $G_HOST,                                         // 定义当项目名称相同时的唯一标识
    'MAIN_ROOT'             => $MAIN_ROOT,                                      // 定义根目录
    'MAIN_PACKAGE'          => $MAIN_ROOT . 'package' . DS,                     // 定义工具包目录
    'MAIN_MODEL'            => $MAIN_ROOT . 'model' . DS,                       // 定义模块目录
    'MAIN_VIEW'             => $MAIN_ROOT . $G_VIEW . DS,                       // 定义前端目录
    'MAIN_VIEW_NAME'        => $G_VIEW,                                         // 定义前端目录名称
    'MAIN_DATA'             => $MAIN_ROOT . 'data' . DS,                        // 定义写数据目录
    'MAIN_LOG_DIR'          => $MAIN_ROOT . 'data' . DS . 'logs' . DS,          // 定义默认日志目录
    'MAIN_USED_LOG'         => true,                                            // 是否启用日志记录
    'MAIN_MODEL_VAR'        => 'model',                                         // 定义用作判断是哪个模块的参数名称
    'MAIN_ENCODING'         => 'UTF-8',                                         // 指定页面字符编码
    'MAIN_TIMEZONE'         => 'PRC',                                           // 指定默认时间区域
    'MAIN_HOST'             => $G_HOST,                                         // 当前动态域名
    'MAIN_SCHEME'           => $G_SCHEME,                                       // 当前uri协议
    'MAIN_DYNAMIC_DOMAIN'   => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/",                      // 动态资源域名
    'MAIN_STATIC_DOMAIN'    => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/",                      // 静态资源域名（需要每个model自行定义）
    'MAIN_COOKIE_DOMAIN'    => $G_HOST,                                         // cookie数据保存域名

    'MAIN_STATIC_VER'	    => "20211108",                                  // 新框架静态资源版本号
    'DEF_STATIC_VER'	    => "20211108",                                  // 旧框架静态资源版本号

    'IS_PRIVATE'            => 1,                                           //是否私有化

    //----------------------------私有化授权相关-------------------------------------
    'API_AUTH_SRC'          => 'wx',
    'API_AUTH_CALLER'       => $ADVANCE_CONF['key']['auth']['caller'],
    'API_AUTH_SECRET'       => $ADVANCE_CONF['key']['auth']['secret'],
    'API_AUTH_URL'          => 'https://api.easywork365.com/api/private',

    //----------------------------QY端、PC端、WX端及CDN-------------------------------------
    'PC_DYNAMIC_DOMAIN'     => $DOMAIN_CONF['pc']['root_url'],                 // PC端动态域名
    'QY_DYNAMIC_DOMAIN'     => $DOMAIN_CONF['qy']['root_url']."{$QY_SIDE}/",                 // QY端动态域名

    //---------------------------多媒体相关--------------------------
    'MEDIA_SRC'             => 'wx',                                                // 多媒体资源服务来源标识
    'MEDIA_CALLER'          => $ADVANCE_CONF['key']['file']['caller'],                                                      // 调用者
    'MEDIA_URL_KEY'         => $ADVANCE_CONF['key']['file']['secret'],                                                      // 多媒体资源服务的加密后缀
    'MEDIA_URL_PREFFIX'     => $DOMAIN_CONF['file']['scheme'] . '://' . $DOMAIN_CONF['file']['full_host'] . '/data/',       // 多媒体资源url前缀
    'CDN_MEDIA_URL_HOST'    => $DOMAIN_CONF['file']['scheme'] . '://' . $DOMAIN_CONF['file']['full_host'],                  // 多媒体资源服务的cdn地址

    'MEDIA_PREVIEW_PREFFIX' => 'https://cdn.file.easywork365.com/api/file/preview/external/view',                           // 预览服务url前缀
    'MEDIA_PREVIEW_CALLER'  => $ADVANCE_CONF['key']['preview']['caller'],                                                   // 预览者
    'MEDIA_PREVIEW_KEY'     => $ADVANCE_CONF['key']['preview']['secret'],                                                   //

    //---------------------------第三方接口相关-----------------------------
    'SIGN_URL_HOST'         => 'https://3rdapi.easywork365.com',                    // 领签项目地址
    'SIGN_MEDIA_URL_HOST'   => 'https://acdn.3rdapi.easywork365.com/data',          // 领签项目静态资源地址

    //----------------------------消息服务相关--------------------------
    'MESSAGER_KEY'          => $ADVANCE_CONF['key']['msg']['secret'],                                                       // 消息服务交互密钥
    'MESSAGER_PREFFIX'      => $DOMAIN_CONF['msg']['scheme'] . '://' . $DOMAIN_CONF['msg']['full_host'] . '/',              // 消息推送服务地址

    //--------------------------------访问统计相关---------------------------------------
    'REDIS_ACCESS_KEY'      => '80a5221ece0bef9fd56599a9e1eb3cea', //MD5(access_key_ew365)  // 统一缓存KEY
    'REDIS_ACCESS_TYPE'     => 'WX',                                                        // 访问端类型
);

$ENV_CONF['SYSTEM_APP_INFO_ICON_PATH'] = $ENV_CONF['QY_DYNAMIC_DOMAIN'] . 'data/api/upload/icon/2/';    // 应用图标地址
$ENV_CONF['SYSTEM_MSG_INFO_ICON_PATH'] = $ENV_CONF['MAIN_STATIC_DOMAIN'] . 'data/api/upload/msg_icon/';  // 消息推送显示图标

// 多媒体资源服务的host地址
$media_full_host = $DOMAIN_CONF['file']['full_host'];
isset($DOMAIN_CONF['file']['local_full_host']) and $media_full_host = $DOMAIN_CONF['file']['local_full_host'];
$ENV_CONF['MEDIA_URL_HOST'] = sprintf("%s://%s", $DOMAIN_CONF['file']['scheme'], $media_full_host);

return $ENV_CONF;
//END