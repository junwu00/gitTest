<?php
/**
 * 声明常量
 * User: yangpz
 * Date: 2017-08-30
 * Time: 16:05
 */

!defined('DS') && define('DS', DIRECTORY_SEPARATOR);
!defined('APPSPATH') && define('APPSPATH', dirname(dirname(__DIR__)));
!defined('WEBPATH') && define('WEBPATH', dirname(APPSPATH));

// end