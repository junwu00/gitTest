<?php
/**
 * 运行环境配置
 * User: yangpz
 * Date: 2017/1/5
 * Time: 11:57
 */
require (__DIR__.'/env.define.config.php');

$ENV = include (__DIR__ . '/env.now.config.php');
$MAIN_ROOT = APPSPATH . DS;
$CONF_BASE_DIR = WEBPATH . "/install_conf/{$ENV}/";

$SYSTEM_CONF    = json_decode(file_get_contents($CONF_BASE_DIR . 'system.config.json'), true);
$ADVANCE_CONF   = json_decode(file_get_contents($CONF_BASE_DIR . 'advance.config.json'), true);
$DOMAIN_CONF    = json_decode(file_get_contents($CONF_BASE_DIR . 'domain.config.json'), true);
$STATIC_CONF    = json_decode(file_get_contents($CONF_BASE_DIR . 'static.config.json'), true);
$CACHE_CONF     = json_decode(file_get_contents($CONF_BASE_DIR . 'cache.config.json'), true);
$REDIS_CONF     = json_decode(file_get_contents($CONF_BASE_DIR . 'redis.config.json'), true);
$EXT_CONF       = json_decode(file_get_contents($CONF_BASE_DIR . 'extends.config.json'), true);
$THIS_DOMAIN_CONF = $DOMAIN_CONF['wx'];

// 当前访问域名
$G_HOST = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
empty($G_HOST) && $G_HOST = $THIS_DOMAIN_CONF['host'];

// 当前协议
$G_SCHEME = $THIS_DOMAIN_CONF["scheme"];
$QY_SIDE = "qy";
$WX_SIDE = "wx";

$EXTEND_CONF = array(
    //---------------------------项目涉及企业-----------------------------------------
    'MAIN_CONF_PATH'        => $CONF_BASE_DIR,

    //----------------------------安装/项目初始化相关----------------------------------
    // 安装文件锁定路径
    'MAIN_INSTALL_LOCK_FILE' => $MAIN_ROOT . 'data' . DS . "install.lock",

    'MAIN_NAME'             => $SYSTEM_CONF['base']['name'],                // 定义项目名称
    'MAIN_FAVICON'          => $SYSTEM_CONF['base']['favicon'],             // 浏览器标签页icon
    'MAIN_LOGO'             => $SYSTEM_CONF['base']['logo'],                // 品牌logo
    'MAIN_VERSION'          => $SYSTEM_CONF['base']['version'],             // 定义项目版本号
    'MAIN_COPYRIGHT'        => $SYSTEM_CONF['base']['copyright'],           // 默认版权信息
    'MAIN_POWERBY'          => $SYSTEM_CONF['powerby']['desc'],             // 默认技术支持
    'MAIN_POWERBY_URL'      => $SYSTEM_CONF['powerby']['url'],              // 默认技术支持链接
    'MAIN_IDENT'            => $THIS_DOMAIN_CONF['host'],                   // 定义当项目名称相同时的唯一标识

    'MAIN_HOST'             => $G_HOST,                                     // 当前动态域名
    'MAIN_SCHEME'           => $G_SCHEME,                                   // 当前uri协议
    'MAIN_DYNAMIC_DOMAIN'   => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/",                  // 动态资源域名
    'MAIN_STATIC_DOMAIN'    => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/",                  // 静态资源域名（需要每个model自行定义）
    'MAIN_HTTP_LOGO'        => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/data/logo.png",     // 企业LOGO
    'MAIN_HTTP_QRCODE'      => "{$G_SCHEME}://{$G_HOST}/{$WX_SIDE}/data/qrcode.jpg",   // 企业号二维码
    'MAIN_COOKIE_DOMAIN'    => $G_HOST,                                     // cookie数据保存域名
    'MAIN_COOKIE_PATH'      => "/{$WX_SIDE}",

    'DIRECT_SUITE_AGT_ID'   => 0,                                           // 默认账号的代理商ID
    'DIRECT_SUITEE_AGT_ID'  => 0,                                           // 默认账号的代理商ID
    'MAIN_STATIC_VER'	    => $STATIC_CONF['main']['version'],             // 新框架静态资源版本号
    'DEF_STATIC_VER'	    => $STATIC_CONF['default']['version'],          // 旧框架静态资源版本号

    'IS_PRIVATE'            => 1,                                           //是否私有化
    'USE_SIGN'              => $EXT_CONF['use_sign'],                       //是否启用电子签署


    //----------------------------私有化授权相关-------------------------------------
    'API_AUTH_SRC'          => 'wx',
    'API_AUTH_CALLER'       => $ADVANCE_CONF['key']['auth']['caller'],
    'API_AUTH_SECRET'       => $ADVANCE_CONF['key']['auth']['secret'],
    'API_AUTH_URL'          => $EXT_CONF['api_auth_url'],


    //----------------------------PC端、WX端及CDN-------------------------------------
    'QY_HOST'               => $DOMAIN_CONF['qy']['host'],
    'PC_HOST'               => $DOMAIN_CONF['qy']['host'],
    'WX_HOST'               => $DOMAIN_CONF['wx']['host'],

    'QY_MEDIA_CDN'          => $DOMAIN_CONF['qy']['root_url'],                      // qy端静态资源CDN地址

    'QY_DYNAMIC_DOMAIN'     => $DOMAIN_CONF['qy']['root_url']."{$QY_SIDE}/",                      // QY端动态域名
    'WX_DYNAMIC_DOMAIN'     => $DOMAIN_CONF['wx']['root_url']."{$WX_SIDE}/",                      // WX端动态域名
    'PC_DYNAMIC_DOMAIN'     => $DOMAIN_CONF['pc']['root_url'],                      // PC端动态域名

    //------------------------------代理商相关------------------------------------------
    'AGENCY_HTTP_DOAMIN'    => 'https://agent.easywork365.com',                     // 代理商后台域名

    //----------------------------多媒体相关--------------------------
    'MEDIA_SRC'             => 'qy',                                                                                        // 多媒体资源服务来源标识
    'MEDIA_CALLER'          => $ADVANCE_CONF['key']['file']['caller'],                                                      // 调用者
    'MEDIA_URL_KEY'         => $ADVANCE_CONF['key']['file']['secret'],                                                      // 多媒体资源服务的加密后缀
    'MEDIA_URL_PREFFIX'     => $DOMAIN_CONF['file']['scheme'] . '://' . $DOMAIN_CONF['file']['full_host'] . '/data/',       // 多媒体资源url前缀
    'CDN_MEDIA_URL_HOST'    => $DOMAIN_CONF['file']['scheme'] . '://' . $DOMAIN_CONF['file']['full_host'],                  // 多媒体资源服务的cdn地址

    'MEDIA_PREVIEW_PREFFIX' => $EXT_CONF['media_preview_prefix'],                                                           // 预览服务url前缀
    'MEDIA_PREVIEW_CALLER'  => $ADVANCE_CONF['key']['preview']['caller'],                                                   // 预览者
    'MEDIA_PREVIEW_KEY'     => $ADVANCE_CONF['key']['preview']['secret'],                                                   //

    //---------------------------第三方接口相关-----------------------------
    'SIGN_URL_HOST'         => $EXT_CONF['sign_domain'],                    // 领签项目地址
    'SIGN_MEDIA_URL_HOST'   => $EXT_CONF['sign_cdn_prefix'],                // 领签项目静态资源地址

    //----------------------------消息服务相关--------------------------
    'MESSAGER_KEY'          => $ADVANCE_CONF['key']['msg']['secret'],                                                       // 消息服务交互密钥
    'MESSAGER_PREFFIX'      => $DOMAIN_CONF['msg']['scheme'] . '://' . $DOMAIN_CONF['msg']['full_host'] . '/',              // 消息推送服务地址

    //-----------------------------redis服务----------------------------
    'REDIS_HOST'            => $REDIS_CONF['host'],
    'REDIS_PORT'            => $REDIS_CONF['port'],

    //------------------------------Token信息保存----------------------------------------
    'REDIS_QY_EW365_SUITE_ACCESS_TOKEN'         => $CACHE_CONF['redis']['suite_access_token'],
    'REDIS_QY_EW365_SUITE_ACCESS_TOKEN_TIME'    => $CACHE_CONF['redis']['suite_access_token_time'],
    'REDIS_QY_EW365_COMPANY_ACCESS_TOKEN'       => $CACHE_CONF['redis']['com_access_token'],
    'REDIS_QY_EW365_COMPANY_ACCESS_TOKEN_TIME'  => $CACHE_CONF['redis']['com_access_token_time'],

    //--------------------------------访问统计相关---------------------------------------
    'REDIS_ACCESS_KEY'                          => $CACHE_CONF['redis']['access_key'], //MD5(access_key_ew365)       // 统一缓存KEY
    'REDIS_ACCESS_TYPE'                         => 'WX',                                                             // 访问端类型
);

$EXTEND_CONF['SYSTEM_APP_INFO_ICON_PATH'] = $EXTEND_CONF['MAIN_STATIC_DOMAIN'] . 'data/api/upload/icon/2/';                           // 应用图标地址
$EXTEND_CONF['SYSTEM_MSG_INFO_ICON_PATH'] = rtrim($DOMAIN_CONF['wx']['root_url'], '\\/') . '/data/api/upload/msg_icon/';           // wx消息推送显示图标

// 多媒体资源服务的host地址
$media_full_host = $DOMAIN_CONF['file']['full_host'];
isset($DOMAIN_CONF['file']['local_full_host']) and $media_full_host = $DOMAIN_CONF['file']['local_full_host'];
$EXTEND_CONF['MEDIA_URL_HOST'] = sprintf("%s://%s", $DOMAIN_CONF['file']['scheme'], $media_full_host);

return $EXTEND_CONF;
//END