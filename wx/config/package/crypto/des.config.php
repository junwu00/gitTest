<?php
/**
 * des加密默认配置文件
 *
 * @author JianMing Liang
 * @create 2016-01-28 10:13:04
 */

$des_conf = array(
    // des加密的两组密钥
    'DES_KEY' => array(17, 47, 77, 47, 57, 87, 37, 57),
    'DES_IV'  => array(18, 28, 28, 38, 48, 38, 28, 88),

    // des plus的加密增加选项
    // 密钥偏移量：0-15，一般更新密钥仅需调整该值
    'DES_OFFSET' => 9
);

return $des_conf;

/* End of this file */