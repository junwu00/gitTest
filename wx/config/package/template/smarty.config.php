<?php
/**
 * smarty的默认配置
 *
 * @author LiangJianMing
 * @create 2016-01-12 11:36:04
 */

$smarty_conf = array(
    // 是否开启调试模式
    'debugging' => false,
    // 当未定义变量时，是否报错
    'error_unassigned' => true,
    // 是否启用子目录分级
    'use_sub_dirs' => true,
    // 是否开启编译检测，即模板文件内容有无改变
    'compile_check' => true,
    // smarty变量左边界
    'left_delimiter' => '<{',
    // smarty变量右边界
    'right_delimiter' => '}>',
    // 错误报告类型
    'error_reporting' => null,

    // -------以下是自定义参数------------------
    // smarty缓存目录
    'cache_dir' => MAIN_DATA . 'smarty/cache',
    // smarty编译文件缓存目录
    'compile_dir' => MAIN_DATA . 'smarty/compile_dir',
    // 模板根目录
    'template_dir' => MAIN_VIEW,
);

return $smarty_conf;

/* End of this file */