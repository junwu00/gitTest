<?php
/**
 * redis_cookie默认配置文件
 *
 * @author JianMing Liang
 * @create 2016-01-26 17:01:21
 */

$cookie_conf = array(
    // cookie中的每个变量(包含cookie_id本身), 在redis中的最大生存时间
    'max_life_time' => 24 * 3600,
    // 用作生成 cookie_id 的标识值
    'key_preffix' => 'redis_cookie_id',
    // 客户端cookie_id的变量名
    'id_client_var' => 'EW_COOKIE_ID',
    // 客户端cookie_id的保存domain
    'id_client_host' => MAIN_HOST,
    // cookie_id保存于客户端时的有效时长，单位：秒
    'id_valid_time' => 365 * 24 * 3600,
);

return $cookie_conf;

/* End of this file */