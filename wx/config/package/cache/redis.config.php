<?php
/**
 * redis默认配置文件
 *
 * @author JianMing Liang
 * @create 2016-01-26 17:01:21
 */

$redis_file = dirname(MAIN_ROOT) . '/install_conf/redis.config.json';
$MAIN_REDIS_CONFIG = json_decode(file_get_contents($redis_file), true);

$redis_conf = array(
    'host' => $MAIN_REDIS_CONFIG["host"],
    'port' => $MAIN_REDIS_CONFIG["port"],
);

if(!empty($MAIN_REDIS_CONFIG["pass"])){
    $redis_conf['pass'] = $MAIN_REDIS_CONFIG["pass"];
}

return $redis_conf;

/* End of this file */