<?php
/**
 * redis_session 默认配置文件
 *
 * @author JianMing Liang
 * @create 2016-01-26 17:01:21
 */

$session_conf = array(
    // session_id 无操作后的生存时间
    'max_life_time' => 3600,
    // 用作生成 session_id 的标识值
    'key_preffix' => 'WXEASYWORK365:REDISSESSION',
);

return $session_conf;

/* End of this file */