<?php
/**
 * gmagick图片处理默认配置
 *
 * @author JianMing Liang
 * @create 2016-01-28 10:13:04
 */

$gm_conf = array(
    // 缩放处理后的图片质量比例（%）
    'resize_quality' => '60',
);

return $gm_conf;

/* End of this file */