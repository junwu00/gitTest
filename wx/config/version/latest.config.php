<?php
/**
 * 最新的版本配置文件
 *
 * @author JianMing Liang
 * @create 2016-01-26 17:01:21
 */

$version_conf = array(
    // 项目版本号
    'project_version' => '3.0.1',
);

return $version_conf;

/* End of this file */