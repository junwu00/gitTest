<?php
/**
 * 负载加载主框架配置，其他配置需要手动按需加载
 *
 * @author JianMing Liang
 * @create 2016-01-26 10:21:10
 */
$config_root_dir = __DIR__;
$config_init_file_name = basename(__FILE__);

load_config("main/");
// 仅加载主框架配置
load_all_file($config_root_dir . '/main');

/**
 * include指定文件，并返回加载结果
 *
 * @access public
 * @param string $name 配置文件简称，如：/config/package/smarty.config.php 可简称为 package/smarty
 * @return mixed
 */
function load_config($name) {
    if (is_file($name)) {
        $ret = include($name);
        return $ret;
    }

    $sfx = ".config.php";
    preg_match("/.+\/$/", $name) and $sfx = "config.php";

    $file_path = __DIR__ . "/{$name}{$sfx}";
    if (!is_file($file_path)) return false;

    $ret = include($file_path);
    return $ret;
}

/* End of this file */