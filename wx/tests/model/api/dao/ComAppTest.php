<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class ComAppTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_com_app');
	}
	
	/** 获取应用的我方套件id */
	public function test_get_sie_id() {
		$ret = $this -> instance -> get_sie_id(1);
		return $ret;
	}

	/** 根据企业ID获取应用列表 */
	public function test_list_by_com_id() {
		$ret = $this -> instance -> list_by_com_id(1);
		return $ret;
	}

	/** 获取企业指定应用 */
	public function test_get_by_app_id() {
		$ret = $this -> instance -> get_by_app_id(1);
		return $ret;
	}

	/** 获取所有已安装的应用信息 */
	public function test_list_all_auth_app() {
		$ret = $this -> instance -> list_all_auth_app(1);
		return $ret;
	}

	/** 根据token获取企业应用信息 */
	public function test_get_by_token() {
		$ret = $this -> instance -> get_by_token('123');
		return $ret;
	}

	/** 判断应用是否推送操作指引 */
	public function test_is_send_guide() {
		$ret = $this -> instance -> is_send_guide(1, 3);
		return $ret;
	}
	
}

com_app_test_main();

function com_app_test_main() {
	session_start();
	$instance = new ComAppTest();
	$debug = FALSE;
	
	echo 'test_get_sie_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_sie_id());
	
	echo 'test_list_by_com_id-------------------------------------------------------------------------------<br>';
	$debug && var_dump($instance -> test_list_by_com_id());
	
	echo 'test_get_by_app_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_app_id());
	
	echo 'test_list_all_auth_app-------------------------------------------------------------------------------<br>';
	$debug && var_dump($instance -> test_list_all_auth_app());
	
	echo 'test_get_by_token-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_token());
	
	echo 'test_is_send_guide-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_is_send_guide());
	
}

//end