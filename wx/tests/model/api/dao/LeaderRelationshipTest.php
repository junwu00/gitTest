<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class LeaderRelationshipTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_leader');
		$conf = array(
			'com_id'	=> 1,
			'root_id'	=> 1,
			'user_id'	=> 80225	
		);
		$this -> instance -> set_conf($conf);
	}
	
	/** 获取指定部门的所有领导 */
	public function test_list_leader_by_dept_id() {
//		$ret = $this -> instance -> list_leader_by_dept_id(80139, TRUE);
		$ret = $this -> instance -> list_leader_by_dept_id(80139, FALSE);
		return $ret;
	}
	
	/** 获取指定成员的所有领导（只取主部门上级+成员上级） */
	public function test_list_leader_by_user_id() {
		$ret = $this -> instance -> list_leader_by_user_id(80225, TRUE);
//		$ret = $this -> instance -> list_leader_by_user_id(80225, FALSE);
		return $ret;
	}
	
	/** 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用） */
	public function test_list_sub_by_user_id() {
//		$ret = $this -> instance -> list_sub_by_user_id(80225, TRUE);
		$ret = $this -> instance -> list_sub_by_user_id(80225, FALSE);
		return $ret;
	}
	
}

leader_test_main();

function leader_test_main() {
	session_start();
	$instance = new LeaderRelationshipTest();
	
	echo 'test_list_leader_by_dept_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_leader_by_dept_id());
	
	echo 'test_list_leader_by_user_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_list_leader_by_user_id());
	
	echo 'test_list_sub_by_user_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_list_sub_by_user_id());

}

//end