<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class AppTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_app');
		$conf = array(
			'com_id'	=> 1,
			'root_id'	=> 1,
			'user_id'	=> 80225	
		);
		$this -> instance -> set_conf($conf);
	}
	
	/** 根据ID获取应用*/
	public function test_get_by_id() {
		$ret = $this -> instance -> get_by_id(1);
		return $ret;
	}
	
	/** 根据应用名称获取应用信息 */
	public function test_get_by_name() {
		$ret = $this -> instance -> get_by_name('contact');
		return $ret;
	}
	
	/** 获取企业指定应用列表 */
	public function test_list_by_ids() {
		$ret = $this -> instance -> list_by_ids(array(2,3));
		return $ret;
	}
	
}

app_test_main();

function app_test_main() {
	session_start();
	$instance = new AppTest();
	$debug = FALSE;
	
	echo 'test_get_by_id-------------------------------------------------------------------------------<br>';
	$debug && var_dump($instance -> test_get_by_id());
	
	echo 'test_get_by_name-------------------------------------------------------------------------------<br>';
	$debug && var_dump($instance -> test_get_by_name());
	
	echo 'test_list_by_ids-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_list_by_ids());

}

//end