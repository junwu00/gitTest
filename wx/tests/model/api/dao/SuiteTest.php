<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class SuiteTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_suite');
	}
	
	/** 通过suite_id获取套件信息 */
	public function test_get_by_suite_id() {
		$ret = $this -> instance -> get_by_suite_id('tj2d6bd55c99b4a002');
		return $ret;
	}
	
	/** 通过id获取套件信息 */
	public function test_get_by_id() {
		$ret = $this -> instance -> get_by_id(79012);
		return $ret;
	}
	
	/** 通过token获取suite信息 */
	public function test_get_by_token() {
		$ret = $this -> instance -> get_by_token('tAWKSehI5URNaPGaemXPECB55RI');
		return $ret;
	}
	
}

suite_test_main();

function suite_test_main() {
	session_start();
	$instance = new SuiteTest();
	$debug = FALSE;
	
	echo 'test_get_by_suite_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_suite_id());
	
	echo 'test_get_by_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_id());
	
	echo 'test_get_by_token-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_token());

}

//end