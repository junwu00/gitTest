<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class DeptTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_dept');
		$conf = array(
			'com_id'	=> 1,
			'root_id'	=> 1,
			'user_id'	=> 80225	
		);
		$this -> instance -> set_conf($conf);
	}
	
	/** 查询单个部门 */
	public function test_get_by_id() {
		$ret = $this -> instance -> get_by_id(1);
		return $ret;
	}
	
	/** 批量查询部门 */
	public function test_list_by_ids() {
		$ret = $this -> instance -> list_by_ids(array(1, 2));
		return $ret;
	}
	
	/** 获取部门及其子..部门ID字符串列表（不做过滤） */
	public function test_list_all_child_ids() {
		$ret = $this -> instance -> list_all_child_ids(TRUE, 'array');
		return $ret;
	}
	
	/** 获取指定层级的部门列表 */
	public function test_list_by_lev() {
		$ret = $this -> instance -> list_by_lev(1);
		return $ret;
	}
	
	/** 获取指定部门的下级部门 */
	public function test_list_direct_child() {
		$ret = $this -> instance -> list_direct_child(1);
		return $ret;
	}
	
	/** 获取指定部门及其的子..部门（支持部门树） */
	public function test_list_all_child() {
//		$ret = $this -> instance -> list_all_child(80744, TRUE);
		$ret = $this -> instance -> list_all_child(80744, FALSE);
		return $ret;
	}
	
	/** 获取指定部门及其的子..部门（支持部门树） */
	public function test_list_all_parent_ids() {
		$ret = $this -> instance -> list_all_parent_ids(array(80744));
		return $ret;
	}
	
	/** 判断指定部门是否存在子部门 */
	public function test_list_has_child_by_ids() {
		$ret = $this -> instance -> list_has_child_by_ids(array(80744));
		return $ret;
	}
	
	/** 获取部门领导列表 */
	public function test_list_leader() {
		$ret = $this -> instance -> list_leader(1);
		return $ret;
	}
	
}

dept_test_main();

function dept_test_main() {
	session_start();
	$instance = new DeptTest();
	
	echo 'test_get_by_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_get_by_id());

	echo 'test_list_by_ids-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_by_ids());

	echo 'test_list_all_child_ids-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_all_child_ids());

	echo 'test_list_by_lev-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_by_lev());

	echo 'test_list_direct_child-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_direct_child());

	echo 'test_list_all_child-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_all_child());

	echo 'test_list_all_parent_ids-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_list_all_parent_ids());

	echo 'test_list_has_child_by_ids-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_has_child_by_ids());

	echo 'test_list_leader-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_leader());
}

//end