<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class AgencyInfoTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_agency_info');
	}
	
	/** 获取技术支持信息*/
	public function test_get_support_info() {
		$ret = $this -> instance -> get_support_info(1);
		return $ret;
	}
	
	/** 根据ID获取代理商信息 */
	public function test_get_by_id() {
		$ret = $this -> instance -> get_by_id(1);
		return $ret;
	}
	
}

agency_info_test_main();

function agency_info_test_main() {
	session_start();
	$instance = new AgencyInfoTest();
	$debug = FALSE;
	
	echo 'test_get_support_info-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_support_info());
	
	echo 'test_get_by_id-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_get_by_id());
	

}

//end