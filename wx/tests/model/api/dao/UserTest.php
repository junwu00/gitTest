<?php
namespace tests\model\api\dao;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件

class UserTest /*extends \PHPUnit_Framework_TestCase*/ {
	
	private $instance;
	
	public function __construct() {
		$this -> instance = g('dao_user');
		$conf = array(
			'com_id'	=> 1,
			'root_id'	=> 1,
			'user_id'	=> 80225	
		);
		$this -> instance -> set_conf($conf);
	}
	
	/** 据成员账号获取成员信息 */
	public function test_get_by_acct() {
		$ret = $this -> instance -> get_by_acct('002');
		return $ret;
	}
	
	/** 根据成员ID获取成员信息 */
	public function test_get_by_id() {
		$ret = $this -> instance -> get_by_id(80225);
		return $ret;
	}
	
	/** 获取成员主部门信息/仅ID */
	public function test_get_main_dept() {
//		$ret = $this -> instance -> get_main_dept(80225, TRUE);
		$ret = $this -> instance -> get_main_dept(80225, FALSE);
		return $ret;
	}
	
	/** 根据ID集合获取成员列表 */
	public function test_list_by_ids() {
//		$ret = $this -> instance -> list_by_ids(array(80225, 80139), '*', FALSE);
		$ret = $this -> instance -> list_by_ids(array(80225, 80139), '*', TRUE);
		return $ret;
	}
	
	/** 获取成员所有部门信息/仅ID */
	public function test_list_all_dept() {
		$ret = $this -> instance -> list_all_dept(80225, FALSE);
//		$ret = $this -> instance -> list_all_dept(80225, TRUE);
		return $ret;
	}
	
	/** 根据ID获取成员的部门及其父..级部门 */
	public function test_list_all_parent_dept() {
//		$ret = $this -> instance -> list_all_parent_dept(80225, FALSE);
		$ret = $this -> instance -> list_all_parent_dept(80225, TRUE);
		return $ret;
	}
	
	/** 根据部门ID，获取部门直属成员列表 */
	public function test_list_direct_by_dept_id() {
		$ret = $this -> instance -> list_direct_by_dept_id(80139);
		return $ret;
	}
	
	/** 分页获取成员信息(含子部门成员) */
	public function test_page_list_by_dept_ids() {
		$ret = $this -> instance -> page_list_by_dept_ids(array(80139));
		return $ret;
	}
	
	/** 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用） */
	public function test_list_sub_by_id() {
		$ret = $this -> instance -> list_sub_by_id(80225);
		return $ret;
	}
	
	/** 获取指定成员的所有领导 */
	public function test_list_leader_by_id() {
		$ret = $this -> instance -> list_leader_by_id(80225);
		return $ret;
	}

}

user_test_main();

function user_test_main() {
	session_start();
	$instance = new UserTest();
	
	echo 'test_get_by_acct-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_get_by_acct());
	
	echo 'test_get_by_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_get_by_id());
	
	echo 'test_get_main_dept-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_get_main_dept());
	
	echo 'test_list_by_ids-------------------------------------------------------------------------------<br>';
	var_dump($instance -> test_list_by_ids());
	
	echo 'test_list_all_dept-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_all_dept());
	
	echo 'test_list_all_parent_dept-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_all_parent_dept());
	
	echo 'test_list_direct_by_dept_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_direct_by_dept_id());
	
	echo 'test_page_list_by_dept_ids-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_page_list_by_dept_ids());
	
	echo 'test_list_sub_by_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_sub_by_id());
	
	echo 'test_list_leader_by_id-------------------------------------------------------------------------------<br>';
//	var_dump($instance -> test_list_leader_by_id());

}

//end