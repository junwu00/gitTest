<?php
/**
 * /package/cache/Redis 测试类
 *
 * @author JianMing Liang
 * @create 2016-01-08 18:40:29
 */
namespace tests\package\cache;

// 加载自动加载文件
require_once(dirname(dirname(dirname(__DIR__))) . '/init.php');
g_map('\\package\\cache\\Redis', 'redis');
g_map('\\package\\cache\\RedisSession', 'rs');

class RedisSessionTest extends \PHPUnit_Framework_TestCase {
    public function test_default_conf() {
        $this -> assertFalse(g('rs') -> max_life_time === 0);
        $this -> assertFalse(g('rs') -> key_preffix === '');
        $this -> assertFalse(g('rs') -> redis === null);
    }

    public function test_session() {
        g('rs') -> start();

        $id = session_id();
        $this -> assertTrue(strlen($id) === 40);
        
        $redis_key = g('rs') -> get_cache_key($id);

        $test = 'abcde';
        $_SESSION['test'] = $test;
        // session_commit();

        // $redis_val = g('redis') -> get($redis_key);
        // $redis_val = unserialize($redis_val);
        // $this -> assertTrue($redis_val['test'] === $test);

        session_destroy();
        $check = g('redis') -> exists($redis_key);

        g('rs') -> start();
        $id_2 = session_id();
        
        $this -> assertFalse($id === $id_2);
        $this -> assertFalse($check);
    }
}

/* End of this file */