<?php
/**
 * /package/cache/Redis 测试类
 *
 * @author JianMing Liang
 * @create 2016-01-08 18:40:29
 */
namespace tests\package\cache;

// 加载自动加载文件
require_once(dirname(dirname(dirname(__DIR__))) . '/init.php');
g_map('\\package\\cache\\Redis', 'redis');

class RedisTest extends \PHPUnit_Framework_TestCase {
    public function test_get_handler() {
        g('redis') -> reconnect();
        $han_1 = g('redis') -> get_handler();

        $this -> assertTrue(is_object($han_1));
    }

    public function test_reconect() {
        g('redis') -> reconnect();
        $han_1 = g('redis') -> get_handler();
        g('redis') -> reconnect();
        $han_2 = g('redis') -> get_handler();

        g('redis') -> reconnect(true);
        $han_3 = g('redis') -> get_handler();

        $this -> assertTrue($han_1 === $han_2);
        $this -> assertTrue($han_1 !== $han_3);
    }


    public function test_set() {
        $key = md5(__FUNCTION__ . ':' . time());

        $result = g('redis') -> set($key, 1);
        $this -> assertTrue($result);

        return $key;
    }

    /**
     * @depends test_set
     */
    public function test_exists($key) {
        $result = g('redis') -> exists($key);
        $this -> assertTrue($result);
    }

    /**
     * @depends test_set
     */
    public function test_get($key) {
        $result = g('redis') -> get($key);
        $this -> assertTrue($result !== false);
    }

    /**
     * @depends test_set
     */
    public function test_delete($key) {
        $result = g('redis') -> delete($key);
        $check = g('redis') -> exists($key);

        $this -> assertTrue($result);
        $this -> assertFalse($check);
    }

    public function test_clear() {
        $key = md5(__FUNCTION__ . ':' . time());
        g('redis') -> set('abc', 1);
        
        $result = g('redis') -> clear();
        $check = g('redis') -> exists($key);

        $this -> assertTrue($result);
        $this -> assertFalse($check);
    }

    public function test_push() {
        $key = md5(__FUNCTION__ . ':' . time());

        $res_1 = g('redis') -> push($key, 'left', false);
        $res_2 = g('redis') -> push($key, 'right', true);
        
        $check = g('redis') -> exists($key);

        $this -> assertTrue($res_1);
        $this -> assertTrue($res_2);

        $this -> assertTrue($check);

        return $key;
    }

    /**
     * @depends test_push
     */
    public function test_pop($key) {
        $res_1 = g('redis') -> push($key, 'left_top', false);

        $left = g('redis') -> pop($key, true);
        $right = g('redis') -> pop($key, false);
        g('redis') -> delete($key);

        $this -> assertTrue($res_1);
        $this -> assertTrue($left === 'left_top');
        $this -> assertTrue($right === 'right');
    }

    public function test_increase() {
        $key = md5(__FUNCTION__ . ':' . time());

        $res_1 = g('redis') -> increase($key);
        $val_1 = g('redis') -> get($key);

        $res_2 = g('redis') -> increase($key);
        $val_2 = g('redis') -> get($key);

        $this -> assertTrue($val_1 === '1');
        $this -> assertTrue($val_2 === '2');
        $this -> assertTrue($res_1);
        $this -> assertTrue($res_2);

        return $key;
    }

    /**
     * @depends test_increase
     */
    public function test_decrease($key) {
        $res = g('redis') -> decrease($key);
        $val = g('redis') -> get($key);
        g('redis') -> delete($key);

        $this -> assertTrue($res);
        $this -> assertTrue($val === '1');
    }
}

/* End of this file */