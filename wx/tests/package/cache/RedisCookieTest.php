<?php
/**
 * /package/cache/Redis 测试类
 *
 * @author JianMing Liang
 * @create 2016-01-08 18:40:29
 */
namespace tests\package\cache;

// 加载自动加载文件
require_once(dirname(dirname(dirname(__DIR__))) . '/init.php');
g_map('\\package\\cache\\Redis', 'redis');
g_map('\\package\\cache\\RedisCookie', 'rc');

class RedisCookieTest extends \PHPUnit_Framework_TestCase {
    public function test_default_conf() {
        $this -> assertFalse(g('rc') -> max_life_time === 0);
        $this -> assertFalse(g('rc') -> key_preffix === '');
        $this -> assertFalse(g('rc') -> redis === null);

        $this -> assertFalse(g('rc') -> id_client_var === '');
        $this -> assertFalse(g('rc') -> id_client_host === '');
        $this -> assertFalse(g('rc') -> id_valid_time === 0);
    }

    public function test_create_id() {
        $id = g('rc') -> get_now_id();

        g('rc') -> destory();
        $id_2 = g('rc') -> get_now_id();

        $this -> assertTrue(strlen($id) === 40);
        $this -> assertTrue($id !== $id_2);
    }

    public function test_data() {
        g('rc') -> start();

        $name = 'aa';
        $value = '132';
        $res = g('rc') -> set($name, $value);
        $res_2 = g('rc') -> get($name);

        g('rc') -> set($name, $value, -1);
        $res_3 = g('rc') -> get($name);

        $this -> assertTrue($res);
        $this -> assertTrue($res_2 === $value);
        $this -> assertFalse($res_3);
    }
}

/* End of this file */