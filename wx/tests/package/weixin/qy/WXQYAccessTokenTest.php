<?php
/**
 * /package/weixin/WXQYAccessToken 测试类
 *
 * @author yangpz
 * @create 2016-02-15
 */
namespace tests\package\weixin\qy;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
g_map('\\package\\weixin\\qy\\WXQYAccessToken', 'pkg_wxqy_atoken');

class WXQYAccessTokenTest extends \PHPUnit_Framework_TestCase {
	private $corp_id = 'wxb5d1ccf6cf178ceb';
	private $secret = '812i86tjoXTmDa5YIi2XYy1E1f3SDHblMsNH5mDzzKAhOnIYv4rQIpHtR6fyZ1ZA';
	private $debug = FALSE;
	
    public function test_update_access_token() {
    	$token = g('pkg_wxqy_atoken') -> update_access_token($this -> corp_id, $this -> secret);
    	$is_success = !empty($token['token']);
    	
        $this -> assertTrue($is_success, $token['token']);
    }

}

/* End of this file */