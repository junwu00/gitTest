<?php
/**
 * /package/weixin/WXQYUser 测试类
 *
 * @author yangpz
 * @create 2016-02-15
 */
namespace tests\package\weixin\qy;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
g_map('\\package\\weixin\\qy\\WXQYAccessToken', 'pkg_wxqy_atoken');
g_map('\\package\\weixin\\qy\\WXQYMenu', 'pkg_wxqy_menu');

class WXQYMenuTest extends \PHPUnit_Framework_TestCase {
	private $corp_id = 'wxb5d1ccf6cf178ceb';
	private $secret = '812i86tjoXTmDa5YIi2XYy1E1f3SDHblMsNH5mDzzKAhOnIYv4rQIpHtR6fyZ1ZA';
	private $token = '';
	
	private $instance = NULL;
	
	public function __construct() {
    	$token = g('pkg_wxqy_atoken') -> update_access_token($this -> corp_id, $this -> secret);
    	$this -> token = $token['token'];
    	$this -> instance = g('pkg_wxqy_menu', array('[com:1][admin:1]'));
		parent::__construct();
	}
	
	/** 开/关当前要测试的用例 */
	public function test_main() {
		$this -> _test_create_menu();
		$this -> _test_get_menu();
		$this -> _test_delete_menu();
	}
	
	/** 创建菜单 */
	private function _test_create_menu() {
		$agent_id = 27;
		$menu_data = array(
			array('name' => '最新活动', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=new_activity'),
			array('name' => '所有活动', 'sub_button' => array(
					array('name' => '活动列表', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=all'),
					array('name' => '活动相册', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=all_album'),
				),
			),
			array('name' => '我的活动', 'sub_button' => array(
					array('name' => '发起活动', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=add'),
					array('name' => '我参与的', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=action'),
					array('name' => '我发起的', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=publiced'),
					array('name' => '我的相册', 'type' => 'view', 'url' => 'http://easywork365.com/index.php?app=activity&m=show&a=publiced_album'),
				),
			),
		);
		$menu = $this -> instance -> create_menu($this -> token, $agent_id, $menu_data);
        $this -> assertTrue(!empty($menu), $menu);
	}	

	/** 获取菜单 */
	private function _test_get_menu() {
		$agent_id = 27;
		$menu = $this -> instance -> get_menu($this -> token, $agent_id);
        $this -> assertTrue(!empty($menu), $menu);
	}
	
	/** 删除菜单 */
	private function _test_delete_menu() {
		$agent_id = 27;
		$menu = $this -> instance -> delete_menu($this -> token, $agent_id);
        $this -> assertTrue(!empty($menu), $menu);
	}
	
}

/* End of this file */