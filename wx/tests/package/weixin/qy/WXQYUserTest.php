<?php
/**
 * /package/weixin/WXQYUser 测试类
 *
 * @author yangpz
 * @create 2016-02-15
 */
namespace tests\package\weixin\qy;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
g_map('\\package\\weixin\\qy\\WXQYAccessToken', 'pkg_wxqy_atoken');
g_map('\\package\\weixin\\qy\\WXQYUser', 'pkg_wxqy_user');

class WXQYUserTest extends \PHPUnit_Framework_TestCase {
	private $corp_id = 'wxb5d1ccf6cf178ceb';
	private $secret = '812i86tjoXTmDa5YIi2XYy1E1f3SDHblMsNH5mDzzKAhOnIYv4rQIpHtR6fyZ1ZA';
	private $token = '';
	
	private $instance = NULL;
	private $add_user_id = array();
	
	public function __construct() {
    	$token = g('pkg_wxqy_atoken') -> update_access_token($this -> corp_id, $this -> secret);
    	$this -> token = $token['token'];
    	$this -> instance = g('pkg_wxqy_user', array('[com:1][admin:1]'));
		parent::__construct();
	}
	
	/** 开/关当前要测试的用例 */
	public function test_main() {
//		$this -> _test_list_user_message_by_dept();
//		$this -> _test_list_user_by_dept();
//		$this -> _test_get_user();
		$this -> _test_create_user();
		$this -> _test_update_user();
		$this -> _test_delete_user();
		
		$this -> _test_create_user();
		$this -> _test_batch_delete_user();
	}
	
	/** 获取部门成员列表[详细] */
    private function _test_list_user_message_by_dept() {
    	//含子部门(所有状态)
    	$user_list = $this -> instance -> list_user_message_by_dept($this -> token, 1, 1, 0);
        $this -> assertTrue(!empty($user_list), $user_list);
        
    	//含子部门(指定状态)
    	$user_list = $this -> instance -> list_user_message_by_dept($this -> token, 1, 1, 1);
        $this -> assertTrue(!empty($user_list), $user_list);
        
        //不含子部门(所有状态)
    	$user_list = $this -> instance -> list_user_message_by_dept($this -> token, 1, 0, 0);
        $this -> assertTrue(!empty($user_list), $user_list);
    	
        //不含子部门(指定状态)
    	$user_list = $this -> instance -> list_user_message_by_dept($this -> token, 1, 0, 1);
        $this -> assertTrue(!empty($user_list), $user_list);
    }
    
	/** 获取部门成员列表 */
    private function _test_list_user_by_dept() {
    	//含子部门(所有状态)
    	$user_list = $this -> instance -> list_user_by_dept($this -> token, 1, 1, 0);
        $this -> assertTrue(!empty($user_list), $user_list);
        
    	//含子部门(指定状态)
    	$user_list = $this -> instance -> list_user_by_dept($this -> token, 1, 1, 1);
        $this -> assertTrue(!empty($user_list), $user_list);
        
        //不含子部门(所有状态)
    	$user_list = $this -> instance -> list_user_by_dept($this -> token, 1, 0, 0);
        $this -> assertTrue(!empty($user_list), $user_list);
        
        //不含子部门(指定状态)
    	$user_list = $this -> instance -> list_user_by_dept($this -> token, 1, 0, 1);
        $this -> assertTrue(!empty($user_list), $user_list);
    }
    
    /** 获取单个成员详细信息 */
    private function _test_get_user() {
    	$user = $this -> instance -> get_user($this -> token, '00006');
        $this -> assertTrue(!empty($user), $user);
    }
    
    /** 创建成员 */
    private function _test_create_user() {
    	$id = $name = $position = $weixin_id = 'test_'.time().get_rand_str(4);
    	$department = array(1);
    	$mobile = $tel = $email = '';
    	$gender = 1;
    	$ext_attr = array();
    	
	    $user = $this -> instance -> create_user($this -> token, $id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr);
	    array_push($this -> add_user_id, $user);
        $this -> assertTrue(!empty($user), $user);
    }
    
    /** 更新成员 */
    private function _test_update_user() {
    	$id = $name = $position = $weixin_id = 'test_u_'.time();
    	$department = array(1);
    	$mobile = $tel = $email = '';
    	$gender = 1;
    	$ext_attr = array();
    	$enable = 1;
    	
    	$id = $this -> add_user_id[0];
    	$user = $this -> instance -> update_user($this -> token, $id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr, $enable);
        $this -> assertTrue(!empty($user), $user);
    }
    
    /** 删除成员 */
    private function _test_delete_user() {
    	$id = array_shift($this -> add_user_id);
    	$user = $this -> instance -> delete_user($this -> token, $id);
        $this -> assertTrue(!empty($user), $user);
    }
    
    /** 批量删除成员 */
    private function _test_batch_delete_user() {
    	$user = $this -> instance -> batch_delete_user($this -> token, $this -> add_user_id);
        $this -> assertTrue(!empty($user), $user);
    }
    
}

/* End of this file */