<?php
/**
 * /package/weixin/WXQYDept 测试类
 *
 * @author yangpz
 * @create 2016-02-15
 */
namespace tests\package\weixin\qy;

// 加载自动加载文件
require_once(dirname(dirname(dirname(dirname(__DIR__)))) . '/init.php');
g_map('\\package\\weixin\\qy\\WXQYAccessToken', 'pkg_wxqy_atoken');
g_map('\\package\\weixin\\qy\\WXQYDept', 'pkg_wxqy_dept');

class WXQYDeptTest extends \PHPUnit_Framework_TestCase {
	private $corp_id = 'wxb5d1ccf6cf178ceb';
	private $secret = '812i86tjoXTmDa5YIi2XYy1E1f3SDHblMsNH5mDzzKAhOnIYv4rQIpHtR6fyZ1ZA';
	private $token = '';
	
	private $instance = NULL;
	private $add_dept_id = -1;
	
	public function __construct() {
    	$token = g('pkg_wxqy_atoken') -> update_access_token($this -> corp_id, $this -> secret);
    	$this -> token = $token['token'];
    	$this -> instance = g('pkg_wxqy_dept', array('[com:1][admin:1]'));
		parent::__construct();
	}
	
	/** 开/关当前要测试的用例 */
	public function test_main() {
		$this -> _test_create_dept();
		$this -> _test_update_dept();
		$this -> _test_delete_dept();
		$this -> _test_list_dept();
	}
	
	/** 获取企业所有部门列表 */
    private function _test_list_dept() {
    	$dept_list = $this -> instance -> list_dept($this -> token);
    	
    	$is_success = !empty($dept_list);
        $this -> assertTrue($is_success, $dept_list);
    }
    
    /** 创建部门 */
    private function _test_create_dept() {
    	$dept = $this -> instance -> create_dept($this -> token, '新框架部门_'.time(), 1, 1);
    	$this -> add_dept_id = $dept;
    	
    	$is_success = !empty($dept);
        $this -> assertTrue($is_success, $dept);
    }
    
    /** 更新部门 */
    private function _test_update_dept() {
    	$dept = $this -> instance -> update_dept($this -> token, $this -> add_dept_id, '新框架部门_u_'.time(), 20372, 1);
    	
    	$is_success = !empty($dept);
        $this -> assertTrue($is_success, $dept);
    }
    
    /** 删除部门 */
    private function _test_delete_dept() {
    	$dept = $this -> instance -> delete_dept($this -> token, $this -> add_dept_id);
    	
    	$is_success = !empty($dept);
        $this -> assertTrue($is_success, $dept);
    }
    
}

/* End of this file */