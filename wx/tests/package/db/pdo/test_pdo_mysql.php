<?php
/**
 * /package/pdo/Publish.class.php类(mysql驱动)的测试实现
 *
 * @author LiangJianMing
 * @create 2016-01-08 18:40:29
 */

// 成功
$exit = 0;

$map = array(
    '\package\db\pdo\mysql\Base' => 'base',
    '\package\db\pdo\Publish' => 'p',
);
g_maps($map);

$start_time = time();
w("开始测试类文件 /package/pdo/Publish.class.php");

g('base') -> set_conf($GLOBALS['MAIN_DB_CONFIG']);
// g('base') -> reconnect(true);
$db = g('base') -> get_pdo();
$conf = g('base') -> get_conf();
$db1 = g('base') -> get_pdo();
$db2 = g('base') -> get_pdo(true);

if ($db !== $db1) {
    w("base -> reconnect函数复用无效");
    $exit = 0;
}

if ($db === $db2) {
    w("base -> reconnect函数强制重新实例化无效");
    $exit = 0;
}

// $result = g('p') -> query("show variables like 'character%'");

// if (empty($result)) {
//     w("publish -> query 异常");
//     $exit = 0;
// }
// g('p') -> change_charset('latin1');
// $result1 = g('p') -> query("show variables like 'character%'");
// if ($result1 === $result) {
//     w("publish -> change_charset 失败");
//     print_r($result1);
//     $exit = 0;
// }
// g('p') -> change_charset('utf8');

// $result = g('p') -> query("show tables");
// g('p') -> change_db('mysql');
// $result1 = g('p') -> query("show tables");
// if ($result1 === $result) {
//     w("publish -> change_db 失败");
//     print_r($result1[0]);
// }
// g('p') -> change_db('mysql');

$drop_sql = '';
$create_table_sql = <<<EOF
DROP TABLE IF EXISTS `advice_class`;
CREATE TABLE `advice_class` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `com_id` int(11) unsigned NOT NULL DEFAULT 0 COMMENT '企业id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `weight` smallint(6) unsigned NOT NULL DEFAULT 1 COMMENT '权重, 取值：1-99, 值越大优先级越高',
  PRIMARY KEY (`id`),
  KEY `idx_com_id` (`com_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='意见反馈分类表';
EOF;
$result = g('p') -> exec($create_table_sql);
if (!$result) {
    w("create_table_sql 失败");
}

g('p') -> begin_trans();
$table = 'advice_class';
$data = array(
    'com_id' => 1,
    'name' => 'insert_' . time(),
);
$result = g('p') -> insert($table, $data);
var_dump($result);
if (!$result) {
    w("insert 失败");
}
g('p') -> commit();

$fileds = array(
    'com_id', 'name',
);
$data = array(
    array(
        1,
        'batch_insert_' . time(),
    ),
    array(
        1,
        'batch_insert_' . time(),
    ),
    array(
        2,
        'batch_insert_' . time(),
    ),
    array(
        2,
        'batch_insert_' . time(),
    ),
);
$result = g('p') -> batch_insert($table, $fileds, $data, 1);
if (!$result) {
    w("batch_insert 失败");
}

$data = array(
    'com_id' => 2,
    'name' => 'update_' . time(),
);
$condition = array(
    'name LIKE' => '%batch_insert_%',
    '__OR_1' => array(
        'name LIKE' => '%batch_insert_%',
        array(
            '__OR_1' => array(
                'name LIKE' => '%batch_insert_%',
                '__1__name LIKE' => '%batch_insert_%',
                '__2__name LIKE' => '%batch_insert_%',
            ),
            'name LIKE' => '%batch_insert_%',
        ),
        '__1__name LIKE' => '%batch_insert_%',
        '__2__name LIKE' => '%batch_insert_%',
    ),
    'com_id=' => 1,
);
$result = g('p') -> update($table, $condition, $data);
if (!$result) {
    w("update 失败");
}

$condition = array(
    'name LIKE' => '%update_%',
);
$result = g('p') -> delete($table, $condition);
if (!$result) {
    w("delete 失败");
}

$fileds = array(
    'id', 'name', 'weight',
);
$condition = array(
    'name LIKE' => '%batch_insert_%',
);
$order_by = 'id DESC';
$result = g('p') -> select($table, $fileds, $condition, 1, 1, '', $order_by, true);
var_dump($result);

$result = g('p') -> select_one($table, $fileds, $condition);
var_dump($result);

$result = g('p') -> select_first_val($table, $fileds, $condition);
var_dump($result);

$result = g('p') -> check_exists($table, $condition);
var_dump($result);

end:
w('执行完成，消耗时间：' . (time() - $start_time) . "秒");
exit($exit);

function w($msg) {
    $str = sprintf("{$msg} \n");
    echo $str;
}

/* End of this file */