<?php
ini_set('session.cookie_secure',1);
include_once "package/AntiXSS/AntiXSS.php";

$xss = new AntiXSS();
// 需要过滤字段，防止被攻击
if($_GET){
    $filter_array = ["app","code", "state","m","corpurl","a","id","free","model","corpid"];

    foreach ($_GET as $k => &$v){
        in_array($k,$filter_array) && $v = $xss->xss_clean($v);
    }unset($k, $v);
}


if($_POST){
    $data_is_json = false;
    $data = isset($_POST['data'])?$_POST['data']:[];
    if(!is_array($data)){
        $data_is_json = true;
        $data = json_decode($data,true);
    }

    //指定参数，指定接口进行过滤
    $filter_array = ["acct","type_id", "code","password","type","cache_acct","page_size","offset"];
    $filter_api = ['process|ajax|index|121'];

    if($data && is_array($data) ){
        $model = isset($_GET['model'])?$_GET['model']:'index';
        $m = isset($_GET['m'])?$_GET['m']:'index';
        $a = isset($_GET['a'])?$_GET['a']:'index';
        $cmd = isset($_GET['cmd'])?$_GET['cmd']:'';

        foreach ($data as $k=> &$v){
            //in_array($k,$filter_array) && $v = $xss->xss_clean($v);

            //判断请求的接口地址是否为需要过滤的地址
            //in_array("$model|$m|$a|$cmd",$filter_api) && $v = $xss->xss_clean($v);
            $v = $xss->xss_clean($v);

        }unset($v);

        if($data_is_json){
            $_POST['data'] = json_encode($data);
        }else{
            $_POST['data'] = $data;
        }
    }
}


/**
 * 主框架入口文件
 *
 * @author LiangJianMing
 * @create 2015-12-15
 */
$g_use_new_app = FALSE;
$debug_var = 1;
$complete_app = array('legwork', 'index', 'process','demo','workshift', 'finstat');
isset($_GET['app']) && in_array($_GET['app'], $complete_app) && $g_use_new_app = TRUE;

//默认使用新框架中应用（包括未开发完成）
!isset($g_use_new_app) && $g_use_new_app = TRUE;

// 转发到默认的模块
$model_var = 'model';
$model = isset($_GET[$model_var]) ? strval($_GET[$model_var]) : '';
$app_var = 'app';
$g_app = isset($_GET[$app_var]) ? strval($_GET[$app_var]) : '';
empty($model) && $model = 'default';
$model_dir = __DIR__ . '/model/';

if($g_app == 'process'){
	$key = array(
			'mv_open' => array(
					'notify_detail' => array(
							'm' => 'index',
							'a' => 'detail',
							'change' => array(
									'notify_id' => 'id',
									'corpurl' => 'corpurl'
								),
							'add' => array(
									'activekey' => 0,
									'listType' => 3,
								)
						),
					'detail' => array(
							'm' => 'index',
							'a' => 'detail',
							'change' => array(
									'workitem_id' => 'id',
									'corpurl' => 'corpurl'
								),
							'add' => array(
									'listType' => 1,
									'activekey' => 0,
								),
						),
					'mine_detail' => array(
							'm' => 'index',
							'a' => 'detail',
							'change' => array(
									'formsetinit_id' => 'id',
									'corpurl' => 'corpurl'
								),
							'add' => array(
									'listType' => 2,
									'activekey' => 0,
								)
						)
				),
		);

	$m = isset($_GET['m']) ? $_GET['m'] : 'index';
	$a = isset($_GET['a']) ? $_GET['a'] : 'index';

	$k = isset($key[$m][$a]) ? $key[$m][$a] : false;

	if($k){
		$change = '';
		foreach ($k['change'] as $key => $val) {
			$v = $_GET[$key];
			$change .= "&{$val}={$v}";
		}unset($val);
		foreach ($k['add'] as $key => $val) {
			$change .= "&{$key}={$val}";
		}
		$new_url =  '/index.php?app=process&m='.$k['m'].'&a='.$k['a'].$change;
		header('Location:'.$new_url);		
		exit();
	}
}

//应用改造完成
if ($g_app !== '' && is_dir($model_dir . $g_app) && $g_use_new_app) {
	$model = $g_app;
}

if ($model !== 'default') {
    // 使用新的入口
    include_once('init.php');
    $model_dir = MAIN_MODEL;
}

$page_name = isset($_GET['page_name']) ? strval($_GET['page_name']) : '';
$page_name = ltrim($page_name, '/');
empty($page_name) and $page_name = 'index.php';
$file_path = $model_dir . "{$model}/{$page_name}";

if (!file_exists($file_path)) {
    die('模型入口不存在');
}
include($file_path);

/* End of this file */
