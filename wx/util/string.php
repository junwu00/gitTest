<?php
/**
 * 字符串相关的函数库
 *
 * @author LiangJianMing
 * @create 2016-01-06 12:43:27
 */

/**
 * 生成随机字符串
 *
 * @global
 * @param integer $leng 需要生成的字符串长度
 * @param integer $type 字符串类型,取值：1-15，改值会先转换为2进制，如：14=1110，第一位表示使用特殊符号、第二位表示使用数字、第三位表示使用小写字母、第四位表示不使用大写字母
 * @param boolean $dark 是否去除字符集{O,o,0}
 * @return string
 */
function get_rand_string($leng, $type=7, $dark=FALSE){
    $tmp_array = array(
        '1' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        '2' => 'abcdefghijklmnopqrstuvwxyz',
        '4' => '0123456789',
        '8' => '~!@$&()_+-=,./<>?;\'\\:"|[]{}`'
    );
    $return = $target_string = '';
    $array = array();
    $bin_string = decbin($type);
    $bin_leng  = strlen($bin_string);
    for($i = 0; $i < $bin_leng; $i++) if($bin_string{$i} == 1) $array[] = pow(2, $bin_leng - $i - 1);
    if(in_array(1, $array, TRUE)) $target_string .= $tmp_array['1'];
    if(in_array(2, $array, TRUE)) $target_string .= $tmp_array['2'];
    if(in_array(4, $array, TRUE)) $target_string .= $tmp_array['4'];
    if(in_array(8, $array, TRUE)) $target_string .= $tmp_array['8'];
    $target_leng = strlen($target_string);
    mt_srand((double)microtime()*1000000);
    while(strlen($return) < $leng){
        $tmp_string = substr($target_string, mt_rand(0, $target_leng), 1);
        $dark && $tmp_string = (in_array($tmp_string, array('0', 'O', 'o'))) ? '' : $tmp_string;
        $return .= $tmp_string;
    }
    return $return;
}

/**
 * 获取唯一字符串
 *
 * @global
 * @param integer $mode 模式：1-简易、2-更高的唯一性
 * @return string
 */
function get_unique_str($mode=1) {
    if ($mode === 2) {
        $ret = uniqid(MAIN_IDENT);
        // 获取9位随机数
        $rand_str = get_rand_str(9);
        $ret = sha1($rand_str . $ret);
        return $ret;
    }

    $ret = uniqid();

    return $ret;
}

/**
 * 获取唯一标识码 唯一性高
 * @return 
 */
function unique_code(){
    $uniqid = __FUNCTION__ . uniqid() . time() . rand(100, 999);
    $unique_code = md5($uniqid);
    return $unique_code;
}

/** 
 * 生成随机字符串
 * 
 * @global
 * @param integer $len 需要生成的字符串长度
 * @param string $mode 二进制数值字符串, 取值：0001 - 1111，
 *                      第一位表示使用特殊符号、第二位表示使用数字、
 *                      第三位表示使用小写字母、第四位表示使用大写字母
 * @param boolean $is_filter 是否去除字符集{O,o,0}
 * @return string
 */
function get_rand_str($len, $mode='0111', $is_filter=false){
    $tmp_arr = array(
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        'abcdefghijklmnopqrstuvwxyz',
        '0123456789',
        '~!@$&()_+-=,./<>?;\'\\:"|[]{}`'
    );
    
    $ret = $src_str = '';
    $arr = array();

    $mode[0] === '1' and $src_str .= $tmp_arr[3];
    $mode[1] === '1' and $src_str .= $tmp_arr[2];
    $mode[2] === '1' and $src_str .= $tmp_arr[1];
    $mode[3] === '1' and $src_str .= $tmp_arr[0];

    $filter_arr = array('0', 'O', 'o');
    $is_filter and $src_str = str_replace($filter_arr, '', $src_str);
    
    $src_len = strlen($src_str);
    mt_srand((double)microtime() * 1000000);

    while (strlen($ret) < $len) {
        $tmp_char = $src_str[mt_rand(0, $src_len - 1)];
        $ret .= $tmp_char;
    }

    return $ret;
}

/** 
 * 检验数据合法性
 * 
 * @global
 * @param string $string 	需要检验的字符串
 * @param type $type 		检验类型
 * @return boolean
 */
function check_data($string, $type=''){
    $return = FALSE;
    
    switch($type){
        //昵称,只能以英文或中文开头,只能包含英文、中文、数字、下划线
        case 'sname'        :{ $return = preg_match("/^[\x{4e00}-\x{9fa5}A-Za-z]{1}[\x{4e00}-\x{9fa5}_A-Za-z0-9]*$/u", $string); BREAK; }
	    //普通账号格式
        case 'account'      :{ $return = preg_match("/^[A-Za-z0-9@_]{6,50}$/", $string); BREAK; }
    	//判断密码格式
        case 'password'     :{ $return = preg_match("/^[A-Za-z0-9~!@#$%^&\*\(\)_]+$/", $string); BREAK; }
        //是否为email
        case 'email'     	:{ $return = preg_match("/^(\w+[-+.]*\w+)*@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/", $string); BREAK; }
        //是否为url
        case 'http'     	:{ $return = preg_match("/^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/", $string); BREAK; }
        //是否为qq号码
        case 'qq'         	:{ $return = preg_match("/^[1-9]\d{4,11}$/", $string); BREAK; }
        //是否为邮政编码
        case 'post'     	:{ $return = preg_match("/^[1-9]\d{5}$/", $string); BREAK; }
        //是否为身份证号码
        case 'idnum'     	:{ $return = preg_match("/^\d{15}(\d{2}[A-Za-z0-9])?$/", $string); BREAK; }
        //是否为全中文
        case 'china'     	:{ $return = preg_match("/^[\x{4e00}-\x{9fa5}]+$/u", $string); BREAK; }
        //是否为全英文
        case 'english'     	:{ $return = preg_match("/^[A-Za-z]+$/", $string); BREAK; }
        //是否为手机号码
        case 'mobile'       :{ $return = preg_match("/^1[34578]\d{9}$/", $string); BREAK; }
        //是否为手机号码(仅验证类型长度)
        case 'mobile_s'     :{ $return = preg_match("/^\d{11}$/", $string); BREAK; }
        //是否为固话号码
        case 'phone'     	:{ $return = preg_match("/^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/", $string); BREAK; }
        //是否为年龄
        case 'age'         	:{ $return = (preg_match("/^(-{0,1}|\+{0,1})[0-9]+(\.{0,1}[0-9]+)$/", $string) && intval($string) <= 130 && intval($string) >= 12) ? TRUE : FALSE; BREAK; }
        //是否仅包含大小写英文字母及数字
        case 'eng_num'     	:{ $return = preg_match("/^[A-Za-z0-9]+$/", $string); BREAK; }
        //是否为datetime
        case 'datetime'    	:{ $return = preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}\s[\d]{1,2}:[\d]{1,2}:[\d]{1,2}$/', $string); BREAK; }
        //是否为date
        case 'date'     	:{ $return = preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $string); BREAK; }
        //是否为datetime中的time部分
        case 'time'     	:{ $return = preg_match('/^[\d]{1,2}:[\d]{1,2}:[\d]{1,2}$/', $string); BREAK; }
        //是否为ip地址
        case 'ip'         	:{ $return = preg_match("/^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/", $string); BREAK; }
        //是否包含中文
        case 'with_chinese'	:{ $return = preg_match('/[\x{4e00}-\x{9fa5}]+/u', $string); BREAK; }
        //是否为域名
        case 'domain' 		:{ $return = preg_match('/^[A-Za-z0-9][A-Za-z0-9-]+(\.[A-Za-z0-9-]+){1,3}$/', $string); BREAK; }
        //是否为中文域名
        case 'cndomain'		:{ $return = preg_match('/^([-a-zA-Z0-9\.]*[\x{4e00}-\x{9fa5}]*[-a-zA-Z0-9\.]*)+\.(中国|公司|网络|CN|COM|NET)$/iu', $string); BREAK; }
        //是否是ipv6地址
        case 'ipv6'			:{ $return = preg_match('/^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/', $string); BREAK;}
    }
    return $return ? TRUE : FALSE;
}

/**
 * 下划线类名转驼峰式类名
 * @param unknown_type $str
 */
function camelName($str) {
	$arr = explode('_', $str);
	$ret = '';
	foreach ($arr as $a) {
		$ret .= ucfirst($a);
	}unset($a);
	return $ret;
}

function displayIdcard($idcard,$show_leng=6){

    $idcard_len = strlen($idcard);
    if($idcard_len<$show_leng)
        return $idcard;

    $return = substr($idcard,0,$show_leng);
    while ($show_leng<$idcard_len){
        $return.='*';
        $show_leng++;
    }

    return $return;
}

/**
 * 判断字符串是否json格式数据
 *
 * @global
 * @param string $str 字符串
 * @return boolean
 */
function is_json($str) {

    if(is_array($str)){
        return false;
    }

    if (strpos($str, '[') !== 0 && strpos($str, '{') !== 0) return FALSE;
    if (is_null(json_decode($str))) return FALSE;
    return TRUE;
}

/* End of this file */