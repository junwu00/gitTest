<?php
/**
 * 日志记录库，必须先加载system.php
 *
 * @author LiangJianMing
 * @create 2016-01-06 10:33:46
 */

// 定义错误类日志级别
$error_lev = array(
    // 致命级别
    'MAIN_LOG_FATAL' => 'Fatal',
    // 错误级别
    'MAIN_LOG_ERROR' => 'Error',
    // 警告级别
    'MAIN_LOG_WARN' => 'Warn',
);

// 定义正常信息日志级别
$info_lev = array(
    // 突出通知级别
    'MAIN_LOG_NOTICE' => 'Notice',
    // 正常信息级别
    'MAIN_LOG_INFO' => 'Info',
    // 调试信息级别
    'MAIN_LOG_DEBUG' => 'Debug',
);

// 全部日志级别
$levels = $error_lev + $info_lev;
batch_defined($levels);

$GLOBALS['levels'] = $levels;
$GLOBALS['error_lev'] = $error_lev;
$GLOBALS['info_lev'] = $info_lev;

/** 
 * 默认日志记录器
 * 输出格式如：[写入时间][错误级别][自定义信息][自定义信息][自定义信息]...
 * 
 * @global
 * @param string $level 已定义的常量
 * @param string $file_path 日志文件路径
 * @param string $str 自定义内容
 * @param 可接受无穷个字符串变量
 * @return mixed 禁止写入日志返回FALSE，否则返回TRUE
 */
function to_log($level, $file_path='', $str) {
    if (empty($str)) {
        return false;
    }
    
    if(!defined('MAIN_USED_LOG')) {
        error_log("未定义的变量：MAIN_USED_LOG");
        return false;
    }

    if (!MAIN_USED_LOG) {
        return false;
    }

    $levels = $GLOBALS['levels'];
    $error_lev = $GLOBALS['error_lev'];
    $info_lev = $GLOBALS['info_lev'];

    if (!in_array($level, $levels)) {
        to_log(MAIN_LOG_ERROR, '', "日志级别\"{$level}\"不存在");
        return false;
    }

    // 如果指定的文件目录不存在，则使用默认的路径
    $dir_path = dirname($file_path);
    if (!is_dir($dir_path)) {
        // 默认是fcgi模式
        $sapi_str = '';
        PHP_SAPI != 'fpm-fcgi' and $sapi_str = '.' . PHP_SAPI;
        
        $type_str = '';
        in_array($level, $error_lev) and $type_str = '.error';

        $suffix = $sapi_str . $type_str . '.log';

        $file_path = MAIN_LOG_DIR . date('Y-m-d') . $suffix;
    }

    // 获取文件句柄
    $fp = fopen($file_path, "a");
    if ($fp === false) {
        error_log("打开日志句柄失败，文件路径：{$file_path}");
        return false;
    }

    $date = date('Y-m-d H:i:s');
    $content = sprintf("[%s] [%s] [%s]", $date, $level, $str);

    // 获取全部参数
    $param = func_get_args();
    unset($param[0]);
    unset($param[1]);
    unset($param[2]);

    foreach ($param as $val) {
        $tmp_str = strval($val);
        $content .= " [{$val}]";
    }
    $content .= "\r\n";

    fwrite($fp, $content);
    fclose($fp);
    return true;
}


function errorPage($msg=''){
    $errpage = <<<EOF
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="福清工会管理后台"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="format-detection" content="telephone=no">
    
		<title>错误</title>
		<style type="text/css">
			* {font-family: "Microsoft YaHei", "微软雅黑", Helvetica, "黑体",Arial,Tahoma; color: #333; font-weight: normal;}
			html, body {margin: 0; padding: 0;}
			#img-bar {width: 100%; text-align: center; background: #fff; background-size: 100% 100%; padding: 15px 0;}
			#img-bar img{
				width:60%;
			}
			h3 {width: 90%; margin: 0 auto; border-bottom: 1px solid #888; text-align: center; padding: 15px 0;}
			#main {width: 90%; margin: 0 auto; padding-top: 15px;text-align: center;}
			#main p {text-indent: 2em;}
		</style>
	</head>
	<body>
		<div id="img-bar"><img src="/static/image/err.png"></div>
		<div id="main"><p>{$msg}</p></div>
	</body>
</html>
EOF;
    die($errpage);
}

/* End of this file */