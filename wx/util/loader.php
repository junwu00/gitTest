<?php
/**
 * 加载器
 *
 * @author JianMing Liang
 * @create 2016-01-26 09:59:50
 */

/**
 * 使用include_once加载目录下的全部php文件
 *
 * @global
 * @param string $dir 目录绝对路径
 * @param array $exclude 排除这些文件（可以是绝对路径，也可以是文件名）
 * @param string $ext 指定文件扩展名
 * @return boolean
 */
function load_all_file($dir, array $exclude=array(), $ext='.php') {
    if (!is_dir($dir)) return false;

    $exclude[] = '.';
    $exclude[] = '..';

    $dh = opendir($dir);
    if ($dh === false) return false;

    while(($file = readdir($dh)) !== false) {
        if (in_array($file, $exclude)) {
            continue;
        }

        $tmp_path = rtrim($dir, '/') . "/{$file}";
        if (in_array($tmp_path, $exclude)) {
            continue;
        }

        if (is_dir($tmp_path)) {
            load_all_file($tmp_path, $exclude, $ext);
            continue;
        }

        preg_match("/{$ext}$/i", $file) and include_once($tmp_path);
    }

    closedir($dh);
    
    return true;
}

/* End of this file */