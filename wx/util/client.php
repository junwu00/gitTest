<?php
/**
 * 字符串相关的函数库
 *
 * @author LiangJianMing
 * @create 2016-01-06 12:43:27
 */

/**
     * 判断客户端平台类型
     *
     * @access public
     * @return integer 取值：0-其他、1-ios、2-android、3-wp
     */
function get_platid() {
    $agent = $_SERVER['HTTP_USER_AGENT'];

    if (preg_match('/(iPod|iPad|iPhone)/i', $agent)) {
        return 1;
    }

    if (preg_match('/android/i', $agent)) {
        return 2;
    }

    if (preg_match('/WP/', $agent)) {
        return 3;
    }

    if (preg_match('/Windows/', $agent)) {
        return 4;
    }

    if (preg_match('/Mac/', $agent)) {
        return 5;
    }

    return 0;
}

function is_wxwork(){
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? trim($_SERVER['HTTP_USER_AGENT']) : '';
    if ( strpos($user_agent , 'wxwork') !== false ) {
        return true;
    }
    return false;
}


/* End of this file */