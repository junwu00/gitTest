<?php
/**
 * 字符串相关的函数库
 *
 * @author LiangJianMing
 * @create 2016-01-06 12:43:27
 */

/**
 * 获取唯一字符串
 *
 * @global
 * @param integer $mode 模式：1-简易、2-更高的唯一性
 * @return string
 */

function get_this_url() {
    return MAIN_SCHEME . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

/**
 * 修改 URL 参数
 * @param string $url
 * @param string $name
 * @param string| $value
 * @return array|string|string[]|null
 */
function changeURLParam($url, $name, $value)
{
    $reg = "/([\?|&]" . $name . "=)[^&]*/i";
    $value = urlencode(trim($value));
    if (empty($value)) {
        return preg_replace($reg, '', $url);  //正则替换
    } else {
        if (preg_match($reg, $url)) {
            return preg_replace($reg, '${1}${2}' . $value, $url);
        } else {
            return $url . (strpos($url, '?') === false ? '?' : '&') . $name . '=' . $value;
        }
    }
}

/* End of this file */