<?php
/**
 * 数组相关的函数库
 *
 * @author yangpz
 * @create 2016-02-15
 */

/**
 * 对数组中所有中文进行urlencode
 * @param unknown_type $input
 */
function urlencode_array($input) {
    if (!is_array($input))
        return urlencode($input);
 
    return array_map('urlencode_array', $input);
}

/**
 * 对数组中所有值强制转换成int
 * @param unknown_type $input
 */
function intval_array($input) {
    if (!is_array($input))
        return intval($input);
 
    return array_map('intval_array', $input);
}

/**
 * 对数组中所有值强制转换成string
 * @param unknown_type $input
 */
function strval_array($input) {
    if (!is_array($input))
        return strval($input);
 
    return array_map('strval_array', $input);
}