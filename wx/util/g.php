<?php
/**
 * 加载相关的函数库
 *
 * @author LiangJianMing
 * @create 2016-01-06 15:49:36
 */

/**
 * 方便调用g类方法时，可以使用类别名
 * 该函数仅对同一次php请求有效
 *
 * @access public
 * @param string $src_name 原类名称，如果该值为空，则表示获取 other_name 的原类名称
 * @param string $other_name 别名，该值不能为空
 * @return mixed
 */
function g_map($src_name, $other_name) {
    static $pool = array();

    $src_name = strval($src_name);
    $other_name = strval($other_name);

    if ($other_name === '') {
        return false;
    }

    // 获取别名对应的原类名称
    if ($src_name === '') {
        if (isset($pool[$other_name])) {
            return $pool[$other_name];
        }

        return false;
    }

    if (isset($pool[$other_name]) && $pool[$other_name] !== $src_name) {
    	throw new \Exception("[{$other_name}]已被定义,已生效[$pool[$other_name]],未生效 [{$src_name}]");
    }
    $pool[$other_name] = $src_name;

    return false;
}

/**
 * 批量进行g_map的映射建立
 *
 * @access public
 * @param array $map 映射数组，格式如：array($src_name => $other_name)
 * @return void
 */
function g_maps(array $map) {
    !is_array($map) and $map = array();

    foreach ($map as $key => $val) {
        g_map($key, $val);
    }
}

/** 
 * 统一类实例化函数，一般以单例模式运行，
 * 两种用法:
 *      1. g_map('\package\cache\redis', 'redis');
 *         g('redis') -> set();
 *
 *      2. $obj = g('\package\cache\redis');
 *          $obj -> set();
 * 
 * @global
 * @param string $name 完整类名称（包含完整命名空间），或使用g_map设置的别名
 * @param array $param 该数组包含执行new构造函数时的全部参数列表，将按顺序注入到构造函数
 * @param boolean $is_new 是否强制进行新的实例化
 * @return object
 */
function g($name, array $param=array(), $is_new=FALSE) {
    static $pool = array();

    $name = strval($name);

    if ($name === '') {
        error_log(__FUNCTION__ . ": 非法的参数[name:{$name}]");
        return false;
    }

    $real_name = $name;
    $map_str = g_map('', $name);
    $map_str !== false and $real_name = $map_str;

    $index = md5($real_name . '::' . json_encode($param));
    if (isset($pool[$index]) and is_object($pool[$index]) and !$is_new) {
        return $pool[$index];
    }

    if (empty($param)) {
        $obj = new $real_name();
    }else {
        if (!class_exists('ReflectionClass', false)) {
            error_log(__FUNCTION__ . ": ReflectionClass类不存在");
            return false;
        }

        $rc = new ReflectionClass($real_name);
        $obj = $rc -> newInstanceArgs($param);
    }
   
    $pool[$index] = $obj;
    return $obj;
}

/** 
 * 用作统一访问类静态成员
 * 
 * @global
 * @param string $name 完整类名称（包含完整命名空间），或使用g_map设置的别名
 * @param string $member 成员名称
 * @param array $param 如果该成员是方法，则使用该参数执行方法
 * @return mixed 失败或不存在时返回false
 */
function sg($name, $member, array $param=array()) {
    $name = strval($name);
    $member = strval($member);

    if ($name === '' or $member === '') {
        error_log(__FUNCTION__ . ": 非法的参数[name:{$name}, member:{$member}]");
        return false;
    }

    $real_name = $name;
    $map_str = g_map('', $name);
    $map_str !== false and $real_name = $map_str;

    if (!class_exists('ReflectionClass', false)) {
        error_log(__FUNCTION__ . ": ReflectionClass类不存在");
        return false;
    }

    try {
        $rc = new ReflectionClass($real_name);

        // 检测属性是否存在
        $check = $rc -> hasProperty($member);
        if ($check) {
            $ret = $rc -> getStaticPropertyValue($member);
            return $ret;
        }

        // 检测方法是否存在
        $check = $rc -> hasMethod($member);
        if ($check) {
            $ret = $rc -> getMethod($member);
            // 执行方法
            $ret = $ret -> invokeArgs(null, $param);
            return $ret;
        }

        // 检测常量是否存在
        $check = $rc -> hasConstant($member);
        if ($check) {
            $ret = $rc -> getConstant($member);
            return $ret;
        }
    }catch(ReflectionException $e) {
        error_log(__FUNCTION__ . ": " . $e -> getMessage());
    }

    return false;
}

/* End of this file */