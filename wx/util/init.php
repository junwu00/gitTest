<?php
/**
 * 负载加载util
 *
 * @author JianMing Liang
 * @create 2016-01-26 09:59:50
 */
$util_root_dir = dirname(__FILE__);
$this_file_name = basename(__FILE__);

$loader_file_name = 'loader.php';
$loader_file_path = $util_root_dir . "/{$loader_file_name}";
require_once($loader_file_path);

$system_file_name = 'system.php';
$system_file_path = $util_root_dir . "/{$system_file_name}";
require_once($system_file_path);

$exclude = array(
    $this_file_name, $loader_file_name, $system_file_name,
);
// 加载当前目录下的全部文件
load_all_file($util_root_dir, $exclude);

/* End of this file */