<?php
/**
 * 系统级常用函数
 *
 * @author LiangJianMing
 * @create 2016-01-06 10:28:26
 */

/**
 * 批量定义常量
 *
 * @global
 * @param array $config 配置数组
 * @return void
 */
function batch_defined(array $config) {
    foreach ($config as $key => $val) {
        !defined($key) and define($key, $val);
    }
}

/**
 * 批量引入文件，使用include_once
 *
 * @global
 * @param array $files 文件集合
 * @return void
 */
function batch_include(array $files) {
    foreach ($files as $path) {
        file_exists($path) and include_once($path);
    }
}

/**
 * 批量引入文件，使用require_once，引入失败会终止执行
 *
 * @global
 * @param array $files 文件集合
 * @return void
 */
function batch_require(array $files) {
    foreach ($files as $path) {
        if (!file_exists($path)) {
            error_log(__FUNCTION__ . ": 无法打开{$path}文件, 文件不存在");
            continue;
        }

        require_once($path);
    }
}

/* End of this file */