<?php

namespace model\helper\view;

class HelperIndex extends Base {
	
	public function __construct($log_pre_str='') {
		parent::__construct($log_pre_str);
	}
	
	public function index() {
		$title = g('ser_index') -> get_page_title();
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/index.html');
	}
	
	public function get_user_by_id() {
		$user = g('ser_index') -> get_user_by_id(1);
		parent::echo_ok(array('data' => $user));
	}

	//通用UI页面
	public function uiDemo() {
		$title = '通用UI页面';
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/commonUI_Demo.html');
	}

	public function test() {
		$title = '测试';
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/test/test.html');
	}
	
	public function getList() {
		$title = '获取列表';
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/list/index.html');
	}

	public function gdmap(){
		$title = '地图';
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/gdmap.html');
	}
}

//end