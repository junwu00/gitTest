<?php
/**
 * 信息发布类型处理类
 * @author yangpz
 * @date 2015-06-13
 */

namespace model\helper\dao;

class NewsType extends DaoBase {
	
	private static $TABLE = 'news_type';
	
	/** 删除 0 */
	public static $STATE_DEL 	= 0;
	/** 启用 1 */
	public static $STATE_ON 	= 1;
	/** 禁用 2 */
	public static $STATE_DFF 	= 2;
	
	/** 默认查找的字段 */
	private static $DEF_FIELD = 'id, name';
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$TABLE);
	}
	
	/**
	 * 根据ID获取信息发布分类 
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $id		分类ID
	 * @param unknown_type $fields	查找的字段
	 * @throws \SCException
	 */
	public function get_by_id($com_id, $id, $fields='') {
		empty($fields) && $fields = self::$DEF_FIELD;
		$cond = array(
			'com_id=' 	=> $com_id,
			'id=' 		=> $id,
			'state!=' 	=> self::$STATE_DEL
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
}

//end