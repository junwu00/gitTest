<?php
/**
 * 信息发布文章处理类
 * @author yangpz
 * @date 2015-06-17
 */

namespace model\helper\dao;

class NewsRoad extends DaoBase {
	
	private static $TABLE = 'news_road';
	
	/** 禁用 0 */
	public static $STATE_OFF 	= 0;
	/** 启用 1 */
	public static $STATE_ON 	= 1;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$TABLE);
	}
	
	/**
	 * 根据分类、文章ID获取文章信息
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $content_id	文章ID
	 * @param unknown_type $user_id		用户ID
	 */
	public function isexist_user($com_id,$content_id,$user_id) {
		if (empty($content_id))							throw new \Exception('文章不能为空');
		if (empty($user_id))							throw new \Exception('成员ID不能为空');
		$info_state = self::$STATE_ON;
		//查数据
		$table = self::$TABLE;
		$sql = <<<EOF
		SELECT count(1) cnt FROM {$table}  
		WHERE info_state = {$info_state} 
		AND user_id = {$user_id} AND com_id={$com_id}
		AND news_id = {$content_id}
EOF;
		$ret = $this -> query($sql);
		return $ret[0]['cnt'];
	}
	
	
	/**
	 * 保存阅读信息
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		用户ID
	 * @param unknown_type $id			文章ID
	 */
	public function save($com_id,$user_id, $content_id ) {
		$data = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'news_id' => $content_id,
			'road_time' => time(),
			'info_state' => self::$STATE_ON,
		);
		return $this -> insert($data, '保存阅读记录失败');
	}
	
}

//end