<?php
/**
 * 数据库操作类基础类
 * 主要用于初始化配置
 * @author yangpz
 * @date 2016-02-17
 * 
 */

namespace model\helper\dao;

abstract class DaoBase {
	/** 企业ID */
	protected $com_id = 0;
	/** 企业根部门ID */
	protected $root_id = 0;
	/** 成员ID */
	protected $user_id = 0;
	/** 调用企业号接口的Access Token */
	protected $atoken = '';
	
	/** 缓存key前缀 */
	protected $cache_key_prefix;
	
	/** 当前操作的库表 */
	private $table;
	
	public function __construct() {
		if (session_id()) {
			isset($_SESSION[SESSION_VISIT_COM_ID]) && $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
			isset($_SESSION[SESSION_VISIT_DEPT_ID]) && $this -> root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
			isset($_SESSION[SESSION_VISIT_USER_ID]) && $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
		}
		$this -> cache_key_prefix = MAIN_HOST;
	}
	
	/**
	 * 变更当前处理的企业/成员信息（暂不验证之间对应关系是否正确）
	 * @param array $conf
	 */
	public function set_conf(array $conf) {
		isset($conf['com_id']) && $this -> com_id = intval($conf['com_id']);
		isset($conf['root_id']) && $this -> root_id = intval($conf['root_id']);
		isset($conf['user_id']) && $this -> user_id = intval($conf['user_id']);
		return $this;
	}
	
	/**
	 * 设置access token
	 * @param unknown_type $atoken
	 */
	public function set_atoken($atoken) {
		$this -> atoken = $atoken;
		return $this;
	}
	
	/**
	 * 根据指定条件获取信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function get_by_cond(array $cond, $fields='*') {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return FALSE;
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 根据指定条件获取应用信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function list_by_cond(array $cond, $fields='*') {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return array();
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond);
		return $ret ? $ret : array();
	}
	
	/**
	 * 分页查询
	 * @param array $cond				查询条件
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $order_by	排序
	 * @param unknown_type $with_cnt	是否返回总数，默认：FALSE
	 */
	protected function list_by_page(array $cond, $fields, $page, $page_size, $order_by='', $with_cnt=FALSE) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond, $page, $page_size, $order_by, $with_cnt);
		if ($with_cnt) {
			$ret = array(
                'data' 	=> $ret['data'] ? $ret['data'] : array(),
                'count' => $ret['count'],
            );
		} else {
			$ret = $ret ? $ret : array();
		}
		return $ret;
	}
	
	/**
	 * 插入数据
	 * @param array $data			要插入的数据
	 * @param unknown_type $exp_msg	异常提示描述
	 */
	protected function insert(array $data, $exp_msg) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return array();
		$ret = g('pkg_db') -> insert($this -> table, $data);
		if (!$ret) {
			throw new \Exception($exp_msg);
		}
		return $ret;
	}
	
	/**
	 * 执行sql查询语句
	 * @param unknown_type $string	sql语句
	 */
	protected function query($sql) {
		return g('pkg_db') -> prepare_query($sql);
	}
	
	/**
	 * 执行sql非查询语句
	 * @param unknown_type $sql
	 * @param unknown_type $exp_msg	异常提示描述
	 */
	protected function exec($sql, $exp_msg) {
		$ret = g('pkg_db') -> prepare_exec($sql);
		if (!$ret) {
			throw new \Exception($exp_msg);
		}
		return $ret;
	}
	
	/**
	 * 更新数据
	 * @param array $cond				更新条件
	 * @param array $data				更新结果
	 * @param unknown_type $errmsg		错误提示
	 * @param unknown_type $throw_exp	是否抛异常,默认:TRUE
	 */
	protected function update_by_cond(array $cond, array $data, $errmsg, $throw_exp=TRUE) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return TRUE;
		$ret = g('pkg_db') -> update($this -> table, $cond, $data);
		if (!$ret && $throw_exp) {
			throw new \Exception($errmsg);
		}
		return $ret;
	}
	
	/**
	 * 验证当前操作是否指定具体的企业，不指定将抛异常
	 */
	protected function _verify_necessary_cond(array $cond=array('root_id', 'com_id')) {
		foreach ($cond as $item) {
			if (!isset($this -> $item))	continue;
			
			if (empty($this -> $item))	throw new \Exception('未指定必须的查询条件');
		}unset($item);
	}
	
	/** 初始化当前库表 */
	protected function set_table($table) {
		$this -> table = $table;
		return $this;
	}
	
	//日志记录===================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, '', $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, '', $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, '', $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, '', $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, '', $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, '', $str);
	}
	
}

//end