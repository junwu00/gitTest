<?php
/**
 * 信息发布文章附件类
 * @author yangpz
 * @date 2015-12-04
 */

namespace model\helper\dao;

class NewsFile extends DaoBase {
	private static $TABLE = 'news_file';
	
	/** 删除  0 */
	public static $STATE_DEL = 0;
	/** 启用 1 */
	public static $STATE_ON = 1;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$TABLE);
	}
	
	/**
	 * 根据ID获取附件信息
	 * @param unknown_type $id			附件ID
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $content_id	文件ID
	 * @param unknown_type $fields		查看的字段
	 */
	public function get_by_id($id, $com_id, $content_id, $fields='*') {
		$cond = array(
			'id=' 			=> $id,
			'com_id=' 		=> $com_id,
			'content_id=' 	=> $content_id,
			'state=' 		=> self::$STATE_ON
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
	/**
	 * 获取文章所有有效附件
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $content_id	文章ID
	 * @param unknown_type $fileds		查看的字段
	 */
	public function list_by_content_id($com_id, $content_id, $fields='*') {
		$cond = array(
			'com_id=' 		=> $com_id,
			'content_id=' 	=> $content_id,
			'state=' 		=> self::$STATE_ON
		);
		return $this -> list_by_cond($cond, $fields);
	}
    
}

//end of file