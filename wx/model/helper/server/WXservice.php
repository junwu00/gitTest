<?php
/**
 * 新闻公告
 * 
 * @author yangpz
 * @date 2015-06-26
 *
 */
namespace model\helper\server;

class WXService extends \model\api\server\WXAppService {
	/** 操作指引的图文 */
	private $help_art;
	
	public function __construct() {
		$this -> help_art = load_config('model/helper/guide');
		parent::__construct();
	}
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy_msg') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy_msg') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file