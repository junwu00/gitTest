<?php

namespace model\helper\server;

class Index extends Base {
	
	public function __construct($log_pre_str='') {
		parent::__construct($log_pre_str);
	}
	
	public function get_page_title() {
		$user = g('dao_user') -> get_by_id(80225);
		return $user['acct'] . '_自定义页面标题';
	}
	
	public function get_user_by_id($id) {
		return g('dao_user') -> get_by_id($id);
	}
	
}

//end