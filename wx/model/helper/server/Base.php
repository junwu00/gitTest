<?php

namespace model\helper\server;

abstract class Base {
	
	/** 当前模块名称 */
	protected $mod_name;
	/** 当前模块中文名称 */
	protected $mod_cn_name;
	/** 当前模块对应的日志存放的根目录 */
	protected $log_dir;
	/** 当前模块对应的本地上传的根目录 */
	protected $upload_dir;

	/** 当前模块需记录的日志级别 */
	protected $log_lev;
	/** 当前模块对应的正常/警告日志的路径 */
	protected $log_file_path;
	/** 当前模块对应的错误日志的路径 */
	protected $err_file_path;
	
	/** 日志中一般要补充的前置的信息，如：[com:123][admin:123] */
	protected $log_pre_str;
	
	/**
	 * 构造方法
	 * 初始化模块内通用配置、变量等
	 * 
	 * $log_pre_str 日志中一般要补充的前置的信息，如：[com:123][admin:123]
	 */ 
	public function __construct($log_pre_str='') {
		$this -> log_pre_str = $log_pre_str;
		
		global $mod_conf;
		$this -> mod_name 		= $mod_conf['name'];
		$this -> mod_cn_name 	= $mod_conf['cn_name'];
		$this -> log_dir 		= $mod_conf['log_dir'];
		$this -> upload_dir		= $mod_conf['upload_dir'];
		
		$today = date('Ymd');
		$this -> log_lev 		= $mod_conf['log_lev'];
		$this -> log_file_path	= $this -> log_dir . DS . $today . '.log';
		$this -> err_file_path	= $this -> log_dir . DS . $today . '.error.log';
	}
	
	//分级日志记录：由配置文件中log_lev参数控制===========================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str, $encode=FALSE) {
		if ($this -> log_lev > 0)		return;
		
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str, $encode=FALSE) {
		if ($this -> log_lev > 1)		return;
		
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str, $encode=FALSE) {
		if ($this -> log_lev > 2)		return;
		
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str, $encode=FALSE) {
		if ($this -> log_lev > 3)		return;
		
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str, $encode=FALSE) {
		if ($this -> log_lev > 4)		return;
		
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str, $encode=FALSE) {
		$encode && $str = urldecode(json_encode(urlencode_array($str)));
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	//END 分级日志记录===========================================================================================
	
}

//end