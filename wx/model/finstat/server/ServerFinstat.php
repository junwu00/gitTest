<?php 
/**
 * 财务报表业务逻辑处理类
 * @author luja
 * @create 2016-08-12
 */
namespace model\finstat\server;

class ServerFinstat extends \model\api\server\ServerBase {

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	/** 表单报表 */
	private static $Report_Table = 1;
	/** excel报表 */
	private static $Report_Excel = 2;

	private static $Privilege_Name = 'REPORT';

	public function __construct($log_pre_str='') {
		parent::__construct('finstat', $log_pre_str);

	}

    /** 获取表格数据 */
    public function get_table_data_for_search($table_id, $order_list, $count=FALSE, $start = 0, $page_size = 20, $other_condition=[],$is_export=false){
        $table = g('api_report') -> get_table_by_id($this -> com_id, $table_id, '*');
        if(empty($table))
            throw new \Exception('报表不存在');

        $table['type'] == 2 && $count=true;

        $form_ids = json_decode($table['form_ids'],true);
        !is_array($form_ids) && $form_ids=[$form_ids];
        $forms = g('mv_form')->get_forms_by_ids($form_ids,'id');
        if(count($forms)!=count($form_ids))
            throw new \Exception('报表数据来源表单不存在');

        $rowkeys = json_decode($table['row_list'],true);
        if(empty($rowkeys))
            throw new \Exception('报表关联字段不存在');

        $row = json_decode($table['row_list'], TRUE);
        $col_key = json_decode($table['col_key'], TRUE);
        $cond_list = json_decode($table['cond_list'], TRUE);
        $join_list = json_decode($table['join_list'], TRUE);
        $other_list = json_decode($table['other_list'], TRUE);
        empty($order_list) && $order_list = json_decode($table['order_list'], TRUE);
        $sum = TRUE;

        //根据其他条件
        if($other_condition){
            foreach ($other_condition as &$tmp){
                if( !in_array($tmp['field_key'],array_keys($rowkeys)) ){
                    throw new \Exception('报表关联字段不存在');
                }

                $tmp['operator'] = 'like';
                switch ($row[$tmp['field_key']]['type']){
                    case 'dept':
                        $tmp['operator'] = 'in';
                        //通过关键词查询部门id
                        $cond = array(
                            'root_id='=>$_SESSION[SESSION_VISIT_DEPT_ID],
                            'state!='=>0,
                            'name like' => '%'.$tmp['field_val'].'%',
                        );
                        $depts = g('dao_dept')->list_by_cond($cond,'id');
                        $depts && $tmp['field_val'] = implode(',',array_column($depts,'id'));
                        break;
                    case 'people':
                    case 'creater_id':

                        $tmp['operator'] = 'in';
                        //通过关键词查询用户id
                        $cond = array(
                            'com_id='=>$this->com_id,
                            'root_id='=>$_SESSION[SESSION_VISIT_DEPT_ID],
                            'state!='=>0,
                            'name like' => '%'.$tmp['field_val'].'%',
                        );
                        $users = g('dao_user')->list_by_cond($cond,'id');
                        $users && $tmp['field_val'] = implode(',',array_column($users,'id'));
                        break;

                    case 'schoolDept':
                        $tmp['operator'] = 'in';
                        //通过关键词查询用户id
                        $cond = array(
                            'state!='=>0,
                            'name like' => '%'.$tmp['field_val'].'%',
                        );
                        $users = g('dao_user')->list_by_cond($cond,'id');
                        $users && $tmp['field_val'] = implode(',',array_column($users,'id'));
                        break;
                    case 'com_id':
                    case 'school':
                        $tmp['operator'] = 'in';
                        //通过关键词查询用户id
                        $cond = array(
                            'state!='=>0,
                            'name like' => '%'.$tmp['field_val'].'%',
                        );
                        $coms = g('dao_com')->list_by_cond($cond,'id');
                        $coms && $tmp['field_val'] = implode(',',array_column($coms,'id'));
                        break;
                    case 'date':
                    case 'create_time':
                        if(strpos($tmp['field_val'], ' - ')!==false){
                            $tmp['operator'] = 'between';
                            $vals = explode(' - ',$tmp['field_val']);
                            foreach ($vals as &$v){
                                $v = strtotime($v);
                            }unset($v);
                            $tmp['field_val'] = implode(' - ',$vals);
                        }else{
                            $tmp['operator'] = '=';
                        }
                        break;
                    case 'money':
                        if(strpos($tmp['field_val'], ' - ')!==false){
                            $tmp['operator'] = 'between';
                            break;
                        }else{
                            $tmp['operator'] = '=';
                        }
                        break;
                }

            }unset($tmp);
        }

        foreach ($row as &$val) {
            if(strpos($val['input_key'], "=")){
                //关联条件
                $input_key = explode("=", $val['input_key']);
                $val['input_key'] = array_shift($input_key);
            }
        }unset($val);

        $col_value = array();
        foreach ($col_key as $key) {
            $row[$key]['count_type'] = 0;
            $col_value  =  array_merge($col_value, $row[$key]['list_key']);
        }unset($key);

        empty($col_key) && $count = FALSE;

        $data = g('api_report') -> get_conf_data($row, $col_value, $cond_list, $join_list, $order_list, $other_list, $count, $sum, $start, $page_size, $other_condition);

        if($is_export){
            $ret = array(
                'data' => $data,
                'sum' => $sum,
                'count' => $count
            );
        }else{
            //获取报表信息
            $report_id = $table['report_id'];
            $report = $this->get_report_info($report_id, 'id, name, `desc`, type, visit_dept, visit_group, visit_user, has_table, has_chart, create_id, state, info_state');
            if($report['state'] == 2){
                throw new \Exception('当前报表已被管理员禁用');
            }
            $has_chart = $report['has_chart'];
            $report_conf = $this->get_table_by_report($report_id, 0, $cond, $has_chart, $page_size);

            $ret = $this -> data2table($col_key, $row, $data, $count, $sum);
            $ret['table_list'] = $report_conf['table_list'];
        }

        return $ret;
    }


    /** 获取表格数据 */
	public function get_table_data_for_process($table_id, $order_list, $count=FALSE, $start = 0, $page_size = 20, $other_condition=[]){
		$table = g('api_report') -> get_table_by_id($this -> com_id, $table_id, '*');
        if(empty($table))
            throw new \Exception('报表不存在');

        $table['type'] == 2 && $count=true;

        $form_ids = json_decode($table['form_ids'],true);
        !is_array($form_ids) && $form_ids=[$form_ids];
        $forms = g('mv_form')->get_forms_by_ids($form_ids,'id');
        if(count($forms)!=count($form_ids))
            throw new \Exception('报表数据来源表单不存在');

        $rowkeys = json_decode($table['row_list'],true);
        if(empty($rowkeys))
            throw new \Exception('报表关联字段不存在');

        //根据其他条件
        if($other_condition){
            foreach ($other_condition as $tmp){
                if( !in_array($tmp['field_key'],array_keys($rowkeys)) ){
                    throw new \Exception('报表关联字段不存在');
                }
            }unset($tmp);
        }


        $row = json_decode($table['row_list'], TRUE);
		$col_key = json_decode($table['col_key'], TRUE);
		$cond_list = json_decode($table['cond_list'], TRUE);
		$join_list = json_decode($table['join_list'], TRUE);
		$other_list = json_decode($table['other_list'], TRUE);
		empty($order_list) && $order_list = json_decode($table['order_list'], TRUE);
		$sum = TRUE;

		foreach ($row as &$val) {
			if(strpos($val['input_key'], "=")){
				//关联条件
				$input_key = explode("=", $val['input_key']);
				$val['input_key'] = array_shift($input_key);
			}
		}unset($val);

		$col_value = array();
		foreach ($col_key as $key) {
			$row[$key]['count_type'] = 0;
			$col_value  =  array_merge($col_value, $row[$key]['list_key']);
		}unset($key);

		empty($col_key) && $count = FALSE;

		$data = g('api_report') -> get_conf_data($row, $col_value, $cond_list, $join_list, $order_list, $other_list, $count, $sum, $start, $page_size, $other_condition);
		$ret = $this -> data2table($col_key, $row, $data, $count, $sum);

		return $ret;
	}

    /** 获取表格数据 */
    public function get_table_data($table_id, $order_list, $count=FALSE, $start = 0, $page_size = 20){
        $table = g('api_report') -> get_table_by_id($this -> com_id, $table_id, '*');
        $row = json_decode($table['row_list'], TRUE);
        $col_key = json_decode($table['col_key'], TRUE);
        $cond_list = json_decode($table['cond_list'], TRUE);
        $join_list = json_decode($table['join_list'], TRUE);
        $other_list = json_decode($table['other_list'], TRUE);
        empty($order_list) && $order_list = json_decode($table['order_list'], TRUE);
        $sum = TRUE;

        foreach ($row as &$val) {
            if(strpos($val['input_key'], "=")){
                //关联条件
                $input_key = explode("=", $val['input_key']);
                $val['input_key'] = array_shift($input_key);
            }
        }unset($val);

        $col_value = array();
        foreach ($col_key as $key) {
            $row[$key]['count_type'] = 0;
            $col_value  =  array_merge($col_value, $row[$key]['list_key']);
        }unset($key);

        empty($col_key) && $count = FALSE;

        $data = g('api_report') -> get_conf_data($row, $col_value, $cond_list, $join_list, $order_list, $other_list, $count, $sum, $start, $page_size);
        $ret = $this -> data2table($col_key, $row, $data, $count, $sum);
        return $ret;
    }

    /** 获取图数据 */
	public function get_chart_data($chart_id){
		$fields = 'c.id, c.value_key, c.classify_key col_key, t.row_list,  t.cond_list, t.join_list, t.other_list, t.order_list';
		$ret = g('api_report') -> get_chart_by_id($this -> com_id, $chart_id, $fields, TRUE);
		if(empty($ret)){
			return array();
		}
		$value_key = json_decode($ret['value_key'], TRUE);
		$col_key = json_decode($ret['col_key'], TRUE);
		$row_list = json_decode($ret['row_list'], TRUE);

		foreach ($row_list as &$val) {
			$input_key = explode("=", $val['input_key']);
			$val['input_key'] = array_shift($input_key);
		}unset($val);

		$row = array();
		$col_list = array();
		foreach ($col_key as $key) {
			$row_list[$key]['count_type'] = 0;
			$col_list = array_merge($col_list, $row_list[$key]['list_key']);
			// $col_list[] = $row_list[$key]['input_key'];
			$row[] = $row_list[$key];
		}unset($key);

		foreach ($value_key as $key) {
			$row_list[$key]['count_type'] = 1;
			$row[] = $row_list[$key];
		}unset($key);

		$cond_list = json_decode($ret['cond_list'], TRUE);
		$join_list = json_decode($ret['join_list'], TRUE);
		$other_list = json_decode($ret['other_list'], TRUE);
		$order_list = json_decode($ret['order_list'], TRUE);
		
		$chart_join_list = array();
		foreach ($join_list as $join) {
			$join_value = explode("=", $join['value']);
			if(in_array($join_value[0], $col_list)){
				$chart_join_list[] = $join;
			}
		}unset($join);
		
		$ret =  g('api_report') -> get_conf_data($row, $col_list, $cond_list, $chart_join_list, $order_list, $other_list);
		foreach ($ret as &$val) {
			$val = array_values($val);
		}unset($val);
		return $ret;
	}	

	/** 获取报表内表格 */
	public function get_table_by_report($report_id, $table_id, $cond=array(), $with_chart=TRUE, $page_size = 20){
		$ret = g('api_report') -> get_table_by_report($this -> com_id, $report_id, $table_id, $with_chart);
		$data = array();
		if($with_chart) {
			//整合表格和图
			foreach ($ret as $val) {
				if(!isset($data[$val['id']])){
					$data[$val['id']] = array(
							'id' => $val['id'],
							'name' => $val['name'],
							'type' => $val['type'],
							'row_list' => json_decode($val['row_list'], TRUE),
							'other_list' => json_decode($val['other_list'], TRUE),
							'cond_list' => json_decode($val['cond_list'], TRUE),
							'join_list' => json_decode($val['join_list'], TRUE),
							'col_key' => json_decode($val['col_key'], TRUE),
							'chart_list' => array()
						);
					$data[$val['id']]['col_key_count'] = count($data[$val['id']]['col_key']);
				}
				$value_key = json_decode($val['value_key'], TRUE);
				$col_key = json_decode($val['classify_key'], TRUE);

				$row_list = array();
				 foreach ($value_key as $key) {
					$row_list[] = array(
							"key" => $data[$val['id']]['row_list'][$key]['row_key'],
		                    "input_type" => $data[$val['id']]['row_list'][$key]['type'],
		                    "input_format" => $data[$val['id']]['row_list'][$key]['format'],
		                    "name" => $data[$val['id']]['row_list'][$key]['name']
						);
				}unset($row);
				if(!empty($value_key) && !empty($col_key)){
					$data[$val['id']]['chart_list'][] = array(
							'id' => $val['chart_id'],
							'name' => $val['chart_name'],
							'chart_type' => $val['chart_type'],
							'col_key' => $col_key,
							'value_key' => json_decode($val['value_key']),
							'row_list' => $row_list,
						);
				}
			}unset($val);
			$data = array_values($data);
		}else{
			foreach ($ret as $val){
				$col_key_count = json_decode($val['col_key'], TRUE);
				$data[] = array(
						'id' => $val['id'],
						'name' => $val['name'],
						'type' => $val['type'],
						'row_list' => json_decode($val['row_list'], TRUE),
						'other_list' => json_decode($val['other_list'], TRUE),
						'cond_list' => json_decode($val['cond_list'], TRUE),
						'join_list' => json_decode($val['join_list'], TRUE),
						'col_key' => json_decode($val['col_key'], TRUE),
						'chart_list' => array(),
						'col_key_count' => count($col_key_count),
					);
			}unset($val);
		}

		$first_table_data = array();
		$cond_conf_list = array();
		if(!empty($data)){
			$first_table = $data[0];

			$row = $first_table['row_list'];

			// gch add
			// 其他选项限制
			// todo
			// $cond_list = $first_table['cond_list'];
			$cond_list = array();
			if (!empty($cond['cond_list'])) {
				$cond_list = array_merge($first_table['cond_list'], $cond['cond_list']);
			} else {
				$cond_list = $first_table['cond_list'];
			}
			// var_dump($cond_list);
			
			$join_list = $first_table['join_list'];

			// 时间限制
			$other_list = array();
			$c_time = explode(' - ', $cond['create_time']);
			$o_time = explode(' - ', $first_table['other_list']['create_time']);
			if(isset($c_time[0]) && $c_time[0] !== "") {
				// 有时间筛选
				if (isset($o_time[0]) && $o_time[0] !== "") {
					// 后台有时间筛选
					$c_time_s = strtotime($c_time[0]);
					$c_time_e = strtotime($c_time[1]);
					$o_time_s = strtotime($o_time[0]);
					$o_time_e = strtotime($o_time[1]);

					$flag_1 = $c_time_s > $o_time_s && $c_time_s < $o_time_e;
					$flag_2 = $c_time_e > $o_time_s && $c_time_e < $o_time_e;

					if ($flag_1 || $flag_2) {
						$time_s = $c_time_s > $o_time_s ? $c_time_s : $o_time_s;
						$time_e = $c_time_e < $o_time_e ? $c_time_e : $o_time_e;

						$other_list['create_time'] = date('Y-m-d H:i', $time_s) . ' - ' . date('Y-m-d H:i', $time_e);
					}
				} else {
					// 后台没有时间筛选
					$other_list['create_time'] = $cond['create_time'];
				}
			} else {
				// 没有时间筛选
				$other_list['create_time'] = $first_table['other_list']['create_time'];
			}

			// 提交人限制
			if (!empty($cond['create_id']) && !empty($first_table['other_list']['create_id'])) {
				// 有提交人筛选，表单也配置有提交人筛选
				$tmp_arr = array();
				foreach ($cond['create_id'] as $value) {
					in_array($value, $first_table['other_list']['create_id']) && $tmp_arr[] = $value;
				}
				$other_list['create_id'] = $tmp_arr;
			} elseif (!empty($first_table['other_list']['create_id'])) {
				$other_list['create_id'] = $first_table['other_list']['create_id'];
			} else {
				$other_list['create_id'] = $cond['create_id'];
			}
			
			// $other_list = $first_table['other_list'];
			$col_key = $first_table['col_key'];

			foreach ($row as &$val) {
				if(strpos($val['input_key'], "=")){
					//关联条件
					$input_key = explode("=", $val['input_key']);
					$val['input_key'] = array_shift($input_key);
				}
			}unset($val);

			$col_value = array();
			foreach ($col_key as $key) {
				// $col_value[] = $row[$key]['input_key'];
				$row[$key]['count_type'] = 0;
				$col_value  =  array_merge($col_value, $row[$key]['list_key']);
			}unset($key);

			$count = empty($first_table['col_key']) ? FALSE : TRUE;
			$order_list = array();
			$sum = TRUE;

			$ret = g('api_report') -> get_conf_data($row, $col_value, $cond_list, $join_list, $order_list, $other_list, $count, $sum, 0, $page_size);

			$table_data  = $this -> data2table($col_key, $row, $ret, $count, $sum);
			$first_table_data['table_data'] = $table_data;
			$first_table_data['chart_data'] = array();

			foreach ($first_table['chart_list'] as $val) {
				$col_list = array();
				$row_list = array();
				foreach ($val['col_key'] as $key) {
					$col_list = array_merge($col_list, $row[$key]['list_key']);
					$row_list[] = $row[$key];
				}unset($key);

				foreach ($val['value_key'] as $key) {
					$row[$key]['count_type'] = 1;
					$row_list[] = $row[$key];
				}unset($key);

				$chart_join_list = array();
				foreach ($join_list as $join) {
					$join_value = explode("=", $join['value']);
					if(in_array($join_value[0], $col_list)){
						$chart_join_list[] = $join;
					}
				}unset($join);

				$count = FALSE;
				$sum = FALSE;
				$ret = g('api_report') -> get_conf_data($row_list, $col_list, $cond_list, $chart_join_list, $order_list, $other_list, $count, $sum, 0, 0);
				foreach ($ret as &$val) {
					$val = array_values($val);
				}unset($val);
				$first_table_data['chart_data'][] = array(
						"data" => $ret
					);
			}unset($val);

		}

		foreach ($data as &$val) {
			// gch add
			// unset($val['row_list']);
			unset($val['col_key']);
			unset($val['other_list']);
			// unset($val['cond_list']);
			unset($val['join_list']);
		}unset($val);

		$ret = array(
				"table_list" => $data,
				"first_data" => $first_table_data,
			);
		return $ret;
	}

	/** 
	  * 将查询结果按照表格插件格式返回 
	  *
	  *	 row_key 分类key数组
	  *	 title   表头数据
	  *  data    查询结果
	  *  count   汇总数据
	  */
	private function data2table($row_key, $title, $data, $count, $sum=0){
		$thead = array();
		$tbody = array();
		$tfoot = array();
		$body_merge_col = array();

		if(!empty($title)){
			$thead_tmp = array();
			foreach ($title as $key => $val) {
				 $t = array(
						"key" => $key,
						"val" => $val['name'],
					);
				if(($val['type']== "text" && ($val['format'] == "money" || $val['format'] == "number" || $val['format'] == "function")) || $val['type'] == "money"){
					$t['type'] = "number";
				}elseif($val['type']== "dangerous"){
                    $t['type'] = 'dangerous';
                }else{
					$t['type'] = "text";
				}
				$thead_tmp[] = $t;
			}unset($val);
			$thead[] = array("data" => $thead_tmp);
		}

		//行数
		$r = 0;
		$last_key = array();
		foreach ($data as $row) {
			$row_tmp = array();
			$tmp = array();
				
			if(empty($row_key)){
				$max_count = 1;
				foreach ($row as $key => &$val) {
					if(is_array($val) && $max_count < count($val)){
						$max_count = count($val);
					}
				}
				$num = 0;
				while($num < $max_count){
					$r_t = array();
					$col = 0;
					foreach ($row as $key => &$val) {
						if(is_array($val)){
							$k_key = "{$key}_data_{$r}_{$num}";
							$t_val = isset($val[$num]) ? $val[$num] : "";
						}else{
							$k_key = "{$key}_data_{$r}";
							$t_val = $val;
							if($max_count > 1){
								$body_merge_col[] = $col;
							}
						}

						$r_t[] = array(
								"key" => $k_key,
								"val" => $t_val
							);
						$col++;
					}unset($val);
					$row_tmp[] = array(
							"data" => $r_t
						);
					$num++;
				}
			}else{
				$row_state = 0;
				$col = 0;
				foreach ($row as $key => &$val) {
					$k_key = "{$key}_data_{$r}";
					//汇总表
					if(in_array($key, $row_key)){
						if(isset($last_key[$key]) && $last_key[$key]['val'] == $val && $row_state == 0){
							//分类KEY
							$k_key = $last_key[$key]['key'];
							$body_merge_col[] = $col;
						}else{
							$row_state = 1;
						}
						$last_key[$key] = array(
								"key" => $k_key,
								"val" => $val
							);
					}
					$tmp[] = array(
							'key' => $k_key,
							'val' => $val
						);
					$col++;
				}unset($val);
			}
			$r++;
			if(empty($row_tmp)){
				$tbody[] = array(
						'data' => $tmp
					);
			}else{
				$tbody = array_merge($tbody, $row_tmp);
			}
		}unset($row);

		if(!empty($count)){
			$tfoot_tmp = array();
			foreach ($count as $key => $val) {
				$k_key = "{$key}_count";
				if(in_array($key, $row_key)){
					$k_key = "count";
					$val = "汇总";
				}
				$tfoot_tmp[] = array(
						"key" => $k_key,
						"val" => $val
					);
			}unset($val);
			$tfoot[] = array("data" => $tfoot_tmp);
		}

		if(!empty($body_merge_col)){
			$body_merge_col = array_unique($body_merge_col);
		}

		$ret = array(
					"data" => array(
							"thead" => $thead,
							"tbody" => $tbody,
							"tfoot" => $tfoot,
						),
					"sum" => $sum,
					"body_merge_col" => $body_merge_col,
					"page_length" => count($data),
				);

		return $ret;
	}

	/**
	 * 获取报表基本信息
	 * @param  [type] $report_id     
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_report_info($report_id, $fields='*', $preview=FALSE) {
		$report = g('api_report') -> get_report_by_id($this -> com_id, $report_id, $fields);

		if($preview) return $report;
		
		if (!$report || $report['info_state'] == 0)  throw new \Exception('当前报表已被管理员删除');

		$visit_user = json_decode($report['visit_user'], TRUE);
		if (in_array($this -> user_id, $visit_user)) {
			return $report;
		}
		$visit_dept = json_decode($report['visit_dept'], TRUE);
		$all_dept = $this -> get_user_dept();
		if(array_intersect($all_dept, $visit_dept)){
			return $report;
		}
		$visit_group = json_decode($report['visit_group'], TRUE);
		$all_group = $this -> get_user_group();
		if (array_intersect($visit_group, $all_group)) {
			return $report;
		}

		throw new \Exception('没有权限查看该报表');
	}

	/** 获取报表列表 */
	public function get_report_list($keyword, $page, $page_size, $with_count = FALSE){
		$all_dept = $this -> get_user_dept();
		$all_group = $this -> get_user_group();

		$cond = array(
				'state=' => 1
			);
		$cond['__OR'] = array();
		$cond['__OR']['visit_user LIKE '] = '%"' .$this -> user_id. '"%'; 
		foreach ($all_dept as $key => $dept) {
			$cond['__OR']['__'.$key.'__visit_dept LIKE '] = '%"'.$dept.'"%';
		}unset($dept);
		foreach ($all_group as $key => $group) {
			$cond['__OR']['__'.$key.'__visit_group LIKE '] = '%"' .$group. '"%';
		}unset($group);

		if(!empty($keyword)){
			$cond['name LIKE'] = "%{$keyword}%";
		}

		$fields = 'id, name, `desc`, has_table, has_chart, type, create_time';
		$order = 'ORDER BY id DESC';
		return g('api_report') -> get_report_list($this -> com_id, $cond, $fields, $page, $page_size, $order, $with_count);
	}

	/** 检查是否开通润恒报表功能 */
	public function check_runheng($com_id){
		return g('dao_acct') -> get_by_com($com_id);
	}

	public function check_privilege($com_id){
		$privilege = g('dao_service') -> check_privilege($com_id, self::$Privilege_Name);
		$vip_info = g('dao_vip') -> get_vip_by_com($com_id, 'is_vip, is_svip, past_time');
		if($vip_info['is_vip']==0 || ($vip_info['is_vip']==1 && $vip_info['past_time'] < time())){
			if(!$privilege){
				return false;
			}
		}
		$report_list = $this -> get_report_list('', 1, 1);
		return empty($report_list) ? FALSE : TRUE;
	}

	/**
	 * 获取报表
	 * @param  integer $page      
	 * @param  integer $page_size 
	 * @return [type]             
	 */
	public function get_list($account_id, $page=1, $page_size=20) {
		$table = ' runheng_file ';
		$cond = array(
			'account_id=' => $account_id,
			'info_state=' => self::$StateOn
			);
		$fileds = 'id, file_name, upload_name, upload_time, file_hash, report_name, year, month, file_ext, create_time';
		$order = ' ORDER BY report_name DESC, id desc ';

		$ret = g('dao_finstat') -> get_list_cond($table, $fileds, $cond, $page, $page_size, $order, TRUE);
		return $ret;
	}

	public function get_account_list($com_id){
        $sql = <<<EOF
select a.*, max(UNIX_TIMESTAMP(f.upload_time)) as last_upload_time
	from runheng_account a, runheng_file f
	where com_id=? and a.id=f.account_id and f.info_state=1
	group by a.id
	order by last_upload_time desc
EOF;
        $params = array($com_id);
        $ret = g('pkg_db') -> prepare_query($sql, 1, $params);
        empty($ret) && $ret = array();

        foreach ($ret as &$val) {
            $val['last_upload_time'] = $val['last_upload_time'];
        }unset($val);
		return $ret;
	}

	/**
	 * 获取报表详情
	 * @param  [type] $id     
	 * @param  string $fileds 
	 * @return [type]         
	 */
	public function get_by_id($id, $fileds='*') {
		$sql = " SELECT {$fileds} FROM runheng_file WHERE id={$id} AND info_state=".self::$StateOn;
		$ret = g('pkg_db') -> query($sql);

		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 获取excel文件信息
	 * @param  [type] $id 
	 * @return [type]     
	 */
	public function excel_report_detail($id) {
		$fields = array(
			'mprf.id', 'mprf.report_id', 'mprf.file_name', 'mprf.file_path', 'mprf.file_hash', 
			'mprf.file_ext', 'mprf.file_size', 'mprf.create_time', 'mprc.name report_name', 
			'mprc.visit_dept', 'mprc.visit_group', 'mprc.visit_user', 'mprf.info_state mprf_state', 
			'mprc.info_state', 'mprc.state'
			);
		$fields = implode(',', $fields);

		$excel = g('dao_excel') -> get_excel_by_id($id, $this -> com_id, $fields);
		if (!$excel) throw new \Exception('文件不存在');

		if ($excel['info_state'] == 0) {
			throw new \Exception('当前报表已被管理员删除');
		} elseif ($excel['state'] == 2) {
			throw new \Exception('当前报表已被管理员禁用');
		} elseif ($excel['mprf_state'] == 0) {
			throw new \Exception('文件已被删除');
		}
		
		return $excel;
	}

	/**
	*	用户名转id
	*	
	*	@access public
	*	@param array $name 用户名
	*	@return array 
	*	@author gch 2017-04-01 
	*/
	public function get_userid_by_names($name, $com_id) {
		$table = 'sc_user';
		$fields = '`id`';
		$cond['com_id='] = intval($com_id);
		$cond['state!='] = 0;
		if (is_array($name)) {
			$cond['name REGEXP'] .= '(';
			foreach ($name as $key => $value) {
				$cond['name REGEXP'] .= $key . "|";
			}
			$cond['name REGEXP'] = rtrim($cond['name REGEXP'], "|");
			$cond['name REGEXP'] .= ')';
		} else {
			$cond['name LIKE'] = '%' . $name . '%';
		}
		$ret = g('pkg_db') -> select($table, $fields, $cond);
		!is_array($ret) && $ret = array();
		$return = array();
		foreach ($ret as $v) {
			$return[] = $v['id'];
		}
		return $return;
		
	}

	/**
	*	部门名转id
	*	
	*	@access public
	*	@param 	array $name 部门名
	*	@return array 
	*	@author gch 2017-04-05 
	*/
	public function get_deptid_by_names($name, $root_id) {
		$table = 'sc_dept';
		$fields = '`id`';
		$cond['root_id='] = intval($root_id);
		$cond['state!='] = 0;
		$cond['name REGEXP'] .= '(';
		foreach ($name as $key => $value) {
			$cond['name REGEXP'] .= $key . "|";
		}
		$cond['name REGEXP'] = rtrim($cond['name REGEXP'], "|");
		$cond['name REGEXP'] .= ')';
		$ret = g('pkg_db') -> select($table, $fields, $cond);
		!is_array($ret) && $ret = array();
		$return = array();
		foreach ($ret as $v) {
			$return[] = $v['id'];
		}
		return $return;
		
	}


//----------------------------------内部实现---------------------------------

	/** 获取当前用户的所有部门 */
	private function get_user_dept(){
		$user = g('dao_user') -> get_by_id($this -> user_id, ' dept_tree ');
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$all_dept = array();
		foreach ($dept_tree as $dept) {
			$all_dept = array_merge($all_dept, $dept);
		}unset($dept);
		$all_dept = array_unique($all_dept);
		return $all_dept;
	}

	/** 获取当前用户的所在分组 */
	private function get_user_group() {
		$ids = array();
		$groups = g('dao_group') -> get_by_user_id($this -> com_id, $this -> user_id, 'id');
		$groups && $ids = array_column($groups, 'id');
		
		return $ids;
	}

}
// end of file