<?php
/**
 * 财务报表全部ajax交互类
 *
 * @author luja
 * @create 2016-08-12
 */

namespace model\finstat\view;

class FinstatAjax extends \model\api\view\ViewBase {

	// 错误提示码
	private $busy = 0;

	private static $Workshift_App_id = 25;

	public function __construct() {
		parent::__construct('finstat');

		// 未知原因（系统繁忙1）
		$this -> busy = \model\api\server\Response::$Busy;
	}


	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				// 获取报表列表
				$this -> get_finstat_list();
				break;
			}
			case 102 : {
				// 获取产品套帐列表
				$this -> get_account_list();
				break;	
			}
			case 103 : {
				//获取第三方服务
				$this -> get_server();
				break;	
			}
			case 104 : {
				//获取我的报表列表
				$this -> get_report();
			}
			case 105 : {
				//获取报表详情
				$this -> get_report_detail();
			}
			case 106 : {
				//获取表格数据
				$this -> get_table_data();
			}
			case 107 : {
				//获取图数据
				$this -> get_chart_data();
			}

            case 201 :{
                //获取表格数据
                $this -> get_table_data_for_process();
                break;
            }

            case 202 :{
                //获取表格数据-头部数据
                $this -> get_th_data_for_process();
                break;
            }

            case 203 :{
                //获取表格数据,支持搜索
                $this -> get_table_data_for_search();
                break;
            }

			default : {
				//非法的请求
				parent::echo_busy();
				break;
			}
		}
	}

	/** 查看excel报表详情 */
	public function report_detail(){
		try{
			//检查权限
			$privilege =  g('ser_finstat') -> check_privilege($com_id);
			if (!$privilege) {
				$vip = parent::get_vip_power(false);
				if ($vip['is_vip'] == 0) {
					throw new \Exception('您还不是VIP，暂时无法使用该功能');
				} elseif ($vip['vip_state'] == 0) {
					throw new \Exception('您的VIP会员已过期，暂时无法使用该功能');
				}
			}
			$id = (int)g('pkg_val') -> get_get('id', FALSE);

			if (empty($id)) throw new \Exception('报表不存在');

			$excel = g('ser_finstat') -> excel_report_detail($id);

			$no_power = FALSE;
			$excel_user = json_decode($excel['visit_user'], TRUE);
			$excel_group = json_decode($excel['visit_group'], TRUE);
			$excel_dept = json_decode($excel['visit_dept'], TRUE);

			if (!$no_power && !empty($excel_user)) {
				if (in_array($this -> user_id, $excel_user)) {
					$no_power = TRUE;
				}
			}

			if (!$no_power && !empty($excel_group)) {
				$groups = array();
				$user_group = g('dao_group') -> get_by_user_id($this -> com_id, $this -> user_id, 'id');
				$user_group && $groups = array_column($user_group, 'id');
				
				$same_group = array_intersect($groups, $excel_group);
				$same_group && $no_power = TRUE;
			}
			
			if (!$no_power && !empty($excel_dept)) {
				$user = g('dao_user') -> get_by_id($this -> user_id, 'dept_tree');
				$dept_tree = json_decode($user['dept_tree'], TRUE);
				$dept_ids = array();
				foreach ($dept_tree as $tree) {
					$dept_ids = array_merge($dept_ids, $tree);
				}unset($tree);
				$dept_ids = array_unique($dept_ids);
				$same_dept = array_intersect($excel_dept, $dept_ids);
				
				$same_dept && $no_power = TRUE;
			}

            if (!$no_power) throw new \Exception('没有权限');

			g('api_media') -> cloud_rename_preview($excel['file_name'], $excel['file_hash'], '', TRUE);

		}catch(\Exception $e){
			g('view_notice') -> error($e -> getMessage());			
		}
	}

    /** 获取表格数据 */
    private function get_table_data_for_search(){
        try{
            $fields = array('table_id');
            $data = parent::get_post_data($fields);
            $table_id = (int)$data['table_id'];
            $start = isset($data['start']) ? (int)$data['start'] : 0;
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
            $count = false;
            $key = (isset($data['key']) && !empty($data['key'])) ? $data['key'] : "";
            $desc = isset($data['order']) ? (int)$data['order'] : 0;

            $other_condition = isset($data['condition'])?$data['condition']:[];
            !is_array($other_condition) && $other_condition = json_decode($other_condition,true);

            if($other_condition){
                $tmp_o_cond = [];
                foreach ($other_condition as $item) {
                    if($item['field_val']!=='')
                        $tmp_o_cond[] = $item;
                }
                $other_condition = $tmp_o_cond;
            }

            $order = array();
            if(!empty($key)){
                $order = array(
                    'key' => $key,
                    'type' => $desc
                );
            }

            $data = g('ser_finstat') -> get_table_data_for_search($table_id, $order, $count, $start, $page_size, $other_condition);

            parent::echo_ok($data);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }

    /** 获取表格数据 */
    private function get_table_data_for_process(){
        try{
            $fields = array('table_id');
            $data = parent::get_post_data($fields);
            $table_id = (int)$data['table_id'];
            $start = isset($data['start']) ? (int)$data['start'] : 0;
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
            $count = empty($data['count']) ? FALSE : TRUE;
            $key = (isset($data['key']) && !empty($data['key'])) ? $data['key'] : "";
            $desc = isset($data['order']) ? (int)$data['order'] : 0;

            $other_condition = isset($data['condition'])?$data['condition']:[];
            !is_array($other_condition) && $other_condition = json_decode($other_condition,true);

            if($other_condition){
                $tmp_o_cond = [];
                foreach ($other_condition as $item) {
                    if($item['field_val']!=='')
                        $tmp_o_cond[] = $item;
                }
                $other_condition = $tmp_o_cond;
            }

            $order = array();
            if(!empty($key)){
                $order = array(
                    'key' => $key,
                    'type' => $desc
                );
            }

            $data = g('ser_finstat') -> get_table_data_for_process($table_id, $order, $count, $start, $page_size, $other_condition);

            if( !$other_condition && isset($data['data']['tbody']) ){
                $data['data']['tbody'] = [];
                $data['data']['tfoot'] = [];
                $data['sum'] = 0;
            }

            parent::echo_ok($data);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }

    /** 获取表格数据 */
    private function get_th_data_for_process(){
        try{
            $fields = array('table_id');
            $data = parent::get_post_data($fields);
            $table_id = (int)$data['table_id'];
            $table = g('api_report') -> get_table_by_id($this -> com_id, $table_id, '*');
            if(empty($table))
                throw new \Exception('报表不存在');

            $return = [];
            $row_list = json_decode($table['row_list'],true)?json_decode($table['row_list'],true):[];

            foreach ($row_list as $item){
                $format = '';
                if($item['type']=='date' || $item['type']=='text'){

                    $key_arr = explode(".", $item['input_key']);
                    if(count($key_arr)==2){
                        $form_id = str_replace('form','',$key_arr[0]);
                        //获取最新版本的内容
                        $input = g('mv_form')->getNewestInput($form_id,$key_arr[1]);
                        $other = json_decode($input['other'],true)?json_decode($input['other'],true):[];
                        $format = isset($other['format'])?$other['format']:'';
                    }
                }
                $return[] = array(
                    'key'=>$item['row_key'],
                    'val'=>$item['name'],
                    'type'=>$item['type'],
                    'format'=>$format,
                );
            }

            parent::echo_ok(array(
                'data'=>$return
            ));
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }

	/** 获取表格数据 */
	private function get_table_data(){
		try{
			$fields = array('table_id');
			$data = parent::get_post_data($fields);
			$table_id = (int)$data['table_id'];
			$start = isset($data['start']) ? (int)$data['start'] : 0;
			$page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
			$count = empty($data['count']) ? FALSE : TRUE;
			$key = (isset($data['key']) && !empty($data['key'])) ? $data['key'] : "";
			$desc = isset($data['order']) ? (int)$data['order'] : 0;

			$order = array();
			if(!empty($key)){
				$order = array(
						'key' => $key,
						'type' => $desc
					);
			}

			$data = g('ser_finstat') -> get_table_data($table_id, $order, $count, $start, $page_size);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取表格数据 */
	private function get_chart_data(){
		try{
			$fields = array('chart_id');
			$data = parent::get_post_data($fields);
			$chart_id = (int)$data['chart_id'];

			$ret = g('ser_finstat') -> get_chart_data($chart_id);
			$data = array(
					'data' => $ret
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取报表列表 */
	private function get_report(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);
			$page = isset($data['page']) ? (int)$data['page'] : 1;
			$page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
			$key_word = isset($data['keywords']) ? $data['keywords'] : "";
			$report = g('ser_finstat') -> get_report_list($key_word, $page, $page_size, TRUE);
			parent::echo_ok($report);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取报表详情 */
	private function get_report_detail(){
		try{
			//检查权限
			$privilege =  g('ser_finstat') -> check_privilege($com_id);
			if (!$privilege) {
				$vip = parent::get_vip_power(false);
				if ($vip['is_vip'] == 0) {
					throw new \Exception('您还不是VIP，暂时无法使用该功能');
				} elseif ($vip['vip_state'] == 0) {
					throw new \Exception('您的VIP会员已过期，暂时无法使用该功能');
				}
			}

			$fields = array();
			$data = parent::get_post_data($fields);
			$report_id = isset($data['report_id']) ? (int)$data['report_id'] : 0;
			$code = isset($data['code']) ? $data['code'] : 0;
			$page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;

			// 模拟测试
			// $data['create_id'] = array(9135450,9135451);
			// $data['create_time'] = '2017-03-25 - 2017-03-30';
			// $data['cond_list'] = array(
			// 	// 单行文本
			// 	array(
			// 		'key' => "form52.input1491364397000", 
			// 		'type' => "dept", 
			// 		'format' => '',
			// 		'is_child' => 0, 
			// 		'value' => array('509676'),
			// 	),
			// );

			// gch add
			$cond = array(
				// 'cond_list' => $data['cond_list']
			);
			// 2017-03-07 00:00 - 2017-04-23 23:00
			if (isset($data['create_time']) && !empty($data['create_time'])) {
				$cond_time = explode(' - ', $data['create_time']);
				if (!isset($c_time[0]) && $c_time[0] === "") {
					throw new \Exception('筛选时间格式违法！');
				}
				
				$cond['create_time'] = $data['create_time'];
			}
			
			// array(9135451)
			if (isset($data['creater_id']) && !empty($data['creater_id'])) {
				if (!is_array($data['creater_id'])) {
					throw new \Exception('提交人id格式违法');
				}
				// 提交人姓名转id
				$tmp_arr = array();
				foreach ($data['creater_id'] as $value) {
					$create_id_arr = g('ser_finstat') -> get_userid_by_names($value, $_SESSION[SESSION_VISIT_COM_ID]);
					foreach ($create_id_arr as $val) {
						isset($val['id']) && !empty($val['id']) && $tmp_arr[] = intval($val['id']);
					}
				}
				$cond['create_id'] = $tmp_arr;
			}

			!isset($data['cond_list']) and $data['cond_list'] = array();
			
			$cond['cond_list'] = array();
			$check_arr = array(
				"radio", "select", "people", "dept"
			);

			// 需要转成user_id的全部人员名称集合
			$user_list = array();
			// 需要转成dept_id的全部部门名称集合
			$dept_list = array();

			// 过滤后的条件集合
			$cond_list = array();
			// 过滤为空的条件，并且收集需要转换成id的用户名、部门名
			foreach ($data['cond_list'] as $value) {
				if (in_array($value["type"], $check_arr)) {
					$tmp_val_arr = array();
					foreach ($value["value"] as $str) {
						$tmp_str = trim($str);
						$tmp_str !== "" and $tmp_val_arr[] = $tmp_str;
					}
					$value["value"] = $tmp_val_arr;
				}

				// 如果值是空，继续循环
				if (empty($value["value"])) {
					continue;
				}

				$cond_list[] = $value;
				// 值非空，且是部门或者人员字段时，需要将多个名称转换为对应的id(user_id、dept_id)
				switch ($value["type"]) {
					case "people" : {
						$tmp_arr = array_flip($value["value"]);
						$user_list = array_merge($user_list, $tmp_arr);
						break;
					}
					case "dept" : {
						$tmp_arr = array_flip($value["value"]);
						$dept_list = array_merge($dept_list, $tmp_arr);
						break;
					}
					default : {}
				}
			}

			// 转换成功的全部人员id集合
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$ex_user_list = g('ser_finstat') -> get_userid_by_names($user_list, $com_id);
			// 转换成功的全部部门id集合
			$root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
			$ex_dept_list = g('ser_finstat') -> get_deptid_by_names($dept_list, $root_id);

			foreach ($cond_list as $k => $v) {
				switch ($v["type"]) {
					case "people" : {
						$cond_list[$k]['value'] = $ex_user_list;
						break;
					}
					case "dept" : {
						$cond_list[$k]['value'] = $ex_dept_list;
						break;
					}
					default : {}
				}
			}
			//注意部门名称不能一样
			$cond['cond_list'] = $cond_list;

			if(empty($report_id) && empty($code)){
			//if(empty($report_id)){
				throw new \Exception('报表不存在');
			}

			if(empty($report_id)){
				//从redis获取即将要预览的报表ID
				$com_id = $_SESSION[SESSION_VISIT_COM_ID];
				$report_id = $this -> get_redis_data($com_id, $code);
				if(empty($report_id)) throw new \Exception('预览已经过期');
 				parent::log_i("id:".$report_id);
				$preview = TRUE;
			}else{
				$preview = FALSE;
			}

			$report = g('ser_finstat') -> get_report_info($report_id, 'id, name, `desc`, type, visit_dept, visit_group, visit_user, has_table, has_chart, create_id, state, info_state', $preview);

			if($report['state'] == 2){
				throw new \Exception('当前报表已被管理员禁用');
			}

			$has_chart = $report['has_chart'];
			$ret = g('ser_finstat') -> get_table_by_report($report_id, 0, $cond, $has_chart, $page_size);

			unset($report['has_chart']);
			unset($report['has_table']);
			unset($report['visit_dept']);
			unset($report['visit_group']);
			unset($report['visit_user']);
			unset($report['create_id']);
			unset($report['type']);
			$report['table_list'] = $ret['table_list'];
			$report['first_data'] = $ret['first_data'];

			$data = array(
					'data' => $report,
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/**
	 * 获取报表列表
	 * @return
	 */
	private function get_finstat_list() {
		try {
			$fields = array('id');
			$data = parent::get_post_data($fields);
			$account_id = (int)$data['id'];

			$page = isset($data['page']) ? intval($data['page']) : 1;

			$page < 1 && $page = 1;
			$page_size = 20;

			parent::log_i('开始获取报表');

			$ret = g('ser_finstat') -> get_list($account_id, $page, $page_size);
			$report_list = array();
			if (!empty($ret['data'])) {
				foreach ($ret['data'] as $val) {
					$val['url'] = MAIN_DYNAMIC_DOMAIN ."index.php?model=finstat&m=ajax&a=get_finstat_detail&f_id=".$val['id'];
					if(!isset($report_list[$val['report_name']])){
						$report_list[$val['report_name']] = array(
								'report_name' => $val['report_name'],
								'list' => array(),
							);
					}
					$report_list[$val['report_name']]['list'][] = $val;
				}unset($val);
			}
			$report_list = array_values($report_list);
			$data = array(
					'data' => $report_list,
					'count' => $ret['count']
				);
			parent::log_i('成功获取报表');
			parent::echo_ok($data);
		} catch (\Exception $e) {
			parent::log_e('获取报表失败，异常【' . $e -> getMessage() .'】');
			parent::echo_exp($e);
		}
	}

	/** 获取套帐 */
	private function get_account_list(){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$account_list = g('ser_finstat') -> get_account_list($com_id);
		$data = array(
				'data' => $account_list
			);
		parent::echo_ok($data);
	}

	/**
	 * 获取润恒报表页面
	 * @return
	 */
	public function get_finstat_detail() {
		try {
			$id = g('pkg_val') -> get_get('f_id', FALSE);
			if (empty($id)) throw new \Exception('文件不存在！');

			$ret = g('ser_finstat') -> get_by_id($id, 'file_path');
			if (!$ret) throw new \Exception('文件不存在！');
			$file_path = $ret['file_path'];
            $file_path = preg_replace("/(.*)(\/data\/runheng\/)(.*)/u", "$2$3", $file_path);
            $file_url = rtrim(QY_DYNAMIC_DOMAIN, '\\/') . $file_path;

            g('pkg_excel') -> export_remote_html($file_url);
//			g('pkg_excel') -> export_html($file_path);
			exit();
		} catch (\Exception $e) {
			g('view_notice') -> error('文件已经被覆盖');
		}
	}
	

	/** 检查是开通润恒功能 */
	private function get_server(){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$runheng = g('ser_finstat') -> check_runheng($com_id);
		$server = array();
		if($runheng){
			$server[] = array(
					'name' => '润衡报表',
					'image' => MAIN_STATIC_DOMAIN. DS . 'data'. DS. 'api' . DS .'upload' . DS .'runheng'. DS. 'logo.png',
					'type' => 'runheng',
				);
		}

		$privilege =  g('ser_finstat') -> check_privilege($com_id);
		if($privilege){
			$server[] = array(
					'name' => '我的报表',
					'icon' => "rulericon-formdata c-blue",
					'type' => 'report',	
				);
		}
		$app_info = g('dao_com_child_app') -> get_by_app_id($com_id, self::$Workshift_App_id, 'state');
		if($app_info['state'] == 1){
			$vip_power = false;
			$app_config = load_config("model/api/appset");
			if ($app_config[self::$Workshift_App_id]['open_time'] < time()) {
				$vip_power = true;
			}

			if (!$vip_power) {
				$vip = parent::get_vip_power(false);
				if ($vip['vip_state'] == 1 || ($vip['is_vip'] == 2 && in_array($app_config[self::$Workshift_App_id]['key'], $vip['app_key']))) {
					$vip_power = true;
				}
			}
			
			$vip_power && $server[] = array(
						'icon' => '',
						'name' => '我的考勤',
						'image' => SYSTEM_MSG_INFO_ICON_PATH. 'Checkwork_icon.png',
						'type' => 'checkwork',	
					);
		}

		// gch add material
		$materialFlag = g('dao_material') -> checkConfig($com_id, $this -> user_id);
		if ($materialFlag) {
			$server[] = array(
				'name' => '物资导入',
				'icon' => "c-red rulericon-upload",
				'type' => 'material',	
			);
		}

		$data = array(
				'server_list' => $server
			);
		parent::echo_ok($data);
	}

	
	/**
	 *  从redis中获取即将要预览的report_id
	 */
	private function get_redis_data($com_id, $code){
		$data_str = g('pkg_redis') -> get($code);
		$data = json_decode($data_str, TRUE);
		if($com_id != $data['com_id']){
			return FALSE;
		}else{
			return $data['id'];
		}
	}

}
// end of file