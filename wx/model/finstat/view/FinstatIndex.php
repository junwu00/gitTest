<?php

namespace model\finstat\view;

class FinstatIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('finstat',$log_pre_str);
	}
	
	//创建计划
	public function index(){
		g('pkg_smarty') -> assign('title', '文件列表');
		g('pkg_smarty') -> show($this -> view_dir . 'page/index.html');
	}

	public function chart(){
		g('pkg_smarty') -> assign('title', '我的报表');
		g('pkg_smarty') -> show($this -> view_dir . 'page/chart.html');
	}
}

//end