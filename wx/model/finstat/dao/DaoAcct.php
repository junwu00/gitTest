<?php 
/**
 * 财务报表数据模型
 *
 * @author chenyihao
 * @create 2016-09-14
 */
namespace model\finstat\dao;

class DaoAcct extends \model\api\dao\DaoBase {

	private static $Table = 'runheng_acct';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	/** 根据com_id获取 */
	public function get_by_com($com_id, $fields = 'id'){
		$cond = array(
				'com_id=' => $com_id
			);
		return $this -> get_by_cond($cond, $fields);
	}

}
// end of file