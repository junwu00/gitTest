<?php 
/**
 * 财务报表数据模型
 *
 * @author chenyihao
 * @create 2016-09-14
 */
namespace model\finstat\dao;

class DaoAccount extends \model\api\dao\DaoBase {

	private static $Table = 'runheng_account';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	public function get_list($com_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id
			);
		return $this -> list_by_cond($cond, $fields);
	}


}
// end of file