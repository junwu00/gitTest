<?php 
/**
 * 财务报表数据模型
 *
 * @author Luja
 * @create 2016-08-12
 */
namespace model\finstat\dao;

class DaoFinstat extends \model\api\dao\DaoBase {


	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * 根据条件获取数据
	 * @param  string  $table      
	 * @param  string  $fileds     
	 * @param  array   $cond       
	 * @param  integer $page       
	 * @param  integer $page_size  
	 * @param  string  $order      
	 * @param  boolean $with_count 是否获取分页前的总数据量
	 * @return [type]              
	 */
	public function get_list_cond($table, $fileds, $cond=array(), $page=0, $page_size=0, $order='', $with_count=FALSE) {
		$ret = g('pkg_db') -> select($table, $fileds, $cond, $page, $page_size,'', $order, $with_count);

		return $ret;
	}


}
// end of file