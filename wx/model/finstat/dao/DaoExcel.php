<?php 
/**
 * excel报表数据模型
 *
 * @author Luja
 * @create 2016-10-26
 */
namespace model\finstat\dao;

class DaoExcel extends \model\api\dao\DaoBase {

	/** excel文件信息表 */
	private static $Table = 'mv_proc_report_file';
	/** 报表信息表 */
	private static $TableConf = 'mv_proc_report_conf';

	/** 正常 */
	private static $InfoOn = 1;
	/** 删除 */
	private static $InfoDel = 0;

	/** 启用 1 */
	private static $StateOn = 1;
	/** 禁用 2 */
	private static $StateOff = 2;

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * 根据条件获取数据
	 * @return [type]              
	 */
	public function get_excel_by_id($id, $com_id, $fileds='mprf.*') {
		$table = self::$Table . ' mprf, '. self::$TableConf . ' mprc';
		$cond = array(
			'mprc.id='			=> $id,
			'mprf.com_id=' 		=> $com_id,
			'^mprf.report_id=' 	=> 'mprc.id'
			);
		$ret = g('pkg_db') -> select($table, $fileds, $cond);

		return $ret ? $ret[0] : FALSE;
	}


}
// end of file