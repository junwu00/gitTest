<?php
/**
 * 通讯录应用数据模型
 *
 * @author LiangJianMing
 * @create 2015-06-26
 */

namespace model\api\dao;

class Contact extends \model\api\dao\DaoBase {

    /**
     * 获取虚拟结构的权限条件
     */
    public function get_struct_condition() {
        $ret = array();
        
        $dept_ids = g('dao_struct') -> get_permission_dept();
        if (empty($dept_ids)) {
            return $ret;
        }

        // 使用正则表达式
        $ret['dept_tree REGEXP '] = '"(' . implode('|', $dept_ids) . ')"';

        return $ret;
    }

    /**
     * 获取通讯录配置
     */
    public function get_contact_setting() {
        $conf = g('cont_conf') -> get_by_dept($this -> root_id);
        $conf['search_list'] = json_decode($conf['search_list'], TRUE);
        !is_array($conf['search_list']) and $conf['search_list'] = array();

        $conf['show_list'] = json_decode($conf['show_list'], TRUE);
        !is_array($conf['show_list']) and $conf['show_list'] = array();

        $conf['secret_list'] = json_decode($conf['secret_list'], TRUE);
        !is_array($conf['secret_list']) and $conf['secret_list'] = array();

        $conf['general_list'] = json_decode($conf['general_list'], TRUE);
        !is_array($conf['general_list']) and $conf['general_list'] = array();

        return $conf;
    }

}

/* End of this file */