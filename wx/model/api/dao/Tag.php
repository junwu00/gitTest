<?php
/**
 * 表单标签操作类
 * @author yangpz
 * @date 2016-06-28
 *
 */
namespace model\api\dao;

class Tag extends \model\api\dao\DaoBase {
	
	/** 对应的库表 */
	public static $TABLE = 'mv_proc_tag';
	public static $TABLE_MAP = 'mv_proc_material_tag_map';
	public static $TABLE_MATERIAL = 'mv_proc_material_model';
	
	/** 删除/禁用 0 */
	private static $STATE_OFF 	= 0;
	/** 启用 1 */
	private static $STATE_ON 	= 1;
	
	public function __construct($conf=array()) {
		$this -> set_table(self::$TABLE);
	}
	
	/**
	* 根据标签名获取标签,若不存在则新建
	*   
	* @access public
	* @param int com_id  
	* @param string  
	* @return   
	*/
	public function get_tag_by_name($com_id, $name, $type = 1){
		$tag_info = array();
		if (empty($name)) {
			return $tag_info;
		}

		$fields = array('id', '`name`');
		$fields = implode(',', $fields);

		$cond = array(
				"com_id=" => $com_id,
				"name=" => $name,
				"info_state=" => self::$STATE_ON,
				"type=" => $type,
			);

		$tag_row = $this -> get_by_cond($cond, $fields);
		if (empty($tag_row)) {
			$save_data = array(
				'update_time' => time(),
				'create_time' => time(),
				'info_state' => self::$STATE_ON,
				'com_id' => $com_id,
				'name' => $name,
				'type' => $type,
			);
			$tag_id = $this -> insert($save_data);
			
			$tag_info = array(
				'id' => $tag_id,
				'name' => $name,
			);
		} else {
			$tag_info = $tag_row;
		}

		return $tag_info;
		
	}

	/**
	* 根据标签名进行批量插入
	*   
	* @access public
	* @param int com_id  
	* @param array names  标签名
	* @return   
	*/
	public function batch_insert_tag($com_id, $admin_id, $admin_name, $tag_names, $type = 1){
		$return = FALSE;
		if (empty($tag_names)) {
			return $return;
		}

		$save_data = array();
		$base_data = array(
			'update_time' => time(),
			'create_time' => time(),
			'info_state' => self::$STATE_ON,
			'com_id' => $com_id,
			'type' => $type,
			'creater_name' => $admin_name,
			'creater_id' => $admin_id,
		);

		foreach ($tag_names as $tag_name) {
			$tmp_data = array(
				'name' => $tag_name
			);
			$save_data[] = array_merge($base_data, $tmp_data);
		}
		$fields = array_keys($save_data[0]);

		$return = g('pkg_db') -> batch_insert(self::$TABLE, $fields, $save_data);
		
		return $return;
		
	}

	/**
	 * 根据标签id获取物资
	 * @param int $com_id	企业根标签ID
	 * @param int $tag_ids	标签ID
	 * @param string $fields	查询的字段
	 * 
	 * @return array 标签信息（数组），找不到则返回空数组
	 */
	public function get_data_by_tag_id($com_id, $tag_ids, $external_cond = array(), $tag_fields = array(), $material_fields = array(), $page= 0, $page_size = 0, $order='', $group = '', $with_cnt = FALSE) {
		$return = array();

		$table = self::$TABLE_MAP . ' as map left join ' . self::$TABLE_MATERIAL . ' as t1 on map.material_id=t1.id left join ' . self::$TABLE . ' as t2 on map.tag_id=t2.id ';

		$fields = array();
		if (!empty($tag_fields)) {
			foreach ($tag_fields as $f) {
				$fields[] = 't2.' . $f;
			}
			unset($f);
		}
		if (!empty($material_fields)) {
			foreach ($material_fields as $f) {
				$fields[] = 't1.' . $f;
			}
			unset($f);
		}
		if (empty($fields)) {
			$fields = array(
				't1.id',
				't1.name',
			);
		}
		is_array($fields) && $fields = implode(',', $fields);

		$cond = array(
			'map.tag_id IN'		=> $tag_ids,
			'map.com_id='		=> $com_id,
			't1.com_id='		=> $com_id,
			'map.info_state='	=> self::$STATE_ON,
		);
		$cond = array_merge($cond, $external_cond);
		$return = g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, $group, $order, $with_cnt);
		if (empty($return)) {
			if ($with_cnt) {
				$return  = array(
					'data' => array(),
					'count' => 0,
				);
			} else {
				$return = array();
			}
		}

		return $return;
	}

	/**
	 * 根据物资表id获取标签
	 * @param int $com_id	企业根标签ID
	 * @param int $tag_ids	标签ID
	 * @param string $fields	查询的字段
	 * 
	 * @return array 标签信息（数组），找不到则返回空数组
	 */
	public function getTagByMaterialId($com_id, $materialIds) {
		$return = array();

		$table = self::$TABLE_MAP . ' as t1 left join ' . self::$TABLE . ' as t2 on t1.tag_id=t2.id ';

		$fields = array(
			"t2.`id`", "t2.`name`", "t1.`material_id`",
		);
		$fields = implode(',', $fields);

		$cond = array(
			't1.material_id IN'	=> $materialIds,
			't1.com_id='		=> $com_id,
			't2.com_id='		=> $com_id,
			't1.info_state='	=> self::$STATE_ON,
			't2.info_state='	=> self::$STATE_ON,
		);
		$return = g('pkg_db') -> select($table, $fields, $cond);
		!is_array($return) && $return = array();

		return $return;
	}

	/**
	 * 根据搜索条件获取标签
	 * @param int $com_id	企业根标签ID
	 * @param int $tag_ids	标签ID
	 * @param string $fields	查询的字段
	 * 
	 * @return array
	 */
	public function list_data_by_cond($com_id, $cond = array(), $fields = array(), $page= 0, $page_size = 0, $order='', $group = '', $type = 1, $withCnt = true) {
		$return = array(
			'data' => array(),
			'count' => 0,
		);

		if (empty($fields)) {
			return $return;
		}
		
		$fields = implode(',', $fields);

		$base_cond = array(
			'type='	=> $type,
			'com_id='	=> $com_id,
			'info_state='	=> self::$STATE_ON,
		);
		$cond = array_merge($base_cond, $cond);
		$return = $this -> list_by_page($cond, $fields, $page, $page_size, $order, $withCnt);

		return $return;
	}

	/**
	 * 更新标签
	 * @param int $type	1-物资标签映射表
	 * 
	 * @return array
	 */
	public function update_tag($com_id, $ids, $udata) {
		$cond = array(
			'id IN'	=> $ids,
			'com_id='	=> $com_id,
			'info_state='	=> self::$STATE_ON,
		);

		$baseData = array(
			'update_time'	=> time(),
		);
		$data = array_merge($baseData, $udata);

		$return = g('pkg_db') -> update(self::$TABLE, $cond, $data);
		return $return;
	}

	/**
	 * 删除标签映射关系
	 * @param int $type	1-物资标签映射表
	 * 
	 * @return array
	 */
	public function delete_map_by_ids($com_id, $ids, $mid) {
		$cond = array(
			'tag_id IN'	=> $ids,
			'com_id='	=> $com_id,
			'info_state='	=> self::$STATE_ON,
			'material_id=' => $mid,
		);

		$data = array(
			'info_state'	=> self::$STATE_OFF,
		);

		$return = g('pkg_db') -> update(self::$TABLE_MAP, $cond, $data);
		return $return;
	}

	/**
	 * 添加标签和物资的映射
	 * @param int $com_id	企业根标签ID
	 * @param int $form_id	表单id
	 * @param array $tag_id	标签ID
	 * 
	 * @return array 标签信息（数组），找不到则返回空数组
	 */
	public function insert_material_tag_map($com_id, $material_id, $tag_ids) {
		$return = FALSE;
		if (empty($tag_ids)) {
			return $return;
		}

		$save_data = array();
		$base_data = array(
			'com_id' => $com_id,
			'material_id' => $material_id,
			'create_time' => time(),
			'update_time' => time(),
			'info_state' => self::$STATE_ON,
		);

		foreach ($tag_ids as $tag_id) {
			$tmp_data = array(
				'tag_id' => $tag_id
			);
			$save_data[] = array_merge($base_data, $tmp_data);
		}
		$fields = array_keys($save_data[0]);

		$return = g('pkg_db') -> batch_insert(self::$TABLE_MAP, $fields, $save_data);
		return $return;
	}

}

//end