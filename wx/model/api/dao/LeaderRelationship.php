<?php
/**
 * 上下级关系查询类
 * @author yangpz
 * @date 2015-12-22
 *
 */
namespace model\api\dao;

class LeaderRelationship extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_leader_relationship';
	
	/** 删除 0 */
	public static $StateDel = 0;
	/** 启用 1 */
	public static $StateOn = 1;
	
	/** 下级类型为成员 1 */
	public static $TypeUser = 1;
	/** 下级类型为部门 2 */
	public static $TypeDept = 2;
	
	/** 组织架构过滤 */
	private $struct_filter = FALSE;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	
	/**
	 * 设置组织架构配置
	 * @param unknown_type $open_filter	是否开启过滤
	 * @param array $conf				过滤配置信息
	 */
	public function set_struct($open_filter=TRUE, array $conf=array()) {
		$this -> struct_filter = $open_filter;
		parent::set_conf($conf);
	}

    /** 获取上下级关系 type 0：上下级， 1：下级 2：上级 */
    public function get_relationship($com_id, $user_id, $type = 0, $fields='*'){

        $user = g('dao_user') -> get_by_id($user_id, 'dept_list');
        if(empty($user)) return array();

        $dept_list = json_decode($user['dept_list'], true);
        $main_dept_id = array_shift($dept_list);

        $cond = array();
        $cond['com_id='] = $com_id;
        $cond['info_state='] = self::$StateOn;

        if($type == 0){
            $cond['__OR'] = array();
            $cond['__OR']['sup_user_id='] = $user_id;
            $cond['__OR'][] = array(
                'sub_type=' => self::$TypeUser,
                'sub_id=' => $user_id,
            );
            if($main_dept_id){
                $cond['__OR'][] = array(
                    'sub_type=' => self::$TypeDept,
                    'sub_id=' => $main_dept_id,
                );
            }
        }else if($type == 1){
            $cond['sup_user_id='] = $user_id;
        }else{
            $cond['__OR'] = array();
            $cond['__OR'][] = array(
                'sub_type=' => self::$TypeDept,
                'sub_id=' => $main_dept_id,
            );
            $cond['__OR'][] = array(
                'sub_id=' => $user_id,
                'sub_type=' => self::$Type_User,
            );
        }
        $ret = $this -> list_by_cond($cond, $fields);

        return $this -> filter_rela($ret, $user_id);
    }

    /** 过滤互为上下级的关系 */
    private function filter_rela($rela_list=array(), $user_id) {
        if (empty($rela_list)) return array();

        $leader_ids = array();
        $sub_ids = array();
        $sub_depts = array();
        foreach ($rela_list as $key => $rela) {
            array_push($leader_ids, $rela['sup_user_id']);
            if ($rela['sub_type'] == 1) {
                array_push($sub_ids, $rela['sub_id']);
            } elseif ($rela['sub_type'] == 2 && $rela['sup_user_id'] == $user_id) {
                array_push($sub_depts, $rela['sub_id']);
            }
        }unset($rela);

        $same = array_intersect($leader_ids, $sub_ids);
        $same = array_diff($same, array($user_id));
        if (!empty($same)) {
            //人员领导关系和部门领导关系有冲突，优先保留人员领导关系
            foreach ($rela_list as $key => &$rela) {
                if (in_array($rela['sup_user_id'], $same) && $rela['sub_type'] == 2) {
                    unset($rela_list[$key]);
                }
            }unset($rela);
        }

        if (!empty($sub_depts)) {
            //过滤掉自己管辖的部门的其他部门管辖者（同级关系）
            foreach ($rela_list as $key => &$rela) {
                if ($rela['sup_user_id'] != $user_id && in_array($rela['sub_id'], $sub_depts) && $rela['sub_type'] == 2) {
                    unset($rela_list[$key]);
                }
            }unset($rela);
        }

        return array_values($rela_list);
    }
	
	//GET-----------------------------------------------------------------------------------------------

	/**
	 * 根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	public function get_by_cond(array $cond, $fields='*') {
		if (!isset($cond['com_id=']) || empty($cond['com_id='])) {
			throw new \Exception('未指定企业ID');
		}
		return parent::get_by_cond($cond, $fields);
	}
	
	//LIST-----------------------------------------------------------------------------------------------
	
	/**
	 * (批量)根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	public function list_by_cond(array $cond, $fields='*') {
		if (!isset($cond['com_id=']) || empty($cond['com_id='])) {
			throw new \Exception('未指定企业ID');
		}
		
		return parent::list_by_cond($cond, $fields);
	}
	
	/**
	 * 获取指定部门的所有领导
	 * @param unknown_type $dept_id		部门ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 */
	public function list_leader_by_dept_id($dept_id, $only_id=TRUE) {
		$cond = array(
			'com_id=' 		=> $this -> com_id,
			'sub_id=' 		=> $dept_id,
			'info_state=' 	=> self::$StateOn,
			'sub_type=' 	=> self::$TypeDept,
		);
		$ret = $this -> list_by_cond($cond, 'sup_user_id');

		$user_ids = array();
		foreach ($ret as $item) {
			$user_ids[] = $item['sup_user_id'];
		}unset($item);
		$dept_ids = array();
		
		$api_user = $this -> _get_api_user_instance();
		$users = $api_user -> list_by_ids($user_ids, '*');
		if ($only_id) {											//仅ID（由于成员可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			return $user_ids;
		}
		return $users;
	}
	
	/**
	 * 获取指定成员的所有领导（只取主部门上级+成员上级）
	 * @param unknown_type $user_id		用户ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 * @param unknown_type $user_fields	查找的字段（成员）
	 */
	public function list_leader_by_user_id($user_id, $only_id=TRUE, $user_fields='id,name,pic_url') {
		$api_user = $this -> _get_api_user_instance();
		$dept_id = $api_user -> get_main_dept($user_id, TRUE);
		//1.查看该成员是否为某些部门上级，是的话剔除所有这些部门的上级（同部门上级之间不能互为上级，如需要为上下级关系，可增加人与人关联）
		$fields = '*';
		$cond = array(
			'com_id=' 		=> $this -> com_id,
			'sup_user_id=' 	=> $user_id,
			'sub_type=' 	=> self::$TypeDept,
			'info_state=' 	=> self::$StateOn
		);
		$ret = $this -> list_by_cond($cond, $fields);
		$leader_dept_ids = array(0);		//为哪些部门的上级
		if (!empty($ret)) {
			foreach ($ret as $item) {
				$leader_dept_ids[] = $item['sub_id'];
			}unset($item);
			unset($ret);
		}
		
		//2.获取成员所在部门的上级（这些也为成员上级） + 直接配置的成员的上组
		
		$cond = array(
			'com_id='		=> $this -> com_id,
			'info_state='	=> self::$StateOn,
			'__OR_10'		=> array(
				'__OR_11' 	=> array(
					'sub_type='		=> self::$TypeDept,
					'sub_id='		=> $dept_id,
					'sub_id NOT IN'	=> $leader_dept_ids,
				),
				'__OR_12' 	=> array(
					'sub_type='		=> self::$TypeUser,
					'sub_id='		=> $user_id,
				),
			),
		);
		$ret = $this -> list_by_cond($cond, ' DISTINCT(sup_user_id) ');
		
		$user_ids = array();
		foreach ($ret as $item) {
			$user_ids[] = $item['sup_user_id'];
		}unset($item);
		unset($ret);
		$dept_ids = array();
		
		$api_user = $this -> _get_api_user_instance();
		$users = $api_user -> list_by_ids($user_ids, $user_fields, FALSE);	//上面已进行架构过滤，此处不再过滤
		if ($only_id) {														//仅ID（由于成员可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			return $user_ids;
		}	
		return $users; 
	}
	
	/**
	 * 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用）
	 * @param unknown_type $user_id			成员ID
	 * @param unknown_type $only_ids		仅返回成员/部门ID 或 返回成员/部门信息，默认返回ID
	 * @param unknown_type $dept_fields		部门查找的字段
	 * @param unknown_type $user_fields		成员查找的字段
	 */
	public function list_sub_by_user_id($user_id, $only_ids=TRUE, $dept_fields='id,name', $user_fields='id,name,pic_url') {
		$fields='*';
		$cond = array(
			'com_id=' => $this -> com_id,
			'sup_user_id=' => $user_id,
			'info_state=' => self::$StateOn,
		);
		$ret = $this -> list_by_cond($cond, $fields);
		//没有记录
		if (empty($ret))	return array('users' => array(), 'depts' => array());
		//分离部门/成员记录
		$dept_ids = array();
		$user_ids = array();
		foreach ($ret as $item) {
			if ($item['sub_type'] == self::$TypeDept) {
				$dept_ids[] = $item['sub_id'];
			} else if ($item['sub_type'] == self::$TypeUser) {
				$user_ids[] = $item['sub_id'];
			}
		}unset($item);
		$dept_ids = array_unique($dept_ids);
		$user_ids = array_unique($user_ids);
		
		$api_user = $this -> _get_api_user_instance();
		$api_dept = $this -> _get_api_dept_instance();
		$users = $api_user -> list_by_ids($user_ids, $user_fields, FALSE);	//上面的ID已过滤，此处不再过滤
		$depts = $api_dept -> list_by_ids($dept_ids, $dept_fields, FALSE);	//上面的ID已过滤，此处不再过滤
		if ($only_ids) {													//仅ID（由于成员/部门可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			$dept_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			foreach ($depts as $d) {
				$dept_ids[] = $d['id'];
			}unset($d);
			return array('users' => $user_ids, 'depts' => $dept_ids);
		}		
		return array('users' => $users, 'depts' => $depts);
	}

	//内部实现===============================================================================

	/**
	 * 根据当前信息，获取成员数据库操作类
	 */
	private function _get_api_user_instance() {
		$api_user = g('dao_user');
		$conf = array(
			'com_id'	=> $this -> com_id,
			'root_id'	=> $this -> root_id,
			'user_id'	=> $this -> user_id,
		);
		$api_user -> set_struct($this -> struct_filter, $conf);
		return $api_user;
	}
	
	/**
	 * 根据当前信息，获取部门数据库操作类
	 */
	private function _get_api_dept_instance() {
		$instance = g('dao_dept');
		$conf = array(
			'com_id'	=> $this -> com_id,
			'root_id'	=> $this -> root_id,
			'user_id'	=> $this -> user_id
		);
		$instance -> set_struct($this -> struct_filter, $conf);
		return $instance;
	}
}

//end