<?php
/**
 * 消息的数据库操作类
 * @author chenyihao
 * @create 2016-08-10
 */
namespace model\api\dao;

class News extends \model\api\dao\DaoBase {
	
	private static $Table = 'sc_news';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		parent::set_table(self::$Table);
	}

	/**
	 * 根据条件获取信息
	 * @param  $table  
	 * @param  string $fields 
	 * @param  array  $cond   
	 * @return 
	 */
	public function get_by_cond($table, $fields='', $cond=array()) {
		if (empty($table) || empty($fields) || empty($cond)) return FALSE;

		return g('pkg_db') -> select($table, $fields, $cond);
	}

	/** 根据ID数组获取消息 */
	public function get_by_ids($ids, $fields = '*'){
		$cond = array(
				'id IN' => $ids,
			);
		$order = 'order by success_time desc';
		return $this -> list_by_page($cond, $fields, 0, 0, $order);
	}

	/** 搜索消息 */
	public function search_news($user_id, $key_word, $page, $page_size){
		$cond = array(
				'news_map.user_id=' => $user_id,
				"__OR" => array(
						'news.title LIKE' => "%".$key_word."%",
						'news.content LIKE' => "%".$key_word."%",
					),
			);
		$fields = 'news.*';
		$table = ' sc_news news LEFT JOIN sc_news_user_map news_map ON news.id = news_map.news_id ';
		return  g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, '', '', true);
	}

	/** 根据messageId来获取消息 */
	public function get_by_message_ids($message_ids, $fields = '*'){
		$cond = array(
				'msg_id IN' => $message_ids,
			);
		return $this -> list_by_cond($cond, $fields);
	}

	/** 保存信息 */
	public function insert_news($data){
		return $this -> insert($data, "", FALSE);
	}

}

/* End of this file */