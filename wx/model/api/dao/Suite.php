<?php
/**
 * 套件
 * @author yangpz
 * @date 2016-02-19
 */

namespace model\api\dao;

class Suite extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_suite';

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	
	/**
	 * 通过suite_id获取套件信息
	 * @param unknown_type $suite_id	套件ID
	 */
	public function get_by_suite_id($suite_id, $fields='*'){
		$cond = array(
			'suite_id='	=> $suite_id 
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
	
	/**
	 * 通过id获取套件信息
	 * @param unknown_type $id	套件记录ID
	 */
	public function get_by_id($id, $fields='*'){
		$cond = array(
			'id='	=> $id 
		);
		return $this -> get_by_cond($cond, $fields);
	}

	/**
	 * 通过token获取suite信息
	 * @param unknown_type $token	套件Token
	 * @param unknown_type $fidles	查询字段
	 */
	public function get_by_token($token, $fields='*'){
		$cond = array(
			'token='	=> $token 
		);
		return $this -> get_by_cond($cond, $fields);
	}

}

// end of file