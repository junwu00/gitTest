<?php
/**
 * 管理员服务类
 * @author chenyihao
 * @date 2016-06-24
 *
 */
namespace model\api\dao;

class Admin extends \model\api\dao\DaoBase {

	/** 对应的库表名称 */
	private static $Table = 'sc_admin';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	public function __construct(){
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	/** 检查是否是管理员 */
	public function check_admin($com_id, $user_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id,
				'user_id='  => $user_id,
				'state=' => self::$StateOn
			);
		return $this -> get_by_cond($cond, $fields);
	}

//================================内部实现============================


}

// end of file