<?php
/**
 * vip功能
 * @author Luja
 * @date 2016-10-18
 *
 */
namespace model\api\dao;

class FormAnalysisReport extends \model\api\dao\DaoBase {
	
	/** 企业表单配置表 */
	private static $Table = 'sc_form_analysis_report';
    private static $TableDetail = 'sc_form_analysis_report_detail';
    private static $TableSituation = 'sc_form_analysis_report_situation';


	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}


    /**
     * 获取全局周报表记录
     * @param $cond
     * @return void
     */
    public function getReport($com_id,$id,$fields='rd.*'){
        $table = self::$Table.' ar 
        left join '.self::$TableDetail.' rd on rd.analysis_id = ar.id
        left join sc_company sc on sc.id= rd.com_id';

        $cond = array(
            'ar.id='=>$id,
        );

        if($com_id)
            $cond['rd.com_id='] = $com_id;

        $order_by = ' rd.instance_count desc ';
        $data = g('pkg_db')->select($table,$fields,$cond,0,0,'',$order_by);
        return $data?$data:[];
    }

    /**
     * 获取全局周报表记录
     * @param $cond
     * @return void
     */
    public function getReportByBeginDay($com_id,$begin_day,$fields='rd.*'){
        $table = self::$Table.' ar 
        left join '.self::$TableDetail.' rd on rd.analysis_id = ar.id
        left join sc_company sc on sc.id= rd.com_id';

        $cond = array(
            'ar.begin_day='=>$begin_day,
        );

        if($com_id)
            $cond['rd.com_id='] = $com_id;

        $order_by = ' rd.instance_count desc';
        $data = g('pkg_db')->select($table,$fields,$cond,0,0,'',$order_by);
        return $data?$data:[];
    }

    //获取表单实例使用情况
    public function getAppCountBeginDay($query_com_id,$begin_day,$fields='*'){
        $table = self::$TableSituation.' rd 
        left join '.self::$Table.' ar on rd.analysis_id = ar.id ';

        $cond = array(
            'ar.begin_day='=>$begin_day,
            'rd.usage_count!='=>0,
            'rd.instance!='=>0
        );

        if($query_com_id)
            $cond['rd.com_id='] = $query_com_id;

        $order_by = ' rd.instance desc ';
        $ret = g('pkg_db')->select($table,$fields,$cond,0,0,'',$order_by);
        return $ret?$ret:[];

    }

}
//end of file