<?php
/**
 * 企业成员
 * 
 * @author yangpz
 * @date 2014-10-22
 *
 */
class User_general {
	
	private static $Table = 'sc_user_general';
	
	/**
	 * 根据id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_ids($uid, $fields='*') {
		if(empty($uid)){
			return array();
		}
		
		$cond = array();
		if(is_array($uid)){
			$cond['user_id IN'] = $uid;
		}else{
			$cond['user_id IN'] = $uid;
		}

		return g('pkg_db') -> select(self::$Table, $fields, $cond);
	}

	/**
	 * 更新
	 * @param unknown_type $condition  array 条件
	 * @param unknown_type $data	更新哪些字段
	 */
	public function update($id, $general_list=array(), $del_list=array()) {
		$general_list = array_values($general_list);
		$del_list = array_values($del_list);
		$condition = array(
			'id=' => $id,
		);
		$data = array(	
			'general_list' => json_encode($general_list),
			'del_list' => json_encode($del_list),
		);
	    $result = g('pkg_db') -> update(self::$Table, $condition, $data);
	    if(!$result) {
		    throw new SCException('更新常用联系人失败');
	    }
	    return $result;
	}
	/**
	 * 插入个人常用联系人
	 * @param unknown_type $dept_id		企业对应的dept_id
	 * @param unknown_type $user_info	用户数据
	 * @return 信息不会或插入失败：FALSE；成功：用户ID
	 */
	public function save($id, $general_list=array(), $del_list=array()) {
		$general_list = array_values($general_list);
		$del_list = array_values($del_list);
		$data = array(
			'user_id' => $id,
			'general_list' => json_encode($general_list),
			'del_list' => json_encode($del_list),
		);
		
	    $result =  g('pkg_db') -> insert(self::$Table, $data);
	    if(!$result) {
		    throw new SCException('保存常用联系人失败');
	    }
	    return $result;
	}
}

// end of file