<?php
/**
 * 通讯录应用数据模型
 *
 * @author LiangJianMing
 * @create 2015-06-26
 */

namespace model\api\dao;

class ProcessSchoolContact extends \model\api\dao\DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'school_role';
    private static $Ref_Table = 'school_role_ref';
    private static $Ref_Val_Table = 'school_role_value';

    /** 禁用0 */
    public static $StateOff = 0;
    /** 启用1 */
    public static $StateOn = 1;

    const TYPE_USER = 'user';
    const TYPE_DEPT = 'dept';

    /**
     * 通过学校和角色获取对应人员部门配置
     * @param $parent_unit_id
     * @param $school_com_id
     * @param $role_ids
     * @return void
     */
    public function getBySIdAndRIdsWithRole($parent_unit_id,$school_com_id,$role_ids=[]){
        !is_array($role_ids) && $role_ids=json_decode($role_ids,true);
        $table = self::$Ref_Val_Table.'  refv 
                left join '.self::$Ref_Table.' ref on refv.ref_id=ref.id
                left join '.self::$Table.' r on r.id=refv.role_id';
        $cond = array(
            'refv.info_state='=>1,
            'ref.info_state='=>1,
            'ref.parent_unit_id='=>$parent_unit_id,
            'ref.school_id='=>$school_com_id,
        );

        if($role_ids){
            $cond['refv.role_id IN'] = $role_ids;
        }

        $fields = 'refv.role_id,refv.type,refv.value,r.name role_name';
        $ret = g('pkg_db')->select($table,$fields,$cond);
        return $ret?$ret:[];
    }

    /**
     * 通过学校和角色获取对应人员部门配置
     * @param $parent_unit_id
     * @param $school_com_id
     * @param $role_ids
     * @return void
     */
    public function getBySIdAndRIds($parent_unit_id,$school_com_id,$role_ids=[],$fields = 'refv.role_id,refv.type,refv.value'){
        !is_array($role_ids) && $role_ids=json_decode($role_ids,true);
        $table = self::$Ref_Val_Table.'  refv left join '.self::$Ref_Table.' ref on refv.ref_id=ref.id';
        $cond = array(
            'refv.info_state='=>1,
            'ref.info_state='=>1,
            'ref.parent_unit_id='=>$parent_unit_id,
            'ref.school_id='=>$school_com_id,
        );

        if($role_ids){
            $cond['refv.role_id IN'] = $role_ids;
        }

        $ret = g('pkg_db')->select($table,$fields,$cond);
        return $ret?$ret:[];
    }

    /**
     * 获取学校对应角色人员列表
     * @param $school_com_id
     * @param $role_ids
     * @return array
     */
    public function listUsersBySIdAndRids($school_com_id,$role_ids=[]){
        !is_array($role_ids) && $role_ids=json_decode($role_ids,true);
        $table = self::$Ref_Val_Table.'  refv left join '.self::$Ref_Table.' ref on refv.ref_id=ref.id';
        $cond = array(
            'refv.info_state='=>1,
            'ref.info_state='=>1,
            'ref.school_id='=>$school_com_id,
        );

        if($role_ids){
            $cond['refv.role_id IN'] = $role_ids;
        }

        //返回的人员列表
        $user_list = [];

        $fields = 'refv.role_id,refv.type,refv.value';
        $ret = g('pkg_db')->select($table,$fields,$cond);

        if($ret){
            $user_ids = [];
            foreach ($ret as $item){
                $value = json_decode($item['value'],true);
                switch ($item['type']){
                    case 'user':
                        $user_ids = array_merge($user_ids,$value);
                        break;
                    case 'dept':
                        $users = g('dao_user')->get_by_depts('',$value,'id,name');
                        $user_ids = array_merge($user_ids,array_column($users,'id'));
                        break;
                    case 'tag':
                        $groups = g('dao_group')->get_by_ids('',$value,'id,user_list');
                        if($groups){
                            foreach ($groups as $g){
                                $tmp_user_list = json_decode($g['user_list'],true);
                                $user_ids = array_merge($user_ids,$tmp_user_list);
                            }unset($g);
                        }
                        break;
                }
            }unset($item);

            if($user_ids){
                $user_ids = array_unique($user_ids);
                $user_list = g('dao_user')->get_by_ids('',$user_ids,'id,name');
            }
        }

        return $user_list;
    }

}

/* End of this file */