<?php
/**
 * vip功能
 * @author Luja
 * @date 2016-10-18
 *
 */
namespace model\api\dao;

class ComForm extends \model\api\dao\DaoBase {
	
	/** 企业表单配置表 */
	private static $Table = 'sc_com_form';
    // 物资入库
	const TYPE_MATERIAL_WAREHOUSING = 1;
    // 物资领用
    const TYPE_MATERIAL_APPLY = 2;
    // 物资采购
    const TYPE_MATERIAL_PURCHASE = 3;
    // 撤销流程
    const TYPE_REVOKE = 4;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	public function getRevokePublicFormId($com_id)
    {
        // 获取撤销流程表单id
        $form = $this->getByComId($com_id,ComForm::TYPE_REVOKE);
        if (!$form) {
            return false;
        }
        $public_form = g('mv_form') -> get_by_his_id($form['form_history_id'], $com_id);
        return $public_form['id'];
    }
	/**
     * @throws
     */
    public function getByComId($com_id,$type)
    {
        return $this->get_by_cond([
            'com_id='=>$com_id,
            'type='=>$type,
            'state='=>1
        ]);
    }
}
//end of file