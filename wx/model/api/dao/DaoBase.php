<?php
/**
 * 数据库操作类基础类
 * 主要用于初始化配置
 * 
 * @author yangpz
 * @date 2016-04-08
 * 
 */

namespace model\api\dao;

use package\db\pdo\Mysql;

abstract class DaoBase {
	/** 企业ID */
	protected $com_id = 0;
	/** 企业根部门ID */
	protected $root_id = 0;
	/** 成员ID */
	protected $user_id = 0;
	/** 调用企业号接口的Access Token */
	protected $atoken = '';
	
	/** 缓存key前缀 */
	protected $cache_key_prefix;
	
	/** 当前操作的库表 */
	private $table;

    /** 通用方法參數 */
    public $InfoStateOn = 1;
    public $InfoStateOff = 0;

    /** @var Mysql */
    protected $db;

	public function __construct() {
		$this -> cache_key_prefix = MAIN_HOST;
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;
		$this -> root_id = isset($_SESSION[SESSION_VISIT_DEPT_ID]) ? $_SESSION[SESSION_VISIT_DEPT_ID] : 0;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : 0;
		$this->db = new Mysql();
	}

    ##########   需要加解密可重写desEncodeData和desDecodeData   ##########
    /** 加密字段 */
    protected function desEncodeData(&$data)
    {
        return $data;
    }
    /** 加密字段 */
    protected function desDecodeData(&$data)
    {
        return $data;
    }
    /** 加解密数据 */
    public function desData(&$data, $isEncode = true, $isArray = false)
    {
        if (!$data) {
            return;
        }
        $method = $isEncode ? 'desEncodeData' : 'desDecodeData';
        if (!$isArray) {
            $this->$method($data);
            return;
        }
        foreach ($data as &$datum) {
            $this->$method($datum);
        }unset($datum);
    }
    protected function db_select_one($fields,$cond)
    {
        $data = $this->db->select_one($this->table,$fields,$cond);
        $this->desData($data, false);
        return $data;
    }
    protected function db_select($fields, array $cond=array(), $page=0, $page_size=0, $group_by='', $order_by='', $with_count=FALSE)
    {
        $data = $this->db->select($this->table, $fields, $cond, $page, $page_size, $group_by, $order_by, $with_count);
        if (!$data) {
            $data = $with_count ? ['data'=>[],"count"=>0] : [];
        }
        if ($with_count) {
            $this->desData($data['data'], false, true);
        } else {
            $this->desData($data, false, true);
        }
        return $data;
    }
    protected function db_batch_insert($fields, $data)
    {
        $this->desData($data, true, true);
        return $this->db->batch_insert($this->table, $fields, $data);
    }
    protected function db_insert($insert_data)
    {
        $this->desData($insert_data, true);
        return $this->db->insert($this->table, $insert_data);
    }
    protected function db_update($cond, $update_data)
    {
        $this->desData($update_data, true);
        return $this->db->update($this->table, $cond, $update_data);
    }

    /** 獲取列表 */
    public function base_get_list($data,$exclude_ext_cond=false){
        //條件
        $cond = [
            "info_state="=>$this->InfoStateOn
        ];
        if(!$exclude_ext_cond){
            isset($data['cond']) && $cond = array_merge($cond,$data['cond']);
        }else{
            isset($data['cond']) && $cond = $data['cond'];
        }
        //排序
        !isset($data['order']) && $data['order'] = "create_time desc";
        //欄位
        !isset($data['fields']) && $data['fields'] = "*";
        //返回個數
        !isset($data['with_count']) && $data['with_count'] = 1;
        //頁碼
        !isset($data['page']) && $data['page'] = 0;
        //每頁條數
        !isset($data['page_size']) && $data['page_size'] = 0;

        return $this->db_select($data['fields'],$cond,$data['page'],$data['page_size'],"",$data['order'],$data['with_count']);
    }
    /** 獲取 */
    public function base_get($id,$fields="*"){
        $cond = [
            "id="=>$id,
            "info_state="=>1,
        ];
        $data = $this->db_select_one($fields,$cond);
        return $data?$data:[];
    }
    /** 根據條件獲取單個 */
    public function base_get_by_cond($cond,$fields="*"){
        $cond["info_state="] = $this->InfoStateOn;
        $data = $this->db_select_one($fields,$cond);
        return $data?$data:[];
    }
    /** 根據條件獲取多個 */
    public function base_gets_by_cond($cond,$fields="*"){
        $cond["info_state="] = $this->InfoStateOn;
        $data = $this->db_select($fields,$cond);
        return $data?$data:[];
    }

    /**
     * 批量插入数据
     * @param $data
     * @return bool
     */
    public function base_batch_insert($data)
    {
        if (!$data) {
            return false;
        }
        foreach ($data as &$datum) {
            $datum['info_state'] = 1;
            $datum['update_time'] = time();
            $datum['create_time'] = time();
        }unset($datum);
        $fields = array_keys($data[0]);
        return $this->db_batch_insert($fields, $data);
    }
    /** 添加 */
    public function base_add($data){
        $insert_data = [
            "info_state" => 1,
            "create_time"=>time(),
            "update_time"=>time(),
        ];
        $insert_data = array_merge($data,$insert_data);
        $res = $this->db_insert($insert_data);

        return $res?$res:0;
    }
    /** 批量更新 */
    public function base_update_by_cond($cond,$data){
        $update_data = [
            "update_time"=>time(),
        ];
        $update_data = array_merge($data,$update_data);
        $res = $this->db_update($cond,$update_data);
        return $res?$res:0;
    }
    /** 更新 */
    public function base_update($id,$data){
        $cond = [
            "id="=>$id,
        ];
        $update_data = [
            "update_time"=>time(),
        ];
        $update_data = array_merge($data,$update_data);
        $res = $this->db_update($cond,$update_data);
        return $res?$res:0;
    }
    /** 刪除 */
    public function base_delete($ids){
        !is_array($ids) && $ids = [$ids];
        $cond = [
            "id IN"=>$ids,
            "info_state="=>$this->InfoStateOn,
        ];
        $data = [
            "update_time"=>time(),
            "info_state"=>$this->InfoStateOff,
        ];
        $res = g('pkg_db')->update($this->table,$cond,$data);
        return $res?$res:0;
    }
    /** 刪除 */
    public function base_delete_by_cond($cond){
        $cond = array_merge($cond,[
            "info_state="=>$this->InfoStateOn
        ]);
        $data = [
            "update_time"=>time(),
            "info_state"=>$this->InfoStateOff,
        ];
        $res = g('pkg_db')->update($this->table,$cond,$data);
        return $res?$res:0;
    }
	
	/**
	 * 变更当前处理的企业/成员信息（暂不验证之间对应关系是否正确）
	 * @param array $conf
	 */
	public function set_conf(array $conf) {
		isset($conf['com_id']) && $this -> com_id = intval($conf['com_id']);
		isset($conf['root_id']) && $this -> root_id = intval($conf['root_id']);
		isset($conf['user_id']) && $this -> user_id = intval($conf['user_id']);
		isset($conf['atoken']) && $this -> atoken = intval($conf['atoken']);
		return $this;
	}
	
	/**
	 * 设置access token
	 * @param unknown_type $atoken
	 */
	public function set_atoken($atoken) {
		$this -> atoken = $atoken;
		return $this;
	}
	
	/**
	 * 根据指定条件获取信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function get_by_cond(array $cond, $fields='*') {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return FALSE;
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 根据指定条件获取应用信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function list_by_cond(array $cond, $fields='*',$page=0, $page_size=0, $order = '') {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return array();
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond, $page, $page_size, '', $order);
		return $ret ? $ret : array();
	}
	
	/**
	 * 分页查询
	 * @param array $cond				查询条件
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $order_by	排序
	 * @param unknown_type $with_cnt	是否返回总数，默认：FALSE
	 */
	protected function list_by_page(array $cond, $fields, $page, $page_size, $order_by='', $with_cnt=FALSE) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		$ret = g('pkg_db') -> select($this -> table, $fields, $cond, $page, $page_size, '', $order_by, $with_cnt);
		if ($with_cnt) {
			$ret = array(
                'data' 	=> $ret['data'] ? $ret['data'] : array(),
                'count' => $ret['count'],
            );
		} else {
			$ret = $ret ? $ret : array();
		}
		return $ret;
	}
	
	/**
	 * 执行sql查询语句
	 * @param unknown_type $string	sql语句，参数值使用?占位
	 * @param unknown_type $params	参数，必须与sql中?先后顺序一致
	 */
	protected function query($sql, array $params) {
		return g('pkg_db') -> prepare_query($sql, 1, $params);
	}
	
	/**
	 * 插入数据
	 * @param array $data				数据
	 * @param unknown_type $errmsg		错误提示
	 * @param unknown_type $throw_exp	是否抛异常,默认:TRUE
	 */
	public function insert(array $data, $errmsg, $throw_exp=TRUE) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($data))	return TRUE;
		$ret = g('pkg_db') -> insert($this -> table, $data);
		if (!$ret && $throw_exp) {
			throw new \ExceptionBase($errmsg);
		}
		return $ret;
	}
	
	/**
	 * 更新数据
	 * @param array $cond				更新条件
	 * @param array $data				更新结果
	 * @param unknown_type $errmsg		错误提示
	 * @param unknown_type $throw_exp	是否抛异常,默认:TRUE
	 */
	protected function update_by_cond(array $cond, array $data, $errmsg, $throw_exp=TRUE) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');
		
		if (empty($cond))	return TRUE;
		$ret = g('pkg_db') -> update($this -> table, $cond, $data);
		if (!$ret && $throw_exp) {
			if (is_array($errmsg)) {
				throw new \ExceptionBase($errmsg);
				
			} else {
				throw new \Exception($errmsg);
			}
		}
		return $ret;
	}
	
	
	/** 初始化当前库表 */
	protected function set_table($table) {
		$this -> table = $table;
		return $this;
	}

	/**
	 * 验证某条数据库记录是否存在
	 * @param array $cond			查询条件
	 * @return boolean 存在返回TRUE，否则返回FALSE
	 */
	protected function record_exists(array $cond) {
		if (empty($this -> table))	throw new \Exception('未指定要操作的库表');

		if (empty($cond))	return TRUE;

		$ret = g('pkg_db') -> select_one($this -> table, '1', $cond);
		return $ret ? TRUE : FALSE;
	}
	
	//日志记录===================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, '', $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, '', $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, '', $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, '', $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, '', $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, '', $str);
	}
	
}

//end