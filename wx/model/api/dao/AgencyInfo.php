<?php
/**
 * 代理商信息表
 * @author yangpz
 * @date 2014-12-19
 */
namespace model\api\dao;

class AgencyInfo extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 't_agency_info';
	
	/** 默认技术支持描述 */
	private $def_support_desc;
	/** 默认技术支持链接 */
	private $def_support_url;
	
	public function __construct() {
		$conf = load_config('model/api/powerby');
		$this -> def_support_desc = $conf['desc'];
		$this -> def_support_url = $conf['url'];
		parent::__construct();
	}
	
	/**
	 * 获取技术支持信息
	 * @param unknown_type $agt_id
	 */
	public function get_support_info($agt_id) {
		if ($agt_id == 0) {
			return array('info' => $this -> def_support_desc, 'url' => $this -> def_support_url);
		}
		
		$agt = $this -> get_by_id($agt_id);
		if (!$agt) {
			parent::log_e('找不到代理商信息: agt_id='.$agt_id);
			return array('info' => '', 'url' => 'javascript:void(0);');
		}
		
		$oem_id = $agt['oem_id'];
		if ($oem_id == 0) {
			return array('info' => $this -> def_support_desc, 'url' => $this -> def_support_url);
		} 
		
		$agt = $this -> get_by_id($oem_id);
		if (!$agt) {
			parent::log_e('找不到代理商信息: agt_id='.$agt_id);
			return array('info' => '', 'url' => 'javascript:void(0);');
		}
		
		$info = (empty($agt['oem_support_info'])) ? '' : $agt['oem_support_info'];
		$url = (empty($agt['oem_support_url'])) ? 'javascript:void(0);' : $agt['oem_support_url'];
		return array('info' => $info, 'url' => $url);
	}
	
	/**
	 * 根据ID获取代理商信息
	 * @param unknown_type $agt_id
	 */
	public function get_by_id($agt_id, $fields='*') {
		$cond = array(
			'agt_id=' => $agt_id,
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
}

// end of file