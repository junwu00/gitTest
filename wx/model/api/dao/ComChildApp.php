<?php
/**
 * 新应用配置
 * @author chenyihao
 * @date 2016-07-28
 *
 */
namespace model\api\dao;

class ComChildApp extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_com_child_app';
	
	/** 禁用 0 */
	public static $StateOff = 0;
	/** 启用 1 */
	public static $StateOn = 1;
	/** 未授权 2 */
	public static $StateNoAuth = 2;
	
	public function __construct($conf=array()) {
		empty($conf) && $conf = array();
		parent::__construct($conf);
		$this -> set_table(self::$Table);
	}


	/** 获取授权的子应用 */
	public function get_auth_app($com_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id,
				'state=' => self::$StateOn
			);
		return $this -> list_by_cond($cond, $fields);
	}

	/** 根据app_id获取 */
	public function get_by_app_id($com_id, $app_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id,
				'app_id=' => $app_id,
			);
		$ret = $this -> get_by_cond($cond, $fields);
		return $ret;
	}

	/** 安装APP */
	public function install_app($com_id, $app_id, $dept_list=array(), $user_list=array()){
		$cond = array(
				'com_id=' => $com_id,
				'app_id=' => $app_id,
			);
		$data = array(
				'state' => self::$StateOn,
				'install_time' => time(),
				'visit_depts' => json_encode($dept_list),
				'visit_users' => json_encode($user_list)
			);
		$ret = $this -> update_by_cond($cond, $data);
		return $ret;
	}

	/** 卸载APP */
	public function uninstall_app($com_id, $app_id){
		$cond = array(
				'com_id=' => $com_id,
				'app_id=' => $app_id,
			);
		$data = array(
				'state' => self::$StateNoAuth,
				'cannel_time' => time()
			);
		$ret = $this -> update_by_cond($cond, $data);
		return $ret;
	}

	/** 改变可见范围 */
	public function change_visit($com_id, $app_id, $depts = array(), $users = array(), $groups = array()){
		$cond = array(
				'com_id=' => $com_id,
				'app_id=' => $app_id,
			);
		$data = array(
				'visit_depts' => json_encode($depts),
				'visit_users' => json_encode($users),
				'visit_groups' => json_encode($groups),
			);
		$ret = $this -> update_by_cond($cond, $data);	
		return $ret;
	}

	/** 根绝企业ID获取应用安装信息 */
	public function get_by_com($com_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id,
			);
		return $this -> list_by_cond($cond, $fields);
	}

	/** 批量插入 */
	public function batch_insert($com_id, $app_ids){
		$app = array();
		foreach ($app_ids as $app_id) {
			$app[] = array(
					'app_id' => $app_id,
					'state' => self::$StateNoAuth,
					'com_id' => $com_id,
					'visit_depts' => '[]',
					'visit_users' => '[]',
					'visit_groups' => '[]',
					'create_time' => time(),
					'first_install_time' => 0,
					'install_time' => 0,
					'cannel_time' => 0,
				);
		}

		$fields = array('app_id', 'state', 'com_id', 'visit_depts', 'visit_users', 'visit_groups', 'create_time', 'first_install_time', 'install_time', 'cannel_time');

		return g('pkg_db') -> batch_insert(self::$Table, $fields, $app);
	}

	/** 检查是否已经安装指定应用 */
	public function check_install($com_id, $app_id, $fields = '*'){
		$cond = array(
				'com_id=' => $com_id,
				'app_id=' => $app_id,
			);
		$app = $this -> get_by_cond($cond, $fields);
		return $app['state'] == self::$StateOn ? $app : false;
	}

}

// end of file
