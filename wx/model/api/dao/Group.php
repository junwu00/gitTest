<?php
/**
 * 组别公共服务类
 * @author chenyihao
 * @date 2016-06-24
 *
 */
namespace model\api\dao;

class Group extends \model\api\dao\DaoBase {

	/** 对应的库表名称 */
	private static $Table = 'sc_group';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	public function __construct(){
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	/**
	 * 根据ID获取组别信息
	 * @param   $group_id
	 * @param   $fields
	 * @return 
	 */
	public function get_by_id($com_id, $group_id, $fields='*'){
		$cond = array(
				'id=' => $group_id,
				'com_id=' => $com_id,
				'info_state=' => self::$StateOn,
			);
		return $this -> get_by_cond($cond, $fields);
	}

	/**
	 * 根据ID数组获取组别信息
	 * @param   $group_ids
	 * @param   $fields
	 * @return 
	 */
	public function get_by_ids($com_id, $group_ids, $fields='*'){
		$cond = array(
				'id IN' => $group_ids,
				'info_state=' => self::$StateOn,
			);

        if($com_id){
            $cond['com_id='] = $com_id;
        }

		return $this -> list_by_cond($cond, $fields);
	}

	/**
	 * 根据组别ID数组获取组别成员信息（去重）
	 * @param  [type] $com_id    
	 * @param  [type] $group_ids 
	 * @param  string $fields    
	 * @return array
	 */
	public function get_user_by_ids($com_id, $group_ids, $fields='id', $page=1, $page_size=20, $user_order = '', $user_id = 0) {
		$cond = array(
			'id IN' => $group_ids,
			'com_id=' => $com_id,
			'info_state=' => self::$StateOn,
		);

        // gch add contact config
        $hiddenConfig = g('dao_contact_config') -> getHiddenConfig($com_id, $user_id);
        if (!empty($hiddenConfig['group'])) {
			$cond['__OR_99'] = array(
				'id NOT REGEXP ' => '(' . implode('|', $hiddenConfig['group']) . ')',
			);
        }
        if (!empty($hiddenConfig['user'])) {
			$cond['__OR_99']['user_list NOT REGEXP '] = "\"(" . implode('|', $hiddenConfig['user']) . ")\"";
        }
        $watchOther = g('dao_contact_config') -> checkWatchOther($com_id, $user_id);
        if ($watchOther['flag'] && !empty($watchOther['group'])) {
			$cond['__OR_100'] = array(
				'id REGEXP ' => '(' . implode('|', $watchOther['group']) . ')',
			);
        }
        if ($watchOther['flag'] && !empty($watchOther['user'])) {
			$cond['__OR_100']['user_list REGEXP '] = "\"(" . implode('|', $watchOther['user']) . ")\"";
        }

		$ret = $this -> list_by_page($cond, 'user_list', $page, $page_size);
		if (!$ret) return array();

		$user_ids = array();
		foreach ($ret as $key => $users) {
			$user_ids = array_merge($user_ids, json_decode($users['user_list'], TRUE));
		}unset($users);
		$user_ids = array_unique($user_ids);

		$user_info = g('dao_user') -> list_by_ids($user_ids, $fields, TRUE, $user_order);

		return $user_info;
	}

	/**
	 * 根据USER_ID获取其所在分组
	 * @param   $com_id
	 * @param   $user_id
	 * @param  string $fields
	 * @return 
	 */
	public function get_by_user_id($com_id, $user_id, $fields='*'){
		$cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$StateOn,
				'user_list LIKE ' => '%"'.$user_id.'"%',
			);

		$ret = $this -> list_by_cond($cond, $fields);
		return $ret ? $ret : array();
	}

	/**
	 * 检查该员工是否在组别中
	 * @param   $group_id
	 * @param   $user_id
	 * @return 
	 */
	public function check_user_in_group($com_id, $group_id, $user_id){
		$cond = array(
				'com_id=' => $com_id,
				'id=' => $group_id,
				'user_list LIKE ' => '%"'.$user_id.'"%'
			);
		$ret = $this -> record_exists($cond);
		return $ret;
	}

	/**
	 * 根据企业ID获取公司所有分组信息,包含共享分组信息 
	 * @param int $com_id
	 * @param string $fields
	 * @param int $user_id 用户user_id 用于微信可见范围
	 * @return 
	 */
	public function get_group_tree($com_id, $fields = '*', $user_id = 0){
		$cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$StateOn,
		);
		if (!empty($user_id)) {
			$cond['wx_user_list LIKE '] = '%"' . $user_id . '"%';
		}

        // gch add contact config
        $hiddenConfig = g('dao_contact_config') -> getHiddenConfig($com_id, $user_id);
        if (!empty($hiddenConfig['group'])) {
            $cond['id NOT IN'] = $hiddenConfig['group'];
        }
        $watchOther = g('dao_contact_config') -> checkWatchOther($com_id, $user_id);
        if ($watchOther['flag'] && !empty($watchOther['group'])) {
            $cond['id IN'] = $watchOther['group'];
		}

		$ret = $this -> list_by_cond($cond, $fields);
		return $ret ? $ret : array();
	}


//================================内部实现============================


}

// end of file