<?php
/**
 * 企业的应用配置
 * @author yangpz
 * @date 2014-11-14
 *
 */
namespace model\api\dao;

class ComApp extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_com_app';
	
	/** 禁用 0 */
	public static $StateOff = 0;
	/** 启用 1 */
	public static $StateOn = 1;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	
	/**
	 * 获取应用的我方套件id
	 *
	 * @access public
	 * @param integer $app_id 我方定义的应用id
	 * @param integer $com_id 企业id
	 * @return boolean
	 */
	public function get_sie_id($app_id, $com_id=0) {
		return 0;
		empty($com_id) && $com_id = $this -> com_id;
		if (empty($com_id))	throw new \Exception('未指定企业ID');

		$key_str = __FUNCTION__ . "::{$com_id}::{$app_id}";
		$cache_key = $this -> _get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);

		if ($ret !== false) return $ret;
		
		$cond = array(
			'com_id=' 	=> $com_id,
			'app_id='	=> $app_id,
			'state='	=> self::$StateOn
		);
		$ret = $this -> list_by_page($cond, 'sie_id', 1, 1, ' ORDER BY id DESC ');
		if (empty($ret)) {
			parent::log_w("企业未安装该应用: com={$com_id}, app={$app_id}");
			$sie_id = FALSE;
			
		} else {
			$sie_id = $ret[0]['sie_id'];
		}
		
		// 缓存1分钟
		g('pkg_redis') -> set($cache_key, $sie_id, 60);
		return $sie_id;
	}
	
	/**
	 * 根据企业ID获取应用列表
	 * @param unknown_type $com_id	企业ID
	 */
	public function list_by_com_id($com_id=0, $state=NULL, $fields='*') {
		empty($com_id) && $com_id = $this -> com_id;
		if (empty($com_id))	throw new \Exception('未指定企业ID');
		
		$cond = array(
			'com_id=' => $com_id,
		);
		!is_null($state) && $cond['state='] = $state;

		$key_str = __FUNCTION__ . "::" . json_encode($cond);
		$cache_key = $this -> _get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);

		if ($ret !== false) {
			$ret = json_decode($ret, true);
			!is_array($ret) and $ret = array();
			return $ret;
		}

		$ret = $this -> list_by_cond($cond, $fields);

		// 缓存1分钟
		g('pkg_redis') -> set($cache_key, json_encode($ret), 60);

		return $ret;
	}
	
	/**
	 * 获取企业指定应用
	 * @param unknown_type $app_id
	 * @param unknown_type $com_id
	 */
	public function get_by_app_id($app_id, $com_id=0, $fields='*') {
		empty($com_id) && $com_id = $this -> com_id;
		if (empty($com_id))	throw new \Exception('未指定企业ID');
		
		$cond = array(
			'com_id=' => $com_id,
			'app_id=' => $app_id,
		);

		$key_str = __FUNCTION__ . "::" . json_encode($cond);
		$cache_key = $this -> _get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);

		if ($ret !== false) {
			$ret = json_decode($ret, true);
			!is_array($ret) and $ret = array();
			return $ret;
		}

		$ret = $this -> list_by_cond($cond, $fields);
		!empty($ret) and $ret = array_shift($ret);
		!is_array($ret) and $ret = array();

		// 缓存1分钟
		g('pkg_redis') -> set($cache_key, json_encode($ret), 60);

		return $ret;
	}

	/**
	 * 获取所有已安装的应用信息
	 * @param unknown_type $com_id			企业ID
	 */
	public function list_all_auth_app($com_id=0, $fields='a.id id,a.`name` `name`') {
		empty($com_id) && $com_id = $this -> com_id;
		if (empty($com_id))	throw new \Exception('未指定企业ID');
		
		try {
			$sql = <<<EOF
SELECT {$fields} 
FROM sc_com_app c, sc_app a 
WHERE c.state = ? AND a.id = c.app_id AND c.com_id = ?
EOF;
			$params = array(
				self::$StateOn,
				$com_id
			);
			$ret = $this -> query($sql, $params);
			return $ret ? $ret : array();
			
		} catch(\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 根据token获取企业应用信息
	 * @param unknown_type $token
	 * @param unknown_type $fields
	 */
	public function get_by_token($token, $fields='*') {
		$cond = array(
			'token=' => $token,
		);
		$ret = $this -> get_by_cond($cond, $fields);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 判断应用是否推送操作指引
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $app_id	应用ID
	 */
	public function is_send_guide($com_id, $app_id) {
		$fields = ' count(id) AS cnt ';
		$cond = array(
			'com_id=' 		=> $com_id,
			'app_id=' 		=> $app_id,
			'send_guide=' 	=> 1
		);
		$cnt = $this -> get_by_cond($cond, $fields);
		return $cnt['cnt'] > 0;
	}
	
	//内部实现 ==================================================================================================
	
	/**
	 * 获取缓存变量名
	 *
	 * @access private
	 * @param string $str 关键字符串
	 * @return string
	 */
	private function _get_cache_key($str) {
		$key_str = $this -> cache_key_prefix . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}
}

// end of file