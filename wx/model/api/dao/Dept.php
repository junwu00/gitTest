<?php
/**
 * 企业部门查询类
 * @author yangpz
 * @date 2015-12-18
 */

namespace model\api\dao;

class Dept extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_dept';
	
	/** 禁用 */
	public static $StateOff = 0;
	/** 启用 */
	public static $StateOn = 1;
	
	/** 组织架构过滤 */
	private $struct_filter = FALSE;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}

    /**
     * (批量)根据指定条件获取成员信息
     * @param array $cond		查询条件
     * @param unknow $fields	查询字段
     */
    public function list_by_cond(array $cond, $fields='*', $page=0, $page_size=0, $order = '') {
//        if (!isset($cond['root_id=']) || empty($cond['root_id='])) {
//            throw new \Exception('未指定企业根部门ID');
//        }

        return parent::list_by_cond($cond, $fields, $page, $page_size, $order);
    }
	/**
	 * 设置组织架构配置
	 * @param unknown_type $open_filter	是否开启过滤
	 * @param array $conf				过滤配置信息
	 */
	public function set_struct($open_filter=TRUE, array $conf=array()) {
		$this -> struct_filter = $open_filter;
		parent::set_conf($conf);
	}
	
	//GET-----------------------------------------------------------------------------------------------

	/**
	 * 根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	public function get_by_cond(array $cond, $fields='*') {
//		if (!isset($cond['root_id=']) || empty($cond['root_id='])) {
//			throw new \Exception('未指定企业根部门ID');
//		}
		return parent::get_by_cond($cond, $fields);
	}
	
	/**
	 * 根据部门ID，获取部门信息
	 * @param unknown_type $dept_id	部门id
	 * @param unknown_type $fields	查找的字段，默认查找全部
	 * 
	 * @return 部门信息（数组），找不到则返回FALSE
	 */
	public function get_by_id($id, $fields='*') {
        $cond = array(
			'id =' 		=> $id,
            //'root_id=' 	=> $this -> root_id,
        	'state=' 	=> self::$StateOn
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
	/**
	 * 根据ID数组查询部门信息
	 * @param unknown_type $root_id	企业根部门ID
	 * @param unknown_type $dept_id	部门ID
	 * @param unknown_type $fields	查询的字段
	 */
	public function get_by_ids($dept_ids, $fields=NULL) {
		$cond = array(
			'id IN'		=> $dept_ids,
			'root_id=' 	=> $this -> root_id,
			'state='	=> self::$StateOn
		);
		return parent::list_by_cond($cond, $fields);
	}



	//LIST-----------------------------------------------------------------------------------------------

	/**
	 * (批量)根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	// public function list_by_cond(array $cond, $fields='*') {
	// 	if (!isset($cond['root_id=']) || empty($cond['root_id='])) {
	// 		throw new \Exception('未指定企业根部门ID');
	// 	}
		
	// 	return parent::list_by_cond($cond, $fields);
	// }
	
	/**
	 * 根据ID数组查找部门
	 * @param unknown_type $root_id	企业根部门
	 * @param array $ids			部门ID列表
	 * @param unknown_type $fields	查找的字段，默认查找全部
	 * 
	 * @return 部门信息（数组），找不到则返回空数组
	 */
	public function list_by_ids(array $ids, $fields='*') {
		if (empty($ids))	return array();
		
		$cond = array(
			//'root_id='	=> $this -> root_id,
			'state='	=> self::$StateOn,
			'id IN'		=> $ids,
		);
		return $this -> list_by_cond($cond, $fields);
	}
	
	/**
	 * 获取部门及其子..部门ID字符串列表（不做过滤）
	 * @param unknown_type $no_cache		是否不从缓存中取数据，默认为：FALSE
	 * @param unknown_type $type			返回的数据的value的数据类型，默认为string
	 * 
	 * @return 如：array(1 => '2,22,3,33')，表示部门ID为'2,22,3,33'的部门是部门ID为1的部门的子..部门
	 */
	public function list_all_child_ids($no_cache=FALSE, $type='string') {
		$fields = 'id,p_id';
		$cache_key = __FUNCTION__.':'.$this -> root_id.':'.$fields.':'.$type;
		$cache_key = $this -> _get_cache_key($cache_key);

		if ($no_cache) {
			$ret = $this -> list_by_lev(0, $fields);
			$ret = $this -> _sort_child_ids_str($ret, $type);
			g('pkg_redis') -> set($cache_key, json_encode($ret), 300);			//缓存5分钟
			
		} else {
			$ret = g('pkg_redis') -> get($cache_key);
			if ($ret) {
				$ret = json_decode($ret, TRUE);
				
			} else {
				$ret = $this -> list_by_lev(0, $fields);
				$ret = $this -> _sort_child_ids_str($ret, $type);	
				g('pkg_redis') -> set($cache_key, json_encode($ret), 300);		//缓存5分钟
			}
		}

		return $ret;
	}

	/**
	 * 获取指定层级的部门列表
	 * gch edit
	 * @param unknown_type $lev			层级，默认为: 0(表示获取该企业所有层级)
	 * @param unknown_type $fields		查找的字段，默认查找全部
	 */
	public function list_by_lev($lev=0, $fields='*') {
		$table = self::$Table . ' as t1';

		!is_array($fields) && $fields = explode(',', $fields);
		if (empty($fields)) {
			return array();
		}
		array_walk($fields, function(&$v, $k, $prefix) {
			$v = trim($v);
			$v = $prefix . $v;
		}, 't1.');
		$fields = implode(',', $fields);

		$cond = array( 
			't1.root_id=' 	=> $this -> root_id,
			't1.state=' 	=> self::$StateOn
		);
		$lev > 0 && $cond['t1.lev='] = $lev;

		// gch add contact config
		$hiddenConfig = g('dao_contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
		if (!empty($hiddenConfig['dept'])) {
			$cond['t1.id NOT REGEXP '] = '(' . implode('|', $hiddenConfig['dept']) . ')';
		}
		$watchOther = g('dao_contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
		if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$dept_sql = "(SELECT * FROM sc_dept t2 WHERE t2.id IN (" . implode(',', $watchOther['dept']) . ") AND (t2.p_list REGEXP (t1.id) OR t2.id=t1.id))";
			$cond['^EXISTS '] = $dept_sql;
		}
		
		$order = ' ORDER BY t1.lev, t1.idx desc ';

		// $ret = $this -> list_by_page($cond, $fields, 0, 0, ' ORDER BY lev, idx ');
		$ret = g('pkg_db') -> select($table, $fields, $cond, 0, 0, '', $order, false);
		!is_array($ret) || empty($ret) && $ret = array();

		return $ret;
	}
    /**
     * 获取指定部门的下级部门
     */
    public function list_search($name, $fields='id, p_id, name, idx, lev') {
        $table = self::$Table . ' as t1';

        !is_array($fields) && $fields = explode(',', $fields);
        if (empty($fields)) {
            return array();
        }
        array_walk($fields, function(&$v, $k, $prefix) {
            $v = trim($v);
            $v = $prefix . $v;
        }, 't1.');
        $fields = implode(',', $fields);

        $cond = array(
            't1.root_id=' 	=> $this -> root_id,
            't1.state=' 	=> self::$StateOn,
            't1.name like' 	=> "%{$name}%"
        );

        // gch add contact config
        $hiddenConfig = g('dao_contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
        if (!empty($hiddenConfig['dept'])) {
            $cond['^t1.id NOT REGEXP '] = '"(' . implode('|', $hiddenConfig['dept']) . ')"';
        }
        $watchOther = g('dao_contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
        if ($watchOther['flag'] && !empty($watchOther['dept'])) {
            $dept_sql = "(SELECT * FROM sc_dept t2 WHERE t2.id IN (" . implode(',', $watchOther['dept']) . ") AND (t2.p_list REGEXP (t1.id) OR t2.id=t1.id))";
            $cond['^EXISTS '] = $dept_sql;
        }

        $order = ' ORDER BY t1.idx desc ';

        // $ret = $this -> list_by_page($cond, $fields, 0, 0, ' ORDER BY idx ');
        $ret = g('pkg_db') -> select($table, $fields, $cond, 0, 0, '', $order, false);
        !is_array($ret) || empty($ret) && $ret = array();

        return $ret;
    }
	/**
	 * 获取指定部门的下级部门
	 * @param unknown_type $id		部门ID
	 * @param unknown_type $fields	查找的字段
	 */
	public function list_direct_child($id, $fields='id, p_id, name, idx, lev') {
		$table = self::$Table . ' as t1';

		!is_array($fields) && $fields = explode(',', $fields);
		if (empty($fields)) {
			return array();
		}
		array_walk($fields, function(&$v, $k, $prefix) {
			$v = trim($v);
			$v = $prefix . $v;
		}, 't1.');
		$fields = implode(',', $fields);

		$cond = array(
			't1.root_id=' 	=> $this -> root_id,
			't1.state=' 	=> self::$StateOn,
			't1.p_id=' 	=> $id
		);

		// gch add contact config
		$hiddenConfig = g('dao_contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
		if (!empty($hiddenConfig['dept'])) {
			$cond['^t1.id NOT REGEXP '] = '"(' . implode('|', $hiddenConfig['dept']) . ')"';
		}
		$watchOther = g('dao_contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
		if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$dept_sql = "(SELECT * FROM sc_dept t2 WHERE t2.id IN (" . implode(',', $watchOther['dept']) . ") AND (t2.p_list REGEXP (t1.id) OR t2.id=t1.id))";
			$cond['^EXISTS '] = $dept_sql;
		}

		$order = ' ORDER BY t1.idx desc ';

		// $ret = $this -> list_by_page($cond, $fields, 0, 0, ' ORDER BY idx ');
		$ret = g('pkg_db') -> select($table, $fields, $cond, 0, 0, '', $order, false);
		!is_array($ret) || empty($ret) && $ret = array();

		return $ret;
	}

	/**
	 * 获取指定部门及其的子..部门（支持部门树）
	 * @param unknown_type $id			要查找的部门的ID，默认为企业根部门
	 * @param unknown_type $is_tree		是否分层级显示，默认为TRUE
	 * @param unknown_type $fields		要查找的字段，默认为全部
	 * @param unknown_type $no_cache	是否不从缓存中取数据，默认为：FALSE
	 * 
	 * @return 部门树 或 子..部门数组
	 */
	public function list_all_child($id=NULL, $is_tree=TRUE, $fields='*', $no_cache=FALSE) {
		is_null($id) && $id = $this -> root_id;
		
		//返回部门树
		if ($is_tree) {	//返回部门树
			$list = $this -> list_by_lev(0, $fields, FALSE);
			$list = $this -> _sort_child($list, $id);
			
			if ($id == $this -> root_id) {					
				$ret = $list['data'];
			} else if (!empty($list['target_data'])) {		
				$ret = array($list['target_data']);
			} else {									//找不到部门，返回空数组	
				return array();	
			}
			
			if ($this -> struct_filter) {
				//增加虚拟根
				$this -> _flag_disable_dept(isset($ret['childs']) ? $ret['childs'] : array(), $ret, TRUE);					
			}
			
			return $ret;
		}
		
		//所有子..部门列表
		$child_ids_arr = $this -> list_all_child_ids($no_cache, 'array');
		$child_ids_arr = isset($child_ids_arr[$id]) ? $child_ids_arr[$id] : array();
		return $this -> list_by_ids($child_ids_arr, $fields);
	}
	
	/**
	 * 获取指定部门及其父..级部门的ID
	 * @param unknown_type $dept_ids	指定部门的ID
	 * @param unknown_type $with_self_id	父级ID中是否包括自身ID，包括中该ID在数组最后一个。默认为TRUE
	 */
	public function list_all_parent_ids(array $dept_ids, $with_self_id=TRUE) {
		if (empty($dept_ids))	return array();
		
		$page_size = 5000;	//每次最多处理的上限
		$deal_total = count($dept_ids);
		
		$fields = 'id, p_list';
		$ret = array();
		if ($deal_total > $page_size) {
			//获取每个分页要处理的ID
			$ids_list = array();
			$pages = ceil($deal_total / $page_size);
			for ($i=0; $i<$pages; $i++) {
				$ids = array_slice($dept_ids, $i*$page_size, $page_size);
				$ids_list[] = $ids;
			}
			//分页处理
			foreach ($ids_list as $ids) {
				$cond = array(
					'root_id=' 	=> $this -> root_id,
					'state=' 	=> self::$StateOn,
					'id IN' 	=> $ids,
				);
				$tmp_ret = $this -> list_by_cond($cond, $fields);
				foreach ($tmp_ret as $t) {
					$p_list = json_decode($t['p_list'], TRUE);
					!is_array($p_list) && $p_list = array();
					
					$ret[$t['id']] = $p_list;
					$with_self_id && $ret[$t['id']][] = $t['id'];
				}unset($t);
			}unset($ids);
			
		} else {
			$cond = array(
				'root_id=' 	=> $this -> root_id,
				'state=' 	=> self::$StateOn,
				'id IN' 	=> $dept_ids, 
			);
			$tmp_ret = $this -> list_by_cond($cond, $fields);
			foreach ($tmp_ret as $t) {
				$p_list = json_decode($t['p_list'], TRUE);
				!is_array($p_list) && $p_list = array();
				
				$ret[$t['id']] = $p_list;
				$with_self_id && $ret[$t['id']][] = $t['id'];
			}unset($t);
		}

		$users = array();
		
		foreach ($ret as &$item) {
			$item = array_values($item);
			$item = strval_array($item);
			$item = array_reverse($item);
		}
		return $ret;
	}
	
	/**
	 * 判断指定部门是否存在子部门
	 * @param unknown_type $dept_ids	部门ID列表
	 */
	public function list_has_child_by_ids(array $dept_ids) {
		if (empty($dept_ids)) 	return array();
		
		$max_size = 500;
		$len = count($dept_ids);
		if ($len > $max_size) {
			parent::log_w(__CLASS__.':'.__FUNCTION__.'[超出查询数量范围[上限:'.$max_size.', 当前: '.$len.']');
			throw new \Exception('系统繁忙');
		}
		
		$fields = 'p_id';
		$cond = array(
			'root_id=' 	=> $this -> root_id,
			'state=' 	=> self::$StateOn,
			'p_id IN' 	=> $dept_ids,
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond, 0, $max_size, ' group by p_id ');
		$ret = $ret ? $ret : array();
		$tmp = array();
		foreach ($ret as $item) {
			$tmp[$item['p_id']] = TRUE; 
		}unset($item);
		
		$ret = array();
		foreach ($dept_ids as $item) {
			$ret[$item] = isset($tmp[$item]) ? TRUE : FALSE;
		}unset($item);
		return $ret;
	}
	
	/**
	 * 获取部门领导列表
	 * @param unknown_type $dept_id		部门ID
	 * @param unknown_type $only_id		仅返回成员ID，还是返回成员信息，默认为仅成员ID
	 */
	public function list_leader($dept_id, $only_id=TRUE) {
		$api_leader = $this -> _get_api_leader_instance();
		return $api_leader -> list_leader_by_dept_id($dept_id, $only_id);
	}
	
	//内部实现=================================================================================================
	
    /**
     * 获取缓存变量名
     * @param string $str 关键字符串
     * @return string
     */
    private function _get_cache_key($str) {
    	$filter_str = $this -> struct_filter ? '1' : '0';
        $key_str = $this -> cache_key_prefix . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str.':'.$filter_str;
        return md5($key_str);
    }
	
	/**
	 * 将部门列表，按层级关系排序
	 * @param unknown_type $dept_list		部门列表
	 * @param unknown_type $target_dept_id	目标部门ID，用于仅显示该部门及其子..部门的信息
	 */
	private function _sort_child(array $dept_list, $target_dept_id) {
		$dept_by_lev = array();
		$target_data = array();
		
		//删除所有不在组织架构的部门
		if ($this -> struct_filter) {
			$new_dept_list = array();
			foreach ($dept_list as $dept) {
				$new_dept_list[$dept['id']] = $dept;
			}unset($dept);
			
			$dept_list = $new_dept_list;
			unset($new_dept_list);
			$new_dept_list = array();
			
			$scope_st = $this -> struct_scope_all;
			$scope_st = intval_array($scope_st);
			$scope_st = array_flip($scope_st);
			foreach ($dept_list as $dept) {
				if (isset($scope_st[$dept['id']])) {								//在组织架构中，保留部门，开始递归验证上级
					$tmp = $dept;
					$flag = FALSE;
					while (TRUE) {
						$p_id = $tmp['p_id'];
						if ($p_id == 0) 	BREAK;
						
						if (isset($scope_st[$p_id])) {								//当前部门的上级部门在组织架构中，标识
							$flag = TRUE;
						}
						if (!isset($scope_st[$p_id])) {								//上..级部门不在组织架构中
							!$flag && $dept['p_id'] = $dept_list[$p_id]['p_id'];	//p_id取上级部门p_id
							$dept['lev']--;											//每一个“上一级部门”无权限，部门层级上移一级
						}
						$tmp = $dept_list[$p_id];
					}
					
					$new_dept_list[$dept['id']] = $dept;
				}
			}unset($dept);
			
			$dept_list = $new_dept_list;
			unset($new_dept_list);
		}
		
		$sorted_dept = array();
		foreach ($dept_list as $dept) {
			$dept['order'] = $dept['idx'];
			$sorted_dept[$dept['id']] = $dept;
		}unset($dept);
		unset($dept_list);
		
		$top_lev = 9999;
		foreach ($sorted_dept as &$dept) {
			if ($top_lev > intval($dept['lev']))			$top_lev = $dept['lev'];
			if ($dept['id'] == $target_dept_id) 			$target_data = &$dept;
			
			if ($dept['p_id'] != 0 && isset($sorted_dept[$dept['p_id']])) {
				$sorted_dept[$dept['p_id']]['childs'][] = &$sorted_dept[$dept['id']];
			}
		}
		
		$ret = array();
		foreach ($sorted_dept as $d) {
			if ($d['p_id'] == 0) {
				$ret[] = $d;
			}
		}unset($d);
		
		return array('data' => $ret, 'top_lev' => $top_lev, 'target_data' => $target_data);
	}

	
	/**
	 * 递归，将子部门添加进父部门
	 * @param unknown_type $parent_dept
	 * @param unknown_type $childs
	 */
	private function _sort_child_recurse(array &$parent_dept, array $childs, array $dept_list) {
		foreach ($childs as $dept) {
			if ($dept['p_id'] == $parent_dept['id']) {
				
				if (isset($dept_list[$dept['lev']+1]))	{
					$this -> _sort_child_recurse($dept, $dept_list[$dept['lev']+1], $dept_list);
				}
				
				!isset($parent_dept['childs']) && $parent_dept['childs'] = array(); 
				array_push($parent_dept['childs'], $dept);
			}
		}
		unset($dept);
	}
	
	/**
	 * 标识有权限查看的部门（含其上级部门）
	 * @param array $dept_tree
	 * @param array $curr_dept
	 */
	private function _flag_disable_dept(array &$dept_tree, array &$curr_dept, $append_root=FALSE) {
		if ($append_root) {		//增加虚拟根
			if (count($curr_dept) > 1) {
				foreach ($curr_dept as &$item) {	//不展开
					$item['open'] = FALSE;
				}
				$curr_dept = array (
					array(
						'id' 			=> 0,
						'name' 			=> $this -> struct_info['root_name'],
						'open' 			=> TRUE,
						'enable' 		=> 0,
						'enable_cnt' 	=> $lev1_cnt,
						'enable_w' 		=> 0,
						'childs' 		=> $curr_dept
					)
				);
			}
		}
	}
	
	/**
	 * 获取部门层级关系(部门=》下级部门列表(含自身\子子...部门))
	 * @param unknown_type $dept_list	部门列表
	 * @param unknown_type $type	返回的数组的value类型, string：string类型；array: 数组类型
	 */
	private function _sort_child_ids_str(array $dept_list, $type=TRUE) {
		$sorted_dept = array();
		foreach ($dept_list as $dept) {
			$sorted_dept[$dept['id']] = $dept;
		}unset($dept);
		
		$max_lev = 15;
		foreach ($dept_list as &$dept) {
			$sorted_dept[$dept['id']]['childs'][] = $dept['id'];
			$p_id = $dept['p_id'];
			$curr_lev = 0;
			while ($p_id > 0) {
				if (isset($sorted_dept[$p_id])) {
					$sorted_dept[$p_id]['childs'][] = $dept['id'];
					$p_id = $sorted_dept[$p_id]['p_id'];
					
				} else {
					break;
				}
				if (++$curr_lev > $max_lev)	break;
			}
		}
		
		$ret = array();
		if (strtolower($type) == 'array') {
			foreach ($sorted_dept as $d) {
				$ret[$d['id']] = $d['childs'];
			}unset($d);
			
		} else {
			foreach ($sorted_dept as $d) {
				$ret[$d['id']] = implode(',', $d['childs']);
			}unset($d);
		}
		return $ret;
	}

	/**
	 * 根据当前信息，获取上下级关系数据库操作类
	 */
	private function _get_api_leader_instance() {
		$instance = g('dao_leader');
		$conf = array(
			'com_id'	=> $this -> com_id,
			'root_id'	=> $this -> root_id,
			'user_id'	=> $this -> user_id
		);
		$instance -> set_struct($this -> struct_filter, $conf);
		return $instance;
	}
	
}

//end