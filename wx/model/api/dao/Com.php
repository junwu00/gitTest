<?php
/**
 * 企业信息
 * @author yangpz
 * @date 2016-02-19
 */
namespace model\api\dao;

class Com extends \model\api\dao\DaoBase {
	
	/** 对应的库表名称 */
	private static $Table = 'sc_company';
	
	/** 禁用0 */
	public static $StateOff = 0;
	/** 启用1 */
	public static $StateOn = 1;
	/** 试用2 */
	public static $StateTest = 2;
	/** 过期3 */
	public static $StateOverTime = 3;

    public function list_company(){
        $cond = array(
            'state!='=>0,
            'corp_url!='=>''
        );
        $ret = g('pkg_db') -> select(self::$Table, '*', $cond);
        return $ret?$ret:[];
    }


    public function get_by_ids($com_ids,$fields='*'){
        $cond = array(
            'id IN' => $com_ids
        );
        $ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
        return $ret ? $ret : [];
    }

	/**
	 * 根据企业ID获取企业信息
	 * @param unknown_type $com_id	企业ID，默认为当前企业
	 * @param unknown_type $fields	查询字段
	 */
	public function get_by_id($com_id=NULL, $fields='*') {
		empty($com_id) && $com_id = $this -> com_id;
		
		if (empty($com_id))		throw new \Exception('未指定企业ID');
		
		$cond = array(
			'id=' => $com_id
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 根据企业根部门ID获取企业信息
	 * @param unknown_type $root_id	企业根部门ID
	 * @param unknown_type $fields	查询字段
	 */
	public function get_by_root_id($root_id=NULL, $fields='*') {
		empty($root_id) && $root_id = $this -> root_id;
		
		if (empty($root_id))		throw new \Exception('未指定企业根部门ID');
		
		$cond = array(
			'dept_id=' => $this -> root_id
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 根据企业配置的url，查找对应的企业信息
	 * @param unknown_type $corp_url
	 * @param unknown_type $fields		查询字段
	 */
	public function get_by_corp_url($corp_url, $fields='*') {
		$cond = array(
			'corp_url=' => $corp_url,
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			throw new \Exception('corp url记录重复：'.$corp_url);
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 根据corpid 获取企业 信息
	 * @param unknown_type $corp_id
	 * @param unknown_type $fields		查询字段
	 */
	public function get_by_corp_id($corp_id, $fields='*') {
		$cond = array(
			'corp_id=' => $corp_id,
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			throw new \Exception('corp id记录重复：'.$corp_id);
		}
		return $ret ? $ret[0] : FALSE;
	}

    /**
     * 获取企业列表
     * @param $cond
     * @param $fields
     * @return array
     */
    public function list_by_cond($cond,$fields='*'){
        $ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
        return $ret?$ret:[];
    }
	
}

//end