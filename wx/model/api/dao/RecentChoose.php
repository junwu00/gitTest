<?php
/**
 * 成员选择人员、部门记录操作
 * @author luja
 * @date 2017-06-21
 */

namespace model\api\dao;

class RecentChoose extends \model\api\dao\DaoBase {

	private static $Table = "sc_user_recent_choose";

	/** 正常 */
	public static $InfoOn = 1;
	/** 删除 */
	public static $InfoDel = 0;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}


	/**
	 * 获取最近选用的人员、部门
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @param  string $fields  
	 */
	public function get_recent_info($com_id, $user_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'info_state=' => self::$InfoOn
			);
		
		return parent::get_by_cond($cond, $fields);
	}

	/**
	 * 保存最近选用的人员、部门
	 * @param  [type] $com_id    
	 * @param  [type] $user_id   
	 * @param  array  $user_list array("id"=>"time")
	 * @param  array  $dept_list 
	 */
	public function save_recent_choose($com_id, $user_id, $user_list=array(), $dept_list=array()) {
		if (empty($user_list) && empty($dept_list)) return true;
		$time = time();
		$data = array(
			'user_id' 		=> $user_id,
			'com_id' 		=> $com_id,
			'user_list' 	=> json_encode($user_list),
			'dept_list' 	=> json_encode($dept_list),
			'create_time' 	=> $time,
			'update_time' 	=> $time
			);

		return parent::insert($data, '', false);
	}

	/**
	 * 更新最近选用的人员、部门	
	 * @param  [type] $com_id    
	 * @param  [type] $user_id   
	 * @param  array  $user_list 
	 * @param  array  $dept_list 
	 */
	public function update_recent_choose($com_id, $user_id, $user_list=array(), $dept_list=array()) {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id
			);

		$data = array(
			'user_list' 	=> json_encode($user_list),
			'dept_list' 	=> json_encode($dept_list),
			'update_time' 	=> time()
			);

		return parent::update_by_cond($cond, $data, '', false);
	}
	
}
//end