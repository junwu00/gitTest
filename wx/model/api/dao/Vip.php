<?php
/**
 * vip功能
 * @author Luja
 * @date 2016-10-18
 *
 */
namespace model\api\dao;

class Vip extends \model\api\dao\DaoBase {
	
	/** 企业vip信息表 */
	private static $Table = 'sc_company_vip';
	/** 企业信息表 */
	private static $TableCom = 'sc_company';
	/** 企业vip活动表 */
	private static $TableAd = 'sc_com_vip_ad';

	/** 是svip */
	private static $IsSvip = 1;
	/** 非svip */
	private static $NotSvip = 0;

	/** 是vip */
	private static $IsVip = 1;
	/** 非vip */
	private static $NotVip = 0;


	/** 启用 */
	private static $StateOn = 1;
	/** 禁用 */
	private static $StateOff = 0;

	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	/**
	 * 是否是vip（包括svip）
	 * @param  [type]  $com_id 
	 * @param  boolean $svip   true：仅判断svip
	 * @return boolean         
	 */
	public function check_vip($com_id, $svip=FALSE) {
		$cond = array(
			'com_id=' => $com_id,
			'__OR' => array(
				array(
					'is_svip=' => self::$IsSvip
					),
				),
			);
		if (!$svip) {
			$cond['__OR'][] = array(
				'is_vip=' => self::$IsVip,
				'past_time>' => time()
				);
		}
		

		$ret = $this -> get_by_cond($cond, '1');
		return $ret ? TRUE : FALSE;
	}


	/**
	 * 获取vip信息(单表)
	 * @param  [type]  	$com_id 
	 * @param  [type] 	$fields
	 * @return [type]         
	 */
	public function get_vip_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			);

		return $this -> get_by_cond($cond, $fields);
	}

	/**
	 * 获取vip活动列表
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_vip_activities($fields='*') {
		$cond = array(
			'start_time<=' => time(),
			'end_time>=' => time(),
			'state=' => self::$StateOn
			);
		$order = " ORDER BY idx ASC ";

		$ret = g('pkg_db') -> select(self::$TableAd, $fields, $cond, 0, 0, '', $order);
		return $ret ? $ret : array();
	}

}
//end of file