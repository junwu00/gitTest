<?php
/**
 * 企业信息
 * @author yangpz
 * @date 2016-02-19
 */
namespace model\api\dao;

class ComBlacklist extends \model\api\dao\DaoBase {
	
	/** 对应的库表名称 */
	private static $Table = 'sc_com_blacklist';

	/** @var int 表单黑名单 */
	const TYPE_FORM = 1;

	public function checkFormBlackList($com_id)
    {
        $cond = array(
            'type=' => self::TYPE_FORM,
        );
        $ret = g('pkg_db') -> select_one(self::$Table, ["com_ids"], $cond);
        if (!$ret) {
            return true;
        }
        $com_ids = $ret['com_ids']?json_decode($ret['com_ids'],true):[];
        if (!in_array($com_id,$com_ids)){
            return true;
        }
        return false;
    }
}

//end