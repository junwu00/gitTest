<?php
/**
 * 功能点服务类
 * @author chenyihao
 * @date 2016-11-11
 *
 */
namespace model\api\dao;

class Service extends \model\api\dao\DaoBase {
	
	/** 对应的库表 */
	public static $TABLE = 'ser_open_info';
	
	/** 禁用 0 */
	public static $STATE_OFF 		= 0;
	/** 启用 1 */
	public static $STATE_ON 		= 1;
	/** 删除 2 */
	public static $STATE_DEL 		= 2;
	/** 未激活 3 */
	public static $STATE_NOT_ACTIVE = 3;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$TABLE);
	}
	
	/** 获取当前使用所有功能 */
	public function get_all_privilege($com_id){
		$cond = array(
				'com_id=' => $com_id,
				'__OR' => array(
						'buyout=' => 1,
						'past_time>' => time(),
					),
			);
		$fields = 'service_privilege';
		$data = $this -> list_by_cond($cond, $fields);

		$privilege_list = array();
		foreach ($data as $val) {
			$privilege_list[] = $val['service_privilege'];
		}unset($val);

		return $privilege_list;
	}

	/** 检查是否拥有指定功能的权限 */
	public function check_privilege($com_id, $privilege_name){
		$cond = array(
				'com_id=' => $com_id,
				'service_privilege=' => $privilege_name,
				'__OR' => array(
						'buyout=' => 1,
						'past_time>' => time(),
					),
			);
		$fields = 'id';
		$privilege = $this -> get_by_cond($cond, $fields);
		return empty($privilege) ? FALSE : TRUE;
	}

}

//end