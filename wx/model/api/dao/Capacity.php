<?php
/**
 * 容量相关的数据模型
 *
 * @author LiangJianMing
 * @create 2015-09-24
 */
namespace model\api\dao;

class Capacity extends \model\api\dao\DaoBase {
    //是否打开企业容量检测
    private $open_check = FALSE;

    //企业信息表
    private $com_table = 'sc_company';
    //用户信息表
    private $user_table = 'sc_user';
    //企业额外信息表
    private $com_info_table = 'sc_company_info';
    //企业容量清单表
    private $list_table = 'sc_file_list';

    public function __construct() {
        isset($_SESSION[SESSION_VISIT_COM_ID]) and $this -> set_init($_SESSION[SESSION_VISIT_COM_ID]);
    }

    /**
     * 配置初始化信息
     *
     * @access public
     * @param integer $com_id 企业id
     * @return void
     */
    public function set_init($com_id) {
        $this -> com_id = $com_id;
    }

    /**
     * 将上传文件添加到清单列表
     *
     * @access public
     * @param array $data 数据集合
     *  => integer com_id 归属的企业id
     *  => string app_src 应用英文名称
     *  => string file_name 上传时的文件名称，不带扩展名
     *  => string search_hash 云存储的查询哈希
     *  => string file_hash 文件内容哈希
     *  => string ext 文件扩展名
     *  => string file_size 文件大小
     *  => string state 状态：1-正常、2-已删除
     *  => string entity_table 记录文件实体在哪一张数据表中
     * @param boolean $active 如果记录已经存在，是否更新状态
     * @param array $exists_condition 重写用作判断是否已存在的条件
     * @return boolean
     */
    public function add_list(array $data, $active=false, array $exists_condition=array()) {
        empty($data['com_id']) and $data['com_id'] = $this -> com_id;
        !isset($data['state']) and $data['state'] = 1;

        $time = time();
        $data['create_time'] = $time;
        $data['update_time'] = $time;

        if (empty($exists_condition)) {
            $condition = array(
                'com_id=' => $data['com_id'],
                'app_src=' => $data['app_src'],
                'file_hash=' => $data['file_hash'],
                'ext=' => $data['ext'],
                'entity_table=' => $data['entity_table'],
                'table_id=' => isset($data['table_id']) ? $data['table_id'] : 0,
            );
        }else {
            $condition = $exists_condition;
        }

        $check = g('pkg_db') -> check_exists($this -> list_table, $condition);
        if ($check) {
            if ($active) {
                $up_data = array(
                    'state' => $data['state'],
                    'file_name' => $data['file_name'],
                );
                $result = $this -> update_list($condition, $up_data);
                return $result;
            }

            return TRUE;
        }

        $result = g('pkg_db') -> insert($this -> list_table, $data);
        return $result ? $result : FALSE;
    }

    /**
     * 从缓存中读取文件并加入到企业容量文件列表中
     *
     * @access public
     * @param integer $com_id 企业id
     * @param string $key 两种赋值：1、云存储中的查询哈希；2、云存储中的文件存储相对路径；
     * @param string $app_src 来源应用英文名
     * @param string $entity_table 记录文件实体在哪一张数据表中
     * @param integer $table_id 实体所在数据表中的主键id
     * @return boolean
     */
    public function add_list_by_cache($com_id, $key, $app_src, $entity_table, $table_id) {
        empty($com_id) and $com_id = $this -> com_id;
        
        $info = $this -> get_capacity_cache($key);
        if (empty($info)) return false;

        $data = array(
            'com_id' => $com_id,
            'app_src' => $app_src,
            'file_name' => empty($info['origin_file_name']) ? basename($info['path']) : $info['origin_file_name'],
            'file_hash' => $info['file_hash'],
            'search_hash' => $info['hash'],
            'ext' => $info['ext'],
            'file_size' => $info['origin_size'],
            'entity_table' => $entity_table,
            'table_id' => $table_id,
            'state' => 1,
        );

        $result = $this -> add_list($data, true);
        return $result;
    }

    /**
     * 从缓存中读取文件，并将企业容量文件的状态变更为删除
     *
     * @access public
     * @param integer $com_id 企业id
     * @param string $key 两种赋值：1、云存储中的查询哈希；2、云存储中的文件存储相对路径；
     * @param string $app_src 来源应用英文名
     * @param string $entity_table 记录文件实体在哪一张数据表中
     * @param integer $table_id 实体所在数据表中的主键id
     * @return boolean
     */
    public function delete_file_by_cache($com_id, $key, $app_src, $entity_table, $table_id) {
        empty($com_id) and $com_id = $this -> com_id;
        
        $info = $this -> get_capacity_cache($key);
        if (empty($info)) return false;

        $condition = array(
            'com_id=' => $com_id,
            'app_src=' => $app_src,
            'file_hash=' => $info['file_hash'],
            'ext=' => $info['ext'],
            'entity_table=' => $entity_table,
            'table_id=' => $table_id,
        );

        $data = array(
            'state=' => 2,
        );

        $result = $this -> update_list($condition, $data);
        return $result;
    }

    /**
     * 更新对应的文件列表
     *
     * @access public
     * @param array $condition 条件集合
     * @param array $data 数据集合
     * @return boolean
     */
    public function update_list(array $condition, array $data) {
        empty($condition['com_id=']) and $condition['com_id='] = $this -> com_id;

        unset($data['id']);
        unset($data['com_id']);

        $data['update_time'] = time();

        $result = g('pkg_db') -> update($this -> list_table, $condition, $data);
        return $result ? TRUE : FALSE;
    }

    /**
     * 获取企业的容量信息
     *
     * @access public
     * @param integer $com_id 企业id
     * @return array
     */
    public function get_capacity_info($com_id=0) {
        empty($com_id) and $com_id = $this -> com_id;

        $key_str = 'capacity_info:' . $com_id;
        $cache_key = $this -> get_cache_key($key_str);

        $ret = g('pkg_redis') -> get($cache_key);
        if (!empty($ret)) {
            return json_decode($ret, TRUE);
        }

        $fields = array(
            'member_capacity', 'file_capacity', 'base_capacity',
            'vip_capacity', 'other_capacity', 'buy_capacity',
            'max_capacity', 'vip_level', 'capacity_time',
            'file_correct_time',
        	'is_full', 'notice_time', 'buffer_time', 'pre_buffer_time',
        );
        $fields = implode(',', $fields);

        $cond = array(
                'com_id=' => $com_id,
            );
        $ret = g('pkg_db') -> select_one($this -> com_info_table, $fields, $cond);
        
        if (is_array($ret)) {
            //已使用量
            $ret['used_capacity'] = $ret['member_capacity'] + $ret['file_capacity'];
            //剩余容量
            $ret['balance_capacity'] = $ret['max_capacity'] - $ret['used_capacity'];

            //用作展示的携带单位容量
            $ret['member_capacity_str'] = $this -> prase_humen_size($ret['member_capacity']);
            $ret['file_capacity_str'] = $this -> prase_humen_size($ret['file_capacity']);
            $ret['base_capacity_str'] = $this -> prase_humen_size($ret['base_capacity']);
            $ret['vip_capacity_str'] = $this -> prase_humen_size($ret['vip_capacity']);
            $ret['other_capacity_str'] = $this -> prase_humen_size($ret['other_capacity']);
            $ret['buy_capacity_str'] = $this -> prase_humen_size($ret['buy_capacity']);
            $ret['max_capacity_str'] = $this -> prase_humen_size($ret['max_capacity']);
            $ret['used_capacity_str'] = $this -> prase_humen_size($ret['used_capacity']);
            $ret['balance_capacity_str'] = $this -> prase_humen_size($ret['balance_capacity']);
            $ret['info_time'] = time();

            g('pkg_redis') -> set($cache_key, json_encode($ret), 3600);
        }else {
            $ret = array();
        }

        return $ret;
    }

    /**
     * 检测可用剩余容量，一旦使用量已满，跳转到提醒页面
     *
     * @access public
     * @param integer $com_id 企业id
     * @param boolean $jump 是否跳转
     * @return void
     */
    public function check_balance($com_id=0, $jump=true) {
        empty($com_id) and $com_id = $this -> com_id;

        $info = $this -> get_capacity_info($com_id);

        $check = true;
        if (!empty($info)) {
            if ($info['max_capacity'] <= $info['member_capacity'] + $info['file_capacity']) {
                $check = false;
            }
        }

        if (!$check and $jump and $this -> open_check) {
            throw new \Exception("企业容量使用已到达上限，请添加容量后再使用。");
        }

        return $check;
    }

    /**
     * 任何文件上传后，调用该方法设置文件属性
     *
     * @access public
     * @param string $key_word 可以有两种赋值：1、云存储中的查询哈希；2、云存储中的文件存储相对路径；
     * @param array $data 云存储上传接口返回的数据部分
     * @return boolean
     */
    public function set_capacity_cache($key_word, array $data) {
        if (empty($key_word) or empty($data)) return false;
        $key_str = 'capacity_cache::' . $key_word;
        $cache_key = $this -> get_cache_key($key_str);
        //仅缓存一小时
        g('pkg_redis') -> set($cache_key, json_encode($data), 3600);
        return true;
    }

    /**
     * 调用该方法获取之前上传过的文件
     *
     * @access public
     * @param string $key_word 可以有两种赋值：1、云存储中的查询哈希；2、云存储中的文件存储相对路径；
     * @return array
     */
    public function get_capacity_cache($key_word) {
        $ret = array();
        if (empty($key_word)) return $ret;
        $key_str = 'capacity_cache::' . $key_word;
        $cache_key = $this -> get_cache_key($key_str);

        $ret = g('pkg_redis') -> get($cache_key);
        $ret = json_decode($ret, true);

        //如果获取不到，调用云存储的接口查询该信息
        if (empty($ret)) {
            $func = 'get_info_by_search_hash';
            strlen($key_word) != 32 and $func = 'get_info_by_path';
            try {
                $info = g('api_media') -> $func($key_word);
            }catch(\Exception $e) {
                return $ret;
            }

            //获取后重新缓存
            g('pkg_redis') -> set($cache_key, json_encode($info), 3600);

            $ret = $info;
        }

        return $ret;
    }

    /**
     * 获取缓存变量名
     *
     * @access public
     * @param string $str 关键字符串
     * @return string
     */
    public function get_cache_key($str) {
        $domain = trim(MAIN_ROOT, '\\/');
        $domain = basename($domain);
        $key_str = $domain . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' . $this -> com_id . ':' . $str;
        $key = md5($key_str);
        return $key;
    }

//=====================================私有方法================================
    /**
     * 将字节数转换为带单位的字符串
     *
     * @access 
     * @param integer $size 字节大小
     * @return string
     */
    private function prase_humen_size($size) {
        $val = floatval($size);

        $preffix = '';
        if ($val < 0) {
            $preffix = '-';
            $val = abs($val);
        }

        $unit = 1;
        while (($val / 1024) > 1) {
            $val = $val / 1024;
            $unit++;
        }

        $ret = round($val, 2);

        switch ($unit) {
            case 1 : {
                $ret .= 'B';
                break;
            }
            case 2 : {
                $ret .= 'KB';
                break;
            }
            case 3 : {
                $ret .= 'MB';
                break;
            }
            case 4 : {
                $ret .= 'GB';
                break;
            }
            case 5 : {
                $ret .= 'TB';
                break;
            }
        }

        $ret = $preffix . $ret;

        return $ret;
    }

}

/* End of this file */