<?php
/**
 * 成员信息数据库操作类
 * @author yangpz
 * @date 2016-02-18
 *
 */
namespace model\api\dao;

class User extends \model\api\dao\DaoBase {
	
	private static $Table = 'sc_user';
	
	/** 已删除 */
	public static $StateDelete = 0;
	/** 已关注 */
	public static $StateSubscribe = 1;
	/** 已冻结 */
	public static $StateFrozen = 2;
	/** 未关注 */
	public static $StateUnsubscribe = 4;
	
	/** 组织架构过滤 */
	private $struct_filter = FALSE;

	/** 通讯录应用过滤 */
	private $contact_filter = FALSE;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	
	/**
	 * 设置组织架构配置
	 * @param unknown_type $open_filter	是否开启过滤
	 * @param array $conf				过滤配置信息
	 */
	public function set_struct($open_filter=TRUE, array $conf=array()) {
		$this -> struct_filter = $open_filter;
		parent::set_conf($conf);
	}
	
	/**
	 * 设置组通讯录应用过滤配置
	 * @param unknown_type $open_filter	是否开启过滤
	 * @param array $conf				过滤配置信息
	 */
	public function set_contact($open_filter=TRUE, array $conf=array()) {
		$this -> contact_filter = $open_filter;
		parent::set_conf($conf);
	}
	
	//GET-----------------------------------------------------------------------------------------------
    /** 根据主部门获取员工 */
    public function get_by_main_dept($root_id, $dept_ids=array(), $fields='*') {
        if (empty($dept_ids)) return array();

        $state = self::$StateDelete;
        $sql = " SELECT {$fields} 
				FROM sc_user 
				WHERE root_id={$root_id} AND state!={$state} 
				AND (";

        foreach ($dept_ids as $id) {
            $sql .= " dept_list LIKE '%[\"{$id}\"%' OR";
        }
        $sql = substr($sql, 0, -2);
        $sql .= ")";

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret : array();
    }
	
	/**
	 * 根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	public function get_by_cond(array $cond, $fields='*') {
		if (!isset($cond['root_id=']) || empty($cond['root_id='])) {
			throw new \Exception('未指定企业根部门ID');
		}
		return parent::get_by_cond($cond, $fields);
	}
	
	/**
	 * 根据成员账号获取成员信息
	 * @param unknown_type $acct		成员账号
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $state		状态，多个用逗号分隔，如：1,2
	 * @param unknown_type $with_del	是否包含被删除的记录,默认为否
	 */
	public function get_by_acct($acct, $fields='*', $state=NULL, $with_del=FALSE, $root_id='') {
	    empty($root_id) && $root_id = $this -> root_id;
		$cond = array(
			'root_id='	=> $root_id,
			'acct='		=> $acct,
		);
		if (!is_null($state)) {		//指定状态
			$cond['state IN'] = explode(',', $state);
			
		} else if (!$with_del) {	//未指定状态，但不显示删除的记录
			$cond['state !='] = self::$StateDelete;
		}
		return parent::get_by_cond($cond, $fields);
	}
	
	/**
	 * 根据成员ID获取成员信息
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $state		状态，多个用逗号分隔，如：1,2
	 * @param unknown_type $with_del	是否包含被删除的记录,默认为否
	 */
	public function get_by_id($id, $fields='*', $state=NULL, $with_del=FALSE) {
		$cond = array(
			//'root_id='	=> $this -> root_id,
			'id='		=> $id
		);
		if (!is_null($state)) {		//指定状态
			$cond['state IN'] = explode(',', $state);
			
		} else if (!$with_del) {	//未指定状态，但不显示删除的记录
			$cond['state !='] = self::$StateDelete;
		}
		return parent::get_by_cond($cond, $fields);
	}
	public function getById($id,$fields="*", $state=NULL, $with_del=FALSE){
        $cond = array(
            'id='		=> $id
        );
        if (!is_null($state)) {		//指定状态
            $cond['state IN'] = explode(',', $state);

        } else if (!$with_del) {	//未指定状态，但不显示删除的记录
            $cond['state !='] = self::$StateDelete;
        }
        return parent::get_by_cond($cond, $fields);
    }
    public function getByIds($ids,$fields="*", $state=NULL, $with_del=FALSE){
	    if (!$ids) return [];

        $cond = array(
            'id IN'		=> $ids
        );
        if (!is_null($state)) {		//指定状态
            $cond['state IN'] = explode(',', $state);

        } else if (!$with_del) {	//未指定状态，但不显示删除的记录
            $cond['state !='] = self::$StateDelete;
        }
        return parent::list_by_cond($cond, $fields);
    }
	/** 根据id数组获取员工信息 */
	public function get_by_ids($root_id, $ids, $fields, $with_del=FALSE){
		$cond = array(
				'id IN' => $ids,
			);

        if($root_id){
            $cond['root_id='] = $root_id;
        }

		if(!$with_del){
			$cond['state!='] = self::$StateDelete;
		}
		return parent::list_by_cond($cond, $fields);
	}

	/**
	 * 获取成员主部门信息/仅ID
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	是否仅获取部门ID，默认为TRUE
	 * 
	 * @return 
	 * $only_dept_id为TRUE时，成员主部门ID，找不到则返回FALSE
	 * $only_dept_id为FALSE时，成员主部门信息，找不到则返回FALSE
	 */
	public function get_main_dept($id, $only_dept_id=TRUE, $dept_field = '*') {
		$dept_list = $this -> list_all_dept($id, TRUE);
		$main_dept_id = empty($dept_list) ? FALSE : array_shift($dept_list);
		if ($only_dept_id) {
			return $main_dept_id;
		}
		
		if (empty($main_dept_id))		return FALSE;
		
		$api_dept = $this -> _get_api_dept_instance();
		return $api_dept -> get_by_id($main_dept_id, $dept_field);		//前面已进行虚拟架构过滤，此处不再过滤
	}

	//LIST-----------------------------------------------------------------------------------------------
	
	/**
	 * (批量)根据指定条件获取成员信息
	 * @param array $cond		查询条件
	 * @param unknow $fields	查询字段
	 */
	public function list_by_cond(array $cond, $fields='*', $page=0, $page_size=0, $order = '') {
//		if (!isset($cond['root_id=']) || empty($cond['root_id='])) {
//			throw new \Exception('未指定企业根部门ID');
//		}
		
		return parent::list_by_cond($cond, $fields, $page, $page_size, $order);
	}
	
	/**
	 * 根据ID集合获取成员列表
	 * @param array $ids				成员ID数组
	 * @param unknown_type $fields		查找的字段，默认为全部
	 * @param unknown_type $with_del	是否包含被删除的记录,默认为否
	 */
	public function list_by_ids(array $ids, $fields='*', $with_del=FALSE, $order = '') {
		if (empty($ids))		return array();
		
		$cond = array(
			//'root_id='	=> $this -> root_id,
			'id IN' 	=> $ids,
		);

		!$with_del && $cond['state!='] = self::$StateDelete;

		return parent::list_by_cond($cond, $fields, 0, 0, $order);
	}

	/**
	 * 获取成员所有部门信息/仅ID
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	是否仅获取部门ID，默认为FALSE
	 * 
	 * @return 
	 * $only_dept_id为TRUE时，成员所有部门ID，找不到则返回空数组
	 * $only_dept_id为FALSE时，成员所有部门信息，找不到则返回空数组
	 */
	public function list_all_dept($id, $only_dept_id=TRUE) {
		$user = $this -> get_by_id($id, '*', NULL, TRUE);
		if (empty($user))				throw new \Exception('成员不存在');
		
		$dept_list = json_decode($user['dept_list'], TRUE);
		!is_array($dept_list) && $dept_list = array();
		if ($only_dept_id) {
			return $dept_list;
		}
		
		if (empty($dept_list))		return array();
		
		$api_dept = $this -> _get_api_dept_instance();
		return $api_dept -> list_by_ids($dept_list, '*');
	}
	
	/**
	 * 根据ID获取成员的部门及其父..级部门（无序）
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	仅返回部门ID/返回部门信息
	 */
	public function list_all_parent_dept($id, $only_dept_id=TRUE) {
		$all_dept_ids = $this -> list_all_dept($id);
		$api_dept = $this -> _get_api_dept_instance();
		$parent_dept_ids = $api_dept -> list_all_parent_ids($all_dept_ids);
		$p_ids = array();
		foreach ($parent_dept_ids as $p) {
			foreach ($p as $s) {
				$p_ids[$s] = $s;
			}unset($s);
		}unset($p);
		$p_ids = array_values($p_ids);
		
		if ($only_dept_id)	return $p_ids;
		
		return $api_dept -> list_by_ids($p_ids, '*');
	}

	/**
	 * 根据部门ID，获取部门直属成员列表
	 * @param unknown_type $dept_id 	部门ID
	 * @param unknown_type $fields 		查找的字段，默认为：全部字段
	 * @param unknown_type $sort_boss 	是否标识部门负责人并置顶，默认为：TRUE
	 * 
	 * @return 成员信息列表，无记录时返回空数组
	 */
	public function list_direct_by_dept_id($dept_id, $fields='*', $sort_boss=TRUE) {
		$api_dept = $this -> _get_api_dept_instance();
		
		$dept = $api_dept -> get_by_id($dept_id, 'boss, second_boss');
		if(empty($dept))	return array();

		//部门负责人置顶
		$order = ' state, all_py ';
		/*
		if ($sort_boss) {
			$boss_ids = array();
			$second_boss = json_decode($dept['second_boss'], TRUE);
			!is_array($second_boss) && $second_boss = array();
	        !empty($dept['second_boss']) && $boss_ids = array_merge($boss_ids,$second_boss);
			!empty($dept['boss']) && $boss_ids = array_merge($boss_ids, array($dept['boss']));
			!empty($boss_ids) && $order = ' FIELD(id, '.implode(',', $boss_ids).') DESC ,state, all_py ';
		}
		*/
		
		$cond = array(
			'root_id='			=> $this -> root_id,
			'state!='			=> self::$StateDelete,
			'dept_list REGEXP' 	=> '"('. $dept_id .')"'
		);
		$data = parent::list_by_page($cond, $fields, 0, 0, $order);
		
		/*
		//添加部门负责人标识
		if ($sort_boss) {
			foreach ($data as &$user) {
				if(!empty($dept['boss']) && $user['id'] == $dept['boss']){
					$user['boss'] = true;
				}
				if(in_array($user['id'], $second_boss)){
					$user['second_boss'] = true;	
				}
			}
		}
		*/

		return $data;
	}

	/**
	 * 分页获取成员信息(含子部门成员)
	 * @param unknown_type $dept_ids	要查找的部门
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $params		要进行匹配搜索的字段信息
	 * @param unknown_type $with_child	是否包含子部门成员，默认为FALSE
	 * @param unknown_type $fields		查找的字段， 默认为全部
	 * @param unknown_type $order		排序
	 */
	public function page_list_by_dept_ids(array $dept_ids, $page=1, $page_size=50, array $params=array(), $fields='*', $order='', $with_count=TRUE) {
		$empty_resp = array('data' 	=> array(),	'count' => 0);
		
		$has_root = FALSE;	//当前查找的部门是否有根部门（或虚拟根部门），有则需要架构过滤
		foreach ($dept_ids as $id) {
			if ($id == $this -> root_id || $id <= 0) {
				$has_root = TRUE;
				break;
			}
		}unset($id);
		
		if (empty($dept_ids)) 	return $with_count ? $empty_resp : array();
		
		$cond = array(
			'root_id=' 	=> $this -> root_id,
			'state!=' 	=> self::$StateDelete,
		);

		// gch add contact config
		$hiddenConfig = g('dao_contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
		if (!empty($hiddenConfig['user'])) {
			// $cond['__OR_99'] = array(
			// 	'id NOT REGEXP' => '(' . implode('|', $hiddenConfig['user']) . ')',
			// );
			$cond['id NOT REGEXP'] = '(' . implode('|', $hiddenConfig['user']) . ')';
		}
		// if (!empty($hiddenConfig['dept'])) {
		// 	$cond['dept_tree NOT REGEXP'] = "\"(" . implode('|', $hiddenConfig['dept']) . ")\"";
		// 	// $cond['__OR_99']['dept_tree NOT REGEXP'] = "\"(" . implode('|', $hiddenConfig['dept']) . ")\"";
		// }
		$watchOther = g('dao_contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
		if ($watchOther['flag'] && !empty($watchOther['user'])) {
			$cond['__OR_100'] =  array(
				'id REGEXP' => '(' . implode('|', $watchOther['user']) . ')',
			);
		}
		if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$cond['__OR_100']['dept_tree REGEXP'] = "\"(" . implode('|', $watchOther['dept']) . ")\"";
		}

        //虚拟结构条件
        if ($has_root && $this -> struct_filter) {
	        //查询虚拟结构条件
	        $st_con = g('dao_contact') -> get_struct_condition();
	        if (empty($st_con)) {
	            return $empty_resp;
	        }
	        $cond['__OR_123'] = $st_con;
	        
        } else {
			// 使用正则表达式
      		$ret['dept_tree REGEXP'] = '"(' . implode('|', $dept_ids) . ')"';
			$cond['__OR_321'] = $ret;
        }
        
        //通讯录应用过滤
        if ($this -> contact_filter) {
        	$conf = g('dao_contact') -> get_contact_setting();
        
	        $search = $conf['search_list'];
	        $secret = $conf['secret_list'];
	
	        $tmp_secret = array();
	        foreach ($secret as $val) {
	            $tmp = intval($val);
	            !empty($tmp) and $tmp_secret[] = $tmp;
	        }
	        unset($tmp);
	        $secret = $tmp_secret;		//保密成员
	        !empty($secret) and $cond['id NOT IN'] = $secret;
	        
	        if (isset($params['keyword'])) {
	        	$key_word = ($params['keyword']);
	        	if (!empty($key_word)) {
		            $or_con = array();
		            in_array('name', $search) && $or_con['name LIKE '] = '%' . $key_word . '%';
		            in_array('acct', $search) && $or_con['acct LIKE '] = '%' . $key_word . '%';
		            in_array('position', $search) && $or_con['position LIKE '] = '%' . $key_word . '%';
		            in_array('wx_acct', $search) && $or_con['weixin_id LIKE '] = '%' . $key_word . '%';
		            in_array('phone', $search) && $or_con['mobile LIKE '] = '%' . $key_word . '%';
		            in_array('tel', $search) && $or_con['tel LIKE '] = '%' . $key_word . '%';
		
		            !empty($or_con) and $cond['__OR_1'] = $or_con;
	        	}
	        }
	        
        } else {
        	if (isset($params['keyword'])) {
	        	$key_word = ($params['keyword']);
	        	if (!empty($key_word)) {
        			$params['__OR_2'] = array(
	        			'name LIKE ' 		=> '%' . $key_word . '%',
	        			'all_py LIKE ' 		=> '%' . $key_word . '%',
	        			'first_py LIKE '	=> '%' . $key_word . '%',
        			);
        		}
        	}
        }
        
		$page_size > 500 && $page_size = 500;		//一次最多查询的数据量
		empty($order) && $order = ' all_py ';
		
		if (!empty($params)) {
			foreach ($params as $column => $val) {
				if ($column === 'keyword')	continue;
				
				$cond[$column] = $val;
			}unset($column);unset($val);
		}
		
		$users = $this -> list_by_page($cond, $fields, $page, $page_size, " order by {$order} ");
		$users = $users ? $users : array();
		if (!$with_count) {
			return $users;
		}
		
		$where = g('pkg_db') -> compose_prepare_where($cond);
		$sql = 'SELECT ' . $fields . ' FROM ' . self::$Table;
        $where['sql'] !== '' && $sql .= " WHERE {$where['sql']}";

		$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') counta';
		$count = g('pkg_db') -> prepare_query($sql, 1, $where['param']);
		$count = $count[0]['count'];
		return array(
			'data' 	=> $users,
			'count' => $count,
		);
	}
	
	/**
	 * 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用）
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $only_ids	仅返回成员/部门ID 或 返回成员/部门信息，默认返回ID
	 * @param unknown_type $dept_fields	查找的部门字段
	 * @param unknown_type $user_fields	查找的成员字段
	 */
	public function list_sub_by_id($id, $only_ids=TRUE, $dept_fields='id,name', $user_fields='id,name,pic_url') {
		$api_leader = $this -> _get_api_leader_instance();
		return $api_leader -> list_sub_by_user_id($id, $only_ids, $dept_fields, $user_fields);
	}
	
	/**
	 * 获取指定成员的所有领导
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 * @param unknown_type $user_fields	查找的字段（成员）
	 */
	public function list_leader_by_id($id, $only_ids=TRUE, $user_fields='id,name,pic_url,dept_list') {
		$api_leader = $this -> _get_api_leader_instance();
		return $api_leader -> list_leader_by_user_id($id, $only_ids, $user_fields);
	}
	
	//UPDATE-----------------------------------------------------------------------------------------------
	
	/**
	 * 根据成员账号修改其状态
	 * @param unknown_type $acct	成员账号
	 * @param unknown_type $state	状态
	 */
	public function change_state_by_acct($acct, $state) {
		$cond = array(
			'root_id=' 	=> $this -> root_id,
			'acct=' 	=> $acct,
			'state!='	=> self::$StateDelete
		);
		$data = array(
			'state'			=> $state,
			'update_time'	=> time()
		);
		return parent::update_by_cond($cond, $data, '更新成员状态失败');
	}
	
	/**
	 * 根据成员账号更新员工头像及微信号
	 * @param unknown_type $acct	成员账号
	 */
	public function update_pic_and_wx_by_acct($token, $root_id, $acct) {
		$user = g('wxqy_user') -> get_user($token, $acct);
		$cond = array(
			'root_id=' 	=> $root_id,
			'acct=' 	=> $acct,
			'state!='	=> self::$StateDelete,
		);
		$data = array(
			'pic_url' 		=> isset($user['avatar']) ? $user['avatar'] : '',
			'weixin_id' 	=> $user['weixinid'],
			'update_time' 	=> time(),
		);
		return parent::update_by_cond($cond, $data, '更新员工头像失败');
	}
	
	//内部实现----------------------------------------------------------------------------------------------------

    /**
     * 获取缓存变量名
     * @param string $str 关键字符串
     * @return string
     */
    protected function _get_cache_key($str) {
    	$filter_str = $this -> struct_filter ? '1' : '0';
        $key_str = $this -> cache_key_prefix . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str.':'.$filter_str;
        return md5($key_str);
    }
    
	/**
	 * 根据当前信息，获取部门数据库操作类
	 */
	private function _get_api_dept_instance() {
		$instance = g('dao_dept');
		$conf = array(
			'com_id'	=> $this -> com_id,
			'root_id'	=> $this -> root_id,
			'user_id'	=> $this -> user_id
		);
		$instance -> set_struct($this -> struct_filter, $conf);
		return $instance;
	}
	
	/**
	 * 根据当前信息，获取上下级关系数据库操作类
	 */
	private function _get_api_leader_instance() {
		$instance = g('dao_leader');
		$conf = array(
			'com_id'	=> $this -> com_id,
			'root_id'	=> $this -> root_id,
			'user_id'	=> $this -> user_id
		);
		$instance -> set_struct($this -> struct_filter, $conf);
		return $instance;
	}

	/**
	* 获取用户的通讯录信息
	*   
	* @access public
	* @param   
	* @return   
	*/
	public function getUserContact($comId, $userId) {
		$return = array(
			'user_id' => $userId,
			'main_dept' => 0,
			'dept_list' => array(),
			'dept_tree' => array(),
			'group_id' => array(),
		);

		$userCondition = array(
			'com_id=' => $comId,
			'id=' => $userId,
		);
		$userInfo = g('pkg_db') -> select('sc_user', 'id, dept_list, dept_tree', $userCondition);
		if (!empty($userInfo)) {
			$userInfo = array_shift($userInfo);
			$deptList = json_decode($userInfo['dept_list'], TRUE);
			$userInfo['dept_tree'] = json_decode($userInfo['dept_tree'], TRUE);
			$deptTree = array();
			foreach ($userInfo['dept_tree'] as $v) {
				$deptTree = array_merge($deptTree, $v);
			}
			$deptTree = array_unique($deptTree);

			$return['main_dept'] = reset($deptList);
			$return['dept_list'] = $deptList;
			$return['dept_tree'] = $deptTree;
		}

		$groupCondition = array(
			'user_list like ' => '%"' . $userId . '"%',
			'com_id=' => $comId,
			'info_state=' => 1,
		);
		$groupInfo = g('pkg_db') -> select('sc_group', 'id', $groupCondition);
		if (!empty($groupInfo)) {
			$return['group_id'] = array_column($groupInfo, 'id');
		}

		return $return;
	}

    /**
     * 获取指定部门的下的员工信息
     * $with_child 是否包含子部门
     **/
    public function get_by_depts($root_id,$depts,$fields='*',$with_child=false){
        $col_name = 'dept_list';
        if($with_child){
            $col_name = 'dept_tree';
        }

        $cond = array(
            //'root_id=' => $root_id,
            'state!=' => self::$StateDelete,
        );

        if($root_id){
            $cond['root_id='] = $root_id;
        }

        $cond['__OR'] = array();
        foreach($depts as $key => $d){
            $cond['__OR']['__'.$key.'__'.$col_name.' like '] = '%"'.$d.'"%';
        }
        $users = $this -> list_by_cond($cond, $fields);
        return empty($users) ? array() : $users;
    }

    /**
     * 记录用户登录信息
     * @param $com_id
     * @param $user_id
     * @return mixed
     */
    public function saveLoginLog($com_id,$wxacct){
        $cond = array(
            'com_id='=>$com_id,
            'acct='=>$wxacct,
        );
        $user = parent::get_by_cond($cond, 'id');
        if(!$user){
            return false;
        }
        $tabel = 'sc_user_login_log';
        $time = time();
        $save_data = array(
            'com_id'=>$com_id,
            'user_id'=>$user['id'],
            'info_state'=>1,
            'login_time'=>$time,
            'create_time'=>$time,
            'update_time'=>$time,
        );
        $ret = g('pkg_db')->insert($tabel,$save_data);
        return $ret;
    }
	
}

//end