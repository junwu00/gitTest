<?php
/**
 * 物资表单操作类
 * @author gch
 * @date 2018-01-23 09:45:34
 *
 */
namespace model\api\dao;

class Material extends \model\api\dao\DaoBase {
	
	/** 对应的库表 */
	public static $TABLE = 'mv_proc_material_model';
	public static $TABLE_LABEL = 'mv_proc_material_model_label';
	public static $TABLE_CONFIG = 'mv_proc_material_config';
    public static $TABLE_WARNING = 'mv_proc_material_warning';
	
	/** 删除/禁用 0 */
	private static $STATE_OFF 	= 0;
	/** 启用 1 */
	private static $STATE_ON 	= 1;

	/** 历史版本 1 */
	private static $VERSION_HISTORY	= 1;
	/** 发布版本 2 */
	private static $VERSION_USE 	= 2;
	
	public function __construct($conf=array()) {
		$this -> set_table(self::$TABLE);
	}
	public function getVersionUseId($id)
    {
        $cond = [
            "id="=>$id,
            "info_state="=>self::$STATE_ON,
        ];
        $res = g('pkg_db')->select_one(self::$TABLE,"*",$cond);
        if (!$res) {
            return 0;
        }
        if ($res['version'] == self::$VERSION_USE) {
            return $id;
        }

        $cond = [
            "history_id="=>$res['history_id'],
            "version="=>self::$VERSION_USE,
            "info_state="=>self::$STATE_ON,
        ];
        $res = g('pkg_db')->select_one(self::$TABLE,"*",$cond);
        return $res?$res['id']:0;
    }
	public function getMaterial($id,$fields="*")
    {
        $cond = [
            "id="=>$id,
            "info_state="=>self::$STATE_ON,
        ];
        $res = g('pkg_db')->select_one(self::$TABLE,$fields,$cond);
        return $res?$res:[];
    }
	/**
	 * 删除物资
	 * @param int $type	1-物资标签映射表
	 * 
	 * @return array
	 */
	public function deleteMaterial($com_id, $ids) {
		$cond = array(
			'id IN'	=> $ids,
			'com_id='	=> $com_id,
			'info_state='	=> self::$STATE_ON,
		);

		$data = array(
			'update_time'	=> time(),
			'info_state'	=> self::$STATE_OFF,
		);

		$return = g('pkg_db') -> update(self::$TABLE, $cond, $data);
		return $return;
	}

	/**
	* 新建物资
	*   
	* @access public
	* @param int com_id  
	* @param array materialData  物资数据 一维数组
	* @param array materialInputData  属性数据 二维数据
	* @param array subMaterialInputData  子表单属性数据 二维数据
	* @param int historyId  历史版本id
	* @return   
	*/
	public function saveMaterial($com_id, $materialData, $materialInputData, $historyId = 0){
		$now = time();

		// 新增物资
		$baseData = array(
			'update_time' => $now,
			'create_time' => $now,
			'info_state' => self::$STATE_ON,
			'version' => self::$VERSION_USE,
			'com_id' => $com_id,
		);
		$saveData = array_merge($baseData, $materialData);

		$return = g('pkg_db') -> insert(self::$TABLE, $saveData);		
		if (!$return) {
			to_log('Info', '', '新增物资失败:' . json_encode($saveData));
			return 0;
		}
		$materialId = intval($return);

		// 新增物资属性
		$saveData = array();
		$baseData = array(
			'update_time' => $now,
			'material_id' => $materialId,
			'p_label_id' => 0,
			'create_time' => $now,
			'info_state' => self::$STATE_ON,
			'com_id' => $com_id,
		);
		foreach ($materialInputData as $m) {
			$subMaterialInputData = $m['sub'];
			unset($m['sub']);
			$saveData = array_merge($baseData, $m);

			$labelId = g('pkg_db') -> insert(self::$TABLE_LABEL, $saveData);
			if (!$labelId) {
				to_log('Info', '', '新增物资属性失败:' . json_encode($saveData));
				continue;
			}
	
			// 新增物资子表单属性数据
			if (!empty($subMaterialInputData)) {
				$saveData = array();
				$baseData = array(
					'p_label_id' => $labelId,
					'material_id' => $materialId,
					'create_time' => $now,
					'update_time' => $now,
					'info_state' => self::$STATE_ON,
					'com_id' => $com_id,
				);
				foreach ($subMaterialInputData as $m) {
					$saveData[] = array_merge($baseData, $m);
				}
				$fields = array_keys($saveData[0]);
				$subLabelId = g('pkg_db') -> batch_insert(self::$TABLE_LABEL, $fields, $saveData);	
				if (!$subLabelId) {
					to_log('Info', '', '新增物资子表单属性失败:' . json_encode($saveData));
				}
			}

		}

		// 更新上一个历史物资的版本号
		if (!empty($historyId)) {
			$cond = array(
				'history_id=' => $historyId,
			);
			$saveData = array(
				'update_time' => $now,
				'version' => self::$VERSION_HISTORY,
			);
			$return = g('pkg_db') -> update(self::$TABLE, $cond, $saveData);	
			if (!$return) {
				to_log('Info', '', '更新上一个历史物资的版本号,历史id:' . $historyId);
			}
		}

		// 更新发布版本物资的历史版本id
		empty($historyId) && $historyId = $materialId;
		$cond = array(
			'id=' => $materialId,
		);
		$saveData = array(
			'history_id' => $historyId,
		);
		$return = g('pkg_db') -> update(self::$TABLE, $cond, $saveData);	
		if (!$return) {
			to_log('Info', '', '更新发布版本物资的历史版本id,历史id:' . $historyId);
		}

		return $materialId;
	}

	/**
	 * 获取物资
	 * @param int $com_id	企业根标签ID
	 * @param int $tag_ids	标签ID
	 * @param string $fields	查询的字段
	 * 
	 * @return array 标签信息（数组），找不到则返回空数组
	 */
	public function getMaterialList($com_id, $cond = array(), $materialFields = array(), $page= 1, $pageSize = 20, $order='', $withCnt = true) {
		$return = array();

		$table = self::$TABLE . ' as m';
		$fields = array();
		if (!empty($materialFields)) {
			foreach ($materialFields as $f) {
				$fields[] = 'm.`' . $f . '`';
			}
		}
		if (empty($fields)) {
			return $return;
		}
		$fields = implode(',', $fields);

		$condition = array(
			'm.info_state=' => self::$STATE_ON,
			'm.version=' => self::$VERSION_USE,
			'm.com_id=' => $com_id,
		);
		if (isset($cond['m1.id='])) {
			$table = self::$TABLE . ' as m right join ' . self::$TABLE . ' as m1 on m1.history_id=m.history_id';
		}
		!empty($cond) && $condition = array_merge($cond, $condition);
		
		$return = g('pkg_db') -> select($table, $fields, $condition, $page, $pageSize, '', $order, $withCnt);
		if ($withCnt) {
			$return  = array(
				'data' => !empty($return['data']) ? $return['data'] : array(),
				'count' => !empty($return['data']) ? $return['count'] : 0,
			);
		} else {
			!is_array($return) && $return = array();
		}

		return $return;
	}

	/**
	 * 获取物资属性
	 * @param int $com_id	企业根标签ID
	 * @param int $material_ids	物资ID
	 * @param string $fields	查询的字段
	 * 
	 * @return array 标签信息（数组），找不到则返回空数组
	 */
	public function getMaterialLabel($comId, $materialIds) {
		$return = array();

		$fields = array(
			'id', '`name`','`describe`','`material_id`','`p_label_id`','`input_key`','`type`','`must`','`other`',
		);
		$fields = implode(',', $fields);

		$cond = array(
			'info_state=' => self::$STATE_ON,
			'material_id IN' => $materialIds,
			'com_id=' => $comId,
		);
		$order = ' id asc ';
		$return = g('pkg_db') -> select(self::$TABLE_LABEL, $fields, $cond, 0, 0, '', $order);
		if (!empty($return)) {
			$tmpData = array();
			$materialId = array_column($return, 'material_id');
			$materialId = array_unique($materialId);
			$tmpData = array_fill_keys($materialId, array());

			foreach ($return as $r) {
				// 把other合并过来
				$other = json_decode($r['other'], TRUE);
				unset($r['other']);
				!is_array($other) && $other = array();
				$r = array_merge($other, $r);

				if (!empty($r['p_label_id'])) {
					// 子表单
					if (isset($tmpData[$r['material_id']][$r['p_label_id']]["table_arr"])) {
						$tmpData[$r['material_id']][$r['p_label_id']]["table_arr"][$r['input_key']] = $r;
					} else {
						$tmpData[$r['material_id']][$r['p_label_id']]["table_arr"][$r['input_key']] = array($r);
					}

				} else {
					$tmpData[$r['material_id']][$r['id']] = $r;

				}

			}

			// 恢复input_key作为键
			foreach ($tmpData as &$td) {
				$inputKey = array_column($td, 'input_key');
				$td = array_combine($inputKey, $td);
			}

			$return = $tmpData;
		}
		return $return;
	}

	/**
	* 检测我是否有权限进行物资导入
	*   
	* @access public
	* @param   
	* @return boolean true-有 false-没有  
	*/
	public function checkConfig($comId, $userId){
		$flag = false;

		// 用户通讯录信息
		$userInfo = g('dao_user') -> getUserContact($comId, $userId);
		
		$cond = array(
			'com_id=' => $comId,
			'info_state=' => 1,
			"__OR_1" => array(
				'user_list REGEXP ' => '"(' . $userInfo['user_id'] . ')"',
				'dept_list REGEXP ' => '"(' . implode('|', $userInfo['dept_tree']) . ')"',
				'group_list REGEXP ' => '"(' . implode('|', $userInfo['group_id']) . ')"',
			),
		);

		$flag = g('pkg_db') -> check_exists(self::$TABLE_CONFIG, $cond);
		return $flag;
	}


    /** 获取物品库存报表信息 */
    public function get_material_form($warehousingFormId,$applyFormId){
        $sql = "SELECT
	( form114412.input1487687030000_1520856402000__0 ) AS NAME,
	SUM( IFNULL( form114412.input1487687030000_1487687065000__1, 0 ) + 0 ) AS inventory,
	SUM( IFNULL( form114413.input1487687030000_1487687065000__1, 0 ) + 0 ) AS recipients,
	SUM( IFNULL( form114412.input1487687030000_1487687065000__1, 0 ) - IFNULL( form114413.input1487687030000_1487687065000__1, 0 ) ) AS remaining,
	other 
FROM
	(
	SELECT
		`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
		SUM( `input1487687030000_1487687065000` ) 'input1487687030000_1487687065000__1',
		other 
	FROM
		(
		SELECT
			formsetinst_id,
			cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1520856402000',
			cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1487687065000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1487687065000',
			cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', other, '' ) SEPARATOR '' ) AS CHAR ) other 
		FROM
			mv_proc_formsetinst_label form114412 
		WHERE
			form_history_id = {$warehousingFormId} 
			AND state = 2 
			AND info_state = 1 
			AND p_label_id != 0 
		GROUP BY
			formsetinst_id,
			p_label_id,
			label_id 
		HAVING
			input1487687030000_1520856402000 != '' 
			AND input1487687030000_1487687065000 != '' 
		order by formsetinst_id desc 
		) t 
	GROUP BY
		input1487687030000_1520856402000 
	) form114412
	LEFT JOIN (
	SELECT
		`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
		SUM( `input1487687030000_1487687065000` ) 'input1487687030000_1487687065000__1' 
	FROM
		(
		SELECT
			formsetinst_id,
			cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1520856402000',
			cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1487687065000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1487687065000' 
		FROM
			mv_proc_formsetinst_label form114413 
		WHERE
			form_history_id = {$applyFormId} 
			AND state = 2 
			AND info_state = 1 
			AND p_label_id != 0 
		GROUP BY
			formsetinst_id,
			p_label_id,
			label_id 
		HAVING
			input1487687030000_1520856402000 != '' 
			AND input1487687030000_1487687065000 != '' 
		) t 
	GROUP BY
		input1487687030000_1520856402000 
	) form114413 ON form114412.input1487687030000_1520856402000__0 = form114413.input1487687030000_1520856402000__0 
GROUP BY
	form114412.input1487687030000_1520856402000__0,
	form114413.input1487687030000_1520856402000__0 UNION
	(
	SELECT
		( form114412.input1487687030000_1520856402000__0 ) AS NAME,
		SUM( IFNULL( form114412.input1487687030000_1487687065000__1, 0 ) + 0 ) AS inventory,
		SUM( IFNULL( form114413.input1487687030000_1487687065000__1, 0 ) + 0 ) AS recipients,
		SUM( IFNULL( form114412.input1487687030000_1487687065000__1, 0 ) - IFNULL( form114413.input1487687030000_1487687065000__1, 0 ) ) AS remaining,
		other 
	FROM
		(
		SELECT
			`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
			SUM( `input1487687030000_1487687065000` ) 'input1487687030000_1487687065000__1',
			other 
		FROM
			(
			SELECT
				formsetinst_id,
				cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1520856402000',
				cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1487687065000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1487687065000',
				cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', other, '' ) SEPARATOR '' ) AS CHAR ) other 
			FROM
				mv_proc_formsetinst_label form114412 
			WHERE
				form_history_id = {$warehousingFormId} 
				AND state = 2 
				AND info_state = 1 
				AND p_label_id != 0 
			GROUP BY
				formsetinst_id,
				p_label_id,
				label_id 
			HAVING
				input1487687030000_1520856402000 != '' 
				AND input1487687030000_1487687065000 != '' 
			order by formsetinst_id desc 
			) t 
		GROUP BY
			input1487687030000_1520856402000 
		) form114412
		RIGHT JOIN (
		SELECT
			`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
			SUM( `input1487687030000_1487687065000` ) 'input1487687030000_1487687065000__1' 
		FROM
			(
			SELECT
				formsetinst_id,
				cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1520856402000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1520856402000',
				cast( GROUP_CONCAT( IF ( `input_key` = 'input1487687030000_1487687065000', val, '' ) SEPARATOR '' ) AS CHAR ) 'input1487687030000_1487687065000' 
			FROM
				mv_proc_formsetinst_label form114413 
			WHERE
				form_history_id = {$applyFormId} 
				AND state = 2 
				AND info_state = 1 
				AND p_label_id != 0 
			GROUP BY
				formsetinst_id,
				p_label_id,
				label_id 
			HAVING
				input1487687030000_1520856402000 != '' 
				AND input1487687030000_1487687065000 != '' 
			) t 
		GROUP BY
			input1487687030000_1520856402000 
		) form114413 ON form114412.input1487687030000_1520856402000__0 = form114413.input1487687030000_1520856402000__0 
	GROUP BY
		form114412.input1487687030000_1520856402000__0,
	form114413.input1487687030000_1520856402000__0 
	)";
        
        $res  = g('pkg_db')->query($sql);
        $data = [];
        if($res){
            foreach ($res as $item){
                $other = json_decode($item['other'],true);
                $id = $other['value']['history_id'];
                $data[$id] = $item;
            }
        }
        return $data?$data:[];
    }
    /** 获取物品库存报表信息 废弃 */
    public function _get_material_form(){
        $sql = "SELECT
	(
		form114412.input1487687030000_1520856402000__0
	) AS NAME,
	SUM(
		form114412.input1487687030000_1487687065000__1 + 0
	) AS inventory,
	SUM(
		form114413.input1487687030000_1487687065000__1 + 0
	) AS recipients,
	SUM(
		form114412.input1487687030000_1487687065000__1 - form114413.input1487687030000_1487687065000__1
	) AS remaining,
	other
FROM
	(
		SELECT
			`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
			SUM(
				`input1487687030000_1487687065000`
			) 'input1487687030000_1487687065000__1',
			other
		FROM
			(
				SELECT
					formsetinst_id,
					cast(
						GROUP_CONCAT(

							IF (
								`input_key` = 'input1487687030000_1520856402000',
								val,
								''
							) SEPARATOR ''
						) AS CHAR
					) 'input1487687030000_1520856402000',
					cast(
						GROUP_CONCAT(

							IF (
								`input_key` = 'input1487687030000_1487687065000',
								val,
								''
							) SEPARATOR ''
						) AS CHAR
					) 'input1487687030000_1487687065000',
					cast(
						GROUP_CONCAT(

							IF (
								`input_key` = 'input1487687030000_1520856402000',
								other,
								''
							) SEPARATOR ''
						) AS CHAR
					) other
				FROM
					mv_proc_formsetinst_label form114412
				WHERE
					form_history_id = 114266
				AND (state = 2 OR state=1)
				AND info_state = 1
				AND p_label_id != 0
				GROUP BY
					formsetinst_id,
					p_label_id,
					label_id
				HAVING
					input1487687030000_1520856402000 != ''
				AND input1487687030000_1487687065000 != ''
			) t
		GROUP BY
			input1487687030000_1520856402000
	) form114412
INNER JOIN (
	SELECT
		`input1487687030000_1520856402000` 'input1487687030000_1520856402000__0',
		SUM(
			`input1487687030000_1487687065000`
		) 'input1487687030000_1487687065000__1'
	FROM
		(
			SELECT
				formsetinst_id,
				cast(
					GROUP_CONCAT(

						IF (
							`input_key` = 'input1487687030000_1520856402000',
							val,
							''
						) SEPARATOR ''
					) AS CHAR
				) 'input1487687030000_1520856402000',
				cast(
					GROUP_CONCAT(

						IF (
							`input_key` = 'input1487687030000_1487687065000',
							val,
							''
						) SEPARATOR ''
					) AS CHAR
				) 'input1487687030000_1487687065000'
			FROM
				mv_proc_formsetinst_label form114413
			WHERE
				form_history_id = 114268
			AND (state = 2 OR state=1)
			AND info_state = 1
			AND p_label_id != 0
			GROUP BY
				formsetinst_id,
				p_label_id,
				label_id
			HAVING
				input1487687030000_1520856402000 != ''
			AND input1487687030000_1487687065000 != ''
		) t
	GROUP BY
		input1487687030000_1520856402000
) form114413 ON form114412.input1487687030000_1520856402000__0 = form114413.input1487687030000_1520856402000__0
GROUP BY
	form114412.input1487687030000_1520856402000__0,
	form114413.input1487687030000_1520856402000__0";
        $res  = g('pkg_db')->query($sql);
        $data = [];
        if($res){
            foreach ($res as $item){
                $other = json_decode($item['other'],true);
                $id = $other['value']['history_id'];
                $data[$id] = $item;
            }
        }
        return $data?$data:[];
    }
    /** 获取物资预警值 */
    public function get_material_worning($com_id,$material_id,$fields="*"){
        $cond = [
            //"com_id="=>$com_id,
            "material_id="=>$material_id,
            "info_state="=>self::$STATE_ON,
        ];
        $res = g('pkg_db')->select_one(self::$TABLE_WARNING,$fields,$cond);
        return $res?$res:[];
    }
    /** 获取物资预警人 */
    public function get_material_worning_user($com_id,$fields="*"){
        $cond = [
            "com_id="=>$com_id,
            "info_state="=>self::$STATE_ON,
        ];
        $res = g('pkg_db')->select_one(self::$TABLE_CONFIG,$fields,$cond);
        return $res?$res:[];
    }
}

//end