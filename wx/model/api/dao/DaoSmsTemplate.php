<?php
/**
 * 短信的数据库操作类
 * @author chenyihao
 * @create 2016-09-18
 */
namespace model\api\dao;

class DaoSmsTemplate extends \model\api\dao\DaoBase {
	
	private static $Table = 'sms_template';


	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		parent::set_table(self::$Table);
	}


}

/* End of this file */