<?php
/**
 * 虚拟组织结构类
 *
 * @author LiangJianMing
 * @create 2015-11-17
 */
class ShareStruct {
    //企业id
    private $com_id = 0;
    //用户id
    private $user_id = 0;
    //根部门id
    private $root_id = 0;

    //企业信息表
    private $com_table = 'sc_company';
    //用户信息表
    private $user_table = 'sc_user';
    //部门信息表
    private $dept_table = 'sc_dept';
    //虚拟组织结构信息表
    private $st_table = 'sc_struct';

    public function __construct() {
        isset($_SESSION[SESSION_VISIT_COM_ID]) and $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
        isset($_SESSION[SESSION_VISIT_USER_ID]) and $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
        isset($_SESSION[SESSION_VISIT_DEPT_ID]) and $this -> root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
    }

    /**
     * 根据用户id获取部门树
     *
     * @access public
     * @param integer $user_id 用户id
     * @return array
     */
    public function get_depts_tree($user_id=0) {
        empty($user_id) and $user_id = $this -> user_id;

        $key_str = __FUNCTION__ . "::{$user_id}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if ($ret) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();

            return $ret;
        }

        $ret = array();
        $st_ids = $this -> get_struct_ids($user_id);
        if (empty($st_ids)) {
            return $ret;
        }

        if (in_array(0, $st_ids)) {
            $ret = $this -> get_depts_tree_by_st(0);
            return $ret;
        }

        sort($st_ids);
        foreach ($st_ids as $val) {
            $ret[] = $this -> get_depts_tree_by_st($val);
        }
        unset($val);

        // 缓存60秒
        g('pkg_redis') -> setex($cache_key, json_encode($ret), 60);

        return $ret;
    }

    /**
     * 根据虚拟结构id获取部门树
     *
     * @access public
     * @param integer $st_id 虚拟结构id
     * @return array 
     */
    public function get_depts_tree_by_st($st_id=0) {
        $key_str = __FUNCTION__ . "::{$st_id}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if ($ret) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();

            return $ret;
        }

        $ret = array();
        if ($st_id != 0) {
            $condition = array(
                'id=' => $st_id,
                'state=' => 1,
            );
            $info = $this -> get_struct_list($condition, 1, 1);
            is_array($info) and $info = array_shift($info);
            
            $dept_ids = $this -> get_struct_permission_dept($st_id);
        }else {
            $root_id = $this -> root_id;
            $dept_ids = array($root_id);
        }

        if (empty($dept_ids)) {
            return $ret;
        }

        $dept_maper = $this -> get_depts_child($dept_ids);
        $ret = $this -> compose_dept_tree($dept_maper);

        //添加虚拟根
        if ($st_id != 0) {
            if (count($ret) == 1) {
                $ret = array_shift($ret);
                $ret['p_id'] = 0;

                // $ret['name'] = $info['root_name'];
            }else {
                // 负数，且保证虚拟结构id的唯一性
                $tmp_st_id = -1 * $st_id;

                foreach ($ret as &$val) {
                    $val['p_id'] = $tmp_st_id;
                }
                unset($val);

                $tmp_dept = array(
                    // 虚拟一个不存在的根id
                    'id' => $tmp_st_id,
                    'p_id' => 0,
                    'name' => $info['root_name'],
                    'children' => $ret,
                );

                $ret = &$tmp_dept;
            }
        }

        // 缓存20分钟
        g('pkg_redis') -> setex($cache_key, json_encode($ret), 1200);

        return $ret;
    }

    /**
     * 根据部门id获取其下的全部子部门信息
     *
     * @access public
     * @param mixed $dept_ids 部门id集合，如果是integer，会自动转换为array，返回结果包含这些集合
     * @param boolean $map 是否返回映射结构：
     *          true - 例如：array(id_val => array(id_val, name_val, ...), ...)
     *          false - 例如：array(id_val, id_val, ...)
     * @return array
     */
    public function get_depts_child($dept_ids, $map=false) {
        !is_array($dept_ids) and $dept_ids = array($dept_ids);

        $ret = array();
        if (empty($dept_ids)) {
            return $ret;
        }

        $root_id = $this -> root_id;
        $condition = array(
            'root_id=' => $root_id,
            '__OR' => array(
                'id IN' => $dept_ids,
                'p_list REGEXP ' => '"(' . implode('|', $dept_ids) . ')"',
            )
        );

        $ret = $this -> get_depts_list($condition, 0, 0, $map);

        return $ret;
    }

    /**
     * 获取部门列表
     *
     * @access public
     * @param array $condition 条件集合
     * @param integer $page 页码
     * @param integer $page_size 每页的记录数
     * @param boolean $with_map 是否返回映射数据
     * @return array
     */
    public function get_depts_list($condition, $page=0, $page_size=0, $with_map=false, array $fields=array()) {
        $table = $this -> dept_table . ' as t1';
        if (empty($fields)) {
            $fields = array(
                'id', 'p_id', 'name',
                'root_id', 'lev', 'idx',
                'p_list', 'state',
            );
        }
        $fields_key = array_flip($fields);
        !isset($fields_key['id']) and $fields_key['id'] = 1;
        $fields = array_keys($fields_key);

        array_walk($fields, function(&$v, $k, $prefix) {
			$v = trim($v);
			$v = $prefix . $v;
        }, 't1.');
        $fields = implode(',', $fields);
        
        // 对条件循环添加t1.前缀
        $tmpCondition = array();
        foreach ($condition as $key => $value) {
            if (strpos($key, '__OR') === 0) {
                foreach ($value as $key1 => $value1) {
                    $k1 = 't1.' . $key1;
                    $tmpCondition[$key][$k1] = $value1;
                }
            } else {
                $k = 't1.' . $key;
                $tmpCondition[$k] = $value;
            }
        }
        $condition = $tmpCondition;

        // gch add contact config
        $hiddenConfig = g('dao_contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
        if (!empty($hiddenConfig['dept'])) {
            $condition['^t1.id NOT REGEXP '] = '"(' . implode('|', $hiddenConfig['dept']) . ')"';
        }
        $watchOther = g('dao_contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
        if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$dept_sql = "(SELECT * FROM sc_dept t2 WHERE t2.id IN (" . implode(',', $watchOther['dept']) . ") AND (t2.p_list REGEXP (t1.id) OR t2.id=t1.id))";
			$condition['^EXISTS '] = $dept_sql;
        }

        $order_by = ' ORDER BY t1.idx, t1.id ';
        $ret = g('pkg_db') -> select($table, $fields, $condition, $page, $page_size, '', $order_by);
        !is_array($ret) and $ret = array();

        if ($with_map) {
            $tmp_ret = array();
            foreach ($ret as &$val) {
                $tmp_ret[$val['id']] = &$val;
            }
            unset($val);
            $ret = &$tmp_ret;
        }

        return $ret;
    }

    /**
     * 获取虚拟结构列表
     *
     * @access public
     * @param array $condition 条件集合
     * @param integer $page 页码
     * @param integer $page_size 每页的记录数
     * @param boolean $with_map 是否返回映射数据
     * @return array
     */
    public function get_struct_list(array $condition=array(), $page=0, $page_size=0, $with_map=false) {
        !is_array($condition) and $condition = array();
        $condition['com_id='] = $this -> com_id;

        $fields = array(
            'id AS st_id', 'st_pid', 'name',
            'root_name', 'with_depts', 'level',
            'p_list',
        );
        $fields = implode(',', $fields);

        $order_by = ' ORDER BY level, id ';

        $ret = g('pkg_db') -> select($this -> st_table, $fields, $condition, $page, $page_size, $order_by);
        !is_array($ret) and $ret = array();

        foreach ($ret as &$val) {
            $tmp_depts = json_decode($val['with_depts'], true);
            !is_array($tmp_depts) and $tmp_depts = array();

            $val['with_depts_arr'] = &$tmp_depts;
            unset($tmp_depts);

            $tmp_list = json_decode($val['p_list'], true);
            !is_array($tmp_list) and $tmp_list = array();

            $val['p_list_arr'] = &$tmp_list;
            unset($tmp_list);
        }
        unset($val);

        if ($with_map) {
            $tmp_ret = array();
            foreach ($ret as $val) {
                $tmp_ret[$val['st_id']] = $val;
            }
            unset($val);
            $ret = &$tmp_ret;
        }

        return $ret;
    }

    /**
     * 查询有权查看的全部部门id集合
     *
     * @access public
     * @return array
     */
    public function get_permission_dept() {
        $user_id = $this -> user_id;

        $key_str = __FUNCTION__ . "::{$user_id}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if ($ret) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();

            return $ret;
        }

        $ret = array();

        $st_ids = $this -> get_struct_ids();

        if (empty($st_ids)) return $ret;

        //代表拥有全局组织结构权限
        if (in_array(0, $st_ids)) {
            $ret[] = $this -> root_id;
            g('pkg_redis') -> setex($cache_key, json_encode($ret), 60);
            return $ret;
        }

        foreach ($st_ids as $val) {
            $tmp_arr = $this -> get_struct_permission_dept($val);
            $ret = array_merge($ret, $tmp_arr);
        }
        unset($val);

        $ret = array_unique($ret);

        g('pkg_redis') -> setex($cache_key, json_encode($ret), 60);

        return $ret;
    }

    /**
     * 获取权限内的顶层部门
     *
     * @access public
     * @param integer $user_id 用户id
     * @return array
     */
    public function get_top_depts($user_id=0) {
        empty($user_id) and $user_id = $this -> user_id;

        $key_str = __FUNCTION__ . "::{$user_id}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if ($ret) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();

            return $ret;
        }

        $ret = array();
        $st_ids = $this -> get_struct_ids();
        if (empty($st_ids)) return $ret;

        $condition = array(
            'root_id=' => $this -> root_id,
        );
        // 代表拥有全局组织结构权限
        if (in_array(0, $st_ids)) {
            $condition['lev<='] = 2;
            $depts = $this -> get_depts_list($condition);
            $ret = $this -> compose_dept_tree($depts);
            // 缓存60秒
            g('pkg_redis') -> setex($cache_key, json_encode($ret), 60);
            return $ret;
        }

        $tmp_con = array(
            'id IN' => $st_ids,
        );
        $st_list = $this -> get_struct_list($tmp_con, 0, 0, true);

        $dept_ids = array();
        foreach ($st_list as &$val) {
            $val['real_depts_arr'] = $this -> get_struct_permission_dept($val['st_id']);
            $dept_ids = array_merge($dept_ids, $val['real_depts_arr']);
        }
        unset($val);

        $dept_ids = array_unique($dept_ids);
        if (empty($dept_ids)) {
            return $ret;
        }

        $condition['id IN'] = $dept_ids;
        $dept_list = $this -> get_depts_list($condition, 0, 0, true);

        // 获取每个虚拟结构的顶级部门
        foreach ($st_list as $val) {
            $tmp_lev = array();
            foreach ($val['real_depts_arr'] as $cval) {
                if (!isset($dept_list[$cval])) continue;

                $tmp_arr = &$dept_list[$cval];
                $tmp_lev[$tmp_arr['lev']][] = $tmp_arr;
            }

            ksort($tmp_lev);
            $p_str = '';
            $tmp_dept = array();
            foreach ($tmp_lev as $tmp_child) {
                foreach ($tmp_child as $cval) {
                    $p_list = json_decode($cval['p_list'], true);
                    !is_array($p_list) and $p_list = array();
                    
                    // 父级部门为空时，必定为顶级部门
                    if (empty($p_list)) {
                        if ($cval['lev'] == 1) {
                            $tmp_dept[] = $cval;
                            $p_str .= ",{$cval['id']},";
                        }

                        continue;
                    }

                    $m_str = ',(' . implode('|', $p_list) . '),';
                    // 如果匹配到父级id，则丢弃该部门
                    if (preg_match("/{$m_str}/", $p_str)) continue;

                    $tmp_dept[] = $cval;
                    $p_str .= ",{$cval['id']},";
                }
            }

            if (empty($tmp_dept)) continue;

            $tree = $this -> compose_dept_tree($tmp_dept);

            // 添加虚拟根
            if (count($tree) == 1) {
                $tree = array_shift($tree);
                $tree['p_id'] = 0;
            }else {
                // 负数，且保证虚拟结构id的唯一性
                $tmp_st_id = -1 * $val['st_id'];

                foreach ($tree as &$cval) {
                    $cval['p_id'] = $tmp_st_id;
                }
                unset($cval);

                $tmp_tree = array(
                    // 虚拟一个不存在的根id
                    'id' => $tmp_st_id,
                    'p_id' => 0,
                    'name' => $val['root_name'],
                    'children' => $tree,
                );

                $tree = &$tmp_tree;
            }

            $ret[] = $tree;
        }

        // 缓存60秒
        g('pkg_redis') -> setex($cache_key, json_encode($ret), 60);

        return $ret;
    }

    /**
     * 查询虚拟结构内经过与父结构对比的，真正有效的部门id
     *
     * @access public
     * @param integer $st_id 虚拟结构id
     * @return array
     */
    public function get_struct_permission_dept($st_id=0) {
        $ret = array();
        $st_id = intval($st_id);
        if ($st_id < 0) {
            return $ret;
        }

        if ($st_id === 0) {
            $ret[] = $this -> root_id;
            return $ret;
        }

        $condition = array(
            'id=' => $st_id,
        );
        $info = $this -> get_struct_list($condition, 1, 1);
        $info = array_shift($info);
        !is_array($info) and $info = array();

        if (empty($info)) {
            return $ret;
        }

        $p_list = $info['p_list_arr'];
        if (empty($p_list)) {
            return $info['with_depts_arr'];
        }
        
        $condition = array();
        if (!empty($p_list)) {
            $condition['id IN'] = $p_list;
        }
        $list = $this -> get_struct_list($condition, 0, 0, true);
        // 按照id排列，顺序从父级到子级
        ksort($list);
        $list = array_values($list);

        $tmp_arr = null;
        $count = count($list);
        for ($i = 1; $i < $count; $i++) {
            if ($tmp_arr === null) {
                $tmp_arr = $this -> get_compare_depts($list[$i]['with_depts_arr'], $list[$i - 1]['with_depts_arr']);
                continue;
            }

            $tmp_arr = $this -> get_compare_depts($list[$i]['with_depts_arr'], $tmp_arr);
        }

        $tmp_arr === null and $tmp_arr = $list[0]['with_depts_arr'];
        $ret = $this -> get_compare_depts($info['with_depts_arr'], $tmp_arr);

        return $ret;
    }

//--------------以下是私有实现---------------------------------------------
    /**
     * 组合部门树
     *
     * @access private
     * @param array $data 父数组，必须使用非自定义的数字索引
     * @param array $child 这个子元素要并入到父数组
     * @param array $is_recurse 是否递归调用
     * @return array
     */
    private function compose_dept_tree(array &$data, array &$child=array(), $is_recurse=false) {
        $ret = array();
        if (empty($data)) {
            return $ret;
        }

        // 非递归调用需要初始化
        if (!$is_recurse) {
            $lev_arr = array();
            // 指定最高级别
            $min_lev = -1;
            foreach ($data as &$val) {
                !isset($val['children']) and $val['children'] = array();

                // 最高级别
                if ($min_lev === -1) {
                    $min_lev = $val['lev'];
                }else if ($min_lev > $val['lev']) {
                    $min_lev = $val['lev'];
                }

                $lev_arr[$val['lev']][] = &$val;
            }
            unset($val);

            $begin = count($lev_arr) + $min_lev - 1;
            for ($i = $begin; $i > $min_lev; $i--) {
                $this -> compose_dept_tree($lev_arr[$i - 1], $lev_arr[$i], true);
            }

            $ret = &$lev_arr[$min_lev];
            return $ret;
        }

        if (empty($child)) return;

        // $data 数据中的 id 与 数组索引的映射关系
        $idx_map = array();
        foreach ($data as $key => &$val) {
            $idx_map[$val['id']] = $key;
        }
        unset($val);

        foreach ($child as &$val) {
            if (!isset($idx_map[$val['p_id']])) {
                // 提高部门级别
                $data[] = &$val;
                continue;
            }

            $idx = $idx_map[$val['p_id']];
            if ($val['state'] != 1 or $data[$idx]['state'] != 1) {
                continue;
            }

            $data[$idx]['children'][] = &$val;
        }
        unset($val);
        unset($idx_map);

        return;
    }

    /**
     * 获取指定用户受影响的虚拟结构id集合
     *
     * @access private
     * @param integer $user_id 用户id，默认是当前用户
     * @return array
     */
    private function get_struct_ids($user_id=0) {
        empty($user_id) and $user_id = $this -> user_id;
        
        $ret = array();
        // 获取自身所在的部门id及父id集合
        $dept_arr = $this -> get_user_dept_ids($user_id);
        if (empty($dept_arr)) {
            return $ret;
        }

        // 拥有根目录权限
        if (in_array($this -> root_id, $dept_arr['dept_list'])) {
            $ret[] = 0;
            return $ret;
        }

        $condition = array(
            'with_depts REGEXP ' => '"(' . implode('|', $dept_arr['dept_tree']) . ')"',
            'state=' => 1,
        );
        // 结果是按level升序排列的
        $result = $this -> get_struct_list($condition, 0, 0);
        if (empty($result)) {
            $ret[] = 0;
            return $ret;
        }

        $tmp_arr = array();
        foreach ($result as $val) {
            $tmp_arr[$val['level']][] = $val;
        }
        // level从大到小排序
        krsort($tmp_arr);

        $not_match = '';
        foreach ($tmp_arr as $val) {
            foreach ($val as $cval) {
                if (strpos($not_match, "\"{$cval['st_id']}\"") !== false) {
                    continue;
                }

                $ret[] = intval($cval['st_id']);
                $not_match .= $cval['p_list'];
            }
        }

        $compare_depts = array();
        $tmp_ret = array();
        foreach ($ret as $val) {
            $tmp_dept = $this -> get_struct_permission_dept($val);
            if (empty($tmp_dept)) continue;
            // 判断是否包含关系
            $compare = $this -> get_compare_depts($dept_arr['dept_list'], $tmp_dept);
            $check = array_intersect($compare, $dept_arr['dept_list']);
            if (empty($check)) continue;

            $tmp_ret[] = $val;
            $compare_depts = array_merge($compare_depts, $compare);
        }
        $compare_depts = array_unique($compare_depts);

        foreach ($dept_arr['dept_list'] as $val) {
            // 不是完全被虚拟架构包含
            if (!in_array($val, $compare_depts)) {
                $tmp_ret = array(0);
                break;
            }
        }

        $ret = &$tmp_ret;
        return $ret;
    }

    /**
     * 查询用户的所在的部门id及全部父部门
     *
     * @access public
     * @param integer $user_id 用户id，默认是当前用户
     * @return array
     */
    public function get_user_dept_ids($user_id=0) {
        empty($user_id) and $user_id = $this -> user_id;

        $key_str = __FUNCTION__ . "::{$user_id}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if (!empty($ret)) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();

            return $ret;
        }

        $fields = array(
            'dept_list', 'dept_tree',
        );
        $fields = implode(',', $fields);

        $condition = array(
            'root_id=' => $this -> root_id,
            'id=' => $user_id,
        );

        $result = g('pkg_db') -> select($this -> user_table, $fields, $condition, 1, 1);
        !is_array($result) and $result = array();

        !empty($result) and $ret = $result[0];

        $ret['dept_list'] = json_decode($ret['dept_list'], true);
        !is_array($ret['dept_list']) and $ret['dept_list'] = array();

        $ret['dept_tree'] = json_decode($ret['dept_tree'], true);
        !is_array($ret['dept_tree']) and $ret['dept_tree'] = array();

        $tmp_ret = array();
        foreach ($ret['dept_list'] as $val) {
            $tmp_int = intval($val);
            $tmp_int !== 0 and $tmp_ret[$tmp_int] = 1;
        }
        $ret['dept_list'] = array_keys($tmp_ret);

        $tmp_ret = array();
        foreach ($ret['dept_tree'] as $val) {
            if (!is_array($val)) continue;
            
            foreach ($val as $cval) {
                $tmp_int = intval($cval);
                $tmp_int !== 0 and $tmp_ret[$tmp_int] = 1;
            }
        }
        $ret['dept_tree'] = array_keys($tmp_ret);

        g('pkg_redis') -> setex($cache_key, json_encode($ret), 30);

        return $ret;
    }

    /**
     * 查找部门的全部父部门
     *
     * @access private
     * @param array $dept_ids 部门id集合
     * @return array
     */
    private function get_parent_depts(array $dept_ids) {
        $ret = array();
        if (empty($dept_ids)) {
            return $ret;
        }

        $dept_ids = array_unique($dept_ids);
        sort($dept_ids);
        $key_str = __FUNCTION__ . "::" . json_encode($dept_ids);
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);
        if (!empty($ret)) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();
            return $ret;
        }

        $fields = array(
            'id', 'p_list'
        );
        $condition = array(
            'id IN' => $dept_ids,
        );
        $ret = $this -> get_depts_list($condition, 0, 0, true, $fields);

        $tmp_ret = array();
        foreach ($ret as $key => $val) {
            $p_list = json_decode($val['p_list'], true);
            !is_array($p_list) and $p_list = array();

            // 强制转换为整型
            foreach ($p_list as &$cval) {
                $cval = intval($cval);
            }
            unset($cval);

            $tmp_ret[intval($key)] = $p_list;
        }
        $ret = &$tmp_ret;

        // p_list不会改变，所以缓存2小时
        g('pkg_redis') -> setex($cache_key, json_encode($ret), 2 * 3600);

        return $ret;
    }

    /**
     * 比较并获得真实部门权限
     *
     * @access private
     * @param array $child 子部门id集
     * @param array $parent 父部门id集
     * @return array
     */
    private function get_compare_depts(array $child, array $parent) {
        $ret = array();
        if (empty($child) or empty($parent)) {
            return $ret;
        }

        // 转为整型
        foreach ($child as &$val) {
            $val = intval($val);
        }
        unset($val);
        foreach ($parent as &$val) {
            $val = intval($val);
        }
        unset($val);

        $merge = array_merge($child, $parent);
        $parent_map = $this -> get_parent_depts($merge);

        foreach ($child as $val) {
            if (!isset($parent_map[$val])) continue;

            // 如果子部门对应的真实父部门，存在于需要比较的父部门中，
            // 则拥有该子部门的权限
            $tmp_arr = $parent_map[$val];
            $tmp_arr[] = $val;
            foreach ($tmp_arr as $cval) {
                if (in_array($cval, $parent, true)) {
                    $ret[] = $val;
                    break;
                }
            }
        }

        foreach ($parent as $val) {
            if (!isset($parent_map[$val])) continue;

            // 如果父部门对应的真实父部门，存在于需要比较的子部门中，
            // 则拥有该父部门的权限
            $tmp_arr = $parent_map[$val];
            $tmp_arr[] = $val;
            foreach ($tmp_arr as $cval) {
                if (in_array($cval, $child, true)) {
                    $ret[] = $val;
                    break;
                }
            }
        }

        $ret = array_unique($ret);

        return $ret;
    }

    /**
     * 获取缓存变量名
     *
     * @access private
     * @param string $str 关键字符串
     * @return string
     */
    private function get_cache_key($str) {
        $com_id = $this -> com_id;
        $key_str = SYSTEM_COOKIE_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . '::' . $com_id . ':' .$str;
        $key = md5($key_str);
        return $key;
    }
}

// end of file