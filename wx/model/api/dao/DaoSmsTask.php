<?php
/**
 * 短信的数据库操作类
 * @author chenyihao
 * @create 2016-09-18
 */
namespace model\api\dao;

class DaoSmsTask extends \model\api\dao\DaoBase {
	
	private static $Table = 'sms_send_task';

	private static $FormSmsTabel = 'mv_proc_form_sms_model';

	private static $UserTable = 'sc_user';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		parent::set_table(self::$Table);
	}

    /**
     * 加入短信消息队列
     * @param $data
     * @return bool
     */
    public function addTask($data){
	    $time = time();
	    $data['create_time'] = $time;
	    $data['update_time'] = $time;
	    $data['info_state'] = 1;

	    return $this->insert($data,'');
    }



}

/* End of this file */