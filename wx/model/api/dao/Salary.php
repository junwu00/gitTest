<?php
/**
 * 工资条查询类
 * @author yangpz
 * @date 2015-12-18
 */

namespace model\api\dao;

class Salary extends \model\api\dao\DaoBase {
    const SALARY_TYPE_PERSON_NUMBER = 'person_number';
    const SALARY_TYPE_ID_CARD = 'id_card';

	/** 对应的库表名称 */
	private static $Table = 'salary_info';
	
	/** 禁用 */
	public static $StateOff = 0;
	/** 启用 */
	public static $StateOn = 1;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	/**
	 * 根据工资条ID，获取工资条信息
	 * @param unknown_type $id	工资条id
	 * @param unknown_type $fields	查找的字段，默认查找全部
	 * 
	 * @return 工资条信息（数组），找不到则返回FALSE
	 */
	public function get_by_id($com_id, $id, $fields='*') {
        $cond = array(
			'id=' 		=> $id,
            'com_id=' 	=> $com_id,
        	'info_state=' 	=> self::$StateOn
		);
		return $this -> get_by_cond($cond, $fields);
	}

	/**
	 * 根据条件获取工资条信息
	 * @param   $ids
	 * @param   $fields
	 * @return 
	 */
	public function get_data_by_cond($com_id, $cond, $fields='*', $page = 0, $page_size = 0, $order_by='', $with_cnt=FALSE){
		$base_cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$StateOn,
		);
		$cond = array_merge($cond, $base_cond);
		return $this -> list_by_page($cond, $fields, $page, $page_size, $order_by, $with_cnt);
	}
	
}

//end