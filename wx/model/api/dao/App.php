<?php
/**
 * 应用配置
 * @author yangpz
 * @date 2016-02-19
 *
 */
namespace model\api\dao;

class App extends \model\api\dao\DaoBase {
	/** 对应的库表名称 */
	private static $Table = 'sc_app';
	
	/** 禁用 0 */
	public static $StateOff = 0;
	/** 启用 1 */
	public static $StateOn = 1;
	
	public function __construct() {
		parent::__construct();
		$this -> set_table(self::$Table);
	}
	
	/**
	 * 根据ID获取应用
	 * @param unknown_type $id	应用ID
	 */
	public function get_by_id($id, $fields='*') {
		$cond = array(
			'id=' 		=> $id,
			'state=' 	=> self::$StateOn,
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
	/**
	 * 根据应用名称获取应用信息
	 * @param unknown_type $name	应用英文名称
	 * @param unknown_type $fields	查询字段
	 */
	public function get_by_name($name, $fields='*') {
		$cond = array(
			'name=' 	=> $name,
			'state=' 	=> self::$StateOn,
		);
		return $this -> get_by_cond($cond, $fields);
	}
	
	public function get_all_app($fields='*'){
		$cond = array(
				'state=' => self::$StateOn,
			);
		return $this -> list_by_cond($cond, $fields);
	}

	/**
	 * 获取企业指定应用列表
	 * @param unknown_type $ids		应用ID数组
	 * @param unknown_type $fields	查询字段
	 */
	public function list_by_ids(array $ids, $fields='*') {
		if (empty($ids))	return array();
		
		$cond = array(
			'id IN' => $ids, 
		);
		return $this -> list_by_cond($cond, $fields);
	}
	
}

// end of file