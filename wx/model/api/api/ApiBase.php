<?php

/**
 * 对外接口数据处理类
 * @author yangpz
 * @date 2016-04-18
 */
namespace model\api\api;

abstract class ApiBase {
	
	private $conf;
	
	public function __construct($mod_name='api') {
		$this -> conf = load_config("model/{$mod_name}/api");
	}
	
	/** 权限验证 */
	public abstract function check();
	
	/** 验证请求来源 */
	public function check_referer() {
		return TRUE;
		$conf_host 		= $this -> conf['referer']['host'];
		$conf_referer 	= $this -> conf['referer']['list'];
		if (empty($conf_host) && empty($conf_referer))	return TRUE;
		
		$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
		if (empty($referer)) {
			return FALSE;
		}
		
		//验证域名
		if (!empty($conf_host)) {
			$req_host = parse_url($referer);
			$req_host = $req_host['host'];
			if (!in_array($req_host, $conf_host))		return FALSE;
		}
		//验证完整来源
		if (!empty($conf_referer)) {
			if (!in_array($referer, $conf_referer))		return FALSE;
		}
		return TRUE;
	}
	
	/** 验证请求IP */
	public function check_ip() {
		//暂不提供全局验证
	}
	
	/**
	 * 获取POST请求的数据
	 * @param array $need	必带字段
	 */
	protected function get_post_data(array $need=array()) {
		$data = g('pkg_val') -> get_post('data', FALSE);
		if (is_null($data)) {
			$this -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW);
		}
		
		!is_array($data) && $data = json_decode($data, TRUE);
		if (!is_array($data)) {
			$this -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW);
		}
		
		$keys = array_keys($data);
		foreach ($need as $field) {	//验证必备字段
			if (!in_array($field, $keys)) {
				$this -> echo_resp(\ERRCODE_API::$REQUEST_PARAMS_NOT_COMPLETE);
			}
		}unset($field);
		
		return $data;
	}
	
	//请求响应/输出==============================================================================================
	
	/**
	 * 通用信息输出
	 * @param unknown_type $errcode	错误码，默认为成功
	 * @param array $ext_data		附加信息(支持覆盖默认信息)
	 */
	protected function echo_resp($errcode=NULL, array $ext_data=array()) {
		g('api_ser_resp') -> echo_resp($errcode, $ext_data);
	}
	
	/**
	 * 输出异常信息
	 * @param unknown_type $e			exception对象
	 * @param unknown_type $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_exp($e, array $ext_data=array()) {
		g('api_ser_resp') -> echo_exp($e, $ext_data);
	}
	
	/**
	 * 输出成功信息
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_ok(array $ext_data=array(), $errcode=NULL) {
		g('api_ser_resp') -> echo_ok($ext_data, $errcode);
	}
	
	/**
	 * 输出系统繁忙
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_busy(array $ext_data=array(), $errcode=NULL) {
		g('api_ser_resp') -> echo_busy($ext_data, $errcode);
	}
	
	//其他-----------------------------------------------------------
	
	/**
	 * 将静态资源的链接转为http开头的绝对路径
	 * @param unknown_type $str	要处理的字符串
	 */
	protected function trans_static_path($str, $domain_prefix='') {
		empty($domain_prefix) && $domain_prefix = MAIN_STATIC_DOMAIN;
		
		$domain_prefix = rtrim($domain_prefix, '\\/');
		$reg = '/(<img src=")(\/.*)"(.*)/';
		return preg_replace($reg, "$1".$domain_prefix."$2$3", $str);
	}
	
}

//end