<?php
/**
 * 公用提示
 * @author yangpz
 * @date 2016-04-18
 *
 */
namespace model\api\api;

abstract class ApiNoticeBase {
	
	/**
	 * 非法请求
	 * @param unknown_type $msg	提示信息
	 */
	public function forbid($msg='') {
		if (empty($msg)) {
			g('api_ser_resp') -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW);
			
		} else {
			!is_array($msg) && $msg = array($msg);
			g('api_ser_resp') -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW, array('info' => $msg));
		}
	}
	
}

//end