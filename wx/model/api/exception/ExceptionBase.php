<?php

/**
 * 自定义异常
 * @author yangpz
 * @date 2016-04016
 *
 */

class ExceptionBase extends \Exception {
	
	public function __construct(array $custom_exp_info) {
		if (!is_array($custom_exp_info) || !isset($custom_exp_info['code']) || !isset($custom_exp_info['desc'])) {
			parent::__construct(\ERRCODE_API::$UNKNOW_EXP['desc'], \ERRCODE_API::$UNKNOW_EXP['code']);
			
		} else {
			parent::__construct($custom_exp_info['desc'], $custom_exp_info['code']);
		}
	}
	
}

//end