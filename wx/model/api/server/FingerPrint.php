<?php
/**
 * 指纹相关服务类
 * @author luja
 * @date 2017-02-08
 */
namespace model\api\server;

class FingerPrint extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $Info_On = 1;
	/** 数据状态删除 */
	private static $Info_Delete = 0;

	/** 状态启用 */
	private static $State_On = 1;
	/** 状态禁用 */
	private static $State_Off = 0;

	/** 指纹信息表 */
	private static $Table_FP = 'sc_user_fingerprint';

	public function __construct($mod_name='', $log_pre_str='') {
		if (empty($mod_name)) {
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}


	/**
	 * 获取用户指纹设置信息
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @param  string $fields  
	 */
	public function get_user_fingerprint($com_id, $user_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'info_state=' => self::$Info_On
			);

		$ret = g('pkg_db') -> select(self::$Table_FP, $fields, $cond);

		return $ret ? $ret[0] : FALSE;
	}


	/**
	 * 保存密码和指纹
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @param  [type] $pwd     
	 */
	public function save_pwd($com_id, $user_id, $pwd, $fingerprint) {
		!is_string($fingerprint) && $fingerprint = json_encode($fingerprint);
		$data = array(
			'com_id' 	=> $com_id,
			'user_id' 	=> $user_id,
			'pwd' 		=> $pwd,
			'fingerprint' => $fingerprint,
			'state'		=> self::$State_On,
			'create_time' => time()
			);

		$ret = g('pkg_db')->insert(self::$Table_FP, $data);
		if (!$ret) throw new \Exception('插入数据错误');
		
		return $ret;
	}


	/**
	 * 更新指纹
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @param  [type] $fingerprint     
	 */
	public function update_fingerprint($com_id, $user_id, $fingerprint) {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'info_state=' => self::$Info_On
			);

		!is_string($fingerprint) && $fingerprint = json_encode($fingerprint);
		$data = array(
			'state' => self::$State_On,
			'fingerprint' => $fingerprint
			);

		$ret = g('pkg_db')->update(self::$Table_FP, $cond, $data);
		if (!$ret) throw new \Exception('更新数据错误');
		
		return $ret;
	}

	/**
	 * 关闭指纹
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 */
	public function change_fingerprint($com_id, $user_id, $state) {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'info_state=' => self::$Info_On
			);

		$data = array(
			'state' => $state
			);

		$ret = g('pkg_db')->update(self::$Table_FP, $cond, $data);
		if (!$ret) throw new \Exception('更新数据错误');
		
		return $ret;
	}

	/**
     * 验证指纹签名
     * @param $json 指纹信息
     * @param $sign 签名信息
     */
    public function verify_soter_sign($json, $sign) {
        $access_token = g('api_atoken') -> get_by_com($_SESSION[SESSION_VISIT_COM_ID]);
        $auth_info = g('wxqy_auth') -> convert_to_openid($access_token, $_SESSION[SESSION_VISIT_USER_WXACCT]);
        $openid = $auth_info['openid'];

        $check_data = array(
            'open_id'    => $openid,
            'json'      => $json,
            'sign'      => $sign,
        );

        $this->log_i(json_encode($check_data));
        return g('wxqy_soter') -> verify_sign($access_token, $check_data);
    }

}

// end of file