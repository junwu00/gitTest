<?php
/**
 * 微信access token获取
 * @author yangpz
 * @date 2015-01-05
 *
 */
namespace model\api\server;

class AToken extends \model\api\server\Base{
	
	/** access token 生存最长时间 */
	private $atoken_alive_time = 60;
	/** suite access token 生存最长时间 */
	private $stoken_alive_time = 60;
	
	/** 企业ID */
	private $com_id = 0;
	
	/** 对应三尺套件ID */
	private $app_id = 0;
	/** 企业号官方的企业ID */
	private $corp_id = '';
	/** 企业号官方的企业(管理组)Secret码 */
	private $corp_secret = '';
	/** 已授权的所有套件对应的永久授权码 */
	private $auth_codes = '';
	/** 已授权的所有套件的信息 */
	private $auth_suites = '';
	
	/** token相关缓存key前缀配置 */
	private $atoken_cache_key_prefix;
	private $atoken_time_cache_key_prefix;
	private $stoken_cache_key_prefix;
	private $stoken_time_cache_key_prefix;
	
	public function __construct($log_pre_str='') {
		//读取配置
		$conf = load_config('model/api/atoken');
		isset($conf['alive_time']['atoken']) && $this -> atoken_alive_time = intval($conf['alive_time']['atoken']);
		isset($conf['alive_time']['stoken']) && $this -> stoken_alive_time = intval($conf['alive_time']['stoken']);
		
		$this -> atoken_cache_key_prefix = $conf['cache_key_prefix']['atoken'];
		$this -> atoken_time_cache_key_prefix = $conf['cache_key_prefix']['atoken_time'];
		$this -> stoken_cache_key_prefix = $conf['cache_key_prefix']['stoken'];
		$this -> stoken_time_cache_key_prefix = $conf['cache_key_prefix']['stoken_time'];
		//获取当前企业/套件信息
		if (session_id()) {
			isset($_SESSION[SESSION_VISIT_COM_ID]) && $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
			isset($_SESSION[SESSION_VISIT_CORP_ID]) && $this -> corp_id = $_SESSION[SESSION_VISIT_CORP_ID];
		}
		empty($log_pre_str) && $log_pre_str = "[AToken][com:{$this -> com_id}][corp_id:{$this -> corp_id}]";
		parent::__construct($log_pre_str);
	}
	
	/**
	 * 设置配置信息
	 * @param array $conf	配置信息，支持：combo, corp_id, secret, auth_codes, auth_suites参数
	 */
	public function set_conf(array $conf) {
		isset($conf['com_id']) && $this -> com_id = $conf['com_id'];
		isset($conf['app_id']) && $this -> app_id = $conf['app_id'];
		isset($conf['corp_id']) && $this -> corp_id = $conf['corp_id'];
		isset($conf['secret']) && $this -> corp_secret = $conf['secret'];
		isset($conf['auth_codes']) && $this -> auth_codes = $conf['auth_codes'];
		isset($conf['auth_suites']) && $this -> auth_suites = $conf['auth_suites'];
		return $this;
	}

	/**
	 * 根据企业ID，应用所属套餐，获取access token
	 * @param unknown_type $com_id		默认为session中对应的企业
	 */
	public function get_by_com($com_id=NULL, $app_id=0) {
		try {
			$com = g('dao_com') -> get_by_id($com_id);
			if (!$com) {
				throw new \Exception('找不到对应的企业信息');
			}
			$conf = array(
				'com_id'		=> $com_id,
				'app_id'		=> $app_id,
				'corp_id'		=> $com['corp_id'],
				'secret'		=> $com['corp_secret'],
				'auth_codes'	=> $com['permanent_code'],
				'auth_suites'	=> $com['permanent_suite_id'],
			);
			$token = $this -> set_conf($conf) -> get();
			return $token;
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取access token
	 */
	public function get() {
		parent::log_i('开始获取access token');
		
		try {
			// if (!empty($this -> auth_codes)) {
			// 	if (empty($this -> auth_suites) || empty($this -> corp_id)) {
			// 		throw new \Exception('请先安装应用后再使用该功能');
			// 	}
				
			// 	$combo_suite_arr = json_decode($this -> auth_suites, TRUE);
			// 	if (!is_array($combo_suite_arr) || count($combo_suite_arr) == 0) {
			// 		throw new \Exception('管理员在安装应用时未配置“通讯录管理权限”');
			// 	}
				
			// 	$auth_code_arr = json_decode($this -> auth_codes, TRUE);
			// 	if (!empty($this -> app_combo)) {
			// 		if (!isset($combo_suite_arr[$this -> app_combo])) {		//指定套餐
			// 			throw new \Exception('该应用未授权');
			// 		}
			// 		$suite_id = $combo_suite_arr[$this -> app_combo];
			// 		$auth_code = $auth_code_arr[$this -> app_combo];
					
			// 	} else {													//不指定套餐（通讯录管理时使用）
			// 		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			// 			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
						
			// 		} else {
			// 			$com = g('dao_com') -> get_by_corp_id($this -> corp_id, 'id');
			// 			$com_id = $com['id'];
			// 		}
					
			// 		$auth_apps = g('dao_com_app') -> list_all_auth_app($com_id, 'c.*');
			// 		empty($auth_apps) && $auth_apps = array();
			// 		$sie_ids = array();
			// 		foreach ($auth_apps as $auth_app) {
			// 			$sie_ids[$auth_app['sie_id']] = $auth_app['sie_id'];
			// 		}unset($auth_app);
					
			// 		$key_arr = array_keys($combo_suite_arr);
			// 		$key = '';
			// 		foreach ($key_arr as $item) {
			// 			if (in_array($item, $sie_ids)) {
			// 				$key = $item;
			// 				break;
			// 			}
			// 		}unset($item);
					
			// 		if (empty($key)) {
			// 			throw new \Exception('请先安装应用');
			// 		}
			// 		$suite_id = $combo_suite_arr[$key];
			// 		$auth_code = $auth_code_arr[$key];
			// 	}
			// 	$token = $this -> _get_by_suite_id($auth_code, $suite_id);
				
			// } else {
			// 	if (empty($this -> corp_secret) || empty($this -> corp_id)) {
			// 		throw new \Exception('请先安装应用后再使用该功能');
			// 	}
				
			// 	$token = $this -> _get_by_secret();
			// }
			
			if(empty(!$this -> app_id)){
				if(empty($this -> com_id)){
					$com = g('dao_com') -> get_by_corp_id($this -> corp_id, '*');
					$this -> com_id = $com['id'];
				}
				$app_info = g('dao_com_app') -> get_by_app_id($this -> app_id, $this -> com_id);
				if(!empty($app_info['secret'])){
					$this -> corp_secret = $app_info['secret'];
				}
			}
			if (empty($this -> corp_secret) || empty($this -> corp_id)) {
				throw new \Exception('请先安装应用后再使用该功能');
			}
			$token = $this -> _get_by_secret();

			parent::log_i('获取成功，corp='.$this -> corp_id.', token：'.$token);
			return $token;
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	//---------------------------------------------内部实现
	
	/**
	 * 根据套件信息获取access token
	 * @param unknown_type $auth_code	套件永久授权码
	 * @param unknown_type $suite_id	套件ID
	 */
	private function _get_by_suite_id($auth_code, $suite_id, $with_redis=TRUE) {
		try{
			//初始化缓存key
			$atoken_key_el = array(
				$this -> atoken_cache_key_prefix,
				$this -> corp_id,
				$suite_id,
			);
			$atoken_key = md5(implode(':', $atoken_key_el));
			$atoken_time_key_el = array(
				$this -> atoken_time_cache_key_prefix,
				$this -> corp_id,
				$suite_id,
			);
			$atoken_time_key = md5(implode(':', $atoken_time_key_el));
			
			$token = '';
			$time = 0;
			$with_redis && $token = g('pkg_redis') -> get($atoken_key);
			$with_redis && $time = g('pkg_redis') -> get($atoken_time_key);
			$is_over_time = (time() - $time >= $this -> atoken_alive_time) ? TRUE : FALSE;		//是否过期
			
			if ($is_over_time || empty($token)) {
				$suite = g('dao_suite') -> get_by_suite_id($suite_id);
				if(!$suite)			throw new \Exception('不存在的套件');
				
				$suite_token = $this -> _get_stoken($suite_id);
				parent::log_i("suite_id={$suite_id}, suite_token={$suite_token}");
				
				$token =  g('wxqy_suite') -> update_access_token($suite_id, $suite_token, $this -> corp_id, $auth_code);
				if($token){
					$with_redis && g('redis') -> set($atoken_key, $token, 3600);
					$with_redis && g('redis') -> set($atoken_time_key, time(), 3600);
					
				} else {
					throw new \Exception('获取access token失败');
				}
			}
			return $token;
			
		} catch(\RedisException $e) {
			if ($with_redis) {
				return $this -> _get_by_suite_id($auth_code, $suite_id, FALSE);	
			}
			throw $e;
			
		} catch(\Exception $e) {
			throw $e;
			
		}
	}
	
	/**
	 * 根据企业id，secret获取access token
	 */
	private function _get_by_secret($with_redis=TRUE) {
		try {
			//初始化缓存key
			$atoken_key_el = array(
				$this -> atoken_cache_key_prefix,
				$this -> corp_id,
				$this -> corp_secret,
			);
			$atoken_key = md5(implode(':', $atoken_key_el));
			$atoken_time_key_el = array(
				$this -> atoken_time_cache_key_prefix,
				$this -> corp_id,
				$this -> corp_secret,
			);
			$atoken_time_key = md5(implode(':', $atoken_time_key_el));
			
			$token = '';
			$time = 0;
			$with_redis && $token = g('pkg_redis') -> get($atoken_key);
			$with_redis && $time = g('pkg_redis') -> get($atoken_time_key);
			$is_over_time = (time() - $time >= $this -> atoken_alive_time) ? TRUE : FALSE;		//是否过期
			
			if ($is_over_time || empty($token)) {
				$new_one = g('wxqy_token') -> update_access_token($this -> corp_id, $this -> corp_secret);
				$with_redis && g('pkg_redis') -> set($atoken_key, $new_one['token'], 3600);
				$with_redis && g('pkg_redis') -> set($atoken_time_key, time(), 3600);
				$token = $new_one['token'];
			}
			parent::log_i("corpid={$this -> corp_id}, access token={$token}");
			return $token;
			
		} catch(\RedisException $e) {
			if ($with_redis) {
				return $this -> _get_by_secret(FALSE);
			}
			throw $e;
			
		} catch(\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取套件令牌
	 * @param unknown_type $suite_id	套件编号
	 */
	private function _get_stoken($suite_id, $with_redis=TRUE) {
		$suite = g('dao_suite') -> get_by_suite_id($suite_id);
		if(!$suite){
			parent::log_w('suite_id 错误: suite_id='.$suite_id);
			return;
		}
		
		try{
			//初始化缓存key
			$stoken_key_el = array(
				$this -> stoken_cache_key_prefix,
				$suite_id,
			);
			$stoken_key = md5(implode(':', $stoken_key_el));
			$stoken_time_key_el = array(
				$this -> stoken_time_cache_key_prefix,
				$suite_id,
			);
			$stoken_time_key = md5(implode(':', $stoken_time_key_el));
			
			parent::log_i('开始获取suite token: suite_id='.$suite_id);
			$token = '';
			$time = 0;
			$with_redis && $token = g('pkg_redis') -> get($stoken_key);
			$with_redis && $time = g('pkg_redis') -> get($stoken_time_key);
			$is_over_time = (time() - $time >= $this -> stoken_alive_time) ? TRUE : FALSE;		//是否过期
		
			if ($is_over_time || empty($token)) {
				$token = g('wxqy_suite') -> update_suite_token($suite['suite_id'], $suite['suite_secret'], $suite['suite_ticket']);
				$with_redis && g('pkg_redis') -> set($stoken_key, $token, 3600);
				$with_redis && g('pkg_redis') -> set($stoken_time_key, time(), 3600);
			}
			
			parent::log_i('获取suite token成功: suite_id='.$suite_id.', token：'.$token);
			return $token;
			
		} catch (\RedisException $e) {
			if ($with_redis) {
				return $this -> _get_stoken($suite_id, FALSE);	
			}
			throw $e;
			
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
}

// end of file
