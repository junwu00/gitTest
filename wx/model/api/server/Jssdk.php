<?php
/**
 * js_sdk 签名类
 *
 * @author LiangJianMing
 * @create 2015-01-16
 */
namespace model\api\server;
class Jssdk extends \model\api\server\Base{
	//三尺科技的认证企业号公司id
	const auth_com_id = 0;
	//三尺科技的认证企业号crop_id
	const auth_corp_id = '';
	
	//是否显示日志
	private static $do_log = true;

	public function __construct($log_pre_str='') {
		empty($log_pre_str) && $log_pre_str = "[Jssdk]";
		parent::__construct($log_pre_str);
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 自定义字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$preffix = 'domain:'.MAIN_DYNAMIC_DOMAIN.':class:'.__CLASS__.':';
		$key_str = md5($preffix.$str);
		return $key_str;
	}

	/**
	 * 获取企业号的jsapi_ticket
	 *
	 * @access public
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return string
	 */
	public function get_jsapi_ticket($com_id = 0) {
		empty($com_id) && $com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		if (empty($com_id)) {
			throw new \Exception('非法访问');
		}
		
		$key_str = __FUNCTION__.':com_id:'.$com_id;
		$cache_key = $this -> get_cache_key($key_str);

		$ret = g('pkg_redis') -> get($cache_key);
		if (empty($ret)) {
			$access_token = g('api_atoken') -> get_by_com($com_id);

			$url = 'https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token='.$access_token;
			$result = g('pkg_http') -> quick_get($url);

			if ($result['httpcode'] == 200) {
				$data = json_decode($result['content'], TRUE);
				isset($data['ticket']) && $ret = $data['ticket'];
				$ttl = isset($data['expires_in']) ? ($data['expires_in'] - 1000) : 3600;
				!empty($ret) && g('pkg_redis') -> set($cache_key, $ret, $ttl);

			} else {
				throw new \Exception('获取jsapi ticket失败');	
			}
		}
		return $ret;
	}

	/**
	 * 获取js_sdk签名
	 *
	 * @access public
	 * @param array $data 加密所需的其他信息集合
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return mixed
	 */
	public function get_jsapi_sign(array $data, $com_id = 0) {
		$data['jsapi_ticket'] = $this -> get_jsapi_ticket($com_id);
		if (empty($data['jsapi_ticket'])) {
			//throw new \Exception('获取jsapi ticket失败');
			return false;
		}
		self::$do_log && parent::log_i('jsapi_ticket: '.$data['jsapi_ticket']);

		ksort($data);
		$str = '';
		foreach ($data as $key => $val) {
			$str .= $key.'='.$val.'&';
		}
		unset($key);
		unset($val);
		$str = substr($str, 0, -1);
		$sign = sha1($str);

		return $sign;
	}
	
	/**
	 * 在smarty中声明js-sdk相关的变量
	 *
	 * @access public
	 * @param string $app_id 指定使用该企业号的微信app_id
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return void
	 */
	public function assign_jssdk_sign($app_id = '', $com_id = 0) {
		$time = time();
		$nonce_str = get_rand_str(10);
		$this_url = get_this_url();

		$params = array(
			'noncestr' => $nonce_str,
			'timestamp' => $time,
			'url' => $this_url,
		);
		try{
			$sign = $this -> get_jsapi_sign($params, $com_id);
		}catch(\Exception $e){
			$sign = "";
		}

		if (empty($app_id)) {
			if (!empty($com_id)) {
				$app_id = $this -> get_crop_id($com_id);
			}else {
				$app_id = $_SESSION[SESSION_VISIT_CORP_ID];
			}
		}
		
		g('pkg_smarty') -> assign('app_id', $app_id);
		g('pkg_smarty') -> assign('time', $time);
		g('pkg_smarty') -> assign('nonce_str', $nonce_str);
		g('pkg_smarty') -> assign('this_url', $this_url);
		g('pkg_smarty') -> assign('sign', $sign);
		
		if (self::$do_log) {
			parent::log_i('app_id: '.$app_id);	
			parent::log_i('time: '.$time);	
			parent::log_i('nonce_str: '.$nonce_str);	
			parent::log_i('this_url: '.$this_url);	
			parent::log_i('sign: '.$sign);	
		}
	}

    /**
     * js-sdk相关的变量
     * @param string $app_id
     * @param int $com_id
     * @return array
     * ModifyUser: hwp
     * ModifyDate: 2023/6/15 22:22
     */
    public function assign_jssdk_sign2($this_url, $app_id = '', $com_id = 0) {
        $time = time();
        $nonce_str = get_rand_str(10);
        // $this_url = get_this_url();
        $this_url = urldecode($this_url);
        $params = array(
            'noncestr' => $nonce_str,
            'timestamp' => $time,
            'url' => $this_url,
        );
        try{
            $sign = $this -> get_jsapi_sign($params, $com_id);
        }catch(\Exception $e){
            $sign = "";
        }

        if (empty($app_id)) {
            if (!empty($com_id)) {
                $app_id = $this -> get_crop_id($com_id);
            }else {
                $app_id = $_SESSION[SESSION_VISIT_CORP_ID];
            }
        }

        if (self::$do_log) {
            parent::log_i('app_id: '.$app_id);
            parent::log_i('time: '.$time);
            parent::log_i('nonce_str: '.$nonce_str);
            parent::log_i('this_url: '.$this_url);
            parent::log_i('sign: '.$sign);
        }

        return [
            'app_id' => $app_id,
            'time' => $time,
            'nonce_str' => $nonce_str,
            'this_url' => $this_url,
            'sign' => $sign,
        ];
    }

	/**
	 * 根据公司id获取crop_id
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @return mixed
	 */
	public function get_crop_id($com_id) {
		$key_str = __FUNCTION__ . '::' . $com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		if ($ret !== FALSE) return $ret;

		$fields = 'corp_id';
		$cond = array(
				'id=' => $com_id,
			);
		$table = 'sc_company';
		$ret = g('pkg_db') -> select($table, $fields, $cond);
		$ret = $ret[0];
		//缓存1小时
		g('pkg_redis') -> set($cache_key, $ret, 3600);

		return $ret ? $ret : FALSE;
	}

	/**
	 * 使用三尺科技认证企业号生成js_sdk配置
	 *
	 * @access public
	 * @param integer  
	 * @return boolean
	 */
	public function auth_assign_jssdk_sign() {
		$com_id = self::auth_com_id;
		$crop_id = self::auth_corp_id;
		$this -> assign_jssdk_sign($crop_id, $com_id);
	}

//=================================私有方法================
	/** 
	 * 生成随机字符串
	 * 
	 * @global
	 * @param integer $leng 需要生成的字符串长度
	 * @param integer $type 字符串类型,取值：1-15，改值会先转换为2进制，如：14=1110，第一位表示使用特殊符号、第二位表示使用数字、第三位表示使用小写字母、第四位表示不使用大写字母
	 * @param boolean $dark 是否去除字符集{O,o,0}
	 * @return string
	 */
	private function get_rand_string($leng, $type=7, $dark=FALSE){
	    $tmp_array = array(
	        '1' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
	        '2' => 'abcdefghijklmnopqrstuvwxyz',
	        '4' => '0123456789',
	        '8' => '~!@$&()_+-=,./<>?;\'\\:"|[]{}`'
	    );
	    $return = $target_string = '';
	    $array = array();
	    $bin_string = decbin($type);
	    $bin_leng  = strlen($bin_string);
	    for($i = 0; $i < $bin_leng; $i++) if($bin_string{$i} == 1) $array[] = pow(2, $bin_leng - $i - 1);
	    if(in_array(1, $array, TRUE)) $target_string .= $tmp_array['1'];
	    if(in_array(2, $array, TRUE)) $target_string .= $tmp_array['2'];
	    if(in_array(4, $array, TRUE)) $target_string .= $tmp_array['4'];
	    if(in_array(8, $array, TRUE)) $target_string .= $tmp_array['8'];
	    $target_leng = strlen($target_string);
	    mt_srand((double)microtime()*1000000);
	    while(strlen($return) < $leng){
	        $tmp_string = substr($target_string, mt_rand(0, $target_leng), 1);
	        $dark && $tmp_string = (in_array($tmp_string, array('0', 'O', 'o'))) ? '' : $tmp_string;
	        $return .= $tmp_string;
	    }
	    return $return;
	}

}

/* End of this file */