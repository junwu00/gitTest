<?php
/**
 * 基于http长轮询的消息操作类
 *
 * @author LiangJianMing
 * @create 2015-10-16
 */
namespace model\api\server;
class Messager extends \model\api\server\Base{
    // 定义可用的消息分类集合，例如：pc、wx、qy
    private $types = array(
        'qy' => 'qy',
        'pc' => 'pc',
        'wx' => 'wx',
    );

    // 当前用户id
    private $user_id = 0;

    // 消息服务地址前缀
    private $msg_preffix = '';

    // 用户信息表
    private $user_table = 'sc_user';
    // 部门信息表
    private $dept_table = 'sc_dept';

    public function __construct($log_pre_str = '') {
        !empty($_SESSION[SESSION_VISIT_USER_ID]) and $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];

        defined('MESSAGER_PREFFIX') and $this -> msg_preffix = MESSAGER_PREFFIX;
        empty($log_pre_str) && $log_pre_str = "[Messager][user_id:{$this -> user_id}]";
        parent::__construct($log_pre_str);
    }

    /**
     * 如果不是使用当前用户id，则必须执行该函数
     *
     * @access public
     * @param integer $user_id 用户id
     * @return void
     */
    public function set_init($user_id=0) {
        $this -> user_id = $user_id;
    }

    /**
     * 发布消息，支持批量发布
     *
     * @access public
     * @param mixed $channels 频道id集合，如果是单个字符串，则自动转变为数组
     * @param array $data 数据集合，关键参数说明如下
     *      => string msg_id 该消息的唯一md5，用作执行消息的删除时使用
     *      => integer type 消息状态：1-正常、2-删除，默认为1，该参数是必须的
     *      => integer time 该消息发送的业务时间戳，如果没有则自动补全
     * @return boolean
     */
    public function publish($channels, array $data) {
        if (empty($channels) or empty($data)) {
            return false;
        }
        !is_array($channels) and $channels = array($channels);

        $api_url = $this -> msg_preffix . "api/pub";

        $get_param = array(
            'time' => time(),
        );
        $sign = g('pkg_sign') -> get_sign($get_param, MESSAGER_KEY);
        $get_param['sign'] = $sign;
        $api_url .= "?" . http_build_query($get_param);

        !isset($data['type']) and $data['type'] = 1;
        !isset($data['time']) and $data['time'] = time();
        // 如果没有指定msg_id，则自动计算
        ksort($data);
        empty($data['msg_id']) and $data['msg_id'] = sha1(json_encode($data));

        $real_data = array(
            'channels' => implode(',', $channels),
            'msg' => json_encode($data),
        );
        $result = g('pkg_http') -> post($api_url, $real_data);
        $ret = TRUE;
        if (!is_array($result) or $result['httpcode'] != 200) {
            $ret = FALSE;
        }
        return $ret;
    }

    /**
     * 根据user_id获取消息消费地址
     *
     * @access public
     * @param string $type $this -> types 的索引，如：pc、qy、wx
     * @param integer $user_id 用户id，如果为0表示获取当前用户
     * @return string 如果获取失败返回空字符串
     */
    public function get_consume_url_by_user($type, $user_id=0) {
        $channels = $this -> get_channel_by_users($type, $user_id);

        $url = $this -> get_consume_url(array_shift($channels));
        return $url;
    }

    /**
     * 快速发布消息
     *
     * @access public
     * @param string $type $this -> types 的索引，如：pc、qy、wx
     * @param integer $user_id 用户id，如果为0表示获取当前用户
     * @param array $data 数据集合，关键参数说明如下
     *      => string msg_id 该消息的唯一md5，用作执行消息的删除时使用
     *      => integer type 消息状态：1-正常、2-删除，默认为1，该参数是必须的
     *      => integer time 该消息发送的业务时间戳，如果没有则自动补全
     * @return boolean
     */
    public function publish_by_user($type, $user_id, array $data=array()) {
        $channels = $this -> get_channel_by_users($type, $user_id);
        if (empty($channels)) return false;

        return $this -> publish($channels, $data);
    }

    /**
     * 支持使用部门id及用户id集合发送推送消息
     *
     * @access public
     * @param string $type $this -> types 的索引，如：pc、qy、wx
     * @param array $dept_ids 部门id集合
     * @param array $user_ids 用户id集合
     * @param array $data 数据集合，关键参数说明如下
     *      => string msg_id 该消息的唯一md5，用作执行消息的删除时使用
     *      => integer type 消息状态：1-正常、2-删除，默认为1，该参数是必须的
     *      => integer time 该消息发送的业务时间戳，如果没有则自动补全
     * @param integer $root_id 所在公司的根部门id，默认取session中的值
     * @return boolean 返回true表示全部消息发送成功，否则返回false
     */
    public function publish_by_ids($type, array $dept_ids, array $user_ids, array $data=array(), $root_id=0) {
        $ret = false;

        if (!isset($this -> types[$type])) return $ret;

        if (empty($dept_ids) and empty($user_ids)) {
            return $ret;
        }

        empty($root_id) and isset($_SESSION[SESSION_VISIT_DEPT_ID]) and $root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
        
        if (empty($root_id)) {
            return $ret;
        }

        $duser_ids = array();
        !empty($dept_ids) and $duser_ids = $this -> get_users_by_depts($root_id, $dept_ids);
        $user_ids = array_merge($duser_ids, $user_ids);
        $user_ids = array_unique($user_ids);
        $channels = $this -> get_channel_by_users($type, $user_ids);
        $ret = $this -> publish($channels, $data);
        return $ret;
    }

    /**
     * 获取消息消费地址
     *
     * @access public
     * @param string $channel 频道id
     * @return string
     */
    public function get_consume_url($channel) {
        $param = array(
            'channel' => $channel,
            'time' => time(),
        );

        $sign = g('pkg_sign') -> get_sign($param, MESSAGER_KEY);
        $param['sign'] = $sign;

        $param = http_build_query($param);

        $url = $this -> msg_preffix . "api/subs?{$param}";
        return $url;
    }

//-------------------以下是内部实现--------------------------------------
    /**
     * 获取频道id
     *
     * @access private
     * @param string $str 关键字符串
     * @return string
     */
    private function get_channel_id($str='') {
        $key_str = PC_DYNAMIC_DOMAIN . "::ew365::channel::" . $str;
        $key = md5($key_str);
        return $key;
    }

    /**
     * 根据user_id获取发布对应的消息频道
     *
     * @access private
     * @param string $type $this -> types 的索引，如：pc、qy、wx
     * @param mixed $user_ids 用户id，如果为0表示获取当前用户，如果该值是int型，则自动转换为array
     * @return array
     */
    private function get_channel_by_users($type, $user_ids=0) {
        $user_ids === 0 and $user_ids = $this -> user_id;
        !is_array($user_ids) and $user_ids = array($user_ids);

        $salt = '';
        isset($this -> types[$type]) and $salt = $this -> types[$type];

        if (empty($salt)) {
            return '';
        }

        $ret = array();

        foreach ($user_ids as $val) {
            $str = "{$salt}:user_id:{$val}";
            $ret[] = $this -> get_channel_id($str);
        }

        return $ret;
    }

    /**
     * 获取指定部门下的全部用户id，包含子部门
     *
     * @access private
     * @param integer $root_id 根部门id
     * @param mixed $dept_ids 部门id集合
     * @return array
     */
    private function get_users_by_depts($root_id, $dept_ids) {
        $ret = array();

        $table = $this -> user_table;
        $fields = array(
            'id'
        );
        $fields = implode(',', $fields);
        
        !is_array($dept_ids) and $dept_ids = array($dept_ids);
        
        $tmp_depts = array();
        foreach ($dept_ids as $val) {
            $tmp = intval($val);
            !empty($tmp) and $tmp_depts[] = $tmp;
        }
        $dept_ids = &$tmp_depts;

        if (empty($dept_ids)) {
            return $ret;
        }

        $condition = array(
            'root_id=' => $root_id,
            'state=' => 1,
        );
        $condition['dept_tree REGEXP '] = '"(' . implode('|', $dept_ids) . ')"';

        $order = ' ORDER BY id ASC ';
        $ret = g('pkg_db') -> select($table, $fields, $condition, 0, 0, '', $order);
        !is_array($ret) and $ret = array();

        $tmp_arr = array();
        foreach ($ret as $val) {
            $tmp_int = intval($val['id']);
            !empty($tmp_int) and $tmp_arr[] = $tmp_int;
        }
        unset($val);

        $ret = $tmp_arr;
        
        return $ret;
    }
}

/* End of this file */
