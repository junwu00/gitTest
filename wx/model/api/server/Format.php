<?php
/**
 * 自定义格式化类
 *
 * @author LiangJianMing
 * @create 2014-07-04
 * @version 1.0
 */
namespace model\api\server;
class Format{
	/**
	 * 将时间戳格式化成更友好的时间
	 *
	 * @access public
	 * @param integer $time 		时间戳
	 * @param boolean $width_sec 	是否显示秒数
	 * @return string
	 */
	public function http_time_format($time, $width_sec=TRUE) {
		if (time() - $time < 0) return FALSE;
		
		$this_time = time();
		$poor = $this_time - $time;
		$des = '';
		if ($poor < 30) {
			$des .= '刚刚';
		}else {
			$width_sec && ($sec = $poor % 60) != 0 && $des = $sec.'秒前';
			
			$temp = intval( $poor / 60);
			$temp != 0 && ($min = $temp % 60) != 0 && $des = $min.'分钟'.$des;
			!$width_sec && $des.='前';
			
			$temp = intval($temp / 60);
			$temp != 0 && ($hor = $temp % 24) != 0 && $des = $hor.'小时'.$des;
			$temp = intval($temp / 24);
			$temp != 0 && $des = $temp.'天'.$des;
		}
		$date = date('Y-m-d H:i:s', $time);

		!$width_sec && $des == '前' && $des = '刚刚';
		return $des;
	}
	
	/**
	 * 将时间戳格式化成更友好的时间
	 *
	 * @access public
	 * @param integer $time 		时间戳
	 * @param boolean $width_sec 	是否显示秒数
	 * @param integer $max_interval 最大时间间隔，超过该时间直接返回当前日期，默认是30天
	 * @return string
	 */
	public function friendly_time_format($time, $width_sec=TRUE, $max_interval=2592000) {
		$this_time = time();
		$poor = $this_time - $time;

		if ($poor < 0) return FALSE;
		if ($poor >= $max_interval) {
			return date('Y-m-d H:i:s', $time);
		}
		
		$des = '';
		if ($poor < 30) {
			$des .= '刚刚';
		}else {
			$width_sec and ($sec = $poor % 60) != 0 and $des = $sec.'秒前';
			
			$temp = intval($poor / 60);
			$temp != 0 and ($min = $temp % 60) != 0 and $des = $min.'分钟'.$des;
			!$width_sec and $des.='前';
			
			$temp = intval($temp / 60);
			$temp != 0 and ($hor = $temp % 24) != 0 and $des = $hor.'小时'.$des;
			$temp = intval($temp / 24);
			$temp != 0 and $des = $temp.'天'.$des;
		}
		$date = date('Y-m-d H:i:s', $time);

		!$width_sec and $des == '前' and $des = '刚刚';
		return $des;
	}
	
	/**
	 * 将秒数转换成具体的X天X小时X分X秒
	 * @param unknown_type $second  秒数(不支持负数)
	 * @param unknown_type $format	要转换的格式 	d天H小时i分s秒
	 */
	public function second_to_date($second,$format) {
		if(!$second || !is_numeric($second) || $second<0){
			return false;
		}
		$d = $H = $i = $s =0;
		
		if(strpos($format,'d')!==false){
			$d = intval($second/(60*60*24));
			$format = str_replace('d',$d, $format);
		}
		if(strpos($format,'H')!==false){
			$H = intval($second/(60*60))%24;
			$format = str_replace('H', $H, $format);
		}
		if(strpos($format,'i')!==false){
			$i =  intval($second/(60))%60;
			$format = str_replace('i',$i, $format);
		}
		if(strpos($format,'s')!==false){
			$s = $format = str_replace('s',$second - ((($d*24)+$H)*60 +$i)*60, $format);
		}
		
		return $format;
	}
}