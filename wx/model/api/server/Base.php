<?php

namespace model\api\server;

abstract class Base {
	
	/** 当前模块名称 */
	protected $mod_name;
	/** 当前模块中文名称 */
	protected $mod_cn_name;
	/** 当前模块对应的视图的根目录 */
	protected $view_dir;
	/** 当前模块对应的日志存放的根目录 */
	protected $log_dir;
	/** 当前模块对应的本地上传的根目录 */
	protected $upload_dir;

	/** 当前模块需记录的日志级别 */
	protected $log_lev;
	/** 当前模块对应的正常/警告日志的路径 */
	protected $log_file_path;
	/** 当前模块对应的错误日志的路径 */
	protected $err_file_path;
	
	/** 日志中一般要补充的前置的信息，如：[com:123][admin:123] */
	protected $log_pre_str;
	
	/** 缓存key前缀 */
	protected $cache_key_prefix;
	
	/**
	 * 构造方法
	 * 初始化模块内通用配置、变量等
	 * 
	 * $target_model_conf	当前调用该API的模块的配置，要
	 * $log_pre_str 		日志中一般要补充的前置的信息，如：[com:123][admin:123]
	 */ 
	public function __construct($log_pre_str='') {
		load_config('model/api/classmap');
		$mod_conf = load_config('model/api/model');
		$user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : '';
		$this -> log_pre_str = $log_pre_str."[user_id:{$user_id}]";
		
		$this -> mod_name 		= $mod_conf['name'];
		$this -> mod_cn_name 	= $mod_conf['cn_name'];
		$this -> view_dir 		= $mod_conf['view_dir'];
		$this -> log_dir 		= $mod_conf['log_dir'];
		$this -> upload_dir		= $mod_conf['upload_dir'];
		
		$today = date('Ymd');
		$this -> log_lev 		= $mod_conf['log_lev'];
		$this -> log_file_path	= $this -> log_dir . DS . $today . '.log';
		$this -> err_file_path	= $this -> log_dir . DS . $today . '.error.log';
		
		$this -> cache_key_prefix = MAIN_HOST;
		
		// $smarty_conf = load_config('package/template/smarty');
		// g('pkg_smarty') -> get_handler() -> clearCompiledTemplate();
		// g('pkg_smarty') -> get_handler() -> addPluginsDir($mod_conf['smarty_plugin']);
		// g('pkg_smarty') -> assign('VIEW_ROOT', $smarty_conf['template_dir']);
		// g('pkg_smarty') -> assign('title', MAIN_NAME);										//设置页面默认title，避免具体页面记录配置而显示异常
		
		// $this -> _assign_static_res_conf();
	}
	
	//分级日志记录：由配置文件中log_lev参数控制===========================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str) {
		if ($this -> log_lev > 0)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str) {
		if ($this -> log_lev > 1)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str) {
		if ($this -> log_lev > 2)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str) {
		if ($this -> log_lev > 3)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str) {
		if ($this -> log_lev > 4)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	//END 分级日志记录===========================================================================================
	
	/**
	 * 根据指定条件获取信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function _get_by_cond($table, array $cond, $fields='*') {
		if (empty($cond) || empty($table))	return FALSE;
		
		$ret = g('pkg_db') -> select($table, $fields, $cond, 1, 1);
		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 根据指定条件获取应用信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function _list_by_cond($table, array $cond, $fields='*') {
		if (empty($cond) || empty($table))	return array();
		
		$ret = g('pkg_db') -> select($table, $fields, $cond);
		return $ret ? $ret : array();
	}
	
	/**
	 * 分页查询
	 * @param array $cond				查询条件
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $order_by	排序
	 * @param unknown_type $with_cnt	是否返回总数，默认：FALSE
	 */
	protected function _list_by_page($table, array $cond, $fields, $page, $page_size, $order_by='', $with_cnt=FALSE) {
		if (empty($cond) || empty($table))	return array();
		
		$ret = g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, '', $order_by, $with_cnt);
		if ($with_cnt) {
			$ret = array(
                'data' 	=> $ret['data'] ? $ret['data'] : array(),
                'count' => $ret['count'],
            );
		} else {
			$ret = $ret ? $ret : array();
		}
		return $ret;
	}
	
	//根据当前请求结果返回数据或页面===============================================================================

	/**
	 * 重定向到提示页面/输出提示信息
	 * @param unknown_type $reason_arr
	 */
	protected function show_notice_page($reason_arr) {
		!is_array($reason_arr) && $reason_arr = array($reason_arr);
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {		//返回数据
			!empty($reason_arr) && !is_array($reason_arr) && $reason_arr = array($reason_arr);
			g('api_resp') -> echo_resp(sg('api_resp', 'Notice'), $reason_arr);
			
		} else {													//返回页面			
			g('api_notice') -> notice($reason_arr);
		}
	}

	/**
	 * 重定向到警告页面/输出警告信息
	 * @param unknown_type $reason_arr
	 */
	protected function show_warn_page($reason_arr) {
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {		//返回数据
			!empty($reason_arr) && !is_array($reason_arr) && $reason_arr = array($reason_arr);
			g('api_resp') -> echo_resp(sg('api_resp', 'Warn'), $reason_arr);
			
		} else {													//返回页面			
			g('api_notice') -> warn($reason_arr);
		}
	}

	/**
	 * 重定向到错误页面/输出错误信息
	 * @param unknown_type $reason_arr
	 */
	protected function show_err_page($reason_arr) {
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {		//返回数据
			!empty($reason_arr) && !is_array($reason_arr) && $reason_arr = array($reason_arr);
			g('api_resp') -> echo_resp(sg('api_resp', 'Error'), $reason_arr);
			
		} else {													//返回页面			
			g('api_notice') -> error($reason_arr);
		}
	}
	
	//END 根据当前请求结果返回数据或页面===========================================================================
	
	
	/** 传递静态资源配置内容 */
	private function _assign_static_res_conf() {
		$static_conf_dir = MAIN_VIEW . 'config' . DS;
		$static_conf_common = file_get_contents($static_conf_dir . 'common-map.json');
		$static_conf_common = json_decode($static_conf_common, TRUE);
		
		$static_conf_self = file_get_contents($static_conf_dir . $this -> mod_name . '-map.json');
		$static_conf_self = json_decode($static_conf_self, TRUE);
		
		$static_conf = array_merge($static_conf_common['res'], $static_conf_self['res']);
		
		$static_res = array();
		foreach ($static_conf as $key => $item) {
			if ($item['type'] === 'js') {
				if (preg_match('/^common\:(.*)jquery\.(.*)\.js$/', $key)) {
					$key = 'jquery';
				} else {
					$key = str_replace('.js', '', $key);
				}
				$static_res[$key] = substr($item['uri'], 0, strlen($item['uri']) - 3);
				
			} else if ($item['type'] === 'css') {
				$key = str_replace('.css', '', $key);
				$static_res[$key] = $item['uri'];
				$static_res[$key] = substr($item['uri'], 0, strlen($item['uri']) - 4);
			}
		}unset($item);
		echo "<br><br><br><br><br>";
		echo "<br><br><br><br><br>";
		g('pkg_smarty') -> assign('static_res', $static_res);
	}
	
}

//end