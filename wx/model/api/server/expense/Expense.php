<?php
/**
 * 报销
 * @author Luja
 * @date 2017-03-06
 */
namespace model\api\server\expense;

class Expense extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	private static $StateDraft = 0;
	/** 运行中 */
	private static $StateDone = 1;
	/** 已结束 */
	private static $StateFinish = 2;
	/** 终止 */
	private static $StateStop = 3;

	/** 已支付 */
	private static $Is_Pay = 1;
	/** 未支付 */
	private static $Not_Pay = 0;

	/** 超级管理员 */
	private static $Is_Super = 1;

	private static $IsExpenseProc = 6;

	private static $Table_Type = 'expense_type';

	private static $Table_Record = 'expense_record';

	private static $Table_Config = 'expense_config';

	private static $Table_Invoice = 'expense_invoice';

	private static $Table_Form = 'mv_proc_form_model';

	private static $Table_Instlabel = 'mv_proc_formsetinst_label';

	private static $Table_Forminst = 'mv_proc_formsetinst';

	private static $Table_Rh_Classify = "runheng_account_classify";

	private static $Table_Rh_Acct = "runheng_acct";

	private static $Invoice_State_Num = array(
		'init' => 1,
		'lock' => 2,
		'closure' => 3
		);

	private static $Invoice_State = array(
		'1' => 'INVOICE_REIMBURSE_INIT',	//发票初始状态，未锁定
		'2' => 'INVOICE_REIMBURSE_LOCK',	//发票已锁定
		'3' => 'INVOICE_REIMBURSE_CLOSURE',	//发票已核销
		);

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}


	/** 获取报销配置 */
	public function get_expense_config($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id
			);

		return parent::_get_by_cond(self::$Table_Config , $cond, $fields);
	}

	/**
	 * 获取报销类型
	 * @param  [type] $com_id 
	 * @param  string $fields 
	 */
	public function get_expense_type($com_id, $fields='*', $ids=array()) {
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' => self::$InfoOn
			);
		!empty($ids) && is_array($ids) && $cond['id IN'] = $ids;

		return parent::_list_by_cond(self::$Table_Type, $cond, $fields);
	}


	/**
	 * 保存报销申请记录
	 * @param  [type]  $com_id            
	 * @param  [type]  $user_id           
	 * @param  [type]  $formsetinst_id    
	 * @param  [type]  $sum_money         总金额
	 * @param  [type]  $sum_money_capital 总金额（大写）
	 * @param  integer $is_electro_inv    是否是电子报销，0：否，1：是
	 * @return [type]                     
	 */
	public function save_record($com_id, $user_id, $formsetinst_id, $sum_money, $sum_money_capital, $is_electro_inv=0) {
		$time = time();
		$data = array(
			'com_id' 			=> $com_id,
			'create_id' 		=> $user_id,
			'sum_money' 		=> $sum_money,
			'sum_money_capital' => $sum_money_capital,
			'create_time' 		=> $time,
			'update_time' 		=> $time,
			'info_state' 		=> self::$InfoOn,
			'formsetinst_id' 	=> $formsetinst_id,
			'is_electro_invoice' => $is_electro_inv
			);

		$ret = g('pkg_db')->insert(self::$Table_Record, $data);
		if (!$ret) throw new \Exception('保存报销记录失败');
		
		return $ret;
	}

	/**
	 * 更新报销申请记录
	 * @param  [type]  $com_id            
	 * @param  [type]  $formsetinst_id    
	 * @param  [type]  $sum_money         总金额
	 * @param  [type]  $sum_money_capital 总金额（大写）
	 * @param  integer $is_electro_inv    是否是电子报销，0：否，1：是
	 * @return [type]                     
	 */
	public function update_record($com_id, $formsetinst_id, $sum_money, $sum_money_capital, $is_electro_inv=0) {
		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id,
			'info_state=' => self::$InfoOn
			);

		$data = array(
			'sum_money' 		=> $sum_money,
			'sum_money_capital' => $sum_money_capital,
			'update_time' 		=> time(),
			'is_electro_invoice' => $is_electro_inv
			);

		$ret = g('pkg_db')->update(self::$Table_Record, $cond, $data);
		if (!$ret) throw new \Exception('更新报销记录失败');
		
		return $ret;
	}

	/**
	 * 获取报销申请记录
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  string $fields         
	 * @return [type]                 
	 */
	public function get_record_by_id($com_id, $formsetinst_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id,
			'info_state=' => self::$InfoOn
			);

		$ret = g('pkg_db')->select(self::$Table_Record, $fields, $cond);
		
		return $ret ? $ret[0] : false;
	}

	/**
	 * 保存发票信息
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  [type] $card_id        
	 * @param  [type] $encrypt_code   
	 * @param  [type] $file_hash      
	 * @param  [type] $invoice_state  
	 */
	public function save_invoice($com_id, $formsetinst_id, $card_id, $encrypt_code, $file_hash, $invoice_state) {
		$data = array(
			'com_id' 		=> $com_id,
			'card_id' 		=> $card_id,
			'encrypt_code' 	=> $encrypt_code,
			'file_hash' 	=> $file_hash,
			'invoice_state' => $invoice_state,
			'formsetinst_id' => $formsetinst_id,
			'info_state'	=> self::$InfoOn
			);

		$ret = g('pkg_db')->insert(self::$Table_Invoice, $data);
		if (!$ret) throw new \Exception('保存发票信息失败');
		
		return $ret;
	}


	/**
	 * 更新发票信息
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  [type] $card_id        
	 * @param  [type] $invoice_state  
	 */
	public function update_invoice($com_id, $formsetinst_id, $card_id, $invoice_state) {
		$cond = array(
			'com_id=' 			=> $com_id,
			'formsetinst_id=' 	=> $formsetinst_id,
			'card_id=' 			=> $card_id,
			'info_state='		=> self::$InfoOn
			);
		
		$data = array(
			'invoice_state' => $invoice_state,
			);

		$ret = g('pkg_db')->update(self::$Table_Invoice, $cond, $data);
		if (!$ret) throw new \Exception('更新发票信息失败');
		
		return $ret;
	}

	/**
	 * 删除发票信息
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  [type] $card_id        
	 * @param  [type] $id        
	 */
	public function del_invoice($com_id, $formsetinst_id, $card_id='', $id=array()) {
		$cond = array(
			'com_id=' 			=> $com_id,
			'formsetinst_id=' 	=> $formsetinst_id,
			);
		
		!empty($card_id) && $cond['card_id='] = $card_id;
		!empty($id) && $cond['id IN'] = $id;
		$data = array(
			'info_state' => self::$InfoDelete
			);

		$ret = g('pkg_db')->update(self::$Table_Invoice, $cond, $data);
		if (!$ret) throw new \Exception('更新发票信息失败');
		
		return $ret;
	}

	/**
	 * 删除发票附件
	 * @param  [type] $com_id         
	 * @param  [type] $user_id        
	 * @param  [type] $formsetinst_id 
	 * @param  [type] $file_hash      
	 * @return [type]                 
	 */
	public function del_invoice_file($com_id, $user_id, $formsetinst_id, $file_hash){
		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id,
			'file_key=' => $file_hash,
			'info_state=' => self::$InfoOn
		);

		$data = array(
			'info_state' => self::$InfoDelete,
			'info_time' => time(),
			'info_userid' => $user_id
		);
		$ret = g('pkg_db')->update(self::$Table, $cond, $data);

		return $ret;
	}

	/**
	 * 获取实例下的发票信息
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  string $fields         
	 * @return [type]                 
	 */
	public function get_invoice_by_id($com_id, $formsetinst_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id,
			'info_state=' => self::$InfoOn
			);

		$ret = g('pkg_db')->select(self::$Table_Invoice, $fields, $cond);

		return $ret ? $ret : array();
	}

	/**
	 * 更新电子发票状态
	 * @param  [type] $card_id      
	 * @param  [type] $encrypt_code 
	 * @return [type]               
	 */
	public function change_invoice_state($com_id, $card_id, $encrypt_code, $invoice_state) {
		$access_token = g('api_atoken') -> get_by_com($com_id);

		return g('wxqy_card')->update_invoice_info($access_token, $card_id, $encrypt_code, self::$Invoice_State[$invoice_state]);
	}

	/** 获取我的报销记录 */
	public function get_record_list($com_id, $user_id, $cond=array(), $fields='', $page=1, $page_size=10, $order='', $with_count=FALSE){
		$table = self::$Table_Record.' er, '.self::$Table_Forminst.' mpf ';

		empty($fields) && $fields = 'er.*,mpf.formsetinst_name,mpf.creater_id, mpf.creater_name,mpf.state formsetinst_state';
		$cond['er.com_id='] 		= $com_id;
		$cond['er.create_id='] 		= $user_id;
		$cond['^mpf.id='] 			= 'er.formsetinst_id';
		$cond['er.info_state='] 	= self::$InfoOn;
		$cond['mpf.state!='] 		= self::$StateDraft;
		$cond['mpf.info_state='] 	= self::$InfoOn;
		$cond['mpf.is_other_proc='] = self::$IsExpenseProc;

		empty($order) && $order = ' ORDER BY mpf.id DESC';
		
		$ret = g('pkg_db')->select($table, $fields, $cond, $page, $page_size, '', $order, true);
		return $ret;
	}

	/**
	 * 获取表单实例控件中的报销类型
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @param  string $fields         
	 * @return [type]                 
	 */
	public function get_type_by_label($formsetinst_id, $fields='*', $input_type='', $input_key='') {
		$cond = array(
			'formsetinst_id=' => $formsetinst_id,
			'info_state=' => self::$InfoOn
			);

		!empty($input_key) && $cond['input_key='] = $input_key;
		!empty($input_type) && $cond['type='] = $input_type;

		$ret = g('pkg_db')->select(self::$Table_Instlabel, $fields, $cond);

		return $ret ? $ret : array();
	}

	/**
	 * 删除报销单
	 * @param  [type] $com_id         
	 * @param  [type] $formsetinst_id 
	 * @return [type]                 
	 */
	public function delete_expense($com_id, $formsetinst_id) {
		$record = $this->get_record_by_id($com_id, $formsetinst_id);
		if (!$record) throw new \Exception('查询报销单记录失败');
		// if ($record['is_electro_invoice ']) {
		// 	$invoices = $this->get_invoice_by_id($com_id, $formsetinst_id);
		// 	foreach ($invoices as $inv) {
		// 		if ($inv['invoice_state'] == 2) {
		// 			//解锁发票
		// 			$inv_ret = $this->change_invoice_state($com_id, $inv['card_id'], $inv['encrypt_code'], 1);
		// 		}

		// 		$this->del_invoice($com_id, $formsetinst_id, '', array($inv['id']));
		// 	}unset($inv);
		// }

		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id
			);

		$data = array(
			'info_state' => self::$InfoDelete,
			'update_time' => time()
			);

		$ret = g('pkg_db')->update(self::$Table_Record, $cond, $data);
		if (!$ret) throw new \Exception('删除报销记录失败');
		
		return true;
	}

	/**
	 * 结束报销申请
	 * @param  [type] $com_id         
	 * @param  [type] $user_id        
	 * @param  [type] $formsetinst_id 
	 * @return [type]                 
	 */
	public function expense_finish($com_id, $formsetinst_id) {
		$invs = $this->get_invoice_by_id($com_id, $formsetinst_id);
		if (empty($invs)) return true;

		foreach ($invs as $inv) {
			//核销发票
			$this->change_invoice_state($com_id, $inv['card_id'], $inv['encrypt_code'], 3);
			//更新状态
			$this->update_invoice($com_id, $formsetinst_id, $inv['card_id'], 3);
		}unset($inv);

		return true;
	}

	/**
	 * 检查指定管理员是否有应用所有权限
	 * @return
	 */
	public function check_admin_privilege($admin_id, $key){
		$info_state = self::$InfoOn;
		$is_super = self::$Is_Super;
		$sql = <<<EOF
		SELECT a.id
		FROM sc_admin a LEFT JOIN sc_admin_group g on a.group_id = g.id
		WHERE a.id = {$admin_id} and (a.is_super={$is_super} or g.power_list like '%"{$key}"%') and a.state = {$info_state}
EOF;
		$ret = g('pkg_db') -> query($sql);
		return $ret ? TRUE : FALSE;
	}

	/**
	 * 获取润衡科目树
	 * @param  [type] $com_id 
	 * @return [type]         
	 */
	public function get_subjects($com_id, $fields='*', $ids=array(), $pay_type=array(), $with_del=false) {
		$cond = array(
			'com_id=' => $com_id
			);
		!$with_del && $cond['info_state='] = self::$InfoOn;
		!empty($ids) && is_array($ids) && $cond['id IN'] = $ids;
		!empty($pay_type) && is_array($pay_type) && $cond['pay_type IN'] = $pay_type;

		$order = " ORDER BY p_id ASC ";
		$ret = g('pkg_db')->select(self::$Table_Rh_Classify, $fields, $cond);
		return $ret ? $ret : array();
	}

	/** 检查是否开启润衡 */
	public function check_runheng($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id
			);
		$ret = g('pkg_db')->select(self::$Table_Rh_Acct, $fields, $cond);
		return $ret ? $ret[0] : false;
	}

}

//end