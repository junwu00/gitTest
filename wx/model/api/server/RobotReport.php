<?php
/**
 * Created by PhpStorm.
 * User: chenjunwu
 * Date: 2021/12/1
 * Time: 17:04
 */
namespace model\api\server;

class RobotReport{
    const REPORT_URL="https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=94f22697-8acd-4c47-acf1-f95b00ddab12";

    public static function reportMessage($str){
        try{
            $curl = new \package\net\http\CurlClient();
            $data = $curl->post(self::REPORT_URL,json_encode([
                "msgtype"=>"text",
                "text"=>[
                    "content"=>$str,
                ]
            ]));
            to_log('Info','','机器人汇报结果:'.json_encode($data));
        }catch (\Exception $e){
            to_log('Info','','机器人汇报异常:'.$e->getMessage());
        }
    }


}