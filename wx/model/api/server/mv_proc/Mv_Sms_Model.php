<?php
/**
 * 表单短信模板信息
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */
namespace model\api\server\mv_proc;

class Mv_Sms_Model extends \model\api\server\ServerBase{
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_form_sms_model';

    const TypeApproved = 1;
    const TypeRevocationAfterApproved = 2;
    const TypeRejectAfterApproved = 3;

    //短信通知对象
    const NotifyRuleFixedReceiver = 1;
    const NotifyRuleFormInput = 2;
    const NotifyRuleFormCreater = 5;

    //控件拆分标识
    const InputSplitPeopleName = 'people-name';
    const InputSplitPeoplePhone = 'people-phone';
    const InputSplitCarName = 'car-name';
    const InputSplitCarDriverName = 'car-driver-name';
    const InputSplitCarDriverPhone = 'car-driver-phone';
    const InputSplitCarSeatNum = 'car-seat-num';

    /**
     * 获取表单模板对应的短信模板
     * @param $com_id
     * @param $form_id
     */
	public function get_by_form_id($com_id,$form_id,$type='',$fields='*'){
        $cond = array(
            'form_id='=>$form_id,
            //'com_id='=>$com_id
        );
        if($type){
            $cond['type=']=$type;
        }
        $ret = g('pkg_db') -> select(self::$Table, $fields,$cond);
        return $ret?$ret:[];
    }
	
}

// end of file