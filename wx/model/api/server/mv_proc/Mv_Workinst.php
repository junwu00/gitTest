<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
namespace model\api\server\mv_proc;

class Mv_Workinst extends \model\api\server\ServerBase{
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_workitem';
    private static $TableFormsetinstNode = 'mv_proc_formsetinst_node';
    private static $TableFormsetinst = 'mv_proc_formsetinst';
    private static $TableWorkNode = 'mv_proc_work_node';
    private static $TableUser = 'sc_user';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;

    /** 已退回 1 */
    private static $CallBackYes = 1;
    /** 未退回 0 */
    private static $CallBackNo = 0;
	
	/**
	 * 人员状态
	 */
	private static $UserDel = 0;//删除用户
	
	private static $UserFollow  = 2;//关注用户
	
	private static $UserFloze = 3;//冻结用户
	
	private static $UserNotFollow  = 4;//未关注
	
	/**
	 * 人员选择范围
	 */
	private static $RecriverFixed = 1;//固定接收人
	
	private static $RecriverCreater = 2;//流程发起人
	
	private static $RecriverCreaterLeader = 3;//流程发起人领导
	
	private static $RecriverCreaterDept = 4;//流程发起人部门
	
	private static $RecriverHandler = 5;//当前处理人
	
	private static $RecriverHandlerLeader = 6;//当前处理人上级领导
	
	private static $RecriverHandlerDept = 7;//当前处理人部门
	
	private static $RecriverInput = 8;//表单具体数据
	
	/**
	 * 比较符号
	 */
	
	private static $Compare_Greater = 1;//大于
	
	private static $Compare_Lesser = 2;//小于
	
	private static $Compare_Equeal = 3;//等于
	
	private static $Compare_Lesser_Equeal = 4;//小于等于
	
	private static $Compare_Greater_Equeal = 5;//大于等于
	
	private static $Compare_Not_Equeal = 6;//不等于
	
	//人员规则
	private static $People_Position = 7;//人员职位
	
	private static $People_Type_Creater =2;//发起人
	
	private static $People_Type_Done =1;//当前处理人
	
	private static $Compare_Contain = 8;//包含
	
	private static $Compare_Not_Contain = 9;//不包含
	
	/**
	 * 工作项状态
	 */
	/** 新收到1 */
	private static $StateNew = 1;
	/** 已阅读 2 */
	private static $StateRead = 2;
	/** 已保存 3 */
	private static $StateSave = 3;
	/** 等待中 4 */
	private static $StateWait = 4;
	/** 回收 5 */
	private static $StateRecovery = 5;
	/** 终止 6 */
	private static $StateStop = 6;
	/** 已处理 7 */
	private static $StateSend = 7;
	/** 已退回 8 */
	private static $StateCallback = 8;
	/** 已通过 9 */
	private static $StateDone = 9;
	/** 退回 10 */
	private static $StateSendback = 10;
    /** 转审 11 */
    protected static $StateTurn = 11;
    /** 已撤销 12 */
    protected static $StateRevoke = 12;
    /** 撤销中 13 */
    protected static $StateRevoking = 13;

    /** 流程监控工作项状态集 */
    public static $StateWorkitem = [1,2,3,4,5,6,7,8,9,12,13,14];
    /** 流程监控待办工作项状态集 */
    public static $StateWorkingWorkitem = [1,2,3];
    /** 流程监控已办工作项状态集 */
    public static $StateWorkedWorkitem = [7,8,9,6];
    /** 流程监控处理工作项状态集 */
    public static $StateDealedWorkitem = [7,8];
	
	/** 是否为开始工作项  1 是 */
	private static $StartWItem = 1;
	/** 是否为开始工作项  0 是 */
	private static $NotStartWItem = 0;
	
	/** 表单实例状态 */
	/** 草稿 */
	private static $StateDraft = 0;
	/** 运行中 */
	private static $StateRun = 1;
	/** 已结束 */
	private static $StateOver = 2;
	/** 被终止 */
	private static $StateInterrupt = 3;

	/** 应用版本1.0 */
	private static $Version_v1 = 0;
	/** 应用版本2.0 */
	private static $Version_v2 = 1;

    public function getFirstWorkitemId($formsetinit_id)
    {
        $cond = array(
            'formsetinit_id='=>$formsetinit_id,
        );
        $order = "receive_time ASC";
        $ret = g('pkg_db') -> select(self::$Table, "id", $cond, 1, 1, '', $order);
        return $ret?$ret[0]['id']:0;
    }

    public function getLastWorkitemId($formsetinit_id,$user_id)
    {
        $cond = array(
            'formsetinit_id='=>$formsetinit_id,
            'handler_id='=>$user_id,
        );
        $order = "receive_time desc";
        $ret = g('pkg_db') -> select(self::$Table, "id", $cond, 1, 1, '', $order);
        return $ret?$ret[0]['id']:0;
    }
    /**
     * 终止流程
     * @param $workitem_id
     * @param $com_id
     * @param $work_item
     * @return mixed
     * @throws \Exception
     */
    public function updateWorkitem($workitem_id,$com_id,$work_item){
        $cond = array(
            'id=' => $workitem_id,
            //'com_id=' => $com_id,
        );

        $data = array(
            'complete_time' => $work_item["complete_time"],
            'state' => self::$StateStop,
            'consuming_time' => $work_item["consuming_time"],
            'target_node' => $work_item["target_node"],
            'judgement' => $work_item["judgement"],
            'judgement_desc' => isset($work_item["judgement_desc"])?$work_item["judgement_desc"]:'',
        );
        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);
        if (!$ret) throw new \Exception('更新工作项失败');
        return $ret;
    }
	/**
	 * 保存工作项new
	 * @param array $work_item 工作项数组
	 */
	public function save_workitem($work_item) {
		$data = array(
			'formsetinit_id' 	=> $work_item["formsetinit_id"],
			'workitem_name' 	=> $work_item["workitem_name"],
			'com_id' 			=> $work_item["com_id"],
			'handler_id' 		=> $work_item["handler_id"],
			'handler_name' 		=> $work_item["handler_name"],
			'handler_dept_id' 	=>$work_item["handler_dept_id"],
			'handler_dept_name' =>$work_item["handler_dept_name"],
			'receive_time' 		=> time(),
			'receiver_id' 		=> empty($work_item["receiver_id"])?NULL:$work_item["receiver_id"],
			'receiver_name' 	=> empty($work_item["receiver_name"])?NULL:$work_item["receiver_name"],
			'receiver_dept_id' 	=>empty($work_item["receiver_dept_id"])?NULL:$work_item["receiver_dept_id"],
			'receiver_dept_name' =>empty($work_item["receiver_dept_name"])?NULL:$work_item["receiver_dept_name"],
			'state' 			=> $work_item["state"],
			'creater_id' 		=> $work_item["creater_id"],
			'creater_name' 		=> $work_item["creater_name"],
			'creater_dept_id' 	=>$work_item["creater_dept_id"],
			'creater_dept_name' =>$work_item["creater_dept_name"],
			'work_node_id' 		=>$work_item["work_node_id"],
			'pre_work_node_id'	=>empty($work_item["pre_work_node_id"])?NULL:$work_item["pre_work_node_id"],
			'is_returnback' 	=>$work_item["is_returnback"],
			'returnback_node_id' =>$work_item["returnback_node_id"],
			'info_state' 		=>self::$StateOn,
            'complete_time' 	=> isset($work_item["complete_time"])?$work_item['complete_time']:null,
            'consuming_time' 	=> isset($work_item["consuming_time"])?$work_item['consuming_time']:null,
            'judgement' 	    => isset($work_item["judgement"])?$work_item['judgement']:null,
            'judgement_desc' 	=> isset($work_item["judgement_desc"])?$work_item['judgement_desc']:null,
		);
		$ret = g('pkg_db') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new \Exception('保存表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 
	 * 更新工作项内容new
	 * @param int $workitem_id
	 * @param int $com_id
	 * @param string $judgement
	 * @throws SCException
	 */
	public function update_workitem($workitem_id,$com_id,$judgement,$is_linksign=0){
		$cond = array(
			'id=' => $workitem_id,
			//'com_id=' => $com_id,
		);

		$data = array(
			'state' => self::$StateSave,
			'is_linksign' => $is_linksign
		);
		if(!empty($judgement) && $judgement !== 0 ){
			$data['judgement'] = $judgement;
		}
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) throw new \Exception('更新工作项失败');
		
		return $ret;
	}
	
	/**
	 * 更新事项的状态new
	 * @param int $work_node_id
	 * @param int $formsetinst_id
	 * @param int $com_id
	 * @param int $state 原来状态
	 * @param int $change_state 修改状态
	 */
	public function update_state_workitem($com_id,$formsetinst_id,$work_node_id,$state,$change_state){
		$cond = array(
			'work_node_id=' => $work_node_id,
			'com_id=' => $com_id,
			'state=' =>$state,
			'formsetinit_id=' =>$formsetinst_id
		);

		$data = array(
			'state' => $change_state,
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) throw new \Exception('更新工作项状态失败');
		
		return $ret;
	}

    /**
     * 根据工作项ids获取工作项和节点信息不带权限验证
     *
     */
    public function get_by_itemids($formsetinst_id, $workitem_ids, $com_id, $is_node=TRUE, $field=' mpw.* '){
        $sql = "SELECT {$field} FROM ";
        if($is_node){
            $sql.=" mv_proc_workitem mpw,mv_proc_work_node mpwn WHERE mpwn.id = mpw.work_node_id AND ";
        }
        else{
            $sql.=" mv_proc_workitem mpw WHERE ";
        }
        $sql.="mpw.info_state=".self::$StateOn." 
			AND mpw.formsetinit_id = ".intval($formsetinst_id)." 
			AND mpw.id IN( ".implode(",",$workitem_ids).") 
			AND mpw.com_id = ".$com_id;

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret : array();
    }

    /**
     * 根据工作项id获取工作项和节点信息不带权限验证
     * @param int $workitem_id
     * @param int $com_id
     * @param int $user_id
     * @param boolean $is_node 是否需要节点信息
     * @param string $field
     */
    public function get_by_itemid($formsetinst_id, $workitem_id, $com_id, $is_node=TRUE, $field='mpw.*'){
        $sql = "SELECT {$field} FROM ";
        if($is_node){
            $sql.=" mv_proc_workitem mpw,mv_proc_work_node mpwn WHERE mpwn.id = mpw.work_node_id AND ";
        }
        else{
            $sql.=" mv_proc_workitem mpw WHERE ";
        }
        $sql.="mpw.info_state=".self::$StateOn." 
			AND mpw.formsetinit_id = ".intval($formsetinst_id)." 
			AND mpw.id = ".intval($workitem_id);
            // ." AND mpw.com_id = ".$com_id;

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret[0] : FALSE;
    }

    /**
     * 根据formsetinst_id查找工作项
     * @param int $formsetinst_id
     * @param int $com_id
     * @param string $fields
     */
    public function get_item_by_formsetinst_id($formsetinst_id,$com_id,$fields='*'){
        $table = self::$Table;
        $sql = "SELECT {$fields} 
				FROM {$table} 
				WHERE formsetinit_id = {$formsetinst_id} 
					AND info_state=".self::$StateOn;

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret : FALSE;
    }

    /**
     * 根据节点ids查找工作项
     * @param int $formsetinst_id
     * @param array $work_node_ids
     * @param int $com_id
     * @param array $conds 其它条件
     * @param string $fields
     */
    public function get_item_by_fwc($formsetinst_id, $work_node_ids, $com_id, $conds, $fields='*', $order=''){
        if(empty($work_node_ids)) return array();

        $table = self::$Table;
        $sql = "SELECT {$fields} 
				FROM {$table} 
				WHERE formsetinit_id={$formsetinst_id} 
					AND work_node_id IN(".implode(',', $work_node_ids).") 
					AND info_state=".self::$StateOn;

        if(!empty($conds)){
            foreach ($conds as $key => $cond){
                if (is_numeric($cond)) {
                    $sql.= " AND ".$key.intval($cond);
                }
                else {
                    $sql.= " AND ".$key.$cond;
                }

            }unset($cond);
        }

        empty($order) && $order = " ORDER BY id DESC ";
        $sql .= $order;

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret : array();
    }


    /**
	 * 
	 * 发送流程
	 * @param unknown_type $workitemid
	 * @param unknown_type $com_id
	 * @param unknown_type $work_item
	 * @throws SCException
	 */
	public function send_workitem($workitemid,$com_id,$work_item){
		$cond = array(
			'id=' => $workitemid,
			//'com_id=' => $com_id,
		);

		$data = array(
			'complete_time' => $work_item["complete_time"],
			'state' => self::$StateSend,
			'consuming_time' => $work_item["consuming_time"],
			'receiver_info' => $work_item["receiver_info"],
			'target_node' => $work_item["target_node"]
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 退回工作项new
	 */
	public function callback_workitem($workitem_id,$com_id,$work_item){
		$cond = array(
			'id=' => $workitem_id,
			//'com_id=' => $com_id,
		);

		$data = array(
			'complete_time' => $work_item["complete_time"],
			'state' => self::$StateCallback,
			'consuming_time' => $work_item["consuming_time"],
			'receiver_info' => $work_item["receiver_info"],
			'target_node' => $work_item["target_node"],
			'judgement' => $work_item["judgement"],
            'judgement_desc' => isset($work_item["judgement_desc"])?$work_item["judgement_desc"]:'',
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('更新工作项失败');
		}
		return $ret;
	}
	
	
	/**
	 * 
	 * 根据流程实例id删除工作项
	 * @param unknown_type $workitemid
	 * @param unknown_type $com_id
	 * @param unknown_type $work_item
	 * @throws SCException
	 */
	public function delete_workitem($formsetinst_id,$com_id,$user_id){
		$cond = array(
			'formsetinit_id=' => $formsetinst_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_user_id' => $user_id
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('删除工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 根据工作项id更新工作项的状态new
	 * @param int $workitem_id
	 * @param int $com_id
	 * @param int $state
	 * @throws SCException
	 */
	public function update_workitem_state($workitem_id,$com_id,$state){
		$cond = array(
			'id=' => $workitem_id,
			//'com_id=' => $com_id,
		);
		$data = array(
			'state' => $state,
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('更新工作项状态失败');
		}
		return $ret;
	}
	
	
	/**
	 * 更新前一工作项id new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param string $pre_work_node_id 前工作项id集合
	 * @param int $com_id
	 */
	public function update_same_handlers($formsetinst_id,$work_node_id,$pre_work_node_id,$com_id){
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn,
			'formsetinit_id=' =>$formsetinst_id,
			'work_node_id=' =>$work_node_id
		);

		$data = array(
			'pre_work_node_id' => $pre_work_node_id,
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 更新退回状态的工作项
	 */
	public function update_sendback_workitem($formsetinit_id,$returnback_node_id,$com_id){
		
		$update = "update ".self::$Table." set state=temp_state".
		" where  returnback_node_id=".$returnback_node_id." and info_state=".self::$StateOn.
		" and state=".self::$StateSendback."  and formsetinit_id = ".intval($formsetinit_id);
		
		$ret = g('pkg_db') -> exec($update);
		if (!$ret) {
			throw new \Exception('更新工作项状态失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 更新当前同一个节点的工作项的状态new
	 * @param int $workitem_id
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $state
	 * @param int $returnback_node_id 退回工作项id
	 */
	public function update_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,$state,$select_state,$com_id,$returnback_node_id=0){
		$set_str="";
		if($returnback_node_id!=0){
			$set_str .="returnback_node_id=".intval($returnback_node_id).",temp_state=state,";
		}
		$set_str .= "state=".$state;
		$update = "update ".self::$Table." set ".$set_str.
		" where id!=".intval($workitem_id).
		" and formsetinit_id=".intval($formsetinst_id).
		" and work_node_id=".intval($work_node_id).
		" and info_state=".self::$StateOn.
		" and state in(".implode(",",$select_state).")";
		
		$ret = g('pkg_db') -> exec($update);
		if (!$ret) {
			throw new \Exception('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 更新等待中工作项的状态new
	 * @param unknown_type $workitem_id
	 * @param unknown_type $formsetInst_id
	 * @param unknown_type $work_id
	 */
	public function update_formset_state_item($formsetinst_id,$state,$select_state,$com_id,$returnback_node_id=0){
		$set_str="";
		if($returnback_node_id!=0){
			$set_str .="returnback_node_id=".intval($returnback_node_id).",temp_state=state,";
		}
		$set_str .= "state=".$state;
		$update = "update ".self::$Table." set ".$set_str.
		" where formsetinit_id=".intval($formsetinst_id).
		" and state in(".implode(",",$select_state).")".
		" and info_state=".self::$StateOn;
		
		$ret = g('pkg_db') -> exec($update);
		if (!$ret) {
			throw new \Exception('更新工作项失败');
		}
		return $ret;
	}

    /**
     * 根据节点信息和处理人信息更新退回状态
     * @param int $formsetinst_id 实例id
     * @param int $work_node_id 节点id
     * @param int $handler_id 处理人id
     * @param int $com_id 企业id
     */
    public function update_cs_by_i($formsetinst_id, $workitem_id, $com_id){
        $table = self::$Table;
        $set_str = " is_callback = ".self::$CallBackYes;
        $update = "UPDATE {$table} 
					SET {$set_str} 
					WHERE formsetinit_id = {$formsetinst_id} 
					AND id = {$workitem_id} 
					AND info_state = ".self::$StateOn." 
					AND is_callback = ".self::$CallBackNo;

        $ret = g('pkg_db') -> exec($update);
        if (!$ret) throw new \Exception('更新工作项失败');
        return $ret;
    }
	
	/**
	 * 根据工作项id获取表单信息，流程信息new
	 * @param int $workitem_id 工作项id
	 * @param int $com_id
	 * @param int $user_id
	 * @param string $field
	 */
	 
	public function get_curr_workitem($workitem_id,$com_id,$user_id,$field=''){
		if (empty($field)) {
			// gch add mpwn.notify_input
			// gch edit trigger form add 'mpf.form_id'
			$field = array(
				'mpw.*', 'su.pic_url', 'mpfm.form_describe', 'mpfm.inputs', 'mpwn.is_start', 'mpf.form_id',
				'mpwn.is_auto_notify', 'mpwn.notify_rule', 'mpwn.notify_user', 'mpwn.notify_group', 'mpwn.notify_input', 
				'mpf.work_id', 'mpwn.input_visble_rule', 'mpwn.input_visit_rule', 'mpwn.node_type', 
				'mpwn.circulation_limit_rule', 'mpwn.work_node_describe', 'mpwn.file_type', 
				'mpwn.work_type','mpf.state formsetinst_state'
				);
			$field = implode(',', $field);
		}
//		$sql = "select ".$field."
//		from mv_proc_workitem mpw,mv_proc_formsetinst mpf,mv_proc_form_model mpfm,mv_proc_work_node mpwn,sc_user su
//		where mpfm.id = mpf.form_id and mpw.formsetinit_id = mpf.id and mpwn.id = mpw.work_node_id and su.id = mpw.creater_id
//		and mpw.id = ".intval($workitem_id)." and mpw.com_id = ".$com_id." and mpw.handler_id = ".$user_id;

        $relation_power_sql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');
        $sql = "SELECT {$field}  
				FROM mv_proc_workitem mpw
				left join mv_proc_formsetinst mpf on mpf.id=mpw.formsetinit_id
				left join mv_proc_form_model mpfm on mpfm.id=mpf.form_id
				left join mv_proc_work_node mpwn on mpwn.id=mpw.work_node_id
				left join sc_user su on su.id = mpw.creater_id
				WHERE mpfm.id = mpf.form_id 
					AND mpw.formsetinit_id = mpf.id 
					AND mpwn.id = mpw.work_node_id 
					AND su.id = mpw.creater_id 
					AND ( mpw.com_id={$com_id} or {$relation_power_sql} )
					AND mpw.id = {$workitem_id}  ";

        if($user_id){
            $sql .= " AND (mpw.handler_id={$user_id}  ) ";
        }

		$ret = g('pkg_db') -> query($sql);
		
		return $ret ? $ret[0] : FALSE;
	}
	
/**
	 * 根据节点id获取对应的工作项new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $com_id
	 * @param string $field
	 */
	public function get_workitem_by_nodeid($formsetinst_id,$work_node_id,$com_id,$user_id,$field='mpw.*,su.pic_url,mpwn.is_start,mpwn.is_auto_notify,mpwn.notify_rule,mpwn.input_visble_rule,mpwn.input_visit_rule,mpwn.node_type,mpwn.circulation_limit_rule,mpwn.work_node_describe,mpwn.file_type,mpwn.work_type '){
		$sql = "select ".$field.
		" from mv_proc_workitem mpw,mv_proc_work_node mpwn,sc_user su 
		where mpw.work_node_id = mpwn.id and su.id = mpw.handler_id 
		and mpw.formsetinit_id=".$formsetinst_id.
		" and mpw.work_node_id = ".$work_node_id.
		" and mpw.com_id = ".$com_id.
		" and mpw.handler_id = ".$user_id;

		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 根据工作项ids获取表单信息，流程信息new
	 * 
	 */
	public function get_item_by_ids($workitemids,$com_id){

        $relationPowerSql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');

		$sql = "select mpw.*,su.pic_url,mpwn.is_start 
		from mv_proc_workitem mpw,mv_proc_formsetinst mpf,mv_proc_form_model mpfm,mv_proc_work_node mpwn,sc_user su 
		where mpfm.id = mpf.form_id and mpw.formsetinit_id = mpf.id and mpwn.id = mpw.work_node_id and su.id = mpw.creater_id 
		and mpw.info_state=".self::$StateOn." and (mpw.com_id = ".$com_id ." or {$relationPowerSql} )";
		
		if(count($workitemids)==0){
			return FALSE;
		}
		$sql .= " and mpw.id in(".implode(",",$workitemids).")";
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 获取当前节点指定状态的工作项（除去指定的事项id）new
	 * @param int $workitem_id 工作项id
	 * @param int $formsetinst_id 实例id
	 * @param int $work_node_id 节点id
	 */
	public function get_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,$state,$com_id,$fields='*'){
		$sql = "select ".$fields." from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		// " and com_id = ".$com_id.
		" and work_node_id = ".$work_node_id.
		" and id !=".$workitem_id.
		" and info_state=".self::$StateOn .
		" and state in(".implode(",",$state).")";
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取当前表单实例指定状态的工作项（除去指定的事项id）new
	 * @param int $workitem_id 工作项id
	 * @param int $formsetinst_id 实例id
	 * @param int $com_id
	 */
	public function get_formset_state_item_nwi($formsetinst_id,$workitem_id,$state,$com_id,$fields='*'){
		$sql = "select ".$fields." from ".self::$Table." 
		where formsetinit_id = ".intval($formsetinst_id).
		// " and com_id = ".$com_id.
		" and info_state=".self::$StateOn.
		" and id !=".intval($workitem_id)." 
		and state in(".implode(",",$state).")";
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 判断同一节点是否有同一个处理人new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $handler_id 处理人id
	 * @param int $com_id
	 */
	public function is_same_handlers($formsetinst_id,$work_node_id,$handler_id,$com_id){
		$sql = "select * from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		" and work_node_id = ".$work_node_id.
		" and handler_id = ".$handler_id.
		" and info_state=".self::$StateOn .
		" and ( state =".self::$StateNew.
		" or state =".self::$StateRead.
		" or state =".self::$StateSave.
		" or state =".self::$StateWait.")";
		
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 *  根据节点id获取节点某个状态的事项new
	 *  @param int $com_id
	 *  @param int $formsetinst_id
	 *  @param int $work_node_id 当前节点id
	 *  @param array $state 需要获取的状态
	 */
	public function get_node_state_item($formsetinst_id,$work_node_id,$state,$com_id,$field='*'){
		$sql = "select ".$field." from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		// " and com_id = ".$com_id.
		" and work_node_id=".$work_node_id.
		" and info_state = ".self::$StateOn.
		" and state in(".implode(",",$state).")";
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 获取当前表单实例指定状态的工作项
	 * @param int $formsetinst_id
	 * @param array $state
	 * @param int $com_id
	 */
	public function get_formset_state_workitem($formsetinst_id,$state,$com_id,$field='*'){
		$sql = "select ".$field." from ".self::$Table." 
		where formsetinit_id=".intval($formsetinst_id).
		// " and com_id = ".$com_id.
		" and info_state=".self::$StateOn;
		if(!empty($state)){
			$sql .= " and state in(".implode(",",$state).")";
		}
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取流程监控数据new
	 */
	public function get_workitem_list($formsetinst_id,$com_id,$page=0,$page_size=0,$fields='mpw.*,su.pic_url,mpwn.work_type',$order_by=" order by receive_time, complete_time is null,complete_time ",$with_count = FALSE){
		$state_str = " and mpw.state in(".implode(',',self::$StateWorkitem).")";

        $relationPowerSql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');
		$sql = "select ".$fields." from ".self::$Table." mpw, sc_user su,mv_proc_work_node mpwn,mv_proc_form_model mpfm 
		where mpw.handler_id = su.id and mpw.work_node_id = mpwn.id and mpfm.work_id=mpwn.work_id
		and (mpwn.com_id=".$com_id." or ".$relationPowerSql." )
		and (mpw.com_id = ".$com_id." or ".$relationPowerSql." )
		and mpw.info_state =".self::$StateOn." and mpw.formsetinit_id =".$formsetinst_id.$state_str;
		
		
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}

		$ret = g('pkg_db') -> query($limit_sql);
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('pkg_db')->query($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count[0]['count'] : 0,
			);
		}
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取待办已办列表
	 */
	public function get_do_list($condition = Array(),$com_id,$user_id,$is_finish=0,$is_proc_type=NULL,$page=0,$page_size=0,$fields='mpf.formsetinst_name,mpf.create_time,mpfm.form_name,mpf.state formsetinst_state,mpw.*,su.pic_url',$order_by=" order by receive_time desc ",$with_count = FALSE){
        $relation_power_sql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');

		$sql = "select ".$fields." from mv_proc_form_model mpfm,mv_proc_formsetinst mpf,mv_proc_workitem mpw,sc_user su , mv_proc_work_node mpwn 
		where mpfm.id = mpf.form_id and mpf.id = mpw.formsetinit_id and su.id = mpw.creater_id and mpwn.id = mpw.work_node_id 
		and mpw.handler_id = ".$user_id." 
		and (mpf.com_id = ".$com_id." or ".$relation_power_sql." ) 
		and mpw.info_state =".self::$StateOn." and mpf.info_state =".self::$StateOn." and mpf.state!=0 ";
		
		if (!is_null($is_proc_type)) {
			if (is_numeric($is_proc_type)) {
				$is_proc_type = intval($is_proc_type);
				$sql .= " AND mpf.is_other_proc={$is_proc_type}";
			} elseif (is_array($is_proc_type) && !empty($is_proc_type)) {
				$sql .= " AND mpf.is_other_proc IN (". implode(',', $is_proc_type).")";
			}
		}

		if($is_finish==0){//待办
			$state = " and (mpw.state in (".implode(',',self::$StateWorkingWorkitem)."))
			 and mpf.state =".Mv_Formsetinst::$StateDone;
		}
		else{//已办
			$state = " and (mpw.state in (".implode(',',self::$StateWorkedWorkitem).")) 
			and mpwn.is_start =0 ";
		}
		$sql = $sql.$state;
		foreach ($condition as $key => $val) {
			if($key=="formsetinst_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.formsetinst_name like '%".mysql_escape_string($val)."%'";
				continue;
			}
			if($key=="creater_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.creater_name like '%".mysql_escape_string($val)."%'";
				continue;
			}
			
			if($key=="s_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time>=".mysql_escape_string($val);
				continue;
			}
			if($key=="e_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time<".mysql_escape_string($val);
				continue;
			}
			if($key=="s_receive_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.receive_time>=".mysql_escape_string($val);
				continue;
			}
			if($key=="e_receive_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.receive_time<".mysql_escape_string($val);
				continue;
			}
			if($key=="state"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.state=".mysql_escape_string($val);
				continue;
			}
			if($key=="formsetinst_state"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.state=".mysql_escape_string($val);
				continue;
			}
			if($key == "app_version" && $val !== ""){
				$sql = $sql." and mpf.app_version=" . $val;
				continue;
			}
		}
		unset($val);
		
		
		if($is_finish==1){
			$sql = "SELECT * from (".$sql." order by mpw.formsetinit_id,mpw.id desc) a  GROUP BY a.formsetinit_id ";
		}
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}

		$ret = g('pkg_db') -> query($limit_sql);
		
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('pkg_db') -> query($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count[0]['count'] : 0,
			);
		}
		return $ret ? $ret : FALSE;
	}

//----------------------------主页型应用接口新增-----------------------------------------------------

	/**
	 * 获取待办流程总数
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @return [type]          
	 */
	public function get_handler_proc_sum($com_id, $user_id, $is_other_proc=array()) {

        $relationPowerSql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');

		$stateOn = self::$StateOn;
		$stateDraft = self::$StateDraft;
		$stateNew = self::$StateNew;
		$stateRead = self::$StateRead;
		$stateSave = self::$StateSave;
		$app_version = self::$Version_v2;
		$sql = " SELECT COUNT(0) count 
			FROM mv_proc_form_model mpfm, mv_proc_formsetinst mpf, mv_proc_workitem mpw, sc_user su 
			WHERE mpfm.id=mpf.form_id 
				AND mpf.id=mpw.formsetinit_id 
				AND su.id=mpw.creater_id 
				AND mpw.handler_id={$user_id} 
				AND mpw.info_state={$stateOn} 
				AND (mpf.com_id={$com_id} or {$relationPowerSql}) 
				AND mpf.info_state={$stateOn} 
				AND mpf.state!={$stateDraft} 
				AND mpf.app_version={$app_version} 
				AND mpf.is_other_proc IN(".implode(',', $is_other_proc) .") 
				AND mpw.state in (".implode(',',[self::$StateNew,self::$StateRead,self::$StateSave]).") 
				AND mpf.state not in (".implode(',',[Mv_Formsetinst::$StateRevoke,Mv_Formsetinst::$StateFinishRevoke,Mv_Formsetinst::$StateStop]).")";

		$count = g('pkg_db') -> query($sql);

		$count = $count ? $count[0]['count'] : 0;
		return $count;
	}


	//---------------------------------------考勤管理接口-------------------------------------------
	/**
	 * 通过实例ID和user_id获取工作项信息
	 * @param   $formsetinit_id
	 * @param   $user_id
	 * @return 
	 */
	public function get_workitem_by_user_id($formsetinit_id, $user_id, $fields = "*"){
		$cond = array(
				'formsetinit_id=' => $formsetinit_id,
				'handler_id=' => $user_id,
				'info_state=' => self::$StateOn
			);
		$order = " order by id desc ";
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond, 1, 1, '', $order);
		return $ret ? $ret[0] : array();
	}

    /**
     * 根据节点id获取对应的工作项,获取开始节点详细信息用,带权限验证
     * @param int $formsetinst_id
     * @param int $work_node_id
     * @param int $com_id
     * @param string $field
     */
    public function get_node_workitem($formsetinst_id, $work_node_id, $com_id, $user_id='', $field=''){
        if (empty($field)) {
            $field = array(
                'mpw.*', 'su.pic_url', 'su.acct', 'mpwn.is_start', 'mpwn.is_auto_notify', 'mpwn.notify_rule',
                'mpwn.input_visble_rule', 'mpwn.input_visit_rule', 'mpwn.node_type', 'mpwn.circulation_limit_rule',
                'mpwn.work_node_describe', 'mpwn.file_type', 'mpwn.work_type'
            );
            $field = implode(',', $field);
        }
        $order = " ORDER BY mpw.id DESC";

        $relationPowerSql = $this->getRelationFromWhereSql($com_id,'mpfm.form_history_id');
        $sql = "SELECT {$field}
				FROM mv_proc_workitem mpw
				left join mv_proc_work_node mpwn on mpwn.id=mpw.work_node_id  
				left join mv_proc_form_model mpfm on mpfm.work_id = mpwn.work_id
				left join sc_user su on  su.state != 0 and (su.com_id = {$com_id} or {$relationPowerSql} )  and su.id = mpw.handler_id
				WHERE  mpw.formsetinit_id={$formsetinst_id} 
					AND mpw.work_node_id={$work_node_id} 
					AND (mpw.com_id= {$com_id} or {$relationPowerSql})";

        if (!empty($user_id)) {
            is_numeric($user_id) && $sql .= " AND mpw.handler_id={$user_id} ";
            is_array($user_id) && $sql .= " AND mpw.handler_id IN(".implode(',', $user_id).") ";
        }
        $sql .= $order;

        $ret = g('pkg_db') -> query($sql);
        return $ret ? $ret : array();
    }

    /**
     *
     * 根据work_node_id,state更新工作项状态
     * @param int $workitem_id
     * @param int $formsetinst_id
     * @param int $work_node_id
     * @param int $state
     * @param int $is_returnback 退回工作项id
     */
    public function update_ws_by_ns($formsetinst_id,$work_node_id,$change_state,$select_state,$com_id,$workitem_id=FALSE,$is_returnback=FALSE){
        $set_str="";
        if(!empty($is_returnback)){
            $set_str .="returnback_node_id=".intval($workitem_id).",temp_state=state,";
        }
        $set_str .= "state=".$change_state;
        $update = "UPDATE ".self::$Table." 
					SET {$set_str}  
					WHERE formsetinit_id=".intval($formsetinst_id)." 
					AND com_id=".intval($com_id)." 
					AND info_state=".self::$StateOn." 
					AND state in(".implode(",",$select_state).")";

        if(is_array($work_node_id)){
            $update .=" AND work_node_id in(".implode(",",$work_node_id).")";
        }
        else {
            $update .=" AND work_node_id = ".intval($work_node_id);
        }

        if(!empty($workitem_id)){//除去指定工作项
            $update .=" AND id!=".intval($workitem_id);
        }

        $ret = g('pkg_db') -> exec($update);
        if (!$ret) throw new \Exception('更新工作项失败');
        return $ret;
    }

    /**
     * 获取工作项
     * @param int $id
     * @return
     */
    public function get_by_id($id,$fields="*") {
        $cond = array(
            'id='=>$id,
        );
        $ret = g('pkg_db') -> select_one(self::$Table, $fields,$cond);
        return $ret?$ret:[];
    }
    /** 检验工作项是否为撤销流程工作项 */
    public function check_revoke_workitem($workitem_id)
    {
        // 获取工作项
        $workitem = $this->get_by_id($workitem_id);

        $node_id = $workitem['work_node_id'];
        $formsetinit_id = $workitem['formsetinit_id'];

        // 获取表单实例信息
        $formsetinit = g('pkg_db') -> select_one(self::$TableFormsetinst, "relate_formsetinst_id",['id='=>$formsetinit_id]);
        $relate_formsetinst_id = $formsetinit['relate_formsetinst_id'];
        if (!$relate_formsetinst_id) {
            return [false,[]];
        }
        // 获取节点信息是否已存在
        $node_info = g('pkg_db') -> select_one(self::$TableFormsetinstNode, "next_node",[
            "formsetinst_id="=>$relate_formsetinst_id,
            "node_id="=>$node_id
        ]);
        if (!$node_info) {
            return [false,[]];
        }
        $next_node = json_decode($node_info['next_node'],true);
        $next_node_id = $next_node['id'];
        $reciver_arr = $next_node['receiver_arr'];
        $receive_array = [];
        foreach ($reciver_arr as $reciver){
            $user = g('pkg_db')-> select_one(self::$TableUser, "id,name,pic_url",[
                "id="=>$reciver['receiver_id']
            ]);
            $receive_array[] = $user;
        }
        // 获取下一节点信息
        $next_node_info = g('pkg_db')-> select_one(self::$TableWorkNode, "next_node_id,work_node_name",[
            "id="=>$next_node_id
        ]);
        $node_list = [
            "id" =>  $next_node_id,
            "is_end" => !$next_node_info['next_node_id']?true:false,
            "receive_array" => $receive_array,
            "select_array" => [],
            "work_node_name" => $next_node_info['work_node_name']
        ];
        return [true,[
            'node_list'=>$node_list,// 前端返回用
            'node'=>$next_node,// 后端接口调用
        ]];
    }

    /** 保存表单实例节点信息 */
    public function save_node($workitem_id,$node)
    {
        // 获取工作项
        $workitem = $this->get_by_id($workitem_id);

        $node = json_encode($node);
        $com_id = $workitem['com_id'];
        $node_id = $workitem['work_node_id'];
        $formsetinit_id = $workitem['formsetinit_id'];
        // 获取当前节点信息是否已存在
        $node_info = g('pkg_db') -> select_one(self::$TableFormsetinstNode, "id",[
            "formsetinst_id="=>$formsetinit_id,
            "node_id="=>$node_id
        ]);
        if ($node_info) {
            $data = [
                'update_time'=> time(),
                'next_node'=>$node,
            ];
            $ret = g('pkg_db') -> update(self::$TableFormsetinstNode, ['id='=>$node_info['id']], $data);
        } else {
            $data = [
                'info_state'=>1,
                'create_time'=> time(),
                'update_time'=> time(),
                'com_id'=>$com_id,
                'formsetinst_id'=>$formsetinit_id,
                'node_id'=>$node_id,
                'next_node'=>$node,
            ];
            $ret = g('pkg_db') -> insert(self::$TableFormsetinstNode, $data);
        }
        return $ret;
    }
    /** 查找待办审批工作项 */
    public function getWorkingWorkitem($formsetinst_id,$fields="*")
    {
        $cond = array(
            'formsetinit_id='=>$formsetinst_id,
            'state IN'=>self::$StateWorkingWorkitem,
            'info_state='=>self::$StateOn,
        );
        $ret = g('pkg_db') -> select(self::$Table, $fields,$cond);
        return $ret?$ret:[];

    }
    /** 查找已处理审批工作项 */
    public function getWorkedWorkitem($formsetinst_id,$fields="mpw.*")
    {
        $table = "mv_proc_workitem mpw, mv_proc_work_node mpwn ";
        $cond = array(
            'mpw.work_node_id = mpwn.id and 1='=>1,
            'mpw.formsetinit_id='=>$formsetinst_id,
            'mpw.state IN'=>self::$StateDealedWorkitem,
            'mpw.info_state='=>self::$StateOn,
            'mpwn.is_start ='=>0,
        );
        $ret = g('pkg_db') -> select($table, $fields,$cond);
        return $ret?$ret:[];

    }
    /** 查找待办审批工作项 */
    public function getWorkitemByState($formsetinst_id,$state,$fields="*")
    {
        $cond = array(
            'formsetinit_id='=>$formsetinst_id,
            'state='=>$state,
            'info_state='=>self::$StateOn,
        );
        $ret = g('pkg_db') -> select_one(self::$Table, $fields,$cond);
        return $ret?$ret:[];

    }
    /**
     *
     * 更新工作项内容new
     * @param int $workitem_id
     * @param int $com_id
     * @param string $judgement
     * @throws \Exception
     * @return
     */
    public function update_workitem_data($workitem_id,$state,$judgement){
        $cond = array(
            'id=' => $workitem_id,
        );
        $data = array(
            'state' => $state,
            'judgement' => $judgement
        );
        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);
        if (!$ret) throw new \Exception('更新工作项失败');

        return $ret;
    }
    /**
     *
     * 更新工作项内容new
     * @param int $workitem_id
     * @throws \Exception
     * @return
     */
    public function delete_by_id($workitem_id){
        $cond = array(
            'id=' => $workitem_id,
        );
        $data = array(
            'info_state' => self::$StateOff,
        );
        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);
        if (!$ret) throw new \Exception('删除工作项失败');

        return $ret;
    }

    /**
     * 获取流程节点信息
     * @param $formsetinit_id
     * @param $work_node_id
     * @param array $states
     * @param string $fields
     * @return array
     */
    public function get_workitem_by_pre_node_id($formsetinit_id,$work_node_id,$com_id,$states=array(),$fields='*'){
        //获取节点所有工作项
        $workitems = $this->get_node_workitem($formsetinit_id, $work_node_id, $com_id,'','mpw.id');
        if(empty($workitems)) return array();

        $cond = array(
            'formsetinit_id=' => $formsetinit_id,
            'pre_work_node_id regexp' => '"'.implode('"|"',array_column($workitems,'id')).'"',
            'info_state=' => self::$StateOn,
        );

        if($states ){
            $cond['state IN']=$states;
        }

        $order = " ORDER BY id DESC ";
        $ret = g('pkg_db') -> select(self::$Table, $fields, $cond, 0, 0, '', $order);
        return $ret ? $ret : array();
    }

    //---------------------------------------内部实现---------------------------------------------------------

    /**
     * 获取互联表单权限
     * @return void
     */
    private function getRelationFromWhereSql($com_id,$form_history_id){
//        $sql = "EXISTS(
//            SELECT form_history_id
//            FROM mv_proc_form_relation_ref
//            where relation_com_id={$com_id}
//            and form_history_id={$form_history_id})";

        $sql = "EXISTS(
            SELECT id 
            FROM school_role_ref 
            where school_id={$com_id}  
            and info_state=1
            and EXISTS( 
                select form_history_id 
                from mv_proc_form_model 
                where info_state=1 
                and form_history_id={$form_history_id} 
                and com_id=school_role_ref.parent_unit_id
                and type='".Mv_Form::TYPE_RELATION."') 
            )
            or 
            EXISTS( 
                select form_history_id 
                from sc_com_form
                where form_history_id={$form_history_id} 
                and type='4') 
            ";

        return $sql;
    }
	
	
	
}

// end of file