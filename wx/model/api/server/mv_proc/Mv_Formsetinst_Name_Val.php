<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */
namespace model\api\server\mv_proc;

class Mv_Formsetinst_Name_Val extends \model\api\server\ServerBase{
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_formsetinst_name_val';

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}
    /**
     * 获取表单实例名值
     * @param $com_id
     * @param $formsetinst_id
     * @param $name_key
     * @return mixed
     * @throws \Exception
     */
    public function get($com_id,$formsetinst_id,$name_key)
    {
        $cond = [
            'com_id='=>$com_id,
            'formsetinst_id='=>$formsetinst_id,
            'name_key='=>$name_key,
            'info_state='=>1,
        ];
        $ret = g('pkg_db') -> select_one(self::$Table, "*", $cond);
        return $ret?$ret:[];
    }
    /**
     * 保存表单实例名值
     * @param $com_id
     * @param $formsetinst_id
     * @param $name_key
     * @param $name_value
     * @return mixed
     * @throws \Exception
     */
    public function save($com_id,$formsetinst_id,$name_key,$name_value)
    {
        $value = $this->get($com_id,$formsetinst_id,$name_key);
        $time = time();
        if ($value) {
            // 更新
            $cond = [
                'id='=>$value['id']
            ];
            $data['update_time'] = $time;
            $data['name_value'] = $name_value;
            $ret = g('pkg_db') -> update(self::$Table, $cond, $data);
        } else {
            // 新增
            $data = [
                'com_id'=>$com_id,
                'info_state'=>1,
                'formsetinst_id'=>$formsetinst_id,
                'name_key'=>$name_key,
                'name_value'=>$name_value,
                'update_time'=>$time,
                'create_time'=>$time,
            ];
            $ret = g('pkg_db') -> insert(self::$Table, $data);
        }
        if (!$ret) throw new \Exception('保存表单实例名值失败');
        return $ret;
    }
}

// end of file