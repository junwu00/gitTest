<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
namespace model\api\server\mv_proc;

use model\api\dao\ComForm;

class Mv_Form extends \model\api\server\ServerBase{
    const TYPE_SHARE = 'share';
    const TYPE_RELATION = 'relation';
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_form_model';
	/** 分类表 */
	private static $TableClassify = 'mv_proc_form_classify';
	/** 流程模板表 */
	private static $TableWorkModel = 'mv_proc_work_model';
	/** 流程节点表 */
	private static $TableWorkNodeModel = 'mv_proc_work_node';
	/** 表单控件信息 */
	private static $TableFormLable = 'mv_proc_form_model_label';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/** 发布版本 */
	private static $StatePublic = 2;
	/** 历史版本 */
	private static $StateHistory = 1;

	/** 流程自定义表单类型 */
	private static $IsProc = 0;
	/** 请假表单类型 */
	private static $IsRest = 1;
	/** 报销表单类型 */
	private static $IsExaccount = 2;

	/** 应用版本1.0 */
	private static $Version_v1 = 0;
	/** 应用版本2.0 */
	private static $Version_v2 = 1;

    /**
     * 获取关联企业信息
     * @return void
     */
    public function listSchoolCom($parent_unit_id,$keyword=''){
        $table = ' school_role_ref ref left join sc_company sc on sc.id=ref.school_id ';
        $cond = array(
            'ref.info_state='=>1,
            'ref.parent_unit_id='=>$parent_unit_id,
            'sc.state!='=>0,
        );

        if($keyword!==''){
            $cond['sc.name like'] = '%'.$keyword.'%';
        }

        $fields = 'sc.id,sc.name';
        $ret = g('pkg_db') -> select($table, $fields, $cond,0,0,'sc.id');
        return $ret?$ret:[];
    }
    /**
     * 获取关联企业信息
     * @return void
     */
    public function listRelationCom($form_history_id,$keyword=''){
        $table = ' mv_proc_form_relation_ref mpfr left join sc_company  sc on sc.id=mpfr.relation_com_id ';
        $cond = array(
            'sc.state!='=>0,
            'mpfr.info_state='=>1,
            'mpfr.form_history_id='=>$form_history_id,
        );

        if($keyword!==''){
            $cond['sc.name like'] = '%'.$keyword.'%';
        }

        $fields = 'sc.id,sc.name';
        $ret = g('pkg_db') -> select($table, $fields, $cond,0,0,'sc.id');
        return $ret?$ret:[];
    }

    /**
     * 提醒已读时间
     * @param $user_id
     * @param $push_time
     * @return void
     */
    public function setRemindReadtime($user_id,$form_history_id,$push_time){
        $update_data = array(
            'read_time'=>time(),
            'state'=>2,
        );
        $cond = array(
            'user_id='=>$user_id,
            'form_history_id='=>$form_history_id,
            'push_time='=>$push_time,
            'read_time='=>0
        );

        return g('pkg_db')->update('mv_proc_form_remind_push',$cond,$update_data);

    }
	
	/**
	 * 根据ID获取审批表单new
	 * @param int $com_id
	 * @param int $id
	 */
	public function get_by_id($id,$com_id , $fields='*') {
		$cond = array(
			'id=' => $id,
			//'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return !$ret||count($ret)==0?FALSE:$ret[0];
	}
	
	/** 根据表单ID数组获取表单信息 */
	public function get_forms_by_ids($ids, $fields ='*'){
		$cond = array(
				'id IN' => $ids,
				'info_state=' =>self::$StateOn
			);
		return g('pkg_db') -> select(self::$Table, $fields, $cond);
	}

	//获取最新版本控件内容信息
    public function getNewestInput($form_id,$input_key,$fields='l.*'){
	    $table = self::$Table.' f left join '.self::$TableFormLable.' l on l.form_id=f.id ';
        $cond = array(
            'l.input_key='=>$input_key,
            'f.version='=>2,
            '^EXISTS(select id from '.self::$Table.' where id='.$form_id.' and form_history_id=f.form_history_id)'=>' ',
        );
        $return = g('pkg_db') -> select($table, $fields, $cond);
        return $return?$return[0]:[];
    }

	/**
	 * 根据历史版本id获取发布版本的表单new
	 * @param int $form_history_id 历史版本id
	 * @param int $com_id 企业id
	 */
	public function get_by_his_id($form_history_id,$com_id,$fields='*'){
		$cond = array(
			'form_history_id=' => $form_history_id,
			//'com_id=' => $com_id,
			'version=' => self::$StatePublic,
			'info_state=' =>self::$StateOn
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return !$ret||count($ret)==0?FALSE:$ret[0];
	}

    /**
     * 根据历史版本id获取发布版本的表单new
     * @param int $form_history_id 历史版本id
     * @param int $com_id 企业id
     */
    public function list_by_his_id($form_history_ids,$fields='*'){
        $cond = array(
            'form_history_id IN' => $form_history_ids,
            'version=' => self::$StatePublic,
        );
        $ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
        return $ret?$ret:[];
    }
	
	
	/**
	 * 获取有表单的所有分类new
	 * @param  [type]  $com_id      
	 * @param  [type]  $user_id     
	 * @param  integer $app_version 应用版本，0：版本v1，1：版本v2
	 * @return [type]               
	 */
	public function get_classify($com_id,$user_id, $app_version=0){
		$classify_sql ="select classify_id,dominated,classify_name from ".self::$Table." mpfm,".self::$TableClassify." mpfc 
		where mpfm.classify_id = mpfc.id and mpfc.app_version={$app_version} and mpfm.app_version={$app_version} 
		and mpfm.version = ".self::$StatePublic." and mpfm.info_state=".self::$StateOn." and mpfm.com_id=".$com_id.
		" group by classify_id,dominated,classify_name order by dominated";
		
		$classifys = g('pkg_db') -> query($classify_sql);
		
		return !$classifys||count($classifys)==0 ? FALSE : $classifys;
	}
	
	/**
	 * 根据分类id获取表单信息new
	 * @param int $com_id
	 * @param int $classify_id
	 * @param  integer $app_version 应用版本，0：版本v1，1：版本v2
	 */
	public function get_form_by_classify($classify_id,$com_id,$user_id, $fields='*',$app_version=0){
		$form_sql ="select ".$fields." from ".self::$Table."
		where classify_id = ".$classify_id." and app_version={$app_version} 
		and version = ".self::$StatePublic." and info_state=".self::$StateOn." and com_id=".$com_id.
		"  order by create_time desc";
		
		$form_list = g('pkg_db') -> query($form_sql);
		
		return !$form_list||count($form_list)==0 ? FALSE : $form_list;
	}
	
	/**
	 * 根据表单id获取控件
	 * @param  [type] $form_id 
	 * @return [type]          
	 */
	public function get_inputs_by_form($form_id) {
		$cond = array(
			'form_id=' => $form_id 
			);

		$inputs = g('pkg_db') -> select(self::$TableFormLable, '*', $cond);
		if (!$inputs) throw new \Exception('获取表单控件失败');

		$inputs = $this -> build_inputs($inputs);
		return $inputs;
	}

	/**
	 * 组装表单控件信息
	 * @param  [type] $inputs 
	 * @return [type]         
	 */
	public function build_inputs($inputs) {
		$data = array();
		foreach ($inputs as $key => $input) {
			if ($input['p_label_id'] != 0) continue;
			$tmp_data = array(
				'type' 		=> $input['type'],
				'must' 		=> (int)$input['must'],
				'describe' 	=> $input['describe'],
				'input_key' => $input['input_key'],
				'name' 		=> $input['name'],
				);

			$other = json_decode($input['other'], TRUE);
			$tmp_data = array_merge($tmp_data, $other);

			//子表单控件
			if ($input['type'] == 'table') {
				$tmp_table = array();
				foreach ($inputs as $key2 => $in) {
					if ($in['p_label_id'] != 0 && $input['id'] == $in['p_label_id']) {
						$tmp_arr = array(
							'type' 		=> $in['type'],
							'must' 		=> (int)$in['must'],
							'describe' 	=> $in['describe'],
							'th_key'	=> $in['input_key'],
							'name' 		=> $in['name'],
							);
						$in_other = json_decode($in['other'], TRUE);
						$tmp_arr = array_merge($tmp_arr, $in_other);

						$tmp_table[$in['input_key']] = $tmp_arr;
					}
				}unset($in);

				$tmp_data['table_arr'] = $tmp_table;
			}

			$data[$input['input_key']] = $tmp_data;
		}unset($input);

		return $data;
	}
	
	
	/**
	 * 直接获取发布状态表单（请假和报销用）
	 */
	public function get_public_proc($is_other_proc,$com_id, $fields=' mpfm.*'){
		$sql ="select ". $fields ." from ".self::$Table." mpfm,".self::$TableWorkModel." mpwm 
		where mpwm.form_id = mpfm.id 
		and mpfm.is_other_proc = ".$is_other_proc." and mpfm.com_id = ".$com_id.
		" and mpfm.info_state=".self::$StateOn." and mpfm.version=".self::$StatePublic." order by id desc";
		$ret = g('pkg_db') -> query($sql);
		if (!$ret) {
			$ret = array();
		}
		return $ret[0];
	}

	/**
	 * gch add trigger form
	 * 根据form_id获取发布版本表单
	 */
	public function getPublicFormById($comId, $formId, $fields='m2.*'){
		$sql ="SELECT " . $fields . " FROM " . self::$Table . " m1 LEFT JOIN " . self::$Table . " m2 ON m1.form_history_id=m2.form_history_id where m1.id=" . $formId . " AND m2.version=2 AND m1.com_id=" . $comId;
		$ret = g('pkg_db') -> query($sql);

		if (!is_array($ret)) {
			$ret = array();
		} else {
			$ret = array_shift($ret);
		}

		return $ret;
	}

    public function listClassByIds($classIds,$fields='*'){
        if(empty($classIds))
            return [];

        $cond = array(
            'id IN'=>$classIds
        );
        $ret = g('pkg_db')->select(self::$TableClassify,$fields,$cond);
        return $ret?$ret:[];
    }

//----------------------------主页型应用接口新增-----------------------------------------------------


	/** 根据部门IDs获取发布版本的表单 */
	public function get_publish_form($com_id, $dept_ids=array(), $group_ids=array(), $page=1, $page_size=10, $fields='*', $apps=array(),$user_ids=[]) {
		if (empty($apps)) {
			$return = array(
				'data' => array(),
				'count' => 0
				);
			return $return;
		}

		$scopes = '';
		$groups = '';
        $users = '';
		!empty($dept_ids) && $scopes .= " scope REGEXP '" . '"' . implode('"|"', $dept_ids) . '"' . "'"; 
		!empty($group_ids) && $groups .= " groups REGEXP '" . '"' . implode('"|"', $group_ids) . '"' . "'";
        !empty($user_ids) && $users .= " user_list REGEXP '" . '"' . implode('"|"', $user_ids) . '"' . "'";
		$power = '';
		if (!empty($scopes) && !empty($groups)) {
			$power = $scopes . ' OR ' . $groups;
		} elseif (!empty($scopes)  && empty($groups)) {
			$power = $scopes;
		} elseif (!empty($groups)  && empty($scopes)) {
			$power = $groups;
		}

        if($users){
            if($power){
                $power = $power.' or '. $users;
            }else{
                $power = $users;
            }
        }


		$ext = " is_other_proc IN (".implode(',', $apps) .") AND ({$power})";

        //一些特殊表单不用显示出来
        $dao_com_form = new ComForm();
        $revoke_form = $dao_com_form->getByComId($com_id,ComForm::TYPE_REVOKE);
        if($revoke_form){
            $ext = " is_other_proc IN (".implode(',', $apps) .") AND ({$power}) AND form_history_id != '{$revoke_form['form_history_id']}'";
        }

        $sql = "SELECT COUNT(1) AS count 
			FROM mv_proc_form_model
			WHERE com_id={$com_id} AND version=". self::$StatePublic .
				" AND info_state=". self::$StateOn .
				" AND is_show_wx=". self::$StateOn .
				" AND app_version=". self::$Version_v2 .
				" AND {$ext}";

		$count = g('pkg_db') -> query($sql);
		$count = $count ? $count[0]['count'] : 0;

		if ($count == 0) {
			$return = array(
				'data' => array(),
				'count' => $count
				);
			return $return;
		}

		$sql = "SELECT {$fields} FROM mv_proc_form_model
			WHERE com_id={$com_id} AND version=". self::$StatePublic .
				" AND info_state=". self::$StateOn .
				" AND is_show_wx=". self::$StateOn .
				" AND app_version=". self::$Version_v2 .
				" AND {$ext} 
				ORDER BY id DESC";
		if ($page > 0 && $page_size > 0) {
            $begin = ($page - 1) * $page_size;
            $limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
            $sql .= $limit;
        }
		$form = g('pkg_db') -> query($sql);

		$return = array(
			'data' => $form,
			'count' => $count
			);

		return $return;
	}
	
	
	//---------------------------------------内部实现

}

// end of file