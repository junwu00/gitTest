<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */
namespace model\api\server\mv_proc;

class Mv_Reference extends \model\api\server\ServerBase{
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_reference';
	
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/** 历史 0 */
	private static $StateNotUsing = 0;
	/** 正在使用 1 */
	private static $StateUsing = 1;
	
	public function get_reference_by_userid($user_id){
		$sql = "select * from ".self::$Table." 
		where info_state=".self::$StateOn." and is_using=".self::$StateUsing;
		
		$list = g('pkg_db') -> query($sql);
		
		if(!$list){
			return array();
		}
		
		$ret = array();
		
		//员工可见流程
		$user = g('dao_user') -> get_by_id($user_id);
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$depts = array();
		foreach ($dept_tree as $dept) {
			$depts = array_merge($depts, $dept);
		}
		unset($dept);
		
		$i = 0;
		foreach ($list as $item) {			//遍历所有流程
			$dept_ids = json_decode($item['dept_ids'], TRUE);
			if (count($dept_ids) == 0) {				//企业内可见
				$ret[$i] = $item;
				$i++;
				
			} else {
				foreach ($dept_ids as $s) {			//记录可见的流程
					if (in_array($s, $depts)) {
						$ret[$i] = $item;
						$i++;
						break;
					}
				}
				unset($s);
			}
		}
		unset($item);
		
		return $ret ? $ret : Array();
	}
	
	
	/**
	 * 获取当前文号
	 */
	public function get_cur_reference($id,$user_id,$field="*"){
		$sql = "select ".$field." from ".self::$Table." 
		where info_state=".self::$StateOn." and id=".$id;
		
		$item = g('pkg_db') -> query($sql);
		
		if(!$item){
			throw new \Exception('查找文号数据失败');
		}
		
		$ret = FALSE;
		
		//员工可见流程
		$user = g('dao_user') -> get_by_id($user_id);
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$depts = array();
		foreach ($dept_tree as $dept) {
			$depts = array_merge($depts, $dept);
		}
		unset($dept);
		
		$i = 0;
		$dept_ids = json_decode($item['dept_ids'], TRUE);
		if (count($dept_ids) == 0) {				//企业内可见
			$ret = $item;
			$i++;
			
		} else {
			foreach ($dept_ids as $s) {			//记录可见的流程
				if (in_array($s, $depts)) {
					$ret = $item;
					$i++;
					break;
				}
			}
			unset($s);
		}
		unset($item);
		return $ret ;
	}
	
	
	/**
	 * 更新当前文号（当前值）
	 */
	public function update_reference($id,$cur_num){
		$cond = array(
			'id=' => $id,
		);

		$data = array(
			'cur_num' => $cur_num,
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('更新文号信息失败');
		}
		return $ret;
	}
	
	
}

// end of file