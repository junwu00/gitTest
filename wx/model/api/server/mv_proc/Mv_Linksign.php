<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */
namespace model\api\server\mv_proc;

class Mv_Linksign extends \model\api\server\ServerBase{
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_linksign';
	
	/** 关闭 0 */
	private static $StateLinksignClose = 0;
	/** 启用 1 */
	private static $StateLinksignOpen = 1;
	
	
	/**
	 * 查询领签配置
	 * @param int $com_id
	 */
	public function select_linksign($com_id,$is_other_proc=0) {
		$sql = "select * from ".self::$Table." where com_id=".$com_id." and is_other_proc=".$is_other_proc;
		$ret = g('pkg_db') -> query($sql);
		$ret = empty($ret) ? array() : $ret[0];
		return !$ret||!$ret["is_linksign"]? FALSE : TRUE;
	}
	
}

// end of file