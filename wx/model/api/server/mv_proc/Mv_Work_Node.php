<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
namespace model\api\server\mv_proc;

class Mv_Work_Node extends \model\api\server\ServerBase{
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_work_node';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/** 开始节点标示 */
	protected static $StateStart = 1;
	/** 非开始节点标示 */
    protected static $StateNotStart = 0;

    /** 开启驳回 */
    protected static $BreakStateOn = 1;
    /** 关闭驳回 */
    protected static $BreakStateOff = 0;

    /** 开启撤销 */
    protected static $RevokeStateOn = 1;
    /** 关闭撤销 */
    protected static $RevokeStateOff = 0;


    public function getStartNode($work_id,$fields='*'){
        $cond = array(
            'work_id='=>$work_id,
            'is_start='=>1,
            'info_state=' =>self::$StateOn
        );

        $ret = g('pkg_db') -> select(self::$Table, $fields, $cond);

        return $ret ? $ret[0] : FALSE;
    }

	/**
	 * 根据ID获取流程节点new
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function get_by_id($com_id, $id, $fields='*') {
		$cond = array(
			'id=' => $id,
			//'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		
		return $ret ? $ret[0] : FALSE;
	}
	
	
	public function get_worknode_by_condition($com_id,$work_id,$cond=array(),$fields='*'){
		$condition = array(
			//'com_id='=>$com_id,
			'work_id=' =>$work_id
		);
		
		$cond = array_merge($condition,$cond);
		
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret : FALSE;
	}
	
	
	
	/**
	 * 根据ids获取节点信息new
	 * @param int $com_id
	 * @param string $ids
	 */
	public function get_work_node_by_id($com_id,$ids){
		//$sql = "select * from ".self::$Table." where com_id=".$com_id." and id in(".$ids.") and info_state=".self::$StateOn;
        $sql = "select * from ".self::$Table." where id in(".$ids.") and info_state=".self::$StateOn;
		$ret = g('pkg_db') -> query($sql);
		return $ret ? $ret : FALSE;
		
	}

    /**
     * 获取节点信息
     * @param  [type] $com_id
     * @param  [type] $work_id
     * @return [type]
     */
    public function get_work_node_by_ids($ids, $fields='*') {
        $cond = array(
            'id IN' => $ids,
            'info_state=' => self::$StateOn
        );

        $orderBy = " idx asc ";
        $nodes = g('pkg_db') -> select(self::$Table, $fields, $cond, 0 , 0, '', $orderBy);
        return $nodes ? $nodes : array();
    }
	
	/**
	 * 
	 * 查询模板的开始节点new
	 * @param 公司id $com_id
	 * @param 流程模板id $work_id
	 * @param 查询字段 $fields
	 */
	public function get_start_by_workid($com_id,$work_id, $fields='*'){
		$cond = array(
			'work_id=' => $work_id,
			'is_start=' => self::$StateStart,
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
    /**
     *
     * 查询模板的开始节点new
     * @param int 公司id $com_id
     * @param int 流程模板id $work_id
     * @param string 查询字段 $fields
     * @return array
     */
    public function get_end_by_workid($com_id,$work_id, $fields='*'){
        $cond = array(
            'work_id=' => $work_id,
            'next_node_id is null and 1=' => 1,
            'com_id=' => $com_id,
            'info_state=' =>self::$StateOn
        );
        $ret = g('pkg_db') -> select_one(self::$Table, $fields, $cond);
        return $ret ? $ret : [];
    }
	//---------------------------------------内部实现

}

// end of file