<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
namespace model\api\server\mv_proc;

class Mv_Formsetinst extends \model\api\server\ServerBase{
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_formsetinst';
	/** 表单实例控件信息 */
	private static $TableFormsetinstLable = 'mv_proc_formsetinst_label';
	
	/** 禁用 0*/
	private static $StateOff = 0; 
	/** 启用 1*/
	private static $StateOn = 1;
	
	/**
	 * 表单实例状态
	 */
	/** 草稿 */
    public static $StateDraft = 0;
	/** 运行中 */
    public static $StateDone = 1;
	/** 已结束 */
    public static $StateFinish = 2;
	/** 终止 */
    public static $StateStop = 3;
    /** 撤回 */
    public static $StateRevoke = 4;
    /** 撤销中 */
    public static $StateRevoking = 5;
    /** 通过后已撤销 */
    public static $StateFinishRevoke = 6;

    /** @var string 撤销流程表单撤销理由控件input_key */
    const REVOKE_REASON_INPUT_KEY = 'input1636360210000';

    /** 获取撤销表单实例控件值 */
    public function getRevokeFormsetinstLabel($formsetinst_name,$revoke_reason)
    {
        return [
            "input1636360195000"=>[
                "type"=>"text",
                "must"=>0,
                "describe"=>"",
                "input_key"=>"input1636360195000",
                "name"=>"撤销申请",
                "text_size"=>15,
                "titlecolor"=>"646566",
                "desccolor"=>"c8c9cc",
                "placeholder"=>"",
                "format"=>"default",
                "formula"=>"",
                "formula_str"=>"",
                "visit_input"=>1,
                "edit_input"=>1,
                "val"=>$formsetinst_name
            ],
            "input1636360201000"=>[
                "type"=>"date",
                "must"=>0,
                "describe"=>"",
                "input_key"=>"input1636360201000",
                "name"=>"提交时间",
                "text_size"=>15,
                "titlecolor"=>"646566",
                "desccolor"=>"c8c9cc",
                "default_val"=>0,
                "placeholder"=>"",
                "format"=>"datetime",
                "visit_input"=>1,
                "edit_input"=>1,
                "val"=>date("Y-m-d H:i")
            ],
            self::REVOKE_REASON_INPUT_KEY=>[
                "type"=>"textarea",
                "must"=>0,
                "describe"=>"",
                "input_key"=>self::REVOKE_REASON_INPUT_KEY,
                "name"=>"撤销理由",
                "text_size"=>15,
                "titlecolor"=>"646566",
                "desccolor"=>"c8c9cc",
                "placeholder"=>"",
                "visit_input"=>1,
                "edit_input"=>1,
                "val"=>$revoke_reason
            ]
        ];
    }
    /** 获取撤销表单撤销理由 */
    public function getFormsetinstRevokeReason($formsetinst_id)
    {
        $cond = [
            'formsetinst_id='=>$formsetinst_id,
            'input_key='=>self::REVOKE_REASON_INPUT_KEY
        ];
        $label = g('pkg_db') ->select_one(self::$TableFormsetinstLable, '*',$cond);
        return $label?$label['val']:"";
    }
    /**
     * 获取表单实例
     * @param int $formsetinst_id
     * @return
     */
    public function get_by_id($formsetinst_id,$fields="*") {
        $cond = array(
            'id='=>$formsetinst_id,
        );
        $ret = g('pkg_db') -> select_one(self::$Table, $fields,$cond);
        return $ret?$ret:[];
    }
	/**
	 * 保存表单实例new
	 * @param int $form_id 表单模板id
	 * @param int $work_id 流程模板id
	 * @param string $formsetinst_name 实例名称
	 * @param int $creater_id 创建人id
	 * @param string $creater_name 创建人名称
	 * @param int $com_id 企业id
	 * @param array $form_vals 表单内容
	 * @param int $is_other_proc 实例模块 0 流程 1请假 2报销 3补录 4新请假 5外勤
	 * @param int $create_dept_id 创建人部门id
	 * @param string $create_dept_name 创建人部门名称
	 * @param  integer $app_version 应用版本，0：版本v1，1：版本v2
	 */
	public function save_formsetinst($form_id, $work_id, $formsetinst_name,$creater_id,$creater_name,$com_id,$form_vals,$is_other_proc=0,$create_dept_id,$create_dept_name, $app_version=0) {
		$form_vals = json_encode(array());
		$data = array(
			'form_id' 			=> $form_id,
			'work_id' 			=> $work_id,
			'formsetinst_name' 	=> $formsetinst_name,
			'create_time' 		=> time(),
			'creater_id' 		=> $creater_id,
			'creater_name' 		=> $creater_name,
			'info_state' 		=> self::$StateOn,
			'com_id' 			=> $com_id,
			'form_vals'	 		=> $form_vals,
			'state' 			=> self::$StateDraft,
			'is_other_proc' 	=> $is_other_proc,
			'creater_dept_id' 	=> $create_dept_id,
			'creater_dept_name' => $create_dept_name,
			'app_version' 		=> $app_version
		);

		$ret = g('pkg_db') -> insert(self::$Table, $data);
		if (!$ret) throw new \Exception('保存表单实例失败');
		
		return $ret;
	}
    /**
     * 根据ID更新审批表单内容
     * @param int $formsetinst_id
     * @param string $name
     * @return bool
     * @throws
     */
    public function update_formsetinst_name($formsetinst_id,$name) {
        $data = array(
            'formsetinst_name' => $name
        );
        $cond = array(
            'id='=>$formsetinst_id,
        );
        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);

        if (!$ret) {
            throw new \Exception('更新表单实例失败');
        }
        return $ret;
    }

    /**
     * 根据ID更新审批表单内容
     * @param int $formsetinst_id
     * @param string $name
     * @return bool
     * @throws
     */
    public function update_formsetinst_ftime($formsetinst_id,$finish_time,$finish_time_remark='') {

        //if(!$finish_time) return true;

        $data = array(
            'finish_time' => $finish_time,
            'finish_time_remark' => $finish_time_remark,
        );
        $cond = array(
            'id='=>$formsetinst_id,
        );
        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);

        if (!$ret) {
            throw new \Exception('更新表单实例失败');
        }
        return $ret;
    }
	
	/**
	 * 根据ID更新审批表单内容
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function update_formsetinst($formsetinst_id,$com_id,$form_vals) {
		$form_vals = json_encode($form_vals);
		$data = array(
			'form_vals' => $form_vals
		);
		$cond = array(
			'id='=>$formsetinst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new \Exception('更新表单实例失败');
		}
		return $ret;
	}
	
	
	
	/**
	 * 根据ID更新审批表单状态new
	 * @param int $com_id
	 * @param int $formsetinst_id
	 * @param int $state
     * @throws
     * @return
	 */
	public function update_formsetinst_state($formsetinst_id,$com_id,$state) {
		$data = array(
			'state' => $state
		);
		$cond = array(
			'id='=>$formsetinst_id,
			//'com_id=' => $com_id,
		); 
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);

		$cond1 = array(
			'formsetinst_id=' => $formsetinst_id,
			'info_state=' => self::$StateOn
			);
		$ret && g('pkg_db') -> update(self::$TableFormsetinstLable, $cond1, $data);
		
		if (!$ret) {
			throw new \Exception('更新表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据ID更新审批步骤new
	 * @param int $formsetinst_id
	 * @param int $com_id
	 * @param string $workitem_rule 流向字段
	 * @param int $curr_node_id 当前节点id
	 */
	public function update_formsetinst_workitemrule($formsetinst_id,$com_id,$workitem_rule,$curr_node_id) {
		
		$data = array(
			'workitem_rule' => $workitem_rule,
			'curr_node_id' =>	$curr_node_id
		);
		$cond = array(
			'id='=>$formsetinst_id,
			//'com_id=' => $com_id,
		); 
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new \Exception('更新表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 删除表单实例
	 */
	public function del_formsetInst($formsetInst_id,$com_id,$user_id) {
		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_user_id' => $user_id
		);
		$cond = array(
			'id='=>$formsetInst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new \Exception('删除表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据id获取表单实例new
	 * @param int $formsetinst_id
	 * @param int $com_id
	 */
	public function get_formsetinst_by_id($formsetinst_id,$com_id,$user_id,$fields='mpf.*,mpfm.form_name,mpfm.form_describe,mpfm.is_linksign, mpfm.is_other_proc '){
//		$formsetinstsql = "select ".$fields."
//		from mv_proc_formsetinst mpf,mv_proc_form_model mpfm
//		where mpfm.id = mpf.form_id and mpf.id = ".$formsetinst_id." and mpf.com_id = ".$com_id."
//		and (EXISTS (SELECT 1 from mv_proc_workitem where formsetinit_id={$formsetinst_id} AND (creater_id ={$user_id} or handler_id ={$user_id})) or
//		EXISTS (SELECT 1 from mv_proc_notify where formsetinst_id={$formsetinst_id} AND notify_id ={$user_id}))";

        $formsetinstsql = "SELECT {$fields}  
						FROM mv_proc_formsetinst mpf,mv_proc_form_model mpfm 
						WHERE mpfm.id=mpf.form_id 
							AND mpf.id={$formsetinst_id} 
							";

		$formsetinst = g('pkg_db') -> query($formsetinstsql);
		parent::log_i('------------------' . $formsetinst_id . '---------------');
		return empty($formsetinst) ? array() : $formsetinst[0];
	}

	/**
	*	根据表单模板的id查询该表单模板下的所有实例id
	*	
	*	@access public
	*	@param int form_id 表单模板id
	*	@return array 实例数组
	*	@author gch 2017-03-30
	*/
	public function get_formsetinst_by_form_id($form_id, $com_id) {	
		$return = array();
		// 获取表单模板的历史版本号
		$cond_1['com_id='] = intval($com_id);
		$cond_1['id='] = intval($form_id);

		$form_history_return = g('pkg_db') -> select_one('mv_proc_form_model', 'form_history_id', $cond_1);
		$form_history_id = $form_history_return['form_history_id'];
		if (empty($form_history_id)) {
			return $return;
		}

		// 获取表单模板历史版本号下的所有表单模板id
		$cond_2['form_history_id='] = intval($form_history_id);
		$cond_2['com_id='] = intval($com_id);

		$form_id_list = g('pkg_db') -> select('mv_proc_form_model', 'id', $cond_2);
		$form_id_str = '';
		foreach ($form_id_list as $value) {
			$form_id_str .= $value['id'] . ',';
		}
		empty($form_id_str) && $form_id_str = '0,';
		$form_id_str = rtrim($form_id_str, ',');

		// 获取表单模板历史版本号下的所有实例id
		$cond_3['form_id IN'] = '(' . $form_id_str . ')';
		$cond_3['com_id='] = intval($com_id);
		$cond_3['info_state='] = self::$StateOn;
		
		$formsetinst_id = g('pkg_db') -> select('mv_proc_formsetinst', 'id', $cond_3);
		!is_array($formsetinst_id) && $formsetinst_id = array();

		foreach ($formsetinst_id as $value) {
			$return[] = $value['id'];
		}

		return $return;
	}
	
	
	/**
	 * 获取我起草的列表
	 */
	public function get_do_list($condition=Array(),$com_id,$user_id,$is_finish=0,$is_proc_type=NULL,$page=0,$page_size=0,$fields='mpf.*,mpwn.work_node_name,mpfm.form_name',$order_by=" order by mpf.create_time desc ",$with_count = FALSE){
		$sql = "select ".$fields." from mv_proc_formsetinst mpf,mv_proc_work_node mpwn,mv_proc_form_model mpfm
		where mpf.curr_node_id = mpwn.id and mpfm.id = mpf.form_id 
		and mpf.creater_id = ".$user_id." and mpf.com_id = ".$com_id." and mpf.info_state =".self::$StateOn;
		
		if (!is_null($is_proc_type)) {
			if (is_numeric($is_proc_type)) {
				$is_proc_type = intval($is_proc_type);
				$sql .= " AND mpf.is_other_proc={$is_proc_type}";
			} elseif (is_array($is_proc_type) && !empty($is_proc_type)) {
				$sql .= " AND mpf.is_other_proc IN (". implode(',', $is_proc_type).")";
			}
		}

		foreach ($condition as $key => $val) {
			if($key=="formsetinst_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.formsetinst_name like '%".$val."%'";
				continue;
			}

			if($key=="s_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time>=".$val;
				continue;
			}
			if($key=="e_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time<".$val;
				continue;
			}
			if($key=="state"&&$val!=""){
				$sql = $sql." and mpf.state=".$val;
				continue;
			}
			if($key == "app_version" && $val !== ""){
				$sql = $sql." and mpf.app_version=" . $val;
				continue;
			}
		}
		unset($val);
		if($is_finish==0){//运行中
			$state = " and mpf.state=".self::$StateDone;
		}
		else if($is_finish==1){//已结束
			$state = " and mpf.state in (".implode(',',[self::$StateFinish,self::$StateStop,self::$StateRevoke,self::$StateRevoking,self::$StateFinishRevoke]).")";
		}
		else{
			$state = " and mpf.state=".self::$StateDraft;
		}
		
		$sql = $sql.$state;
		
		
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('pkg_db') -> query($limit_sql);

		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('pkg_db')->query($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count[0]['count'] : 0,
			);
		}
		
		return $ret ? $ret : FALSE;
	}


	/**
	 * 保存表单实例控件信息
	 * @param  [type]  $form_val       表单实例控件值
	 * @param  [type]  $form_id        表单id
	 * @param  [type]  $history_id     表单历史id
	 * @param  [type]  $formsetinst_id 实例id
	 * @param  integer $p_label_id     父控件（子表单特有）
	 * @param  integer $label_id       控件顺序（子表单特有）
	 * @return [type]                  
	 */
	public function save_form_vals($form_val, $creater_id, $form_id, $history_id, $formsetinst_id, $state, $p_label_id=0, $label_id=0) {
		if (!is_array($form_val) || empty($form_val)) throw new \Exception('控件值为空');

		!($form_val['val'] || $form_val['val'] === '0' || $form_val['val'] === 0) && $form_val['val'] = '';

		//文件控件处理
        $tmp_form_val = $form_val['val'];
		is_array($form_val['val']) && $form_val['val'] = json_encode($form_val['val'],JSON_UNESCAPED_UNICODE);
		
		$data = array(
			'form_id' 			=> $form_id,
			'form_history_id' 	=> $history_id,
			'formsetinst_id' 	=> $formsetinst_id,
			'state' 			=> $state,
			'creater_id' 		=> $creater_id,
			'p_label_id' 		=> $p_label_id,
			'label_id' 			=> $label_id,
			'type' 				=> $form_val['type'],
			'name' 				=> isset($form_val['name']) ? $form_val['name'] : '',
			'must' 				=> isset($form_val['must']) ? $form_val['must'] : 0,
			'input_key' 		=> $form_val['input_key'],
			'`describe`' 		=> isset($form_val['describe']) ? $form_val['describe'] : '',
			'other' 			=> json_encode($form_val['other']),
			'val' 				=> $form_val['val'],
			'create_time' 		=> time(),
            'special_val'       => isset($form_val['special_val'])?$form_val['special_val']:'',
			);

		$ret = g('pkg_db') -> insert(self::$TableFormsetinstLable, $data);
		if (!$ret) throw new \Exception('保存控件失败：' . $form_val['name']);

        if($form_val['type']=='file' && is_array($tmp_form_val)){
            foreach ($tmp_form_val as $item){
                g('dao_capacity') -> add_list_by_cache('', $item['hash'], 'process', self::$TableFormsetinstLable, $ret);
            }
        }

		return $ret;
	}

	/**
	 * 更新表单实例控件信息
	 * @param  [type]  $form_val       
	 * @param  [type]  $formsetinst_id 
	 * @param  [type]  $form_history_id  历史表单id 
	 * @return [type]                  
	 */
	public function update_form_vals($form_val, $formsetinst_id, $state, $form_history_id) {
        $tmp_form_val = $form_val['val'];
	    is_array($form_val['val']) && $form_val['val'] = json_encode($form_val['val'],JSON_UNESCAPED_UNICODE);

		$cond = array(
			'formsetinst_id=' 	=> $formsetinst_id,
			'form_history_id='	=> $form_history_id,
			'input_key=' 		=> $form_val['input_key'],
			'p_label_id=' 		=> 0,
			);
		$data = array(
			'state' => $state,
			'other' => json_encode($form_val['other']),
			'val' 	=> $form_val['val'],
            'special_val' => isset($form_val['special_val'])?$form_val['special_val']:'',
			);

		$ret = g('pkg_db') -> update(self::$TableFormsetinstLable, $cond, $data);
		if (!$ret) throw new \Exception('更新控件信息失败：' . $form_val['name']);

        if($form_val['type']=='file' && is_array($tmp_form_val)){
            //获取控件记录标识
            $exist = g('pkg_db') ->select_one(self::$TableFormsetinstLable, '*',$cond);
            foreach ($tmp_form_val as $item){
                g('dao_capacity') -> add_list_by_cache('', $item['hash'], 'process', self::$TableFormsetinstLable, $exist['id']);
            }
        }

		return $ret;
	}

	/**
	 * 删除子表单实例控件信息
	 * @param  [type]  $formsetinst_id 
	 * @return [type]                  
	 */
	public function del_form_vals($formsetinst_id, $p_label_id) {
		$cond = array(
			'formsetinst_id=' 	=> $formsetinst_id,
			'p_label_id=' 		=> $p_label_id,
			);
		$data = array(
			'info_state' => self::$StateOff
			);

		$ret = g('pkg_db') -> update(self::$TableFormsetinstLable, $cond, $data);
		if (!$ret) throw new \Exception('删除子表单实例控件信息失败');

		return $ret;
	}

	/**
	 * 获取表单实例控件
	 * @param  [type] $formsetinst_id 
	 * @param  array  $cond           
	 * @param  string $fields         
	 * @return [type]                 
	 */
	public function get_form_vals($formsetinst_id, $cond=array(), $fields='*') {
		$cond['formsetinst_id='] = $formsetinst_id;
		$cond['info_state='] = self::$StateOn;

		$ret = g('pkg_db') -> select(self::$TableFormsetinstLable, $fields, $cond);

		return $ret ? $ret : array();
	}

	/**
	 * 删除实例控件
	 * @param  [type] $id              
	 * @param  [type] $form_history_id 
	 * @param  [type] $com_id          
	 * @return [type]                  
	 */
	public function del_formsetinst_label($id, $form_history_id) {
		if (empty($id)) return TRUE;
		
		$cond = array(
			'form_history_id=' => $form_history_id,
			'formsetinst_id=' => $id
			);

		$data = array(
			'info_state' => self::$StateOff
			);

		$ret = g('pkg_db') -> update(self::$TableFormsetinstLable, $cond, $data);
		if (!$ret) throw new \Exception('删除控件失败');
		
		return $ret;
	}
    /**
     * 更新关联表单实例id
     * @param  [type] $id
     * @param  [type] $form_history_id
     * @param  [type] $com_id
     * @return [type]
     */
    public function update_relate_formsetinst_id($id, $relate_formsetinst_id) {

        $cond = array(
            'id=' => $id
        );

        $data = array(
            'relate_formsetinst_id' => $relate_formsetinst_id
        );

        $ret = g('pkg_db') -> update(self::$Table, $cond, $data);
        return $ret;
    }
	//---------------------------------------内部实现-------------------------------

}

// end of file