<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */

namespace model\api\server\mv_proc;

class Mv_File extends \model\api\server\ServerBase{
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_file';

	private static $Table_Node = 'mv_proc_work_node';

	//应用名称
	private $app_name = 'process';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	//文件类型对用文件扩展名
	private static $file_type = array(
		'image' => array('jpg', 'jpeg', 'gif', 'png', 'bmp'),
		'voice' => array('mp3', 'wma', 'wav', 'ram'),
		'video'	=> array('rm', 'rmvb', 'wmv', 'avi', 'mpg', 'mpeg', 'mp4'),
		'file'	=> array('txt', 'xml', 'pdf', 'zip', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx'),
	);

	/**
	 * 添加文件信息
	 * Enter description here ...
	 * @param array $files 文件信息 array('file_hash','file_name','file_path','file_ext')
	 * @param int $com_id 企业id
	 * @param int $user_id 操作用户id
	 * @param string $user_name 操作用户名称
	 * @param int $formsetinst_id 实例id
	 * @param int $work_node_id 节点id
	 * @throws SCException
	 */
	public function add_file($files,$com_id,$user_id,$user_name,$formsetinst_id,$work_node_id,$is_other_proc=0){
		foreach ($files as $file){
			$time = time();
			$data['formsetinst_id'] = $formsetinst_id;
			$data['create_time'] = $time;
			$data['create_id'] = $user_id;
			$data['create_name'] = $user_name;
			$data['com_id'] = $com_id;
			$data['file_key'] = $file['file_hash'];
			$data['file_name'] = $file['file_name'];
			$data['file_url'] = $file['file_path'];
			$data['info_state'] = self::$StateOn;
			$data['extname'] = $file['file_ext'];
			$data['work_node_id'] = $work_node_id;
			$result = g('pkg_db') -> insert(self::$Table, $data);
			if (!$result) {
				throw new \Exception('添加失败，请稍后再试!');
			}
			
			//加入容量文件列表
			if($is_other_proc==1) {
				$this -> app_name = 'rest';
			} else if($is_other_proc == 2){
				$this -> app_name = 'exaccount';
			}  else if($is_other_proc == 3 || $is_other_proc == 4 || $is_other_proc == 5){
				$this -> app_name = 'workshift';
			}  else if($is_other_proc == 6){
				$this -> app_name = 'exaccount';
			} else{
				$this -> app_name = 'process';
			}
			g('dao_capacity') -> add_list_by_cache($com_id, $data['file_key'], $this -> app_name, self::$Table, $result);
		}
		unset($file);
		return TRUE;
	}
	
	/**
	 * 根据节点id删除文件 new
	 * Enter description here ...
	 * @param 公司id $com_id
	 * @param 用户id $user_id
	 * @param 节点id $work_node_id
	 * @throws SCException
	 */
	public function del_file_by_node_id($com_id,$user_id,$work_node_id,$formsetinst_id){
		$cond = array(
			'work_node_id=' => $work_node_id,
			'com_id=' => $com_id,
			'create_id=' =>$user_id,
			'formsetinst_id=' => $formsetinst_id
		);

		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_userid' => $user_id
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('删除文件数据失败');
		}
		return $ret;
	}
	
	/**
	 * 根据表单实例id删除文件
	 * Enter description here ...
	 * @param int $com_id 企业id
	 * @param int $user_id 用户id
	 * @param int $formsetinst_id 实例id
	 * @throws SCException
	 */
	public function del_file_by_formsetid($com_id,$user_id,$formsetinst_id){
		$cond = array(
			'formsetinst_id=' => $formsetinst_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_userid' => $user_id
		);
		$ret = g('pkg_db') -> update(self::$Table, $cond, $data);
		if (!$ret) {
			throw new \Exception('删除文件数据失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据实例id获取文件(单表操作)
	 * Enter description here ...
	 * @param int $com_id 企业id
	 * @param int $formsetinst_id 实例id
	 */
	public function get_file_by_formsetid($com_id,$formsetinst_id, $fields='*'){
		$sql = "select ". $fields ." from ".self::$Table." 
		where  com_id = ".$com_id." and formsetinst_id=".mysql_escape_string($formsetinst_id)." 
		and info_state=".self::$StateOn." order by create_time";
		$ret = g('pkg_db') -> query($sql);
		$ret = $ret ? $ret : array();
		return $ret;
	}
	
	/**
	 * 根据实例id获取文件(连表操作)
	 * Enter description here ...
	 * @param int $com_id 企业id
	 * @param int $formsetinst_id 实例id
	 */
	public function get_files($com_id,$formsetinst_id, $fields='mpf.*,mpwn.work_node_name'){
		$sql = "select {$fields} from ".self::$Table." mpf,".self::$Table_Node." mpwn  
		where mpwn.id =mpf.work_node_id and mpf.com_id = ".$com_id." and mpf.formsetinst_id=".$formsetinst_id." and mpf.info_state=".self::$StateOn." order by mpf.create_time";
		
		$ret = g('pkg_db') -> query($sql);
		
		$ret = $ret ? $ret : Array();
		
		return $ret;
	}
	
	/**
	 * 用户下载文件new
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return TRUE
	 * @throws SCException
	 */
	public function download_file($cf_id,$com_id,$formsetinst_id, $is_other_proc=0, $size='') {
		try {
			$file_info = $this -> get_file_by_id($cf_id,$com_id,$formsetinst_id);

			$url = g('api_download') -> download_file('', $file_info['file_key'], $file_info['file_name'], $file_info['extname'], $size, TRUE);

			return $url;
		}catch(\Exception $e) {
			throw $e;
		}
	}
	
	
//-------------------------------------------------------------------内部方法	
	
	/**
	 * 
	 * 根据id获取文件信息
	 * @param int $id 文件id
	 * @param int $com_id 企业id
	 * @param int $formsetinst_id 实例id
	 * @throws SCException
	 */
	private function get_file_by_id($id,$com_id,$formsetinst_id){
		$sql = "select * from ".self::$Table." 
		where  com_id = ".$com_id." and formsetinst_id=".mysql_escape_string($formsetinst_id)." 
		and info_state=".self::$StateOn." and id=".mysql_escape_string($id);
		
		$ret = g('pkg_db') -> query($sql);
		
		if(!$ret){
			throw new \Exception('查找文件数据失败');
		}
		
		return $ret ? $ret[0] : FALSE;
	}
	
}

// end of file