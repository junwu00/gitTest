<?php
/**
 * 流程表单显隐规则处理
 * @author chenjw
 * @date 2022-03-22
 */
namespace model\api\server\mv_proc;

class Mv_Form_Rule extends \model\api\server\ServerBase {
    /** 对应的库表名称 */
    private static $Table = 'mv_proc_form_model_rule';

    private static $TableCondition = 'mv_proc_form_model_condition';

    /** 禁用 0 */
    private static $StateOff = 0;
    /** 启用 1 */
    private static $StateOn = 1;

    public function __construct($log_pre_str='') {
        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);
    }

    /**
     * 获取表单规则
     */
    public function listFormRule($com_id,$form_id,$field='*'){
        $cond = array(
            //'r.com_id='=>$com_id,
            //'c.com_id='=>$com_id,
            'r.info_state='=>self::$StateOn,
            'c.info_state='=>self::$StateOn,
            'r.form_id='=>$form_id,
        );
        $table = self::$TableCondition.' c left join '.self::$Table.' r on r.id=c.rule_id ';

        $ret = g('pkg_db') -> select($table, $field, $cond, 0, 0, '','order by r.order,c.order');
        return $ret ? $ret : array();
    }

    /**
     * 获取表单规则
     */
    public function listMultiFormRule($com_id,$form_ids,$field='*'){
        if(empty($form_ids))
            return [];

        $cond = array(
            'r.com_id='=>$com_id,
            'c.com_id='=>$com_id,
            'r.info_state='=>self::$StateOn,
            'c.info_state='=>self::$StateOn,
            'r.form_id IN '=>$form_ids,
        );
        $table = self::$TableCondition.' c left join '.self::$Table.' r on r.id=c.rule_id ';

        $ret = g('pkg_db') -> select($table, $field, $cond, 0, 0, '','order by r.order,c.order');
        return $ret ? $ret : array();
    }


}

// end of file