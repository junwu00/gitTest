<?php
/**
 * 流程统计报表模型
 * @author Luja
 * @date 2016-10-10
 */
namespace model\api\server\mv_proc;

class Mv_Report extends \model\api\server\ServerBase {
	/** 流程统计报表 */
	private static $Table = 'mv_proc_report_conf';
	/** 分类表 */
	private static $TableTable = 'mv_proc_report_table';
	/** 图表 */
	private static $TableChart = 'mv_proc_report_chart';
	/** excel文件表 */
	private static $Tablefile = 'mv_proc_report_file';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	public function __construct($log_pre_str='') {
		global $mod_name;
		parent::__construct($mod_name, $log_pre_str);
	}


	/**
	 * 获取流程报表
	 * @param  [type]  $com_id     
	 * @param  array   $cond       
	 * @param  string  $fields     
	 * @param  integer $page       
	 * @param  integer $page_size  
	 * @param  string  $order      
	 * @param  boolean $with_count 
	 * @return [type]              
	 */
	public function get_report_list($com_id, $cond=array(), $fields='*', $page=1, $page_size=10, $order='', $with_count=FALSE) {
		$cond['com_id='] = $com_id;
		$cond['info_state='] = self::$StateOn;

		empty($order) && $order = ' ORDER BY id DESC ';
		$report_list = g('pkg_db') -> select(self::$Table, $fields, $cond, $page, $page_size, '', $order, $with_count);
		return $report_list;
	}



}
//end of file