<?php
/**
 * 登录状态验证
 * @author yangpz
 *
 */
namespace model\api\server;

class Login extends \model\api\server\Base{
	
	/**
	 *	可绕过check_user方法的url参数配置
	 *	需要url中带free为参数名的参数，如：http://host/index.php?app=xxx&free。或不带free，默认进行用户身份验证
	 *	如（以下四项不能都为空）：
	 *	array(
	 *		'app' => 'process',	//可为空，如：'app' => '',
	 *		'm' => 'apply',		//可为空，如：'m' => '',
	 *		'a' => 'apply',		//可为空，如：'a' => '',
	 *		'match' => '',		//可为空，如：'match' => ''。正则表达式，用于检测url是否与该表达式匹配
	 *		'lev' => 1,			//放行等级：1=>不检测企业和员工；2=>检测企业和员工，检测失败继续放行，
	 *	),
	 */
	private $free_list;
	// private $cache_key_prefix = '';

	public function __construct($log_pre_str='') {
		$this -> cache_key_prefix = MAIN_HOST;
		$this -> free_list = load_config('model/api/free');
		empty($log_pre_str) && $log_pre_str = "[Login]";
		parent::__construct($log_pre_str);

		global $app;
        $app = g('pkg_val') -> get_get("model");
        empty($app) && $app = g('pkg_val') -> get_get("app");

	}
	
	/**
	 * 验证页面访问权限
	 */
	public function check_auth() {
		//页面访问统计
		g('api_access') -> count();

		$free = $this -> _check_free();

		switch ($free) {
			case 1:		//不检测企业和员工
				break;
				
			case 2:		//检测企业和员工，检测失败继续放行
				try {
					$this -> _check_com();
                    $this -> _check_com_state();
					$this -> _check_app();
					$this -> _check_user(2);
				} catch (\Exception $e) {}
				break;
				
			default:	//检测企业和员工，失败不放行
				try {
					$this -> _check_com();
                    $this -> _check_com_state();
					$this -> _check_app();
					$this -> _check_user();	
				} catch (\Exception $e) {
					$this -> _unset_session_info();
					parent::show_err_page($e -> getMessage());
				}
				break;
		}
	}
	
	//内部实现------------------------------------------------------------------------------
	
	/**
	 * 当前请求是否可绕过授权
	 * @return 放行的级别
	 */
	private function _check_free() {
		if (!isset($_GET['free']))	return FALSE;
		
		$m = g('pkg_val') -> get_get('m', FALSE);
		$a = g('pkg_val') -> get_get('a', FALSE);
		$app = g('pkg_val') -> get_get('app', FALSE);
		
		$flag = FALSE;
		$url = get_this_url();
		foreach ($this -> free_list as $item) {
			//配置的四项都空，若都为空，跳过该配置
			if (empty($item['app']) && empty($item['m']) && empty($item['a']) && empty($item['match']))	continue;		
			
			if (!empty($item['app']) && strcmp($item['app'], $app) != 0)		continue;
			if (!empty($item['m']) && strcmp($item['m'], $m) != 0)				continue;
			if (!empty($item['a']) && strcmp($item['a'], $a) != 0)				continue;
			if (!empty($item['match']) && !preg_match($item['match'], $url))	continue;

			$flag = $item['lev'];
			break;
		}unset($item);
		
		return $flag;
	}

	//验证企业信息=============================================================================================
	
	/**
	 * 验证要访问的企业
	 */
	private function _check_com() {
		$corp_url = g('pkg_val') -> get_get('corpurl');
		$corp_id = g('pkg_val') -> get_get('corpid');
		
		if(!empty($corp_id) && empty($corp_url)){
			$com = g('dao_com') -> get_by_corp_id($corp_id);
			if(empty($com)) throw new \Exception('企业不存在');

			if (!(isset($_SESSION[SESSION_VISIT_CORP_URL]) && strcmp($com['corp_url'], $_SESSION[SESSION_VISIT_CORP_URL]) == 0)){	
				//访问不同企业号，清除session
				$this -> _unset_session_info();	
			}
			$this -> _set_com_cookie_session($com['corp_url'], FALSE, $com);
			return TRUE;
		}
		if (empty($corp_url)) {									//未指定企业

			//1.尝试访问SESSION中的企业信息
			if (isset($_SESSION[SESSION_VISIT_CORP_URL])) {
                $this -> _update_support_info($_SESSION[SESSION_VISIT_CORP_URL]);
				return TRUE;
			}
			
			//2.尝试访问最后次访问的企业信息（cookie中）
			$last_corp_url = g('pkg_rcookie') -> get(COOKIE_VISIT_LAST_CORP_URL);
			if ($last_corp_url) {
				$this -> _set_com_cookie_session($last_corp_url['corp_url']);
				return TRUE;
			}
			

			//3.提示未指定企业
			parent::log_d('未指定要访问的企业号: ');
			
			global $app;
			$corp_tip = '未指定要访问的企业号';
			if ($app == 'entry') {
				$corp_tip = '授权已失效，请使用 "综合服务" 进行授权登录!';
			}

			throw new \Exception($corp_tip);
		}
		
		//指定旧的企业信息
		if (isset($_SESSION[SESSION_VISIT_CORP_URL]) && strcmp($corp_url, $_SESSION[SESSION_VISIT_CORP_URL]) == 0) {
            $this -> _update_support_info($_SESSION[SESSION_VISIT_CORP_URL]);
			return TRUE;
		} 

		$this -> _unset_session_info();	
		//访问另一个企业号信息
		//1.尝试访问已保存在cookie中
		$cookie = g('pkg_rcookie') -> get($corp_url);
		if ($cookie && isset($cookie[SESSION_VISIT_COM_ID])) {
			$this -> _set_com_cookie_session($corp_url, TRUE);
			$this -> _build_session($corp_url);
			//技术支持信息
			$support_info = $this -> _get_support_info($corp_url);
			$_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
			$_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
			return TRUE;
		}
		
		//2.通过微信授权
		parent::log_d('开始验证企业url: url='.$corp_url);
		
		if (empty($corp_url)) {
			throw new \Exception('未指定要访问的企业号。');
		}
		
		$this -> _set_com_cookie_session($corp_url);
		return TRUE;
	}

	/**
	 * 根据corp url保存企业信息cookie和session
	 * @param unknown_type $corp_url			企业corp url
	 * @param unknown_type $only_update_last	是否仅更新cookie中的corpurl，默认为FALSE（即更新所有）
	 */
	private function _set_com_cookie_session($corp_url, $only_update_last=FALSE, $com = null) {
		//保存最后一次访问的corp url
		g('pkg_rcookie') -> set(COOKIE_VISIT_LAST_CORP_URL, array('corp_url' => $corp_url));

		if ($only_update_last) 		return;

		if(is_null($com) || empty($com)){
			$com = g('dao_com') -> get_by_corp_url($corp_url);
		}
		if (!$com) {
			throw new \Exception('指定的企业号不存在');
		}
		
		$this -> _build_com_cookie_session($com);
		
		//技术支持信息
		$support_info = $this -> _get_support_info($corp_url, $com['agt_id']);
		$_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
		$_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
		
		parent::log_d('验证成功：url='.$corp_url.', com='.$_SESSION[SESSION_VISIT_COM_ID]);
	}

    /**
     * 更新技术支持信息
     * @param $corp_url
     */
	private function _update_support_info($corp_url) {
        $com = g('dao_com') -> get_by_corp_url($corp_url);
        $support_info = $this -> _get_support_info($corp_url, $com['agt_id']);
        $_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
        $_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
    }

	/**
	 * 保存当前访问员工所属企业的企业信息到 cookie和session
	 * @param unknown_type $com_info
	 */
	private function _build_com_cookie_session($com_info) {
		$cookie = array(
			//企业信息
			SESSION_VISIT_CORP_URL 		=> $com_info['corp_url'],
			SESSION_VISIT_COM_ID 		=> $com_info['id'],
			SESSION_VISIT_DEPT_ID 		=> $com_info['dept_id'],
			SESSION_VISIT_CORP_ID 		=> $com_info['corp_id'],
			SESSION_VISIT_CORP_SECRET 	=> $com_info['corp_secret'],
			SESSION_VISIT_AUTH_CODE 	=> $com_info['permanent_code'],
		);
		$ret = g('pkg_rcookie') -> set($com_info['corp_url'], $cookie);
		
		//保存企业信息到session
		foreach ($cookie as $key => $val) {
			$_SESSION[$key] = $val;
		}unset($val);
		
		if (!$ret) {
			parent::log_f('保存员工cookie失败');
			throw new \Exception('系统繁忙');
		}
	}
	
	/**
	 * 获取技术支持信息
	 * @param unknown_type $corp_url
	 */
	private function _get_support_info($corp_url, $agt_id=NULL) {
		if (is_null($agt_id)) {
			$com = g('dao_com') -> get_by_corp_url($corp_url);
			if (!$com) {
				throw new \Exception('指定的企业号不存在或被禁用');
			}	
			$agt_id = $com['agt_id'];
		}
		//技术支持信息
		$support_info = g('dao_agency_info') -> get_support_info($agt_id);
		return $support_info;
	}

	/** 检查企业状态是否异常 */
	private function _check_com_state() {
        //*
        if (!isset($_SESSION[SESSION_VISIT_CORP_URL]))            return;

        $com = g('dao_com') -> get_by_corp_url($_SESSION[SESSION_VISIT_CORP_URL], 'state');
        if ($com['state'] == 0) {
            die('账号已禁用，请联系管理员进行处理');
        }
        //*/
    }
	
	//验证应用信息=============================================================================================

	/**
	 * 验证app有效性和权限
	 * @param unknown_type $app_name
	 */
	private function _check_app() {
		global $app;
		
		if (empty($app)) {
			// parent::show_warn_page(array('未指定要访问的应用'));
			return TRUE;	//允许没有APP
		}
		
		if (!file_exists(MAIN_MODEL.$app)) {
			throw new \Exception('应用不存在: '.$app);
		}
		
		if($app == "index" || $app == "workshift" || $app == "process" || $app == "legwork"){
			$app = "bpm";
		}

		$app_obj = g('dao_app') -> get_by_name($app);
		if (!$app_obj) {
			$app = "bpm";
			$app_obj = g('dao_app') -> get_by_name($app);
		}
		
		$com_app = g('dao_com_app') -> get_by_app_id($app_obj['id'], $_SESSION[SESSION_VISIT_COM_ID]);
		if (!$com_app || $com_app['state'] == sg('dao_app', 'StateOff')) {
			throw new \Exception("企业未开启该应用: com={$_SESSION[SESSION_VISIT_COM_ID]}, app={$app}");
		}
		
		global $app_id;
		global $sc_app_id;
		$app_id = $com_app['wx_app_id'];
		$sc_app_id = $com_app['app_id'];
	}
	
	//验证成员信息=============================================================================================
	
	/**
	 * 验证要访问的员工信息
	 */
	private function _check_user($lev=3) {
		//获取当前用户所在当前企业号对应的微信账号
		$wxacct = $this -> _get_wxacct($lev);
		if (!$wxacct) 	throw new \Exception('找不到您的信息。可能是管理员未同步您的数据');

		
		$cond = array(
				'root_id=' => $_SESSION[SESSION_VISIT_DEPT_ID],
				'acct=' => $wxacct,
				'state!=' => sg('dao_user', 'StateDelete'),
			);
		$user = g('dao_user') -> get_by_cond($cond, '*');
		// $user = g('dao_user') -> get_by_acct($wxacct);
		if ($user) {
			if ($user['state'] == sg('dao_user', 'StateUnsubscribe')) {	//未关注，改为已关注
				parent::log_d('开始更新用户关注状态为: 已关注, user_id='.$user['id']);
				g('dao_user') -> change_state_by_acct($user['acct'], sg('dao_user', 'StateSubscribe'));
				$atoken = g('api_atoken') -> get_by_com($_SESSION[SESSION_VISIT_COM_ID]);
				try{
					g('dao_user') -> update_pic_and_wx_by_acct($atoken, $_SESSION[SESSION_VISIT_DEPT_ID], $user['acct']);
				}catch(\Exception $e){}
				
				parent::log_d('成功更新用户关注状态为: 已关注');
			}
			if ($user['state'] == sg('dao_user', 'StateFrozen')) {
				throw new \Exception('您的账号已被禁用。请联系管理员');	
			}
			
			$dept_list = $user['dept_list'];
			$dept_list = json_decode($dept_list, TRUE);
			$this -> _append_user_cookie($_SESSION[SESSION_VISIT_CORP_URL], $user, $dept_list[0]);
//			parent::log_d('获取成员成功：user_id='.$_SESSION[SESSION_VISIT_USER_ID]);
									
		} else {
			throw new \Exception('找不到您的信息。可能是管理员未同步您的数据');	
		}
	}

	/**
	 * 获取员工对应的微信账号
	 *
	 * @param integer $lev 放行等级
	 * @return mixed
	 */
	private function _get_wxacct($lev=3) {
		if (isset($_SESSION[SESSION_VISIT_USER_WXACCT])) {
			$wxacct = $_SESSION[SESSION_VISIT_USER_WXACCT];
			
		} else {
			//检测cookie中的user信息是否有变更，无变更直接使用，无需授权
			$wxacct = $this -> _check_user_cache($_SESSION[SESSION_VISIT_CORP_URL]);
			$wxacct && parent::log_d("COOKIE缓存可用，无需授权，wxacct={$wxacct}");
		}

		if ($wxacct) return $wxacct;

		$code = isset($_GET['code']) ? $_GET['code'] : NULL;
		if (empty($code)) {			//未授权
			$this_url = get_this_url();
			$url = g('wxqy_auth') -> get_oauth_url($_SESSION[SESSION_VISIT_CORP_ID], $this_url);
			if ($lev != 1) {
				//非ajax请求才会跳转
				$m = g('pkg_val') -> get_get('m');
				if ($m != 'ajax' and !preg_match('/ajax_ident=1/', $this_url)) {
					parent::log_d('跳转到授权页面: url='.$url);
					header('location:' . $url);
					exit();
					
				} else {
					parent::log_d('ajax请求，不跳转跳转到授权页面: url='.$url);
				}
			}
			
		//已授权，未记录到session
		} else {
			global $app_id;
			global $sc_app_id;
			parent::log_i('开始获取员工信息: code='.$code);

			$cache_key = $this -> _get_cache_key($code);
			$wxacct = g('pkg_redis') -> get($cache_key);

			if ($wxacct !== FALSE) {
				parent::log_i('该授权code已被锁定，进入授权等待');
				$count = 0;
				
				while($wxacct == $cache_key && $count < 50) {
					parent::log_d('休眠等待获取授权用户，已等待' . ($count + 1) . '秒');
					$wxacct = g('pkg_redis') -> get($cache_key);
					$count++;
					sleep(1);
				}
				parent::log_d('等待完成，获得授权用户[' . $wxacct . ']');
				
			} else {
				//锁定授权code，持续60秒
				g('pkg_redis') -> set($cache_key, $cache_key, 60);
				$wxacct = $this -> _get_visit_user_wxacct($code, $app_id, $sc_app_id);

                g('dao_user')->saveLoginLog($_SESSION[SESSION_VISIT_COM_ID],$wxacct);

				if (!$wxacct) {
					g('pkg_redis') -> delete($cache_key);
					
				}else {
					g('pkg_redis') -> set($cache_key, $wxacct);
				}
			}
		}

		return $wxacct ? $wxacct : FALSE;
	}

	/**
	 * 增加user信息到cookie，session
	 * @param unknown_type $corp_url
	 * @param unknown_type $user_info
	 * @param unknown_type $detp_id
	 */
	private function _append_user_cookie($corp_url, $user_info, $detp_id) {
		//员工信息
		$user_cookie = array(
			SESSION_VISIT_USER_DEPT 	=> $detp_id,										
			SESSION_VISIT_USER_ID 		=> $user_info['id'],								//记录当前员工ID
			SESSION_VISIT_USER_NAME 	=> $user_info['name'],								//记录当前员工姓名
			SESSION_VISIT_USER_WXID 	=> $user_info['weixin_id'],							//记录当前员工微信ID
			SESSION_VISIT_USER_WXACCT 	=> $user_info['acct'],								//当前员工账号
			SESSION_VISIT_IP 			=> g('pkg_ip') -> get_ip(),
		);
		
		//保存企业信息到session
		foreach ($user_cookie as $key => $val) {
			$_SESSION[$key] = $val;
		}unset($val);
		
		$ret = g('pkg_rcookie') -> set($corp_url, $_SESSION);
		if (!$ret) {
			parent::log_f('保存员工cookie失败');
			throw new \Exception('系统繁忙');
		}

		$this -> _set_user_cache($corp_url, $user_info);
	}

	/**
	 * 保存用户缓存信息
	 *
	 * @param string $corp_url 企业唯一标识
	 * @param array $info 用户信息集合
	 */
	private function _set_user_cache($corp_url, $info) {
		$data = array(
			'acct' 		=> $info['acct'],
			'tel' 		=> $info['tel'],
			'email' 	=> $info['email'],
			'weixin_id' => $info['weixin_id'],
			'state' 	=> $info['state'],
		);
		$cache_key = $this -> _get_cookie_key($corp_url);
		g('pkg_rcookie') -> set($cache_key, $data);
	}
		
	/**
	 * 检测用户缓存信息是否可用
	 * @param string	$corp_url
	 */
	private function _check_user_cache($corp_url) {
		$cache_key = $this -> _get_cookie_key($corp_url);
		$data = g('pkg_rcookie') -> get($cache_key);
		if (!is_array($data) or empty($data)) {
			parent::log_d("员工信息解密失败: corp_url={$corp_url}");
			return FALSE;
		}

		$condition = array(
			'acct=' 		=> $data['acct'],
			'tel=' 			=> $data['tel'],
			'email=' 		=> $data['email'],
			'weixin_id=' 	=> $data['weixin_id'],
			'state=' 		=> $data['state'],
			'state!='		=> 0,
			'root_id='		=> $_SESSION[SESSION_VISIT_DEPT_ID],
		);
		$check = g('dao_user') -> get_by_cond($condition);
		if (!$check) {
			parent::log_d("检测到员工信息有变更，缓存信息不可用，corp_url={$corp_url}");
			return FALSE;
		}
		return $data['acct'];
	}

	/**
	 * 获取用户的企业号账号
	 * @param unknown_type $code		获取员工信息的标识
	 * @param unknown_type $agent_id	企业号中应用的ID
	 */
	private function _get_visit_user_wxacct($code, $agent_id=0, $app_id) {
		try {
			$token = g('api_atoken') -> get_by_com($_SESSION[SESSION_VISIT_COM_ID], $app_id);
			return g('wxqy_auth') -> get_oauth_user_id($token, $code, $agent_id);

		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	//session、cookie公共操作相关==================================================================================

	/**
	 * 根据cookie中的内容，保存session
	 * @param unknown_type $corp_url
	 */
	private function _build_session($corp_url) {
		$cookie = g('pkg_rcookie') -> get($corp_url);
		if ($cookie) {
			foreach ($cookie as $key => $val) {
				if ($key == SESSION_VISIT_USER_WXACCT)	continue;	//不保存到session，以便重新获取员工信息
				$_SESSION[$key] = $val;
			}unset($val);
		}
	}
	
	/**
	 * 清除session中的企业信息
	 */
	private function _unset_session_info() {
		session_unset();
		session_destroy();
		session_start();
	}
	
	/**
	 * 清除对应企业的COOKIE信息
	 */
	private function _unset_cookie_info() {
		if (isset($_SESSION[SESSION_VISIT_CORP_URL])) {
			g('pkg_rcookie') -> delete($_SESSION[SESSION_VISIT_CORP_URL]);
		}
	}

	/**
	 * 获取用作cookie的缓存key
	 *
	 * @access public
	 * @param string
	 * @return string
	 */
	private function _get_cookie_key($str='') {
		$key_str = $this -> cache_key_prefix . ':' . __CLASS__ . ':' .__FUNCTION__ . ':cookie:' . $str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	private function _get_cache_key($str) {
		$key_str = $this -> cache_key_prefix . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

}

//end
