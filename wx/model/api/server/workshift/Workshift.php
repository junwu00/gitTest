<?php
/**
 * 考勤，请假，外出服务类
 * @author chenyihao
 * @date 2016-10-09
 */
namespace model\api\server\workshift;

class Workshift extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	private static $Config_Table = 'ws_config';

	private static $Info_Table = 'ws_workshift_info';

	private static $Fix_Table = 'ws_workshift_fix';

	private static $Plan_Table = 'ws_workshift_plan';

	private static $Date_Table = 'ws_workshift_date';

	/** 固定排班 */
	private static $Type_fix = 1;
	/** 自由排班 */
	private static $Type_plan = 2;

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}

		parent::__construct($mod_name, $log_pre_str);
	}

	/**
	 * 获取排班内假期或补班的日期信息
	 * @param  [type]  $com_id       
	 * @param  [type]  $workshift_id 
	 * @param  string  $fields       
	 * @param  integer $type        1：假期 2：补班 
	 * @return [type]                
	 */
	public function get_date_by_workshift($com_id, $workshift_id, $fields='*', $type=NULL) {
		$cond = array(
			'com_id=' 		=> $com_id,
			'workshift_id=' => $workshift_id,
			'info_state=' 	=> self::$InfoOn
			);
		!is_null($type) && $cond['type='] = $type; 

		$days = g('pkg_db') -> select(self::$Date_Table, $fields, $cond);
		return $days ? $days : array();
	}


	/** 获取指定排班的详情信息 */
	/** detail true:获取详情，包含班次信息 false:不包含 */
	public function get_workshift_detail($com_id, $id, $fields = '*', $detail=false){
		$cond = array(
				'id=' => $id,
				'com_id=' => $com_id,
				'info_state=' => self::$InfoOn,
			);
		$ret = $this -> _get_by_cond(self::$Info_Table, $cond, $fields);
		if($detail){
			$t_cond = array(
					'workshift_id=' => $id,
					'info_state=' => self::$InfoOn,
				);
			$t_fields = 'week_day, start_time, end_time, rest_start_time, rest_end_time';
			$shift = $this -> _list_by_cond(self::$Fix_Table, $t_cond, $t_fields);
			$ret['shift'] = $shift;
		}
		return $ret;
	}

	/**
	 * 获取固定排班班次信息
	 * @param  [type] $workshift_id 
	 * @param  string $fields       
	 * @return [type]               
	 */
	public function get_shift_fix($workshift_id, $fields='*') {
		$cond = array(
			'workshift_id=' => $workshift_id,
			'info_state=' => self::$InfoOn,
			);
		return $this -> _list_by_cond(self::$Fix_Table, $cond, $fields);
	}


	/** 获取指定自由排班的详情信息 */
	/** detail true:获取详情，包含班次信息 false:不包含 */
	public function get_workshift_plan($com_id, $id, $fields = '*', $detail=false){
		$cond = array(
				'id=' => $id,
				'com_id=' => $com_id,
				'info_state=' => self::$InfoOn,
			);
		$ret = $this -> _get_by_cond(self::$Info_Table, $cond, $fields);
		if($detail){
			$t_cond = array(
				'workshift_id=' => $id,
				'info_state=' => self::$InfoOn,
				);
			$t_fields = 'id, shift_name, start_time, end_time';
			$shift = $this -> _list_by_cond(self::$Plan_Table, $t_cond, $t_fields);
			$ret['shift'] = $shift;
		}
		return $ret;
	}

	/**
	 * 获取自由排班班次信息
	 * @param  [type] $workshift_id 
	 * @param  string $fields       
	 * @return [type]               
	 */
	public function get_shift_plan($workshift_id, $fields='*') {
		$cond = array(
			'workshift_id=' => $workshift_id,
			'info_state=' => self::$InfoOn,
			);
		return $this -> _list_by_cond(self::$Plan_Table, $cond, $fields);
	}

	/** 获取指定用户对应的排班ID */
	public function get_workshift_by_user_id($com_id, $user_id, $only_id=TRUE){
		$redis_key  = $this -> get_redis_key($user_id);
		$workshift = g('pkg_redis') -> get($redis_key);
		if(!empty($workshift)){
			return $only_id ? $workshift['id'] : $workshift;
		}else{
			$user = g('dao_user') -> get_by_id($user_id, " dept_list, dept_tree ");
			$dept_list = json_decode($user['dept_list'], TRUE);
			$main_dept = array_shift($dept_list);
			$dept_tree = json_decode($user['dept_tree'], TRUE);
			$main_dept_tree = array();
			foreach ($dept_tree as $d) {
				if(in_array($main_dept, $d)){
					$main_dept_tree = $d;
					break;
				}
			}unset($d);

			$workshift = array();
			$workshift_list = $this -> get_all_workshift($com_id, $user_id, $main_dept_tree, 'dept_list, user_list, id, type');
			if(count($workshift_list) == 1){
				$workshift = $workshift_list[0];
			}else if(count($workshift_list) > 1){
				$dept_work_id = array();
				foreach ($workshift_list as $w) {
					$dept_list = json_decode($w['dept_list'], TRUE);
					$user_list = json_decode($w['user_list'], TRUE);

					foreach ($main_dept_tree as $key => $d_id) {
						if(in_array($d_id, $dept_list)){
							$dept_work_id[$key] = $w;
						}
					}unset($did);

					if(in_array($user_id, $user_list)){
						$workshift = $w;
					}

					$key = array_search($w['id'], $main_dept_tree);
					$dept_work_id[$key] = $w;
				}unset($w);
				sort($dept_work_id);
				empty($workshift) && $workshift = array_shift($dept_work_id);
			}
			g('pkg_db') -> set($redis_key, json_encode($workshift), 60);
			return $only_id ? $workshift['id'] : $workshift;
		}

	}
	
	/** 获取所有排班信息 */
	public function get_all_workshift($com_id, $user_id, $dept_ids, $fields='*'){
		$cond = array(
				'com_id='  => $com_id,
				'info_state=' => self::$InfoOn,
				'__OR' => array()
			);
		$cond['__OR']['user_list like'] = '%"'.$user_id.'"%';
		foreach ($dept_ids as $key =>  $id) {
			$cond['__OR']["__{$key}__dept_list like"] = '%"'.$id.'"%'; 
		}
		return $this -> _list_by_cond(self::$Info_Table, $cond, $fields);
	}

	/** 获取制定年份的节假日 */
	public function get_holiday_by_year($year=2016){
		$holidays = array(
			'year' => $year,
			'except_days' => array(),
			'add_days' => array()
			);
		$config = load_config('model/workshift/workshift_holiday');

		if (isset($config[$year])) {
			$holidays['except_days'] = $config[$year]['except_days'];
			$holidays['add_days'] = $config[$year]['add_days'];
		}

		return $holidays;
	}

	/**
	 * 获取用户对应的排班信息
	 * @param  [type] $com_id   
	 * @param  [type] $user_ids 
	 * @param  string $fields   
	 * @param  string $by_user_id      以用户id为键值返回数组
	 * @return [type]           
	 */
	public function get_workshift_by_user_ids($com_id, $user_ids, $fields='*', $by_user_id=FALSE){
		if (empty($user_ids)) return array();
		$cond = array(
			'com_id=' => $com_id,
			'type=' => self::$Type_plan,
			'info_state=' => self::$InfoOn,
			);

		if (count($user_ids) == 1) {
			$cond['user_list LIKE '] = '%"' . $user_ids[0] . '"%';
		} else {
			$cond['user_list REGEXP '] = '"' . implode('"|"', $user_ids) . '"';
		}

		$ret = g('pkg_db') -> select(self::$Info_Table, $fields, $cond);

		if (!empty($ret) && $by_user_id) {
			$tmp_ret = array();
			foreach ($user_ids as $id) {
				$data = array();
				foreach ($ret as $val) {
					$tmp_user = json_decode($val['user_list']);
					if (in_array($id, $tmp_user)) {
						$data = $val;
						break;
					}
				}unset($val);
				if (!empty($data)) {
					$tmp_ret[$id] = $data;
				}
			}unset($id);

			$ret = $tmp_ret;
		}

		return $ret ? $ret : array();
	}

//-----------------------------------------内部实现---------------------------------------------------

	/** 获取缓存KEY */
	private function get_redis_key($str){
		return md5("WORKSHIFT_KEY:".$str.":WORKSHIFT");
	}

}

// end of file