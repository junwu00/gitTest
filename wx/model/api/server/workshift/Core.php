<?php
/**
 * 考勤，请假，外出核心服务类
 * @author chenyihao
 * @date 2016-10-09
 */
namespace model\api\server\workshift;

class Core extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/** 固定排班 */
	private static $Type_Fix = 1;
	/** 排班制 */
	private static $Type_Plan = 2;


	/** 假期 */
	private static $Date_Type_Holiday = 1;
	/** 补班 */
	private static $Date_Type_Makeup = 2;

	/** 排班信息表 */
	private static $Info_Table = 'ws_workshift_info';

	/** 假期/补班表 */
	private static $Date_Table = 'ws_workshift_date';

	/** 请假外出表 */
	private static $Absence_Table = 'ws_absence_date';

	/** 班次表 */
	private static $Shift_Table = 'ws_workshift_shift';

	/** 请假记录表 */
	private static $Rest_Record_Table = 'ws_rest_record';

	/** 外出记录表 */
	private static $Legwork_Record_Table = 'ws_legwork_record';

	/** 全局配置表 */
	private static $Config_Table = 'ws_config';

	/** 全局配置表 */
	private static $Dept_Table = 'sc_dept';

	/** 全局配置表 */
	private static $User_Table = 'sc_user';

	private static $Form_Makeup_Type = 3;

	private static $Form_Rest_Type = 4;

	private static $Form_Legwork_Type = 5;		

	private static $Fix_Wokrshift = 1;

	private static $Plan_Wokrshift = 2;

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	/** 获取指定时间段内的有效时间 */
	public function get_absence_time($com_id, $user_id, $start_time, $end_time){
		$today_time = strtotime(date('Ymd'));
		if($end_time < strtotime("+1 day", $today_time)){
			$hours = $this -> get_plan_shift_time($user_id, $start_time, $end_time);
		}else{
			$workshift = g('ws_workshift') -> get_workshift_by_user_id($com_id, $user_id, false);
			if($workshift['type'] == 1){
				$hours = $this -> get_fix_shift_time($workshift['id'], $start_time, $end_time);
			}else{
				$hours = $this -> get_plan_shift_time($user_id, $start_time, $end_time);
			}
		}
		return $hours;
	}


	/** 表单申请完成之后处理逻辑 */
	public function finsh_formsetinst($form_type, $formsetinst_id){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		switch ($form_type) {
			case self::$Form_Makeup_Type :{
				$this -> makeup_deal($com_id, $formsetinst_id);
				break;
			}
			case self::$Form_Rest_Type :{
				$this -> rest_deal($com_id, $formsetinst_id);
				break;
			}
			case self::$Form_Legwork_Type :{
				$this -> legwork_deal($com_id, $formsetinst_id);
				break;
			}
			default:{
				break;
			}
		}
	}

	/** 请假申请完成之后的处理业务 */
	public function rest_deal($com_id, $formsetinst_id){
		$rest_record = g('ws_rest') -> get_record_by_formsetinst($com_id, $formsetinst_id);
		$this -> get_absence_by_record($com_id, $rest_record['create_id'], $rest_record, 3, 1);
	}

	/** 外出申请完成之后的处理业务 */
	public function legwork_deal($com_id, $formsetinst_id){
		$legwork_record = g('ws_legwork') -> get_record_by_formsetinst($com_id, $formsetinst_id);
		$this -> get_absence_by_record($com_id, $legwork_record['user_id'], $legwork_record, 4, 2);
	}

	/** 补录申请完成之后的处理业务 */
	public function makeup_deal($com_id, $formsetinst_id){
		$makeup_record = g('ws_checkwork') -> get_makeup_by_formsetinst($com_id, $formsetinst_id);
		$shift = g('ws_shift') -> get_shift_by_time($com_id, $makeup_record['makeup_id'], $makeup_record['makeup_time']);
		$time = $shift['type'] == 1 ? $shift['start_time'] : $shift['end_time'];
		$shift_time = array(
				array(
					'id' => $shift['id'],
					'time' => $time,
					'type' => $shift['type']
					)
			);
		$this -> update_shift($shift_time, 2);
	}

	/** 获取外出/请假记录 */
	public function get_other_record($user_id, $start_date, $end_date){
		$start_time = strtotime($start_date);
		$end_time = strtotime($end_date." 23:59:59");

		$rest_record = $this -> get_rest_record($user_id, $start_time, $end_time);
		$legwork_record = $this -> get_legwork_record($user_id, $start_time, $end_time);

		return array(
				'rest' => $rest_record,
				'legwork' => $legwork_record
			);
	}

	public function get_rest_record($user_id, $start_time, $end_time, $fields="r.id, r.formsetinst_id, r.create_id user_id, r.start_time, r.end_time, f.state state"){
		$cond = array(
				'r.create_id=' => $user_id,
				'r.info_state=' => self::$InfoOn,
				"f.state!=" => 0,
				"__OR" => array(),
			);
		$cond["__OR"][] = array(
				'r.start_time>=' => $start_time,
				'r.start_time<=' => $end_time,
			);
		$cond["__OR"][] = array(
				'r.end_time>=' => $start_time,
				'r.end_time<=' => $end_time,
			);
		$table = self::$Rest_Record_Table." r inner join mv_proc_formsetinst f on f.id = r.formsetinst_id";
		return $this -> _list_by_cond($table, $cond, $fields);
	}

	public function get_legwork_record($user_id, $start_time, $end_time, $fields ="l.id, l.formsetinst_id, l.user_id, l.title, l.start_time, l.end_time, f.state"){
		$cond = array(
				'l.user_id=' => $user_id,
				'l.info_state=' => self::$InfoOn,
				"f.state!=" => 0,
				"__OR" => array(),
			);
		$cond["__OR"][] = array(
				'l.start_time>=' => $start_time,
				'l.start_time<=' => $end_time,
			);
		$cond["__OR"][] = array(
				'l.end_time>=' => $start_time,
				'l.end_time<=' => $end_time,
			);
		$table = self::$Legwork_Record_Table." l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id";
		return $this -> _list_by_cond($table, $cond, $fields);	
	}


	public function get_fix_shift_time($workshift_id, $start_time, $end_time){
		$shift_list = g('ws_workshift') -> get_shift_fix($workshift_id, 'start_time, end_time, rest_start_time, rest_end_time, week_day');
		if(empty($shift_list)) return 0;

		$workshift_date = $this -> get_workshift_date($workshift_id);
		$shift_info = array();
		foreach ($shift_list as $shift) {
			if(!isset($shift_info[$shift['week_day']])){
				$shift_info[$shift['week_day']] = array();
			}
			$shift_info[$shift['week_day']][] = $shift;
		}unset($shift);

		$start_date = date('Ymd', $start_time);
		$end_date = date('Ymd', $end_time);

		$date_list = array();
		$tmp_date = $start_date-1;
		while ($tmp_date <= $end_date) {
			$date_list[] = $tmp_date;
			$tmp_date++;
		}
		if(empty($date_list)){
			return 0;
		}
		$second = 0;
		foreach ($date_list as $date) {
			$date_time = strtotime($date);
			$week_day = date('w', $date_time);

			$d = date('Ymd', $date_time);
			//假期 直接跳过
			if(isset($workshift_date[$d]) && $workshift_date[$d] == 1) continue;
			//放假
			if(!isset($shift_info[$week_day]) && !isset($workshift_date[$d]))  continue;

			if(!isset($shift_info[$week_day])){
				//补班 取第一天的班次
				$shift = array_shift($shift_info);
			}else{
				$shift = $shift_info[$week_day];
			}
			foreach ($shift as $s) {
				$shift_start_time = strtotime($date ." ". $s['start_time']);
				$shift_end_time = strtotime($date ." ". $s['end_time']);
				if($shift_end_time < $shift_start_time){
					$shift_end_time = strtotime("+1 day", $shift_end_time);
				}

				if(!empty($s['rest_start_time'])){
					$shift_rest_start_time = strtotime($date ." ". $s['rest_start_time']);
					$shift_rest_end_time = strtotime($date ." ". $s['rest_end_time']);
					$second += $this -> get_time_meage($start_time, $end_time, $shift_start_time, $shift_rest_start_time);
					$second += $this -> get_time_meage($start_time, $end_time, $shift_rest_end_time, $shift_end_time);
				}else{
					$second += $this -> get_time_meage($start_time, $end_time, $shift_start_time, $shift_end_time);
				}
			}unset($s);
		}unset($date);
		$hours = round($second/60/60, 2);
		return $hours;
	}

	/** 获取指定员工 指定时间段内的请假/外出时间 返回小时制 */
	public function get_plan_shift_time($user_id, $start_time, $end_time){
		if(!is_array($user_id)) $user_id = array($user_id);

		$start_date = date('Ymd', $start_time);
		$end_date = date('Ymd', $end_time);
		$shift_list = g('ws_shift') -> get_shift_by_date($user_id, 0, $start_date-1, $end_date);
		/** 请假时长秒数 */
		$time_count = 0;
		foreach ($shift_list as $shift) {
			if(!empty($shift['rest_start_time'])){
				$time_count += $this -> get_time_meage($start_time, $end_time, $shift['start_time'], $shift['rest_start_time']);
				$time_count += $this -> get_time_meage($start_time, $end_time, $shift['rest_end_time'], $shift['end_time']);
			}else{
				$time_count += $this -> get_time_meage($start_time, $end_time, $shift['start_time'], $shift['end_time']);
			}
		}unset($shift);
		//秒数转小时
		$hour = round($time_count/60/60, 2);
		return $hour;
	}

	/** 获取指定排班的适用范围 */
	public function get_workshift_user($com_id, $workshift_id){
		$all_workshift = $this -> get_all_workshift($com_id);
		if(empty($all_workshift)){
			/** 没有排班 返回空 */
			return array();
		}
		
		//取出所有的部门和员工
		$ws_scope_dept = array();
		$ws_scope_other_user = array();
		$ws_scope_workshift_dept = array();
		$ws_scope_workshift_user = array();
		foreach ($all_workshift as $workshift) {
			$tmp_dept = json_decode($workshift['dept_list'], true);
			empty($tmp_dept) && $tmp_dept = array();
			$tmp_user = json_decode($workshift['user_list'], true);
			empty($tmp_user) && $tmp_user = array();

			$state = $workshift['id'] == $workshift_id ?  true : false; 

			foreach ($tmp_dept as $id) {
				$ws_scope_dept[$id] = $workshift['id'];
				$state && $ws_scope_workshift_dept[] = $id;
			}unset($id);

			foreach ($tmp_user as $id) {
				$state ? $ws_scope_workshift_user[] = $id : $ws_scope_other_user[] = $id;
			}unset($id);

		}unset($workshift);

		$dept_ids = array_keys($ws_scope_dept);
		if(empty($dept_ids)){
			return array();
		}
		$all_dept = $this -> get_depts_lev($dept_ids, 'id, lev, p_list, root_id');
		$lev = 0;
		$tree = array();
		$root_id = 0;
		foreach ($all_dept as $key => &$dept) {
			$root_id == 0 && $root_id = $dept['root_id'];
			$dept['workshift_id'] = $ws_scope_dept[$dept['id']];
			$dept['p_list'] = json_decode($dept['p_list'], true);
			!is_array($dept['p_list']) && $dept['p_list'] = array();

			if($dept['id'] == $dept['root_id']){
				$tree = $dept;
			}
		}unset($dept);
		empty($tree) && $tree = array('id' => $root_id);

		$this -> array_to_tree($tree, $all_dept);

		$user_ids = array();
		foreach ($ws_scope_workshift_dept as $id) {
			$tree_data = $this -> get_child_tree($workshift_id, $id, array($tree));
			if(!empty($tree_data['scope'])){
				$ids = $this -> get_dept_user($root_id, $tree_data['scope'], $tree_data['beside'], $ws_scope_other_user);
				$user_ids = array_merge($user_ids, $ids);
			}
		}unset($id);

		$ws_scope_workshift_user = array_merge($ws_scope_workshift_user, $user_ids);
		$ws_scope_workshift_user = array_unique($ws_scope_workshift_user);
		return $ws_scope_workshift_user;
	}

	/** 检查指定日期是否需要工作 */
	/** return 0:不在补班/假期内，1：假期内，2：补班内 */
	public function check_date_work($workshift_id, $date){
		$cond = array(
				'workshift_id=' => $workshift_id,
				'date=' => $date,
				'info_state=' => self::$InfoOn,
			);
		$fields = 'type, date';
		$ret = $this -> _list_by_cond(self::$Date_Table, $cond, $fields);

		$state = 0;
		if(empty($ret)){
			return $state;
		}
		foreach ($ret as $d) {
			if($d['type']==self::$Date_Type_Makeup){
				$state = self::$Date_Type_Makeup;
				break;
			}
			if($d['type']==self::$Date_Type_Holiday){
				$state = self::$Date_Type_Holiday;
			}
		}unset($d);
		return $state;
	}

	/** 获取全局配置 */
	public function get_config($com_id, $fields='com_id, start_date, work_hour'){
		$cond = array(
				'com_id=' => $com_id,
			);
		$ret = $this -> _get_by_cond(self::$Config_Table, $cond, $fields);
		return $ret;
	}

//---------------------------------------内部实现---------------------------------
	/** 获取所有排班 */
	private function get_all_workshift($com_id, $fields = 'id, dept_list, user_list'){
		$cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$InfoOn
			);
		$ret = $this -> _list_by_cond(self::$Info_Table, $cond, $fields);
		return $ret;
	}

	/** 获取指定部门的层次信息 */
	private function get_depts_lev($dept_ids, $fields='id, p_id, root_id, lev'){
		$cond = array(
				'id IN' => $dept_ids,
				'state!=' => self::$InfoDelete,
			);
		$order = 'order by lev asc';
		return g('pkg_db') -> select(self::$Dept_Table, $fields, $cond, 0, 0, '', $order);
	}

	/** 获取指定部门的员工 */
	private function get_dept_user($root_id, $dept_ids, $beside_dept=array(), $beside_user=array()){
		if(empty($dept_ids)) return array();
		$cond = array(
				'root_id=' => $root_id,
				'state!=' => self::$InfoDelete,
				'__OR1' => array(),
			);
		if(!empty($beside_user)){
			$cond['id not IN'] = $beside_user;
		}
		foreach ($dept_ids as $key => $id) {
			//只查找符合条件所有
			$cond['__OR1']["__{$key}__dept_tree like"] = '%"'.$id.'"%';
		}unset($id);
		$k_str = ' ';
		foreach ($beside_dept as $key => $id) {
			//去除不满足的
			$cond[$k_str."dept_tree not like"] = '%"'.$id.'"%';
			$k_str .= " ";
		}unset($id);

		$fields = 'id';
		$ret = $this -> _list_by_cond(self::$User_Table, $cond, $fields);
		$data = array();
		foreach ($ret as $user) {
			$data[] =  $user['id'];
		}unset($user);
		return $data;
	}

	private function array_to_tree(&$dept, &$all_depts){
		if(empty($all_depts)) return;
		foreach ($all_depts as $key => $d) {
			if(in_array($dept['id'], $d['p_list'])){
				!isset($dept['child']) && $dept['child'] = array();
				unset($all_depts[$key]);
				$this -> array_to_tree($d, $all_depts);
				$dept['child'][] = $d;
			}
		}unset($d);
	}

	/** 根据ID获取子树 */
	private function get_child_tree($workshift_id, $dept_id, $tree){
		if(empty($tree)) return array();
		foreach ($tree as $dept) {
			if($dept['id'] == $dept_id){
				$scope = array();
				$beside = array();
				$this -> get_tree_dept_id($workshift_id, $dept, $scope, $beside);
				return array(
						'scope' => $scope,
						'beside' => $beside
					);
			}else if(isset($dept['child'])){
				return $this -> get_child_tree($workshift_id, $dept_id, $dept['child']);
			}
		}unset($dept);
	}

	private function get_tree_dept_id($workshift_id, $dept, &$scope, &$beside){
		if($dept['workshift_id'] != $workshift_id){
			array_push($beside, $dept['id']);
		}else{
			array_push($scope, $dept['id']);
		}
		if(isset($dept['child'])){
			foreach ($dept['child'] as $d) {
				$this -> get_tree_dept_id($workshift_id, $d, $scope, $beside);
			}unset($d);
		}
	}

	/** 获取两段时间的相交时间段长度 秒 */
	private function get_time_meage($s1_time, $e1_time, $s2_time, $e2_time){
		$second = min($e1_time, $e2_time) - max($s1_time, $s2_time);
		if($second < 0 ) $second = 0;
		return $second;
	}

	//申请完成之后处理业务， 增加absence 数据， 修复班次数据 等
	private function get_absence_by_record($com_id, $user_id, $record, $shift_type, $form_type){
		$start_time = $record['start_time'];
		$end_time = $record['end_time'];

		$start_date = date("Ymd", $start_time);
		$end_date = date('Ymd', $end_time);

		$date_list = array();

		$tmp_date = $start_date-1;
		while ($tmp_date <= $end_date) {
			$date_list[] = $tmp_date;
			$tmp_date = date('Ymd', strtotime("+1 day", strtotime($tmp_date)));
		}

		foreach ($date_list as $date) {
			$shift_info = g('ws_shift') -> get_shift_by_date(array($user_id), 0, $date, $date, 'id, start_time, start_clock_time, end_time, end_clock_time, rest_start_time, rest_end_time');

			$shift_time = array();
			$one_shift_time = array();
			if(!empty($shift_info)){
				foreach ($shift_info as $shift) {
					$second = 0;
					$s_time = $e_time = 0;
					$s_s_time = $s_e_time = 0;
					$tmp_shift = array();
					$tmp_one_shift = array();
					if(!empty($shift['rest_start_time'])){
						$second += $this -> get_time_meage_return($start_time, $end_time, $shift['start_time'], $shift['rest_start_time'], $s_time, $e_time);
						$s_s_time = $s_time;
						if($shift['start_time'] == $s_time && $second != 0 ){
							$tmp_shift[] = array("id" => $shift['id'], "type" => 1, "time" => $shift['start_time']);
							//判断是否需要校正 迟到、早退时间
							if($shift['start_clock_time'] != 0){
								$tmp_start_time = 0;
								if($shift['rest_start_time'] == $e_time){
									if($end_time > $shift['rest_start_time'] && $end_time <= $shift['rest_end_time']){
										$tmp_start_time = $shift['rest_end_time'];
									}else if($end_time > $shift['rest_end_time'] && $end_time < $shift['end_time']){
										$tmp_start_time = $end_time;
									}
								}else{
									$tmp_start_time = $e_time;
								} 
								if($tmp_start_time != 0){
									$late_time = ($shift['start_clock_time'] - $tmp_start_time > 0) ? ($shift['start_clock_time'] - $tmp_start_time) : 0; 
									$tmp_one_shift['id'] = $shift['id'];
									$tmp_one_shift['late_time'] = $late_time;
									$tmp_one_shift['start_time'] = $tmp_start_time;
									
								}
							}
						}

						$second += $this -> get_time_meage_return($start_time, $end_time, $shift['rest_end_time'], $shift['end_time'], $s_time, $e_time);
						$s_e_time = $e_time;
						if($shift['end_time'] == $e_time && $second != 0){
							$tmp_shift[] = array("id" => $shift['id'], "type" => 2, "time" => $shift['end_time']);	
							//判断是否需要校正 迟到、早退时间
							if($shift['end_clock_time'] != 0){
								$tmp_end_time = 0;
								if($shift['rest_end_time'] == $s_time){
									if($start_time > $shift['rest_start_time'] && $start_time <= $shift['rest_end_time']){
										$tmp_end_time = $shift['rest_start_time'];
									}else if($start_time < $shift['rest_start_time'] && $start_time > $shift['start_time']){
										$tmp_end_time = $start_time;
									}
								}else{
									$tmp_end_time = $s_time;
								} 
								if($tmp_end_time != 0){
									$early_time = ($tmp_end_time - $shift['end_clock_time'] > 0) ? ($tmp_end_time - $shift['end_clock_time']) : 0; 
									$tmp_one_shift["id"] = $shift['id'];
									$tmp_one_shift["early_time"] = $early_time;
									$tmp_one_shift["end_time"] = $tmp_end_time;
								}
							}
						}
					}else{
						$second += $this -> get_time_meage_return($start_time, $end_time, $shift['start_time'], $shift['end_time'], $s_time, $e_time);
						$s_s_time = $s_time;
						$s_e_time = $e_time;	
						if($shift['start_time'] == $s_time && $second != 0){
							$tmp_shift[] = array("id" => $shift['id'], "type" => 1, "time" => $shift['start_time']);
							//判断是否需要校正 迟到时间
							if($shift['start_clock_time'] != 0 && $shift['end_time'] != $e_time){
								$late_time = ($shift['start_clock_time'] - $e_time > 0) ? ($shift['start_clock_time'] - $e_time) : 0; 
								$tmp_one_shift["id"] = $shift['id'];
								$tmp_one_shift["late"] = $late_time;
								$tmp_one_shift["start_time"] = $e_time;
							}
						}
						if($shift['end_time'] == $e_time && $second != 0){
							$tmp_shift[] = array("id" => $shift['id'], "type" => 2, "time" => $shift['end_time']);
							//判断是否需要校正 早退时间
							if($shift['end_clock_time'] != 0 && $shift['start_time'] != $s_time){
								$early_time = ($s_time - $shift['end_clock_time'] > 0) ? ($s_time - $shift['end_clock_time']) : 0; 
								$tmp_one_shift["id"] = $shift['id'];
								$tmp_one_shift["early_time"] = $early_time;
								$tmp_one_shift["end_time"] = $s_time;
							}
						}
					}
					if(count($tmp_shift) == 2){
						//如果不是完全覆盖，不进行自动打卡
						$shift_time = array_merge($shift_time, $tmp_shift);
					}else{
						$tmp_one_shift['rest_start_time'] = $shift['rest_start_time'];
						$tmp_one_shift['rest_end_time'] = $shift['rest_end_time'];
						$tmp_one_shift['start_clock_time'] = $shift['start_clock_time'];
						$tmp_one_shift['end_clock_time'] = $shift['end_clock_time'];
						!isset($tmp_one_shift['start_time']) && $tmp_one_shift['start_time'] = $shift['start_time'];
						!isset($tmp_one_shift['end_time']) && $tmp_one_shift['end_time'] = $shift['end_time'];
						$one_shift_time[] = $tmp_one_shift;
					}
					if(!empty($second)){
						$user_second[] = array(
							'record_id' => $record['id'],
							'user_id' => $user_id,
							'second' => $second,
							'date' => $date,
							'start_time' => $s_s_time,
							'end_time' => $s_e_time,
						);
					}
				}unset($shift);
			}
		}unset($date);

		//增加absence数据
		$this -> add_absence_data($com_id, $user_second, $form_type);
		//自动打卡
		$this -> update_shift($shift_time, $shift_type);
		//校正班次信息
		parent::log_i("校正信息：".json_encode($one_shift_time));
		$this -> update_one_shift($one_shift_time);
	}


	/** 批量增加absence信息 */
	private function add_absence_data($com_id, $user_second, $type){
		if(empty($user_second)) return;
		$data = array();
		foreach ($user_second as $user_id => $second) {
			$data[] = array(
					'com_id' => $com_id,
					'user_id' => $second['user_id'],
					'record_id' => $second['record_id'],
					'date' => $second['date'], 
					'type' => $type,
					'hours' => round($second['second']/60/60, 3),
					'start_time' => $second['start_time'],
					'end_time' => $second['end_time'],
					'info_state' => 1,
					'create_time' => time()
				);
		}unset($second);
		$table = "ws_absence_date";
		$fields = array('com_id', 'user_id', 'record_id', 'date', 'type', 'hours', 'start_time', 'end_time', 'info_state', 'create_time');
		g('pkg_db') -> batch_insert($table, $fields, $data);
	}

	/** 更新班次信息 */
	private function update_shift($shift_time, $type){
		if(empty($shift_time)) return;
		foreach ($shift_time as $shift) {
			if($shift['type'] == 1){
				$data_str = "start_clock_time = {$shift['time']} , start_state = {$type}, late_time = 0 ";
			}else{
				$data_str = "end_clock_time = {$shift['time']} , end_state = {$type} , early_time = 0 ";
			}
			$sql = "update ws_workshift_shift set {$data_str} , hours = if(start_clock_time != 0 and end_clock_time != 0, round((if(end_clock_time > end_time, end_time, end_clock_time) - if(start_clock_time > start_time, start_clock_time, start_time) - if((start_clock_time < rest_start_time and end_clock_time < rest_start_time) or (start_clock_time > rest_end_time and end_clock_time > rest_end_time), 0, (if(end_clock_time <= rest_end_time, end_clock_time, rest_end_time) - if(start_clock_time >= rest_start_time, start_clock_time, rest_start_time)) ))/60/60, 3), 0) where id ={$shift['id']}";
			g('pkg_db') -> exec($sql);
		}unset($shift);
	}

	//请假申请完成之后，校正迟到时间和早退时间 和工时
	private function update_one_shift($shift_time){
		
		foreach ($shift_time as $time) {
			$data = array();
			if(isset($time['late_time'])){
				$data["late_time"] = $time['late_time'];
			}
			if(isset($time['early_time'])){
				$data["early_time"] = $time['early_time'];
			}
			if(!empty($time['start_clock_time']) && !empty($time['end_clock_time'])){
				$s_time = 0;
				$e_time = 0;
				if(!empty($time['rest_start_time']) && !empty($time['rest_end_time'])){
					$second1 = $this -> get_time_meage_return($time['start_time'], $time['rest_start_time'], $time['start_clock_time'], $time['end_clock_time'], $s_time, $e_time);

					$second2 = $this -> get_time_meage_return($time['rest_end_time'], $time['end_time'], $time['start_clock_time'], $time['end_clock_time'], $s_time, $e_time);
					$hours = round(($second1 + $second2)/60/60, 3);
				}else{
					$second = $this -> get_time_meage_return($time['start_time'], $time['end_time'], $time['start_clock_time'], $time['end_clock_time'], $s_time, $e_time);
					$hours = round(($second)/60/60, 3);
				}
				$data["hours"] = $hours;
			}
			if(empty($data)){
				continue;
			}
			$cond = array(
					"id=" => $time['id']
				);
			parent::log_i("update_data:".json_encode($data));
			g('pkg_db') -> update(self::$Shift_Table, $cond, $data);
		}unset($time);
	}

	/** 获取两段时间的相交时间段长度 秒 */
	private function get_time_meage_return($s1_time, $e1_time, $s2_time, $e2_time, &$s_time, &$e_time){
		$s_time = max($s1_time, $s2_time);
		$e_time = min($e1_time, $e2_time);
		$second = $e_time - $s_time;
		if($second < 0 ) $second = 0;
		return $second;
	}

	/** 获取排班日期 */
	public function get_workshift_date($workshift_id, $fields = "type, date"){
		$cond = array(
				'workshift_id=' => $workshift_id,
				'info_state=' => self::$InfoOn
			);
		$date_list = $this -> _list_by_cond(self::$Date_Table, $cond, $fields);
		$all_date = array();
		foreach ($date_list as $date) {
			$date['date'] = str_replace("-", "", $date['date']);
			$all_date[$date['date']] = $date['type'];
		}unset($date);
		return $all_date;
	}

	/** 获取指定用户指定日期的absence数据 */
	public function get_absence_by_date($user_id, $date, $fields = "*"){
		$cond = array(
				"user_id=" => $user_id,
				"date=" => $date,
				"info_state=" => self::$InfoOn
			);
		return $this -> _list_by_cond(self::$Absence_Table, $cond, $fields);
	}

}

// end of file