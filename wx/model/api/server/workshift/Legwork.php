<?php
/**
 * 外勤服务类
 * @author chenyihao
 * @date 2016-10-09
 */
namespace model\api\server\workshift;

class Legwork extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	private static $Config_Table = 'ws_legwork_config';

	private static $Record_Table = 'ws_legwork_record';	

	private static $Clock_Table = 'ws_legwork_clock';	

	private static $File_Table = 'ws_legwork_file';	

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	public function save_legwork_record($com_id, $user_id, $formsetinst_id, $start_time, $end_time, $legwork_hours){
		$data = array(
				'com_id' => $com_id, 
				'user_id' => $user_id,
				'formsetinst_id' => $formsetinst_id,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'time' => $legwork_hours,
				'info_state' => self::$InfoOn,
				'create_time' => time()
			);
		return g('pkg_db') -> insert(self::$Record_Table, $data);
	}

	public function update_legwork_record($formsetinst_id, $start_time, $end_time, $legwork_hours){
		$cond = array(
				'formsetinst_id=' => $formsetinst_id,
			);
		$data = array(
				'start_time' => $start_time,
				'end_time' => $end_time,
				'time' => $legwork_hours
			);
		return g('pkg_db') -> update(self::$Record_Table, $cond, $data);
	}


	public function get_record_by_time($com_id, $user_id, $start_time, $end_time, $fields = 'l.*, f.state'){
		$cond = array(
				'l.com_id=' => $com_id, 
				'l.user_id=' => $user_id,
				'l.info_state=' => self::$InfoOn,
				"f.state!=" => 0,
				'f.info_state=' => self::$InfoOn,
				'__OR' => array(),
			);
		$cond['__OR'][] = array(
				'start_time>=' => $start_time,
				'start_time<' => $end_time,
			);
		$cond['__OR'][] = array(
				'end_time>' => $start_time,
				'end_time<=' => $end_time,
			);
		$table = self::$Record_Table." l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	public function get_record_by_formsetinst($com_id, $formsetinst_id, $fields = 'l.*, f.state'){
		$cond = array(
				'l.com_id=' => $com_id, 
				'l.formsetinst_id=' => $formsetinst_id,
				'l.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
		$table = self::$Record_Table." l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	public function get_record_by_id($com_id, $record_id, $fields = 'l.*, f.state'){
		$cond = array(
				'l.com_id=' => $com_id, 
				'l.id=' => $record_id,
				'l.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
		$table = self::$Record_Table." l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	/** 获取当前的外出记录 */
	public function get_record($com_id, $user_id, $fields = 'l.*, f.state'){
		$cond = array(
				'l.com_id=' => $com_id, 
				'l.user_id=' => $user_id,
				'l.start_time<=' => time(),
				'l.end_time>=' => time(),
				'l.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
		$table = self::$Record_Table." l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	/** 获取定位信息 */
	public function get_clock($com_id, $record_id, $fields ='c.*, f.file_path'){
		$cond = array(
				'c.com_id=' => $com_id,
				'c.record_id=' => $record_id,
			);
		$table = self::$Clock_Table .' c LEFT JOIN '. self::$File_Table.' f ON c.id = f.clock_id ';
		return $this -> _list_by_cond($table, $cond, $fields);
	}

	/** 保存定位信息 */
	public function save_clock($com_id, $user_id, $record_id, $addr, $lng, $lat, $mark){
		$data = array(
				'com_id' => $com_id,
				'user_id' => $user_id,
				'record_id' => $record_id,
				'addr_name' => $addr,
				'lng' => $lng,
				'lat' => $lat,
				'clock_time' => time(),
				'mark' => $mark,
				'create_time' => time()
			);
		return g('pkg_db') -> insert(self::$Clock_Table, $data);
	}

	/** 保存文件 */
	public function save_file($clock_id, $file_list){
		$data = array();
		foreach ($file_list as $file) {
			$tmp = array(
					'clock_id' => $clock_id,
					'file_name' => isset($file['name']) ? $file['name'] : "",
					'file_ext' => $file['ext'],
					'file_hash' => $file['hash'],
					'file_path' => $file['url'],
					'create_time' => time(),
					'info_state' => self::$InfoOn
				);
			$data[] = $tmp;
		}unset($file);
		$fields = array('clock_id','file_name','file_ext','file_hash','file_path','create_time','info_state');
		return g('pkg_db') -> batch_insert(self::$File_Table, $fields, $data);
	}


	/** 获取外出配置 */
	public function get_config_by_com($com_id, $fields = 'task_photo'){
		$cond = array(
				'com_id=' => $com_id
			);
		$config = $this -> _get_by_cond(self::$Config_Table, $cond, $fields);
		return $config['task_photo'];
	}

	/** 根据表单申请实例删除外勤记录信息 */
	/** 删除外勤记录时，同时删除实例 */
	public function delete_legwork($com_id, $formsetinst_id){
		if ($formsetinst_id < 1) return TRUE;

		//删除外出实例，回滚数据
		$this -> legwork_rollback($com_id, $formsetinst_id);

		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id
			);
		$data = array(
			'info_state' => self::$InfoDelete
			);

		$ret = g('pkg_db') -> update(self::$Record_Table, $cond, $data);
		if (!$ret) throw new \Exception('删除外出记录失败');
		
		return $ret;
	}

//--------------------------------------内部实现---------------------------------------

	/** 外出信息回滚 */
	private function legwork_rollback($com_id, $formsetinst_id){
		if(empty($formsetinst_id)) return;
		$cond = array(
				'l.com_id=' => $com_id,
				'l.formsetinst_id=' => $formsetinst_id,
				'f.state=' => 2
			);
		$table = self::$Record_Table.' l inner join mv_proc_formsetinst f on f.id = l.formsetinst_id ';
		$legwork = $this -> _get_by_cond($table, $cond, "l.start_time, l.end_time, l.user_id");
		if(empty($legwork)) return;
		$now = time();
		$state = 4;
		$start_time = $legwork['start_time'];
		$end_time = $legwork['end_time'];
		$user_id = $legwork['user_id'];
		$update_sql = "update ws_workshift_shift set start_clock_time = if(start_time >= {$start_time} and start_time <= {$end_time} and start_state = {$state}, 0, start_clock_time), end_clock_time = if(end_time >= {$start_time} and end_time <= {$end_time} and end_state = {$state}, 0, end_clock_time), start_state = if(start_clock_time = 0 and start_state = {$state}, if(start_time > {$now}, 0, 1), start_state), end_state = if(end_clock_time = 0 and end_state = {$state}, if(end_time > {$now}, 0, 1), end_state), hours = if(start_clock_time =0 or end_clock_time = 0, 0, hours) where user_id = {$user_id} and ((start_time>={$start_time} and start_time<={$end_time} and start_state={$state}) or (end_time>={$start_time} and end_time<={$end_time} and end_state={$state})) and info_state = 1";
		g('pkg_db') -> exec($update_sql);
	}

}

// end of file