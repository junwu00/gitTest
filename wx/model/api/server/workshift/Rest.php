<?php
/**
 * 请假服务类
 * @author chenyihao
 * @date 2016-11-29
 */
namespace model\api\server\workshift;

class Rest extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	private static $Type_Table = 'ws_rest_type';

	private static $Record_Table = 'ws_rest_record';

	private static $Year_Table = 'ws_rest_year';

	private static $User_Table = 'sc_user';
	
	private static $Dept_Table = 'sc_dept';

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	public function get_rest_type($com_id, $ids = array(), $fields = 'id, name, idx, min_unit'){
		$cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$InfoOn
			);
		if(!empty($ids)){
			$cond['id IN'] = $ids;
		}
		return g('pkg_db') -> select(self::$Type_Table, $fields, $cond);
	}

	public function get_record_by_time($com_id, $user_id, $start_time, $end_time, $fields = 'r.*, f.state'){
		$cond = array(
				'r.com_id=' => $com_id, 
				'r.create_id=' => $user_id,
				'r.info_state=' => self::$InfoOn,
				"f.state!=" => 0,
				'f.info_state=' => self::$InfoOn,
				'__OR' => array(),
			);
		$cond['__OR'][] = array(
				'start_time>=' => $start_time,
				'start_time<' => $end_time,
			);
		$cond['__OR'][] = array(
				'end_time>' => $start_time,
				'end_time<=' => $end_time,
			);
		$table = self::$Record_Table." r inner join mv_proc_formsetinst f on f.id = r.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	/** 根据实例ID获取记录信息 */
	public function get_record_by_formsetinst($com_id, $formsetinst_id, $fields = 'r.*, f.state'){
		$cond = array(
				'r.com_id=' => $com_id, 
				'r.formsetinst_id=' => $formsetinst_id,
				'r.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
		$table = self::$Record_Table." r inner join mv_proc_formsetinst f on f.id = r.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	/** 保存请假记录 */
	public function save_rest_record($com_id, $user_id, $formsetinst_id, $start_time, $end_time, $type, $time){
		$data = array(
				'com_id' => $com_id, 
				'create_id' => $user_id, 
				'formsetinst_id' => $formsetinst_id,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'time' => $time,
				'type' => $type,
				'create_time' => time(),
				'update_time' => time(),
				'info_state' => self::$InfoOn
			);
		return g('pkg_db') -> insert(self::$Record_Table, $data);
	}


	/** 更新请假记录 */
	public function update_rest_record($formsetinst_id, $start_time, $end_time, $type, $time){
		$cond = array(
				'formsetinst_id=' => $formsetinst_id,
			);
		$data = array(
				'start_time' => $start_time,
				'end_time' => $end_time,
				'time' => $time,
				'type' => $type,
				'update_time' => time(),
			);
		return g('pkg_db') -> update(self::$Record_Table, $cond, $data);
	}


	/** 批量获取请假信息 */
	public function get_by_ids($com_id, $ids, $fields=' id, type '){
		$cond = array(
				'com_id=' => $com_id,
				'id IN' => $ids,
				'info_state=' => self::$InfoOn
			);
		return $this -> _list_by_cond(self::$Record_Table, $cond, $fields);
	}

	/** 通过名称获取类型ID */
	public function get_by_name($com_id, $name, $fields ="*"){
		$cond = array(
				'com_id=' => $com_id,
				'name=' => $name,
				'info_state=' => self::$InfoOn
			);
		$ret = g('pkg_db') -> select(self::$Type_Table, $fields, $cond);
		return $ret ? $ret[0] : array();
	}

	/**
	 * 获取请假类型
	 * @param  [type] $com_id 
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_type_by_com($com_id, $fields='*', $order='') {
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' => self::$InfoOn
			);

		empty($order) && $order = ' ORDER BY id ASC ';
		$ret = g('pkg_db') -> select(self::$Type_Table, $fields, $cond, 0, 0, '', $order);

		return $ret ? $ret : array();
	}

	/**
	 * 获取企业员工年假配置
	 * @param  [type] $com_id 
	 * @return [type]         
	 */
	public function get_com_user_config($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' => self::$InfoOn
			);

		$ret = g('pkg_db') -> select(self::$Year_Table, $fields, $cond);

		return $ret ? $ret : array();
	}

	/**
	 * 获取企业员工年假信息（包括用户、部门信息‘年假使用时间）
	 * @param  [type] $com_id 
	 * @param  [type] $clear_date	上一年假清零日期  2015-12-31
	 * @return [type]         
	 */
	public function get_com_user_year($com_id, $user_id, $clear_time) {
		$ext .= " AND wry.user_id = {$user_id}";

		$fields = array(
			'wry.user_id', 'su.name user_name', 'sd.name dept_name', 
			'wry.days', 'wry.hours', 'wr.h_count'
			);
		$fields = implode(',', $fields);
		$info_state = self::$InfoOn;
		$sql = " SELECT {$fields} 
				FROM ws_rest_year wry LEFT JOIN 
					(SELECT wrr.create_id, SUM(wrr.time) h_count
					FROM ws_rest_record wrr, ws_rest_type wrt, mv_proc_formsetinst mpf 
					WHERE wrr.type = wrt.id 
						AND wrr.com_id={$com_id} AND wrr.info_state={$info_state} 
						AND mpf.id=wrr.formsetinst_id AND mpf.state != 0 AND mpf.info_state={$info_state} 
						AND wrr.start_time>{$clear_time} AND wrt.com_id={$com_id} 
						AND wrt.name='年假' AND wrt.info_state={$info_state} 
					GROUP BY wrr.create_id) wr ON wry.user_id=wr.create_id, sc_user su, sc_dept sd
				WHERE wry.com_id={$com_id} and wry.user_id=su.id AND wry.dept_id=sd.id AND wry.info_state={$info_state} ";
		
		!empty($ext) && $sql .= $ext;
		$ret = g('pkg_db') -> query($sql);

		return $ret ? $ret[0] : array();
	}

	/** 检查年假 */
	public function count_year_rest($com_id, $user_id, $rest_hours){
		$config = g('ws_core') -> get_config($com_id , 'clear_date, work_hour');
		$clear_date = $config['clear_date'];
		$work_hour = $config['work_hour'];

		$next_clear_time = $last_clear_time = strtotime(date('Y-').$clear_date);
		if($next_clear_time >= time()){
			$last_clear_time = strtotime('-1 year', $next_clear_time);
		}
		$rest = $this -> get_com_user_year($com_id, $user_id, $last_clear_time);
		if(empty($rest)){
			$hours = -$rest_hours;
		}else{
			$hours = $work_hour * $rest['days'] - ($rest_hours + $rest['h_count']);
		}
		return $hours;
	}


	/** 根据表单申请实例删除请假记录信息 */
	/** 删除请假记录时，同时删除实例 */
	public function delete_rest($com_id, $formsetinst_id){
		if ($formsetinst_id < 1) return TRUE;

		//删除请假 回滚数据
		$this -> rest_rollback($com_id, $formsetinst_id);

		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id
			);
		$data = array(
			'info_state' => self::$InfoDelete
			);

		$ret = g('pkg_db') -> update(self::$Record_Table, $cond, $data);
		if (!$ret) throw new \Exception('删除请假记录失败');
		
		return $ret;
	}

//-------------------------------------------内部实现-------------------------------------
	//请假数据删除，回滚
	private function rest_rollback($com_id, $formsetinst_id){
		if(empty($formsetinst_id)) return;
		$cond = array(
				'r.com_id=' => $com_id,
				'r.formsetinst_id=' => $formsetinst_id,
				'f.state=' => 2
			);
		$table = self::$Record_Table.' r inner join mv_proc_formsetinst f on f.id = r.formsetinst_id ';
		$rest = $this -> _get_by_cond($table, $cond, "r.start_time, r.end_time, r.create_id user_id");
		if(empty($rest)) return;
		$now = time();
		$state = 3;
		$start_time = $rest['start_time'];
		$end_time = $rest['end_time'];
		$user_id = $rest['user_id'];
		$update_sql = "update ws_workshift_shift set start_clock_time = if(start_time >= {$start_time} and start_time <= {$end_time} and start_state = {$state}, 0, start_clock_time), end_clock_time = if(end_time >= {$start_time} and end_time <= {$end_time} and end_state = {$state}, 0, end_clock_time), start_state = if(start_clock_time = 0 and start_state = {$state}, if(start_time > {$now}, 0, 1), start_state), end_state = if(end_clock_time = 0 and end_state = {$state}, if(end_time > {$now}, 0, 1), end_state), hours = if(start_clock_time =0 or end_clock_time = 0, 0, hours) where user_id = {$user_id} and ((start_time>={$start_time} and start_time<={$end_time} and start_state={$state}) or (end_time>={$start_time} and end_time<={$end_time} and end_state={$state})) and info_state = 1";
		g('pkg_db') -> exec($update_sql);
	}

}

// end of file