<?php
/**
 * 班次服务类
 * @author chenyihao
 * @date 2016-11-29
 */
namespace model\api\server\workshift;

class Shift extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/** 固定排班 */
	private static $Type_fix = 1;
	/** 自由排班 */
	private static $Type_plan = 2;

	/** 排班表 */
	private static $Shift_Table = 'ws_workshift_shift';

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	/** 获取快捷选择时间段 */
	public function get_quest_time($com_id, $user_id, $start_date){
		$now_date = date("Ymd");
		// if($now_date >= $start_date){
		// 	$time_list = $this -> get_shift_time($user_id, $start_date);
		// }else{
			$workshift = g('ws_workshift') -> get_workshift_by_user_id($com_id, $user_id, false);
			if($workshift['type'] == 1){
				$time_list = $this -> get_fix_shift_time($workshift['id'], $start_date);
			}else{
				$time_list = $this -> get_shift_time($user_id, $start_date);
			}
		// }
		return $time_list;
	}

	/** 根据时间获取班次 */
	public function get_shift_by_time($com_id, $user_id, $time, $fields="id, start_time, end_time"){
		$cond = array(
				'com_id=' => $com_id,
				'user_id=' => $user_id,
				'info_state=' => self::$InfoOn,
				'__OR' => array(
						"start_time="  => $time,
						"end_time="  => $time,
					)
			);
		$shift = $this -> _get_by_cond(self::$Shift_Table, $cond, $fields);
		if($shift['start_time'] == $time){
			$shift['type'] = 1;
		}else if($shift['end_time'] == $time){
			$shift['type'] = 2;
		}
		return $shift;
	}

	/** 根据日期范围获取指定员工的班次信息 */
	public function get_shift_by_date($user_ids, $type=0, $start_date, $end_date, $fields='*'){
		$cond = array(
				'user_id IN' => $user_ids,
				'date>=' => $start_date,
				'date<=' => $end_date,
				'info_state=' => self::$InfoOn,
			);
		if(!empty($type)) $cond['shift_type='] = $type;
		return $this -> _list_by_cond(self::$Shift_Table, $cond, $fields);
	}


	/** 获取班次的使用信息 */
	public function get_shift_by_shift($com_id, $shift_ids, $type=NULL, $fields='*'){
		if (empty($shift_ids)) return array();
		
		$cond = array(
				'com_id=' => $com_id,
				'info_state=' => self::$InfoOn
			);
		if (count($shift_ids) == 1) {
			$cond['shift_id='] = $shift_ids[0];
		} else {
			$cond['shift_id IN'] = $shift_ids;
		}
		$type != 1 && $type != 2 && $type = NULL; 
		!is_null($type) && $cond['shift_type='] = intval($type);

		return $this -> _list_by_cond(self::$Shift_Table, $cond, $fields);
	}

	/** 通过班次ID获取班次信息 */
	public function get_by_id($id, $fields){
		$cond = array(
				'id =' => $id,
				'info_state=' => self::$InfoOn
			);
		$ret = $this -> _get_by_cond(self::$Shift_Table, $cond, $fields);
		return $ret;
	}

	/** 获取缺卡的时间点 */
	public function get_noclock_shift($com_id, $user_id, $start_date, $end_date, $fields="*"){
		$cond = array(
				'com_id=' => $com_id,
				'user_id=' => $user_id,
				'date>=' => $start_date,
				'date<=' => $end_date,
				"__OR" => array(
						"start_clock_time=" => 0,
						"end_clock_time=" => 0,
					),
				'info_state=' => self::$InfoOn
			);
		return  $this -> _list_by_cond(self::$Shift_Table, $cond, $fields);
	}

//---------------------------------------内部实现-----------------------------------
	/** 获取班次里面最近一天的上班时间 */
	private function get_shift_time($user_id, $start_date){
		$sql = "select date, CAST(GROUP_CONCAT(time separator '^') as char) time from ( select id, date, CONCAT(start_time, '-', end_time) time from ws_workshift_shift where user_id = 81873 and info_state = 1 and date >= {$start_date} ORDER BY date asc ,start_time asc ) t GROUP BY date  LIMIT 2";
		$data = g("pkg_db") -> query($sql);
		$time_list = array();
		foreach ($data as $date) {
			if($date['date'] > $start_date){
				$time = explode("^", $date['time']);
				$date = date("Y-m-d", strtotime($date['date']));
				$start_time = 9999999999;
				$end_time = 0;
				foreach ($time as $t) {
					$s_e_time = explode('-', $t);
					$s_e_time[0] < $start_time && $start_time = $s_e_time[0];
					$s_e_time[1] > $end_time && $end_time = $s_e_time[1];
				}

				if($start_time != 9999999999 && !empty($end_time)){
					$time_list[] = array(
							'start_time' => $start_time,
							'end_time' => $end_time,
							'date' => $date
						);
				}
				break;
			}else{
				$time = explode("^", $date['time']);
				foreach ($time as $t) {
					$s_e_time = explode('-', $t);
					if(count($s_e_time) == 2){
						$time_list[] = array(
							'start_time' => $s_e_time[0],
							'end_time' => $s_e_time[1],
						);
					}
				}
			}
		}unset($date);
		return $time_list;
	}

	/** 获取固定排班的上班时间 */
	private function get_fix_shift_time($workshift_id, $start_date){
		$shift_list = g('ws_workshift') -> get_shift_fix($workshift_id, 'start_time, end_time, rest_start_time, rest_end_time, week_day');
		if(empty($shift_list)) return array();

		$workshift_date = g('ws_core') -> get_workshift_date($workshift_id);
		$shift_info = array();
		foreach ($shift_list as $shift) {
			if(!isset($shift_info[$shift['week_day']])){
				$shift_info[$shift['week_day']] = array();
			}
			$shift_info[$shift['week_day']][] = $shift;
		}unset($shift);

		$time_list = array();
		$tmp_date = $start_date;
		while ($tmp_date) {
			$week_day = date("w", strtotime($tmp_date));
			if(isset($workshift_date[$tmp_date]) && $workshift_date[$tmp_date] == 1){
				//假期内
				$tmp_date++;
				continue;
			}
			if(!isset($shift_info[$week_day]) && !isset($workshift_date[$tmp_date])){
				//放假内
				$tmp_date++;
				continue;
			}

			if(!isset($shift_info[$week_day])){
				//补班
				$shift = array_shift($shift_info);
			}else{
				$shift = $shift_info[$week_day];
			}
			if($tmp_date != $start_date){
				$start_time = 9999999999;
				$end_time = 0;
				foreach ($shift as $s){
					$shift_start_time = strtotime($tmp_date ." ". $s['start_time']);
					$shift_end_time = strtotime($tmp_date ." ". $s['end_time']);
					if($shift_end_time < $shift_start_time){
						$shift_end_time = strtotime("+1 day", $shift_end_time);
					}
					if($shift_start_time < $start_time) $start_time = $shift_start_time;
					if($shift_end_time > $end_time) $end_time = $shift_end_time;
				}unset($s);
				$date = date("Y-m-d", strtotime($tmp_date));
				$time_list[] = array("start_time" => $start_time, "end_time" => $end_time, "date" => $date);
				break;
			}else{
				$today_time =array();
				$start_time = 9999999999;
				$end_time = 0;
				foreach ($shift as $s){
					if(!empty($s['rest_start_time'])){
						$shift_start_time = strtotime($tmp_date ." ". $s['start_time']);
						$shift_rest_start_time = strtotime($tmp_date ." ". $s['rest_start_time']);
						$shift_end_time = strtotime($tmp_date ." ". $s['end_time']);
						$shift_rest_end_time = strtotime($tmp_date ." ". $s['rest_end_time']);
						$today_time[] = array("start_time" => $shift_start_time, "end_time" => $shift_rest_start_time);
						$today_time[] = array("start_time" => $shift_rest_end_time, "end_time" => $shift_end_time);
					}else{
						$shift_start_time = strtotime($tmp_date ." ". $s['start_time']);
						$shift_end_time = strtotime($tmp_date ." ". $s['end_time']);
						if($shift_end_time < $shift_start_time){
							$shift_end_time = strtotime("+1 day", $shift_end_time);
						}
						$today_time[] = array("start_time" => $shift_start_time, "end_time" => $shift_end_time);
					}
					if($shift_start_time < $start_time) $start_time = $shift_start_time;
					if($shift_end_time > $end_time) $end_time = $shift_end_time;
				}unset($s);
				if(count($today_time) > 1){
					array_unshift($today_time, array("start_time" => $start_time, "end_time"=>$end_time, "date"=>date("Y-m-d", $start_time)));
				}
				$time_list = array_merge($today_time, $time_list);
				$tmp_date++;
			}

		}
		return $time_list;
	}

}

// end of file