<?php
/**
 * 考勤服务类
 * @author chenyihao
 * @date 2016-10-09
 */
namespace model\api\server\workshift;

class Checkwork extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/** 状态启用 */
	private static $StateOn = 1;
	/** 状态禁用 */
	private static $StateOff = 2;

	/** 班次表 */
	private static $Shift_Table = 'ws_workshift_shift';

	/** 补录表 */
	private static $Makeup_Table = 'ws_checkwork_makeup';

	/** 考勤记录表 */
	private static $Checkwork_Record_Table = 'ws_checkwork_record';

	/** 请假外出表 */
	private static $Absence_Table = 'ws_absence_date';

	/** 请假外出表 */
	private static $Rest_Record_Table = 'ws_rest_record';	

	/** 员工表 */
	private static $User_Table = 'sc_user';	

	/** 员工部门表 */
	private static $Dept_Table = 'sc_dept';	

	private static $Addr_Table = 'ws_checkwork_addr';

	private static $Config_Table = 'ws_checkwork_config';

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	/** 通过时间获取补录记录 */
	public function get_makeup_by_time($com_id, $user_id, $time, $fields = 'm.*, f.state'){
		$cond = array(
				'm.com_id=' => $com_id,
				'm.type=' => 2,	//补录类型 表单补录
				'm.makeup_id=' => $user_id,
				'm.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
				
		$table = self::$Makeup_Table." m inner join mv_proc_formsetinst f on f.id = m.formsetinst_id ";
			if(!empty($time)){
				if(is_array($time)){
					$cond['m.makeup_time IN'] = $time;
					return $this -> _list_by_cond($table, $cond, $fields);
				}else{
					$cond['m.makeup_time='] = $time;
					return $this -> _get_by_cond($table, $cond, $fields);
				}
			}
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	public function get_makeup_by_formsetinst($com_id, $formsetinst_id, $fields = 'm.*, f.state'){
		$cond = array(
				'm.com_id=' => $com_id, 
				'm.formsetinst_id=' => $formsetinst_id,
				'm.info_state=' => self::$InfoOn,
				'f.info_state=' => self::$InfoOn,
				"f.state!=" => 0
			);
		$table = self::$Makeup_Table." m inner join mv_proc_formsetinst f on f.id = m.formsetinst_id ";
		return $this -> _get_by_cond($table, $cond, $fields);
	}

	/** 保存补录记录 */
	public function save_makeup_record($com_id, $user_id, $formsetinst_id, $makeup_type, $makeup_time, $shift_id){
		$data = array(
				'com_id' => $com_id,
				'type' => 2,
				'makeup_id' => $user_id,
				'formsetinst_id' => $formsetinst_id,
				'makeup_type' => $makeup_type,
				'makeup_time' => $makeup_time,
				'shift_id' => $shift_id,
				'create_time' => time(),
				'update_time' => time(),
				'info_state' => self::$InfoOn
			);
		return g('pkg_db') -> insert(self::$Makeup_Table, $data);
	}

	public function update_makeup_record($formsetinst_id, $makeup_type, $makeup_time, $shift_id){
		$cond = array(
				'formsetinst_id=' => $formsetinst_id,
			);
		$data = array(
				'makeup_type' => $makeup_type,
				'makeup_time' => $makeup_time,
				'shift_id' => $shift_id,
				'update_time' => time()
			);
		return g('pkg_db') -> update(self::$Makeup_Table, $cond, $data);
	}

	/** 根据班次id获取 */
	public function get_makeup_by_shift($shift_list, $fields="m.*, f.state", $type=null){
		$cond = array(
				'm.shift_id IN' => $shift_list,
				'm.info_state=' => self::$InfoOn,
				'f.state!=' => 0
			);
		!is_null($type) && $cond['m.type='] = $type;
		$table = self::$Makeup_Table . " m inner join mv_proc_formsetinst f on f.id = m.formsetinst_id ";
		return $this -> _list_by_cond($table, $cond, $fields);
	}

	/** 打卡 */
	public function clock($user_id, $shift_id, $type){
		$shift = g('ws_shift') -> get_by_id($shift_id, '*');
		if(empty($shift)){
			throw new \Exception('班次不存在');
		}

		//判断该班次是否有请假记录
		$absence_list = g('ws_core') -> get_absence_by_date($user_id, $shift['date'], "start_time, end_time");
		if(!empty($absence_list)){
			$start_time = $shift['start_time'];
			$rest_start_time = $shift['rest_start_time'];
			$end_time = $shift['end_time'];
			$rest_end_time = $shift['rest_end_time'];
			foreach ($absence_list as $absence) {
				if($start_time >= $absence['start_time'] && $start_time <= $absence['end_time']){
					//记录包含班次开始时间
					$start_time = $absence['end_time'];
					if(!empty($rest_start_time) && !empty($rest_end_time) && $absence['end_time'] > $rest_start_time && $absence['end_time'] < $rest_end_time){
						$start_time = $rest_end_time;
						$rest_start_time = 0;
						$rest_end_time = 0;
					}
				}else if($end_time >= $absence['start_time'] && $end_time <= $absence['end_time']){
					//记录包含班次结束时间
					$end_time = $absence['start_time'];
					if(!empty($rest_start_time) && !empty($rest_end_time) && $absence['start_time'] > $rest_start_time && $absence['start_time'] < $rest_end_time){
						$end_time = $rest_start_time;
						$rest_start_time = 0;
						$rest_end_time = 0;
					}
				}
				if($start_time >= $end_time){
					break;
				}
			}unset($absence);

			if($start_time < $end_time){
				if($start_time != $shift['start_time']){
					$shift['start_time'] = $start_time;
				}
				if($end_time != $shift['end_time']){
					$shift['end_time'] = $end_time;
				}
				$shift["rest_start_time"] = $rest_start_time;
				$shift["rest_end_time"] = $rest_end_time;
			}
		}

		$now = time();
		$data = array();
		if($type == 1){
			//开始时间打卡
			$data['start_clock_time'] = time();
			$data['start_state'] = 1;
			if($now > $shift['rest_start_time']){
				if($now > $shift['rest_end_time']){
					$data['late_time'] = $now - $shift['rest_end_time'] + $shift['rest_start_time'] - $shift['start_time'];
				}else{
					$data['late_time'] = $shift['rest_start_time'] - $shift['start_time'];
				}
			}else{
				$data['late_time'] = $now - $shift['start_time'];
			}
			if(!empty($shift['end_clock_time'])){
				$start = max($data['start_clock_time'], $shift['start_time']);
				$end = min($shift['end_clock_time'], $shift['end_time']);
				$hours = round(($end - $start - $shift['rest_end_time'] + $shift['rest_start_time'])/60/60, 3);
				$hours < 0 && $hours = 0;
				$data['hours'] = $hours;
			}
		}else if($type == 2){
			//结束时间打卡
			$data['end_clock_time'] = time();
			$data['end_state'] = 1;
			if($now < $shift['rest_end_time']){
				if($now < $shift['rest_start_time']){
					$data['early_time'] = $shift['rest_start_time'] - $now + $shift['end_time'] - $shift['rest_end_time'];
				}else{
					$data['early_time'] = $shift['end_time'] - $shift['rest_end_time'];
				}
			}else{
				$data['early_time'] = $shift['end_time'] - $now;
			}
			if(!empty($shift['start_clock_time'])){
				$start = max($shift['start_clock_time'], $shift['start_time']);
				$end = min($data['end_clock_time'], $shift['end_time']);
				$hours = round(($end - $start - $shift['rest_end_time'] + $shift['rest_start_time'])/60/60, 3);
				$hours < 0 && $hours = 0;
				$data['hours'] = $hours;
			}
		}else{
			return false;
		}
		$cond = array(
				'id=' => $shift_id,
				'user_id=' => $user_id
			);
		return g('pkg_db') -> update(self::$Shift_Table, $cond, $data);
	}

	/** 保存打卡记录 */
	public function save_checkwork_record($com_id, $user_id, $shift_id, $addr_id, $addr_name, $scope, $lng, $lat){
		$data = array(
				'com_id' => $com_id, 
				'user_id' => $user_id,
				'clock_time' => time(),
				'lng' => $lng,
				'lat' => $lat,
				'clock_scope' => $scope,
				'addr_id' => $addr_id,
				'addr_name' => $addr_name,
				'date' => date("Ymd"),
				'create_time' => time(),
			);
		return g('pkg_db') -> insert(self::$Checkwork_Record_Table, $data);
	}

	/** 补录 */
	/** admin_id 补录管理员， user_id 被补录人id, shift_id 被补录班次ID， type 补录类型，1：开始打卡，2：结束打卡 */
	public function makeup($com_id, $shift_id, $time){
		$shift = g('api_ser_shift') -> get_by_id($shift_id, '*');
		if($shift['com_id'] != $com_id){
			throw new \Exception('没有权限');
		}
		$start_time = date('H:i', $shift['start_time']);
		$end_time = date('H:i', $shift['end_time']);
		$data = array();
		$type = 0;
		if($start_time == $time){
			//补录开始时间
			$type = 1;
			if(!empty($shift['start_clock_time'])) throw new \Exception('该记录已经补录');
			$data['start_clock_time'] = $shift['start_time'];
			$data['start_state'] = 2;
			if(!empty($shift['end_clock_time'])){
				$data['hours'] = round((min($shift['end_clock_time'], $shift['end_clock_time']) - $shift['start_time'] - $shift['rest_end_time'] + $shift['rest_start_time'])/60/60, 3);
			}
		}else if($end_time == $time){
			//补录开始时间
			$type = 2;
			if(!empty($shift['end_clock_time'])) throw new \Exception('该记录已经补录');
			$data['end_clock_time'] = $shift['end_time'];
			$data['end_state'] = 2;
			if(!empty($shift['start_clock_time'])){
				$data['hours'] = round(($shift['end_time'] - max($shift['start_clock_time'], $shift['start_time']) - $shift['rest_end_time'] + $shift['rest_start_time'])/60/60, 3);
			}
		}else{
			//补录错误
			throw new \Exception('操作错误');
		}

		if(!empty($data)){
			$cond = array(
					'id=' => $shift_id,
				);
			$ret = g('pkg_db') -> update(self::$Shift_Table, $cond, $data);
			if(!$ret) throw new \Exception('补录失败');
		}
		return $type;
	}

	public function wx_get_checkwork_count($com_id, $user_id, $start_date, $end_date){
		$fields = "s.date,s.user_id, cast(GROUP_CONCAT(concat(s.id, ':', s.start_clock_time) separator '^') as char) as start_clock_time, cast(GROUP_CONCAT(concat(s.id, ':', s.end_clock_time) separator '^') as char) as end_clock_time, cast(GROUP_CONCAT(concat(s.id, ':', if(s.late_time<0, 0, s.late_time)) separator '^') as char) as late_time, cast(GROUP_CONCAT(concat(s.id, ':', if(s.early_time <0, 0, s.early_time)) separator '^') as char) as early_time , cast(GROUP_CONCAT(concat(d.record_id, ':', d.type) separator '^') as char) as type";
		$table = self::$Shift_Table.' s LEFT JOIN ws_absence_date d ON s.date = d.date and d.user_id = s.user_id and d.info_state='.self::$InfoOn;
		$user_cond = '';
		$user_cond = " s.user_id = {$user_id} and ";
		$other_cond = "";

		$info_state = self::$InfoOn;
		$sql = <<<EOF
		SELECT {$fields} 
		FROM {$table} 
		WHERE {$user_cond} s.com_id = {$com_id} and s.date >= {$start_date} and s.date <= {$end_date} and s.info_state = {$info_state} {$other_cond} 
		GROUP BY s.user_id, s.date
EOF;
	
		$data = g('pkg_db') -> query($sql);
		$date_data = array();

		$rest_time = array();
		$legwork_time = array();
		$start_no_clock_time = array();
		$end_no_clock_time = array();
		$late_time = array();
		$early_time = array();
		$clock_time = array();
		foreach ($data as $d) {
			$no_clock1 = $this -> get_state($d['start_clock_time'], $start_no_clock_time, TRUE);
			$no_clock2 = $this -> get_state($d['end_clock_time'], $end_no_clock_time, TRUE);
			$late_early1 = $this -> get_state($d['late_time'], $late_time, false);
			$late_early2 = $this -> get_state($d['early_time'], $early_time, false);

			$legwork_rest = $this -> get_other_state($d['type'], $legwork_time, $rest_time);

			$legwork = $legwork_rest['legwork'];
			$rest = $legwork_rest['rest'];

			if(empty($no_clock1) && empty($no_clock2) && empty($late_early1) && empty($late_early2) && empty($legwork) && empty($rest)){
				$clock_time[$d['date']] = 1;
				$clock = 1;
			}else{
				$clock = 0;
			}

			$date_data[$d['date']] = array(
					'date' => strtotime($d['date']),
					'clock' => $clock,
					'no_clock' => (empty($no_clock1) && empty($no_clock2)) ? 0 : 1,
					'late_early' => (empty($late_early1) && empty($late_early2)) ? 0 : 1,
					'rest' => $rest,
					'legwork' => $legwork
				);
		}unset($d);
		$count = array(
				'clock' => count($clock_time),
				'no_clock' => count($start_no_clock_time) + count($end_no_clock_time),
				'early' => count($early_time),
				'late' => count($late_time),
				'rest' => count($rest_time),
				'legwork' => count($legwork_time),
			);
		return array(
				'date_info' => array_values($date_data),
				'count' => $count
			);
	}


	/**
	 * 获取排班内考勤地址
	 * @param  [type] $com_id       
	 * @param  [type] $workshift_id 
	 * @param  string $fields       
	 * @return [type]               
	 */
	public function get_addr_by_workshift($com_id, $workshift_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'workshift_id=' => $workshift_id,
			'info_state=' => self::$InfoOn
			);

		$addrs = g('pkg_db') -> select(self::$Addr_Table, $fields, $cond);
		return $addrs ? $addrs : array();
	}

	/**
	 * 获取考勤设置基本信息
	 * @param  [type] $com_id 
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_config_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id
			);

		$ret = g('pkg_db') -> select(self::$Config_Table, $fields, $cond);
		if (!$ret) throw new \Exception('获取考勤设置信息失败');

		return $ret[0];
	}

	/** 删除补录记录 */
	public function del_record($com_id, $formsetinst_id) {
		if ($formsetinst_id < 1) return TRUE;

		//删除补录实例 回滚数据
		$this -> makeup_rollback($com_id, $formsetinst_id);

		$cond = array(
			'com_id=' => $com_id,
			'formsetinst_id=' => $formsetinst_id
			);

		$data = array(
			'info_state' => self::$InfoDelete
			);

		$ret = g('pkg_db') -> update(self::$Makeup_Table, $cond, $data);
		if (!$ret) throw new \Exception('删除补录记录失败');
		
		return $ret;
	}


//=------------------------------------------内部实现----------------------------------------------------

	/** 补录信息回滚 */
	private function makeup_rollback($com_id, $formsetinst_id){
		if(empty($formsetinst_id)) return;
		$cond = array(
				'c.com_id=' => $com_id,
				'c.formsetinst_id=' => $formsetinst_id,
				'f.state=' => 2
			);
		$table = self::$Makeup_Table.' c inner join mv_proc_formsetinst f on f.id = c.formsetinst_id ';
		$makeup = $this -> _get_by_cond($table, $cond, "c.makeup_time, c.shift_id, c.makeup_type");
		if(empty($makeup)) return;
		$cond = array('id=' => $makeup['shift_id']);
		$data = array();
		if($makeup['makeup_type'] == 1){
			$cond['start_state='] = 2;
			$data['start_state'] = 1;
			$data['start_clock_time'] = 0;
		}else{
			$cond['end_state='] = 2;
			$data['end_state'] = 1;
			$data['end_clock_time'] = 0;
		}
		$ret = g('pkg_db') -> update(self::$Shift_Table, $cond, $data);
	}

	/** 获取状态 */
	private function get_state($str, &$state_array, $empty = false){
		$str_array = explode("^", $str);
		$state = 0;
		foreach ($str_array as $str_a) {
			$array = explode(":", $str_a);
			if($empty){
				if(empty($array[1])){
					$state_array[$array[0]] = $v;
					$state = 1;
				}
			}else{
				if(!empty($array[1])){
					$state_array[$array[0]] = $v;	
					$state = 1;
				}
			}
		}unset($str_a);
		return $state;
	}

	public function get_other_state($str, &$legwork, &$rest){
		if(is_null($str) || empty($str)){
			return array("legwork" => 0, "rest" => 0);
		}
		$str_array = explode("^", $str);
		$legwork_state = 0;
		$rest_state = 0;
		foreach ($str_array as $str_a) {
			$array = explode(":", $str_a);
			if($array[1] == 1){
				//请假
				$rest_state = 1;
				$rest[$array[0]] = 1;
			}else if($array[1] == 2){
				//外出
				$legwork_state = 1;
				$legwork[$array[0]] = 1;
			}
		}unset($str_a);
		return array(
				'legwork' => $legwork_state,
				'rest' => $rest_state,
			);
	}

}

// end of file