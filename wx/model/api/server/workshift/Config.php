<?php
/**
 * 考勤管理基本配置
 * @author Luja
 * @date 2016-12-02
 */
namespace model\api\server\workshift;

class Config extends \model\api\server\ServerBase {

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	private static $Config_Table = 'ws_config';

	private static $Cwork_config_Table = 'ws_checkwork_config';

	private static $Legwork_config_Table = 'ws_legwork_config';

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}



	/**
	 * 获取考勤基本配置
	 * @param  [type] $com_id 
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_config_by_com($com_id, $fields='') {
		$table = self::$Config_Table .' wc,'. self::$Cwork_config_Table .' cc,'. self::$Legwork_config_Table .' lc';
		if (empty($fields)) {
			$fields = array(
				'wc.start_date', 'wc.work_hour', 'wc.clear_date', 
				'cc.remind_state', 'cc.earliest_time', 'cc.start_remind_time', 'cc.start_remind_content', 
				'cc.end_remind_time', 'cc.end_remind_content', 'lc.task_photo'
				);
			$fields = implode(',', $fields);
		}

		$cond = array(
			'wc.com_id=' => $com_id,
			'^cc.com_id=' => 'wc.com_id',
			'^lc.com_id=' => 'wc.com_id'
			);

		$ret = g('pkg_db') -> select($table, $fields, $cond);
		if (!$ret) throw new \Exception('查询考勤基本设置失败');
		
		return $ret[0];
	}


	/** 获取考勤管理基本配置信息 */
	public function get_ws_config($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id
			);

		$ret = g('pkg_db') -> select(self::$Config_Table, $fields, $cond, 1, 1);
		return $ret ? $ret[0] : FALSE;
	}

	/** 修改考勤管理基本配置信息 */
	public function update_ws_config($com_id, $info=array()) {
		$cond = array(
			'com_id=' => $com_id
			);

		$data = array(
			'start_date' => $info['start_date'],
			'work_hour' => $info['work_hour'],
			'clear_date' => $info['clear_date'],
			);

		$ret = g('pkg_db') -> update(self::$Config_Table, $cond, $data);

		return $ret;
	}


}

// end of file