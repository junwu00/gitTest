<?php
/**
 * 上传资源处理类
 *
 * @author LiangJianMing
 * @create 2015-06-22
 */
namespace model\api\server;
class Media extends \model\api\server\Base{
    //可上传的文件扩展名集合
    public $ext_list;
    
    //支持的文件扩展名
    public $file_ext = array(
        'txt', 'xml', 'pdf', 
        'rar', 'zip', 
        'doc', 'docx',
        'ppt', 'pptx', 
        'xls', 'xlsx',
    );
    //支持的图片扩展名
    public $image_ext = array(
        'jpeg', 'jpg', 'png',
    );
    //支持的视频扩展名
    public $video_ext = array(
        'rm', 'rmvb', 'wmv', 
        'avi', 'mpg', 'mpeg', 
        'mp4',
    );
    //支持的音频扩展名
    public $voice_ext = array(
        'mp3', 'wma', 'wav', 
        'amr',
    );

    //安卓下支持直接下载的文件列表
    public $android_download = array(
        'txt',  
        'docx', 
        'xls', 'xlsx',
        'ppt', 'pptx',
        'pdf',
    );

    //支持预览的文件扩展名
    public $preview_type = array(
        //返回直接下载地址
        'direct' => array(
            'txt', 'jpg', 'jpeg',
            'png', 'pdf',
        ),
        'other' => array(
            'doc', 'docx', 'ppt', 
            'pptx', 'xls', 'xlsx', 
        ),
    );

    // 这些文件会调用云服务进行预览
    public $remote_ext = array(
        'doc', 'docx', 'ppt',
        'pptx', 'xls', 'xlsx',
        'pdf', 'txt'
    );

    public function __construct($log_pre_str = '') {
        $this -> ext_list = array_merge($this -> file_ext, $this -> image_ext, $this -> video_ext, $this -> voice_ext);
        empty($log_pre_str) && $log_pre_str = "[Media]";
        parent::__construct($log_pre_str);
    }

    /**
     * 改写图片文件路径，使用全局定义的图片尺寸
     *
     * @access public
     * @param string $url 图片文件路径
     * @param string $image_idx 图片规格索引
     * @return string
     */
    public function rebuild_image_url($url, $image_idx) {
        $sizes = $GLOBALS['DEFAULT_IMG_SIZES'];
        !is_array($sizes) and $sizes = array();

        $size = isset($sizes[$image_idx]) ? $sizes[$image_idx] : '';
        if (empty($size)) return $url;

        $url = str_replace('/resize/', "/resize/{$size}/", $url);

        return $url;
    }

    /**
     * 本地分段断点续传（临时保存在data目录中）
     *
     * @access public
     * @param array $part_data 片段参数
     *          => string $hash 文件内容的哈希值
     *          => integer $offset 该片段在源文件中的开始位置
     *          => string $ext 文件扩展名，不带"."
     *          => string $part 文件的一个片段内容
     *          => boolean $is_end 是最后一个片段
     *          => string $app_name 应用英文名
     * @param array $max_size 最大值集合
     *          => integer $part 每个片段的最大值
     *          => integer $file 片段整合完成后的文件最大值
     * @param array $ext_list 可上传扩展名列表
     * @param array $cloud_param 云存储相关的参数
     *          => boolean $finish 默认为TRUE，即上传完成后，自动调用云存储接管文件
     *          => array $sizes 默认值为空，当文件类型为图片时，需要将图片处理为该尺寸集合的额外单位
     * @return array
     * @throws \Exception
     */
    public function part_upload(array &$data, array $max_size, array &$ext_list=array(), array $cloud_param=array()) {
        parent::log_i("开始上传文件片段，hash={$data['hash']}");
        empty($ext_list) and $ext_list = $this -> ext_list;
        if (strlen($data['part']) > $max_size['part']) {
            parent::log_w("单个片段大小超出限制，hash={$data['hash']}");
            throw new \Exception("单个片段大小超出限制", -1001);
        }

        if (!in_array($data['ext'], $ext_list)) {
            parent::log_w("扩展名不合法，hash={$data['hash']}");
            throw new \Exception("扩展名不合法", -1002);
        }

        !is_dir(SYSTEM_PART_DIR) and mkdir(SYSTEM_PART_DIR, 0755, TRUE);
        $file_name = $data['hash'] . '.' . $data['ext'];
        $file_path = SYSTEM_PART_DIR . $file_name;

        $ret = array(
            'status' => 0,
            'offset' => 0,
        );
        $server_offset = $this -> get_offset($file_path);
        if ($data['offset'] != $server_offset) {
            parent::log_i("单个片段上传完成，hash={$data['hash']}");
            $ret['offset'] = $server_offset;
            return $ret;
        }

        if ($data['part'] != '') {
            $status = $this -> save_part($file_path, $data['part']);
            if ($status !== 1) {
                if ($status === 0) {
                    parent::log_e("上传片段未知异常，hash={$data['hash']}");
                    throw new \Exception("系统异常", -1003);
                }elseif ($status === -1002) {
                    parent::log_n("其他用户正在上传，hash={$data['hash']}");
                    throw new \Exception("其他用户正在上传", -1004);
                }
            }
        }

        $server_offset = $this -> get_offset($file_path);
        if ($server_offset > $max_size['file']) {
            parent::log_w("文件大小超出限制，hash={$data['hash']}");
            throw new \Exception("文件大小超出限制", -1006);
        }
        if (!$data['is_end']) {
            parent::log_i("单个片段上传完成，hash={$data['hash']}");
            $ret['offset'] = $server_offset;
            return $ret;
        }

        $file_hash = md5_file($file_path);
        if ($file_hash != $data['hash']) {
            unlink($file_path);
            parent::log_w("片段整合完成，但文件已损坏，hash={$data['hash']}");
            throw new \Exception("文件已损坏", -1005);
        }

        empty($cloud_param['finish']) and $cloud_param['finish'] = TRUE;
        empty($cloud_param['sizes']) and $cloud_param['sizes'] = array();

        $ret['status'] = 1;

        try {
            $info = $this -> cloud_get_info($data['hash'], $data['ext']);
            $cloud_param['finish'] and $ret['info'] = $info;
            parent::log_i("片段整合完成，但云存储中已经存在相同的记录");

            unlink($file_path);

            return $ret;
        }catch(\Exception $e) {
            parent::log_w("片段整合完成，查询异常：" . $e -> getMessage());
        }

        parent::log_i("开始上传整合后的文件到云存储");

        if ($cloud_param['finish']) {
            $type = $this -> get_file_type($data['ext']);
            try {
                $info = $this -> cloud_upload($type, $file_path, $cloud_param['sizes']);
                parent::log_i("片段整合完成，并且上传到云存储成功");

                unlink($file_path);
                $ret['info'] = $info;
            }catch(\Exception $e) {
                parent::log_w("片段整合完成，但上传异常：" . $e -> getMessage());
            }
        }

        return $ret;
    }

    /**
     * 根据文件内容哈希，获取云存储中的文件信息
     *
     * @access public
     * @param string $file_hash 文件内容哈希
     * @param string $ext 文件扩展名
     * @return array
     * @throws \Exception
     */
    public function cloud_get_info($file_hash, $ext) {
        $search_hash = md5($file_hash . "." . $ext);

        try {
            $ret = get_info_by_search_hash($search_hash);
        }catch(\Exception $e) {
            throw $e;
        }

        return $ret;
    }

    /**
     * 根据云存储的保存路径来获取对应的文件内容哈希
     *
     * @access public
     * @param string $path 文件相对路径
     * @return array
     * @throws \Exception
     */
    public function get_info_by_path($path) {
        $path = trim($path);
        if (empty($path)) {
            throw new \Exception('路径不能为空');
        }

        $path = str_replace('/res/', '/resize/', $path);
        $path = str_replace('/ori/', '/origin/', $path);

        $url = MEDIA_URL_HOST . '/api/file/info/single';
        $suffix = MEDIA_URL_KEY;
        $param  = array(
            'caller' => MEDIA_CALLER,
            'path' => $path,
            'time' => time(),
        );

        $sign = get_sign($param, $suffix);
        $param['sign'] = $sign;
        
        $url = $url . '?' . http_build_query($param);
        $result = g('pkg_http') -> get($url, array(), 10);
        
        if (!is_array($result) || $result['httpcode'] != 200) throw new \Exception('网络异常');
        $content = json_decode($result['content'], TRUE);
        if ($content['status'] != 1) throw new \Exception($content['desc']);
        return $content['data'];
    }

    /**
     * 根据云存储的查询哈希来获取对应的文件内容哈希
     *
     * @access public
     * @param string $search_hash 云存储的查询哈希
     * @return array
     * @throws \Exception
     */
    public function get_info_by_search_hash($search_hash) {
        $url = MEDIA_URL_HOST . '/api/file/info/single';
        $suffix = MEDIA_URL_KEY;
        $param  = array(
            'caller' => MEDIA_CALLER,
            'hash' => $search_hash,
            'time' => time(),
        );

        $sign = get_sign($param, $suffix);
        $param['sign'] = $sign;
        
        $url = $url . '?' . http_build_query($param);
        $result = g('pkg_http') -> get($url, array(), 10);
        
        if (!is_array($result) || $result['httpcode'] != 200) throw new \Exception('网络异常');
        $content = json_decode($result['content'], TRUE);
        if ($content['status'] != 1) throw new \Exception($content['desc']);
        return $content['data'];
    }

    /**
     * 根据文件扩展名获取文件类型
     *
     * @access public
     * @param string $extname 文件扩展名
     * @return string
     */
    public function get_file_type($extname) {
        $type = '';
        $extname = strtolower($extname);
        $file_type = array(
            'file' => $this -> file_ext,
            'image' => $this -> image_ext,
            'video' => $this -> video_ext,
            'voice' => $this -> voice_ext,
        );
        foreach ($file_type as $key => $val) {
            if (in_array($extname, $val)) {
                $type = $key;
                BREAK;
            }
        }
        unset($val);

        return empty($type) ? 'file' : $type;
    }

    /**
     * 本地上传（data目录）
     *
     * @access public
     * @param array $file $_FILES数组中的单个元素
     * @param integer $max_size 单位：字节，缺省值为2Mb
     * @param array $ext_list 可自定义支持扩展名，若为空则使用缺省值
     * @return array
     * @throws \Exception
     */
    public function local_upload($file, $max_size=0, $ext_list=array()) {
        try {
            $type = $this -> verify_file($file, $max_size, $ext_list);
        }catch(\Exception $e) {
            throw $e;
        }

        $ext = preg_replace('/.*\.([^\.]*)$/', "$1", $file['name']);
        $hash = md5_file($file['tmp_name']);

        $dir_1 = substr($hash, 0, 2);
        $dir_2 = substr($hash, 2, 2);
        $relative_path = "{$type}/{$dir_1}/{$dir_2}/";
        $upload_path = MAIN_DATA . 'tmp' . DS.$relative_path;

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0755, TRUE);
        }
        $filename = $hash . '.' . $ext;
        $filepath = $upload_path . $filename;
        
        if (!file_exists($filepath)) {
            $destination = $upload_path . $filename;
            $location = $file['tmp_name'];
            move_uploaded_file($location, $destination);
        }
        
        $data = array(
            'hash' => $hash,
            'name' => $file['name'],
            'filesize' => $file['size'],
            'ext' => $ext,
            'type' => $type,
            'src' => MEDIA_SRC,
            'path' => str_replace(SYSTEM_UPLOAD_DIR, '', ($upload_path . $filename))
        );

        return $data;
    }

    /**
     * 上传到云存储服务
     *
     * @access public
     * @param string $type 文件类别
     * @param string $file $_FILES中的单个子元素的json字符串，或一个文件的绝对路径
     * @param array $sizes 当type是image有效，表示需要缩放的尺寸规格集合
     * @return array
     * status   integer     是   0-失败，1-成功
     * errcode  integer     是   错误码
     * errmsg   string      是   错误消息
     * data     array       否   自定义数据集合
     * hash     string      是   文件的hash
     * path     string      是   文件的相对保存路径
     * 
     */
    public function cloud_upload($type, $file, array $sizes=array(),$is_verify=true) {
    	self::_verify_capacity();
    	
        $max_size = 200 * 1024 * 1024;

        $url = MEDIA_URL_HOST . '/api/file/write/upload';
        $suffix = MEDIA_URL_KEY;
        $get_param  = array(
            'caller' => MEDIA_CALLER,
            'src'   => MEDIA_SRC,
            'time'  => time(),
        );

        if ($check = ((is_string($file) and is_json($file)) or is_array($file))) {
            if ($is_verify) {
                try {
                    $tmp_type = $this -> verify_file($file, $max_size);
                }catch(\Exception $e) {
                    throw $e;
                }
            }

            !is_array($file) and $file = json_decode($file, TRUE);
            $file_name = $file['name'];
            $file_path = $file['tmp_name'];
            
            $ext = preg_replace('/.*\.([^\.]*)$/', "$1", $file_name);
            $get_param['ext'] = $ext;
        }else {
            $file_path = $file;
            $file_name = basename($file_path);
        }

        $sign = get_sign($get_param, $suffix);
        $get_param['sign'] = $sign;
       
        $param['file'] = "@" . $file_path;
        $param['file_name'] = $file_name;
        $url .= "?" . http_build_query($get_param);
        $result = g('pkg_http') -> post($url, $param, 60);
    
        if (!is_array($result) || $result['httpcode'] != 200) throw new \Exception('网络异常');
        $content = json_decode($result['content'], TRUE);
        if ($content['status'] != 1) throw new \Exception($content['desc']);
        $ret = $content['data'];

        //预写入到缓存，供企业容量调用
        $cache_data = $ret;
        $cache_data['origin_file_name'] = $file_name;
        g('dao_capacity') -> set_capacity_cache($ret['hash'], $cache_data);
        g('dao_capacity') -> set_capacity_cache($ret['path'], $cache_data);

        $file_hash = isset($_REQUEST['file_hash'])?$_REQUEST['file_hash']:'';
        if($file_hash){
            $check_file_hash = md5(file_get_contents($file_path));
            $return_file_hash = isset($ret['file_hash'])?$ret['file_hash']:'';
            if( $file_hash!=$check_file_hash || ($return_file_hash && $return_file_hash!=$file_hash) ){
                throw new \Exception('文件数据不匹配');
            }
        }

        return $ret;
    }

    /**
     * 根据hash获取媒体的media_id
     *
     * @access public
     * @param integer $com_id 企业id
     * @param integer $combo 套件号
     * @param string $hash 文件哈希
     * @param string $token 令牌
     * @param string $name 自定义文件名
     * @param string $size 仅当type为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
     * @return array
     */
    public function get_media_id($com_id=0, $combo, $hash, $name, $size='') {
        empty($com_id) and $com_id = $_SESSION[SESSION_VISIT_COM_ID];

        $key_str = "com_id:{$com_id}:combo:{$combo}:hash:{$hash}:name:{$name}:{$size}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('pkg_redis') -> get($cache_key);

        if (!empty($ret)) {
            return json_decode($ret, TRUE);
        }

        $token = g('api_atoken') -> get_by_com($com_id, $combo);

        $url    = MEDIA_URL_HOST . '/api/wechat/push';
        $suffix = MEDIA_URL_KEY;
        $param  = array(
            'caller' => MEDIA_CALLER,
            'token' => $token,
            'hash'  => $hash,
            'filename' => $name,
            'time'  => time(),
        );

        $sign = get_sign($param, $suffix);
        $param['sign'] = $sign;
        
        $url = $url . '?' . http_build_query($param);
        $result = g('pkg_http') -> get($url, array(), 60);

        if (!is_array($result) || $result['httpcode'] != 200) throw new \Exception('网络异常');
        $content = json_decode($result['content'], TRUE);
        if ($content['status'] != 1) throw new \Exception($content['errmsg']);
        $ret = $content['data'];

        //缓存1小时
        g('pkg_redis') -> set($cache_key, json_encode($ret), 3600);
        return $ret;
    }

    /**
     * 推送资源到指定微信端
     *
     * @access public
     * @param string $type 微信方资源类型，允许取值：file、image、voice、video
     * @param string $media_id 推送到微信端的多媒体id
     * @param string $app_name 发起应用的英文名，如企业云盘：cabinet
     * @param array $user_list 需要发送的用户id（我方的）集合
     * @param array $dept_list 需要发送的部门id（我方的）集合
     * @param array $other 当文件类型是video时，需传入 array('title' => '标题', 'desc' => '简介');
     * @return boolean
     */
    public function rename_download($type, $media_id, $app_name, array $user_list=array(), array $dept_list=array(), array $other=array()) {
        g('api_nmsg') -> init($_SESSION[SESSION_VISIT_COM_ID], $app_name);

        g('api_nmsg') -> send_text('正在推送文件，请稍后...', $dept_list, $user_list);
        $ret = FALSE;
        if ($type == 'video') {
            $ret = g('api_nmsg') -> send_video($media_id, $other['title'], $other['desc'], $dept_list, $user_list);
        }else {
            $ret = g('api_nmsg') -> send_files($type, $media_id, $dept_list, $user_list);
        }

        return $ret;
    }

    /**
     * 可以让客户端重命名下载指定文件
     *
     * @access public
     * @param string $rename 重命名
     * @param string $hash 文件内容哈希
     * @param string $size 仅当type为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
     * @param boolean $jump 是否执行跳转
     * @return mixed
     */
    public function cloud_rename_download($rename, $hash, $size='', $jump=true) {
        $url    = CDN_MEDIA_URL_HOST . '/api/file/read/download';
        $suffix = MEDIA_URL_KEY;
        $param  = array(
            'caller' => MEDIA_CALLER,
            'name' => $rename,
            'hash'  => $hash,
            'position' => 1,
            'time'  => time(),
        );

        !empty($size) and $param['size'] = $size;

        $sign = get_sign($param, $suffix);
        $param['sign'] = $sign;

        $url .= '?' . http_build_query($param);

        if ($jump) {
            header('Location:' . $url);
            exit;
        }

        return $url;
    }

    /**
     * 可以让客户端重命名在线预览指定文件（需要浏览器原生支持）
     *
     * @access public
     * @param string $rename 重命名
     * @param string $hash 文件内容哈希
     * @param string $size 仅当type为图片时有效，代表图片尺寸，如：100x200
     * @param boolean $jump 是否执行跳转
     * @return mixed
     */
    public function cloud_rename_preview($rename, $hash, $size='', $jump=true) {
        $info = $this -> get_info_by_search_hash($hash);
        if (in_array(strtolower($info['ext']), $this -> remote_ext)) {
            $url = $this -> cloud_rename_download($rename, $hash, '', false);
            $real_param = array(
                'caller' => MEDIA_PREVIEW_CALLER,
                'src' => urlencode($url),
                'name' => $rename,
                'time' => time(),
            );

            $real_param['sign'] = get_sign($real_param, MEDIA_PREVIEW_KEY);
            $real_url = MEDIA_PREVIEW_PREFFIX . '?' . http_build_query($real_param);
            $url = $real_url;

        } else {
            $url    = CDN_MEDIA_URL_HOST . '/api/file/preview/view';
            $param  = array(
                'caller' => MEDIA_CALLER,
                'name' => $rename,
                'hash'  => $hash,
                'time'  => time(),
            );
            !empty($size) and $param['size'] = $size;
            $param['sign'] = get_sign($param, MEDIA_URL_KEY);
            $url .= '?' . http_build_query($param);
        }

        if ($jump) {
            header('Location:' . $url);
            exit;
        }

        return $url;
    }

    /**
     * 通知云存储下载微信media_id对应的文件
     *
     * @access public
     * @param string $access_token 微信企业号接口令牌
     * @param string $media_id 微信方定义的多媒体资源id
     * @param array $sizes 图片处理规格集合
     * @return array
     * status   integer     是   0-失败，1-成功
     * errcode  integer     是   错误码
     * errmsg   string      是   错误消息
     * data     array       否   自定义数据集合
     * hash     string      是   文件的hash
     * path     string      是   文件的相对保存路径
     * 
     */
    public function cloud_get_media($access_token, $media_id, array $sizes=array()) {
    	self::_verify_capacity();
    	
        $url    = MEDIA_URL_HOST . '/api/wechat/get';
        $suffix = MEDIA_URL_KEY;
        $param  = array(
            'caller' => MEDIA_CALLER,
            'token' => $access_token,
            'media_id' => $media_id,
            'time'  => time(),
        );

        $sign = get_sign($param, $suffix);
        $param['sign'] = $sign;

        $url .= "?" . http_build_query($param);
        $result = g('pkg_http') -> curl($url, array(), 60);
        
        if (!is_array($result) || $result['httpcode'] != 200) throw new \Exception('网络异常');
        $content = json_decode($result['content'], TRUE);
        if ($content['status'] != 1) throw new \Exception($content['desc']);
        $ret = $content['data'];

        //预写入到缓存，供企业容量调用
        $cache_data = $ret;
        g('dao_capacity') -> set_capacity_cache($ret['hash'], $cache_data);
        g('dao_capacity') -> set_capacity_cache($ret['path'], $cache_data);

        return $ret;
    }

    /**
     * 构造上传云服务的参数
     * @param unknown_type $max_size
     * @param unknown_type $ext_list
     * @throws \Exception
     */
    /**
     * 构造上传云服务的参数
     *
     * @access public
     * @param integer $max_size
     * @param array $max_size
     * @return boolean
     */
    public function cloud_build_params($max_size, $ext_list=NULL) {
        try {
            $file = array_shift($_FILES);
            if (empty($file) || !is_array($file)) {
                cls_resp::echo_err(cls_resp::$FileNotFound, '找不到该上传文件，请重新上传！');
            }
            
            $type = $this -> verify_file($file, $max_size, $ext_list);
            
            return array('type' => $type, 'file' => json_encode($file), 'name' => $file['name'], 'size' => $file['size']);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    //------------------------------------内部实现-------------------------------------------------
    /**
     * 验证上传文件的合法性
     *
     * @access public
     * @param array $file $_FILES数组中的单个元素
     * @param integer $max_size 单位：字节，缺省值为20Mb
     * @param array $ext_list 可自定义支持扩展名，若为空则使用缺省值 
     * @return string
     * @throws \Exception
     */
    private function verify_file($file, $max_size, $ext_list=NULL) {
        !is_array($file) and $file = json_decode($file, TRUE);
        !is_array($file) and $file = array();

        if (empty($file)) {
            throw new \Exception('未找到上传的文件');
        }

        if ($file['error'] > 0) {
            $errmsg = '';
            switch($file['error']) {  
                case 1:    $errmsg = '文件大小超出了服务器的空间大小 ';    break;   
                case 2:    $errmsg = '要上传的文件大小超出浏览器限制';     break;    
                case 3:    $errmsg = '文件仅部分被上传';                    break;    
                case 4:    $errmsg = '没有找到要上传的文件';              break;    
                case 5:    $errmsg = '服务器临时文件夹丢失';              break;    
                case 6:    $errmsg = '文件写入到临时文件夹出错';            break;    
                default:    $errmsg = '上传异常';
            }
            throw new \Exception($errmsg);
        }
        
        //允许的文件类型
        empty($ext_list) and $ext_list = $this -> ext_list;
        empty($max_size) and $max_size = 20 * 1024 * 1024;
        
        //空文件
        if ($file['size'] == 0) {
            throw new \Exception('空文件');
        }
        
        //大小超限
        if ($file['size'] > $max_size) {
            throw new \Exception('文件大小超限');
        }

        $info = pathinfo($file['name']);
        $ext = strtolower($info['extension']);

        //错误文件格式
        if (!in_array($ext, $ext_list)) {
            throw new \Exception('仅支持 '.implode(',', $ext_list).' 格式文件的上传');
        }

        $type = $this -> get_file_type($ext);
        return $type;
    }

    /**
     * 获取缓存变量名
     *
     * @access private
     * @param string $str 关键字符串
     * @return string
     */
    private function get_cache_key($str) {
        $key_str = MAIN_HOST . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
        $key = md5($key_str);
        return $key;
    }

    /** 
     * 获取指定文件的上传偏移量
     * 
     * @access private
     * @param string $file 上传的临时文件路径
     * @return integer
     */
    private function get_offset($file) {
        $offset = 0;
        clearstatcache();
        if (file_exists($file)) {
            $offset = filesize($file);
        }
        return $offset;
    }

    /**
     * 保存文件片段
     * 
     * @access private
     * @param string $file 上传的临时文件路径
     * @param string $part 文件片段内容
     * @return integer
     */
    private function save_part($file, $part){
        $fp = fopen($file, 'a+');
        if (!$fp) {
            //服务器异常
            return 0;
        }
        
        if (!flock($fp, LOCK_EX | LOCK_NB)) {
            //别的用户正在上传
            fclose($fp);
            return 1002;
        }
        if (fwrite($fp, $part) === FALSE) {
            fclose($fp);
            return 0;
        }
        flock($fp, LOCK_UN);
        fclose($fp);

        //保存成功
        return 1;
    }
    
	/** 验证空间容量是否充足、允许上传 */
	private static function _verify_capacity() {
		$cap_info = g('dao_capacity') -> get_capacity_info();
		if ($cap_info['is_full']) {
			$forbit_time = $cap_info['notice_time'] + $cap_info['pre_buffer_time'] + $cap_info['buffer_time'];
			if ($cap_info['info_time'] >= $forbit_time) {
				throw new \Exception('您的企业服务器 <font color="#f74949">空间容量不足！</font><br>图片和文件上传功能暂时无法使用<br>请告知管理员扩容空间喔!');
			}
		}
	}
}

// end of file