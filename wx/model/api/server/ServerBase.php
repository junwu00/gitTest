<?php

namespace model\api\server;

abstract class ServerBase {

	protected $com_id = 0;
	
	/** 当前用户ID */
	protected $user_id = 0;
	/** 当前模块名称 */
	protected $mod_name;
	/** 当前模块中文名称 */
	protected $mod_cn_name;
	/** 当前模块对应的视图的根目录 */
	protected $view_dir;
	/** 当前模块对应的日志存放的根目录 */
	protected $log_dir;
	/** 当前模块对应的本地上传的根目录 */
	protected $upload_dir;

	/** 当前模块需记录的日志级别 */
	protected $log_lev;
	/** 当前模块对应的正常/警告日志的路径 */
	protected $log_file_path;
	/** 当前模块对应的错误日志的路径 */
	protected $err_file_path;
	
	/** 日志中一般要补充的前置的信息，如：[com:123][admin:123] */
	protected $log_pre_str;
	/** 缓存key前缀 */
	protected $cache_key_prefix;
	
	/**
	 * 构造方法
	 * 初始化模块内通用配置、变量等
	 * 
	 * $mod_name 			模块名
	 * $log_pre_str 		日志中一般要补充的前置的信息，如：[com:123][admin:123]
	 */ 
	public function __construct($mod_name, $log_pre_str='') {
		if (empty($log_pre_str) && isset($_SESSION[SESSION_VISIT_USER_ID])) {
			$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$log_pre_str = "[com_id:{$this -> com_id}][user_id:{$this -> user_id}]";
		}
		
		$mod_conf = load_config("model/{$mod_name}/model");
		
		$this -> log_pre_str = $log_pre_str;
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : 0;
		
		$this -> mod_name 		= $mod_conf['name'];
		$this -> mod_cn_name 	= $mod_conf['cn_name'];
		$this -> log_dir 		= $mod_conf['log_dir'];
		$this -> view_dir 		= $mod_conf['view_dir'];
		$this -> upload_dir		= $mod_conf['upload_dir'];
		
		$today = date('Ymd');
		$this -> log_lev 		= $mod_conf['log_lev'];
		$this -> log_file_path	= $this -> log_dir . DS . $today . '.log';
		$this -> err_file_path	= $this -> log_dir . DS . $today . '.error.log';
	}

    //上传=========================================================

    /**
     * 普通上传
     * @param array $watermark_conf 水印配置，为空数组则不加水印
     *
     *  参数示例
        $watermark_conf = array(
            'text_list' => array(
                array('val' => '移步到微'),
                array('val' => '广东省广州市天河区天平架XXX'),
                array('val' => '2016-12-07 19:12:34'),
            )
        );
     */
    public function normal_upload(array $watermark_conf=array()) {
        $file = array_shift($_FILES);
        if (empty($file) or !is_array($file)) {
            throw new \Exception('找不到该上传文件，请重新上传！');
        }

        $file_type = explode('/', $file['type']);
        if (empty($watermark_conf) || $file_type[0] != 'image') {
            //非图片类型不加水印
            $data = g('api_media') -> cloud_upload('', $file);

        } else {
            //加水印
            $file_name = $file['name'];
            $info = pathinfo($file_name);
            $watermark_conf['bg_path'] = $file['tmp_name'];
            $watermark_conf['bg_ext'] = strtolower($info['extension']);
            $ret_file = g('pkg_watermark') -> mark($watermark_conf);
            $upload_file = array(
                'name' => $file_name,
                'tmp_name' => $ret_file,
                'size' => filesize($ret_file),
            );
            $data = g('api_media') -> cloud_upload('', $upload_file);
            unlink($ret_file);  //删除本地图片

        }

        $data['image_path'] = MEDIA_URL_PREFFIX . $data['path'];
        return $data;
    }

    /**
     * JSSDK上传
     * @param array $watermark_conf 水印配置，为空数组则不加水印
     * 参数示例参参考上方的 normal_upload 方法
     */
    public function jssdk_upload($media_id, array $watermark_conf=array()) {
        $com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $token = g('api_atoken') -> get_by_com($com_id);
        if (empty($token)) {
            throw new \Exception('上传失败');
        }

        if (empty($watermark_conf)) {
            //不加水印
            $data = g('api_media') -> cloud_get_media($token, $media_id);
            return $data;

        } else {
            //加水印
            //1.获取图片并保存到本地
            $ret = g('wxqy_media') -> get_media($token, $media_id);
            $file_ext = explode('/', $ret['info']['content_type']);
            $file_ext = $file_ext[count($file_ext) - 1];
            $file_name = md5($ret['file']) . '.' . $file_ext;
            $file_path = MAIN_DATA . 'tmp' . DS . $file_name;
            if (!file_exists($file_path)) {
                file_put_contents($file_path, $ret['file']);
            }

            //加水印
            $watermark_conf['bg_path'] = $file_path;
            $watermark_conf['bg_ext'] = $file_ext;
            $ret_file = g('pkg_watermark') -> mark($watermark_conf);
            $upload_file = array(
                'name'      => $file_name,
                'tmp_name'  => $ret_file,
                'size'      => filesize($ret_file),
            );
            $data = g('api_media') -> cloud_upload('', $upload_file);
            unlink($file_path);
            unlink($ret_file);  //删除本地图片

            return $data;
        }

    }

    //END 上传=========================================================

	//分级日志记录：由配置文件中log_lev参数控制===========================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str) {
		if ($this -> log_lev > 0)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str) {
		if ($this -> log_lev > 1)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str) {
		if ($this -> log_lev > 2)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str) {
		if ($this -> log_lev > 3)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str) {
		if ($this -> log_lev > 4)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	//END 分级日志记录===========================================================================================

	/**
	 * 根据指定条件获取信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function _get_by_cond($table, array $cond, $fields='*') {
		if (empty($cond) || empty($table))	return FALSE;
		
		$ret = g('pkg_db') -> select($table, $fields, $cond, 1, 1);
		return $ret ? $ret[0] : FALSE;
	}

	/**
	 * 根据指定条件获取应用信息 
	 * @param array $cond			查询条件
	 * @param unknown_type $fields	查询字段
	 */
	protected function _list_by_cond($table, array $cond, $fields='*') {
		if (empty($cond) || empty($table))	return array();
		
		$ret = g('pkg_db') -> select($table, $fields, $cond);
		return $ret ? $ret : array();
	}
	
	/**
	 * 分页查询
	 * @param array $cond				查询条件
	 * @param unknown_type $fields		查询字段
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $order_by	排序
	 * @param unknown_type $with_cnt	是否返回总数，默认：FALSE
	 */
	protected function _list_by_page($table, array $cond, $fields, $page, $page_size, $order_by='', $with_cnt=FALSE) {
		if (empty($cond) || empty($table))	return array();
		
		$ret = g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, '', $order_by, $with_cnt);
		if ($with_cnt) {
			$ret = array(
                'data' 	=> $ret['data'] ? $ret['data'] : array(),
                'count' => $ret['count'],
            );
		} else {
			$ret = $ret ? $ret : array();
		}
		return $ret;
	}

}

//end