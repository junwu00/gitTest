<?php
/**
 * 新同步机制,替换原有获取所有部门和所有成员的方法
 *
 * @author ChenYihao
 * @create 2016-03-09
 */
namespace model\api\server;

class ServerMedia extends \model\api\server\ServerBase{

    public function __construct($log_pre_str='') {
        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);
    }

    /** @throws  */
    public function sendFile2Qywx($com_id,$user_ids,$filename,$tmp_name,$app_name="bpm")
    {
        // 文件上传到云端
        $fl = [
            'name' => $filename,
            'tmp_name' => $tmp_name,
        ];
        $fl = json_encode($fl);
        $serverMedia = new Media();
        $ret = $serverMedia->cloud_upload('', $fl,[],false);

        $file_key = $ret["hash"];
        $serverMedia = new Media();
        $media = $serverMedia -> get_media_id($com_id, 23,$file_key, $filename);

        if (empty($media)) {throw new \Exception('下载失败，文件不存在！');}

        $serverMedia -> rename_download('file', $media["media_id"],$app_name,$user_ids);
    }
}

/* End of this file */