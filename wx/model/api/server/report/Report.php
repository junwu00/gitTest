<?php
/**
 * 报表服务类
 * @author chenyihao
 * @date 2016-10-09
 */
namespace model\api\server\report;

class Report extends \model\api\server\ServerBase {	
	/** 报表配置 */
	private static $ReportConf = 'mv_proc_report_conf';
	/** 报表表格 */
	private static $ReportTable = 'mv_proc_report_table';
	/** 报表图 */
	private static $ReportChart = 'mv_proc_report_chart';
	/** 报表文件 */
	private static $ReportFile = 'mv_proc_report_file';

	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/** 状态启用 */
	private static $StateOn = 1;
	/** 状态禁用 */
	private static $StateOff = 2;

	/** 报表类型 表格报表 */
	private static $ReportTypeTable = 1;
	/** 报表类型 Excel报表 */
	private static $ReportTypeExcel = 2;

	/** 表格类型 数据表 */
	private static $TableData = 1;
	/** 表格类型 汇总表 */
	private static $TableSummary = 2;

	/** 汇总表汇总方式 */
	private static $CountType = array(
			1 => "SUM",
			2 => "AVG",
			3 => "MAX",
			4 => "MIN"
		);

	/** 数据表排序方式 */
	private static $OrderType = array(
			0 => 'ASC',
			1 => 'DESC'
		);

	/** 时间类型 */
	private static $TimeType = array(
			0 => "FROM_UNIXTIME(?, '%Y-%m-%d %H:%i')",	//年月日时分
			1 => "FROM_UNIXTIME(?, '%Y-%m-%d')",		//年月日
			2 => "FROM_UNIXTIME(?, '%Y-%m')",			//年月
			3 => "FROM_UNIXTIME(?, '%Y')",				//年
		);

	/** 数据总数缓存时间 */
	private static $Data_Time = 120;

	public function __construct($mod_name='', $log_pre_str='') {
		if(empty($mod_name)){
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	/** 获取报表内所有表格 */
	public function get_table_by_report($com_id, $report_id, $table_id, $with_chart = TRUE){
		if($with_chart){
			$fields = 't.id, t.name, t.type, t.row_list, t.form_ids, t.col_key, t.cond_list, t.join_list, t.order_list, t.other_list, c.id chart_id, c.name chart_name, c.chart_type, c.classify_key, c.value_key';
			$table = self::$ReportTable. ' t LEFT JOIN '.self::$ReportChart.' c ON t.id = c.table_id  and c.info_state='.self::$InfoOn;
			$cond = array(
					't.com_id=' => $com_id,
					't.report_id=' => $report_id,
					't.info_state=' => self::$InfoOn,
				);
			if(!empty($table_id)){
				$cond['t.id='] = $table_id;
			}
		}else{
			$fields = 'id, name, type, form_ids, row_list, col_key, cond_list, join_list, order_list, other_list';
			$table = self::$ReportTable;
			$cond = array(
					'com_id=' => $com_id, 
					'report_id=' => $report_id,
					'info_state=' => self::$InfoOn,
				);
			if(!empty($table_id)){
				$cond['id='] = $table_id;
			}
		} 
		$data = g('pkg_db') -> select($table, $fields, $cond);
		return $data;
	}

	/** 根据ID获取表格信息 */
	public function get_table_by_id($com_id, $table_id, $fields='*'){
		$cond = array(
				'com_id=' => $com_id,
				'id=' => $table_id,
				'info_state=' 	=> self::$StateOn
			);
		$ret =  g('pkg_db') -> select(self::$ReportTable, $fields, $cond, 1, 1);
		return empty($ret) ? array() : $ret[0];
	}

	/** 根据ID获取表格信息 */
	public function get_chart_by_id($com_id, $chart_id, $fields='*', $with_table = TRUE){
		if($with_table){
			$table = self::$ReportTable.' t LEFT JOIN '.self::$ReportChart.' c ON c.table_id = t.id ';
			$cond = array(
					'c.id=' => $chart_id,
					'c.com_id=' => $com_id,
					't.com_id=' => $com_id,
					'c.info_state=' => self::$InfoOn
				);
		}else{
			$table = self::$ReportChart;
			$cond = array(
					'com_id=' => $com_id,
					'id=' => $chart_id,
					'info_state=' 	=> self::$InfoOn
				);
		}
		$ret =  g('pkg_db') -> select($table, $fields, $cond, 1, 1);
		return empty($ret) ? array() : $ret[0];
	}

	/**
	 * 获取报表信息
	 * @param  [type] $com_id 
	 * @param  [type] $report_id 	报表id
	 * @param  [type] $fields
	 * @return [type]       
	 */
	public function get_report_by_id($com_id, $report_id, $fields='*') {
		$cond = array(
			'id=' 			=> $report_id,
			'com_id=' 		=> $com_id,
			// 'info_state=' 	=> self::$StateOn
			);
		$ret = g('pkg_db') -> select(self::$ReportConf, $fields, $cond, 1, 1);
		return $ret ? $ret[0] : FALSE;
	}

	/** 
	 * 根据配置条件返回数据
	 * 
	 * @param    $row_list	列表头
	 * @param    $col_key 	行表头
	 * @param    $cond_list  过滤条件
	 * @param    $join_list  关联条件
	 * @param    $order 排序条件
	 * @param    $other_cond 其他条件
	 * @param   &$count 是否需要汇总
	 * @param   &$sum 	是否需要返回总数据量
	 * @param   $start  开始
	 * @param   $page_size 分页大小
	 * @return 
	 */
	public function get_conf_data($row_list, $col_key, $cond_list, $join_list, $order, $other_cond, &$count=FALSE, &$sum=FALSE, $start=0, $page_size=0, $other_condition=[]){
		$user_key = array();
		$dept_key = array();
		$table_key = array();
        $school_key = array();
        $com_key = array();

		//提取创建人，子表单字段，部门字段，人员字段
		foreach ($row_list as $row) {
			if(strpos($row['input_key'], 'creater_id')){
				$user_key[] = $row['row_key'];
			}else if(strpos($row['input_key'], 'create_time')){
			}else if(strpos($row['input_key'], 'com_id')){
                $com_key[] = $row['row_key'];
            }else if(empty($col_key) && strpos($row['input_key'], '_') && $row['type'] == 0){
				$val_key = explode('_', $row['input_key']);
				if(isset($val_key[0]) && !empty($val_key[0])){
					if(!isset($table_key[$val_key[0]])){
						$table_key[$val_key[0]] = array();
					}
				}
				$table_key[$val_key[0]][] = $row['row_key'];
			}
			if($row['type'] == 'people' || $row['type'] == 'schoolDept' ){
				$user_key[] = $row['row_key'];
			}else if($row['type'] == 'dept'){
				$dept_key[] = $row['row_key'];
			}else if($row['type'] == 'school'){
                $school_key[] = $row['row_key'];
            }
		}unset($row);

		$data = $this -> new_get_table_data($row_list, $col_key, $cond_list, $join_list, $order, $other_cond, $count, $sum, $start, $page_size, $other_condition);
		if(!empty($data)){
			if(!empty($table_key)){
				$this -> change_table_val($data, $table_key);
			}
			if(!empty($user_key)){
				$this -> change_user_id($data, $user_key);
			}

			if(!empty($dept_key)){
				$this -> change_dept_id($data, $dept_key);
			}

            if(!empty($com_key)){
                $this -> change_com_id($data, $com_key);
            }

            if(!empty($school_key)){
                $this -> change_school_ids($data, $school_key);
            }


        }

		return $data;
	}

	/**
	 * 获取流程报表
	 * @param  [type]  $com_id
	 * @param  array   $cond
	 * @param  string  $fields
	 * @param  integer $page
	 * @param  integer $page_size
	 * @param  string  $order
	 * @param  boolean $with_count
	 * @return [type]
	 */
	public function get_report_list($com_id, $cond=array(), $fields='*', $page=1, $page_size=20, $order='', $with_count=FALSE) {
		$cond['com_id='] = $com_id;
		$cond['info_state='] = self::$StateOn;

		empty($order) && $order = ' ORDER BY id DESC ';
		$report_list = g('pkg_db') -> select(self::$ReportConf, $fields, $cond, $page, $page_size, '', $order, $with_count);
		return $report_list;
	}

//-----------------------------内部实现-------------------------------------

	/**
	 * @param  $row_list  	列表头数据
	 * @param  $col_key		行表头
	 * @param  $cond_list	过滤条件
	 * @param  $join_list	联表条件
	 * @param  $order 		排序条件
	 * @param  $other_cond	其他条件（提交人，提交时间的限制）
	 * @param  $page 		页码
	 * @param  $page_size 	页大小
	 * @return 返回结果
	 */
    private function new_get_table_data($row_list=array(), $col_key=array(), $cond_list=array(), $join_list=array(), $order=array(), $other_cond = array(), &$count, &$sum, $start=1, $page_size=5, $other_condition=[]){
        //所有的fields值
        $fields = array();
        //所有字段的key值
        $key_list = array(
            0 => array(),	//不汇总
            1 => array(),	//求和
            2 => array(),	//求平均
            3 => array(),	//求最大
            4 => array(),	//求最小
        );
        if(!empty($col_key)){
            //若存在分类，把分类放在第一个
            $key_list[0] = $col_key;
        }


        if($other_condition ){
            foreach ($other_condition as $o_cond){
                if( !empty($row_list[$o_cond['field_key']]) ){
                    $tmp_row = $row_list[$o_cond['field_key']];
                    $format = $tmp_row['format'];
                    $operator = '=';
                    isset($o_cond['operator']) && $operator = $o_cond['operator'];
                    in_array($tmp_row['type'],['dept','people','school','schoolDept']) && $operator='in';

                    if($tmp_row['type']=='date' || $tmp_row['type']=='text'){

                        $key_arr = explode(".", $tmp_row['input_key']);
                        if(count($key_arr)==2){
                            $form_id = str_replace('form','',$key_arr[0]);
                            //获取最新版本的内容
                            $input = g('mv_form')->getNewestInput($form_id,$key_arr[1]);
                            $other = json_decode($input['other'],true)?json_decode($input['other'],true):[];
                            $format = isset($other['format'])?$other['format']:'';
                        }else{
                            $format = '';
                        }
                    }

                    $cond_list[] = array(
                        'key'=>$tmp_row['input_key'],
                        'type'=>$tmp_row['type'],
                        'format'=>$format,
                        'operator'=>$operator,
                        'value'=>explode(',',$o_cond['field_val']),
                        'is_child'=>!empty($tmp_row['p_label_id'])?1:0,
                    );
                }
            }unset($o_cond);
        }


        //联表条件
        $join = array();
        //子表单汇总字段
        $count_table = array();
        //条件key
        $cond_key = array();
        //计算指标key
        $function_key = array();
        foreach ($row_list as $row) {
            if (!$row['isfunction']) {
                if (count($row['list_key'])>1){
                    $field1 = "IFNULL({$row['list_key'][0]}__{$row['count_type']},{$row['list_key'][1]}__{$row['count_type']})";
                    $field2 = "IFNULL({$row['list_key'][0]},{$row['list_key'][1]})";
                } else {
                    $field1 = "IFNULL({$row['input_key']}__{$row['count_type']},0)";
                    $field2 = "IFNULL({$row['input_key']},0)";
                }
            } else {
                $_field = $row['input_key'];
                foreach ($row['list_key'] as $item){
                    if(strpos($item,'input')!==false){
                        $_field = str_replace($item,"IFNULL({$item},0)",$_field);
                    }
                }
                $field1 = $_field;
                $field2 = $_field;
            }

            if($row['type'] == "create_time"){
                $k = array_shift($row['list_key']);
                $row['list_key'] = array("{$k}@{$row['time_type']}");
            }
            $key_list[$row['count_type']] = array_merge($key_list[$row['count_type']], $row['list_key']);

            $type = isset(self::$CountType[$row['count_type']]) ? self::$CountType[$row['count_type']] : '';
            $count_key[] = " {$type}(IF({$row['row_key']} IS NULL, 0, {$row['row_key']})) {$row['row_key']}";

            if($row['isfunction'] == 1){
                $function_key[$field2] = array(
                    "key" => $row['row_key'],
                    'key_list' => $row['list_key'],
                );
                $function_child = FALSE;
                foreach ($row['list_key'] as $key){
                    if(strpos($key, '_')){
                        $function_child = TRUE;
                        break;
                    }
                }unset($key);
                if($function_child && empty($col_key)){
                    //数据表子表单计算指标
                    $fields[] = " {$row['row_key']} ";
                }else{
                    $value = $field2;
                    $row['list_key'] = array_unique($row['list_key']);
                    foreach ($row['list_key'] as $key) {
                        if(strpos($key, "input")){
                            $value = str_replace($key, $key."__".$row['count_type'], $value);
                        }
                    }unset($key);
                    $fields[] = " {$type}({$value}) as {$row['row_key']} ";
                }
                continue;
            }

            if(strpos($row['input_key'], '_') && $row['count_type'] != 0){
                //若统计子表单字段数据
                $val_list = explode('.', $field2);
                $count_table[$val_list[1]] = $row['count_type'];
                if($row['count_type'] == 2){
                    //统计子表单字段平均数
                    $fields[] = "convert((SUM({$field1})/SUM({$field1})+0), decimal) as {$row['row_key']}";
                    continue;
                }
            }

            if($type == 'MAX' || $type == 'MIN'){
                $fields[] = "{$type}(if({$field1}+0= 9999999999 OR {$field1}+0= -9999999999, 0, {$field1}+0)) as {$row['row_key']}";
            }else if($type == 'SUM'){
                $fields[] = "SUM({$field1}+0) as {$row['row_key']}";
            }else if($type == 'AVG'){
                $fields[] = "convert(AVG({$field1}+0), decimal) as {$row['row_key']}";
            }else{
                if($this->isPrivateInput($row['input_key'])){
                    $fields[] = "{$type}({$field1}) as {$row['row_key']}";
                }else{
                    $fields[] = "{$type}({$field2}) as {$row['row_key']}";
                }
            }
        }unset($row);

        //汇总表提交人提交时间
        if($join_list){
            $tmp_data1 = explode('.',$join_list[0]['key_list'][0]);
            $tmp_data2 = explode('.',$join_list[0]['key_list'][1]);
            $tmp_cond_list = $cond_list;
            foreach ($tmp_cond_list as $tmp_c){
                if(empty($tmp_c['operator']))
                    continue ;
                if($tmp_c['type']=='create_time' || $tmp_c['type']=='creater_id'){
                    $tmp_data = explode('.',$tmp_c['key']);
                    if(count($tmp_data)==2 && count($tmp_data1)==2 && count($tmp_data2)==2){
                        if($tmp_data[0] == $tmp_data1[0]){
                            $tmp_c["key"] = $tmp_data2[0].'.'.$tmp_data[1];
                            $cond_list[] = $tmp_c;
                        }else{
                            $tmp_c["key"] = $tmp_data1[0].'.'.$tmp_data[1];
                            $cond_list[] = $tmp_c;
                        }
                    }
                }
            }unset($tmp_c);
        }

        foreach ($cond_list as $c) {
            $key_list[0][]  = $c['key'];
        }unset($c);

        foreach ($join_list as $j) {
            $key_list[0] = array_merge($key_list[0], $j['key_list']);
            foreach ($j['key_list'] as $key) {
                if($this->isPrivateInput($key)){
                    $j['value'] = str_replace($key, $key."__0", $j['value']);
                }
            }unset($key);
            $join[] = $j['value'];
        }

        //拆分条件
        $ret = $this -> compose_cond($cond_list);

        //表单条件
        $cond = $ret['cond_list'];
        //子表单条件
        $child_cond = $ret['child_cond'];

        $form = $this -> get_form_fields($key_list, $col_key, $function_key);

        $fields = implode(',', $fields);

        if(count($form) == 1){
            //单表
            $form = array_shift($form);
            $tmp_child_cond = isset($child_cond[$form['key']]) ? $child_cond[$form['key']] : array();
            $tmp_cond = isset($cond[$form['key']]) ? $cond[$form['key']] : array();
            $sql = $this -> get_simple_table_sql($form, $other_cond, $tmp_child_cond, $tmp_cond);
            $default_order_key = $form['key'].".formsetinst_id";
        }else if(count($form) == 2){
            if(empty($col_key)) return array();
            //双表
            $sql = array();
            foreach ($form as $f) {
                $tmp_child_cond = isset($child_cond[$f['key']]) ? $child_cond[$f['key']] : array();
                $tmp_cond = isset($cond[$f['key']]) ? $cond[$f['key']] : array();
                $sql[] = $this -> get_simple_table_sql($f, $other_cond, $tmp_child_cond, $tmp_cond,true);
            }unset($f);

            foreach ($col_key as &$key) {
                if($this->isPrivateInput($key)){
                    $key .= "__0";
                }
            }unset($key);
            $col_str = implode(",", $col_key);

            $t1 = $sql[0];
            $t2 = $sql[1];
            $join_str = implode(' and ', $join);

            $sql1 = " {$t1} LEFT JOIN   {$t2} ON {$join_str} GROUP BY {$col_str} ";
            $sql2 = " {$t1} RIGHT JOIN   {$t2} ON {$join_str} GROUP BY {$col_str} ";
            $sql = " {$sql1} UNION SELECT {$fields} FROM $sql2";

            $key_form = current($form);
            $default_order_key = $key_form['key'].".formsetinst_id";
        }else{
            throw new \Exception('查询错误');
        }

        // $cond = implode(' AND ', $cond);
        if($page_size != 0){
            empty($start) && $start = 0;
            $limit = " LIMIT {$start},{$page_size} ";
        }else{
            $limit = '';
        }
        $select_sql = "SELECT {$fields} FROM {$sql} ";

        // if(!empty($cond)){
        // 	$select_sql .= " WHERE {$cond} ";
        // }

        if(!empty($col_key)){
            if (count($form) != 2) {
                foreach ($col_key as &$key) {
                    if($this->isPrivateInput($key)){
                        $key .= "__0";
                    }
                }unset($key);
                $col_str = implode(",", $col_key);
                $select_sql .= " GROUP BY {$col_str}";
            }
            if(!empty($order)){
                $order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
                $select_sql = "SELECT * FROM ({$select_sql}) t ";
                $select_sql .= " ORDER BY {$order['key']} {$order_type}";
            }
        }else{
            if(!empty($order)){
                $order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
                $select_sql .= " ORDER BY {$order['key']} {$order_type}";
            }else{
                $select_sql .= " ORDER BY {$default_order_key} desc";
            }
        }

        //通过表头字段进一步进行筛选
        if($other_condition){
            $other_where = $this->getOtherWhere($other_condition,$row_list);
            $select_sql = "select * from ($select_sql) t where {$other_where}";
        }

        if(!empty($limit)){
            $sql = $select_sql . $limit;
        }else{
            $sql = $select_sql;
        }

        $data = g('pkg_db') -> query($sql);

        if(!empty($data)){
            if($sum){
                $sum = $this -> get_sum($select_sql);
            }
            if($count){
                //需要返回统计数据
                $fields = implode(',', $count_key);
                $select_sql = " SELECT {$fields} FROM ({$select_sql}) t1 ";
                $count = $this -> get_count($select_sql);
            }
        }else{
            $sum = 0;
            $count = array();
        }
        return $data;
    }

    /**
     * 获取其他筛选条件
     * @param $other_condition
     * @param $row_list
     * @return string
     */
    private function getOtherWhere($other_condition,$row_list){
        $other_where = ' 1=1 ';
        foreach ($other_condition as $tmp){
            if(!empty($tmp['operator'])){
                $this->getOtherWhere2($tmp,$row_list,$other_where);
            }else{
                $this->getOtherWhere1($tmp,$row_list,$other_where);
            }
        }unset($tmp);

        return $other_where;
    }

    /**
     * 获取other条件关联控件使用
     * @param $tmp
     * @param $row_list
     * @param $other_where
     * @return void
     */
    private function getOtherWhere2($tmp,$row_list,&$other_where){
        $input_type = $row_list[$tmp['field_key']]['type'];
        if($input_type == 'date' && $row_list[$tmp['field_key']]['format']!='time' ){
            $tmp['field_key'] = "UNIX_TIMESTAMP({$tmp['field_key']})";
        }

        if($input_type == 'create_time' ){
            $tmp['field_key'] = "UNIX_TIMESTAMP({$tmp['field_key']})";
        }
        switch ($tmp['operator']){
            case '=':
                $other_where .= " and {$tmp['field_key']}='{$tmp['field_val']}' ";
                break;
            case 'like':
                $other_where .= " and {$tmp['field_key']} like '%{$tmp['field_val']}%' ";
                break;
            case 'in':
                $tmp_arr = $tmp['field_val'];
                !is_array($tmp_arr) && $tmp_arr = explode(',',$tmp_arr);
                //$other_where .= " and {$tmp['field_key']} in (\"".implode('","',$tmp_arr)." \" ) ";

                if($input_type == 'com_id'){
                    $tmp_tmp_cond = " {$tmp['field_key']} in (\"".implode('","',$tmp_arr)." \" )";
                }else{
                    $tmp_tmp_cond = ' 1=0 ';
                    foreach ($tmp_arr as $v){
                        $tmp_tmp_cond .= ' or ('.$tmp['field_key'] .' like "%'.$v.'%" and LENGTH('.$tmp['field_key'].')>=LENGTH(\''.$v.'\') ) ';
                    }
                }

                $other_where .=  " and {$tmp_tmp_cond}";
                break;
            case 'between':
                $vals = explode(' - ',$tmp['field_val']);
                $v1 = isset($vals[0])?$vals[0]:0;
                $v2 = isset($vals[1])?$vals[1]:0;

                if($row_list[$tmp['field_key']]['type'] == 'date' && $row_list[$tmp['field_key']]['format']=='time' ){
                    $tmp['field_key'] = "UNIX_TIMESTAMP({$tmp['field_key']})";
                    $v1 && $v1 = "UNIX_TIMESTAMP('".date('H:i',$v1)."')";
                    $v2 && $v2 = "UNIX_TIMESTAMP('".date('H:i',$v2)."')";
                }
                $other_where .= " and {$tmp['field_key']} >= {$v1} and {$tmp['field_key']} <= {$v2}";

                break;
        }
    }
    /**
     * 获取other条件关联控件使用
     * @param $tmp
     * @param $row_list
     * @param $other_where
     * @return void
     */
    private function getOtherWhere1($tmp,$row_list,&$other_where){
        if($row_list[$tmp['field_key']]['type'] == 'date' ){
            if($row_list[$tmp['field_key']]['format']!='time'){
                $tmp['field_key'] = "UNIX_TIMESTAMP({$tmp['field_key']})";
            }else{
                $tmp['field_val'] = date('H:i',$tmp['field_val']);
            }
        }
        switch ($row_list[$tmp['field_key']]['type']){
            case 'dept':
            case 'people':
                $vals = explode(',',$tmp['field_val']);
                foreach ($vals as $v){
                    $other_where .= " and {$tmp['field_key']} like '%\"{$v}\"%' ";
                }
                break;
            default:
                if(!empty($row_list[$tmp['field_key']]['p_label_id']) && $row_list[$tmp['field_key']]['p_label_id']!='0-0' ){
                    //子表字段通过模糊匹配处理
                    $other_where .= " and {$tmp['field_key']} like '%{$tmp['field_val']}%' ";
                }else{
                    $other_where .= " and {$tmp['field_key']}='{$tmp['field_val']}' ";
                }
                break;
        }
    }

    private function _new_get_table_data($row_list=array(), $col_key=array(), $cond_list=array(), $join_list=array(), $order=array(), $other_cond = array(), &$count, &$sum, $start=0, $page_size=5){
        //所有的fields值
        $fields = array();
        //所有字段的key值
        $key_list = array(
            0 => array(),	//不汇总
            1 => array(),	//求和
            2 => array(),	//求平均
            3 => array(),	//求最大
            4 => array(),	//求最小
        );
        if(!empty($col_key)){
            //若存在分类，把分类放在第一个
            $key_list[0] = $col_key;
        }

        //联表条件
        $join = array();
        //子表单汇总字段
        $count_table = array();
        //条件key
        $cond_key = array();
        //计算指标key
        $function_key = array();
        foreach ($row_list as $row) {
            if($row['type'] == "create_time"){
                $k = array_shift($row['list_key']);
                $row['list_key'] = array("{$k}@{$row['time_type']}");
            }
            $key_list[$row['count_type']] = array_merge($key_list[$row['count_type']], $row['list_key']);


            $type = isset(self::$CountType[$row['count_type']]) ? self::$CountType[$row['count_type']] : '';
            $count_key[] = " {$type}(IF({$row['row_key']} IS NULL, 0, {$row['row_key']})) {$row['row_key']}";

            if($row['isfunction'] == 1){
                $function_key[$row['input_key']] = array(
                    "key" => $row['row_key'],
                    'key_list' => $row['list_key'],
                );
                $function_child = FALSE;
                foreach ($row['list_key'] as $key){
                    if(strpos($key, '_')){
                        $function_child = TRUE;
                        break;
                    }
                }unset($key);
                if($function_child && empty($col_key)){
                    //数据表子表单计算指标
                    $fields[] = " {$row['row_key']} ";
                }else{
                    $value = $row['input_key'];
                    $row['list_key'] = array_unique($row['list_key']);
                    foreach ($row['list_key'] as $key) {
                        if(strpos($key, "input")){
                            $value = str_replace($key, $key."__".$row['count_type'], $value);
                        }
                    }unset($key);
                    $fields[] = " {$type}({$value}) as {$row['row_key']} ";
                }
                continue;
            }

            if(strpos($row['input_key'], '_') && $row['count_type'] != 0){
                //若统计子表单字段数据
                $val_list = explode('.', $row['input_key']);
                $count_table[$val_list[1]] = $row['count_type'];
                if($row['count_type'] == 2){
                    //统计子表单字段平均数
                    $fields[] = "convert((SUM({$row['input_key']}__{$row['count_type']})/SUM({$row['input_key']}_count__{$row['count_type']})+0), decimal) as {$row['row_key']}";
                    continue;
                }
            }

            if($type == 'MAX' || $type == 'MIN'){
                $fields[] = "{$type}(if({$row['input_key']}__{$row['count_type']}+0= 9999999999 OR {$row['input_key']}__{$row['count_type']}+0= -9999999999, 0, {$row['input_key']}__{$row['count_type']}+0)) as {$row['row_key']}";
            }else if($type == 'SUM'){
                $fields[] = "SUM({$row['input_key']}__{$row['count_type']}+0) as {$row['row_key']}";
            }else if($type == 'AVG'){
                $fields[] = "convert(AVG({$row['input_key']}__{$row['count_type']}+0), decimal) as {$row['row_key']}";
            }else{
                if($this->isPrivateInput($row['input_key'])){
                    $fields[] = "{$type}({$row['input_key']}__{$row['count_type']}) as {$row['row_key']}";
                }else{
                    $fields[] = "{$type}({$row['input_key']}) as {$row['row_key']}";
                }
            }
        }unset($row);

        foreach ($cond_list as $c) {
            $key_list[0][]  = $c['key'];
        }unset($c);

        foreach ($join_list as $j) {
            $key_list[0] = array_merge($key_list[0], $j['key_list']);
            foreach ($j['key_list'] as $key) {
                if($this->isPrivateInput($key)){
                    $j['value'] = str_replace($key, $key."__0", $j['value']);
                }
            }unset($key);
            $join[] = $j['value'];
        }

        //拆分条件
        $ret = $this -> compose_cond($cond_list);
        //表单条件
        $cond = $ret['cond_list'];
        //子表单条件
        $child_cond = $ret['child_cond'];

        $form = $this -> get_form_fields($key_list, $col_key, $function_key);

        if(count($form) == 1){
            //单表
            $form = array_shift($form);
            $tmp_child_cond = isset($child_cond[$form['key']]) ? $child_cond[$form['key']] : array();
            $tmp_cond = isset($cond[$form['key']]) ? $cond[$form['key']] : array();
            $sql = $this -> get_simple_table_sql($form, $other_cond, $tmp_child_cond, $tmp_cond);
        }else if(count($form) == 2){
            //双表
            $sql = array();
            foreach ($form as $f) {
                $tmp_child_cond = isset($child_cond[$f['key']]) ? $child_cond[$f['key']] : array();
                $tmp_cond = isset($cond[$f['key']]) ? $cond[$f['key']] : array();
                $sql[] = $this -> get_simple_table_sql($f, $other_cond, $tmp_child_cond, $tmp_cond);
            }unset($f);
            $sql = $sql[0] . ' INNER JOIN ' . $sql[1] .' ON ' . implode(' and ', $join);
        }else{
            throw new \Exception('查询错误');
        }

        $fields = implode(',', $fields);
        // $cond = implode(' AND ', $cond);
        if($page_size != 0){
            empty($start) && $start = 0;
            $limit = " LIMIT {$start},{$page_size} ";
        }else{
            $limit = '';
        }

        $select_sql = "SELECT {$fields} FROM {$sql} ";
        // if(!empty($cond)){
        // 	$select_sql .= " WHERE {$cond} ";
        // }

        if(!empty($col_key)){
            foreach ($col_key as &$key) {
                if($this->isPrivateInput($key)){
                    $key .= "__0";
                }
            }unset($key);
            $col_str = implode(",", $col_key);
            $select_sql .= " GROUP BY {$col_str}";
            if(!empty($order)){
                $order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
                $select_sql = "SELECT * FROM ({$select_sql}) t ";
                $select_sql .= " ORDER BY {$order['key']} {$order_type}";
            }
        }else{
            if(!empty($order)){
                $order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
                $select_sql .= " ORDER BY {$order['key']} {$order_type}";
            }
        }
        if(!empty($limit)){
            $sql = $select_sql . $limit;
        }else{
            $sql = $select_sql;
        }
        // parent::log_i($sql);
        $data = g('pkg_db') -> query($sql);

        if(!empty($data)){
            if($sum){
                $sum = $this -> get_sum($select_sql);
            }
            if($count){
                //需要返回统计数据
                $fields = implode(',', $count_key);
                $select_sql = " SELECT {$fields} FROM ({$select_sql}) t1 ";
                $count = $this -> get_count($select_sql);
            }
        }else{
            $sum = 0;
            $count = array();
        }
        return $data;
    }

    /** 获取单表sql */
    private function get_simple_table_sql($form, $other_cond, $child_cond, $crow_cond, $is_count=false){
        $form_fields = array(
            "f"	 => array(),	//数据表头
            'cf' => array(),	//汇总表头
        );
        //分类key
        $col_key = isset($form['col_key']) ? $form['col_key'] : array();
        //计算指标是否包含子表单
        $function_child = isset($form['function_child']) ? $form['function_child'] : FALSE;
        //计算指标
        $function_key = isset($form['function']) ? $form['function'] : array();

        //检查分类是否包含子表单
        foreach ($col_key as $key) {
            if(preg_match('/input[\d]+_[\d]+$/', $key)){
                //子表单控件作为分类
                return $this -> get_child_table_sql($form, $other_cond, $child_cond, $crow_cond);
            }
        }unset($key);


        foreach ($form['fields'] as $v => $type) {
            if($v == "creater_id"){
                //字段为申请人
                $form_fields["cf"][] = $form_fields["f"][] = " creater_id ";
            }else if(strpos($v, "@")){
                //字段为申请时间
                $k = explode('@', $v);
                $date_type = self::$TimeType[$k[1]];
                $form_fields["f"][] = str_replace('?', 'create_time', $date_type).' create_time ';
                $form_fields["cf"][] = " create_time ";
            }else if($v == "create_time"){
                //字段为申请时间,多余字段，直接跳过
                $tmp_field = "FROM_UNIXTIME(create_time, '%Y-%m-%d') create_time ";
                !in_array($tmp_field , $form_fields["f"]) && $form_fields["f"][] = $tmp_field;
                $is_count && !in_array($tmp_field, $form_fields["cf"]) && $form_fields["cf"][] = $tmp_field;
                continue;
            }else if($v == "com_id"){
                $form_fields["f"][] = " (select com_id from mv_proc_form_model where id=form_id) com_id ";
                $form_fields["cf"][] = " com_id ";
            } else if(strpos($v, '_')){
                //子表单字段
                if(isset($child_cond[$v])){
                    //如果字段在子表单条件中，加上过滤条件
                    $c_cond = ' and '. implode(' and ', $child_cond[$v]);
                }else{
                    $c_cond = "";
                }
                $type = array_unique($type);
                foreach ($type as $t) {
                    //汇总类型
                    $count_type = isset(self::$CountType[$t]) ? self::$CountType[$t] : "";
                    //设置默认值
                    if($t == 3 || $t == 4){
                        //求最大值或最小值
                        $default_val = 2*($t - 3.5) * 9999999999;
                    }else{
                        $default_val = 0;
                    }
                    if($t == 2){
                        //求平均值
                        $form_fields["f"][] = "SUM(IF(`input_key`='{$v}' {$c_cond}, 1, 0)) '{$v}_count__{$t}'";
                        $form_fields["cf"][] = "SUM(`{$v}_count__{$t}`) '{$v}_count__{$t}'";
                        $form_fields["f"][] = "SUM(IF(`input_key`='{$v}' {$c_cond}, val, {$default_val})) '{$v}__{$t}'";
                        $form_fields["cf"][] = "SUM(`{$v}__{$t}`) '{$v}__{$t}'";
                    }else if($t != 0){
                        //求和，最大，最小
                        $form_fields["f"][] = $count_type."(IF(`input_key`='{$v}' {$c_cond}, IF(val='', 0, cast(val as signed)), {$default_val})) '{$v}__{$t}'";
                        $form_fields["cf"][] = $count_type."(`{$v}__{$t}`) '{$v}__{$t}'";
                    }else{
                        $form_fields["f"][] = "cast(GROUP_CONCAT(
                            IF(
                                `input_key`='{$v}' {$c_cond}, 
                                concat(
                                    label_id, 
                                    '[:]', 
                                    if( 
                                        (type='picture' or type='people' or type='dept' or type='school' or type='schoolDept' or type='information') and ( val='null' or val='[]' or val='[\"\"]'),
                                        '',
                                        if(type='dangerous',other,val)
                                    )   
                                ), 
                                ''
                            ) separator '[^]') as char) '{$v}__{$t}' ";
                    }
                }unset($t);
            }else{
                //非子表单字段
                if(!is_array($type)){
                    $type = array($type);
                }
                $type = array_unique($type);
                foreach ($type as $t) {
                    $count_type = isset(self::$CountType[$t]) ? self::$CountType[$t] : "";
                    $form_fields["f"][] = "GROUP_CONCAT(
                        IF(
                            `input_key`='{$v}', 
                             if( 
                                (type='picture' or type='people' or type='dept' or type='school' or type='schoolDept' or type='information') and ( val='null' or val='[]' or val='[\"\"]'),
                                '',
                                if(type='dangerous',other,val)
                             ), 
                             ''
                        ) separator '') '{$v}__{$t}' ";
                    $form_fields["cf"][] = $count_type."(`{$v}__{$t}`) '{$v}__{$t}' ";
                }
            }
        }unset($type);

        if(!empty($other_cond)){
            //其他条件不为空
            $cond_str = array();
            foreach ($other_cond as $key => $val) {
                if(empty($val)) continue;
                if($key == 'creater_id' || $key == 'create_id'){
                    $cond_str[] = 'creater_id IN ('. implode(',', $val) .')';
                }else if($key == 'create_time'){
                    $time = explode(' - ', $val);
                    if(count($time) == 2){
                        $time[0] = strtotime($time[0]);
                        $time[1] = strtotime($time[1]);
                        $cond_str[] = 'create_time >='. $time[0];
                        $cond_str[] = 'create_time <='. $time[1];
                    }
                }
            }unset($val);

            if(!empty($cond_str)){
                $cond = ' AND '. implode(' AND ', $cond_str);
            }else{
                $cond = '';
            }
        }else{
            $cond = '';
        }

        if($function_child && empty($col_key)){
            //数据表，子表单计算指标
            $f_fields = array();
            $cf_fields = array();
            $key_array = array();
            foreach ($function_key as $value => $arr) {
                $f_fields[] = "cast(GROUP_CONCAT(concat(label_id, '[:]', ({$value})) separator '[^]') as char) {$arr["key"]}";
                $key_array[] = $arr['key'];
                foreach ($arr['key_list'] as $key) {
                    $cf_fields[$key] = "GROUP_CONCAT(
                        IF(
                            `input_key`='{$key}', 
                            if( 
                                (type='picture' or type='people' or type='dept' or type='school' or type='schoolDept' or type='information') and ( val='null' or val='[]' or val='[\"\"]'),
                                '',
                                if(type='dangerous',other,val)
                            ), 
                            ''
                        ) separator '') '{$key}'";
                }unset($key);
            }unset($k_list);
            $f_fields = implode(",", $f_fields);
            $cf_fields = implode(",", $cf_fields);
            $function_sql = <<<EOF
			SELECT formsetinst_id, {$f_fields}
			FROM (
				SELECT formsetinst_id, label_id , p_label_id, {$cf_fields}
				FROM mv_proc_formsetinst_label {$form['key']}
				WHERE 
				  ( 
                      form_history_id = {$form['history_id']} or 
                      EXISTS(select id from mv_proc_form_share_ref where form_history_id={$form['history_id']} and info_state=1 and share_to_form_history_id={$form['key']}.form_history_id) 
                  ) 
				  AND state=2 AND info_state = 1 and p_label_id != 0
				GROUP BY formsetinst_id ,p_label_id, label_id
			) {$form['key']}
			GROUP BY formsetinst_id
EOF;
        }else{
            $function_sql = "";
        }

        $form_fields["f"] = array_unique($form_fields["f"]);
        $form_fields["cf"] = array_unique($form_fields["cf"]);
        $f = implode(',', $form_fields["f"]);
        $cf = implode(',', $form_fields["cf"]);

        $sql = <<<EOF
		SELECT formsetinst_id, {$f}
		FROM mv_proc_formsetinst_label {$form['key']}
		WHERE 
		  ( 
              form_history_id = {$form['history_id']} or 
              EXISTS(select id from mv_proc_form_share_ref where form_history_id={$form['history_id']} and info_state=1 and share_to_form_history_id={$form['key']}.form_history_id) 
          )
		  AND state=2 AND info_state = 1 {$cond}
		GROUP BY formsetinst_id 
EOF;

        if(!empty($crow_cond)){
            $c_cond_str = " WHERE ". implode(" and ", $crow_cond);
            $sql = "SELECT * FROM ({$sql}) t {$c_cond_str} ";
        }

        if(!empty($function_sql)){
            $key_fields = implode(",", $key_array);
            $sql = <<<EOF
		SELECT t.*, {$key_fields}
		FROM ({$sql}) t LEFT JOIN ({$function_sql}) t2 on t.formsetinst_id = t2.formsetinst_id
EOF;
        }

        if(!empty($col_key)){
            //包含分类，先进行汇总
            foreach ($col_key as &$key) {
                if($this->isPrivateInput($key)){
                    $key .= "__0";
                }
            }unset($key);
            $col_key = implode(",", $col_key);
            $sql = <<<EOF
		(SELECT {$cf}
		FROM ({$sql}) t
		GROUP BY {$col_key}) {$form['key']} 
EOF;
        }else{
            //不包含分类
            $sql = <<<EOF
		({$sql}) {$form['key']} 
EOF;
        }
        return $sql;
    }


	private function get_child_table_sql($form, $other_cond, $child_cond, $crow_cond){
		$fields = array(
				"f"	 => array(),	//非子表单表头
				'cf' => array(),	//子表单表头
				'af' => array(),	//合并表头
				'cf_h' => array(),	//子表单表头
			);
		//分类key
		$col_key = isset($form['col_key']) ? $form['col_key'] : "";
		//计算指标是否包含子表单
		$function_child = isset($form['function_child']) ? $form['function_child'] : FALSE;
		//计算指标
		$function_key = isset($form['function']) ? $form['function'] : array();

		foreach ($form['fields'] as $v => $type) {
			if($v == "creater_id"){
				//字段为申请人
				$form_fields["f"][] = " creater_id ";
				$form_fields["af"][] = " creater_id ";
			}else if(strpos($v, "@")){
				//字段为申请时间
				$k = explode('@', $v);
				$date_type = self::$TimeType[$k[1]];
				$form_fields["f"][] = str_replace('?', 'create_time', $date_type).' create_time ';
				$form_fields["af"][] = " create_time ";
			}else if($v == "create_time"){
				//字段为申请时间,多余字段，直接跳过
				continue;
			}else if(strpos($v, '_')){
				//子表单字段
				if(isset($child_cond[$v])){
					//如果字段在子表单条件中，加上过滤条件
					$c_cond = ' and '. implode(' and ', $child_cond[$v]);
				}else{
					$c_cond = "";
				}

				$form_fields["cf"][] = "cast(GROUP_CONCAT(IF(`input_key`='{$v}' {$c_cond}, val, '') separator '') as char) '{$v}'";
				$form_fields["cf_h"][] = " {$v} != '' ";
				$type = array_unique($type);
				foreach ($type as $t) {
					//汇总类型
					$count_type = isset(self::$CountType[$t]) ? self::$CountType[$t] : "";
					//设置默认值
					if($t == 3 || $t == 4){
						//求最大值或最小值
						$default_val = 2*($t - 3.5) * 9999999999;
					}else{
						$default_val = 0;
					}
					if($t == 2){
						//求平均值
						$form_fields["af"][] = "SUM(`{$v}`) '{$v}__{$t}'";
						$form_fields["af"][] = "COUNT(`{$v}`) '{$v}_count__{$t}'";
					}else if($t != 0){
						//求和，最大，最小
						$form_fields["af"][] = $count_type."(`{$v}`) '{$v}__{$t}'";
					}else{
						$form_fields["af"][] = "`{$v}` '{$v}__{$t}'";
					}
				}unset($t);
			}else{
				//非子表单字段
				if(!is_array($type)){
					$type = array($type);
				}
				$form_fields["f"][] = "GROUP_CONCAT(IF(`input_key`='{$v}', val, '') separator '') '{$v}' ";
				$type = array_unique($type);
				foreach ($type as $t) {
					$count_type = isset(self::$CountType[$t]) ? self::$CountType[$t] : "";
					$form_fields["af"][] = "{$count_type}(`{$v}`) '{$v}__{$t}' ";
				}
			}
		}unset($type);

		if(!empty($other_cond)){
			//其他条件不为空
			$cond_str = array();
			foreach ($other_cond as $key => $val) {
				if(empty($val)) continue;
				if($key == 'creater_id' || $key == 'create_id'){
					$cond_str[] = 'creater_id IN ('. implode(',', $val) .')';
				}else if($key == 'create_time'){
					$time = explode(' - ', $val);
					if(count($time) == 2){
						$time[0] = strtotime($time[0]);
						$time[1] = strtotime($time[1]);
						$cond_str[] = 'create_time >='. $time[0];
						$cond_str[] = 'create_time <='. $time[1];
					}
				}
			}unset($val);

			if(!empty($cond_str)){
				$cond = ' AND '. implode(' AND ', $cond_str);
			}else{
				$cond = "";
			}
		}else{
			$cond = '';
		}

		$cf_fields = implode(",", $form_fields['cf']);
		$cf_h_fields = implode(" and ", $form_fields['cf_h']);
		$col_sql = <<<EOF
		SELECT formsetinst_id, {$cf_fields}
		FROM mv_proc_formsetinst_label {$form['key']}
		WHERE form_history_id = {$form['history_id']} AND state=2 AND info_state = 1 and p_label_id != 0
		GROUP BY formsetinst_id ,p_label_id, label_id
		HAVING {$cf_h_fields}
EOF;

		if(!empty($form_fields["f"])){
			$form_fields['af'] = array_unique($form_fields['af']);
			$f = implode(',', $form_fields["f"]);
			$af = implode(',', $form_fields["af"]);
			$col_key_str = implode(",", $col_key);

			$sql = <<<EOF
			SELECT formsetinst_id, {$f}
			FROM mv_proc_formsetinst_label {$form['key']} 
			WHERE form_history_id = {$form['history_id']} AND state=2 AND info_state = 1 {$cond}
			GROUP BY formsetinst_id 
EOF;
			if(!empty($crow_cond)){
				foreach ($crow_cond as &$val) {
					$val = str_replace("__0", "", $val);
				}unset($val);
				$c_cond_str = " WHERE ". implode(" and ", $crow_cond);
			}else{
				$c_cond_str = '';
			}
			$a_sql = <<<EOF
			(SELECT t.formsetinst_id, {$af}
			FROM ({$col_sql}) t LEFT JOIN ({$sql}) t2 ON t.formsetinst_id = t2.formsetinst_id 
			{$c_cond_str}
			GROUP BY {$col_key_str}) {$form['key']} 
EOF;
		}else{
			$af = implode(',', $form_fields["af"]);
			$col_key_str = implode(",", $col_key);
			$a_sql = "( SELECT {$af} FROM ({$col_sql}) t group by {$col_key_str}) {$form['key']} ";
		}
		return $a_sql;
	}

	/**
	 * @param  $row_list  	列表头数据
	 * @param  $col_key		行表头
	 * @param  $cond_list	过滤条件
	 * @param  $join_list	联表条件
	 * @param  $order 		排序条件
	 * @param  $other_cond	其他条件（提交人，提交时间的限制）
	 * @param  $page 		页码
	 * @param  $page_size 	页大小
	 * @return
	 */
	private function get_table_data($row_list='', $col_key='', $cond_list='', $join_list='', $order='', $other_cond = '', &$count, &$sum, $col_is_table=FALSE, $page=0, $page_size=0){
		$fields = array();
		$key_list = array();
		if(!empty($col_key)){
			$key_list[] = $col_key;
		}
		$join = array();
		$count_table = array();
		$count_key = array();
		$function_key = array();
		$special_row = array();
		foreach ($row_list as $row) {
			if($row['input_type'] == "function"){
				$function_key = array_merge($function_key, $row['key_list']);
			}
			$key_list = array_merge($key_list, $row['key_list']);
			$type = isset(self::$CountType[$row['type']]) ? self::$CountType[$row['type']] : '';
			$count_key[] = " {$type}(IF({$row['key']} IS NULL, 0, {$row['key']})) {$row['key']}";
			if(strpos($row['value'], '_') && $row['type'] != 0 && $row['type'] != 2){
				//若统计子表单字段数据
				$val_list = explode('.', $row['value']);
				$count_table[$val_list[1]] = $row['type'];
			}else if(strpos($row['value'], '_') && $row['type'] == 2){
				//统计子表单字段平均数
				$val_list = explode('.', $row['value']);
				$count_table[$val_list[1]] = $row['type'];
				$fields[] = "convert((SUM({$row['value']})/SUM({$row['value']}_count)+0), decimal) as {$row['key']}";
				continue;
			}
			if($type == 'MAX' || $type == 'MIN'){
				$fields[] = "{$type}(if({$row['value']}+0= 9999999999 OR {$row['value']}+0= -9999999999, 0, {$row['value']})+0) as {$row['key']}";
			}else if($type == 'SUM'){
				$fields[] = 'SUM('.$row['value'].'+0) as '. $row['key'];
				$special_row['SUM('.$row['value'].'+0) as '. $row['key']] = array(
						'key' => $row['value'],
						'as' => $row['key'],
						'type' => $row['input_type'],
						'key_list' => $row['key_list'],
						'val' => 'CAST(SUM('.$row['value'].'+0)/COUNT(DISTINCT <$>.r) AS SIGNED) as '. $row['key'],
					);
			}else if($type=='AVG'){
				$fields[] = 'convert(AVG('.$row['value'].'+0), decimal) as '. $row['key'];
			}else{
				$fields[] = $type.'('.$row['value'].') as '. $row['key'];
			}
		}unset($row);
		foreach ($cond_list as $c) {
			$key_list[] = $c['key'];
		}unset($c);

		foreach ($join_list as $j) {
			$key_list = array_merge($key_list, $j['key_list']);
			$join[] = $j['value'];
		}

		$function_key = $this -> get_function_key($function_key);

		$ret = $this -> compose_cond($cond_list);

		$cond = $ret['cond_list'];
		$child_cond = $ret['child_cond'];
		$key_list = array_unique($key_list);
		$form = $this -> get_form_fields($key_list);
		if($col_is_table){
			$form = array_shift($form);
			$sql = $this -> get_sql_by_col_table($form, $other_cond, $child_cond, $count_table, $function_key);
		}else{
			if(count($form) == 1){
				//单表
				$form = array_shift($form);
				$sql = $this -> get_sql($form, $other_cond, $child_cond, $count_table, $function_key);
			}else if(count($form) == 2){
				//双表
				$sql = array();
				foreach ($form as $f) {
					$sql[] = $this -> get_sql($f, $other_cond, $child_cond, $count_table, $function_key);
				}unset($f);

				$sql = $sql[0] . ' INNER JOIN ' . $sql[1] .' ON ' . implode(',', $join);

				if(!empty($special_row) && !empty($col_key)){
					$this -> add_special_row($fields, $special_row, $form);
				}
			}else{
				throw new \Exception('查询错误');
			}
		}
		$fields = implode(',', $fields);
		$cond = implode(' AND ', $cond);

		$limit = '';
		if($page != 0 && $page_size != 0){
			$start = ($page - 1) * $page_size;
			$limit = " LIMIT {$start}, {$page_size} ";
		}

		$select_sql = "SELECT {$fields} from {$sql} ";
		if(!empty($cond)){
			$select_sql .= " WHERE {$cond}";
		}
		if(!empty($col_key)){
			$select_sql .= " GROUP BY {$col_key}";
			if(!empty($order)){
				$order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
				$select_sql = "select * from ({$select_sql}) t ";
				$select_sql .= " ORDER BY {$order['key']} {$order_type}";
			}
		}else{
			if(!empty($order)){
				$order_type = isset(self::$OrderType[$order['type']]) ? self::$OrderType[$order['type']] : array_shift(self::$OrderType);
				$select_sql .= " ORDER BY {$order['key']} {$order_type}";
			}
		}
		if(!empty($limit)){
			$sql = $select_sql . $limit;
		}else{
			$sql = $select_sql;
		}
		$data = g('pkg_db') -> query($sql);
		if(!empty($data)){
			if($sum){
				$sum = $this -> get_sum($select_sql);
			}
			if($count){
				//需要返回统计数据
				$fields = implode(',', $count_key);
				$select_sql = " SELECT {$fields} FROM ({$select_sql}) t1 ";
				$count = $this -> get_count($select_sql);
			}
		}else{
			$sum = 0;
			$count = array();
		}
		return $data;
	}

	/** 增加特殊的列 汇总表中，多对多关系的特殊处理 */
	public function add_special_row(&$fields, $special_row, $form){
		$function = array();
		$row = array();
		foreach ($special_row as $key => &$v) {
			if($v['type']=="function"){
				$function[$key] = $v;
				continue;
			}
			if(!in_array($key, $fields)){
				continue;
			}

			$k_list = explode('.', $v['key']);
			$k = $k_list[0];
			$other_k = "";
			foreach ($form as $ke => $f) {
				if($k != $ke){
					$other_k = $ke;
					break;
				}
			}unset($f);
			if(empty($other_k)){
				continue;
			}
			$v['val'] = str_replace('<$>', $other_k, $v['val']);
			$index = array_search($key, $fields);
			$fields[$index] = $v['val'];
			$row[$v['key']] = $v['val'];
		}unset($v);

		if(!empty($function)){
			foreach ($function as $key => $v) {
				foreach ($v['key_list'] as $k) {
					$vv = preg_replace('/.as rowkey[\d]+$/', '', $row[$k]);
					$v['key'] = str_replace($k, $vv, $v['key']);
				}unset($k);
				$index = array_search($key, $fields);
				$fields[$index] = $v['key']." as ". $v['as'];
			}unset($v);
		}
	}

	/** 获取指定SQL返回的数据总数 */
	private function get_sum($sql){
		$key = $this -> get_redis_key($sql);
		$sum = g('pkg_redis') -> get($key);
		if(empty($sum)){
			$sum_sql = " SELECT count(*) s FROM ({$sql}) t1 ";
			$sum_data = g('pkg_db') -> query($sum_sql);
			if(empty($sum_data)){
				$sum = 0;
			}else{
				$sum = $sum_data[0]['s'];
			}
			g('pkg_redis') -> set($key, $sum, self::$Data_Time);
		}
		return $sum;
	}

	private function get_count($sql){
		$key = $this -> get_redis_key($sql);
		$count = g('pkg_redis') -> get($key);
		if(empty($count)){
			$count_data = g('pkg_db') -> query($sql);
			if(empty($count_data)){
				$count_data = array();
			}else{
				$count_data = array_shift($count_data);
			}
			g('pkg_redis') -> set($key, json_encode($count_data), self::$Data_Time);
		}else{
			$count_data = json_decode($count, TRUE);
		}
		return $count_data;
	}

	/** 获取单个表单查询SQL */
	private function get_sql($form, $other_cond = array(), $child_cond=array(), $count_table = array(), $function_key=array()){
		$form_fields = array();
		foreach ($form['fields'] as $v) {
			if($v == "creater_id"){
				$form_fields[] = " creater_id ";
			}else if(strpos($v, "@")){
				$k = explode('@', $v);
				$date_type = self::$TimeType[$k[1]];
				$form_fields[] = str_replace('?', 'create_time', $date_type).' create_time ';
			}else if($v == "create_time"){
			}else if(strpos($v, '_')){
				//子表单字段
				if(isset($child_cond[$v])){
					$c_cond = ' and '. implode(' and ', $child_cond[$v]);
				}
				if(isset($count_table[$v])){
					//在统计范围
					$count_type = self::$CountType[$count_table[$v]];
					$default_val = 0;
					if($count_table[$v] == 3 || $count_table[$v] ==4){
						$default_val = 2*($count_table[$v] - 3.5) * 9999999999;
					}

					if($count_table[$v] == 2){
						$form_fields[] = "SUM(IF(`input_key`='{$v}' {$c_cond}, 1, 0)) {$v}_count";
						$form_fields[] = "SUM(IF(`input_key`='{$v}' {$c_cond}, val, {$default_val})) {$v}";
					}else{
						$form_fields[] = $count_type."(IF(`input_key`='{$v}' {$c_cond}, IF(val='', 0, val), {$default_val})) {$v}";
					}
				}else{
					//不在统计范围
					$form_fields[] = "cast(GROUP_CONCAT(
					    IF(
					        `input_key`='{$v}' {$c_cond}, 
					        concat(
					            label_id, 
					            ':', 
					            if( 
					                (type='picture' or type='people' or type='dept' or type='school' or type='schoolDept' or type='information') and ( val='null' or val='[]' or val='[\"\"]'),
					                '',
					                if(type='dangerous',other,val)
					            )   
					        ), 
					        ''
					    ) separator '^') as char)'{$v}' ";
				}
			}else{
				if(in_array($v, $function_key)){
					$form_fields[] = "GROUP_CONCAT(IF(`input_key`='{$v}' {$c_cond}, if(val>0, val, 0), '') separator '') '{$v}' ";
				}else{
					$form_fields[] = "GROUP_CONCAT(IF(`input_key`='{$v}' {$c_cond}, val, '') separator '') '{$v}' ";
				}
			}

		}unset($v);

		$cond = '';
		if(!empty($other_cond)){
			//其他条件不为空
			$cond_str = array();
			foreach ($other_cond as $key => $val) {
				if(empty($val)) continue;
				if($key == 'creater_id' || $key == 'create_id'){
					$cond_str[] = 'creater_id IN ('. implode(',', $val) .')';
				}else if($key == 'create_time'){
					$time = explode(' - ', $val);
					if(count($time) == 2){
						$time[0] = strtotime($time[0]);
						$time[1] = strtotime($time[1]);
						$cond_str[] = 'create_time >='. $time[0];
						$cond_str[] = 'create_time <='. $time[1];
					}
				}
			}unset($val);
			if(!empty($cond_str)){
				$cond = ' AND '. implode(' AND ', $cond_str);
			}
		}

		$f = implode(',', $form_fields);
		$sql = <<<EOF
		(SELECT {$f} , rand() r
		FROM mv_proc_formsetinst_label {$form['key']}
		WHERE form_history_id = {$form['history_id']} AND state=2 AND  info_state = 1 {$cond}
		GROUP BY formsetinst_id) {$form['key']} 
EOF;
		return $sql;
	}

	/** 获取SQL */
	private function get_sql_by_col_table($form, $other_cond = array(), $child_cond=array(),  $count_table = array(), $function_key = array()){
		$form_fields = array();
		$cond = array();
		$l_cond = array();
		foreach ($form['fields'] as $v) {
			if(preg_match('/input[\d]+_[\d]+$/', $v)){
				$c_cond = '';
				$cond[] = " input_key = '{$v}' ";
				//子表单字段
				if(isset($child_cond[$v])){
					$c_cond = ' and '. implode(' and ', $child_cond[$v]);
				}
				if(isset($count_table[$v])){
					//在统计范围
					$count_type = self::$CountType[$count_table[$v]];
					$default_val = 0;
					if($count_table[$v] == 3 || $count_table[$v] ==4){
						$default_val = 2*($count_table[$v] - 3.5) * 9999999999;
					}
					if($count_table[$v] == 2){
						$form_fields[] = "SUM(IF(`input_key`='{$v}' {$c_cond}, 1, 0)) {$v}_count";
						$form_fields[] = "SUM(IF(`input_key`='{$v}' {$c_cond}, val, {$default_val})) {$v}";
					}else{
						$form_fields[] = $count_type."(IF(`input_key`='{$v}' {$c_cond}, IF(val='', 0, val), {$default_val})) {$v}";
					}
				}else{
					//不在统计范围
					$form_fields[] = "cast(GROUP_CONCAT(IF(`input_key`='{$v}' {$c_cond}, val, '') separator '') as char)'{$v}' ";
				}
			}else{
				$form_label_fields[] = "cast(GROUP_CONCAT(IF(`input_key`='{$v}', val, '') separator '') as char)'{$v}' ";
			}
		}unset($v);

		$l_cond = '';
		if(!empty($other_cond)){
			//其他条件不为空
			$cond_str = array();
			foreach ($other_cond as $key => $val) {
				if(empty($val)) continue;
				if($key == 'creater_id' || $key == 'create_id'){
					$cond_str[] = 'creater_id IN ('. implode(',', $val) .')';
				}else if($key == 'create_time'){
					$time = explode(' - ', $val);
					if(count($time) == 2){
						$time[0] = strtotime($time[0]);
						$time[1] = strtotime($time[1]);
						$cond_str[] = 'create_time >='. $time[0];
						$cond_str[] = 'create_time <='. $time[1];
					}
				}
			}unset($val);
			if(!empty($cond_str)){
				$l_cond = ' AND '. implode(' AND ', $cond_str);
			}
		}
		$l_sql = "";
		$t_f = "";
		if(!empty($form_label_fields) || !empty($l_cond)){
			$ff = empty($form_label_fields) ? '' : ' ,'.implode(',', $form_label_fields);
			$t_f = ', t.*';
			$l_sql = <<<EOF
			INNER JOIN (SELECT formsetinst_id {$ff} 
			FROM mv_proc_formsetinst_label
			WHERE form_history_id = {$form['history_id']} AND state=2 AND label_id=0 AND info_state = 1 {$l_cond}
			GROUP BY formsetinst_id) t ON t.formsetinst_id =  {$form['key']}.formsetinst_id 
EOF;
		}


		if(!empty($cond)){
			$cond = "(" . implode(' OR ', $cond) . ")";
		}else{
			$cond = '';
		}

		$f = implode(',', $form_fields);
		$sql = <<<EOF
		(SELECT {$f} {$t_f}
		FROM mv_proc_formsetinst_label {$form['key']} {$l_sql}
		WHERE form_history_id = {$form['history_id']} AND state=2 AND info_state = 1 AND {$cond}
		GROUP BY formsetinst_id,label_id ) {$form['key']} 
EOF;

		return $sql;
	}

	/** 获取表单查询数据 */
	private function get_form_fields($key_list, $col_key, $function_key){
		$form = array();
		$all_id = array();
		foreach ($key_list as $k => $k_list) {
			foreach ($k_list as $key) {
				$keys = explode('.', $key);
				if(count($keys) != 2) continue;
				if(!isset($form[$keys[0]])){
					$form[$keys[0]] = array();
					$id = str_replace('form', '' , $keys[0]);
					$id = (int)$id;
					$form[$keys[0]]['id'] = $id;
					$form[$keys[0]]['key'] = $keys[0];
					$all_id[] = $id;
				}
				if(!isset($form[$keys[0]]['fields'][$keys[1]])){
					$form[$keys[0]]['fields'][$keys[1]] = array();
				}
				$form[$keys[0]]['fields'][$keys[1]][] = $k;
			}unset($key);
		}unset($k_list);
		foreach ($col_key as $key) {
			$keys = explode('.', $key);
			if(count($keys) != 2) continue;
			if(!isset($form[$keys[0]])){
				$form[$keys[0]] = array();
				$id = str_replace('form', '' , $keys[0]);
				$id = (int)$id;
				$form[$keys[0]]['id'] = $id;
				$form[$keys[0]]['key'] = $keys[0];
				$all_id[] = $id;
			}
			$form[$keys[0]]['col_key'][] = $keys[1];
		}unset($key);

		foreach ($function_key as $value => $arr) {
			foreach ($arr['key_list'] as $key) {
				$keys = explode('.', $key);
				if(count($keys) != 2) continue;
				if(!isset($form[$keys[0]])){
					$form[$keys[0]] = array();
					$id = str_replace('form', '' , $keys[0]);
					$id = (int)$id;
					$form[$keys[0]]['id'] = $id;
					$form[$keys[0]]['key'] = $keys[0];
					$all_id[] = $id;
				}
				if(strpos($keys[1], "_")){
					$form[$keys[0]]['function_child'] = true;
				}else{
					//非子表单不特殊处理
					continue;
				}
				if(!isset($form[$keys[0]]['function'][$value])){
					$form[$keys[0]]['function'][$value] = array(
							"key" => $arr['key'],
							"key_list" => array(),
						);
				}
				$form[$keys[0]]['function'][$value]["key_list"][] = $keys[1];
			}unset($key);
		}unset($key);

		$history_ids = $this -> get_form_history_id($all_id);
		foreach ($form as &$v) {
			$v['history_id'] = $history_ids[$v['id']];
		}unset($v);
		return $form;
	}

	/** 获取表单的历史版本ID */
	private function get_form_history_id($form_ids){
		$forms = g('mv_form') -> get_forms_by_ids($form_ids, 'id, form_history_id');
		$data = array();
		foreach ($forms as $v) {
			$data[$v['id']] = $v['form_history_id'];
		}unset($v);
		return $data;
	}

	/** 将提交人ID转换成名称 */
	private function change_user_id(&$data, $user_key){
		$user_ids = array();
		foreach ($user_key as $key) {
			foreach ($data as $val) {
				if(is_numeric($val[$key])){
					$user_ids[] = $val[$key];
				}else if(is_array($val[$key])){
					foreach ($val[$key] as $v) {
						$u = json_decode($v, TRUE);
						if(!empty($u)){
							$user_ids = array_merge($user_ids, $u);
						}
					}
				}else{
					$u = json_decode($val[$key], TRUE);
					if(!empty($u)){
						$user_ids = array_merge($user_ids, $u);
					}
				}
			}unset($val);
		}unset($key);
		$user_ids = array_unique($user_ids);
		$user_ids = array_filter($user_ids);
		if(empty($user_ids)){
			return;
		}

		$user_list = g('dao_user') -> get_by_ids('', $user_ids, 'id, name', true);
		$users = array();
		foreach ($user_list as $val) {
			$users[$val['id']] = $val['name'];
		}unset($val);

		foreach ($user_key as $key) {
			foreach ($data as &$val) {
				$user = $val[$key];
				if(is_numeric($user)){
					$val[$key] = isset($users[$user]) ? $users[$user] : '提交人';
				}else if(is_array($user)){
					foreach ($val[$key] as &$v) {
						$names = array();
						$user_list = json_decode($v, TRUE);
						if(!empty($user_list) && is_array($user_list)){
							foreach ($user_list as $u) {
								isset($users[$u]) && $names[] = $users[$u];
							}unset($u);
						}
						$v = implode(';', $names);
					}unset($v);
				}else {
					$user_list = json_decode($user, TRUE);
					$names = array();
					if(!empty($user_list) && is_array($user_list)){
						foreach ($user_list as $u) {
							$names[] = $users[$u];
						}unset($u);
					}
					$val[$key] = implode(';', $names);
				}
			}unset($val);
		}
	}

	/** 将提交人ID转换成名称 */
	private function change_dept_id(&$data, $dept_key){
		$dept_ids = array();
		foreach ($dept_key as $key) {
			foreach ($data as $val) {
				if(is_array($val[$key])){
					foreach ($val[$key] as $v) {
						$u = json_decode($v, TRUE);
						if(!empty($u)){
							$dept_ids = array_merge($dept_ids, $u);
						}
					}unset($v);
				}else if(!empty($val[$key]) && !is_array($val[$key])){
					$u = json_decode($val[$key], TRUE);
					if(!empty($u)){
						$dept_ids = array_merge($dept_ids, $u);
					}
				}
			}unset($val);
		}unset($key);

		$dept_ids = array_unique($dept_ids);
		$dept_ids = array_filter($dept_ids);
		if(empty($dept_ids)){
			return;
		}

		$dept_list = g('dao_dept') -> get_by_ids($dept_ids, 'id, name');

		$depts = array();
		foreach ($dept_list as $val) {
			$depts[$val['id']] = $val['name'];
		}unset($val);

		foreach ($dept_key as $key) {
			foreach ($data as &$val) {
				$dept = $val[$key];
				if(is_array($dept)){
					foreach ($val[$key] as &$v) {
						$dept_list = json_decode($v, TRUE);
						$names = array();
						if(!empty($dept_list) && is_array($dept_list)){
							foreach ($dept_list as $u) {
								isset($depts[$u]) && $names[] = $depts[$u];
							}unset($u);
						}
						$v = implode(';', $names);
					}unset($v);
				}else{
					$dept_list = json_decode($dept, TRUE);
					$names = array();
					if(!empty($dept_list) && is_array($dept_list)){
						foreach ($dept_list as $u) {
							$names[] = $depts[$u];
						}unset($u);
					}
					$val[$key] = implode(';', $names);
				}
			}unset($val);
		}
	}

    /**
     * 修改企业名称
     * @param $data
     * @param $com_key
     * @return void
     */
    private function change_com_id(&$data, $com_key){
        $com_ids = array();
        foreach ($com_key as $key) {
            foreach ($data as $val) {
                $com_ids[] = $val[$key];
            }unset($val);
        }unset($key);
        $com_ids = array_unique($com_ids);
        $com_ids = array_filter($com_ids);
        if(empty($com_ids)){
            return;
        }

        $com_list = g('dao_com') -> get_by_ids($com_ids, 'id, name');
        $com_list = array_column($com_list,null,'id');

        foreach ($com_key as $key) {
            foreach ($data as &$val) {
                $tmp_com_id = $val[$key];
                $val[$key] = isset($com_list[$tmp_com_id]['name'])?$com_list[$tmp_com_id]['name']:'';
            }unset($val);
        }
    }

    private function change_school_ids(&$data, $com_key){
        $com_ids = array();
        foreach ($com_key as $key) {
            foreach ($data as $val) {
                $tmp_com_ids = json_decode($val[$key],true);
                $tmp_com_ids && $com_ids = array_merge($com_ids,$tmp_com_ids);
            }unset($val);
        }unset($key);
        $com_ids = array_unique($com_ids);
        $com_ids = array_filter($com_ids);
        if(empty($com_ids)){
            return;
        }

        $com_list = g('dao_com') -> get_by_ids($com_ids, 'id, name');
        $com_list = array_column($com_list,null,'id');

        foreach ($com_key as $key) {
            foreach ($data as &$val) {
                $tmp_com_names = [];
                $tmp_com_ids = json_decode($val[$key],true);
                foreach ($tmp_com_ids as $tmp_com_id){
                    if( isset($com_list[$tmp_com_id]['name']) )
                        $tmp_com_names[] = $com_list[$tmp_com_id]['name'];
                }
                $val[$key] = implode(';',$tmp_com_names);
            }unset($val);
        }
    }
    /** 将子表单字段拆分 */
	private function change_table_val(&$data, $table_key){
		foreach ($data as &$val) {
			//保留KEY
			$key_state  = 0;
			foreach ($table_key as $table) {
				$key_array = array();
				foreach ($table as $key) {
					$v = $val[$key];
					$v_list = explode('[^]', $v);
					$v_list = array_filter($v_list);
					$list = array();
					foreach ($v_list as $va) {
						$tmp = explode('[:]', $va);
                        isset($tmp[1]) && $list[$tmp[0]] = $tmp[1];
					}unset($va);
					ksort($list);
					$vals = array_values($list);
					$keys = array_keys($list);

					if(empty($key_array)){
						$key_array = $keys;
					}else{
						$key_array = array_intersect($key_array, $keys);
					}

					$val[$key] = $list;
				}unset($key);
				foreach ($table as $key) {
					foreach ($val[$key] as $k => &$v) {
						if(!in_array($k, $key_array)){
							unset($val[$key][$k]);
						}
					}unset($v);
					$val[$key] = array_values($val[$key]);
				}unset($key);
			}unset($table);
		}unset($val);
	}

	/** 组合过滤条件 区分子表单字段过滤条件 */
	private function compose_cond($cond){
		$cond_list = array();
		$child_cond = array();
        $operators = array(
            '=', '!=', 'in', 'noin', 'like' , 'nolike', 'null' ,'nonull',
            '>=', '<=', '>', '<', 'between', 'userwhere' , 'datewhere'
        );

		foreach ($cond as $c){
            if( !empty($c['operator']) && in_array(strtolower($c['operator']),$operators)  ){
                $this->handle_compose_cond($c,$cond_list,$child_cond);
            }else{
                $this->handle_compose_cond_old($c,$cond_list,$child_cond);
            }
		}unset($c);
		$data = array(
				'child_cond' => $child_cond,
				'cond_list' => $cond_list
			);
		return $data;
	}

    /**
     * 格式化sql 数值
     * @param $c
     * @return array
     */
    private function format_compose_val($c,$return_arr=false,$changeToId=false){

        !is_array($c['value']) && is_json($c['value']) && $c['value'] = json_decode($c['value'],true);

        if($return_arr){
            $return = is_array($c['value'])?$c['value']:[strval($c['value'])];
        }else{
            $return = is_array($c['value'])?implode(';',$c['value']):strval($c['value']);
        }

        //时间控件时分 转化
        if($c['type']=='date' && $c['format']=='time' ){
            if(is_array($return)){
                foreach ($return as &$item){
                    $item = "UNIX_TIMESTAMP('".date('2022-01-01 H:i',$item)."')";
                }
            }else{
                $tmp_return = [];
                $data_list = explode(' - ', $return);
                foreach ($data_list as $item){
                    $tmp_return[] = "UNIX_TIMESTAMP('".date('2022-01-01 H:i',$item)."')";
                }
                $return = implode(' - ',$tmp_return);
            }
        }

        if($changeToId){
            switch ($c['type']){
                case 'school':
                    $cond = array('state!='=>0);
                    if(is_array($return)){
                        foreach ($return as $item){
                            $cond['__OR'][] = array(
                                'name like' => '%'.$item.'%',
                            );
                        }unset($item);
                    }else{
                        $cond['name like'] = '%'.$return.'%';
                    }

                    $companys = g('dao_com')->list_by_cond($cond,'id');
                    $companys && $return = array_column($companys,'id');
                    break;
            }
        }

        return $return;
    }

    /**
     * 循环内的新处理方式，可指定运算符
     * @return string|void
     */
    private function handle_compose_cond($c,&$cond_list,&$child_cond){
        if(!isset($c['value'])  ){
            return '';
        }
        $key_arr = explode(".", $c['key']);
        if(count($key_arr) != 2){
            //key值错误
            return ;
        }
        $form_id = $key_arr[0];
        !isset($cond_list[$form_id]) && $cond_list[$form_id] = array();
        !isset($child_cond[$form_id]) && $child_cond[$form_id] = array();

        $tmp = array();
        if($c['is_child']){
            $ckey = $key_arr[1];
            $c['key'] = 'val';
        }elseif ( $c['type']=='creater_id' ){
            $c['key'] = $key_arr[1];
        }elseif ( $c['type']=='com_id' ){
            $c['key'] = $key_arr[1];
        }elseif( $c['type']=='create_time' ){
            $c['format'] = '';
            $c['key'] = "UNIX_TIMESTAMP({$key_arr[1]})";
        }else{
            $c['key'] = $key_arr[1]."__0";
        }

        //时间控件需要转换成时间戳 进行比较
        $source_key = $c['key'];
        switch ($c['type']){
            case 'date':
                $c['key'] =  $c['format']=='time' ? "UNIX_TIMESTAMP( CONCAT_WS(' ','2022-01-01',{$c['key']}) )" :"UNIX_TIMESTAMP({$c['key']})";
                break;
        }

        !isset($c['format']) && $c['format'] = '';
        switch ($c['format']){
            case 'number':
            case 'function':
                //$tmp[] = "{$source_key} REGEXP '^-?[1-9][0-9]*[.]?[0-9]+' ";
                $tmp[] = "{$source_key} REGEXP '^-?[1-9][0-9]*([.][0-9]+)?' ";
                break;
            case 'date':
                $tmp[] = "{$source_key} REGEXP '^[0-9]{4}-[0-1][0-9]-[0-9]{2}$' ";
                break;
            case 'time':
                $tmp[] = "{$source_key} REGEXP '^[0-9]{2}:[0-9]{2}$' ";
                break;
            case 'datetime':
                $tmp[] = "{$source_key} REGEXP '^[0-9]{4}-[0-1][0-9]-[0-9]{2} [0-9]{2}:[0-9]{2}$' ";
                break;
        }

        switch (strtolower($c['operator'])){
            case '=':
                //全等于
                //部门人员须重新定义
                switch ($c['type']){
                    case 'dept':
                    case 'people':
                    case 'school':
                    case 'schoolDept':
                        $tmp_tmp_cond = ' 1=1 ';

                        $value = $this->format_compose_val($c,true);
                        foreach ($value as $v){
                            $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%"'.$v.'"%\'';
                        }

                        $tmp_tmp_cond .= ' and LENGTH('.$c['key'].')=LENGTH(\''.json_encode($value).'\')';

                        $tmp[] = '( '.$tmp_tmp_cond.' )';
                        break;
                    default:
                        $value = $this->format_compose_val($c,false);
                        if(strpos($value,'UNIX_TIMESTAMP')!==false || is_numeric($value) ){
                            $tmp[] = $c['key'] .' = ' . $value;
                        }else{
                            $tmp[] = $c['key'] .' = "' . $value.'"';
                        }

                        break;
                }

                break;
            case '!=':
                //全不等于
                //部门人员须重新定义
                switch ($c['type']){
                    case 'dept':
                    case 'people':
                    case 'school':
                    case 'schoolDept':
                        $tmp_tmp_cond = ' 1=1 ';

                        $value = $this->format_compose_val($c,true);
                        foreach ($value as $v){
                            $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%"'.$v.'"%\'';
                        }

                        $tmp_tmp_cond .= ' and LENGTH('.$c['key'].')=LENGTH(\''.json_encode($value).'\')';

                        $tmp[] = '!( '.$tmp_tmp_cond.' )';
                        break;
                    default:
                        $value = $this->format_compose_val($c,false);
                        if(strpos($value,'UNIX_TIMESTAMP')!==false || is_numeric($value) ){
                            $tmp[] = $c['key'] .' != ' . $value;
                        }else{
                            $tmp[] = $c['key'] .' != "' . $value.'"';
                        }
                        break;
                }

                break;
            case 'in':
                //等于任意一个
                $value = $this->format_compose_val($c,true);
                $tmp_tmp_cond = ' 1=0 ';
                foreach ($value as $v){
                    if($c['type']=='school' || $c['type']=='schoolDept'){
                        $tmp_tmp_cond .= ' or ('.$c['key'] .' like "%\"'.$v.'\"%" and LENGTH('.$c['key'].')>=LENGTH(\''.$v.'\') ) ';
                    }else{
                        $tmp_tmp_cond .= ' or ('.$c['key'] .' like "%'.$v.'%" and LENGTH('.$c['key'].')>=LENGTH(\''.$v.'\') ) ';
                    }
                }

                $tmp[] = '( '.$tmp_tmp_cond.' )';
                break;
            case 'noin':
                //不等于任意一个
                $value = $this->format_compose_val($c,true);
                $tmp_tmp_cond = ' 1=0 ';
                foreach ($value as $v){
                    $tmp_tmp_cond .= ' or ('.$c['key'] .' like "%'.$v.'%" and LENGTH('.$c['key'].')>=LENGTH(\''.$v.'\') ) ';
                }

                $tmp[] = '!( '.$tmp_tmp_cond.' )';
                break;
            case 'like':
                //包含
                //部门人员须重新定义
                $tmp_tmp_cond = ' 1=1 ';
                $changeToIds = $c['type']=='school';
                if($c['type']=='school'){
                    $tmp_tmp_cond = ' 1!=1 ';
                }

                $value = $this->format_compose_val($c,true,$changeToIds);
                foreach ($value as $v){
                    if($c['type']=='dept' || $c['type']=='people' ){
                        $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%"'.$v.'"%\'';
                    }elseif($c['type']=='school'){
                        $tmp_tmp_cond .= ' or '.$c['key'] .' like \'%"'.$v.'"%\'';
                    }else{
                        $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%' . $v. '%\'';
                    }

                }

                $tmp[] = '( '.$tmp_tmp_cond.' )';

                break;
            case 'nolike':
                //不包含
                //部门人员须重新定义
                $tmp_tmp_cond = ' 1=1 ';
                $changeToIds = $c['type']=='school';
                if($c['type']=='school'){
                    $tmp_tmp_cond = ' 1!=1 ';
                }

                $value = $this->format_compose_val($c,true);
                foreach ($value as $v){
                    if($c['type']=='dept' || $c['type']=='people' ){
                        $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%"'.$v.'"%\'';
                    }elseif($c['type']=='school'){
                        $tmp_tmp_cond .= ' or '.$c['key'] .' like \'%"'.$v.'"%\'';
                    }else{
                        $tmp_tmp_cond .= ' and '.$c['key'] .' like \'%' . $v. '%\'';
                    }

                }

                $tmp[] = '!( '.$tmp_tmp_cond.' )';

                break;
            case 'null':
                $tmp[] = $source_key .' = ""';
                break;
            case 'nonull':
                $tmp[] = $source_key .' != ""';
                break;
            case 'between':
                $value = $this->format_compose_val($c,false);
                $data_list = explode(' - ', $value);

                if(isset($data_list[0]) && $data_list[0] !== ""){
                    $tmp[] = "{$c['key']}>={$data_list[0]}  AND {$source_key}!='' ";
                }
                if(isset($data_list[1]) && $data_list[1] !== ""){
                    $tmp[] = "{$c['key']}<={$data_list[1]}  AND {$source_key}!='' ";
                }
                break;
            case 'userwhere':
                $value = $this->format_compose_val($c,true);
                //判断当前管理员是否绑定成员，是否为超管
                if( !in_array('user_all',$value)  ){

                    $tmp_value = [];
                    //获取当前管理员绑定的成员
                    $root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
                    $user_id = $this->user_id;
                    if($user_id){
                        //获取相关人员id ,user_all, user_main, user_sub, user_self
                        foreach ($value as $v){
                            switch ($v){
                                case 'user_main':
                                    $main_dept = g('dao_user')->get_main_dept($user_id,false);
                                    $main_dept_users = g('dao_user')->get_by_main_dept($root_id,array($main_dept['id']),'id');
                                    $main_dept_users && $tmp_value = array_merge($tmp_value,array_column($main_dept_users,'id'));
                                    break;
                                case 'user_sub':
                                    //获取下级成员
                                    $relationship = g('dao_leader') -> get_relationship($this->com_id, $user_id, 1);
                                    foreach ($relationship as $val) {
                                        if($val['sub_type'] == 1){
                                            $tmp_value = array_merge($tmp_value,[$val['sub_id']]);
                                        }else{
                                            $main_dept_users = g('dao_user')->get_by_main_dept($root_id,array($val['sub_id']),'id');
                                            $main_dept_users && $tmp_value = array_merge($tmp_value,array_column($main_dept_users,'id'));
                                        }
                                    }unset($val);
                                    break;
                                case 'user_self':
                                    $tmp_value = array_merge($tmp_value,[$user_id]);
                                    break;
                            }
                        }

                        $tmp_value = array_unique($tmp_value);
                    }

                    if($tmp_value){
                        $tmp[] = $c['key'] .' in ("' . implode('","',$tmp_value) . '")';
                    }else{
                        $tmp[] = ' 1=0 ';
                    }

                }

                break;
            case 'datewhere':
                //时间动态筛选
                $begin_time = 0;
                $end_time = 0;
                $value = $this->format_compose_val($c,false);
                switch ($value){
                    case 'yesterday':
                        $end_time = strtotime(date('Y-m-d'));
                        $begin_time = $end_time  - 24*3600;
                        break;
                    case 'today':
                        $begin_time = strtotime(date('Y-m-d'));
                        $end_time = $begin_time + 24*3600;
                        break;
                    case 'tomorrow':
                        $begin_time = strtotime(date('Y-m-d'))+ 24*3600;
                        $end_time = $begin_time + 24*3600;
                        break;
                    case 'week':
                        $begin_time = strtotime('Sunday -6 day');
                        $end_time = $begin_time+7*24*3600;
                        break;
                    case 'lastweek':
                        $end_time = strtotime('Sunday -6 day');
                        $begin_time = $end_time-7*24*3600;
                        break;
                    case 'nextweek':
                        $begin_time = strtotime('Sunday -6 day')+7*24*3600;
                        $end_time = $begin_time+7*24*3600;
                        break;
                    case 'month':
                        $begin_time = strtotime(date('Y-m-01'));
                        $end_time = strtotime(date('Y-m-01',strtotime('+1 month')) );
                        break;
                    case 'lastmonth':
                        $begin_time = strtotime(date('Y-m-01',strtotime('-1 month')) );
                        $end_time = strtotime(date('Y-m-01'));
                        break;
                    case 'nextmonth':
                        $begin_time = strtotime(date('Y-m-01',strtotime('+1 month')) );
                        $end_time = strtotime(date('Y-m-01',strtotime('+2 month')) );
                        break;
                    case 'quarter':
                        $season = ceil(date('n')/3);
                        $begin_time = strtotime(date('Y-m-01',mktime(0,0,0,($season-1)*3+1,1,date('Y')) ));
                        $end_time = strtotime(date('Y-m-t 23:59:59',mktime(0,0,0,$season*3,1,date('Y')) ))+1;
                        break;
                    case 'lastquarter':
                        $season = ceil(date('n')/3);
                        $end_time = strtotime(date('Y-m-01',mktime(0,0,0,($season-1)*3+1,1,date('Y')) ));
                        $begin_time = strtotime('-3 month',$end_time);
                        break;
                    case 'nextquarter':
                        $season = ceil(date('n')/3);
                        $begin_time = strtotime(date('Y-m-t 23:59:59',mktime(0,0,0,$season*3,1,date('Y')) ))+1;
                        $end_time = strtotime('+3 month',$begin_time);
                        break;
                    case 'year':
                        $begin_time = strtotime(date('Y-01-01'));
                        $end_time = strtotime(date('Y-01-01',strtotime('+1 year')) );
                        break;
                    case 'lastyear':
                        $begin_time = strtotime(date('Y-01-01',strtotime('-1 year')) );
                        $end_time = strtotime(date('Y-01-01'));
                        break;
                    case 'nextyear':
                        $begin_time = strtotime(date('Y-01-01',strtotime('+1 year')) );
                        $end_time = strtotime(date('Y-01-01',strtotime('+2 year')) );
                        break;

                }

                $tmp[] = "{$c['key']}>={$begin_time}";
                $tmp[] = "{$c['key']}<{$end_time}";
                break;
            case '>=':
                $value = $this->format_compose_val($c,false);
                $tmp[] = "{$c['key']}>={$value}  AND {$source_key}!='' ";
                break;
            case '<=':
                $value = $this->format_compose_val($c,false);
                $tmp[] = "{$c['key']}<={$value} AND {$source_key}!='' ";
                break;
            case '>':
                $value = $this->format_compose_val($c,false);
                $tmp[] = "{$c['key']}>{$value}  AND {$source_key}!=''  ";
                break;
            case '<':
                $value = $this->format_compose_val($c,false);
                $tmp[] = "{$c['key']}<{$value} AND {$source_key}!='' ";
                break;

            default:
                return '';
        }


        if($c['is_child']){
            !isset($child_cond[$form_id][$ckey]) && $child_cond[$form_id][$ckey] = array();
            $child_cond[$form_id][$ckey] = array_merge($child_cond[$form_id][$ckey], $tmp);

            if($this->isPrivateInput($ckey)){
                $ckey .= "__0";
            }
            $cond_list[$form_id][] = "REPLACE({$ckey}, '[^]', '') != ''";
        }else{
            $cond_list[$form_id] = array_merge($cond_list[$form_id], $tmp);
        }

    }


    /**
     * 循环内的旧处理方式
     * @return string|void
     */
    private function handle_compose_cond_old($c,&$cond_list,&$child_cond){
        if(!isset($c['value']) || empty($c['value'])){
            return '';
        }
        $key_arr = explode(".", $c['key']);
        if(count($key_arr) != 2){
            //key值错误
            return '';
        }
        $form_id = $key_arr[0];
        !isset($cond_list[$form_id]) && $cond_list[$form_id] = array();
        !isset($child_cond[$form_id]) && $child_cond[$form_id] = array();

        $tmp = array();
        if($c['is_child']){
            $ckey = $key_arr[1];
            $c['key'] = 'val';
        }else{
            $c['key'] = $key_arr[1]."__0";
        }
        if($c['type'] == 'people' || $c['type'] == 'dept'){
            if(!is_array($c['value'])){
                $c['value'] = json_decode($c['value'], TRUE);
            }
            foreach ($c['value'] as $id) {
                $tmp[] = $c['key'] .' LIKE \'%"' .$id.'"%\' ' ;
            }unset($ids);
        }else if($c['type'] == 'date'){
            $data_list = explode(' - ', $c['value']);
            if(isset($data_list[0]) && $data_list[0] !== ""){
                $s_time = strtotime($data_list[0]);
                $tmp[] = "UNIX_TIMESTAMP({$c['key']})>={$s_time}";
            }
            if(isset($data_list[1]) && $data_list[1] !== ""){
                $e_time = strtotime($data_list[1]);
                $tmp[] = "UNIX_TIMESTAMP({$c['key']})<={$e_time}";
            }
        }else if(($c['type'] == 'text' && $c['format'] == 'number') || $c['type'] == 'money'){
            $data_list = explode(' - ', $c['value']);
            if(isset($data_list[0]) && $data_list[0] !== ""){
                $tmp[] = "{$c['key']}>={$data_list[0]}";
            }
            if(isset($data_list[1]) && $data_list[1] !== ""){
                $tmp[] = "{$c['key']}<={$data_list[1]}";
            }
        }else if($c['type'] == 'checkbox'){
            foreach ($c['value'] as $v) {
                $tmp[] = "{$c['key']} like '%{$v}%'";
            }unset($v);
        }else if($c['type'] == 'radio' || $c['type'] == 'select'){
            $t = array();
            foreach ($c['value'] as $v) {
                $t[] = "{$c['key']} like '%{$v}%'";
            }unset($v);
            if(!empty($t)){
                $tmp[] = "(". implode(' OR ', $t) .")";
            }
        }else if($c['type'] == 'text' || $c['type'] == 'textarea'){
            $tmp[] = "{$c['key']} like '%{$c['value']}%'";
        }else{
            return '';
        }
        if($c['is_child']){
            !isset($child_cond[$form_id][$ckey]) && $child_cond[$form_id][$ckey] = array();
            $child_cond[$form_id][$ckey] = array_merge($child_cond[$form_id][$ckey], $tmp);
            $cond_list[$form_id][] = "REPLACE({$ckey}__0, '^', '') != ''";
        }else{
            $cond_list[$form_id] = array_merge($cond_list[$form_id], $tmp);
        }
    }

	/** 根据字符串获取缓存KEY */
	private function get_redis_key($str){
		return md5('REPORT:'. __FUNCTION__ .$str);
	}

	/** 拆分获取计算指标的KEY */
	private function get_function_key($function_key){
		$key_list = array();
		foreach ($function_key as $val) {
			$tmp_key = explode('.', $val);
			$key_list[] = $tmp_key[1];
		}unset($val);
		return $key_list;
	}

    /**
     * 非固定控件
     * @return bool
     */
    private function isPrivateInput($key){
        return FALSE === strpos($key, "creater_id") && FALSE === strpos($key, "create_time") && FALSE === strpos($key, "com_id");
    }


}

// end of file