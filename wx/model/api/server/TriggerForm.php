<?php
/**
 * 触发表单逻辑处理
 * @author gch
 * @date 2018-02-27 11:29:44
 */
namespace model\api\server;

class TriggerForm extends \model\api\server\Base {

	/**
	 * 控件是否可编辑
	 */
	/** 是*/
	protected static $EditInput = 1;
	/** 否*/
	protected static $NotEditInput = 0;
	
	/**
	 * 控件是否可见
	 */
	/** 是*/
	protected static $VisitInput = 1;
	/** 否*/
	protected static $NotVisitInput = 0;

	public function __construct($mod_name='', $log_pre_str='') {
		if (empty($mod_name)) {
			global $mod_name;
		}
		parent::__construct($mod_name, $log_pre_str);
	}

	/**
	* 检查是否开启
	*   
	* @access public
	* @param int $workNodeId 表单节点id  
	* @return int $formId 0-不开启，不为0-开启
	*/
	public function checkOpen($comId, $workNodeId) {
		$formId = 0;

		$nodeInfo = g("mv_work_node") -> get_by_id($comId, $workNodeId, 'trigger_form');
		if (!empty($nodeInfo)) {
			$triggerForm = json_decode($nodeInfo["trigger_form"], TRUE);
			is_array($triggerForm) && isset($triggerForm["form_id"]) && !empty($triggerForm["form_id"]) && $formId = intval($triggerForm["form_id"]);
		}

		return $formId;
	}

	/**
	* 发起触发表单
	*   
	* @access public
	* @param int $createrId 当前流程实例发起人  
	* @param int $triggerFormId 触发表单id  
	* @param int $currentFormsetinstId 当前表单实例id
	* @param int $currWorkitemId 当前流程实例id
	* @return boolean 
	*/
	public function SendTriggerForm($comId, $createrId, $triggerFormId, $currentFormsetinstId, $currWorkitemId, $currHandlerId) {
		$flag = false;
		
		// 获取当前表单实例信息
		$formFields = array(
			'mpf.id', 'mpf.work_id','mpf.formsetinst_name', 'mpf.state', 'mpf.create_time', 'mpfm.id form_id', 
			'mpfm.form_name', 'mpfm.form_describe', 'mpfm.is_linksign', 'mpf.form_vals', 'mpf.info_state', 'mpf.is_other_proc'
		);
		$formFields = implode(',', $formFields);
		$formsetinst = g("mv_formsetinst")->get_formsetinst_by_id($currentFormsetinstId, $comId, $createrId, $formFields);
		
		$formsetinst['form_vals'] = $this -> get_form_vals($currentFormsetinstId);
		to_log('Info', '', '获取当前表单实例信息:' . json_encode($formsetinst));

		// // 获取当前工作步骤信息
		// $currFields = array(
		// 	'mpw.id', 'mpw.is_returnback', 'mpwn.is_start', 'mpw.work_node_id', 'mpwn.file_type',
		// 	'mpw.handler_id', 'mpw.handler_name', 'mpw.creater_id', 'mpw.creater_name', 
		// 	'su.pic_url creater_pic_url', 'mpwn.work_type', 'mpw.state', 'mpwn.work_node_describe', 
		// 	'mpw.info_state', 'mpw.formsetinit_id', 'mpwn.input_visble_rule', 'mpwn.input_visit_rule'
		// );
		// $currFields = implode(',', $currFields);
		// $currWorkitem = g("mv_workinst")->get_curr_workitem($currWorkitemId,$comId,$currHandlerId,$currFields);
		// to_log('Info', '', '获取当前工作步骤信息:' . json_encode($currWorkitem));

		// // 查看那些控件可编辑
		// $this -> get_form_edit($formsetinst['form_vals'], $currWorkitem);
		// $formsetinst['form_vals'] = json_decode($formsetinst['form_vals'], TRUE);
		// to_log('Info', '', '1123123*******');
		
		
		
		return $flag;
	}

	/**
	 * 表单实例控件信息
	 * @param  [type] $formsetinst_id [description]
	 * @return [type]                 [description]
	 */
	public function get_form_vals($formsetinst_id) {
		$input_list = g('mv_formsetinst') -> get_form_vals($formsetinst_id, array());
		if (empty($input_list)) throw new \Exception('获取表单控件失败');

		$form_vals = array();
		foreach ($input_list as $key1 => $input) {
			$tmp_data = array();
			if ($input['p_label_id'] == 0) {
				$tmp_data = array(
					'type' 		=> $input['type'],
					'must' 		=> $input['must'],
					'describe' 	=> $input['describe'],
					'input_key' => $input['input_key'],
					'name' 		=> $input['name'],
					);
				$other = json_decode($input['other'], TRUE);
				$tmp_data = array_merge($tmp_data, $other);

				if ($input['type'] != 'people' && $input['type'] != 'dept' && $input['type'] != 'table') {
					$tmp_data['val'] = $input['val'];
				}


				if ($input['type'] == 'table') {
					// 子表单
					$p_id = $input['id'];
					$rec = array();
					foreach ($input_list as $key2 => $c_input) {
						$c_data = array();
						if ($c_input['p_label_id'] == $p_id) {
							$c_data = array(
								'type' 		=> $c_input['type'],
								'must' 		=> $c_input['must'],
								'describe' 	=> $c_input['describe'],
								'th_key' 	=> $c_input['input_key'],
								'name' 		=> $c_input['name'],
								);
							$c_other = json_decode($c_input['other'], TRUE);
							$c_data = array_merge($c_data, $c_other);

							if ($c_input['type'] != 'people' && $c_input['type'] != 'dept') {
								$c_data['val'] = $c_input['val'];
							}

							$rec[(int)$c_input['label_id']][$c_input['input_key']] = $c_data;
							unset($input_list[$key2]);
						}
					}

					!empty($rec) && $tmp_data['rec'] = $rec;
				}

				$form_vals[$input['input_key']] = $tmp_data;
				unset($input_list[$key1]);
			}

		}
		to_log('Info', '', '表单实例控件信息:' . json_encode($form_vals));
		return $form_vals;
	}

	/**
	 * 获取该节点中那些控件不可编辑
	 * 
	 * @param string $form_vals 表单(字符串)
	 * @param array $work_node 步骤节点信息
	 */
	private function get_form_edit(&$form_vals,$work_node){
		if(empty($work_node)){
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}
				$form_val["visit_input"] = self::$VisitInput;
				$form_val["edit_input"] = self::$NotEditInput;
				if($form_val["type"]=="table"){
					$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
					foreach ($table_arr as $table_key=>&$table_td){
						$table_td["edit_input"] = self::$NotEditInput;
						$table_td["visit_input"] = self::$VisitInput;
					}
					$form_val["table_arr"]=$table_arr;
				}
			} unset($form_val);
			$form_vals = json_encode($form_vals);
		} else {
			$input_visble_rule = $work_node["input_visble_rule"];
			$input_visble_rule = json_decode($input_visble_rule, TRUE);
			
			$input_visit_rule = $work_node["input_visit_rule"];
			$input_visit_rule = json_decode($input_visit_rule, TRUE);
			
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			
			$is_start = $work_node["is_start"];
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}
				if(!empty($input_visit_rule)&&in_array($input_key,$input_visit_rule)){
					$form_val["edit_input"] = self::$NotEditInput;
					$form_val["visit_input"] = self::$NotVisitInput;
				}
				else{
					$form_val["visit_input"] = self::$VisitInput;
					if($is_start == self::$StartNode){
						//开始节点，如果存在，则为不可编辑控件
						if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = self::$NotEditInput;
						}
						else{
							$form_val["edit_input"] = self::$EditInput;
						}
						if($form_val["type"]=="table"){
							//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = self::$NotVisitInput;
									$table_td["edit_input"] = self::$NotEditInput;
								} else {
									$table_td["visit_input"] = self::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = self::$NotEditInput;
									} else {
										$table_td["edit_input"] = self::$EditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;
						}
					} else {
						//其他节点，如果存在，则为可编辑控件
						if (!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = self::$EditInput;
						} else {
							$form_val["edit_input"] = self::$NotEditInput;
						}
						if ($form_val["type"]=="table"){
							//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = self::$NotVisitInput;
									$table_td["edit_input"] = self::$NotEditInput;
								} else {
									$table_td["visit_input"] = self::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = self::$EditInput;
									} else {
										$table_td["edit_input"] = self::$NotEditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;
						}
					}
				}
			} unset($form_val);
			$form_vals = json_encode($form_vals);
		}
	}
}

// end of file