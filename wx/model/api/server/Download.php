<?php
/**
 * 文件下载服务类
 * @author chenyihao
 * @date 2016-06-30
 *
 */
namespace model\api\server;

class Download extends \model\api\server\Base{
	
	//安卓下不支持直接跳转下载的文件列表
    public $n_android_download = array(
        'xml', 'mp4', 
        'zip', 'wav',
    );

    //ios下不支持下载的文件列表
    public $n_ios_download = array(
        'rar', 'wmv', 
        'zip', 'amr',
    );

	//公司id
	private $com_id = NULL;

	public function __construct($log_pre_str='') {
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		empty($log_pre_str) && $log_pre_str = '[download]';

		parent::__construct($log_pre_str);
	}

	/**
	 * 用户下载文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @param string $size 为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
	 * @param boolean $is_push 是否强制通过微信推送
	 * @param boolean $increase 是否增加文件下载数
	 * @return TRUE
	 * @throws SCException
	 */
	/**
	旧下载代码
	public function download_file($app_name, $file_hash, $file_name, $file_ext, $size='', $is_push=false) {
		$platid = $this -> get_mobile_platid();
		$ad_download = g('api_media') -> android_download;
		$file_name = str_replace('.'.$file_ext, "", $file_name);
		$ext = strtolower($file_ext);
		if (!$is_push) {
			//如果是ios平台支持的文件，或者是安卓平台下支持直接下载的文件类型，则直接跳转下载
			if (($platid === 1 and $ext != 'rar' and $ext != 'zip') or ($platid === 2 and in_array($ext, $ad_download))) {
				g('api_media') -> cloud_rename_download($file_name, $file_hash, $size);
				return true;
			}
		}
		
		$file_name = $file_name . $ext;
		try {
			$media_info = g('api_media') -> get_media_id($this -> com_id, $this -> app_conf['combo'], $file_hash, $file_name, $size);
			$media_id = $media_info['media_id'];
		}catch(\Exception $e) {
			throw new \Exception('系统正忙，请稍后再试！');
		}

		$file_type = g('api_media') -> get_file_type($ext);

		g('api_nmsg') -> init($this -> com_id, $app_name);

		if ($file_type == 'video') {
			$result = g('api_nmsg') -> send_video($media_id, '', '', array(), array($_SESSION[SESSION_VISIT_USER_ID]));
		}else {
			$result = g('api_nmsg') -> send_files($file_type, $media_id, array(), array($_SESSION[SESSION_VISIT_USER_ID]));
		}
		
		if (!$result) {
			$err_tip = '系统正忙，请稍后再试！';
			throw new \Exception($err_tip);
		}

		return true;
	}
	*/
	/**
	 * 用户下载文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @param string $size 为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
	 * @param boolean $is_push 是否强制通过微信推送
	 * @param boolean $increase 是否增加文件下载数
	 * @return TRUE
	 * @throws SCException
	 */
	public function download_file($app_name, $file_hash, $file_name, $file_ext, $size='', $is_push=false) {
		try {
			$platid = $this -> get_mobile_platid();
			$ad_download = g('api_media') -> android_download;
			$file_name = str_replace('.'.$file_ext, "", $file_name);
			$ext = strtolower($file_ext);
			if ($platid == 2 && in_array($ext, $this -> n_android_download)) {
				throw new \Exception("手机系统暂不支持{$ext}文件下载，请使用PC端微信进行下载");
			}
			if ($platid == 1 && in_array($ext, $this -> n_ios_download)) {
				throw new \Exception("手机系统暂不支持{$ext}文件下载，请使用PC端微信进行下载");
			}

			$jump = TRUE;
			//TXT文件返回下载链接
			$ext == 'txt' && $jump = FALSE;

			$url = g('api_media') -> cloud_rename_download($file_name, $file_hash, $size, $jump);

			if ($ext == 'txt') {
				return $url;
			} else {
				return TRUE;
			}
		} catch (\Exception $e) {
			parent::log_e('下载文件异常：【' . $e -> getMessage() . '】');
			throw $e;
		}
	}

//====================================内部实现================================
	/**
     * 判断手机平台
     *
     * @access public
     * @return integer 取值：0-其他、1-ios、2-android、3-wp
     */
    private function get_mobile_platid() {
        $agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(iPod|iPad|iPhone)/i', $agent)) {
            return 1;
        }

        if (preg_match('/android/i', $agent)) {
            return 2;
        }

        if (preg_match('/WP/', $agent)) {
            return 3;
        }

        if (preg_match('/Windows/', $agent)) {
            return 4;
        }

        if (preg_match('/Mac/', $agent)) {
            return 5;
        }

        return 0;
    }
	
}

// end of file