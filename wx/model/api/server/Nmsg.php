<?php
/**
 * 常用便捷的消息类
 *
 * @author LiangJianMing
 * @create 2015-03-22
 */
namespace model\api\server;
class Nmsg extends \model\api\server\Base{
	//app的应用配置
	private $app_conf = array();
	//该应用对应的微信应用id
	private $wx_app_id = NULL;
	//公司id
	private $com_id = NULL;
	//公司的access_token
	private $access_token = NULL;
	//企业唯一标识
	private $corp_url = NULL;

	//用户数据表
	public $user_table = 'sc_user';
	//公司信息表
	public $company_table = 'sc_company';
	//公司的应用对应微信应用的映射关系表
	public $app_map_table = 'sc_com_app';
	//我方部门id与微信方部门id的映射关系表
	public $dept_map_table = 'sc_dept_map';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct($log_per_str = '') {
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		empty($log_per_str) && $log_per_str = "[Nmsg][{$this -> com_id}]";
		parent::__construct($log_per_str);
	}

	/**
	 * 使用前必须执行初始化函数
	 *
	 * @access public
	 * @param integer $com_id 公司id，将使用该企业的权限进行消息操作
	 * @param string $app_name 应用名称，仅对该应用进行消息操作
	 * @param string $corp_url 企业唯一标识
	 * @return boolean
	 */
	public function init($com_id = 0, $app_name = '', $corp_url = '') {
		$this -> com_id = $com_id;

		// $app_conf_url = SYSTEM_APPS . $app_name . '/config.php';
		// file_exists($app_conf_url) && $this -> app_conf = include($app_conf_url);

		$this -> app_conf = load_config("model/{$app_name}/app");

		$this -> access_token = NULL;
		$this -> wx_app_id = NULL;
		if (empty($corp_url)) {
			try {
				$com_info = $this -> get_com_info();
				$this -> corp_url = $com_info['corp_url'];
			}catch(\Exception $e) {}
		}else {
			$this -> corp_url = $corp_url;
		}
	}

	/**
	 * 获取当前应用对应的微信应用id
	 *
	 * @access public
	 * @return integer
	 * @throws \Exception
	 */
	public function get_wx_app_id() {
		if (empty($this -> wx_app_id) and (empty($this -> com_id) or empty($this -> app_conf))) {
			throw new \Exception('配置项异常！');
		}

		$com_id = $this -> com_id;
		$app_id = $this -> app_conf['id'];

		$key_str = __FUNCTION__ . ':com_id:' . $com_id . ':app_id:' . $app_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		if (!empty($ret)) {
			$this -> wx_app_id = $ret;
			return $ret;
		}

		$fields = 'wx_app_id';
		$condition = array(
			'com_id=' => $com_id,
			'app_id=' => $app_id,
		);

		$ret = g('pkg_db') -> select($this -> app_map_table, $fields, $condition, 1, 1);

		if (empty($ret)) {
			throw new \Exception('获取微信应用id失败！');
		}

		$ret = $ret[0]['wx_app_id'];
		g('pkg_redis') -> set($cache_key, $ret, 900);

		$this -> wx_app_id = $ret;

		return $ret;
	}

	/**
	 * 获取该企业对应该应用的access_token
	 *
	 * @access public
	 * @return string
	 * @throws \Exception
	 */
	public function get_access_token() {
		if (!empty($this -> access_token)) return $this -> access_token;

		$key_str = __FUNCTION__ . ':com_id:' . $this -> com_id . ':app_conf:' . json_encode($this -> app_conf);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		// if (!empty($ret)) {
		// 	$this -> access_token = $ret;
		// 	return $ret;
		// }
		try{
			$info = $this -> get_com_info();
			if (!empty($this -> app_conf['combo'])) {
				$combo = $this -> app_conf['combo'];
			}else {
				$combo = g('dao_com_app') -> get_sie_id($this -> app_conf['id'], $this -> com_id);
			}
			$conf = array(
					'combo' => $combo,
                    'app_id' => $this -> app_conf['id'],
					'corp_id' => $info['corp_id'],
					'secret' => $info['corp_secret'],
					'auth_codes' => $info['permanent_code'],
					'auth_suites' => $info['permanent_suite_id'],
				);
			$ret = g('api_atoken') -> set_conf($conf) -> get();
		}catch(\Exception $e) {
			throw $e;
		}

		if (empty($ret)) {
			throw new \Exception('获取令牌失败！');
		}

		$this -> access_token = $ret;
		g('pkg_redis') -> set($cache_key, $ret, 3600);

		return $ret;
	}

	/**
	 * 获取企业的信息
	 *
	 * @access public
	 * @return array
	 * @throws \Exception
	 */
	public function get_com_info() {
		$com_id = $this -> com_id;
		if (empty($com_id)) {
			throw new \Exception('获取企业信息失败！');
		}

		$key_str = __FUNCTION__ . ':com_id:' . $com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		if (!empty($ret)) {
			$ret = json_decode($ret, TRUE);
			return $ret;
		}

		$fields = array(
			'corp_id', 'corp_secret', 'permanent_code',
			'corp_url', 'permanent_suite_id',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'id=' => $com_id,
		);

		$ret = g('pkg_db') -> select($this -> company_table, $fields, $condition, 1, 1);
		if (empty($ret)) {
			throw new \Exception('获取企业信息失败！');
		}

		$ret = $ret[0];
		g('pkg_redis') -> set($cache_key, json_encode($ret), 1800);

		return $ret;
	}

	/**
	 * 发送图文消息
	 *
	 * @access public
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @param array $art_list 需要发送的图文消息集合
	 * @return boolean
	 */
	public function send_news(array $dept_list=array(), array $user_list=array(), array $art_list=array()) {
		foreach ($art_list as &$val) {
			!empty($val['url']) and $val['url'] .= ('&corpurl=' . $this -> corp_url);
		}
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = !empty($dept_list) ? $this -> get_wx_depts($dept_list) : array();
			$user_list = !empty($user_list) ? $this -> get_wx_accts($user_list) : array();
			g('wxqy_msg') -> send_news($access_token, $user_list, $wx_app_id, $art_list, array(), $dept_list);
			return TRUE;
		}catch(\Exception $e) {
			parent::log_w('发送图文消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送文本消息
	 *
	 * @access public
	 * @param string $content 文本内容
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_text($content, array $dept_list=array(), array $user_list=array()) {
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy_msg') -> send_text($access_token, $user_list, $wx_app_id, $content, array(), $dept_list);
		}catch(\Exception $e) {
			parent::log_w('发送文本消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送多种文件
	 *
	 * @access public
	 * @param string $type 微信方资源类型，允许取值：file、image、voice
	 * @param string $media_id 推送到微信方资源id
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_files($type, $media_id, array $dept_list=array(), array $user_list=array()) {
		try {
			$type == 'image' and $type = 'img';
			$func = 'send_' . $type;

			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy_msg') -> $func($access_token, $user_list, $wx_app_id, $media_id, array(), $dept_list);
		}catch(\Exception $e) {
			parent::log_w('发送' . $type . '消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送视频
	 *
	 * @access public
	 * @param string $media_id 推送到微信方资源id
	 * @param string $title 标题
	 * @param string $desc 简介
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_video($media_id, $title, $desc, array $dept_list=array(), array $user_list=array()) {
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy_msg') -> send_video($access_token, $user_list, $wx_app_id, $media_id, $title, $desc, array(), $dept_list);
		}catch(\Exception $e) {
			parent::log_w('发送视频消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 根据部门id集合，获取微信方的部门id集合
	 *
	 * @access public
	 * @param array $dept_list 部门id集合
	 * @return array
	 */
	public function get_wx_depts(array $dept_list) {
		if (empty($dept_list)) return array();

		sort($dept_list);
		$key_str = __FUNCTION__ . ':dept_list:' . json_encode($dept_list);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		if (!empty($ret)) {
			return json_decode($ret, TRUE);
		}

		$fields = 'wx_id';
		$condition = array(
			'id IN' => $dept_list,
		);

		$ret = array();
		$result = g('pkg_db') -> select($this -> dept_map_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[] = $val['wx_id'];
			}
			unset($val);
			return $ret;
		}

		!empty($ret) && g('pkg_redis') -> set($cache_key, json_encode($ret), 1800);
		return $ret;
	}

	/**
	 * 根据用户id集合，获取微信方的用户账号集合
	 *
	 * @access public
	 * @param array $user_ids 用户id集合
	 * @return array
	 */
	public function get_wx_accts(array $user_ids) {
		if (empty($user_ids)) return array();
		
		sort($user_ids);
		$key_str = __FUNCTION__ . ':user_ids:' . json_encode($user_ids);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);
		if (!empty($ret)) {
			return json_decode($ret, TRUE);
		}

		$fields = 'acct';
		$condition = array(
			'id IN' => $user_ids,
		);

		$ret = array();
		$result = g('pkg_db') -> select($this -> user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[] = $val['acct'];
			}
			unset($val);
			return $ret;
		}
		
		!empty($ret) && g('pkg_redis') -> set($cache_key, json_encode($ret), 1800);
		return $ret;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符
	 * @return string
	 */
	public function get_cache_key($str = '') {
		$key_str = MAIN_COOKIE_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':'.$str;
		$key = md5($key_str);
		return $key;
	}
}
//end