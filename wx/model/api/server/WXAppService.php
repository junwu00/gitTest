<?php
/**
 * 微信端应用请求响应的基础类
 * @author yangpz
 * @date 2014-10-21
 *
 */
namespace model\api\server;

abstract class WXAppService extends \model\api\server\Base{
	protected $post_data;
	/** 企业ID	*/
	protected $com_id;
	/** 企业根部门的 ID */
	protected $root_id;
	/** 应用 ID */
	protected $app_id;
	/** 应用英文名称 */
	protected $app_name;
	/** 应用所属套餐的类型 */
	protected $app_combo;
	
	/** 不推送操作指引的企业的com_id */
	private $not_send_guide_com_list;
	
	public function __construct($log_pre_str= '') {
		$this -> not_send_guide_com_list = load_config('model/api/guide_send');
		empty($log_pre_str) && $log_pre_str = "[WXAppService]";
		parent::__construct($log_pre_str);
	}
	
	public function init($app_name, $com_id, $root_id) {
		$app_conf = load_config("model/{$app_name}/app");
		
		$this -> app_id 	= $app_conf['id'];
		$this -> app_name 	= $app_name;
		$this -> app_combo 	= $app_conf['combo'];
		$this -> com_id 	= $com_id;
		$this -> root_id 	= $root_id;
	}
	
	public function do_reply($post_data) {
		$this -> post_data = $post_data;
		
		//根据消息类型进行回复
		switch($this -> post_data['MsgType']) {
			case 'text':
				$this -> reply_text();
				break;
				
			case 'image':
				$this -> reply_image();
				break;
				
			case 'event':
				$this -> reply_event();
				break;
				
			default:
				break;
		}
	}
	
	//--------------------------------------abstract-------------------------------------------
	
	/**
	 * 响应文本请求
	 */
	protected function reply_text() {}

	/**
	 * 响应图片请求
	 */
	protected function reply_image() {}
	
	/**
	 * 处理地理位置上报
	 */
	protected function reply_location() {}

	/**
	 * 对指定用户回复文本请求
	 *
	 * @access protected
	 * @return void
	 */
	protected function reply_click() {}
	
	/**
	 * 扫码等待事件
	 */
	protected function reply_scancode_waitmsg() {}
	
	/**
	 * 进入应用
	 */
	protected function enter_advice() {}
	
	/** 关注事件 */
	protected function reply_subscribe() {}

	//---------------------------------------------内部实现-----------------------------------
	
	/**
	 * 响应事件请求
	 */
	private function reply_event() {
		switch ($this -> post_data['Event']) {
            case 'subscribe':			//关注
				if (in_array($this -> com_id, $this -> not_send_guide_com_list))	return;
				
				//配置为推送时才推送
				$need_send_guide = g('com_app') -> is_send_guide($this -> com_id, $this -> app_id);
				if ($need_send_guide) {			
	            	$this -> reply_subscribe();
				}	
            	break;
            	
            case 'unsubscribe':			//取消关注
            	break;
            	
            case 'LOCATION':			//地理位置
            	$this -> reply_location();
            	break;
            	
            case 'enter_agent':			//进入应用
            	$this -> enter_advice();
            	break;
            	
            case 'click':				//点击菜单
            	$this -> reply_click();
            	break;
            	
            case 'VIEW':				//点击菜单链接
            	break;
            	
            case 'scancode_push':		//扫码
            	break;
            	
            case 'scancode_waitmsg': 	//扫码推事件且弹出“消息接收中”提示框
            	$this -> reply_scancode_waitmsg();
            	break;
            	
            case 'pic_sysphoto':		//弹出系统拍照发图
            	break;
            	
            case 'pic_photo_or_album':	//弹出拍照或者相册发图
            	break;
            	
            case 'pic_weixin':			//弹出微信相册发图器
            	break;
            	
            case 'location_select':		//弹出地理位置选择器
            	break;
		}
	}
	
} 

// end of file