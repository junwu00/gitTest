<?php
/**
 * 请求的响应/输出
 * @author yangpz
 * @date 2016-02-16
 *
 */
namespace model\api\server;

class Response{
	/** 成功 */
	public static $OK	 				= 0;	
	/** 未知原因（系统繁忙） */
	public static $Busy					= -99999;	
	
	/** 提示 */
	public static $Notice				= -90001;	
	/** 警告 */
	public static $Warn					= -90002;	
	/** 错误 */
	public static $Error				= -90003;	
	
	/** 错误码对应中文解释 */
	public static $ErrorCodeMap = array(
		0		=> '成功',
		-99999	=> '系统繁忙',
		
		-90001	=> '提示',
		-90002	=> '警告',
		-90003	=> '错误',
	);
	
	/**
	 * 输出异常信息
	 * @param unknown_type $e			exception对象
	 * @param unknown_type $ext_data	附加信息(支持覆盖默认信息)
	 */
	public function echo_exp($e, array $ext_data=array()) {
		$code = $e -> getCode();
		$data = array(
			'errcode' 	=> empty($code) ? -999 : $code,
			'errmsg' 	=> preg_replace('/[\d]+[,，]/', '', $e -> getMessage()),		//去掉错误码
		);
		$data = $this -> _merge_ext_data($data, $ext_data);

		RobotReport::reportMessage( urldecode(json_encode(urlencode_array($data))) );
		echo urldecode(json_encode(urlencode_array($data)));	//异常信息，使中文可正常显示
		exit();
	}
	
	/**
	 * 返回操作失败
	 * @param unknown_type $errcode	错误码
	 * @param unknown_type $errmsg	错误描述
	 */
	public static function echo_err($errcode, $errmsg, $data_name=NULL, $data=NULL) {
		$json = array(
			'errcode' => $errcode,
			'errmsg' => $errmsg,
		);
		!is_array($data) && $data = array();
		if (!is_null($data_name) && !empty($data_name)) {
			$json[$data_name] = $data;
		}

		RobotReport::reportMessage(urldecode(json_encode(urlencode_array($json))));
		echo urldecode(json_encode(urlencode_array($json)));	//非成功信息，使中文可正常显示
		exit();
	}

	/**
	 * 通用信息输出
	 * @param unknown_type $errcode	错误码，默认为成功
	 * @param array $ext_data		附加信息(支持覆盖默认信息)
	 */
	public function echo_resp($errcode=NULL, array $ext_data=array()) {
		is_null($errcode) && $errcode = self::$OK;
		$errcode = intval($errcode);
		
		$data = array(
			'errcode' 	=> $errcode,
			'errmsg' 	=> isset(self::$ErrorCodeMap[$errcode]) ? self::$ErrorCodeMap[$errcode] : '',
		);
		$data = $this -> _merge_ext_data($data, $ext_data);
		
		if ($errcode != self::$OK) {

		    RobotReport::reportMessage(urldecode(json_encode(urlencode_array($data))));
			echo urldecode(json_encode(urlencode_array($data)));	//非成功信息，使中文可正常显示
			
		} else {
			echo json_encode($data);
		}
		exit();
	}
	
	/**
	 * 输出成功信息
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	public function echo_ok(array $ext_data=array(), $errcode=NULL) {
		is_null($errcode) && $errcode = self::$OK;
		$this -> echo_resp($errcode, $ext_data);
	}
	
	/**
	 * 输出系统繁忙
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	public function echo_busy(array $ext_data=array(), $errcode=NULL) {
		is_null($errcode) && $errcode = self::$Busy;
		$this -> echo_resp($errcode, $ext_data);
	}
	

	/**
	 * 重定向到错误页面
	 * @param unknown_type $reason_arr
	 */
	public static function show_err_page($reason_arr) {
		if (!is_array($reason_arr)) {
			$reason_arr = array($reason_arr);
		}

        RobotReport::reportMessage(json_encode($reason_arr,JSON_UNESCAPED_UNICODE));

		g('smarty') -> assign('reason', $reason_arr);
		//错误页面未迁移
		g('smarty') -> show('error/index.html');
		die();
	}

	/**
	 * 显示提示页面
	 */
	public static function show_warn_page($reason_arr, $title=NULL, $extend_html=NULL) {
		if (!is_array($reason_arr)) {
			$reason_arr = array(strval($reason_arr));
		}
		
		g('smarty') -> assign('title', empty($title) ? '提示' : strval($title));
		!empty($extend_html) && g('smarty') -> assign('extend_html', $extend_html);
		g('smarty') -> assign('reason', $reason_arr);
		//提示页面未迁移
		g('smarty') -> show('warn_index.html');
		die();
	}

	//内部实现-------------------------------------------------------
	
	/**
	 * 合并其他自定义输出信息(直接使用array_merge效率可能较低)
	 * @param unknown_type $data		默认带有的字段信息
	 * @param unknown_type $ext_data	自定义的其他信息
	 */
	private function _merge_ext_data(array $data, array $ext_data) {
		if (empty($ext_data))	return $data;
		
		foreach ($ext_data as $key => $item) {
			$data[$key] = $item;
		}unset($key);unset($item);
		return $data;
	}
	
}

//end