<?php
/**
 * 访问统计入口
 * 
 * @author chenyihao
 * @date 2016-7-15
 *
 */
namespace model\api\server;
class Access extends \model\api\server\ServerBase{
	/** 访问记录表 */
	private $Table = 'sc_access';

	/** 应用缓存信息 */
	private $App_ids = array();

	public function __construct($log_pre_str=''){
		$this -> App_ids = $this -> get_app_info();
		global $mod_name;
		parent::__construct($mod_name, $log_pre_str);
	}

	/**
	 * 统计
	 * @return 
	 */
	public function count(){
		$app = g('pkg_val')-> get_get('app');
		empty($app) && $app = g('pkg_val')-> get_get('model');
		empty($app) && $app = 'index';

		$m = g('pkg_val')-> get_get('m');
		empty($m) && $m = 'index';

		$a = g('pkg_val')-> get_get('a');
		empty($a) && $a = 'index';

		$cmd = g('pkg_val')-> get_value('cmd');
		empty($cmd) && $cmd = g('pkg_val')-> get_value('ajax_act');
		empty($cmd) && $cmd = 'index';

		$link = "{$app}|{$m}|{$a}|{$cmd}";

		$app_id = isset($this -> App_ids[$app]) ? $this -> App_ids[$app] : 0;

		$create_time = time();
		$date_str = date('Y-m-d', $create_time);
		$date = explode('-', $date_str);
		$y = $date[0];
		$m = $date[1];
		$d = $date[2];

		$ip = g('pkg_ip') -> get_ip();

		$com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;
		$user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : 0;

		if (isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) === 'get') {
			$method = 'GET';
		}else{
			$method = 'POST';
		}

		$data = array(
				'com_id' => $com_id,
				'user_id' => $user_id,
				'app_id' => $app_id,
				'app_name' => $app,
				'link' => $link,
				'type' => REDIS_ACCESS_TYPE,	//访问客户端
				'method' => $method,
				'ip' => $ip,
				'year' => $y,
				'month' => $m,
				'day' => $d,
				'create_time' => $create_time
			);

		$this -> save($data);
	}

	/**
	 * 从redis中获取访问信息
	 * @param   $data
	 * @return 
	 */
	public function get(){
		$key = $this -> get_redis_key();
		$data = g('pkg_redis') -> pop($key);
		$data = json_decode($data, true);
		return $data;
	}

	/**
	 * 从redis中获取所有访问信息
	 * @param   $data
	 * @return 
	 */
	public function get_all(){
		$key = $this -> get_redis_key();
		$all_data = array();
		while($record = g('pkg_redis') -> pop($key)){
			$record = json_decode($record, true);
			array_push($all_data, $record);
		}
		return $all_data;
	}


	/**
	 * 批量插入数据
	 * @param   $records
	 * @return 
	 */
	public function batch_insert($records){
		$fields = array('com_id','user_id','app_id','app_name','link', 'type', 'method', 'ip','year','month','day','create_time');
		$data = array();
		foreach ($records as $val) {
			$tmp = array();
			foreach ($fields as $f) {
				$tmp[$f] = $val[$f];
			}
			array_push($data, $tmp);
		}

		// $fields = implode(',', $fields);

		$ret = g('pkg_db') -> batch_insert($this -> Table, $fields, $data);
		return $ret;
	}

//===============================内部实现=============================
	/**
	 * 将访问信息保存到redis
	 * @param   $data
	 * @return 
	 */
	private function save($data){
		$data = json_encode($data);
		$key = $this -> get_redis_key();
		g('pkg_redis') -> push($key, $data);
	}

	/**
	 * 获取缓存KEY
	 * @return 
	 */
	private function get_redis_key(){
		return REDIS_ACCESS_KEY;
	}

	/**
	 * 从缓存中获取应用信息（额外保存index = 0）
	 * @return 
	 */
	private function get_app_info(){
		$key = __function__ . '_app_info_access';
		$key = md5($key);

		$app_info = g('pkg_redis') -> get($key);
		if(empty($app_info)){
			$all_app_info = g('dao_app') -> get_all_app();
			$app_info = array();
			foreach ($all_app_info as $val) {
				$app_info[$val['name']] = $val['id'];
			}unset($val);
			unset($all_app_info);
			$app_info['index'] = 0;
			g('pkg_redis') -> set($key, json_encode($app_info), 60*60);
		}else{
			$app_info = json_decode($app_info, true);
		}
		return $app_info;
	}

}

// end of file