<?php
/**
 * 公用提示页面
 * @author yangpz
 * @date 2016-04-12
 *
 */
namespace model\api\view;

class ViewNoticeBase extends \model\api\view\ViewBase {
	
	public function __construct($mod_name='api', $log_str_pre='') {
		parent::__construct($mod_name, $log_str_pre);
	}
	
	public function notice($notice) {
		!is_array($notice) && $notice = array($notice);
		$this -> assign('notice_arr', $notice);
		$this -> assign('title', '提示');
		$this -> show($this -> view_dir . 'page/notice.html');
	}
	
	public function warn($warn) {
		!is_array($warn) && $warn = array($warn);
		$this -> assign('warn_arr', $warn);
		$this -> assign('title', '警告');
		$this -> show($this -> view_dir . 'page/warn.html');
	}
	
	public function error($error) {
		!is_array($error) && $error = array($error);
		$this -> assign('error_arr', $error);
		$this -> assign('title', '错误');
		$this -> show($this -> view_dir . 'page/error.html');
	}
}

//end