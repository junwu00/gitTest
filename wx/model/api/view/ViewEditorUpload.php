<?php

/**
 * 视图基础类
 * @author yangpz
 * @date 2016-04-12
 *
 */

namespace model\api\view;

class ViewEditorUpload extends \model\api\view\ViewBase {
	
	public function __construct($mod_name='api', $log_str_pre='') {
		parent::__construct($mod_name, $log_str_pre);
	}
	
	public function index() {
		$uploader_dir = dirname(__DIR__) . DS . 'editor_upload' . DS;
		include $uploader_dir . 'controller.php';
	}
	
}

//end