<?php

/**
 * 视图基础类
 * @author yangpz
 * @date 2016-04-12
 *
 */
namespace model\api\view;

use Rtgm\sm\RtSm2;

abstract class ViewBase {
	
	/** 当前访问用户的ID */
	protected $user_id = 0;
	protected $com_id = 0;

	/** 当前模块对应的视图的根目录 */
	protected $view_dir;
	/** 当前模块名 */
	protected $mod_name;
	/** 当前模块链接 */
	protected $mod_url;
	
	/** 当前模块对应的日志存放的根目录 */
	protected $log_dir;
	/** 当前模块需记录的日志级别 */
	protected $log_lev;
	/** 当前模块对应的正常/警告日志的路径 */
	protected $log_file_path;
	/** 当前模块对应的错误日志的路径 */
	protected $err_file_path;
	/** 日志中一般要补充的前置的信息，如：[com:123][admin:123] */
	protected $log_pre_str;
	
	/** 应用所在套件ID */
	protected $suite_id;
	/** 应用ID */
	protected $app_id;

	private static $VIP_REDIS_KEY = 'VIP_REDIS_KEY::';

	public function __construct($mod_name, $log_pre_str='') {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : 0;
		
		if (empty($log_pre_str) && isset($_SESSION[SESSION_VISIT_USER_ID])) {
			$log_pre_str = "[com_id:{$this -> com_id}][user_id:{$this -> user_id}]";
		}
		$mod_conf = load_config("model/{$mod_name}/model");			//加载应用配置文件
		// $errcode_conf = load_config("model/api/errcode");				//加载全局错误码文件
		
		$this -> view_dir = $mod_conf['view_dir'];
		$this -> mod_name = $mod_name;
		$this -> mod_url = MAIN_DYNAMIC_DOMAIN . 'index.php?' . MAIN_MODEL_VAR . '=' . $this -> mod_name;
		
		$today = date('Ymd');
		$this -> log_pre_str 	= $log_pre_str;
		$this -> log_dir 		= $mod_conf['log_dir'];
		$this -> log_lev 		= $mod_conf['log_lev'];
		$this -> suite_id 		= $mod_conf['suite_id'];
		$this -> app_id 		= $mod_conf['app_id'];
		$this -> log_file_path	= $this -> log_dir . DS . $today . '.log';
		$this -> err_file_path	= $this -> log_dir . DS . $today . '.error.log';
		
		if(isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) === 'get'){
			$this -> _assign_static_res_conf();
			$this -> _assign_common($mod_name);
			$this -> _assign_config();
		}
	}
	
	protected function assign($key, $val) {
		g('pkg_smarty') -> assign($key, $val);
	}
	
	protected function show($path) {
		if (!file_exists($path)) {
			die('smarty template not found');
		}
		g('pkg_smarty') -> show($path);
	}
	
	/**
	 * 获取POST请求的数据
	 * @param array $need	必带字段
	 */
	protected function get_post_data(array $need=array()) {
		$data = g('pkg_val') -> get_post('data', FALSE);
        if (is_null($data)) {
			$this -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW);
		}

        $config = load_config("model/api/sm2");
        //判断是否未json字符串，否则进行解密操作
        if(!is_json($data) && !is_array($data) && $config['enable']){
            $rtSm2 = new RtSm2();
            $data = $rtSm2->doDecrypt($data,$config['privateKey']);
        }

		!is_array($data) && $data = json_decode($data, TRUE);
		empty($data) && $data = array();
		if (!is_array($data)) {
			$this -> echo_resp(\ERRCODE_API::$REQUEST_NOT_ALLOW);
		}
		
		$keys = array_keys($data);
		foreach ($need as $field) {	//验证必备字段
			if (!in_array($field, $keys)) {
				$this -> echo_resp(\ERRCODE_API::$REQUEST_PARAMS_NOT_COMPLETE);
			}
		}unset($field);
		return $data;
	}

	/**
	 * 获得分页信息
	 * @return 
	 */
	public function get_pageinfo($curr_page, $page_size, $total_count, $curr_count) {
		$info = array(
			'curr_page' => $curr_page,
			'page_size' => $page_size,
			'total_count' => $total_count,
			'curr_count' => $curr_count,
			'total_page' => ceil($total_count / $page_size),
		);
		return $info;
	}
	
	
	//请求响应/输出==============================================================================================
	
	/**
	 * 通用信息输出
	 * @param unknown_type $errcode	错误码，默认为成功
	 * @param array $ext_data		附加信息(支持覆盖默认信息)
	 */
	protected function echo_resp($errcode=NULL, array $ext_data=array()) {
		g('api_resp') -> echo_resp($errcode, $ext_data);
	}
	
	/**
	 * 输出异常信息
	 * @param \Exception $e			exception对象
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_exp($e, array $ext_data=array()) {
		g('api_resp') -> echo_exp($e, $ext_data);
	}
	
	/**
	 * 输出自定义的错误信息
	 * @param int $errcode			错误码
	 * @param string $errmsg			错误信息
	 */
	protected function echo_err($errcode, $errmsg, $data_name=NULL, $data=NULL) {
		g('api_resp') -> echo_err($errcode, $errmsg, $data_name, $data);
	}

	/**
	 * 输出成功信息
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_ok(array $ext_data=array(), $errcode=NULL) {
		g('api_resp') -> echo_ok($ext_data, $errcode);
	}
	
	/**
	 * 输出系统繁忙
	 * @param array $ext_data	附加信息(支持覆盖默认信息)
	 */
	protected function echo_busy(array $ext_data=array(), $errcode=NULL) {
		g('api_resp') -> echo_busy($ext_data, $errcode);
	}
	
	//END 请求响应/输出===========================================================================================
	
	//分级日志记录：由配置文件中log_lev参数控制===========================================================================================
	
	/** 调试日志记录 */
	protected function log_d($str) {
		if ($this -> log_lev > 0)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_DEBUG'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 正常日志记录 */
	protected function log_i($str) {
		if ($this -> log_lev > 1)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_INFO'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 需突出提示的日志记录 */
	protected function log_n($str) {
		if ($this -> log_lev > 2)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_NOTICE'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
	}
	
	/** 警告类日志记录 */
	protected function log_w($str) {
		if ($this -> log_lev > 3)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_WARN'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 错误类日志记录 */
	protected function log_e($str) {
		if ($this -> log_lev > 4)		return;
		
		$level = $GLOBALS['levels']['MAIN_LOG_ERROR'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	/** 致使错误级别的日志记录 */
	protected function log_f($str) {
		$level = $GLOBALS['levels']['MAIN_LOG_FATAL'];
		to_log($level, $this -> log_file_path, $this -> log_pre_str . $str);
		to_log($level, $this -> err_file_path, $this -> log_pre_str . $str);
	}
	
	//END 分级日志记录===========================================================================================
	
	//员工选人控件相关逻辑代码===================================================================

	/** 设置选人控件部门 */
	public function assign_contact_dept($st_filter=FALSE) {	//默认不执行架构过滤
		// gch add contact config
		$watchAll = g('dao_contact_config') -> checkWatchAll($this -> com_id, $this -> user_id);
		if ($watchAll) {
			$data = array(
				'data' => array(
						'user_dept' => array(),
						'child_dept_url' => get_this_url().'&a=contact_load_sub_dept',
						'dept_user_url'	=> get_this_url().'&a=contact_list_user',
						'init_url' => get_this_url().'&a=contact_load_by_ids',
						'user_detail_url' => get_this_url().'&a=contact_load_user_detail',
						'dept_list' => array(),
						'group_list' => array(),
						'group_user_url' => get_this_url().'&a=contact_load_group_user',
						'base_url' => get_this_url(),
					)
				);
			g('api_resp') -> echo_ok($data);
		}

		if ($st_filter) {
			$root_dept = g('dao_share_struct') -> get_top_depts();

			$d_ids = array();
			foreach ($root_dept as $d) {
				$d['id'] > 0 && $d_ids[] = $d['id'];
			}unset($d);
			$has_childs = g('dao_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
			foreach ($root_dept as &$dept) {
				if ($dept['id'] <= 0) {
					$dept['has_child'] = TRUE;
					$dept['virtual'] = TRUE;
					
				} else {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
				if (isset($dept['children']) && !empty($dept['children'])) {
					$dept['childs'] = $dept['children'];
					
					$c_ids = array();
					foreach ($dept['children'] as $d) {
						$c_ids[] = $d['id'];
					}unset($d);
					$c_has_childs = g('dao_dept') -> list_has_child_by_ids($c_ids);		//是否有子部门
					
					foreach ($dept['children'] as $key => $c) {
						$dept['childs'][$key]['has_child'] = $c_has_childs[$c['id']];
					}unset($key);unset($c);
					unset($dept['children']);
				}
			}
			
		} else {
			$root_dept = g('dao_dept') -> list_by_lev(1, 'id, p_id, lev, name, idx');
			!empty($root_dept) && $root_dept = $root_dept[0];
			
			$dept_list = g('dao_dept') -> list_by_lev(2, 'id, p_id, lev, name, idx');
			if (!empty($dept_list)) {			
				$d_ids = array();
				foreach ($dept_list as $d) {
					$d_ids[] = $d['id'];
				}unset($d);
				$has_childs = g('dao_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
				$root_dept['childs'] = $dept_list;
				foreach ($dept_list as &$dept) {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
				$root_dept['childs'] = $dept_list;
			}
			!empty($root_dept) && $root_dept = array($root_dept);
		}
		$user_dept = g('dao_dept') -> get_by_id($_SESSION[SESSION_VISIT_USER_DEPT], 'id,name');
		// gch add
		$group_list = g('dao_group') -> get_group_tree($_SESSION[SESSION_VISIT_COM_ID], 'id,group_name', $this -> user_id);

		$data = array(
			'data' => array(
					'user_dept' => $user_dept,
					'child_dept_url'	=> get_this_url().'&a=contact_load_sub_dept',
					'dept_user_url'	=> get_this_url().'&a=contact_list_user',
					'init_url'	=> get_this_url().'&a=contact_load_by_ids',
					'user_detail_url' => get_this_url().'&a=contact_load_user_detail',
					'dept_list' => $root_dept,
					'group_list' => $group_list,
					'group_user_url'	=> get_this_url().'&a=contact_load_group_user',
					'base_url' => get_this_url(),
				)
			);
		g('api_resp') -> echo_ok($data);
		
	// 	g('pkg_smarty') -> assign('c_search_user_dept', $user_dept);
	// 	g('pkg_smarty') -> assign('c_search_dept_url', get_this_url().'&a=contact_load_sub_dept');
	// 	g('pkg_smarty') -> assign('c_search_user_url', get_this_url().'&a=contact_list_user');
	// 	g('pkg_smarty') -> assign('c_search_user_init_url', get_this_url().'&a=contact_load_by_ids');
	// 	g('pkg_smarty') -> assign('c_search_user_detail_url', get_this_url().'&a=contact_load_user_detail');
	// 	g('pkg_smarty') -> assign('c_search_base_url', get_this_url());
	// 	g('pkg_smarty') -> assign('c_search_dept', $root_dept);
	}
	

	/** 员工选择控件，分页查找人员 */
	public function contact_list_user() {
		try {
			// gch add contact config
			$watchAll = g('dao_contact_config') -> checkWatchAll($this -> com_id, $this -> user_id);
			if ($watchAll) {
				throw new \Exception("您没有权限查看通讯录");
			}
			
			$need = array('name', 'dept', 'offset', 'limit');
			$data = $this -> get_post_data($need);
			
			$cond = array();
			if ($this -> mod_name === 'contact') {	//通讯录应用，使用过滤
				g('dao_user_general') -> set_struct();
				g('dao_user_general') -> set_struct();
			}
			
			$fields = array(
	            'root_id', 'id', 'name', 'all_py', 'first_py', 'pic_url'
	        );
	        $fields = implode(',', $fields);
			$page = $data['offset'] == 0 ? 1 : (ceil($data['offset'] / $data['limit']) + 1);
			$params = array(
				'keyword' => $data['name']
			);
			// $data['dept'] == 0 && $data['dept'] = $_SESSION[SESSION_VISIT_DEPT_ID];
			$dept_ids = explode(',', $data['dept']);
			$dept_ids = array_filter($dept_ids);
			if (empty($dept_ids)) {
				$dept_ids = array($_SESSION[SESSION_VISIT_DEPT_ID]);
			}

			$ret = g('dao_user') -> page_list_by_dept_ids($dept_ids, $page, $data['limit'], $params, $fields, ' all_py ');
			$data = array(
					'info' => $ret,
				);

			g('api_resp') -> echo_ok($data);
			
		} catch (\Exception $e) {
			g('api_resp') -> echo_exp($e);
		}
	}
	
	/** 员工选择控件：加载员工详情信息 */
	public function contact_load_user_detail() {
		try {
			$need = array('user');
			$data = $this -> get_post_data($need);
			
			$fields = 'id, name, acct, pic_url, gender, position, dept_list, all_py, first_py';
			if (isset($data['ext_attr']) && !empty($data['ext_attr'])) {
				$fields .= ','.implode(',', $data['ext_attr']);
			}
			$ret = g('dao_user') -> get_by_id($data['user'], $fields);
			if ($ret) {
				$ret['dept_list'] = json_decode($ret['dept_list'], TRUE);
				$ret['dept_list'] = g('dao_dept') -> list_by_ids($ret['dept_list'], 'id, name');
			}
			
			if (isset($data['chk_general']) && $data['chk_general'] == 1) {	//判断是否为常用联系人
				$general = g('dao_user_general') -> get_by_ids(array($_SESSION[SESSION_VISIT_USER_ID]), 'general_list');
				$general = $general ? json_decode($general[0]['general_list'], TRUE) : array();
				$ret['general'] = in_array($ret['id'], $general, FALSE);
			}

			$data = array(
					'info' => $ret
				);

			g('api_resp') -> echo_ok($data);
			
		} catch (\Exception $e) {
			g('api_resp') -> echo_exp($e);
		}
	}
	
	/** 加载已选择的员工的数据（用于初始化数据） */
	public function contact_load_by_ids() {
		try {
			$need = array();
			$data = $this -> get_post_data($need);
			$result = array();
			
			$depts = isset($data['dept']) ? $data['dept'] : array();
			$users = isset($data['user']) ? $data['user'] : array();
			if(!empty($users)){
				!is_array($users) && $users = json_decode($users, TRUE);
				$ret = g('dao_user') -> list_by_ids($users, 'id,name,pic_url');
				if(!empty($ret)) foreach ($ret as &$val) {
					$val['isDept'] = FALSE;
				}
				$result = array_merge($result, $ret);
			}
			if(!empty($depts)){
				!is_array($depts) && $depts = json_decode($depts, TRUE);
				$ret = g('dao_dept') -> list_by_ids($depts, 'id,name');
				if(!empty($ret)) foreach ($ret as &$val) {
					$val['isDept'] = TRUE;
				}
				$result = array_merge($result, $ret);
			}

			$data = array(
					'info' => $result,
				);
			g('api_resp') -> echo_ok($data);
			
		} catch (\Exception $e) {
			g('api_resp') -> echo_exp($e);
		}
	}
    /** 搜索子部门 */
    public function contact_search_dept() {
        try {
            // gch add contact config
            $watchAll = g('dao_contact_config') -> checkWatchAll($this -> com_id, $this -> user_id);
            if ($watchAll) {
                throw new \Exception("您没有权限查看通讯录");
            }


            $need = array('name');
            $data = $this -> get_post_data($need);

            $dept_list = g('dao_dept') -> list_search($data['name']);
            if (!empty($dept_list)) {
                $d_ids = array();
                foreach ($dept_list as $d) {
                    $d_ids[] = $d['id'];
                }unset($d);
                $has_childs = g('dao_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门

                $root_dept['childs'] = $dept_list;
                foreach ($dept_list as &$dept) {
                    $dept['has_child'] = $has_childs[$dept['id']];
                }
            }
            $data = array(
                'info' => $dept_list,
            );
            g('api_resp') -> echo_ok($data);

        } catch (\Exception $e) {
            g('api_resp') -> echo_exp($e);
        }
    }
	/** 加载子部门 */
	public function contact_load_sub_dept() {
		try {
			// gch add contact config
			$watchAll = g('dao_contact_config') -> checkWatchAll($this -> com_id, $this -> user_id);
			if ($watchAll) {
				throw new \Exception("您没有权限查看通讯录");
			}

			$need = array('d_id');
			$data = $this -> get_post_data($need);

			$dept_list = g('dao_dept') -> list_direct_child($data['d_id']);
			if (!empty($dept_list)) {			
				$d_ids = array();
				foreach ($dept_list as $d) {
					$d_ids[] = $d['id'];
				}unset($d);
				$has_childs = g('dao_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
				
				$root_dept['childs'] = $dept_list;
				foreach ($dept_list as &$dept) {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
			}
			$data = array(
					'info' => $dept_list,
				);
			g('api_resp') -> echo_ok($data);
			
		} catch (\Exception $e) {
			g('api_resp') -> echo_exp($e);
		}
	}
    /** 加载上级人员 */
    public function load_leader_list() {
        try {
            $leader_list = g('dao_user')->list_leader_by_id($this->user_id,false);
            foreach ($leader_list as &$item){
                $item['dept_list'] = json_decode($item['dept_list']);
            }
            $user = g('dao_user')->get_by_id($this->user_id,'id,name,pic_url,dept_list');
            $user['dept_list'] = json_decode($user['dept_list']);
            $data = array(
                'leader_list' => $leader_list,
                'user_info'=>$user
            );
            g('api_resp') -> echo_ok($data);

        } catch (\Exception $e) {
            g('api_resp') -> echo_exp($e);
        }
    }
	// gch add
	/*	加载组别下的员工 */
	public function contact_load_group_user()
	{
		try {
			$need = array('name', 'id', 'offset', 'limit');
			$data = $this -> get_post_data($need);
			
			$group_ids = array();
			if (is_array($data['id'])) {
				$group_ids = array($group_ids, $data['id']);
			} else {
				$group_ids = array($data['id']);
			}
			!is_array($group_ids) && $group_ids = array();

			$user_field = ' `id`,`name`,`pic_url`,`all_py`,`first_py` ';
			$page = $data['offset'] == 0 ? 1 : (ceil($data['offset'] / $data['limit']) + 1);
			$page_size = intval($data['limit']);
			$user_order = ' first_py asc ';

			if (isset($data['name']) && !empty($data['name'])) {
				$ret = g('dao_group') -> get_user_by_ids($this -> com_id, $group_ids, $user_field, 0, 0, '', $this -> user_id);
				$user_ids = array_column($ret, 'id');
				!is_array($user_ids) && $user_ids = array();

				$suser_field = '`id`, `name`, `pic_url`';
				$cond = array(
					'`name` LIKE ' => '%' . strval($data['name']) . '%',
					'id IN' => $user_ids,
					'state!=' => 0, 
					'com_id=' => $_SESSION[SESSION_VISIT_COM_ID], 
				);
				$ret = g('pkg_db') -> select('sc_user', $suser_field, $cond, $page, $page_size, '', $user_order);
			} else {
				$ret = g('dao_group') -> get_user_by_ids($this -> com_id, $group_ids, $user_field, $page, $page_size, $user_order, $this -> user_id);
			}
			
			$data = array(
					'info' => $ret,
			);

			g('api_resp') -> echo_ok($data);
			
		} catch (\Exception $e) {
			g('api_resp') -> echo_exp($e);
		}
	}
    /**
     * 获取最近选用的人员、部门
     * type：1、获取人员，2、获取部门，其他值、获取人员和部门
     */
    public function get_recent_user_depts() {
        try {
            $data = $this->get_post_data();
            $type = isset($data['type']) ? intval($data['type']) : 0;
            $type != 1 && $type != 2 && $type = 0;

            $recently = g('dao_recent_choose')->get_recent_info($this->com_id, $this->user_id);

            $info = array(
                'data' => array(),
                'count' => '0'
            );
            if (!empty($recently)) {
                $dept_list = json_decode($recently['dept_list'], true);
                !$dept_list && $dept_list = array();
                $user_list = json_decode($recently['user_list'], true);
                !$user_list && $user_list = array();
                $tmp_dept = array();
                $tmp_user = array();
                if ($type != 1) {		//获取部门
                    if (!empty($dept_list)) {
                        $dept_ids = array_keys($dept_list);
                        $fields = 'id,name,wx_id,state';
                        $depts = g('dao_dept')->get_by_ids($dept_ids, $fields);
                        //过滤禁用部门
                        foreach ($depts as $dept) {
                            $dept['dept_name'] = $dept['name'];
                            if ($dept['state'] == 1) {
                                unset($dept['state']);
                                $dept['is_dept'] = '1';
                                $tmp_dept[$dept['id']] = $dept;
                            }
                        }unset($dept);
                    }

                }
                if ($type != 2) {	//获取人员
                    if (!empty($user_list)) {
                        $user_ids = array_keys($user_list);
                        $fields = 'id,acct,name,pic_url,state,dept_list';
                        $users = g('dao_user')->get_by_ids($_SESSION[SESSION_VISIT_DEPT_ID], $user_ids, $fields);
                        foreach ($users as &$user){
                            $dept_list = json_decode($user['dept_list']);
                            $user['main_dept'] = $dept_list?current($dept_list):0;
                            unset($user['dept_list']);
                        }
                        //获取用户主部门
                        $main_depts = array_column($users, 'main_dept');
                        if (!empty($main_depts) && is_array($main_depts)) {
                            $tmp_depts = g('dao_dept')->list_by_ids($main_depts, 'id,wx_id,name');
                            $depts = array_column($tmp_depts, 'name', 'id');
                            $dept_wxids = array_column($tmp_depts, 'wx_id', 'id');
                            foreach ($users as &$user) {
                                $user['dept_name'] = '';
                                $user['dept_wx_id'] = '';
                                isset($depts[$user['main_dept']]) && $user['dept_name'] = $depts[$user['main_dept']];
                                isset($depts[$user['main_dept']]) && $user['dept_wx_id'] = $dept_wxids[$user['main_dept']];
                                unset($user['main_dept']);
                            }unset($user);
                        }
                        //过滤非启用人员
                        foreach ($users as $user) {
                            if ($user['state'] == 1 || $user['state'] == 4) {
                                unset($user['state']);
                                $user['is_dept'] = '0';
                                $tmp_user[$user['id']] = $user;
                            }
                        }unset($user);
                    }

                }

                if ($type == 1 && !empty($tmp_user)) {
                    //仅获取人员
                    foreach ($user_list as $id => $time) {
                        isset($tmp_user[$id]) && $info['data'][] = $tmp_user[$id];
                    }unset($time);

                } elseif ($type == 2 && !empty($tmp_dept)) {
                    //仅获取部门
                    foreach ($dept_list as $id => $time) {
                        isset($tmp_dept[$id]) && $info['data'][] = $tmp_dept[$id];
                    }

                } elseif(!empty($tmp_user) || !empty($tmp_dept)) {
                    //获取部门和人员
                    foreach ($dept_list as $id => $time) {
                        $dept_list[$id] = array(
                            'id' => $id,
                            'time' => $time
                        );
                    }
                    foreach ($user_list as $id => $time) {
                        $user_list[$id] = array(
                            'id' => $id,
                            'time' => $time
                        );
                    }
                    $user = null;
                    $dept = null;
                    $i=0;
                    while (count($info['data']) <= 20) {
                        if (empty($dept_list) && empty($user_list) && $i++ % 2 == 1) break;
                        !$dept && $dept = array_shift($dept_list);
                        !$user && $user = array_shift($user_list);

                        !$dept && $dept['time'] = 0;
                        !$user && $user['time'] = 0;
                        if ($user['time'] >= $dept['time']) {
                            isset($tmp_user[$user['id']]) && $info['data'][] = $tmp_user[$user['id']];
                            $user = null;
                        } else {
                            isset($tmp_dept[$dept['id']]) && $info['data'][] = $tmp_dept[$dept['id']];
                            $dept = null;
                        }
                    }

                }

            }

            $info['count'] = (string)count($info['data']);

            $this->echo_ok(array('info' => $info));
        } catch (\Exception $e) {
            $this->echo_exp($e);
        }
    }
    /** 保存最近选用的人员、部门 */
    public function save_recent_user_depts() {
        try {
            $data = $this->get_post_data();
            $user_list = isset($data['user_list']) ? array_unique($data['user_list']) : array();
            $dept_list = isset($data['dept_list']) ? array_unique($data['dept_list']) : array();

            empty($user_list) && empty($dept_list) && $this->echo_ok();

            $time = time();
            $tmp_user = array();
            $tmp_dept = array();
            foreach ($user_list as $id) {
                if (count($tmp_user) >= 20) break;
                $tmp_user[$id] = $time;
            }unset($id);
            foreach ($dept_list as $id) {
                if (count($tmp_dept) >= 20) break;
                $tmp_dept[$id] = $time;
            }unset($id);

            $recently = g('dao_recent_choose')->get_recent_info($this->com_id, $this->user_id);
            if (!$recently) {
                g('dao_recent_choose')->save_recent_choose($this->com_id, $this->user_id, $tmp_user, $tmp_dept);

            } else {
                $old_user = json_decode($recently['user_list'], true);
                !$old_user && $old_user = array();
                $old_dept = json_decode($recently['dept_list'], true);
                !$old_dept && $old_dept = array();

                foreach ($old_user as $id => $t) {
                    if (count($tmp_user) >= 20) break;
                    if (in_array($id, $user_list)) continue;

                    $tmp_user[$id] = $t;
                }unset($t);

                foreach ($old_dept as $id => $t) {
                    if (count($tmp_dept) >= 20) break;
                    if (in_array($id, $dept_list)) continue;

                    $tmp_dept[$id] = $t;
                }unset($t);

                g('dao_recent_choose')->update_recent_choose($this->com_id, $this->user_id, $tmp_user, $tmp_dept);
            }

            $this->echo_ok();
        } catch (\Exception $e) {
            $this->echo_exp($e);
        }
    }
	//END员工选人控件===============================================

	//START获取URL

	//END获取URL

	//内部实现========================================================
	
	/**
	 * 传递基础信息到页面
	 */
	private function _assign_common($mod_name) {
		$mod_conf = load_config("model/{$mod_name}/model");
		$smarty_conf = load_config('package/template/smarty');
//		g('pkg_smarty') -> get_handler() -> clearCompiledTemplate();
		g('pkg_smarty') -> get_handler() -> addPluginsDir($mod_conf['smarty_plugin']);
		$this -> assign('SYSTEM_DOMAIN', MAIN_DYNAMIC_DOMAIN);
		$this -> assign('VIEW_ROOT', $smarty_conf['template_dir']);
		$this -> assign('VIEW_PUBLISH', MAIN_VIEW_NAME);
		$this -> assign('title', MAIN_NAME);										//设置页面默认title，避免具体页面记录配置而显示异常

		// 用户客户端类型
		$user_agent = $this -> get_mobile_platid();
		$this -> assign('platid', $user_agent);
	}

	/** 传递静态资源配置内容 */
	private function _assign_static_res_conf() {
		$static_conf_dir = MAIN_VIEW . 'config' . DS;
		$static_conf_common = $this -> _load_static_res_conf($static_conf_dir . 'common-map.json');
		$static_conf_self = $this -> _load_static_res_conf($static_conf_dir . $this -> mod_name . '-map.json');
		
		$static_conf = array_merge($static_conf_common['res'], $static_conf_self['res']);
		
		//需要特殊处理的短名称映射
		$special_map = array(
			'common:static/js/common.js'									=> 'common',
			'common:static/js/frame.js'										=> 'frame',
			'common:static/js/table.js'										=> 'table',
		);
		
		$static_res = array();
		foreach ($static_conf as $key => $item) {
			if ($item['type'] === 'js') {
				if (preg_match('/^common\:widget\/lib\/(.*)jquery\.(1.9.1.min)\.js$/', $key)) {
					$key = 'jquery';
					
				} else if (isset($special_map[$key])) {
					$key = $special_map[$key];
					
				} else {
					$key = str_replace('.js', '', $key);
				}
				$static_res[$key] = substr($item['uri'], 0, strlen($item['uri']) - 3);
				
			} else if ($item['type'] === 'css') {
				$key = str_replace('.css', '', $key);
				$static_res[$key] = $item['uri'];
				$static_res[$key] = substr($item['uri'], 0, strlen($item['uri']) - 4);
			}
			//设置域名
			isset($static_res[$key]) && (strtolower(substr($static_res[$key], 0, 4)) != 'http') && $static_res[$key] = MAIN_STATIC_DOMAIN . $static_res[$key];
		}unset($item);
		
		$this -> assign('static_res', $static_res);
	}
	
	/**
	 * 传域名信息到前端
	 * @return 
	 */
	private function _assign_config(){
		$this -> assign('HTTP_DOMAIN', MAIN_DYNAMIC_DOMAIN);
		$this -> assign('CDN_DOMAIN', MAIN_STATIC_DOMAIN);
		$this -> assign('MEDIA_DOMAIN', MEDIA_URL_PREFFIX);
		$this -> assign('VERSION_VER', MAIN_STATIC_VER);

		$power = load_config('model/api/powerby');
		$this -> assign('POWERBY', $power['desc']);
	}

	/**
	 * 读取静态资源配置文件（去掉注释）
	 * @param unknown_type $path		文件绝对路径
	 */
	private function _load_static_res_conf($path) {
		$static_conf = file_get_contents($path);
		$static_conf = preg_replace('/\/\*(.*)\*\//', '', $static_conf);
		$static_conf = json_decode($static_conf, TRUE);
		return $static_conf;
	}
	
	/**
	 * @return 获取微信菜单
	 */
	protected function get_menu(){
		$menu = array();

		$mod_name = $this -> mod_name;
		$app_config = load_config("model/{$mod_name}/app");
		if(isset($app_config['menu'])){
			$menu = $app_config['menu'];
		}
		g('pkg_smarty') -> assign('APP_MENU', $menu);
	}

	protected function get_user_info($return = false){
		$user = g('dao_user') -> get_by_id($this -> user_id, 'id, name, pic_url, dept_list');
		$dept_list = json_decode($user['dept_list'], TRUE);
		$dept_id = array_shift($dept_list);
		$dept = g('dao_dept') -> get_by_id($dept_id, 'id, name');
		
		$admin = g('dao_admin') -> check_admin($this -> com_id, $this -> user_id, 'id');

		/**
	 	 * IS_VIP：1：普通用户，2、VIP，3、SVIP
	 	 */
 		$ret = $this->get_vip_power(false);
 		$is_vip = $ret['is_vip']+1;
 		$vip_state = $ret['vip_state'];
 		$past_time = $ret['past_time'];
 		$past_date = $vip_state ? (int)ceil(($past_time - time()) / (60 * 60 * 24)) : 0;
 		$past_date > 30 && $past_date = 0;

 		//指纹设置状态
 		$fp_state = 0;
 		$has_fp = 0;
 		$fp_info = g('api_fingerprint')->get_user_fingerprint($this->com_id, $this->user_id);
 		if ($fp_info) {
 			$fp_info['state'] && $fp_state = 1;
			!empty($fp_info['fingerprint']) && $has_fp = 1;
 		}

         $com_info = g('dao_com')->get_by_id($this->com_id);

 		if($return){
 		    return array(
                'FP_STATE'=>$fp_state,
                'HAS_FP'=>$has_fp,
                'IS_VIP'=>$is_vip,
                'VIP_STATE'=>$vip_state,
                'PAST_TIME'=>$past_time,
                'PAST_DATE'=>$past_date,
                'USER_ID'=>$user['id'],
                'IS_ADMIN'=>empty($admin) ? 0 : 1,
                'USER_NAME'=>$user['name'],
                'USER_PIC'=>$user['pic_url'],
                'USER_DEPT_ID'=>$dept['id'],
                'USER_DEPT_NAME'=>$dept['name'],
                'COM_ID'=>$this -> com_id,
                'COM_TYPE'=>$com_info['type'],
            );
        }

 		g('pkg_smarty') -> assign('FP_STATE', $fp_state);
 		g('pkg_smarty') -> assign('HAS_FP', $has_fp);

 		g('pkg_smarty') -> assign('IS_VIP', $is_vip);
 		g('pkg_smarty') -> assign('VIP_STATE', $vip_state);
 		g('pkg_smarty') -> assign('PAST_TIME', $past_time);
 		g('pkg_smarty') -> assign('PAST_DATE', $past_date);

		g('pkg_smarty') -> assign('USER_ID', $user['id']);
		g('pkg_smarty') -> assign('IS_ADMIN', empty($admin) ? 0 : 1);
		g('pkg_smarty') -> assign('USER_NAME', $user['name']);
		g('pkg_smarty') -> assign('USER_PIC', $user['pic_url']);
		g('pkg_smarty') -> assign('USER_DEPT_ID', $dept['id']);
		g('pkg_smarty') -> assign('USER_DEPT_NAME', $dept['name']);
        g('pkg_smarty') -> assign('COM_ID', $this -> com_id);
	}


	/**
	 * 判断是否是vip（包括svip）
	 * @param  boolean $only_state 是否只返回是否是VIP， true:返回state,false:返回详细VIP信息（与svip无关）
	 * IS_VIP：0：普通用户，1、VIP，2、SVIP
	 * VIP_STATE：（IS_VIP==1时有效）0：已过期，1：未过期
	 * PAST_TIME：（IS_VIP==1时有效）过期时间
	 * @return 
	 */
	protected function get_vip_power($only_state = TRUE) {
		$key = $this -> get_redis_key(self::$VIP_REDIS_KEY . $this ->com_id);
		$data = g('pkg_redis') -> get($key);
		if(empty($data)){
			$ret = g('dao_vip') -> get_vip_by_com($this ->com_id);
			$is_vip = 0;
			$vip_state = 0;
			$past_time = 0;
			$app_key = array();

			if (empty($ret) || ($ret['is_svip'] == 0 && $ret['is_vip'] == 0)) {
				//非VIP 或 超级VIP被撤销
			} elseif ($ret['is_vip'] == 1 && $ret['is_svip'] == 0) {
				$is_vip = 1;
				$past_time = $ret['past_time'] - 1;
				$vip_state = $past_time > time() ? 1 : 0;
			} elseif ($ret['is_svip'] == 1) {
				if ($ret['is_vip'] == 1 && $ret['past_time'] > time()) {
					$is_vip = 1;
					$past_time = $ret['past_time'] - 1;
					$vip_state = 1;
				} else {
					$is_vip = 2;
					$app_key = json_decode($ret['app_key'], true);
					!$app_key && $app_key = array();
				}
			}

			$vip_info = array(
					'is_vip' => $is_vip,
					'past_time' => $past_time,
					'vip_state' => $vip_state,
					'svip_update_time' => isset($ret['svip_update_time']) ? $ret['svip_update_time'] : 0,
					'first_open_time' => isset($ret['first_open_time']) ? $ret['first_open_time'] : 0,
					'last_open_time' => isset($ret['last_open_time']) ? $ret['last_open_time'] : 0,
					'app_key' => $app_key
				);
			g('pkg_redis') -> set($key, json_encode($vip_info), 60);
		}else{
			$vip_info = json_decode($data, TRUE);
		}
		return $only_state ? $vip_info['vip_state'] : $vip_info;
	}


	/**
	 * 发送单图文消息到WX
	 * @param   $title 	图文标题
	 * @param   $content 内容概要
	 * @param   $url 图文链接
	 * @param   $pic_url 图文封面图片链接
	 * @param  array  $user_list 员工id数组
	 * @param  array  $dept_list 部门wx_id数组
	 * @return 
	 */
	protected function send_wx_news($title, $content, $url, $pic_url='', $user_list=array(), $dept_list = array()){
		g('api_nmsg') -> init($_SESSION[SESSION_VISIT_COM_ID], $this -> mod_name, $_SESSION[SESSION_VISIT_CORP_URL]);
		$art_list = array(
				array(
						'title' 		=> $title,
						'description' 	=> $content,
						'url' 			=> $url,
						'picurl' 		=> $pic_url,
					)
			);
		return g('api_nmsg') -> send_news($dept_list, $user_list, $art_list);
	}

	/**
	 * 推送消息到PC
	 * @param   $title 	图文标题
	 * @param   $url 图文链接
	 * @param  array  $user_list 员工id数组
	 * @param  array  $dept_list 部门id数组
	 * @return 
	 */
	protected function send_pc_news($title, $url, $user_list=array(), $dept_list = array()){
		if(!is_array($user_list) || !is_array($dept_list)){
			return false;
		}
		$data = array(
				'title' => $title,
	        	'url' => $url,
	            'app_name' => $this -> mod_name,
			);
		return g('api_messager') -> publish_by_ids('pc', $dept_list, $user_list, $data);
	}	

	/** 获取轮询URL */
	protected function get_consume_url(){
		$url =  g('api_messager') -> get_consume_url_by_user('wx');
		g('pkg_smarty') -> assign('CONSUME_URL', $url);
		return $url;
	}

	/**
	 * 新应用发送消息接口
	 * @param   $title 标题
	 * @param   $content 描述
	 * @param  string $pic_url 封面图片
	 * @param  string $url 消息链接
	 * @param  int $type 消息类型 流程类：1：待办， 2：通过，3：知会，其他应用 0：默认消息
	 * @param  user_id 发送人ID 系统发送则为0
	 * @param  array  $user_list 接收人数组
	 * @param  array  $dept_list 接收部门数组
	 * @param  array  $group_list 组别数组
	 * @return 
	 */
	protected function send_news($title, $content, $pic_url='', $url='', $app_id=0, $type=0, $user_id=0, $user_list=array(), $dept_list=array(), $msg_title=''){
		if(empty($user_list) && empty($dept_list)){
			return false;
		}
		if($user_id != 0){
			$user = g('dao_user') -> get_by_id($user_id, 'id, com_id, pic_url');
		}

		$app_info = load_config('model/api/app');
		empty($app_id) && $app_id = isset($app_info['bpm']) ? $app_info['bpm']['id'] : 0;

		$data = array(
				'title' => $title,
				'msg_title' => $msg_title,
	        	'content' => $content,
				'pic' => $pic_url,
	        	'url' => $url,
	        	'app_id' => $app_id,
	        	'type' => $type,
	        	'user_id' => $user_id,
				'user_pic' => $user_id == 0 ? '' : $user['pic_url'],
			);
	    $data['msg_id'] = $this -> get_msg_id(json_encode($data));

	    if(strpos($url,'?')!==false){
            $url = str_replace('?','?msg_id='.$data['msg_id'].'&',$url);
        }else{
            $url .= '?msg_id='.$data['msg_id'];
        }

        $data['url'] = $url;


		$data['com_id'] = $this -> com_id;
		$data['receive_depts'] = json_encode($dept_list);
		$data['receive_users'] = json_encode($user_list);
		$data['create_time'] = time();
		$data['success_time'] = time();
		$data['send_times'] = 0;

        //跨企业审批使用
        $com_ids = [];

        if($user_list){
            $tmp_com_ids = g('dao_user') -> get_by_ids('',$user_list, 'com_id');
            $com_ids = array_merge($com_ids,array_column($tmp_com_ids,'com_id'));
        }

        if($dept_list){
            //获取部门所在企业
            $tmp_deps = g('dao_dept')->list_by_ids($dept_list,'root_id');
            $root_ids = array_unique(array_column($tmp_deps,'root_id'));
            if($root_ids){
                $tmp_cond = array(
                    'dept_id IN'=>$root_ids
                );
                $tmp_com_ids = g('dao_com')->list_by_cond($tmp_cond,'id');
                $com_ids = array_merge($com_ids,array_column($tmp_com_ids,'com_id'));
            }
        }

        if($com_ids){
            $com_ids = array_unique($com_ids);
            $tmp_cond = array(
                'id IN'=>$com_ids
            );
            $tmp_coms = g('dao_com')->list_by_cond($tmp_cond,'id,corp_url');
            foreach ($tmp_coms as $tmp_com){
                $data['com_id'] = $tmp_com['id'];
                $tmp_url = $data['url'];
                $tmp_url = changeURLParam($tmp_url,'corpurl',$tmp_com['corp_url']);
                $data['url'] = $tmp_url;
                $news_id = g('dao_news') -> insert_news($data);
            }
        }else{
            $news_id = g('dao_news') -> insert_news($data);
        }


        if (!$news_id) {
            $this -> log_e('发送消息失败, 接收人：'.json_encode($user_list). '-'. json_encode($dept_list));
            return false;
        }

		if($type==9){
            //lyc 新方式推送消息 定时任务跑时，推送到队列
            return TRUE;
        }



//		$data['id'] = $news_id;
//
//		//unset掉不需要的信息
//		unset($data['com_id']);
//		unset($data['receive_depts']);
//		unset($data['receive_users']);
//		unset($data['create_time']);
//		unset($data['success_time']);
//		unset($data['send_times']);
//		unset($data['msg_title']);
//
//		$ret = g('api_messager') -> publish_by_ids('wx', $dept_list, $user_list, $data);
//		if(!$ret){
//			$this -> log_e('发送WX消息失败, 接收人：'.json_encode($user_list). '-'. json_encode($dept_list));
//			return FALSE;
//		}
		return TRUE;
	}

	/** 获取用户已读取过的一次性消息 */
	public function get_onetime_msg() {
		try {
			$redis_key = $this->get_redis_key('one_time_msg::'.$this->com_id.'::'.$this->user_id);
			
			$cache = g('pkg_redis')->get($redis_key);
			if ($cache) {
				$msg_keys = json_decode($cache, true);
			} else {
				$cond = array(
					'reader_id=' => $this->user_id,
					'is_admin=' => 0
					);
				$ret = g('pkg_db')->select('onetime_msg_user', '*', $cond);
				$msg_keys = array();
				$ret && $msg_keys = json_decode($ret[0]['msg'], true);

				g('pkg_redis')->set($redis_key, json_encode($msg_keys), 120);
			}

			$return = array();
			foreach ($msg_keys as $k) {
				$return[$k] = '1';
			}unset($k);

			return $return;
		} catch (\Exception $e) {
			throw $e;
		}	
	}

	/** 更新用户已读取的一次性消息 */
	public function update_onetime_msg() {
		try {
			$data = $this->get_post_data(array('msg_key'));
			$msg_key = trim($data['msg_key']);
			if (!empty($msg_key)) {
				$msg_keys = load_config("model/api/onetime_msg");
				if (!in_array($msg_key, $msg_keys)) {
					throw new \Exception('消息不存在');
				}

				$cond = array(
					'reader_id=' => $this->user_id,
					'is_admin=' => 0
					);
				$ret = g('pkg_db')->select('onetime_msg_user', 'msg', $cond);
				if ($ret) {
					$msgs = json_decode($ret[0]['msg'], true);
					$msgs[] = $msg_key;
					//剔除在配置文件中已删除的消息key
					$msgs = array_intersect($msgs, $msg_keys);
					$msgs = array_unique($msgs);
					$msgs = array_values($msgs);

					$data = array('msg' => json_encode($msgs));

					$ret = g('pkg_db')->update('onetime_msg_user', $cond, $data);
				} else {
					$msgs = array($msg_key);
					$data = array(
						'reader_id' => $this->user_id,
						'is_admin' => 0,
						'create_time' => time(),
						'msg' => json_encode($msgs)
						);
					$ret = g('pkg_db')->insert('onetime_msg_user', $data);
				}
			}

			$redis_key = $this->get_redis_key('one_time_msg::'.$this->com_id.'::'.$this->user_id);
			g('pkg_redis')->delete($redis_key);

			$this->echo_ok();
		} catch (\Exception $e) {
			$this->echo_exp($e);
		}
	}

	/** 获取一条推送消息的message_id */
	private function get_msg_id($str){
		return md5($str . unique_code());
	}

	/**
     * 判断手机平台
     *
     * @access public
     * @return integer 取值：0-其他、1-ios、2-android、3-wp
     */
    protected function get_mobile_platid() {
        $agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(iPod|iPad|iPhone)/i', $agent)) {
            return 1;
        }

        if (preg_match('/android/i', $agent)) {
            return 2;
        }

        if (preg_match('/WP/', $agent)) {
            return 3;
        }

        if (preg_match('/Windows/', $agent)) {
            return 4;
        }

        if (preg_match('/Mac/', $agent)) {
            return 5;
        }

        return 0;
    }

    /** 获取缓存KEY */
	private function get_redis_key($str){
		return md5("VIEW_BASE_KEY::".$str);
	}

}

//end