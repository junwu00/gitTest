<?php
namespace model\api\view;

/**
 * 私有化部署初始化类
 *
 * @author JianMing Liang
 * @create 2016-02-27 14:26:57
 */
class ViewPrivateInit {
    private $com_table = 'sc_company';

    public function check_auth() {
        $cache_key = md5(__CLASS__ . ":" . __FUNCTION__);
        $cache = g('pkg_redis') -> get($cache_key);

        if (!empty($cache)) {
            $cache_str = g('pkg_des_plus') -> decode($cache);
            $cache_arr = json_decode($cache_str, true);
            !is_array($cache_arr) and $cache_arr = array();

            if (!isset($cache_arr['time']) or time() - $cache_arr['time'] > 1800) {
                //重新查询授权情况
            } else {
                return;
            }
        }

        $sql = "select count(1) as corp_count from {$this -> com_table}";
        $corp_count = g('pkg_db') -> query($sql);
        if ($corp_count === false) {
            $this -> show_error();
        }

        $wx_domain = MAIN_DYNAMIC_DOMAIN;
        if (empty($wx_domain)) {
            $this -> show_error();
        }

        $index_md5 = md5_file(MAIN_ROOT . 'index.php');
        $param  = array(
            'caller'        => API_AUTH_CALLER,
            'wx_domain'     => $wx_domain,
            'src'           => API_AUTH_SRC,
            'ver'           => MAIN_VERSION,
            'core_md5'      => $index_md5,
            'corp_count'    => $corp_count[0]['corp_count'],
            'time'          => time(),
        );

        $sign = get_sign($param, API_AUTH_SECRET);
        $param['sign'] = $sign;

        $url = API_AUTH_URL . '?' . http_build_query($param);
        $result = g('pkg_curl') -> get($url, array(), 30);

        $res_arr = json_decode($result['content'], TRUE);
        !is_array($res_arr) and $res_arr = array();

        if ($result['httpcode'] != 200 or !isset($res_arr['status']) or $res_arr['status'] != 1 or $res_arr['data'] != $sign) {
            $log_str = '授权验证失败: params=' . json_encode($param, JSON_UNESCAPED_UNICODE) . ', ret=' . json_encode($result, JSON_UNESCAPED_UNICODE);
            to_log($GLOBALS['levels']['MAIN_LOG_WARN'], MAIN_DATA . 'logs' . DS . 'warn.log', $log_str);
            $this -> show_error();
        }

        $data = array(
            'token' => get_rand_str(33),
            'time' => time(),
        );
        $cache_val = g('pkg_des_plus') -> encode(json_encode($data));

        // 缓存10分钟
        g('pkg_redis') -> set($cache_key, $cache_val, 600);
    }

    function show_error() {
        $html = <<<EOF

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="三尺科技"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="format-detection" content="telephone=no">
    
		<title>错误</title>
		
		<style type="text/css">
			* {font-family: "Microsoft YaHei", "微软雅黑", Helvetica, "黑体",Arial,Tahoma; color: #333; font-weight: normal;}
			html, body {margin: 0; padding: 0;}
			#img-bar {width: 100%; text-align: center; background: url('static/image/err_bg.png') no-repeat; background-size: 100% 100%; padding: 15px 0;}
			h3 {width: 90%; margin: 0 auto; border-bottom: 1px solid #888; text-align: center; padding: 15px 0;}
			#main {width: 90%; margin: 0 auto; padding-top: 15px;}
			#main p {text-indent: 2em;}
		</style>
		
	</head>
	
	<body>
		<div id="img-bar"> 
			<img src="/static/image/err.png">
		</div>
		<h3>哦噢，出错了！</h3>
		<div id="main">
			可能的原因：
                <p>您的私有化版本授权验证失败，请联系移步到微官方进行处理！</p>
        </div>
	</body>
</html>
EOF;
        die($html);
    }
}

/* End of this file */