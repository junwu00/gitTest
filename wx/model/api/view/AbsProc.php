<?php
/**
 * 审批流程通用父类
 * @author lijs
 * @date 2016-01-22
 * @version 1.1
 */
namespace model\api\view;

use model\api\dao\Com;
use model\api\dao\ComForm;
use model\api\dao\ProcessSchoolContact;
use model\api\server\mv_proc\Mv_Formsetinst;
use model\api\server\mv_proc\Mv_Formsetinst_Name_Val;
use model\api\server\mv_proc\Mv_Work;
use model\api\server\mv_proc\Mv_Work_Node;
use model\api\server\mv_proc\Mv_Workinst;

class AbsProc extends \model\api\view\ViewBase {
	
	protected static $PageSize = 20;
	/**
	 * 人员状态
	 */
	protected static $UserDel = 0;//删除用户
	
	protected static $UserFollow  = 2;//关注用户
	
	protected static $UserFloze = 3;//冻结用户
	
	protected static $UserNotFollow  = 4;//未关注
	
	/**
	 * 工作项状态
	 */
	/** 新收到1 */
	protected static $StateNew = 1;
	/** 已阅读 2 */
	protected static $StateRead = 2;
	/** 已保存 3 */
	protected static $StateSave = 3;
	/** 等待中 4 */
	protected static $StateWait = 4;
	/** 回收 5 */
	protected static $StateRecovery = 5;
	/** 终止 6 */
	protected static $StateStop = 6;
	/** 已处理 7 */
	protected static $StateSend = 7;
	/** 已退回 8 */
	protected static $StateCallback = 8;
	/** 已通过 9 */
	protected static $StateDone = 9;
	/** 退回 10 */
	protected static $StateSendback = 10;
    /** 转审 11 */
    protected static $StateTurn = 11;
    /** 已撤销 12 */
    protected static $StateRevoke = 12;
    /** 撤销中 13 */
    protected static $StateRevoking = 13;
    /** 通过后撤销 14 */
    protected static $StateFinishRevoke = 14;


    /**
	 * 是否退回状态
	 */
	/** 退回*/
	protected static $Callback = 1;
	/** 不是退回 */
	protected static $Not_Callback = 0;
	
	/**
	 * 是否开始节点
	 *
	 */
	/** 是*/
	protected static $StartNode = 1;
	/** 不是*/
	protected static $NotStartNode = 0;
	
	
	/** 知会未读 1*/
	protected static $StateReading = 1;
	/** 知会已读 2*/
	protected static $StateRoad = 2;
	
	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	protected static $StateFormDraft = 0;
	/** 运行中 */
	protected static $StateFormDone = 1;
	/** 已结束 */
	protected static $StateFormFinish = 2;
	/** 终止 */
	protected static $StateFormStop = 3;
    /** 撤销 */
    protected static $StateFormRevoke = 4;
    /** 撤销中 */
    protected static $StateFormRevoking = 5;
    /** 通过后已撤销 */
    protected static $StateFormFinishRevoke = 6;
	
	/**
	 * 退回状态
	 */
	/** 退回上一步骤*/
	protected static $CallBackPer = 1;
	/** 退回开始步骤*/
	protected static $CallBackStart = 2;
	
	/**
	 * 流程图状态
	 */
	/** 已经完成的节点标示*/
	protected static $PicStateDone = 1;
	
	/** 正在进行的节点标示*/
	protected static $PicStateDoing = 2;

    /** 终止的节点标示*/
    protected static $PicStateStop = 3;
	
	
	/**
	 * 是否自动知会
	 */
	/** 开始时知会*/
	protected static $NotifyAutoStart = 2;
	/** 结束时知会*/
	protected static $NotifyAutoEnd = 1;
    /** 结束时有隐患知会*/
    protected static $NotifyAutoEndForDangeous = 11;
	/** 否*/
	protected static $NotNotifyAuto = 0;
	
	/**
	 * 知会标示
	 */
	/** 知会上级  */
	protected static $StateNotifyLeader = 3;
	/** 知会上级的上级  */
	protected static $StateNotifyILeader = 4;
	/** 知会发起人  */
	protected static $StateNotifySelf = 5;
	/** 知会固定人  */
	protected static $StateNotifyPeople = 1;
	/** 知会表单控件  */
	protected static $StateNotifyInput = 2;

    /**
     * 隐患控件知会标示
     */
    /** 知会发起人上级  */
    protected static $InputStateNotifyLeader = 2;
    /** 知会发起人  */
    protected static $InputStateNotifySelf = 1;
    /** 知会固定人  */
    protected static $InputStateNotifyPeople = 3;

    protected static $InputStateNotify = [1,2,3];
	
	/**
	 * 控件是否可编辑
	 */
	/** 是*/
	protected static $EditInput = 1;
	/** 否*/
	protected static $NotEditInput = 0;
	
	/**
	 * 控件是否可见
	 */
	/** 是*/
	protected static $VisitInput = 1;
	/** 否*/
	protected static $NotVisitInput = 0;
	
	/** 流程*/
	protected static $IsProc = 0;
	/** 旧版请假*/
	protected static $IsRest = 1;
	/** 旧版报销*/
	protected static $IsExaccount = 2;
	/** 补录*/
	protected static $IsMakeup = 3;
	/** 新请假*/
	protected static $IsNewRest = 4;
	/** 外勤*/
	protected static $IsLegwork = 5;
	/** 新版报销*/
	protected static $IsNewExaccount = 6;
	
	/** 申请人主部门 */
	protected static $ApplyerDept = 1;
	/** 申请人名称 */
	protected static $ApplyerName = 2;
	/** 申请人日期（年-月） */
	protected static $ApplyDateM = 3;
	/** 申请人日期（年-月-日） */
	protected static $ApplyDateD = 4;
	/** 申请人日期（年-月-日 时：分） */
	protected static $ApplyDateMin = 5;
	/** 表单名称 */
	protected static $FormName = 6;
	/** 自定义控件 */
	protected static $Custom = 7;
	
	/** 发布版本 */
	protected static $StatePublic = 2;
	/** 历史版本 */
	protected static $StateHistory = 1;
	
	/** 禁用 0*/
	protected static $StateOff = 0; 
	/** 启用 1*/
	protected static $StateOn = 1;
	
	/** 是否为开始工作项  1 是 */
	protected static $StartWItem = 1;
	/** 是否为开始工作项  0 是 */
	protected static $NotStartWItem = 0;
	
	
	/**
	 * 人员选择范围
	 */
	protected static $RecriverFixed = 1;//固定接收人
	
	protected static $RecriverCreater = 2;//流程发起人
	
	protected static $RecriverCreaterLeader = 3;//流程发起人领导
	
	protected static $RecriverCreaterDept = 4;//流程发起人部门
	
	protected static $RecriverHandler = 5;//当前处理人
	
	protected static $RecriverHandlerLeader = 6;//当前处理人上级领导
	
	protected static $RecriverHandlerDept = 7;//当前处理人部门
	
	protected static $RecriverInput = 8;//表单具体数据
	
	/**
	 * 比较符号
	 */
	
	protected static $Compare_Greater = 1;//大于
	
	protected static $Compare_Lesser = 2;//小于
	
	protected static $Compare_Equeal = 3;//等于
	
	protected static $Compare_Lesser_Equeal = 4;//小于等于
	
	protected static $Compare_Greater_Equeal = 5;//大于等于
	
	protected static $Compare_Not_Equeal = 6;//不等于
	
	protected static $Compare_Contain = 8;//包含
	
	protected static $Compare_Not_Contain = 9;//不包含
	
	//人员规则
	protected static $People_Position = 7;//人员职位
	
	protected static $People_Type_Creater =2;//发起人
	
	protected static $People_Type_Done =1;//当前处理人
	
	/**
	 * 是否是领签标示
	 */
	protected static $N_Linksign =0;//否
	
	protected static $Linksign =1;//是

    /**
     * 工作项状态对应的工作的状态
     */
    /** 处理中状态  包括 新收到1 已阅读 2 已保存 3*/
    protected static $StateInHand = array(1,2,3);
    /** 运行中状态  包括 新收到1 已阅读 2 已保存 3 等待中 4*/
    protected static $StateDoing = array(1,2,3,4);
    /** 已完成状态  包括 已处理 7 已退回 8 已通过 9*/
    protected static $StateFinish = array(7,8,9,10,11);

    /** 处理意见状态  包括 已终止 6 ；已处理 7 ；已退回 8 ；已退回 10 ；通过已撤销 14*/
    protected static $StateWorkitemJudgement = array(6,7,8,10,14);

    /** 已退回 1 */
    protected static $CallBackYes = 1;
    /** 未退回 0 */
    protected static $CallBackNo = 0;

    /** 开启终止 1 */
    protected static $BreakYes = 1;
    /** 未开启终止 0 */
    protected static $BreakNo = 0;
    /** 终止按钮名称 1 */
    protected static $BreakButtonName = '驳回';

    /** 开启撤销 1 */
    protected static $RevokeYes = 1;
    /** 未开启撤销 0 */
    protected static $RevokeNo = 0;

    /** @var string 表单数据缓存key */
    const CUSTOM_INSTANCE = "custom_instance";

	
	protected $ComId;
	
	protected $UserId;
	
	protected $UserName;
	
	protected $IsProcType;
	
	protected $PcDomain;
	
	protected $WxDomain;
	
	protected $AppName;
	
	protected $AppCName;

	protected $LinksignUrl;
	
	/** 需要拆分的表单控件字段 */
	private static $input_val = array('input_key', 'type', 'name', 'must', 'describe', 'val');
	/** 需要拆分的子表单控件字段 */
	private static $c_input_val = array('th_key', 'type', 'name', 'must', 'describe', 'val');
    /** 需要知会，接收人的子表单控件字段 */
    private static $notify_input_val = array('car','people');
	
	public function __construct($app_name) {
		parent::__construct($app_name);

		$app_conf = load_config("model/{$app_name}/model");

		if($app_name=='rest'){
			$this->IsProcType = self::$IsRest;
		}
		else if($app_name=='exaccount'){
			$this->IsProcType = self::$IsExaccount;
		}
		else{
			$this->IsProcType = self::$IsProc;
		}
		$this->ComId = $_SESSION[SESSION_VISIT_COM_ID];
		$this->UserId = $_SESSION[SESSION_VISIT_USER_ID];
		$this->UserName = $_SESSION[SESSION_VISIT_USER_NAME];
		$this->PcDomain = PC_DYNAMIC_DOMAIN;
		$this->WxDomain = MAIN_DYNAMIC_DOMAIN;
		$this->AppName = $app_name;
		$this->AppCName = $app_conf["cn_name"];
		$this->LinksignUrl = SIGN_MEDIA_URL_HOST;
	}

    public function getMaterialWarehousingFormId($isPublic=false)
    {
        $dao_form = new ComForm();
        $form = $dao_form->getByComId($this->com_id,ComForm::TYPE_MATERIAL_WAREHOUSING);
        if (!$form) {
            throw new \Exception("系统未配置物资入库表单，请联系管理员！");
        }
        if (!$isPublic) {
            return $form['form_history_id'];
        }
        $public_form = g('mv_form') -> get_by_his_id($form['form_history_id'], $this->com_id);
        if (!$public_form) {
            throw new \Exception("系统配置物资入库表单错误，请联系管理员！");
        }
        return $public_form['id'];
    }
    public function getMaterialApplyFormId($isPublic=false)
    {
        $dao_form = new ComForm();
        $form = $dao_form->getByComId($this->com_id,ComForm::TYPE_MATERIAL_APPLY);
        if (!$form) {
            throw new \Exception("系统未配置物资领用表单，请联系管理员！");
        }
        if (!$isPublic) {
            return $form['form_history_id'];
        }

        $public_form = g('mv_form') -> get_by_his_id($form['form_history_id'], $this->com_id);
        if (!$public_form) {
            throw new \Exception("系统配置物资领用表单错误，请联系管理员！");
        }
        $form_id = $public_form['id'];
        return $form_id;
    }

    /**
     * 获取表单模板显隐规则
     * @param $com_id
     * @param $form_id
     */
    public function getFormRule($com_id,$form_id){
        $return = [];
        $form_rules = g('mv_form_rule')->listFormRule($com_id,$form_id,'r.id,r.relation_rule,r.result,r.order,c.input_key,c.value,c.order');
        if($form_rules){
            $tmp_rules = array();
            foreach ($form_rules as $rule){
                if( !isset($tmp_rules[$rule['id']]) ){
                    $tmp_rules[$rule['id']] = array(
                        'relation_rule'=>$rule['relation_rule'],
                        'result'=>is_array($rule['result'])?$rule['result']:json_decode($rule['result'],true),
                        'order'=>$rule['order'],
                        'condition'=>[],
                    );
                }

                $tmp_rules[$rule['id']]['condition'][] = array(
                    'input_key'=>$rule['input_key'],
                    'value'=>$rule['value'],
                    'order'=>$rule['order'],
                );
            }

            $return = array_values($tmp_rules);
        }

        return $return;
    }

    /**
     * 检测物资预警
     * @param $formsetinst
     */
    private function checkMaterialPrewarning($formsetinst){
        $form_history_id = $formsetinst['form_history_id'];
        $config = load_config("model/process/model");
        $material_management = $config['material_management'];
        if( $form_history_id == $material_management['item_claim_form'] ){
            //获取当前表单实例数值
            $form_vals = $this->get_form_vals($formsetinst['id']);
            //库存判断
            $material_form_data = g('dao_material')->get_material_form($this->getMaterialWarehousingFormId(),$this->getMaterialApplyFormId());
            $check_data = $form_vals[$material_management['goods_table']]['rec'];
            $warning2 = 0;
            $content2 = [];
            $user_list = [];

            foreach ($check_data as $item){
                //用户请求
                $goods_id = $item[$material_management['goods_input_key']]['value']['history_id'];
                //库存 remaining
                $material = array_key_exists($goods_id,$material_form_data)?$material_form_data[$goods_id]:[];
                $remaining = array_key_exists("remaining",$material)?$material['remaining']:0;
                $material_worning = g('dao_material')->get_material_worning($this -> com_id,$goods_id);
                $prewarning_value = $material_worning?$material_worning['warning_value']:0;

                if( $remaining<=$prewarning_value){
                    //库存不足以给用户
                    $warning2 = 1;
                    $warning_data = [
                        "prewarning_value" =>$prewarning_value,
                        "number"=>$remaining,
                        "val"=>$item[$material_management['goods_input_key']]['val'],
                    ];
                    $content2[] = $warning_data;
                }
            }
            if($warning2){
                $title = "物资预警";
                $content = "";

                //预警推送需要调整到审批结束
                if($content2){
                    $content .= "以下物资低于预警值<br/>";
                    foreach ($content2 as $val){
                        $content .= $val['val']."预警值：".$val['prewarning_value'].",库存剩余：".$val['number'].";<br/>";
                    }
                }
                $material_worning_user = g('dao_material')->get_material_worning_user( $this -> com_id,"warning_user_list");
                $user_list = $material_worning_user?json_decode($material_worning_user['warning_user_list']):[];
                $app_id = 23;

                $url = MAIN_DYNAMIC_DOMAIN.'/index.php?model=index&a=msg_detail&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
                $user_list && parent::send_news($title,str_replace("<br/>","\r\n",$content), $pic_url='', $url, $app_id, 9, 0, $user_list, $dept_list=array(), $msg_title='');
            }
        }

    }

    /**
     * 终止流程
     * @throws
     */
    public function breakWorkitem($data){
        $workitem_id = isset($data["workitem_id"])?$data["workitem_id"]:0;
        !isset($data['judgement']) && $data['judgement'] = '';
        !isset($data['judgement_desc']) && $data['judgement_desc'] = '';

        //获取当前步骤的信息
        $curr_w_fields = "mpw.state,mpw.info_state,mpw.formsetinit_id,mpw.handler_id,mpf.work_id,
        mpwn.break_state,mpwn.break_button_name,mpfm.id form_id,
        mpwn.work_id,mpwn.id curr_node_id,mpw.receive_time,mpf.formsetinst_name,mpf.state formsetinst_state,mpfm.form_name,mpf.creater_id formsetinst_creater_id,mpf.app_version,mpf.relate_formsetinst_id";
        $curr_workitem = $this->get_curr_workitem($workitem_id,$curr_w_fields);

        $curr_state = $curr_workitem["state"];
        $curr_receive_time = $curr_workitem['receive_time'];
        $formsetinst_state = $curr_workitem['formsetinst_state'];

        if($this->user_id != $curr_workitem["handler_id"])
            throw new \Exception('无权限操作');

        if ($curr_workitem['formsetinst_state'] == self::$StateFormFinish) {
            $end_node =  g('mv_work_node') -> get_end_by_workid($this->ComId, $curr_workitem['work_id']);
            if (!$end_node)
                throw new \Exception('无权限操作');

            if(!$end_node['break_state'])
                throw new \Exception('结束节点未开启终止流程功能');

            $break_user_list = json_decode($end_node['break_user_list'],true);
            $break_user_list = $break_user_list ? array_column($break_user_list,'id') : [];
            if (!in_array($this->user_id,$break_user_list)) {
                throw new \Exception('无权限操作');
            }
        } else {
            $dao_com_form = new ComForm();
            $revoke_form_id = $dao_com_form->getRevokePublicFormId($this->com_id);
            if ($revoke_form_id && $revoke_form_id == $curr_workitem['form_id']){
                $curr_workitem['break_state'] = self::$BreakYes;
                $curr_workitem['break_button_name'] = self::$BreakButtonName;
            }

            if(!$curr_workitem['break_state'])
                throw new \Exception('当前节点未开启终止流程功能');

            if($formsetinst_state == self::$StateFormFinish) {
                throw new \Exception("流程已终止");
            }

            if($formsetinst_state == Mv_Formsetinst::$StateRevoke) {
                throw new \Exception("流程已撤销");
            }

            if(!in_array($curr_state,self::$StateInHand)){
                throw new \Exception('该工作项已经处理，请返回代办列表');
            }
        }

        // 更新工作项
        if ($curr_workitem['formsetinst_state'] == self::$StateFormFinish) {
            // 新增保存结束节点
            $user = g('dao_user')->get_by_id($this->user_id);
            $handle_m_dept = g("dao_user")->get_main_dept($this->user_id,FALSE);
            $handle_dept_id = $handle_m_dept["id"];
            $handle_dept_name = $handle_m_dept["name"];

            //保存驳回工作项信息
            $end_node = g('mv_work_node') ->get_end_by_workid($this->com_id,$curr_workitem['work_id']);
            $work_node_id = $end_node['id'];
            $pre_work_node_id = json_encode([strval($work_node_id)]);

            $work_item = array(
                'formsetinit_id' => $curr_workitem["formsetinit_id"],
                'workitem_name' => $end_node['work_node_name'],
                'com_id' => $this->ComId,
                'handler_id' => $this->user_id,
                'handler_name' => $user["name"],
                'handler_dept_id' => $handle_dept_id,
                'handler_dept_name' => $handle_dept_name,
                'state' => self::$StateStop,
                'creater_id' => $this->user_id,
                'creater_name' => $user["name"],
                'creater_dept_id' => $handle_dept_id,
                'creater_dept_name' => $handle_dept_name,
                'receiver_id' => $this->user_id,
                'receiver_name' => $user["name"],
                'receiver_dept_id' => $handle_dept_id,
                'receiver_dept_name' => $handle_dept_name,
                'work_node_id' =>$work_node_id,
                'pre_work_node_id' =>$pre_work_node_id,
                'is_returnback' =>self::$Not_Callback,
                'returnback_node_id' =>0,
                'complete_time' => time(),
                'consuming_time' => 0,
                'judgement'=>$data['judgement'],
                'judgement_desc'=>$data['judgement_desc'],
            );
            $ret = g("mv_workinst")->save_workitem($work_item);
        } else {
            //更新当前事项的状态
            $curr_workitem["complete_time"] = time();
            if($curr_receive_time!=0){
                $curr_workitem["consuming_time"] = time()-$curr_receive_time;
            }
            $curr_workitem["judgement"] = $data['judgement'];
            $curr_workitem["judgement_desc"] = $data['judgement_desc'];
            $curr_workitem["target_node"] = "送{$curr_workitem['break_button_name']}";
            $ret = g('mv_workinst')->updateWorkitem($workitem_id,$this->com_id,$curr_workitem);
        }
        if(!$ret) throw new \Exception('操作失败');

        //判断实例是否申请通过后驳回，需查看配置推送短信 通过后驳回
        if($curr_workitem['formsetinst_state'] == self::$StateFormFinish){
            $this->sendSmsNotify($this->com_id,$curr_workitem['formsetinit_id'],\model\api\server\mv_proc\Mv_Sms_Model::TypeRejectAfterApproved);
        }


        //更新申请实例状态 终止
        $ret = g('mv_formsetinst')->update_formsetinst_state($curr_workitem['formsetinit_id'],$this->com_id,self::$StateFormStop);
        if(!$ret) throw new \Exception('操作失败');

        // 更新关联表单实例为已通过
        $this->recoverRevokeRelateFormsetinst($workitem_id);

        //实例名
        $formsetinst_name = $curr_workitem["formsetinst_name"];

        //通知推送给发起人
        //获取表单实例内容
        $formsetinst_id = $curr_workitem['formsetinit_id'];
        // 推送消息
        $user_id = $curr_workitem['formsetinst_creater_id'];
        $user = g('dao_user') -> get_by_id($user_id);

        $wx_title = $this -> AppCName."驳回提醒";
        $desc = $this -> AppCName."："."您发起的《".$curr_workitem["formsetinst_name"]."》流程已被驳回。";

        $wx_url = $this -> WxDomain.'?app='.$this -> AppName.'&a=detail&listType=2&activekey=0&id='.$formsetinst_id;
        $pc_url = $this -> PcDomain.'index.php?app='.$this -> AppName.'&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;

        //企业号提醒
        $qydev_title = "您的《{$curr_workitem["form_name"]}》申请表已被驳回";
        $qydev_desc = "您的《{$formsetinst_name}》已被驳回,请及时查看";

        try {
            $this -> _push_news($qydev_title, $wx_title, '', $qydev_desc, $desc, $wx_url, $pc_url, '', 2, $user_id, array($user_id), array(), $curr_workitem['app_version']);
        } catch (\Exception $e) {
            parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
        }

        // 知会
        $notify_user_ids = [];
        // 获取当前流程审批人 - 当前审批人
        $working_items = g('mv_workinst')->getWorkedWorkitem($formsetinst_id);
        if ($working_items) {
            $user_ids = array_unique(array_column($working_items,'handler_id'));
            $notify_user_ids = array_diff($user_ids,[$this->user_id]);
        }
        // 知会表单知会人员-当前申请人
        $_notify_user_ids = g("mv_notify")->getNotifysByFormsetinitId($formsetinst_id);
        if ($_notify_user_ids) {
            $_notify_user_ids = array_diff($_notify_user_ids,[$this->user_id]);
            $notify_user_ids = array_merge($notify_user_ids,$_notify_user_ids);
        }
        if ($notify_user_ids) {
            $notify_user_ids = array_unique($notify_user_ids);
            $users = g('dao_user')->getByIds($notify_user_ids);
            $this->notify_break_parent($formsetinst_id,$users);
        }



        return $ret;
    }
    /**
     * 恢复撤销关联的表单实例
     * @throws
     */
    public function recoverRevokeRelateFormsetinst($workitem_id)
    {
        $workitem = g('mv_workinst')->get_by_id($workitem_id);
        $formsetinit = g('mv_formsetinst')->get_by_id($workitem['formsetinit_id']);
        if (!$formsetinit['relate_formsetinst_id']) {
            return ;
        }

        $relate_formsetinst_id = $formsetinit['relate_formsetinst_id'];
        //更新申请实例状态已通过
        $ret = g('mv_formsetinst')->update_formsetinst_state($relate_formsetinst_id,$this->com_id,self::$StateFormFinish);
        if(!$ret) throw new \Exception('操作失败');

        $formsetinit_id = $formsetinit['id'];

        /** 更新原表单撤销中的工作项为通过后撤销 */
        // 获取原表单撤销中的工作项
        $workitem = g('mv_workinst')->getWorkitemByState($relate_formsetinst_id,self::$StateRevoking);
        // 获取撤销表单撤销理由
        // $reason = g('mv_formsetinst')->getFormsetinstRevokeReason($formsetinit_id);
        // 删除工作项
        g('mv_workinst')->delete_by_id($workitem['id']);
        return true;
    }
    /**
     * 撤销关联的表单实例
     * @throws
     */
    public function revokeRelateFormsetinst($workitem_id)
    {
        $workitem = g('mv_workinst')->get_by_id($workitem_id);
        $formsetinit = g('mv_formsetinst')->get_by_id($workitem['formsetinit_id']);
        if (!$formsetinit['relate_formsetinst_id']){
            return ;
        }
        $relate_formsetinst_id = $formsetinit['relate_formsetinst_id'];
        //更新申请实例状态撤销
        $ret = g('mv_formsetinst')->update_formsetinst_state($relate_formsetinst_id,$this->com_id,self::$StateFormFinishRevoke);
        if(!$ret) throw new \Exception('操作失败');

        $formsetinit_id = $formsetinit['id'];

        /** 更新原表单撤销中的工作项为通过后撤销 */
        // 获取原表单撤销中的工作项
        $workitem = g('mv_workinst')->getWorkitemByState($relate_formsetinst_id,self::$StateRevoking);
        // 获取撤销表单撤销理由
        $reason = g('mv_formsetinst')->getFormsetinstRevokeReason($formsetinit_id);
        // 更新工作项为通过已撤销
        g('mv_workinst')->update_workitem_data($workitem['id'],self::$StateFinishRevoke,$reason);

        // 知会原表单知会人员-当前申请人
        $notify_user_ids = g("mv_notify")->getNotifysByFormsetinitId($relate_formsetinst_id);
        $relate_formsetinit = g('mv_formsetinst')->get_by_id($relate_formsetinst_id);
        $notify_user_ids = array_diff($notify_user_ids,[$relate_formsetinit['creater_id']]);
        if ($notify_user_ids) {
            $users = g('dao_user')->getByIds($notify_user_ids);
            $this->notify_revoke_parent($relate_formsetinst_id,$users);
        }

        //短信通知 通过后撤销
        $this->sendSmsNotify($this->com_id,$relate_formsetinst_id,\model\api\server\mv_proc\Mv_Sms_Model::TypeRevocationAfterApproved);

        return true;
    }
    /**
     * 撤销流程
     * @param int $formsetinst_id
     * @param bool $is_finish [true 结束后撤销；false 直接撤销]
     * @throws
     * @return
     */
    public function revokeWorkitem($formsetinst_id,$is_finish=false){
        $formsetinst = g('mv_formsetinst')->get_by_id($formsetinst_id);

        if (!$formsetinst) {
            throw new \Exception("表单实例不存在！");
        }
        if($this->user_id != $formsetinst["creater_id"])
            throw new \Exception('无权限操作');

        if($formsetinst['state'] == self::$StateFormStop) {
            throw new \Exception("流程已终止");
        }
        if($formsetinst['state'] == self::$StateFormRevoke) {
            throw new \Exception("流程已撤销");
        }
        if (!$is_finish && $formsetinst['state'] == self::$StateFormFinish) {
            throw new \Exception("流程已结束，请发起撤销流程");
        }

        $start_node = $this->get_start_node($formsetinst['work_id']);
        if ($is_finish) {
            if($start_node['finish_revoke_state'] != self::$RevokeYes)
                throw new \Exception("当前流程未开启通过后撤销功能");
        } else {
            if($start_node['revoke_state'] != self::$RevokeYes)
                throw new \Exception("当前流程未开启撤销功能");
        }

        $user = g('dao_user')->get_by_id($this->user_id);

        $handle_m_dept = g("dao_user")->get_main_dept($this->user_id,FALSE);
        $handle_dept_id = $handle_m_dept["id"];
        $handle_dept_name = $handle_m_dept["name"];

        //保存撤销工作项信息
        if ($is_finish) {
            $state = self::$StateRevoking;
            $workitem_name = "结束";
            $end_node = g('mv_work_node') ->get_end_by_workid($this->com_id,$formsetinst['work_id']);
            $work_node_id = $end_node['id'];
            $pre_work_node_id = json_encode([strval($work_node_id)]);
        } else {
            $state = self::$StateRevoke;
            $workitem_name = "开始";
            $work_node_id = $start_node["id"];
            $pre_work_node_id = null;
        }
        $work_item = array(
            'formsetinit_id' => $formsetinst["id"],
            'workitem_name' => $workitem_name,
            'com_id' => $this->ComId,
            'handler_id' => $this->user_id,
            'handler_name' => $user["name"],
            'handler_dept_id' => $handle_dept_id,
            'handler_dept_name' => $handle_dept_name,
            'state' => $state,
            'creater_id' => $this->user_id,
            'creater_name' => $user["name"],
            'creater_dept_id' => $handle_dept_id,
            'creater_dept_name' => $handle_dept_name,
            'receiver_id' => $this->user_id,
            'receiver_name' => $user["name"],
            'receiver_dept_id' => $handle_dept_id,
            'receiver_dept_name' => $handle_dept_name,
            'work_node_id' =>$work_node_id,
            'pre_work_node_id' =>$pre_work_node_id,
            'is_returnback' =>self::$Not_Callback,
            'returnback_node_id' =>0,
            'complete_time' => time(),
            'consuming_time' => 0,
        );
        $workitem_id = g("mv_workinst")->save_workitem($work_item);
        if(!$workitem_id) throw new \Exception('操作失败');

        // ------ 通过后撤销 -------
        if ($is_finish) {
            //更新申请实例状态撤销中
            $ret = g('mv_formsetinst')->update_formsetinst_state( $formsetinst_id,$this->com_id,self::$StateFormRevoking);
            if(!$ret) throw new \Exception('操作失败');
            return $formsetinst;
        }

        // ------ 直接撤销 ------
        //更新申请实例状态撤销
        $ret = g('mv_formsetinst')->update_formsetinst_state( $formsetinst_id,$this->com_id,self::$StateFormRevoke);
        if(!$ret) throw new \Exception('操作失败');

        // 通知当前审批人
        //实例名
        $formsetinst_name = $formsetinst["formsetinst_name"];
        // 推送消息
        $user_id = $formsetinst['creater_id'];
        $user = g('dao_user') -> get_by_id($user_id);

        $wx_title = $this -> AppCName."撤销提醒";
        $desc = $this -> AppCName."："."您申请的《".$formsetinst_name."》流程已撤销。";

        $wx_url = $this -> WxDomain.'?app='.$this->AppName.'&a=detail&listType=2&activekey=0&id='.$formsetinst['id'];
        $pc_url = $this -> PcDomain.'index.php?app='.$this -> AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;

        //企业号提醒
        $qydev_title = "您申请的《{$formsetinst_name}》已撤销";
        $qydev_desc = "您申请的《{$formsetinst_name}》已撤销,请及时查看";

        // 通知自己
        $user_ids = [$user_id];
        try {
            $this -> _push_news($qydev_title, $wx_title, '', $qydev_desc, $desc, $wx_url, $pc_url, '', 2, $user_id, $user_ids, array(), $formsetinst['app_version']);
        } catch (\Exception $e) {
            parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
        }

        // 知会
        // 获取当前流程审批人
        $notify_user_ids = [];
        $working_items = g('mv_workinst')->getWorkedWorkitem($formsetinst_id);
        if ($working_items) {
            $notify_user_ids = array_column($working_items,'handler_id');
        }

        // 知会原表单知会人员-当前申请人
        $_notify_user_ids = g("mv_notify")->getNotifysByFormsetinitId($formsetinst_id);
        if ($_notify_user_ids) {
            $_notify_user_ids = array_diff($_notify_user_ids,[$this->user_id]);
            $notify_user_ids = array_merge($notify_user_ids,$_notify_user_ids);
        }
        // 知会推送
        if ($notify_user_ids) {
            $notify_user_ids = array_unique($notify_user_ids);
            $users = g('dao_user')->getByIds($notify_user_ids);
            $this->notify_revoke_parent($formsetinst_id,$users);
        }

        return $formsetinst;
    }
    /**
     * 获取所有的退回节点信息
     * @param int $formsetinst_id 实例id
     * @param int $workitem_id 工作项id
     * @param boolean $level 往上递归次数 -1为递归到开始节点
     * @param string $node_fields 查找节点的信息
     */
    public function get_callback_parent($formsetinst_id, $workitem_id, $level, $node_fields='*') {
        if (empty($formsetinst_id) || empty($workitem_id)) return array();

        $curr_item = g("mv_workinst")->get_by_itemid($formsetinst_id, $workitem_id, $this->ComId, TRUE,' mpw.work_node_id,mpwn.work_id ');

        //只获取已经发生的流程节点信息就可以,来遍历获取可回退节点信息
        $work_nodes_cond = array();
        $formsetinst_nodes = g('mv_workinst')->get_item_by_formsetinst_id($formsetinst_id, $this->ComId);
        if($formsetinst_nodes){
            $work_node_ids = array_column($formsetinst_nodes,'work_node_id');
            $work_node_ids = array_unique($work_node_ids);
            $work_nodes_cond =array(
                'id IN'=>$work_node_ids,
            );
        }

        $work_nodes = g("mv_work_node")->get_worknode_by_condition($this->ComId, $curr_item["work_id"],$work_nodes_cond);
        $tmp_nodes = array();
        foreach ($work_nodes as $node) {
            $tmp_nodes[$node['id']] = $node;
        }unset($node);
        $work_nodes = $tmp_nodes;

        $pre_node_ids = array();
        $this->get_pre_node($pre_node_ids, $work_nodes, $curr_item["work_node_id"], $level);
        //获取节点所属的工作项
        $pre_other_conds = array(
            'is_callback=' => self::$CallBackNo,
            'state=' =>self::$StateSend
        );
        $pre_nodes = g("mv_workinst")->get_item_by_fwc($formsetinst_id, $pre_node_ids, $this->ComId, $pre_other_conds, $node_fields);

        if (empty($pre_nodes)) throw new \Exception('找不到退回的工作项');
        return $pre_nodes;
    }

    /**
     * 递归获取上一步骤节点信息
     * @param array $pre_node_ids 结果集
     * @param array $work_nodes 节点数组
     * @param int $work_node_id 当前流程节点id
     * @param int $level 递归次数
     */
    private function get_pre_node(&$pre_node_ids, $work_nodes, $work_node_id, $level){
        $new_pre_nodes = array();
        foreach ($work_nodes as $key => $work_node){
            $next_node_ids = $work_node["next_node_id"];
            $next_node_ids = json_decode($next_node_ids, TRUE);
            if(in_array($work_node_id, $next_node_ids)){
                $pre_node_ids[] = $work_node["id"];
                $new_pre_nodes[] = $work_node["id"];
                unset($work_nodes[$key]);
            }
        }unset($work_node);

        $level == -1 ? $level : $level--;
        if (!(empty($new_pre_nodes) || $level == 0)) {
            foreach ($new_pre_nodes as $new_pre_node) {
                $this->get_pre_node($pre_node_ids, $work_nodes, $new_pre_node, $level);
            }unset($pre_node);
        }
        return;
    }
	
	/**
	 * 推送二维码消息
	 * @param string $md5_key this_url md5
	 * @param string $func 回调js
	 */
	public function send_pc_info_parent($md5_key,$func){
		try{
			//发送PC提醒
			$data = array(
				'ident' => 5,
				'ajax_url' => $this->PcDomain.'index.php?&key='.$md5_key.'&fun='.$func
			);
			g('api_messager') -> publish_by_user('pc', $this->UserId, $data);
		}catch(\Exception $e){
			parent::log_e("fail_user_id:".$user_id);
		}
	}
	
	public function save_linksign($workitem_id,$img_url){
		g("mv_workinst")->update_workitem($workitem_id,$this->ComId,$img_url,self::$Linksign);
	}
	
	
	/**
	 * 获取下一步骤new
	 * @param int $workitemid 工作项id
	 * @throws SCException
	 */
	public function get_next_workitem($workitem_id){
		$field = ' mpw.*,mpwn.* ';
		$workitem = $this->get_curr_workitem($workitem_id,$field);
		$circulation_rule = $workitem["circulation_rule"];//流转规则
		
		$formsetinst_id = $workitem['formsetinit_id'];
		$formsetinst_val = $this -> get_form_vals($formsetinst_id);
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
		$formsetinst['form_vals'] = $formsetinst_val;

		$next_node_id_arr = array();
		if(empty($circulation_rule)){//流转规则为空
			$next_node_ids = $workitem["next_node_id"];
			$next_node_ids = json_decode($next_node_ids, TRUE);
			
			$this->get_receiver_rule($next_node_ids, $next_node_id_arr,$workitem,$formsetinst,$this->ComId);
		} else{//如果存在流转规则
			$circulation_rule = json_decode($circulation_rule, TRUE);
			// 请假、外出表单转换时长控件
			if ($formsetinst['is_other_proc'] == self::$IsNewRest || $formsetinst['is_other_proc'] == self::$IsLegwork) {
				$this -> change_special_input($formsetinst['is_other_proc'], $formsetinst_val);
			}
			// if ($formsetinst['is_other_proc'] == 0) {
			// 	$formsetinst_val = $this -> get_form_vals($formsetinst_id);
			// } else {
			// 	$formsetinst_val = $formsetinst["form_vals"];
			// 	$formsetinst_val = json_decode($formsetinst_val, TRUE);
			// }

			$next_node_ids = $workitem["next_node_id"];
			$next_node_ids = json_decode($next_node_ids, TRUE);
			$tmp_next_node_ids = $next_node_ids;
			$work_result_id = array();
			foreach ($circulation_rule as $val){
				//格式：XXXX#XXX#XXX;XXXX#XXX#XXX;XXXX#XXX#XXX@work_node_id
				$circulation_rule_arr = explode("@", $val);
				$work_id = $circulation_rule_arr[1];
				$rule_arr = explode(";",$circulation_rule_arr[0]);
				//验证表单规则
				$this->is_circulation_rule($rule_arr, $formsetinst_val, $work_id, $work_result_id,$workitem,$next_node_ids);
				
			}
			if(count($work_result_id)>0){
				$next_node_ids = $work_result_id; 
			}
			else{
				if(count($next_node_ids)==0){
					$next_node_ids = $tmp_next_node_ids;
				}
			}
			$this->get_receiver_rule($next_node_ids, $next_node_id_arr,$workitem,$formsetinst,$this->ComId);
		}
		
		if(!is_array($next_node_id_arr)){
			throw new \Exception('找不到下一步接收人，请联系管理员');
		}
		return $next_node_id_arr;
	}
	
	
	/**
	 * 判断是否直接发送new
	 * @param array $next_node_id_arr
	 */
	public function is_send($next_node_id_arr){
		$is_send = "TRUE";//是否直接发送
		if(count($next_node_id_arr)!=1){
			$is_send = "FALSE";
		}
		else{
			$next_node_id = $next_node_id_arr[0];
			
			if((count($next_node_id["select_array"])!=0)||((count($next_node_id["receive_array"])==0)&&(!empty($next_node_id['next_node_id'])))){
				$is_send = "FALSE";
			}
		}
		return $is_send;
	}
	
	
	/**
	 * 获取当前实例new
	 * @param int $formsetinst_id 实例id
	 */
	public function get_formsetinst_by_id($formsetinst_id, $fields='mpf.*,mpfm.form_name,mpfm.form_describe,mpfm.is_linksign,mpfm.form_history_id '){
		$formsetinst = g("mv_formsetinst")->get_formsetinst_by_id($formsetinst_id,$this->ComId,$this->UserId, $fields);
		
		if(is_array($formsetinst)){
			if($formsetinst["info_state"]==self::$StateOff){
				throw new \Exception('该实例已被删除!');
			}
		}
		else{
			throw new \Exception('没有权限查看该步骤！');
		}
		
		return $formsetinst;
	}
	
	
	/**
	 * 获取开始节点的信息new
	 * @param int $work_id 流程模板id
	 */
	public function get_start_node($work_id){
		$start_node = g('mv_work_node') -> get_start_by_workid($this->ComId, $work_id);
		if(!$start_node){
			throw new \Exception('流程有误，找不到开始节点，请联系管理员！');
		}
		return $start_node;
	}
	
	/**
	 * 获取当前步骤new
	 * @param int $workitem_id
     * @throws
     * @return
	 */
	public function get_curr_workitem($workitem_id,$fields=NULL){
		if(empty($fields)){
			$cur_workitem = g("mv_workinst")->get_curr_workitem($workitem_id,$this->ComId,$this->UserId);
		} else {
			$cur_workitem = g("mv_workinst")->get_curr_workitem($workitem_id,$this->ComId,$this->UserId,$fields);
		}

		if(is_array($cur_workitem)){
			if($cur_workitem["info_state"]==self::$StateOff){
				throw new \Exception('该工作项已被发起人删除！');
			}
		} else{
			throw new \Exception('没有权限访问该工作项！');
		}
		return $cur_workitem;
	}

    /** 根据节点获取工作项信息 */
    public function get_workitems_by_nodeid($formsetinst_id, $node_id, $fields='') {
        if (empty($fields)) {
            $fields = " mpw.id, mpw.workitem_name,mpw.handler_id,su.name handler_name,
			su.pic_url handler_pic_url,su.acct handler_acct,mpw.state,mpw.is_callback ";
        }
        $workitems = g('mv_workinst') -> get_node_workitem($formsetinst_id, $node_id, $this -> ComId, '', $fields);
        $item_list = array();

        foreach ($workitems as $key => $item) {
            if ( $item['state']== self::$StateTurn || ($item['is_callback'] && in_array($item['state'],[self::$StateCallback,self::$StateDone,self::$StateSendback])) ) {
                unset($workitems[$key]);
            } else {
                !isset($item_list[$item['handler_id']]) && $item_list[$item['handler_id']] = $item;
            }
        }

        return array_values($item_list);
    }


    /**
     * 获取开始节点工作项new
     * @param string $workitem_rule
     * @param int $formsetinit_id
     */
    public function get_start_item($work_id, $formsetinst_id, $fields='', $user_id=0){
        if(empty($work_id)) throw new \Exception('流程id为空，请联系管理员！');

        $start_node = g("mv_work_node")->get_start_by_workid($this->ComId, $work_id, 'id');
        if(empty($start_node)) throw new \Exception('找不到开始节点，请联系管理员！');

        empty($user_id) && $user_id = $this->UserId;
        //开始节点的工作项有可能有多个
        $start_workitems = g("mv_workinst")->get_node_workitem($formsetinst_id, $start_node["id"], $this->ComId, $user_id, $fields);
        if(empty($start_workitems)) throw new \Exception('找不到开始节点的工作项，请联系管理员！');

        return $start_workitems[0];
    }
	
	
	/**
	 * 获取知会详情new
	 * @param int $notify_id
	 */
	
	public function get_notify_item($notify_id, $fields=' mpn.*,mpf.form_vals,mpf.creater_id,mpf.creater_name,mpf.create_time,mpf.state formsetinst_state,su.pic_url,mpfm.form_name,mpfm.form_describe '){
		$notify = g("mv_notify")->get_notify_by_id($notify_id,$this->ComId,$this->UserId, $fields);
		
		if(is_array($notify)){
			if($notify["info_state"] == self::$StateOff){
				throw new \Exception('该表单实例已被发起人删除！');
			}
		}
		else{
			throw new \Exception('没有权限访问该知会信息！');
		}
		
		return $notify;
	}
	
	/**
	 * 更新知会状态new
	 * @param int $notify_id 知会id
	 */
	public function update_notify_state_parent($notify_id){
		g("mv_notify")->update_notify_state($notify_id,$this->ComId,$this->UserId);
	}
	
	
	/**
	 * 根据节点ID获取模板信息new
	 * @param int $work_id 流程模板ID 
	 */
	public function get_work_node($work_id){
		$work_model = g("mv_work")->get_by_id($this->ComId,$work_id);
			
		if(!is_array($work_model)){
			throw new \Exception('找不到对应的流程，请联系管理员！');
		}	
		
		return $work_model;
	}
	
	
	/**
	 * 获取流程图的信息new
	 * @param array $formsetinst 实例对象
	 */
	public function get_pic_rule($formsetinst){
		
		$workitem_rule = $formsetinst["workitem_rule"];
		$curr_node_id = $formsetinst["curr_node_id"];
		
		if(strripos($workitem_rule,"@")>-1){
			$workitem_rule_arr = explode("@",$workitem_rule);
			$workitem_rule = array_shift($workitem_rule_arr);
			$workitem_rule = explode(',', $workitem_rule);
			$workitem_back_arr = $workitem_rule_arr;
			foreach ($workitem_back_arr as $wb_arr){
				$back_key = 0;
				foreach ($workitem_rule as $w_key=>$w_rule){
					if($w_rule==$wb_arr){
						$back_key = $w_key;
					}
				}
				$workitem_rule = array_slice($workitem_rule, 0, $back_key+1);
			}
			
			$workitem_rule = implode(',', $workitem_rule);
		}
		
		$worknodes = g("mv_work_node")->get_work_node_by_id($this->ComId,$workitem_rule);
		
		$workitem_rule = explode(',', $workitem_rule);
		$workitem_rule = array_unique($workitem_rule);
		
		if(!is_array($worknodes)||count($workitem_rule)!=count($worknodes)){
			throw new \Exception('找不到对应的节点，请联系管理员！');
		}
		
		$worknodes_arr = array();
		foreach($workitem_rule as $w_rule){
			$cur_worknode=NULL;
			foreach ($worknodes as $worknode){
				if($w_rule == $worknode["id"]){
					$cur_worknode = $worknode;
					break;
				}
			}
			unset($worknode);
			if($curr_node_id==$cur_worknode["id"]&&$formsetinst["state"]==self::$StateFormDone){
				$worknodes_arr[$cur_worknode["node_key"]] = self::$PicStateDoing;
			}else if($curr_node_id==$cur_worknode["id"]&&$formsetinst["state"]==self::$StateFormStop){
                $worknodes_arr[$cur_worknode["node_key"]] = self::$PicStateStop;
            }
			else{
				$worknodes_arr[$cur_worknode["node_key"]] = self::$PicStateDone;
			}
		}
		$worknodes_arr = json_encode($worknodes_arr);
		return $worknodes_arr;
	}
    /**
     * 知会流程
     */
    public function notify_break_parent($formsetinst_id,$users){
        //获取表单流程实例
        $fields = 'mpf.id,mpf.formsetinst_name,mpf.info_state,mpf.app_version,mpfm.form_name,mpf.creater_name';
        $formsetinst = $this->get_formsetinst_by_id($formsetinst_id, $fields);
        $creater_name = $formsetinst['creater_name'];
        foreach($users as $user){
            $receiver_id = $user["id"];
            $receiver_name = $user["name"];

            $notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);

            if (empty($receiver_id)) throw new \Exception('找不到知会的人员！');

            $wx_title = $this -> AppCName . '知会提醒';
            $desc = $this->AppCName.'：'.$creater_name.'的《'.$formsetinst["formsetinst_name"].'》申请已被驳回，请及时查看';
            $new_desc = $creater_name."的《".$formsetinst["formsetinst_name"]."》申请已被驳回，请及时查看";
            $new_title = "收到【".$creater_name."】关于《".$formsetinst["form_name"]."》的驳回知会信息";
            // $new_title = $formsetinst["form_name"];
            $msg_title = "收到了【" . $creater_name .'】的被驳回知会信息';

            $wx_url = $this->WxDomain.'index.php?app='.$this->AppName.'&a=detail&listType=3&activekey=0&id='.$notify_id;
            $pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;

            try {
                $this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 3, $this -> user_id, array($receiver_id), array(), $formsetinst['app_version']);
            } catch (\Exception $e) {
                parent::log_e("推送消息给【 " . $receiver_name . "】失败！");
            }
        }unset($user);
    }
    /**
     * 知会流程
     */
    public function notify_revoke_parent($formsetinst_id,$users){
        //获取表单流程实例
        $fields = 'mpf.id,mpf.formsetinst_name,mpf.info_state,mpf.app_version,mpfm.form_name,mpf.creater_name';
        $formsetinst = $this->get_formsetinst_by_id($formsetinst_id, $fields);

        $creater_name = $formsetinst['creater_name'];
        foreach($users as $user){
            $receiver_id = $user["id"];
            $receiver_name = $user["name"];

            $notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);

            if (empty($receiver_id)) throw new \Exception('找不到知会的人员！');

            $wx_title = $this -> AppCName . '知会提醒';
            $desc = $this->AppCName.'：'.$creater_name.'的《'.$formsetinst["formsetinst_name"].'》申请已撤销，请及时查看';
            $new_desc = $creater_name."的《".$formsetinst["formsetinst_name"]."》申请已撤销，请及时查看";
            $new_title = "收到【".$creater_name."】关于《".$formsetinst["form_name"]."》的撤销知会信息";
            // $new_title = $formsetinst["form_name"];
            $msg_title = "收到了【" . $creater_name .'】的撤销知会信息';

            $wx_url = $this->WxDomain.'index.php?app='.$this->AppName.'&a=detail&listType=3&activekey=0&id='.$notify_id;
            $pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;

            try {
                $this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 3, $this -> user_id, array($receiver_id), array(), $formsetinst['app_version']);
            } catch (\Exception $e) {
                parent::log_e("推送消息给【 " . $receiver_name . "】失败！");
            }
        }unset($user);

    }
	/**
	 * 知会流程
	 */
	public function notify_workitem_parent($data){
		//获取表单流程实例
		$fields = 'mpf.id,mpf.formsetinst_name,mpf.info_state,mpf.app_version,mpfm.form_name';
		$formsetinst = $this->get_formsetinst_by_id($data["formsetinst_id"], $fields);
		
		foreach($data["receivers"] as $receiver){
			$receiver_id = $receiver["receiver_id"];
			$receiver_name = $receiver["receiver_name"];
			$notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);
			
			if (empty($receiver_id)) throw new \Exception('找不到知会的人员！');

			$wx_title = $this -> AppCName . '知会提醒';
			$desc = $this->AppCName.'：'.$this->UserName.'将流程《'.$formsetinst["formsetinst_name"].'》知会给您，请及时了解流程详情。';
            $new_desc = $this -> UserName."将《".$formsetinst["formsetinst_name"]."》知会给您，请及时查看";
            $new_title = "收到【".$this -> UserName."】关于《".$formsetinst["form_name"]."》的知会信息";
			// $new_title = $formsetinst["form_name"];
			$msg_title = "收到了【" . $this -> UserName .'】的知会信息';

			$wx_url = $this->WxDomain.'index.php?app='.$this->AppName.'&a=detail&listType=3&activekey=0&id='.$notify_id;
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;

			try {
				$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 3, $this -> user_id, array($receiver_id), array(), $formsetinst['app_version']);
			} catch (\Exception $e) {
				parent::log_e("推送消息给【 " . $receiver_name . "】失败！");
			}
		}unset($receiver);

	}
	
	/**
	 * 催办流程
	 */
	public function press_workitem_parent($data){
		
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($data["formsetinst_id"]);
	
		//获取当前正在进行的工作项
		$curr_node_id = $formsetinst["curr_node_id"];
		$doing_workitems = g("mv_workinst")->get_node_state_item($data["formsetinst_id"],$curr_node_id,array(self::$StateNew,self::$StateRead,self::$StateSave),$this->ComId);
		if(!is_array($doing_workitems)){throw new \Exception('找不到催办的工作项！');}
		
		foreach ($doing_workitems as $workitem){
			$handler_id = $workitem["handler_id"];
			if (empty($handler_id)) throw new \Exception('找不到催办的人员！');

			$wx_title = $this -> AppCName."催办提醒";
			$desc = $this -> AppCName."：".$this -> UserName."对您需要处理的流程《".$formsetinst["formsetinst_name"]."》进行了催办，请及时审批流程。";
            $new_title = "收到【{$this -> UserName}】关于《{$formsetinst['form_name']}》的催办信息";
            $new_desc = "收到《{$formsetinst["formsetinst_name"]}》的催办信息，请及时处理";
			// $new_title = $formsetinst["form_name"];
			$msg_title = "收到了【" .$this -> UserName . '】的催办信息';

			$wx_url = $this->WxDomain.'index.php?app='.$this->AppName.'&a=detail&listType=1&activekey=0&id='.$workitem["id"];
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem["id"];

			try {
				$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 1, $this -> user_id, array($handler_id), array(), $formsetinst['app_version']);
			} catch (\Exception $e) {
				parent::log_e("推送消息失败！id:".$handler_id);
			}
		} unset($workitem);
	}
	
	/**
	 * 删除工作项
	 */
	public function del_workitem_parent($data){
		$ret = array();
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		if($curr_workitem["is_start"]==0){throw new \Exception('没有权限删除工作项！');}
		
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		
		//获取未完成的工作项
		$not_finish_items = g("mv_workinst")->get_formset_state_item_nwi($formsetinst["id"],$data["workitem_id"],array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId);
		$count = empty($not_finish_items)?0:count($not_finish_items);
		if($count==0&&($curr_workitem["state"]==self::$StateNew||$curr_workitem["state"]==self::$StateRead||$curr_workitem["state"]==self::$StateSave)){
			g("mv_formsetinst")->del_formsetInst($formsetinst["id"],$this->ComId,$this->UserId);
			g("mv_workinst")->delete_workitem($formsetinst["id"],$this->ComId,$this->UserId);
			g("mv_file")->del_file_by_formsetid($this->ComId,$this->UserId,$formsetinst["id"]);
			g("mv_notify")->delete_notify($formsetinst["id"],$this->ComId,$this->UserId);
			g("mv_formsetinst") -> del_formsetinst_label($formsetinst['id'], $formsetinst['form_history_id']);
			//删除特殊表单记录
			if ($formsetinst['is_other_proc'] == self::$IsMakeup) {
				g('ws_checkwork') -> del_record($this->ComId, $formsetinst["id"]);
			} elseif ($formsetinst['is_other_proc'] == self::$IsNewRest) {
				g('ws_rest') -> delete_rest($this->ComId, $formsetinst["id"]);
			} elseif ($formsetinst['is_other_proc'] == self::$IsLegwork) {
				g('ws_legwork') -> delete_legwork($this->ComId, $formsetinst["id"]);
			}
			$errmsg = '流程删除成功！';
			$errcode = 0;
		}
		else{
			$errmsg = '删除失败，流程已经在流转或者已经结束，没有权限删除工作项！';
			$errcode = 1;
		}
		$ret["errmsg"] = $errmsg;
		$ret["errcode"] = $errcode;
		
		return $ret;
	}
	
	/**
	 * 发送到退回步骤
	 */
	public function sendback_workitem_parent($data){
		$ret = array();
			
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		
		if($curr_workitem["state"]!=self::$StateNew&&$curr_workitem["state"]!=self::$StateRead&&$curr_workitem["state"]!=self::$StateSave){
			throw new \Exception('该工作项已经处理，请返回代办列表');
		}
		
		$returnback_node_id = $curr_workitem["returnback_node_id"];
		if(empty($returnback_node_id)){throw new \Exception('找不到退回节点！');}
		
		$returnback_workitem = g("mv_workinst")->get_item_by_ids(array($returnback_node_id),$this->ComId);
		$returnback_workitem = $returnback_workitem[0];
		
		//将同级退回状态状态转成原来的状态
		g("mv_workinst")->update_sendback_workitem($curr_workitem["formsetinit_id"],$returnback_node_id,$this->ComId);

		$handle_id = $returnback_workitem["handler_id"];
		$create_id = $returnback_workitem["creater_id"];
		$receiver_id = $curr_workitem["handler_id"];
		$handle_m_dept = g("dao_user")->get_main_dept($handle_id,FALSE);
		if($handle_m_dept){
			$handle_dept_id = $handle_m_dept["id"];
			$handle_dept_name = $handle_m_dept["name"];
		}
		else{
			$handle_dept_id = -1;
			$handle_dept_name = "无部门";
		}
		$create_m_dept = g("dao_user")->get_main_dept($create_id,FALSE);
		if($create_m_dept){
			$create_dept_id = $create_m_dept["id"];
			$create_dept_name = $create_m_dept["name"];
		}
		else{
			$create_dept_id = -1;
			$create_dept_name = "无部门";
		}
		$receiver_m_dept = g("dao_user")->get_main_dept($receiver_id,FALSE);
		if($receiver_m_dept){
			$receiver_dept_id = $receiver_m_dept["id"];
			$receiver_dept_name = $receiver_m_dept["name"];
		}
		else{
			$receiver_dept_id = -1;
			$receiver_dept_name = "无部门";
		}

        $com_id = isset($curr_workitem['com_id'])?$curr_workitem['com_id']:$this->ComId;
		//保存工作项信息
		$work_item = array(
			'formsetinit_id' => $curr_workitem["formsetinit_id"],
			'workitem_name' => $returnback_workitem["workitem_name"],
			'com_id' => $com_id,
			'handler_id' => $returnback_workitem["handler_id"],
			'handler_name' => $returnback_workitem["handler_name"],
			'handler_dept_id' =>$handle_dept_id,
			'handler_dept_name' =>$handle_dept_name,
			'state' => self::$StateNew,
			'creater_id' => $returnback_workitem["creater_id"],
			'creater_name' => $returnback_workitem["creater_name"],
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name,
			'receiver_id' => $curr_workitem["handler_id"],
			'receiver_name' => $curr_workitem["handler_name"],
			'receiver_dept_id' =>$receiver_dept_id,
			'receiver_dept_name' =>$receiver_dept_name,
			'work_node_id' =>$returnback_workitem["work_node_id"],
			'pre_work_node_id'=>$returnback_workitem["pre_work_node_id"],
			'is_returnback' =>self::$Not_Callback,
			'returnback_node_id' =>0,
		);
		$workitem_id = g("mv_workinst")->save_workitem($work_item);
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["judgement"] = $data["judgement"];
		$curr_workitem["target_node"] = "送".$returnback_workitem["workitem_name"];
		g("mv_workinst")->send_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
		
		//更新流程流转
		$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		$workitem_rule = $formsetinst["workitem_rule"];
		$workitem_rule = substr($workitem_rule,0,strripos($workitem_rule, "@".$curr_workitem["work_node_id"]));
		g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$returnback_workitem["work_node_id"]);
		
		//消息提醒
		$user_id = $returnback_workitem["handler_id"];
		$user = g('dao_user') -> get_by_id($user_id);
		
		$wx_title = $this -> AppCName."待办提醒";
		$desc = $this->AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》待办事项，请及时处理。";
        $new_desc = "收到《".$formsetinst["formsetinst_name"]."》的待办信息，请及时处理";
        $new_title = "收到【".$formsetinst['creater_name']."】关于《".$formsetinst["form_name"]."》的待办信息";
		// $new_title = $formsetinst["form_name"];
		$msg_title = "收到了【" .$formsetinst['creater_name'] . '】的待办信息';

		$wx_url = $this->WxDomain.'?app='.$this->AppName.'&a=detail&listType=1&activekey=0&id='.$workitem_id;
		$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;

		try {
			$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 1, $formsetinst["creater_id"], array($user_id), array(), $formsetinst['app_version']);
		} catch (\Exception $e) {
			parent::log_e("推送消息给【 " . $user["name"] . "】失败！");
		}

		$ret["workitem_name"] = $returnback_workitem["workitem_name"];
		$ret["receiver_name"] = $user["name"];

        //退回的工作节点信息
        $work_nodes = array(
            array(
                'id'=>$returnback_workitem["work_node_id"],
                'receiver_arr'=>[
                    ['receiver_id'=>$curr_workitem["handler_id"]]
                ],
            ),
        );
        $notity_people = $this->notify_process($curr_workitem,$curr_workitem["formsetinit_id"],$work_nodes);

//		//发送知会给隐患控件相关人员
//        $formsetinst_form_vals = $this->get_form_vals($curr_workitem["formsetinit_id"]);
//        $input_notify_people = $this->getInputNotifyUsers($curr_workitem,$formsetinst_form_vals);
//        $this->pushNotifyNews($input_notify_people,$formsetinst);

		return $ret;
	}
	
	
	/**
	 * 驳回指定步骤流程
	 */
	public function callback_select_parent($data){
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		
		//将同级工作项状态转成退回状态
		g("mv_workinst")->update_node_state_item_nwi($curr_workitem["formsetinit_id"],$curr_workitem["id"],$curr_workitem["work_node_id"],self::$StateSendback,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$curr_workitem["id"]);
		//将等待中的工作项状态转成退回状态
		g("mv_workinst")->update_formset_state_item($curr_workitem["formsetinit_id"],self::$StateSendback,array(self::$StateWait),$this->ComId,$curr_workitem["id"]);

        //更新退回节点的退回状态
        g("mv_workinst")->update_cs_by_i($curr_workitem["formsetinit_id"],$data["callback_workitem_id"],$this->ComId);
		
		$callback_workitem = g("mv_workinst")->get_item_by_ids(array($data["callback_workitem_id"]),$this->ComId);
		if(!is_array($callback_workitem)){throw new \Exception('退回步骤不存在，请联系管理员');}
		$callback_workitem = $callback_workitem[0];
		
		$handle_id = $callback_workitem["handler_id"];
		$create_id = $curr_workitem["creater_id"];
		$receiver_id = $this->UserId;
		$handle_m_dept = g("dao_user")->get_main_dept($handle_id,FALSE);
		if($handle_m_dept){
			$handle_dept_id = $handle_m_dept["id"];
			$handle_dept_name = $handle_m_dept["name"];
		}
		else{
			$handle_dept_id = -1;
			$handle_dept_name = "无部门";
		}
		$create_m_dept = g("dao_user")->get_main_dept($create_id,FALSE);
		if($create_m_dept){
			$create_dept_id = $create_m_dept["id"];
			$create_dept_name = $create_m_dept["name"];
		}
		else{
			$create_dept_id = -1;
			$create_dept_name = "无部门";
		}
		$receiver_m_dept = g("dao_user")->get_main_dept($receiver_id,FALSE);
		if($receiver_m_dept){
			$receiver_dept_id = $receiver_m_dept["id"];
			$receiver_dept_name = $receiver_m_dept["name"];
		}
		else{
			$receiver_dept_id = -1;
			$receiver_dept_name = "无部门";
		}
		
		//保存工作项信息
		$work_item = array(
			'formsetinit_id' => $curr_workitem["formsetinit_id"],
			'workitem_name' => $callback_workitem["workitem_name"],
			'com_id' => $this->ComId,
			'handler_id' => $callback_workitem["handler_id"],
			'handler_name' => $callback_workitem["handler_name"],
			'handler_dept_id' =>$handle_dept_id,
			'handler_dept_name' =>$handle_dept_name,
			'state' => self::$StateNew,
			'creater_id' => $curr_workitem["creater_id"],
			'creater_name' => $curr_workitem["creater_name"],
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name,
			'receiver_id' => $this->UserId,
			'receiver_name' => $this->UserName,
			'receiver_dept_id' =>$receiver_dept_id,
			'receiver_dept_name' =>$receiver_dept_name,
			'work_node_id' =>$callback_workitem["work_node_id"],
			'pre_work_node_id' => $callback_workitem["pre_work_node_id"],
			'is_returnback' =>self::$Callback,
			'returnback_node_id' =>$curr_workitem["id"],
		);
		$workitem_id = g("mv_workinst")->save_workitem($work_item);			
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["judgement"] = $data["judgement"];
		$curr_workitem["target_node"] = "退回".$callback_workitem["workitem_name"];
		g("mv_workinst")->callback_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
		//更新流程流转
		$formsetInst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		$workitem_rule = $formsetInst["workitem_rule"];
		$workitem_rule.="@".$callback_workitem["work_node_id"];
		g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$callback_workitem["work_node_id"]);
		
		//消息提醒
		$user_id = $callback_workitem["handler_id"];
		$user = g('dao_user') -> get_by_id($user_id);

		$wx_title = $this -> AppCName."退回提醒";
		$desc = $this -> AppCName."："."您收到了《".$formsetInst["formsetinst_name"]."》退回事项，请及时处理。";
		$new_desc = "您的《".$formsetInst["form_name"]."》已退回，请知晓";
		$new_title = "流程申请已退回";
		// $new_title = $formsetInst["form_name"];
		$msg_title = "收到了【" . $this -> UserName . '】的退回提醒';

		$wx_url = $this->WxDomain.'?app='.$this->AppName.'&a=detail&listType=1&activekey=0&id='.$workitem_id;
		$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;

		try {
			$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 1, $this -> user_id, array($user_id), array(), $formsetinst['app_version']);
		} catch (\Exception $e) {
			parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
		}

		$ret = array();
		$ret["workitem_name"] = $callback_workitem["workitem_name"];
		$ret["receiver_name"] = $user["name"];
		return $ret;
	}


    /** 驳回开始步骤流程 */
    public function callback_start_parent($data){
        //获取当前步骤的信息
        $fields = " mpw.formsetinit_id,mpw.info_state,mpf.work_id,mpf.creater_id ";
        $item = $this->get_curr_workitem($data["workitem_id"], $fields);

        $back_item = $this->get_start_item($item['work_id'], $item['formsetinit_id'], 'mpw.id', $item['creater_id']);
        //驳回
        $judgement_desc = isset($data['judgement_desc'])?$data['judgement_desc']:'';
        $this->callback_parent($data['workitem_id'], $back_item['id'], $data['judgement'], $judgement_desc);
    }

    /** 退回上一步骤 */
    public function callback_pre_parent($data){
        //获取当前步骤的信息
        $item = $this->get_curr_workitem($data["workitem_id"], "mpw.formsetinit_id,mpw.info_state");

        $fields = "id,handler_name,workitem_name,complete_time";
        $item_list = $this->get_callback_parent($item['formsetinit_id'], $data["workitem_id"], 1, $fields);

        if (count($item_list) == 1) {
            $pre_workitem = $item_list[0];
            //只有一个节点
            $judgement_desc = isset($data['judgement_desc'])?$data['judgement_desc']:'';
            $this->callback_parent($data['workitem_id'], $pre_workitem['id'], $data['judgement'],$judgement_desc);

        } else {
            //弹出退回的人
            $ret = array(
                'errcode' => 1,
                'errmsg' => '选择流程退回步骤',
                'callback_workitem' => $item_list
            );
            return $ret;
        }
    }

    /**
     * 退回到指定步骤
     * @param int $curr_workitem_id 当前工作项id
     * @param int $back_workitem_id 退回工作项id
     * @param string $judgement 意见
     * @param string $judgement_desc 意见补充描述
     */
    public function callback_parent($curr_workitem_id, $back_workitem_id, $judgement, $judgement_desc='') {
        //获取当前步骤的信息
        $field = " mpw.com_id,mpw.formsetinit_id,mpw.info_state,mpw.work_node_id,mpw.pre_work_node_id,mpw.receive_time,mpwn.next_node_id,mpw.state ";
        $curr_workitem = $this->get_curr_workitem($curr_workitem_id, $field);
        if(!in_array($curr_workitem["state"], self::$StateInHand)){
            throw new \Exception('该工作项已经处理，请返回代办列表');
        }
        $curr_formsetinit_id = $curr_workitem["formsetinit_id"];
        $curr_work_node_id = $curr_workitem["work_node_id"];
        $curr_receive_time = $curr_workitem["receive_time"];
        $curr_next_node_id = json_decode($curr_workitem["next_node_id"],TRUE);
        //将同级工作项处理中状态转成退回状态
        g("mv_workinst")->update_ws_by_ns($curr_formsetinit_id,$curr_work_node_id,self::$StateSendback,self::$StateDoing,$this->ComId,$curr_workitem_id,TRUE);
        //将下一节点等待中的工作项状态转成退回状态
        g("mv_workinst")->update_ws_by_ns($curr_formsetinit_id,$curr_next_node_id,self::$StateSendback,array(self::$StateWait),$this->ComId,$curr_workitem_id, true);
        //更新退回节点的退回状态
        g("mv_workinst")->update_cs_by_i($curr_formsetinit_id,$back_workitem_id,$this->ComId);

        $fields = "mpw.*,mpwn.is_start";
        $callback_workitem = g("mv_workinst")->get_by_itemid($curr_formsetinit_id,$back_workitem_id,$this->ComId, true, $fields);
        if(!is_array($callback_workitem)){throw new \Exception('查询工作项失败');}
        $callback_handle_id = $callback_workitem["handler_id"];
        $callback_create_id = $callback_workitem["creater_id"];
        $callback_workitem_name = $callback_workitem["workitem_name"];
        $callback_handle_name = $callback_workitem["handler_name"];
        $callback_creater_name = $callback_workitem["creater_name"];
        $callback_work_node_id = $callback_workitem["work_node_id"];
        $callback_pre_work_node_id = $callback_workitem["pre_work_node_id"];
        $is_start = $callback_workitem['is_start'] == self::$StartNode ? true : false;
        $is_robbot = 0;

        $receiver_id = $this->UserId;

        $handle_m_dept = g("dao_user")->get_main_dept($callback_handle_id,FALSE);
        if($handle_m_dept){
            $handle_dept_id = $handle_m_dept["id"];
            $handle_dept_name = $handle_m_dept["name"];
        }
        else{
            $handle_dept_id = -1;
            $handle_dept_name = "无部门";
        }
        $create_m_dept = g("dao_user")->get_main_dept($callback_create_id,FALSE);
        if($create_m_dept){
            $create_dept_id = $create_m_dept["id"];
            $create_dept_name = $create_m_dept["name"];
        }
        else{
            $create_dept_id = -1;
            $create_dept_name = "无部门";
        }
        $receiver_m_dept = g("dao_user")->get_main_dept($receiver_id,FALSE);
        if($receiver_m_dept){
            $receiver_dept_id = $receiver_m_dept["id"];
            $receiver_dept_name = $receiver_m_dept["name"];
        }
        else{
            $receiver_dept_id = -1;
            $receiver_dept_name = "无部门";
        }

        $com_id = isset($curr_workitem['com_id'])?$curr_workitem['com_id']:$this->ComId;
        //保存工作项信息
        $work_item = array(
            'formsetinit_id' 	=> $curr_formsetinit_id,
            'workitem_name' 	=> $callback_workitem_name,
            'com_id' 			=> $com_id,
            'handler_id' 		=> $callback_handle_id,
            'handler_name' 		=> $callback_handle_name,
            'handler_dept_id' 	=> $handle_dept_id,
            'handler_dept_name' => $handle_dept_name,
            'state' 			=> self::$StateNew,
            'creater_id' 		=> $callback_create_id,
            'creater_name' 		=> $callback_creater_name,
            'creater_dept_id' 	=> $create_dept_id,
            'creater_dept_name' => $create_dept_name,
            'receiver_id' 		=> $this->UserId,
            'receiver_name' 	=> $this->UserName,
            'receiver_dept_id' 	=> $receiver_dept_id,
            'receiver_dept_name' => $receiver_dept_name,
            'work_node_id' 		=> $callback_work_node_id,
            'pre_work_node_id'	=> $callback_pre_work_node_id,
            'is_returnback' 	=> self::$Callback,
            'returnback_node_id' => $curr_workitem_id,
            'is_robbot'          => $is_robbot,
        );
        $new_workitem_id = g("mv_workinst")->save_workitem($work_item);

        //更新当前事项的状态
        $curr_workitem["complete_time"] = time();
        if($curr_receive_time!=0){
            $curr_workitem["consuming_time"] = time()-$curr_receive_time;
        }
        $curr_workitem["judgement"] = $judgement;
        $curr_workitem["judgement_desc"] = $judgement_desc;
        $curr_workitem["target_node"] = "退回".$callback_workitem_name;
        g("mv_workinst")->callback_workitem($curr_workitem_id,$this->ComId,$curr_workitem);

        //更新流程流转
        $formsetinst_fields = " mpf.*,mpfm.form_name ";
        $formsetinst = $this->get_formsetinst_by_id($curr_formsetinit_id,$formsetinst_fields);

        $formsetinst["workitem_rule"].="@".$callback_work_node_id;
        g("mv_formsetinst")->update_formsetinst_workitemrule($curr_formsetinit_id,$this->ComId,$formsetinst["workitem_rule"],$callback_work_node_id);

        //消息提醒
        $user_id = $callback_handle_id;
        $user = g('dao_user') -> get_by_id($user_id);

        $wx_title = $this -> AppCName."退回提醒";
        $desc = $this -> AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》退回事项，请及时处理。";
        $new_desc = "收到《".$formsetinst["formsetinst_name"]."》的退回信息，请及时查看";
        $new_title = "【" .$this -> UserName . "】退回了《".$formsetinst["form_name"]."》申请表";
        // $new_title = $formsetinst["form_name"];
        $msg_title = "收到了【" .$this -> UserName . '】的退回提醒';

        $wx_url = $this -> WxDomain.'?app='.$this->AppName.'&a=detail&listType=1&activekey=0&id='.$new_workitem_id;
        $pc_url = $this -> PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$new_workitem_id;

        try {
            $this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 1, $this -> user_id, array($user_id), array(), $formsetinst['app_version']);
        } catch (\Exception $e) {
            parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
        }

    }


    /**
	 * 保存流程new
	 * @param array $data
	 */
	public function save_workitem_parent($data){
        $finish_time_remark = isset($data['finish_time_remark'])?$data['finish_time_remark']:'';

		//如果为空，是新发起流程，需要增加forsetInst数据
		if(empty($data["formsetInst_id"])){
			$create_id = $this->UserId;
			$create_name = $this -> UserName;

			$current_create_m_dept = g("dao_user") -> get_main_dept($this->UserId,FALSE);
			if($current_create_m_dept){
				$current_creater_dept_id = $current_create_m_dept["id"];
				$current_creater_dept_name = $current_create_m_dept["name"];
			}
			else{
				$current_creater_dept_id = -1;
				$current_creater_dept_name = "无部门";
			}

			// gch add trigger form
			isset($data['creater_id']) && $create_id = $data['creater_id'];
			isset($data['creater_name']) && $create_name = $data['creater_name'];

			$is_other_proc = empty($data["is_other_proc"]) ? 0 : $data["is_other_proc"];
			$app_version = isset($data['app_version']) ? intval($data['app_version']) : 0;

            $formsetinst_name = !empty($data['new_formsetinst_name'])? $data['new_formsetinst_name']: $data["form_name"];

			$create_m_dept = g("dao_user") -> get_main_dept($create_id,FALSE);
			if($create_m_dept){
				$creater_dept_id = $create_m_dept["id"];
				$creater_dept_name = $create_m_dept["name"];
			}
			else{
				$creater_dept_id = -1;
				$creater_dept_name = "无部门";
			}

			$formsetinst_id = g("mv_formsetinst")->save_formsetinst($data["form_id"],$data["work_id"],$formsetinst_name,$create_id,$create_name,$this->ComId,$data["form_vals"],$is_other_proc,$creater_dept_id,$creater_dept_name, $app_version);
			
			//获取开始步骤的节点id
			$start_work_node =$this -> get_start_node($data["work_id"]);
				
			//保存工作项信息
			$work_item = array(
				'formsetinit_id' 	=> $formsetinst_id,
				'workitem_name' 	=> "开始",
				'com_id' 			=> $this->ComId,
				'handler_id' 		=> $this->UserId,
				'handler_name' 		=> $this -> UserName,
				'handler_dept_id' 	=> $current_creater_dept_id,
				'handler_dept_name' => $current_creater_dept_name,
				'state' 			=> self::$StateNew,
				'creater_id' 		=> $create_id,
				'creater_name' 		=> $create_name,
				'creater_dept_id' 	=> $creater_dept_id,
				'creater_dept_name' => $creater_dept_name,
				'work_node_id' 		=> $start_work_node["id"],
				'is_returnback' 	=> self::$Not_Callback,
				'returnback_node_id' => 0,
                'judgement'          => $finish_time_remark,
			);
			$workitem_id = g("mv_workinst") -> save_workitem($work_item);
			//保存表单控件
			$this -> save_form_vals($this -> ComId, $data["form_vals"], $formsetinst_id);
			//更新流程规则
			g("mv_formsetinst") -> update_formsetinst_workitemrule($formsetinst_id,$this->ComId,$start_work_node["id"],$start_work_node["id"]);
			g("mv_file") -> add_file($data["files"],$this->ComId,$create_id,$create_name,$formsetinst_id,$start_work_node["id"],$is_other_proc);
			
			//通用流程添加常用记录
			if($this->IsProcType == self::$IsProc){
				$form_model = g('mv_form') -> get_by_id($data["form_id"],$this->ComId);
				$is_exist = g("mv_form_user")->is_exist_user_form($form_model["form_history_id"],$create_id,$this->ComId);
				if(!$is_exist){
					g("mv_form_user")->add_common_form($form_model["id"],$form_model["form_history_id"],$create_id,$this->ComId);
				}
				else{
					g("mv_form_user")->update_common_form_time($form_model["form_history_id"],$create_id,$this->ComId);
				}
			}
		} else {
			//有formsetinst，则为正在跑的流程
			if(!empty($data["workitem_id"])){
				$formsetinst_id = $data["formsetInst_id"];

                // 更新表单实例名
                $formsetinst_name = isset($data["new_formsetinst_name"])?$data["new_formsetinst_name"]:'';
                //判断是否已存在 前端自定义的实例名数据，存在则可以更新 ，否则不给更新
                $dao_val = new Mv_Formsetinst_Name_Val();
                $can_update = $dao_val->get($this -> ComId,$formsetinst_id,self::CUSTOM_INSTANCE);
                if ( $formsetinst_name && $can_update ) {
                    g("mv_formsetinst")->update_formsetinst_name($formsetinst_id,$formsetinst_name);
                }

				$formsetinst = $this -> get_formsetinst_by_id($formsetinst_id);
				$create_id = $formsetinst['creater_id'];
				//更新表单实例
				// if ($formsetinst['is_other_proc'] == 0) {
					$this -> save_form_vals($this -> ComId, $data["form_vals"], $data["formsetInst_id"], TRUE);
				// } else {
				// 	g("mv_formsetinst")->update_formsetinst($data["formsetInst_id"],$this->ComId,$data["form_vals"]);
				// }
				//更新工作项
				$workitem_id = $data["workitem_id"];
				$cur_workitem = $this->get_curr_workitem($workitem_id);

                $work_node_id = $cur_workitem["work_node_id"];

                //获取开始节点标识
                $start_node = g('mv_work_node')->getStartNode($formsetinst['work_id'],'id');
                $start_node_id = isset($start_node['id'])?$start_node['id']:'';
                if($work_node_id == $start_node_id){
                    $data["judgement"] = $finish_time_remark;
                }

				if($cur_workitem["state"]!=self::$StateSend&&$cur_workitem["state"]!=self::$StateCallback&&$cur_workitem["state"]!=self::$StateDone){//不是完成状态才更新这个字段
					g("mv_workinst")->update_workitem($data["workitem_id"],$this->ComId,$data["judgement"]);
				}
			} else {
				throw new \Exception('数据丢失，请联系管理员');//这种情况不应该存在
			}
			g("mv_file")->del_file_by_node_id($this->ComId,$this->UserId,$work_node_id,$data["formsetInst_id"]);
			if(!empty($data["files"])){
				g("mv_file")->add_file($data["files"],$this->ComId,$this->UserId,$this->UserName,$data["formsetInst_id"],$work_node_id,$is_other_proc);
			}
		}

        if($formsetinst_id){
            $finish_time = isset($data['finish_time'])?$data['finish_time']:0;
            g('mv_formsetinst')->update_formsetinst_ftime($formsetinst_id,$finish_time,$finish_time_remark);
        }

		$ret = array();
		$ret["workitem_id"] = $workitem_id;
		$ret["formsetInst_id"] = $formsetinst_id;
		$ret["user_id"] = $create_id;
		
		return $ret;
	}
	
	/**
	 * 发送流程new
	 * @param array $data
     * @throws
	 */
	public function send_workitem_parent($data){
		$workitem_id = $data["workitem_id"];
		$work_nodes = $data["work_nodes"];
		
		if(empty($work_nodes)||count($work_nodes)==0){throw new \Exception('非法请求');}
		
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($workitem_id);
		if($curr_workitem["state"]!=self::$StateNew&&$curr_workitem["state"]!=self::$StateRead&&$curr_workitem["state"]!=self::$StateSave){
			throw new \Exception('该工作项已经处理，请返回代办列表');
		}
		if (!in_array($curr_workitem['formsetinst_state'],[self::$StateFormDraft,self::$StateFormDone])) {
            throw new \Exception('该实例已变更，请返回代办列表');
        }

		$work_node_id = $curr_workitem["work_node_id"];
		$formsetinst_id = $curr_workitem["formsetinit_id"];
		
		//流转限制条件（全部审批到达 2 一个审批到达 1）
		$circulation_limit_rule = $curr_workitem["circulation_limit_rule"];
		$same_level_workitems =  g("mv_workinst")->get_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId);
		if(is_array($same_level_workitems)){//如果存在同级工作项
			if($circulation_limit_rule ==2){//一个审批到达 2
				//把未完成的事项更新成已通过
				g("mv_workinst")->update_node_state_item_nwi($formsetinst_id,$curr_workitem["id"],$work_node_id,self::$StateDone,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$this->ComId);
				$return_arr = $this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,TRUE);
				$this->notify_process($curr_workitem,$formsetinst_id,$work_nodes);
			}
			else{//全部审批到达
				$return_arr =$this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,FALSE);
			}
		}
		else{
			$return_arr =$this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,TRUE);
			$this->notify_process($curr_workitem,$formsetinst_id,$work_nodes);
		}
		
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["target_node"] = "送".$return_arr[1];
		g("mv_workinst")->send_workitem($workitem_id,$this->ComId,$curr_workitem);
		
		$ret = array();
		$ret["return_arr"] = $return_arr;

		if ($ret['return_arr'][0] == 'TRUE'){
            // 如果是撤销流程，则更新撤销原流程
            self::revokeRelateFormsetinst($workitem_id);
        }

		return $ret;
	}
	
	
	/**
	 * 判断工作项的状态是否更变
	 * @param int $workitem_id
	 */
	
	public function is_finish($workitem_id){
		$is_finish = false;
		
		$cur_workitem = $this->get_curr_workitem($workitem_id);
		
		// if( $cur_workitem["state"]==self::$StateSend||$cur_workitem["state"]==self::$StateCallback||$cur_workitem["state"]==self::$StateDone||$cur_workitem["state"]==self::$StateSendback ){
		// 	$is_finish=true;
		// }
        // 工作项状态判断
        if (!in_array($cur_workitem['state'],Mv_Workinst::$StateWorkingWorkitem)) {
            $is_finish=true;
        }
        // 实例状态判断
        if (!in_array($cur_workitem['formsetinst_state'],[self::$StateFormDraft,self::$StateFormDone])) {
            $is_finish=true;
        }
		return $is_finish;
	}
	
	/**
	 * 根据格式的数字字符串获取实例名new
	 * @param int $num_format
	 * @param  integer $app_version 应用版本，0：版本v1，1：版本v2
	 */

	public function get_formset_by_num($form,&$is_special, $app_version=0){
		if($app_version==1){
			$formset_format = trim($form["formsetinst_name"]);
		}
		else{
			$formset_format = g('mv_formsetinst_name')->get_recode_by_id($this->ComId,$this->IsProcType);
			if(!empty($formset_format)){
				$formset_format = $formset_format["format"];
			}
		}

		$num_format = "6;2;5";
		if(!empty($formset_format)){
			$num_format = $formset_format;
		}
		if(strstr($num_format,self::$Custom."")){
			$is_special = 1;
		}
		
		$formset_name = array();
		$num_format = explode(";",$num_format);
		foreach ($num_format as $n_format){
			$temp_str = "";
			if($n_format==self::$ApplyerDept){
				$create_m_dept = g("dao_user")->get_main_dept($this->UserId,FALSE);
				if(empty($create_m_dept)){
					$depts = g("dao_user")->list_all_dept($this->UserId,FALSE,FALSE);
					$formset_name[$n_format] = $depts[0]["name"];
				}
				else{
					$formset_name[$n_format] = $create_m_dept["name"];
				}
			}
			else if($n_format == self::$ApplyerName){
				$temp_str = $this->UserName;
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateM){
				$temp_str = date("Ym",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateD){
				$temp_str = date("Ymd",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateMin){
				$temp_str = date("YmdHi",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$FormName){
				$temp_str = $form['form_name'];
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$Custom){
				$temp_str = " ";
				$formset_name[$n_format] = $temp_str;
			}else{
                $temp_str = "";
                $formset_name[$n_format] = $temp_str;
            }
		}
		return $formset_name;
	}
	
	
	/**
	 * 根据实例id获取事项信息new
	 * @param int $formsetinit_id
	 */
	public function get_workitem_list_parent($formsetinit_id, $fields='mpw.*,su.pic_url,mpwn.work_type'){
		$res_list = array();
		$list = g("mv_workinst")->get_workitem_list($formsetinit_id,$this->ComId, 0, 0, $fields);
		if(!empty($list)){
			$t_node =  $list[0];
			$temp_state_list = array();
			$temp_state_list[] = $t_node["state"];

			$temp_item_list = array();
			$temp_item_list[] = $t_node;
			$temp_item_name = $t_node["workitem_name"];
			for ($i=1;$i<count($list);$i++){
				$c_node = $list[$i];
				// 下一个节点与当前节点是同一个节点时合并
				if($c_node["work_node_id"]==$t_node["work_node_id"]){
					if( in_array($t_node['state'],[self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait,self::$StateStop]) || $c_node["handler_id"]!=$t_node["handler_id"]){
						$temp_item_list[] = $c_node;
						$temp_state_list[] = $c_node["state"];
					}
				}
				else{
					$temp_state = $this->getWorkitemState($temp_state_list);
					$res_list[] = [
					    "workitem_name"=>$temp_item_name,
                        'state'=>$temp_state,
                        'workitem_list'=>$temp_item_list,
                    ];

					$t_node = $c_node;
					$temp_state_list = array();
					$temp_state_list[] = $c_node["state"];
					$temp_item_list = array();
					$temp_item_list[] = $c_node;
					$temp_item_name = $c_node["workitem_name"];
				}
			}
			$temp_state = $this->getWorkitemState($temp_state_list);
            $res_list[] = [
                "workitem_name"=>$temp_item_name,
                'state'=>$temp_state,
                'workitem_list'=>$temp_item_list,
            ];
		}
		return $res_list;
	}
	// 获取流程节点图标状态
	private function getWorkitemState($temp_state_list)
    {
        $state_f = 0; // 正常
        $state_b = 1; // 退回
        $state_d = 2; // 处理
        $state_w = 3; // 等待
        $state_s = 4; // 驳回
        $state_r = 5; // 撤销

        if(in_array(self::$StateCallback, $temp_state_list)){
            return $state_b;
        }

        if(in_array(self::$StateNew, $temp_state_list)||in_array(self::$StateRead, $temp_state_list)||in_array(self::$StateSave, $temp_state_list)){
            return $state_d;
        }

        if(in_array(self::$StateWait, $temp_state_list)){
            return $state_w;
        }

        if(in_array(self::$StateStop, $temp_state_list)){
            return $state_s;
        }

        if(in_array(self::$StateRevoke, $temp_state_list) || in_array(self::$StateRevoking, $temp_state_list) || in_array(self::$StateFinishRevoke, $temp_state_list)){
            return $state_r;
        }

        return $state_f;
    }
	
	public function get_linksign_state(){
		$linksign = g('mv_linksign')->select_linksign($this->ComId,$this->IsProcType);
		return $linksign;
	}



	/**
	 * 拆分并保存表单实例控件信息
	 * @param  [type] $form_vals 	表单控件
	 * @param  [type] $formsetinst_id 	实例id
	 * @param  [type] $update  是否是更新
	 * @return [type]         
	 */
	public function save_form_vals($com_id, $form_vals, $formsetinst_id, $update=FALSE) {
		if (!is_array($form_vals) || empty($form_vals)) {
			throw new \Exception('表单控件错误');
		}

		$formsetinst = g('mv_formsetinst') -> get_formsetinst_by_id($formsetinst_id, $this -> com_id, $this -> user_id, 'mpf.creater_id,mpf.state,mpfm.id form_id,mpfm.form_history_id');
		if (!$formsetinst) throw new \Exception('表单不存在');
		$creater_id = $formsetinst['creater_id'];
		$form_id = $formsetinst['form_id'];
		$history_id = $formsetinst['form_history_id'];
		$state = $formsetinst['state'];

		parent::log_i('开始保存控件');
		foreach ($form_vals as $key1 => $input) {
			$type = $input['type'];
			$tmp_data = array();
			$other = array();
			$rec = array();
			foreach ($input as $key2 => $val) {
				if ($key2 == 'th_key') continue;
				if (in_array($key2, self::$input_val)) {
					$tmp_data[$key2] = $val;
				} elseif ($type == 'table' && $key2 == 'rec') {
					$rec = $val;
				} else {
					$other[$key2] = $val;
				}

			}unset($val);
			$type == 'table' && $tmp_data['val'] = '';
			// 人员和部门的值保存id
			if ($type == 'people' || $type == 'dept' || $type == 'school' || $type == 'schoolDept') {
				$other['val'] = isset($input['val']) ? $input['val'] : '';
				$ids = isset($input['ids']) ? $input['ids'] : '';
				$ids = !empty($ids) ?  explode(',', $input['ids']) : array();
				$tmp_data['val'] = json_encode($ids);
			}

            //单选框、下拉框保存val的id为特殊值
            if ($type == 'select' || $type == 'radio') {
                if (isset($input['val_id']) && (!empty($input['val_id']) || $input['val_id'] === 0 || $input['val_id'] === '0')) {
                    $tmp_data['special_val'] = $input['val_id'];
                } else {
                    $opts = explode(',', $input['opts']);
                    $opts_nums = explode(',', $input['opts_nums']);
                    $opts_key = array_search($tmp_data['val'], $opts);

                    $tmp_data['special_val'] = $opts_key !== false ? $opts_nums[$opts_key] : '';
                }

            }
            // 车辆控件
            if ($type == 'car') {
                $tmp_val = [];
                foreach ($input['val_list'] as $item) {
                    $seat_str = $item['seat']?"-{$item['seat']}座":"";
                    $driver_str = $item['driver']['name']?" 司机：{$item['driver']['name']}":"";
                    $tmp_val[] = "{$item['name']}{$seat_str}{$driver_str}";
                }
                $tmp_data['val'] = implode("\n",$tmp_val);
            }

			$tmp_data['other'] = $other;

			if (!$update) {
				$input_id = g('mv_formsetinst') -> save_form_vals($tmp_data, $creater_id, $form_id, $history_id, $formsetinst_id, $state);
			} else {
				g('mv_formsetinst') -> update_form_vals($tmp_data, $formsetinst_id, $state, $history_id);
			}

			// 子表单
			if ($type == 'table') {
				if ($update) {
					$cond = array(
						'input_key=' => $tmp_data['input_key'],
						'p_label_id=' => 0
						);
					//查询子表单下的控件
					$c_form_inputs = g('mv_formsetinst') -> get_form_vals($formsetinst_id, $cond, 'id');
					if (empty($c_form_inputs)) throw new \Exception('子表单不存在');
					
					$input_id = $c_form_inputs[0]['id'];
					g('mv_formsetinst') -> del_form_vals($formsetinst_id, $input_id);
				}

				if (is_array($rec) && !empty($rec)) {
					foreach ($rec as $key3 => $rec_inputs) {
						foreach ($rec_inputs as $key4 => $r) {
							$c_type = $r['type'];
							$r_data = array();
							$r_other = array();
							foreach ($r as $key5 => $val) {
								if ($key5 == 'input_key') continue;
								if (in_array($key5, self::$c_input_val)) {
									$r_data[$key5] = $val;
								} else {
									$r_other[$key5] = $val;
								}

							}unset($val);
							$r_data['input_key'] = $r_data['th_key'];
							if ($c_type == 'people' || $c_type == 'dept') {
								$r_other['val'] = isset($r['val']) ? $r['val'] : '';
								$ids = isset($r['ids']) ? explode(',', $r['ids']) : array();
								$r_data['val'] = json_encode($ids);
							}
							$r_data['other'] = $r_other;

                            //单选框、下拉框保存val的id为特殊值
                            if ($c_type == 'select' || $c_type == 'radio') {
                                if (isset($r['val_id']) && (!empty($r['val_id']) || $r['val_id'] === 0 || $r['val_id'] === '0')) {
                                    $r_data['special_val'] = $r['val_id'];
                                } else {
                                    $opts = explode(',', $r['opts']);
                                    $opts_nums = explode(',', $r['opts_nums']);
                                    $opts_key = array_search($r_data['val'], $opts);

                                    $r_data['special_val'] = $opts_key !== false ? $opts_nums[$opts_key] : '';
                                }

                            }

							$c_input_id = g('mv_formsetinst') -> save_form_vals($r_data, $creater_id, $form_id, $history_id, $formsetinst_id, $state, $input_id, $key3);
						
						}unset($r);

					}unset($rec_inputs);

				}
			}

		}unset($input);
		parent::log_i('成功保存控件');
	}


	/**
	 * 表单实例控件信息
	 * @param  [type] $formsetinst_id [description]
	 * @return [type]                 [description]
	 */
	public function get_form_vals($formsetinst_id) {
		$input_list = g('mv_formsetinst') -> get_form_vals($formsetinst_id, array());
		if (empty($input_list)) throw new \Exception('获取表单控件失败');

		$form_vals = array();
		foreach ($input_list as $key1 => $input) {
			$tmp_data = array();
			if ($input['p_label_id'] == 0) {
				$tmp_data = array(
					'type' 		=> $input['type'],
					'must' 		=> $input['must'],
					'describe' 	=> $input['describe'],
					'input_key' => $input['input_key'],
					'name' 		=> $input['name'],
					);
				$other = json_decode($input['other'], TRUE);
				$tmp_data = array_merge($tmp_data, $other);

				if ($input['type'] != 'people' && $input['type'] != 'dept' && $input['type'] != 'table' && $input['type'] != 'school' &&  $input['type'] != 'schoolDept') {
					$tmp_data['val'] = $input['val'];
				}

                if( in_array($input['type'],['file','dangerous']) && is_string($input['val']) ){
                    $tmp_data['val'] = is_array(json_decode($input['val'],true))?json_decode($input['val'],true):$input['val'];
                }

				if ($input['type'] == 'table') {
					// 子表单
					$p_id = $input['id'];
					$rec = array();
					foreach ($input_list as $key2 => $c_input) {
						$c_data = array();
						if ($c_input['p_label_id'] == $p_id) {
							$c_data = array(
								'type' 		=> $c_input['type'],
								'must' 		=> $c_input['must'],
								'describe' 	=> $c_input['describe'],
								'th_key' 	=> $c_input['input_key'],
								'name' 		=> $c_input['name'],
								);
							$c_other = json_decode($c_input['other'], TRUE);
							$c_data = array_merge($c_data, $c_other);

							if ($c_input['type'] != 'people' && $c_input['type'] != 'dept' && $c_input['type'] != 'school' && $c_input['type'] != 'schoolDept') {
								$c_data['val'] = $c_input['val'];
							}

                            if( in_array($c_input['type'],['file','dangerous']) && is_string($c_input['val']) ){
                                $c_data['val'] = is_array(json_decode($c_input['val'],true))?json_decode($c_input['val'],true):$c_input['val'];
                            }

							$rec[(int)$c_input['label_id']][$c_input['input_key']] = $c_data;
							unset($input_list[$key2]);
						}
					}

					!empty($rec) && $tmp_data['rec'] = $rec;
				}

				$form_vals[$input['input_key']] = $tmp_data;
				unset($input_list[$key1]);
			}

		}

		return $form_vals;
	}
	
	//--------------------------------------内部实现--------------------------------
	
	
	/**
	 * 获取接收人规则 new
	 * @param array $next_node_ids节点id数组
	 * @param array $next_node_id_arr 结果集
	 * @param array $workitem 工作项对象
	 * @param array $formsetinst 实例对象
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	private function get_receiver_rule($next_node_ids,&$next_node_id_arr,$workitem,$formsetinst,$com_id){
		if(count($next_node_ids)==0){
			throw new \Exception('找不到下一步的流程节点');
		}
		else{
            //节点排序，通过数据库查询 idx进行排序升序
            $tmp_next_node_ids = g("mv_work_node")->get_work_node_by_ids($next_node_ids,'id');
            $next_node_ids = array_column($tmp_next_node_ids,'id');
			foreach ($next_node_ids as $next_node_id){
				$next_work = g("mv_work_node")->get_by_id($com_id,$next_node_id);
				if(is_array($next_work)){
					$receive_array = array();
					$receive_rules = $next_work["receive_rule"];//下一步接收人规则
					
					//结束节点标示
					$is_end = FALSE;
					if(empty($next_work["next_node_id"])){//结束节点
						$is_end = TRUE;
					}
					
					if(empty($receive_rules)){//如果接收人规则为空，则显示选人按钮，使用地址本选择人
						//这里可以添加选择地址本的参数，用地址本的参数控制人员选择范围
					}
					else{
						$receive_rule_arr = explode(",",$receive_rules);
						
						foreach ($receive_rule_arr as $receive_rule){
							
							if($receive_rule==self::$RecriverFixed){
								$receive_user = $next_work["receive_user"];
								$user_ids = array();
								if(!empty($receive_user)){
									$user_ids = explode(",",$receive_user);
								}
								$receive_group = $next_work["receive_group"];
								if(!empty($receive_group)){
									$group_ids = explode(",",$receive_group);
									$group_user_ids = g("dao_group")->get_user_by_ids($com_id,$group_ids);
									$tmp_user_ids = array_column($group_user_ids, 'id');
									$user_ids = array_merge($user_ids, $tmp_user_ids);
								}
								
								$users = g("dao_user")->list_by_ids($user_ids,'*',FALSE);
								if(is_array($users)){//如果用户不存在，则使用地址本
									foreach ($users as $user){
										$this->set_receiver($receive_array, $user);
									}
									unset($user);
								}
							}
							if($receive_rule==self::$RecriverCreater){
								$user_id = $workitem["creater_id"];
								$this->get_user_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverCreaterLeader){
								$user_id = $workitem["creater_id"];
								$this->get_user_leader_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverHandler){
								$user_id = $workitem["handler_id"];
								$this->get_user_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverHandlerLeader){
								$user_id = $workitem["handler_id"];
								$this->get_user_leader_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverInput){
								/**
								* 接收人控件
								* Modify by gch
								*/
								// ----start
								
								// 自动接收人userid
								// input_key
								$receive_input_arr = explode(",", $next_work['receive_input']);

								if (!empty($receive_input_arr) && !empty($formsetinst['form_vals'])) {
									$receive_input_userids = $this->getInputUserIds($receive_input_arr,$formsetinst['form_vals']);

									$receive_input_userids = array_filter($receive_input_userids);
									// 获取人员信息
									$this->get_users_receiver_list($receive_array, $receive_input_userids);
								}

								// ----end
							}
						}
						unset($receive_rule);
					}
					
					$select_array = array();
					$select_rules = $next_work["select_rule"];//下一步选择人员规则
					$select_group = $next_work["select_group"];//下一步选择组别规则
                    $select_input = $next_work["select_input"];//下一步选择人员控件
					
					$select_users = array();
					if(!empty($select_rules)){
						$select_users = explode(",",$select_rules);
					}
					
					if(!empty($select_group)){
						$select_group = explode(",",$select_group);
						$group_user_ids = g("dao_group")->get_user_by_ids($com_id,$select_group);
						$tmp_user_ids = array_column($group_user_ids, 'id');
						$select_users = array_merge($select_users, $tmp_user_ids);
					}

                    if(!empty($select_input)){
                        $select_inputs = explode(',',$select_input);
                        if (!empty($select_inputs) && !empty($formsetinst['form_vals'])) {
                            $select_input_userids = $this->getInputUserIds($select_inputs,$formsetinst['form_vals']);
                            $select_users = array_merge($select_users,$select_input_userids);
                        }
                    }
					
					$select_users = g("dao_user")->list_by_ids($select_users,'*',FALSE);
					
					$temp_users = array();
					//将固定接收人和选择人员重复者去掉
					foreach ($select_users as $user){
						$is_receiver = false;
						foreach ($receive_array as $receiver){
							if($user["id"]==$receiver["id"]){
								$is_receiver = true;
								break;
							}
						}
						if(!$is_receiver){
							$temp_users[]=$user;
						}
					}
					unset($user);
					$select_array = $temp_users;
					
					
					$next_work["is_end"] = $is_end;
					$next_work["select_array"] = $select_array;
					$next_work["receive_array"] = $receive_array;
					array_push($next_node_id_arr, $next_work);
				}
			}
		}
	}

    /**
     * 获取控件人员信息(接收人，选择人)
     * @param $receive_input_arr
     * @param $form_val_arr
     * @return array
     */
    private function getInputUserIds($receive_input_arr,$form_val_arr){
        $receive_input_userids = array();
        foreach ($receive_input_arr as $v) {
            $tmp_arr = explode('@',$v);
            $v = $tmp_arr[0];
            $detail_id = isset($tmp_arr[1])?$tmp_arr[1]:0;

            if (isset($form_val_arr[$v]) && isset($form_val_arr[$v]['ids'])) {
                $receive_input_userids = array_merge($receive_input_userids, explode(',', $form_val_arr[$v]['ids']));
            }
            // 接收控件
            if (!isset($form_val_arr[$v])) {
                continue;
            }
            $input = $form_val_arr[$v];
            $input_type = $input['type'];
            switch ($input_type){
                case 'people':
                case 'schoolDept':
                    if (isset($input['ids'])) {
                        $receive_input_userids = array_merge($receive_input_userids, explode(',', $input['ids']));
                    }
                    break;
                case 'school':
                    if (isset($input['ids'])) {
                        $school_ids = explode(',', $input['ids']);
                        if($school_ids){
                            $dao_school_contact = new ProcessSchoolContact();
                            foreach ($school_ids as $school_id){
                                $tmp_users = $dao_school_contact->listUsersBySIdAndRids($school_id,[$detail_id]);
                                $receive_input_userids = array_merge($receive_input_userids, array_column($tmp_users,'id'));
                            }
                        }
                    }
                    break;
                case 'car':
                    foreach ($input['val_list'] as $item){
                        $receive_input_userids[] = $item['driver']['id'];
                    }
                    break;
            }
        }

        return $receive_input_userids;
    }
	
	/**
	 * 流转规则规则判断，返回TRUE FALSE
	 * @param array $rule_arr
	 * @param array $formsetinst_val
	 * @param int $work_id
	 * @param int $work_result_id
	 * @param int $workitem
	 * @param int $next_node_ids
	 */
	private function is_circulation_rule(&$rule_arr,$formsetinst_val,$work_id,&$work_result_id,$workitem,&$next_node_ids){
		if(count($rule_arr)==0){
			throw new \Exception('流转规则保存有误，请联系管理员！流程节点ID为:'.$work_id);
		}
		$is_true=FALSE;
		
		foreach ($rule_arr as $key=>$rule){
			$rule = explode("#", $rule);
			$input_key = $rule[0];
			$symbol = $rule[1];
			$compare_val = $rule[2];//对于人员控件和部门控件新的应该有ids这个参数
			$compare_ids = "";
			if(stripos($compare_val,"&")){
				$compare_val = explode("&", $compare_val);
				$compare_ids = $compare_val[1];
				$compare_val = $compare_val[0];
			}
			$input_val = "";
			$input_ids = "";
			$input_type = "";
			if($symbol!=self::$People_Position){
				foreach ($formsetinst_val as $form_val){//获取表单某个控件的值
					if($form_val["input_key"]==$input_key){
						if(!isset($form_val["val"])){
							$form_val["val"]="";
						}
						$input_val = $form_val["val"];
						$input_type = $form_val["type"];
						if(isset($form_val["ids"])){$input_ids=$form_val["ids"];}

                        switch ($form_val['type']){
                            case 'school':
                                $input_val = $input_ids;
                                break;
                            case 'dangerous':
                                $danger_list = isset($form_val['danger_list'])?$form_val['danger_list']:[];
                                $danger_vals = array_column($danger_list,'val');
                                if(in_array(2,$danger_vals)){
                                    $input_val = '有隐患';
                                }elseif(in_array(1,$danger_vals)){
                                    $input_val = '无隐患';
                                }else{
                                    $input_val = '无隐患';
                                }
                                break;
                        }

						break;
					}
				}
			}

			if($symbol==self::$Compare_Equeal){
				if($input_type=="checkbox"){
					$input_val = str_replace(";",",",$input_val);
				}
				
				if($input_val==$compare_val||($input_val==""&&$compare_val=="null")){
					if(!empty($compare_ids)&&!empty($input_ids)){//人员部门控件比较
						if($compare_ids==$input_ids){
							$is_true = TRUE;
						}
						else{
							$is_true = FALSE;
							break;
						}
					}
					else{
						$is_true = TRUE;
					}
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Not_Equeal){
				if($input_type=="checkbox"){
					$input_val = str_replace(";",",",$input_val);
				}
				if(($input_val!=""&&$input_val!=$compare_val)||($input_val!=""&&$compare_val=="null")){
					if(!empty($compare_ids)&&!empty($input_ids)){//人员部门控件比较
						if($compare_ids!=$input_ids){
							$is_true = TRUE;
						}
						else{
							$is_true = FALSE;
							break;
						}
					}
					else{
						$is_true = TRUE;
					}
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Greater){
				if($input_val>$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Greater_Equeal){
				if($input_val>=$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Lesser){
				if($input_val<$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Lesser_Equeal){
				if($input_val<=$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$People_Position){//职位规则
				$user_id="";
				if($input_key==self::$People_Type_Creater){//发起人
					$user_id = $workitem["creater_id"];
				}
				else if($input_key==self::$People_Type_Done){//当前处理人
					$user_id =$workitem["handler_id"]; 
				}
				$user_id = intval($user_id);
				$user = g('dao_user') -> get_by_id($user_id);
				
				if($user["position"]==$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Contain){
				if($input_type=="dept"||$input_type=="people"){//人员部门控件是用逗号隔开的
					$tmp_input_val = explode(",", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					if(!empty($compare_ids)&&!empty($input_ids)){
						$tmp_compare_ids = explode(",", $compare_ids);
						$tmp_input_ids = explode(",", $input_ids);
						foreach ($tmp_input_ids as $tmp_id){
							if(in_array($tmp_id, $tmp_compare_ids)){
								$is_contain = TRUE;
								break;
							}
						}
						unset($tmp_id);
					}
					else{
						foreach ($tmp_input_val as $tmp_val){
							if(in_array($tmp_val, $tmp_compare_val)){
								$is_contain = TRUE;
								break;
							}
						}
						unset($tmp_val);
					}
					if($is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
				else{
					$tmp_input_val = explode(";", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					foreach ($tmp_input_val as $tmp_val){
						if(in_array($tmp_val, $tmp_compare_val)){
							$is_contain = TRUE;
							break;
						}
					}
					unset($tmp_val);
					if($is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
			}
			else if($symbol==self::$Compare_Not_Contain){
				if($input_type=="dept"||$input_type=="people"){//人员部门控件是用逗号隔开的
					$tmp_input_val = explode(",", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					if(!empty($compare_ids)&&!empty($input_ids)){
						$tmp_compare_ids = explode(",", $compare_ids);
						$tmp_input_ids = explode(",", $input_ids);
						foreach ($tmp_input_ids as $tmp_id){
							if(in_array($tmp_id, $tmp_compare_ids)){
								$is_contain = TRUE;
							}
						}
						unset($tmp_id);
					}
					else{
						foreach ($tmp_input_val as $tmp_val){
							if(in_array($tmp_val, $tmp_compare_val)){
								$is_contain = TRUE;
							}
						}
						unset($tmp_val);
					}
					if(!$is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
				else{
					$tmp_input_val = explode(";", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					foreach ($tmp_input_val as $tmp_val){
						if(in_array($tmp_val, $tmp_compare_val)){
							$is_contain = TRUE;
						}
					}
					unset($tmp_val);
					if(!$is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
			}
			
		}
		unset($rule);
		foreach( $next_node_ids as $key=>$value) {//删除存在表单流转规则的节点
	   		if($work_id == $value) unset($next_node_ids[$key]);
		}
		unset($value);
		if($is_true==TRUE){
			$work_result_id[] = $work_id;
		}
		
	}
	
	
	/**
	 * 新增下一步的工作项new
	 * @param array $work_nodes
	 * @param int $formsetinst_id
	 * @param array $curr_workitem 当前工作项对象
	 */
	private function save_new_workitem($work_nodes,$formsetinst_id,$curr_workitem,$is_last){
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
        $com_id = isset($formsetinst['com_id'])?$formsetinst['com_id']:$this->ComId;

		$workitem_rule = $formsetinst["workitem_rule"];
		//如果流转规则存在@,截取去掉@后面的字符串
		if(strripos($workitem_rule,"@")>-1){
			$workitem_rule_arr = explode("@",$workitem_rule);
			$workitem_rule = $workitem_rule_arr[0];
			$workitem_rule_last = $workitem_rule_arr[count($workitem_rule_arr)-1];
			$workitem_rule = substr($workitem_rule,0,strripos($workitem_rule, $workitem_rule_last)+strlen($workitem_rule_last));
		}
		
		//返回的步骤名 和接收人名称
		$return_arr = array();
		$msg_send_arr = array();
		$receiver_str = array();
		$curr_workitem_id = $curr_workitem["id"];
		$work_node = $work_nodes[0];
		$work_node_id = $work_node['id'];
		$receiver_arr = $work_node['receiver_arr'];
		$work_node = g("mv_work_node")->get_by_id($com_id,$work_node_id);
		if(!is_array($work_node)){
			throw new \Exception("流程发送失败，找不到该节点信息，节点id为".$work_node_id);
		}
		$work_node_name = $work_node["work_node_name"];

        //判断当前审批节点是否已有人审批，审批意见是否统一
        $curr_work_node_id = isset($curr_workitem['work_node_id'])?$curr_workitem['work_node_id']:'';
        if($curr_work_node_id){
            $next_ready_workitems = g('mv_workinst')->get_workitem_by_pre_node_id($formsetinst_id,$curr_work_node_id,$com_id,[self::$StateWait],'id,work_node_id');
            if($next_ready_workitems){
                $next_ready_work_nodes = array_unique(array_column($next_ready_workitems,'work_node_id'));

                if(count($next_ready_work_nodes)>1 || !in_array($work_node_id,$next_ready_work_nodes)){
                    throw new \Exception('该节点的所有审批处理人对下一个节点流向选择必须保持一致');
                }
            }
        }


		$is_finish = "FALSE";//
		if(!empty($work_node["next_node_id"])){//不是结束步骤
			if($is_last){
				//流转规则字段更新
				$workitem_rule.=",".$work_node_id;
				$formsetinst["curr_node_id"] = $work_node_id;
				//获取当前节点等待中的事项
				$waiting_item = g("mv_workinst")->get_node_state_item($formsetinst_id,$work_node["id"],array(self::$StateWait),$com_id);
				if(is_array($waiting_item)){
					//把本来等待中状态的事项更新成新收到
					g("mv_workinst")->update_state_workitem($com_id,$formsetinst_id,$work_node["id"],self::$StateWait,self::$StateNew);
					//消息提醒数组插入
					foreach ($waiting_item as $item){
						$msg_send = array();
						$msg_send["receiver_id"] = $item["handler_id"];
						$msg_send["workitem_id"] = $item["id"];
						$msg_send_arr[] =$msg_send;
					}
					unset($item);
				}
			}
			foreach ($receiver_arr as $receiver){
				$receiver_str[]=$receiver["receiver_name"];
				//判断下一节点是否存在同一个处理人
				$is_same_handler = g("mv_workinst")->is_same_handlers($formsetinst_id,$work_node["id"],$receiver["receiver_id"],$com_id);
				
				if(!$is_same_handler){//不存在，直接新建
					$pre_work_node_id = array();
					$pre_work_node_id[] = $curr_workitem["id"];
					
					$handle_id = $receiver["receiver_id"];
					$create_id = $curr_workitem["creater_id"];
					$receiver_id = $curr_workitem["handler_id"];
					$handle_m_dept = g("dao_user")->get_main_dept($handle_id,FALSE);
					if($handle_m_dept){
						$handle_dept_id = $handle_m_dept["id"];
						$handle_dept_name = $handle_m_dept["name"];
					}
					else{
						$handle_dept_id = -1;
						$handle_dept_name = "无部门";
					}
					$create_m_dept = g("dao_user")->get_main_dept($create_id,FALSE);
					if($create_m_dept){
						$create_dept_id = $create_m_dept["id"];
						$create_dept_name = $create_m_dept["name"];
					}
					else{
						$create_dept_id = -1;
						$create_dept_name = "无部门";
					}
					$receiver_m_dept = g("dao_user")->get_main_dept($receiver_id,FALSE);
					if($receiver_m_dept){
						$receiver_dept_id = $receiver_m_dept["id"];
						$receiver_dept_name = $receiver_m_dept["name"];
					}
					else{
						$receiver_dept_id = -1;
						$receiver_dept_name = "无部门";
					}
					
					$new_work_item = array(
						'formsetinit_id' 	=> $formsetinst_id,
						'workitem_name' 	=> $work_node["work_node_name"],
						'com_id' 			=> $com_id,
						'handler_id' 		=> $receiver["receiver_id"],
						'handler_name' 		=> $receiver["receiver_name"],
						'handler_dept_id' 	=> $handle_dept_id,
						'handler_dept_name' => $handle_dept_name,
						'receiver_id' 		=> $curr_workitem["handler_id"],
						'receiver_name' 	=> $curr_workitem["handler_name"],
						'receiver_dept_id' 	=> $receiver_dept_id,
						'receiver_dept_name' => $receiver_dept_name,
						'creater_id' 		=> $curr_workitem["creater_id"],
						'creater_name' 		=> $curr_workitem["creater_name"],
						'creater_dept_id' 	=> $create_dept_id,
						'creater_dept_name' => $create_dept_name,
						'work_node_id' 		=> $work_node["id"],
						'pre_work_node_id'	=> json_encode($pre_work_node_id),
						'is_returnback' 	=> self::$Not_Callback,
						'returnback_node_id' => 0,
					);
					if($is_last){
						$new_work_item["state"] = self::$StateNew;
					}
					else{
						$new_work_item["state"] = self::$StateWait;
					}
					$new_workitem_id = g("mv_workinst")->save_workitem($new_work_item);
					if($is_last){
						$msg_send = array();
						$msg_send["receiver_id"] = $receiver["receiver_id"];
						$msg_send["workitem_id"] = $new_workitem_id;
						$msg_send_arr[] =$msg_send;
					}
				}
				else{//存在，合并工作项
					$temp_workitem = $is_same_handler[0];
					$temp_pre_work_node_id = json_decode($temp_workitem["pre_work_node_id"],TRUE);
					if(!in_array($curr_workitem["id"],$temp_pre_work_node_id)){
						$temp_pre_work_node_id[] = $curr_workitem["id"];
						g("mv_workinst")->update_same_handlers($formsetinst_id,$work_node["id"],json_encode($temp_pre_work_node_id),$com_id);
					}
					
				}
			}
			unset($receiver);
			g("mv_formsetinst")->update_formsetinst_state($formsetinst_id,$com_id,self::$StateFormDone);
		}
		else{//结束节点
			//判断是否还有工作项没有结束
			$not_finish_items = g("mv_workinst")->get_formset_state_item_nwi($formsetinst_id,$curr_workitem_id,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$com_id);
			$count = empty($not_finish_items)?0:count($not_finish_items);
			
			if($count==0){//没有更新表单实例状态
				g("mv_formsetinst")->update_formsetinst_state($formsetinst_id,$com_id,self::$StateFormFinish);

                //推送物资预警信息
                $this->checkMaterialPrewarning($formsetinst);

                //实例申请完成触发
				g('ws_core') -> finsh_formsetinst($formsetinst['is_other_proc'], $formsetinst_id);
				$workitem_rule.=",".$work_node_id;
				$formsetinst["curr_node_id"] = $work_node_id;
				
				// 推送消息
				$user_id = $formsetinst["creater_id"];
				$user = g('dao_user') -> get_by_id($user_id);

				$wx_title = $this -> AppCName."通过提醒";
				$desc = $this -> AppCName."："."您发起的《".$formsetinst["formsetinst_name"]."》流程已审批通过。";
				$new_desc = "您的《".$formsetinst["formsetinst_name"]."》已审批通过，请及时查看";

				$new_title = "您的《".$formsetinst["form_name"]."》申请表已审批通过";
				// $new_title = $formsetinst["form_name"];
				$msg_title = "您的申请已审批通过";

				$wx_url = $this -> WxDomain.'?app='.$this -> AppName.'&a=detail&listType=2&activekey=0&id='.$formsetinst_id;
				$pc_url = $this -> PcDomain.'index.php?app='.$this -> AppName.'&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;

				try {
					$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 2, $formsetinst["creater_id"], array($user_id), array(), $formsetinst['app_version']);
				} catch (\Exception $e) {
					parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
				}

				//读取配置判断是否需要短信推送 审批通过 (正常流程的审批通过)
                $this->sendSmsNotify($this->com_id,$formsetinst['id'],\model\api\server\mv_proc\Mv_Sms_Model::TypeApproved);

				$is_finish = "TRUE";

				// gch add trigger form
				// 检查表单开启了触发表单
				parent::log_i('检查表单开启了触发表单');
				$triggerFormId = $this -> checkOpen($this -> ComId, $work_node_id);
				if (!empty($triggerFormId)) {
					parent::log_i('触发表单id:' . $triggerFormId);
					$triggerFormId = g('mv_form') -> getPublicFormById($this -> ComId, $triggerFormId);
					$triggerFormId = isset($triggerFormId['id']) ? $triggerFormId['id'] : 0;
					parent::log_i('触发发布版本表单id:' . $triggerFormId);
					
					parent::log_i('对比当前表单和触发表单是否控件一致,当前表单id:' . $curr_workitem["form_id"]);
					// 一致则触发，否则跳过
					$inputkeyMap = array();
					$consistencyFlag = $this -> checkInputConsistency($this -> ComId, $triggerFormId, $curr_workitem["form_id"], $inputkeyMap);
					if ($consistencyFlag) {
						// 控件一致并且触发开启->发起触发表单
						$flag = $this -> SendTriggerForm($this -> ComId, $curr_workitem["creater_id"], $triggerFormId, $formsetinst_id, $inputkeyMap);
						if (!$flag) {
							parent::log_i("发起触发表单【 表单实例id：" . $formsetinst_id . "】失败！");
						}

					}
				}
				
			}
			else{
				//获取所有等待中的事项
				$waiting_items = g("mv_workinst")->get_formset_state_workitem($formsetinst_id,array(self::$StateWait),$com_id);
				
				if(is_array($waiting_items)&&$count==count($waiting_items)){
					$waiting_item = $waiting_items[0];
					$work_node_name = $waiting_item["workitem_name"];
					$workitem_rule.=",".$waiting_item["work_node_id"];
					$formsetinst["curr_node_id"] = $waiting_item["work_node_id"];
					//获取某一个等待中节点的事项
					$new_waiting_item = g("mv_workinst")->get_node_state_item($formsetinst_id,$waiting_item["work_node_id"],array(self::$StateWait),$com_id);
					g("mv_workinst")->update_state_workitem($com_id,$formsetinst_id,$waiting_item["work_node_id"],self::$StateWait,self::$StateNew);
					foreach ($new_waiting_item as $item){
						$msg_send = array();
						$msg_send["receiver_id"] = $item["handler_id"];
						$msg_send["workitem_id"] = $item["id"];
						$msg_send_arr[] =$msg_send; 
						$receiver_str[] = $item["handler_name"];
					}
					unset($item);
				}
				else{
					$is_finish = "TRUE";
				}
			}
		}
		if(count($receiver_str)!=0){
			$receiver_str = implode(",",$receiver_str);
		}
		
		//更新流程规则
		g("mv_formsetinst")->update_formsetInst_workitemrule($formsetinst_id,$com_id,$workitem_rule,$formsetinst["curr_node_id"]);
		
		
		//消息提醒
		foreach ($msg_send_arr as $msg_send){
			$user_id = $msg_send["receiver_id"];
			$workitem_id = $msg_send["workitem_id"];
			$user = g('dao_user') -> get_by_id($user_id);

			$wx_title = $this -> AppCName."待办提醒";
			$desc = $this -> AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》待办事项，请及时处理。";
            $new_desc ="收到《".$formsetinst["formsetinst_name"]."》的待办信息，请及时处理";
            $new_title = "收到【".$formsetinst["creater_name"]."】关于《".$formsetinst["form_name"]."》的待办信息";
			// $new_title = $formsetinst["form_name"];
			$msg_title = "收到了【" . $formsetinst['creater_name'] . '】的待办信息';

			$wx_url = $this -> WxDomain.'?app='.$this->AppName.'&a=detail&listType=1&activekey=0&id='.$workitem_id;
			$pc_url = $this -> PcDomain.'index.php?app='.$this -> AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;

			try {
				$this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 1, $formsetinst["creater_id"], array($user_id), array(), $formsetinst['app_version']);
			} catch (\Exception $e) {
				parent::log_e("推送消息给【 " . $user['name'] . "】失败！");
			}
		}unset($msg_send);
		
		$return_arr[0] = $is_finish;
		$return_arr[1] = $work_node_name;
		$return_arr[2] = $receiver_str;
		return $return_arr;
	}
	
	/**
	 * 自动知会流程new
	 * @param array $curr_workitem
	 * @param int $formsetinst_id
	 * @param array $work_nodes
	 */
	private function notify_process($curr_workitem,$formsetinst_id,$work_nodes){
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
		// gch add 
		$formsetinst_form_vals = $this -> get_form_vals($formsetinst_id);

		if(is_array($formsetinst)){
            $input_notify_people = $this->getInputNotifyUsers($curr_workitem,$formsetinst_form_vals);

			$is_end_notify = $curr_workitem["is_auto_notify"];
			$notify_people = array();
			$end_notify_people = array();
			if($is_end_notify==self::$NotifyAutoEnd&&!empty($curr_workitem["notify_rule"])){//结束后知会
				$notify_rule 	= $curr_workitem["notify_rule"];
				$notify_rule 	= explode(",",$notify_rule);
				$notify_user 	= $curr_workitem["notify_user"];
				$notify_user 	= explode(",",$notify_user);
				$notify_group 	= $curr_workitem["notify_group"];
				$notify_group 	= explode(",",$notify_group);
				// gch add
				$notify_input 	= $curr_workitem["notify_input"];
				$notify_input 	= explode(",",$notify_input);
				
				$notify_arr = array(
					'notify_rule' => $notify_rule ? $notify_rule : array(),
					'notify_user' => $notify_user ? $notify_user : array(),
					'notify_group' => $notify_group ? $notify_group : array(),
					// gch add
					'notify_input' => $notify_input ? $notify_input : array(),
					'formsetinst_form_vals' => $formsetinst_form_vals ? $formsetinst_form_vals : array(),
				);
				$end_notify_people = $this->set_notify_user($notify_arr,$curr_workitem);
			}

            $end_notify_people_for_dangerous = array();
            //判断是否存在隐患，结束有隐患知会
            if($is_end_notify==self::$NotifyAutoEndForDangeous&&!empty($curr_workitem["notify_rule"])){//结束后知会
                $notify_rule 	= $curr_workitem["notify_rule"];
                $notify_rule 	= explode(",",$notify_rule);
                $notify_user 	= $curr_workitem["notify_user"];
                $notify_user 	= explode(",",$notify_user);
                $notify_group 	= $curr_workitem["notify_group"];
                $notify_group 	= explode(",",$notify_group);
                // gch add
                $notify_input 	= $curr_workitem["notify_input"];
                $notify_input 	= explode(",",$notify_input);

                $notify_arr = array(
                    'notify_rule' => $notify_rule ? $notify_rule : array(),
                    'notify_user' => $notify_user ? $notify_user : array(),
                    'notify_group' => $notify_group ? $notify_group : array(),
                    // gch add
                    'notify_input' => $notify_input ? $notify_input : array(),
                    'formsetinst_form_vals' => $formsetinst_form_vals ? $formsetinst_form_vals : array(),
                );

                //判断是否存在隐患
                $exist_dangerous = g('mv_formsetinst')->get_form_vals($formsetinst_id,['type='=>'dangerous','val like'=>'%2%'],'id');

                if($exist_dangerous){
                    $end_notify_people_for_dangerous = $this->set_notify_user($notify_arr,$curr_workitem);
                    $end_notify_people = array_merge($end_notify_people, $end_notify_people_for_dangerous);
                }
            }
			
			$work_node = $work_nodes[0];
			$work_node_id = $work_node['id'];
			$work_node = g("mv_work_node")->get_by_id($this->ComId,$work_node_id);
			$start_notify_people = Array();
			if(is_array($work_node)&&$work_node["is_auto_notify"]==self::$NotifyAutoStart&&!empty($work_node["notify_rule"])){
				$notify_rule 	= $work_node["notify_rule"];
				$notify_rule 	= explode(",",$notify_rule);
				$notify_user 	= $work_node["notify_user"];
				$notify_user 	= explode(",",$notify_user);
				$notify_group 	= $work_node["notify_group"];
				$notify_group 	= explode(",",$notify_group);
				// gch add
				$notify_input 	= $work_node["notify_input"];
				$notify_input 	= explode(",",$notify_input);
				
				$notify_arr = array(
					'notify_rule' => $notify_rule ? $notify_rule : array(),
					'notify_user' => $notify_user ? $notify_user : array(),
					'notify_group' => $notify_group ? $notify_group : array(),
					// gch add
					'notify_input' => $notify_input ? $notify_input : array(),
					'formsetinst_form_vals' => $formsetinst_form_vals ? $formsetinst_form_vals : array(),
				);
				$start_notify_people = $this -> set_notify_user($notify_arr,$curr_workitem,$work_nodes);
			}

			$notify_people = array_merge($end_notify_people, $start_notify_people);

			//判断知会人是否已存在，避免重复推送
            foreach ($input_notify_people as $item){
                if(!in_array($item['id'],array_column($notify_people,'id')) ){
                    $notify_people[] = $item;
                }
            }

			$this->pushNotifyNews($notify_people,$formsetinst);

		} else{
			throw new \Exception('没有权限知会该流程！');
		}
	}

    /**
     * 推送知会消息
     * @param $notify_people
     * @param $formsetinst
     * @return string
     */
	private function pushNotifyNews($notify_people,$formsetinst){
	    if(empty($notify_people))
	        return '';

        foreach($notify_people as $user){
            $receiver_id = $user["id"];
            $receiver_name = $user["name"];
            $notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);

            // 消息推送
            $wx_title = $this -> AppCName . '知会提醒';
            $desc = $this -> AppCName."：".$this -> UserName."将流程《".$formsetinst["formsetinst_name"]."》知会给您，请及时了解流程详情。";
            $new_desc = $this -> UserName."将《".$formsetinst["formsetinst_name"]."》知会给您，请及时查看";
            $new_title = "收到【".$this -> UserName."】关于《".$formsetinst["form_name"]."》的知会信息";
            // $new_title = $formsetinst["form_name"];
            $msg_title = "收到了【" . $this -> UserName . '】的知会信息';

            $wx_url = $this -> WxDomain.'?app='.$this -> AppName.'&a=detail&listType=3&activekey=0&id='.$notify_id;
            $pc_url = $this -> PcDomain.'index.php?app='.$this -> AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;

            try {
                $this -> _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, '', 3, $this -> user_id, array($receiver_id), array(), $formsetinst['app_version']);
            } catch (\Exception $e) {
                parent::log_e("推送消息给【 " . $receiver_name . "】失败！");
            }
        } unset($user);

        return '';
    }

    /**
     * 获取控件知会人员信息
     */
	private function getInputNotifyUsers($curr_workitem,$formsetinst_form_vals){
        $input_notify_people = array();

        // 知会控件配置
        $notify_input = explode(';',$curr_workitem['notify_input']);
        !$notify_input = $notify_input = [];

        //判断当前是否为开始节点，是判断隐患控件是否需要知会
        if($curr_workitem['is_start']==1){
            $input_notify_rule = [];
            $input_notify_user = [];
            $input_notify_input = [];

            //判断是否存在隐患控件 || lyc：申请人+车辆
            foreach ($formsetinst_form_vals as $item){
                if($item['type']=='dangerous' && in_array($item['notify_type'],self::$InputStateNotify) && in_array(2,$item['val']) ){
                    switch ($item['notify_type']){
                        case self::$InputStateNotifyPeople:
                            $input_notify_rule[] = self::$StateNotifyPeople;
                            if(!empty($item['notify_list'])){
                                $input_notify_user = array_merge($input_notify_user,array_column($item['notify_list'],'id'));
                            }
                            break;
                        case self::$InputStateNotifyLeader:
                            $input_notify_rule[] = self::$StateNotifyLeader;
                            break;
                        case self::$InputStateNotifySelf:
                            $input_notify_rule[] = self::$StateNotifySelf;
                            break;
                    }
                }
                if (in_array($item['type'],self::$notify_input_val) && in_array($item['input_key'],$notify_input) ){
                    if (!in_array(self::$StateNotifyInput,$input_notify_rule)){
                        $input_notify_rule[] = self::$StateNotifyInput;
                    }
                    $input_notify_input[] = $item['input_key'];
                }
            }unset($item);

            //整改
            $notify_arr = array(
                'notify_rule' => $input_notify_rule ? $input_notify_rule : array(),
                'notify_user' => $input_notify_user ? $input_notify_user : array(),
                'notify_group' => array(),
                'notify_input' => $input_notify_input,
                'formsetinst_form_vals' => $formsetinst_form_vals ? $formsetinst_form_vals : array(),
            );
            $input_notify_people = $this -> set_notify_user($notify_arr,$curr_workitem);
        }

        return $input_notify_people;
    }
	
	/**
	 * 根据知会规则和当前工作项封装知会人信息new
	 * @param array $notify_arr
	 * @param array $curr_workitem
	 */
	private function set_notify_user($notify_arr,$curr_workitem,$next_nodes=[]){
		$notify_people = Array();

		foreach ($notify_arr['notify_rule'] as $notify_rule){
			
			if($notify_rule==self::$StateNotifyPeople){
				$user_ids = $notify_arr["notify_user"];
				$group_ids = $notify_arr["notify_group"];
				if(!empty($group_ids)){
					$group_user_ids = g("dao_group")->get_user_by_ids($this -> com_id, $group_ids);
					$tmp_user_ids = array_column($group_user_ids, 'id');
					$user_ids = array_merge($user_ids, $tmp_user_ids);
				}
				$users = g("dao_user")->list_by_ids($user_ids,'*',FALSE);
				if(is_array($users)){
					foreach ($users as $user){
						$this->set_receiver($notify_people, $user);
					}
					unset($user);
				}
			}
			
			else if ($notify_rule==self::$StateNotifySelf){
				$user_id = $curr_workitem["creater_id"];
				$user = g("dao_user")->get_by_id($user_id);
				if(is_array($user)){
					$this->set_receiver($notify_people, $user);
				}
			}
			else if($notify_rule==self::$StateNotifyLeader){
				$user_id = $curr_workitem["creater_id"];
				$user_leader = g("dao_user")->list_leader_by_id($user_id,FALSE,'*');
				if(is_array($user_leader)){
					foreach ($user_leader as $user){
						if(is_array($user)){
							$this->set_receiver($notify_people, $user);
						}
					}
					unset($user);
				}
			}
			else if($notify_rule==self::$StateNotifyILeader){ //知会上级领导
                if($next_nodes){
                    foreach ($next_nodes as $node){
                        $receiver_arr = isset($node['receiver_arr'])?$node['receiver_arr']:[];
                        foreach ($receiver_arr as $r){
                            $this->get_user_leader_receiver_list($notify_people, $r['receiver_id']);
                        }unset($r);
                    }
                }else{
                    $user_id = $curr_workitem["handler_id"];
                    $this->get_user_leader_receiver_list($notify_people, $user_id);
                }
            }
			else if($notify_rule==self::$StateNotifyInput){
				// gch add | lyc edit
				if (!empty($notify_arr['notify_input']) && !empty($notify_arr['formsetinst_form_vals'])) {
					$notify_userids = array();
					foreach ($notify_arr['notify_input'] as $v) {
                        $tmp_arr = explode('@',$v);
                        $v = $tmp_arr[0];
                        $detail_id = isset($tmp_arr[1])?$tmp_arr[1]:0;

						// 知会控件
                        if (!isset($notify_arr['formsetinst_form_vals'][$v])) {
                            continue;
                        }
                        $input = $notify_arr['formsetinst_form_vals'][$v];
                        $input_type = $input['type'];
                        switch ($input_type){
                            case 'people':
                            case 'schoolDept':
                                if (isset($input['ids'])) {
                                    $notify_userids = array_merge($notify_userids, explode(',', $input['ids']));
                                }
                                break;
                            case 'school':
                                if (isset($input['ids'])) {
                                    $school_ids = explode(',', $input['ids']);
                                    if($school_ids){
                                        $dao_school_contact = new ProcessSchoolContact();
                                        foreach ($school_ids as $school_id){
                                            $tmp_users = $dao_school_contact->listUsersBySIdAndRids($school_id,[$detail_id]);
                                            $notify_userids = array_merge($notify_userids, array_column($tmp_users,'id'));
                                        }
                                    }
                                }
                                break;
                            case 'car':
                                foreach ($input['val_list'] as $item){
                                    $notify_userids[] = $item['driver']['id'];
                                }
                                break;
                        }
					}
					// 获取人员信息
					if (!empty($notify_userids)) {
						$root_id = isset($_SESSION[SESSION_VISIT_DEPT_ID]) ? $_SESSION[SESSION_VISIT_DEPT_ID] : 0;
						$user = g("dao_user")->get_by_ids('', $notify_userids, '*');
						
						if (is_array($user) && !empty($user)) {//如果用户不存在，则使用地址本
							foreach ($user as $v) {
								if(is_array($v)){
									$this->set_receiver($notify_people, $v);
								}
							}
						}
					}
				}
			}
			
		}
		unset($notify_rule);

		return $notify_people;
	}

	
	/**
	 * 判断接收人数组中是否有有当前需要添加的人员
	 * $receive_array数组结果集 $user将要加入的用户
	 */
	private function set_receiver(&$receive_array,$user){
		$is_same = FALSE;
		foreach ($receive_array as $receiver){
			if($receiver["id"]==$user["id"]){
				$is_same = TRUE;
				break;
			}
		}
		if(!$is_same){
			$receive_array = array_merge($receive_array, array($user));
		}
	}
	
	/**
	 * 获取用户接收人信息
	 */
	private function get_user_receiver_list(&$receive_array,$userid){
		$user = g("dao_user")->get_by_id($userid);
		if(is_array($user)){//如果用户不存在，则使用地址本
			$this->set_receiver($receive_array, $user);
		}
	}

	/**
	 * gch add
	 * 获取多用户接收人信息
	 */
	private function get_users_receiver_list(&$receive_array,$userids){
		if (empty($userids)) {
			return;
		}

		$root_id = isset($_SESSION[SESSION_VISIT_DEPT_ID]) ? $_SESSION[SESSION_VISIT_DEPT_ID] : 0;
		$user = g("dao_user")->get_by_ids('', $userids, '*');
		if (is_array($user) && !empty($user)) {//如果用户不存在，则使用地址本
			foreach ($user as $v) {
				$this->set_receiver($receive_array, $v);
			}
		}
	}

	/**
	 * 获取用户上级领导接收人信息
	 */
	//  gch edit
	protected function get_user_leader_receiver_list(&$receive_array,$userid, $field = '*'){
		$user_leader = g("dao_user")->list_leader_by_id($userid,FALSE, $field);

		if(is_array($user_leader)){//如果用户不存在，则使用地址本
			foreach ($user_leader as $user){
				if(is_array($user)){
					$this->set_receiver($receive_array, $user);
				}
			}
		}
	}
	
	/**
	 * 获取用户部门接收人信息
	 */
	private function get_user_dept_receiver_list(&$receive_array,$userid){
		$user = g("dao_user")->get_by_id($userid);
		$dept_list = $user["dept_list"];
		if(empty($dept_list)){
			throw new \Exception('创建人不存在部门，请设置创建人的部门');
		}
		$depts = json_decode($dept_list, TRUE);
		foreach ($depts as $dept_id){
			$dept_user = g("dao_user")->list_direct_by_dept_id($dept_id,'*',FALSE);
			if(is_array($dept_user)){
				foreach ($dept_user as $user){
					$this->set_receiver($receive_array, $user);
				}
			}
		}
	}

	/**
	 * 推送消息
	 * @param  [type]  $new_title   主页型推送标题
	 * @param  [type]  $wx_title    
	 * @param  [type]  $msg_title   推送标题
	 * @param  [type]  $desc        推送内容
	 * @param  [type]  $new_desc    推送内容
	 * @param  [type]  $wx_url      wx端链接
	 * @param  [type]  $pc_url      pc端链接
	 * @param  string  $pic_url     图片链接
	 * @param  integer $type        消息类型 流程类：1：待办， 2：通过，3：知会，其他应用 0：默认消息
	 * @param  integer $send_user   发送人ID 系统发送则为0
	 * @param  array   $user_list   
	 * @param  array   $dept_list   
	 * @param  integer $app_version 应用版本，0：旧版，1：新版
	 * @return [type]               
	 */
	private function _push_news($new_title, $wx_title, $msg_title, $new_desc, $desc, $wx_url, $pc_url, $pic_url='', $type=0, $send_user=0, $user_list=array(), $dept_list=array(), $app_version=0) {
		if ($app_version == 1) {
			// 新方式推送
			$wx_url .= "&corpurl=". $_SESSION[SESSION_VISIT_CORP_URL];
			$ret = parent::send_news($new_title, $new_desc, $pic_url, $wx_url, 0, $type, $send_user, $user_list, $dept_list,$msg_title);
			if (!$ret) throw new \Exception('发送消息失败');
		} else {
			// 旧方式推送
			// 推送wx端
			parent::send_wx_news($wx_title, $desc, $wx_url, '', $user_list, $dept_list);
			// 推送pc端
			parent::send_pc_news($desc, $pc_url, $user_list, $dept_list);
		}
	}

	/**
	 * 将请假、外出表单的时长控件的小时转换为天
	 * @param  [type] $is_other_proc    
	 * @param  [type] &$formsetinst_val 
	 * @return [type]                   
	 */
	public function change_special_input($is_other_proc, &$formsetinst_val) {
		$input_key = '';
		$config = load_config('model/workshift/form');
		switch ($is_other_proc) {
			case 4:
				$input_key = $config['Hide_rest'][0];
				break;
			case 5:
				$input_key = $config['Hide_legwork'][0];
				break;
		}
		if (empty($input_key)) return;

		//每天工时
		$ws_config = g('ws_config') -> get_ws_config($this -> com_id);
		if (!$ws_config) return;
		$work_hour = $ws_config['work_hour'];

		foreach ($formsetinst_val as $key => &$val) {
			if ($key == $input_key) {
				$val['val'] = round($val['val']/$work_hour, 2);
			}

		}unset($val);

	}

	/**
	 * 将请假表单的请假类型控件退回开始节点时设为不可编辑
	 * @param  [type] $is_other_proc    
	 * @param  [type] &$formsetinst_val 
	 * @return [type]                   
	 */
	public function change_rest_input($is_other_proc, $is_start=0, $is_returnback=0, &$formsetinst_val) {
		if (!($is_other_proc ==self::$IsNewRest && $is_start && $is_returnback)) return;
		
		$config = load_config('model/workshift/form');
		$input_key = $config['Fix_rest'][0];
		if (!$input_key) return;

		foreach ($formsetinst_val as $key => &$val) {
			$key == $input_key && $val['edit_input'] = 0;
		}unset($val);

	}

	/**
	 * 检测当前用户是否设置并启用指纹审批
	 * @param  boolean $on 是否声明js-sdk相关的变量
	 */
	public function check_fingerprint($on=false) {
		$fp_info = g('api_fingerprint')->get_user_fingerprint($this->ComId, $this->UserId);
		//没有设置指纹或关闭了指纹
		if (!$fp_info || !$fp_info['state']) return false;

		if (!empty($fp_info['fingerprint'])) {
			$on && g('api_jssdk') -> assign_jssdk_sign('', $this->ComId);
			return true;
		}
		return false;
	}


	public function get_microtime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	/**
	* 检查是否开启
	* gch add trigger form
	* @access public
	* @param int $workNodeId 表单节点id  
	* @return int $formId 0-不开启，不为0-开启
	*/
	public function checkOpen($comId, $workNodeId) {
		$formId = 0;

		$nodeInfo = g("mv_work_node") -> get_by_id($comId, $workNodeId, 'trigger_form');
		if (!empty($nodeInfo)) {
			$triggerForm = json_decode($nodeInfo["trigger_form"], TRUE);
			is_array($triggerForm) && isset($triggerForm["form_id"]) && !empty($triggerForm["form_id"]) && $formId = intval($triggerForm["form_id"]);
		}

		return $formId;
	}

	/**
	* 对比当前表单和触发表单是否控件一致
	* gch add trigger form
	* @access public
	* @param int $workNodeId 表单节点id  
	* @return int $formId 0-不开启，不为0-开启
	*/
	public function checkInputConsistency($comId, $triggerFormId, $currFormId, &$inputkeyMap) {
		$flag = false;

		$field = array(
			'`type`', '`name`', 'input_key'
		);

		$cond = array(
			'form_id=' => $currFormId,
		);
		$currInputs = g('pkg_db') -> select('mv_proc_form_model_label', $field, $cond);
		parent::log_i('获取当前表单控件:' . json_encode($currInputs));
		
		$cond = array(
			'form_id=' => $triggerFormId,
		);
		$triggerInputs = g('pkg_db') -> select('mv_proc_form_model_label', $field, $cond);
		parent::log_i('获取触发表单控件:' . json_encode($triggerInputs));

		$currInputKey = array_column($currInputs, 'input_key');
		$triggerInputKey = array_column($triggerInputs, 'input_key');
		$inputkeyMap = array_combine($currInputKey, $triggerInputKey);

		// 对比控件是否一致
		$currInputs = array_column($currInputs, 'type', 'name');
		ksort($currInputs);
		$currInputs = http_build_query($currInputs);
		$currInputs = md5($currInputs);

		$triggerInputs = array_column($triggerInputs, 'type', 'name');
		ksort($triggerInputs);
		$triggerInputs = http_build_query($triggerInputs);
		$triggerInputs = md5($triggerInputs);

		$currInputs == $triggerInputs && $flag = true;
		parent::log_i('对比当前表单和触发表单是否控件一致:' . $flag);

		return $flag;
	}

	/**
	* 发起触发表单
	* gch add trigger form
	* @access public
	* @param int $createrId 当前流程实例发起人  
	* @param int $triggerFormId 触发表单id  
	* @param array $inputkeyMap 控件key对应关系
	* @return boolean 
	*/
	public function SendTriggerForm($comId, $createrId, $triggerFormId, $currentFormsetinstId, $inputkeyMap) {
		$flag = false;
		
		// 获取发起人信息
		$createrInfo = g('dao_user') -> get_by_id($createrId);
		
		$triggerFormInfo = g('mv_form') -> get_by_id($triggerFormId, $comId);
		to_log('Info', '', '获取触发表单信息:' . json_encode($triggerFormInfo));

		$cond = array(
			'form_id=' => $triggerFormId,
			'com_id=' => $comId,
		);
		$triggerWorkInfo = g('pkg_db') -> select_one("mv_proc_work_model", '*', $cond);
		to_log('Info', '', '获取触发流程模板信息:' . json_encode($triggerWorkInfo));

		$formVals = $this -> get_form_vals($currentFormsetinstId);
		// 替换控件key
		$formVals = $this -> replaceInputKey($inputkeyMap, $formVals);
		// 最后一个表单修改控件值
		$formVals = $this -> changeInputVal($comId, $triggerWorkInfo['work_pic_json'], $formVals);
		to_log('Info', '', '控件信息:' . json_encode($formVals));

		// 保存触发表单
		$formName = isset($triggerFormInfo["form_name"]) ? $triggerFormInfo["form_name"] : '';
		$formName .= '-' . $createrInfo['name'] . '-' . date('YmdHi');
		$data = array(
			'form_id' => $triggerFormId,
			'form_name' => $formName,
			'work_id' => isset($triggerWorkInfo["id"]) ? $triggerWorkInfo["id"] : 0, // 流程模板id
			'form_vals' => $formVals,
			'app_version' => isset($triggerFormInfo['app_version']) ? $triggerFormInfo['app_version'] : 0,
			'is_other_proc' => isset($form['is_other_proc']) ? $form['is_other_proc'] : 0,
			'judgement' => '',
			'workitem_id' => 0,
			'formsetInst_id' => 0,
			'files' => array(),
			'creater_id' => $createrId,
			'creater_name' => $createrInfo['name'],
		);
		to_log('Info', '', '保存触发表单信息:' . json_encode($data));
		$workitemRet = $this -> save_workitem_parent($data);

		$cond = array(
			'id=' => $workitemRet['workitem_id'],
			'com_id=' => $comId,
		);
		$triggerWorkitemInfo = g('pkg_db') -> select_one("mv_proc_workitem", '*', $cond);
		to_log('Info', '', '获取触发流程实例信息:' . json_encode($triggerWorkitemInfo));

		// 发送触发表单
		$field = ' mpw.*,mpwn.* ';
		$workitem = $this->get_curr_workitem($workitemRet['workitem_id'],$field);
		$next_node_id = json_decode($workitem["next_node_id"], TRUE);
		// 获取第一个节点
		$next_node_id = is_array($next_node_id) ? array_shift($next_node_id) : 0;
		
		$data = array(
			"work_nodes" => array(
				array(
					"id" => $next_node_id,
					"receiver_arr" => array(),
				),
			),
			"workitem_id" => $workitemRet['workitem_id'],
		);

		$next_node_id_arr = $this -> get_next_workitem($workitemRet['workitem_id']);
		to_log('Info', '', '获取下一步骤new:' . json_encode($next_node_id_arr));
		foreach ($next_node_id_arr as $val) {
			foreach ($val as $node) {
				foreach ($node as $u) {
					$tmp_data = array();
					$tmp_data['receiver_id'] = $u['id'];
					$tmp_data['receiver_name'] = $u['name'];
					$data["work_nodes"][0]["receiver_arr"][] = $tmp_data;
				}unset($u);
			}unset($node);
		}unset($val);
		to_log('Info', '', '发送触发表单数据：' . json_encode($data));

		$ret = $this -> send_workitem_parent($data);
		if ($ret['return_arr'][0] == 'TRUE') {
			$msg = '流程流转结束';
		} else {
			$msg = '流程发送成功，下一步骤为'. $ret['return_arr'][1] . ' 接收人为' . json_encode($ret['return_arr'][2]);
		}
		to_log('Info', '', $msg);

		$flag = true;
		return $flag;
	}

	/**
	* 替换成触发控件key
	* gch add trigger form
	* @access public
	* @param   
	* @return   
	*/
	public function replaceInputKey($inputkeyMap, $formVals) {
		$formValsJson = json_encode($formVals);

		foreach ($inputkeyMap as $currInputKey => $triggerInputKey) {
			$currInputKey = '"' . $currInputKey . '"';
			$triggerInputKey = '"' . $triggerInputKey . '"';
			$formValsJson = str_replace($currInputKey, $triggerInputKey, $formValsJson);
		}

		$formVals = json_decode($formValsJson, true);
		return $formVals;
	}

	/**
	* 最后一个表单修改控件值
	* gch add trigger form
	* @access public
	* @param   
	* @return   
	*/
	public function changeInputVal($comId, $work_pic_json, $formVals) {
		// 判断是否是最后一个触发表单
		$flag = preg_match('/"trigger_form":{"form_id":"[0-9]+"/i', $work_pic_json);
		if ($flag == 1) {
			return $formVals;
		}

		foreach ($formVals as &$v) {
			// 将"备注"控件值修改为入库数据
			$v['type'] == 'text' && $v['name'] == '备注' && $v['val'] = '入库数据';
			
			// 将"数量"控件置为0
			$v['type'] == 'text' && $v['name'] == '数量' && $v['val'] = 0;
			if ($v['type'] == 'table' && isset($v['rec']) && !empty($v['rec'])) {
				foreach ($v['rec'] as &$rec) {
					foreach ($rec as &$input) {
						$input['type'] == 'text' && $input['name'] == '数量' && $input['val'] = 0;
					}
				}
			}

		}unset($v);

		return $formVals;
	}

    /**
     * 发送短信知会
     * @param $formsetinst_id 表单实例标识
     * @param $type 短信通知类型
     */
	private function sendSmsNotify($com_id,$formsetinst_id,$type){
	    $root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
	    $formsetinst = g('mv_formsetinst')->get_by_id($formsetinst_id);
        if(empty($formsetinst))
            throw new \Exception('实例不存在');

        //获取表单模板信息
        $form_model = g('mv_form')->get_by_id($formsetinst['form_id'],$com_id);
        $form_model_name = isset($form_model['form_name'])?$form_model['form_name']:'';

        //判断当前表单模板对应的短信信息
        $server_sms_model = new \model\api\server\mv_proc\Mv_Sms_Model();
        $models = $server_sms_model->get_by_form_id($com_id,$formsetinst['form_id'],$type);
        if(empty($models))
            return ;

        //获取表单实例对应控件内容
        $formsetinst_vals = g('mv_formsetinst')->get_form_vals($formsetinst_id);
        $formsetinst_vals = array_column($formsetinst_vals,null,'input_key');

        //根据表单模板短信配置，将消息数据插入队列
        $dao_sms_task = new \model\api\dao\DaoSmsTask();
        $dao_sms_template = new \model\api\dao\DaoSmsTemplate();
        foreach ($models as $model){
            try{
                $send_data = [];

                //获取短信内容
                $sms_template = $dao_sms_template->base_get($model['sms_template_id']);
                if(empty($sms_template)){
                    throw new \Exception('短信模板不存在');
                }
                $sms_content = $sms_template['sms_content'];

                //短信内容替换
                //获取短信模板对应参数数值
                $sms_param = [];
                $message_fill = is_array($model['message_fill'])?$model['message_fill']:json_decode($model['message_fill'],true);
                //[{"index":"1","input_map":[{"input_key":"input1639472899000","input_name":"{单行文本}","type":""}],"input_string":"哈哈{单行文本}哈哈"}]
                foreach ($message_fill as $item){
                    $sms_param[$item['index']] = $item['input_string'];
                    //查询对应控件数值
                    $input_map = isset($item['input_map'])?$item['input_map']:[];
                    foreach ($input_map as $im){
                        $im['type'] = $im['type'].'';
                        $real_val = '';
                        switch ($im['input_key']){
                            case self::$ApplyerDept:
                                $create_m_dept = g("dao_user")->get_main_dept($formsetinst['creater_id'],FALSE);
                                if(empty($create_m_dept)){
                                    $depts = g("dao_user")->list_all_dept($formsetinst['creater_id'],FALSE,FALSE);
                                    $real_val = $depts[0]["name"];
                                }else{
                                    $real_val = $create_m_dept["name"];
                                }
                                break;
                            case self::$ApplyerName:
                                $user = g('dao_user')->get_by_id($formsetinst['creater_id']);
                                $real_val = isset($user['name'])?$user['name']:'';
                                break;
                            case self::$ApplyDateM:
                                $real_val = date('Ym',$formsetinst['create_time']);
                                break;
                            case self::$ApplyDateD:
                                $real_val = date('Ymd',$formsetinst['create_time']);
                                break;
                            case self::$ApplyDateMin:
                                $real_val = date('Ymd H:i',$formsetinst['create_time']);
                                break;
                            case self::$FormName:
                                $real_val = $form_model_name;
                                break;
                            default:
                                if(isset($formsetinst_vals[$im['input_key']])){
                                    $other = json_decode($formsetinst_vals[$im['input_key']]['other'],true);
                                    //表单控件对应赋值
                                    switch ($formsetinst_vals[$im['input_key']]['type']){
                                        case 'text':
                                        case 'conflict':
                                        case 'date':
                                        case 'checkbox':
                                        case 'muselect':
                                        case 'radio':
                                        case 'select':
                                        case 'wuzhi_attr':
                                        case 'wuzhi_chose':
                                        case 'textarea':
                                            $real_val = $formsetinst_vals[$im['input_key']]['val'];
                                            break;
                                        case 'car':
                                            $tmp_real_val = [];
                                            $var_list = isset($other['val_list'])?$other['val_list']:[];
                                            foreach ($var_list as $v){
                                                switch ($im['type']){
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitCarDriverName:
                                                        $tmp_real_val[] = !empty($v['driver']['name'])?$v['driver']['name']:'';
                                                        break;
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitCarDriverPhone:
                                                        $tmp_user = g('dao_user')->get_by_id($v['driver']['id'],'mobile');
                                                        $tmp_real_val[] = !empty($tmp_user['mobile'])?$tmp_user['mobile']:'';
                                                        break;
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitCarName:
                                                        $tmp_real_val[] = !empty($v['name'])?$v['name']:'';
                                                        break;
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitCarSeatNum:
                                                        $tmp_real_val[] = !empty($v['seat'])?$v['seat'].'座':'';
                                                        break;
                                                    default:
                                                        // 默认跟后台显示保持一致
                                                        if($v['driver']['name']!=='' && $v['seat']!==''){
                                                            $tmp_real_val[]=$v['name'].'-'.$v['seat'].'座 司机：'.$v['driver']['name'];
                                                        }elseif($v['driver']['name']==='' && $v['seat']!==''){
                                                            $tmp_real_val[]=$v['name'].'-'.$v['seat'].'座';
                                                        }elseif($v['driver']['name']!=='' && $v['seat']===''){
                                                            $tmp_real_val[]=$v['name'].' 司机：'.$v['driver']['name'];
                                                        }elseif($v['driver']['name']==='' && $v['seat']===''){
                                                            $tmp_real_val[] = $v['name'];
                                                        }
                                                        break;
                                                }
                                            }unset($v);
                                            $real_val = implode(';',$tmp_real_val);
                                            break;
                                        case 'dangerous':
                                            $tmp_real_val = [];
                                            $vals = json_decode($formsetinst_vals[$im['input_key']]['val'],true);
                                            $snake_list = isset($other['snake_list'])?$other['snake_list']:[];
                                            foreach ($snake_list as $k=>$s){
                                                $flag = '';
                                                if(isset($vals[$k])){
                                                    $vals[$k]==1 && $flag='(√)';
                                                    $vals[$k]==2 && $flag='(×)';
                                                    $vals[$k]==3 && $flag='(无)';
                                                }
                                                $tmp_real_val[] = $s.$flag;
                                            }unset($s);
                                            $real_val = implode(';',$tmp_real_val);
                                            break;
                                        case 'dept':
                                            $dept_ids = json_decode($formsetinst_vals[$im['input_key']]['val'],true);
                                            if($dept_ids){
                                                $dept_list = g('dao_dept')->get_by_ids($dept_ids,'id,name');
                                                $real_val = implode(';',array_column($dept_list,'name'));
                                            }
                                            break;
//                                        case 'file':
//                                            break;
                                        case 'money':
                                            $real_val = trim($formsetinst_vals[$im['input_key']]['val']);
                                            switch ($other['money_type']){
                                                case 0:
                                                    $real_val = $real_val!==''?'￥'.$real_val:'';
                                                    break;
                                                case 1:
                                                    $real_val = $real_val!==''?'$'.$real_val:'';
                                                    break;
                                                case 2:
                                                    $real_val = $real_val!==''?'€'.$real_val:'';
                                                    break;
                                                case 3:
                                                    $real_val = $real_val!==''?'HK$'.$real_val:'';
                                                    break;
                                            }
                                            break;
                                        case 'people':
                                            $user_ids = json_decode($formsetinst_vals[$im['input_key']]['val'],true);
                                            if($user_ids){
                                                $user_list = g('dao_user')->getByIds($user_ids,'name,mobile');
                                                switch ($im['type']){
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitPeopleName:
                                                        $real_val = implode(';',array_column($user_list,'name'));
                                                        break;
                                                    case \model\api\server\mv_proc\Mv_Sms_Model::InputSplitPeoplePhone:
                                                        $real_val = implode(';',array_column($user_list,'mobile'));
                                                        break;
                                                    default:
                                                        $real_val = implode(';',array_column($user_list,'name'));
                                                        break;
                                                }
                                            }
                                            break;
//                                        case 'table':
//                                            break;
                                    }
                                }
                                break;
                        }
                        //字符替换
                        $sms_param[$item['index']] = str_replace($im['input_name'],$real_val,$sms_param[$item['index']]);
                    }
                }unset($item);

                //将腾讯云的短信模板内容进行替换，显示给用户看
                $match_arr = [];
                preg_match_all("/\{[^\{\}]+\}/u", $sms_content, $match_arr);
                if(isset($match_arr[0])){
                    foreach ($match_arr[0] as $index){
                        $tmp = '';
                        if(isset($sms_param[$index])){
                            $tmp = $sms_param[$index];
                        }
                        $sms_content = str_replace($index,$tmp,$sms_content);
                    }
                }

                //获取短信接收人信息
                $receiver_ids = [];
                $receiver_list = [];

                $notify_rule = json_decode($model['notify_rule'],true);
                if(empty($notify_rule))
                    throw new \Exception('未指定通知对象');

                foreach ($notify_rule as $r){
                    switch ($r){
                        case \model\api\server\mv_proc\Mv_Sms_Model::NotifyRuleFixedReceiver:
                            //用户
                            $model['notify_user'] = json_decode($model['notify_user'],true);
                            if(!empty($model['notify_user'])){
                                $receiver_ids = array_merge($receiver_ids,array_column($model['notify_user'],'id'));
                            }
                            //部门人员
                            $model['notify_dept'] = json_decode($model['notify_dept'],true);
                            if(!empty($model['notify_dept'])){
                                $tmp_dept_ids = array_column($model['notify_dept'],'id');
                                $dept_users = g('dao_user')->get_by_depts($root_id,$tmp_dept_ids,'id',true);
                                $dept_users && $receiver_ids = array_merge($receiver_ids,array_column($dept_users,'id'));
                            }
                            //分组成员
                            $model['notify_group'] = json_decode($model['notify_group'],true);
                            if(!empty($model['notify_group'])){
                                $notify_group_ids = array_column($model['notify_group'],'id');
                                $group_list = g('dao_group')->get_by_ids($com_id,$notify_group_ids,'user_list');
                                if($group_list){
                                    foreach ($group_list as $g){
                                        $tmp_user_ids = json_decode($g['user_list'],true);
                                        $tmp_user_ids && $receiver_ids = array_merge($receiver_ids,$tmp_user_ids);
                                    }unset($g);
                                }
                            }

                            break;
                        case \model\api\server\mv_proc\Mv_Sms_Model::NotifyRuleFormCreater:
                            $receiver_ids[] = $formsetinst['creater_id'];
                            break;
                        case \model\api\server\mv_proc\Mv_Sms_Model::NotifyRuleFormInput:
                            $inputs = !empty($model['notify_input'])?json_decode($model['notify_input'],true):[];

                            if($inputs){
                                $inputs = array_column($inputs,'input_key');
                                foreach ($inputs as $i){
                                    if(isset($formsetinst_vals[$i])){
                                        $other = json_decode($formsetinst_vals[$i]['other'],true);
                                        //表单控件对应赋值
                                        switch ($formsetinst_vals[$i]['type']){
                                            case 'car':
                                                $var_list = isset($other['val_list'])?$other['val_list']:[];
                                                foreach ($var_list as $v){
                                                    isset($v['driver']['id']) && $receiver_ids[]=$v['driver']['id'];
                                                }
                                                break;
                                            case 'people':
                                                $user_ids = json_decode($formsetinst_vals[$i]['val'],true);
                                                $receiver_ids = array_merge($receiver_ids,$user_ids);
                                                break;
                                        }
                                    }
                                }unset($i);
                            }
                            break;
                    }
                }unset($r);

                $receiver_ids = array_unique($receiver_ids);
                if($receiver_ids){
                    $receiver_list = g('dao_user')->getByids($receiver_ids,'id,name,mobile');
                }

                foreach ($receiver_list as $receiver){
                    try{
                        $send_data = array(
                            'com_id'=>$com_id,
                            'sms_content'=>$sms_content,
                            'receiver_id'=>$receiver['id'],
                            'receiver_name'=>$receiver['name'],
                            'receiver_phone'=>$receiver['mobile'],
                            'sms_model_id'=>$model['id'],
                            'sms_param'=>is_array($sms_param)?json_encode($sms_param,JSON_UNESCAPED_UNICODE):$sms_param,
                            'sms_template_id'=>$model['sms_template_id'],
                        );
                        $ret = $dao_sms_task->addTask($send_data);
                        if(!$ret)
                            throw new \Exception('消息加入队列失败');
                    }catch (\Exception $e){
                        to_log('Info','','加入推送列表失败:'.json_encode($model));
                        to_log('Info','','加入推送列表失败:'.json_encode($send_data));
                    }
                }

            }catch (\Exception $e){
                to_log('Info','','加入推送列表失败:'.json_encode($model));
                to_log('Info','','加入推送列表失败:'.json_encode($send_data));
            }

        }

        return ;
    }



}

// end of file