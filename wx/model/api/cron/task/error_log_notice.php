<?php
/**
 * 错误/异常日志汇总统计
 * 
 * @author yangpz
 * @create 2016-11-18
 */

PHP_SAPI != 'cli' && die('仅允许在cli模式下执行');

$root_dir = dirname(dirname(dirname(dirname(__DIR__))));
$init_file = $root_dir . '/init.php';
!file_exists($init_file) && die('初始化文件缺失');

include_once($init_file);
require_once dirname(dirname(__DIR__)) .'/exception/ExceptionBase.php';
load_config('model/api/classmap');
$mod_api_conf = load_config('model/api/model');

//设定脚本执行时间限制
set_time_limit(0);
//最大可用内存
ini_set('memory_limit', -1);

//要统计的目录
$dir_list = array(
    MAIN_DATA . 'logs' . DS,

    MAIN_DATA . 'api' . DS . 'logs' . DS,
    MAIN_DATA . 'activity' . DS . 'logs' . DS,
    MAIN_DATA . 'advice' . DS . 'logs' . DS,
    MAIN_DATA . 'default' . 'log' . DS,
    MAIN_DATA . 'exaccount' . DS . 'logs' . DS,
    MAIN_DATA . 'finstat' . DS . 'logs' . DS,
    MAIN_DATA . 'helper' . DS . 'logs' . DS,
    MAIN_DATA . 'index' . DS . 'logs' . DS,
    MAIN_DATA . 'legwork' . DS . 'logs' . DS,
    MAIN_DATA . 'preview' . DS . 'logs' . DS,
    MAIN_DATA . 'process' . DS . 'logs' . DS,
    MAIN_DATA . 'rest' . DS . 'logs' . DS,
    MAIN_DATA . 'vip' . DS . 'logs' . DS,
);

//记录统计结果到日志
$save_path = rtrim($mod_api_conf['log_dir'], '\\/') . DS . 'global.error.notice.log';
$content = g('pkg_err_file_notice') -> lists_and_log($dir_list, $save_path);

//邮件通知
if (!empty($content)) {
    $ser_conf = array(
        'send_service' 	=> 'smtp.exmail.qq.com',        //邮箱服务器地址
        'port' 			=> 465,					        //邮箱服务器端口
        'ssl' 			=> 1,					        //是否使用ssl
    );
    $form_user = array(
        'email'			=> 'technology@3ruler.com',		//邮箱账号
        'password'		=> 'ypzteam20140504',			//邮箱密码
        'name'			=> '移步到微',					//发件人名称
    );
    $to_user = array(
        array('email' => '783601800@qq.com',        'name'  => 'yangpz'),
        array('email' => '156815771@qq.com',        'name'  => 'chenyh'),
    );

    $notice_time = array(                              //以上时间点有异常才发邮件通知
        '0800', '1000', '1200',
        '1400', '1600', '1800',
        '2000',
    );
    $now = date('Hi');
    if (in_array($now, $notice_time)) {
        $content = str_replace("\n", '<br>', $content);
        $email_conf = load_config('package/email/global');
        g('pkg_email') -> send($ser_conf, $form_user, $to_user, '', '', '移步到微_WX_错误/异常日志', $content);
    }
}

/* End of this file */