<?php
/**
 * 主框架服务类
 * @author chenyihao
 * @date 2016-08-20
 *
 */

namespace model\open\server;


use model\api\server\mv_proc\Mv_Form;

class ServerOpenIndex extends \model\api\server\ServerBase {
    private $dao_api_caller_info;

	public function __construct($log_pre_str='') {
		global $mod_name;
		parent::__construct($mod_name, $log_pre_str);

		$this->dao_api_caller_info = new \model\open\dao\DaoApiCallerInfo();
	}

    /**
    ■ 唯一标识 (实例id MD5)
    ■ 巡检类型
    ● 教育局巡检
    ● 学校上报
    ■ 检查日期
    ■ 检查学校
    ■ 记录检查人
    ■ 整改超出时间
    ● 未设置
    ● 未超出
    ● 3（天数；保留小数点2为）
    ■ 隐患整体描述 （多行文本控件，中文匹配）
    ■ 整改意见
    ■ 所有隐患控件
    ● 消防安全
    ○ list1（×）
    ○
    ● 其他安全
    ○ 1111（V）
    ○ 2222(X)

     */
    public function listFormsetinstDetailForReport($page=0,$page_size=0){
        !$page && $page=1; !$page_size && $page_size=10;

        $time_input_names = $this->reportFiledsMap('check_time');

        $table = ' mv_proc_formsetinst mpf
    left join mv_proc_form_model mpfm on mpfm.id=mpf.form_id ';
        $cond = array(
            'mpf.info_state='=>1,
            'mpfm.info_state='=>1,
            'mpf.state!='=>0,
            '^(EXISTS( select id from mv_proc_formsetinst_label where formsetinst_id=mpf.id and info_state=1 and type="dangerous" ))' => '',
            '^( EXISTS(
                select id 
                from mv_proc_formsetinst_label 
                where formsetinst_id=mpf.id 
                and type="date" 
                and val!="" 
                and name in("'.implode('","',$time_input_names).'")
                )
             )'=>'',
        );

        $fields = 'mpf.id,mpf.com_id,mpfm.type,mpfm.complete_date_status,mpf.finish_time,mpf.state';
        $order_by = ' mpf.id desc ';
        $group_by='';
        $data = g('pkg_db')->select($table, $fields, $cond, $page, $page_size, $group_by, $order_by);
        if($data){
            $com_ids = array_column($data,'com_id');
            $companys = g('dao_com')->get_by_ids($com_ids,'id,name');
            $companys = array_column($companys,'name','id');
            foreach ($data as &$item){
                $tmp_type = $item['type'];
                $tmp_id = $item['id'];
                $item['id'] = md5($item['id']);
                $item['type'] = $tmp_type == Mv_Form::TYPE_RELATION?'教育局巡检':'学校上报';
                $item['check_school'] = isset($companys[$item['com_id']])?$companys[$item['com_id']]:' - ';

                //获取实例控件内容值
                $input_cond = array(
                    '__OR'=>array(
                        ['name IN'=>$this->reportFiledsMap('check_time'),'type='=>'date'],
                        ['name IN'=>$this->reportFiledsMap('check_school'),'type='=>'school'],
                        ['name IN'=>$this->reportFiledsMap('check_user'),'type='=>'people'],
                        ['name IN'=>$this->reportFiledsMap('dangerous_desc')],
                        ['name IN'=>$this->reportFiledsMap('dangerous_advice')],
                        ['name IN'=>$this->reportFiledsMap('check_result'),'type='=>'radio'],
                        ['type='=>'dangerous'],
                    ),
                );
                $inputs = g('mv_formsetinst')->get_form_vals($tmp_id,$input_cond,'name,val,type,other');
                if($tmp_type == Mv_Form::TYPE_RELATION){
                    $item['check_school'] = $this->reportInputValue('check_school',$inputs);
                }
                $item['check_time'] = $this->reportInputValue('check_time',$inputs);
                $item['check_user'] = $this->reportInputValue('check_user',$inputs);
                $item['check_result'] = $this->reportInputValue('check_result',$inputs);
                $item['dangerous_desc'] = $this->reportInputValue('dangerous_desc',$inputs);
                $item['dangerous_advice'] = $this->reportInputValue('dangerous_advice',$inputs);

                //获取隐患内容
                $dangerous_info = ' - ';
                if($inputs){
                    $tmp_info = [];
                    foreach ($inputs as $input){
                        if($input['type']!='dangerous')
                            continue ;

                        $other = json_decode($input['other'],true);
                        !is_array($input['val']) && $input['val'] = json_decode($input['val'],true);
                        if($input['type']== 'dangerous' && is_array($other['snake_list'])){
                            //隐患控件导出处理
                            $snake_list = [];
                            foreach ($other['snake_list'] as $key=>$tmp_item){
                                $flog = '';
                                if(!empty($input['val'][$key])){
                                    switch ($input['val'][$key]){
                                        case 1:
                                            $flog = '(√)';
                                            break;
                                        case 2:
                                            $flog = '(×)';
                                            break;
                                        case 3:
                                            $flog = '(无)';
                                            break;
                                        default :
                                            break;
                                    }
                                }
                                $flog && $snake_list[] = $tmp_item.$flog;
                            }unset($tmp_item);

                            if($snake_list){
                                $tmp_info[] = $input['name'];
                                $tmp_info[] = implode("\r\n",$snake_list);
                            }
                        }
                    }unset($input);

                    $tmp_info && $dangerous_info = implode("\r\n",$tmp_info);
                }
                $item['dangerous'] = $dangerous_info;

                $timeout_desc = '未设置';
                if($item['complete_date_status'] && $item['finish_time']){
                    $out_time = 0;
                    if($item['state']==2){
                        $max_complete_time_info = g('pkg_db')->select('mv_proc_workitem', 'max(complete_time) complete_time', ['formsetinit_id'=>$tmp_id], 1, 1);
                        $max_complete_time = !empty($max_complete_time_info[0]['complete_time'])?$max_complete_time_info[0]['complete_time']:time();
                        $out_time = $max_complete_time-$item['finish_time'];
                    }else{
                        $out_time = time() - $item['finish_time'];
                    }

                    if($out_time>0){
                        $timeout_desc = number_format($out_time/(3600*24),2);
                    }else{
                        $timeout_desc = '未超出';
                    }
                }
                $item['timeout_desc'] = $timeout_desc;

                unset($item['com_id']);
                unset($item['complete_date_status']);
                unset($item['finish_time']);
                unset($item['state']);
            };
        }

        return $data;
    }

    /**
     * 罗列申请实例数据
     * @return void
     *
    ■ 唯一标识 （控件inputkey md5）
    ■ 巡检类型
    ● 教育局巡检
    ● 学校上报
    ■ 检查日期
    ■ 检查学校
    ■ 记录检查人
    ■ 隐患类型 （隐患控件名称）
    ■ 隐患数量  (打叉的数量，若数量为0，类型不呈现)
     */
    public function listFormsetinstLabelForReport($page=0,$page_size=0){
        !$page && $page=1; !$page_size && $page_size=10;
        $table = ' mv_proc_formsetinst_label mpfl
		left join mv_proc_formsetinst mpf on mpf.id = mpfl.formsetinst_id
		left join mv_proc_form_model mpfm on mpfm.id=mpf.form_id ';

        $time_input_names = $this->reportFiledsMap('check_time');

        $cond = array(
            'mpf.info_state='=>1,
            'mpf.state!='=>0,
            'mpfl.info_state='=>1,
            'mpfm.info_state='=>1,
            'mpfl.type='=>'dangerous',
            'mpfl.val like '=>'%2%',
            '^( EXISTS(
                select id 
                from mv_proc_formsetinst_label 
                where formsetinst_id=mpf.id 
                and type="date" 
                and val!="" 
                and name in("'.implode('","',$time_input_names).'")
                )
             )'=>'',
        );

        $fields = 'mpfl.id,mpf.com_id,mpfm.type,mpfl.formsetinst_id,mpfl.`name`,mpfl.val';
        $order_by = ' mpf.id desc,mpfl.label_id asc ';
        $group_by='';
        $data = g('pkg_db')->select($table, $fields, $cond, $page, $page_size, $group_by, $order_by);

        if($data){
            $com_ids = array_column($data,'com_id');
            $companys = g('dao_com')->get_by_ids($com_ids,'id,name');
            $companys = array_column($companys,'name','id');
            foreach ($data as &$item){
                $tmp_type = $item['type'];
                $tmp_id = $item['formsetinst_id'];
                $tmp_val = json_decode($item['val'],true);
                !$tmp_val && $tmp_val = [];
                $array_count_Values = array_count_values($tmp_val);

                $item['id'] = md5($item['id']);
                $item['type'] = $tmp_type == Mv_Form::TYPE_RELATION?'教育局巡检':'学校上报';
                $item['check_school'] = isset($companys[$item['com_id']])?$companys[$item['com_id']]:' - ';
                $item['dangerous_type'] = $item['name'];
                $item['dangerous_num'] = isset($array_count_Values[2])?$array_count_Values[2]:0;
                //获取实例控件内容值
                $input_cond = array(
                    '__OR'=>array(
                        ['name IN'=>$this->reportFiledsMap('check_time'),'type='=>'date'],
                        ['name IN'=>$this->reportFiledsMap('check_school'),'type='=>'school'],
                        ['name IN'=>$this->reportFiledsMap('check_user'),'type='=>'people'],
                    ),
                );
                $inputs = g('mv_formsetinst')->get_form_vals($tmp_id,$input_cond,'name,val,type');
                if($tmp_type == Mv_Form::TYPE_RELATION){
                    $item['check_school'] = $this->reportInputValue('check_school',$inputs);
                }
                $item['check_time'] = $this->reportInputValue('check_time',$inputs);
                $item['check_user'] = $this->reportInputValue('check_user',$inputs);

                unset($item['com_id']);
                unset($item['formsetinst_id']);
                unset($item['val']);
                unset($item['name']);
            }unset($item);
        }

        return $data;
    }


    /**
     * 罗列申请实例数据
     * @return void
     * select mpf.id,mpf.com_id,mpfm.type from mv_proc_formsetinst mpf
    left join mv_proc_form_model mpfm on mpfm.id=mpf.form_id
    and EXISTS(
    select id from mv_proc_formsetinst_label where formsetinst_id=mpf.id and info_state=1 and type='dangerous'
    )
     *
    ■ 唯一标识 （id md5）
    ■ 巡检类型 (教育局巡检，学校上报)
    ● 教育局巡检(互联)
    ● 学校上报(共享+普通)
    ■ 检查日期 (时间控件，中文匹配)
    ■ 检查学校
    ● 互联表单 学校控件内容(中文匹配)
    ● 共享-普通表单 提交企业
    ■ 记录检查人 (人员控件，中文匹配)
    ■ 最终整改结果 (单选控件，中文匹配)

     */
    public function listFormsetinstForReport($page=0,$page_size=0){
        !$page && $page=1; !$page_size && $page_size=10;

        $time_input_names = $this->reportFiledsMap('check_time');

        $table = ' mv_proc_formsetinst mpf
    left join mv_proc_form_model mpfm on mpfm.id=mpf.form_id ';
        $cond = array(
            'mpf.info_state='=>1,
            'mpfm.info_state='=>1,
            'mpf.state!='=>0,
            '^(EXISTS( select id from mv_proc_formsetinst_label where formsetinst_id=mpf.id and info_state=1 and type="dangerous" ))' => '',
            '^( EXISTS(
                select id 
                from mv_proc_formsetinst_label 
                where formsetinst_id=mpf.id 
                and type="date" 
                and val!="" 
                and name in("'.implode('","',$time_input_names).'")
                )
             )'=>'',
        );

        $fields = 'mpf.id,mpf.com_id,mpfm.type';
        $order_by = ' mpf.id desc ';
        $group_by='';
        $data = g('pkg_db')->select($table, $fields, $cond, $page, $page_size, $group_by, $order_by);

        if($data){
            $com_ids = array_column($data,'com_id');
            $companys = g('dao_com')->get_by_ids($com_ids,'id,name');
            $companys = array_column($companys,'name','id');
            foreach ($data as &$item){
                $tmp_type = $item['type'];
                $tmp_id = $item['id'];
                $item['id'] = md5($item['id']);
                $item['type'] = $tmp_type == Mv_Form::TYPE_RELATION?'教育局巡检':'学校上报';
                $item['check_school'] = isset($companys[$item['com_id']])?$companys[$item['com_id']]:' - ';

                //获取实例控件内容值
                $input_cond = array(
                    '__OR'=>array(
                        ['name IN'=>$this->reportFiledsMap('check_time'),'type='=>'date'],
                        ['name IN'=>$this->reportFiledsMap('check_school'),'type='=>'school'],
                        ['name IN'=>$this->reportFiledsMap('check_user'),'type='=>'people'],
                        ['name IN'=>$this->reportFiledsMap('check_result'),'type='=>'radio'],
                    ),
                );
                $inputs = g('mv_formsetinst')->get_form_vals($tmp_id,$input_cond,'name,val,type');
                if($tmp_type == Mv_Form::TYPE_RELATION){
                    $item['check_school'] = $this->reportInputValue('check_school',$inputs);
                }
                $item['check_time'] = $this->reportInputValue('check_time',$inputs);
                $item['check_user'] = $this->reportInputValue('check_user',$inputs);
                $item['check_result'] = $this->reportInputValue('check_result',$inputs);

                unset($item['com_id']);
            }unset($item);
        }

        return $data;
    }

    /**
     * 获取控件数值
     * @param $key
     * @param $inputs
     * @return void
     */
    private function reportInputValue($key,$inputs=[]){
        $return = ' - ';
        $input_map = $inputs ? array_column($inputs,null,'name') :[];
        $names = $this->reportFiledsMap($key);
        foreach ($names as $name){
            $type = isset($input_map[$name]['type']) ?$input_map[$name]['type']:'';
            $val = isset($input_map[$name]['val']) ?$input_map[$name]['val']:'';

            switch ($type){
                case 'school':
                    $ids = json_decode($val,true);
                    $ids = $ids?$ids:[0];
                    $companys = g('dao_com')->get_by_ids($ids,'id,name');
                    $val = implode(';',array_column($companys,'name'));
                    break;
                case 'people':
                    $ids = json_decode($val,true);
                    $ids = $ids?$ids:[0];
                    $users = g('dao_user')->get_by_ids('',$ids,'id,name');
                    $val = implode(';',array_column($users,'name'));
                    break;
                default:
                    break;
            }

            if(trim($val)!==''){
                $return = $val;
            }
        }
        return $return;
    }

    /**
     * 获取字段映射
     * @param $key
     * @return array
     */
    private function reportFiledsMap($key){
        $return = [''];
        switch ($key){
            case 'check_school':
                $return[] = '检查学校';
                $return[] = '巡检学校';
                break;
            case 'check_time':
                $return[] = '检查日期';
                break;
            case 'check_user':
                $return[] = '记录检查人';
                $return[] = '检查人员';
                break;
            case 'check_result':
                $return[] = '最终整改结果';
                $return[] = '最后排查结果';
                break;
            case 'dangerous_desc':
                $return[] = '隐患描述';
                $return[] = '隐患整体描述';
                break;
            case 'dangerous_advice':
                $return[] = '整改意见';
                break;
        }

        return $return;
    }

    /**
     * 检测access_token
     * @param $access_token
     * @throws \Exception
     */
	public function checkAccessToken($access_token){
        $caller_info = g('pkg_redis')->get($access_token);
        if(empty($caller_info))
            throw new \Exception('无效access_token');

        $caller_info = json_decode($caller_info,true);
        if(empty($caller_info) || !is_array($caller_info))
            throw new \Exception('无效access_token');

        $caller_info = $this->checkCaller($caller_info['api_key'],$caller_info['api_secret']);

        //模拟登陆状态
        $com = g('dao_com')->get_by_id($caller_info['com_id']);
        if($com){
            $_SESSION[SESSION_VISIT_COM_ID] = $caller_info['com_id'];
            $_SESSION[SESSION_VISIT_DEPT_ID] = $com['dept_id'];
            $_SESSION[SESSION_VISIT_CORP_ID] = $com['corp_id'];
            $_SESSION[SESSION_VISIT_CORP_URL] = $com['corp_url'];
        }

        $user = g('dao_user') -> get_by_acct($caller_info['acct']);
        if($user){
            $_SESSION[SESSION_VISIT_USER_ID] = $user['id'];
            $_SESSION[SESSION_VISIT_USER_WXID] = $user['acct'];
            $_SESSION[SESSION_VISIT_USER_WXACCT] = $user['acct'];
            $_SESSION[SESSION_VISIT_USER_WXACCT] = $user['acct'];
            $_SESSION[SESSION_VISIT_USER_NAME] = $user['name'];
        }

        return $caller_info;
    }

    /**
     * 检查调用者信息
     * @param $api_key
     * @param $api_secret
     * @throws \Exception
     */
	private function checkCaller($api_key,$api_secret){
        //判断是否可以请求接口
        $caller_info = $this->dao_api_caller_info->getByKey($api_key);
        if(empty($caller_info))
            throw new \Exception('用户api_key不存在');

        if($api_secret!=$caller_info['api_secret'])
            throw new \Exception('用户api_secret错误');

        $ip = g('pkg_ip')->get_ip();
        if($caller_info['allow_ip'] != '*' && !in_array($ip,explode(';',$caller_info['allow_ip'])))
            throw new \Exception($ip.'不在访问白名单内');

        return $caller_info;
    }

    /**
     * 生成令牌
     * @param $api_key
     * @param $api_secret
     */
    public function setToken($api_key,$api_secret){

        $caller_info = $this->checkCaller($api_key,$api_secret);
        $timeout = empty($caller_info['timeout'])?7200:$caller_info['timeout'];
        $access_token = $this->getCacheKey(
            [time(),$api_key,$api_secret,$caller_info['id'],uniqid()]
        );

        $ret = g('pkg_redis')->set($access_token,json_encode($caller_info),$timeout);
        if(!$ret)
            throw new \Exception('生成失败');

        return array(
            'access_token'=>$access_token,
            'expires_in'=>$timeout,
        );
    }

    private function getCacheKey($arr){
        return md5(implode('-',$arr));
    }



}

//end