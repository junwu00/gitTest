<?php

/**
 * 开放接口逻辑调整
 * 
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\open\view;


class OpenIndex extends \model\api\view\AbsProc {

    private $server;
	
	//命令对应函数名的映射表
    private $cmd_map = array(
        //文件上传
        101 => 'upload',
        //发送公文
        102 => 'sendDoc',
    );

	public function __construct() {
        $this->server = new \model\open\server\ServerOpenIndex();

        $this->init();

        parent::__construct('process');
	}

	private function init(){
        try{
            to_log('Info','','请求地址:'.get_this_url());
            to_log('Info','','请求参数:'.json_encode($_REQUEST));
            $a = g('pkg_val') -> get_value('a', true);
            if($a=='ajax'){
                $access_token = g('pkg_val') -> get_value('access_token', true);
                $this->server->checkAccessToken($access_token);
            }
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function ajax() {
		try {
			$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
			if (empty($cmd)) throw new \Exception('命令参数错误');

			$map = $this -> cmd_map;
			if (!isset($map[$cmd])) throw new \Exception('命令参数错误');

			$this -> $map[$cmd]();
		} catch (\Exception $e) {
			parent::echo_err(\model\api\server\Response::$Busy, $e -> getMessage());
		}

	}

    /**
     * 获取接口令牌
     */
    public function getToken(){
        try{
            $api_key = g('pkg_val') -> get_get('api_key', true);
            $api_secret= g('pkg_val') -> get_get('api_secret', true);

            if(empty($api_key))
                throw new \Exception('api_key不能为空');

            if(empty($api_secret))
                throw new \Exception('api_secret不能为空');

            $data = $this->server->setToken($api_key,$api_secret);

            parent::echo_ok($data);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    //----------------------------学校上报报表------------------------------
    //上报 大表盘，隐患控件的申请实例
    public function listFormsetinstForReport(){
        try{
            $page = g('pkg_val') -> get_get('page', true);
            $page_size= g('pkg_val') -> get_get('page_size', true);

            $data = $this->server->listFormsetinstForReport($page,$page_size);

            parent::echo_ok(['data'=>$data]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }
    //上报 隐患统计，隐患控件列表
    public function listFormsetinstLabelForReport(){
        try{
            $page = g('pkg_val') -> get_get('page', true);
            $page_size= g('pkg_val') -> get_get('page_size', true);

            $data = $this->server->listFormsetinstLabelForReport($page,$page_size);

            parent::echo_ok(['data'=>$data]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    //上报 隐患统计，隐患控件列表
    public function listFormsetinstDetailForReport(){
        try{
            $page = g('pkg_val') -> get_get('page', true);
            $page_size= g('pkg_val') -> get_get('page_size', true);

            $data = $this->server->listFormsetinstDetailForReport($page,$page_size);

            parent::echo_ok(['data'=>$data]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

// --------------------------接口内部实现函数------------------------------------------------
    private function upload(){
        try{
            $file = array_shift($_FILES);
            if (empty($file) or !is_array($file)) {
                $tip_str = '找不到该上传文件，请重新上传！';
                throw new \Exception($tip_str);
            }

            //线上使用
            $data = g('api_media') -> cloud_upload('', $file);

            $data['file_name'] = $file['name'];
            $data['file_ext'] = $data['ext'];
            $data['real_path'] = MEDIA_URL_PREFFIX . $data['path'];
            $data['image_path'] = MEDIA_URL_PREFFIX . $data['path'];
            parent::echo_ok(array(
                'data'=>$data
            ));
        }catch (\Exception $e){
            parent::echo_exp($e);
        }

    }

    private function sendDoc(){
        try{
            $begin_trans = 0;
            $level = g('pkg_val') -> get_post('level', true);
            $type = g('pkg_val') -> get_post('type', true);
            $title = g('pkg_val') -> get_post('title', true);
            $release_time = g('pkg_val') -> get_post('release_time', true);
            $release_people = g('pkg_val') -> get_post('release_people', true);
            $content_files = g('pkg_val') -> get_post('content_files', true);
            $attach_files = g('pkg_val') -> get_post('attach_files', true);

            if( $level===''
                || $type===''
                || $title===''
                || $release_time===''
                || $release_people==='' ){
                throw new \Exception('请求参数不能为空字符串');
            }

            //通过配置获取表单模板内容
            $form_conf = load_config('model/open/form');
            $form_conf = array_pop($form_conf);

            //获取表单模板详情
            $proc = $this -> get_form_by_id($form_conf['form_history_id'], $this -> com_id, $this -> user_id);
            $is_special = 0;

            //获取表单实例名称
            $tmp_formset_name = [];
            $formset_name = parent::get_formset_by_num($proc, $is_special, $proc['app_version']);
            if($formset_name){
                foreach ($formset_name as $key=>$value){
                    $tmp_formset_name[] = !empty(${$form_conf['input_param_map'][$key]})?${$form_conf['input_param_map'][$key]}:$value;
                }unset($value);
            }
            $formset_name = implode('-',$tmp_formset_name);


            //组装表单数据
            $form_vals = array();
            $inputs = json_decode($proc['inputs'],true);
            if($inputs){
                foreach ($inputs as $key=>$item){
                    $item['visit_input'] = 1;
                    $item['edit_input'] = 1;

                    if($item['type']=='text' || $item['type']=='textarea' || $item['type']=='file'){
                        $val = ${$form_conf['input_param_map'][$key]};
                        $item['val'] = is_array($val)?json_encode($val,JSON_UNESCAPED_UNICODE):$val;
                    }

                    $form_vals[$key] = $item;
                }
            }

            $data = array(
                'form_id'=>$proc['id'],
                'form_name'=>$formset_name,
                'work_id'=>$proc['work_id'],
                'form_vals'=>$form_vals,
                'app_version'=>$proc['app_version'],
                'is_other_proc'=>$proc['is_other_proc'],
            );
            //开始生成实例,保存草稿
            g('pkg_db') -> begin_trans();
            $begin_trans = 1;
            $workitem_info = parent::save_workitem_parent($data);

            //获取下一个接收节点
            $next_nodes = $this->get_next_node($workitem_info['workitem_id']);
            if(empty($next_nodes))
                throw new \Exception('找不到下一个节点');

            //发送给下一步接收人
            $receiver_arr = [];
            foreach ($next_nodes['node_list'][0]['receive_array'] as $item){
                $receiver_arr[] = array(
                    'receiver_id'=>$item['id'],
                    'receiver_name'=>$item['name'],
                );
            }
            $data = array(
                'workitem_id'=>$next_nodes['workitem_id'],
                'work_nodes'=>array(
                    array(
                        'id'=>$next_nodes['node_list'][0]['id'],
                        'receiver_arr'=>$receiver_arr,
                    ),
                ),
            );

            parent::send_workitem_parent($data);

            g('pkg_db') -> commit();

            parent::echo_ok();
        }catch (\Exception $e){
            $begin_trans && g('pkg_db') -> rollback();
            parent::echo_exp($e);
        }
    }

    private function get_next_node($workitem_id){
        $next_node_id_arr = parent::get_next_workitem($workitem_id);
        $is_send = parent::is_send($next_node_id_arr);

        $fields = array('id', 'work_node_name', 'receive_array', 'select_array', 'is_end');
        $info = array(
            'workitem_id' 	=> (string)$workitem_id,
            'is_send' 		=> $is_send === "TRUE" ? TRUE : FALSE,
            'node_list' 	=> array(),
        );

        foreach ($next_node_id_arr as $val) {
            $return = array(
                'id' 				=> 0,
                'work_node_name' 	=> '',
                'is_end' 			=> FALSE,
                'select_array' 		=> array(),
                'receive_array' 	=> array(),
            );
            foreach ($val as $key => $node) {
                if (in_array($key, $fields)) {
                    if ($key == 'select_array') {
                        foreach ($node as $key1 => $u) {
                            $tmp_data = array();
                            $tmp_data['id'] = $u['id'];
                            $tmp_data['name'] = $u['name'];
                            $tmp_data['pic_url'] = $u['pic_url'];
                            $return[$key][] = $tmp_data;
                        }unset($u);
                        unset($tmp_data);
                    } elseif ($key == 'receive_array') {
                        foreach ($node as $key2 => $u1) {
                            $tmp_data = array();
                            $tmp_data['id'] = $u1['id'];
                            $tmp_data['name'] = $u1['name'];
                            $tmp_data['pic_url'] = $u1['pic_url'];
                            $return[$key][] = $tmp_data;
                        }unset($u1);
                        unset($tmp_data);
                    } else {
                        $return[$key] = $node;
                    }
                }
            }unset($node);
            $info['node_list'][] = $return;
        }unset($val);

        return $info;
    }

    /**
     * 获取表单详情
     * @param  [type] $form_id
     * @param  [type] $com_id
     * @param  [type] $user_id
     * @return [type]
     */
    private function get_form_by_id($form_id, $com_id, $user_id) {
        $fields = array(
            'id', 'form_name', 'form_describe', 'scope', 'inputs',
            'form_history_id', 'version', 'app_version',
            'classify_id', 'work_id', 'is_other_proc', 'is_linksign',
            'formsetinst_name', 'groups'
        );
        $fields = implode(',', $fields);
        $proc = g('mv_form') -> get_by_id($form_id, $com_id, $fields);
        if (!$proc) throw new \Exception('该模板不存在！');

        //获取最新版本的流程
        if ($proc['version'] == self::$StateHistory) {
            $proc = g('mv_form') -> get_by_his_id($proc['form_history_id'], $com_id, $fields);
            if (!$proc) throw new \Exception('该模板不存在！');
        }

        //获取表单控件
        $inputs = g('mv_form') -> get_inputs_by_form($proc['id']);
        $proc['inputs'] = json_encode($inputs);

        $scope = json_decode($proc['scope'], TRUE);
        if (count($scope) == 0) {
            $proc['scope'] = array();
        } else {
            $proc['scope'] = g('dao_dept') -> list_by_ids($scope);
        }

        $user = g('dao_user') -> get_by_id($user_id);

        // 判断是否有权限发起该流程
        $dept_arr = $proc['scope'];
        $is_apply = FALSE;
        foreach ($dept_arr as $dept) {
            if (strstr($user['dept_tree'], '"'.$dept["id"].'"')) {
                $is_apply = TRUE;
                break;
            }
        }unset($dept);
        // 判断分组权限是否有权限发起该流程
        if (!$is_apply) {
            $user_group = g('dao_group') -> get_by_user_id($com_id, $user_id);
            $group_ids = array_column($user_group, 'id');
            $groups = $proc['groups'] === NULL ? array() : json_decode($proc['groups'], TRUE);
            $same = array_intersect($group_ids, $groups);
            !empty($same) && $is_apply = TRUE;
        }

        if (!$is_apply) throw new \Exception('没有权限！');

        return $proc;
    }

}

//end of file