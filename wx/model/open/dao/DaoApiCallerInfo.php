<?php
/**
 * 组别公共服务类
 * @author chenyihao
 * @date 2016-06-24
 *
 */
namespace model\open\dao;

class DaoApiCallerInfo extends \model\api\dao\DaoBase {

	/** 对应的库表名称 */
	private static $Table = 'api_caller_info';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	public function __construct(){
		parent::__construct();
		$this -> set_table(self::$Table);
	}

	public function getByKey($key){
	    $cond = array(
	        'api_key='=>$key,
            'status='=>1,
        );
	    $ret = $this->get_by_cond($cond);

	    return $ret ? $ret:[];
    }



//================================内部实现============================


}

// end of file