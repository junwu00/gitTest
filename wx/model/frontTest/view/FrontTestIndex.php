<?php

namespace model\frontTest\view;

class FrontTestIndex extends Base {
	
	public function __construct($log_pre_str='') {
		parent::__construct($log_pre_str);
	}
	
	public function index() {
		g('pkg_smarty') -> show($this -> view_dir . 'page/index.html');
	}
	
	//通用UI页面
	public function uiDemo() {
		$title = '通用UI页面';
		g('pkg_smarty') -> assign('title', $title);
		g('pkg_smarty') -> show($this -> view_dir . 'page/commonUI_Demo.html');
	}
}

//end