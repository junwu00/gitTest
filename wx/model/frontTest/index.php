<?php
/**
 * 模块单入口文件
 */

load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件
load_config('model/api/errcode');

g('pkg_rsession') -> start();											//引入公用的类映射配置文件

//start 支持URL使用test参数绕过微信身份验证,上线时删除对应debug文件即可
if (isset($debug_var)) {
	$debug_session_file = (dirname(__DIR__)) . DS . 'debug.php';
	file_exists($debug_session_file) && include $debug_session_file; 
}
//END 支持URL使用test参数绕过微信身份验证

//加载必要的配置文件
$mod_dir = dirname(__FILE__);												//模块的根路径
$mod_conf = load_config('model/frontTest/model');								//模块信息配置文件
load_config('model/frontTest/classmap');										//引入模块内类映射配置文件

//登录验证
g('api_login') -> check_auth();

//当前模块内逻辑
$mod = g('pkg_val') -> get_get('m');
$act = g('pkg_val') -> get_get('a');
empty($mod) && $mod = 'index';
empty($act) && $act = 'index';


$cls_name = ucwords($mod_conf['name']).ucwords($mod);
$mod_file = $mod_dir . DS . 'view' . DS . $cls_name . '.php';
$mod_cls = "\\model\\{$mod_conf['name']}\\view\\{$cls_name}";

!file_exists($mod_file) && g('view_notice') -> error('页面不存在');

include_once ($mod_file);
if (!file_exists($mod_file) || !class_exists($mod_cls, FALSE) || !method_exists($mod_cls, $act)) {
	g('view_notice') -> error('页面不存在');
}

g('view_' . $mod) -> $act();	//处理业务逻辑

// end of file