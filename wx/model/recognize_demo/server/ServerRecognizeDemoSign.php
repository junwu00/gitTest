<?php 
/**
 * 签名验证相关
 * @author yangpz
 * @create 2016-12-09
 */

namespace model\recognize_demo\server;

class ServerRecognizeDemoSign extends \model\api\server\ServerBase {

	public function __construct($log_pre_str='') {
        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);
    }

    /**
     * 验证指纹签名
     * @param $json 指纹信息
     * @param $sign 签名信息
     */
    public function verify_soter_sign($json, $sign) {
        $access_token = g('api_atoken') -> get_by_com($_SESSION[SESSION_VISIT_COM_ID]);
        $auth_info = g('wxqy_auth') -> convert_to_openid($access_token, $_SESSION[SESSION_VISIT_USER_WXACCT]);
        $openid = $auth_info['openid'];

        $check_data = array(
            'openid'    => $openid,
            'json'      => $json,
            'sign'      => $sign,
        );
        return g('wxqy_soter') -> verify_sign($access_token, $check_data);
    }

}
//end of file