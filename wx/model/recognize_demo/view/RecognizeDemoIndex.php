<?php

namespace model\recognize_demo\view;

class RecognizeDemoIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('recognize_demo',$log_pre_str);
	}
	
	public function index() {
        g('api_jssdk') -> assign_jssdk_sign();
		$this -> show($this -> view_dir . 'page/index.html');
	}

    /** 验证指纹签名 */
	public function verify_sign() {
        try {
            $this -> log_d('开始验证指纹签名');
            $need = array('json', 'sign');
            $data = $this -> get_post_data($need);
            $ret = g('ser_sign') -> verify_soter_sign($data['json'], $data['sign']);
            $this -> echo_ok(array('info' => $ret));

        } catch (\Exception $e) {
            $this -> echo_exp($e);
        }
    }

}

//end