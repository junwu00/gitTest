<?php

/**
 * 工会逻辑处理接口
 *
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\union\view;

use Exception;
use model\api\view\ViewBase;
use model\union\dao\ElectronicCoilDetailDao;
use model\union\server\ElectronicServer;

class UnionElectronic extends ViewBase
{
    /** 当前访问用户的ID */
    protected $user_id = 0;
    protected $com_id = 0;

    private $server;
    public function __construct()
    {
        parent::__construct('union');
        $this->server = new ElectronicServer();
    }

    private $cmdMethod = [
        100 => 'getCouponCollectDetail',
        101 => 'getTaskDetail',
        102 => 'confirmCollar',
        103 => 'confirmWriteOff',
        104 => 'getDetailList',
    ];

    /**
     * 方法入口
     * ModifyUser: lyc
     * ModifyDate: 2023/6/14 01:33
     */
    public function index()
    {
        try {
            $cmd = g('pkg_val')->get_get('cmd');
            if (!isset($this->cmdMethod[$cmd])) {
                throw new \Exception("路由错误！");
            }
            $method = $this->cmdMethod[$cmd];
            $data = $this->$method();
            parent::echo_ok($data);
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }


    /**
     * 确认核销卡券
     * @return array
     * @throws Exception
     */
    private function confirmWriteOff()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id']);
        $post['user_id'] = $this->user_id;
        $data = $this->server->confirmWriteOff($post);
        return [
            'data' => $data
        ];
    }


    /**
     * 获取卡券详情
     * @return array
     * @throws Exception
     */
    private function getCouponCollectDetail()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id']);
        $data = $this->server->getCouponCollectDetail($post['id'],$this->user_id);
        return [
            'data' => $data
        ];
    }

    /**
     * 获取任务详情
     * @return array
     * @throws Exception
     */
    private function getTaskDetail(){
        $post = parent::get_post_data();
        $this->checkParam($post, ['task_id']);
        $data = $this->server->getTaskDetail($post['task_id']);
        return [
            'data' => $data
        ];
    }

    /**
     * 确认领卷
     * @return array
     * @throws Exception
     */
    private function confirmCollar(){
        $post = parent::get_post_data();
        $this->checkParam($post, ['id','template_id']);
        $data = $this->server->confirmCollar($this->user_id,$post['id'],$post['template_id']);
        return [
            'data' => $data
        ];
    }

    /**
     * 获取卡卷列表
     * @return void
     */
    private function getDetailList(){
        $post = parent::get_post_data();
        $this->checkParam($post, ['state']);
        if (!is_array($post['state'])) {
            $post['state'] = [$post['state']];
        }
        $check = array_diff($post['state'], [
            ElectronicCoilDetailDao::STATE_GRANT,
            ElectronicCoilDetailDao::STATE_WRITE_OFF,
            ElectronicCoilDetailDao::STATE_EXPIRE
        ]);
        if ($check) {
            throw new \Exception("参数state错误！");
        }
        $post['user_id'] = $this->user_id;
        $data = $this->server->getCouponList($post);
        return [
            'data' => $data
        ];
    }

    /**
     * 参数校验
     * @param $data
     * @param $need
     * @throws Exception
     */
    private function checkParam($data, $need)
    {
        foreach ($need as $value) {
            if (!isset($data[$value])) {
                throw new \Exception("缺少参数：{$value}");
            }
        }
    }




}

//end of file