<?php

/**
 * 工会逻辑处理接口
 *
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\union\view;

use Exception;
use model\api\dao\User;
use model\api\server\AToken;
use model\api\server\Jssdk;
use model\api\server\Nmsg;
use model\api\server\Response;
use model\api\view\ViewBase;
use model\union\server\ServerUnion;
use package\weixin\qy\WXQYMedia;

class UnionAjax extends ViewBase
{
    public function __construct()
    {
        parent::__construct('union');
    }

    public function userInfo()
    {
        parent::echo_ok(['data' => (new ServerUnion())->userInfo()]);
    }

    /** 入会申请详情 */
    public function applyDetail()
    {
        try {
            $post = parent::get_post_data(['id']);
            $data = (new ServerUnion())->applyDetail($post);
            parent::echo_ok(['data' => $data]);
        } catch (Exception $e) {
            parent::echo_exp($e);
        }
    }

    /** 入会申请 */
    public function applyMembership()
    {
        try {
            $need = [
                'id', 'name', 'gender', 'photo_url', 'education', 'technical_title', 'political', 'ethnic', 'native_place',
                'start_working_date', 'born_twelve_animals', 'department_id', 'employee_number', 'hobby', 'strong_point',
                'work_unit', 'id_card', 'mobile', 'main_family', 'resume', 'elec_signature_url', 'sign_date',
            ];
            $post = parent::get_post_data($need);

            $err = (new ServerUnion())->saveApplyMembership($post);
            if ($err->isEmpty()) {
                $this->echo_ok();
            } else {
                $this->echo_err(Response::$Busy, $err->generateErrorMsg());
            }
        } catch (Exception $e) {
            parent::throw_exp($e->getMessage());
        }
    }

    /** 初始化jssdk */
    public function assignJssdkSign()
    {
        $post = parent::get_post_data(['url']);
        $this->echo_ok([
            'data' => (new Jssdk())->assign_jssdk_sign2($post['url'])
        ]);
    }

    /** 推送会员文档 */
    public function pushPdfMemberFiles()
    {
        $post = parent::get_post_data(['url']);
        if (empty($post['url'])) {
            $this->echo_exp(new Exception("获取文件失败"));
        }
        $post['url'] = (new ServerUnion())->replaceUrl($post['url']);
        // 获取用户名
        $user = (new User())->getById($this->user_id, 'name');
        if (empty($user['name'])) {
            $this->echo_exp(new Exception("找不到用户"));
        }

        // 获取token
        $tokenObj = new AToken();
        $tokenObj->get_by_com($this->com_id, $this->app_id);
        $token = $tokenObj->get();
        if (empty($token)) {
            $this->echo_exp(new Exception("推送失败"));
        }

        // 发送文件到wx素材库
        try {
            $file = dirname(__FILE__) . "/{$this->user_id}.pdf";
            $this->forceDownload($file, $post['url']);
            $wxqy = new WXQYMedia();
            $wxFile = $wxqy->upload_media($token, 'file', $file, $user['name'] . '.pdf');
            if (empty($wxFile['media_id'])) {
                $this->echo_exp(new Exception("获取推送文件失败:" . $wxFile['media_id']));
            }
        } catch (Exception $e) {
        } finally {
            !empty($file) && is_file($file) && unlink($file);
        }

        // 根据获取到的 media_id 推送文件到用户端
        if (empty($wxFile['media_id'])) {
            $this->echo_exp(new Exception("异常"));
        } else {
            $nmsg = new Nmsg();
            $nmsg->init($this->com_id, 'union');
            if (!$nmsg->send_files('file', $wxFile['media_id'], [], [$this->user_id])) {
                $this->echo_exp(new Exception("推送文件失败:" . $wxFile['media_id']));
            }
        }

        $this->echo_ok([]);
    }

// --------------------------接口内部实现函数------------------------------------------------

    private function forceDownload($localPath, $filename)
    {
        file_put_contents($localPath, file_get_contents($filename));
    }
}

//end of file