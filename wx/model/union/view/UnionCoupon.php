<?php

/**
 * 工会逻辑处理接口
 *
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\union\view;

use Exception;
use model\api\view\ViewBase;
use model\union\dao\CouponCollectDetailDao;
use model\union\server\CouponServer;

class UnionCoupon extends ViewBase
{
    /** 当前访问用户的ID */
    protected $user_id = 0;
    protected $com_id = 0;

    /** @var CouponServer */
    private $server;
    public function __construct()
    {
        parent::__construct('union');
        $this->server = new CouponServer();
    }

    private $cmdMethod = [
        100 => 'getCouponCollectDetail',
        101 => 'saveCouponCollectDetail',
        102 => 'getCouponList',
        103 => 'confirmWriteOff',
        104 => 'getCouponShop',
        105 => 'saveCouponFile',
        106 => 'checkMemberShip',
        107 => 'getCurrentCoupon',
    ];

    /**
     * 方法入口
     * ModifyUser: lyc
     * ModifyDate: 2023/6/14 01:33
     */
    public function index()
    {
        try {
            $cmd = g('pkg_val')->get_get('cmd');
            if (!isset($this->cmdMethod[$cmd])) {
                throw new \Exception("路由错误！");
            }
            $method = $this->cmdMethod[$cmd];
            $data = $this->$method();
            parent::echo_ok($data);
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /**
     * 获取用户当前卡券
     * @return array
     */
    private function getCurrentCoupon()
    {
        $data = $this->server->getCurrentCoupon($this->user_id);
        return [
            'data' => $data
        ];
    }

    /**
     * 检验是否会员
     * @return array
     * @throws Exception
     */
    private function checkMemberShip()
    {
        $data = $this->server->checkMemberShip($this->user_id);
        return [
            'data' => $data
        ];
    }

    /**
     * 确认核销卡券
     * @return array
     * @throws Exception
     */
    private function confirmWriteOff()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id']);
        $post['user_id'] = $this->user_id;
        $data = $this->server->confirmWriteOff($post);
        return [
            'data' => $data
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getCouponList()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['state']);
        if (!is_array($post['state'])) {
            $post['state'] = [$post['state']];
        }
        $check = array_diff($post['state'], [
            CouponCollectDetailDao::STATE_GRANT,
            CouponCollectDetailDao::STATE_WRITE_OFF,
            CouponCollectDetailDao::STATE_EXPIRE
        ]);
        if ($check) {
            throw new \Exception("参数state错误！");
        }
        $post['user_id'] = $this->user_id;
        $data = $this->server->getCouponList($post);
        return [
            'data' => $data
        ];
    }

    /**
     * 获取卡券详情
     * @return array
     * @throws Exception
     */
    private function getCouponCollectDetail()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id']);
        $data = $this->server->getCouponCollectDetail($post['id']);
        return [
            'data' => $data
        ];
    }

    /**
     * 提交卡券申请
     * @return array
     * @throws Exception
     */
    private function saveCouponCollectDetail()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id', 'shop_id']);
        $post['user_id'] = $this->user_id;
        $data = $this->server->saveCouponCollectDetail($post);
        return [
            'data' => $data
        ];
    }

    /**
     * 获取卡券店铺
     * @return array
     */
    private function getCouponShop()
    {
        $params = ['com_id' => $this->com_id];
        $data = $this->server->getCouponShop($params);
        return [
            'data' => $data
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    private function saveCouponFile()
    {
        $post = parent::get_post_data();
        $this->checkParam($post, ['id', 'files']);
        if (!$post || !is_array($post['files'])) {
            throw new \Exception("参数files错误！");
        }
        $data = $this->server->saveCouponFile($post);
        return [
            'data' => $data
        ];
    }

    /**
     * 参数校验
     * @param $data
     * @param $need
     * @throws Exception
     */
    private function checkParam($data, $need)
    {
        foreach ($need as $value) {
            if (!isset($data[$value])) {
                throw new \Exception("缺少参数：{$value}");
            }
        }
    }
}

//end of file