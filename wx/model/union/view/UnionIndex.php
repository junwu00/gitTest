<?php

/**
 * 工资条
 * @author yangpz
 * @date 2016-08-12
 */

namespace model\union\view;

class UnionIndex extends \model\api\view\ViewBase {

	public function __construct($log_pre_str='') {
		parent::__construct('union',$log_pre_str);
	}

	/** 列表 */
	public function index() {
		$this -> show($this -> view_dir . 'page/demo.html');
	}

    /** 列表 */
    public function home() {
        header("Location:/wx/view/union/dist/index.html");
        //$this -> show($this -> view_dir . 'page/demo.html');
    }


    /** 列表 */
    public function coupon() {
        header("Location:/wx/view/union/dist/index.html#/coupon");
        //$this -> show($this -> view_dir . 'page/demo.html');
    }

    /** 列表 */
    public function couponApply() {
        header("Location:/wx/view/union/dist/index.html#/couponApply");
        //$this -> show($this -> view_dir . 'page/demo.html');
    }

    //后端跳转到新地址
    public function couponDetail(){
        $id = $_GET['id'];
        header('location:/wx/view/union/dist/index.html#/couponDetail?id='.$id.'&listType=0');
    }

    public function couponChoose(){
        $id = $_GET['id'];
        header('location:/wx/view/union/dist/index.html#/couponChoose?id='.$id);
    }
}

//end