<?php
/**
 * 工资条业务逻辑处理
 *
 * @author gch
 * @create 2017-09-28 15:05:24
 */

namespace model\union\server;

use Exception;
use model\api\server\ServerBase;
use model\union\common\Error;
use model\union\dao\DaoUnionMembership;
use model\union\dao\DaoUnionMembershipApply;
use package\cache\Redis;
use package\crypto\Des;

class ServerUnion extends ServerBase
{
    /** @var Des */
    private $des;

    /** @var string 入会后可否修改配置 */
    private $keyCanMemberSubmitModify = "keyCanMemberSubmitModify";

    /** @var Redis redis */
    private $redis;

    public function __construct($log_pre_str = '')
    {
        $this->des = new Des();

        $this->redis = g('pkg_redis');

        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);
    }

    public function replaceUrl($url){
        $image_url = MAIN_DYNAMIC_DOMAIN.'/index.php?model=index&m=index&a=showImage&image=';
        //将图片地址进行解码
        if(strpos($url,$image_url)!==false){
            return $this->des->decode(str_replace($image_url,'',$url));
        }
        return $url;
    }

    public function saveApplyMembership(array $post)
    {


        $err = new Error();
        try {
            $uid = $this->user_id;

            //将图片地址进行解码
            $post['elec_signature_url'] = $this->replaceUrl($post['elec_signature_url']);
            $post['photo_url'] = $this->replaceUrl($post['photo_url']);

            $saveData = $post;
            $saveData['user_id'] = $uid;

            // 尝试获取当前用户的入会申请
            $dao = new DaoUnionMembershipApply();

            g('pkg_db')->begin_trans();
            $everApplyDTO = $dao->list_by_cond(['info_state=' => 1, 'user_id=' => $uid], 'id, audit_status', 0, 0, 'create_time desc');
            if (count($everApplyDTO) > 0) {
                $everApply = $everApplyDTO[0];
                switch ($everApply['audit_status']) {
                    case DaoUnionMembershipApply::$Audit_Status_Wait:
                        throw new Exception("申请审核中，不可再次提交");
                }
            }

            // 新增申请数据
            unset($saveData['id']);
            $saveData['main_family'] = json_encode($post['main_family']);
            $saveData['apply_time'] = time();
            $saveData['create_time'] = time();
            $saveData['update_time'] = time();
            $saveData['name'] = $this->des->encode($saveData['name']);
            $saveData['mobile'] = $this->des->encode($saveData['mobile']);
            $saveData['id_card'] = $this->des->encode($saveData['id_card']);
            $saveData['is_newest'] = 1;
            $saveData['info_state'] = 1;

            $ok = $dao->insert($saveData, [], false);

            // 旧记录设置 is_newest = 0
            $ids = array_column($everApplyDTO, 'id');
            if ($ok && count($ids) > 0) {
                $ok = $dao->update_by_cond(
                    ['id IN' => array_column($everApplyDTO, 'id')],
                    ['is_newest' => 0, 'update_time' => time()],
                    [],
                    false
                );
            }

            if ($ok === false) {
                throw new Exception("保存数据失败");
            }
            g('pkg_db')->commit();
            return $err;
        } catch (Exception $e) {
            g('pkg_db')->rollback();
            return $err->add($e->getMessage());
        }
    }

    public function applyDetail(array $post)
    {
        $image_url = MAIN_DYNAMIC_DOMAIN.'/index.php?model=index&m=index&a=showImage&image=';
        $apply = (new DaoUnionMembershipApply())->base_get_by_cond(['id=' => $post['id']]);
        if ($apply) {
            $dept = g('dao_dept')->get_by_id($apply['department_id'], 'name');
            $apply['department_name'] = isset($dept['name']) ? $dept['name'] : '';
            $apply['main_family'] = json_decode($apply['main_family'], true);
            $apply['name'] = $this->des->decode($apply['name']);
            $apply['mobile'] = $this->des->decode($apply['mobile']);
            $apply['id_card'] = $this->des->decode($apply['id_card']);

            $member = (new DaoUnionMembership())->base_get_by_cond(['apply_id=' => $post['id']]);
            $apply['file_url'] = empty($member['file_url']) ? '' : $image_url.urlencode($this->des->encode($member['file_url']));

            $apply['photo_url'] = $apply['photo_url']?$image_url.urlencode($this->des->encode($apply['photo_url'])):'';
            $apply['elec_signature_url'] = $apply['elec_signature_url']?$image_url.urlencode($this->des->encode($apply['elec_signature_url'])):'';
        }
        return $apply;
    }

    public function userInfo()
    {
        $uid = $this->user_id;

        // 入会状态
        $member = (new DaoUnionMembership())->base_get_by_cond(['user_id =' => $uid], 'member_status, apply_id');

        // 申请
        $newestApply = (new DaoUnionMembershipApply())->base_get_by_cond(['is_newest=' => 1, 'user_id=' => $uid], 'id');
        $newestApply = $newestApply ? $newestApply : [];

        if (empty($member['member_status']) || $member['member_status'] == DaoUnionMembership::$MEMBER_STATUS_ON) {
            $applyId = isset($newestApply['id']) ? $newestApply['id'] : 0;
        } else {
            $applyId = $member['apply_id'];
        }

        // 用户个人信息
        $user = g('dao_user')->get_by_id($uid, 'acct, dept_list, dept_tree');
        $deptList = json_decode($user['dept_list']);
        $mainDeptId = (is_array($deptList) && !empty($deptList[0])) ? $deptList[0] : 0;

        // 用户主部门
        $dept = $mainDeptId ? g('dao_dept')->get_by_id($mainDeptId, 'name') : [];

        // 是否退休
        $retireDept = g('dao_dept')->list_by_cond(['name=' => '已退休', 'state=' => 1], 'id');
        $retireDeptIds = empty($retireDept) ? [] : array_column($retireDept, 'id');

        $isRetireUser = false;
        if (!empty($retireDeptIds)) {
            foreach ($retireDeptIds as $id) {
                if (!empty($user['dept_tree']) && strstr($user['dept_tree'], '"' . $id . '"') !== false) {
                    $isRetireUser = TRUE;
                }
            }
        }

        return [
            'user_id' => $uid,
            'name' => $_SESSION[SESSION_VISIT_USER_NAME],
            'dept_id' => $mainDeptId,
            'dept_name' => isset($dept['name']) ? $dept['name'] : '',
            'apply_id' => $applyId,
            'member_status' => isset($member['member_status']) ? $member['member_status'] : 0,
            'employee_number' => isset($user['acct']) ? $user['acct'] : '',
            'is_retire_user' => $isRetireUser,
            'can_member_modify' => $this->getCanModify(),
        ];
    }

    public function pushPdfOfMemberFiles()
    {

    }

    /**
     * 获取入会后可否修改配置
     * @return int
     * ModifyUser: hwp
     * ModifyDate: 2023/7/12 16:48
     */
    private function getCanModify()
    {
        $d = $this->redis->get($this->keyCanMemberSubmitModify);
        if ($d === false) {
            return 0;
        }
        return intval($d);
    }
}
//end of file