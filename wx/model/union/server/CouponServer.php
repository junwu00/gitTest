<?php
/**
 * 卡券服务
 * @author lyc
 * @create 2023-6-16 22:33:26
 */

namespace model\union\server;

use Exception;
use model\api\dao\User;
use model\api\server\ServerBase;
use model\union\dao\CouponCollectDetailDao;
use model\union\dao\CouponShopDao;
use model\union\dao\CouponTaskDao;
use model\union\dao\CouponTemplateDao;
use model\union\dao\CouponUserLogDao;
use model\union\dao\DaoUnionMembership;
use package\db\pdo\Mysql;

class CouponServer extends ServerBase
{
    private $daoCouponShop;
    private $daoCouponCollectDetail;
    private $daoCouponTask;
    private $daoUnionMembership;
    private $daoCouponUserLog;
    private $daoCouponTemplate;
    private $daoUser;
    /** @var Mysql */
    private $db;

    public function __construct($log_pre_str = '')
    {
        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);

        $this->daoCouponShop = new CouponShopDao();
        $this->daoCouponCollectDetail = new CouponCollectDetailDao();
        $this->daoCouponTask = new CouponTaskDao();
        $this->daoCouponUserLog = new CouponUserLogDao();
        $this->daoUnionMembership = new DaoUnionMembership();
        $this->daoCouponTemplate = new CouponTemplateDao();
        $this->daoUser = new User();
        $this->db = new Mysql();
    }

    /**
     * 获取用户当前卡券
     * @param $userId
     * @return array
     */
    public function getCurrentCoupon($userId)
    {
        // 获取当前年和下一年任务ID
        $curYear = date('Y');
        $task = $this->daoCouponTask->base_gets_by_cond([
            'year IN' => [$curYear, ($curYear + 1)]
        ], 'id, year');
        $taskYear = array_column($task, 'year', 'id');
        $taskIds = array_column($task, 'id');
        $coupon = $this->daoCouponCollectDetail->base_gets_by_cond([
            'user_id=' => $userId,
            'task_id IN' => $taskIds,
        ]);
        // 姓名
        $user = $this->daoUser->getById($userId, 'name');
        $userName = $user ? $user['name'] : '';
        foreach ($coupon as &$value) {
            $value['year'] = $taskYear[$value['task_id']];
            $value['name'] = $userName;
        }
        $years = array_column($coupon, 'year');
        array_multisort($years, SORT_ASC, $coupon);
        return $coupon;
    }

    /**
     * 检验是否是会员
     * @param $userId
     * @throws Exception
     * @return bool
     */
    public function checkMemberShip($userId)
    {
        $membership = $this->daoUnionMembership->base_get_by_cond([
            'user_id=' => $userId,
            'member_status=' =>DaoUnionMembership::$MEMBER_STATUS_ON
        ]);
        return $membership ? 1 : 0;

    }

    /**
     * 确认核销
     * @param $params
     * @return int
     * @throws Exception
     */
    public function confirmWriteOff($params)
    {
        $detail = $this->daoCouponCollectDetail->base_get($params['id']);
        if (!$detail) {
            throw new \Exception("卡券不存在！");
        }
        if ($detail['user_id'] != $params['user_id']) {
            throw new \Exception("无权限！");
        }
        if ($detail['state'] == CouponCollectDetailDao::STATE_WRITE_OFF) {
            throw new \Exception("已核销！");
        }
        if ($detail['state'] != CouponCollectDetailDao::STATE_GRANT) {
            throw new \Exception("未发劵，无法核销！");
        }
        // 保存日志
        $this->daoCouponUserLog->base_add([
            'com_id' => $detail['com_id'],
            'admin_id' => 0,
            'event_type' => CouponUserLogDao::EVENT_TYPE_4,
            'coupon_collect_detail_id' => $detail['id'],
            'user_id' => $params['user_id']
        ]);
        return $this->daoCouponCollectDetail->base_update($params['id'], [
            'state' => CouponCollectDetailDao::STATE_WRITE_OFF
        ]);
     }

    /**
     * 获取卡券列表
     * @param $params
     * @return mixed
     */
    public function getCouponList($params)
    {
        $list = $this->daoCouponCollectDetail->getList($params);
        $data = [];
        foreach ($list as &$value) {
            $value['files'] = json_decode($value['files']);
            $data[$value['state']][]  = $value;
        }
        return $data;
    }

    /**
     * 获取卡券详情
     * @param $id
     * @throws Exception
     * @return array
     */
    public function getCouponCollectDetail($id)
    {
        $detail = $this->daoCouponCollectDetail->base_get($id);
        if (!$detail) {
            throw new \Exception("id错误！");
        }
        // 生日不存在则从会员表获取生日
        if (!$detail['birthday']) {
            $birthData = $this->daoUnionMembership->getBirthDay([$detail['user_id']]);
            if ($birthData[$detail['user_id']]['birthday']) {
                $birth = $birthData[$detail['user_id']];
                $this->daoCouponCollectDetail->base_update($id, [
                    'birthday' => $birth['birthday'],
                    'birth_month' => $birth['birthMonth'],
                ]);
                $detail['birthday'] = $birth['birthday'];
                $detail['birth_month'] = $birth['birthMonth'];
            }
        }
        $task = $this->daoCouponTask->base_get($detail['task_id'], 'year');
        $detail['year'] = $task['year'];
        // 姓名
        $user = $this->daoUser->getById($detail['user_id'], 'name');
        $detail['name'] = $user ? $user['name'] : '';
        // 卡券模板
        $detail['card_name'] = '';
        $detail['instructions'] = '';
        if ($detail['apply_shop_id']) {
            $template = $this->daoCouponTemplate->base_get_by_cond([
                'shop_id=' => $detail['apply_shop_id']
            ], 'card_name, instructions');
            $detail['card_name'] = $template['card_name'];
            $detail['instructions'] = $template['instructions'];
        }
        // 卡券文件转数组
        $detail['files'] = $detail['files'] ? json_decode($detail['files']) : [];
        return $detail;
    }

    /**
     * 保存蛋糕卡券
     * @param $params
     * @return int
     * @throws Exception
     */
    public function saveCouponFile($params)
    {
        $detail = $this->daoCouponCollectDetail->base_get($params['id']);
        if (!$detail) {
            throw new \Exception("id错误！");
        }
        return $this->daoCouponCollectDetail->base_update($params['id'], [
           'files' => json_encode($params['files'])
        ]);
    }

    /**
     * 提交领劵申请
     * @param $params
     * @return int
     * @throws Exception
     */
    public function saveCouponCollectDetail($params)
    {
        $detail = $this->daoCouponCollectDetail->base_get($params['id']);
        if (!$detail) {
            throw new \Exception("id错误！");
        }
        if ($detail['user_id'] != $params['user_id']) {
            throw new \Exception("不可提交其他人卡券");
        }
        if ($detail['apply_shop_id']) {
            throw new \Exception("已提交，无法修改");
        }
        $membership = $this->daoUnionMembership->base_get_by_cond([
            'user_id=' => $detail['user_id'],
            'member_status=' =>DaoUnionMembership::$MEMBER_STATUS_ON
        ]);
        if (!$membership) {
            throw new \Exception("必须填写入会档案后才能进行领劵申请", 100);
        }
        $shop = $this->daoCouponShop->base_get($params['shop_id']);
        if (!$shop) {
            throw new \Exception("shop_id错误！");
        }
        // 更新任务领劵人数+1
        $this->daoCouponTask->incApplicantsNumber($detail['task_id']);
        // 保存日志
        $this->daoCouponUserLog->base_add([
            'com_id' => $detail['com_id'],
            'admin_id' => 0,
            'event_type' => CouponUserLogDao::EVENT_TYPE_1,
            'coupon_collect_detail_id' => $detail['id'],
            'user_id' => $params['user_id']
        ]);
        // 保存领劵任务明细
        return $this->daoCouponCollectDetail->base_update($params['id'], [
            'is_apply' => CouponCollectDetailDao::IS_APPLY_YES,
            'apply_shop_id' => $params['shop_id'],
        ]);
    }

    /**
     * 获取卡券店铺
     * @param $params
     * @return array
     */
    public function getCouponShop($params)
    {
        // 获取卡券模板
        $template = $this->daoCouponTemplate->base_gets_by_cond([
            'com_id='=>$params['com_id']
        ], 'shop_id');
        $shopIds = array_column($template, 'shop_id');
        $cond = [
            'com_id='=>$params['com_id'],
            'id IN' => $shopIds,
        ];
        return $this->daoCouponShop->base_gets_by_cond($cond);
    }
}
