<?php
/**
 * 卡券服务
 * @author lyc
 * @create 2023-6-16 22:33:26
 */

namespace model\union\server;

use Exception;
use model\api\dao\User;
use model\api\server\ServerBase;
use model\union\dao\CouponCollectDetailDao;
use model\union\dao\CouponUserLogDao;
use model\union\dao\ElectronicCoilDetailDao;
use model\union\dao\ElectronicCoilLogDao;
use model\union\dao\ElectronicCoilTaskDao;
use model\union\dao\ElectronicCoilTemplateDao;
use package\db\pdo\Mysql;

class ElectronicServer extends ServerBase
{

    private $daoDetail;
    private $daoTask;
    private $daoTemplate;
    private $daoLog;
    private $daoUser;
    /** @var Mysql */
    private $db;

    public function __construct($log_pre_str = '')
    {
        global $mod_name;
        parent::__construct($mod_name, $log_pre_str);

        $this->daoDetail = new ElectronicCoilDetailDao();
        $this->daoTask = new ElectronicCoilTaskDao();
        $this->daoLog = new ElectronicCoilLogDao();
        $this->daoTemplate = new ElectronicCoilTemplateDao();
        $this->daoUser = new User();
        $this->db = new Mysql();
    }

    /**
     * 确认核销
     * @param $params
     * @return int
     * @throws Exception
     */
    public function confirmWriteOff($params)
    {
        $detail = $this->daoDetail->base_get($params['id']);
        if (!$detail) {
            throw new \Exception("卡券不存在！");
        }
        if ($detail['user_id'] != $params['user_id']) {
            throw new \Exception("无权限！");
        }
        if ($detail['state'] == CouponCollectDetailDao::STATE_WRITE_OFF) {
            throw new \Exception("已核销！");
        }
        if ($detail['state'] != CouponCollectDetailDao::STATE_GRANT) {
            throw new \Exception("未领卷，无法核销！");
        }
        // 保存日志
        $this->daoLog->base_add([
            'com_id' => $detail['com_id'],
            'admin_id' => 0,
            'event_type' => ElectronicCoilLogDao::EVENT_TYPE_4,
            'detail_id' => $detail['id'],
            'user_id' => $params['user_id']
        ]);
        return $this->daoDetail->base_update($params['id'], [
            'state' => CouponCollectDetailDao::STATE_WRITE_OFF
        ]);
    }


    /**
     * 获取卡券详情
     * @param $id
     * @throws Exception
     * @return array
     */
    public function getCouponCollectDetail($id,$user_id)
    {
        $detail = $this->daoDetail->base_get($id,'*');
        if (!$detail) {
            throw new \Exception("id错误！");
        }

        if ($detail['user_id'] != $user_id) {
            throw new \Exception("无权限！");
        }
        //获取任务到期时间
        $task = $this->daoTask->base_get($detail['task_id']);
        if(!$task){
            throw new \Exception('任务不存在');
        }
        $detail['expiration_time'] = $task['expiration_time'];
        //获取模板数据信息
        $template = $this->daoTemplate->base_get($detail['template_id']);
        $detail['template_name'] = $template['card_name'];
        $detail['template_instructions'] = $template['instructions'];
        $detail['template_image'] = $template['image_url']?json_decode($template['image_url'],true):'';
        if($detail['template_image']){
            foreach ($detail['template_image'] as &$v){
                //补充图片域名
                $v = MEDIA_URL_PREFFIX.$v;
            }unset($v);
        }

        return $detail;
    }

    /**
     * 获取任务信息
     * @param $task_id
     * @return void
     */
    public function getTaskDetail($task_id)
    {
        $task = $this->daoTask->base_get($task_id,'id,name,template_ids,expiration_time');
        if(!$task){
            throw new \Exception('任务不存在');
        }

        if($task['expiration_time'] < time()){
            throw new \Exception('任务已过期');
        }

        //获取任务对应的卡卷模板
        $template_ids = json_decode($task['template_ids'],true);
        if(empty($template_ids)){
            throw new \Exception('任务异常，请联系管理员');
        }
        $template_list = $this->daoTemplate->base_gets_by_cond(['id IN'=>$template_ids],'id,card_name,instructions');

        $task['template_list'] = $template_list;

        return $task;

    }

    /**
     * 确认领卷
     * @param $id
     * @param $template_id
     * @return void
     */
    public function confirmCollar($user_id,$id, $template_id)
    {
        //判断用户的明细是否存在
        $detail = $this->daoDetail->base_get($id);
        if(!$detail){
            throw new \Exception('用户记录不存在，请联系管理员');
        }

        if ($detail['user_id'] != $user_id) {
            throw new \Exception("无权限！");
        }

        //判断任务是否存在
        $task = $this->daoTask->base_get($detail['task_id']);
        if(!$task){
            throw new \Exception('该任务不存在');
        }
        $template_ids = json_decode($task['template_ids'],true);
        if(!in_array($template_id,$template_ids)){
            throw new \Exception('领卷模板不存在');
        }

        //判断模板是否存在
        $template = $this->daoTemplate->base_get($template_id);
        if(!$template){
            throw new \Exception('领卷模板不存在');
        }

        if($detail['state']!=ElectronicCoilDetailDao::STATE_NO_GRANT){
            throw new \Exception('已领卷，请勿重复领取');
        }

        $this->daoDetail->base_update($id,[
            'state'=>ElectronicCoilDetailDao::STATE_GRANT,
            'template_id'=>$template_id,
        ]);

        // 保存日志
        $this->daoLog->base_add([
            'com_id' => $detail['com_id'],
            'admin_id' => 0,
            'event_type' => ElectronicCoilLogDao::EVENT_TYPE_1,
            'detail_id' => $detail['id'],
            'user_id' => $user_id
        ]);

    }

    /**
     * 获取卡卷列表
     * @param $post
     * @return void
     */
    public function getCouponList($params)
    {
        $user_id = isset($params['user_id'])?$params['user_id']:'';
        $states = isset($params['state'])?$params['state']:[];

        $cond = array(
            'state IN'=>$states,
            'user_id='=>$user_id,
        );
        $detail_list = $this->daoDetail->base_gets_by_cond($cond);

        $return = [];
        if($detail_list){
            $task_ids = array_column($detail_list,'task_id');
            $template_ids = array_column($detail_list,'template_id');

            $template_list = $this->daoTemplate->base_gets_by_cond(['id IN'=>$template_ids],'id,card_name,instructions');
            $template_map = array_column($template_list,null,'id');

            $task_list = $this->daoTask->base_gets_by_cond(['id IN'=>$task_ids],'id,name,expiration_time');
            $task_map = array_column($task_list,null,'id');

            foreach ($detail_list as $item){
                $return[$item['state']][] = array(
                    'id'=>$item['id'],
                    'task_id'=>$item['task_id'],
                    'template_id'=>$item['template_id'],
                    'create_time'=>$item['create_time'],
                    'update_time'=>$item['update_time'],
                    'template_name'=>isset($template_map[$item['template_id']]['card_name'])?$template_map[$item['template_id']]['card_name']:'',
                    'template_instructions'=>isset($template_map[$item['template_id']]['instructions'])?$template_map[$item['template_id']]['instructions']:'',
                    'expiration_time'=>isset($task_map[$item['task_id']]['expiration_time'])?$task_map[$item['task_id']]['expiration_time']:'',
                );
            }
        }

        return $return;

    }


}
