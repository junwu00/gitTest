<?php

namespace model\union\dao;

use model\api\dao\DaoBase;
use package\db\pdo\Mysql;

class CouponTaskDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'coupon_task';

    /** @var Mysql */
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }

    /**
     * 领劵人数+1
     * @param $taskId
     * @return int
     */
    public function incApplicantsNumber($taskId)
    {
        $sql = "UPDATE coupon_task SET `applicants_number` = `applicants_number` + 1, update_time = ? WHERE id= ?";
        return $this->db->prepare_exec($sql, true, [time(), $taskId]);
    }
}
