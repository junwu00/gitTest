<?php

namespace model\union\dao;

use model\api\dao\DaoBase;
use package\crypto\Des;
use package\db\pdo\Mysql;

class ElectronicCoilDetailDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'electronic_coil_detail';

    /** @var int 领劵状态（1未发券；2待使用；3已核销；4已过期） */
    const STATE_NO_GRANT = 1;
    const STATE_GRANT = 2;
    const STATE_WRITE_OFF = 3;
    const STATE_EXPIRE = 4;

    /** @var Mysql */
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
        $this->db = new Mysql();
    }



    /**
     * 获取核销状态中文
     * @param $state
     * @return string
     */
    public static function getStateCn($state)
    {
        if ($state == self::STATE_NO_GRANT) {
            return '未领卷';
        }
        if ($state == self::STATE_GRANT) {
            return "已发劵待核销";
        }
        if ($state == self::STATE_WRITE_OFF) {
            return '已发劵已核销';
        }
        if ($state == self::STATE_EXPIRE) {
            return '已过期';
        }
        return "未知状态：{$state}";
    }


    ##########    -敏感数据加解密- ###############
    protected function desEncodeData(&$data)
    {
        $des = new Des();
        if (isset($data['birthday'])) {
            $data['birthday'] = $des->encode($data['birthday']);
        }
        return $data;
    }
    /** 加密字段 */
    protected function desDecodeData(&$data)
    {
        $des = new Des();
        if (isset($data['birthday'])) {
            $data['birthday'] = $des->decode($data['birthday']);
        }
        return $data;
    }
}
