<?php

namespace model\union\dao;

use ExceptionBase;
use model\api\dao\DaoBase;

class DaoUnionMembership extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'union_membership';

    // 在会中
    public static $MEMBER_STATUS_ON = 1;
    // 已退休
    public static $MEMBER_STATUS_RETIRE = 2;
    // 已退会
    public static $MEMBER_STATUS_EXIT = 3;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }

    /**
     * @param array $data
     * @param array $errmsg
     * @param bool $throw_exp
     * @return bool
     * @throws ExceptionBase
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 10:22
     */
    public function insert(array $data, $errmsg, $throw_exp=TRUE) {
        return parent::insert($data, $errmsg, $throw_exp);
    }

    /**
     * @param array $cond
     * @param array $data
     * @param array $errmsg
     * @param bool $throw_exp
     * @return bool
     * @throws ExceptionBase
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 10:24
     */
    public function update_by_cond(array $cond, array $data, $errmsg, $throw_exp=TRUE) {
        return parent::update_by_cond($cond, $data, $errmsg, $throw_exp);
    }
    /**
     * 获取用户生日
     * @param $userIds
     * @return array
     */
    public function getBirthDay($userIds)
    {
        $membership = $this->base_gets_by_cond([
            'user_id IN' => $userIds,
        ], 'user_id, id_card');
        $userData = [];
        foreach ($membership as $value) {
            $data = [
                'birthday' => substr($value['id_card'],6,8),
                'birthMonth' => substr($value['id_card'],10,2),
            ];
            $userData[$value['user_id']] = $data;
        }
        foreach ($userIds as $userId) {
            if (!isset($userData[$userId])) {
                $userData[$userId] = [
                    'birthday' => '',
                    'birthMonth' => 0,
                ];
            }
        }
        return $userData;
    }
}
