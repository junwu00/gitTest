<?php

namespace model\union\dao;

use model\api\dao\DaoBase;
use package\crypto\Des;
use package\db\pdo\Mysql;

class CouponCollectDetailDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'coupon_collect_detail';

    /** @var int 领劵状态（1未发券；2待使用；3已核销；4已过期） */
    const STATE_NO_GRANT = 1;
    const STATE_GRANT = 2;
    const STATE_WRITE_OFF = 3;
    const STATE_EXPIRE = 4;

    /** 是否可发券（1可发券；2不可发券） */
    const IS_CAN_SEND_COUPON_YES = 1;
    const IS_CAN_SEND_COUPON_NO = 2;

    /** 是否提交领劵申请（1是；2否） */
    const IS_APPLY_YES = 1;
    const IS_APPLY_NO = 2;

    /** @var Mysql */
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
        $this->db = new Mysql();
    }

    /**
     * 获取卡券列表
     * @param $params
     * @return mixed
     */
    public function getList($params)
    {
        $cond = [
            'ccd.info_state=' => 1,
            'ccd.state IN' => $params['state'],
            'ccd.user_id=' => $params['user_id'],
        ];
        $table = self::$Table . ' ccd 
        inner join coupon_shop cs on cs.id = ccd.apply_shop_id and cs.info_state = 1
        inner join coupon_task cts on cts.id = ccd.task_id and cts.info_state = 1
        inner join coupon_template ct on ct.shop_id = ccd.apply_shop_id  and ct.info_state = 1 ';
        $fields = 'ccd.*, cts.year, ct.card_name, cs.name as shop_name, cs.logo as shop_logo';
        $res = $this->db->select($table, $fields, $cond, 0, 0, '', 'create_time desc');
        $res = $res ? $res :[];
        $this->desData($res, false, true);
        return $res;
    }

    /**
     * 获取核销状态中文
     * @param $state
     * @return string
     */
    public static function getStateCn($state)
    {
        if ($state == self::STATE_NO_GRANT) {
            return '未发券';
        }
        if ($state == self::STATE_GRANT) {
            return "已发劵待核销";
        }
        if ($state == self::STATE_WRITE_OFF) {
            return '已发劵已核销';
        }
        if ($state == self::STATE_EXPIRE) {
            return '已过期';
        }
        return "未知状态：{$state}";
    }

    /**
     * 获取任务入会成员数
     * @param $taskIds
     * @return mixed
     */
    public function getMembershipByTaskIds($taskIds)
    {
        $table = self::$Table . ' ccd inner join union_membership um on ccd.user_id = um.user_id ';
        $fields = "task_id, count(user_id) as user_number";
        $cond = [
            "um.info_state=" => 1,
            "ccd.info_state=" => 1,
            'um.member_status=' => DaoUnionMembership::$MEMBER_STATUS_ON,
            'ccd.task_id in' => $taskIds
        ];
        $groupBy = "task_id";
        return $this->db->select($table, $fields, $cond, 0, 0, $groupBy);

    }


    ##########    -敏感数据加解密- ###############
    protected function desEncodeData(&$data)
    {
        $des = new Des();
        if (isset($data['birthday'])) {
            $data['birthday'] = $des->encode($data['birthday']);
        }
        return $data;
    }
    /** 加密字段 */
    protected function desDecodeData(&$data)
    {
        $des = new Des();
        if (isset($data['birthday'])) {
            $data['birthday'] = $des->decode($data['birthday']);
        }
        return $data;
    }
}
