<?php

namespace model\union\dao;

use model\api\dao\DaoBase;

class CouponTemplateDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'coupon_template';

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }
}
