<?php

namespace model\union\dao;

use model\api\dao\DaoBase;

class ElectronicCoilTemplateDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'electronic_coil_template';

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }
}
