<?php

namespace model\union\dao;

use Exception;
use ExceptionBase;
use model\api\dao\DaoBase;

class DaoUnionMembershipApply extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'union_membership_apply';

    // 待审核
    public static $Audit_Status_Wait = 1;
    // 审核同意
    public static $Audit_Status_Agree = 2;
    // 审核拒绝
    public static $Audit_Status_Refuse = 3;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }

    /**
     * @param array $data
     * @param array $errmsg
     * @param bool $throw_exp
     * @return bool
     * @throws Exception
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 10:22
     */
    public function insert(array $data, $errmsg, $throw_exp=TRUE) {
        try {
            $insertRet = parent::insert($data, $errmsg, $throw_exp);
            if ($insertRet) {
                parent::log_i(sprintf("[data:%s]", json_encode($data)));
            }
            return $insertRet !== false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param array $cond
     * @param array $data
     * @param array $errmsg
     * @param bool $throw_exp
     * @return bool
     * @throws Exception
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 10:24
     */
    public function update_by_cond(array $cond, array $data, $errmsg, $throw_exp=TRUE) {
        try {
            $updateRet = parent::update_by_cond($cond, $data, $errmsg, $throw_exp);
            if ($updateRet) {
                parent::log_i(sprintf("[cond:%s data:%s]", json_encode($cond), json_encode($data)));
            }
            return $updateRet !== false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param array $cond
     * @param string $fields
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @return array
     * @throws Exception
     * ModifyUser: hwp
     * ModifyDate: 2023/6/22 14:30
     */
    public function list_by_cond(array $cond, $fields='*',$page=0, $page_size=0, $order = '') {
        return parent::list_by_cond($cond, $fields, $page, $page_size, $order);
    }
}
