<?php

namespace model\union\dao;

use model\api\dao\DaoBase;

class ElectronicCoilLogDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'electronic_coil_log';

    /** 日志类型：
     * 1.用户填写了领劵信息
     * 2.系统自动发券
     * 3.管理员手动发券
     * 4.用户确认核销电子卡券
     * 5.管理员推送领劵申请
     * 6.管理员设置可领券
     * 7.管理员设置不可领劵',
     */
    const EVENT_TYPE_1 = 1;
    const EVENT_TYPE_2 = 2;
    const EVENT_TYPE_3 = 3;
    const EVENT_TYPE_4 = 4;
    const EVENT_TYPE_5 = 5;
    const EVENT_TYPE_6 = 6;
    const EVENT_TYPE_7 = 7;

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }

    /**
     * 获取领取详细日志
     * @param $detailId
     * @return array
     */
    public function getsByDetailId($detailId)
    {
        $table = self::$Table;
        $field = '*';
        $orderBy = 'create_time desc';
        $cond = [
            'detail_id=' => $detailId
        ];
        return $this->db->select($table, $field, $cond, 0, 0, '', $orderBy);
    }
}
