<?php

namespace model\union\dao;

use model\api\dao\DaoBase;

class CouponShopDao extends DaoBase {
    /** 对应的库表名称 */
    private static $Table = 'coupon_shop';

    public function __construct()
    {
        parent::__construct();
        $this -> set_table(self::$Table);
    }
}
