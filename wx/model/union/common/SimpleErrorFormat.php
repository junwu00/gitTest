<?php

namespace model\union\common;

class SimpleErrorFormat extends ErrorFormat
{
    public function __construct()
    {
        parent::__construct("[", "]", "<", ">", ",");
    }
}