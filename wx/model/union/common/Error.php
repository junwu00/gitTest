<?php

namespace model\union\common;

class Error
{
    private $errs = [];

    /** @var ErrorFormat 展示格式 */
    private $format;

    public function __construct(ErrorFormat $format = null, $err = "")
    {
        if ($format === null) {
            $this->format = new SimpleErrorFormat();
        } else {
            $this->format = $format;
        }

        if (!($err === "" || $err === [])) {
            $this->add($err);
        }
    }

    /**
     * 添加数据
     * @param string | string[] $err
     * @return $this
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 10:15
     */
    public function add($err)
    {
        switch (gettype($err)) {
            case "string" :
                $this->errs[] = $err;
                break;
            case "array" :
                foreach ($err as $e) {
                    $this->errs[] = strval($e);
                }
                break;
        }
        return $this;
    }

    public function __toString()
    {
        return $this->generateErrorMsg();
    }

    /**
     * @return bool
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 11:16
     */
    public function isEmpty()
    {
        return count($this->errs) == 0;
    }

    /**
     * 生成报错信息
     * @return string
     * ModifyUser: hwp
     * ModifyDate: 2023/6/12 11:13
     */
    public function generateErrorMsg()
    {
        if (count($this->errs) == 0) {
            return "";
        }

        $msg = $this->format->errLeft;
        foreach ($this->errs as $e) {
            $msg .= sprintf("%s%s%s", $this->format->eLeft, $e, $this->format->eRight);
        }
        $msg .= $this->format->errRight;
        return $msg;
    }
}