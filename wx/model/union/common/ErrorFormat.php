<?php

namespace model\union\common;

class ErrorFormat
{
    /** @var string 整个消息左侧 */
    public $errLeft;
    /** @var string 整个消息右侧 */
    public $errRight;
    /** @var string 单个消息左侧 */
    public $eLeft;
    /** @var string 单个消息右侧 */
    public $eRight;
    /** @var string 分隔 */
    public $separator;

    public function __construct($errLeft, $errRight, $eLeft, $eRight, $separator)
    {
        $this->errLeft = $errLeft;
        $this->errRight = $errRight;
        $this->eLeft = $eLeft;
        $this->eRight = $eRight;
        $this->separator = $separator;
    }
}