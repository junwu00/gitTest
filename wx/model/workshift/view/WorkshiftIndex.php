<?php

namespace model\workshift\view;

class WorkshiftIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('workshift',$log_pre_str);
	}
	
	public function index() {
		$this -> show($this -> view_dir . 'page/index.html');
	}

	public function timeTag() {
		parent::get_user_info();
		$this -> show($this -> view_dir . 'page/timeTag.html');
	}
	
	public function signIn() {
		parent::get_user_info();
		g('api_jssdk') -> assign_jssdk_sign();
		$this -> show($this -> view_dir . 'page/signIn.html');
	}

	public function myLocation() {
		parent::get_user_info();
		g('api_jssdk') -> assign_jssdk_sign();
		$this -> show($this -> view_dir . 'page/myLocation.html');
	}
}

//end