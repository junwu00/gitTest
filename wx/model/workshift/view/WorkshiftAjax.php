<?php

namespace model\workshift\view;

class WorkshiftAjax extends \model\api\view\AbsProc {
	
	private $Server = "";

	//表单类型 补录
	private static $Makeup_Proc_Type = 3;
	//表单类型 请假
	private static $Rest_Proc_Type = 4;
	//表单类型 外出
	private static $Legwork_Proc_Type = 5;

	public function __construct($log_pre_str='') {
		parent::__construct('workshift',$log_pre_str);
		$this -> Server = g('ser_workshift');
	}
	
	public function index() {
		$cmd = g('pkg_val') -> get_get('cmd', FALSE);
		switch ($cmd) {
			case 101 : {
				/** 获取打卡信息 */
				$this -> get_clock_info();
				break; 
			}
			case 102 : {
				/** 打卡 / 重新打卡 */
				$this -> clock();
				break;
			}
			case 103 : {
				/** 获取定位信息 */
				$this -> get_legwork_info();
				break;
			}
			case 104 : {
				/** 定位 */
				$this -> clock_legwork();
				break;
			}
			case 105 : {
				/** 获取排班表信息 */
				$this -> get_shift_info();
				break;
			}
			case 106 : {
				/** 获取考勤汇总 （包含请假/外出记录） */
				$this -> get_checkwork_count();
				break;
			}
			case 107 : {
				/** 获取考勤详情 （包含请假/外出记录）*/
				$this -> get_checkwork_detail();
				break;
			}
			case 108 : {
				/** 获取定位详情*/
				$this -> get_legwork_clock();
				break;
			}
			case 109 : {
				/** 保存表单记录 */
				$this -> save_workitem();
				break;
			}
			case 110 : {
				/** 获取补录表单信息 */
				$this -> get_makeup_form();
				break;	
			}
			case 111 : {
				/** 获取请假类型 */
				$this -> get_rest_type();
				break;	
			}
			default : {
				// 非法访问
				parent::echo_busy();
				break;
			}

		}
	}
	
	/** 上传 */
	public function upload(){
		try{
			$fields = array("addr", "media_id");
			$data = parent::get_post_data($fields);
			$addr_name = $data['addr'];
			$media_id = $data['media_id'];
			parent::log_i("接受参数：".json_encode($data));
			if(empty($addr_name)) throw new \Exception('无法获取定位信息');

			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			$time = date("Y-m-d H:i");

			$watermark_conf = array(
					"text_list" => array(
							array('val' => $user_name, "ratio" => 1, "logo" => MAIN_ROOT."data/api/logo/logo_white.png"),
							array('val' => $addr_name, "ratio" => 0.7),
							array('val' => $time, "ratio" => 0.7),
						)
				);
			parent::log_i(json_encode($watermark_conf));
			// $file = $this -> Server -> normal_upload($watermark_conf);
			$file = $this -> Server -> jssdk_upload($media_id, $watermark_conf);
			$file['image_path'] = MEDIA_URL_PREFFIX.$file['path'];
			parent::log_i("返回信息：".json_encode($file));
			parent::echo_ok($file);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

//-----------------------------------内部实现-----------------------------------------

	/** 获取请假类型 */
	private function get_rest_type(){
		try{

			$fields = array("ids");
			$data = parent::get_post_data($fields);

			$ids = $data['ids'];
			if(empty($ids) || !is_array($ids)){
				throw new \Exception("表单配置错误");
			}

			$com_id =$_SESSION[SESSION_VISIT_COM_ID];
			$rest_type = g('ws_rest') -> get_rest_type($com_id, $ids);
			$data = array(
					'rest_type' => $rest_type
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取补录表单信息 */
	private function get_makeup_form(){
		try{

			$com_id =$_SESSION[SESSION_VISIT_COM_ID];
			$form_id = $this -> Server -> get_makeup_form($com_id);
			$data = array(
					'form_id' => $form_id,
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 保存流程 */
	private function save_workitem(){
		try {
			$needs = array('form_id', 'form_name', 'work_id', 'form_vals');
			$data = parent::get_post_data($needs);
			!isset($data['judgement']) && $data['judgement'] = '';
			!isset($data['workitem_id']) && $data['workitem_id'] = 0;
			!isset($data['formsetInst_id']) && $data['formsetInst_id'] = 0;
			!isset($data['files']) && $data['files'] = array();
			$save_state = empty($data['formsetInst_id']) ? 0 : 1;

			parent::log_i('开始保存信息');
			g('pkg_db') -> begin_trans();

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data["workitem_id"]);
			}

			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更，请返回待办列表！');
			}

			// // 获取表单模板对应的应用版本
			$form = g('mv_form') -> get_by_id($data['form_id'], $this -> com_id, 'app_version, is_other_proc');
			$data['app_version'] = $form ? $form['app_version'] : 0;
			$data['is_other_proc'] = $form['is_other_proc'];

			if($data['is_other_proc'] == self::$Rest_Proc_Type){
				$key_config = load_config("model/workshift/form");
				$fix_key = $key_config['Fix_rest'];
				$start_time_key = $fix_key[1];
				$end_time_key = $fix_key[2];

				if($data['form_vals'][$start_time_key]['format'] == "date"){
					$data['form_vals'][$start_time_key]['format'] = "datetime";
					$data['form_vals'][$start_time_key]['val'] .= " 00:00";
					$data['form_vals'][$end_time_key]['format'] = "datetime";
					$data['form_vals'][$end_time_key]['val'] .= " 23:59";
				}
			}

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$ret = parent::save_workitem_parent($data);

			$formsetinst_id = $ret['formsetInst_id'];
			switch ($data['is_other_proc']) {
				case self::$Makeup_Proc_Type:{
					$this -> Server -> save_makeup_record($this -> com_id, $ret['user_id'], $formsetinst_id, $data['form_name'], $data['form_vals'], $save_state);
					break;
				}
				case self::$Rest_Proc_Type:{
					$this -> Server -> save_rest_record($this -> com_id, $ret['user_id'], $formsetinst_id, $data['form_name'], $data['form_vals'], $save_state);
					break;
				}
				case self::$Legwork_Proc_Type:{
					$this -> Server -> save_legwork_record($this -> com_id, $ret['user_id'], $formsetinst_id, $data['form_name'], $data['form_vals'], $save_state);
					break;
				}
				default: break;
			}
			g('pkg_db') -> commit();

			$info = array(
				'info' => array(
					'workitem_id' => $ret['workitem_id'],
					'formsetInst_id' => $ret['formsetInst_id'],
					),
				);

			parent::log_i('保存流程信息成功');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('保存流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err(9999, $e -> getMessage());
		}
	}
	


	/** 获取打卡信息 */
	private function get_clock_info(){
		try{
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$info = $this -> Server -> get_clock_info($com_id, $user_id);
			if(empty($info)){
				$info = array(
						'addr_info' => array(),
						'clock_record' => array(),
						'type' => 1,
						'shift_info' => array(),
						'curr_shift' => array(),
					);
			}
			$info['time'] = time();
			parent::echo_ok($info);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}	

	/** 获取打卡信息 */
	private function clock(){
		try{
			$fields = array("lng", "lat");
			$data = parent::get_post_data($fields);
			$id = isset($data['id']) ? (int)$data['id'] : 0;
			$time = isset($data['time']) ? (int)$data['time'] : "";
			$lng = $data['lng'];
			$lat = $data['lat'];
			$cover = isset($data['cover']) ? (int)$data['cover'] : 0;

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$clock = $this -> check_clock($user_id);
			if($clock){
				$ret =$this -> Server -> clock_checkwork($com_id, $user_id, $id, $time, $lng, $lat, $cover);
				if($ret['type'] == 0){
					$this -> lock_clock($user_id);
					$ret['time'] = time();
				}else if($ret['type'] == 1){
					$ret['title'] = "确定打卡？";
					$ret['desc'] = "这一时段已打过卡，继续操作将会更新上次打卡喔";
				}else if($ret['type'] == 3){
					$ret['title'] = "确定打卡？";
					$ret['desc'] = "早退的话，会被老板发现的喔！";
				}
			}else{
				$ret = array(
						'type' => 2,
						'title' => "15秒内不能重复打卡喔，休息一下再来吧！",
					);
			}

			parent::echo_ok($ret);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}	

	/** 获取定位信息 */
	private function get_legwork_info(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);

			$formsetinst_id = isset($data['formsetinst_id']) ? intval($data['formsetinst_id']) : 0;
			$record_id = isset($data['record_id']) ? intval($data['record_id']) : 0;

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$data = $this -> Server -> get_legwork_info($com_id, $user_id, $record_id, $formsetinst_id);
			$data['time'] = time();
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 外出定位 */
	private function clock_legwork(){
		try{
			$fields = array("id", "addr", "lng", "lat");
			$data = parent::get_post_data($fields);
			$id = (int)$data['id'];
			$addr = $data['addr'];
			$lng = $data['lng'];
			$lat = $data['lat'];
			$file_list = isset($data['file_list']) ? $data['file_list'] : array();
			$mark = isset($data['mark']) ? $data['mark'] : "";
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$legwork_info = g('ws_legwork') -> get_record_by_id($com_id, $id, 'l.user_id, l.formsetinst_id');;
			if(empty($legwork_info) || $legwork_info['user_id'] != $user_id ){
				throw new \Exception("没有权限定位");
			}

			$task_photo = $this -> Server -> get_legwork_config($com_id);
			if($task_photo && (empty($file_list) || !is_array($file_list))){
				throw new \Exception("必须进行拍照定位");
			}

			$ret = $this -> Server -> clock_legwork($com_id, $user_id, $id, $addr, $lng, $lat, $mark, $file_list);
			$this -> remind_legwork($user_id, $com_id, $id, $legwork_info['formsetinst_id']);

			if(!$ret) throw new \Exception("定位失败");
			$data = array(
					'time' => time()
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取排班表信息 */
	private function get_shift_info(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);
			$year = isset($data['year']) ? (int)$data['year'] : date("Y");
			$month = isset($data['month']) ? (int)$data['month'] : date("m");

			if($month < 10 && strlen($month) == 1) $month = "0".$month;

			$start_date = $year.$month.'01';
			$start_time = strtotime($start_date);
			$end_time = strtotime("-1 day", strtotime("+1 month", $start_time));
			$end_date = date("Ymd", $end_time);

			if($start_date > $end_date) throw new \Exception("日期配置错误");

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$shift_list = $this -> Server -> get_my_workshift($com_id, $user_id, $start_date, $end_date);
			$shift_list['user_name'] = $_SESSION[SESSION_VISIT_USER_NAME];
			$shift_list['time'] = time();
			parent::echo_ok($shift_list);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取我的考勤汇总信息 */
	private function get_checkwork_count(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);
			$year = (isset($data['year']) && !empty($data['year'])) ? (int)$data['year'] : date("Y");
			$month = (isset($data['month']) && !empty($data['month'])) ? (int)$data['month'] : date("m");

			if(empty($month)){
				throw new \Exception("月份错误");
			}

			if($month<10 && strlen($month) == 1) $month ='0'.$month; 
			$start_date = $year.$month.'01';
			$start_time = strtotime($start_date); 
			$end_time = strtotime("-1 day", strtotime("+1 month", $start_time));
			$end_date = date("Ymd", $end_time);
			$now_date = date("Ymd");

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$count = $this -> Server -> get_checkwork_count($com_id, $user_id, $start_date, $end_date);
			$count['user_name'] = $_SESSION[SESSION_VISIT_USER_NAME];
			parent::echo_ok($count);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取考勤详情 */
	private function get_checkwork_detail(){
		try{
			$fields = array('date');
			$data = parent::get_post_data($fields);
			$date = $data['date'];

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$date = str_replace("-", "", $date);

			if($date > date("Ymd")){
				$ret = array(
						'clock_info' => array(),
						'rest_record' => array(),
						'legwork_record' => array()
					);
			}else{
				$ret = $this -> Server -> get_checkwork_detail($com_id, $user_id, $date, $date);
			}

			parent::echo_ok($ret);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	private function remind_legwork($user_id, $com_id, $record_id, $formsetinst_id){
		$user_list = $this -> Server -> get_remind_user($com_id, $user_id, $formsetinst_id);
		if(empty($user_list)) return;

		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		$title = "{$user_name}的定位汇报";
		$content = "{$user_name}将实时定位推送给您，请及时查看";
		$msg_title = "收到了[{$user_name}]的实时定位";

		$url = MAIN_DYNAMIC_DOMAIN . "/index.php?app=workshift&a=myLocation&id={$record_id}";
		parent::send_news($title, $content, '', $url, 25, 3, $user_id, $user_list, array(), $msg_title);

	}

	/** 检查是否可以打卡 */
	private function check_clock($user_id){
		$key = $this -> get_redis_key($user_id);
		$date = g('pkg_redis') -> get($key);
		if(empty($date)){
			return true;
		}else{
			return false;
		}
	}

	/** 打卡成功之后，加15秒锁 */
	private function lock_clock($user_id){
		$key = $this -> get_redis_key($user_id);
		$date = g('pkg_redis') -> set($key, 1, 15);
	}

	/** 获取缓存key */
	private function get_redis_key($str){
		return md5($str.":workshift:clock");
	}

}

//end