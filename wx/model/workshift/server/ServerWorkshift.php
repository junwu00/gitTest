<?php 
/**
 * 考勤管理业务逻辑处理类
 */

namespace model\workshift\server;

class ServerWorkshift extends \model\api\server\ServerBase {

	private static $Makeup_Proc_type = 3;

	public function __construct($log_pre_str='') {
		parent::__construct('workshift', $log_pre_str);
	}

	public function get_remind_user($com_id, $user_id, $formsetinst_id){
		$handler_list = g("mv_workinst") -> get_formset_state_workitem($formsetinst_id, 0, $com_id, 'handler_id');
		if(empty($handler_list)) return;

		$all_user = array();
		foreach ($handler_list as $handler) {
			if($user_id != $handler['handler_id']){
				$all_user[] = $handler['handler_id'];
			}
		}unset($handler);

		$all_user = array_unique($all_user);
		return $all_user;
	}

	/** 获取企业补录表单信息 */
	public function get_makeup_form($com_id){
		$form = g('mv_form') -> get_public_proc(self::$Makeup_Proc_type, $com_id, 'mpfm.id, is_show_wx');
		if(empty($form) || !$form['is_show_wx']){
			throw new \Exception("补录表单未启用");
		}
		return $form['id'];
	}

	/** 保存补录记录 */
	public function save_makeup_record($com_id, $user_id, $formsetinst_id, $form_name, $form_vals, $save_state){
		$key_config = load_config("model/workshift/form");
		$key = $key_config['Fix_makeup'];
		if(empty($key)) throw new \Exception('配置错误');

		$makeup_time_key = $key[0];
		$makeup_time = $form_vals[$makeup_time_key]['val'];
		if(empty($makeup_time)) throw new \Exception("补录时间为空");

		$makeup_time = strtotime($makeup_time);

		$shift = g('ws_shift') -> get_shift_by_time($com_id, $user_id, $makeup_time);
		if(empty($shift)) throw new \Exception("指定班次不存在");

		$shift_id = $shift['id'];
		$makeup_type = $shift['type'];
		if(!$save_state){
			$makeup = g('ws_checkwork') -> get_makeup_by_time($com_id, $user_id, $makeup_time);
			if(!empty($makeup) && $record['formsetinst_id'] != $formsetinst_id) throw new \Exception("该时间点已经申请补录，无需重复申请");		


			//新建
			$ret = g('ws_checkwork') -> save_makeup_record($com_id, $user_id, $formsetinst_id, $makeup_type, $makeup_time, $shift_id);
			if(!$ret) throw new \Exception('保存补录失败');
		}else{
			//保存
			$ret = g('ws_checkwork') -> update_makeup_record($formsetinst_id, $makeup_type, $makeup_time, $shift_id);
			if(!$ret) throw new \Exception('保存补录失败');
		}
	}

	/** 保存请假记录 */
	public function save_rest_record($com_id, $user_id, $formsetinst_id, $form_name, $form_vals, $save_state){
		$key_config = load_config("model/workshift/form");
		$fix_key = $key_config['Fix_rest'];
		$hide_key = $key_config['Hide_rest'];
		if(empty($fix_key) || empty($hide_key)) throw new \Exception('配置错误');

		$start_time_key = $fix_key[1];
		$end_time_key = $fix_key[2];
		$rest_type_key = $fix_key[0];

		$rest_hours_key = $hide_key[0];

		$start_time = strtotime($form_vals[$start_time_key]['val']);
		$end_time = strtotime($form_vals[$end_time_key]['val']);
		$rest_hours = $form_vals[$rest_hours_key]['val'];
		$rest_type_name = $form_vals[$rest_type_key]['val'];

		// $rest_type = $this -> get_rest_type($com_id, $rest_type_name);
		$opts_nums = explode(',', $form_vals[$rest_type_key]['opts_nums']);
		$opts = explode(',', $form_vals[$rest_type_key]['opts']);
		$rest_type = $opts_nums[array_search($rest_type_name, $opts)];
		
		if(empty($start_time) || empty($end_time) || $end_time < $start_time || $rest_hours <= 0){
			throw new \Exception("开始时间/结束时间错误");
		}

		if(!$save_state){
			$record = g('ws_legwork') -> get_record_by_time($com_id, $user_id, $start_time, $end_time);
			if(!empty($record)) throw new \Exception("该时间段已经存在外出申请，无法重复申请");

			$record = g('ws_rest') -> get_record_by_time($com_id, $user_id, $start_time, $end_time);
			if(!empty($record) && $record['formsetinst_id'] != $formsetinst_id) throw new \Exception("该时间段已经存在请假申请，无法重复申请");

			if($rest_type_name == "年假"){
				//计算年假剩余
				$ret = $this -> count_year_rest($com_id, $user_id, $rest_hours);
			}
			g('ws_rest') -> save_rest_record($com_id, $user_id, $formsetinst_id, $start_time, $end_time, $rest_type, $rest_hours);
		}else{
			g('ws_rest') -> update_rest_record($formsetinst_id, $start_time, $end_time, $rest_type, $rest_hours);
		}
	}

	/** 保存外出记录 */
	public function save_legwork_record($com_id, $user_id, $formsetinst_id, $form_name, $form_vals, $save_state){
		$key_config = load_config("model/workshift/form");
		$fix_key = $key_config['Fix_legwork'];
		$hide_key = $key_config['Hide_legwork'];
		if(empty($fix_key) || empty($hide_key)) throw new \Exception('配置错误');

		$start_time_key = $fix_key[0];
		$end_time_key = $fix_key[1];

		$legwork_hours_key = $hide_key[0];

		$start_time = strtotime($form_vals[$start_time_key]['val']);
		$end_time = strtotime($form_vals[$end_time_key]['val']);
		$legwork_hours = $form_vals[$legwork_hours_key]['val'];

		if(empty($start_time) || empty($end_time) || $end_time < $start_time || $legwork_hours <= 0){
			throw new \Exception("开始时间/结束时间错误");
		}

		if(!$save_state){
			$record = g('ws_rest') -> get_record_by_time($com_id, $user_id, $start_time, $end_time);
			if(!empty($record)) throw new \Exception("该时间段已经存在请假申请，无法重复申请");

			$record = g('ws_legwork') -> get_record_by_time($com_id, $user_id, $start_time, $end_time);
			if(!empty($record) && $record['formsetinst_id'] != $formsetinst_id) throw new \Exception("该时间段已经存在外出申请，无法重复申请");

			g('ws_legwork') -> save_legwork_record($com_id, $user_id, $formsetinst_id, $start_time, $end_time, $legwork_hours);
		}else{
			g('ws_legwork') -> update_legwork_record($formsetinst_id, $start_time, $end_time, $legwork_hours);
		}
	}


	/** 获取打卡信息 */
	public function get_clock_info($com_id, $user_id){
		//标识昨天还是今天 1：今天， 2：昨天
		$today_yestoday = 1;
		$today = date("Ymd");
		$shift = g('ws_shift') -> get_shift_by_date(array($user_id), 0, $today, $today);
		if(!empty($shift)){
			//获取最早打卡时间和排班信息ID
			$min_clock_time = 999999999999;
			$workshift_info_id = 0;
			foreach ($shift as $s) {
				$s['start_time'] < $min_clock_time && $min_clock_time = $s['start_time'];
				$workshift_info_id == 0 && $workshift_info_id  = $s['workshift_id'];
			}unset($s);

			$config = g('ws_checkwork') -> get_config_by_com($com_id, 'earliest_time');
			$earliest_time = $config['earliest_time'];
			$first_clock_time = $min_clock_time - $earliest_time * 60;
			if($first_clock_time > time()){
				//未到第二天最早打卡时间  读取昨天的班次
				$yesterday_shift = $this -> get_yesterday_shift($user_id);
				if(!empty($yesterday_shift)){
					$today = date("Ymd", strtotime("-1 day"));
					$shift = $yesterday_shift;
				}
			}
		}else{
			//今天没有班次 读取昨天的班次
			$yesterday_shift = $this -> get_yesterday_shift($user_id);
			if(!empty($yesterday_shift)){
				$today = date("Ymd", strtotime("-1 day"));
				$shift = $yesterday_shift;
			}
		}
		if(empty($shift)){
			//今天没有班次
			$return = array(
					'addr_info' => array(),
					'clock_record' => array(),
					'type' => 0,
					'shift_info' => array(),
					'curr_shift' => array(),
					'clcok_time' => array(),
				);
			return $return;
		}

		$shift_info = array();
		$rest_legwork = g('ws_core') -> get_other_record($user_id, $today, $today);
		//根据请假和外出记录，获取最新的班次信息
		$shift = $this -> get_merge_shift($shift, $rest_legwork, $shift_info);

		$s = $shift[0];
		$workshift_id = $s['workshift_id'];
		$type = $s['shift_type'];
		
		$addr_info = g('ws_checkwork') -> get_addr_by_workshift($com_id, $workshift_id, 'addr_name, lng, lat, scope');

		//获取打卡记录
		$record_array = $this -> get_record_list($shift);
		//从班次中获取今天要显示的班次
		$curr_shift = $this -> get_show_record($record_array);
		//从班次中获取打卡时间点
		$clcok_time = $this -> get_clock_time($shift);

		sort($shift_info);
		$record_array = array_values($record_array);
		$return = array(
				'addr_info' => $addr_info,
				'clock_record' => $record_array,
				'type' => $type,
				'shift_info' => $shift_info,
				'curr_shift' => $curr_shift,
				'clcok_time' => $clcok_time,
				'rest_legwork_record' => $rest_legwork
			);
		return $return;
	}
	
	/** 考勤打卡 判断能否打卡/重新打卡 */
	public function clock_checkwork($com_id, $user_id, $shift_id, $time, $lng, $lat, $cover = 0){
		$all_shift = array();
		if(!empty($shift_id)){
			$shift = g('ws_shift') -> get_by_id($shift_id, '*');
			$date = $shift['date'];
			$all_shift = g('ws_shift') -> get_shift_by_date(array($user_id), 0, $date, $date, '*');

			$rest_legwork = g('ws_core') -> get_other_record($user_id, $date, $date);
			$shift_info = array();
			$all_shift = $this -> get_merge_shift($all_shift, $rest_legwork, $shift_info);
			
			//获取最早打卡时间和排班信息ID 并判断是否已经到达最早打卡时间
			$min_clock_time = 999999999999;
			$workshift_info_id = 0;
			foreach ($all_shift as $s) {
				$s['start_time'] < $min_clock_time && $min_clock_time = $s['start_time'];
				$workshift_info_id == 0 && $workshift_info_id  = $s['workshift_id'];
			}unset($s);

			$config = g('ws_checkwork') -> get_config_by_com($com_id, 'earliest_time');
			$earliest_time = $config['earliest_time'];
			$first_clock_time = $min_clock_time - $earliest_time * 60;
			if($first_clock_time > time()){
				throw new \Exception("未到开始打卡时间");
			}

			//获取将已过去和第一个未过去的打卡点
			$ret = $this -> get_last_and_next($all_shift);
			$last_shift = $ret['last_shift'];
			$next_shift = $ret['next_shift'];
		}else{
			$last_shift = array("time" => 0, 'id' => 0, 'clock' => 0, "type" => 0, "state" => 0);
			$next_shift = array("time" => 99999999999, 'id' => 0, 'clock' => 0, "type" => 0, "state" => 0);
		}

		$clock_shift = array();
		
		//第一次打卡，如果检测没有可以第一次打卡的记录，则认为重新打卡最后一次记录
		if((!empty($last_shift['id']) && empty($last_shift['clock'])) || ($last_shift['type'] == 2 &&  !empty($last_shift['clock']) && $last_shift['clock']<$last_shift['time']) || empty($next_shift['id']) ){
			$clock_shift = $last_shift;
		}else if((!empty($next_shift['id']) && empty($next_shift['clock'])) || $next_shift['type'] == 2){
			if($next_shift['type'] == 2 && $cover){
				$clock_shift = $next_shift;
			}else if($next_shift['type'] == 2 && !$cover && $next_shift['time'] > time()){
				//下班打卡，并且未到打卡时间, 并且未打卡过
				return array('type' => 3);
			}else{
				$clock_shift = $next_shift;
			}
		}else if((!empty($next_shift['id']) && empty($next_shift['clock'])) || $next_shift['type'] == 1){
			//下一次打卡是开始打卡，并且已经打卡
			if($cover){
				$clock_shift = $next_shift;
			}else{
				return array('type' => 1);
			}
		}

		if(empty($clock_shift)){
			throw new \Exception("未到打卡时间");
		}
		$addr_list = g('ws_checkwork') -> get_addr_by_workshift($com_id, $clock_shift['workshift_id']);
		if(empty($addr_list)){
			throw new \Exception("未设置考勤地点，无法打卡");
		}
		$check_addr = $this -> check_scope($addr_list, $lng, $lat);


		if($clock_shift['state'] != 3 && $clock_shift['state'] != 4){
			//请假和外出的记录，不再覆盖
			try{
				g('pkg_db') -> begin_trans();
				g('ws_checkwork') -> clock($user_id, $clock_shift['id'], $clock_shift['type']);
				$ret = g('ws_checkwork') -> save_checkwork_record($com_id, $user_id, $clock_shift['id'], $check_addr['addr_id'], $check_addr['addr_name'], $check_addr['scope'], $lng, $lat);
				if(!$ret){
					throw new \Exception("保存打卡记录失败");
				}
				g('pkg_db') -> commit();
			}catch(\Exception $e){
				g('pkg_db') -> rollback();
				throw $e;
			}
		}

		if(isset($all_shift) && !empty($clock_shift['id'])){
			$time = date("H:i", $clock_shift['time']);
			foreach ($all_shift as &$s) {
				if($s['id'] == $clock_shift['id']){
					if($time == date("H:i", $s['start_time'])){
						$s['start_clock_time'] = time();
						$late = $s['start_clock_time'] - $s['start_time'];
						$s['late_time'] = ($late > 0) ? $late : 0;
					}else if($time == date("H:i", $s['end_time'])){
						$s['end_clock_time'] = time();
						$early_time = $s['end_time'] - $s['end_clock_time'];
						$s['early_time'] = ($early_time > 0) ? $early_time : 0;
					}
				}
			}unset($s);
			$record_array = $this -> get_record_list($all_shift);
			$curr_shift = $this -> get_show_record($record_array);
			$clock_time = $this -> get_clock_time($all_shift);
			$data = array(
					'clock_record' => $record_array,
					'curr_shift' => $curr_shift,
					'clock_time' => $clock_time,
					'type' => 0
				);

			return $data;
		}

		return array();
	}

	/** 获取外勤信息 */
	public function get_legwork_info($com_id, $user_id, $record_id, $formsetinst_id=0){
		if(!empty($formsetinst_id)){
			//表单详情页面
			$legwork_record = g('ws_legwork') -> get_record_by_formsetinst($com_id, $formsetinst_id, 'l.id, l.user_id, l.formsetinst_id, l.start_time, l.end_time, f.state');
		}else if(!empty($record_id)){
			//定位推送页面
			$legwork_record = g('ws_legwork') -> get_record_by_id($com_id, $record_id, 'l.id, l.user_id,  l.formsetinst_id, l.start_time, l.end_time, f.state');
			if(!empty($legwork_record) && $user_id != $legwork_record['user_id']){
				//不是本人
				$workitem = g('mv_workinst') -> get_workitem_by_user_id($legwork_record['formsetinst_id'], $user_id, "id");
				$legwork_record['workitem_id'] = $workitem['id'];
			}
		}else{
			//定位页面
			$legwork_record = g('ws_legwork') -> get_record($com_id, $user_id, 'l.id, l.user_id,  l.formsetinst_id, l.start_time, l.end_time, f.state');
		}

		$user_info = g('dao_user') -> get_by_id($legwork_record['user_id'],'name, pic_url');

		if(empty($legwork_record)){
			throw new \Exception("没有外出信息");
		}

		$clock_list = g('ws_legwork') -> get_clock($com_id, $legwork_record['id'], 'c.id, c.addr_name, c.clock_time, c.mark, f.file_path path, lng, lat');
		$clock_record = array();
		foreach ($clock_list as $c) {
			if(!isset($clock_record[$c['id']])){
				$c['file_path'] = array();
				$clock_record[$c['id']] = $c;
			}
			if(!empty($c['path'])){
				$clock_record[$c['id']]['file_path'][] = MEDIA_URL_PREFFIX.$c['path'];
			}
			unset($clock_record[$c['id']]['path']);
		}unset($c);
		$clock_record = array_values($clock_record);
		$clock_record = array_reverse($clock_record);

		$task_photo = $this -> get_legwork_config($com_id);

		$legwork_record['record_id'] = $legwork_record['id'];
		$legwork_record['id'] = $legwork_record['formsetinst_id'];

		if(isset($legwork_record['workitem_id'])){
			$legwork_record['id'] = $legwork_record['workitem_id'];			
		}

		unset($legwork_record['formsetinst_id']);
		$data = array(
				"legwork_record" => $legwork_record,
				"clock_list" => $clock_record,
				"is_photo" => $task_photo,
				"user_info" => $user_info,
			);
		return $data;
	}

	/** 外出定位 */
	public function clock_legwork($com_id, $user_id, $record_id, $addr, $lng, $lat, $mark, $file_list=array()){
		$clock_id = g('ws_legwork') -> save_clock($com_id, $user_id, $record_id, $addr, $lng, $lat, $mark);
		if(!empty($file_list)){
			g('ws_legwork') -> save_file($clock_id, $file_list);
		}
		return true;
	}

	/** 获取外出拍照配置 */
	public function get_legwork_config($com_id){
		return g('ws_legwork') -> get_config_by_com($com_id);
	}

	/** 获取我的排班表 */
	public function get_my_workshift($com_id, $user_id, $start_date, $end_date){
		$shift_list = g('ws_shift') -> get_shift_by_date(array($user_id), 2, $start_date, $end_date, "date, shift_name, shift_id, start_time, end_time");
		$all_shift = array();
		if(!empty($shift_list)){
			$date_shift = array();
			foreach ($shift_list as $shift) {
				if(!isset($date_shift[$shift['date']])){
					$date_shift[$shift['date']] = array();
				}
				if(!isset($all_shift[$shift['shift_id']])){
					$key = (date("Hi",$shift['start_time']) + ("0.".$shift['shift_id']))."";
					$all_shift[$key] = array(
							'id' => $shift['shift_id'],
							'name' => $shift['shift_name'],
							'time' => date('H:i', $shift['start_time']).' - '. date('H:i', $shift['end_time']),
						);
				}
				$date_shift[$shift['date']][] = $shift;
			}unset($shift);
		}else{
			$date_shift = array();
		}
		$date = $start_date;
		$shift_table = array();
		while ($date <= $end_date) {
			$date_time = strtotime($date);
			$shift_table[$date_time] = "";

			if(isset($date_shift[$date])){
				$shift_name = array();
				foreach ($date_shift[$date] as $shift) {
					$shift_name[$shift['start_time']] = $shift['shift_id'];
				}unset($shift);
				ksort($shift_name);
				$shift_name = array_values($shift_name);
				$shift_table[$date_time] = implode('^', $shift_name);
			}
			$date++;
		}
		$title = array_keys($shift_table);
		$info = array_values($shift_table);

		ksort($all_shift);
		$all_shift = array_values($all_shift);

		return $data = array(
				'title' => $title,
				'info' => $info,
				'shift_info' => $all_shift
			);
	}


	/** 获取我的考勤汇总 需要对今天的记录特殊处理 */
	public function get_checkwork_count($com_id, $user_id, $start_date, $end_date){
		$now_date = date("Ymd");
		if($end_date > $now_date){
			$shidt_end_date = $now_date;
		}else{
			$shidt_end_date = $end_date;
		}
		$count = g('ws_checkwork') -> wx_get_checkwork_count($com_id, $user_id, $start_date, $shidt_end_date);
		
		$record = g('ws_core') -> get_other_record($user_id, $start_date, $end_date);
		$count['rest_record'] = $record['rest'];
		$count['legwork_record'] = $record['legwork'];
		return $count;
	}

	public function get_checkwork_detail($com_id, $user_id, $start_date, $end_date){
		$now_date = date("Ymd");
		if($end_date > $now_date){
			$shidt_end_date = $now_date;
		}else{
			$shidt_end_date = $end_date;
		}
		$date_shift = g('ws_shift') -> get_shift_by_date(array($user_id), 0, $start_date, $shidt_end_date);
		$shift_info = array();
		$detail = $this -> get_record_list($date_shift, $shift_info);
		foreach ($detail as $key => $d) {
			if($d['time'] >= time() && empty($d['clock_time'])){
				unset($detail[$key]);
				continue;
			}
			// if(!empty($detail[$key]['record_id'])){
				// $detail[$key]['id'] = $detail[$key]['record_id'];
				// unset($detail[$key]['record_id']);
			// }
		}unset($d);

		$detail = array_values($detail);
		$data = array(
				'clock_info' => $detail,
			);
		
		$record = g('ws_core') -> get_other_record($user_id, $start_date, $end_date);
		$data['rest_record'] = $record['rest'];
		$data['legwork_record'] = $record['legwork'];
		return $data;	
	}

//---------------------------------------内部实现---------------------------------------------
	//从班次中获取打卡时间点
	private function get_clock_time($shift){
		$clock_time = array();
		foreach ($shift as $s) {
			if($s['end_time'] <= $s['start_time']){
				//请假时间包含 开始和结束时间的情况
				continue;
			}

			if(isset($s['record_type'])){
				$clock_time[$s['start_time']] = array(
						"time" => date("H:i", $s['start_time']),
						"type" => $s['record_type'], 
						"state" => 1,
					);
				$clock_time[$s['end_time']] = array(
						"time" => date("H:i", $s['end_time']),
						"type" => $s['record_type'], 
						"state" => 1,
					);
			}else{
				$clock_time[$s['start_time']] = array(
						"time" => date("H:i", $s['start_time']),
						"type" => 1, 
						"state" => empty($s['start_clock_time']) ? 0 : 1,
					);
				$clock_time[$s['end_time']] = array(
						"time" => date("H:i", $s['end_time']),
						"type" => 2, 
						"state" => empty($s['end_clock_time']) ? 2 : 1,
					);
			}

		}unset($s);
		ksort($clock_time);
		$state = 0;
		foreach ($clock_time as $key => &$v) {
			if($key > time()){
				$v['state'] == 0 && $v['state'] = 2;
			}
		}unset($v);
		$clock_time = array_values($clock_time);
		return $clock_time;
	}

	private function get_record_list($shift){
		$record_array = array();
		$now = time();
		$makeup_list = array();
		foreach ($shift as $key => $s) {
			if(($s['start_state'] == 3 || $s['start_state'] == 4) && ($s['end_state'] == 3 || $s['end_state'] == 4)){
				continue;
			}
			$tmp_r_s = array();
			$tmp_r_s['time'] = $s['start_time'];
			$tmp_r_s['clock_time'] = $s['start_clock_time'];
			$tmp_r_s['late'] = ($s['start_clock_time'] < $s['start_time'] || $s['late_time'] < 0) ? 0 : $s['late_time'];
			$tmp_r_s['early'] = 0;
			$tmp_r_s['id'] = $s['id'];
			$tmp_r_s['record_id'] = 0;
			$tmp_r_s['type'] = $s['start_state'] == 2 ? 1 : $s['start_state'];
			$record_array[$tmp_r_s['time']] = $tmp_r_s;

			$tmp_r_e = array();
			$tmp_r_e['time'] = $s['end_time'];
			$tmp_r_e['clock_time'] = $s['end_clock_time'];
			$tmp_r_e['early'] = ($s['end_clock_time'] > $s['end_time'] || $s['early_time'] < 0) ? 0 : $s['early_time'];
			$tmp_r_e['late'] = 0;
			$tmp_r_e['id'] = $s['id'];
			$tmp_r_e['record_id'] = 0;
			$tmp_r_e['type'] = $s['end_state'] == 2 ? 1 : $s['end_state'];
			$record_array[$tmp_r_e['time']] = $tmp_r_e;

			if(empty($s['start_clock_time']) || empty($s['end_clock_time']) || $s['start_state'] == 2 || $s['end_state'] == 2){
				$makeup_list[] = $s['id'];
			}
		}unset($s);
		
		if(!empty($makeup_list)){
			$makeup_list = g('ws_checkwork') -> get_makeup_by_shift($makeup_list, 'm.shift_id, m.formsetinst_id record_id, makeup_time', 2);
			if(!empty($makeup_list)){
				foreach ($makeup_list as $m) {
					foreach ($record_array as $key => $r) {
						if($m['shift_id'] == $r['id'] && $m['makeup_time'] == $r['time']){
							$record_array[$key]['type'] = 2;
							$record_array[$key]['record_id'] = $m['record_id'];
						}
					}unset($r);
				}unset($m);
			}
		}
		return $record_array;
	}

	/** 检查考勤地点是否合法, 并返回考勤地点信息 */
	private function check_scope($addr_list, $lng, $lat){
		$curr_addr = array();
		$scope = 0;
		foreach ($addr_list as $addr) {
			$scope = g('pkg_map') -> get_distance($addr['lng'], $addr['lat'], $lng, $lat);
			if($addr['scope'] >= $scope || empty($addr['scope'])){
				$curr_addr = $addr;
				break;
			}
		}unset($addr);

		if(empty($curr_addr)){
			throw new \Exception("请在考勤范围内打卡");
		}
		$addr_info = array(
				'addr_id' => $curr_addr['id'],
				'addr_name' => $curr_addr['addr_name'],
				'scope' => (int)($scope),
			);
		return $addr_info;
	}

	/** 获取班次中，上一个和下一个打卡时间点 */
	private function get_last_and_next($all_shift){
		$last_shift = array("time" => 0, 'id' => 0, "clock" => 0, 'type'=> 0);
		$next_shift = array("time" => 99999999999, 'id' => 0, "clock" => 0, 'type'=> 0);
		if(!empty($all_shift)){
			$now = time();
			$state = 0;
			foreach ($all_shift as $shift) {
				if($shift['start_time'] < $now){
					if($shift['start_time'] > $last_shift['time']){
						$last_shift = array(
								'time' => $shift['start_time'],
								'id' => $shift['id'],
								'clock' => $shift['start_clock_time'],
								'state' => $shift['start_state'],
								'workshift_id' => $shift['workshift_id'],
								'type' => 1,
							);
					}
				}else{
					if($shift['start_time'] < $next_shift['time']){
						$next_shift = array(
								'time' => $shift['start_time'],
								'id' => $shift['id'],
								'clock' => $shift['start_clock_time'],
								'state' => $shift['start_state'],
								'workshift_id' => $shift['workshift_id'],
								'type' => 1,
							);
					}
				}

				if($shift['end_time'] < $now){
					if($shift['end_time'] > $last_shift['time']){
						$last_shift = array(
								'time' => $shift['end_time'],
								'id' => $shift['id'],
								'clock' => $shift['end_clock_time'],
								'state' => $shift['end_state'],
								'workshift_id' => $shift['workshift_id'],
								'type' => 2,
							);
					}
				}else{
					if($shift['end_time'] < $next_shift['time']){
						$next_shift = array(
								'time' => $shift['end_time'],
								'id' => $shift['id'],
								'clock' => $shift['end_clock_time'],
								'state' => $shift['end_state'],
								'workshift_id' => $shift['workshift_id'],
								'type' => 2,
							);
					}
				}
			}unset($shift);
		}
		$data = array(
				'last_shift' => $last_shift,
				'next_shift' => $next_shift,
			);
		return $data;
	}

	/** 获取昨天班次,并检查是否有未打卡/早退班次, 如果没有，返回空 */
	private function get_yesterday_shift($user_id){
		$yesterday = date("Ymd", strtotime("-1 day"));
		$shift = g('ws_shift') -> get_shift_by_date(array($user_id), 0, $yesterday, $yesterday);
		$last_time = 0;
		$last_state = 1;
		foreach ($shift as $s) {
			if($s['end_time'] > $last_time){
				if($s['end_time'] > $s['end_clock_time']){
					$last_state = 0;
				}else{
					$last_state = 1;
				}
			}
		}unset($s);
		if($last_state == 0){
			return $shift;
		}else{
			return array();
		}
	}

	/** 去除不需要显示的记录 */
	private function get_show_record(&$record_array){
		if(empty($record_array)) return array();
		$now = time();
		sort($record_array);
		$data = array();
		$state = 0;
		foreach ($record_array as $record) {
			if($record['time'] <= $now){
				$data[$record['time']] = $record;
			}else if($state == 0){
				$data[$record['time']] = $record;
				$state = 1;
			}
		}unset($record);

		rsort($data);
		if(empty($data[0]['clock_time'])){
			$curr_shift = array_shift($data);
			if(isset($data[0]) && empty($data[0]['clock_time']) && $curr_shift['time'] > $now){
				$curr_shift = $data[0];
				unset($data[0]);
			}
		}else{
			$curr_shift = $data[0];
		}
		$curr_shift = array(
				'time' => date('H:i', $curr_shift['time']),
				'id' => $curr_shift['id']
			);

		$record_array = $data;
		return $curr_shift;
	}

	/** 从表单信息里面提取请假类型 */
	private function get_rest_type($com_id, $type_name){
		$type = g('ws_rest') -> get_by_name($com_id, $type_name, "id");
		return $type ? $type['id'] : "";
	}

	/** 检查年假 */
	private function count_year_rest($com_id, $user_id, $rest_hours){
		$config = g('ws_core') -> get_config($com_id , 'clear_date, work_hour');
		$clear_date = $config['clear_date'];
		$work_hour = $config['work_hour'];

		$next_clear_time = $last_clear_time = strtotime(date('Y-').$clear_date);
		if($next_clear_time >= time()){
			$last_clear_time = strtotime('-1 year', $next_clear_time);
		}
		$rest = g('ws_rest') -> get_com_user_year($com_id, $user_id, $last_clear_time);
		if(empty($rest) || ($work_hour * $rest['days'] < ($rest_hours + $rest['h_count'])) ){
			throw new \Exception("年假余额不足");
		}
		return true;
	}

	//根据请假和外出记录，获取最新的班次信息
	private function get_merge_shift($all_shift, &$all_record, &$shift_info){
		if(!empty($all_record)){
			foreach ($all_record['rest'] as &$record) {
				$record['record_type'] = 3;
			}unset($record);
			foreach ($all_record['legwork'] as &$record) {
				$record['record_type'] = 4;
			}unset($record);
			$all_record = array_merge($all_record['rest'], $all_record['legwork']);
		}

		$return = array();
		foreach ($all_shift as $key => $shift) {
			$shift_info[$shift['start_time']] = array(
					"name" => $shift['shift_name'],
					"start_time" => date('H:i', $shift['start_time']),
					"end_time" => date('H:i', $shift['end_time']),
				);
			$start_time = $shift['start_time'];
			$end_time = $shift['end_time'];
			$rest_start_time = $shift['rest_start_time'];
			$rest_end_time = $shift['rest_end_time'];
			$record_type = 0;

			foreach ($all_record as $record) {
				if($start_time >= $record['start_time'] && $start_time <= $record['end_time']){
					//记录包含班次开始时间
					$start_time = $record['end_time'];
					if(!empty($rest_start_time) && !empty($rest_end_time) && $record['end_time'] >= $rest_start_time && $record['end_time'] < $rest_end_time){
						$start_time = $rest_end_time;
					}
				}else if($end_time >= $record['start_time'] && $end_time <= $record['end_time']){
					//记录包含班次结束时间
					$end_time = $record['start_time'];
					if(!empty($rest_start_time) && !empty($rest_end_time) && $record['start_time'] > $rest_start_time && $record['start_time'] <= $rest_end_time){
						$end_time = $rest_start_time;
					}
				}
				if($start_time >= $end_time){
					$record_type = $record['record_type'];
					break;
				}
			}unset($record);
			if($end_time <= $start_time){
				//请假包含整个班次
				$shift['record_type'] = $record_type;
				$return[] = $shift;
			}else{
				//请假未包含整个班次
				if($shift['start_time'] != $start_time){
					$shift['start_time'] = $start_time;
					$shift['start_state'] = empty($shift['start_clock_time']) ? 0 : 1;
				}
				if($shift['end_time'] != $end_time){
					$shift['end_time'] = $end_time;
					$shift['end_state'] = empty($shift['end_clock_time']) ? 0 : 1;
				}
				$return[] = $shift;
			}
		}unset($shift);
		
		return $return;
	}

}
/* End of this file */