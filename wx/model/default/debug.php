<?php
/**
 * 用作远程接口调试的页面
 *
 * @author LiangJianMing
 * @create 2015-04-02
 */
include_once('global.config.php');
define('DEBUG_HOST', $_SERVER['HTTP_HOST']);

$debug_dir = dirname(__FILE__) . '/debug/';

$app = get_var_value('app');

$cls_name = $app . '_debug';
$cls_file = $debug_dir . 'cls_' . $cls_name . '.php';
!file_exists($cls_file) and die('对应的调试类不存在！');

include_once($debug_dir . 'cls_ndebug.php');
include_once($cls_file);

g($cls_name) -> entry();