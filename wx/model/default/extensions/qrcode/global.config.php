<?php
/** 
 * 微信企业号API功能扩展 配置文件
 * 
 * @author yangpz
 * @create 2014-10-18 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('QRCODE_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

include_once(QRCODE_ROOT.'phpqrcode/phpqrcode.php');

$cls_qrcode_url = QRCODE_ROOT.'cls_qrcode.php';
!class_exists('cls_qrcode') && file_exists($cls_qrcode_url) && include_once($cls_qrcode_url);

/* end of file */ 