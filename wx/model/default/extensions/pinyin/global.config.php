<?php
/** 
 * 中文拼音处理功能扩展 配置文件
 * 
 * @author yangpz
 * @create 2014-11-20
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('PINYIN_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_pinyin_url = PINYIN_ROOT.'cls_pinyin.php';
!class_exists('cls_pinyin') && file_exists($cls_pinyin_url) && include_once($cls_pinyin_url);

/* end of file */ 