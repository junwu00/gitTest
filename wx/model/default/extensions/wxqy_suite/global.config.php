<?php
/** 
 * 微信企业号API功能扩展 配置文件
 * 
 * @author yangpz
 * @create 2014-10-18 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('WXQY_SUITE_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_wxqy_url = WXQY_SUITE_ROOT.'cls_wxqy_suite.php';
!class_exists('cls_wxqy_suite') && file_exists($cls_wxqy_url) && include_once($cls_wxqy_url);


//---------------------------------------管理权限的secret-------------------------

/** CORPID */
//!defined('SC_QY_CORPID') && define('SC_QY_CORPID', 'wx1fbdcdc878ed9b99');
//!defined('SC_QY_CORPID') && define('SC_QY_CORPID', 'wx38cd49c95021c912');
/** 超级管理员：所有权限 */
//define('SC_WXQY_SUPER_MGR', 'RvJt30_cbkOLPryKehV6ei7vDnY67166N6eDBr2rSHMgnWenEFnUWP_LhkpKSbrq');
//define('SC_WXQY_SUPER_MGR', 'w42DLkcBdeTF67tNJUqyZyc-mpwuumSykjPJ03VwYsr1azzHROQoa52Rjdi9lPhN');

/* end of file */ 