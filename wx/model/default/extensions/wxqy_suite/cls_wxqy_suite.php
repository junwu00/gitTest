<?php
class cls_wxqy_suite {

	/**
	 * 获取套件令牌
	 * Enter description here ...
	 * @param unknown_type $suite_id
	 */
	public function get_suite_access_token($suite_id, $max_alive_time=60, $with_redis=TRUE) {
		$suite = g('suite') -> get_by_suite_id($suite_id);
		if(!$suite){
			log_write('suite_id 错误！');
			return;
		}
		log_write('开始获取suite_access_token');
		$with_redis && $time = g('redis') -> get(md5(REDIS_QY_EW365_SUITE_ACCESS_TOKEN_TIME.':'.$suite_id));
		
		(!isset($time) || is_null($time) || empty($time)) && $time = 0;
		$is_over_time = (time() - $time >= $max_alive_time) ? TRUE : FALSE;		//是否过期
		if ($is_over_time) {
			try{
				$token =  $this -> update_suite_access_token($suite['suite_id'], $suite['suite_secret'], $suite['suite_ticket']);
				$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_SUITE_ACCESS_TOKEN.':'.$suite_id), $token, 3600);
				$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_SUITE_ACCESS_TOKEN_TIME.':'.$suite_id),time(),3600);
				return $token;
				
			} catch (SCException $e) {
				throw $e;
				
			} catch (RedisException $e) {
				if ($with_redis) {
					return $this -> get_suite_access_token($suite_id, $max_alive_time, FALSE);	
				}
				throw $e;
			}
			
		} else {
			$token = g('redis') -> get(md5(REDIS_QY_EW365_SUITE_ACCESS_TOKEN.':'.$suite_id));
			log_write('获取成功，token：'.$token);
			return $token;
		}
	}
	
	
	/**
	 * 更新套件令牌
	 * Enter description here ...
	 * @param unknown_type $suite_id     套件ID
	 * @param unknown_type $suite_secret 套件secret
	 * @param unknown_type $suite_ticket 套件ticket
	 * @throws SCException
	 */
	public function update_suite_access_token($suite_id,$suite_secret,$suite_ticket){
		log_write('开始更新套件令牌：suite_access_token');
		try{
			$url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token';
			$data = array(
				'suite_id' => $suite_id,
				'suite_secret'=>$suite_secret,
				'suite_ticket'=>$suite_ticket
			);
			$result = $this -> do_post($url,$data,true);
			if($result['suite_access_token']){
				log_write('更新套件令牌成功！token:'.$result['suite_access_token']);
				return $result['suite_access_token'];
			}
		}catch(SCException $e){
			log_write('更新套件令牌失败！');
			throw $e;
		}
	}
	
	/**
	 * 获取预授权码
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌
	 * @param unknown_type $appid				套件需要授权的应用的ID数组，默认为所有
	 * @throws SCException
	 */
	public function get_pre_auth_code($suite_id,$suite_access_token,$appid=array()){
		log_write('开始获取预授权码：pre_auth_code');
		try{	
			$url ='https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token='.$suite_access_token;
			$data=array(
				'suite_id' => $suite_id,
				'appid'=>$appid
			);
			$result =$this -> do_post($url, $data);
			if($result['pre_auth_code']){
				log_write('获取预授权码成功');
				return $result['pre_auth_code'];
			}
		}catch(SCException $e){
			log_write('获取预授权码失败');
			throw $e;
		}
	}
	
	/**
	 * 获取企业号永久授权码（包含企业信息，企业授权应用信息）
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌说
	 * @param unknown_type $suite_access_token	临时授权码（由企业确认授权之后，回调获得）
	 * @throws SCException
	 */
	public function get_permanent_code($suite_id,$suite_access_token,$auth_token){
		log_write('开始获取企业永久授权码及企业信息：permanent_code');
		try{	
			$url ='https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token='.$suite_access_token;
			$data=array(
				'suite_id' => $suite_id,
				'auth_code'=>$auth_token
			);
			$result =$this -> do_post($url, $data);
			if($result){
				log_write('获取永久授权码及企业信息成功');
				return $result;
			}
		}catch(SCException $e){
			log_write('获取永久授权码及企业信息失败');
			throw $e;
		}
	}
	
	
	/**
	 * 获取企业信息
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌
	 * @param unknown_type $auth_corpid			企业ID
	 * @param unknown_type $permanent_code		企业永久授权码
	 * @throws SCException
	 */
	public function get_auth_corp_info($suite_id,$suite_access_token,$auth_corpid,$permanent_code){
		log_write('开始获取企业信息');
		try{	
			$url ="https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token={$suite_access_token}";
			$data=array(
				'suite_id' => $suite_id,
				'auth_corpid'=>$auth_corpid,
				'permanent_code'=>$permanent_code
			);
			$result =$this -> do_post($url, $data);
			log_write('获取企业信息成功');
			return $result;
		}catch(SCException $e){
			log_write('获取企业信息失败');
			throw $e;
		}
	}
	
	/**
	 * 获取企业应用信息
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌
	 * @param unknown_type $auth_corpid			企业ID
	 * @param unknown_type $permanent_code		企业永久授权码
	 * @param unknown_type $agentid				企业内应用ID
	 * @throws SCException
	 */
	public function get_auth_corp_agent($suite_id,$suite_access_token,$auth_corpid,$permanent_code,$agentid){
		log_write('开始获取企业应用信息');
		try{	
			$url ='https://qyapi.weixin.qq.com/cgi-bin/service/get_agent?suite_access_token={$suite_access_token}';
			$data=array(
				'suite_id' => $suite_id,
				'auth_corpid'=>$auth_corpid,
				'permanent_code'=>$permanent_code,
				'agentid' => $agentid
			);
			$result =$this -> do_post($url, $data);
			log_write('获取企业应用信息成功');
			return $result;
		}catch(SCException $e){
			log_write('获取企业应用信息失败');
			throw $e;
		}
	}
	
	/**
	 * 设置企业应用信息
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌
	 * @param unknown_type $auth_corpid			企业ID
	 * @param unknown_type $permanent_code		企业永久授权码
	 * @param unknown_type $agent				企业内应用信息
	 * @throws SCException
	 */
	public function set_auth_corp_agent($suite_id,$suite_access_token,$auth_corpid,$permanent_code,$agent){
		log_write('设置企业应用信息');
		try{	
			$url ='https://qyapi.weixin.qq.com/cgi-bin/service/set_agent?suite_access_token='.$suite_access_token;
			$data=array(
				'suite_id' => $suite_id, 
				'auth_corpid'=>$auth_corpid,
				'permanent_code'=>$permanent_code,
				'agent' => $agent
			);
			$result =$this -> do_post($url, $data);
			log_write('设置企业应用信息成功');
			return true;
		}catch(SCException $e){
			log_write('设置企业应用信息失败');
			throw $e;
		}
	}
	
	/**
	 * 更新企业号access_token
	 * Enter description here ...
	 * @param unknown_type $suite_id			套件ID
	 * @param unknown_type $suite_access_token	套件令牌
	 * @param unknown_type $auth_corpid			企业ID
	 * @param unknown_type $permanent_code		企业永久授权码
	 * @throws SCException
	 */
	public function update_auth_access_token($suite_id,$suite_access_token,$auth_corpid,$permanent_code){
		log_write('更新企业号access_token');
		try{	
			$url ='https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token='.$suite_access_token;
			$data=array(
				'suite_id' => $suite_id,
				'auth_corpid'=>$auth_corpid,
				'permanent_code'=>$permanent_code
			);
			$result =$this -> do_post($url, $data);
			
			if($result){
				log_write('获取企业号access_token成功');
				return $result['access_token'];
			}else{
				log_write('获取企业号access_token失败');
				return false;
			}
		}catch(SCException $e){
			log_write('获取企业号access_token失败');
			throw $e;
		}
	}
	/**
	 * POST数据到对应的微信URL
	 * @param unknown_type $url			POST请求的目标URL
	 * @param unknown_type $data		请求发送的数据
	 * @param unknown_type $return_all_info	数据是否返回所有数据，不判断errcode
	 * @param unknown_type $is_json		数据是否为json格式，默认为FALSE，即数据为数组
	 * @return 数组形式的结果
	 */
	private function do_post($url, $data, $return_all_info = FALSE,$is_json=FALSE) {
		if ($is_json) {
			$json_data = $data;
		} else {
			$data = urlencode_array($data);
			$json_data = urldecode(json_encode($data));	
			log_write($json_data);
		}
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		if(defined('CURL_SSLVERSION_TLSv1')){
	    	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$info = curl_exec($ch);
		
		if (curl_errno($ch)) {
			log_write('POST请求失败:' .curl_error($ch));
			curl_close($ch);
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$PostExp);
		}
		
		curl_close($ch);
		$info = json_decode($info, TRUE);
		if (is_null($info) || empty($info)) {
			throw new SCException('解析POST返回数据失败');
		} 
		if(!$return_all_info){
			if (isset($info['errcode']) && $info['errcode'] != 0) {
				throw new cls_wxqy_exp($info['errcode']);
			}
		}
		return $info;
	}
	
	/**
	 * GET数据到对应的URL，需要返回格式为json字符串
	 * @param unknown_type $url	请求的目标URL
	 * @return
	 */
	private function do_get($url) {
		$content = get_content($url, FALSE);
		if (is_null($content) || empty($content)) {
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$GetExp);
		}
		$content = json_decode($content, TRUE);
		if (isset($content['errcode']) && $content['errcode'] != 0) {
			throw new cls_wxqy_exp($content['errcode']);
		}
		return $content;
	}

}  