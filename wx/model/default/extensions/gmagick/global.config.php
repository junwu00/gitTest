<?php
/**
 * @copyright 2014
 * @description: gmagick图片处理
 * @file: global.config.php
 * @author: Liang Jian Ming
 * @charset: UTF-8
 * @time: 2014-03-05 16:43:57
 * @version 1.0.0
**/

!defined('SYSTEM') && die('ACCESS DENIED');

//定义插件名称
define('GMAGICK', 			'GMAGICK');	
//定义插件版本
define('GMAGICK_VERSION', 	'1.0.0');
//定义插件根目录
define('GMAGICK_ROOT', 		preg_replace('/[\/\\\\]+/', '/', dirname(__FILE__).'/'));

//定义产品图片尺寸集,任何尺寸定义都必须严格按照此格式,仅允许出现字符：[0-9]%x
if(!isset($GLOBALS['GMAGIC_ITEM_SIZE'])){
	$GLOBALS['GMAGIC_ITEM_SIZE'] =	array('100%x100%', '616x436', '548x386', '136x136', '120x120', '100x80', '64x50');	
}
//定义店铺图片尺寸集,任何尺寸定义都必须严格按照此格式,仅允许出现字符：[0-9]%x
if(!isset($GLOBALS['GMAGIC_STORE_SIZE'])){
	$GLOBALS['GMAGIC_STORE_SIZE'] =	array('100%x100%', '172x172', '160x160', '154x154', '150x150', '134x134');	
}
//定义成功案例图片尺寸集,任何尺寸定义都必须严格按照此格式,仅允许出现字符：[0-9]%x
if(!isset($GLOBALS['GMAGIC_SUCCESS_SIZE'])){
	$GLOBALS['GMAGIC_SUCCESS_SIZE'] =	array('100%x100%', '175x110');	
}
//定义用户头像尺寸集,任何尺寸定义都必须严格按照此格式,仅允许出现字符：[0-9]%x
if(!isset($GLOBALS['GMAGIC_USER_SIZE'])){
	$GLOBALS['GMAGIC_USER_SIZE'] =	array('100%x100%', '158x158', '140x140');
}
//定义admin管理后台的店铺环境图片尺寸集,任何尺寸定义都必须严格按照此格式,仅允许出现字符：[0-9]%x
if(!isset($GLOBALS['GMAGIC_ADMIN_STORE_SIZE'])){
	$GLOBALS['GMAGIC_ADMIN_STORE_SIZE'] =	array('100%x100%');
}

$cls_gmagick_url = GMAGICK_ROOT.'cls_gmagick.php';
!class_exists('cls_gmagick') && file_exists($cls_gmagick_url) && include_once($cls_gmagick_url);

/** 
 * 图片智能缩放处理
 * 
 * @access public
 * @param: string $file 原图片全路径
 * @param: string $to_file 图片保存路径
 * @param: integer|string $width 缩放至此宽度，支持百分比
 * @param: integer|string $height 缩放至此高度，支持百分比
 * @return boolean
 */
if(!is_exists('call_gm_resize', 'function')){
	function call_gm_resize($file, $to_file, $width, $height){
		return g('gmagick') -> gm_resize($file, $to_file, $width, $height);
	}
}

/** 
 * 构造图片不同尺寸的存储目录
 * 
 * @access public
 * @param: string $file 原图片绝对路径
 * @param: string $to_dir 处理后的图片保存到该目录
 * @param: array $size_global 图片需要处理为该尺寸集合
 * @return mixed 任何失败都会返回FALSE，否则返回array(尺寸[string] => 图片的存储路径[string])
 */
if(!is_exists('call_gm_create_path', 'function')){
	function call_gm_create_path($file, $to_dir, $size_global){
		if(!is_string($file)) return FALSE;
		if(!is_exists($file, 'file')) return FALSE;
		if(!isset($GLOBALS[$size_global]) || !is_array($GLOBALS[$size_global])) return FALSE;
		$sizes = $GLOBALS[$size_global];
		$to_dir = preg_replace('/[\\\\\/]+$/', '', $to_dir).'/';
		if(!preg_match('/[\s\S]+\/([^\/]+)(\.[^\/]+)$/', $file, $match)) return FALSE;
		$prefix_name = $match[1];
		$suffix_name = $match[2];
		
		$save_path = array();
		foreach($sizes as $val){
			$newname = md5($prefix_name.time());
			$dir_length = rand(1, 3);
			$first_dir = '';
			for($i = 0; $i < $dir_length; $i++) $first_dir .= $newname[rand(0, 31)];
			$first_dir = $to_dir.$first_dir;
			if(!is_exists($first_dir, 'file')){
				if(!mkdir($first_dir)) return FALSE;
			}
			$first_dir .= '/';
			
			$dir_length = rand(1, 3);
			$second_dir = '';
			for($i = 0; $i < $dir_length; $i++) $second_dir .= $newname[rand(0, 31)];
			$second_dir = $first_dir.$second_dir;
			if(!is_exists($second_dir, 'file')){
				if(!mkdir($second_dir)) return FALSE;
			}
			$second_dir .= '/';
			
			$save_path[$val] = $second_dir.$newname.$suffix_name;
		}
		unset($val);
		return $save_path;
	}
}

/** 
 * 自动按照已经定义的产品图片尺寸集合进行图片缩放处理
 * 
 * @access public
 * @param: string $file 原图片绝对路径
 * @param: string $to_dir 处理后的图片保存到该目录
 * @param: string $size_global 全局图片尺寸参数下标[如:GMAGIC_ITEM_SIZE]
 * @return mixed 任何由非call_gm_resize导致的错误都会返回FALSE，否则返回array('size' => 尺寸[string], 'file' => 图片绝对路径[string], 'result' => 处理结果[boolean])
 */
if(!is_exists('call_gm_auto_resize', 'function')){
	function call_gm_auto_resize($file, $to_dir, $size_global='GMAGIC_ITEM_SIZE'){
		if(!is_string($file) || !is_string($to_dir)) return FALSE;
		if(!is_exists($file, 'file') || !is_exists($to_dir, 'file')) return FALSE;
		$paths = call_gm_create_path($file, $to_dir, $size_global);
		if(!is_array($paths)) return FALSE;
		
		$result = array();
		foreach($paths as $key => $val){
			if(!preg_match('/^([\d%]+)x([\d%]+)$/', $key, $match)) return FALSE;
			$width = $match[1];
			$height = $match[2];
			unset($match);
			
			$is_resize = call_gm_resize($file, $val, $width, $height);
			$result[] = array(
				'size' => $key,
				'file' => $val,
				'result' => $is_resize
			);
		}
		unset($key);
		unset($val);
		return $result;
	}
}

/* End of file global.config.php */ 