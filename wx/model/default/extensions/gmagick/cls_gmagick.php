<?php
/** 
 * gmagick图片处理类 
 * 
 * 使用gmagick扩展进行图片处理的类
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

class cls_gmagick{
	private $image = NULL;						//图片对象
	
  	/** 
  	 * 析构函数
  	 * 
  	 * @access public
  	 * @return viod
  	 */
  	public function __destruct(){
  		$this -> gm_destroy();
  	}
  	
  	/** 
  	 * 加载图片创建图片对象
  	 * 
  	 * @access public
  	 * @param: string $file 图片全路径
  	 * @return 
  	 */
  	public function load_image($file=''){
  		$this -> gm_destroy();
  		if(!is_string($file) || !is_exists($file, 'file')) return FALSE;
  		try{
  			$this -> image = new Gmagick($file);
  		}catch(GmagickException $e){
  			return FALSE;
  		}
  		return TRUE;
  	}
  	
  	/** 
  	 * 缩小或放大图片
  	 * 
  	 * @access public
  	 * @param: string $file 原图片全路径
  	 * @param: string $to_file 输出全路径
  	 * @param: string $width 将图片调整到此宽度
  	 * @param: string $height 将图片调整到此高度
  	 * @return boolean
  	 */
  	public function gm_resize($file, $to_file, $width, $height){
  		//初始化图片对象
  		if(!$this -> load_image($file)) return FALSE;
  		//参数严格验证
  		if(!is_string($to_file) || is_empty($to_file)) return FALSE;
  		if(is_empty($width) || is_empty($height)) return FALSE;
  		if(preg_match('/[^\.%\d]+/', $width) || preg_match('/[^\.%\d]+/', $height)) return FALSE;
  		
  		//获取图片的宽度与高度
  		try{
  			$ori_width = $this -> image -> getimagewidth();
  			$ori_height = $this -> image -> getimageheight();
  		}catch(GmagickException $e){
  			return FALSE;
  		}
  		
  		//支持按百分比参数
  		preg_match('/%$/', $width) && $width = intval($ori_width * intval(preg_replace('/%$/', '', $width)) / 100);
  		preg_match('/%$/', $height) && $height = intval($ori_height * intval(preg_replace('/%$/', '', $height)) / 100);
  		
  		$width = intval($width);
  		$height = intval($height);
  		
  		if($width == 0 || $height == 0) return FALSE;
  		
  		$x = 0;
  		$y = 0;
  		
  		//根据缩放比例裁剪中央区域的图片算法
  		if($width >= $ori_width && $height >= $ori_height){
  			if($ori_width * $height / $width <= $ori_height){
  				$tmp_height = intval($ori_width * $height / $width);
  				$y = intval(($ori_height - $tmp_height) / 2);
  				$ori_height = $tmp_height;
  			}else{
  				$tmp_width = intval($ori_height * $width / $height);
  				$x = intval(($ori_width - $tmp_width) / 2);
  				$ori_width = $tmp_width;
  			}
  			
  		}elseif($width < $ori_width && $height < $ori_height){
  			if($ori_width * $height / $width <= $ori_height){
  				$tmp_height = intval($ori_width * $height / $width);
  				$y = intval(($ori_height - $tmp_height) / 2);
  				$ori_height = $tmp_height;
  			}else{
  				$tmp_width = intval($ori_height * $width / $height);
  				$x = intval(($ori_width - $tmp_width) / 2);
  				$ori_width = $tmp_width;
  			}
  		}else{
  			if($width > $ori_width){
  				$tmp_height = intval($ori_width * $height / $width);
  				$y = intval(($ori_height - $tmp_height) / 2);
  				$ori_height = $tmp_height;
  			}else{
  				$tmp_width = intval($ori_height * $width / $height);
  				$x = intval(($ori_width - $tmp_width) / 2);
  				$ori_width = $tmp_width;
  			}
  		}
  		
  		//执行图片裁剪及缩放处理
  		try{
  			$this -> image -> cropimage($ori_width, $ori_height, $x, $y)
  						   -> thumbnailImage($width, $height)
  						   -> setCompressionQuality(90)
  						   -> write($to_file);
  			return TRUE;
  		}catch(GmagickException $e){
  			return FALSE;
  		}
  	}
  	
  	/** 
  	 * 销毁图片对象，清空缓存
  	 * 
  	 * @access public
  	 * @return boolean
  	 */
  	public function gm_destroy(){
  		if(is_object($this -> image)){
  			try{
  				$this -> image -> destroy();
  			}catch(GmagickException $e){
  				return FALSE;
  			}
  		}
  		return TRUE;
  	}
}

/* End of file cls_gmagick.php */ 