<?php
/** 
 * redis配置文件
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

$redis_file = dirname(dirname(dirname(SYSTEM_ROOT))) . "/install_conf/redis.config.json";
$conf = json_decode(file_get_contents($redis_file), true);
!is_array($conf) and $conf = array();

define('REDIS_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //扩展根目录

$cls_redis_url = REDIS_ROOT.'cls_redis.php';
class_exists('redis') && !class_exists('cls_redis') && file_exists($cls_redis_url) && include_once($cls_redis_url);

$REDIS_CONFIG_LIST = array(
    array(
        'host' => $conf["host"],
        'port' => $conf["port"],
        'pass' => isset($conf["pass"])?$conf["pass"]:'',
    ),
);
!isset($GLOBALS['REDIS_CONFIG_LIST']) && $GLOBALS['REDIS_CONFIG_LIST'] = $REDIS_CONFIG_LIST;

/* End of file global.config.php */ 