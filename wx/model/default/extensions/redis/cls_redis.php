<?php
/** 
 * redis操作类 
 * 
 * 仅封装了简单常用的redis操作，其他复杂操作可以使用源对象操作
 * 
 * @author LiangJianMing
 * @create 2014-06-07 
 * @version 1.0
 */
class cls_redis {
	private $redis=NULL;	//redis源对象
	private $host=NULL;		//redis服务器地址
	private $port=NULL;		//redis服务器端口
    private $pass=NULL;		//redis服务器密码
	
	/** 
	 * 构造函数，初始化redis连接
	 * 
	 * @access public
	 * @param string $object_mark 对象标记
	 * @return void
	 */
	public function __construct($object_mark=NULL){
		$redis_config_list = $GLOBALS['REDIS_CONFIG_LIST'];
		$redis_config = '';
		if(!empty($redis_config_list) && is_array($redis_config_list)){
			if(!empty($object_mark)){
				if(isset($redis_config_list[$object_mark]) && !empty($redis_config_list[$object_mark]) && is_array($redis_config_list[$object_mark])){
					$redis_config = $redis_config_list[$object_mark];
				}
			}
			//取第一个配置
			if(empty($redis_config)){
				$redis_config = array_shift($redis_config_list);
			}else if(!is_array($redis_config)){
				$redis_config = array_shift($redis_config_list);
			}
			
			$this -> host = isset($redis_config['host']) ? $redis_config['host'] : '127.0.0.1';
			$this -> port = isset($redis_config['port']) ? $redis_config['port'] : '6379';
            $this -> pass = isset($redis_config['pass']) ? $redis_config['pass'] : '';
		
			try {
				$this -> redis = new redis();
				$this -> redis -> connect($this -> host, $this -> port);
                $this -> pass && $this -> redis -> auth($this -> pass); //设置密码
			}catch(RedisException $e) {}
		}
	}
	
	/** 
	 * 判断redis是否已经连接
	 * 
	 * @access public
	 * @return boolean 已连接返回TRUE，否则返回FALSE
	 */	
	public function is_connected() {
		if(!is_object($this -> redis)) return FALSE;
		try{
			$this -> redis -> ping();
		}catch(RedisException $e){
			return FALSE;
		}
		return TRUE;
	}
	
	/** 
	 * 重新连接redis
	 * 
	 * @access public
	 * @return void
	 */
	public function reconnect() {
		if (!$this -> is_connected()) {
			try {
				$this -> redis -> connect($this -> host, $this -> port);
                $this -> pass && $this -> redis -> auth($this -> pass); //设置密码
			}catch(RedisException $e){
				return FALSE;
			}
		}
	}
	
	/** 
	 * 设置缓存数据,仅支持字符串及整型
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $value 缓存变量值
	 * @param string $ttl 缓存生存时间，单位:秒
	 * @return boolean 成功返回TRUE,失败返回FALSE
	 */
	public function set($key, $value, $ttl=3600) {
		if(!is_string($key) || (!is_string($value) && !is_integer($value))) return FALSE;
		$ttl = intval($ttl);
		
		try {
			$result = $this -> redis -> set($key, $value);
			$ttl > 0 && $this -> redis -> setTimeout($key, $ttl);
		}catch(RedisException $e){
			return FALSE;
		}
		return $result ? TRUE : FALSE;
	}

	/** 
	 * 获取缓存变量值
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return mixed 成功返回变量值，失败返回FALSE
	 */
	public function get($key) {
		if(!is_string($key)) return FALSE;
		try {
			$result = $this -> redis -> get($key);
		}catch(RedisException $e){
			return FALSE;
		}
		return $result ? $result : FALSE;
	}
	
	/** 
	 * 带生存时间的设置缓存数据，仅支持字符串及整型
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $value 缓存变量值
	 * @param string $ttl 缓存生存时间，单位:秒
	 * @return boolean 成功返回TRUE,失败返回FALSE
	 */
	public function setex($key, $value, $ttl = 3600){
		if(!is_string($key) || (!is_string($value) && !is_integer($value))) return FALSE;
		$ttl = intval($ttl);
		
		try {
			$result = $this -> redis -> setex($key, $ttl, $value);
		}catch(RedisException $e){
			return FALSE;
		}
		return $result ? TRUE : FALSE;
	}
	
	
	/** 
	 * 删除一个缓存变量
	 * 
	 * @access public
	 * @param string $key 缓存变量值
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function delete($key) {
		if(!is_string($key)) return FALSE;
		try {
			$status = $this -> redis -> delete($key);
		}catch(RedisException $e){
			return FALSE;
		}
		return  $status ? TRUE : FALSE;
	}
	
	/** 
	 * 清空redis中的所有数据
	 * 
	 * @access public
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function clear() {
		try {
			return $this -> redis -> flushAll() ? TRUE : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}
	
	/** 
	 * 将缓存变量放入redis队列，仅支持字符串及整型
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param string $value 缓存变量值
	 * @param boolean $right 是否从右边入列
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function push($key, $value, $right=TRUE) {
		if(!is_string($key) || (!is_string($value) && !is_integer($value))) return FALSE;
		
		try {
			$result = $right ? $this -> redis -> rPush($key, $value) : $this -> redis -> lPush($key, $value);
			return $result ? TRUE : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}
	/** 
	 * 缓存变量出列
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param boolean $left 是否从左边出列
	 * @return boolean 成功返回缓存变量值，失败返回FALSE
	 */
	public function pop($key , $left=TRUE) {
		if(!is_string($key)) return FALSE;
		
		try {
			$value = $left ? $this -> redis -> lPop($key) : $this -> redis -> rPop($key);
			return $value ? $value : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}
	
	/** 
	 * 缓存变量自增
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function increase($key) {
		if(!is_string($key)) return FALSE;
		try {
			return $this -> redis -> incr($key) ? TRUE : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}

	/** 
	 * 缓存变量自减
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function decrease($key) {
		if(!is_string($key)) return FALSE;
		try {
			return $this -> redis -> decr($key) ? TRUE : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}
	
	/** 
	 * 判断缓存变量是否已经存在
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return boolean 存在返回TRUE，否则返回FALSE
	 */
	public function exists($key) {
		if(!is_string($key)) return FALSE;
		try {
			return $this -> redis -> exists($key) ? TRUE : FALSE;
		}catch(RedisException $e){
			return FALSE;
		}
	}
	
	/** 
	 * 返回redis源对象
	 * 
	 * @access public
	 * @return object
	 */
	public function get_handler() {
		return $this -> redis;
	}
}

/* End of file cls_redis.php */ 