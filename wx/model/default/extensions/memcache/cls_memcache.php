<?php
/** 
 * 单memcache服务器操作类 
 * 
 * 仅封装了简单常用的memcache操作，其他复杂操作可以使用源对象操作
 * 
 * @author LiangJianMing
 * @create 2014-06-07 
 * @version 1.0
 */
class cls_memcache {
	private $memcache=NULL;	//memcache源对象
	private $host=NULL;			//memcache服务地址
	private $port=NULL;			//memcache服务端口
	private $weight=NULL;		//memcache缓存权重
	private $connected=NULL;	//是否已经连接
	
	/** 
	 * 构造函数，初始化memcache连接
	 * 
	 * @access public
	 * @param string $object_mark 对象标记
	 * @return void
	 */
	public function __construct($object_mark=NULL){
		$memcache_config_list = $GLOBALS['MEMCACHE_CONFIG_LIST'];
		$memcache_config = '';
		if(!empty($memcache_config_list) && is_array($memcache_config_list)){
			if(!empty($object_mark)){
				if(isset($memcache_config_list[$object_mark]) && !empty($memcache_config_list[$object_mark]) && is_array($memcache_config_list[$object_mark])){
					$memcache_config = $memcache_config_list[$object_mark];
				}
			}
			//取第一个配置
			if(empty($memcache_config)){
				$memcache_config = array_shift($memcache_config_list);
			}else if(!is_array($memcache_config)){
				$memcache_config = array_shift($memcache_config_list);
			}
			
			$this -> host 	= isset($memcache_config['host']) ? $memcache_config['host'] : '127.0.0.1';
			$this -> port 	= isset($memcache_config['port']) ? $memcache_config['port'] : 11211;
			$this -> weight = isset($memcache_config['weight']) ? $memcache_config['weight'] : 3;
			
			$this -> memcache = new Memcache();
			$this -> connected = $this -> memcache -> addServer($this -> host, $this -> port, $this -> weight);
		}
	}
	
	/** 
	 * 是否已经连接
	 * 
	 * @access public
	 * @return boolean
	 */
	public function is_connected() {
		return $this -> connected ? TRUE : FALSE;
	}
	
	/** 
	 * 重新连接memcache
	 * 
	 * @access public
	 * @return void
	 */
	public function reconnect() {
		if(!$this -> is_connected()) {
			!is_object($this -> memcache) && $this -> memcache = new Memcached();
			$this -> connected = $this -> memcache -> addServer($this -> host, $this -> port, $this -> weight);
		} 
	}
	
	/** 
	 * 设置缓存数据,可支持任意类型变量
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @param mixed $value 缓存变量值
	 * @param string $ttl 缓存生存时间，单位:秒
	 * @return boolean 成功返回TRUE,失败返回FALSE
	 */
	public function set($key, $value, $ttl=3600) {
		if(!is_string($key)) return FALSE;
		$ttl = intval($ttl);
		
		$result = $this -> memcache -> set($key, $value, 0, $ttl);
		return $result ? TRUE : FALSE;
	}

	/** 
	 * 获取缓存变量值
	 * 
	 * @access public
	 * @param string $key 缓存变量名
	 * @return mixed 成功返回变量值，失败返回FALSE
	 */
	public function get($key) {
		if(!is_string($key)) return FALSE;
		$result = $this -> memcache -> get($key);
		return $result ? $result : FALSE;
	}
	
	/** 
	 * 删除一个缓存变量
	 * 
	 * @access public
	 * @param string $key 缓存变量值
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function delete($key) {
		if(!is_string($key)) return FALSE;
		return $this -> memcache -> delete($key) ? TRUE : FALSE;
	}
	
	/** 
	 * 清空memcache中的所有数据
	 * 
	 * @access public
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function clear() {
		return $this -> memcache -> flush() ? TRUE : FALSE;
	}

	/** 
	 * 返回memcache源对象
	 * 
	 * @access public
	 * @return object
	 */
	public function get_handler() {
		return $this -> memcache;
	}
}

/* End of file cls_memcache.php */ 