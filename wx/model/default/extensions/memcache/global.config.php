<?php
/** 
 * memcache配置文件
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('MEMCACHE_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //扩展根目录

$cls_memcache_url = MEMCACHE_ROOT.'cls_memcache.php';
class_exists('Memcache') && !class_exists('cls_memcache') && file_exists($cls_memcache_url) && include_once($cls_memcache_url);
$MEMCACHE_CONFIG_LIST = array(
	array(
		'host' 		=> '127.0.0.1',
		'port' 		=> 11211,
		'weight' 	=> 3
	),
);
!isset($GLOBALS['MEMCACHE_CONFIG_LIST']) && $GLOBALS['MEMCACHE_CONFIG_LIST'] = $MEMCACHE_CONFIG_LIST;

/* End of file global.config.php */ 