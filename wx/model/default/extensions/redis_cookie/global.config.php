<?php
/** 
 * redis_rcookie扩展的配置文件 
 * 
 * 仅能通过此文件调用redis_cookie
 * 必须保证有可用的redis缓存机制
 * 
 * @author LiangJianMing
 * @create 2015-01-29
 * @version 1.0
 */
 
!defined('SYSTEM') && die('Access Deny');

//根目录
define('RCOOKIE_ROOT', dirname(__FILE__) . '/');
//cookie的最大生存时间，6天
define('RCOOKIE_MAX_LIFE_TIME', 6 * 24 * 3600);
//客户端cookie_id的生存时间，1年
define('RCOOKIE_ID_VALID_TIME', 365 * 24 * 3600);
//客户端cookie_id的变量名
define('RCOOKIE_ID_CLIENT_VAR', 'RCOOKIEID');
//客户端cookie_id的保存domain
define('RCOOKIE_ID_CLIENT_HOST', SYSTEM_COOKIE_DOMAIN);
//生成缓存变量名的加盐值
define('RCOOKIE_KEY_PREFFIX', 'RCOOKIE');

$cls_url = RCOOKIE_ROOT.'cls_rcookie.php';
!class_exists('cls_rcookie') && file_exists($cls_url) && include_once($cls_url);

/* End of file global.config.php */ 
