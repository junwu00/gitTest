<?php
/** 
 *     依赖于Redis的服务端cookie操作类，
 * 将cookie数据保存在服务端，模拟session机制
 * 
 * @author LiangJianMing
 * @create 2015-01-29
 * @version 1.0
 */

class cls_rcookie {
	//rcookie每个键值，包含rcookie_id在redis中的最大生存时间
	private $max_life_time;
	//作为生成uid的标识前缀
	private $key_preffix = NULL;
	//客户端cookie_id的变量名
	private $id_client_var = NULL;
	//客户端cookie_id的保存domain
	private $id_client_host = NULL;
	//客户端cookie_id的生存时间
	private $id_valid_time = NULL;
	//当前用作服务端保存cookie数据的哈希名
	private $rcookie_id = NULL;
	
  	/** 
  	 * 构造函数
  	 * 
  	 * @access public
  	 * @return void
  	 */
	public function __construct() {
		$this -> max_life_time = RCOOKIE_MAX_LIFE_TIME ? RCOOKIE_MAX_LIFE_TIME : (24 * 3600);
  		$this -> key_preffix   = RCOOKIE_KEY_PREFFIX ? RCOOKIE_KEY_PREFFIX : 'RCOOKIE';
  		$this -> id_valid_time = RCOOKIE_ID_VALID_TIME ? RCOOKIE_ID_VALID_TIME : (365 * 24 * 3600);
  		$this -> id_client_var = RCOOKIE_ID_CLIENT_VAR ? RCOOKIE_ID_CLIENT_VAR : 'RCOOKIEID';
  		$this -> id_client_host = RCOOKIE_ID_CLIENT_HOST ? RCOOKIE_ID_CLIENT_HOST : $_SERVER['HTTP_HOST'];
	}

	public function start() {
		$this -> rcookie_id();
	}

	/**
	 * 获取或对rcookie赋值
	 *
	 * @access public
	 * @param integer $id 新的rcookie_id
	 * @return string
	 */
	public function rcookie_id($id='') {
		if (!empty($id)) {
			$this -> rcookie_id = $id;
			$this -> save_id();
			$rcookie_id = $id;
		}else {
			$rcookie_id = $this -> get_id();
		}
		return $rcookie_id;
	}

	/**
	 * 获取指定rcookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return mixed 失败返回FALSE，否则返回string
	 */
	public function get($name='') {
		if (empty($name)) return FALSE;
		$hash = $this -> rcookie_id();
		if (empty($hash)) return FALSE;

		$key = $this -> get_cache_key($name);
		$redis = g('redis') -> get_handler();
		$ret = $redis -> hGet($hash, $key);

		if ($ret === FALSE) return FALSE;
		
		$ret = json_decode($ret, TRUE);
		if (!is_array($ret) or empty($ret)) return FALSE;

		if ($ret['ttl'] < time()) {
			$this -> delete($key);
			return FALSE;
		}
		$this -> renew();
		$ret = json_decode($ret['data'], TRUE);
		!is_array($ret) and $ret = FALSE;

		return $ret;
	}

	/**
	 * 设置指定rcookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @param mixed $value 缓存数据
	 * @param string $ttl 有效的生存时间
	 * @return boolean
	 */
	public function set($name, $value, $ttl=3600) {
		if (empty($name)) return FALSE;

		$hash = $this -> rcookie_id();
		if (empty($hash)) return FALSE;

		is_array($value) and $value = json_encode($value);
		$real_ttl = $ttl <= $this -> max_life_time ? $ttl : $this -> max_life_time;
		$eff_end = time() + $real_ttl;
		$cache_data = array(
			'data' => $value,
			'ttl' => $eff_end,
		);

		$key = $this -> get_cache_key($name);
		$redis = g('redis') -> get_handler();
		$json_data = json_encode($cache_data);
		$ret = $redis -> hSet($hash, $key, $json_data);

		if ($ret === FALSE) return FALSE;
		$this -> renew();

		return TRUE;
	}

	/**
	 * 检测cookie_id是否已经被初始化
	 *
	 * @access public
	 * @return boolean
	 */
	public function check_id_exists() {
		if (isset($_COOKIE[$this -> id_client_var])) {
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * 删除指定名称的cookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return boolean
	 */
	public function delete($name='') {
		if (empty($name)) return FALSE;

		$hash = $this -> rcookie_id();
		if (empty($hash)) return FALSE;

		$key = $this -> get_cache_key($name);
		$redis = g('redis') -> get_handler();
		$ret = $redis -> hDel($hash, $key);

		return $ret;
	}

	/**
	 * 删除该rcookie下的全部数据
	 *
	 * @access public
	 * @return boolean
	 */
	public function clear() {
		$hash = $this -> rcookie_id();
		if (empty($hash)) return FALSE;

		$ret = g('redis') -> delete($hash);

		return $ret;
	}

	/**
	 * 获取rcookie_id
	 *
	 * @access private
	 * @return string
	 */
	private function get_id() {
		if (!empty($this -> rcookie_id)) {
			return $this -> rcookie_id;
		}
		
		if (isset($_COOKIE[$this -> id_client_var])) {
			$this -> rcookie_id = $_COOKIE[$this -> id_client_var];
		}else {
			$this -> rcookie_id = $this -> create_id();
			//保存rcookie_id到客户端
			$this -> save_id();
		}
		return $this -> rcookie_id;
	}

	/**
	 * 保存rcookie_id到客户端
	 *
	 * @access public
	 * @return void
	 */
	private function save_id() {
		setcookie($this -> id_client_var, $this -> rcookie_id, time() + $this -> id_valid_time, '/', $this -> id_client_host, false, TRUE);
	}

	/**
	 * 刷新cookie_id的生存时间
	 *
	 * @access private
	 * @return boolean
	 */
	private function renew() {
		$hash = $this -> rcookie_id();
		if (empty($hash)) return FALSE;

		$redis = g('redis') -> get_handler();
		$ret = $redis -> expire($hash, $this -> max_life_time);
		return $ret ? TRUE : FALSE;
	}

	/**
	 * 生成rcookie_id
	 *
	 * @access private
	 * @return string
	 */
	private function create_id() {
		$time = time();
		$ip = get_ip();
		$uid = uniqid($this -> key_preffix, TRUE);
		$rand = get_rand_string(6);
		$key_str = 'rcookie_id:ip:' . $ip . ':uid:' . $uid . ':time:' . $time . ':rand:' . $rand;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取redis缓存变量名
	 *
	 * @access private
	 * @param   
	 * @return string
	 */
	private function get_cache_key($str) {
		$key = md5($str);
		return $key;
	}
}

/* End of file cls_session.php */ 