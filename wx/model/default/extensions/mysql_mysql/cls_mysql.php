<?php
/** 
 * mysql数据库操作类
 * 
 * 该类使用了php的mysql扩展
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

class cls_mysql implements inf_db{
	protected $db_link 		= NULL;		//数据库连接
	protected $db_name 		= NULL;		//数据库名称
	protected $db_chart 		= NULL;		//数据库编码
	protected $db_host 		= NULL;		//数据库主机
	protected $db_port 		= NULL;		//数据库端口3306
	protected $db_user 		= NULL;		//数据库用户名
	protected $db_pass 		= NULL;		//数据库密码
	protected $cur_sql 		= NULL;		//当前执行的sql语句
	protected $list_sql 		= array();	//被执行过的sql集合
	
	/** 
	 * 构造函数
	 * 
	 * @access public
	 * @param string $object_mark 对象标识 
	 * @return void
	 */
	public function __construct($object_mark=NULL){
		$db_config_list = $GLOBALS['DB_CONFIG_LIST'];
		$db_config = '';
		if(!empty($db_config_list) && is_array($db_config_list)){
			if(!empty($object_mark)){
				if(isset($db_config_list[$object_mark]) && !empty($db_config_list[$object_mark]) && is_array($db_config_list[$object_mark])){
					$db_config = $db_config_list[$object_mark];
				}
			}
			//取第一个配置
			if(empty($db_config)){
				$db_config = array_shift($db_config_list);
			}else if(!is_array($db_config)){
				$db_config = array_shift($db_config_list);
			}
			
			!empty($db_config['name']) 		&& $this -> db_name = $db_config['name'];
			!empty($db_config['host']) 		&& $this -> db_host = $db_config['host'];
			!empty($db_config['user']) 		&& $this -> db_user = $db_config['user'];
			!empty($db_config['pass']) 		&& $this -> db_pass = $db_config['pass'];
			!empty($db_config['port']) 		&& $this -> db_port = $db_config['port'];
			!empty($db_config['charset']) 	&& $this -> db_chart = $db_config['charset'];
			
			$this -> connect();
		}
	}
	
	/** 
	 * 析构函数
	 * 
	 * @access public  
	 * @return void
	 */
	public function __destruct(){
		$this -> close();
	}
	
	/** 
	 * 调用该函数未定义的动态方法时，提交到该函数
	 * 
	 * @access public
	 * @param string $fun_name 函数名
	 * @param array $fun_args 调用参数
	 * @return void
	 */
  	public function __call($fun_name, $fun_args){
  		$this -> debug('['.__CLASS__.'] not define ['.$fun_name.']');
  	}
  	
	/** 
	 * 调用该函数未定义的静态方法时，提交到该函数
	 * 
	 * @access public
	 * @param string $fun_name 函数名
	 * @param array $fun_args 调用参数
	 * @return void
	 */
  	public static function __callstatic($fun_name, $fun_args){
  		$this -> debug('['.__CLASS__.'] not define static ['.$fun_name.']');
  	}
	
	/** 
	 * 输出消息并终止程序
	 * 
	 * @access private final
	 * @param: string $message 输出的消息
	 * @param: boolean $is_exit 是否终止程序,TRUE-终止、FALSE-不终止
	 * @return void
	 */
	private final function debug($message, $is_exit=TRUE){
		function_exists('debug') && debug($message, __CLASS__);
		$is_exit && exit;
	}
  	
  	/** 
  	 * 判断是否连接，若未连接则尝试连接
  	 * 
  	 * @access public
  	 * @return void
  	 */
	public function reconnect(){
		!$this -> is_connect() && $this -> connect();
	}
	
	/** 
	 * 连接数据库
	 * 
	 * @access public
	 * @return void
	 */
	public function connect(){	
		$db_link = @mysql_connect($this -> db_host.(empty($this -> db_port) ? '' : ':'.$this -> db_port), $this -> db_user, $this -> db_pass);
		if(!$db_link || empty($db_link)) $this -> debug('Mysql Connect Error: '.mysql_error());
		$this -> db_link = $db_link;
		!empty($this -> db_name) && $this -> select_db($this -> db_name);
		!empty($this -> db_chart) && $this -> select_charct($this -> db_chart);
	}
	
	/** 
	 * 是否连接数据库
	 * 
	 * @access public
	 * @param: boolean $is_real 是否真实连接
	 * @return boolean
	 */
	public function is_connect($is_real=FALSE){
		$db_link = $this -> db_link;
		if(empty($db_link)) return FALSE;
		if(!$is_real){
			if(is_resource($db_link)) return TRUE;
		}else{
			return $this -> query('show tables') ? TRUE : FALSE;
		}
		return FALSE;
	}
	
	/** 
	 * 关闭数据库连接
	 * 
	 * @access public
	 * @return void
	 */
	public function close(){
		$db_link = $this -> db_link;
		$this -> is_connect() && mysql_close($db_link);
	}
	
	/** 
	 * 执行sql语句
	 * 
	 * @access public
	 * @param: string $sql sql语句
	 * @return resource 结果集句柄
	 */
	public function query($sql){
		$this -> cur_sql = $sql = trim($sql);
		$this -> reconnect();
		$db_link = $this -> db_link;
		$start_time = get_cur_time();
		$result = mysql_query($sql, $db_link);
		$this -> list_sql[] = array('sql' => $sql, 'time' => time_diff($start_time), 'result' => ($result ? TRUE : FALSE), 'message' => mysql_error(), 'code' => mysql_errno());
		!$result && $this -> debug('Mysql Query Failure,Sql['.$sql.']!'.mysql_error(), FALSE);
		return $result;
	}
	
	/** 
	 * 释放结果集
	 * 
	 * @access public
	 * @return void
	 */
	public function free_result($result){
		is_resource($result) && mysql_free_result($result);
	}
	
	/** 
	 * 查询入口
	 * 
	 * @access public
	 * @param string $sql sql语句
	 * @param integer $fetch 返回的数组索引类型，1-字段索引、2-数字索引、其他-两种索引
	 * @return mixed 成功返回array,失败返回FALSE
	 */
	public function select($sql, $fetch=1){
		$fetch_style = '';
		switch($fetch) {
			case 1 : {
				$fetch_style = MYSQL_ASSOC;
				BREAK;
			}
			case 2 : {
				$fetch_style = MYSQL_NUM;
				BREAK;
			}
			default : {
				$fetch_style = MYSQL_BOTH;
				BREAK;
			}
		}
		$result = $this -> query($sql);
		$return = array();
		if($result){ 
			while($row = mysql_fetch_array($result, $fetch_style)) $return[] = $row;
		}else{
			$return = FALSE;		
		}
		$this -> free_result($result);
		return $return;
	}
	
	/** 
	 * 获取最后写入数据的Id
	 * 
	 * @access public
	 * @return mixed 成功时返回integer
	 */
	public function insert_id(){
		$this -> reconnect();
		$db_link = $this -> db_link;
		return mysql_insert_id($db_link);
	}
	
	/** 
	 * 取得前一次操作所影响的记录数
	 * 
	 * @access public
	 * @return mixed 调用成功返回integer
	 */
	public function affected_rows(){
		$this -> reconnect();
		$db_link = $this -> db_link;
		return mysql_affected_rows($db_link);
	}
	
	/** 
	 * 选择数据库
	 * 
	 * @access public
	 * @param: string $db_name 数据库名称
	 * @return boolean
	 */
	public function select_db($db_name=NULL){
		empty($db_name) && $db_name = $this -> db_name;
		$this -> reconnect();
		$db_link = $this -> db_link;
		!($return = mysql_select_db($db_name, $db_link)) && $this -> debug('Mysql Select DB ['.$db_name.'] Failure!', FALSE);
		return $return ? TRUE : FALSE;
	}
	
	/** 
	 * 选择数据库编码
	 * 
	 * @access public
	 * @param: string $db_chart 编码
	 * @return boolean
	 */
	public function select_charct($db_chart=NULL){
		empty($db_chart) && $db_chart = $this -> db_chart;
		$this -> reconnect();
		$db_link = $this -> db_link;
		if(function_exists('mysql_set_charset')){
			$return = mysql_set_charset($db_chart, $db_link);
		}else{
			!($return = $this -> query('SET NAMES \''.$db_chart.'\'', $db_link)) && $this -> debug('Mysql Set Char ['.$db_chart.'] Failure!', FALSE);
		}
		return $return ? TRUE : FALSE;
	}
	
	/** 
	 * 获取当前页面执行过的sql信息
	 * 
	 * @access public
	 * @return array 
	 */
	public function get_sql_list(){
		return $this -> list_sql;
	}
	
	/** 
	 * 清空sql记录
	 * 
	 * @access public
	 * @return: void
	 */
	public function clear_sql(){
		$this -> list_sql = array();
	}
	
	
	/** 
	 * session专用查询方法
	 * 
	 * 当使用db_session模块时，该方法才会被使用
	 * 
	 * @access public
	 * @param string $sql sql语句
	 * @param string $sess_data_name session的数据存放字段名
	 * @return mixed 查询失败返回FALSE，成功返回string
	 */
	public function get_session_one($sql, $sess_data_name){
		$return = array();
		$resource = $this -> query($sql);
		if($resource){
			$return = mysql_fetch_array($resource, MYSQL_ASSOC);
		}
		return isset($return[$sess_data_name]) ? $return[$sess_data_name] : FALSE;
	}
}

/* End of file cls_mysql.php */ 