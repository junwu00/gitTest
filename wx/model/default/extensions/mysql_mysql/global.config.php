<?php
/** 
 * mysql扩展操作数据库配置文件
 * 
 * 仅能加载该配置文件来使用mysql操作类
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('MYSQL_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_mysql_url = MYSQL_ROOT.'cls_mysql.php';
function_exists('mysql_connect') && !class_exists('cls_mysql') && file_exists($cls_mysql_url) && include_once($cls_mysql_url);

/* End of file global.config.php */ 