<?php
/** 
 * pdo扩展操作数据库配置文件
 * 
 * 仅能加载该配置文件来使用mysql_pdo操作类
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */
!defined('SYSTEM') && die('ACCESS DENIED');

define('PDO_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_pdo_url = PDO_ROOT.'cls_pdo.php';
class_exists('pdo') && !class_exists('cls_pdo') && file_exists($cls_pdo_url) && include_once($cls_pdo_url);

/* End of file global.config.php */ 