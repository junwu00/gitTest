<?php
/** 
 * mysqli扩展操作数据库配置文件
 * 
 * 仅能加载该配置文件来使用mysqli操作类
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */
!defined('SYSTEM') && die('ACCESS DENIED');

define('MYSQLI_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_mysqli_url = MYSQLI_ROOT.'cls_mysqli.php';
function_exists('mysqli_connect') && !class_exists('cls_mysqli') && file_exists($cls_mysqli_url) && include_once($cls_mysqli_url);

/* End of file global.config.php */ 