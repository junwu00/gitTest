<?php
/** 
 * 微信企业号API功能扩展 配置文件
 * 
 * @author yangpz
 * @create 2014-10-18 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('WXQY_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录
define('WXQY_DEBUG', FALSE);    //是否打印调试日志

include_once(WXQY_ROOT.'const.define.php');
include_once(WXQY_ROOT.'cls_wxqy_exp.php');
//加解密类
include_once(WXQY_ROOT.'crypter/cls_sha1.php');
include_once(WXQY_ROOT.'crypter/cls_pkcs7_encoder.php');
include_once(WXQY_ROOT.'crypter/cls_prpcrypt.php');
include_once(WXQY_ROOT.'crypter/cls_xml_parse.php');
include_once(WXQY_ROOT.'crypter/cls_msg_crypter.php');
//
include_once(WXQY_ROOT.'cls_wxqy_msg.php');

$cls_wxqy_url = WXQY_ROOT.'cls_wxqy.php';
!class_exists('cls_wxqy') && file_exists($cls_wxqy_url) && include_once($cls_wxqy_url);


/* end of file */