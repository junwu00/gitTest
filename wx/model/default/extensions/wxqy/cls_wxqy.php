<?php
/**
 * 微信企业号扩展类
 * 
 * @author yangpz
 * @date 2014-10-08
 * @version 1.0
 */

class cls_wxqy {
	
	private $token;			//应用token
	private $corpid;			
	private $aes_key;		//应用密钥
	
	/**
	 * 初始化
	 * @param unknown_type $token
	 * @param unknown_type $corpid
	 */
	public function init($token, $corpid, $aes_key) {
		$this -> token = $token;
		$this -> corpid = $corpid;
		$this -> aes_key = $aes_key;
	}
	
	//------------------------------验证------------------------------
	
	/**
	 * 验证接入请求
	 * @param unknown_type $msg_signature
	 * @param unknown_type $timestamp
	 * @param unknown_type $nonce
	 * @param unknown_type $echo_str
	 */
	public function verify_url($msg_signature, $timestamp, $nonce, $echo_str) {
		log_write('开始验证接入请求');
		$pc = new cls_msg_crypter($this -> token, $this -> aes_key, $this -> corpid);
		$reply_str = '';
		$pc -> verify_url($msg_signature, $timestamp, $nonce, $echo_str, $reply_str);
		log_write('验证成功');
		return $reply_str;
	}
	
	//-----------------------------消息加密解密-------------------------------
	
	/**
	 * 解密收到的消息
	 * @param unknown_type $post_data		收到的消息（密文）
	 * @param unknown_type $msg_signature	
	 * @param unknown_type $timestamp	
	 * @param unknown_type $nonce
	 */
	public function decrypt_msg($post_data, $msg_signature, $timestamp, $nonce) {
		WXQY_DEBUG && log_write('开始解密消息');
		$crypter = new cls_msg_crypter($this -> token, $this -> aes_key, $this -> corpid);
		$msg = '';
		try {
			$crypter -> decrypt_msg($msg_signature, $timestamp, $nonce, $post_data, $msg);
			WXQY_DEBUG && log_write('解密结果:'.$msg);
			return $msg;
		} catch (SCException $e) {
			log_write('解密消息失败');
			throw $e;			
		}
	}
	
	/**
	 * 加密要响应的消息
	 * @param unknown_type $reply_msg	要响应的消息（明文）
	 * @param unknown_type $timestamp
	 * @param unknown_type $nonce
	 */
	public function encrypt_msg($reply_msg, $timestamp=NULL, $nonce=NULL) {
		WXQY_DEBUG && log_writelog_write('开始加密消息');
		is_null($timestamp) || empty($timestamp) && $timestamp = time();
		is_null($nonce) || empty($nonce) && $nonce = get_rand_string(6);
		
		$crypter = new cls_msg_crypter($this -> token, $this -> aes_key, $this -> corpid);
		$msg = '';
		try {
			$result = $crypter -> encrypt_msg($reply_msg, $timestamp, $nonce, $msg);
			WXQY_DEBUG && log_write('加密成功');
			return $msg;
		} catch (SCException $e) {
			log_write('加密消息失败');
			throw $e;			
		}
	}
	
	//------------------------------------------部门管理-------------------------------
	/*
	 * 1.创建部门
	 * 2.更新部门
	 * 3.删除部门
	 * 4.获取部门列表
	 */
	
	/**
	 * 创建部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及父部门的管理权限。
	 * @param unknown_type $access_token		
	 * @param unknown_type $name		部门名称，1~64字符
	 * @param unknown_type $parent_id	上级部门ID，根部门ID为1
	 * @param unknown_type $order		在上级部门中的次序，从1开始，小号在前
	 */
	public function create_dept($access_token, $name, $parent_id, $order=1) {
		log_write('开始创建部门：name='.$name.', parentid='.$parent_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$access_token}";
		$data = array(
			'name' => $name,
			'parentid' => $parent_id,
			'order'	=> $order,
		);
		try {
			$result = $this -> do_post($url, $data);
			log_write('创建成功:id='.$result['id']);
			return $result['id'];
		} catch (SCException $e) {
			log_write('创建失败');
			throw $e;
		}
	}
	
	/**
	 * 更新部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及该部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $id				部门ID
	 * @param unknown_type $name
	 * @param unknown_type $parent_id
	 * @param unknown_type $order
	 */
	public function update_dept($access_token, $id, $name, $parent_id, $order) {
		log_write('开始更新部门：name='.$name.', id='.$id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={$access_token}";
		$data = array(
			'id' => $id,
			'name' => $name,
			'parentid' => $parent_id,
			'order' => $order,
		);
		try {
			$this -> do_post($url, $data);
			log_write('更新成功');
		} catch (SCException $e) {
	        log_write('更新失败');
			throw $e;
		}
	}
	
	/**
	 * 删除部门<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及该部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $id			部门ID
	 */
	public function delete_dept($access_token, $id) {
		log_write('开始删除部门：id='.$id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token={$access_token}&id={$id}";
		try {
			$this -> do_get($url);
			log_write('删除成功');
		} catch (SCException $e) {
	        log_write('删除失败');
			throw $e;
		}
	}
	
	/**
	 * 获取部门列表<br>
	 * 权限说明：管理员须拥有’获取部门列表’的接口权限，以及对部门的查看权限。
	 * @param unknown_type $access_token
	 * @return 部门列表
	 */
	public function list_dept($access_token) {
		log_write('开始获取部门列表');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={$access_token}";
		try {
			$result = $this -> do_get($url);
			log_write('获取成功');
			return $result['department'];		
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;
		}
	}
	
	//------------------------------------------成员管理-----------------------------------------
	/*
	 * 1.创建成员
	 * 2.更新成员信息
	 * 3.删除成员
	 * 4.获取单个成员详细信息
	 */
	
	/**
	 * 创建成员，mobile,weixin_id, email三者不能同时为空<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id			对应管理端的账号，企业内必须唯一
	 * @param unknown_type $name
	 * @param unknown_type $department		如：array(1, 2);每个部门直属员工上限1000
	 * @param unknown_type $position
	 * @param unknown_type $mobile			企业内必须唯一
	 * @param unknown_type $gender			0男1女
	 * @param unknown_type $tel				
	 * @param unknown_type $email			企业内必须唯一
	 * @param unknown_type $weixin_id		企业内必须唯一
	 * @param unknown_type $ext_attr
	 */
	public function create_user($access_token, $user_id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr=array()) {
		log_write('开始创建成员:user_id='.$user_id.', dept='.json_encode($department));
		$depts = array_values($department);
		$depts = intval_array($depts);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={$access_token}";
		$ext = array('attrs' => $ext_attr);
		$data = array(
			'userid' => $user_id,
			'name' => $name,
			'department' => $depts,
			'position' => $position,
			'mobile' => $mobile,
			'gender' => isset($gender) ? $gender : 0,
			'tel' => $tel,
			'email' => $email,
			'weixinid' => $weixin_id,
			'extattr' => json_encode($ext),
		);
		try {
			$ret = $this -> do_post($url, $data);
			log_write('创建成功');
		} catch (SCException $e) {
	        log_write('创建失败');
			throw $e;
		}
	}
	
	/**
	 * 更新成员信息<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门、成员的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 * @param unknown_type $name
	 * @param unknown_type $department
	 * @param unknown_type $position
	 * @param unknown_type $mobile
	 * @param unknown_type $gender
	 * @param unknown_type $tel
	 * @param unknown_type $email
	 * @param unknown_type $weixin_id
	 * @param unknown_type $ext_attr
	 * @param unknown_type $enable			0禁用1启用
	 */
	public function update_user($access_token, $user_id, $name, $department, $position, $mobile, $gender, $tel, $email, $weixin_id, $ext_attr=array(), $enable=1) {
		log_write('开始更新成员:user_id='.$user_id.', dept='.json_encode($department));
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={$access_token}";
		$ext = array('attrs' => $ext_attr);
		$data = array(
			'userid' => $user_id,
			'name' => $name,
			'department' => $department,
			'positiion' => $position,
			'mobile' => $mobile,
			'gender' =>  isset($gender) ? $gender : 0,
			'tel' => $tel,
			'email' => $email,
			'weixinid' => $weixin_id,
			'extattr' => json_encode($ext),
			'enable' => $enable,
		);
		try {
			$ret = $this -> do_post($url, $data);
			log_write('更新成功');
		} catch (SCException $e) {
	        log_write('更新失败');
			throw $e;
		}
	}
	
	/**
	 * 删除成员<br>
	 * 权限说明：管理员须拥有“操作通讯录”的接口权限，以及指定部门、成员的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 */
	public function delete_user($access_token, $user_id) {
		log_write('开始删除成员：user_id='.$user_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={$access_token}&userid={$user_id}";
		try {
			$ret = $this -> do_get($url);
			log_write('删除成功');
		} catch (SCException $e) {
	        log_write('删除失败');
	        throw $e;
		}
	}
	
	/**
	 * 获取单个成员详细信息<br>
	 * 权限说明：管理员须拥有’获取成员’的接口权限，以及成员的查看权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 * @return 成员信息
	 */
	public function get_user($access_token, $user_id) {
		log_write('开始获取成员信息：user_id='.$user_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={$access_token}&userid={$user_id}";
		try {
			$data = $this -> do_get($url);
			log_write('获取成功');
			return $data;
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;
		}
	}
	
	/**
	 * 获取部门下成员信息<br>
	 * 权限说明：管理员须拥有’获取部门成员’的接口权限，以及指定部门的查看权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $department_id	
	 * @param unknown_type $fetch_child		1/0：是否递归获取子部门下面的成员
	 * @param unknown_type $status			0获取全部员工，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加
	 * @return 部门成员信息(只有userid, name)
	 */
	public function list_user_by_dept($access_token, $department_id, $fetch_child=0, $status=0) {
		log_write('开始获取部门成员列表：user_id='.$user_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={$access_token}&department_id={$department_id}&fetch_child={$fetch_child}&status={$status}";
		try {
			$data = $this -> do_get($url);
			log_write('获取成功');
			return $data['userlist'];
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;
		}
	}
	
	//------------------------------------------------标签管理----------------------------------------------
	/*
	 * 1.创建标签
	 * 2.更新标签名称
	 * 3.删除标签
	 * 4.获取标签成员
	 * 5.增加标签成员（列表）
	 * 6.删除标签成员（列表）
	 * 7.获取标签列表
	 */
	
	/**
	 * 创建标签<br>
	 * 权限说明：无限定。
	 * @param unknown_type $access_token
	 * @param unknown_type $name			标签名称，不可与其他同组的标签重名，也不可与全局标签重名
	 */
	public function create_tag($access_token, $name) {
		log_write('开始创建标签:tag_name='.$name);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token={$access_token}";
		$data = array(
			'tagname' => $name,
		);
		try {
			$result = $this -> do_post($url, $data);
			log_write('创建成功:tag_id='.$result['tagid']);
		} catch (SCException $e) {
			log_write('创建失败');
			throw $e;
		}
	}
	
	/**
	 * 更新标签名称<br>
	 * 权限说明：管理员必须是指定标签的创建者。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id			标签ID
	 * @param unknown_type $name			标签名称，不可与其他同组的标签重名，也不可与全局标签重名
	 */
	public function update_tag($access_token, $tag_id, $name) {
		log_write('开始更新标签名称:tag_id='.$tag_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'tagname' => $name,
		);
		try {
			$this -> do_post($url, $data);
			log_write('更新成功');
		} catch (SCException $e) {
			log_write('更新失败');
			throw $e;
		}
	}
	
	/**
	 * 删除标签<br>
	 * 权限说明：管理员必须是指定标签的创建者，并且标签的成员列表为空。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 */
	public function delete_tag($access_token, $tag_id) {
		log_write('开始删除标签：tag_id='.$tag_id);		
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token={$access_token}$tagid={$tag_id}";
		try {
			$this -> do_get($url);
			log_write('删除成功');
		} catch (SCException $e) {
			log_write('删除失败');
			throw $e;
		}
	}
	
	/**
	 * 获取标签成员<br>
	 * 权限说明：管理员须拥有“获取标签成员”的接口权限，标签须对管理员可见；返回列表仅包含管理员管辖范围的成员。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id		
	 * @return 成员列表
	 */
	public function list_user_by_tag($access_token, $tag_id) {
		log_write('开始获取标签成员列表：tag_id='.$tag_id);		
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token={$access_token}$tagid={$tag_id}";
		try {
			$data = $this -> do_get($url);
			log_write('获取成功');
			return $data['userlist'];
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;
		}
	}
	
	/**
	 * 增加标签成员（列表）<br>
	 * 权限说明：标签对管理员可见且未加锁，成员属于管理员管辖范围。<br>
	 * a)完全正确<br>
	 * b)部分user_id非法<br>
	 * c)全部user_id非法
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 * @param unknown_type $user_list		如：array('user1_id', 'user2_id');
	 * @return 非法user_id字符串
	 */
	public function add_tag_user_list($access_token, $tag_id, $user_list) {
		log_write('开始增加标签成员：tag_id='.$tag_id);
		$users = array_values($user_list);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'userlist' => json_encode($users),
		);
		try {
			$result = $this -> do_post($url, $data);
			if (isset($result['invalidlist'])) {
				log_write('部分user_id非法：'.$result['invalidlist']);
				return $result['invalidlist'];
			} else {
				log_write('全部增加成功');
			}
		} catch (SCException $e) {
			log_write('增加失败');
			throw $e;
		}
	}
	
	/**
	 * 删除标签成员（列表）
	 * 权限说明：标签对管理员可见且未加锁，成员属于管理员管辖范围。
	 * @param unknown_type $access_token
	 * @param unknown_type $tag_id
	 * @param unknown_type $user_list		如：array('user1_id', 'user2_id');
	 * @return 非法user_id字符串
	 */
	public function del_tag_user($access_token, $tag_id, $user_list) {
		log_write('开始删除标签成员：tag_id='.$tag_id);
		$users = array_values($user_list);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token={$access_token}";
		$data = array(
			'tagid' => $tag_id,
			'userlist' => json_encode($users),
		);
		try {
			$result = $this -> do_post($url, $data);
			if (isset($result['invalidlist'])) {
				log_write('部分user_id非法：'.$result['invalidlist']);
				return $result['invalidlist'];
			} else {
				log_write('全部删除成功');
			}
		} catch (SCException $e) {
			log_write('删除失败');
			throw $e;
		}
	}
	
	/**
	 * 获取标签列表
	 * 权限说明：标签对管理员可见
	 * @param unknown_type $access_token
	 * @return 标签列表，含tagid, tagname
	 */
	public function list_tag($access_token) {
		log_write('开始获取标签列表');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token={$access_token}";
		try {
			$data = $this -> do_get($url);
			log_write('获取成功');
			return $data['taglist'];
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;
		}
	}
	
	//----------------------------------------------------菜单管理-------------------------------------------
	/*
	 * 1.创建菜单
	 */
	
	/**
	 * 创建菜单
	 * 权限说明：管理员须拥有应用的管理权限，并且应用必须设置在回调模式。
	 * @param unknown_type $access_token
	 * @param unknown_type $agent_id		企业应用的id，整型
	 * @param unknown_type $data			菜单内容（数组格式，只含一、二级菜单内容，中文需进行urlencode）
	 */
	public function create_menu($access_token, $agent_id, $data) {
		log_write('开始创建菜单：agent_id='.$agent_id);
		$url = "https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token={$access_token}&agentid={$agent_id}";
		$data = array('button' => $data);
//		$data = json_encode($data);
		try {
			$this -> do_post($url, $data);
			log_write('创建成功');
		} catch (SCException $e) {
			log_write('创建失败');
			throw $e;
		}
	}

	
	//------------------------------------------------------二次验证----------------------------------------------
	
	/**
	 * 获取二次验证授权的url，页面将跳转至 redirect_uri/?code=CODE&state=STATE，企业可根据code参数获得员工的userid。
	 * @param unknown_type $corpid			企业号管理员组的corp id
	 * @param unknown_type $redirect_url	目标页面（用于获取code）
	 */
	public function get_oauth_url($corpid, $redirect_url) {
		$redirect_url = urlencode(urlencode($redirect_url));
		$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$corpid}&redirect_uri={$redirect_url}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
		return $url;
	}
	
	/**
	 * 获取授权用户的user_id
	 * 权限说明：管理员须拥有agent的使用权限；agentid必须和跳转链接时所在的企业应用ID相同。
	 * @param unknown_type $access_token
	 * @param unknown_type $code
	 * @param unknown_type $agent_id		跳转链接时所在的企业应用ID，默认为0：企业小助手
	 * @return user_id
	 */
	public function get_oauth_user_id($access_token, $code, $agent_id=0) {
		log_write('开始获取授权用户：agent_id='.$agent_id.', code='.$code);
		if (!isset($code) || is_null($code) || empty($code)) {
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$OauthCodeNotFound);
		}
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$access_token}&code={$code}&agentid={$agent_id}";
		try {
			$data = $this -> do_post($url, array());
			log_write('获取成功：wxacct='.$data['UserId']);
			return $data['UserId'];
		} catch (SCException $e) {
			log_write('获取失败');
			throw $e;			
		}
	}

    /**
     * 获取授权用户的user_id
     * 权限说明：管理员须拥有agent的使用权限；agentid必须和跳转链接时所在的企业应用ID相同。
     * @param unknown_type $access_token
     * @param unknown_type $code
     * @param unknown_type $agent_id		跳转链接时所在的企业应用ID，默认为0：企业小助手
     * @return user_id
     */
    public function get_oauth_info($access_token, $code, $agent_id=0) {
        log_write('开始获取授权用户：agent_id='.$agent_id.', code='.$code);
        if (!isset($code) || is_null($code) || empty($code)) {
            throw new cls_wxqy_exp(WXQY_ERR_CODE::$OauthCodeNotFound);
        }
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$access_token}&code={$code}&agentid={$agent_id}";
        try {
            $data = $this -> do_post($url, array());
            log_write('获取成功：wxinfo='.json_encode($data));
            return $data;
        } catch (SCException $e) {
            log_write('获取失败');
            throw $e;
        }
    }
	
	/**
	 * 完成二次验证，成员关注成功
	 * 权限说明：管理员须拥有userid对应员工的管理权限。
	 * @param unknown_type $access_token
	 * @param unknown_type $user_id
	 */
	public function oauth_user($access_token, $user_id) {
		log_write('开始完成二次验证');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token={$access_token}&userid={$user_id}";
		try {
			$this -> do_get($url);
			log_write('完成二次验证');
		} catch (SCException $e) {
			log_write('二次验证失败');
			throw $e;
		}
	}
	
	//---------------------------------------------被动响应消息---------------------------------------
	/*
	 * 1.获取文本回复
	 * 2.获取（单/多）图文回复
	 */
	
	/**
	 * 获取文本回复
	 * @param unknown_type $post_data	请求内容数组（明文）
	 * @param unknown_type $content		文本内容
	 */
	public function get_text_reply($post_data, $content) {
		$reply_msg = sprintf(cls_wxqy_msg::$TextTemp, $post_data['FromUserName'], $post_data['ToUserName'], time(), $content);
		try {
			$msg = $this -> encrypt_msg($reply_msg);
			return $msg;
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取（单/多）图文回复
	 * @param unknown_type $post_data		请求内容数组（明文）
	 * @param unknown_type $article_list	图文数组，含title, desc, pic_url, url
	 */
	public function get_news_reply($post_data, $article_list) {
		$arts = '';
		foreach ($article_list as $art) {
			$art_item = sprintf(cls_wxqy_msg::$NewsItemTemp, $art['title'], $art['desc'], $art['pic_url'], $art['url']);
			$arts .= $art_item;
		}
		$reply_msg = sprintf(cls_wxqy_msg::$NewsTemp, $post_data['FromUserName'], $post_data['ToUserName'], time(), count($article_list), $arts);
		try {
			$msg = $this -> encrypt_msg($reply_msg);
			return $msg;
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	//--------------------------------------------事件类型---------------------------------------

	
	
	//----------------------------------------上传媒体文件---------------------------------------
	/*
	 * 1.
	 */
	
	/**
	 * 上传媒体文件<br>
	 * 
	 * 图片（image）:1MB，支持JPG格式
	 * 语音（voice）：2MB，播放长度不超过60s，支持AMR格式
	 * 视频（video）：10MB，支持MP4格式
	 * 普通文件（file）：10MB
	 * 
	 * @param unknown_type $access_token
	 * @param unknown_type $type			媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件(file)
	 * @param unknown_type $file_path		要上传的文件的路径
	 * @param unknown_type $file_path		原文件名
	 */
	public function upload_media($access_token, $type, $file_path, $file_name) {
		$url = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={$access_token}&type={$type}";
		try {
			return $this -> do_post_media($url, $file_path, $file_name);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	//------------------------------------------发送消息-----------------------------------------
	/*
	 * 1.发送文本消息
	 * 
	 */
	
	/**
	 * 发送文本消息
	 * @param unknown_type $access_token
	 * @param unknown_type $text		要发送的文本信息
	 * @param unknown_type $user_list	UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
	 * @param unknown_type $agent_id	企业应用的id，整型
	 * @param unknown_type $tag_list	TagID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
	 * @param unknown_type $party_list	PartyID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
	 * @param unknown_type $is_safe		表示是否是保密消息，0表示否，1表示是，默认0
	 */
	public function send_text($access_token, $user_list, $agent_id, $text, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   "touser" => $users,
			   "toparty" => $partys,
			   "totag" => $tags,
			   "msgtype" => "text",
			   "agentid" => $agent_id,
			   "text" => array(
			       "content" => $text,
				),
			   "safe" => $is_safe,
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;			
		}
	}
	
	/**
	 * 发送图片
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_img($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   "touser" => $users,
			   "toparty" => $partys,
			   "totag" => $tags,
			   "msgtype" => "image",
			   "agentid" => $agent_id,
			   "image" => array(
			       "media_id" => $media_id,
				),
			   "safe" => $is_safe,
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送音频
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_voice($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   "touser" => $users,
			   "toparty" => $partys,
			   "totag" => $tags,
			   "msgtype" => "voice",
			   "agentid" => $agent_id,
			   "voice" => array(
			       "media_id" => $media_id,
				),
			   "safe" => $is_safe,
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送视频
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $title
	 * @param unknown_type $desc
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_video($access_token, $user_list, $agent_id, $media_id, $title, $desc, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   "touser" => $users,
			   "toparty" => $partys,
			   "totag" => $tags,
			   "msgtype" => "video",
			   "agentid" => $agent_id,
			   "video" => array(
			  		"media_id" => $media_id,
					"title" => $title,
					"description" => $desc,
				),
			   "safe" => $is_safe,
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送文件
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $media_id
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 * @param unknown_type $is_safe
	 */
	public function send_file($access_token, $user_list, $agent_id, $media_id, $tag_list=array(), $party_list=array(), $is_safe=0) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$data = array(
			   "touser" => $users,
			   "toparty" => $partys,
			   "totag" => $tags,
			   "msgtype" => "file",
			   "agentid" => $agent_id,
			   "file" => array(
			       "media_id" => $media_id,
				),
			   "safe" => $is_safe,
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 发送图文消息
	 * @param unknown_type $access_token
	 * @param unknown_type $user_list
	 * @param unknown_type $agent_id
	 * @param unknown_type $art_list
	 * @param unknown_type $tag_list
	 * @param unknown_type $party_list
	 */
	public function send_news($access_token, $user_list, $agent_id, $art_list, $tag_list=array(), $party_list=array()) {
		try {
			$users = $this -> verify_send_req($user_list, $agent_id);
			$partys = implode('|', array_values($party_list));
			$tags = implode('|', array_values($tag_list));
			
			$arts = array();
			foreach ($art_list as $art) {
				$item = array(
					'title' => $art['title'],
					'description' => $art['description'],
					'url' => $art['url'],
					'picurl' => $art['picurl'],
				);
				$arts[] = $item;
			}
			unset($art);
			
			$data = array(
		 		"touser" => $users,
				"toparty" => $partys,
			  	"totag" => $tags,
			   	"msgtype" => "news",
			   	"agentid" => $agent_id,
			   	"news" => array(
					"articles" => $arts,
				),
			);
			$this -> send_msg($access_token, $data);
		} catch (SCException $e) {
			throw $e;
		}
	}


	//-------------------------------------------家校通知---------------------------------------
    /**
     * 发送文本消息
     * @param unknown_type $access_token
     * @param unknown_type $text		要发送的文本信息
     * @param unknown_type $user_list	UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
     * @param unknown_type $agent_id	企业应用的id，整型
     * @param unknown_type $tag_list	TagID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
     * @param unknown_type $party_list	PartyID列表，多个接受者用‘|’分隔。当touser为@all时忽略本参数
     * @param unknown_type $is_safe		表示是否是保密消息，0表示否，1表示是，默认0
     */
    public function send_school_text($access_token,  $agent_id, $text, $user_list=array(), $party_list=array()) {
        try {
            $this -> verify_send_req($user_list, $agent_id);

            $data = array(
                "to_parent_userid" => $user_list,
                "to_party" => $party_list,
                "msgtype" => "text",
                "agentid" => $agent_id,
                "text" => array(
                    "content" => $text,
                ),
            );
            $this -> send_school_msg($access_token, $data);
        } catch (SCException $e) {
            throw $e;
        }
    }

    /**
     * 发送图片
     * @param unknown_type $access_token
     * @param unknown_type $user_list
     * @param unknown_type $agent_id
     * @param unknown_type $media_id
     * @param unknown_type $tag_list
     * @param unknown_type $party_list
     * @param unknown_type $is_safe
     */
    public function send_school_img($access_token,  $agent_id, $media_id, $user_list, $party_list=array()) {
        try {
            $users = $this -> verify_send_req($user_list, $agent_id);
            $partys = implode('|', array_values($party_list));

            $data = array(
                "to_parent_userid" => $user_list,
                "to_party" => $party_list,
                "msgtype" => "image",
                "agentid" => $agent_id,
                "image" => array(
                    "media_id" => $media_id,
                ),
            );
            $this -> send_school_msg($access_token, $data);
        } catch (SCException $e) {
            throw $e;
        }
    }

    /**
     * 发送音频
     * @param unknown_type $access_token
     * @param unknown_type $user_list
     * @param unknown_type $agent_id
     * @param unknown_type $media_id
     * @param unknown_type $tag_list
     * @param unknown_type $party_list
     * @param unknown_type $is_safe
     */
    public function send_school_voice($access_token,  $agent_id, $media_id, $user_list, $party_list=array()) {
        try {
            $users = $this -> verify_send_req($user_list, $agent_id);
            $partys = implode('|', array_values($party_list));

            $data = array(
                "to_parent_userid" => $user_list,
                "to_party" => $party_list,
                "msgtype" => "voice",
                "agentid" => $agent_id,
                "voice" => array(
                    "media_id" => $media_id,
                ),
            );
            $this -> send_school_msg($access_token, $data);
        } catch (SCException $e) {
            throw $e;
        }
    }


    /**
     * 发送视频
     * @param unknown_type $access_token
     * @param unknown_type $user_list
     * @param unknown_type $agent_id
     * @param unknown_type $media_id
     * @param unknown_type $title
     * @param unknown_type $desc
     * @param unknown_type $tag_list
     * @param unknown_type $party_list
     * @param unknown_type $is_safe
     */
    public function send_school_video($access_token,  $agent_id, $media_id, $title, $desc, $user_list=array(), $party_list=array()) {
        try {
            $users = $this -> verify_send_req($user_list, $agent_id);
            $partys = implode('|', array_values($party_list));

            $data = array(
                "to_parent_userid" => $user_list,
                "to_party" => $party_list,
                "msgtype" => "video",
                "agentid" => $agent_id,
                "video" => array(
                    "media_id" => $media_id,
                    "title" => $title,
                    "description" => $desc,
                ),
            );
            $this -> send_school_msg($access_token, $data);
        } catch (SCException $e) {
            throw $e;
        }
    }

    /**
     * 发送文件
     * @param unknown_type $access_token
     * @param unknown_type $user_list
     * @param unknown_type $agent_id
     * @param unknown_type $media_id
     * @param unknown_type $tag_list
     * @param unknown_type $party_list
     * @param unknown_type $is_safe
     */
    public function send_school_file($access_token, $agent_id, $media_id, $user_list=array(),  $party_list=array()) {
        try {
            $users = $this -> verify_send_req($user_list, $agent_id);
            $partys = implode('|', array_values($party_list));

            $data = array(
                "to_parent_userid" => $user_list,
                "to_party" => $party_list,
                "msgtype" => "file",
                "agentid" => $agent_id,
                "file" => array(
                    "media_id" => $media_id,
                ),
            );
            $this -> send_school_msg($access_token, $data);
        } catch (SCException $e) {
            throw $e;
        }
    }



    //-------------------------------------------内部方法------------------------------------------
	
	/**
	 * 是否为有效的消息发送请求
	 * @param unknown_type $user_list	UserID列表（消息接收者，多个接收者用‘|’分隔）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
	 * @param unknown_type $agent_id	企业应用的id，整型
	 */
	private function verify_send_req($user_list, $agent_id) {
		if (is_null($agent_id) || !is_int(intval($agent_id))) {
			log_write('应用ID找不到: '.json_encode($agent_id));
			throw new SCException('应用ID找不到: '.json_encode($agent_id));
		}
		
		$users = implode('|', array_values($user_list));
		return $users;
	}
	
	/**
	 * 发送消息
	 * @param unknown_type $access_token
	 * @param unknown_type $data		要发送的消息
	 */
	private function send_msg($access_token, $data) {
		log_write('开始发送消息');
		$url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={$access_token}";
		try {
			$this -> do_post($url, $data);
			log_write('发送成功');
			
		} catch(SCException $e) {
			log_write('发送失败:'.$e -> getMessage());
			throw $e;
		}
	}

    /**
     * 发送家校消息
     * @param unknown_type $access_token
     * @param unknown_type $data		要发送的消息
     */
    private function send_school_msg($access_token, $data) {
        log_write('开始发送消息');
        $url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/message/send?access_token={$access_token}";
        try {
            $this -> do_post($url, $data);
            log_write('发送成功');

        } catch(SCException $e) {
            log_write('发送失败:'.$e -> getMessage());
            throw $e;
        }
    }
	
	/**
	 * 验证$corpid和$secret是否为空
	 * @param unknown_type $corpid	企业Id
	 * @param unknown_type $secret	管理组的凭证密钥
	 * @return 
	 */
	private function check_corpid_and_secret($corpid, $secret) {
		if (is_null($corpid) || empty($corpid))	{
			log_write('找不到corp id');
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$CorpIdNotFound);
		}
		if (is_null($secret) || empty($secret)) {
			log_write('找不到corp secret');
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$SecretNotFound);
		}
	}
	
	/**
	 * 更新access token
	 * @param unknown_type $corpid	企业Id
	 * @param unknown_type $secret	管理组的凭证密钥
	 * @return 获取到的access token，获取到的时间
	 */
	public function update_access_token($corpid, $secret) {
		log_write('开始更新access token');

		try {
			$result = $this -> check_corpid_and_secret($corpid, $secret);
			$url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={$corpid}&corpsecret={$secret}";
			$data = $this -> do_get($url);
			!isset($data['access_token']) && log_write('更新失败：重试 1') && $data = $this -> do_get($url);
			!isset($data['access_token']) && log_write('更新失败：重试 2') && $data = $this -> do_get($url);
			
			if (isset($data['access_token'])) {
				log_write('更新成功: token='.$data['access_token']);
				return array(
					'token' => $data['access_token'], 
					'time'	=> time(),
				);
				
			} elseif (isset($data['errcode'])) {
				throw new cls_wxqy_exp($data['errcode']);
				
			} else {
				throw new cls_wxqy_exp(WXQY_ERR_CODE::$GetAccessTokenExp);
			}
		} catch(SCException $e) {
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$GetAccessTokenExp);
		}
	}
	
	/**
	 * 上传文件到微信服务器<br>
	 * 为确认用户收到的文件名与上传时一致，采用以下方法<br>
	 * 1.先从本地复制文件，以用户上传时的文件名命名<br>
	 * 2.上传该临时文件到微信服务器<br>
	 * 3.删除该临时文件<br>
	 * @param unknown_type $url			POST请求的目标URL
	 * @param unknown_type $file_path	文件路径
	 * @param unknown_type $file_name	用户上传时自己命名的文件名
	 * @return 数组形式的结果
	 */
	public function do_post_media($url, $file_path, $file_name) {
		$dirs = explode('/', $file_path);
		$ori_file_name = $dirs[count($dirs) - 1];
		$file_name = iconv('utf-8', 'gbk', $file_name);
		$tmp_dir = get_rand_string(3);	//存储复制的文件的临时目录
		$new_file_path = substr($file_path, 0, strlen($file_path) - strlen($ori_file_name)) . $tmp_dir . '/';
		if (!file_exists($new_file_path)) {
			mkdir($new_file_path, 0777, TRUE);
		}
		$new_file_path .= $file_name;
		copy($file_path, $new_file_path);
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		if(defined('CURL_SSLVERSION_TLSv1')){
	    	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST,true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 
	        //1.文件路径之前必须要加@
	        //2.文件路径带中文就会失败，例如'img_1'=>'@C:\Documents and Settings\Administrator\桌面\Android壁纸\androids.gif'
	        array("media"=> "@".$new_file_path)
	    );
		$info = curl_exec($ch);
		
		file_exists($new_file_path) && file($new_file_path) && unlink($new_file_path);
		
		if (curl_errno($ch)) {
			log_write('POST MEDIA请求失败:' .curl_error($ch));
			curl_close($ch);
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$PostExp);
		}
		
		curl_close($ch);
		$info = json_decode($info, TRUE);
		if (isset($info['errcode']) && $info['errcode'] != 0) {
			throw new cls_wxqy_exp($info['errcode']);
		} else {
			return $info;
		}
	}
	
	/**
	 * POST数据到对应的微信URL
	 * @param unknown_type $url			POST请求的目标URL
	 * @param unknown_type $data		请求发送的数据
	 * @param unknown_type $is_json		数据是否为json格式，默认为FALSE，即数据为数组
	 * @return 数组形式的结果
	 */
	private function do_post($url, $data, $is_json=FALSE) {
		if ($is_json) {
			$json_data = $data;
		} else {
			$data = urlencode_array($data);
			$json_data = urldecode(json_encode($data));	
			log_write($json_data);
		}
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		if(defined('CURL_SSLVERSION_TLSv1')){
	    	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		$info = curl_exec($ch);
		
		if (curl_errno($ch)) {
			log_write('POST请求失败:' .curl_error($ch));
			curl_close($ch);
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$PostExp);
		}
		
		curl_close($ch);
		$info = json_decode($info, TRUE);
		if (isset($info['errcode']) && $info['errcode'] != 0) {
			throw new cls_wxqy_exp($info['errcode']);
		} else {
			return $info;
		}
	}
	
	/**
	 * GET数据到对应的URL，需要返回格式为json字符串
	 * @param unknown_type $url	请求的目标URL
	 * @return
	 */
	private function do_get($url) {
		$content = get_content($url, FALSE);
		if (is_null($content) || empty($content)) {
			throw new cls_wxqy_exp(WXQY_ERR_CODE::$GetExp);
		}
		$content = json_decode($content, TRUE);
		if (isset($content['errcode']) && $content['errcode'] != 0) {
			throw new cls_wxqy_exp($content['errcode']);
		}
		return $content;
	}
	
}

//end of file