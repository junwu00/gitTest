<?php
/**
 * 微信消息类，用于获取消息模板
 * @author yangpz
 * @date 2014-10-20
 */
class cls_wxqy_msg {
	/** 
	 * 文字消息模板: 1.ToUserName, 2.FromUserName, 3.CreateTime, 4.Content
	 */
	public static $TextTemp = '<xml>
	   <ToUserName><![CDATA[%s]]></ToUserName>
	   <FromUserName><![CDATA[%s]]></FromUserName> 
	   <CreateTime>%s</CreateTime>
	   <MsgType><![CDATA[text]]></MsgType>
	   <Content><![CDATA[%s]]></Content>
	</xml>';
	
	/** 
	 * 图文消息模板 : 1.ToUserName, 2.FromUserName, 3.CreateTime, 4.ArticleCount, 5.Articles
	 */
	public static $NewsTemp = '<xml>
	   <ToUserName><![CDATA[%s]]></ToUserName>
	   <FromUserName><![CDATA[%s]]></FromUserName>
	   <CreateTime>%s</CreateTime>
	   <MsgType><![CDATA[news]]></MsgType>
	   <ArticleCount>%d</ArticleCount>
	   <Articles>%s</Articles>
	</xml>';
	
	/** 
	 * 图文消息的消息项模板: 1.Title, 2.Description, 3.PicUrl, 4.Url
	 */
	public static $NewsItemTemp = '<item>
		<Title><![CDATA[%s]]></Title> 
		<Description><![CDATA[%s]]></Description>
		<PicUrl><![CDATA[%s]]></PicUrl>
		<Url><![CDATA[%s]]></Url>
	</item>';
}

// end of file