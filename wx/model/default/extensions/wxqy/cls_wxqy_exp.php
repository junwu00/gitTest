<?php
/**
 * 
 * @author yangpz
 * @date 2014-10-18
 *
 */
class cls_wxqy_exp extends SCException {
	
	public function __construct($err_code, $code = 99999) {
		$err_type = substr($err_code, 0, 1);
		if ($err_type == '4' || $err_type == '5' || $err_type == '6' || $err_type == '8' || $err_code == -1) {	//微信官方错误码
			$msg = WXQY_ERR_CODE::get_wx_desc($err_code);
		} else {														//自定义错误码
			$msg = WXQY_ERR_CODE::get_desc($err_code);
		}
		
		$msg = $err_code.', '.$msg;
        parent::__construct($msg, $code);  
    }  
    
}

// end of file