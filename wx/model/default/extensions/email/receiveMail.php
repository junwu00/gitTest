<?php 
class receiveMail  
{  
    var $server='';  
    var $username='';  
    var $password='';  
    var $path =SYSTEM_FILE;  
    var $marubox='';                      
      
    var $email='';            
      
	public function __construct($service,$port,$ssl,$type,$email,$password) {
		$service .=$type=='pop'||$type=='POP'?'/pop3':''; 
		$this ->server = '{'.$service.':'.$port.($ssl==1 ? "/ssl" : "").'}INBOX';
		$this ->username = $email;
		$this -> password = $password;
	}
    function connect() //Connect To the Mail Box  
    {  
        $this->marubox=@imap_open($this->server,$this->username,$this->password);  
        
        if(!$this->marubox)
        {  
        	log_write('邮件服务器链接失败！');
            return false;
        }  
        log_write('邮件服务器链接成功！');
        return true;
    }  
     function get_uid($mid){
     	return imap_uid($this-> marubox, $mid);
     }
    function get_mail($mid){
      log_write('获取邮件，序号：'.$mid.";");
      $mail_header=imap_header($this->marubox,$mid);   //获取信件标头
	  $content = $this -> matchMailHead($mail_header); //匹配信件标头
		 
		 $text = $this->getBody($mid);
		 $encode = mb_detect_encoding($text, array('ASCII','UTF-8','GB2312','GBK','BIG5'));
		 
		 if($encode== 'UTF-8')
		    $content['content']=$text;
		 else{
		    $text2 = iconv('GBK', 'UTF-8', $text);
		    if(!mb_detect_encoding($text2, array('ASCII','UTF-8','GB2312','GBK','BIG5'))=='UTF-8')
		      $content['content'] = iconv('GB2312', 'UTF-8', $this->test(imap_base64($text)));
		    else 
		     $content['content']=$text2;
		}
		
		$content['attach'] = $this -> getAttach($mid);
		return $content;
    }
      
    function getTotalMails() //Get Total Number off Unread Email In Mailbox  
    {  
        if(!$this->marubox)  
            return false;  
  
        $headers=imap_headers($this->marubox);  
        return count($headers);  
    }  
   	 function matchMailHead($mail_header){
	//		var_dump($mail_header);
	//		var_dump($mail_header->to);
			
		    $sender=$mail_header->from[0];  
		        $sender_replyto=$mail_header->reply_to[0];  
		        if(strtolower($sender->mailbox)!='mailer-daemon' && strtolower($sender->mailbox)!='postmaster')  
		        {  
		            $mail_details=array(
		            		'message_id'=>$mail_header->message_id,
		                    'from'=>strtolower($sender->mailbox).'@'.$sender->host,  
		                    'fromName'=>iconv('GB2312', 'UTF-8', $this->test($sender->personal)),  
		                    'subject'=>iconv('GB2312', 'UTF-8', $this->test($mail_header->subject)),
		                    'date' =>  date('Y-m-d H:i:s',strtotime($mail_header->date)),
		                    'to'=>$this->get_address($mail_header->toaddress),//strtolower( iconv('GB2312', 'UTF-8', $this->test($mail_header->toaddress))),
		                    'cc'=>$this->get_address($mail_header->ccaddress)//strtolower( iconv('GB2312', 'UTF-8', $this->test($mail_header->ccaddress)))
		                );  
		        }  
		        return $mail_details;  
		}

		function get_address($toaddress){
		   $address = array();
		   if(strpos($toaddress, ',')){
		       $arradd=split(",",$toaddress);
		       foreach($arradd as $key => $add){
		       	  $content = strtolower( iconv('GB2312', 'UTF-8',$this->test($add)));
		       	  $content!='' && $address[$key]= $content;
		       }
		   }else{
		          $content = strtolower( iconv('GB2312', 'UTF-8',$this->test($toaddress)));
		          $content!='' && $address[0]= $content;
		   }
		   return $address;
		}
		
	   function test($strHead){   
	   	  
		if(ereg("=\?.{0,}\?[Bb]\?",$strHead)){   

		    $arrHead=split("=\?.{0,}\?[Bb]\?",$strHead);  
		 
		  while(list($key,$value)=each($arrHead)){   
		    if(ereg("\?=",$value)){   
		      $arrTemp=split("\?=",$value);   
		      $arrTemp[0]=base64_decode($arrTemp[0]);   
		      $arrHead[$key]=join("",$arrTemp);
			}   
		  }   
		
		  $strHead=join("",$arrHead);   
		}   
		  return $strHead;   
     }  
		
     
      function get_mime_type(&$structure) //Get Mime type Internal Private Use  
    {   
        $primary_mime_type = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");   
          
        if($structure->subtype) {   
            return $primary_mime_type[(int) $structure->type] . '/' . $structure->subtype;   
        }   
        return "TEXT/PLAIN";   
    }   
     
    
     function get_part($msg_number, $mime_type, $structure = false, $part_number = false) //Get Part Of Message Internal Private Use  
    {   
        if(!$structure) {   
            $structure = imap_fetchstructure( $this->marubox, $msg_number);   
        }   
        if($structure) {   
            if($mime_type == $this->get_mime_type($structure))  
            {   
                if(!$part_number)   
                {   
                    $part_number = "1";   
                }   
                $text = imap_fetchbody( $this->marubox, $msg_number, $part_number);   
                if($structure->encoding == 3)   
                {  
                    return imap_base64($text);   
                }   
                else if($structure->encoding == 4)   
                {   
                    return imap_qprint($text);   
                }   
                else  
                {   
                    return $text;   
                }   
            }   
            if($structure->type == 1) /* multipart */   
            {   
                while(list($index, $sub_structure) = each($structure->parts))  
                {   
                    if($part_number)  
                    {   
                        $prefix = $part_number . '.';   
                    }   
                    $data = $this->get_part(  $msg_number, $mime_type, $sub_structure, $prefix . ($index + 1));   
                    if($data)  
                    {   
                        return $data;   
                    }   
                }   
            }   
        }   
        return false;   
    } 
    
      function getBody($mid) // Get Message Body  
    {  

        $body = $this->get_part( $mid, "TEXT/HTML");  
        if ($body == "")  
            $body = $this->get_part( $mid, "TEXT/PLAIN");  
        if ($body == "") {   
            return "";  
        }  
        return $body;  
    } 
    
     function search_unsee_Mails(){
     	if(!$this->marubox)  
            return false; 
            
      //  var_dump(imap_search($this ->marubox,"UNSEEN"));
        return imap_search($this ->marubox,"UNSEEN");
     }

    
     function deleteMails($mid) // Delete That Mail  
    {  
        if(!$this->marubox)  
            return false;  
      
        imap_delete($this->marubox,$mid);  
    }  
    
     function close_mailbox() //Close Mail Box  
    {  
        if(!$this->marubox)  
            return false;  
  
        imap_close($this->marubox,CL_EXPUNGE);  
    }  
    
    
     function GetAttach1($mid, $path) { // Get Atteced File from Mail  
        if (!$this->marubox) {  
            return false;  
        }  
  
        $struckture = imap_fetchstructure($this->marubox, $mid);  
        $ar = "";  
        if ($struckture->parts) {  
        	
            foreach ($struckture->parts as $key => $value) {  
                $enc = $struckture->parts[$key]->encoding;  
                if ($struckture->parts[$key]->subtype == 'OCTET-STREAM') {
                	var_dump($struckture->parts[$key]->parameters[1]);  
                    $name = base64_decode($struckture->parts[$key]->parameters[1]->value);  
                    echo $name;
                    $name = iconv("gbk", 'utf-8', $name);  
                  //  $name = substr($name, 7); // 未知原因 base64解密以后字符串前边多了三个 乱码字符    截取7以后的字符串  
                    $message = imap_fetchbody($this->marubox, $mid, $key + 1);  
                    switch ($enc) {  
                        case 0:  
                            $message = imap_8bit($message);  
                            break;  
                        case 1:  
                            $message = imap_8bit($message);  
                            break;  
                        case 2:  
                            $message = imap_binary($message);  
                            break;  
                        case 3:  
                            $message = imap_base64($message);  
                            break;  
                        case 4:  
                            $message = quoted_printable_decode($message);  
                            break;  
                        case 5:  
                            $message = $message;  
                            break;  
                    }  
                    $name = explode('.', $name);  
                    $firs_name = urlencode($name[0]);  
                    $filename = $firs_name . '.' . $name[1];  
                    $fp = fopen($path . $filename, "w");  //fopen  中文文件名    
                    fwrite($fp, $message);  
                    fclose($fp);  
                    $ar = $ar . $filename . ",";  
                }  
                }  
        }  
        $ar = substr($ar, 0, (strlen($ar) - 1));  
        return $ar;  
     }
     
     
     function getAttach($m){ 
     	
     	$structure = imap_fetchstructure($this->marubox, $m);

       $attachments = array();
       if(isset($structure->parts) && count($structure->parts)) {
         for($i = 0; $i < count($structure->parts); $i++) {
           $attachments[$i] = array(
              'is_attachment' => false,
              'filename' => '',
              'name' => '',
              'attachment' => '');

           if($structure->parts[$i]->ifdparameters) {
             foreach($structure->parts[$i]->dparameters as $object) {
               if(strtolower($object->attribute) == 'filename') {
                 $attachments[$i]['is_attachment'] = true;
                 $attachments[$i]['filename'] = $object->value;
               }
             }
           }

           if($structure->parts[$i]->ifparameters) {
             foreach($structure->parts[$i]->parameters as $object) {
               if(strtolower($object->attribute) == 'name') {
                 $attachments[$i]['is_attachment'] = true;
                 $attachments[$i]['name'] = $object->value;
               }
             }
           }

           if($attachments[$i]['is_attachment']) {
           //	var_dump(imap_fetchbody($this->marubox, $m, $i+1));
             $attachments[$i]['attachment'] = imap_fetchbody($this->marubox, $m, $i+1);
             if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
               $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
             }
             elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
               $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
             }
             $attachments[$i]['length'] = strlen($attachments[$i]['attachment']);
           }             
         } // for($i = 0; $i < count($structure->parts); $i++)
       } // if(isset($str
       $attach = array();
       foreach($attachments as $key => $attachment) {
       			if($attachment['is_attachment']==false){
					log_write(json_encode($attachment['is_attachment']));
       				continue;
       			}
       			$file_name = false;
		        if($attachment['length']<1000000){
	             	$file_name = uniqstr().substr($attachment['name'],strrpos($attachment['name'],'.'));
	             	log_write('保存文件：文件名为：'.$file_name);
			        file_put_contents($this->path.$file_name, $attachment['attachment']);
                }
                $attachment['file_name'] =$file_name;
                
                array_push($attach, array('name'=>$attachment['name'],'fileName'=>$file_name,'length'=>$attachment['length']));
        }
       return $attach;
    }
}
  function get_decode_value($message, $encoding) {
        switch($encoding) {
            case 0:case 1:$message = imap_8bit($message);break;
            case 2:$message = imap_binary($message);break;
            case 3:case 5:$message=imap_base64($message);break;
            case 4:$message = imap_qprint($message);break;
        }
        return $message;
    }