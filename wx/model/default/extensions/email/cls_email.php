<?php
include_once (EMAIL_ROOT.'PHPMailer.php');
include_once (EMAIL_ROOT.'receiveMail.php');
include_once (EMAIL_ROOT.'SMTP.php');
class cls_email {
	/**
	 * 发送邮件
	 * @param unknown_type $service	                     发件邮箱服务器信息
	 * @param unknown_type $from_user		发件人信息
	 * @param unknown_type $cc_user		           抄送人信息
	 * @param unknown_type $bcc_user		密送人信息
	 * @param unknown_type $subject	     	主题
	 * @param unknown_type $content	     	内容
	 */
	public function send($service, $from_user, $to_user,$cc_user,$bcc_user,$subject,$content,$anexfiles=false,$messageID=false){
		if(!$service||!$from_user|| !($to_user || $cc_user || $bcc_user) ||!$content||!is_array($to_user)){
		   throw new SCException('发送信息错误');
		}
		log_write('开始发送邮件：service:'.json_encode($service).',from='.$from_user['email'].', to='.json_encode($to_user).', title='.$subject, 'EMAIL');
		$mail = new PHPMailer(); 
	
		$mail -> IsHTML(TRUE); 	 									            //是否支持HTML邮件
//		$mail->SMTPDebug  = 1;													//是否显示调试信息
												
		$mail -> WordWrap 	= 50; 												// set word wrap 
		$mail -> CharSet 	= 'UTF-8';											//字符设置
		$mail -> Encoding 	= 'base64';											//编码方式

		$mail -> Port       = $service['send_port'];
		$mail -> SMTPSecure = $service['send_ssl']==1?'ssl':'';

		$mail -> IsSMTP(); 														
		$mail -> Host 		= $service['send_service']; 
		$mail -> SMTPAuth 	= TRUE; 											// turn on SMTP authentication 
		$mail -> Username 	= $from_user['email']; 			
		$mail -> Password 	= g('des')->decode($from_user['password']);	

		$mail -> From 		= $from_user['email'];
		$mail -> FromName 	= $from_user['email'];
		
		if($to_user)															//添加接收人
		foreach($to_user as $key => $t_user){
		   $mail -> AddAddress($key, $t_user); 
		}
			

		if($cc_user)															//添加抄送人
	    foreach($cc_user as $key => $c_user){
		   $mail -> addCC($key, $c_user); 

		}
		
		if($bcc_user)															//添加密送人
	    foreach($bcc_user as $key => $b_user){
		   $mail -> addBCC($key, $b_user); 

		}
		if($anexfiles)															//添加附件
     	foreach($anexfiles as $anex){
		   $mail->AddAttachment($anex['path'],$anex['name']);
		}
		if(!$messageID)
			$messageID = time();
		$mail -> setMessageID($messageID);
		
		$mail -> Subject 	= $subject; 
		$mail -> Body 		= $content; 
		
		try{
			$mail -> Send();
			log_write('邮件发送成功','EMAIL');
			return true;		
		}catch(SCException $e){
			log_write('邮件接收失败，失败原因：'. $mail -> ErrorInfo, 'EMAIL');
		    throw $e;
		}
	}
	/*
	 * 验证邮箱账号密码
	 *  @param unknown_type $service 服务器信息
	 *  @param unknown_type $from_user 需要验证的账号信息
	 *  @return true/false
	 */
	public function test($service,$from_user){
		log_write('开始验证邮箱有效性,email:'.$from_user['email'].'EMAIL');
		try{
			$mail = new PHPMailer();
			//$mail -> SMTPDebug  = 1;

			$mail -> IsHTML(TRUE); 	 									            //是否支持HTML邮件
			$mail -> IsSMTP();

			$mail -> WordWrap 	= 50; 												// set word wrap
			$mail -> CharSet 	= 'UTF-8';											//字符设置
			$mail -> Encoding 	= 'base64';											//编码方式
			$mail -> Host 		= $service['send_service'];
			$mail -> Port       = $service['send_port'];
			$mail -> SMTPSecure = $service['send_ssl'] == 1 ? 'ssl' : '';
			$mail -> SMTPAuth 	= TRUE; 											// turn on SMTP authentication
			$mail -> Username 	= $from_user['email'];
			$mail -> Password 	= $from_user['password'];
			$mail -> From 		= $from_user['email'];
			$mail -> FromName 	= $from_user['email'];

			if(!$mail -> smtpConnect())
				throw new SCException('邮箱验证失败');
			
			log_write('邮箱验证成功','EMAIL');
			return true;
	      }catch(SCException $e){
			log_write('邮箱验证失败：'.$mail -> ErrorInfo,'EMAIL');
			throw $e;
		}
	}

	/*
	 * 接收邮件
	 * @param unknown_type $service  服务器信息
	 * @param unknown_type $user  账号信息
	 * @return $content 邮件信息
	 */
	public function receive($service,$user){
		log_write('开始获取邮件,服务器地址：'.$service['service'].',端口：'.$service['receive_port'].',email:'.$user['email'].',password:'.$user['password'],'EMAIL');
		try{
			$receiveMail = new receiveMail($service['service'], $service['receive_port'], $service['receive_ssl'],$service['type'],$user['email'], $user['password']);
			if(!$receiveMail ->connect()){
				throw new SCException('连接服务器失败');
			}
			$unsee = $receiveMail -> search_unsee_Mails();
			$content=false;
			foreach($unsee as $key => $i){
				$content[$key] = $receiveMail ->get_mail($i); 
			}
			$receiveMail -> close_mailbox();
			log_write('获取邮件成功','EMAIL');
		    return $content;	
		}catch(Exception $e){
			log_write('获取邮件失败,errmsg:'.$e->getMessage(),'EMAIL');
			return false;
		}
	}  
}