<?php
/** 
 * 插件扩展的配置文件 
 * 
 * 仅能通过此文件调用插件
 * 
 * @author yangpz
 * @create 2014-06-28
 * @version 1.0
 */
 
!defined('SYSTEM') && die('ACCESS DENIED');

define('EXCEL_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_excel_url = EXCEL_ROOT.'cls_excel.php';
!class_exists('cls_excel') && file_exists($cls_excel_url) && include_once($cls_excel_url);

/* End of file global.config.php */ 
