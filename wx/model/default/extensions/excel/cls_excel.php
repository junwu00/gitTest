<?php
include_once (EXCEL_ROOT.'PHPExcel.php');

/** 
 * excel工具在
 * 
 * @author yangpz
 * @create 2014-06-28
 * @version 1.0
 */

class cls_excel {
	
	/**
	 * 导出数据到EXCEL 2003
	 * @param unknown_type $data			要导出的数据，二维数组形式，如：array(array('name' => 'yanghd', 'acct' => 'sc_yanghd'), array('name' => 'yangpz', 'acct' => 'sc_yangpz'),);
	 * @param unknown_type $fields_key		数据中各记录的KEY值，如：array('name', 'acct'), array('姓名', '账号')
	 * @param unknown_type $fields_name		表头（中文），如：array('姓名', '账号)
	 */
	public function export($data, $fields_key, $fields_name) {
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel 	-> getProperties() 
						-> setCreator("Maarten Balliauw")
						->setLastModifiedBy("Maarten Balliauw")
						->setTitle("Office 2003 XLS Test Document")
						->setSubject("Office 2003 XLS Test Document")
						->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
						->setKeywords("office 2003 openxml php")
						->setCategory("Test result file");
	
		$rows_name = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		for ($i=0; $i<count($fields_key); $i++) {	//设置文件头
			$cell_style = $objPHPExcel -> getActiveSheet() -> getStyleByColumnAndRow($i, 1);
			$cell_style -> getFont() -> setBold(true);
			$cell_style -> getFill() -> setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$cell_style -> getFill() -> getStartColor() -> setARGB('EE62C462');
			$objPHPExcel -> getActiveSheet() -> setCellValue($rows_name[$i].'1', $fields_name[$i]);
			$this -> set_center($objPHPExcel, $rows_name[$i].'1');
		}
		
		for ($i=0; $i<count($data); $i++) {
			for ($j=0; $j<count($fields_key); $j++) {
				$objPHPExcel -> getActiveSheet() -> setCellValue($rows_name[$j].($i+2), $data[$i][$fields_key[$j]]);
			}
		}
		
		$file_name = date('y-m-d h_i_s', time()).'.xls';
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Transfer-Encoding: binary");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Pragma: no-cache");
		
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');  
		header('content-disposition:attachment;filename="' .$file_name . '"');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
	    iconv('utf-8', 'gb2312', $file_name);
	    ob_flush();
		ob_clean();
		$objWriter->save('php://output');
	}
	
		/**
		 * 导入数据到EXCEL 2007
		 * @param unknown_type $data			要导入的文件对象 $_FILES['filename']
		 * @param unknown_type $indata		           返回文件内容二维数组，若文件为空或者文件格式不对，返回false
		 */
	 public function input($data) {
        //判断文件类型
	    if($data['type']!= "application/vnd.ms-excel" && 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'!=$data['type'])
		   return false;
		   
		$PHPExcel = new PHPExcel();
		$PHPReader = new PHPExcel_Reader_Excel2007();
		$PHPExcel = $PHPReader->load($data['tmp_name']);//读取文件

		$indata = $PHPExcel->getSheet(0)->toArray();//转为array
		
		if($indata)
		   return $indata;
		else 
		   return false;
	 }
	 
	 /**
	  * 读取csv文件
	  * @param unknown_type $filename	文件路径
	  * @return 数据数组
	  */
	 public function read_from_csv($filename) {
		$file = fopen($filename, "r");
		$result = array();
		while(! feof($file)) {
			$data = fgetcsv($file);
			$data = eval('return '.iconv('gbk','utf-8',var_export($data, TRUE)).';');
			is_array($data) && array_push($result, $data);
		}
		
		fclose($file);
		return count($result) > 0 ? $result : FALSE;
	 }
	
	/**
	 * 单元格内容居中
	 * @param unknown_type $objPHPExcel	EXCEL对象
	 * @param unknown_type $cell		单元格对象
	 */
	private function set_center($objPHPExcel, $cell) {
		$objStyleA1 = $objPHPExcel -> getActiveSheet() -> getStyle($cell);
	    $objAlignA1 = $objStyleA1 -> getAlignment();  
	    $objAlignA1 -> setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    //左右居中  
	    $objAlignA1 -> setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  		//上下居中  
	}
	
}

/* End of file cls_excel.php */ 