<?php
/**
 * @copyright 2013
 * @description: Smarty配置
 * @file: global.config.php
 * @author: Huang Xiang
 * @charset: UTF-8
 * @time: 2012-06-20 01:19:41
 * @version 1.0
**/

!defined('SYSTEM') && die('ACCESS DENIED');

define('SMARTY', 					'SMARTY');	//定义名称
define('SMARTY_VERSION', 			'1.0.0');	//版本
define('SMARTY_ROOT', 				preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');	//根目录
define('SMARTY_DATA', 				SYSTEM_DATA.'smarty/');		//数据目录
define('SMARTY_DATA_CONFIG', 		SMARTY_DATA.'configs/');	//configs
define('SMARTY_DATA_CACHE', 		SMARTY_DATA.'cache/');		//cache
define('SMARTY_DATA_TPL_C', 		SMARTY_DATA.'templates_c/');//templates_c
define('SMARTY_TPL', 				SYSTEM_TPL);	//模版根路径
define('SMARTY_LEFT_DELI', 			'<{');	//边界 left_delimiter
define('SMARTY_RIGHT_DELI', 		'}>');	//边界 right_delimiter
define('SMARTY_COM_CHECK', 			TRUE);	//compile_check
define('SMARTY_DEBUG', 				FALSE);	//debugging
define('SMARTY_CACHE', 				FALSE);	//caching
define('SMARTY_USE_SUB_DIRS', 		TRUE);	//use_sub_dirs
define('SMARTY_CACHE_LIFE_TIME', 	3600);	//cache_lifetime
//初始操作

$smarty_class_url 	= SMARTY_ROOT.'Smarty.class.php';
$cls_smarty_url 	= SMARTY_ROOT.'cls_smarty.php';

!class_exists('Smarty') 	&& file_exists($smarty_class_url) 	&& include_once($smarty_class_url);
!class_exists('cls_smarty') && file_exists($cls_smarty_url) 	&& include_once($cls_smarty_url);

/* End of file global.config.php */ 