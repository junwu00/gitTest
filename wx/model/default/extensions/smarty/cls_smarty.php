<?php
/**
 * @copyright 2014
 * @description: Smarty重载类
 * @file: cls_smarty.php
 * @author: Huang.Xiang
 * @charset: UTF-8
 * @time: 2014-03-03 19:28:48
 * @version 1.0
**/

class cls_smarty{
	protected $compile_check 	= TRUE;					//是否检测模板内容改变 [true]
	protected $debugging 		= FALSE; 				//debugging [false]
	protected $caching 			= FALSE; 				//是否缓存 [false]
	protected $use_sub_dirs 	= TRUE; 				//是否目录分级缓存 [true]
	protected $cache_lifetime 	= 3600;					//缓存时间 caching是true,1,2才生效 [3600]
	protected $config_dir 		= './configs/';			//配置文件目录 [./configs/]
	protected $cache_dir 		= './cache/'; 			//模板缓存目录 [./cache/]
	protected $template_dir 	= './templates/';		//模板目录 [./templates/]
	protected $compile_dir 		= './templates_c/';		//编译模板目录 [./templates_c/]
	protected $left_delimiter 	= '<{'; 				//左边界符 [<{]
	protected $right_delimiter	= '}>'; 				//右边界符 [}>]
	protected $smarty 			= NULL;					//Smarty对象 [NULL]
	
	/**
	 * @name: __construct
	 * @description: class被实例化时调用此函数
	 * @scope: public
	 * @return: void
	**/
	public function __construct(){
		if(!is_object($this -> smarty)){
			//配置
			$this -> compile_check 		= SMARTY_COM_CHECK;
			$this -> debugging 			= SMARTY_DEBUG;
			$this -> caching 			= SMARTY_CACHE;
			$this -> use_sub_dirs 		= SMARTY_USE_SUB_DIRS;
			$this -> cache_lifetime 	= SMARTY_CACHE_LIFE_TIME;
			$this -> config_dir 		= SMARTY_DATA_CONFIG;
			$this -> cache_dir 			= SMARTY_DATA_CACHE;
			$this -> template_dir 		= SMARTY_TPL;
			$this -> compile_dir 		= SMARTY_DATA_TPL_C;
			$this -> left_delimiter 	= SMARTY_LEFT_DELI;
			$this -> right_delimiter 	= SMARTY_RIGHT_DELI;
			//实例化
			$this -> smarty = new Smarty();
			$this -> smarty -> compile_check 	= $this -> compile_check;
			$this -> smarty -> debugging 		= $this -> debugging;
			$this -> smarty -> caching 			= $this -> caching;
			$this -> smarty -> use_sub_dirs 	= $this -> use_sub_dirs;
			$this -> smarty -> cache_lifetime 	= $this -> cache_lifetime;
			$this -> smarty -> config_dir 		= $this -> config_dir;
			$this -> smarty -> cache_dir 		= $this -> cache_dir;
			$this -> smarty -> template_dir 	= $this -> template_dir;
			$this -> smarty -> compile_dir 		= $this -> compile_dir;
			$this -> smarty -> left_delimiter 	= $this -> left_delimiter;
			$this -> smarty -> right_delimiter 	= $this -> right_delimiter;
		}
	}
	
	/**
	 * @name: smarty
	 * @description: 返回smarty对象
	 * @scope: public
	 * @return: object
	**/
	public function smarty(){
		return $this -> smarty;
	}
	
	/**
	 * @name: show
	 * @description: 显示网页
	 * @scope: public
	 * @param: string 网页模版的路径
	 * @return: void
	**/
	public function show($html_file, $is_exit=TRUE){
		if(!empty($GLOBALS['PAGEINFO'])){
			$this -> assign('PAGEINFO', $GLOBALS['PAGEINFO']);
		}
		$this -> smarty -> display($html_file);
		$is_exit && exit;
	}
	
	
	/**
	 * @name: show
	 * @description: 显示网页,添加ifram遮罩效果
	 * @scope: public
	 * @param: string 网页模版的路径
	 * @return: void
	**/
	public function fshow($html_file, $is_exit=TRUE, $def_temp='mask_iframe.html') {
		$show_frame = get_var_get('sframe');
		if (empty($show_frame)) {	//显示框架
			$this -> smarty -> assign('source_url', get_this_url());
			$target_temp = $def_temp;
		} else {					//显示页面
			if(!empty($GLOBALS['PAGEINFO'])){
				$this -> assign('PAGEINFO', $GLOBALS['PAGEINFO']);
			}
			$target_temp = $html_file;
		}
		$this -> smarty -> display($target_temp);
		$is_exit && exit; 
	}
	
	/**
	 * @name: assign
	 * @description: 设置变量
	 * @scope: public
	 * @param: mixed 被设置的变量
	 * @param: mixed 变量值
	 * @return: void
	**/
	public function assign($var_name, $var_value=NULL){
		$this -> smarty -> assign($var_name, $var_value);
	}
	
	/**
	 * @name: fetch
	 * @description: 获取内容
	 * @scope: public
	 * @param: string 网页模版的路径
	 * @return: string
	**/
	public function fetch($html_file){
		if(!empty($GLOBALS['PAGEINFO'])){
			$this -> assign('PAGEINFO', $GLOBALS['PAGEINFO']);
		}
		return $this -> smarty -> fetch($html_file);
	}
}

/* End of file cls_smarty.php */ 