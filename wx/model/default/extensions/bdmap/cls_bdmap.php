<?php
/**
 * 百度地图处理类
 * 
 * @author yangpz
 * @date 2014-11-21
 */
class cls_bdmap {
	/** 百度地图API的KEY */
	private static $BaiduKey = '8E367092188191ec8521afc2b11bc455';
	/** 高德地图API的KEY */
	private static $GDKey = 'ffed29929ee70ed83eaa90a9d248b07e';
	
	/**
	 * 根据ip获取地理位置坐标
	 * @param unknown_type $ip		IP地址
	 * @param unknown_type $field	指定要获取的字段，详见：http://developer.baidu.com/map/index.php?title=webapi/ip-api
	 */
	public function get_local_by_ip($ip, $fields='*') {
		$url = "http://api.map.baidu.com/location/ip?ak=".self::$BaiduKey."&ip={$ip}&coor=bd09ll";
		$data = get_content($url, FALSE);
		$data = json_decode($data, TRUE);
		if ($data['status'] == 0) {	//获取成功
			if ($fields == '*') 			return $data;
				
			if (!is_array($fields))	 		return isset($data[$fields]) ? $data[$fields] : FALSE;
			
			$ret = $data;
			foreach ($fields as $field) {
				isset($ret[$field]) && $ret = $ret[$field];
			}
			return $ret;
		}
		return FALSE;
	}
	
	/**
	 * 求两个已知经纬度之间的距离,单位为米
	 * @param unknown_type $lng1	A点经度
	 * @param unknown_type $lat1	A点纬度
	 * @param unknown_type $lng2	B点经度
	 * @param unknown_type $lat2	B点纬度
	 */
	public function get_distance($lng1, $lat1, $lng2, $lat2){
		//将角度转为狐度
		$radLat1 = $this -> deg2rad($lat1);//deg2rad()函数将角度转换为弧度
		$radLat2 = $this -> deg2rad($lat2);
		$radLng1 = $this -> deg2rad($lng1);
		$radLng2 = $this -> deg2rad($lng2);
		$a = $radLat1 - $radLat2;
		$b = $radLng1 - $radLng2;
		$s = 2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;
		return $s;
	}
	
	/**
	 * 将GPS的经纬度，转换成百度地图的坐标
	 * 
	 * from: 来源坐标系   （0表示原始GPS坐标，2表示Google坐标）
	 * to: 转换后的坐标   （4就是百度自己啦，好像这个必须是4才行）
	 * x: 经度
	 * y: 纬度
	 * 
	 * @param unknown_type $lng	经度
	 * @param unknown_type $lat	纬度
	 */
	public function trans2baidu($lng, $lat) {
		$url = "http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x={$lng}1&y={$lat}";
		$result = get_content($url, FALSE);
		$result = json_decode($result, TRUE);
		if (isset($result['error']) && $result['error'] == 0) {
			$result = array(
				'lng' => base64_decode($result['x']),
				'lat' => base64_decode($result['y']),
			);
			return $result;
		} else {
			throw new Exception('转换成百度坐标失败', 0);
			exit();
		}
	}
	
	/**
	 * 将GPS的经纬度，转换成高德地图的坐标(小数点5、6位会有变动)
	 * @param unknown_type $lng
	 * @param unknown_type $lat
	 * @throws Exception
	 */
	public function trans2gd($lng, $lat) {
		//返回结果，如：{"status":"1","info":"ok","locations":"113.369857,23.131353"}
		$url = "http://restapi.amap.easywork365.com/v3/assistant/coordinate/convert?locations={$lng},{$lat}&coordsys=gps&output=json&key=".self::$GDKey;
		$result = get_content($url, FALSE);
		$result = json_decode($result, TRUE);
		if (isset($result['status']) && $result['status'] == 1) {
			$position = $result['locations'];
			$position = explode(',', $position);
			$result = array(
				'lng' => $position[0],
				'lat' => $position[1],
			);
			return $result;
		} else {
			throw new Exception('转换成高德坐标失败', 0);
			exit();
		}
	}
	
	/**
	 * 获取高德地图静态图片
	 * @param unknown_type $lng		
	 * @param unknown_type $lat
	 * @param unknown_type $width	图片宽度
	 * @param unknown_type $height	图片高度
	 */
	public function getGdStaticImg($lng, $lat, $width=300, $height=200) {
		$map_url = "http://restapi.amap.easywork365.com/v3/staticmap?location={$lng},{$lat}&zoom=16&size={$width}*{$height}&markers=mid,,A:{$lng},{$lat}&key=".self::$GDKey;
		return $map_url;
	}
	
	//-------------------------------------------------------内部实现-----------------------------------------------

	/**
	 * 角度转为狐度
	 * @param unknown_type $d
	 */
	private function deg2rad($d) {  
	    return $d * 3.1415926535898 / 180.0;  
	}
}

// end of file