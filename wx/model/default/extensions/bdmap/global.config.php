<?php
/** 
 * 百度地图功能扩展 配置文件
 * 
 * @author yangpz
 * @create 2014-11-20
 */

!defined('SYSTEM') && die('ACCESS DENIED');

define('BDMAP_ROOT', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');    //根目录

$cls_bdmap_url = BDMAP_ROOT.'cls_bdmap.php';
!class_exists('cls_bdmap') && file_exists($cls_bdmap_url) && include_once($cls_bdmap_url);

/* end of file */ 