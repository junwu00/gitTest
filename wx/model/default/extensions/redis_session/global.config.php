<?php
/** 
 * redis_session扩展的配置文件 
 * 
 * 仅能通过此文件调用redis_session
 * 必须保证有可用的redis缓存机制
 * 
 * @author LiangJianMing
 * @create 2015-01-29
 * @version 1.0
 */
 
!defined('SYSTEM') && die('ACCESS DENIED');

ini_set('session.save_handler', 'user');
//session的生存时间
define('SESSION_ROOT', 			preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)).'/');	//根目录
define('SET_SESSION_LIFE_TIME', 3600);
define('SET_SESSION_PREFFIX',   SYSTEM . ":REDISSESSION");

$cls_session_url = SESSION_ROOT.'cls_session.php';
!class_exists('cls_session') && file_exists($cls_session_url) && include_once($cls_session_url);
g('session') -> session_init();

/* End of file global.config.php */ 
