<?php
/** 
 * 依赖于Redis的SESSION操作类
 * 
 * @author LiangJianMing
 * @create 2015-01-29
 * @version 1.0
 */

class cls_session{
	private $max_life_time;	//session有效时间[秒]
	private $key_preffix;	//作为生成uid的标识前缀
	
  	/** 
  	 * 构造函数
  	 * 
  	 * @access public
  	 * @return void
  	 */
	public function __construct() {
		session_set_save_handler(
			array(&$this, 'open'),
			array(&$this, 'close'),
			array(&$this, 'read'),
			array(&$this, 'write'),
			array(&$this, 'destroy'),
			array(&$this, 'gc'),
			array(&$this, 'create_id')
		);
		
		$this -> max_life_time = SET_SESSION_LIFE_TIME ? SET_SESSION_LIFE_TIME : 1440;
  		$this -> key_preffix   = SET_SESSION_PREFFIX ? SET_SESSION_PREFFIX : 'REDISSESSION';
	}

	public function session_init() {
	}

	/**
	 * 获取redis缓存变量名
	 *
	 * @access public
	 * @param   
	 * @return string
	 */
	public function get_cache_key($str) {
		$key = md5($str);
		return $key;
	}

	/**
	 * 自定义session_id
	 *
	 * @access public 
	 * @return string
	 */
	public function create_id() {
		$time = time();
		$ip = get_ip();
		$uid = uniqid($this -> key_preffix, TRUE);
		$rand = get_rand_string(13);
		$key_str = 'session_id:ip:' . $ip . ':uid:' . $uid . ':time:' . $time . ':rand:' . $rand;
		$key = sha1($key_str);
		return $key;
	}

	/** 
	 * session_open 将提交到该函数
	 * 
	 * @access public
	 * @return boolean
	 */
  	public function open() {
  		$cache_key = $this -> get_cache_key(session_id());
  		g('redis') -> get_handler() -> setTimeout($cache_key, $this -> max_life_time);
  		return TRUE;
  	}
  	
  	/** 
  	 * session_close将提交到该函数
  	 * 
  	 * @access public
  	 * @return boolean
  	 */
	public function close() {
		return TRUE;
	}
	
	/** 
	 * session_read将提交到该函数
	 * 
	 * @access public
	 * @param: string $sess_id session_id
	 * @return mixed
	 */
	public function read($sess_id) {
		$cache_key = $this -> get_cache_key($sess_id);
		$ret = g('redis') -> get($cache_key);
		//g('redis') -> get_handler() -> setTimeout($cache_key, $this -> max_life_time);
		return $ret;
	}
	
	/** 
	 * session_write将提交到该函数
	 * 
	 * @access public
	 * @param: string $sess_id session_id
	 * @param: string $sess_data session数据
	 * @return boolean
	 */
	public function write($sess_id, $sess_data) {
		$cache_key = $this -> get_cache_key($sess_id);
		$result = g('redis') -> setex($cache_key, $sess_data, $this -> max_life_time);
		return $result;
	}

	/**
	 * session_destroy将提交到该函数
	 * 
	 * @access public
	 * @param: string $sess_id session_id
	 * @return boolean
	 */
	public function destroy($sess_id) {
		session_unset();
		$cache_key = $this -> get_cache_key($sess_id);
		g('redis') -> delete($cache_key);
		return TRUE;
	}

	/** 
	 * session_gc将提交到该函数
	 * 
	 * @access public
	 * @return void
	 */
	public function gc() {
	}
}

/* End of file cls_session.php */ 