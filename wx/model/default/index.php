<?php
/**
 * 单入口文件
 *
 * @author yangpz
 * @create 2014-10-20
 * @version 1.0
 */
include_once('global.config.php');

session_start();
//执行cookie初始化
g('cookie') -> cookie_start();

$app_id = 0;
$app = get_var_get('app', FALSE);
$module	= get_var_get('m', FALSE);
$action	= get_var_get('a', FALSE);
$corp_url = get_var_get('corpurl', FALSE);

empty($module) && $module = 'index';
empty($action) && $action = 'index';

$mod_cls = 'cls_'.$module; 

//错误页
if (strcmp($module, 'error') == 0) {	
	$mod_file = SYSTEM_MODS.'cls_error.php';
	include_once($mod_file);
	$mod = new cls_error();
	$mod -> index();
}

$test = $_GET['test']; 
if (!empty($test)) {
    $user = g("user") -> get_by_id($test, "*", true);
    $_SESSION[SESSION_VISIT_USER_ID] = $test;
    $_SESSION[SESSION_VISIT_USER_NAME] = $user["name"];
    $_SESSION[SESSION_VISIT_USER_PIC] = $user["pic_url"];
    $_SESSION[SESSION_VISIT_USER_WXID] = $user['weixin_id'];
    $_SESSION[SESSION_VISIT_USER_WXACCT] = $user["acct"];
    $_SESSION[SESSION_VISIT_POWERBY] = 'Easywork365技术支持';

    $com_id = $user['com_id'];
    $_SESSION[SESSION_VISIT_COM_ID] = $com_id;
    $com = g('company') -> get_by_id($com_id);
    $_SESSION[SESSION_VISIT_DEPT_ID] = $com['dept_id'];
    $_SESSION[SESSION_VISIT_CORP_ID] = $com['corp_id'];
    $_SESSION[SESSION_VISIT_CORP_URL] = $com['corp_url'];
    $_SESSION[SESSION_VISIT_CORP_SECRET] = $com["corp_secret"];

}

g('access') -> count();
g('httpauth') -> check_auth();

$mod = new $mod_cls();
$mod -> $action();						//处理业务逻辑

// end of file