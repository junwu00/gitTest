<?php
/**
 * 统计应用使用频率
 * @author yangpz
 * @date 2015-05-19
 *
 */
class cls_counter {
	
	public function index() {
		$ret = g('app_visit') -> get_cache_visit();
		cls_resp::echo_ok(cls_resp::$OK, 'data', $ret);
	}
}

//end of file