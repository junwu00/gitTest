<?php
/**
 * 单入口文件，只显示可完全公开的内容
 *
 * @author yangpz
 * @create 2014-11-06
 * @version 1.0
 */
include_once('global.config.php');
session_start();

g('filter') -> run();

//允许访问的模块
$allow_list = array(
	'index|article',
    // 测试卡券
    "card_test|send",
);

$module	= get_var_get('m', FALSE);
$action	= get_var_get('a', FALSE);
empty($module) && $module = 'index';
empty($action) && $action = 'index';

if (!in_array($module.'|'.$action, $allow_list))	//验证是否可访问
	cls_resp::show_warn_page(array('无访问权限'));

	
$mod_file = SYSTEM_MODS.'cls_'.$module.'.php';
if(!file_exists($mod_file)){
	cls_resp::show_warn_page(array('页面不存在'));
}

include_once ($mod_file);
$mod_cls = 'cls_'.$module; 
if (class_exists($mod_cls) && method_exists($mod_cls, $action)) {
	$mod = new $mod_cls();
	$mod -> $action();						//处理业务逻辑
} else {
	cls_resp::show_warn_page(array('页面不存在'));
}


// end of file