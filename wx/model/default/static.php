<?php
/**
 * 响应js、css文件
 *
 * @author LiangJianMing
 * @create 2015-08-19
 */
include_once('global.config.php');

$path = get_var_get('nspath');
$path = trim($path);
$path = rtrim($path, '\\');

$str_404 = 'HTTP/1.1 404 Not Found';

if (empty($path)) {
    Header($str_404);
    exit;
}

if (!preg_match('/([^\\/]+\.(js|css))$/i', $path, $match)) {
    Header($str_404);
    exit;
}

$virtual_name = $match[1];
$ext = $match[2];
$dir_path = str_replace($virtual_name, '', $path);
$dir_path = SYSTEM_ROOT . $dir_path;

$real_file = SYSTEM_ROOT . $path;
$compress_file = $dir_path . SYSTEM_COMPRESS_KEY . $virtual_name;
//$compress_file = $real_file;

$config_name = 'static.config.php';
$check = preg_match('/^apps\/([^\/]+)/', $path, $match);
if (!$check) {
    $conf_file = SYSTEM_ROOT . $config_name;
}else {
    $app_name = $match[1];
    $conf_file = SYSTEM_APPS . $app_name . '/' . $config_name;
}

$static_arr = array();
file_exists($conf_file) and $static_arr = include($conf_file);
!is_array($static_arr) and $static_arr = array();

//没有配置虚拟静态文件映射
if (!isset($static_arr[$virtual_name])) {
    //如果存在压缩文件，则输出压缩文件
    if (file_exists($compress_file)) {
        set_static_header($ext);
        $content = file_get_contents($compress_file);
        die($content);
    }
    //如果压缩文件不存在，取源文件
    if (file_exists($real_file)) {
        set_static_header($ext);
        $content = file_get_contents($real_file);
        die($content);
    }

    Header($str_404);
    exit; 
}

$virtual_arr = $static_arr[$virtual_name];
if (!is_array($virtual_arr) or empty($virtual_arr)) {
    Header($str_404);
    exit;
}

$content = '';
foreach ($virtual_arr as $val) {
    if (!preg_match('/([^\\/]+\.(js|css))$/i', $val, $tmp_match)) {
        continue;
    }
    $tmp_name = $tmp_match[1];
    $tmp_ext = strtolower($tmp_match[2]);
    $tmp_path = substr($val, 0, -strlen($tmp_name));
    $tmp_file = $tmp_path . SYSTEM_COMPRESS_KEY . $tmp_name;

    $tmp_content = '';
    if (file_exists($tmp_file)) {
        if ($tmp_ext == 'css') {
            $tmp_content = replace_css_url($tmp_file);
        }else {
            $tmp_content = file_get_contents($tmp_file);
        }
    //如果压缩文件不存在，取源文件
    }elseif(file_exists($val)) {
        if ($tmp_ext == 'css') {
            $tmp_content = replace_css_url($val);
        }else {
            $tmp_content = file_get_contents($val);
        }
    }

    $content .= $tmp_content;
}
unset($val);

if (empty($content)) {
    Header($str_404);
    exit;
}

set_static_header($ext);
die($content);

/* End of this file */