/*

BMAP_STATUS_SUCCESS				检索成功。对应数值“0”。
BMAP_STATUS_CITY_LIST			城市列表。对应数值“1”。
BMAP_STATUS_UNKNOWN_LOCATION	位置结果未知。对应数值“2”。
BMAP_STATUS_UNKNOWN_ROUTE		导航结果未知。对应数值“3”。
BMAP_STATUS_INVALID_KEY			非法密钥。对应数值“4”。
BMAP_STATUS_INVALID_REQUEST		非法请求。对应数值“5”。
BMAP_STATUS_PERMISSION_DENIED	没有权限。对应数值“6”。(自 1.1 新增)
BMAP_STATUS_SERVICE_UNAVAILABLE	服务不可用。对应数值“7”。(自 1.1 新增)
BMAP_STATUS_TIMEOUT				超时。对应数值“8”。(自 1.1 新增)

*/

var map = new BMap.Map("bd-map");
var map_local_icon_url = undefined;
var user_pic_marker = undefined;
var curr_map_marker = undefined;

var map_obj = {
	init:function(center_point, auto_complete, size) {
		if (size == undefined) size = 12;
		
		map_obj.init_events();
		if (auto_complete)	map_obj.init_auto_complete();
		
		if (center_point == undefined || center_point == null) {
			function myFun(result) {
				var cityName = result.name;
				map.centerAndZoom(cityName, size);            // 初始化地图,设置城市和地图级别
			} 
			var myCity = new BMap.LocalCity();
			myCity.get(myFun);  
			
		} else {
			map.centerAndZoom(center_point, size);            // 初始化地图,设置城市和地图级别
			map_obj.update_place(center_point);
		}
	},
	
	init_control:function() {
		var navigationControl = new BMap.NavigationControl({
		    anchor: BMAP_ANCHOR_TOP_LEFT,			// 靠左上角位置
		    type: BMAP_NAVIGATION_CONTROL_LARGE,	// LARGE类型
		    enableGeolocation: true					// 启用显示定位
		});
		map.addControl(navigationControl);
	},
	
	init_marker_label:function(point, label) {
		var marker = map_obj.get_marker(point)  				// 创建标注
		map.addOverlay(marker);              					// 将标注添加到地图中

		var label = new BMap.Label(label, {offset:new BMap.Size(20, -10)});
		var label = new BMap.Label("我是id=",{offset:new BMap.Size(20,-10)});
		marker.setLabel(label);
	},
	
	init_events:function()　{
		map.enableScrollWheelZoom();   					//启用滚轮放大缩小，默认禁用
		map.enableContinuousZoom();    					//启用地图惯性拖拽，默认禁用
	},
	
	init_local_input:function() {
		$('#update-map-view').unbind('click').bind('click', function() {
			if (!check_data($('#lng').val(), 'double')) {
				$('#lng').val(0);
			}
			if (!check_data($('#lat').val(), 'double')) {
				$('#lat').val(0);
			}
			var point = new BMap.Point($('#lng').val(), $('#lat').val());
			map_obj.update_place(point, 18);
			
		});
	},
	
	init_auto_complete:function(callback) {
		var ac = new BMap.Autocomplete(   				//建立一个自动完成的对象
			{"input" : "suggest_id", "location" : map}
		);
		
		ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
			var str = "";
			var _value = e.fromitem.value;
			var value = "";
			if (e.fromitem.index > -1) {
				value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			}    
			str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;
			
			value = "";
			if (e.toitem.index > -1) {
				_value = e.toitem.value;
				value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			}    
			str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
			$("#search_result_panel").html(str);
		});
		
		var myValue;
		ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
			var _value = e.item.value;
			myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			$("#search_result_panel").html("onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue);
			
			map_obj.set_place(myValue, function(pp) {
				if (callback != undefined) {
					callback(myValue, pp);
				}
			});
		});
	},
	
	init_center:function(point, size) {
		size = size || 12;
		map.centerAndZoom(point, size);
	},
	
	clear_overlays:function() {
		map.clearOverlays();
	},
	
	set_place:function(myValue, callback) {
		map.removeOverlay(curr_map_marker);    				//清除当前定位地点
		function myFun(){
			var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
			map_obj.update_place(pp);
			
			if (callback != undefined) {
				callback(pp);
			}
		}
		var local = new BMap.LocalSearch(map, { 			//智能搜索
			onSearchComplete: myFun
		});
		local.search(myValue);
	},
	
	init_mouse_click:function() {
		map.addEventListener("click",function(e){			//鼠标点击定位
			var point = new BMap.Point(e.point.lng, e.point.lat);
			map_obj.update_place(point);
		});
	},
	
	search:function(name) {
		map.clearOverlays();
		
		var local = new BMap.LocalSearch(map, {
			renderOptions:{map: map},
			onSearchComplete: function(results) {
				map_obj.update_center(undefined, update_map_center_callback);	//update_map_center_callback该方法定义在具体应用页面的js中
				
				if (local.getStatus() == BMAP_STATUS_SUCCESS){
					setTimeout(function() {
						var allOverlay = map.getOverlays();
						for (var i = 0; i < allOverlay.length; i++){
							allOverlay[i].addEventListener("click", function() {
								map_obj.update_center(this.point, update_map_center_callback);
							});
						}
					}, 1000);
				}
				
			}
		});
		local.search(name);
		
		
	},
	
	get_local:function() {
		var geolocation = new BMap.Geolocation();
		geolocation.getCurrentPosition(function(r){
			if(this.getStatus() == BMAP_STATUS_SUCCESS){
				map_obj.update_place(r.point);
			} else {
				frame_obj.tips_toggle('定位失败');
			} 
		});
	},
	
	update_place:function(point, zoom) {
		if (zoom != undefined) map.setZoom(Number(zoom));
		map.removeOverlay(curr_map_marker);    					//清除当前定位地点
		var marker = map_obj.get_marker(point)  				// 创建标注
		map.addOverlay(marker);
		map.panTo(point);
	},

	make_polyline:function(point_list, marker) {
		var polyline = new BMap.Polyline(point_list, {strokeColor:"#01b0df", strokeWeight:2, strokeOpacity:0.5});   	//创建折线
      	map.addOverlay(polyline);																					//增加折线
      	
      	if (marker) {
	      	for (var i in point_list) {
	      		var point = new BMap.Point(point_list[i].lng, point_list[i].lat);
	      		var opts = {
	      				//title : '所在位置' , 			 					信息窗口标题
	      				enableMessage:true,									//	设置允许信息窗发送短息
	      				//message:point_list[i].desc
	      		}
	      		var content = '<h4><img style="width:30px; height: 30px;" src="' + point_list[i].pic + '"> ' + point_list[i].desc + '</h4><br>' + point_list[i].mark + '<br>' + point_list[i].time;
	      		var marker = map_obj.get_marker(point)  				// 创建标注
	      		map.addOverlay(marker);              					// 将标注添加到地图中
	      		addClickHandler(content, opts, marker, point_list[i].handler, point);
	      	}

	      	function addClickHandler(content, opts, marker, handler, point){
	      		marker.addEventListener("click", function(){
	      			openInfo(content, opts, point)
	      		});
	      		$(handler)[0].addEventListener("click", function(){
	      			openInfo(content, opts, point)
	      		});
	      	}
	      	
	      	function openInfo(content, opts, point){
	      		var infoWindow = new BMap.InfoWindow(content, opts);  	// 创建信息窗口对象 
	      		map.openInfoWindow(infoWindow, point); 					// 开启信息窗口
	      	}
      	}

	},
	
	init_update_center:function(callback) {
		map.addEventListener("dragend", function showInfo(){
			map_obj.update_center(undefined, callback);
		});
	},
	
	update_center:function(point, callback) {
		cp = point || map.getCenter();
		map_obj.update_place(cp);
		if (callback != undefined) {
			callback(cp);
		}
	},
	
	loaded:function(callback) {
		map.addEventListener("tilesloaded", callback);
	},
	
	get_point_name:function(point, callback) {
		if (point == undefined) {
			point = map.getCenter();
		}
		var geoc = new BMap.Geocoder();
		geoc.getLocation(point, function(rs){
			var addComp = rs.addressComponents;
			var value = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
			if (callback != undefined) {
				callback(value, point);
			} else {
				return value;
			}
		});  
	},
	
	get_gps_point_name:function(gpsPoint, callback) {
		BMap.Convertor.translate(gpsPoint, 0, function(point) {
			map_obj.get_point_name(point, callback);
		});
	},
	
	get_marker:function(point) {
		var marker = undefined;
		if (map_local_icon_url != undefined) {
			var myIcon = new BMap.Icon(map_local_icon_url, new BMap.Size(149, 152));
			marker = new BMap.Marker(point, {icon:myIcon});   // 创建标注
			
		} else {
			marker = new BMap.Marker(point);  				// 创建标注
		}

		curr_map_marker = marker;
		return marker;
	},
	
	set_marker:function(point, img_url, img_w, img_h, anima, label) {
		var marker = undefined;
		if (img_url != undefined) {
			img_w = img_w || 25;
			img_h = img_h || 25;
			var myIcon = new BMap.Icon(img_url, new BMap.Size(img_w, img_h));
			marker = new BMap.Marker(point, {icon:myIcon});
      		map.addOverlay(marker);
			
		} else {
			marker = new BMap.Marker(point);
      		map.addOverlay(marker);
		}
		if (anima) {
			marker.setAnimation(BMAP_ANIMATION_BOUNCE);
		}
		if (label) {
			var mylabel = new BMap.Label(label, {offset:new BMap.Size(img_w, 0)});
			marker.setLabel(mylabel);
		}
		curr_map_marker = marker;
	},
	
	set_user_pic:function(point, pic_url) {
		if (user_pic_marker != undefined) {
			map.removeOverlay(user_pic_marker);              				
		}
		if (point == undefined) {
			point = map.getCenter();
		}
		
		point.lng = point.lng - 0.0003;
		point.lat = point.lat + 0.0006;
		var myIcon = new BMap.Icon(pic_url, new BMap.Size(64, 64));
		user_pic_marker = new BMap.Marker(point, {icon:myIcon});   // 创建标注
		map.addOverlay(user_pic_marker);              			   // 将标注添加到地图中
	},
};