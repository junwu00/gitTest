var c_search_cache = {};			//员工列表缓存
var c_search_cache_user = {};		//员工详情缓存
var c_search_cache_search = {};		//员工搜索缓存
var c_search_data = {};				//当前选中的内容
var c_search_count = 0;				//当前选中的个数
var c_search_num = 0;				//最多可选个数 0：表示不限	
var c_search_offset = 0;			//当前查找位置
var c_search_limit = 50;			//每次查找数量
var c_search_form;
var c_search_curr_opt = {};			//当前选择器的配置
var c_search_dept_icon = 'static/image/icon-dept.png';

$(document).ready(function(){
	var win_height = $(window).height();
	$('#c-search-container').css({'height': win_height-40});	//40为底部按钮高度
	
	c_search_init_toggle();
	c_search_init_tab_toggle();
	c_search_init_py_position();
	c_search_init_selected_toggle();
	$('#c-search-title').bind('focus', function() {
		$('#c-search-selected').addClass('hide');
	});
});

//显示/隐藏选人控件
function c_search_init_toggle(opt) {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

//点击确定的触发事件、初始化数据
function c_search_init(opt) {
	c_search_curr_opt = opt;
	//预处理事件
	if (typeof(opt.before) === 'function') {
		opt.before();
	}
	if(opt.select_num != undefined){
		c_search_num = opt.select_num;
	}
	//初始化数据
	$('.icheck-cb').each(function() {
		$(this)[0].checked = false;
	});
	$('.icheck-dp').each(function() {
		$(this)[0].checked = false;
	});
	c_search_data = opt.selected || {};
	c_search_count = 0;
	var users = [];	//已选中的员工
	var depts = [];	//已选中的部门
	for (var i in c_search_data) {
		if (c_search_data[i]) {
			if (i.length > 2 && i.substr(0, 2) == 'd-') {
				depts.push(i);
				
			} else {
				users.push(i);
			}
		}
	}
	
	//清空已选	
	$('#c-search-selected').html('');
	$('#c-search-selected-count').html(0);
	//初始化选中员工
	if (users.length > 0) {
		frame_obj.post({
			url		: c_search_init_url,
			data	: {user: users},
			success	: function(resp) {
				if (resp.errcode == 0) {
					var info = resp.info;
					for (var i in info) {
						if (!info[i])	continue;
						
						c_search_data[info[i].id].pic = info[i].pic_url;
						var target = $('.icheck-cb[data-id=' + info[i].id + ']');
						if (target.length > 0) {
							target[0].checked = true;
						}
						c_search_insert_selected(info[i].id, info[i].name, info[i].pic_url);
					}
					
				} else {
					frame_obj.alert(resp.errmsg);
				}
			}
		});
	}
	//初始化选中部门
	if (depts.length > 0) {
		var dept_ids = [];
		for (var i in depts) {
			dept_ids.push(depts[i].substr(2));
		}
		
		frame_obj.post({
			url		: c_search_init_url,
			ajax_key: c_search_init_url + '#' + new Date().getTime(),
			data	: JSON.stringify({user: [], dept: dept_ids}),
			success	: function(resp) {
				if (resp.errcode == 0) {
					var info = resp.info;
					for (var i in info) {
						if (!info[i])	continue;

						var target = $('.icheck-dp[data-id=d-' + info[i].id + ']');
						if (target.length > 0) {
							target[0].checked = true;
						}
						c_search_insert_selected(info[i].id, info[i].name, c_search_dept_icon, 1);
					}
					
				} else {
					frame_obj.alert(resp.errmsg);
				}
			}
		});
	}
	
	var type = opt.type ? Number(opt.type) : 0;
	c_search_curr_opt.type = type;
	var win_height = $(window).height();
	switch (type) {
		case 0:		//部门+成员
			$('.icheck-dp').removeClass('hide');
			$('.c-search-tree .c-search-list').removeClass('hide');
			$('#c-search-head').removeClass('hide');
			$('#c-search-body').css({'margin-top': 50});
			$('#c-search-container').css({'height': win_height-40});	//40为底部按钮高度
			break;
		case 1:		//仅部门
			$('.icheck-dp').removeClass('hide');
			$('#c-search-head').addClass('hide');
			$('.c-search-tree .c-search-list').addClass('hide');
			$('#c-search-body').css({'margin-top': 0});
			
			$('#c-search-dept').removeClass('hide');
			$('#c-search-py').addClass('hide');
			$('#c-search-user').css({left: '100%'});
			$('#c-search-title').attr('data-dept', '0');
			$('#c-search-user').html('');
			$('#c-search-back').addClass('hide');
			$('#c-search-container').css({'height': win_height});		
			break;
		case 2:		//仅成员
			$('.icheck-dp').addClass('hide');
			$('.c-search-tree .c-search-list').removeClass('hide');
			$('#c-search-head').removeClass('hide');
			$('#c-search-body').css({'margin-top': 50});
			$('#c-search-container').css({'height': win_height-40});	//40为底部按钮高度
			break;
	}
	
	//确认事件
	$('#c-search-ok').unbind('click').bind('click', function() {
		$('#c-search-container').css({'display': 'none'});
		if (typeof(opt.ok) === 'function') {
			var ret = c_search_data;
			var split = opt.split || false;
			if (split) {
				var users = {};	//已选中的员工
				var depts = {};	//已选中的部门
				for (var i in ret) {
					if (ret[i]) {
						if (i.length > 2 && i.substr(0, 2) == 'd-') {
							depts[i.substr(2)] = {id: i.substr(2), name: ret[i].name, pic: ret[i].pic};
							
						} else {
							users[i] = {id: i, name: ret[i].name, pic: ret[i].pic};
						}
					}
				}
				ret = {depts: depts, users: users};
			}
			
			opt.ok(ret);
		}
	});
}

//隐藏/显示已选成员
function c_search_init_selected_toggle() {
	$('#c-search-show-selected').unbind('click').bind('click', function() {
		if ($('#c-search-selected').hasClass('hide')) {
			if (c_search_count == 0) {
				frame_obj.tips('当前没有已选择的成员');
				
			} else {
				$('#c-search-selected').removeClass('hide');
			}
			
		} else {
			$('#c-search-selected').addClass('hide');
		}
	});
}

//初始化部门选择事件
function c_search_on_dept_check(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var dept = $(that).attr('data-id');
	var dept_name = $(that).attr('data-name');
	var checked = $(that)[0].checked;
	if (checked) {	//选择部门
		c_search_data[dept] = {id: dept, name: dept_name, pic: c_search_dept_icon};
		c_search_insert_selected(dept, dept_name, c_search_dept_icon, 1);
		
	} else {		//取消选择部门
		$('#c-search-selected img[data-id=' + dept + ']').parent().remove();
		delete(c_search_data[dept]);
		
		$('#c-search-selected-count').html(--c_search_count);
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
	}
}

//加载子部门
function c_search_append_child_dept(d_id, target_li) {
	window.c_search_loading_dept = window.c_search_loading_dept || [];
	if ($.inArray(d_id, window.c_search_loading_dept) !== -1) {	//正在获取中，不再获取
		return;
	}
	window.c_search_loading_dept.push(d_id);
	
	var ul = '<ul class="c-search-tree"><li style="height:48px; line-height: 48px; border-bottom: 1px solid #e7e7e7; color: #9b9b9b; padding-left: 15px;">加载中...</li></ul>';
	$(target_li).append(ul);
	$(target_li).children('ul').slideDown('normal', function() {
		$(target_li).addClass('c-search-down');
		$(target_li).removeClass('c-search-up');
	});
	
	var post_opt = {
		url			: c_search_dept_url,
		data		: {d_id: d_id},
		success		: function(resp) {
			if (resp.errcode == 0) {
				var data = resp.info;
				var html = '';
				var ids = [];
				for (var i in data) {
					var c = data[i];
					ids.push('d-' + c.id);							//记录部门ID，加载后可能需要直接勾选（初始数据中有该部门）
					var cls = c.has_child ? 'c-search-up' : '';
					html += '<li class="' + cls + '" data-id="' + c.id + '">' +
			                '<span onclick="c_search_on_dept_click(this, event);">' +
			                	'<span>' +
			                		'<input type="checkbox" class="icheck-dp ck2" data-id="d-' + c.id + '" data-name="' + c.name + '" onclick="c_search_on_dept_check(this, event);"> ' + c.name +
			                	'</span>' +
			                '</span>' +
			                '<i class="c-search-list" data-id="' + c.id + '" data-name="' + c.name + '"  onclick="c_search_on_dept_ricon_click(this, event);"> ' +
			                	'<span class="icon-angle-right"></span> ' +
			                '</i>' +
						'</li>';
				}
				$(target_li).children('ul').html(html);
				
				for (var i in ids) {
					if (c_search_data[ids[i]]) {
						$('.icheck-dp[data-id=' + ids[i] + ']')[0].checked = true;
					}
				}

				switch (c_search_curr_opt.type) {
					case 0: //部门+成员
						$('.icheck-dp').removeClass('hide');
						break;
					case 1: //仅部门
						$('.icheck-dp').removeClass('hide');
						break;
					case 2: //仅成员
						$('.icheck-dp').addClass('hide');
						break;
				}
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		}
	}
	frame_obj.post(post_opt);
}

//部门点击事件（收缩/展开/叶子部门进入部门成员查看）
function c_search_on_dept_click(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var target_li = $(that).parent();
	if ($(target_li).hasClass('c-search-up')) {				//收缩  => 展开 
		if ($(target_li).children('ul').length == 0) {	//未加载子部门，进行加载
			c_search_append_child_dept($(target_li).attr('data-id'), $(target_li));
			
		} else {
			$(target_li).children('ul').slideDown('normal', function() {
				$(target_li).addClass('c-search-down');
				$(target_li).removeClass('c-search-up');
			});
		}
		
	} else if ($(target_li).hasClass('c-search-down')) {	//展开 => 收缩  
		$(target_li).children('ul').slideUp('normal', function() {
			$(target_li).addClass('c-search-up');
			$(target_li).removeClass('c-search-down');
		});
		
	} else {												//叶子节点
		if (c_search_curr_opt.type && Number(c_search_curr_opt.type) == 1)	return;	//仅部门，不可查看成员
		$(target_li).find('.c-search-list').click();
	}
}

//点击部门列表右侧icon进入成员列表
function c_search_on_dept_ricon_click(that, evt) {
	if (evt) {
		evt.stopPropagation();	
	}
	
	if (c_search_curr_opt.type && Number(c_search_curr_opt.type) == 1)	return;	//仅部门，不可查看成员
	
	var dept = $(that).attr('data-id');
	var dept_name = $(that).attr('data-name');
	
	c_search_dept_select_evt($(that), dept, dept_name, 'dept');
	$('#c-search-back').removeClass('hide');
	if (dept.substr(0, 2) == 'd-') {
		dept = dept.substr(2);
	}
	history.pushState({from: 'dept', target: 'user', handler: dept}, "通讯录_成员信息");
}

//初始化部门、员工、员工详情切换
function c_search_init_tab_toggle() {
	$('#c-search-back').unbind('click').bind('click', function() {
		$('#c-search-head').removeClass('hide');
		$('#c-search-selected').addClass('hide');
		if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				$('#c-search-user-detail').html('');
				$('#c-search-back').removeClass('hide');
			});
			
		} else if ($('#c-search-user').css('left') == '0px') {	//当前在员工选择页面，则返回部门列表页面
			$('#c-search-dept').removeClass('hide');
			$('#c-search-py').addClass('hide');
			$('#c-search-user').animate({left: '100%'}, 'normal', function() {
				$('#c-search-title').attr('data-dept', '0');
				$('#c-search-user').html('');
				$('#c-search-back').addClass('hide');
			});

		} else {												//当前在部门列表页面，则返回上一层
//			$('#c-search-py').addClass('hide');
//			$(c_search_form).css({display: 'block'});
//			$('#c-search-container').css({display: 'none'});
		}
	});
}

//选择部门（显示员工列表）事件
function c_search_dept_select_evt(btn, dept, dept_name, from) {		//from 来源：部门列表的选择 / 员工详情页面的选择
	$('#c-search-selected').addClass('hide');
	$('#c-search-title').attr('data-dept', dept);
	$('#c-search-title').attr('data-dname', dept_name);
	
	function show_cache() {
		var data = [];
		for (var i in c_search_cache[dept]) {
			if (!c_search_cache[dept][i])	continue;
			if (i == 'offset') {
				c_search_offset = c_search_cache[dept][i];	//历史查找位置
				continue;
			}
			if (i == 'complete') {
				$('#c-search-py').removeClass('hide');
				continue;
			}
			
			data = data.concat(c_search_cache[dept][i]);
		}
		$('#c-search-user').html('');
		c_search_build_user_list(data);
	}

	$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	if (c_search_cache[dept]) {								//有本地缓存，取缓存数据
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'normal', function() {
				$('#c-search-dept').addClass('hide');
				show_cache();
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				show_cache();
			});
		}
		
	} else {
		c_search_offset = 0;	//重置查找位置
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'normal', function() {
				$('#c-search-dept').addClass('hide');
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
		}
	}
	//滚动加载
	c_search_scroll_load(function() {
		c_search_list_user(undefined, dept, c_search_offset, c_search_limit);
	});
}

//查找员工
function c_search_on_search(that, evt) {
	var empty_reg = /^[\S]+$/;
	var name = $('#c-search-title').val();
	if (!empty_reg.test(name)) {							//当前内容为空
		if ($('#c-search-title').attr('data-pre') == '') {	//上次内容也为空，不处理
			return;
		}													//上次内容不为空，处理
	}

	$('#c-search-title').attr('data-pre', $.trim(name));
	$('#c-search-user').html('');
	if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
		$('#c-search-user').removeClass('hide');
		$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
			$('#c-search-user-detail').html('');
		});
		
	} else if ($('#c-search-user').css('left') != '0px') {	//当前在部门列表页面，则跳转员工列表页面
		$('#c-search-user').animate({left: '0'}, 'normal');
		$('#c-search-back').removeClass('hide');
	}
	
	var dept = $('#c-search-title').attr('data-dept');
	if (c_search_cache_search[dept] && c_search_cache_search[dept][name]) {	//有缓存
		var data = [];
		var source = c_search_cache_search[dept][name];
		for (var i in source) {
			if (!source[i])	continue;
			if (i == 'offset') {
				c_search_offset = source[i];	//历史查找位置
				continue;
			}
			if (i == 'complete') {
				$('#c-search-py').removeClass('hide');
				continue;
			}
			
			data = data.concat(source[i]);
		}
		c_search_build_user_list(data);
		
	} else {
		c_search_offset = 0;	//重置查找位置
		$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
		c_search_list_user($(this), dept, c_search_offset, c_search_limit, name);
	}

	var dept_name = $('.c-search-list[data-id=' + dept + ']').attr('data-name');
	//滚动加载
	c_search_scroll_load(function() {
		c_search_list_user(undefined, dept, c_search_offset, c_search_limit, name);
	});
}

//获取员工数据
function c_search_list_user(btn, dept_id, offset, limit, name) {
	var empty_reg = /^[\S]+$/;
	var data = new Object();
	data.dept = String(dept_id);
	data.offset = offset || c_search_offset;
	data.limit = limit || c_search_limit;
	data.name = (empty_reg.test(name)) ? $.trim(name) : '';
	
	frame_obj.post({
		url		: c_search_url,
		data	: data,
		btn		: btn,
		success	: function(resp) {
			if (resp.errcode == 0) {
				var info = resp.info;
				if (info.count == 0) {
					if (data.name == '') {
						$('#c-search-user').html('<p class="c-search-empty"><img src="' + c_search_list_empty_icon + '"><br>当前部门暂无成员</p>');
					} else {
						$('#c-search-user').html('<p class="c-search-empty"><img src="' + c_search_list_empty_icon + '"><br>找不到匹配的成员</p>');
					}

					if ($('#c-search-user #c-search-user-dept').length == 0) {
						$('#c-search-user').prepend('<div id="c-search-user-dept"></div>');
					}
					$('#c-search-user-dept').html($('#c-search-title').attr('data-dname'));
					return;
				}
				
				var info = info.data;
				c_search_scroll_load_complete(info.length);
				c_search_offset += info.length;
				
				//缓存数据
				if (info.length != 0 && data.name == '') {						//部门所有员工
					c_search_cache[dept_id] = c_search_cache[dept_id] || {};
					c_search_cache[dept_id][offset] = info;
					c_search_cache[dept_id]['offset'] = c_search_offset;
					c_search_build_user_list(info);
					
				} else if (info.length != 0 && data.name != '') {				//查找员工
					c_search_cache_search[dept_id] = c_search_cache_search[dept_id] || {};
					c_search_cache_search[dept_id][data.name] = c_search_cache_search[dept_id][data.name] || {};
					c_search_cache_search[dept_id][data.name][offset] = info;
					c_search_cache_search[dept_id][data.name]['offset'] = c_search_offset;
					c_search_build_user_list(info);
				}

				//标识为完成加载
				if (offset == 0 && resp.info.count <= resp.info.data.length) {	//只一页，显示右侧拼音搜索
					$('#c-search-py').removeClass('hide');
					if (data.name == '') {
						c_search_cache[dept_id]['complete'] = 1;
						
					} else if (data.name != '') {
						c_search_cache_search[dept_id][data.name]['complete'] = 1;
					}

				} else {
					$('#c-search-py').addClass('hide');
				}

				if (resp.info.data.length == 0 && data.name == '') {
					c_search_cache[dept_id]['complete'] = 1;
					$('#c-search-py').removeClass('hide');
					
				} else if (resp.info.data.length == 0 && data.name != '') {
					c_search_cache_search[dept_id][data.name]['complete'] = 1;
					$('#c-search-py').removeClass('hide');
				}
				
			} else {
				frame_obj.tips('加载员工数据失败');
			}
		}
	});
}

//生成用户选择列表
function c_search_build_user_list(info) {
	var html = '';
	var py = '';
	var py_list = 'abcdefghijklmnopqrstuvwxyz';
	var py_list_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	for (var i in info) {
		if (!info[i])			continue;
		if (i == 'offset')		continue;
		if (i == 'complete')	continue;
		
		var item = info[i];
		var f_py = item.first_py.substr(0, 1);
		var py_idx = py_list.indexOf(f_py);
		if (item.boss) {
			f_py = '第一负责人';
		} else if (item.second_boss) {
			f_py = '第二负责人';
		} else if (py_idx == -1) {
			f_py = '#';
		} else {
			f_py = py_list_up.substr(py_idx, 1);
		}
		if ($('#c-search-user .c-search-py-tab[data-py=' + f_py + ']').length == 0) {//未加载过该拼音开头的数据
			$('#c-search-user').append(html);
			$('#c-search-user').append('<p class="c-search-py-tab" data-py=' + f_py + '>' + f_py + '</p>');
			html = '';
		}
		html += '<p class="c-search-py-item" data-id="' + item.id + '" onclick="c_search_on_user_check(this, event);">' + 
			'<input type="checkbox" class="icheck-cb ck2" data-id="' + item.id + '" data-name="' + item.name + '" onclick="c_search_on_user_check(this, event);">' +
			'<img class="c-search-user-pic" data-id="' + item.id + '" src="' + item.pic_url + '" onerror="this.src=\'static/image/face.png\'" onclick="c_search_show_detail(this, event);">' +
			'<span class="c-search-user-name" data-id="' + item.id + '" onclick="c_search_show_detail(this, event);">' + item.name + '</span>' +
		'</p>';
	}
	$('#c-search-user').append(html);
	//显示当前进入的部门
	if ($('#c-search-user #c-search-user-dept').length == 0) {
		$('#c-search-user').prepend('<div id="c-search-user-dept"></div>');
	}
	$('#c-search-user-dept').html($('#c-search-title').attr('data-dname'));
	
	c_search_init_selected();
}

//初始化员工详情页面展示
function c_search_show_detail(that, evt) {
	evt.stopPropagation();

	$('#c-search-head').addClass('hide');
	$('#c-search-selected').addClass('hide');
	var that = $(that);
	var user = $(that).attr('data-id');
	$('#c-search-user-detail').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	$('#c-search-user-detail').animate({left: '0'}, 'normal', function() {
		$('#c-search-user').addClass('hide');
		c_search_load_user_detail(that, user);
	});
}

//加载员工详情信息
function c_search_load_user_detail(btn, user) {
	var use_detail_cache = true;
	if (use_detail_cache && c_search_cache_user[user]) {
		c_search_build_user_detail(c_search_cache_user[user]);
		
	} else {
		frame_obj.post({
			url		: c_search_detail_url,
			data	: {user: user},
			success	: function(resp) {
				if (resp.errcode == 0) {
					c_search_cache_user[user] = resp.info;
					c_search_build_user_detail(resp.info);
					
				} else {
					frame_obj.tips('加载员工详情信息失败');
				}
			}
		});
	}
}

//生成员工详情页面
function c_search_build_user_detail(data) {
	var depts = '<span>';
	for (var i in data.dept_list) {
		if (!data.dept_list[i])		continue;
		
		var dept = data.dept_list[i];
		var dept_name = dept.name;
		dept = '<span class="c-search-detail-dept" data-id="' + dept.id + '" data-name="' + dept_name + '">' + dept_name + '</span>';
		depts += dept;
	}
	depts += '</span>';
	
	var html = '<div>' +
		'<p id="c-search-detail-title">' +
			'<img src="' + data.pic_url + '" onerror="this.src=\'static/image/face.png\'">' +
			'<span>' +
				'<span id="c-search-detail-name">' + data.name + '</span>' +
				'<span id="c-search-detail-acct">企业号账号 : ' + data.acct + '</span>' +
			'</span>' +
		'</p>' +
		'<p id="c-search-detail-position">' +
			'<label>职位</label>' + data.position + '' +
		'</p>' +
		'<p id="c-search-detail-depts">' +
			'<label>部门</label>' + depts +
		'</p>' +
	'</div>';
	$('#c-search-user-detail').html(html);
//	c_search_detail_user_list();
}

/*由员工详情页面的部门列表点击部门，进入部门员工列表页面
function c_search_detail_user_list() {
	$('.c-search-detail-dept').each(function() {
		$(this).unbind('click').bind('click', function() {
			c_search_dept_select_evt($(this), $(this).attr('data-id'), $(this).attr('data-name'), 'detail');
		});
	});
}
*/

//初始化选中的员工
function c_search_init_selected() {
	for (var i in c_search_data) {
		if (!c_search_data[i])								continue;
		if ($('.icheck-cb[data-id=' + i + ']').length == 0)	continue;
		
		$('.icheck-cb[data-id=' + i + ']')[0].checked = true;
	}
}

//更新人员选择事件
function c_search_on_user_check(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var target = ''; 
	if ($(that).hasClass('c-search-py-item')) {	//非点了checkbox，变更选中状态
		target = $(that).children('.icheck-cb');
		var checked = $(target)[0].checked ? true : false;
		$(target)[0].checked = !checked;
		
	} else {									//点了checkbox，不变更选中状态
		target = $(that);
	}
	var checked = $(target)[0].checked;
	
	if (checked) {		//选中
		var user = $(target).attr('data-id');
		var pic = $(target).parents('.c-search-py-item').children('img').attr('src');
		var name = $(target).attr('data-name');
		c_search_data[$(target).attr('data-id')] = {id: user, name: name, pic: pic};
		c_search_insert_selected(user, name, pic);
		
	} else {			//非选中
		var user = $(target).attr('data-id');
		$('#c-search-selected img[data-id=' + user + ']').parent().remove();
		delete(c_search_data[user]);
		
		$('#c-search-selected-count').html(--c_search_count);
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
	}
}

//选择列表中插入新选择成员/部门
function c_search_insert_selected(id, name, pic, is_dept) {
	is_dept = typeof(is_dept) === undefined ? 0 : is_dept;
	if (is_dept ==1 && id.substr(0, 2) != 'd-')		id = 'd-' + id;
	
	if (c_search_num != 0 && c_search_count >= c_search_num) {
		if (is_dept == 1 && $('.icheck-dp[data-id=' + id + ']').length > 0) {
			$('.icheck-dp[data-id=' + id + ']')[0].checked = false;
			
		} else if ($('.icheck-cb[data-id=' + id + ']').length > 0) {
			$('.icheck-cb[data-id=' + id + ']')[0].checked = false;
		}
		delete(c_search_data[id]);
		frame_obj.tips('最多可选择' + c_search_num + '个成员/部门');
		return;
	}
	
	var item = '<span class="c-search-selected-item">' + 
		'<img src="' + pic + '" data-id="' + id + '" onclick="c_search_delete_evt(this, ' + is_dept + ')" onerror="this.src=\'static/image/face.png\'">' + 
		'<span class="c-search-selected-name">' + name + '</span>' + 
	'</span>';
	$('#c-search-selected').append(item);
	$('#c-search-selected-count').html(++c_search_count);
}

//更新删除部门/员工事件
function c_search_delete_evt(that, is_dept) {
	var id = $(that).attr('data-id');
	$('#c-search-selected img[data-id=' + id + ']').parent().remove();
	delete(c_search_data[id]);
	$('#c-search-selected-count').html(--c_search_count);
	if (c_search_count == 0) {
		$('#c-search-selected').addClass('hide');
	}
	
	if (is_dept == 1) {												//删除部门
		var target = $('.icheck-dp[data-id=' + id + ']');
		if (target.length > 0) {			//选中部门在当前列表中
			target[0].checked = false;		
		}
		
	} else {														//删除成员
		var target = $('.icheck-cb[data-id=' + id + ']');
		if (target.length > 0) {			//选中成员在当前列表中
			target[0].checked = false;
		}
	}
}

//滚动加载
var c_search_curr_scroll_top = 0;
var c_search_is_scroll_end = false;
var c_search_is_scroll_loading = false;
function c_search_scroll_load(callback) {
	var $container = $('#c-search-user');
	var sTimer;
	
	$container[0].scrollTop = c_search_curr_scroll_top;
	$container.unbind('scroll').scroll(function scrollHandler(){
	    clearTimeout(sTimer);
	    sTimer = setTimeout(function() {
	    	var c = $container[0].scrollHeight;		//总高度
	    	var t = $container.scrollTop();			//滚动高度
	    	var h = $container[0].clientHeight;
	    	c_search_curr_scroll_top = t;

	        if(window.loaded == 1){$(window).unbind("scroll", scrollHandler);}
	        if(t + h + 100 > c && !c_search_is_scroll_end && !c_search_is_scroll_loading){
	        	if ($('#c-search-user').children('#c_search_loading').length == 0) {
	        		$('#c-search-user').append('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	        	}
	        	if (typeof(callback) === 'function') {
	        		callback();
	        	};
	        }
	    }, 100);
	});
}

//加载完成回调事件
function c_search_scroll_load_complete(load_length) {
	c_search_is_scroll_loading = false;
	if (load_length == 0) {
		c_search_is_scroll_end = true;
		
	} else {
		c_search_is_scroll_end = false;
	}
	$('#c-search-user').children('#c_search_loading').remove();
}

//初始化拼音定位
function c_search_init_py_position() {
	$('#c-search-py > span').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.py-big').addClass('hide');
			$(this).children('.py-big').removeClass('hide');
		});
	});
	$('.py-big > span').each(function() {
		$(this).unbind('click').bind('click', function(evt) {
			$(this).parent().addClass('hide');
			var py = $(this).html();
			
			var target = $('.c-search-py-tab[data-py=' + py + ']');
			if (target.length == 0) {
				frame_obj.tips('无字母' + py + '开头的成员');
				
			} else {
				$('#c-search-user').animate({scrollTop: target[0].offsetTop});
			}
			evt.stopPropagation();
		});
	});
}