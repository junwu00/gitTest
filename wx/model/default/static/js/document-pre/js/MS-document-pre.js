var MS_document_pre = {
	//MS_url:'http://view.officeapps.live.com/op/view.aspx?src=',
	max_size:{doc:(10*1024*1024),docx:(10*1024*1024),docm:(10*1024*1024),dotm:(10*1024*1024),dotx:(10*1024*1024),
				pptx:(10*1024*1024),ppsx:(10*1024*1024),ppt:(10*1024*1024),pps:(10*1024*1024),pptm:(10*1024*1024),potm:(10*1024*1024),ppam:(10*1024*1024),potx:(10*1024*1024),ppsm:(10*1024*1024),
				xlsx:(10*1024*1024),xlsb:(10*1024*1024),xls:(10*1024*1024),xlsm:(10*1024*1024),pdf:(10*1024*1024),txt:(10*1024*1024)},
	init_pre:function(ext,hash,file_name){
		if(MS_document_pre.is_pre(ext)){
			$('#ajax-url').val($('#system_http').val()+ 'index.php?m=root_ajax&cmd=103&hash=' + hash + '&ext=' + ext+'&free=1');
			frame_obj.do_ajax_post(undefined, '', {}, function(response){
				if(response['errcode']!=0){
					frame_obj.tips(response['errmsg']);
					return '';
				}
				if(response.data.preview_url.length == 0){
					frame_obj.tips('获取预览url失败');
					return '';
				}
				var preview_url = response.data.preview_url;
				location.href = preview_url;
			});
		}else{
			frame_obj.tips('暂不支持该文件预览，请下载后查看');
		}
	},
	is_pre:function(ext){
		if(MS_document_pre.max_size.hasOwnProperty(ext.toLowerCase())){
			return true;
		}else{
			return false;
		}
	}
};