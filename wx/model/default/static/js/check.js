function check_data(str, st){
	if (st == undefined || st == '' || st == null)	st = 'notnull';
	
	var reg = '';
    switch(st){
        case 'sname'    :{ reg = /^[\u4e00-\u9fa5a-zA-Z]{1}[\u4e00-\u9fa5_a-zA-Z0-9]*$/; break;}
    	case 'notnull'  :{ reg = /^[\s\t\n\r]*[\S]+[\s\t\n\r\S]*$/; break;}
    	case 'pwd'  	:{ reg = /^[\w\d_]{6,16}$/; break;}
    	case 'number'  	:{ reg = /^[\d]+$/; break;}
        case 'email'	:{ reg = /^(\w+[-+.]*\w+)*@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/; break; }
        case 'http'     :{ reg = /^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/; break; }
        case 'qq'       :{ reg = /^[1-9]\d{4,11}$/; break; }
        case 'english'  :{ reg = /^[A-Za-z]+$/; break; }
        case 'mobile'   :{ reg = /^1[34578]\d{9}$/; break; }
        case 'mobile_s' :{ reg = /^\d{11}$/; break; }
        case 'phone'    :{ reg = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/; break; }
        case 'en_num'   :{ reg = /^[A-Za-z0-9]+$/; break; }
        case 'ip'       :{ reg = /^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/; break; }
        case 'mac'      :{ reg = /^[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}$/; break; };
        case 'float'	:{ reg = /^(([1-9]\d*)|\d)(\.\d{1,2})?$/;	break;}
        case 'money'	:{ reg = /^(([1-9]{1}[0-9]{0,11}(\.[0-9]{1,2})?)|(0{1}\.[0-9][1-9])|(0{1}\.[1-9]{1,2}))$/; break;}
        case 'double'	:{ reg = /^(([1-9]\d*)|\d)(\.\d+)?$/; break; }
    }
    if(reg == '') {
   		return false;
   	}else{
   		var result = reg.test(str);
   		if(st == 'phone' && !result){//增加400,800电话匹配校验
   			reg = /^[48]00\d+$/;
   			result = reg.test(str);
   		}
   		return result;
   	}
}