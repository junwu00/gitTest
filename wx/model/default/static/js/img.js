/**
 * 图像处理类
 */
var img_obj = {
	select:function(file_element, img_element, max_width, quality) {
		if (max_width == undefined) max_width = 40;
		if (quality == undefined) quality = 100;
		$(file_element).unbind('change').bind('change', function(evt) {
			var files = evt.target.files;
			var file = files[0];
			if (!file.type.match('image.*')) {
				return;
			}
 
			var reader = new FileReader();
			reader.onload = (function(theFile) {
				return function(e) {
					$(img_element).attr('src', e.target.result);
					$(img_element).css('width', max_width + 'px');
					var src = img_obj.compress($(img_element)[0], quality).src;
					$(img_element).attr('src', src);
				};
			})(file);
 
			reader.readAsDataURL(file);
		});
	},
	
	compress: function(source_img_obj, quality){
        var mime_type = "image/jpeg";
        
        var cvs = document.createElement('canvas');
        //naturalWidth真实图片的宽度
        cvs.width = source_img_obj.naturalWidth;
        cvs.height = source_img_obj.naturalHeight;
        var ctx = cvs.getContext("2d").drawImage(source_img_obj, 0, 0);
        var newImageData = cvs.toDataURL(mime_type, quality/100);
        var result_image_obj = new Image();
        result_image_obj.src = newImageData;
        return result_image_obj;
   },
		
	//压缩图像
	render:function(element, src, max_width) {
		var MAX_WIDTH = 100;			// 参数，最大宽度
		if (max_width != undefined) {	
			MAX_WIDTH = Number(max_width);
		}
		
		var image = new Image();									// 创建一个 Image 对象  
		image.onload = function() {									// 绑定 load 事件处理器，加载完成后执行 
			var canvas = $(element)[0];								// 获取 canvas DOM 对象 
			if(image.height > MAX_HEIGHT) {							// 如果高度超标  
				image.width *= MAX_HEIGHT / image.height;			// 宽度等比例缩放 *= 
				image.height = MAX_HEIGHT; 
			} 
	 
			var ctx = canvas.getContext("2d");						// 获取 canvas的 2d 环境对象, 可以理解Context是管理员，canvas是房子 
			ctx.clearRect(0, 0, canvas.width, canvas.height); 		//	canvas清屏
			canvas.width = image.width; 							// 重置canvas宽高 
			canvas.height = image.height; 
			ctx.drawImage(image, 0, 0, image.width, image.height);	// 将图像绘制到canvas上 
			// !!! 注意，image 没有加入到 dom之中 
		}; 
		// 设置src属性，浏览器会自动加载。 
		// 记住必须先绑定事件，才能设置src属性，否则会出同步问题。 
		image.src = src; 
	},
	
	// 加载 图像文件(url路径) 
	load:function(element, src) {
		if(!src.type.match(/image.*/)){ 					// 过滤掉 非 image 类型的文件 
			frame_obj.alert("只能选择图片文件");
			return; 
		} 
		var reader = new FileReader();						// 创建 FileReader 对象 并调用 render 函数来完成渲染. 
		reader.onload = function(e){						// 绑定load事件自动回调函数  
			img_obj.render(element, e.target.result);		// 调用前面的 render 函数  
		}; 
		reader.readAsDataURL(src); 							// 读取文件内容 
	},
	
	// 上传图片
	upload:function(element, callback) {
		var canvas = $(element)[0]; 			// 获取 canvas DOM 对象 
		// 获取Base64编码后的图像数据，格式是字符串 
		// "data:image/png;base64,"开头,需要在客户端或者服务器端将其去掉，后面的部分可以直接写入文件。 
		var dataurl = canvas.toDataURL("image/png"); 
		// 为安全 对URI进行编码 
		// data%3Aimage%2Fpng%3Bbase64%2C 开头 
		var imagedata = encodeURIComponent(dataurl); 
		console.log(dataurl); 
		console.log(imagedata); 
		var data = { 
			imagename: "myImage.png", 
			imagedata: imagedata 
		}; 
		
		//frame_obj.do_ajax_post(undefined, 'upload', JSON.stringify(data), callback);
	}
};