/** 校验文件、上传文件 */

(function($){
	/**
	 * 常用配置参数：
	 * element			要监听的控件（input[type=file]）
	 * options			插件配置
	 * options.uploadUrl	处理上传的URL
	 * options.checkUrl		验证上传的URL
	 * options.form			对应的form表单的ID，如：#form
	 * options.type			支持文件类型配置，如：['jpg', 'jpeg', 'gif', 'png', 'bmp']
	 * options.maxSize		各文件类型分类支持的大小（单位：B），如：{'jpg,jpeg,gif,png,bmp': 2048}
	 * options.defMaxSize	默认文件大小限制（单位：B）
	 * options.quality		压缩质量，默认为 80%
	 * 
	 * function
	 * options.scaning		扫描文件开始、进行
	 * options.scaned		扫描完成
	 * options.preview		预览图片
	 * options.startUpload	开始上传
	 * options.process		上传进度（上传过程监听）
	 * options.success		上传成功
	 * options.error		上传失败
	 */
	var MUpload = function(element, options) {
		var that = this;
		this.element = $(element);
		this.opt = options;
		//对应的form表单
		this.form = options.form || '#form';
		//图片预览控件
		this.previewImg = options.previewImg || 'body';
		this.previewImgW = options.previewImgW || 60;		//预览图宽度
		this.previewImgH = options.previewImgH || 60;		//预览图高度
		//文件预览控件
		this.previewFile = options.previewFile || 'body';
		//是否支持html5，默认为false
		this.html5 = false;
		//图片格式类型（用于判断是否可进行图片压缩）
		this.typeImg = ['jpg', 'jpeg', 'gif', 'png', 'bmp'];
		//文件支持类型
		this.type = options.type || [/*图片*/'jpg', 'jpeg', 'png',/*音频*/'mp3', 'wma', 'wav', 'amr', /*视频*/'mp4', /*文件*/'txt', 'xml', 'pdf', 'zip', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx'];
		this.typeStr = this.type.join(', ');		//数组转字符串
		//默认文件大小： 10MB
		this.defMaxSize = options.defMaxSize || 10*1024*1024;
		//自定义文件大小限制：默认图片5MB，音频2MB，视频10MB，文件20MB
		this.maxSize = options.maxSize || {'jpg,jpeg,gif,png,bmp': 5*1024*1024, 'mp3,wma,wav,ram': 2*1024*1024, 'rm,rmvb,wmv,avi,mpg,mpeg,mp4': 10*1024*1024, 'txt,xml,pdf,zip,doc,ppt,xls,docx,pptx,xlsx': 20*1024*1024};
		//分解文件大小限制，方便进行验证
		var tmpmaxsize = {};
		for (var i in this.maxSize) {
			var fileType = i.split(',');
			for (var j in fileType) {
				tmpmaxsize[fileType[j]] = this.maxSize[i];
			}
		}
		this.maxSize = tmpmaxsize;
		delete(tmpmaxsize);
		//选中文件的信息
		this.fileConf = {};
		//是否压缩图片：默认为true
//		this.zipImg = typeof(options.zipImg) === 'undefined' ? true : options.zipImg;
		//当前图片预览下标
		this.previewImgIdx = 1;
		//处理上传文件的URL
		this.uploadUrl = options.uploadUrl || '';
		//验证上传文件的URL
		this.checkUrl = options.checkUrl || '';
		//多媒体资源url
		this.mediaUrl = options.mediaUrl || '';
		//表单异步请求实例
		this.xhr;
		//压缩质量：默认0.8
		this.quality = options.quality || 0.8;
		//当前是否有文件正在上传(一次只能上传一个): 默认为false
		this.isUploading = false;
		
		this.partSize = options.partSize || 1024*1024;
		
		//表单分隔串
		this.boundary = "sdfdafwefqrqewr";
		//表单类型
		this.contentType = "multipart/form-data, boundary=" + this.boundary;
		
		//图片预览控件ID/class
		this.swiperEl = options.swiperEl || '';
		//图片预览控件实例
		this.swiperIns;
		//显示图片预览的面板
		this.previewPanel;
		
		//是否分段上传
		this.partUpload = false;

		//插件初始化
		this.init();
	};
	
	MUpload.prototype = {
		constructor: MUpload,
		
		init:function() {
			var that = this;
			
			//上传控件
			this.html5 = this._checkHtml5();
//			this.partUpload = this.html5;

			$(this.element).unbind('click').bind('click', function() {
				if (that.isUploading) {
					that._showTips('最多同时上传一个文件');
					return false;
				}
			});
			
			if (this.partUpload) {	//支持html5
				$(this.element).unbind('change').bind('change', function() {
					try {
						if (that.isUploading) {
							that._showTips('最多同时上传一个文件');
							
						} else {
							that.isUploading = true;
							that.filePath = $(that.element).val();
							
							if (!that._initFileConf())	return;
							if (!that._checkFile())		return;
								
							if (that.fileConf.isImg == 1) {			//图片，先压缩再取md5
								that._loadImg(that.fileConf.file);					//压缩图片并上传
								
							} else {								//其他文件，直接取md5
								that._html5Upload(that, that.fileConf.file);
							}
							
						}
					} catch (e) {
						that.isUploading = false;
					}
				});
				
			} else {
				$(this.element).unbind('change').bind('change', function() {
					try {
						if (that.isUploading) {
							that._showTips('最多同时上传一个文件');
							
						} else {
							that.isUploading = true;
							that.filePath = $(that.element).val();
							
							if (!that._initFileConf())	return;
							
							if (typeof(that.opt.scaning) === 'function') {
				    			doContinue = that.opt.scaning(that.filePath, that.fileName, that.fileConf.isImg);	//判断是否继续执行
								
							} else {
								that._showTips('正在扫描文件', {withImg: true, hideTime: 99999});
							}
	
							if (!doContinue) {
								that.isUploading = false;
								return false;
							}
	
							if (typeof(that.opt.scaned) === 'function') {
								that.opt.scaned();
							} else {
								that._hideTips();
							}
	
							if (!that._checkFile())		return;
							
							that._simpleUpload();
						}
					} catch (e) {
						that.isUploading = false;
					}
				});
			}
			//预览控件
			if (typeof(Swiper) !== 'undefined') {
				this._initPreviewPanel();
				this._initSwiper();
			}
		},
		
		//验证是否支持html5
		_checkHtml5:function() {
			try {
				if(typeof(Worker)!== "undefined") {
					//TODO
		            return true;
//		            return false;
		        }
		    } catch(e) {}
		    return false;
		},
		
		//文件校验--------------------------------------------------------
		//初始化文件信息
		_initFileConf:function() {
			var obj = $(this.element)[0];
			var key = obj.value;
			if (!key) {
				this.isUploading = false;
				this._showTips('请先选择文件');
				return false;
			}

			if (this.html5) {
				//记录文件
				this.fileConf.file = obj.files[0];
			}
			//记录key
			this.fileConf.key = SparkMD5.hash(key);
			//记录文件名
			var fileNameIdx = this.filePath.lastIndexOf('/');
			if (fileNameIdx == -1)	fileNameIdx = this.filePath.lastIndexOf('\\');
			var fileName = this.filePath.substr(fileNameIdx+1);
			this.fileName = fileName;
			//记录文件扩展名
			var extIdx = fileName.lastIndexOf('.');	
			var ext = fileName.substr(extIdx+1);
			this.fileConf.ext = ext;
			//判断是否图片
			this.fileConf.isImg = 0;
			if ($.inArray(ext, this.typeImg) != -1) {
				this.fileConf.isImg = 1;
			}
			return true;
		},
		
		//初始化文件 md5
		_initMd5:function(key, file, callback) {
			var that = this;
			var doContinue = true;
			if (typeof(this.opt.scaning) === 'function') {
    			doContinue = this.opt.scaning(this.filePath, this.fileName, this.fileConf.isImg);	//判断是否继续执行
				
			} else {
				this._showTips('正在扫描文件', {withImg: true, hideTime: 99999});
			}
			if (!doContinue) {
				this.isUploading = false;
				return false;
			}
			
			var md5 = localStorage.getItem(key);	//文件MD5
			!md5 && this._getMd5(file, key);

			this._getLocalMd5(key, file, function(md5) {
				if (typeof(that.opt.scaned) === 'function') {
					that.opt.scaned();
				} else {
					that._hideTips();
				}
				if (typeof(callback) === 'function') {
					callback({file: file, md5: md5, key: key, ext: that.fileConf.ext});
				}
			});
		},

		_getLocalMd5:function(key, file, callback) {
			var md5 = localStorage.getItem(key);
			if (md5) {
				if (typeof(callback) === 'function') {
					callback(md5);
				}
				return true;
			}
			var that = this;
			setTimeout(function(){
				that._getLocalMd5(key, file, callback);
			}, 300);
		},
		
		_getMd5:function(file, key) {
			var reader = new FileReader();
			var slice = File.prototype.webkitSlice || File.prototype.mozSlice || File.prototype.slice;

			var partSize = this.partSize || 1024 * 1024;		//每次校验内容大小
			var fileSize = file.size;
			var offset = 0;

			var spark = new SparkMD5();

			var that = this;
			reader.onload = function(e) {
				offset += parseInt(e.target.result.length);
				spark.appendBinary(e.target.result);

				if (offset < fileSize) {
					that._loadPart(file, reader, slice, offset, partSize);
				}else {
					localStorage.setItem(key, spark.end());
				}
			}

			that._loadPart(file, reader, slice, offset, partSize);
		},
		
		//分段读取文件内容
		_loadPart:function(file, reader, slice, offset, partSize) {
			var start = offset;
			var tmp = parseInt(start) + parseInt(partSize);
			var end = tmp >= file.size ? file.size : tmp;
			reader.readAsBinaryString(slice.call(file, start, end));
		},
		
		//验证文件格式、大小等
		_checkFile:function() {
			if ($.inArray(this.fileConf.ext.toLowerCase(), this.type) == -1) {
				this.isUploading = false;
				this._uploadFail({errmsg: '仅支持上传' + this.typeStr + '类型的文件'});
				return false;
			}
			if (this.html5) {
				//验证大小
				var size = this.fileConf.file.size;
				var limit = this.maxSize[this.fileConf.ext] || this.defMaxSize;
				if (size > limit) {
					this._uploadFail({errmsg: this.fileConf.ext + '类型文件最大支持 ' + parseInt(limit/1024/1000) + 'MB'});
					return false;
				}
			} 
			return true;
		},
		
		//验证远程是否有该文件存在
		_checkRemoteFile:function(callback) {
			that = this;
			try {
				$.ajax({
					url 	: that.checkUrl,
					type 	: 'post',
					data 	: {"hash": that.fileConf.md5, 'ext': that.fileConf.ext},	
					dataType : 'json',
					success : function(resp, status, e){
						var exists = false;
						if (resp.errcode == 0) {	//当前存在该文件
							exists = true;
							var data = resp.data;
//							console.log('已存在该文件');
							that._uploadComplete(data);
							
						} else {					//当前不存在该文件
//							console.log('不存在该文件');
						}
						if (typeof(callback) === 'function') {
							callback(exists, resp.data);
						}
					},
					error : function(data, status, e){
						that._showTips('系统繁忙，请稍候再试');
					}
				});
			} catch (e) {
				that._showTips('系统繁忙，请稍候再试');
			}
		},
		
		//html5上传及预览
		_html5Upload:function(that, file) {
			that._initMd5(that.fileConf.key, file, function(resp) {			//获取文件MD5后，执行文件校验、上传
				that.fileConf = $.extend(that.fileConf, resp);
				that._checkRemoteFile(function(exists) {
					if (!exists) {					//不存在，执行上传
						that._startUpload(file);
					}
				});
			});
		},

		//上传完成
		_uploadComplete:function(data) {
			data.file_name = this.fileName;
			data.file_ext = this.fileConf.ext;
			
			localStorage.setItem(this.fileConf.md5, 0);     	//重置起始位置为0
    		if ($.inArray(this.fileConf.ext, this.typeImg) != -1 && typeof(this.opt.preview) === 'function') {	//是图片，显示预览图
    			this.opt.preview(this.fileName, data.path);
    		}
    		
        	if (typeof(this.opt.success) === 'function') {		//自定义处理
				this.opt.success(data, this.fileConf.isImg);
				
        	} else {											//默认处理
    			this._showTips('上传完成');
        	}
        	this.isUploading = false;
		},
		
		//上传失败
		_uploadFail:function(opt) {
			localStorage.setItem(this.fileConf.md5, 0);     	//重置起始位置为0
        	if (typeof(this.opt.error) === 'function') {		//自定义处理
        		opt.errmsg = opt.errmsg || '上传失败';
        		this.opt.error(opt);
        		
        	} else {											//默认处理
    			if (opt.errmsg) {
    				this._showTips(opt.errmsg);
    			}
        	}
        	this.isUploading = false;
		},
		
		//加载选择的图片
		_loadImg:function(src) {
			var that = this;
			var reader = new FileReader();
			reader.onload = function(e){
				that._renderImg(e.target.result);
			};
			reader.readAsDataURL(src);
		},

		//读取并压缩选择的图片
		_renderImg:function(src) {
			var image = new Image();
			var that = this;
			image.onload = function(){
				var canvasId = that.previewImgIdx;
				$(that.previewImg).append('<canvas id="preview-img-' + canvasId + '" style="display: none;"></canvas>');
				var canvas = $('#preview-img-' + (canvasId))[0];
				that.previewImgIdx++;
				
				//上传图
				var upload_ctx = canvas.getContext("2d");
				upload_ctx.clearRect(0, 0, canvas.width, canvas.height);
				canvas.width = image.width;
				canvas.height = image.height;
				upload_ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
				upload_dataurl = canvas.toDataURL(that.fileConf.file.type, that.quality);
				
				//预览图
//				var preview_ctx = canvas.getContext("2d");
//				preview_ctx.clearRect(0, 0, canvas.width, canvas.height);
//				canvas.width = that.previewImgW;
//				canvas.height = that.previewImgH;
//				preview_ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
//				dataurl = canvas.toDataURL(that.fileConf.file.type);
				
				//转换成Blob对象用于传输
				var image_data = that.dataURLtoBlob(upload_dataurl);
				that._html5Upload(that, image_data);
			};
			image.src = src;
		},
		
		dataURLtoBlob:function(dataurl) {
		    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
		        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
		    while(n--){
		        u8arr[n] = bstr.charCodeAt(n);
		    }
		    return new Blob([u8arr], {type:mime});
		},
		
		//提示框----------------------------------------------------------------------------------
		
		//显示提示（自动消失）
		_showTips:function(msg, opt) {	//autoHide是否自动关闭
			opt = opt || {};
			var autoHide = opt.autoHide || true;
			var hideTime = opt.hideTime || msg.length * 200;	//从显示到消失的时长
			var withImg = opt.hideTime || false;
			
			if (hideTime != 99999 && hideTime > 3000)	hideTime = 3000;
			
			clearTimeout(this.tipsFun);
			frame_obj.lock_screen(msg, withImg);
			this.tipsFun = setTimeout(function() {
				frame_obj.unlock_screen();
			}, hideTime);
		},
		
		//关闭提示（自动消失）
		_hideTips:function() {
			frame_obj.unlock_screen();
		},
		
		//显示提示（点击才消息）
		_showWarn:function(msg) {
			this._hideTips();
//TODO		frame_obj.alert(msg);
			alert(msg);
		},
		
		
		//断点续传------------------------------------------------------------

		//开始上传
		_startUpload:function(file, isFile) {
			var that = this;

			var partSize = this.partSize || 1024 * 1024;
			var tmp = localStorage.getItem(that.fileConf.md5);
			var offset = (tmp && tmp != 'undefined') ? tmp : 0;
			
			if(that.xhr) 	that.xhr.abort();		//终止上一个请求
			that.xhr = new XMLHttpRequest();
			if (typeof(that.opt.startUpload) === 'function') {
				that.opt.startUpload();
				
			} else {
				that._showTips('正在上传 0%');
			}
			
			var reader = new FileReader();
			if (file instanceof File) {
				var slice = File.prototype.webkitSlice || File.prototype.mozSlice || File.prototype.slice;
				
			} else {
				var slice = file.slice;
			}
			reader.onload = function(e) {
				var tmp = localStorage.getItem(that.fileConf.md5);	//每次读取前，取缓存记录中该文件的上传位置
				var offset = (tmp && tmp != 'undefined') ? tmp : 0;
				var part = e.target.result;
				that._uploadPart(that.xhr, that.fileConf.md5, file, reader, slice, offset, partSize, part);
			}
			that._loadPart(file, reader, slice, offset, partSize);	//读取文件，读取完毕后回调上传方法: onload
		},
		
		//分段续传
		_uploadPart:function(xhr, md5, file, reader, slice, offset, partSize, part) {
			var end = 0;
			if (parseInt(part.length) + parseInt(offset) >= file.size) {
				end = 1;
			}
//			console.log('part-----------------------------------------------------');
//			console.log('offset0: ' + offset);
//			console.log('file.size: ' + file.size);
//			console.log('part.length: ' + part.length);
//			console.log('end: ' + end);

			var param = new Object();
			param.apikey = 'html5';
			param.part = part;
			param.hash = md5;
			param.offset = offset;
			param.ext = this.fileConf.ext;
			param.timelength = 1;
			param.bitrate = 1;
			param.quality = 1;
			param.is_end = end;
			param.utype = 3;		//标识为分段上传
			
//			var arr = new Array();
//			for (var p in param) {
//				if (param.hasOwnProperty(p)) {
//					arr.push(p);
//				}
//			}
//			arr = this._sortParam(arr, arr.length);
//			param.sign = this._getSign(part, arr, param);

			var data = this._getFormData(param, this.boundary);

			xhr.sendAsBinary(this, data, this.contentType);
			var that = this;
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						var reponse = xhr.responseText;
						try {
							var json = JSON.parse(reponse);
							var data = json;
							if (json.errcode == 0) {
								json = json.data;
								var offset = Number(json.offset || that.fileConf.file.size);
								localStorage.setItem(that.fileConf.md5, offset);	//更新上传位置
								
								var ratio = Math.round(parseInt(offset) / parseInt(that.fileConf.file.size) * 100);
								//显示进度
								if (typeof(that.opt.process) === 'function') {
									that.opt.process(ratio);
									
								} else {
									that._showTips('正在上传 ' + ratio + '%');
								}
								
								if (json.status == 1) {								//明确返回上传完成，终止上传
									var ret = data.data.data;
									if (ret) {
										setTimeout(function() {
											that._uploadComplete(data.data.data);		
										}, 500);
									} else {
										that._uploadFail({errmsg: '系统繁忙，请稍候再试'});
									}
									return;
								}
									
								if (json.offset > file.size) {						//返回的起始位置超出文件大小，终止上传
									that._uploadFail({errmsg: '上传异常，请稍候再试'});
									
								} else {											//继续上传下一部分（读取文件，再回调上传方法）
//									setTimeout(function() {
										that._loadPart(file, reader, slice, offset, partSize);
//									}, 3000);
								}
								
							} else {
								that._uploadFail({errmsg: json.errmsg});
							}
							
						} catch(e) {
//							console.log(e);
							that._uploadFail({errmsg: '服务异常，请稍候再试', error: e});
						}
					} else {
						that._uploadFail({errmsg: '网络异常，请稍候再试'});
					}
				}
			}
		},

		//重新排列参数
		_sortParam:function(arr, n) {
			if (n == 1) {
				return arr;
			}
			var tmp = null;
			for (var i = 0; i + 1 < n; i++) {
				if (arr[i] > arr[i+1]) {
					tmp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = tmp;
				}
			}
			arr = this._sortParam(arr, n - 1);
			return arr;
		},
		
		//获取签名串
		_getSign:function(part, arr, obj) {
			var str = '';
			var suffix = 'ew_mv_upload';
			for (var i in arr) {
				if (arr[i] == 'part') {
					str += 'part' + part;
					continue;
				}
				str += arr[i] + obj[arr[i]];
			}
			str += suffix;
			var spark = new SparkMD5();
			spark.appendBinary(str);
			var sign = spark.end();
			return sign;
		},
		
		//构建form表单数据
		_getFormData:function(obj, boundary) {
			var body = "--" + boundary + "\r\n";

			for (var p in obj) {
				if (obj.hasOwnProperty(p)) {
					body += "--" + boundary + "\r\n";
					body += "Content-Disposition: form-data; name=\"" + p + "\"\r\n\r\n";
					body += obj[p] + "\r\n";
				}
			}
			body += "--" + boundary + "--";

			var data = "Content-Type: multipart/form-data; boundary=" +  boundary + "\r\nContent-Length: " + body.length + "\r\n" + body + "\r\n";
			return data;
		},
		
		
		
		//普通上传-------------------------------------------
		
		_simpleUpload:function() {
			if (typeof(this.opt.startUpload) === 'function') {
				this.opt.startUpload();
				
			} else {
				this._showTips('正在上传', {withImg: true, hideTime: 99999});
			}
			
			var that = this;
			$(this.form).ajaxSubmit({
		        type		: 'post',
		        dataType	: 'json',
		        url			: that.uploadUrl,
		        success		: function(data) {
		        	try {
			        	if (data.errcode == 0) {
			        		that._uploadComplete(data.data);
			        		
			        	} else {
			        		that._uploadFail(data);
			        	}
		        	} catch (e) {
		        		that._uploadFail({errmsg: '上传失败'});
		        	}
		        },
		        error		: function(XmlHttpRequest, textStatus, errorThrown) {
		        	that._uploadFail({request: XmlHttpRequest, status: textStatus, error: errorThrown});
		        }
		    });
		},

		//图片预览

		//初始化预览图片面板
		_initPreviewPanel:function() {
			var panel = $('<div class="mupload-preview swiper-container">' +
					'<div>' +
						'<span class="glyphicon glyphicon-arrow-left pull-left"></span>' +
						'<span class="preivew-current"></span> /' +
						'<span class="preview-total"></span>' +
						'<span class="pull-right file-name"></span>' +
					'</div>' +
					'<div class="mupload-preview-body swiper-wrapper">' +
					'</div>' +
				'</div>');
			$(panel).appendTo('body');
			this.previewPanel = $(panel);
			//适应高度
			var h = ($('html')[0].clientHeight) + 'px';
			$(panel).find('.mupload-preview-body').css({'margin-top': '44px', 'height': h, 'line-height': h});
			//关闭事件
			$(panel).find('.glyphicon-arrow-left').unbind('click').bind('click', function() {
				$(panel).css({'display': 'none'});
			});
		},
		
		//初始化预览插件实例
		_initSwiper:function() {
			var that = this;
			this.swiper = new Swiper(this.swiperEl, {
				onSlideChangeStart : function(t) {
					$(that.previewPanel).find('.preivew-current').html(t.activeIndex+1);
					var fileName = $(that.previewPanel).find('.swiper-slide:eq(' + t.activeIndex + ')').attr('data-file');
					$(that.previewPanel).find('.file-name').html(fileName);
				}
			});
		},
		
		//在最前面插入预览图
		prependPreview:function(newOne) {
			$(this.previewPanel).css({display: 'block'});
			this.swiper.prependSlide($(newOne));				//插入到前面
			this.swiper.update(true);
			$(this.previewPanel).css({display: 'none'});
		},
		
		//在最如果面插入预览图
		appendPreview:function(newOne) {
			$(this.previewPanel).css({display: 'block'});
			this.swiper.appendSlide($(newOne));				//插入到前面
			this.swiper.update(true);
			$(this.previewPanel).css({display: 'none'});
		},
		
		//删除预览图
		removePreview:function(idx) {
			this.swiper.removeSlide(idx);
			this.swiper.update(true);
		},
		
		//滚动到指定的预览图
		slideToPreview:function(idx, speed, fileName) {
			var len = $(this.previewPanel).find('.mupload-preview-body .swiper-slide').length;
			$(this.previewPanel).find('.preivew-current').html(idx+1);
			$(this.previewPanel).find('.preview-total').html(len);
			
			$(this.previewPanel).css({'display': 'block'});
			$(this.previewPanel).find('.file-name').html(fileName);
			speed = speed || 1000;
			this.swiper.slideTo(idx, speed, true);
		},
		
		//当前是否有正在上传的文件
		hasUploading:function() {
			return this.isUploading;
		}
		
	};
	
	$.fn.mupload = function(option) {
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function() {
			var $this = $(this);
			var data = $this.data('mupload');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('mupload', (data = new MUpload(this, $.extend({}, $.fn.mupload.def, options))));
			}
			if (typeof option == 'string' && typeof data[option] == 'function') {
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined) {
					return false;
				}
			}
		});
		if (internal_return !== undefined)
			return internal_return;
		else
			return this;
	};
	
	$.fn.mupload.def = {
		'debug': false,
	};
	
	$.fn.mupload.Constructor = MUpload;
	
})(jQuery);


XMLHttpRequest.prototype.sendAsBinary = function(that, param, contentType) {
	var datastr = param;
	var len = datastr.length;
	var data = new Uint8Array(len);
	for (var i = 0; i < len; i++) {
	    data[i] = datastr.charCodeAt(i);
	}
	var dataArr = new Array();
	dataArr.push(data.buffer);
	var obj = new Object();
	obj.type = contentType;
	var textBlob = new Blob(dataArr, obj);
	this.open("POST", that.uploadUrl);
	this.send(textBlob);
	
	/*
	var fd = new FormData();
	for (var k in param) {
		if (!param[k])	continue;
		
		if (k != 'part') {
			console.log(k);
			fd.append(k, param[k]);
		}
	}
	
	var datastr = param.part;
	var len = datastr.length;
	var data = new Uint8Array(len);
	for (var i = 0; i < len; i++) {
	    data[i] = datastr.charCodeAt(i);
	}
	
	var textBlob
	try {
		textBlob = new Blob([data], {type: contentType});
		
	} catch(e) {
		window.BlobBuilder = window.BlobBuilder ||
                window.WebKitBlobBuilder ||
                window.MozBlobBuilder ||
                window.MSBlobBuilder;

        if (e.name == 'TypeError' && window.BlobBuilder) {
            var bb = new BlobBuilder();
            bb.append(datastr);
            textBlob = bb.getBlob();
			alert('in1');
            console.debug("case 2");
        }
        else if (e.name == "InvalidStateError") {
            // InvalidStateError (tested on FF13 WinXP)
            textBlob = new Blob([data], {type: that.fileConf.file.type});
            console.debug("case 3");
        }
        else {
            // We're screwed, blob constructor unsupported entirely   
            console.debug("Errore");
        }
	}

	fd.append("part", textBlob, that.fileName);
	alert(textBlob.size);
	
	this.send(fd);
	*/
}
