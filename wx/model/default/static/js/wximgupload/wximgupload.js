/**
 * 调用微信jssdk上传图片
 * 
 * 需要引入
 * 1.bootstrap样式，字体
 * 2.jssdk.html
 * 3.jssdk.js, jweixin-1.1.0.js （在2中已包含）
 * 4.wximgupload.css样式
 * 5.wximgupload.js(本文件，一定要在jssdk.html文件后引入，因为要覆盖其部分变量)
 * 
 */

if (Object.prototype.toString.call(jsApiList) == '[object Array]') {	//已有配置，进行追加
	jsApiList.push('uploadImage');
	jsApiList.push('chooseImage');
	jsApiList.push('previewImage');
	
} else {							//只采用图片配置
	jsApiList = ['uploadImage','chooseImage','previewImage'];
}
(function($){
	var WxImgUpload = function(element, options) {
		var that = this;
		this.element = $(element);
		this.opt = options;
		this.img_list = new Array();
		this.rmable = false;
	};
	
	WxImgUpload.prototype = {
		constructor: WxImgUpload,
		init:function() {
			$(this.element).addClass('wx-img-upload');
			
			var base_html = '<div class="wx-img-list"></div>' +
							'<div class="wx-img-opera-bar">' +
								'<span class="wx-img-add-img hide"><i class="icon-camera"></i></span>' +
								'<span class="wx-img-rm-img hide"><i class="icon-trash"></i></span>' +
							'</div>';
			$(this.element).append(base_html);
			
			this.bind_add();
		},
		
		read:function() {
			var media_base = this.opt.media_url;
			var img_list = new Array();
			$(this.element).find('img').each(function() {
				img_list.push($(this).attr('src').substr(media_base.length));
			});
			this.img_list = img_list;
		},
		
		show_icon:function() {
			$(this.element).find('.wx-img-add-img').removeClass('hide');
			$(this.element).find('.wx-img-rm-img').removeClass('hide');
		},
		
		bind_add:function() {
			var that = this;
			var upload_base = this.opt.upload_url;
			$(that.element).find('.wx-img-add-img').unbind('click').bind('click', function() {
				if (that.img_list.length >= that.opt.max_count) {
					frame_obj.tips('最多上传' + that.opt.max_count + '张图片');
					return;
				}
				
				//验证上传权限
				var check_url = upload_base + '&check=1';
				frame_obj.simple_ajax_post(undefined, check_url, '', 'json', function(data) {
					if (data.errcode == 0) {
						start_jssdk_upload();
						
					} else {
						frame_obj.tips(data.errmsg);
					}
				});
				
				//执行上传
				var start_jssdk_upload = function() {
					var err_count = 0;
					jssdk_obj.full_upload_img(
						that.opt.max_count, 
						that.img_list.length,
						function(media_id) {						
							var upload_url = upload_base + '&media_id=' + media_id;
							frame_obj.simple_ajax_post(undefined, upload_url, '', 'json', function(data) {
								if (data.errcode == 0) {
									that.img_list.push(data.data.path);
									that.bind_preview();
									
								} else {
									err_count ++;
								}
							});
						},
						function(res) {
							frame_obj.tips('上传失败');
						},
						function() {
							if (err_count == 0) {
								frame_obj.tips('上传完成');
								
							} else {
								frame_obj.tips('上传完成,' + err_count + '张失败');
							}
						},
						that.opt
					);
				};
			});
		},

		bind_preview:function(with_hide) {
			if (with_hide == undefined)	with_hide = true;
			
			var media_base = this.opt.media_url;
			var new_img_list = new Array();
			
			$(this.element).find('.wx-img-list .wx-img-list-item').remove();

			for (var i in this.img_list) {
				var url = media_base + this.img_list[i];
				new_img_list.push(url);
				var img = '<span class="wx-img-list-item" idx="' + i + '">' +
								'<img src="' + url + '" class="wx-img-img-prev" idx="' + i + '">' +
								'<span class="wx-img-del-prev hide">' +
									'<i class="icon-minus-sign"></i>' +
								'</span>' +
							'</span>';
				$(this.element).find('.wx-img-list').append(img);
			}

			$(this.element).find('.wx-img-list img').each(function() {
				$(this).unbind('click').bind('click', function() {
					var idx = Number($(this).attr('idx'));
					jssdk_obj.preview_img(new_img_list[idx], new_img_list);
				});
			});
			
			this.bind_img_rm();
			if (with_hide) {
				this.hide_img_rm();
			} else {
				this.show_img_rm();
			}
		},

		bind_img_rm:function() {
			var that = this;
			$(this.element).find('.wx-img-rm-img').unbind('click').bind('click', function() {
				if (that.rmabled) {
					that.hide_img_rm();
				} else {
					that.show_img_rm();
				}
			});
			
			$(this.element).find('.wx-img-del-prev').each(function() {
				$(this).unbind('click').bind('click', function() {
					var idx = $(this).parent().attr('idx');
					$(this).parent().remove();
					that.img_list.splice(idx, 1);
					that.bind_preview(false);
				});
			});
		},

		show_img_rm:function() {
			this.rmabled = true;
			$(this.element).find('.wx-img-del-prev').each(function() {
				$(this).removeClass('hide');
			});
		},

		hide_img_rm:function() {
			this.rmabled = false;
			$(this.element).find('.wx-img-del-prev').each(function() {
				$(this).addClass('hide');
			});
		},
		
		set_val:function(img_list) {
			this.img_list = img_list;
		},
		
		get_val:function() {
			return this.img_list;
		}
	};
	
	$.fn.wximgupload = function(option) {
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function() {
			var $this = $(this);
			var data = $this.data('wximgupload');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('wximgupload', (data = new WxImgUpload(this, $.extend({}, $.fn.wximgupload.def, options))));
			}
			if (typeof option == 'string' && typeof data[option] == 'function') {
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined) {
					return false;
				}
			}
		});
		if (internal_return !== undefined)
			return internal_return;
		else
			return this;
	};
	
	$.fn.wximgupload.def = {
		'debug': false,
		'max_count': 1,		//没有不限制
		'media_url': '',
		'upload_url': '',
	};
	
	$.fn.wximgupload.Constructor = WxImgUpload;
	
})(jQuery);