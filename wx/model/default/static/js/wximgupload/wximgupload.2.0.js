/**
 * 调用微信jssdk上传图片
 * 
 * 需要引入
 * 1.bootstrap样式，字体
 * 2.jssdk.html
 * 3.jssdk.js, jweixin-1.1.0.js （在2中已包含）
 * 4.wximgupload.css样式
 * 5.wximgupload.js(本文件，一定要在jssdk.html文件后引入，因为要覆盖其部分变量)
 * 
 */

if (Object.prototype.toString.call(jsApiList) == '[object Array]') {	//已有配置，进行追加
	jsApiList.push('uploadImage');
	jsApiList.push('chooseImage');
	jsApiList.push('previewImage');
	
} else {							//只采用图片配置
	jsApiList = ['uploadImage','chooseImage','previewImage'];
}

/**
 * options
 * 属性
 * media_url		（必填）静态资源路径前缀
 * upload_url		（必填）图片上传地址
 * img_class		img标签对应的类，用于预览图片
 * max_count		总共最大上传数，默认为99
 * per_count		每次最大上传数，默认为1
 * img_list 		当前已上传的图片路径列表（支持url或相对路径，但最终只记录相对路径）
 * sizeType			原图/缩略图['original', 'compressed']，默认都有
 * sourceType		图片来源，相册/拍照，['album', 'camera']，默认都有
 * 
 * 事件(function)
 * ready				jssdk加载成功后调用
 * error				jssdk加载失败后调用
 * upload_error			上传异常（全部上传失败）
 * upload_per_success	每次上传成功
 * upload_per_fail		每次上传失败
 * upload_success		上传成功（全部）
 * upload_success_part	上传成功（部分失败）
 * remove				删除图片
 */
(function($){
	var WxImgUpload = function(element, options) {
		options.img_class = options.img_class || '';
		
		this.element = $(element);
		this.opt = options;
		this.img_list 		= options.img_list || [];					//当前已上传的图片路径列表（支持url或相对路径，但最终只记录相对路径）
		this.browse_btn 	= $(element);								//添加图片按钮
		this.max_count 		= options.max_count || 99;					//总共最大上传数
		this.per_count 		= options.per_count || 1;					//每次最大上传数（最大为9，jssdk限制）
		this.per_count		= this.per_count > 9 ? 9 : this.per_count;

		this._init();
	};
	
	WxImgUpload.prototype = {
		constructor: WxImgUpload,
		//初始化
		_init:function() {
			this._onerror();	//jssdk加载失败时调用
			this._onready();	//初始化jssdk加载成功后事件
			this._read();		//初始化配置的图片
		},
		
		//初始化配置的图片
		_read:function() {
			var media_base = this.opt.media_url;
			this.img_list_data = [];
			for (var i in this.img_list) {
				if (this.img_list[i].substr(0, 4) == 'http') {	//为相对路径，增加静态资源路径前缀
					this.img_list[i] = this.img_list[i].substr(media_base.length-1);
				}
				this.img_list_data.push({path: this.img_list[i], is_new: false});
			}
		},

		//jssdk加载成功后调用
		_onready:function() {
			var that = this;
			wx.ready(function(){
				that._onadd();		//初始化添加事件
				if (typeof(that.opt.ready) === 'function') {
					that.opt.ready(that);
				}
			});
		},
		
		//jssdk加载失败后调用
		_onerror:function() {
			var that = this;
			wx.error(function(res){
				if (typeof(that.opt.error) === 'function') {
					that.opt.error(that);
					
				} else {
					frame_obj.tips('图片上传控件加载失败，请刷新页面后重试');
				}
			});
		},
		
		//初始化添加事件
		_onadd:function() {
			var that = this;
			var upload_base = this.opt.upload_url;
			if ($(that.browse_btn).length == 0) {
				frame_obj.tips('上传按钮未配置');	return;
			}
			
			$(that.browse_btn).unbind('click').bind('click', function() {
				if (that.img_list.length >= that.max_count) {
					frame_obj.tips('总共最多上传' + that.max_count + '张图片');
					return;
				}
				
				//验证上传权限
				var check_url = upload_base + '&check=1';
				frame_obj.simple_ajax_post(undefined, check_url, '', 'json', function(data) {
					if (data.errcode == 0) {
						start_jssdk_upload();
						
					} else {
						frame_obj.tips(data.errmsg);
					}
				});

				//执行上传
				var start_jssdk_upload = function() {
					var err_count = 0;
					var new_img_data = [];		//新增加的图片的信息（回调时可使用）
					jssdk_obj.full_upload_img(
						that.max_count, 
						that.img_list.length,
						function(media_id, idx, total) {						
							var upload_url = upload_base + '&media_id=' + media_id;
							frame_obj.simple_ajax_post(undefined, upload_url, '', 'json', function(data) {
								if (data.errcode == 0) {
									if (!data.data.name) {
										var names = data.data.path.split('/');
										data.data.name = names[names.length-1] || '';
									}
									new_img_data.push(data.data);
									that.img_list_data.push($.extend(data.data, {is_new: true}));
									that.img_list.push(data.data.path);
	
									if (typeof(that.opt.per_success) === 'function') {
										that.opt.per_success(data.data, idx, total);
										
									} else {
										that.update_preview();
									}
									
								} else {
									if (typeof(that.opt.per_fail) === 'function') {
										that.opt.per_fail(data, idx, total);
									}
									err_count ++;
								}
							});
						},
						function(res) {
							if (typeof(that.opt.upload_error) === 'function') {
								that.opt.upload_error(res);
								
							} else {
								frame_obj.tips('上传失败');
							}
						},
						function() {
							if (err_count == 0) {
								if (typeof(that.opt.upload_success) === 'function') {
									that.opt.upload_success(new_img_data);
									
								} else {
									frame_obj.tips('上传成功');
								}
								
							} else {
								if (typeof(that.opt.upload_success_part) === 'function') {
									that.opt.upload_success_part(new_img_data, err_count);
									
								} else {
									frame_obj.tips('上传完成,' + err_count + '张失败');
								}
							}
						},
						{
							count		: that.per_count,
							sizeType	: that.opt.sizeType || ['original', 'compressed'],
							sourceType	: that.opt.sourceType || ['album', 'camera'],
							process		: 0,	//不使用微信自带的遮罩
						}
					);
				};
			});
		},

		//更新预览事件
		update_preview:function() {
			var img_class = this.opt.img_class;
			if ($(img_class).length == 0)	return;	//找不到对应图片
			
			var new_img_list = new Array();
			for (var i in this.img_list) {
				var url = this.opt.media_url + this.img_list[i];
				new_img_list.push(url);
			}

			var idx = 0;
			$(img_class).each(function() {
				$(this).attr('data-idx', idx++);
				$(this).unbind('click').bind('click', function() {
					var idx = Number($(this).attr('data-idx'));
					jssdk_obj.preview_img(new_img_list[idx], new_img_list);
				});
			});
		},

		//删除图片
		remove:function(img_idx) {
			var list = [];
			var list_data = [];
			for (var i in this.img_list_data) {
				if (i != img_idx) {
					list.push(img_list_data[i].path);
					list_data.push(img_list_data[i]);
				}
			}
			this.img_list = list;
			this.img_list_data = list_data;
			this.update_preview();
		},
		
		//获取所有图片路径
		get_val:function() {
			return this.img_list;
		},
		
		//获取所有图片路径，是否新上传等数据
		get_data:function() {
			return this.img_list_data;
		}
	};
	
	$.fn.wximgupload = function(option) {
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function() {
			var $this = $(this);
			var data = $this.data('wximgupload');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('wximgupload', (data = new WxImgUpload(this, $.extend({}, $.fn.wximgupload.def, options))));
			}
			if (typeof option == 'string' && typeof data[option] == 'function') {
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined) {
					return false;
				}
			}
		});
		if (internal_return !== undefined)
			return internal_return;
		else
			return this;
	};
	
	$.fn.wximgupload.def = {
		'debug': false,
		'media_url': '',
		'upload_url': '',
	};
	
	$.fn.wximgupload.Constructor = WxImgUpload;
	
})(jQuery);