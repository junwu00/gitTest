var image_pre = {
	/* 图片预览 */
	img_ext: { 'jpg': 5 * 1024 * 1024, 'jpeg': 5 * 1024 * 1024, 'png': 5 * 1024 * 1024 },
	init_pre: function (ext, hash) {		
		if (image_pre.is_pre(ext)) {
		$('#ajax-url').val($('#system_http').val() + 'index.php?m=root_ajax&cmd=103&hash=' + hash + '&ext=' + ext +'&free=1');		
			frame_obj.do_ajax_post(undefined, '', {}, function (response) {
				if (response['errcode'] != 0) {
					frame_obj.tips(response['errmsg']);
					return '';
				}
				if (response.data.preview_url.length == 0) {
					frame_obj.tips('获取预览url失败');
					return '';
				}
				var userAgent = window.navigator.userAgent;
				// console.log(userAgent, 'user');
				if (userAgent.indexOf("Mobile") >= 0 || userAgent.indexOf("mobile") >= 0) {
					jssdk_obj.preview_img(response.data.preview_url, [response.data.preview_url]);
				} else {
					location.href = response.data.preview_url;
				}

			});
		} else {
			frame_obj.tips('暂不支持该文件预览，请下载后查看');
		}
	},
	is_pre: function (ext) {
		if (image_pre.img_ext.hasOwnProperty(ext.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}
};