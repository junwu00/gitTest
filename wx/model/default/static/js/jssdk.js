var jsApiList = [];	//需要在具体的js文件中指明要调用的接口， 参见：http://qydev.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1JS%E6%8E%A5%E5%8F%A3
/*
版本1.0.0接口
onMenuShareTimeline
onMenuShareAppMessage
onMenuShareQQ
onMenuShareWeibo
startRecord
stopRecord
onVoiceRecordEnd
playVoice
pauseVoice
stopVoice
onVoicePlayEnd
uploadVoice
downloadVoice
chooseImage
previewImage
uploadImage
downloadImage
translateVoice
getNetworkType
openLocation
getLocation
hideOptionMenu
showOptionMenu
hideMenuItems
showMenuItems
hideAllNonBaseMenuItem
showAllNonBaseMenuItem
closeWindow
scanQRCode
*/

var jssdk_obj = {
	check_api:function() {
		wx.checkJsApi({
		    jsApiList: jsApiList, // 需要检测的JS接口列表，所有JS接口列表见附录2,
		    success: function(res) {
		    	
	    	}
	    });
	},
	
	//分享到朋友圈
	share_cycle:function(title, link, img, s_callback, c_callback, f_callback) {
		wx.onMenuShareTimeline({
		    title: 	title, 				// 分享标题
		    link: 	link, 				// 分享链接
		    imgUrl: img, 				// 分享图标
		    success: function () {		// 用户确认分享后执行的回调函数
		        if (s_callback != undefined) 	s_callback();
		    },
		    cancel: function () {		// 用户取消分享后执行的回调函数
		    	if (c_callback != undefined) 	c_callback();
		    },
		    fail:function() {
		    	if (f_callback != undefined) 	f_callback();
		    }
		});
	},
	
	//分享给朋友
	share_friend:function(title, desc, link, img, type, url, s_callback, c_callback, f_callback) {
		wx.onMenuShareAppMessage({
		    title: 		title, 		// 分享标题
		    desc: 		desc, 		// 分享描述
		    link: 		link, 		// 分享链接
		    imgUrl: 	img, 		// 分享图标
		    type: 		type, 		// 分享类型,music、video或link，不填默认为link
		    dataUrl: 	url, 		// 如果type是music或video，则要提供数据链接，默认为空
		    success: function () {	// 用户确认分享后执行的回调函数
		    	if (s_callback != undefined) 	s_callback();
		    },
		    cancel: function () {	// 用户取消分享后执行的回调函数
		    	if (c_callback != undefined) 	c_callback();
		    },
		    fail:function() {
		    	if (f_callback != undefined) 	f_callback();
		    }
		});
	},
	
	//拍照或从手机相册中选图接口
	choose_img:function(s_callback, f_callback, opt) {
		opt.count = opt.count || 9;
		opt.sizeType = opt.sizeType || ['original', 'compressed'];
		opt.sourceType = opt.sourceType || ['album', 'camera'];
		wx.chooseImage({
			count: 		opt.count,				// 默认9
		    sizeType: 	opt.sizeType, 			// 可以指定是原图还是压缩图，默认二者都有
		    sourceType: opt.sourceType, 		// 可以指定来源是相册还是相机，默认二者都有
		    success: function (res) {
		        var localIds = res.localIds; 	// 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
		        if (s_callback != undefined) {
					setTimeout(function() {
						s_callback(localIds);
					}, 100);
				}
		    },
		    fail:function(res) {
		    	if (f_callback != undefined) 	f_callback(res);
		    }
		});
	},
	
	//预览图片接口
	preview_img:function(curr_src, src_list) {
		wx.previewImage({
		    current: curr_src,	 	// 当前显示的图片链接,如：http://host/demo.jpg
		    urls: src_list 			// 需要预览的图片链接列表,如：[http://host/demo.jpg, http://host/demo2.jpg] 
		});
	},
	
	//上传图片接口
	upload_img:function(localId, s_callback, f_callback, with_progress) {
		if (with_progress == undefined)	with_progress = 1;
		
		wx.uploadImage({
		    localId: localId, 					// 需要上传的图片的本地ID，由chooseImage接口获得
		    isShowProgressTips: with_progress,	// 默认为1，显示进度提示
		    success: function (res) {
		    	var serverId = res.serverId; 	// 返回图片的服务器端ID
		    	if (s_callback != undefined) {
		    		s_callback(serverId);
		    	}
		    },
		    fail:function(res) {
		    	if (f_callback != undefined) 	f_callback(res);
		    }
		});
	},
	
	//选择图片、上传图片
	full_upload_img:function(max_count, has_count, s_callback, f_callback, finish_callback, opt) {
		jssdk_obj.choose_img(
			function(localIds) {
				localIds = String(localIds);
				localIds = localIds.split(',');
		        var count = localIds.length;
		        if (count > (max_count - has_count)) {		//上传张数 = 最大张数 - 已有张数
		        	frame_obj.tips('最多上传' + max_count + '张图片');
		        	return false;
		        }
		        
				if (typeof(opt.process) !== 'undefined' && opt.process != 1) {
					frame_obj.lock_screen('上传中 1/' + localIds.length);
				}
		        var idx = 0;
		        function upload() {
		        	jssdk_obj.upload_img(
	        			localIds[idx],
	        			function(img_id) {
		        			idx++;
		        			if (s_callback != undefined) {
		        				s_callback(img_id, idx, count);
		        			}
		        			if (idx < count) {
		        				upload();
		        			} else if (finish_callback != undefined) {
		        				finish_callback();
		        			} else {
		        				frame_obj.unlock_screen();
		        			}
		        		},
		        		function (res) {
		    		    	if (f_callback != undefined) 	f_callback(res);
		        		},
		        		typeof(opt.process) !== 'undefined' ? opt.process : 1	//使用微信自到的上传遮罩层
		        	);
		        }
		        upload();
			},
			function(res) {
				frame_obj.unlock_screen();
		    	frame_obj.tips('打开图片选择器失败');
			},
			opt
		);
	},
	
	get_location:function(s_callback, f_callback) {
		wx.getLocation({
			success: function (res) {
				if (s_callback != undefined) {
					s_callback(res);
				}
			},
			fail:function(res) {
				if (s_callback != undefined) {
					s_callback(res);
				} else {
			    	frame_obj.tips('定位失败,请重试');
				}
			}
		});
	}
};