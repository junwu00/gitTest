//高德地图处理类

//加载地图API
var u = navigator.userAgent;
if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
	document.write('<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad"></script>');
} else if (u.indexOf('iPhone') > -1) {//苹果手机
	document.write('<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad"></script>');
} else if (u.indexOf('Windows Phone') > -1) {//winphone手机
	document.write('<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad"></script>');
} else {
	document.write('<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key=dc5f9beb971f433cf9f1b2d7847ed4ad"></script>');
}

(function($){
	/**
	 * 常用配置参数：
	 * element			要监听的控件（div等）
	 * options			插件配置
	 */
	var GDMap = function(element, options) {
		var that = this;
		this.el = element;
		options = options || {};
		
		options.autoMarkCenter 	= options.autoMarkCenter || 1;				//默认定位中心点
		options.autoFixedCenter = options.autoFixedCenter || 1;				//默认缩放地图时保持中心点不变
		options.controller 		= options.controller || 1;				//默认带缩放等控件
		options.init			= typeof(options.init) === 'undefined' ? true : options.init;
		this.opt = options;

		if (options.init) {
			try {
				this.map = new AMap.Map(this.el);
				
			} catch (e) {
				frame_obj.comfirm('地图加载失败，请刷新页面后重试', function() {
					location.reload();
				});
				return;
			}
			this.init();
		}
	};

	GDMap.prototype = {
		constructor: GDMap,
		
		init:function() {
			opt = this.opt || {};
			var that = this;
			that.map.setZoom(opt.zoom || 10);
			
			if (opt.controller == 1) {
				 this.map.plugin(["AMap.ToolBar"], function () {
					 toolBar = new AMap.ToolBar();
					 that.map.addControl(toolBar);
				 });
			}
			
			AMap.event.addListener(this.map, 'complete', function() {
				if (opt.center) {
					that.setCenter(opt.center);
					that.afterInit();
					
				} else {
					that.getLocal(function(lng, lat) {
						that.setCenter({lng: lng, lat: lat});
						that.afterInit();
					});
					/*
					that.map.getCity(function(ret) {	//默认当前城市为中心位置
						that.setCenter(ret.city);
						that.afterInit();
					});
					*/
				}
			});
		},
		
		//获取当前位置
		getLocal:function(callback) {
			frame_obj.lock_screen('定位中...', true);
			this.map.plugin('AMap.Geolocation', function() {
		        geolocation = new AMap.Geolocation({
		            enableHighAccuracy: true,//是否使用高精度定位，默认:true
		            timeout: 10000,          //超过10秒后停止定位，默认：无穷大
		            buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
		            zoomToAccuracy: true,      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
		            buttonPosition:'RB'
		        });
		        geolocation.getCurrentPosition();
		        //返回定位信息
		        AMap.event.addListener(geolocation, 'complete', function(data) {
					frame_obj.unlock_screen();
		        	callback(data.position.getLng(), data.position.getLat());
		        });
		        AMap.event.addListener(geolocation, 'error', function() {	//返回定位出错信息
					frame_obj.unlock_screen();
		        	alert('获取您当前位置信息失败，请刷新页面后重试');
		        });
		    });
		},
		
		//初始化完毕后调用
		afterInit:function() {
			var that = this;
			var opt = this.opt;
			if (opt.autoMarkCenter == 1) {
				that.markerCenter();
				that.autoMarkCenter();
			}
			
			if (opt.autoFixedCenter == 1) {
				that.autoFixedCenter();
			}
			
			if (typeof(opt.afterMapLoaded) === 'function') {	//地图加载完毕后回调
				opt.afterMapLoaded();
			}
		},
		
		//增加标注点
		addMarker:function(opt) {
			opt = opt || {};
			opt.position = this.trans2Position(opt.position);
			opt.icon = opt.icon || this.opt.defMarkerIcon || "https://webapi.amap.com/theme/v1.3/markers/n/mark_bs.png";
			opt.iconSize = opt.iconSize || this.opt.defMarkerIconSize || [19,33];
			var that = this;
			var marker = new AMap.Marker({
				icon		: opt.icon,
	            position 	: opt.position,
	            offset 		: new AMap.Pixel(-opt.iconSize[0]/2, -opt.iconSize[1]/2),
	            map 		: that.map
			});
		},
		
		//删除地图上所有的覆盖物
		cleanMarker:function() {
			this.map.clearMap();
		},
		
		//清空标注点并增加一个
		cleanAndAddMarker:function(opt) {
			this.cleanMarker();
			this.addMarker(opt);
		},
		
		//标识中心位置
		markerCenter:function(position) {
			if (this.centerMarker) {
				this.centerMarker.setMap(null);
			}

			var that = this;
			position = position || this.getCenter();
			that.opt.centerIconSize = that.opt.centerIconSize || [19,33];
			this.centerMarker = new AMap.Marker({
				icon		: that.opt.centerIcon || "https://webapi.amap.com/theme/v1.3/markers/n/mark_bs.png",
	            position 	: position,
	            offset 		: new AMap.Pixel(-that.opt.centerIconSize[0]/2, -that.opt.centerIconSize[1]/2),
	            map 		: that.map
			});
		},
		
		//自动标识中心点（移点/缩放地图时也会）
		autoMarkCenter:function() {
			var that = this;
			AMap.event.addListener(this.map, 'moveend', function() {
				that.markerCenter();
				
				if (typeof(that.opt.moveend) === 'function') {
					that.opt.moveend(that.getCenter());
				}

	        	if (typeof(that.opt.afterSetCenter) === 'function') {
	        		that.opt.afterSetCenter(that.getCenter());
	        	}
			});
		},
		
		//地图缩放后，中心位置不变
		autoFixedCenter:function() {
			var that = this;
			AMap.event.addListener(this.map, 'zoomstart', function() {
				that.zoomstart_center = that.getCenter();
			});
			AMap.event.addListener(this.map, 'zoomend', function() {
				var center = that.zoomstart_center || that.getCenter();
				that.setCenter(center);
//				that.cleanAndAddMarker();
			});
		},
		
		//获取点心点位置
		getCenter:function() {
			return this.map.getCenter();
		},
		
		//设置中心点
		setCenter:function(position) {
			position = this.trans2Position(position);
			var that = this;
        	that.map.setCenter(position);
        	
        	if (typeof(that.opt.afterSetCenter) === 'function') {
        		that.opt.afterSetCenter(position);
        	}
		},
		
		//地图中心点平移至指定点位置
		panTo:function(position) {
			position = this.trans2Position(position);
			
			var that = this;
	        setTimeout(function() {
	        	that.map.panTo(position);
	        }, 10);
		},
		
		//转换成地图坐标对象
		trans2Position:function(obj) {
			obj = typeof(obj) === 'object' ? obj : this.getCenter();
			if (obj instanceof AMap.LngLat)		return obj;
			
			obj = $.extend({lng: 113, lat: 23}, obj);
			return new AMap.LngLat(obj.lng, obj.lat);
		},
		
		//生成信息窗口
		drawInfoDlg:function(info, position) {
			infoWindow = new AMap.InfoWindow({
	            content: info.join("<br/>")  //使用默认信息窗体框样式，显示信息内容
	        });
	        infoWindow.open(this.map, position);
        	this.panTo(position);
		},
		
		//设置信息窗口内容（标注点+信息窗口）
		setInfoDlgData:function(data, opt) {
			opt = opt || {}
			var that = this;
			var len = data.length;
			for (var i=0; i<len; i++) {
				var item = data[i];
				var position = that.trans2Position({lng:item.lng, lat: item.lat});
				var markerOpt = {position: position};
				if (opt.icon) {markerOpt.icon = opt.icon;}
				if (opt.iconSize) {markerOpt.iconSize = opt.iconSize;}
				that.addMarker(markerOpt);
				
				if (item.handler) {
					$(item.handler).unbind('click').bind('click', function() {
						var idx = $(this).attr('data-idx');
						var positionConf = $(this).attr('data-position');
						positionConf = positionConf.split(',');
						var position = that.trans2Position({lng:positionConf[0], lat: positionConf[1]});
						that.drawInfoDlg(data[idx].info, position);
					});
				}
			}
		},
		
		//绘制轨迹(无标注点)
		drawTrail:function(data, startHandler, stopHandler, opt) {
			var dist = 0;
			opt = opt || {};
			var that = this;
			var marker = new AMap.Marker({
				map: that.map,
		        position: [data[0].lng, data[0].lat],
		        icon: opt.icon || "http://code.mapabc.com/images/car_03.png",
		        offset: typeof(opt.iconSize) === 'undefined' ? new AMap.Pixel(-26, -13) : new AMap.Pixel(-opt.iconSize[0]/2, -opt.iconSize[1]/2),
		        autoRotation: true
			});
			
			var len = data.length;
			var lngX = data[0].lng;
			var latY = data[0].lat;
			lineArr = [];
			lineArr.push([lngX, latY]);
			for (var i = 1; i < len; i++) {
				dist += this.getDistance(data[0], data[i]);
		        lineArr.push([data[i].lng, data[i].lat]);
			}
			// 绘制轨迹
			var polyline = new AMap.Polyline({
				map: that.map,
		        path: lineArr,
		        strokeColor: "#00A",  //线颜色
		        strokeOpacity: 1,     //线透明度
		        strokeWeight: 3,      //线宽
		        strokeStyle: "solid"  //线样式
			});
			this.map.setFitView();
			
			if (startHandler) {
				var showTime = 5;	//大概5秒显示完
				$(startHandler).unbind('click').bind('click', function() {
					marker.moveAlong(lineArr, dist / showTime * 10);
				});
			}

			if (stopHandler) {
				$(stopHandler).unbind('click').bind('click', function() {
					marker.stopMove();
				});
			}
		},
		
		//获取两点间距离
		getDistance:function(point1, point2) {
			var radLat1 = this._deg2rad(point1.lat); 
			var radLat2 = this._deg2rad(point2.lat); 
			
			var a = radLat1 - radLat2; 
			var b = this._deg2rad(point1.lng) - this._deg2rad(point2.lng); 

			var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b/2),2))); 
			var EARTH_RADIUS = 6378137.0; //单位M 
			s = s * EARTH_RADIUS; 
			s = Math.round(s * 10000) / 10000.0; 
			return s; 
		},

		//角度转为狐度
		_deg2rad:function(d) {
			return d * Math.PI / 180.0; 
		},

		//初始化搜索框
		initSearch:function() {
			var opt = this.opt;
			if (!opt.searchInputEl)		return;	//搜索输入框
			if (!opt.searchListPanel)	return;	//搜索结果显示页面
			
			var that = this;
			this._autoSearch(opt);
			$(opt.searchInputEl).unbind('keyup').bind('keyup', function(event) {
				that._searchKeyDown(event, that);
			});
			
			if (opt.searchBtn) {
				$(opt.searchBtn).unbind('click').bind('click', function(event) {
					that._clickSearch(event, that);
				});
			}
			
			var targetId1 = $(opt.searchInputEl)[0].id;
			var targetId2 = $(opt.searchListPanel)[0].id;
			
			//点击空白处，隐藏位置选择页面
			$(document).click(function(evt) {
				if ($(evt.target)[0].id == targetId1 || $(evt.target)[0].id == targetId2) {
					return;
				} else if ($(evt.toElement) && $(evt.toElement)[0]) {
					if ($(evt.toElement)[0].id == targetId1 || $(evt.toElement).parents('#'+targetId1).length > 0) return;
					if ($(evt.toElement)[0].id == targetId2 || $(evt.toElement).parents('#'+targetId2).length > 0) return;
				}
				
                $(opt.searchListPanel).css({'display': 'none'});
			});
			
			//点击地图，将点击的位置设置为中心点
			AMap.event.addListener(that.map, 'click', function(ret) {
				var position = ret.lnglat;
				that.setCenter(ret.lnglat);
			});
		},
		
		//点击搜索按钮查找
		_clickSearch:function(event, that) {
			var opt = that.opt;
			var inputVal = $(opt.searchInputEl).val();
			if ($.trim(inputVal) == '')		return;		//内容为空，不搜索
			frame_obj.lock_screen('搜索中...', true);
			
			//在当前地图显示的城市内查找
			var MSearch;
			AMap.service(["AMap.PlaceSearch"], function() {       
				MSearch = new AMap.PlaceSearch({ //构造地点查询类
					pageSize:	10,
					pageIndex:	1,
//					city:		ret.citycode
				});
				//关键字查询
				MSearch.search(inputVal, function(status, result){
					if(status === 'complete' && result.info === 'OK'){
						that._initWinFun();
						placeSearch_CallBack(result);
					}
				}); 
			});
			/*
			this.map.getCity(function(ret) {
				var MSearch;
				AMap.service(["AMap.PlaceSearch"], function() {       
					MSearch = new AMap.PlaceSearch({ //构造地点查询类
						pageSize:	10,
						pageIndex:	1,
						city:		ret.citycode
					});
					//关键字查询
					MSearch.search(inputVal, function(status, result){
						if(status === 'complete' && result.info === 'OK'){
							that._initWinFun();
							placeSearch_CallBack(result);
						}
					}); 
				});
			});
			*/
		},
		
		 //输入提示\自动查找
		_autoSearch:function(opt) {
			var that = this;
	        var keywords = $(opt.searchInputEl).val();
	        var auto;
	        //加载输入提示插件
	        AMap.service(["AMap.Autocomplete"], function() {
	            var autoOptions = {
	                city: "" //城市，默认全国
	            };
	            auto = new AMap.Autocomplete(autoOptions);
	            //查询成功时返回查询结果
	            if (keywords.length > 0) {
	                auto.search(keywords, function(status, result) {
	                    that.autoCompleteCallBack(result, that);
	                });
	            }
	            else {
	                $(that.searchListPanel).css({'display': 'none'});
	            }
	        });
		},

	    //输出输入提示结果的回调函数
		autoCompleteCallBack:function(data, that) {
			opt = that.opt;
	        var resultStr = "";
	        var tipArr = data.tips;
	        if (tipArr && tipArr.length > 0) {
	            for (var i = 0; i < tipArr.length; i++) {
	                resultStr += "<div id='divid" + (i + 1) + "' onmouseover='openMarkerTipById(" + (i + 1)
	                        + ",this)' onclick='selectResult(" + i + ")' onmouseout='onmouseout_MarkerStyle(" + (i + 1)
	                        + ",this)' style=\"font-size: 13px;cursor:pointer;padding:5px 5px 5px 5px;\"" + "data=" + tipArr[i].adcode + ">" + tipArr[i].name + "<span style='color:#C1C1C1;'>" + tipArr[i].district + "</span></div>";
	            }
	        }
	        else {
	            resultStr = " π__π 亲,人家找不到结果!<br />要不试试：<br />1.请确保所有字词拼写正确<br />2.尝试不同的关键字<br />3.尝试更宽泛的关键字";
	        }
	        
	        var input = $(opt.searchInputEl)[0];
	        var retPanel = $(opt.searchListPanel)[0];
	        retPanel.curSelect = -1;
	        retPanel.tipArr = tipArr;
	        retPanel.innerHTML = resultStr;
	        retPanel.style.display = "block";
	        
	        that._initWinFun();
	        
	        //输入提示框鼠标滑过时的样式
	        window.openMarkerTipById = function(pointid, thiss) {  //根据id打开搜索结果点tip
	        	thiss.style.background = '#CAE1FF';
	        }
	        
	        //输入提示框鼠标移出时的样式
	        window.onmouseout_MarkerStyle = function(pointid, thiss) {  //鼠标移开后点样式恢复
	        	thiss.style.background = "";
	        }
	        
	        //从输入提示框中选择关键字并查询
	        window.selectResult = function(index) {
	        	if (index < 0) {
	        		return;
	        	}
	        	if (navigator.userAgent.indexOf("MSIE") > 0) {
	        		input.onpropertychange = null;
	        		input.onfocus = focus_callback;
	        	}
				frame_obj.lock_screen('搜索中...', true);
	        	//截取输入提示的关键字部分
	        	var text = document.getElementById("divid" + (index + 1)).innerHTML.replace(/<[^>].*?>.*<\/[^>].*?>/g, "");
	        	var cityCode = document.getElementById("divid" + (index + 1)).getAttribute('data');
	        	input.value = text;
	        	retPanel.style.display = "none";
	        	//根据选择的输入提示关键字查询
	        	that.map.plugin(["AMap.PlaceSearch"], function() {
	        		var msearch = new AMap.PlaceSearch();  //构造地点查询类
	        		AMap.event.addListener(msearch, "complete", placeSearch_CallBack); //查询成功时的回调函数
	        		msearch.setCity(cityCode);
	        		msearch.search(text);  //关键字查询查询
	        	});
	        }

	        //定位选择输入提示关键字
	        window.focus_callback = function() {
	            if (navigator.userAgent.indexOf("MSIE") > 0) {
	            	input.onpropertychange = that._autoSearch(opt);
	            }
	        }

	        //鼠标滑过查询结果改变背景样式，根据id打开信息窗体
	        window.openMarkerTipById1 = function(pointid, thiss) {
	            thiss.style.background = '#CAE1FF';
	            windowsArr[pointid].open(that.map, marker[pointid]);
	        }

	    },
	    
	    _initWinFun:function() {
	    	var that = this;
	    	if (typeof(window.placeSearch_CallBack) === 'undefined') {
	    		//输出关键字查询结果的回调函数
	    		window.placeSearch_CallBack = function(data) {
	    			//清空地图上的InfoWindow和Marker
	    			that.windowsArr = [];
	    			that.searchMarker = [];
	    			that.map.clearMap();
	    			var resultStr1 = "";
	    			var poiArr = data.poiList.pois;
	    			var resultCount = poiArr.length;
	    			for (var i = 0; i < resultCount; i++) {
	    				resultStr1 += "<div id='divid" + (i + 1) + "' onmouseover='openMarkerTipById1(" + i + ",this)' onmouseout='onmouseout_MarkerStyle(" + (i + 1) + ",this)' style=\"font-size: 12px;cursor:pointer;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\"><table><tr><td><img src=\"http://webapi.amap.com/images/" + (i + 1) + ".png\"></td>" + "<td><h3><font color=\"#00a6ac\">名称: " + poiArr[i].name + "</font></h3>";
	    				resultStr1 += createContent(poiArr[i].type, poiArr[i].address, poiArr[i].tel) + "</td></tr></table></div>";
	    				addmarker(i, poiArr[i]);
	    			}
	    			that.map.setFitView();

					setTimeout(function() {
						frame_obj.unlock_screen();
					}, 500);
	    		}
	    	}
	    	
	    	if (typeof(window.createContent) === 'undefined') {
		        //窗体内容
		        window.createContent = function(type, address, tel) {  
		            type=parseStr(type);
		            address=parseStr(address);
		            tel=parseStr(tel);
		            var s=[];
		            s.push("地址：" +address);
		            s.push("电话：" +tel);
		            s.push("类型：" +type);
		            return s.join("<br>");
		        }
	    	}
	    	
	    	if (typeof(window.parseStr) === 'undefined') {
		        //infowindow显示内容
		        window.parseStr = function(p){
		            if(!p || p == "undefined" || p == " undefined"||p=="tel"){
		                p="暂无";
		            }
		            return p;
		        }
	    	}
	    	
	    	if (typeof(window.addmarker) === 'undefined') {
		        //添加查询结果的marker&infowindow
		        window.addmarker = function(i, d) {
		            var lngX = d.location.getLng();
		            var latY = d.location.getLat();
		            var markerOption = {
		                map: that.map,
		                icon:"http://webapi.amap.com/theme/v1.3/markers/n/mark_b"+(i+1)+".png",
		                position: [lngX, latY],
		                clickable: true
		            };
		            var mar = new AMap.Marker(markerOption);
		            that.searchMarker.push([lngX, latY]);
	
		            var infoWindow = new AMap.InfoWindow({
		                content: "<h3><font color=\"#00a6ac\">  " + (i + 1) + ". " + d.name + "</font></h3>" + createContent(d.type, d.address, d.tel),
		                size: new AMap.Size(300, 0),
		                autoMove: true,
		                offset: new AMap.Pixel(0, -30)
		            });
		            that.windowsArr.push(infoWindow);
		            var aa = function(e) {
		                infoWindow.open(that.map, mar.getPosition());
		            };
		            mar.on( "mouseover", aa);
		            mar.on( "click", function(ret) {
		            	that.setCenter(ret.lnglat);
		            });
		        }
	    	}
	    },
		
		//搜索时输入内容（变更）
		_searchKeyDown:function(event, that) {
			var opt = that.opt;
	        var input = $(opt.searchInputEl)[0];
	        var retPanel = $(opt.searchListPanel)[0];
	        
	        var key = (event || window.event).keyCode;
	        var result = $(opt.searchListPanel)[0];
	        var cur = result.curSelect;
	        if (key === 40) {//down
	            if (cur + 1 < result.childNodes.length) {
	                if (result.childNodes[cur]) {
	                    result.childNodes[cur].style.background = '';
	                }
	                result.curSelect = cur + 1;
	                result.childNodes[cur + 1].style.background = '#CAE1FF';
	                input.value = result.tipArr[cur + 1].name;
	            }
	        } else if (key === 38) {//up
	            if (cur - 1 >= 0) {
	                if (result.childNodes[cur]) {
	                    result.childNodes[cur].style.background = '';
	                }
	                result.curSelect = cur - 1;
	                result.childNodes[cur - 1].style.background = '#CAE1FF';
	                input.value = result.tipArr[cur - 1].name;
	            }
	        } else if (key === 13) {
	            var res = document.getElementById("result1");
	            if (res && res['curSelect'] !== -1) {
	                selectResult(retPanel.curSelect);
	            }
	        } else {
	            that._autoSearch(opt);
	        }
		},
		
		//获取指定位置信息
		getInfoByPosition:function(position, callback) {
			var that = this;
			var MGeocoder;
	        //加载地理编码插件
	        AMap.service(["AMap.Geocoder"], function() {
	            MGeocoder = new AMap.Geocoder({
	                radius: 1000,
	                extensions: "all"
	            });
	            //逆地理编码
				position = that.trans2Position(position);
				MGeocoder.getAddress(position, function(status, result) {
	                if (status === 'complete' && result.info === 'OK') {
	                	if (typeof(callback) === 'function') {
	                		callback(result);
	                	}
	                }
	            });
	        });
		}
		
	};
	
	$.fn.gdmap = function(option) {
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function() {
			var $this = $(this);
			var data = $this.data('gdmap');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('gdmap', (data = new GDMap(this, $.extend({}, $.fn.gdmap.def, options))));
			}
			if (typeof option == 'string' && typeof data[option] == 'function') {
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined) {
					return false;
				}
			}
		});
		if (internal_return !== undefined)
			return internal_return;
		else
			return this;
	};
	
	$.fn.gdmap.def = {
		'debug': false,
	};
	
	$.fn.gdmap.Constructor = GDMap;
	
})(jQuery);