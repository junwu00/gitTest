var alive_ajax = new Array();
var alive_tip = false;
var in_wxbrowser = false;

$(document).ready(function() {
	in_wxbrowser = frame_obj.is_weixin();
});

var frame_obj = {
	init_frame:function() {
		frame_obj.init_auto_height();
		$(window).resize(function(){                
			frame_obj.init_auto_height();
		});
	},
	
	init_auto_height:function() {
		var height = $(window).height();
	},

	post:function(opt) {
		if(opt.msg){
			frame_obj.lock_screen(opt.msg);
		}
		var url = opt.url || $('#ajax-url').val();
		var ajax_key = opt.ajax_key || url;
		if (opt.btn && $(opt.btn).length > 0)	$(opt.btn).attr('disabled', 'disabled');
		
		if (alive_ajax[ajax_key]) {
			try {
				alive_ajax[ajax_key].abort();
			} catch(e) {}
		}

		var request = $.ajax({
			url 		: url,
			type 		: 'post',
			data 		: {"ajax_act": opt.ajax_act || '', "data": opt.data || {}},	
			dataType 	: opt.data_type || 'json',
			success 	: function(resp){
				frame_obj.unlock_screen();
				if (opt.btn && $(opt.btn).length > 0)	$(opt.btn).attr('disabled', false);
				if (typeof(opt.success) === 'function') {
					opt.success(resp, opt);
				} else if (resp.errmsg) {
					frame_obj.tips(resp.errmsg);
				}
			},
			error : function(resp, status, e){
				frame_obj.unlock_screen();
				if (status == 'abort') 	return;
				
				if (typeof(opt.error) === 'function') {
					opt.error(resp, status, e, opt);
				} else {
					if (opt.btn && $(opt.btn).length > 0)	$(opt.btn).attr('disabled', false);
					if (status != 'abort') {
						frame_obj.tips('系统繁忙，请刷新页面后重试');
					}
				}
			}
		});
		if (typeof(opt.abort) === 'undefined' || opt.abort)	alive_ajax[ajax_key] = request;		//保存请求记录，下次发起时若上次未响应，则断开上次请求
	},
	
	do_ajax_post:function(btn, ajax_act, json_data, callback, params, data_type, ajax_key, wait_msg) {
		var post_opt = {
			url			: $('#ajax-url').val(),
			btn			: btn,
			ajax_act	: ajax_act,
			data		: json_data,
			data_type	: data_type,
			ajax_key	: ajax_key,
			msg			: wait_msg, 
			success		: function(resp) {
				if (typeof(callback) === 'function') {
					callback(resp, params);
				} else if (resp.errmsg) {
					frame_obj.tips(resp.errmsg);
				}
			}
		};
		frame_obj.post(post_opt);
	},
	
	simple_ajax_post:function(btn, url, ajax_act, data_type, callback, params, abort, wait_msg) {
		var post_opt = {
			url			: url,
			btn			: btn,
			ajax_act	: ajax_act,
			data_type	: data_type,
			abort		: typeof(abort) === 'undefined' ? false : abort,
			success		: function(resp) {
				if (typeof(callback) === 'function') {
					callback(resp, params);
				}
			}
		};
		frame_obj.post(post_opt);
	},
	
	ajax_upload:function(btn, form, url, before_fun, callback_fun, callback_params, timeout) {
		if (timeout == undefined) timeout = 30000;			//超时时间,毫秒
		
		btn.unbind('change').bind("change", function() {
			form.submit();
		});
		var options = {
			url : url,
			timeout : timeout,
			beforeSubmit: function() {
				if (before_fun != undefined) {
					return before_fun();
				} else {
					frame_obj.lock_screen('上传中 ...');
					return true;
				}
			},
			success : function(response, status) {
				frame_obj.unlock_screen();
				if (callback_fun != undefined) {
					callback_fun(response, callback_params);
				}
			},
			error: function(response, status, e) {
				frame_obj.unlock_screen();
				if (e == 'timeout') {
					frame_obj.alert('上传超时');		
				} else {
					frame_obj.alert('系统繁忙，请刷新页面后重试');		
				}
			}
		};
		form.submit(function() {
			$(this).ajaxSubmit(options);
			return false;
		});
	},
	
	comfirm:function(title, callback) {
		$('#comfirm-dlg .modal-title').html(title);
		$('#comfirm-dlg').modal('show');
		
		if (callback != undefined && callback != '') {
			$('#comfirm-ok').unbind('click').bind('click', function() {
				$('#comfirm-dlg').modal('hide');
				callback();
			});
		}
	},
	
	alert:function(msg, btn_text, callback) {
		if (btn_text == undefined || btn_text == '') {
			btn_text = '确定';
		}
		$('#alert-ok').html(btn_text);
		
		$('#alert-dlg .modal-title').html(msg);
		$('#alert-dlg').modal('show');
		
		if (callback != undefined && callback != '') {
			$('#alert-ok').unbind('click').bind('click', function() {
				$('#alert-dlg').modal('hide');
				callback();
			});
		}
	},
	
	close_browser:function() {
		WeixinJSBridge.call('closeWindow');
	},
	
	sleep:function(numberMillis) {
	   var now = new Date();
	   var exitTime = now.getTime() + numberMillis;  
	   while (true) { 
	       now = new Date(); 
	       if (now.getTime() > exitTime)    return;
	   }
	},
	
	is_weixin:function() {
	    var ua = window.navigator.userAgent.toLowerCase();
	    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
	        return true;
	    }else{
	        return false;
	    }
	},
	
	show_img:function(src) {
		var html = '<div class="full-screen-img">' +
			'<span>&times;</span>' +
			'<img src="' + src + '">' +
		'</div>';
		$('body').append(html);
		
		$('.full-screen-img span').unbind('click').bind('click', function() {
			$(this).parent().remove();
		});
	},
	
	share:function(img, url, desc, title) {
		WeixinJSBridge.on('menu:share:appmessage', function(argv){ 
		
			WeixinJSBridge.invoke('sendAppMessage', {
		        "appid":		"",                                     //appid 设置空就好了。
		        "img_url":	 	img,                                   	//分享时所带的图片路径
		        "img_width":	"120",                            		//图片宽度
		        "img_height":	"120",                            		//图片高度
		        "link":			url,                                    //分享附带链接地址
		        "desc":			desc,                            		//分享内容介绍
		        "title":		title,		
			}, function(res){});
			
		});
	},
	lock_screen:function(msg, with_img) {
		with_img = typeof(with_img) === 'undefined' ? true : with_img;
		var img = '';
		if (with_img) {
			img = '<img src="' + $('#http-domain').val() + 'apps/common/static/images/loading.jpg" style="width: 24px; vertical-align: middle;">&nbsp;&nbsp;';
		}
		
		if ($('#lock-screen').length > 0) {
			$('#lock-screen > p > span').html(img + msg);
			
		} else {
			var inner_html = '<div id="lock-screen" style="position: fixed; width: 100%; height: 100%; left: 0; top: 0; background: rgba(0,0,0,0.2); z-index: 2000;">' +
					'<p style="color: #FFF; width: 100%; text-align: center; margin-top: 20%; text-align: center;">' +
					'<span style="display: inline-block; max-width: 70%; height: auto; padding: 15px 20px; background-color: rgba(0,0,0,0.5); border-radius: 5px;">' + 
						img +
						msg +
					'</span>' +
				'</p>' +
			'</div>';
			$('body').append(inner_html);
		}
	},
	
	unlock_screen:function() {
		if ($('#lock-screen').length > 0) {
			$('#lock-screen').remove();
		}
	},
	tips:function(msg, opt) {
		opt = opt || {};
		frame_obj.unlock_screen();
		frame_obj.lock_screen(msg, false);
		var time = opt.time || msg.length * 200 + 500;
		if (time > 2500)  time = 2500;		//最长2.5秒
		setTimeout(function() {
			frame_obj.unlock_screen();
		}, time);
	},
	//倒计时: date：Date实例; show_all: 是否一直显示0天0小时等; callback: 为0时调用
	timer:function(el_id, date, show_all, callback) {
		show_all = typeof(show_all) === 'undefined' ? true : show_all;
		var ts = date - new Date();							//计算剩余的毫秒数
		ts /= 1000;
		var dd = parseInt(ts / 60 / 60 / 24, 10);			//计算剩余的天数
		var hh = parseInt(ts / 60 / 60 % 24, 10);			//计算剩余的小时数
		var mm = parseInt(ts / 60 % 60, 10);				//计算剩余的分钟数
		var ss = parseInt(ts % 60, 10);						//计算剩余的秒数
		mm = checkTime(mm);
		ss = checkTime(ss);
		if (parseInt(ss) <= 0) {
			ss = '00';
		}
		
		if (show_all) {
			$(el_id).html(dd + "天" + hh + "小时" + mm + "分钟" + ss + "秒");
		} else {
			var html = '';
			if (parseInt(dd) > 0) 			html = dd + "天" + hh + "小时" + mm + "分钟" + ss + "秒";
			else if (parseInt(hh) > 0)		html = hh + "小时" + mm + "分钟" + ss + "秒";
			else if (parseInt(mm) > 0)		html = mm + "分钟" + ss + "秒";
			else							html = ss + "秒";
			$(el_id).html(html);
		}
		
		if (ts > 0) {
			setTimeout(function() {
				frame_obj.timer(el_id, date, show_all, callback);
			}, 1000);
			
		} else {
			if (typeof(callback) === 'function') {
				callback();
			}
		}

		function checkTime(i) {  
			if (i < 10) {  
				i = "0" + i;  
			}  
			return i;  
		}  
	},
	//将时间戳(单位：秒)格式化成：刚刚、1分钟前...与当前时间相比大于24小时的显示具体时间（需要time2date方法支持，见format.js）
	recent_time:function(timestamp) {
		var diff = (new Date()).getTime() / 1000 - timestamp;
		if (diff >= (24 * 3600)) {
			return time2date(timestamp);
		}
		
		if (diff <= 60) {		
			return '刚刚';								//1分钟前
		}
		
		var mm = parseInt(diff / 60 % 60, 10);
		if (diff <= 3600) {								//1小时前
			return mm + '分钟前';
		}
		
		var hh = parseInt(diff / 60 / 60 % 24, 10);	
		return mm > 0 ? (hh + '小时' + mm + '分钟前') : (hh + '小时前');
	}

};