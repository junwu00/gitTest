<?php
/**
 * 定期清除分段上传临时文件
 *
 * @author LiangJianMing
 * @create 2015-08-28
 */
PHP_SAPI != 'cli' and die('仅允许在cli模式下执行');

$root_dir = dirname(dirname(__DIR__));
$global_file = $root_dir . '/global.config.php';
!file_exists($global_file) and die('全局配置文件缺失');

include_once($global_file);

//设定脚本执行时间限制
set_time_limit(0);
//最大可用内存
ini_set('memory_limit', -1);

$start_time = time();

//最大生存时间为1周
$max_alive_time = 7 * 24 * 60 * 60;

$dir = SYSTEM_PART_DIR;
!is_dir($dir) and die('目录不存在!');

$handler = opendir($dir);
!$handler and die('打开目录失败!');

while(($file = readdir($handler)) !== FALSE) {
    if ($file != '.' and $file != '..') {
        $path = $dir . '/' . $file;
        $edit_time = get_edit_time($path);
        if ($edit_time and time() - $edit_time > $max_alive_time) {
            unlink($path);
        }
    }
}

closedir($handler);

end:
echo '执行完成，消耗时间：' . (time() - $start_time) . "秒\n";
exit(0);

function get_edit_time($file) {
    clearstatcache();
    return filemtime($file);
}

/* End of this file */