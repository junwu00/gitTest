<?php
/**
 * APP访问量记录
 *
 * @author yangpz
 * @create 2015-04-23
 */

PHP_SAPI != 'cli' && die('仅允许在cli模式下执行');

$root_dir = dirname(dirname(__DIR__));
$global_file = $root_dir . '/global.config.php';
!file_exists($global_file) && die('全局配置文件缺失');

include_once($global_file);

//设定脚本执行时间限制
set_time_limit(0);
//最大可用内存
ini_set('memory_limit', -1);

$start = time();
log_write('开始保存应用访问量记录');

// 统计wx包含前三天的应用使用情况
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-3 day')));
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-2 day')));
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-1 day')));

// 统计pc包含前三天的应用使用情况
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-3 day')), 'pc');
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-2 day')), 'pc');
g('app_visit') -> deal_by_day(date('Ymd', strtotime('-1 day')), 'pc');

$end = time();
log_write('保存应用访问量记录完成');
log_write('耗时：'.($end - $start).'秒');

/* End of this file */