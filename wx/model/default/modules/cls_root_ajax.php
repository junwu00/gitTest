<?php
/**
 * 主框架交互异步操作类
 *
 * @author LiangJianMing
 * @create 2015-08-27
 */
class cls_root_ajax {
    //命令对应函数名的映射表
    private $cmd_map = array(
        //上传文件
        101 => 'upload_file',
        //查询文件是否已存在，如果存在，则返回对应信息
        102 => 'get_file_info',
        //获取预览链接
        103 => 'get_preview_url',
        //jssdk初始化
        104 => 'assign_jssdk_sign',
    );

    public function assign_jssdk_sign()
    {
        $data = get_var_post('data', FALSE);
        !is_array($data) && $data = json_decode($data, TRUE);
        $url = isset($data['url'])?$data['url']:'';
        $data = g('js_sdk') -> auth_assign_jssdk_sign($url,true);

        $data = [
             'appid'=>$data['app_id'] ,
             'timestamp'=>$data['time'] ,
             'noncestr'=>$data['nonce_str'] ,
             'signature'=>$data['sign'] ,
             'this_url'=>$data['this_url'] ,
        ];
        cls_resp::echo_ok(NULL, 'data', $data);
    }
    /**
     * ajax行为统一调用方法
     *
     * @access public
     * @return void
     */
    public function index() {
        $cmd = (int)get_var_value('cmd', FALSE);
        empty($cmd) and cls_resp::echo_resp(cls_resp::$ForbidReq);

        $map = $this -> cmd_map;
        !isset($map[$cmd]) and cls_resp::echo_resp(cls_resp::$ForbidReq);

        $this -> $map[$cmd]();
    }

    /**
     * 获取预览url
     *
     * @access public  
     * @return void
     */
    private function get_preview_url() {
        //云存储查询哈希
        $hash = get_var_value('hash');
        $ext = get_var_value('ext');

        $hash = trim($hash);
        $ext = strtolower(trim($ext));

        if (empty($hash) or strlen($hash) != 32) {
            cls_resp::echo_busy();
        }

        $url = g('media') -> cloud_rename_preview('预览文件', $hash, '', false);
        $info = array(
            'preview_url' => $url,
        );

        //已经存在
        cls_resp::echo_ok(NULL, 'data', $info);
    }

    /**
     * 上传文件
     *
     * @access private
     * @return void
     */
    private function upload_file() {
        //判断上传方式
        $utype = (int)get_var_value('utype');
        switch ($utype) {
            case 2 : {
                //js_sdk上传
                $this -> jssdk_upload();
                BREAK;
            }
            case 3 : {
                //分段上传
                $this -> part_upload();
                BREAK;
            }
            default : {
                //常规上传
                $this -> normal_upload();
                BREAK;
            }
        }
    }

    /**
     * 查询文件是否已存在，如果存在，则返回对应信息
     *
     * @access public  
     * @return void
     */
    private function get_file_info() {
        //文件哈希
        $hash = get_var_value('hash');
        //文件扩展名
        $ext = get_var_value('ext');

        $hash = trim($hash);
        $ext = trim($ext);

        if (empty($hash) or empty($ext)) {
            log_write('查询云存储是否已经存在该文件，参数不合法');
            cls_resp::echo_busy();
        }

        try {
            $info = g('media') -> cloud_get_info($hash, $ext);
            log_write("查询云存储是否已经存在该文件，查询成功");
        }catch(SCException $e) {
            log_write("查询云存储是否已经存在该文件，查询异常：" . $e -> getMessage());
            cls_resp::echo_busy();
        }

        //已经存在
        cls_resp::echo_ok(NULL, 'data', $info);
    }

    /**
     * 支持断点续传的上传实现
     *
     * @access private
     * @return array
     * @throws SCException
     */
    private function part_upload() {
        //每一片段最大为1MB
        $part_size = 1 * 1024 * 1024;
        //文件总大小的最大值
        $file_size = 20 * 1024 * 1024;

        //文件内容哈希
        $hash = get_var_value('hash');
        //片段在源文件中的开始位置
        $offset = (int)get_var_value('offset');
        //文件扩展名
        $ext = get_var_value('ext');
        //文件内容片段
        $part = get_var_value('part');
        //是否最后一条片段
        $is_end = (int)get_var_value('is_end');

        $hash = trim($hash);
        $ext = trim($ext);

        $is_end = $is_end ? TRUE : FALSE;

        if (empty($hash) or empty($ext) or (empty($part) and !$is_end)) {
            log_write('分段上传，参数不合法');
            cls_resp::echo_busy();
        }

        $data = array(
            'hash' => $hash,
            'offset' => $offset,
            'ext' => $ext,
            'part' => $part,
            'is_end' => $is_end,
        );

        $max_size = array(
            'part' => $part_size,
            'file' => $file_size,
        );

        try {
            $ret = g('media') -> part_upload($data, $max_size);
        }catch(SCException $e) {
            log_write('上传文件片段失败，异常：[' . $e -> getMessage() . ']');
            //cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
            cls_resp::echo_busy();
        }

        if ($ret['status'] !== 1) {
            $info = array(
                'status' => $ret['status'],
                'offset' => $ret['offset'],
            );

            //单个片段上传完成
            cls_resp::echo_ok(NULL, 'data', $info);
        }

        log_write('分段上传文件成功');

        $info = array(
            'status' => 1,
            'data' => $ret['info'],
        );

        cls_resp::echo_ok(NULL, 'data', $info);
    }

    /**
     * 常规上传实现
     *
     * @access private
     * @return void
     */
    private function normal_upload() {
        log_write('开始常规上传文件');

        $file = array_shift($_FILES);
        if (empty($file) or !is_array($file)) {
            $tip_str = '找不到该上传文件，请重新上传！';
            log_write($tip_str);
            cls_resp::echo_err(cls_resp::$FileNotFound, $tip_str);
        }

        try {
            //本地测试使用
//          $data = g('media') -> local_upload($file,1024*1024*10);
            //线上使用
            $data = g('media') -> cloud_upload('', $file);
        }catch(SCException $e) {
            log_write('常规上传文件失败，异常：[' . $e -> getMessage() . ']');
            cls_resp::echo_exp($e);
        }
        log_write('常规上传文件成功');

        cls_resp::echo_ok(NULL, 'data', $data);
    }

    /**
     * jssdk获取图片，再上传图片
     *
     * @access private
     * @return void
     */
    private function jssdk_upload() {
    	//验证是否允许上传
        $is_ckeck = get_var_value('check');
        if (!empty($is_ckeck)) {
	        $cap_info = g('capacity') -> get_capacity_info();
			if ($cap_info['is_full']) {
				$forbit_time = $cap_info['notice_time'] + $cap_info['pre_buffer_time'] + $cap_info['buffer_time'];
				if ($cap_info['info_time'] >= $forbit_time) {
					cls_resp::echo_err(cls_resp::$ForbidReq, '您的企业服务器 <font color="#f74949">空间容量不足！</font><br>图片和文件上传功能暂时无法使用<br>请告知管理员扩容空间喔!');
				}
			}
			cls_resp::echo_ok();
        }
        
        log_write('开始使用js_sdk上传图片');
        $media_id = get_var_value('media_id');
        $media_id = trim($media_id);
        $com_id = (int)get_var_value('com_id');

        if (empty($media_id)) {
            cls_resp::echo_err(cls_resp::$Busy, '系统繁忙，请稍候再试！');
        }

        $com_id === 0 and $com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $token = g('atoken') -> get_access_token_by_com($com_id);
        if (empty($token)) {
            cls_resp::echo_err(cls_resp::$Busy, '系统繁忙，请稍候再试！');
        }

        try {
            $result = g('media') -> cloud_get_media($token, $media_id);
            log_write('js_sdk上传图片成功');
            cls_resp::echo_ok(NULL, 'data', $result);
            
        }catch(SCException $e) {
            log_write('js_sdk上传图片失败，异常：[' . $e -> getMessage() . ']');
            cls_resp::echo_exp($e);
        }
    }
}

/* End of this file */