<?php
/**
 * 测试微信卡券
 *
 * @author JianMing Liang
 * @create 
 */

class cls_card_test {
    // 签名所需的密钥
    private $secret = "test_card";

    /**
     * 发布卡券给当前用户
     *
     * @access public  
     * @return void
     */
    public function send() {
        // base64编码
        $data = get_var_value("data");

        $m = get_var_value("m");
        $cmd = (int)get_var_value("cmd");
        $time = (int)get_var_value("time");
        $sign = get_var_value("sign");

        $now_time = time();
        if ($now_time - $time > 1800) {
           cls_resp::show_warn_page("不合法的参数");
        }

        $param = array(
            "data" => $data,
            "m" => $m,
            "cmd" => $cmd,
            "time" => $time,
        );
        $real_sign = cls_sign::get_sign($param, $this -> secret);
        if ($sign !== $real_sign) {
            cls_resp::show_warn_page("不合法的参数");
        }

        // $this -> check_power();
        
        $data = base64_decode($data);
        $arr = explode(",", $data);
        $tmp_arr = array(
            "codes" => array(),
            "urls" => array(),
        );
        foreach ($arr as $val) {
            $tmp_val = trim($val);
            if (empty($tmp_val)) {
                continue;
            }

            if (check_data($tmp_val, "http")) {
                $tmp_arr["urls"][] = $tmp_val;
                continue;
            }

            $tmp_arr["codes"] = $tmp_val;
        }
        $card_arr = $tmp_arr;
        if (empty($card_arr["code"]) and empty($card_arr["urls"])) {
            cls_resp::show_warn_page("无效的参数");
        }

        // 避免发卡失败，先记录卡券的发布关系
        $res = g("wxcard") -> add_record($card_arr);

        // 获得全部创建的卡券
        $card_ids = g("wxcard") -> create_cards($card_arr);
        $card_infos = g("wxcard") -> get_card_exts($card_ids);
        g("smarty") -> assign("card_infos", json_encode($card_infos));

        smarty_init();
        g('js_sdk') -> auth_assign_jssdk_sign();

        g('smarty') -> show("card_test.html");
    }
}

/* End of this file */