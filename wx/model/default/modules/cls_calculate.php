<?php
/**
 * 统计应用使用频率
 * @author yangpz
 * @date 2015-04-23
 *
 */
class cls_calculate {
	public function index() {
		$data = get_var_post('data');		//APP名称
		if (empty($data))	return;
		$app_name = $data['app'];
		if (empty($app_name))	return;
		
		$map = app_info::$AppMap;
		$found = FALSE;						//APP ID
		foreach ($map as $app_id => $val) {
			if ($val['name'] == $app_name) {
				$found = $app_id;
				BREAK;
			}
		}
		unset($val);
		if (!$found)	return;
		
		$com_id = 0;						//企业ID
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		}
		
		g('app_visit') -> increase($com_id, $found);
	}
	
}

//end of file