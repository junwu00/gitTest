<?php
/**
 * 错误页面展示页面
 * @author yangpz
 * @date 2014-12-20
 *
 */
class cls_error {
	
	public function index() { 
		$reason = g('cookie') -> get_cookie('errmsg');
		g('smarty') -> assign('reason', $reason);
		g('smarty') -> show('error/index.html');
	}
	
}

//end of file