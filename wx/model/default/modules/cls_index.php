<?php
/** 
 * 默认的访问页面（暂仅用作图文消息查看）
 * 
 * @author yangpz
 * @create 2015-12-16 
 * @version 1.0.0
 */
class cls_index { 
	
	function __construct() {
		smarty_init();
	}

	/**
	 * 展示图文
	 */
	public function article() {
		$url = get_var_get('url');
		$news = g('article') -> get_news_by_url($url);
		
		if ($news) {
			$art = $news['art'];
			$news = $news['news'];
			$com = g('company') -> get_by_dept_id($news['dept_id'], 'id, name');

			//企业号方式
			g('js_sdk') -> assign_jssdk_sign('', $com['id']);
		
			g('smarty') -> assign('news', $art);
			g('smarty') -> assign('com_name', $com['name']);
			g('smarty') -> show('article.html');
			
		} else {
			cls_resp::show_warn_page(array('图文不存在'));
		}
	}

}

// end of file