<?php
/**
 * 快速体控制器
 *
 * @author LiangJianMing
 * @create 2015-1-8
 */
class cls_quick_try {
	/**
	 * 快速体验第一步
	 *
	 * @access public
	 * @return void
	 */
	public function step1 () {
		$qy = get_var_get('qy');		//指定要体验的企业号
		
		$g_exper_confs = $GLOBALS['g_exper_confs'];
		$confs = is_array($g_exper_confs) ? $g_exper_confs : array();
		if (empty($qy)) {			//未指定，取第一个（即非代理商的配置）
//			shuffle($confs);		//随机指定，暂不使用
			$conf = array_shift($confs);
			
		} else {
			$conf = $confs[$qy];
			empty($conf) && $conf = array_shift($confs);	//找不到对应配置，默认使用第一项配置
		}

		$step2_url = SYSTEM_HTTP_DOMAIN.'?m=quick_try&a=step2&free=1&key='.$conf['key'];
		$ajax_url = SYSTEM_HTTP_DOMAIN.'?m=quick_try&a=add_member&free=1&key='.$conf['key'];

		//$max_day = 5;
		$max_people = 1000;

		smarty_init();
		g('smarty') -> assign('conf', $conf);
		g('smarty') -> assign('step2_url', $step2_url);
		g('smarty') -> assign('ajax_url', $ajax_url);
		//g('smarty') -> assign('max_day', $max_day);
		g('smarty') -> assign('max_people', $max_people);

		$file = 'try/step1.html';
		g('smarty') -> show(SYSTEM_TPL.$file);
	}

	/**
	 * 快速体验第二步
	 *
	 * @access public
	 * @return void
	 */
	public function step2() {
		$key = get_var_value('key');

		$g_exper_confs = $GLOBALS['g_exper_confs'];
		if (!is_array($g_exper_confs) || !isset($g_exper_confs[$key])) {
			die('配置文件已变更，请重新选择!');
		}

		$conf = $g_exper_confs[$key];

		//$max_day = 5;

		smarty_init();
		g('smarty') -> assign('conf', $conf);
		//g('smarty') -> assign('max_day', $max_day);

		$file = 'try/step2.html';
		g('smarty') -> show(SYSTEM_TPL.$file);
	}

/*
	public function step3() {
		
		$app_id = 'wx992bf08e8c72348e';
		$app_secret = '4ab5d27592ba2843a49b33a9097d7f3e';

		$time = time();
		$nonce_str = get_rand_string(10);
		$this_url = get_this_url();

		$data = array(
			'noncestr' => $nonce_str,
			'timestamp' => $time,
			'url' => $this_url,
		);

		$sign = g('exper', $key) -> get_jsapi_sign($app_id, $app_secret, $data);

		smarty_init();
		g('smarty') -> assign('conf', $conf);

		g('smarty') -> assign('app_id', $app_id);
		g('smarty') -> assign('time', $time);
		g('smarty') -> assign('nonce_str', $nonce_str);
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('sign', $sign);

		$file = 'try/step3.html';
		g('smarty') -> show(SYSTEM_TPL.$file);
	}
*/
	
	/**
	 * 添加试用用户到对应企业号
	 *
	 * @access public
	 * @return void
	 */
	public function add_member() {
		$data = get_var_value('data');
		$data = json_decode($data, TRUE);
		if (!is_array($data) || empty($data)) {
			log_write('快速体验，data为空！');
			cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请重新进入该页再试!');
		}

		$name = trim($data['name']);
		$mobile = trim($data['mobile']);
		$key = (int)get_var_value('key');

		if (empty($name) || empty($mobile)) {
			cls_resp::echo_err(cls_resp::$Busy, '姓名和手机号码不能为空!');
		}

		if (!check_data($mobile, 'mobile')) {
			cls_resp::echo_err(cls_resp::$Busy, '手机号码格式不正确!');
		}

		$g_exper_confs = $GLOBALS['g_exper_confs'];
		if (!isset($g_exper_confs[$key])) {
			log_write('快速体验，配置文件异常[key=' . $key . '], conf=[' . json_encode($g_exper_confs) . ']');
			cls_resp::echo_err(cls_resp::$Busy, '地址出错，请重新打开！');
		}

		try {
			g('exper', $key) -> add_member($mobile, $name);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			log_write('快速体验，异常[' . $e -> getMessage() . ']');
			cls_resp::echo_err($e -> getCode(), $e -> getMessage());
		}
	}
}