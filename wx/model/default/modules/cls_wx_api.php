<?php
/**
 * 微信请求中转处理
 * 
 * @author yangpz
 * @date 2014-10-18
 *
 */
class cls_wx_api {
	
	private $token;
	private $suite;
	private $corp;
	private $wx_service;
	
	public function __construct() {
		$this -> check_token();
		$this -> wx_service = new cls_wx_service();
		$this -> wx_service -> init($this -> token, $this -> suite, $this -> corp);
	}
	
	/**
	 * 接收消息，验证接入
	 */
	public function index() {
		$this -> wx_service -> verify_token();
		
		if (isset($_GET["echostr"])) {
			echo $this -> wx_service -> verify_signature();
		} else {
			$this -> wx_service -> do_reply();	
		}
	}
	
	/**
	 * 用户进行二次验证
	 */
	public function user_sec_verify() {
	}
	
	//-------------------------内部实现---------------------------------
	
	/**
	 * 验证请求中有没有带token参数
	 */
	private function check_token() {
		if (!isset($_GET['token']) || is_null($_GET['token']) || empty($_GET['token'])) {
            throw new SCException('Token未找到');
        }

		$this -> token = get_var_value('token', FALSE);
		log_write("当前TOKEN={$this -> token}, SUITE={$this -> suite}, CORP={$this -> corp}");
	}
	
}

// end of file