<?php
/**
 * 微信消息处理类
 * 只支持通过授权方式开通的企业
 * 
 * @author yangpz
 * @date 2014-10-18
 */
class cls_wx_service {
	
	private $token;			//应用英文名称的md5
	private $suite;			//应用所在套件的suite id
	private $com_id;		//企业ID
	private $dept_id;		//企业根部门（只有一个）的ID
    private $app_id;		//三尺的应用ID
    private $wx_app_id;		//企业号应用ID
	private $corpid;		//企业corp id
	private $post_str;		//请求数据：字符串
	private $post_data;		//请求数据：解密后的，数组格式
	
	public function init($token, $suite='', $corp='') {
		$this -> token = $token;
		$this -> suite = $suite;
		$this -> corpid = $corp;
	}
	
	/**
	 * 验证token有效性
	 */
	public function verify_token() {
        $com_app = g('com_app') -> get_by_token($this -> token);
        log_write("com_app: " . json_encode($com_app));
        //验证是否在已启用应用中
        if (!$com_app) {
            throw new SCException('不合法的token: '.$this -> token);
        }

        $com = g('company') -> get_by_id($com_app['com_id']);
        $this -> dept_id = $com['dept_id'];
        $this -> com_id = $com['id'];
        $this -> app_id = $com_app['app_id'];
        $this -> wx_app_id = $com_app['wx_app_id'];
        $this -> corpid = $com['corp_id']; //根据token找到企业
        g('wxqy') -> init($this -> token, $this -> corpid, $com_app['aes_key']);

        /*
		//验证是否在已启用应用中
		$app = app_info::get_by_token($this -> token);
		if (!$app) throw new SCException('不合法的token: '.$this -> token);
		
		$com = g('company') -> get_by_corp_id($this -> corpid);
		if (!$com)	throw new SCException('不合法的corp：'.$this -> corpid);
		
		if ($com['state'] == 0) throw new SCException('企业被禁用：corp='.$this -> corpid);
		if ($com['state'] == 3) throw new SCException('企业已过期：corp='.$this -> corpid);
		
		$com_app = g('com_app') -> get_by_app($com['id'], $app['id']);
		if (!$com_app)				throw new SCException('企业应用未配置：token='.$this -> token.', corp='.$this -> corpid);
		if ($com_app['state'] == 0) throw new SCException('企业应用被禁用：token='.$this -> token.', corp='.$this -> corpid);
		if ($com_app['state'] == 2) throw new SCException('企业应用未授权：token='.$this -> token.', corp='.$this -> corpid);
		
		$suite_info = g('suite') -> get_by_suite_id($this -> suite);
		if (!$suite_info)			throw new SCException('不合法的suite: '.$this -> suite);
		
		$this -> dept_id = $com['dept_id'];
		$this -> com_id = $com['id'];
		$this -> app_id = $com_app['app_id'];
		g('wxqy') -> init($suite_info['token'], $this -> corpid, $suite_info['EncodingAESkey']);
        */
	}

	/**
	 * 验证消息来源是否合法
	 * @return 需要返回的消息
	 */
	public function verify_signature() {
        $reply_msg = g('wxqy') -> verify_url($_GET["msg_signature"], $_GET["timestamp"], $_GET["nonce"], $_GET["echostr"]);
        return $reply_msg;
	}
	
	/**
	 * 响应请求
	 */
	public function do_reply() {
		$post_str = isset($GLOBALS["HTTP_RAW_POST_DATA"]) ?  $GLOBALS["HTTP_RAW_POST_DATA"] : "";
		$this -> verify_msg($post_str);
		
		$this -> post_str = g('wxqy') ->  decrypt_msg($post_str, $_GET["msg_signature"], $_GET['timestamp'], $_GET['nonce']);
		$post_obj = simplexml_load_string($this -> post_str, 'SimpleXMLElement', LIBXML_NOCDATA);
		$this -> post_data = obj2arr($post_obj);
		log_write('请求体：' . json_encode($this -> post_data));

        // 授权变更通知，直接更新应用的可见范围
        if (isset($this -> post_data['Event']) and stripos($this -> post_data['Event'], 'subscribe') !== false) {
            $this -> _change_auth();
            // return;
        }

        if ($this -> app_id >= 23) return;      //流程专家，不处理回复

		//根据不同应用，返回不同内容
		$app_handler = app_info::get_handler($this -> app_id, $this -> com_id, $this -> dept_id);
		$app_handler -> do_reply($this -> post_data);
	}
	
	//------------------------------------------内部实现--------------------------------------

	/**
	 * 验证请求消息是否为空
	 * @param unknown_type $msg
	 */
	private function verify_msg($msg) {
		if (is_null($msg) || empty($msg)) {
			throw new SCException('空请求，不处理');
		}
	}

    // 单个应用授权变更
    private function _change_auth() {
        try {
            $token = g('atoken') -> get_access_token_by_com($this -> com_id);
            $url = 'https://qyapi.weixin.qq.com/cgi-bin/agent/get';
            $data = array(
                'access_token'  => $token,
                'agentid'       => $this -> wx_app_id,
            );
            $url .= '?' . http_build_query($data);
            $result = cls_http::curl($url, json_encode($data), 30);

            if (!is_array($result) or $result['httpcode'] != 200) throw new SCException('网络异常');

            $info = json_decode($result['content'], TRUE);
            if ($info['errcode'] != 0) throw new SCException($info['errmsg']);

            $data = array();
            // 应用直接被禁用
            if (empty($info['close'])) {
                $data['state'] = 1;
            }else {
                $data['state'] = 2;
            }

            $cond = array(
                'com_id=' => $this -> com_id,
                'app_id=' => $this -> app_id,
            );

            g('ndb') -> update_by_condition('sc_com_app', $cond, $data);


            g('com_app') -> private_update_visible($this -> com_id, $this -> wx_app_id, $info, $this -> app_id);

        }catch(SCException $e) {
            return;
        }
    }

}

// end of file