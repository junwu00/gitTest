<?php
/**
 * 报销类型信息
 * @author 
 *
 */
class cls_exacct_type {
	/** 对应的库表名称*/
	private static $Tabel = 'exacct_type';
	
	/** 禁用 */
	private static $StateOff = 0;
	/** 启用 */
	private static $StateOn = 1;
	/** 删除 */
	private static $StateDeleted = 2;
	
	/**
	 * 分页获取企业(主)报销类型列表
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $p_id		上级记录ID，默认只取顶级记录
	 * @param unknown_type $state		指定的类型状态，默认为非删除状态的记录
	 * @param unknown_type $fields
	 */
	public function get_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'state=' => self::$StateOn,
			'p_id=' => 0,
		);
		return g('ndb') -> select(self::$Tabel, $fields, $cond, 0, 0, '', ' order by idx, update_time desc ');
	}
	
	/**
	 * 获取企业所有（主）报销类型
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function get_full_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'p_id=' => 0,
		);
		return g('ndb') -> select(self::$Tabel, $fields, $cond);
	}
	
	/**
	 * 根据ID获取报销类型记录
	 * @param unknown_type $id		类型记录ID
	 * @param unknown_type $com_id	企业 ID
	 */
	public function get_by_id($id, $com_id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
		);
		$ret = g('ndb') -> select(self::$Tabel, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 获取类型的子类型
	 * @param unknown_type $pid		父级类型ID
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $fields	
	 */
	public function get_by_pid($pid, $com_id, $fields='*') {
		$cond = array(
			'p_id=' => $pid,
			'com_id=' => $com_id,
			'state=' => self::$StateOn,
		);
		return g('ndb') -> select(self::$Tabel, $fields, $cond);
	}
}

//end of file