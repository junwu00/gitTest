<?php
/** 
 * 分页控制器
 * 
 * 要用分页就用这个类吧
 * 
 * @author LiangJianMing
 * @create 2014-08-10 
 * @version 1.0
 */
class cls_pagination {
	public $page_name   = 'page'; //page标签，用来控制url页。比如说xxx.php?page=1中的page
	public $next_page   = '>'; //下一页
	public $pre_page    = '<'; //上一页
	public $first_page  = 'First'; //首页
	public $last_page   = 'Last'; //尾页
	public $pre_bar     = '<<'; //上一分页条
	public $next_bar    = '>>'; //下一分页条
	public $format_left = '';
	public $format_right= '';
	public $is_ajax     = false; //是否支持AJAX分页模式 
	
	public $page_barnum  = 5; //控制记录条的个数。
	public $total_page   = 0; //总页数
	public $ajax_action_name = ''; //AJAX动作名
	public $now_index    = 1; //当前页
	public $url         = ''; //url地址头
	public $offset      = 0;
 
	/**
	 * constructor构造函数
	 * @param array $array['total'],$array['page_size'],$array['now_index'],$array['url'],$array['ajax']
	 */
	function __construct($array)
	{
		if(is_array($array))
		{
			if(!array_key_exists('total',$array))
			{
				$this->error(__FUNCTION__,'need a param of total');
			}
			$total    = intval($array['total']);
			$page_size = (array_key_exists('page_size',$array)) ? intval($array['page_size']) : 10;
			$now_index = (array_key_exists('now_index',$array)) ? intval($array['now_index']) : '';
			$url      = (array_key_exists('url',$array)) ? $array['url'] : '';
		}
		else
		{
			$total    = $array;
			$page_size  = 10;
			$now_index = '';
			$url      = '';
		}
		if((!is_int($total)) || ($total < 0))
		{
			$this->error(__FUNCTION__,$total.' is not a positive integer!');
		}
		if((!is_int($page_size))||($page_size<=0))
		{
			$this->error(__FUNCTION__,$page_size.' is not a positive integer!');
		}
		if(!empty($array['page_name']))
		{
			$this->set('page_name',$array['page_name']); //设置pagename
		}
		$this->_set_now_index($now_index); //设置当前页
		$this->_set_url($url); //设置链接地址
		$this->total_page = ceil($total/$page_size);
		$this->offset = ($this->now_index-1)*$page_size;
		if(!empty($array['ajax']))
		{
			$this->open_ajax($array['ajax']); //打开AJAX模式
		}
	}
	/**
	 * 设定类中指定变量名的值，如果改变量不属于这个类，将throw一个exception
	 *
	 * @param string $var
	 * @param string $value
	 */
	function set($var,$value)
	{
		if(in_array($var,get_object_vars($this)))
		{
			$this->$var = $value;
		}
		else
		{
			$this->error(__FUNCTION__,$var." does not belong to Pagination!");
		}
	}
	/**
	 * 打开倒AJAX模式
	 *
	 * @param string $action 默认ajax触发的动作。
	 */
	function open_ajax($action)
	{
		$this->is_ajax=true;
		$this->ajax_action_name=$action;
	}
	/**
	 * 获取显示'下一页'的代码
	 * 
	 * @param string $style
	 * @return string
	 */
	function next_page($style='')
	{
		if($this->now_index<$this->total_page)
		{
			return $this->_get_link($this->_get_url($this->now_index+1),$this->next_page,$style);
		}
		return '<li class="disabled"><a href="javascript:void(0);" class="pre_bt">'.$this->next_page.'</a></li>';
	}
	/**
	 * 获取显示'上一页'的代码
	 *
	 * @param string $style
	 * @return string
	 */
	function pre_page($style='')
	{
		if($this->now_index>1)
		{
			return $this->_get_link($this->_get_url($this->now_index-1),$this->pre_page,$style);
		}
		return '<li class="disabled"><a href="javascript:void(0);" class="pre_bt">'.$this->pre_page.'</a></li>';
	}
	/**
	 * 获取显示'首页'的代码
	 *
	 * @return string
	 */
	function first_page($style='')
	{
		if($this->now_index==1)
		{
			return '<li class="disabled"><a href="javascript:void(0);" class="bg_ed">'.$this->first_page.'</a></li>';
		}
		return $this->_get_link($this->_get_url(1),$this->first_page,$style);
	}
 
	/**
	 * 获取显示'尾页'的代码
	 *
	 * @return string
	 */
	function last_page($style='')
	{
		if($this->now_index==$this->total_page)
		{
			return '<li class="disabled"><a href="javascript:void(0);" class="bg_ed">'.$this->last_page.'</a></li>';
		}
		return $this->_get_link($this->_get_url($this->total_page),$this->last_page,$style);
	}
	/**
	 * 获取显示中间页码
	 */
	function nowbar($style='',$now_index_style='')
	{
		$plus = ceil($this->page_barnum/2);
		if($this->page_barnum-$plus+$this->now_index>$this->total_page)
		{
			$plus = ($this->page_barnum-$this->total_page+$this->now_index);
		}
		$begin = $this->now_index-$plus+1;
		$begin = ($begin>=1) ? $begin : 1;
		$return='';
		for($i=$begin;$i<$begin+$this->page_barnum;$i++)
		{
			if($i<=$this->total_page)
			{
				if($i!=$this->now_index)
				{
					$return.=$this->_get_text($this->_get_link($this->_get_url($i),$i,$style));
				}
				else
				{ 
					$return.=$this->_get_text('<li class="'.$now_index_style.'"><a href="javascript:void(0);" class="total">'.$i.'</a></li>');
				}
			}else
			{
				break;
			}
			$return.="\n";
		}
		unset($begin);
		return $return;
	}
	/**
	 * 获取显示跳转按钮的代码
	 *
	 * @return string
	 */
	function select()
	{
		$return='<select name="'.$this->page_name.'">';
		for($i=1;$i<=$this->total_page;$i++)
		{
			if($i==$this->now_index)
			{
				$return.='<option value="'.$i.'" selected>'.$i.'</option>';
			}
			else
			{
				$return.='<option value="'.$i.'">'.$i.'</option>';
			}
		}
		unset($i);
		$return.='</select>';
		return $return;
	}
 
	/**
	 * 获取mysql 语句中limit需要的值
	 *
	 * @return string
	 */
	function offset()
	{
		return $this->offset;
	}
 
	/**
	 * 控制分页显示风格（可以增加相应的风格）
	 *
	 * @param int $mode
	 * @return string
	 */
	function show($mode = 1)
	{
		switch ($mode)
		{
			case 1:
				$this->next_page='下一页';
				$this->pre_page='上一页';
				$this->first_page='首页';
				$this->last_page='尾页';
				return $this->first_page().$this->pre_page().$this->next_page().$this->last_page();
			case 2:
				$this->first_page='首页';
				$this->next_page='下一页';
				$this->pre_page='上一页';
				$this->last_page='尾页';
				return $this->first_page().$this->pre_page().$this->nowbar('', 'active').$this->next_page().$this->last_page();
			default:
				return $this->pre_bar().$this->pre_page().$this->nowbar().$this->next_page().$this->next_bar();
		}
	}
	/**
	 * 设置url头地址
	 * @param string $url
	 * @return boolean
	 */
	function _set_url($url="")
	{
		if(!empty($url))
		{
			//手动设置
			$this->url=$url.((stristr($url,'&'))?'&':'?').$this->page_name."=";
		}
		else
		{
			//自动获取
			if(empty($_SERVER['QUERY_STRING']))
			{
				//不存在QUERY_STRING时
				$this->url=$_SERVER['REQUEST_URI']."?".$this->page_name."=";
			}
			else
			{
				if(stristr($_SERVER['QUERY_STRING'],$this->page_name.'='))
				{
					//地址存在页面参数
					$this->url=str_replace($this->page_name.'='.$this->now_index,'',$_SERVER['REQUEST_URI']);
					$last=$this->url[strlen($this->url)-1];
					if($last=='?'||$last=='&')
					{
						$this->url.=$this->page_name."=";
					}
					else
					{
						$this->url.='&'.$this->page_name."=";
					}
				}
				else
				{
					$this->url=$_SERVER['REQUEST_URI'].'&'.$this->page_name.'=';
				}
			}
		}
	}
	/**
	 * 设置当前页面
	 *
	 */
	function _set_now_index($now_index)
	{
		if(empty($now_index))
		{
			//系统获取
			if(isset($_GET[$this->page_name]))
			{
				$this->now_index=intval($_GET[$this->page_name]);
			}
		}
		else
		{
			//手动设置
			$this->now_index=intval($now_index);
		}
	}
	/**
	 * 为指定的页面返回地址值
	 *
	 * @param int $pageno
	 * @return string $url
	 */
	function _get_url($pageno=1)
	{
		return $this->url.$pageno;
	}
	/**
	 * 获取分页显示文字，比如说默认情况下_get_text('<a href="">1</a>')将返回[<a href="">1</a>]
	 *
	 * @param String $str
	 * @return string $url
	 */ 
	function _get_text($str)
	{
		return $this->format_left.$str.$this->format_right;
	}
	/**
	 * 获取链接地址
	 */
	function _get_link($url,$text,$style='')
	{
		$style=(empty($style))?'':'class="'.$style.'"';
		if($this->is_ajax)
		{
			//如果是使用AJAX模式
			return '<a '.$style.' href="javascript:'.$this->ajax_action_name.'(\''.$url.'\')">'.$text.'</a>';
		}
		else
		{
			return '<li '.$style.'><a href="'.$url.'">'.$text.'</a></li>';
		}
	}
	/**
	 * 出错处理方式
	 */
	function error($function,$errormsg)
	{
		die('Error in file <b>'.__FILE__.'</b> ,Function <b>'.$function.'()</b> :'.$errormsg);
	}
}
//end of this file