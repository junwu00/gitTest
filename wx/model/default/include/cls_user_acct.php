<?php
/**
 * 成员PC账号信息操作类
 * @author yangpz
 * @date 2016-01-04
 */
class cls_user_acct {
	/** 对应的库表名称 */
	private static $Table = 'sc_user_acct';
	
	/**
	 * 根据成员获取账号信息
	 * @param unknown_type $user_id	成员ID
	 */
	public function get_by_user_id($user_id) {
		$fields = 'user_id,acct,create_time,update_time';
		$cond = array(
			'user_id=' => $user_id
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			log_write('成员账号信息重复：'.json_encode($cond));
			throw new SCException('系统繁忙');
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 创建成员账号
	 * @param unknown_type $user_id	成员ID
	 * @param unknown_type $acct	自定义的账号
	 * @param unknown_type $pwd		密码
	 * @throws SCException
	 */
	public function create($user_id, $acct, $pwd) {
		$this -> _check_created($user_id);
		$this -> _check_exists($acct);
		
		$now = time();
		$data = array(
			'user_id' => $user_id,
			'acct' => $acct,
			'pwd' => md5($pwd),
			'create_time' => $now,
			'update_time' => $now
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('创建账号失败');
		}
		return $ret;
	}
	
	/**
	 * 更新密码
	 * @param unknown_type $user_id		成员ID
	 * @param unknown_type $acct		成员账号
	 * @param unknown_type $new_pwd		新密码
	 */
	public function update_pwd($user_id, $acct, $new_pwd) {
		$cond = array(
			'user_id=' => $user_id,
			'acct=' => $acct,
		);
		$data = array(
			'pwd' => md5($new_pwd),
			'update_time' => time()
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新密码失败');
		}
		return $ret;
	}
	
	//内部实现==============================================================
	
	/**
	 * 验证成员是否创建过账号
	 * @param unknown_type $user_id	成员ID
	 */
	private function _check_created($user_id) {
		$fields = '*';
		$cond = array(
			'user_id=' => $user_id
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret) {
			throw new SCException('您已创建过账号');
		}
	}
	
	/**
	 * 验证账号是否被使用
	 * @param unknown_type $acct	要创建的账号
	 */
	private function _check_exists($acct) {
		$fields = '*';
		$cond = array(
			'acct=' => $acct
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret) {
			throw new SCException('该账号已被使用，请重新设置');
		}
	}
	
}
//end