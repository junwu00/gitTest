<?php

/**
 * 审批知会配置
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_notify {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_notify';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/** 未读 1*/
	private static $StateReading = 1;
	/** 已读 2*/
	private static $StateRoad = 2;
	
	
	/**
	 * 根据ID保存审批表单
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function save_notify($formsetinst,$send_id,$send_name,$notify_id,$notify_name,$com_id) {
		$data = array(
			'formsetinst_id' => $formsetinst["id"],
			'formsetinst_name' => $formsetinst["formsetinst_name"],
			'notify_time' => time(),
			'notify_id' => $notify_id,
			'notify_name' => $notify_name,
			'send_id' => $send_id,
			'send_name' => $send_name,
			'info_state' => self::$StateOn,
			'com_id' => $com_id,
			'state' =>self::$StateReading
		);
		
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存知会信息失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 更新知会状态 new
	 * @param int $notify_id id
	 * @param int $com_id 公司id
	 * @param int $user_id 当前用户id
	 * @throws SCException
	 */
	public function update_notify_state($notify_id,$com_id,$user_id){
		$data = array(
			'state' => self::$StateRoad
		);
		$cond = array(
			'id='=>$notify_id,
			'com_id=' => $com_id,
			'notify_id=' => $user_id,
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新知会状态失败');
		}
		return $ret;
	}
	
	
	
	/**
	 * 获取我知会的列表
	 */
	public function get_do_list($condition = Array(),$com_id,$user_id,$is_finish=0,$is_proc_type=0,$page=0,$page_size=0,$fields='mpn.*,su.pic_url,mpfm.form_name',$order_by=" order by mpn.state,mpn.notify_time desc ",$with_count = FALSE){
		
		$sql = "select ".$fields." from mv_proc_formsetinst mpf,mv_proc_notify mpn,sc_user su,mv_proc_form_model mpfm
		where mpn.formsetinst_id = mpf.id and mpn.send_id = su.id and mpfm.id = mpf.form_id 
		and mpn.notify_id = ".$user_id." and mpn.com_id = ".$com_id." and mpn.info_state =".self::$StateOn." and mpf.is_other_proc=".$is_proc_type;
		
		foreach ($condition as $key => $val) {
			if($key=="formsetinst_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpn.formsetinst_name like '%".$val."%'";
				continue;
			}

			if($key=="s_notify_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpn.notify_time>=".$val;
				continue;
			}
			if($key=="e_notify_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpn.notify_time<".$val;
				continue;
			}
			
			if($key=="state"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpn.state=".$val;
				continue;
			}
			
		}
		
		if($is_finish==0){//未阅读
			$state = " and mpn.state=".self::$StateReading;
		}
		else{//已阅读
			$state = " and mpn.state=".self::$StateRoad;
		}
		
		$sql = $sql.$state;
		
		$limit_sql = $sql.$order_by;
		
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
		
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
		}
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 根据知会id获取知会信息new
	 * @param int $notify_id
	 * @param int $com_id
	 * @param int $user_id
	 */
	public function get_notify_by_id($notify_id,$com_id,$user_id){
		$notifysql = "select mpn.*,mpf.form_vals,mpf.creater_id,mpf.creater_name,mpf.create_time,mpf.state formsetinst_state,su.pic_url,mpfm.form_name,mpfm.form_describe 
		 from mv_proc_formsetinst mpf,mv_proc_notify mpn,sc_user su,mv_proc_form_model mpfm
		 where mpfm.id = mpf.form_id and mpn.formsetinst_id = mpf.id and mpf.creater_id = su.id 
		 and mpn.id = ".$notify_id." and mpn.com_id = ".$com_id." and mpn.notify_id = ".$user_id;
		
		$formsetinst = g('db') -> select_one($notifysql);
		
		return $formsetinst;
	}
	
	/**
	 * 
	 * 根据流程实例id删除工作项
	 * @param unknown_type $workitemid
	 * @param unknown_type $com_id
	 * @param unknown_type $work_item
	 * @throws SCException
	 */
	public function delete_notify($formsetinst_id,$com_id,$user_id){
		$cond = array(
			'formsetinst_id=' => $formsetinst_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_user_id' => $user_id
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('删除知会信息失败');
		}
		return $ret;
	}
	
	/**
	 * 根据实例id查找知会信息
	 * @param int $formsetinst_id 实例id
	 * @param int $com_id 企业id
	 */
	public function get_notify_by_formset_id($formsetinst_id,$com_id){
		$sql = "select mpn.*,su.pic_url from mv_proc_notify mpn,sc_user su
		where mpn.notify_id = su.id 
		and mpn.com_id = ".$com_id." and mpn.info_state =".self::$StateOn." and mpn.formsetinst_id =".$formsetinst_id." ORDER BY notify_time";
		$ret = g('db') -> select($sql);
		return $ret?$ret:array();
	}
	
	//---------------------------------------内部实现

}

// end of file