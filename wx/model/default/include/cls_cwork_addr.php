<?php
/**
 * 考勤地点
 * @author yangpz
 * @date 2014-11-11
 */
class cls_cwork_addr {
	/** 对应库表名称 */
	private static $Table = 'cwork_addr';
	
	/** 禁用 */
	private static $StateOff = 0;
	/** 开启 */
	private static $StateOn = 1;
	
	/**
	 * 根据ID获取考勤地点
	 * @param unknown_type $dept_id
	 * @param unknown_type $addr_id
	 * @param unknown_type $fileds
	 */
	public function get_by_id($com_id, $addr_id, $fileds='*') {
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $addr_id,
			'state=' => self::$StateOn,
		);
		$data = g('ndb') -> select(self::$Table, $fileds, $cond);
		return $data ? $data[0] : FALSE;
	}
	
	/**
	 * 分页获取考勤地点列表
	 * @param unknown_type $com_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public static function lists($com_id, $page=0, $page_size=0, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'state=' => self::$StateOn,
		);
		return g('ndb') -> select(self::$Table, $fields, $cond, $page, $page_size, '', ' order by create_time desc ');
	}
}

//end of file