<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_file extends cls_task_base{
	
	private static $APP_NAME = 'task';
	
		//文件类型对用文件扩展名
	private static $file_type = array(
		'image' => array('jpg', 'jpeg', 'gif', 'png', 'bmp'),
		'voice' => array('mp3', 'wma', 'wav', 'ram'),
		'video'	=> array('rm', 'rmvb', 'wmv', 'avi', 'mpg', 'mpeg', 'mp4'),
		'file'	=> array('txt', 'xml', 'pdf', 'zip', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx'),
	);
	
	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($file_list,$objid,$upload_table,$upload_id,$upload_name) {
		if(empty($file_list) || !is_array($file_list)){
			throw new SCException('附件格式错误');
		}
		$com_id = $_SESSION[SESSION_VISIT_COM_ID]; 
		
		$val = '';
		$upload_time = time();

		$in_state = true;

		foreach($file_list as $key => &$file){
			if(empty($file['file_name']) || empty($file['file_key']) || empty($file['file_url']) || empty($file['file_type'])){
				continue;
			}

			$tmp_data = array(
				'objectid' => $objid,
				'com_id' => $com_id,
				'upload_table' => $upload_table,
				'file_name' => $file['file_name'],
				'file_url' => $file['file_url'],
				'file_key' => $file['file_key'],
				'file_type' => $file['file_type'],
				'upload_id' => $upload_id,
				'upload_name' => $upload_name,
				'upload_time' => $upload_time,
				'info_state' => parent::$StateOn,
			);
			$tmp_id = g('ndb') -> insert(parent::$TableFile, $tmp_data);
			if (empty($tmp_id)) {
				$in_state = false;
				break;
			}

			//加入容量文件列表
			g('capacity') -> add_list_by_cache($com_id, $file['file_key'], self::$APP_NAME, parent::$TableFile, $tmp_id);
		}
		unset($file);
		
		if (!$in_state) {
			throw new SCException('保存任务附件信息失败');
		}
		
		return TRUE;
	}
	
	
	/**
	 * 删除任务附件信息
	 */
	public function delete($file_info){
		$data = array(
			'info_state' => parent::$StateOff,
			'info_time' =>time()
		);
		$cond = array(
			'id='=>$file_info["id"]
		); 
		$ret = g('ndb') -> update(parent::$TableFile, $cond, $data);
		if (!$ret) {
			throw new SCException('删除任务附件信息失败');
		}
		return $ret;
	}
	
	
	
	/**
	 * 更新任务附件taskid
	 */
	public function update_file_taskid($objectid,$file_ids){
		$file_ids_arr = explode(",",$file_ids);
		foreach ($file_ids_arr as $file_id)
		{
			if(!$file_id){
				continue;
			}
			log_write($file_id);
			$file_data = array(
				'info_state'=> parent::$StateOn,
				'objectid' => $objectid,
			);
			$cond = array(
				'id='=>$file_id,
				'upload_id=' => $_SESSION[SESSION_VISIT_USER_ID]
			);
			$ret_file = g('ndb') -> update_by_condition(parent::$TableFile, $cond, $file_data);
			if (!$ret_file) {
				throw new SCException('保存任务附件信息失败');
			}
		}
		//return $ret_file;
	}
	
	
	/**
	 * 根据任务id删除任务附件状态
	 */
	public function delete_file_taskid($objectid,$upload_table){
		$file_data = array(
			'info_state' => parent::$StateOff,
			'info_time' =>time()
		);
		$cond = array(
			'objectid='=>$objectid,
			'upload_table='=>$upload_table
		);
		$ret_file = g('ndb') -> update_by_condition(parent::$TableFile, $cond, $file_data);
		if (!$ret_file) {
			throw new SCException('删除任务附件信息失败');
		}
		return $ret_file;
	}
	
	
	//查询附件数目
	public function select_count_taskid($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT COUNT(1) as count FROM '.parent::$TableFile.' where objectid='.$taskid.' and info_state='.parent::$StateOn;
			return g('db')->select_one($sql);
		}
		else{
			return false;
		}
	}
	
/**
	 * 上传文件到云存储服务器
	 *
	 * @access public
	 * @param array $file $_FILES数组中的单个元素
	 * @return array
	 * @throws SCException
	 */
	public function upload_file(array $file) {
		$parts = pathinfo($file['name']);
		$extname = strtolower($parts['extension']);
		$filesize = $file['size'];
		
		if (empty($parts) or empty($extname) or empty($filesize)) {
			throw new SCException('非法的文件，可能是无明确扩展名或空白的文件！');
		}

		try {
			$file_key = '';
			foreach (self::$file_type as $key=>$arr_value){
				
				foreach ($arr_value as $value){
					if($value==$extname){
						$file_key = $key;
						break;
					}
				}
				if($file_key!=''){
					break;
				}
			}
			if(empty($file_key)){
				throw new SCException('非法的文件，该文件类型不允许上传！');
			}
			
			log_write($file_key."  ".$filesize);
			
			if($file_key=='image'){
				if($filesize>1024*1024*2){
					throw new SCException('文件过大，'.$extname.'类型的文件最大只能上传'.(2*1024).'kb,当前文件大小为'.ceil($filesize/1024).'kb！');
				}
			}
			else if($file_key=='voice'){
				if($filesize>1024*1024*2){
					throw new SCException('文件过大，'.$extname.'类型的文件最大只能上传'.(2*1024).'kb,当前文件大小为'.ceil($filesize/1024).'kb！');
				}
			}
			else if($file_key=='video'){
				if($filesize>1024*1024*10){
					throw new SCException('文件过大，'.$extname.'类型的文件最大只能上传'.(10*1024).'kb,当前文件大小为'.ceil($filesize/1024).'kb！');
				}
			}
			else{
				if($filesize>1024*1024*20){
					throw new SCException('文件过大，'.$extname.'类型的文件最大只能上传'.(20*1024).'kb,当前文件大小为'.ceil($filesize/1024).'kb！');
				}
			}
			
			$result = g('media') -> cloud_upload($file_key, $file);
		}catch(SCException $e) {
			throw new SCException($e -> getMessage());
		}
		
		$filesize = $result['filesize'];
		$data = array(
			'hash' => $result['hash'],
			'filepath' => $result['path'],
			'extname' =>$extname
		);

		$cache_key = $this -> get_cache_key($data['hash']);
		//保存60分钟
		g('redis') -> setex($cache_key, json_encode($data), 3600);

		$ret = array(
			'file_key' => $result['hash'],
			'show_url' => $result['path'],
			'extname' =>$extname,
			'file_name' =>$file['name']
		);
		return $ret;
	}
	
	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}
	
	/**
	 * 用户下载文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return TRUE
	 * @throws SCException
	 */
	public function download_file($com_id,$user_id,$file_id,$objid,$type,$key,$app_combo){//$cf_id,$com_id,$formsetinst_id,$app_combo,$app_name,$user_id) {
		try {
			$file_info = $this -> get_file_by_id($file_id,$objid,$type,$key);
		}catch(SCException $e) {
			throw $e;
		}

		$filename = $file_info['file_name'];
		
		$extname = $file_info['file_type'];
		
		log_write(json_encode($file_info));
		$media = g('media') -> get_media_id($com_id, $app_combo, $file_info['file_key'], $filename);
		log_write(json_encode($media));
		if (empty($media)) {
			throw new SCException('下载失败，文件不存在！');
		}
		
		log_write(json_encode($media));
		
		$file_key = '';
		foreach (self::$file_type as $key=>$arr_value){
			
			foreach ($arr_value as $value){
				if($value==$extname){
					$file_key = $key;
					break;
				}
			}
			
			if($file_key!=''){
				break;
			}
		}
		
		log_write($file_key." ".$media["media_id"]." ".self::$APP_NAME." ".$user_id);
		
		g('media') -> rename_download($file_key, $media["media_id"],self::$APP_NAME,array($user_id));
		
		
		
		return TRUE;
	}
	
	
	//查询附件列表
	public function select_list_taskid($objectid,$upload_table,$userid){
	//这里不需要再验证，当获取回报信息的附件时：$objectid为回报信息ID
	//	$is_role =  parent::get_user_role($objectid, $userid);
	//	$is_role = true;
	//	if($is_role==true){
			$sql = "SELECT * FROM ".parent::$TableFile." where objectid=".$objectid." and upload_table ='".$upload_table."' and info_state=".parent::$StateOn;
			return g('db')->select($sql);
//		}
//		else{
//			return false;
//		}
	}    
	
	public function get_file_by_id($file_id,$objid,$type,$key,$fields='*'){
		$cond = array(
			'id=' => $file_id,
			'objectid=' => $objid,
			'upload_table=' => $type,
			'info_state=' => 1,
			'file_key=' => $key,
		);
		$ret = g('ndb') -> select(self::$TableFile,$fields,$cond);
		return empty($ret) ? false : $ret[0];
	}
	
}

// end of file