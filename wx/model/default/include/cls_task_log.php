<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_log extends cls_task_base{

	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($log_info) {
		$data = array(
			'taskid' => $log_info['taskid'],
			'loger_id' => $log_info['loger_id'],
			'loger_name' => $log_info['loger_name'],
			'log_content' => $log_info['log_content'],
			'log_time' => time()
		);
		$ret = g('ndb') -> insert(parent::$TableLog, $data);
		if (!$ret) {
			throw new SCException('保存任务日志失败');
		}
		
		return $ret;
	}
	
	
	public function get_task_log($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT tl.*,su.pic_url pic_url FROM '.parent::$TableLog.' tl LEFT JOIN '.parent::$TableUser.' su on loger_id = su.id where taskid='.$taskid." order by log_time desc";
			return g('db')->select($sql);
		}
		else{
			return false;
		}
	}
	
	
	
}

// end of file