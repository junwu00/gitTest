<?php
/**
 * 素材管理类
 * 
 * @author yangpz
 * @date 2014-10-22
 *
 */
class cls_article {
	/** 对应的库表名称 */
	private static $Table = 'sc_article';
	
	/** 类型0：文本 */
	private static $TypeText 	= 0;
	/** 类型1：图片 */
	private static $TypeImage 	= 1;
	/** 类型2：音频 */
	private static $TypeVoice 	= 2;
	/** 类型3：视频 */
	private static $TypeVideo 	= 3;
	/** 类型4：单/多图文 */
	private static $TypeNews = 4;
	/** 类型5：文件 */
	private static $TypeFile 	= 5;
	
	/** 禁用 */
	private static $StateOff = 0;
	/** 启用 */
	private static $StateOn = 1;
	
	/**
	 * 保存素材
	 * @param unknown_type $dept_id		企业对应的部门ID
	 * @param unknown_type $app_name	APP名称
	 * @param unknown_type $type		素材类型ID
	 * @param unknown_type $content		素材内容(数组)
	 * @param unknown_type $media_path		素材文件路径
	 * @param unknown_type $media_name		素材文件名称
	 * @param unknown_type $media_ext		素材文件类型
	 * @param unknown_type $media_size		素材文件大小KB
	 */
	public function save($dept_id, $app_name, $type, $content, $media_path='', $media_name='', $media_ext='', $media_size='') {
		$data = array(
			'dept_id' => $dept_id,
			'app_name' => $app_name,
			'type' => $type,
			'content' => json_encode($content),
			'media_name' => $media_name,
			'media_path' => $media_path,
			'media_ext' => $media_ext,
			'media_size' => $media_size,
			'state' => self::$StateOn,
			'create_time' => time(),
			'last_use_time' => time(),
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存素材失败');
		}
		return $ret;
	}
	
	/**
	 * 更新素材
	 * @param unknown_type $dept_id
	 * @param unknown_type $app_name
	 * @param unknown_type $art_id
	 * @param unknown_type $type
	 * @param unknown_type $content
	 * @param unknown_type $media_path
	 * @param unknown_type $media_name
	 * @param unknown_type $media_ext
	 * @param unknown_type $media_size
	 */
	public function update($dept_id, $app_name, $art_id, $type, $content, $media_path='', $media_name='', $media_ext='', $media_size='') {
		$data = array(
			'dept_id' => $dept_id,
			'app_name' => $app_name,
			'type' => $type,
			'content' => json_encode($content),
			'state' => self::$StateOn,
			'last_use_time' => time(),
		);
		$media_name != '' && $data['media_name'] = $media_name;
		$media_path != '' && $data['media_path'] = $media_path;
		$media_ext != '' && $data['media_ext'] = $media_ext;
		$media_size != '' && $data['media_size'] = $media_size;
		$ret = g('ndb') -> update(self::$Table, $art_id, $data);
		if (!$ret) {
			throw new SCException('更新素材失败');
		}
	}
	
	/**
	 * 根据ID获取素材
	 * @param unknown_type $dept_id
	 * @param unknown_type $app_name
	 * @param unknown_type $type
	 * @param unknown_type $art_id
	 */
	public function get_by_id($dept_id, $app_name, $type, $art_id) {
		$fields = '*';
		$cond = array(
			'id=' => $art_id,
			'app_name=' => $app_name,
			'dept_id=' => $dept_id,
			'type=' => $type,
			'state=' => self::$StateOn,
		);
		$data = g('ndb') -> select(self::$Table, $fields, $cond);
		return $data ? $data[0] : FALSE;
	}
	
	/**
	 * 分页加载企业素材
	 * @param unknown_type $dept_id		企业对应的部门ID
	 * @param unknown_type $app_name	APP名称
	 * @param unknown_type $type		素材类型
	 * @param unknown_type $page		页数
	 * @param unknown_type $page_size	每次查询的数量上限
	 */
	public function get_art_list($dept_id, $app_name, $type, $page=1, $page_size=10) {
		$fields = array(
			'id',
			'content',
			'media_name',
			'media_ext',
			'media_size',
			'create_time',
		);
		$fields = implode(',', $fields);
		
		$cond = array(
			'dept_id=' => $dept_id,
			'app_name=' => $app_name,
			'type=' => $type,
			'state=' => self::$StateOn,
		);
		return g('ndb') -> select(self::$Table, $fields, $cond, $page, $page_size, '', 'order by create_time desc');
	}
	
	/**
	 * 更新最后一次使用的时间
	 * @param unknown_type $dept_id
	 * @param unknown_type $art_id
	 */
	public function update_last_use_time($dept_id, $art_id) {
		$data = array(
			'last_use_time' => time(),
		);
		$ret = g('ndb') -> update(self::$Table, $art_id, $data);
		if (!$ret) {
			throw new SCException('更新最后一次使用时间失败');
		}
	}
	
	/**
	 * 保存单/多图文
	 * @param unknown_type $dept_id
	 * @param unknown_type $app_name
	 * @param unknown_type $news_list	图文数组
	 */
	public function save_news($dept_id, $app_name, $news_list) { 
		$data = array();
		$url_list = '';
		foreach ($news_list as $news) {
			$url = uniqstr();
			$url_list .= $url . ' ';
			$item = array(
				"title" => $news['title'],
               	"description" => $news['description'],
               	"picurl" => $news['picurl'],
               	"picname" => $news['picname'],
				"ori_url" => $news['ori_url'],
				"author" => $news['author'],
				"content" => $news['content'],
				"show_img" => $news['show_img'],
               	"url" => SYSTEM_HTTP_DOMAIN.'public.php?a=article&url='.$url,
			);
			array_push($data, $item);
		}
		
		$content = array();
		$content['articles'] = $data;
		$content['count'] = count($data);
		
		try {
			return $this -> save($dept_id, $app_name, self::$TypeNews, $content, $url_list);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 更新图文消息
	 * @param unknown_type $dept_id
	 * @param unknown_type $app_name	
	 * @param unknown_type $art_id		原图文ID
	 * @param unknown_type $news_list	图文数组
	 */
	public function update_news($dept_id, $app_name, $art_id, $news_list) {
		$data = array();
		$url_list = '';
		foreach ($news_list as $news) {
			if (isset($news['url']) && !empty($news['url'])) {
				$url_arr = explode('&', $news['url']);
				$url = $url_arr[count($url_arr) - 1];
				$url = substr($url, 4);
			} else {
				$url = uniqstr();
			}
			
			$url_list .= $url . ' ';
			$item = array(
				"title" => $news['title'],
               	"description" => $news['description'],
               	"picurl" => $news['picurl'],
               	"picname" => $news['picname'],
				"ori_url" => $news['ori_url'],
				"author" => $news['author'],
				"content" => $news['content'],
				"show_img" => $news['show_img'],
               	"url" => SYSTEM_HTTP_DOMAIN.'public.php?a=article&url='.$url,
			);
			array_push($data, $item);
		}
		
		$content = array();
		$content['articles'] = $data;
		$content['count'] = count($data);
		
		try {
			$this -> update($dept_id, $app_name, $art_id, self::$TypeNews, $content, $url_list);
		
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 根据URL查找图文消息
	 * @param unknown_type $url		图文的URL
	 */
	public function get_news_by_url($url) {
		if (strlen($url) != 32)	
			return FALSE;	//非32位参数，不存在图文
		
		$url = mysql_escape_string($url);
		$cond = sprintf(' type=%d and state=%d and media_path like "%%%s%%" ', self::$TypeNews, self::$StateOn, $url);
		$sql = sprintf(' select * from %s where %s', self::$Table, $cond);
		$data = g('db') -> select($sql);
		
		if ($data && count($data) == 1) {
			$data = $data[0];
			$arts = $data['content'];
			$arts = json_decode($arts, TRUE);
			
			$arts = $arts['articles'];
			foreach ($arts as $art) {
				if (strpos($art['url'], $url)) {
					$art['create_time'] = $data['create_time'];
					return array('news' => $data, 'art' => $art);
				}
			}
		}
		return FALSE;
	}
	
	//--------------------------------------内部实现----------------------------------
	
}

// end of file