<?php
/**
 * js_sdk 签名类
 *
 * @author LiangJianMing
 * @create 2015-01-16
 */
class cls_js_sdk {
	//三尺科技的认证企业号公司id
	const auth_com_id = 0;
	//三尺科技的认证企业号crop_id
	const auth_corp_id = '';
	
	//是否显示日志
	private static $do_log = FALSE;

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 自定义字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$preffix = 'domain:'.SYSTEM_HTTP_DOMAIN.':class:'.__CLASS__.':';
		$key_str = md5($preffix.$str);
		return $key_str;
	}

	/**
	 * 获取企业号的jsapi_ticket
	 *
	 * @access public
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return string
	 */
	public function get_jsapi_ticket($com_id = 0) {
		empty($com_id) && $com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		if (empty($com_id)) {
			throw new SCException('非法访问');
		}
		
		$key_str = __FUNCTION__.':com_id:'.$com_id;
		$cache_key = $this -> get_cache_key($key_str);

		$ret = g('redis') -> get($cache_key);
		if (empty($ret)) {
			$access_token = g('atoken') -> get_access_token_by_com($com_id);

			$url = 'https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token='.$access_token;
			$result = cls_http::quick_get($url);

			if ($result['httpcode'] == 200) {
				$data = json_decode($result['content'], TRUE);
				isset($data['ticket']) && $ret = $data['ticket'];
				$ttl = isset($data['expires_in']) ? ($data['expires_in'] - 1000) : 3600;
				!empty($ret) && g('redis') -> set($cache_key, $ret, $ttl);
			
			} else {
				throw new SCException('获取jsapi ticket失败');	
			}
		}
		return $ret;
	}

	/**
	 * 获取js_sdk签名
	 *
	 * @access public
	 * @param array $data 加密所需的其他信息集合
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return mixed
	 */
	public function get_jsapi_sign(array $data, $com_id = 0) {
		$data['jsapi_ticket'] = $this -> get_jsapi_ticket($com_id);
		if (empty($data['jsapi_ticket'])) {
			//throw new SCException('获取jsapi ticket失败');
			return false;
		}
		self::$do_log && log_write('jsapi_ticket: '.$data['jsapi_ticket']);

		ksort($data);
		$str = '';
		foreach ($data as $key => $val) {
			$str .= $key.'='.$val.'&';
		}
		unset($key);
		unset($val);
		$str = substr($str, 0, -1);
		$sign = sha1($str);
		return $sign;
	}
	
	/**
	 * 在smarty中声明js-sdk相关的变量
	 *
	 * @access public
	 * @param string $app_id 指定使用该企业号的微信app_id
	 * @param int $com_id 公司id，指定使用该公司的accesson_token
     * @param string $url url链接
     * @param $is_return boolean 是否返回，则smarty传递
	 * @return array || void
	 */
	public function assign_jssdk_sign($app_id = '', $com_id = 0,$url='',$is_return=false) {
		$time = time();
		$nonce_str = get_rand_string(10);
		$this_url = $url ? $url : get_this_url();

		$params = array(
			'noncestr' => $nonce_str,
			'timestamp' => $time,
			'url' => $this_url,
		);
		$sign = $this -> get_jsapi_sign($params, $com_id);

		if (empty($app_id)) {
			if (!empty($com_id)) {
				$app_id = $this -> get_crop_id($com_id);
			}else {
				$app_id = $_SESSION[SESSION_VISIT_CORP_ID];
			}
		}
		if($is_return){
		    return [
                'app_id'=>$app_id,
                'time'=>$time,
                'nonce_str'=>$nonce_str,
                'this_url'=>$this_url,
                'sign'=>$sign,
            ];
        }
		g('smarty') -> assign('app_id', $app_id);
		g('smarty') -> assign('time', $time);
		g('smarty') -> assign('nonce_str', $nonce_str);
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('sign', $sign);
		
		if (self::$do_log) {
			log_write('app_id: '.$app_id);	
			log_write('time: '.$time);	
			log_write('nonce_str: '.$nonce_str);	
			log_write('this_url: '.$this_url);	
			log_write('sign: '.$sign);	
		}
	}

	/**
	 * 根据公司id获取crop_id
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @return mixed
	 */
	public function get_crop_id($com_id) {
		$key_str = __FUNCTION__ . '::' . $com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if ($ret !== FALSE) return $ret;

		$sql = 'SELECT corp_id FROM `sc_company` WHERE id=' . $com_id . ' LIMIT 1';
		$ret = g('db') -> select_first_val($sql);

		//缓存1小时
		g('redis') -> setex($cache_key, $ret, 3600);

		return $ret ? $ret : FALSE;
	}

	/**
	 * 使用三尺科技认证企业号生成js_sdk配置
	 *
	 * @access public
	 * @param $url
     * @param $is_return
     * @return boolean|array
	 */
	public function auth_assign_jssdk_sign($url='',$is_return=false) {
//        $CONF_FILES = include ((dirname(dirname(SYSTEM_ROOT))) . '/config/model/install/file.config.php');
//        $COM_CONF = json_decode(file_get_contents($CONF_FILES['company.config']), true);
//        $corp_id = $COM_CONF[0]['corp_id'];
        $corp_id = $_SESSION[SESSION_VISIT_CORP_ID];
        $com_id = 0;
        $com = g('company') -> get_by_corp_id($corp_id, 'id');
        !empty($com) && $com_id = $com['id'];
		return $this -> assign_jssdk_sign($corp_id, $com_id,$url,$is_return);
	}
}

/* End of this file */