<?php
/**
 * 领签项目统一调用
 * @author chenyihao
 * @date 2016-05-16
 *
 */
class cls_sign_api {

	/** 调用记录表 */
	private static $Record_Table = 'ser_sign_record';
	/** 充值记录表 */
	private static $Recharge_Table = 'ser_sign_recharge';
	/** 领签项目接口URL */
	private static $sign_domain = "";
	/** 签名 */
	private static $Signatures = 1;
	/** 印章 */
	private static $Seals = 2;

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	private $access_secret = "f3121007894ef5d061b56bfad8bacdda";

	private $com_id = 0;
	private $user_id = 0;

	public function __construct() {
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];

		self::$sign_domain = SIGN_URL_HOST;
	}

	/**
	 * 获取签名URL
	 * @return 
	 */
	public function get_sign_url($name, $redirect, $company_name="", $email="", $idcard_type="", $idcard="", $phone=""){
		try{

			$ret =$this -> check_privirage();

			$url = self::$sign_domain . '/index.php?model=sign&a=signatures';
			$data = array(
					'name' => $name,
					'redirect' => $redirect,
					'email' => $email,
					'phone' => $phone,
					'idcard' => $idcard,
					'idcard_type' => $idcard_type,
					'company_name' => $company_name,
				);
			$sign = $this -> sign_data($data);
			$url .= '&sign='.$sign;

			$ret = cls_http::post($url, $data);
			if(empty($ret['content'])){
				//请求失败，重试
				log_write('请求签名链接失败 ，重试');
				$ret = cls_http::post($url, $data);
			}

			$result = json_decode($ret['content'], true);
			if(empty($result) || $result['errcode'] != 0){
				throw new SCException($result['errmsg']);
			}
			$this -> insert_record($this -> user_id, $this -> com_id, $result['id'], self::$Signatures);

			$data = array();
			$data['id'] = $result['id'];
			$data['redirect'] = $result['redirect'];
			log_write('签名链接获取成功：'.$result['id'].'-'.$result['redirect']);
			return $data;
		}catch(SCException $e){
			log_write($e -> getMessage());
			throw $e;
		}
	}

	public function get_image($id){
		$record = $this -> select_one($id);
		if(empty($record)){
			throw new SCException('该图片不存在！');
		}
		if(empty($record['file_path'])){
			$this -> check_privirage(TRUE);
			$url = self::$sign_domain . '/index.php?model=sign&a=image';
			$data = array();
			$data['id'] = $id;
			$ret = cls_http::post($url, $data);
			$result = json_decode($ret['content'], TRUE);
			if($result['errcode'] != 0){
				throw new SCException($result['errmsg']);
			}

			$data = array();
			$data['file_path'] = $result['url'];
			$data['state'] = 1;
			$this -> update_record($id, $data);

			$record['file_path'] = $result['url'];
		}
		return $record['file_path'];
	}

	/**
	 * 检测是否有权限发起（检查次数）
	 * @param   $type true : 检查权限之后，并消耗一次
	 * @return 
	 */
	public function check_privirage($type = false){
		try{
			g('db') -> begin_trans();
			$for_update = $type ? " for update " : "";

			$sql = "select end_time, record_num, use_num from ".self::$Recharge_Table." where com_id = ".$this -> com_id." and info_state = ".self::$StateOn.$for_update;
			$ret = g('db') -> select($sql);
			if(empty($ret)) throw new SCException('未开通服务');
			$recharge = $ret[0];
			$has_time = false;
			$has_num = false;
			$recharge['end_time'] > time() && $has_time = TRUE;
			$recharge['record_num'] > $recharge['use_num'] && $has_num = TRUE;

			if(!$has_time && !$has_num){
				throw new SCException('电子签署次数已经用完了！');
			}

			if($type && $has_num && !$has_time){
				$sql = "update ".self::$Recharge_Table." set use_num = use_num+1 where com_id = ".$this -> com_id." and info_state = ".self::$StateOn;
				$ret = g('db') -> query($sql);
				if(!$ret) throw new SCException('发生未知错误，请联系客服！');
			}

			g('db') -> commit();
			return true;
		}catch(SCException $e){
			g('db') -> rollback();
			throw $e;
		}
	}


	/**
	 * 新增记录
	 * @param   $user_id
	 * @param   $com_id
	 * @param   $unique_code
	 * @param   $type
	 * @return 
	 */
	private function insert_record($user_id, $com_id, $unique_code, $type){
		$now = time();
		$data = array();
		$data['user_id'] = $user_id;
		$data['com_id'] = $com_id;
		$data['unique_code'] = $unique_code;
		$data['type'] = $type;
		$data['create_time'] = $now;
		$data['update_time'] = $now;
		$data['info_state'] = self::$StateOn;

		$ret = g('ndb') -> insert(self::$Record_Table, $data);
		return $ret;
	}

	/**
	 * 更新记录
	 * @param   $unique_code
	 * @param   $data
	 * @return 
	 */
	private function update_record($unique_code, $data){
		$cond = array();
		$cond['unique_code='] = $unique_code;
		$data['update_time'] = time();
		$ret = g('ndb') -> update_by_condition(self::$Record_Table, $cond, $data);

		return $ret;
	}

	/**
	 * 查找
	 * @param   $unique_code
	 * @return 
	 */
	private function select_one($unique_code, $fields='*'){
		$cond = array(
				'unique_code=' => $unique_code,
			);
		$ret = g('ndb') -> select(self::$Record_Table, $fields, $cond);
		return empty($ret) ? false : $ret[0];
	}

	/**
	 * 
	 * @param   $data
	 * @return 
	 */
	private function sign_data($data){
		$data_str = $this -> array2str($data);
		$sign = hash_hmac('sha1', $data_str, $this -> access_secret);
		return $sign;
	}

	/**
	 * 将数组转成字符串
	 * @param   $array
	 * @return 
	 */
	private function array2str($array){
		$tmp_array = array();
		foreach ($array as $key => $val) {
			array_push($tmp_array, $key.'='.$val);
		}unset($val);

		$str = implode('&', $tmp_array);
		return $str;
	}

}

// end of file