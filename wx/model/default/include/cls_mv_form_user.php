<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_form_user {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_form_user';
	
	private static $TableFormModel = 'mv_proc_form_model';
	
	/** 禁用 0*/
	private static $StateOff = 0; 
	/** 启用 1*/
	private static $StateOn = 1;
	
	/**
	 * 获取用户常用表单new
	 * @param int $user_id 用户id
	 * @param i $com_id 企业id
	 */
	public function get_common_form($user_id,$com_id){
		$form_user_sql = "select uf.*,fm.version,fm.form_name,fm.form_describe,fm.scope from ".self::$Table." uf ,".self::$TableFormModel." fm 
		where uf.form_id = fm.id 
		and uf.info_state=".self::$StateOn." and uf.com_id={$com_id} and uf.user_id={$user_id} and fm.info_state=".self::$StateOn." and fm.com_id={$com_id} order by uf.user_time desc";
		$form_user = g('db') -> select($form_user_sql);
		return $form_user;
	}
	
	/**
	 * 插入常用表单数据new
	 * @param int $form_id 表单id
	 * @param int $form_history_id 历史版本id
	 * @param int $user_id 用户id
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	public function add_common_form($form_id,$form_history_id,$user_id,$com_id){
		$data = array(
			'form_id' => $form_id,
			'com_id' => $com_id,
			'user_id' => $user_id,
			'form_history_id' =>$form_history_id,
			'user_time' =>time(),
			'info_state' =>self::$StateOn
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存常用表单失败');
		}
		return $ret;
	}
	
	/**
	 * 根据历史版本更新常用数据
	 * @param int $form_id 新的表单id
	 * @param int $form_history_id 历史版本id
	 * @param int $user_id 用户id
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	public function update_common_form_id($form_id,$form_history_id,$user_id,$com_id){
		$data = array(
			'form_id' => $form_id
		);
		$cond = array(
			'user_id='=>$user_id,
			'com_id=' => $com_id,
			'form_history_id=' =>$form_history_id
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新常用表单失败');
		}
		return $ret;	
	}
	
	/**
	 * 根据历史版本更新常用数据的使用时间new
	 * @param int $form_history_id 历史版本id
	 * @param int $user_id 用户id
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	public function update_common_form_time($form_history_id,$user_id,$com_id){
		$data = array(
			'user_time' => time()
		);
		$cond = array(
			'user_id='=>$user_id,
			'com_id=' => $com_id,
			'form_history_id=' =>$form_history_id
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新常用表单失败');
		}
		return $ret;	
	}
	
	/**
	 * 删除信息（更新字段状态）
	 * @param int $form_history_id 表单历史版本
	 * @param int $user_id 用户id
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	public function del_common_form($form_history_id,$user_id,$com_id){
		$data = array(
			'info_state' => self::$StateOff,
			'info_time' =>time(),
		);
		$cond = array(
			'user_id='=>$user_id,
			'com_id=' => $com_id,
			'form_history_id=' =>$form_history_id
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新常用表单失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 判断是否存在同一个表单
	 * @param int $form_history_id 历史版本id
	 * @param int $user_id 用户id
	 * @param int $com_id 企业id
	 */
	public function is_exist_user_form($form_history_id,$user_id,$com_id){
		$form_user_sql = "select * from ".self::$Table." where com_id={$com_id} and user_id={$user_id} and form_history_id={$form_history_id} and info_state=".self::$StateOn;
		$form_user = g('db') -> select($form_user_sql);
		if(empty($form_user)){
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
}

// end of file