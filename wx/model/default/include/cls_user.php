<?php
/**
 * 企业成员
 * 
 * @author yangpz
 * @date 2014-10-22
 *
 */
class cls_user {
	
	private static $Table = 'sc_user';
	/** 已删除 0 */
	private static $StateDeleted = 0;
	/** 已关注 1 */
	private static $StateSubscribe = 1;
	/** 已冻结 2 */
	private static $StateFrozen = 2;
	/** 未关注 4 */
	private static $StateUnsubscribe = 4;
	/** 页面大小 */
	private static $page_size = 5;

	/**
	 * 判断是否为企业用户
	 * @param unknown_type $wx_acct	企业号账号
	 * @return 错误码，用户ID
	 */
	public function is_user($wx_acct) {
		$result = $this -> get_qy_acct($wx_acct);
		if ($result[0] != cls_resp::$OK) {
			return $result;
		}
		
		///查找数据库记录
		$cond = array(
			'id=' => $result[1]['id'],
			'acct=' => $result[1]['acct'],
			'state=' => self::$StateSubscribe,
		);
		$is_exists = g('ndb') -> record_exists(self::$Table, $cond);
		if (!$is_exists) {
			return array(cls_resp::$UserNotFound, NULL);
		}
		$data = array(
			'id' => $result[1]['id'],
		);
		return array(cls_resp::$OK, $data);
	}
	
	/**
	 * 根据账号获取员工信息，默认取状态为：非删除的记录
	 * @param unknown_type $dept_id
	 * @param unknown_type $wx_acct
	 * @param unknown_type $fields
	 * @param unknown_type $state
	 * @throws SCException
	 */
	public function get_by_acct($dept_id, $wx_acct, $fields='*', $state=NULL, $throw_exp=TRUE) {
		///查找数据库记录
		$cond = array(
			'root_id=' => $dept_id,
			'acct=' => $wx_acct,
		);
		if (is_null($state)) {
			$cond['state!='] = self::$StateDeleted;	
		} else {
			$cond['state='] = $state;	
		}
		$users = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($throw_exp && !$users) {
			throw new SCException('未找到该员工');
		}
		if (count($users) > 1) {
			throw new SCException('员工账号重复：acct='.$wx_acct);
		}
		
		return $users ? $users[0] : FALSE;
	}

	/**
	 * 根据手机号获取员工信息，默认取状态为：非删除的记录
	 * @param unknown_type $mobile
	 * @param unknown_type $fields
	 * @param unknown_type $state
	 * @throws SCException
	 */
	public function getByMobile($mobile, $fields='*', $state=NULL) {
		///查找数据库记录
		$cond = array(
			'mobile=' => $mobile,
		);
		if (is_null($state)) {
			$cond['state!='] = self::$StateDeleted;	
		} else {
			$cond['state='] = $state;	
		}
		$users = g('ndb') -> select(self::$Table, $fields, $cond);
		!is_array($users) && $users = array();
		
		return $users;
	}
	
	/**
	 * 判断用户是否存在（企业内 微信号/手机号/邮箱必须都唯一）
	 * @param unknown_type $root_id		企业根部门ID
	 * @param unknown_type $weixin_id
	 * @param unknown_type $mobile
	 * @param unknown_type $email
	 * @return 存在：TRUE;不存在: FALSE
	 */
	public function is_exists($root_id, $weixin_id, $mobile, $email) {
		$sql = sprintf('select count(1) from %s where root_id=%s and  ( 
			(weixin_id is not null and weixin_id="%s") or 
			(mobile is not null and mobile="%s") or 
			(email is not null and email="%s") )', self::$Table, $root_id, $weixin_id, $mobile, $email);

		$count = g('db') -> select_first_val($sql);
		return $count == 0 ? FALSE : TRUE;
	}
	
	/**
	 * 是否有完整的员工信息
	 * @param unknown_type $user_info
	 * @param unknown_type $throw_exp	默认抛出异常
	 */
	public function is_info_complete($user_info, $throw_exp=TRUE) {
		if ((is_null($user_info['weixin_id']) || empty($user_info['weixin_id'])) &&
			(is_null($user_info['mobile']) || empty($user_info['mobile'])) &&
			(is_null($user_info['email']) || empty($user_info['email']))) {
			
			if ($throw_exp)	throw new SCException('员工信息不完整');
			
			return FALSE;
		}
		return TRUE;
	}
	
	
	/**
	 * 为用户设置主部门
	 * @return 成功：TRUE，失败: FALSE
	 */
	public function set_first_dept($root_id, $id,$deptid) {
		$user = $this -> get_user_by_id($root_id, $id);
		
		if(!$user) {
			throw new SCException('员工找不到');
		}
		
		$condition = array(
		   "id=" => $id,
		   "root_id=" => $root_id
		);
		$dept_list = explode(',', str_replace(array('[',']','"'),'',$user['dept_list']));;
		in_array($deptid,$dept_list) && array_splice($dept_list,array_search($deptid,$dept_list));
		$new_dept_list = array_merge(array($deptid),$dept_list);
		$user_info = array(
			"dept_list"=>json_encode($new_dept_list)
		);
		$result = g('ndb') -> update_by_condition(self::$Table, $condition, $user_info);
		if (!$result) {
			throw new SCException('更新员工主部门失败');
		}
	}
	/**
	 * 冻结用户
	 * @return 成功：TRUE，失败: FALSE
	 */
	public function frozen($root_id, $id) {
		$user = $this -> get_user_by_id($root_id, $id);
	
		if(!$user) {
			throw new SCException('员工找不到');
		}
		   
		$condition = array(
		   "id=" => $id,
		   "root_id=" => $root_id
		);
	  	$user_info = array(
	      	"state"=>$user['state']==2?4:2
	   	);
		$result = g('ndb') -> update_by_condition(self::$Table, $condition, $user_info);
		if (!$result) {
			throw new SCException('冻结用户失败');
		}
	}
	
	/**
	 * 查找用户
	 * @return 
	 */
	public function search($root_id, $arr="", $order="") {
		$root_id = intval($root_id);
		
	    $str = " where ";
	    $count = 1;
	    if(!empty($arr) && is_array($arr)){
	    	$str .= '(';
			foreach ($arr as $k => $a){
				$k = mysql_escape_string($k);
				$a = mysql_escape_string($a);
				$str .= ''.$k.' like "%'.$a.'%" ';
				if ($count < count($arr)){
					$str = $str.' or ';
				}
				$count++;
			}
			unset($a);
			$str .= ') and';
	    }
		
		$sql = 'select * from '.self::$Table.' '.$str.' state!='.self::$StateDeleted.' and root_id = '.$root_id.$order;
		$user = g('db') -> select($sql);
		
		return $user ? $user : false;
	}
	
	/**
	 * 根据id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_ids($ids, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $ids, 'id', $fields);
	}
	
	/**
	 * 根据员工ID获取员工信息(默认不包含已删除员工)
	 * @param unknown_type $id
	 */
	public function get_by_id($id, $fields='*', $all_state=FALSE) {
		$cond = array(
			'id=' => $id,
		);
		!$all_state && $cond['state!='] = self::$StateDeleted;
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 修改用户状态
	 * @param unknown_type $wx_acct	用户微信账号
	 */
	public function change_state($root_id, $wx_acct, $state) {
		if ($state == self::$StateSubscribe) {				//关注事件
			$users = $this -> get_by_acct($root_id, $wx_acct, '*', self::$StateUnsubscribe);
		} else if ($state == self::$StateUnsubscribe) {		//取消关注事件
			$users = $this -> get_by_acct($root_id, $wx_acct, '*', self::$StateSubscribe);
		} else {
			return;
		}
		if (!$users) {
			log_write('找不到员工：root_id='.$root_id.', acct='.$wx_acct.', state='.$state);
			throw new SCException('未找到员工');
		}
		
		$data = array(
			'state' => $state,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update(self::$Table, $users['id'], $data);
		if (!$ret) {
			throw new SCException('修改用户状态失败');
		}
		return $ret;
	}
	
	/**
	 * 更新员工头像\微信号
	 * @param unknown_type $access_token
	 * @param unknown_type $root_id
	 * @param unknown_type $acct
	 */
	public function set_pic_url($access_token, $root_id, $acct) {
		try {
			$user = g('wxqy') -> get_user($access_token, $acct);
			if(!empty($user) && isset($user['weixinid'])){
				$cond = array(
					'root_id=' => $root_id,
					'acct=' => $acct,
				);
				$data = array(
					'pic_url' => isset($user['avatar']) ? $user['avatar'] : '',
					'weixin_id' => $user['weixinid'],
					'update_time' => time(),
				);
				$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
				if (!$ret) {
					throw new SCException('更新员工头像失败');
				}
				return $ret;
			}else{
				return true;
			}
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 更新
	 * @param unknown_type $condition  array 条件
	 * @param unknown_type $data	更新哪些字段
	 */
	public function update($condition, $user_info,$transfer=TRUE) {
		$acct = isset($user_info['acct']) ? $user_info['acct'] : FALSE;
		try {
			$this -> is_info_complete($user_info);
			$this -> check_user($condition['id='], $user_info['weixin_id'], $user_info['email'], $user_info['mobile'],$acct);
			
			g('db') -> begin_trans();
		    $result = g('ndb') -> update_by_condition(self::$Table, $condition, $user_info);
		    if(!$result)throw new SCException('修改用户信息失败');
	        $this -> transfer_update($condition['id='],$user_info);
	       	g('db')->commit();
	       
		} catch (SCException $e) {
			g('db')->rollback();
			throw $e;
		}
	}
	
	/**
	 * 根据部门ID获取用户
	 * @param unknown_type $root_id	企业根部门
	 * @param unknown_type $dept_id
	 */
	public function get_by_dept($root_id, $dept_id, $fields='*') {
		$root_id = intval($root_id);
		$dept_id = intval($dept_id);
		$fields = mysql_escape_string($fields);
		
		$cond = sprintf(" root_id=%d and dept_list like '%%\"%d\"%%'", $root_id, $dept_id);	//无状态限制
		$sql = sprintf(" select %s from %s where %s ", $fields, self::$Table, $cond);
		$data = g('db') -> select($sql);
		return $data;
	}
	
	/**
	 * 获取微信企业账号列表
	 * @param unknown_type $user_list	数据库中保存的员工数据
	 */
	public function get_wx_acct_ids($user_list) {
		$result = array();
		foreach ($user_list as $user) {
			array_push($result, $user['acct']);
		}
		return $result;
	}

     /** 
	 * 根据root_ID返回该下所有员工
	 * @param unknown_type 
	 * @return 
	 */
     public function get_user_by_root($root_id) {
     	$type = get_var_value("type");
     	
     	$page = get_var_value("page");
        if (!$page || $page == NULL) {
			$page = 1;
		}
		
     	$fields = '*';
     	if(!$type){
     	   $cond = array(
			'root_id =' => $root_id           
		   );
     	}else{
     	    $cond = array(
			'root_id =' => $root_id,
            'state =' => $type
		    );
     	}
		
		$result  = g("ndb")->select(self::$Table,$fields,$cond,$page,self::$page_size,"","",true);
		$result['page_info'] = $this->get_page_info($page, self::$page_size, $result['count']);
		return $result ? $result: FALSE; 
	}
	
      /**
	 * 根据ID删除员工信息
	 * @param unknown_type 
	 * @return 
	 */
     public  function delete_user_by_id($root_id,$id) {
     	$condition = array(
     	    "id=" => $id,
     	    "root_id=" => $root_id
     	);
     	
     	try {
	     	g('db') -> begin_trans();
	     	//先删除微信端，再删除本地
	     	$this -> transfer_delete($id);
            g("ndb") -> delete(self::$Table,$condition);
			g('db')->commit();
			
     	} catch (SCException $e) {
	        g('db')->rollback();
     		throw $e;
     	}
	}
	
     /**
	 * 根据ID返回员工信息
	 * @param unknown_type 
	 * @return 
	 */
     public  function get_user_by_id($root_id,$id,$fields='*') {
        $cond = array(
			'root_id =' => $root_id,
     	    'id ='=>$id           
		   );
		$result = g("ndb")->select(self::$Table,$fields,$cond);
		return $result ? $result[0]: FALSE; 
	}
	
     /**
	 * 根据条件返回员工信息
	 * @param unknown_type 
	 * @return 
	 */
     public function get_user_by_condition($condition=array()) {
     	$fields = '*';
		$result  = g("ndb") -> select(self::$Table,$fields,$condition);
		return $result ? $result[0]: FALSE; 
	}
	
	/**
	 * 根据部门ID返回该部门下所有员工（包括子部门）
	 * @param unknown_type $wx_acct
	 * @return 账号id和账号
	 */
     public function get_all_user_by_deptid($root_id,$deptid,$type) {
     	$root_id = intval($root_id);
     	$deptid = intval($deptid);
     	$type = intval($type);
     	
        $page = get_var_value("page");
        if (!$page || $page == NULL) {
			$page = 1;
		}
     	$depts = g("dept")-> get_all_child_dept($deptid); //获得所有子部门的ID
     	
     	$sql = 'select * from sc_user u where root_id ='.$root_id.' and state'.($type?'='.$type:'>0').' and  (id = 0 ';  //id = 0，不可能条件
     	
     	// 使用正则表达式
        $rexp_str = '';
        foreach ($depts as $val) {
            $rexp_str .= $val . '|';
        }

        if ($rexp_str !== '') {
            $rexp_str = substr($rexp_str, 0, -1);
            $sql .= ' OR u.dept_list REGEXP \'"({$rexp_str})"\'';
        }
        
     	$sql .= ')';
     	$limit_sql = $sql;
		if ($page > 0 && self::$page_size > 0) {
			$begin = ($page - 1) * self::$page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval(self::$page_size);
			$limit_sql .= $limit;
		}
		$result = g('db') -> select($limit_sql);

		$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
		$count = g('db')->select_one($sql);
     
     	
     	$page_info = $this -> get_page_info($page,self::$page_size,$count['count']);
     	$return =array();
     	$return['page_info']=$page_info;
        $return['data'] = $result;

        return $return; 
	}
	
	/**
	 * 根据部门ID返回该部门下所有员工（不包括子部门）
	 * @param unknown_type $wx_acct
	 * @return 账号id和账号
	 */
     public  function get_user_by_deptid($root_id,$deptid,$type) {
     	$root_id = intval($root_id);
     	$deptid = intval($deptid);
     	$type = intval($type);
     	
        $page = get_var_value("page");
        if (!$page || $page == NULL) {
			$page = 0;
		}
     	
     	$sql = 'select * from sc_user u where root_id ='.$root_id.' and state'.($type?'='.$type:'>0').' and u.dept_list like "%\"'.$deptid.'\"%" ';  //id = 0，不可能条件
     	
     	$limit_sql = $sql;
		if ($page > 0 && self::$page_size > 0) {
			$begin = ($page) * self::$page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval(self::$page_size);
			$limit_sql .= $limit;
		}
		$result = g('db') -> select($limit_sql);
		if($page!=0){
	     	$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('db')->select_one($sql);
	     	
	     	$page_info = $this -> get_page_info($page+1,self::$page_size,$count['count']);
	     	$return =array();
	     	$return['page_info']=$page_info;
		}
        $return['data'] = $result;

        return $return; 
	}
	
	/**
	 * 获取审批人列表
	 * @param unknown_type $com_id			企业ID
	 * @param unknown_type $send_user		提交申请的人的id
	 * @param unknown_type $recive_role		审批人的角色：0直接上级 1间接上级 2指定人员
	 * @param unknown_type $recive_user_id	指定审批人的ID
	 * 
	 * @return 审批人列表，数组格式
	 */
	public function get_approval_user_list($com_id, $send_user_id, $recive_role, $recive_user_id) {
		$com_id = intval($com_id);
		$send_user_id = intval($send_user_id);
		$recive_role = intval($recive_role);
		$recive_user_id = intval($recive_user_id);
		
		//1.指定人员
		if ($recive_role == 2) {
			$user = $this -> get_by_id($recive_user_id);
			if (!$user) {
				throw new SCException('找不到指定的审批人员');
			}
			return array($user['id']);
		}
		
		//2.直接上级
		try {
			$user = $this -> get_by_id($send_user_id);
			$dept = json_decode($user['dept_list'], TRUE);
			$dept_id = $dept[0];
			
			$dept = g('dept') -> get_dept_by_id($dept_id);
			$boss = NULL;
			$boss_list = $this -> get_boss_list($dept, $boss);
			
			$role = 0;		//当前人员为普通员工
			if ($send_user_id == $boss) {
				$role = 1;	//第一负责人
			} else if (in_array($send_user_id, $boss_list)) {
				$role = 2;	//第二负责人
			}
			
			if ($recive_role == 0 && $role == 0) {	//普通员工直接上级: 当前部门负责人列表
				return $boss_list;
			}

			if ($dept['p_id'] == 0) {				//当前为顶级部门，负责人直接上级为: 当前（顶级）部门负责人
				return $boss_list;
			}
			$p_dept = g('dept') -> get_dept_by_id($dept['p_id']);
			if ($recive_role == 0 && $role == 1) {	//第一负责人直接上级: 上级部门负责人列表
				return $this -> get_boss_list($p_dept);
			}
			if ($recive_role == 0 && $role == 2) {	//第二负责人直接上级: 上级部门负责人列表
				return $this -> get_boss_list($p_dept);
			}
			
			//3.间接上级
			if ($recive_role == 1 && $role == 0) {	//普通员工间接上级: 上级部门负责人列表
				return $this -> get_boss_list($p_dept);
			}
			
			if ($p_dept['p_id'] == 0) {				//上级部门为顶级部门，负责人间接上级为：上级（顶级）部门负责人
				return $this -> get_boss_list($p_dept);
			}
			$pp_dept = g('dept') -> get_dept_by_id($p_dept['p_id']);
			if ($recive_role == 1 && $role == 1) {	//第一负责人间接上级: 上上级部门负责人列表
				return $this -> get_boss_list($pp_dept);
			}
			if ($recive_role == 1 && $role == 2) {	//第二负责人间接上级: 上上级部门负责人列表
				return $this -> get_boss_list($pp_dept);
			}
			
		} catch (SCException $e) {
			throw $e;
		}
		
	}
	
	/*
	 * 根据userid获取上级领导
	 */
	public function get_leader_by_userid($send_user_id){
		$user = $this -> get_by_id($send_user_id);
		$dept = json_decode($user['dept_list'], TRUE);
		$dept_id = $dept[0];
		
		$dept = g('dept') -> get_dept_by_id($dept_id);
		$boss = NULL;
		$boss_list = $this -> get_boss_list($dept, $boss);
		
		$role = 0;		//当前人员为普通员工
		if ($send_user_id == $boss) {
			$role = 1;	//第一负责人
		} else if (in_array($send_user_id, $boss_list)) {
			$role = 2;	//第二负责人
		}
		
		if ( $role == 0) {	//普通员工直接上级: 当前部门负责人列表
			return $boss_list;
		}

		if ($dept['p_id'] == 0) {				//当前为顶级部门，负责人直接上级为: 当前（顶级）部门负责人
			return $boss_list;
		}
		$p_dept = g('dept') -> get_dept_by_id($dept['p_id']);
		if ($role == 1) {	//第一负责人直接上级: 上级部门负责人列表
			return $this -> get_boss_list($p_dept);
		}
		if ($role == 2) {	//第二负责人直接上级: 第一负责人
			return array($boss);
		}
	}
	
	/**
	 * 根据部门ID，员工姓名分页查找员工数据
	 * @param unknown_type $root_id	企业根部门ID
	 * @param unknown_type $dept_id	部门ID
	 * @param unknown_type $offset	分页位置
	 * @param unknown_type $limit	分页大小
	 * @param unknown_type $name	员工姓名/拼音
	 */
	public function page_search_by_dept($root_id, $dept_id, $offset, $limit, $name, $boss=NULL, array $second_boss=array()) {
		$root_id = intval($root_id);
		$dept_id = intval($dept_id);
		$offset = intval($offset);
		$limit = intval($limit);
		$name = mysql_escape_string($name);
		
		$offset = $offset < 0 ? 0 : $offset;
		$limit = $limit > 100 ? 100 : $limit;
		
		$dept_sql = '';
		if ($dept_id != 0) {	//全企业范围内查找
			$dept_sql = " AND dept_tree like '%%\"{$dept_id}\"%%' ";
		}
		$name_sql = '';
		if (!empty($name)) {
			$name_sql = "AND (name like '%%{$name}%%' OR all_py like '%%{$name}%%' OR first_py like '%%{$name}%%')";
		}
		
		$table = self::$Table;
		$total_sql = <<<EOF
			SELECT count(1) as cnt
			FROM {$table}
			WHERE root_id={$root_id} AND state !=0 {$dept_sql} {$name_sql}
EOF;
		$total = g('db') -> select($total_sql);
		$total = $total[0]['cnt'];
		if ($total == 0) {
			return array('count' => 0, 'data' => array());
		}
		
		$boss_ids = array();
        !empty($second_boss) && $boss_ids = array_merge($boss_ids, $second_boss);
		!empty($boss) && $boss_ids = array_merge($boss_ids, array($boss));
		$order = '';
		if(!empty($boss_ids)){
     		$order = ' FIELD(id, '.implode(',', $boss_ids).') desc,  ';
     	}

		$sql = <<<EOF
			SELECT id, name, pic_url, first_py
			FROM {$table}
			WHERE root_id={$root_id} AND state !=0 {$dept_sql} {$name_sql}
			ORDER BY {$order} all_py
			LIMIT {$offset}, {$limit}
EOF;
		$data = g('db') -> select($sql);
		$data = $data ? $data : array();
		log_write($sql);
	
		if(!empty($boss_ids)){
			foreach ($data as &$user) {
				if($user['id'] == $boss){
					$user['boss'] = true;
				} else if(in_array($user['id'], $second_boss)){
					$user['second_boss'] = true;	
				}
			}
		}
		
		return array('count' => $total, 'data' => $data);
	}
	
	//--------------------------------------内部实现-------------------------------
	
	/**
	 * 获取指定部门的负责人
	 * @param unknown_type $dept		部门实例
	 * 
	 * @return 第二负责人列表，数组格式
	 */
	private function get_boss_list($dept, &$boss=NULL) {
		if (!$dept) {
			throw new SCException('找不到主部门信息: '.$dept['name']);
		}

		$boss = (is_null($dept['boss']) || $dept['boss'] == '0') ? NULL : $dept['boss'];		//第一负责人
		$sboss = is_null($dept['second_boss']) ? array() : json_decode($dept['second_boss'], TRUE);	//第二负责人
		if (is_null($boss) && count($sboss) == 0) {
			return FALSE;
		}
		
		!is_null($boss) && array_unshift($sboss, $boss);
		return $sboss;
	}
	
	/**
	 * 获得分页信息
	 */
	private function get_page_info($page,$page_size, $total) {
		$page_count = ($total % $page_size == 0) ? ($total / $page_size) : ($total / $page_size + 1);	//总页数
		if (!$page || $page == NULL) {
			$page = 1;
		}
		$pages = array();	//页码，即1，2，3...
		for ($i=1; $i<=$page_count; $i++) {
			array_push($pages, $i);
		}
		return array("curr_page"=>$page,"pages"=>$pages);
	}
	
	/**
	 * 检查weixinid，Email，mobile，只取状态为：非删除的记录
	 */
	private function check_user($id, $weixinid, $email, $mobile, $acct=false, $root_id =0){
		if ($id == 0) {	//新增员工信息用
		    $weixinid && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "weixin_id=" => $weixinid, 'state!=' => self::$StateDeleted));
		    if($user)	throw new SCException('员工微信号重复');

		    $emisset($user) && ail && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "email=" => $email, 'state!=' => self::$StateDeleted));
			if(isset($user) && $user)	throw new SCException('员工邮箱重复');
			
			$mobile && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "mobile=" => $mobile, 'state!=' => self::$StateDeleted));
			if(isset($user) && $user)	throw new SCException('员工手机号重复');
		    
		    $acct && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "acct=" => $acct, 'state!=' => self::$StateDeleted));
		    if(isset($user) && $user)	throw new SCException('员工账号重复');
		    
		} else {		//更新员工信息用
			$weixinid && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "weixin_id=" => $weixinid, 'state!=' => self::$StateDeleted));
			if(isset($user) && $user && $user['id'] != $id)		throw new SCException('员工微信号重复');
			
			$email && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "email=" => $email, 'state!=' => self::$StateDeleted));
			if(isset($user) && $user && $user['id']!=$id)		throw new SCException('员工邮箱重复');
			
			$mobile && $user = $this -> get_user_by_condition(array("root_id ="=>$root_id, "mobile=" => $mobile, 'state!=' => self::$StateDeleted));
			if(isset($user) && $user && $user['id']!=$id)		throw new SCException('员工手机号重复');
		}
	}
	
//    -------------------同步方法--------------------------------
  /*
     * 同步创建员工
     */
     private function transfer_insert($id,$userinfo){
     	$dept_list=array();
		$dept_ids = explode(',', str_replace(array('[',']','"'),'',$userinfo['dept_list']));
		foreach($dept_ids as $deptid){
			$did = g('dept_map') -> find_by_did($deptid);
			array_push($dept_list, $did['wx_id']) ;
		}
     	
		!isset($userinfo['gender']) && $userinfo['gender'] = 0;
		!isset($userinfo['tel']) && $userinfo['tel'] = '';
		try {
	        $access_token = $this -> get_access_token();
			//创建部门返回ID
	       	return g('wxqy') -> create_user($access_token, $userinfo['acct'], $userinfo['name'], $dept_list, $userinfo['position'], $userinfo['mobile'], $userinfo['gender'], $userinfo['tel'], $userinfo['email'], $userinfo['weixin_id']);

		} catch (SCException $e) {
			throw $e;
		}
     }	
    /*
     * 同步更新员工
     */
     private function transfer_update($id,$userinfo){
     	log_write('微信端同步更新员工信息');
     	$dept_list=array();
		$dept_ids = explode(',', str_replace(array('[',']','"'),'',$userinfo['dept_list']));
	    foreach($dept_ids as $deptid){
	    	$did =  g('dept_map') -> find_by_did($deptid);
	    	//array_push($dept_list, $did['wx_id']) ;
	    }
     	
		!isset($userinfo['gender']) && $userinfo['gender'] = 0;
		!isset($userinfo['tel']) && $userinfo['tel'] = '';
	    try { 
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$result = g('sc_admin') -> get_by_ids($com_id);
	        $access_token = $this -> get_access_token();
	        //创建部门返回ID
	        return g('wxqy') -> update_user($access_token, $userinfo['acct'],$userinfo['name'],$dept_list,$userinfo['position'],$userinfo['mobile'],$userinfo['gender'],$userinfo['tel'],$userinfo['email'],$userinfo['weixin_id']);
	    
	    } catch (SCException $e) {
	    	throw $e;
	    }
     }
     
   /*
     * 同步删除员工
     */
     private function transfer_delete($id){
     	$user = $this-> get_user_by_id(3, $id);
     	
     	if(!$user)	throw new SCException('找不到员工', -1);

     	try {
	     	$access_token = $this -> get_access_token();
	        return g('wxqy') -> delete_user($access_token, $user['acct']);
     	
     	} catch (SCException $e) {
     		throw $e;
     	}
     }

	/**
	 * 根据当前登录信息，获取access token
	 */
	private function get_access_token() {
		try {
			$contact_conf = include (SYSTEM_APPS . 'contact/config.php');	//需要使用通讯录应用权限
			$app_combo = $contact_conf['combo'];
			return g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $app_combo);
			
		} catch (SCException $e) {
			throw $e;
		}
	}
}

// end of file