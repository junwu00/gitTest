<?php
/**
 * 意见信息

 * @author yangpz
 * @date 2015-03-26
 */
class cls_adv_apply {
	
	private static $Table = 'adv_apply';
	
	/**
	 * 保存文本意见
	 *  
	 * @param unknown_type $com_id
	 * @param unknown_type $user_acct
	 * @param unknown_type $detail
	 * @throws SCException
	 */
	public function save_text($com_id, $user_acct, $detail) {
		$data = array(
			'com_id' => $com_id,
			'user_acct' => $user_acct,
			'detail' => $detail,
			'create_time' => time(),		
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存意见失败');
		}
		return $ret;
	}

	/**
	 * 保存图片意见
	 *  
	 * @param unknown_type $com_id
	 * @param unknown_type $user_acct
	 * @param unknown_type $image
	 * @throws SCException
	 */
	public function save_image($com_id, $user_acct, $image) {
		$data = array(
			'com_id' => $com_id,
			'user_acct' => $user_acct,
			'images' => json_encode(array($image)),
			'create_time' => time(),		
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存意见失败');
		}
		return $ret;
	}
	
}