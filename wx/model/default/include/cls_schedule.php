<?php
/**
 * 工作日程
 * 
 * @author chenyihao
 * @date 2014-12-09
 *
 */
class cls_schedule {
	
	private static $Table = 'schedule_schedule';
	private static $PartnerTable = 'sc_partner';
	private static $GroupTable = 'schedule_group';
	
	private static $No_Remind ='0';
	private static $Begin_Remind ='1';
	private static $Before_10_Remind ='2';
	private static $Before_1_H_Remind ='3';
	private static $Before_1_D_Remind ='4';
	
		private static $Repeat_Time = array(
					1 => "+1 day",
					2 => "+1 week",
					3 => "+1 month",
					4 => "+1 year",
		);

	public function load_count($user_id,$com_id,$partner_id = ''){
		$like_sql = '';
		if(!empty($partner_id)){
			$like_sql = "and (( share_type = 1 OR  share_type = 2 ) OR repeat_type != 0 )";
		}
		
$sql = <<<EOF
		SELECT DISTINCT s.repeat_type,s.share_type,s.user_list,s.dept_list,g.group_user_list,g.group_dept_list, FROM_UNIXTIME(s.start_date,'%Y-%m-%d') startdate,FROM_UNIXTIME(s.end_date,'%Y-%m-%d') enddate,if(FROM_UNIXTIME(s.start_date,'%Y-%m-%d') = FROM_UNIXTIME(s.end_date,'%Y-%m-%d'),1,0 ) day,state
		FROM schedule_schedule s LEFT JOIN schedule_group g ON g.id = s.group_list
		WHERE s.com_id = {$com_id} and s.user_id = {$user_id} and s.start_date is not NULL {$like_sql}
EOF;
		$ret = g('db') -> select($sql);
		if(!empty($partner_id)){
			$result = array();
			foreach($ret as $r){
				if($r['share_type']==1){
					array_push($result, $r);
					continue;
				}
				
				$user_list = json_decode($r['user_list'],true);
				if(in_array($partner_id,$user_list)){
					array_push($result, $r);
					continue;
				}
//				$dept_list = json_decode($r['dept_list'],true);
//	//			if(!empty(array_intersect($dept_list,$dept_all))){
//	//				array_push($result, $r);
//	//				continue;
//	//			}
				
	//			$group_dept_list = json_decode($r['group_dept_list'],true);
	//			if(!empty(array_intersect($group_dept_list,$dept_all))){
	//				array_push($result, $r);
	//				continue;
	//			}
				
				$group_user_list = json_decode($r['group_user_list'],true);
				if(in_array($partner_id,$group_user_list)){
					array_push($result, $r);
					continue;
				}
			}
			return $result;
		}else{
			return $ret;
		}
	}
	
	/**
	 * 保存日程
	 * @param unknown_type $uid 用户ID
	 * @param unknown_type $content 日程内容
	 * @param unknown_type $date 	日程日期
	 * @param unknown_type $remind	提醒方式
	 * @param unknown_type $state	日程紧急状态
	 * @param unknown_type $place	日程地点
	 */
	public function save($uid,$com_id,$data){
		log_write("userid:".$uid."开始保存日程",'SCHEDULE');
		$data = array(
			'com_id' => $com_id,
			'user_id'=>$uid,
			'title'=>$data['title'],
			'content'=>$data['desc'],
			'start_date'=>$data['start_date'],
			'end_date'=>$data['end_date'],
			'all_day'=>$data['allday'],
			'remind_type'=>$data['remind'],
			'remind_share'=>$data['remind_share'],
			'repeat_type'=>$data['repeat'],
			'state'=>$data['state'],
			'share_type'=>$data['share_type'],
			'user_list'=>$data['share_user'],
			'dept_list'=>$data['share_dept'],
			'group_list'=>$data['share_group'],
			'place'=>$data['place'],
			'create_date'=>time(),
			'remind_time'=> $data['remind_time'],
			'remind_state'=>0,
		);
		
		$result = g('ndb')->insert(self::$Table,$data);
		if(!$result){
			log_write("插入失败",'SCHEDULE');
	  		throw new SCException('插入失败！');
		}
		return $result;
	}
	/**
	 * 更新日程
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $sid
	 * @param unknown_type $content
	 * @param unknown_type $date
	 * @param unknown_type $remind
	 * @param unknown_type $state
	 * @param unknown_type $place
	 * @throws SCException
	 */
	public function update($uid,$com_id,$sid,$data){
		$data = array(
			'title'=>$data['title'],
			'content'=>$data['desc'],
			'start_date'=>$data['start_date'],
			'end_date'=>$data['end_date'],
			'all_day'=>$data['allday'],
			'remind_type'=>$data['remind'],
			'remind_share'=>$data['remind_share'],
			'repeat_type'=>$data['repeat'],
			'state'=>$data['state'],
			'share_type'=>$data['share_type'],
			'user_list'=>$data['share_user'],
			'dept_list'=>$data['share_dept'],
			'group_list'=>$data['share_group'],
			'place'=>$data['place'],
			'remind_time'=> $data['remind_time'],
			'remind_state'=> $data['remind_state'],
		);
		$cond = array(
			'user_id='=>$uid,
			'id='=>$sid,
			'com_id=' => $com_id,
		); 	
		$result = g('ndb')->update_by_condition(self::$Table,$cond,$data);
		if(!$result){
			log_write("更新失败",'SCHEDULE');
	  		throw new SCException('更新失败！');
		}
		return $result;
	}
	/**
	 * 通过ID获取日程
	 * Enter description here ...
	 * @param unknown_type $uid 用户ID
	 * @param unknown_type $sid 日程ID
	 */
	public function get($uid,$sid){
	log_write("userid:".$uid."开始获取日程",'SCHEDULE');
		$fields ='*';
		$cond = array(
			'user_id='=>$uid,
			'id='=>$sid
		); 	
		$result = g('ndb')->select(self::$Table,$fields,$cond);
		if(!$result)
			log_write("获取失败",'SCHEDULE');
		return $result; 
	}
	/**
	 * 获取指定日期的日程
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $startdate
	 * @param unknown_type $enddate
	 */
	public function get_by_date($uid,$startdate,$enddate){
	log_write("userid:".$uid."开始读取日程",'SCHEDULE');
		$fields ='*';
		$cond = array(
			'user_id='=>$uid,
			'schedule_date>='=>$startdate,
			'schedule_date<'=>$enddate,
		); 	
		$result = g('ndb')->select(self::$Table,$fields,$cond);
		if(!$result)
			log_write("读取失败",'SCHEDULE');
		return $result; 
	}
	
	/**
	 * 删除日程
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $sid
	 */
	public function delete_schedule($com_id,$user_id,$sid,$remove_p = FALSE){
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id
		);
		if($remove_p){
			$cond['__OR'] = array(
				'id=' => $sid,
				'p_id=' =>$sid 
			);
		}else{
			$cond['id='] = $sid;
		}
		$ret = g('ndb') -> delete(self::$Table,$cond);
		return $ret;
	}
	
	/**
	 * 根据开始时间和结束时间获取日程
	 * @param unknown_type $com_id
	 * @param unknown_type $uid
	 * @param unknown_type $starttime
	 * @param unknown_type $endtime
	 */
	public function get_by_time($com_id,$uid,$startdate,$enddate,$partner_id=false){
		$fields ='s.*,g.group_dept_list,g.group_user_list';
		$cond = array(
			's.com_id='=>$com_id,
			's.user_id='=>$uid,
			'__OR' => array(
					array(
						's.start_date>=' => $startdate,
						's.start_date<=' => $enddate,
					),
					array(
						's.end_date>=' => $startdate,
						's.end_date<=' => $enddate,
					),
					array(
						's.start_date<=' => $startdate,
						's.end_date>=' => $enddate,
					),
					array(
						's.repeat_type!=' => 0,
					),
			),
		);
		if(!empty($partner_id)){
			
			$user = g('user') ->get_by_id($partner_id,'dept_tree');
			if(empty($user)){
				throw new SCException('员工不存在');
				return false;
			}
			$dept_tree = json_decode($user['dept_tree']);
			$dept_all = array();
			foreach ($dept_tree as $d){
				$dept_all = array_merge($dept_all,$d);
			}
			//去除重复的部门
			$dept_all = array_flip(array_flip($dept_all));
			
			$cond['__OR_1'] = array(
				's.share_type =' => 2,
				's.share_type=' => 1
			);
		}
		$result = g('ndb')->select(self::$Table.' s LEFT JOIN '.self::$GroupTable .' g ON s.group_list = g.id ',$fields,$cond,'','','',' order by s.all_day desc,s.start_date asc');
		if(empty($result)){
			return false;
		}
		
		$repeat = array();
		foreach($result as $key => $r){
			if($r['repeat_type'] != 0){
				array_push($repeat,$r);
				unset($result[$key]);
			}
		}unset($r);
		$repeat_schedule = $this -> repeat_schedule($repeat,$startdate,$enddate);
		$result = array_merge($result,$repeat_schedule);

		if(!empty($partner_id)){
			$ret = array();
			foreach($result as $r){
				
				if($r['share_type']==1){
					array_push($ret, $r);
					continue;
				}
				
				$user_list = json_decode($r['user_list'],true);
				if(in_array($partner_id,$user_list)){
					array_push($ret, $r);
					continue;
				}
				
//				$dept_list = json_decode($r['dept_list'],true);
//	//			if(!empty(array_intersect($dept_list,$dept_all))){
//	//				array_push($ret, $r);
//	//				continue;
//	//			}
				
	//			$group_dept_list = json_decode($r['group_dept_list'],true);
	//			if(!empty(array_intersect($group_dept_list,$dept_all))){
	//				array_push($ret, $r);
	//				continue;
	//			}
				
				$group_user_list = json_decode($r['group_user_list'],true);
				if(in_array($partner_id,$group_user_list)){
					array_push($ret, $r);
					continue;
				}
			}
			
			return $ret; 
		}else{
			return $result;
		}
	}
	
	/**
	 * 重复日程
	 **/
	public function repeat_schedule($schedule_list,$start_time,$end_time){
		if(empty($schedule_list)){
			return array();
		}
		$all_schedule = array();
		$start = $start_time;
		$end = $end_time;
		$today = date('Y-m-d',$start_time);
		$day = date('d',$start_time);
		$month = date('m',$start_time);
		$year = date('Y',$start_time);
		$week = date('w',$start_time);

		foreach ($schedule_list as $sch) {
			if($sch['start_date'] >= $end_time){
				continue;
			}else if($sch['start_date'] <= $start_time){
				if($sch['repeat_type'] == 1){
					$s_time = date('H:i',$sch['start_date']);
					$e_time = date('H:i',$sch['end_date']);

					$tmp_start = $today.' '.$s_time;
					$tmp_end = $today.' '.$e_time;
					$sch['start_date'] = strtotime($tmp_start);
					$sch['end_date'] = strtotime($tmp_end);

				}else if($sch['repeat_type'] == 2){
					$s_time = date('H:i',$sch['start_date']);
					$e_time = date('H:i',$sch['end_date']);

					$s_week = date('w',$sch['start_date']);
					if($week <= $s_week){
						$s_day = $s_week - $week; 

						$tmp_day = strtotime("+".$s_day." day",strtotime($today));
						$sch['start_date'] = $tmp_day + $this -> time_to_second($s_time);
						$sch['end_date'] = $tmp_day + $this -> time_to_second($e_time);
					}
				}else if($sch['repeat_type'] == 3){
					$s_time = date('H:i',$sch['start_date']);
					$e_time = date('H:i',$sch['end_date']);

					$s_day = date('d',$sch['start_date']);
					$tmp_day = $year .'-'. $month.'-'.$s_day;
					$tmp_time = strtotime($tmp_day);
					if($tmp_time < $start_time){
						$tmp_time = strtotime("+1 month",$tmp_time);
					}
					$sch['start_date'] = $tmp_time + $this -> time_to_second($s_time);
					$sch['end_date'] = $tmp_time + $this -> time_to_second($e_time);

				}else if($sch['repeat_type'] == 4){
					$s_time = date('H:i',$sch['start_date']);
					$e_time = date('H:i',$sch['end_date']);
					$s_month_day = date('-m-d',$sch['start_date']);
					$tmp_day = $year.$s_month_day;
					$tmp_time = strtotime($tmp_day);
					if($tmp_time < $start_time){
						$tmp_time = strtotime("+1 year",$tmp_time);
					}
					$sch['start_date'] = $tmp_time + $this -> time_to_second($s_time);
					$sch['end_date'] = $tmp_time + $this -> time_to_second($e_time);
				}
			}
			$sch_list = $this -> get_repeat($sch,$start_time,$end_time);
			$all_schedule = array_merge($all_schedule,$sch_list);
		}unset($sch);

		return $all_schedule;
	}

	/**
	 * 将时间字符串转换成秒
	 **/
	private function time_to_second($time){
		$array = explode(':',$time);
		if(empty($array) || count($array) != 2){
			return 0;
		}

		return ($array[0] * 60 +  $array[1]) * 60;
	}


	private function get_repeat($schedule,$start_time,$end_time){
		$schedule_list = array();
		if($schedule['start_date'] < $start_time){
			$schedule['start_date'] = strtotime(self::$Repeat_Time[$schedule['repeat_type']],$schedule['start_date']);
			$schedule['end_date'] = strtotime(self::$Repeat_Time[$schedule['repeat_type']],$schedule['start_date']);
		}
		while ($schedule['start_date'] >= $start_time && $schedule['start_date'] < $end_time ) {
			array_push($schedule_list,$schedule);
			$schedule['start_date'] = strtotime(self::$Repeat_Time[$schedule['repeat_type']],$schedule['start_date']);
			$schedule['end_date'] = strtotime(self::$Repeat_Time[$schedule['repeat_type']],$schedule['end_date']);
		}
		return $schedule_list;
	}

	/**
	 * 获取所有包含日程的日期
	 * Enter description here ...
	 * @param unknown_type $uid
	 */
	public function get_all_date($uid){
		$sql = "select FROM_UNIXTIME(s.schedule_date,'%Y-%m-%d') date,MAX(s.state) state,sum(s.isfinish=0) count from schedule_schedule s where s.user_id = '".$uid."' GROUP BY date";
		return g('db') -> select($sql);
	}
	
	/**
	 * 设置日程完成
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $sids
	 * @throws SCException
	 */
	public function finish($uid,$sids){
		foreach ($sids as $sid){
			if(!$sid)
				continue;
			$cond =array(
					'user_id='=>$uid,
					'id='=>$sid
				);
			$data = array(
					'isfinish'=>1,
					'remind_state'=>1
				);
			if(!g('ndb')->update_by_condition(self::$Table,$cond,$data)){
				throw new SCException('设置完成失败');
			}
		}
			
	}
	
	/**
	 * 删除伙伴
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $partner_id
	 */
	public function remove_partner($com_id,$user_id,$partner_id){
		$time = time();
		$sql = "UPDATE ".self::$PartnerTable." SET info_state = 0 ,update_time = {$time} WHERE com_id = {$com_id} AND (user_id = {$user_id} AND partner_id = {$partner_id}) OR (user_id = {$partner_id} AND partner_id = {$user_id})";
		$ret = g('db') -> query($sql);
		return $ret ? true : false;
	}
	
	
	/**
	 * 检查是否为伙伴
	 * @param unknown_type $user_id1
	 * @param unknown_type $user_id2
	 */
	public function check_partner($com_id,$user_id1,$user_id2){
		$sql = "SELECT id FROM sc_partner WHERE com_id = {$com_id} AND state = 2 AND info_state = 1 AND ( (user_id = {$user_id1} and partner_id = {$user_id2} ) or (user_id = {$user_id2} and partner_id = {$user_id1}))";
		$ret = g('db') -> select($sql);
		return empty($ret) ? false : true;
	}
	
	
	/**
	 * 检查是否已经发送过申请
	 * @param unknown_type $user_id1
	 * @param unknown_type $user_id2
	 */
	public function check_apply($com_id,$user_id,$partner_id){
		$sql = "SELECT id FROM sc_partner WHERE com_id = {$com_id} AND (state = 2 or state = 1) AND info_state = 1 AND user_id = {$user_id} and partner_id = {$partner_id} ";
		$ret = g('db') -> select($sql);
		return empty($ret) ? false : true;
	}
	
	/**
	 * 删除日程
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $sids
	 * @throws SCException
	 */
	public function delete($uid,$id){

			$cond =array(
					'user_id='=>$uid,
					'id='=>$id
				);
		
			if(!g('ndb')->delete(self::$Table,$cond)){
				throw new SCException('删除失败');
			}
		}
			
	/**
	 * 获取日程详细信息
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $sid
	 */
	public function get_by_id($com_id,$sid){
		$filde ="s.*,g.group_name,g.group_user_list,g.info_state group_info_state";
		$cond =array(
				's.com_id='=>$com_id,
				's.id='=>$sid,
		);
		$table = self::$Table . ' s LEFT JOIN '.self::$GroupTable .' g ON s.group_list = g.id ';
		$result = g('ndb')-> select($table,$filde,$cond);
		return $result?$result[0]:false;
	}
	
	/**
	 * 删除分组之后，纠正数据
	 */
	public function delete_group($com_id,$user_id,$group_id){
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'group_list=' => $group_id
		);
		$data = array(
			'group_list' => 0
		);
		g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		
	}
	
	
	/**
	 * 申请添加伙伴
	 */
	public function apply_partern($user_id,$com_id,$parterns){
		if(empty($parterns)){
			return;
		}
		$create_time = time();
		$insert_str = array();
		foreach($parterns as $partern){
			array_push($insert_str,"({$user_id},{$com_id},{$partern['id']},'{$partern['name']}',1,{$create_time},1 )");	
		}
		$sql = 'INSERT INTO '.self::$PartnerTable.' (user_id,com_id,partner_id,partner_name,state,create_time,info_state) VALUES'.implode(',', $insert_str);
		$ret = g('db') -> query($sql);
		return $ret ? true : false;
	}
	
	/**
	 * 获取我的所有伙伴
	 * @param unknown_type $user_id
	 * @param unknown_type $com_id
	 */
	public function get_partner($user_id,$com_id,$fields = '*',$state='=2'){
		$sql = "SELECT {$fields} FROM sc_partner p LEFT JOIN sc_user u ON u.id = p.partner_id where p.user_id = {$user_id} and p.com_id = {$com_id} and p.info_state = 1 and p.state {$state} ";
		$ret = g('db') -> select($sql);
		return $ret;
	}
	
	/**
	 * 获取向我申请的伙伴信息
	 * @param unknown_type $user_id
	 * @param unknown_type $com_id
	 */
	public function get_apply_partner($user_id,$com_id,$fields = '*',$state=1){
		$sql = "SELECT {$fields} FROM sc_partner p LEFT JOIN sc_user u ON u.id = p.user_id where p.partner_id = {$user_id} and p.com_id = {$com_id} and p.info_state = 1 and p.state {$state} ";
		log_write($sql);
		$ret = g('db') -> select($sql);
		return $ret;
	}
	
	
	/**
	 * 根据条件获取日程信息
	 * @param unknown_type $uid
	 * @param unknown_type $sid
	 */
	public function get_by_condition($uid,$cond,$fields){
		if( !is_array($cond) || empty($cond) ){
			return false;
		}
		$cond['user_id='] = $uid;
		
		$result = g('ndb')-> select(self::$Table,$fields,$cond);
		return $result?$result:false;
	}
	
	/**
	 * 处理申请
	 * @param unknown_type $user_id
	 * @param unknown_type $apply_id
	 * @param unknown_type $state
	 */
	public function deal_apply($com_id,$user_id,$apply_id,$state){
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $apply_id,
			'partner_id=' => $user_id,
			'state=' => 1
		);
		$data = array(
			'state' => $state,
			'update_time' => time()
		);
		$ret = g('ndb') -> update_by_condition(self::$PartnerTable,$cond,$data);
		return $ret;
	}
	
	
	/**
     * 获取未处理的调研数量
     *
     * @access public
     * @return integer
     */
    public function get_tips_number() {
        $ret = 0;

        $fields = 'COUNT(id) AS tips_count';

        $today_time = strtotime(date('Y-m-d'));
        $condition = array(
        	'com_id=' => $_SESSION[SESSION_VISIT_COM_ID],
        	'user_id=' => $_SESSION[SESSION_VISIT_USER_ID],
        	'isfinish=' => 0,
        	'schedule_date>=' => $today_time,
        	'schedule_date<' => ($today_time + 24 * 3600),
        );

        $data = g('ndb') -> select(self::$Table, $fields, $condition);
        is_array($data) and !empty($data[0]) and $ret = $data[0]['tips_count'];

        $ret = intval($ret);

        return $ret;
    }
}

// end of file