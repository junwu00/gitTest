<?php
/**
 * 邮件附件相关方法
 *
 */
class cls_email_attach {
	
	private static $Table = 'email_attach';
	
	
	/**
	 * ���email_email_id��ѯ����
	 * Enter description here ...
	 * @param unknown_type $email_email_id �ʼ�id
	 */
	public function get_email_mid($email_email_id){
		return g('ndb') -> get_data_by_ids(self::$Table,$email_email_id,'email_email_id');
	}
	
	/**
	 * ���id��ȡ
	 * @param unknown_type $ids		integer��array
	 * @param unknown_type $fields	��ѯ��Щ�ֶ�
	 */
	public function get_by_ids($id, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $id, 'id', $fields);
	}
	
	/**
	 * 通过哈希值获取附件信息
	 * @param unknown_type $hash
	 */
	public function get_by_hash($hash,$filds="*"){
		$cond = array(
			'hash=' => $hash
		);
		$result = g('ndb') -> select(self::$Table,$filds,$cond);
		return $result ? $result[0]:false;
	}
	
	/**
	 * 更新附件
	 */
	public function update($condition, $email_attach) {
		try {
			g('db') -> begin_trans();
		    $result = g('ndb') -> update_by_condition(self::$Table, $condition, $email_attach);
		    if(!$result)throw new SCException('ʧ��');
	       	g('db')->commit();
		} catch (SCException $e) {
			g('db')->rollback();
			throw $e;
		}
	}
	
	/**
	 * 保存邮件附件
	 * @param unknown_type $email_id	邮件ID
	 * @param unknown_type $filehash	附件哈希
	 * @param unknown_type $filename	附件名
	 * @param unknown_type $filepath	附件路径
	 */
	public function save_attach($filehash,$filename,$filepath,$rela_path){
		try{
			log_write('开始保存邮件附件信息','EMAIL');
			$data = array(
				"email_email_id" => 0,		//附件刚上传时，email_id为0
				"attach_name" => $filename,
				"attach_rela_path" => $rela_path,
				"attach_url" => $filepath,
				"hash" => $filehash,
				"create_date" => time(),
			);
			$result = g('ndb') -> insert(self::$Table, $data,TRUE);
			if(!$result) 
				throw new SCException('保存邮件失败');
			else 
				return $result;
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 更新附件信息对应的email_id
	 * @param unknown_type $email_id		邮件ID
	 * @param unknown_type $file_hash_list	邮件附件哈希列表(加密ID)
	 */
	public function update_attach($email_id,$file_hash_list){
		try{
			log_write('开始更新邮件附件信息','EMAIL');
			if(!is_array($file_hash_list))
				throw new SCException('参数格式不对');
				
			$ids = self::decode($file_hash_list);
			$attach = g('email_attach')->get_by_ids($ids);
			if($attach)
				foreach($attach as $a){
					if($a['email_email_id']==0){
						$cond = array(
							"id=" => $a['id'],
						);
						$data = array(
							"email_email_id" => $email_id,		//修改附件对应的邮件ID
						);
						g('ndb') -> update_by_condition(self::$Table,$cond,$data);
					}else{
						$data = array(
							"email_email_id" => $email_id,
							'attach_name' => $a['attach_name'],
							'attach_rela_path' => $a['attach_rela_path'],
							'create_date' =>time(),
							'attach_url' => $a['attach_url'],
							'hash' => $a['hash'],
						);
						g('ndb') -> insert(self::$Table,$data);
					}
				}
			return $attach;
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 将加密数组解密
	 * @param unknown_type $ids
	 */
	public function decode($ids){
		if(!is_array($ids))
			throw new SCException('解密数组格式不对');
		foreach($ids as $key => $i){
			$ids[$key] = g('des')->decode($i);
		}
		return $ids;
	}
	
}
