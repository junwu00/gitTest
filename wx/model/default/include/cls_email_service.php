<?php
/**
 * 企业邮箱服务类
 * 
 * @author yangpz
 * @date 2014-10-20
 *
 */
class cls_email_service {
	/** 对应的库表名称 */
	private static $Table = 'email_service';
	
	
	/**
	 * 插入数据
	 * @param unknown_type 
	 * @return TRUE or false
	 */
	public function insert($root_id,$receive,$receive_port,$receive_ssl,$send,$send_port,$send_ssl,$type) {
		$data = array(
			'root_id' => $root_id,
			'receive_service' => $receive,
		    'receive_port' => $receive_port,
		    'receive_ssl' => $receive_ssl,
		    'send_service' => $send,
		    'send_port' => $send_port,
		    'send_ssl' =>$send_ssl,
		    'type' => $type
		);
		$result = g('ndb') -> insert(self::$Table,$data);
		return $result;
	}
	
    /**
	 * 更新数据
	 * @param unknown_type 
	 * @return TRUE or false
	 */
	public function update($root_id,$id,$receive,$receive_port,$receive_ssl,$send,$send_port,$send_ssl,$type) {

		$cond =array(
		    'root_id ='=>$root_id,
		    'id =' => $id
		);
		
		$data = array(
			'receive_service' => $receive,
		    'receive_port' => $receive_port,
		    'receive_ssl' => $receive_ssl,
		    'send_service' => $send,
		    'send_port' => $send_port,
		    'send_ssl' =>$send_ssl,
		    'type' => $type
		);
		return g('ndb') -> update_by_condition(self::$Table,$cond,$data);
	}
	
     /**
	 * 获取数据
	 * @param unknown_type 
	 * @return TRUE or false
	 */
	public function get_by_root($root_id) {
        $field ='*';
		$cond =array(
		    'root_id ='=>$root_id
		);
		return g('ndb') -> select(self::$Table,$field,$cond);
	}
}

// end of file