<?php
/**
 * 企业云盘数据模型
 *
 * @author LiangJianMing
 * @create 2015-03-14
 */
class cls_cabinet {
	//公司id
	private $com_id 	= NULL;
	//用户id
	private $user_id 	= NULL;
	//基础条件
	private $base_con 	= NULL;
	//该应用对应的微信应用id
	private $wx_app_id  = NULL;
	//公司的access_token
	private $access_token = NULL;
	//corpurl
	private $corp_url = '';

	public $app_conf = array();

	//企业云盘目录表
	public $dir_table = 'cabinet_dicrectory';
	//企业云盘文件存储表
	public $file_table = 'cabinet_file';
	//企业云盘文件收藏表
	public $collect_table = 'cabinet_collected';
	//下载记录表
	private $dl_table = 'cabinet_download';

	//用户信息表
	public $user_table 	 = 'sc_user';
	//部门信息表
	public $dept_table 	 = 'sc_dept';
	//部门信息表
	public $group_table 	 = 'sc_group';

	//公司信息表
	public $company_table = 'sc_company';
	//公司的应用对应微信应用的映射关系表
	public $app_map_table = 'sc_com_app';
	//我方部门id与微信方部门id的映射关系表
	public $dept_map_table = 'sc_dept_map';

	//应用对话的帮助提示
	public $help_tip = "输入关键字可直接搜索哦！";
	//对话框最大可搜索文件数
	public $max_search_file = 20;
	//对话文件序列映射哈希表有效时间
	public $max_valid_time = 1800;

	//推送搜索帮助的按钮key值
	public $help_key = 'easy_search_helper';

	//文件提示消息
	public $file_tip = <<<EOF
文件名: %file_name%
类型: %ext%
大小: %file_size%
上传时间: %create_time%

<a href='%detail_url%'>查看</a>
EOF;

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : NULL;
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);

		$app_conf_url = SYSTEM_APPS . 'cabinet/config.php';
		file_exists($app_conf_url) and $this -> app_conf = include($app_conf_url);

		if (!empty($this -> com_id)) {
			g('nmsg') -> init($this -> com_id, $this -> app_conf['name']);
			try {
				$com_info = g('nmsg') -> get_com_info();
				$this -> corp_url = $com_info['corp_url'];
			}catch(SCException $e) {}
		}
	}

	/**
	 * 重新初始化相关变量
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @param string $acct 微信端账号
	 * @return void
	 */
	public function reinit($com_id, $acct) {
		$this -> com_id = $com_id;
		try {
			$this -> user_id = $this -> get_id_by_acct($acct);
		}catch(SCException $e) {
			throw $e;
		}
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);

		g('nmsg') -> init($this -> com_id, $this -> app_conf['name']);
		try {
			$com_info = g('nmsg') -> get_com_info();
			$this -> corp_url = $com_info['corp_url'];
		}catch(SCException $e) {}
	}

	/**
	 * 根据微信端的用户账号获取员工的用户id
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @param string $acct 员工账号
	 * @return integer
	 * @throws SCException
	 */
	public function get_id_by_acct($acct) {
		try {
			$root_id = g('common') -> get_root_id($this -> com_id);
		}catch(SCException $e) {
			throw $e;
		}

		$table = $this -> user_table;

		$sql = <<<EOF
SELECT id FROM {$table} 
WHERE root_id={$root_id} AND acct='{$acct}' AND state=1 LIMIT 1
EOF;

		$user_id = g('db') -> select_first_val($sql);
		if (!$user_id) {
			throw new SCException('查询用户id失败！');
		}
		return $user_id;
	}

	/**
	 * 收藏文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return TRUE
	 * @throws SCException
	 */
	public function collect_file($cf_id) {
		$check = $this -> check_collected($cf_id);
		if ($check) {
			throw new SCException('请勿重复收藏文件!');
		}

		$data = array(
			'com_id' => $this -> com_id,
			'user_id' => $this -> user_id,
			'cf_id' => $cf_id,
			'create_time' => time(),
		);

		$result = g('ndb') -> insert($this -> collect_table, $data);
		if (!$result) {
			throw new SCException('收藏失败，请稍后再试!');
		}

		$this -> flush_collect_cache();

		return TRUE;
	}

	/**
	 * 取消收藏文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return TRUE
	 * @throws SCException
	 */
	public function uncollect_file($cf_id) {
		$check = $this -> check_collected($cf_id);
		if (!$check) {
			throw new SCException('未收藏该文件，无需取消!');
		}

		$condition = $this -> base_con;
		$condition['user_id='] = $this -> user_id;
		$condition['cf_id='] = $cf_id;

		$result = g('ndb') -> delete($this -> collect_table, $condition);
		if (!$result) {
			throw new SCException('取消失败，请稍后再试!');
		}

		$this -> flush_collect_cache();

		return TRUE;
	}

	/**
	 * 检查文件是否已被收藏
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return boolean
	 */
	public function check_collected($cf_id) {
		$condition = array(
			'com_id=' => $this -> com_id,
			'user_id=' => $this -> user_id,
			'cf_id=' => $cf_id,
		);

		$check = g('ndb') -> record_exists($this -> collect_table, $condition);

		return $check ? TRUE : FALSE;
	}

	/**
	 * 获取目录详细信息
	 *
	 * @access public
	 * @param integer $cd_id 目录id
	 * @return array
	 */
	public function get_dir_info($cd_id) {
		$ret = array();

		if ($cd_id === 0) {
			return $ret;
		}

		$fields = array(
			'cd_pid', 'cd_id', 'name',
		);
		$fields = implode(',', $fields);

		$condition = $this -> base_con;
		$condition['cd_id='] = $cd_id;

		$result = g('ndb') -> select($this -> dir_table, $fields, $condition, 1, 1);

		if ($result) {
			$ret = $result[0];
		}

		return $ret;
	}

	/**
	 * 获取文件详细信息
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return array
	 * @throws SCException
	 */
	public function get_file_info($cf_id) {
		$condition = array(
			'cf_id=' => $cf_id,
		);

		try {
			$info = $this -> get_search_list($condition, 1, 1, '', false);
			$info = isset($info[0]) ? $info[0] : array();
		}catch(SCException $e) {
			throw new SCException('无权查看该文件或文件已被删除！');
		}

		if (!is_array($info) or empty($info)) {
			throw new SCException('无权查看该文件或文件已被删除！');
		}

		$ret = $info;
		return $ret;
	}

	/**
	 * 用户下载文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @param string $size 为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
	 * @param boolean $is_push 是否强制通过微信推送
	 * @param boolean $increase 是否增加文件下载数
	 * @return TRUE
	 * @throws SCException
	 */
	public function download_file($cf_id, $size='', $is_push=false, $increase=true) {
		try {
			$info = $this -> get_file_info($cf_id);
		}catch(SCException $e) {
			throw $e;
		}

		$platid = g('common') -> get_mobile_platid();
		$ad_download = g('media') -> android_download;

		$ext = strtolower($info['extname']);
		//wav格式文件强制下载,android则提示到pc微信下载
		if ($ext == 'wav') {
			$is_push = false;
			if ($platid === 2) throw new SCException('系统不支持该格式文件下载，请到pc微信下载');
		}

		if (!$is_push) {
			//如果是ios平台支持的文件，或者是安卓平台下支持直接下载的文件类型，则直接跳转下载
			if (($platid === 1 and $ext != 'rar' and $ext != 'zip') or ($platid === 2 and in_array($ext, $ad_download)) or $ext == 'wav') {
				$increase and $this -> increase_download_count($cf_id);
				g('media') -> cloud_rename_download($info['name'], $info['hash'], $size);

				$this -> flush_download_cache();

				return true;
			}
		}
		
		$file_name = $info['name'] . '.' . $ext;
		try {
			$media_info = g('media') -> get_media_id($this -> com_id, $this -> app_conf['combo'], $info['hash'], $file_name, $size);
			$media_id = $media_info['media_id'];
		}catch(SCException $e) {
			throw new SCException('系统正忙，请稍后再试！');
		}

		$file_type = g('media') -> get_file_type($ext);

		if ($file_type == 'video') {
			$result = g('nmsg') -> send_video($media_id, '', '', array(), array($this -> user_id));
		}else {
			$result = g('nmsg') -> send_files($file_type, $media_id, array(), array($this -> user_id));
		}

		if (!$result) {
			$err_tip = '系统正忙，请稍后再试！';
			throw new SCException($err_tip);
		}

		$increase and $this -> increase_download_count($cf_id);

		$this -> flush_download_cache();

		return true;
	}

	/**
	 * 给用户推送文件详情入口
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return TRUE
	 * @throws SCException
	 */
	public function push_file_entry($cf_id) {
		try {
			$info = $this -> get_file_info($cf_id);
		}catch(SCException $e) {
			$err_tip = '文件已被删除或无权查看！';

			try {
				g('nmsg') -> send_text($err_tip, array(), array($this -> user_id));
			}catch(SCException $tmpe) {
				throw $tmpe;
			}

			throw $e;
		}

		$file_name = string_auto_cut($info['name'], 10);
		$file_size = prase_humen_size($info['filesize']);
		$detail_url = $this -> compose_file_url($cf_id);

		$create_time = g('format') -> friendly_time_format($info['create_time']);
		$create_time = preg_replace('/ .*$/', '', $create_time);
		$create_time = preg_replace('/天.*$/', '天前', $create_time);

		$file_tip = str_replace('%file_name%', $file_name, $this -> file_tip);
		$file_tip = str_replace('%ext%', $info['extname'], $file_tip);
		$file_tip = str_replace('%file_size%', $file_size, $file_tip);
		$file_tip = str_replace('%create_time%', $create_time, $file_tip);
		$file_tip = str_replace('%detail_url%', $detail_url, $file_tip);

		//发送文件入口
		try {
			g('nmsg') -> send_text($file_tip, array(), array($this -> user_id));
		}catch(SCException $e) {
			throw $e;
		}

		return TRUE;
	}

	/**
	 * 组合文件详情url
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return string
	 */
	public function compose_file_url($cf_id) {
		$app_name = $this -> app_conf['name'];

		$param = array(
			'app' => $this -> app_conf['name'],
			'm' => 'cab_file',
			'a' => 'detail',
			'id' => $cf_id,
			'corpurl' => $this -> corp_url,
		);

		$query_string = http_build_query($param);

		$ret = SYSTEM_HTTP_DOMAIN . '?' . $query_string;
		return $ret;
	}

	/**
	 * 组合目录列表url
	 *
	 * @access public
	 * @param integer $cd_id 文件id
	 * @return string
	 */
	public function compose_dir_url($cd_id) {
		$app_name = $this -> app_conf['name'];

		$param = array(
			'app' => $this -> app_conf['name'],
			'm' => 'cab_dir',
			'a' => 'lister',
			'id' => $cd_id,
		);

		$query_string = http_build_query($param);

		$ret = SYSTEM_HTTP_DOMAIN . '?' . $query_string;
		return $ret;
	}

	/**
	 * 获取目录列表
	 *
	 * @access public
	 * @param integer $cd_pid 父目录id, 0表示根目录
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @param boolean $with_count 是否返回总记录数量
	 * @return array
	 */
	public function get_dir_list($cd_pid, array $condition, $page, $page_size, $order='', $with_count=true) {
		$ret = array();

		//默认排序方式为用户自定义、目录名称拼音排序
		empty($order) and $order = ' ORDER BY `sort` DESC, name_py ASC, cd_id DESC ';

		$condition = $condition + $this -> base_con;
		$condition['state='] = 1;
		$condition['cd_pid='] = $cd_pid;

		$condition = $this -> merage_privilege_condition($condition, true);
		if (!$condition) return $ret;

		//执行排序以提高命中率
		ksort($condition);

		$key_str = __FUNCTION__ . '::' . json_encode($condition) . "::{$page}::{$page_size}::{$order}::{$with_count}";
		$key_arr = $this -> get_dir_cache_key($cd_pid, $key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		$fields = array(
			'cd_id', 'cd_pid', 'name', 
			'name_py', 
			'sort', 'update_time', 'create_time',
		);
		$fields = implode(',', $fields);
		$ret = g('ndb') -> select($this -> dir_table, $fields, $condition, $page, $page_size, '', $order, $with_count);
		!is_array($ret) and $ret = array();

		$tmp_data = &$ret;
		$with_count and $tmp_data = &$ret['data'];

		if (!empty($tmp_data)) {
			$cd_ids = array();
			foreach ($tmp_data as &$val) {
				$cd_ids[$val['cd_id']] = 1;
			}
			unset($val);

			$file_counts = $this -> get_file_count(array_keys($cd_ids));

			foreach ($tmp_data as &$val) {
				$val['file_count'] = $file_counts[$val['cd_id']];
			}
			unset($val);

			$tmp_data = $this -> complete_children_info($tmp_data);
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 获取文件列表
	 *
	 * @access public
	 * @param integer $cd_id 目录id, 0表示根目录, 0> 表示不限制目录
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @param boolean $with_count 是否返回总记录数量
	 * @return array
	 */
	public function get_file_list($cd_id, array $condition, $page, $page_size, $order='', $with_count=false) {
		$ret = array();

		//默认排序方式为文件名称拼音排序
		empty($order) and $order = ' ORDER BY name_py ASC, cf_id DESC ';

		$condition = $condition + $this -> base_con;
		$condition['state='] = 1;
		$cd_id >= 0 and $condition['cd_id='] = $cd_id;

		$condition = $this -> merage_privilege_condition($condition, false);
		if (!$condition) return $ret;

		ksort($condition);

		$key_str = __FUNCTION__ . '::' . json_encode($condition) . "::{$page}::{$page_size}::{$order}::{$with_count}";
		$key_arr = $this -> get_dir_cache_key($cd_id, $key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		$fields = array(
			'cf_id', 'cd_id', 'name',
			'extname', 'filesize', 'filepath', 
			'mark', 'downloads', 'dload_able',
			'update_time', 'create_time', 'hash',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($this -> file_table, $fields, $condition, $page, $page_size, '', $order, $with_count);
		!is_array($ret) and $ret = array();

		$tmp_data = &$ret;
		$with_count and $tmp_data = &$ret['data'];

		if (!empty($tmp_data)) {
			foreach ($tmp_data as &$val) {
				$val['filesize_desc'] = prase_humen_size($val['filesize']);
			}
			unset($val);
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 获取用作搜索的全部文件信息
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @param boolean $with_count 是否返回总记录数量
	 * @return array
	 */
	public function get_search_list(array $condition, $page, $page_size, $order='', $with_count=true) {
		$ret = array();

		//默认排序方式为文件名称拼音排序
		empty($order) and $order = ' ORDER BY name_py ASC, cf_id DESC ';

		$condition = $condition + $this -> base_con;
		$condition['state='] = 1;

		$condition = $this -> merage_privilege_condition($condition, false);
		if (!$condition) return $ret;

		ksort($condition);

		$key_str = __FUNCTION__ . '::' . json_encode($condition) . "::{$page}::{$page_size}::{$order}::{$with_count}";
		$key_arr = $this -> get_dir_cache_key(0, $key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		if ($page > 0) {
			//常规搜索
			$fields = array(
				'cf_id', 'cd_id', 'name',
				'extname', 'filesize', 'filepath', 
				'mark', 'downloads', 'dload_able',
				'name_py', 'name_fpy',
				'update_time', 'create_time', 'hash',
			);
		}else {
			//全部索引
			$fields = array(
				'cf_id', 'cd_id', 'name', 'dload_able',
				'extname', 'name_py', 'name_fpy',
				'create_time',
			);
		}
		$fields = implode(',', $fields);

		if ($page > 0) {
			$ret = g('ndb') -> select($this -> file_table, $fields, $condition, $page, $page_size, '', $order, $with_count);
			!is_array($ret) and $ret = array();

			$tmp_data = &$ret;
			$with_count and $tmp_data = &$ret['data'];

			if (!empty($tmp_data)) {
				foreach ($tmp_data as &$val) {
					$val['filesize_desc'] = prase_humen_size($val['filesize']);
				}
				unset($val);
			}
		}else {
			$ret = g('ndb') -> select($this -> file_table, $fields, $condition, 0, 0, '', $order);
			!is_array($ret) and $ret = array();
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 识别关键字，回复相应信息
	 *
	 * @access public
	 * @param mixed $key_word 只可能为数字或字符串
	 * @return void
	 */
	public function discern_reply($key_word) {
		$cache_hash = $this -> get_serial_hash();
		$redis = g('redis') -> get_handler();

		if (preg_match('/^[\d]+$/', $key_word)) {
			$cache_key = $this -> get_serial_key($key_word);

			try {
				$cf_id = $redis -> hGet($cache_hash, $cache_key);
			}catch(Exception $e) {}

			if (!empty($cf_id)) {
				$this -> push_file_entry($cf_id);
				return;
			}
		}

		$condition = array(
			'name LIKE ' => '%' . $key_word . '%',
		);

		try {
			$file_list = $this -> get_search_list($condition, 1, $this -> max_search_file);
		}catch(SCException $e) {
			$err_tip = '无匹配结果，请尝试其他关键字！';
			try {
				g('nmsg') -> send_text($err_tip, array(), array($this -> user_id));
			}catch(SCException $tmpe) {
				throw $tmpe;
			}
			throw $e;
		}

		g('redis') -> delete($cache_hash);

		if (empty($file_list['count'])) {
			$err_tip = '无匹配结果，请尝试其他关键字！';
			try {
				g('nmsg') -> send_text($err_tip, array(), array($this -> user_id));
			}catch(SCException $tmpe) {
				throw $tmpe;
			}
			return;
		}

		$pre_tip = '';
		if ($file_list['count'] > $this -> max_search_file) {
			$pre_tip = '结果过多，仅列出前' . $this -> max_search_file . '项!' . "\n";
		}

		$list = $this -> reset_serial_hash($file_list['data']);

		$text = $pre_tip . '回复序号查看对应文件，' . "\n" . ($this -> max_valid_time / 60) . '分钟有效！';
		foreach ($list as $key => $val) {
			$text .= "\n" . $key . '、' . $val . '；';
		}

		try {
			g('nmsg') -> send_text($text, array(), array($this -> user_id));
		}catch(SCException $e) {
			g('redis') -> delete($cache_hash);
		}
	}

	/**
	 * 重新生成文件序列哈希映射
	 *
	 * @access public
	 * @param array $file_list 文件列表集合
	 * @return array
	 */
	public function reset_serial_hash(array $file_list) {
		$redis = g('redis') -> get_handler();
		$cache_hash = $this -> get_serial_hash();
		log_write('企业文件，hash=' . $cache_hash);

		try {
			$tmp_arr = array();
			foreach ($file_list as $key => $val) {
				$num = $key + 1;
				$cache_key = $this -> get_serial_key($num);
				$redis -> hSet($cache_hash, $cache_key, $val['cf_id']);
				$tmp_arr[$num] = $val['name'] . '.' . $val['extname'];
			}
			unset($val);

			$redis -> expire($cache_hash, 1800);
		}catch(Exception $e) {}

		return $tmp_arr;
	}

	/**
	 * 获取文件序列缓存hash
	 *
	 * @access public
	 * @return string
	 */
	public function get_serial_hash() {
		$hash_str = __FUNCTION__ . ':hash:com_id:' . $this -> com_id . ':user_id:' . $this -> user_id;
		$cache_hash = $this -> get_cache_key($hash_str);
		return $cache_hash;
	}

	/**
	 * 获取文件序列缓存子key
	 *
	 * @access public
	 * @param integer $number 序号
	 * @return string
	 */
	public function get_serial_key($number) {
		$key_str = __FUNCTION__ . ':key:com_id:' . $this -> com_id . ':user_id:' . $this -> user_id . ':number:' . $number;
		$cache_key = $this -> get_cache_key($key_str);
		return $cache_key;
	}

	/**
	 * 发送搜索帮助消息
	 *
	 * @access public
	 * @return TRUE
	 * @throws SCException
	 */
	public function send_help_info() {
		$result = g('nmsg') -> send_text($this -> help_tip, array(), array($this -> user_id));
		if (!$result) {
			throw new SCException('发送失败');
		}
		
		return TRUE;
	}

	/**
	 * 获取文件的收藏数
	 *
	 * @access public
	 * @param array $cf_ids 文件id集合
	 * @return array
	 */
	public function get_collect_count(array $cf_ids) {
		$cf_ids = implode(',', $cf_ids);
		$condition = $this -> base_con;
		$condition['cf_id IN '] = '(' . $cf_ids . ')';
		ksort($condition);

		$key_str = __FUNCTION__ . '::' . json_encode($condition);
		$key_arr = $this -> get_collect_cache_key($key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		$fields = array(
			'COUNT(id) as num', 'cf_id'
		);
		$fields = implode(',', $fields);

		$group_by = ' GROUP BY cf_id ';

		$ret = array();
		$result = g('ndb') -> select($this -> collect_table, $fields, $condition, 0, 0, $group_by);

		!is_array($result) and $result = array();

		foreach ($result as $val) {
			$ret[$val['cf_id']] = $val['num'];
		}
		unset($val);

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 获取收藏文件列表
	 *
	 * @access public
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param string $order 排序方式
	 * @param boolean $with_count 是否返回总记录数量
	 * @return array
	 */
	public function get_collected_list($page=1, $page_size=10, $order='', $with_count=true) {
		$condition = array(
			'clt.com_id=' => $this -> com_id,
			'clt.user_id=' => $this -> user_id,
			'^clt.cf_id=' => 'file.cf_id',
		);
		ksort($condition);

		$key_str = __FUNCTION__ . "::{$page}::{$page_size}::{$order}::{$with_count}";
		$key_arr = $this -> get_collect_cache_key($key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		$table = $this -> collect_table . ' AS clt, ' . $this -> file_table . ' AS file';
		$fields = array(
			'file.cf_id', 'file.name', 'file.extname',
			'file.filesize', 'clt.create_time',
			'file.hash', 'file.create_time AS file_create_time',
			'file.dload_able',
		);
		$fields = implode(',', $fields);

		empty($order) && $order = ' ORDER BY clt.cf_id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, $with_count);

		$tmp_data = &$ret;
		$with_count and $tmp_data = &$ret['data'];

		if (!empty($tmp_data)) {
			foreach ($tmp_data as &$val) {
				$val['filesize_desc'] = prase_humen_size($val['filesize']);
			}
			unset($val);
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 增加文件下载数
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @return array
	 */
	public function increase_download_count($cf_id) {
		$sql = <<<EOF
UPDATE {$this -> file_table} SET downloads=downloads+1
WHERE cf_id={$cf_id} AND com_id={$this -> com_id}
EOF;
		g('db') -> query($sql);

		$this -> flush_dirs_cache_by_files($cf_id);

		$this -> add_download_list($cf_id);
	}

	/**
	 * 获取目录及及其下全部子目录的映射信息
	 *
	 * @access public
	 * @param integer $cd_pid 父目录id, 0表示根目录
	 * @param boolean $to_tree true-返回树结构, false-返回平级结构
	 * @param boolean $with_privilege true-加入目录可见权限, false-不加入权限
	 * @return array
	 */
	public function get_dir_map($cd_pid=0, $to_tree=false, $with_privilege=false) {
		$cd_pid = intval($cd_pid);
		$cd_pid < 0 and $cd_pid = 0;

		$key_str = __FUNCTION__ . "::{$cd_pid}::{$to_tree}";
		$key_arr = $this -> get_dir_cache_key(0, $key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		$ret = array();

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}

		//生成树时，要加入目录可见权限；否则不加入权限
		$privilege = $with_privilege ? 2 : 0;
		$all_childs = $this -> get_all_child_dirs($cd_pid, $privilege);
		$ids_str = '(' . implode(',', $all_childs) . ')';

		$fields = array(
			'cd_id', 'cd_pid', 'name', 
		);
		$fields = implode(',', $fields);

		$condition = $this -> base_con;
		$condition['cd_id IN '] = $ids_str;
		$condition['state!='] = 2;

		$order_by = ' ORDER BY `sort` DESC, name_py ASC, cd_id DESC ';

		$result = g('ndb') -> select($this -> dir_table, $fields, $condition, 0, 0, '', $order_by);

		if (empty($result)) return $ret;

		$self_data = array();
		if ($cd_pid === 0) {
			$self_data = array(
				'cd_id' => 0,
				'name' => $this -> app_conf['cn_name'],
			);
		}

		foreach ($result as $val) {
			!isset($ret[$val['cd_pid']]) and $ret[$val['cd_pid']] = array();
			$ret[$val['cd_pid']][] = $val;

			$val['cd_id'] == $cd_pid and $self_data = $val;
		}
		unset($val);

		ksort($ret);

		if ($to_tree) {
			$counts = $this -> get_all_file_counts($with_privilege);
			$self_data['file_count'] = 0;
			isset($counts[$self_data['cd_id']]) and $self_data['file_count'] = $counts[$self_data['cd_id']];

			$self_data['id'] = $self_data['cd_id'];
			$self_data['children'] = $this -> perfect_dir_map($cd_pid, $ret, $counts);
			$ret = $self_data;
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 逆向获取全部父级别目录，包含自身
	 *
	 * @access public
	 * @param integer $cd_id 当前目录id
	 * @param array $map_arr 包含映射关系的数组，只能由递归赋值
	 * @return array
	 */
	public function get_parent_by_reverse($cd_id=0, array &$map_arr=array()) {
		$ret = array();

		$cd_id = intval($cd_id);
		$cd_id < 0 and $cd_id = 0;

		if ($cd_id == 0) return $ret;

		//是否递归调用
		$is_recurse = true;
		if (empty($map_arr)) {
			$key_str = __FUNCTION__ . "::{$cd_id}";
			$key_arr = $this -> get_dir_cache_key(0, $key_str);
			$hash = $key_arr['hash'];
			$cache_key = $key_arr['key'];

			try {
				$handler = g('redis') -> get_handler();
				$ret = $handler -> hGet($hash, $cache_key);
				if (!empty($ret)) {
					$ret = json_decode($ret, true);
					!is_array($ret) and $ret = array();
					return $ret;
				}
			}catch(Exception $e){}
			$map_arr = $this -> get_dir_map(0, false, false);

			if (empty($map_arr)) return $ret;

			$is_recurse = false;
		}

		$cd_pid = 0;
		foreach ($map_arr as $val) {
			foreach ($val as $cval) {
				if ($cval['cd_id'] != $cd_id) continue;

				$cd_pid = $cval['cd_pid'];
				$ret[] = $cval;
			}
			unset($cval);
		}
		unset($val);

		$tmp_arr = $this -> get_parent_by_reverse($cd_pid, $map_arr);
		$ret = array_merge($ret, $tmp_arr);

		if (!$is_recurse) {
			//翻转数组
			$ret = array_reverse($ret);

			try {
				$handler -> hSet($hash, $cache_key, json_encode($ret));
			}catch(Exception $e){}
		}

		return $ret;
	}

	/**
	 * 批量获取目录下的文件数量
	 *
	 * @access public
	 * @param array $cd_ids 目录id集合
	 * @return array
	 */
	public function get_file_count(array $cd_ids) {
		$all_count = $this -> get_all_file_counts(true);

		$ret = array();
		foreach ($cd_ids as $val) {
			$ret[$val] = 0;
			isset($all_count[$val]) and $ret[$val] = $all_count[$val];
		}
		unset($val);

		return $ret;
	}

	/**
	 * 获取已下载文件列表
	 *
	 * @access public
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param string $order 排序方式
	 * @param boolean $with_count 是否返回总记录数量
	 * @return array
	 */
	public function get_downloaded_list($page=1, $page_size=10, $order='', $with_count=true) {
		$condition = array(
			'dl.com_id=' => $this -> com_id,
			'dl.user_id=' => $this -> user_id,
			'^dl.cf_id=' => 'file.cf_id',
			'dl.info_state=' => 1,
		);
		ksort($condition);

		$key_str = __FUNCTION__ . "::{$page}::{$page_size}::{$order}::{$with_count}";
		$key_arr = $this -> get_download_cache_key($key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		} catch(Exception $e) {}

		$table = $this -> dl_table . ' AS dl, ' . $this -> file_table . ' AS file';
		$fields = array(
			'dl.id AS dl_id', 'file.cf_id', 'file.name', 'file.extname', 'file.filesize', 'dl.create_time', 'file.hash', 'file.create_time AS file_create_time',
		);
		$fields = implode(',', $fields);

		empty($order) && $order = ' ORDER BY dl.create_time DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, $with_count);
		
		log_write(json_encode($ret));

		$tmp_data = &$ret;
		$with_count and $tmp_data = &$ret['data'];

		if (!empty($tmp_data)) {
			foreach ($tmp_data as &$val) {
				$val['filesize_desc'] = prase_humen_size($val['filesize']);
			}
			unset($val);
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		} catch (Exception $e) {}

		return $ret;
	}

	/**
	 * 删除下载文件
	 *
	 * @access public
	 * @param integer $dl_id 下载记录id
	 * @return TRUE
	 * @throws SCException
	 */
	public function del_download_file($dl_id) {
		$check = $this -> check_downloaded($dl_id);
		if (!$check) {
			throw new Exception('未下载该文件，无需删除!');
		}

		$condition = $this -> base_con;
		$condition = array(
			'id=' => $dl_id,
			'com_id=' => $this -> com_id,
			'user_id=' => $this -> user_id,
			'info_state=' => 1,
		);

		$data = array(
				'info_state' => 0,
			);

		$result = g('ndb') -> update_by_condition($this -> dl_table, $condition, $data);

		if (!$result) {
			throw new SCException('删除失败，请稍后再试！');
		}

		$this -> flush_download_cache();

		return TRUE;
	}

	/**
	 * 检查文件是否已被下载
	 *
	 * @access public
	 * @param integer $dl_id 下载记录id
	 * @return boolean
	 */
	public function check_downloaded($dl_id) {
		$condition = array(
			'id=' => $dl_id,
			'info_state=' => 1,
		);

		$check = g('ndb') -> record_exists($this -> dl_table, $condition);

		return $check ? TRUE : FALSE;
	}

//----------------以下是私有实现--------------------------------
	/**
	 * 根据平级的目录映射关系，返回完整关系的目录结构
	 *
	 * @access private
	 * @param integer $cd_pid 父目录
	 * @param array $map 映射集合
	 * @param array $counts 目录文件数统计信息集合
	 * @return array
	 */
	private function perfect_dir_map($cd_pid, array &$map, array &$counts) {
		$ret = array();
		if (!isset($map[$cd_pid])) {
			return $ret;
		}

		$ret = $map[$cd_pid];

		foreach ($ret as &$val) {
			if (!is_array($val)) continue;

			$val['children'] = $this -> perfect_dir_map($val['cd_id'], $map, $counts);
			$val['id'] = $val['cd_id'];
			$val['file_count'] = 0;
			isset($counts[$val['id']]) and $val['file_count'] = $counts[$val['id']];
		}
		unset($val);

		return $ret;
	}
	
	/**
	 * 查询用户的可见目录
	 *
	 * @access private
	 * @param boolean $more_info 是否获取更多信息
	 * @param boolean $for_dir 是否获取目录可见权限
	 * @return array
	 * @throws SCException
	 */
	private function get_visible_dir($more_info=FALSE, $for_dir=false) {
		$condition = $this -> base_con;
		$condition['state='] = 1;

		$depts = $this -> get_user_dept();
		if (empty($depts)) {
			throw new SCException('获取用户所在部门失败！');
		}
		$groups = $this -> get_user_group($this -> user_id);
		// if (empty($groups)) {
		// 	throw new SCException('获取用户所在组别失败！');
		// }

		$tmp_con = array(
			'user_permission LIKE ' => '%"' . $this -> user_id . '"%',
		);
		foreach ($depts as $key => $dept) {
			$tmp_con['__' . $key . '__dept_permission LIKE '] = '%"' . $dept . '"%';
		}
		unset($dept);
		foreach ($groups as $key => $group) {
			$tmp_con['__' . $key . '__group_permission LIKE '] = '%"' . $group . '"%';
		}
		unset($group);

		$condition['__OR_1'] = $tmp_con;
		ksort($condition);

		//---读取缓存开始
		$key_str = __FUNCTION__ . '::' . json_encode($condition) . "::{$more_info}::{$for_dir}";
		$key_arr = $this -> get_dir_cache_key(0, $key_str);
		$hash = $key_arr['hash'];
		$cache_key = $key_arr['key'];

		$ret = array();
		try {
			$handler = g('redis') -> get_handler();
			$ret = $handler -> hGet($hash, $cache_key);
			if (!empty($ret)) {
				$ret = json_decode($ret, true);
				!is_array($ret) and $ret = array();
				return $ret;
			}
		}catch(Exception $e){}
		//---读取缓存结束

		if ($more_info) {
			$fields = array(
				'cd_id', 'name', 'create_time',
			);
		}else {
			$fields = array('cd_id');
		}
		$fields = implode(',', $fields);

		$result = g('ndb') -> select($this -> dir_table, $fields, $condition);
		if (empty($result)) {
			throw new SCException('获取目录失败！');
		}
		if ($more_info) {
			$ret = $result;
		}else {
			$ret = array();
			foreach ($result as $val) {
				$ret[] = $val['cd_id'];
			}
			unset($val);

			if (!empty($ret) and $for_dir) {
				$tmp_arr = array();
				foreach ($ret as $val) {
					$tmp_parent = $this -> get_parent_by_reverse($val);

					if (!is_array($tmp_parent)) continue;
					foreach ($tmp_parent as $cval) {
						isset($cval['cd_id']) and $tmp_arr[] = $cval['cd_id'];
					}
					unset($cval);
				}
				unset($val);

				$ret = array_unique($tmp_arr);
			}
		}

		try {
			$handler -> hSet($hash, $cache_key, json_encode($ret));
		}catch(Exception $e){}

		return $ret;
	}

	/**
	 * 添加权限查询条件
	 *
	 * @access private
	 * @param array $condition 查询条件集合
	 * @param boolean $for_dir 是否查询目录可见权限
	 * @return mixed 如果没有可查看权限，直接返回FALSE，否则返回组合完成的$condition
	 */
	private function merage_privilege_condition(array $condition, $for_dir=false) {
		try {
			$visible_dirs = $this -> get_visible_dir(false, $for_dir);
		}catch (SCException $e) {
			return false;
		}

		if (empty($visible_dirs)) return false;

		$ids_str = '(' . implode(',', $visible_dirs) . ')';
		$condition['__OR_9'] = array(
			'cd_id IN ' => $ids_str,
		);

		return $condition;
	}

	/**
	 * 获取当前用户的所有归属部门id集合
	 *
	 * @access private
	 * @return array
	 */
	private function get_user_dept($user_id=0) {
		empty($user_id) and $user_id = $this -> user_id;

		$str = __FUNCTION__.':user_id:' . $user_id;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$sql = <<<EOF
SELECT `dept_tree` 
FROM {$this -> user_table} 
WHERE id={$user_id} 
LIMIT 1 
EOF;
		$result = g('db') -> select_first_val($sql);
		$result = json_decode($result, TRUE);
		$dept_arr = array();
		if (is_array($result) && !empty($result)) {
			foreach ($result as $val) {
				foreach ($val as $cval) {
					$dept_arr[] = intval($cval);
				}
				unset($cval);
			}
			unset($val);
		}
		$dept_arr = array_unique($dept_arr);

		//用户部门信息缓存30秒
		g('redis') -> setex($cache_key, json_encode($dept_arr), 30);

		return $dept_arr;
	}

	// gch add
	/**
	 * 获取当前用户的所有归属组别id集合
	 *
	 * @access private
	 * @return array
	 */
	private function get_user_group($user_id=0) {
		$cond = array(
			'com_id=' => $this -> com_id,
			'info_state=' => 1,
			'user_list LIKE ' => '%"'.$user_id.'"%',
		);
		$ret = g('ndb') -> select($this -> group_table, '`id`', $cond);
		!is_array($ret) and $ret = array();

		$return = array();
		foreach ($ret as $value) {
			$return[] = $value['id'];
		}

		return $return;
		
	}

	/**
	 * 获取当前企业的全部目录的文件数量统计数
	 *
	 * @access private
	 * @param boolean $with_privilege 是否加入权限限制
	 * @return array
	 */
	private function get_all_file_counts($with_privilege=false) {
		$cache_key = $this -> get_count_cache_key();
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			$ret = json_decode($ret, true);
			!is_array($ret) and $ret = array();
			return $ret;
		}

		$table = $this -> dir_table . ' AS dir, ' . $this -> file_table . ' AS file';
		$fields = array(
			'dir.cd_id', 'dir.cd_pid', 
			'COUNT(file.cf_id) AS file_count',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'file.com_id=' => $this -> com_id,
			'file.state!=' => 2,
			'^dir.cd_id=' => 'file.cd_id',
		);

		$group_by = ' GROUP BY dir.cd_id ';

		$ret = array();
		$count_map = array();
		$parent_map = array();

		$result = g('ndb') -> select($table, $fields, $condition, 0, 0, $group_by);

		//获取全部可查看文件的子目录
		$all_childs = array();
		if ($with_privilege) {
			//加入文件可见权限
			$all_childs = $this -> get_all_child_dirs(0, 1);
		}

		if ($result) {
			foreach ($result as $val) {
				//对于无权限查看的目录，隐藏文件数量
				if (!$with_privilege or in_array($val['cd_id'], $all_childs)) {
					$count_map[$val['cd_id']] = (int)$val['file_count'];
				}else {
					$count_map[$val['cd_id']] = 0;
				}

				!isset($parent_map[$val['cd_pid']]) and $parent_map[$val['cd_pid']] = array();
				$parent_map[$val['cd_pid']][] = $val['cd_id'];
			}
			unset($val);

			//递归计算每个目录下的文件数量
			foreach ($result as $val) {
				!isset($ret[$val['cd_pid']]) and $ret[$val['cd_pid']] = $this -> count_in_recurse($count_map, $parent_map, $val['cd_pid']);
				!isset($ret[$val['cd_id']]) and $ret[$val['cd_id']] = $this -> count_in_recurse($count_map, $parent_map, $val['cd_id']);
			}
			unset($val);

			//非递归计算每个目录下的文件数量
			// $ret = $count_map;

			//缓存2小时
			g('redis') -> setex($cache_key, json_encode($ret), 2 * 3600);
		}

		return $ret;
	}

	/**
	 * 递归查找全部的子目录，返回结果包含传入的参数
	 *
	 * @access private
	 * @param mixed $cd_ids 目录id集合
	 * @param integer $with_privilege 是否加入权限控制: 0-不加入、1-加入文件可见权限、2-加入目录可见权限
	 * @return array
	 */
	private function get_all_child_dirs($cd_ids, $with_privilege=0) {
		!is_array($cd_ids) and $cd_ids = array($cd_ids);
		$ret = $cd_ids;

		$ids_str = '(' . implode(',', $cd_ids) . ')';

		$fields = array(
			'cd_id',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'cd_pid IN ' => $ids_str,
		);
		$condition = array_merge($condition, $this -> base_con);

		if ($with_privilege > 0) {
			$for_dir = false;
			$with_privilege === 2 and $for_dir = true;
			$condition = $this -> merage_privilege_condition($condition, $for_dir);
		}

		$result = g('ndb') -> select($this -> dir_table, $fields, $condition);
		!is_array($result) and $result = array();

		if (empty($result)) return $ret;

		$tmp_ret = array();
		foreach ($result as $val) {
			$tmp_ret[] = $val['cd_id'];
		}
		unset($val);

		$child_arr = $this -> get_all_child_dirs($tmp_ret, $with_privilege);

		$ret = array_merge($ret, $child_arr);
		$ret = array_unique($ret);
		
		return $ret;
	}

	/**
	 * 递归计算目录的文件数量
	 *
	 * @access private
	 * @param array $count_map 例如：array(cd_id => file_count, ...)
	 * @param array $parent_map 例如：array(cd_pid => array(cd_id, cd_id, ...), ...)
	 * @param integer $cd_id 目录id
	 * @return integer
	 */
	private function count_in_recurse(array &$count_map, array &$parent_map, $cd_id) {
		$ret = 0;

		if (isset($parent_map[$cd_id])) {
			foreach ($parent_map[$cd_id] as $val) {
				$ret += $this -> count_in_recurse($count_map, $parent_map, $val);
			}
			unset($val);
		}

		isset($count_map[$cd_id]) and $ret += $count_map[$cd_id];
		return $ret;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access private
	 * @param string $str 关键字符串
	 * @return string
	 */
	private function get_cache_key($str) {
		$key_str = SYSTEM_COOKIE_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取查询缓存变量
	 *
	 * @access private
	 * @param string $str 关键字符串
	 * @return string
	 */
	private function get_view_cache_key($str) {
		$day_str = date('Ymd'); //每天不同的key
		$key_str = __CLASS__ . ':' . __FUNCTION__ . ':' . $str . ':' . $day_str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取保存当前企业的全部目录文件数量统计的缓存变量名 （包含递归关系）
	 *
	 * @access private
	 * @return string
	 */
	private function get_count_cache_key() {
		$key_str = __FUNCTION__ . '::' . $this -> com_id;
		$ret = $this -> get_view_cache_key($key_str);
		
		return $ret;
	}

	/**
	 * 清除目录文件数量统计缓存变量
	 *
	 * @access private
	 * @return void
	 */
	private function flush_count_cache() {
		$key = $this -> get_count_cache_key();
		g('redis') -> delete($key);
	}

	/**
	 * 获取目录内容缓存变量，如果对应hash在缓存中不存在，则尝试创建之，并设置生存时间
	 *
	 * @access private
	 * @param integer $cd_id 目录id
	 * @param string $str key关键字
	 * @return mixed 如果$str为空，则返回字符串，否则返回数组（一个hash，一个cache_key）
	 */
	private function get_dir_cache_key($cd_id, $str="") {
		$hash_str = __FUNCTION__ . '::' . $this -> com_id . '::' . $cd_id;
		
		$hash = $this -> get_view_cache_key($hash_str);

		try {
			$check = g('redis') -> exists($hash);
			if (!$check) {
				$handler = g('redis') -> get_handler();
				//创建hash，设置生存时间
				$handler -> hSet($hash, '0', '1');
				$handler -> expire($hash, 3 * 3600);
			}
		}catch(Exception $e) {}

		if (empty($str)) return $hash;

		$key_str = $hash_str . '::' . $str;
		$key = $this -> get_view_cache_key($key_str);
		$ret = array(
			'hash' => $hash,
			'key' => $key,
		);

		return $ret;
	}

	/**
	 * 清除指定目录相关的缓存变量
	 *
	 * @access private
	 * @param array $cd_ids 目录id集合
	 * @return void
	 */
	private function flush_dirs_cache($cd_ids) {
		!is_array($cd_ids) and $cd_ids = array($cd_ids);

		foreach ($cd_ids as $val) {
			$hash = $this -> get_dir_cache_key($val);
			g('redis') -> delete($hash);
		}
		unset($val);

		$this -> flush_count_cache();

		//其他全局性的缓存，都保存在 根目录缓存下
		//所以每次都要清空根目录的哈希缓存
		$hash = $this -> get_dir_cache_key(0);
		g('redis') -> delete($hash);
	}

	/**
	 * 根据cd_ids 清除其父目录的相关缓存
	 *
	 * @access private
	 * @param array $cd_ids 目录id集合
	 * @param boolean $return 是否不执行清除，而是返回父目录id集合
	 * @return mixed
	 */
	private function flush_parent_dirs_cache($cd_ids, $return=false) {
		!is_array($cd_ids) and $cd_ids = array($cd_ids);
		$ids_str = implode(',', $cd_ids);

		$fields = array(
			'distinct cd_pid',
		);
		$fields = implode(',', $fields);

		$condition = $this -> base_con;
		$condition['cd_id IN '] = '(' . $ids_str . ')';

		$cd_pids = array();
		$result = g('ndb') -> select($this -> dir_table, $fields, $condition);

		!is_array($result) and $result = array();
		
		foreach ($result as $val) {
			$cd_pids[] = $val['cd_pid'];
		}
		unset($val);

		if ($return) return $cd_pids;

		if (empty($cd_pids)) return;

		$this -> flush_dirs_cache($cd_pids);
	}

	/**
	 * 根据指定文件id集合，清空缓存
	 *
	 * @access private
	 * @param array $cf_ids 文件id集合
	 * @param boolean $return 是否不执行清除，而是返回目录id集合
	 * @return mixed
	 */
	private function flush_dirs_cache_by_files($cf_ids, $return=false) {
		!is_array($cf_ids) and $cf_ids = array($cf_ids);
		$ids_str = implode(',', $cf_ids);

		$fields = array(
			'distinct cd_id',
		);
		$fields = implode(',', $fields);

		$condition = $this -> base_con;
		$condition['cf_id IN '] = '(' . $ids_str . ')';

		$cd_ids = array();
		$result = g('ndb') -> select($this -> file_table, $fields, $condition);
		!is_array($result) and $result = array();
		
		foreach ($result as $val) {
			$cd_ids[] = $val['cd_id'];
		}
		unset($val);

		if ($return) return $cd_ids;

		if (empty($cd_ids)) return;

		$this -> flush_dirs_cache($cd_ids);
	}

	/**
	 * 获取收藏缓存变量，如果对应hash在缓存中不存在，则尝试创建之，并设置生存时间
	 *
	 * @access private
	 * @param string $str 生成key的关键字
	 * @return mixed 如果$str为空，则返回字符串，否则返回数组（一个hash，一个cache_key）
	 */
	private function get_collect_cache_key($str="") {
		$hash_str = __FUNCTION__ . '::' . $this -> com_id . '::' . $this -> user_id;
		
		$hash = $this -> get_view_cache_key($hash_str);

		try {
			$check = g('redis') -> exists($hash);
			if (!$check) {
				$handler = g('redis') -> get_handler();
				//创建hash，设置生存时间
				$handler -> hSet($hash, '0', '1');
				$handler -> expire($hash, 1 * 3600);
			}
		}catch(Exception $e) {}

		if (empty($str)) return $hash;

		$key_str = $hash_str . '::' . $str;
		$key = $this -> get_view_cache_key($key_str);
		$ret = array(
			'hash' => $hash,
			'key' => $key,
		);

		return $ret;
	}

	/**
	 * 清除指定收藏相关的缓存变量
	 *
	 * @access private
	 * @return void
	 */
	private function flush_collect_cache() {
		$hash = $this -> get_collect_cache_key();
		g('redis') -> delete($hash);
	}

	/**
     * 对目录查询结果集补全是否包含子目录信息
     *
     * @access private
     * @param array $data 目录结果集
     * @return array
     */
    private function complete_children_info(array $data) {
        if (empty($data)) return $data;

        $map_arr = $this -> get_dir_map(0, false, true);
        foreach ($data as &$val) {
        	$val['have_children'] = 0;
        	if (isset($map_arr[$val['cd_id']]) and !empty($map_arr[$val['cd_id']])) {
        		$val['have_children'] = 1;
        	}
        }
        unset($val);

        return $data;
    }

    /**
     * 添加下载记录
     *
     * @access private
     * @param integer $cf_id 文件id
     * @param string $desc 下载描述，如：原图下载，快速搜索下载
     * @return boolean
     */
    private function add_download_list($cf_id, $desc='') {
    	$ret = false;

        if (empty($cf_id)) return $ret;

        $data = array(
        	'com_id' => $this -> com_id,
        	'user_id' => $this -> user_id,
        	'cf_id' => $cf_id,
        	'desc' => $desc,
        	'create_time' => time(),
        );

        $ret = g('ndb') -> insert($this -> dl_table, $data);

        return $ret;
    }

    /**
	 * 获取下载缓存变量，如果对应hash在缓存中不存在，则尝试创建之，并设置生存时间
	 *
	 * @access private
	 * @param string $str 生成key的关键字
	 * @return mixed 如果$str为空，则返回字符串，否则返回数组（一个hash，一个cache_key）
	 */
    private function get_download_cache_key($str="") {
    	$hash_str = __FUNCTION__ . '::' . $this -> com_id . '::' . $this -> user_id;

    	$hash = $this -> get_view_cache_key($hash_str);

    	try {
			$check = g('redis') -> exists($hash);
			if (!$check) {
				$handler = g('redis') -> get_handler();
				//创建hash，设置生存时间
				$handler -> hSet($hash, '0', '1');
				$handler -> expire($hash, 1 * 3600);
			}
		}catch(Exception $e) {}

    	if (empty($str)) return $hash;

    	$key_str = $hash_str . '::' . $str;
		$key = $this -> get_view_cache_key($key_str);
    	$ret = array(
			'hash' => $hash,
			'key' => $key,
		);

    	return $ret;
    }

    /**
	 * 清除指定下载相关的缓存变量
	 *
	 * @access private
	 * @return void
	 */
    private function flush_download_cache() {
    	$hash = $this -> get_download_cache_key();
    	g('redis') -> delete($hash);
    }
}

/* End of this file */
