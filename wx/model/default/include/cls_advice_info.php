<?php
/**
 * 意见反馈数据模型
 *
 * @author LiangJianMing
 * @create 2015-12-31
 */

class cls_advice_info {
	// 公司id
	private $com_id = 0;
	// 基础查询条件
	private $base_con = array();
	// 应用配置信息
	private $app_conf = array();

	// 反馈信息表
	private static $info_table = 'advice_info';
	// 反馈回复表
	private static $reply_table = 'advice_reply';
	// 反馈分类表
	private static $class_table = 'advice_class';
	// 反馈主题浏览状态表
	private static $state_table = 'advice_visit_state';
	// 反馈动态表
	private static $trend_table = 'advice_trend';
	// 反馈附件表
	private static $file_table = 'advice_file';

	// 用户信息表
	private static $user_table = 'sc_user';
	// 部门信息表
	private static $dept_table = 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;

		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
		$app_conf_url = SYSTEM_APPS . 'advice/config.php';
		file_exists($app_conf_url) and $this -> app_conf = include($app_conf_url);
		if (!empty($this -> com_id)) {
			g('nmsg') -> init($this -> com_id, $this -> app_conf['name']);
			try {
				$com_info = g('nmsg') -> get_com_info();
				$this -> corp_url = $com_info['corp_url'];
			}catch(SCException $e) {}
		}
	}


	/**
	 * 删除反馈
	 * @param   $advice_id
	 * @return 
	 */
	public function delete($advice_id){
		$cond = array(
				'id=' => $advice_id,
			);
		$data = array(
				'info_state' => 0,
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	/**
	 * 激活反馈
	 * @param   $advice_id
	 * @return 
	 */
	public function resend($advice_id){
		$cond = array(
				'id=' => $advice_id,
				'title_state=' => 2,
			);
		$data = array(
				'title_state' => 1,
				'state' => 2,
				'update_time' => time(),
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	/**
	 * 撤回
	 * @param   $advice
	 * @param   $user_id
	 * @return 
	 */
	public function withdraw($advice_id,$user_id){
		$cond = array(
				'user_id=' => $user_id,
				'id=' => $advice_id,
				'state=' => 0,
			);
		$data = array(
				'title_state' => 0,
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	/**
	 * 草稿状态反馈提交
	 * @param   $advice_id
	 * @param   $user_id
	 * @return 
	 */
	public function submit($advice_id,$user_id){
		$now = time();
		$cond = array(
				'id=' => $advice_id,
				'com_id=' => $this -> com_id,
			);
		$data = array(
				'create_time' => $now,
				'update_time' => $now,
				'title_state' => 1
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}


	/**
	 * 主导人关闭反馈
	 * @param   $advice_id
	 * @param   $user_id
	 * @return 
	 */
	public function close_advice($advice_id,$user_id){
		$cond = array(
				'id=' => $advice_id,
				'primary_id=' => $user_id,
			);
		$data = array(
				'state' => 5,
				'title_state' => 2,
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	/**
	 * 主导人关闭反馈
	 * @param   $advice_id
	 * @param   $user_id
	 * @return 
	 */
	public function finish($advice_id,$user_id){
		$cond = array(
				'id=' => $advice_id,
				'primary_id=' => $user_id,
			);
		$data = array(
				'state' => 4,
				'title_state' => 2,
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	/**
	 * 处理人主导反馈
	 * @param   $advice_id
	 * @param   $user_id
	 * @return 
	 */
	public function primary($advice_id,$user_id,$type=1){
		$cond = array(
				'id=' => $advice_id,
				'com_id=' => $this -> com_id,
				'primary_id=' => 0,
			);

		if($type == 1){
			$cond['primary_id='] = 0;
			$data = array(
				'primary_id' => $user_id,
				'state' => 2,
			);
		}else if($type == 2){
			$cond['primary_id='] = $user_id;
			$data = array(
				'primary_id' => 0,
				'state' => 1,
			);
		}else{
			return false;
		}

		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data);
		return $ret;
	}

	public function add_advice($data){
		$now = time();
		$data['com_id'] = $this -> com_id;
		$data['update_time'] = $now;
		$data['create_time'] = $now;
		$ret = g('ndb') -> insert(self::$info_table,$data);
		return $ret;
	}

	/**
	 * 修改反馈信息
	 * @param   $advice_id
	 * @param   $data
	 * @return 
	 */
	public function update_advice($advice_id,$data){
		$now = time();
		$cond = array(
				'id=' => $advice_id,
				'com_id=' => $this -> com_id,
			);
		$data['update_time'] = $now;
		$data['create_time'] = $now;
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data,false);
		return $ret;
	}

	/**
	 * 根据id获取反馈信息
	 **/
	public function get_advice_by_id($id,$fields = '*'){
		$cond = array(
				'id=' => $id,
				'info_state=' => 1,
			);
		$ret = g('ndb') -> select(self::$info_table,$fields,$cond);
		return empty($ret) ? false : $ret[0];
	}

	/**
	 * 获取反馈详情
	 * type 打开类型 1-发起人打开，2-处理人打开，3-监督人打开
	 **/
	public function get_advice_detail($id,$type){
		$cond = array(
				'i.id=' => $id,
				'i.info_state=' => 1
			);
		if($type == 1){
			$cond['i.user_id='] = $_SESSION[SESSION_VISIT_USER_ID];
		}else if($type == 2){
			$cond['c.handler_ids like '] = '%"'.$_SESSION[SESSION_VISIT_USER_ID].'"%';
		}else{
			$cond['c.supervisor_ids like '] = '%"'.$_SESSION[SESSION_VISIT_USER_ID].'"%';
		}

		$table = 'advice_info i LEFT JOIN advice_class c ON c.id = i.adc_id ';
		$fields = 'i.* ,c.weight,c.name class_name ,c.id class_id,c.handler_ids,c.supervisor_ids ';
		$ret = g('ndb') -> select($table,$fields,$cond);
		if(empty($ret)){
			cls_resp::show_err_page(array('反馈已被撤回或删除！'));
		}
		$advice = $ret[0];
		if($advice['title_state'] == 0 && $type != 1){
			cls_resp::show_err_page(array('反馈已被撤回或删除！'));	
		}

		$user_ids = array();
		array_push($user_ids,$advice['user_id']);

		$handler_ids = json_decode($advice['handler_ids'],true);
		$user_ids = array_merge($user_ids,$handler_ids);

		$supervisor_ids = json_decode($advice['supervisor_ids'],true);
		$user_ids = array_merge($user_ids,$supervisor_ids);

		$all_user = $this -> get_user_name2($user_ids);

		$advice['user'] = array();
		$advice['handler'] = array();
		$advice['supervisor'] = array();

		if($type == 1 || ($advice['staff_anonymous'] == 0 && $advice['ays'] == 0 && isset($all_user[$advice['user_id']]))){
			$user = $all_user[$advice['user_id']];
			$dept_list = json_decode($user['dept_list'],true);
			$dept_id = !empty($dept_list) ? array_shift($dept_list) : 0;
			$dept = g('dept') -> get_dept_by_id($dept_id,'name');
			$user['dept_name'] = $dept['name'];
			$advice['user'] = $user;
		}

		if($type ==2 && $advice['state']==0){
			$advice['state']==1;
		}

		if($advice['primary_id']!=0){
			$advice['primary'] = $all_user[$advice['primary_id']];
		}

		if($type != 1 || $advice['handler_anonymous'] == 0 && !empty($handler_ids)){
			foreach ($handler_ids as $h) {
				if(isset($all_user[$h])){
					array_push($advice['handler'],$all_user[$h]);
				}
			}unset($h);
		}
		if($type != 1 || $advice['supervisor_anonymous'] == 0 && !empty($supervisor_ids)){
			foreach ($supervisor_ids as $s) {
				if(isset($all_user[$s])){
					array_push($advice['supervisor'],$all_user[$s]);
				}
			}unset($s);
		}
		$file = $this -> get_file_by_advice($id,'id,file_name,file_size,file_path,file_hash,file_ext');
		$advice['file'] = $file;

		if($type==2){
            if(count($handler_ids) == 1){
                g('advice_info') -> primary($id,$handler_ids[0]);
				$advice['primary_id'] = $handler_ids[0];
            }
			g('advice_see') -> add_see_advice($id);
		}

		g('advice_see') -> add_see($advice['user_id'],$id);

		$advice['see'] = 1;
		return $advice;
	}

	/**
	 * 获取反馈列表
	 *
	 **/
	public function get_list($cond,$fields='*',$page=1,$page_size=20,$count=false,$com_id=0){
		$cond['i.com_id='] = empty($com_id) ? $this -> com_id : $com_id;

		$order = 'order by i.create_time desc';

		$table = ' advice_info i LEFT JOIN advice_class c ON i.adc_id = c.id ';

		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order,$count);
		return $ret;
	}

	/**
	 * 获取反馈列表
	 *
	 **/
	public function get_other_list($cond,$fields='*',$user_id,$page=1,$page_size=20,$count=false,$com_id=0){
		$cond['i.com_id='] = empty($com_id) ? $this -> com_id : $com_id;

		$order = 'order by i.state asc,c.weight asc,i.update_time desc ';

		$table = ' advice_info i LEFT JOIN advice_class c ON i.adc_id = c.id LEFT JOIN advice_see s ON i.id = s.advice_id and s.user_id = '.$user_id;

		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order,$count);
		return $ret;
	}


	/**
	 * 根据ID获取反馈分类ID
	 **/
	public function get_class_by_id($id,$fields=""){
		$cond = array(
				'id=' => $id,
				'com_id=' => $this -> com_id,
				'state=' => 1,
			);

		if(empty($fields)){
			$fields = array(
				'id', 'com_id', 'name',
				'dept_ids', 'weight','anonymous',
			);
			$fields = implode(',', $fields);
		}
		$order = ' ORDER BY weight DESC ';

		$ret = g('ndb') -> select(self::$class_table, $fields, $cond);
		return !empty($ret) ? $ret[0] : false;

	}

	/**
	 * 保存附件信息
	 **/
	public function save_file($data){
		$ret = g('ndb') -> insert(self::$file_table,$data,false);
		return $ret;
	}


	/**
	 * 用户下载文件
	 *
	 * @access public
	 * @param integer $cf_id 文件id
	 * @param string $size 为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
	 * @param boolean $is_push 是否强制通过微信推送
	 * @param boolean $increase 是否增加文件下载数
	 * @return TRUE
	 * @throws SCException
	 */
	public function download_file($advice_id,$cf_id, $size='', $is_push=false) {
		try {
			$cond = array(
				'ad_id=' => $advice_id,
				'id=' => $cf_id 
			);
			$fields = '*';
			$file = g('ndb') -> select(self::$file_table,$fields,$cond);
			$info = $file[0];
		}catch(SCException $e) {
			throw $e;
		}

		$platid = g('common') -> get_mobile_platid();
		$ad_download = g('media') -> android_download;
		$file_name = str_replace('.'.$info['file_ext'],"",$info['file_name']);
		$ext = strtolower($info['file_ext']);
		if (!$is_push) {
			//如果是ios平台支持的文件，或者是安卓平台下支持直接下载的文件类型，则直接跳转下载
			if (($platid === 1 and $ext != 'rar' and $ext != 'zip') or ($platid === 2 and in_array($ext, $ad_download))) {
				g('media') -> cloud_rename_download($file_name, $info['file_hash'], $size);
				return true;
			}
		}
		
		$file_name = $file_name . $ext;
		try {
			$media_info = g('media') -> get_media_id($this -> com_id, $this -> app_conf['combo'], $info['file_hash'], $file_name, $size);
			$media_id = $media_info['media_id'];
		}catch(SCException $e) {
			throw new SCException('系统正忙，请稍后再试！');
		}

		$file_type = g('media') -> get_file_type($ext);

		if ($file_type == 'video') {
			$result = g('nmsg') -> send_video($media_id, '', '', array(), array($_SESSION[SESSION_VISIT_USER_ID]));
		}else {
			$result = g('nmsg') -> send_files($file_type, $media_id, array(), array($_SESSION[SESSION_VISIT_USER_ID]));
		}
		
		if (!$result) {
			$err_tip = '系统正忙，请稍后再试！';
			throw new SCException($err_tip);
		}

		return true;
	}

	/**
	 * 删除附件信息
	 **/
	public function delete_file($advice_id){
		$cond = array(
				'ad_id=' => $advice_id,
			);
		$data = array(
				'info_state' => 0,
			);
		$ret = g('ndb') -> update_by_condition(self::$file_table,$cond,$data,false);
		return $ret;
	}
	
	public function get_file_by_advice($advice_id,$fields="*"){
		$cond = array(
				'ad_id =' => $advice_id,
				'info_state=' => 1,
			);
		$ret = g('ndb') -> select(self::$file_table,$fields,$cond);
		return $ret;

	}

    // ----------------------以下是内部实现-----------------------------------
    /**
     * 补全反馈分类的展示信息
     *
     * @access private
     * @param array $data 反馈分类的查询数据结果集
     * @return array
     */
    private function complete_class_list(array $data) {
    	if (empty($data)) {
    		return $data;
    	}

    	$all_dept_ids = array();
    	$all_user_ids = array();

    	foreach ($data as &$val) {
    		$dept_ids = json_decode($val['dept_ids'], true);
    		!is_array($dept_ids) and $dept_ids = array();
    		$all_dept_ids = array_merge($all_dept_ids, $dept_ids);

    		$handler_ids = json_decode($val['handler_ids'], true);
    		!is_array($handler_ids) and $handler_ids = array();
    		$all_user_ids = array_merge($all_user_ids, $handler_ids);

    		$supervisor_ids = json_decode($val['supervisor_ids'], true);
    		!is_array($supervisor_ids) and $supervisor_ids = array();
    		$all_user_ids = array_merge($all_user_ids, $supervisor_ids);

    		$val['dept_ids'] = $dept_ids;
    		$val['handler_ids'] = $handler_ids;
    		$val['supervisor_ids'] = $supervisor_ids;
    	}
    	unset($val);

    	$tmp_dept_ids = array();
    	foreach ($all_dept_ids as $val) {
    		$tmp_int = intval($val);
    		$tmp_int > 0 and $tmp_dept_ids[$val] = 1;
    	}
    	$all_dept_ids = array_keys($tmp_dept_ids);

    	$tmp_user_ids = array();
    	foreach ($all_user_ids as $val) {
    		$tmp_int = intval($val);
    		$tmp_int > 0 and $tmp_user_ids[$val] = 1;
    	}
    	$all_user_ids = array_keys($tmp_user_ids);

    	$dept_map = $this -> get_dept_name($all_dept_ids);
    	$user_map = $this -> get_user_name($all_user_ids);
    	foreach ($data as &$val) {
    		$val['dept_desc'] = '';
    		$val['handler_desc'] = '';
    		$val['supervisor_desc'] = '';

    		foreach ($val['dept_ids'] as $cval) {
    			isset($dept_map[$cval]) and $val['dept_desc'] .= "{$dept_map[$cval]}；";
    		}

    		foreach ($val['handler_ids'] as $cval) {
    			isset($user_map[$cval]) and $val['handler_desc'] .= "{$user_map[$cval]}；";
    		}

    		foreach ($val['supervisor_ids'] as $cval) {
    			isset($user_map[$cval]) and $val['supervisor_desc'] .= "{$user_map[$cval]}；";
    		}

    		$val['dept_desc'] = rtrim($val['dept_desc'], '；');
    		$val['handler_desc'] = rtrim($val['handler_desc'], '；');
    		$val['supervisor_desc'] = rtrim($val['supervisor_desc'], '；');
    	}
    	unset($val);

    	return $data;
    }

    /**
	 * 获取部门id与名称的映射数组
	 *
	 * @access private
	 * @param array $dept_ids 部门id集
	 * @return array
	 */
	private function get_dept_name(array $dept_ids) {
		$ret = array();
		if (empty($dept_ids)) return $ret;

		$dept_ids = implode(',', $dept_ids);
		$condition = array(
			'id IN ' => '(' . $dept_ids . ')',
		);

		$fields = array(
			'id', 'name',
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$dept_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val['name'];
			}
			unset($val);
		}

		return $ret;
	}

	/**
	 * 获取用户id与名称的映射数组
	 *
	 * @access private
	 * @param array $user_ids 用户id集
	 * @return array
	 */
	private function get_user_name(array $user_ids) {
		$ret = array();
		if (empty($user_ids)) return $ret;

		$user_ids = implode(',', $user_ids);
		$condition = array(
			'id IN ' => '(' . $user_ids . ')',
		);

		$fields = array(
			'id', 'name','pic_url',
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val['name'];
			}
			unset($val);
		}

		return $ret;
	}


	/**
	 * 获取用户id与名称的映射数组
	 *
	 * @access private
	 * @param array $user_ids 用户id集
	 * @return array
	 */
	private function get_user_name2(array $user_ids) {
		$ret = array();
		if (empty($user_ids)) return $ret;

		$user_ids = implode(',', $user_ids);
		$condition = array(
			'id IN ' => '(' . $user_ids . ')',
		);

		$fields = array(
			'id', 'name','pic_url','dept_list'
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val;
			}
			unset($val);
		}

		return $ret;
	}
}

/* End of this file */