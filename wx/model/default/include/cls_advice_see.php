<?php
/**
 * 意见反馈数据模型
 *
 * @author LiangJianMing
 * @create 2015-12-31
 */

class cls_advice_see {
	// 公司id
	private $com_id = 0;
	// 基础查询条件
	private $base_con = array();

	// 反馈信息表
	private static $info_table = 'advice_info';
	// 反馈回复表
	private static $reply_table = 'advice_reply';
	// 反馈分类表
	private static $class_table = 'advice_class';
	// 反馈主题浏览状态表
	private static $state_table = 'advice_visit_state';
	// 反馈动态表
	private static $trend_table = 'advice_trend';
	// 反馈附件表
	private static $file_table = 'advice_file';
	// 反馈阅读记录表
	private static $see_table = 'advice_see';

	// 用户信息表
	private static $user_table = 'sc_user';
	// 部门信息表
	private static $dept_table = 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;

		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
	}

	/**
	 * 处理人查看反馈信息
	 * @param user_id 
	 * @param advice_id
	 */
	public function add_see($user_id,$advice_id){
		$ret = $this -> get_see($user_id,$advice_id);
		if($ret){
			return true;
		}else{
			$data = array(
					'user_id' => $user_id,
					'advice_id' => $advice_id,
					'create_time' => time(),
				);
			$ret = g('ndb') -> insert(self::$see_table,$data);
			return $ret;
		}
	}

	/**
	 * 反馈信息被查看
	 * @param $advice_id 
	 */
	public function add_see_advice($advice_id){
		$cond = array(
				'id=' => $advice_id,
				'com_id=' => $this -> com_id,
				'state=' => 0,
			);
		$data = array(
				'state' => 1
			);
		$ret = g('ndb') -> update_by_condition(self::$info_table,$cond,$data,false);
		return $ret;
	}


	/**
	 * 获取查阅记录
	 * @param   $user_id
	 * @param   $advice_id
	 * @return 
	 */
	public function get_see($user_id,$advice_id){
		$cond = array(
				'user_id=' => $user_id,
				'advice_id=' => $advice_id
			);
		$fields = '*';
		$ret = g('ndb') -> select(self::$see_table,$fields,$cond);
		return empty($ret) ? false : $ret[0];
	}

}

/* End of this file */