<?php
/**
 * 套件
 * @author chenyihao
 * @date 2014-12-18
 *
 */
class cls_suite{
	
	private static $Table = 'sc_suite';
	
	/**
	 * 通过suite_id获取套件信息
	 * Enter description here ...
	 * @param unknown_type $suite_id	套件ID
	 */
	public function get_by_suite_id($suite_id){
		$fields ='*';
		$cond =array(
			'suite_id='=> $suite_id 
		);
		$result = g('ndb') ->select(self::$Table,$fields,$cond);
		return $result ? $result[0]:false;
	}
	
	
	/**
	 * 通过id获取套件信息
	 * Enter description here ...
	 * @param unknown_type $suite_id	套件ID
	 */
	public function get_by_id($id){
		$fields ='*';
		$cond =array(
			'id='=> $id 
		);
		return g('ndb') ->select(self::$Table,$fields,$cond);
	}
	
	/**
	 * 获取永久授权码
	 * Enter description here ...
	 * @param unknown_type $suite_id
	 * @param unknown_type $suite_access_token
	 * @param unknown_type $auth_token
	 */
	public function get_permanent_code($suite_id,$suite_access_token,$auth_token){
		try{
			return g('wxqy_suite') -> get_permanent_code($suite_id,$suite_access_token,$auth_token);
		}catch(SCException $e){
			throw $e;
		}
	}
	/**
	 * 通过token获取suite信息
	 * Enter description here ...
	 * @param unknown_type $token
	 */
	public function get_by_token($token){
		$fidles ='*';
		$cond = array(
			'token='=>$token 
		);
		$result = g('ndb') -> select(self::$Table,$fidles,$cond);
		if($result)
			return $result[0]; 
		else
			return false;
	}
	
	/**
	 * 根据条件获取套件
	 * Enter description here ...
	 * @param unknown_type $cond
	 */
	public function get($cond,$fidles='*'){
		$result =  g('ndb') -> select(self::$Table,$fidles,$cond);
		return $result ? $result[0]:false;
	}
	
	public function update_ticket($suite_id,$ticket){
		$cond =array(
			'suite_id='=>$suite_id
		);
		$data=array(
			'suite_ticket_date' => time(),
			'suite_ticket' => $ticket
		);
		
		return g('ndb') -> update_by_condition(self::$Table,$cond,$data);
	}
	
	/**
	 * 获取预授权码
	 * Enter description here ...
	 * @param unknown_type $suite_id
	 * @param unknown_type $app_ids
	 */
	public function get_pre_auth_code_in_app_ids($suite_id,$app_ids){
		try{
			$token = g('wxqy_suite') -> get_suite_access_token($suite_id);
			$pre_auth_code = g('wxqy_suite') ->get_pre_auth_code($suite_id,$token,$app_ids);
			return $pre_auth_code;
		}catch(SCException $e){
			throw new SCException('获取预授权码失败');
		}
		
	}
	
	/**
	 * 通过企业ID和套餐类型获取套件
	 * Enter description here ...
	 * @param unknown_type $company_id
	 * @param unknown_type $combo_type
	 * @throws Exception
	 * @throws SCException
	 */
	public function get_suite_by_company_id($company_id, $sie_id){
		try{
			$company = g('company') -> get_by_id($company_id);
			if(!$company)
				throw new Exception('不存在的企业', -1);

			$agt_id = 0;
			if($company['agt_id']!=0){
				$agency = g('agency_info') -> get_by_id($company['agt_id']);
				if(!$agency)
					throw new Exception('不存在的代理商', -2);
				$agt_id = $agency['oem_id'];
			}

			$cond = array(
				'agt_id=' =>$agt_id,
				'id=' => $sie_id,
			);	
			$suite = $this -> get($cond);
			if(!$suite)
				throw new Exception('不存在的套件', -3);
				
			return $suite;
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 获取预授权码
	 * Enter description here ...
	 * @param unknown_type $app_ids			//APPid数组
	 * @param unknown_type $company_id		//企业ID
	 * @param unknown_type $sie_id		//套餐类型
	 * @throws SCException
	 */
	public function get_pre_auth_code($app_ids,$company_id,$combo_type){
		//https://qy.weixin.qq.com/cgi-bin/3rd_auth?action=get&suite_id={$suite_id}&pre_auth_code={$pre_auth_code}&redirect_uri=http://tqy.easywork365.com/suite.php?a=redirect&state=
		try{
			if(!is_array($app_ids)){
				throw new SCException('appid格式错误');
			}
			
			$suite = $this -> get_suite_by_company_id($company_id, $sie_id);
			
			$suite_app_ids = array();			//套件中app的id数组
			foreach($app_ids as $app_id){
				$result = g('suite_app') -> get_suite_app_id($suite['id'], $app_id);
				if($result)
					array_push($suite_app_ids, $result['suite_app_id']);
				else
					log_write('不存在的应用：'.$app_id);
			}
			unset($app_id);
			unset($result);
			
			$pre_auth_code = $this ->  get_pre_auth_code_in_app_ids($suite['suite_id'], $suite_app_ids);
			return array(
				'suite_id' => $suite['suite_id'],
				'pre_auth_code' => $pre_auth_code
			);
			
		}catch(SCException $e){
			throw $e;
		}
	}
}

// end of file