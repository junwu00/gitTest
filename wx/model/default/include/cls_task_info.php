<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_info extends cls_task_base{
	
	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($task_info) {
		
		$starttime = strtotime($task_info['starttime']);
		$endtime = strtotime($task_info['endtime']);
		
		
		
		$data = array(
			'task_title' => $task_info['task_title'],
			'com_id' => $task_info['com_id'],
			'task_describe' => $task_info['task_describe'],
			'assign_id' => $task_info['assign_id'],
			'assign_name' => $task_info['assign_name'],
			'pid' => $task_info['pid'],
			'writer_id' => $task_info['writer_id'],
			'writer_name' => isset($task_info['writer_name'])?$task_info['writer_name']:"",
			'state' => $task_info['state'],
			'task_type' => $task_info['task_type'],
			'starttime' => $starttime,
			'endtime' => $endtime,
			'isfinish' => parent::$IsFinishOff,
			'info_state' => parent::$StateOn,
			'degree'=>$task_info['degree'],
			'createtime' => time(),
			'writer_com_id' =>$task_info['writer_com_id']
		);
		$ret = g('ndb') -> insert(parent::$Table, $data);
		if (!$ret) {
			throw new SCException('保存任务信息失败');
		}
		return $ret;
		
		
	}
	
	
	
	/**
	 * 更新任务信息
	 */
	public function update($task_info,$file_list=false){
		$role_id = parent::get_user_assign($task_info["id"], $task_info['writer_id']);
		if($role_id){
			$data = array(
				'task_title' => $task_info['task_title'],
				'task_describe' => $task_info['task_describe'],
				'state' => $task_info['state'],
				'task_type' => $task_info['task_type'],
				'starttime' => strtotime($task_info['starttime']),
				'endtime' => strtotime($task_info['endtime']),
				'degree'=>$task_info['degree']
			);
			$cond = array(
				'writer_id='=>$task_info['writer_id'],
				'id='=>$task_info["id"]
			); 
			$ret = g('ndb') -> update_by_condition(parent::$Table, $cond,$data);
			if (!$ret) {
				throw new SCException('更新任务信息失败');
			}
			
			return $ret;
		}else{
			throw new SCException('没有权限更新任务信息');
		}
	}
	
	
	
	/**
	 * 删除任务信息
	 */
	public function delete($task_info){
		$role_id = parent::get_user_assign($task_info["id"], $task_info['writer_id']);
		if($role_id){
			$data = array(
				'info_state' => parent::$StateOff,
				'info_time' =>time()
			);
			$cond = array(
				'writer_id='=>$task_info['writer_id'],
				'id='=>$task_info["id"]
			); 
			$ret = g('ndb') -> update_by_condition(parent::$Table, $cond,$data);
			if (!$ret) {
				throw new SCException('删除任务信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限删除任务信息');
		}
	}
	
	/**
	 * 草稿状态任务列表
	 */
	public function get_draft_list($user_id,$fields='*',$order_by='order by createtime',$in_turn='',$page=0,$page_size=0,$with_count = FALSE){
		$task_sql = "select ".$fields." from ".parent::$Table. 
		" where info_state=".parent::$StateOn." and state=".parent::$StateDraft." and writer_id='".$user_id."' ".
		$order_by." ".$in_turn;
		$limit_sql = $task_sql;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
	
		if ($with_count) {
			$sql = 'SELECT COUNT(1) as count FROM (' . $task_sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
			$this -> get_page_info($page, $page_size, $count ? $count['count'] : 0);
		}
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 分配任务列表
	 */
	public function get_assign_list($user_id,$isfinish=0,$fields='*',$order_by='order by createtime',$in_turn='',$page=0,$page_size=0,$with_count = FALSE){
		$task_sql = "select ".$fields." from ".parent::$Table. " task LEFT JOIN ".parent::$TableClassify." tc on task.task_type = tc.id LEFT JOIN ".parent::$TableUser." su on task.assign_id = su.id ".
		" where task.info_state=".parent::$StateOn." and task.state<>".parent::$StateDraft." and task.assign_id='".$user_id."' and task.isfinish=".$isfinish." ".
		$order_by." ".$in_turn;
		
		$limit_sql = $task_sql;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
	
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $task_sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
			$this -> get_page_info($page, $page_size, $count ? $count['count'] : 0);
		}
		return $ret ? $ret : FALSE;
	}
	
	
	
	/**
	 * 查询承办人/监督人列表
	 * $other_table=array("连接表","查询参数")
	 */
	public function get_other_task($other_condition,$fields='*',$order_by='order by createtime',$in_turn='',$page=0,$page_size=10,$with_count = FALSE){
		$task_sql = "select ".$fields." from ".parent::$Table." task LEFT JOIN ".parent::$TableClassify." tc on task.task_type = tc.id,".$other_condition[0]." other LEFT JOIN ".parent::$TableUser." su on other.".$other_condition[1]." = su.id ". 
		" where task.id=other.taskid ";
		foreach ($other_condition[2] as $key=>$value)
		{
			$task_sql = $task_sql." and ".$key.$value; 
		}
		$task_sql = $task_sql." ".$order_by." ".$in_turn;
		
		$limit_sql = $task_sql;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
	
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $task_sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
			$this -> get_page_info($page, $page_size, $count ? $count['count'] : 0);
		}
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 查询承办人/监督人列表
	 * $other_table=array("连接表","查询参数")
	 */
	public function get_helper_task($user_id,$other_table,$isfinish=0,$fields='*',$order_by='order by createtime',$in_turn='',$page=0,$page_size=0,$with_count = FALSE){
		$task_sql = "select ".$fields." from ".parent::$Table." task LEFT JOIN ".parent::$TableClassify." tc on task.task_type = tc.id,".$other_table[0]." other LEFT JOIN ".parent::$TableUser." su on other.perform_id = su.id ". 
		" where task.id=other.taskid and ".$other_table[1]." ='".$user_id.
		"' and task.isfinish=".$isfinish." and task.info_state=".parent::$StateOn." and task.state<>".parent::$StateDraft." ".
		$order_by." ".$in_turn;
		
		$limit_sql = $task_sql;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
	
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $task_sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
			$this -> get_page_info($page, $page_size, $count ? $count['count'] : 0);
		}
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 获取任务信息
	 */
	public function get_by_com($taskid,$userid,$fields='*',$states=false) {
		if($states==101){//新增任务 分配任务
			$is_role = parent::get_user_assign($taskid, $userid);
		}
		else if($states==102){//承办任务
			$is_role = parent::get_user_perform($taskid, $userid);
		}
		else if($states==103){//监督任务
			$is_role = parent::get_user_superintendent($taskid, $userid);
		}
		else if($states==104){//协办任务
			$is_role = parent::get_user_helper($taskid, $userid);
		}
		else{//默认没有权限控制
			$is_role = true;
		}
		if($is_role){
			$task_sql = "select ".$fields." from ".parent::$Table.
			" where id=".$taskid." and info_state=".parent::$StateOn;
			return g('db') -> select_one($task_sql);
		}
		else{
			throw new SCException('没有权限查看该任务信息'.$states);
		}
	}
	
	/**
	 * 查询子任务数字
	 */
	public function get_son_task_count($parent_task_id,$userid){
		$is_role = parent::get_user_role($parent_task_id, $userid);
		if($is_role==true){
			$task_sql = "select count(1) count from ".parent::$Table." where pid = ".$parent_task_id." and state!=0 and info_state=".parent::$StateOn;
			return g('db') -> select_one($task_sql);
		}
		else{
			return false;
		}
	}
	
	/**
	 * 查询子任务列表
	 */
	public function get_son_list($parent_task_id,$userid){
		$is_role = parent::get_user_role($parent_task_id, $userid);
		if($is_role==true){
			$task_sql = "select * from ".parent::$Table." where pid = ".$parent_task_id." and info_state=".parent::$StateOn;
			return g('db') -> select($task_sql);
		}
		else{
			return false;
		}
	}
	
	public function get_son_list_not_role($parent_task_id){
		$task_sql = "select * from ".parent::$Table." where pid = ".$parent_task_id." and info_state=".parent::$StateOn;
		return g('db') -> select($task_sql);
	}
	
	
	
	/**
	 * 查看父任务信息
	 */
	public function gey_parent_info($taskid,$parent_task_id,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$task_sql = "select * from ".parent::$Table." where id = ".$parent_task_id." and info_state=".parent::$StateOn;
			return g('db') -> select_one($task_sql);
		}
		else{
			return false;
		}
	}
	
	/**
	 * 申请完成 延期 退回 更新状态
	 */
	public function update_apply_info($taskid,$userid,$task_data){
		if(parent::get_user_perform($taskid, $userid)){
			$cond = array(
				'id='=>$taskid,
			); 
			$ret = g('ndb') -> update_by_condition(parent::$Table, $cond, $task_data);
			if (!$ret) {
				throw new SCException('更新任务信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('更新任务信息失败');
		}
	}
	
	/**
	 * 审批申请完成 延期 退回 更新状态
	 */
	public function update_approve_info($taskid,$userid,$task_data,$is_need_role=TRUE){
		if(!$is_need_role||parent::get_user_assign($taskid, $userid)){
			$cond = array(
				'id='=>$taskid,
			); 
			$ret = g('ndb') -> update_by_condition(parent::$Table, $cond, $task_data);
			if (!$ret) {
				throw new SCException('更新任务信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('更新任务信息失败');
		}
	}
	
	
	
	//--------------------------------------内部方法---------------------------------------------------------------
	
	/**
	 * 获取分页信息，并传递页码、当前页到smarty
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $total		总记录数
	 * @return 记录起始位置、页码
	 */
	private function get_page_info($page,$page_size, $total) {
		
		$page_count = ($total % $page_size == 0) ? ($total / $page_size) : ($total / $page_size + 1);	//总页数
		if (!$page || $page == NULL) {
			$page = 1;
		}
		$pages = array();	//页码，即1，2，3...
		for ($i=1; $i<=$page_count; $i++) {
			array_push($pages, $i);
		}
		g('smarty') -> assign('curr_page', $page);
		g('smarty') -> assign('pages', $pages);
	}
	
	
}

// end of file