<?php
/**
 * 上下级关系查询类
 * @author yangpz
 * @date 2015-12-22
 *
 */
class cls_api_leader_relationship extends abs_api_contact {
	/** 对应的库表名称 */
	private static $Table = 'sc_leader_relationship';
	
	/** 删除 0 */
	private static $StateDel = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/** 下级类型为成员 1 */
	private static $TypeUser = 1;
	/** 下级类型为部门 2 */
	private static $TypeDept = 2;

	public function __construct($conf) {
		empty($conf) && $conf = array();
		parent::__construct($conf);
	}
	
	/**
	 * 获取指定部门的所有领导
	 * @param unknown_type $dept_id		部门ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 */
	public function list_leader_by_dept_id($dept_id, $only_id=TRUE) {
		$cond = array(
			'com_id=' => $this -> com_id,
			'sub_id=' => $dept_id,
			'info_state=' => self::$StateOn,
			'sub_type=' => self::$TypeDept,
		);
		$ret = g('ndb') -> select(self::$Table, 'sup_user_id', $cond);
		$ret = $ret ? $ret : array();
		$user_ids = array();
		foreach ($ret as $item) {
			$user_ids[] = $item['sup_user_id'];
		}unset($item);
		$dept_ids = array();
		parent::_list_with_struct_filter_id($dept_ids, $user_ids);
		
		$api_user = $this -> _get_api_user_instance();
		$users = $api_user -> list_by_ids($user_ids, '*', FALSE);	//上面已进行架构过滤，此处不再过滤
		if ($only_id) {												//仅ID（由于成员可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			return $user_ids;
		}
		return $users;
	}
	
	/**
	 * 获取指定成员的所有领导（只取主部门上级+成员上级）
	 * @param unknown_type $dept_id		部门ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 * @param unknown_type $user_fields	查找的字段（成员）
	 */
	public function list_leader_by_user_id($user_id, $only_id=TRUE, $user_fields='id,name,pic_url') {
		$api_user = $this -> _get_api_user_instance();
		$dept_id = $api_user -> get_main_dept($user_id, TRUE);
		//1.查看该成员是否为某些部门上级，是的话剔除所有这些部门的上级（同部门上级之间不能互为上级，如需要为上下级关系，可增加人与人关联）
		$fields = '*';
		$cond = array(
			'com_id=' => $this -> com_id,
			'sup_user_id=' => $user_id,
			'sub_type=' => self::$TypeDept,
			'info_state=' => self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		$leader_dept_ids = array(0);		//为哪些部门的上级
		if (!empty($ret)) {
			foreach ($ret as $item) {
				$leader_dept_ids[] = $item['sub_id'];
			}unset($item);
			unset($ret);
		}
		$leader_dept_ids = implode(',', $leader_dept_ids);
		
		//2.获取成员所在部门的上级（这些也为成员上级） + 直接配置的成员的上组
		$table = self::$Table;
		$com_id = $this -> com_id;
		$state_on = self::$StateOn;
		$type_dept = self::$TypeDept;
		$type_user = self::$TypeUser;
		$dept_cond = ' 0 ';
		if (!empty($dept_id)) {
			$dept_cond = " ((sub_type={$type_dept} AND sub_id={$dept_id}) AND sub_id NOT IN ({$leader_dept_ids})) ";
		}
		$sql = <<<EOF
SELECT DISTINCT(sup_user_id) FROM {$table}
WHERE com_id={$com_id} AND info_state={$state_on} AND (
	{$dept_cond}
	OR (sub_type={$type_user} AND sub_id={$user_id})
)
EOF;
		$ret = g('db') -> select($sql);
		$ret = $ret ? $ret : array();
		$user_ids = array();
		foreach ($ret as $item) {
			$user_ids[] = $item['sup_user_id'];
		}unset($item);
		unset($ret);
		$dept_ids = array();
		parent::_list_with_struct_filter_id($dept_ids, $user_ids);
		
		$api_user = $this -> _get_api_user_instance();
		$users = $api_user -> list_by_ids($user_ids, $user_fields, FALSE);	//上面已进行架构过滤，此处不再过滤
		if ($only_id) {														//仅ID（由于成员可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			return $user_ids;
		}	
		return $users; 
	}
	
	/**
	 * 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用）
	 * @param unknown_type $user_id			成员ID
	 * @param unknown_type $only_ids		仅返回成员/部门ID 或 返回成员/部门信息，默认返回ID
	 * @param unknown_type $dept_fields		部门查找的字段
	 * @param unknown_type $user_fields		成员查找的字段
	 */
	public function list_sub_by_user_id($user_id, $only_ids=TRUE, $dept_fields='id,name', $user_fields='id,name,pic_url') {
		$fields='*';
		$cond = array(
			'com_id=' => $this -> com_id,
			'sup_user_id=' => $user_id,
			'info_state=' => self::$StateOn,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		$ret = $ret ? $ret : array();
		//没有记录
		if (empty($ret))	return array('users' => array(), 'depts' => array());
		//分离部门/成员记录
		$dept_ids = array();
		$user_ids = array();
		foreach ($ret as $item) {
			if ($item['sub_type'] == self::$TypeDept) {
				$dept_ids[] = $item['sub_id'];
			} else if ($item['sub_type'] == self::$TypeUser) {
				$user_ids[] = $item['sub_id'];
			}
		}unset($item);
		$dept_ids = array_unique($dept_ids);
		$user_ids = array_unique($user_ids);
		parent::_list_with_struct_filter_id($dept_ids, $user_ids);
		
		$api_user = $this -> _get_api_user_instance();
		$api_dept = $this -> _get_api_dept_instance();
		$users = $api_user -> list_by_ids($user_ids, $user_fields, FALSE);	//上面的ID已过滤，此处不再过滤
		$depts = $api_dept -> list_by_ids($dept_ids, $dept_fields, FALSE);	//上面的ID已过滤，此处不再过滤
		if ($only_ids) {													//仅ID（由于成员/部门可能被删除，此处需要查找成员表后再返回ID）
			$user_ids = array();
			$dept_ids = array();
			foreach ($users as $u) {
				$user_ids[] = $u['id'];
			}unset($u);
			foreach ($depts as $d) {
				$dept_ids[] = $d['id'];
			}unset($d);
			return array('users' => $user_ids, 'depts' => $dept_ids);
		}		
		return array('users' => $users, 'depts' => $depts);
	}

	//内部实现===============================================================================

	/**
	 * 根据当前信息，获取成员数据库操作类
	 */
	private function _get_api_user_instance() {
		$conf = $this -> api_conf;
		if ($this -> struct_filter) {	//使用已计算出的架构过滤信息，避免重复计算
			$conf['struct_use_conf'] = TRUE;
			$conf['struct_scope_all'] = $this -> struct_scope_all;
			$conf['struct_scope_read'] = $this -> struct_scope_read;
			$conf['struct_scope_write'] = $this -> struct_scope_write;
		}
		return g('api_user', $conf);
	}
	
	/**
	 * 根据当前信息，获取部门数据库操作类
	 */
	private function _get_api_dept_instance() {
		$conf = $this -> api_conf;
		if ($this -> struct_filter) {	//使用已计算出的架构过滤信息，避免重复计算
			$conf['struct_use_conf'] = TRUE;
			$conf['struct_scope_all'] = $this -> struct_scope_all;
			$conf['struct_scope_read'] = $this -> struct_scope_read;
			$conf['struct_scope_write'] = $this -> struct_scope_write;
		}
		return g('api_dept', $conf);
	}
}

//end