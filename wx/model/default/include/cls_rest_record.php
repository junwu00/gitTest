<?php
/**
 * 请假申请明细 
 * @author yangpz
 *
 */
class cls_rest_record {
	/** 对应的库表名称 */
	private static $Table = 'rest_record';
	
	private static $TableFormSetInst = 'mv_proc_formsetinst';

	private static $StateOn = 1;
	
	private static $StateOff = 0;
	
	
	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	private static $StateFormDraft = 0;
	/** 运行中 */
	private static $StateFormDone = 1;
	/** 已结束 */
	private static $StateFormFinish = 2;
	/** 终止 */
	private static $StateFormStop = 3;
	
	/**
	 * 保存申请记录
	 * @param unknown_type $reason
	 * @param unknown_type $type		请假类型，对应rest_type表中的id
	 * @param unknown_type $start_time	开始时间，yyyy-mm-dd hh:ii格式
	 * @param unknown_type $end_time	结束时间，yyyy-mm-dd hh:ii格式
	 * @param unknown_type $days		假期总天数
	 * @param unknown_type $hours		假期除去整数天数后的小时数
	 */
	public function save($reason, $type, $start_time, $end_time, $days, $hours,$formsetinst_id) {
		$data = array(
			'reason' => $reason,
			'type' => $type,
			'start_time' => strtotime($start_time),
			'end_time' => strtotime($end_time),
			'days' => $days,
			'hours' => $hours,
			'create_time' => time(),
			'update_time' => time(),
			'info_state' =>self::$StateOn,
			'com_id' => $_SESSION[SESSION_VISIT_COM_ID],
			'create_id' => $_SESSION[SESSION_VISIT_USER_ID],
			'formsetinst_id' =>$formsetinst_id
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存请假申请明细失败');
		}
		return $ret;
	}
	
	/**
	 * 更新申请记录
	 * @param unknown_type $id
	 * @param unknown_type $reason
	 * @param unknown_type $type		请假类型，对应rest_type表中的id
	 * @param unknown_type $start_time	开始时间，yyyy-mm-dd hh:ii格式
	 * @param unknown_type $end_time	结束时间，yyyy-mm-dd hh:ii格式
	 * @param unknown_type $days		假期总天数
	 * @param unknown_type $hours		假期除去整数天数后的小时数
	 */
	public function update($id, $reason, $type, $start_time, $end_time, $days, $hours) {
		$data = array(
			'reason' => $reason,
			'type' => $type,
			'start_time' => strtotime($start_time),
			'end_time' => strtotime($end_time),
			'days' => $days,
			'hours' => $hours,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update(self::$Table, $id, $data);
		if (!$ret) {
			throw new SCException('更新请假申请明细失败');
		}
		return $ret;
	}
	
	/**
	 * 根据实例id查询记录（一对一的关系）
	 * Enter description here ...
	 * @param unknown_type $formsetinst_id
	 */
	public function get_record_by_instid($formsetinst_id){
		$formsetinst_id = intval($formsetinst_id);
		$tb = self::$Table;
		$sql = <<<EOF
SELECT * 
FROM {$tb} 
WHERE formsetinst_id = {$formsetinst_id}
EOF;
		$ret = g('db') -> select($sql);
		
		return $ret ? $ret[0] : FALSE;
	}
	
	
	/**
	 * 获取员工已申请年假的时长（包括成功、审批中）
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $proc_type	流程类型
	 * @param unknown_type $type		请假类型
	 * @param unknown_type $start_time
	 * @param unknown_type $end_time
	 */
	public function get_year_times($com_id, $user_id, $type, $start_time, $end_time) {
		$start_time = intval($start_time);
		$end_time = intval($end_time);
		$start_year = date('Y', $start_time);
		$end_year = date('Y', $end_time);
		if ($start_year != $end_year) {
			throw new SCException('年假不能跨年');
		}
		
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$type = intval($type);
		$info_state = self::$StateOn;
		
		$year_start = strtotime($start_year.'0101000000');	//阳历的年初
		$year_end = strtotime($start_year.'1231235959');	//阳历的年末
		
		$tb = self::$Table;
		$tb_rec = self::$TableFormSetInst;
		$state = self::$StateFormDone.','.self::$StateFormFinish;
		$sql = <<<EOF
SELECT sum(days) as total_days, sum(hours) as total_hours 
FROM {$tb} r, {$tb_rec} pr
WHERE pr.com_id={$com_id} 
	and pr.state in({$state}) 
	and pr.info_state={$info_state} 
	and pr.id=r.formsetinst_id
	and r.create_id = {$user_id}
	and r.type={$type}
	and r.start_time>={$year_start}
	and r.end_time<={$year_end}
EOF;
		$ret = g('db') -> select($sql);
		return $ret ? $ret[0] : array('total_days' => 0, 'total_hours' => 0);
	}
	
	/**
	 * 当前的时间段，是否已申请（包括成功、审批中）过
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $start_time
	 * @param unknown_type $end_time
	 */
	public function is_date_applyed($com_id, $from_user_id, $start_time, $end_time,$formsetinst_id) {
		$com_id = intval($com_id);
		$from_user_id = intval($from_user_id);
		$start_time = intval($start_time);
		$end_time = intval($end_time);
		$formsetinst_id = intval($formsetinst_id);
		$state = self::$StateFormDone.','.self::$StateFormFinish;
		
		$tb = self::$Table;
		$tb_prec = self::$TableFormSetInst;
		$info_state = self::$StateOn;
		//a————————a1————————b1————————b			a1~b1为本次请求时间段
		//重复情况，即 a <= b1 && b >= a1
		$sql = <<<EOF
SELECT count(1) as cnt
FROM {$tb} r, {$tb_prec} pr
WHERE pr.com_id={$com_id}
	and pr.id=r.formsetinst_id
	and pr.info_state={$info_state}
	and pr.state in({$state})
	and r.start_time <= {$end_time}
	and r.end_time >= {$start_time}
	and r.create_id = {$from_user_id}
	and pr.id != {$formsetinst_id}
EOF;
		$ret = g('db') -> select($sql);
		return $ret[0]['cnt'] > 0;
	}
}

// end of file