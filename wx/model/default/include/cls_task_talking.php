<?php
/**
 * 任务评论信息
 * @author lijuns
 * @date 2014-12-05
 */
class cls_task_talking extends cls_task_base{
	
	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($talking_info) {
		$is_role = parent::get_user_role($talking_info['taskid'], $talking_info['talking_id']);
		if($is_role==true){
			$talking_data = array(
				'taskid' => $talking_info['taskid'],
				'talking_id' => $talking_info['talking_id'],
				'talking_name' => $talking_info['talking_name'],
				'talking_content' => $talking_info['talking_content'],
				'talking_time' => time()
			);
			$ret_talking = g('ndb') -> insert(parent::$TableTalking, $talking_data);
			if (!$ret_talking) {
				throw new SCException('保存评论信息失败');
			}
			return $ret_talking;
		}
		else{
			throw new SCException('没有权限保存评论信息');
		}
	}
	
	/**
	 * 查询评论列表
	 */
	public function select_list_taskid($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT tt.*,su.pic_url pic_url FROM '.parent::$TableTalking.' tt LEFT JOIN '.parent::$TableUser.' su on talking_id = su.id where taskid='.$taskid." order by talking_time desc";
			return g('db')->select($sql);
		}
		else{
			return false;
		}
	}
	
	/**
	 * 查询评论数目
	 */
	public function select_count_taskid($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT COUNT(1) as count FROM '.parent::$TableTalking.' where taskid='.$taskid;
			$count = g('db')->select_one($sql);
			return $count;
		}
		else{
			return false;
		}
	}
	
	
	
}

// end of file