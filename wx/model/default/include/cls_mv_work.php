<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_work {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_work_model';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/**
	 * 根据ID获取审批表单
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function get_by_id($com_id, $id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		
		return $ret ? $ret[0] : FALSE;
	}
	
	//---------------------------------------内部实现

	
}

// end of file