<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_report extends cls_task_base{

	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($report_info) {
		$is_role = (parent::get_user_perform($report_info['taskid'], $report_info['reporter_id'])||parent::get_user_helper($report_info['taskid'], $report_info['reporter_id']));
		if($is_role==true){
			$report_data = array(
			'taskid' => $report_info['taskid'],
			'reporter_id' => $report_info['reporter_id'],
			'reporter_name' => $report_info['reporter_name'],
			'report_content' => $report_info['report_content'],
			'report_time' => time(),
			);
			$ret = g('ndb') -> insert(parent::$TableReport, $report_data);
			if (!$ret) {
				throw new SCException('保存汇报信息失败');
			}
			
			return $ret;
		}
		else{
			throw new SCException('没有权限保存汇报信息');
		}
		
	}

	
	/**
	 * 查询汇报数目
	 */
	public function select_count_taskid($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT COUNT(1) as count FROM '.parent::$TableReport.' where taskid='.$taskid;
			$count = g('db')->select_one($sql);
			return $count;
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * 查询汇报列表
	 */
	public function select_list_taskid($taskid,$userid){
		$is_role =  parent::get_user_role($taskid, $userid);
		if($is_role==true){
			$sql = 'SELECT tr.*,su.pic_url pic_url FROM '.parent::$TableReport.' tr LEFT JOIN '.parent::$TableUser.' su on reporter_id = su.id where taskid='.$taskid." order by report_time desc";
			return g('db')->select($sql);
		}
		else{
			return false;
		}
	}
	
	
}

// end of file