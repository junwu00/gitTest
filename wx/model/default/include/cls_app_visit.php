<?php
/**
 * APP访问次数统计
 * @author yangpz
 * 
 */
class cls_app_visit {
	//定义统计来源
	private $src = 'wx';

	private $app_table = 'sc_app_visit';

	/**
	 * 检测当前的url类型，非ajax类型就增加浏览次数
	 *
	 * @access public
	 * @param integer $app_id 当前的应用id
	 * @return void
	 */
	public function check_increase($app_id) {
		if (!isset($_SESSION[SESSION_VISIT_COM_ID])) return;

		//判断是否ajax请求
		$check_1 = trim(get_var_value('m')) == 'ajax';
		$check_2 = get_var_value('ajax_act') !== NULL;
		$check_3 = strtolower($_SERVER['REQUEST_METHOD']) == 'post';

		if ($check_1 or $check_2 or $check_3) return;

		$this -> increase($_SESSION[SESSION_VISIT_COM_ID], $app_id);
	}

	/**
	 * 统计数增加
	 *
	 * @access public
	 * @param integer $com_id 企业id
	 * @param integer $app_id 应用id
	 * @param string $src 统计来源
	 * @return boolean
	 */
	public function increase($com_id, $app_id, $src='') {
		empty($src) and $src = $this -> src;

		$hash_str = date('Ymd') . ':' . $src;
		$hash = $this -> get_cache_key($hash_str);

		$cache_key = $com_id . ':' . $app_id;

		try {
			$result = g('redis') -> get_handler() -> hIncrBy($hash, $cache_key, 1);
		}catch(Exception $e) {}
		return $result;
	}

	/**
	 * 获取缓存中的统计信息
	 *
	 * @access public
	 * @param string $date 指定日期
	 * @return array
	 */
	public function get_cache_visit($date='') {
		empty($date) and $date = date('Ymd');

		$wx_src = $this -> src;
		$pc_src = 'pc';

		$ret = array(
			$wx_src => array(),
			$pc_src => array(),
		);

		//先获取wx的统计数据
		$hash_str = $date . ':' . $wx_src;
		$hash = $this -> get_cache_key($hash_str);

		$handler = g('redis') -> get_handler();
		$all_1 = $handler -> hGetAll($hash);
		!is_array($all_1) and $all_1 = array();

		//获取pc的统计数据
		$hash_str = $date . ':' . $pc_src;
		$hash = $this -> get_cache_key($hash_str);

		$handler = g('redis') -> get_handler();
		$all_2 = $handler -> hGetAll($hash);
		!is_array($all_2) and $all_2 = array();

		foreach ($all_1 as $key => $val) {
			$tmp_arr = explode(':', $key);
			if (count($tmp_arr) != 2) continue;

			!isset($ret[$wx_src][$tmp_arr[0]]) and $ret[$wx_src][$tmp_arr[0]] = array();
			$ret[$wx_src][$tmp_arr[0]][$tmp_arr[1]] = array(
				'com_id' => $tmp_arr[0],
				'app_id' => $tmp_arr[1],
				'count' => $val,
				'src' => $wx_src,
				'date' => $date,
			);
		}
		unset($val);

		foreach ($all_2 as $key => $val) {
			$tmp_arr = explode(':', $key);
			if (count($tmp_arr) != 2) continue;

			!isset($ret[$pc_src][$tmp_arr[0]]) and $ret[$pc_src][$tmp_arr[0]] = array();
			$ret[$pc_src][$tmp_arr[0]][$tmp_arr[1]] = array(
				'com_id' => $tmp_arr[0],
				'app_id' => $tmp_arr[1],
				'count' => $val,
				'src' => $pc_src,
				'date' => $date,
			);
		}
		unset($val);

		return $ret;
	}
	
	/**
	 * 保存指定日期的统计记录
	 *
	 * @access public
	 * @param string $date 指定日期，格式 'Ymd'
	 * @param string $src 统计来源
	 * @return void
	 */
	public function deal_by_day($date, $src='') {
		empty($src) and $src = $this -> src;

		$hash_str = $date . ':' . $src;
		$hash = $this -> get_cache_key($hash_str);

		$handler = g('redis') -> get_handler();
		$all = $handler -> hGetAll($hash);
		!is_array($all) and $all = array();

		$now_time = time();
		foreach ($all as $key => $val) {
			$tmp_arr = explode(':', $key);
			if (count($tmp_arr) != 2) continue;

			$tmp_data = array(
				'com_id' => $tmp_arr[0],
				'app_id' => $tmp_arr[1],
				'count' => $val,
				'src' => $src,
				'date' => $date,
				'create_time' => $now_time,
			);

			$result = g('ndb') -> insert($this -> app_table, $tmp_data);
			if (!$result) continue;

			//删除该key
			$handler -> hDel($hash, $key);
		}
		unset($val);

		$all = $handler -> hGetAll($hash);
		!is_array($all) and $all = array();
		//全部统计完毕后删除该hash
		count($all) == 0 and $handler -> delete($hash);
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}
}
//end