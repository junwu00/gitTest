<?php
/**
 * 企业成员查询类
 * 
 * @author yangpz
 * @date 2015-12-18
 *
 */
class cls_api_school_user_parent extends abs_api_contact {
	
	private static $Table = 'school_user_parents';
	/** 已删除 */
	private static $StateDelete = 0;
	
	public function __construct($conf=array()) {
		empty($conf) && $conf = array();
		parent::__construct($conf);
	}
	
	/**
	 * 根据员工ID，获取成员信息
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $state		成员状态，为空则查询未删除的成员
	 * @param unknown_type $fields		查找的字段
	 * 
	 * @return	成员信息（数组），找不到则返回FALSE
	 */
	public function get_by_id($id, $state=NULL, $fields='*') {
		$cond = array(
			'id=' => $id,
		);
		if (is_null($state)) {
			$cond['info_state!='] = self::$StateDelete;
		} else if (!empty($state)) {
			$cond['info_state='] = $state;
		}
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		$ret = $ret ? $ret[0] : FALSE;
		parent::_get_with_struct_filter_info($this -> filter_type_user, $ret);
		return $ret;
	}

}

//end