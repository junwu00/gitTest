<?php
/**
 * 考勤规则
 * @author yangpz
 * @date 2014-11-11
 */
class cls_cwork_rule {
	/** 对应库表名称 */
	private static $Table = 'cwork_rule';
	
	/** 禁用 */
	private static $StateOff = 0;
	/** 开启 */
	private static $StateOn = 1;
	
	/**
	 * 获取指定ID的规则
	 * @param unknown_type $com_id
	 * @param unknown_type $rule_id
	 * @param unknown_type $fileds
	 */
	public function get_by_id($com_id, $rule_id, $fields='*') {
		$cond = array(
			'id=' => $rule_id,
			'com_id=' => $com_id,
			'state=' => self::$StateOn,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($result) {
			$this -> decode_rule_info($result);
			return $result[0];
		}
		return FALSE;
	}
	
	//--------------------------------内部实现-------------------------------------
	
	/**
	 * 将规则中的JSON格式数据转换成数组格式
	 * @param unknown_type &$rule_list
	 */
	private function decode_rule_info(&$rule_list) {
		if ($rule_list == FALSE)	return;
		
		foreach ($rule_list as &$val) {
			$val['dept_list'] = g('dept') -> get_dept_name(json_decode($val['dept_list'], TRUE));
			$val['days'] = json_decode($val['days'], TRUE);
			$val['detail'] = json_decode($val['detail'], TRUE);
		}
	}
	
}

//end of file