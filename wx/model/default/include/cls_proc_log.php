<?php
/**
 * 审批日志
 * 
 * @author yangpz
 * @date 2014-12-10
 */
class cls_proc_log {
	/** 对应的库表名称 */
	private static $Table = 'proc_log';
	
	/** 审批中 0 */
	private static $StateWait = 0;
	/** 同意 1 */
	private static $StateAgree = 1;
	/** 驳回 2 */
	private static $StateBack = 2;
	/** 撤销 3 */
	private static $StateCancel = 3;
	/** 催办 4 */
	private static $StateWarn = 4;
	
	/**
	 * 保存审批记录
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_record_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $to_user_id
	 * @param unknown_type $step			流程到达的步骤
	 * @param unknown_type $state
	 * @param unknown_type $remark			同意/驳回/催办 理由
	 */
	public function save($com_id, $proc_record_id, $from_user_id, $to_user_id, $step, $state, $remark) {
		$data = array(
			'com_id' => $com_id,
			'proc_record_id' => $proc_record_id,
			'from_user_id' => $from_user_id,
			'to_user_id' => $to_user_id,
			'step' => $step,
			'state' => $state,
			'remark' => $remark,
			'create_time' => time(),
			'update_time' => time(),
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存审批日志记录失败');
		}
		return $ret;
	}
	
	/**
	 * 更新日志记录为 同意 状态
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_record_id
	 * @throws SCException
	 */
	public function change_to_agree($com_id, $proc_record_id) {
		$fields = 'max(id) as id';
		$cond = array(
			'com_id=' => $com_id,
			'proc_record_id=' => $proc_record_id,
			'state=' => self::$StateWait,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if (!$ret) {
			throw new SCException('找不到流程日志记录');
		}
		$id = $ret[0]['id'];
		$data = array(
			'state' => self::$StateAgree,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update(self::$Table, $id, $data);
		if (!$ret) {
			throw new SCException('更新流程日志记录状态失败');
		}
		return $ret;
	}
	
	/**
	 * 获取流程所有日志
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_record_id
	 * @param unknown_type $send_user_id	发起人id
	 * @param unknown_type $curr_user_id	查看人id
	 */
	public function get_all_log_list($com_id, $proc_record_id, $send_user_id, $curr_user_id) {
		$com_id = intval($com_id);
		$proc_record_id = intval($proc_record_id);
		$send_user_id = intval($send_user_id);
		$curr_user_id = intval($curr_user_id);
//		$state = self::$StateBack.','.self::$StateWarn;
		
		if ($send_user_id == $curr_user_id) {	//发起人，查看所有日志
			$sql = "select l.*, u1.pic_url as from_user_pic, u1.name as from_user_name, u2.pic_url as to_user_pic, u2.name as to_user_name 
				from proc_log l, sc_user u1, sc_user u2
				where l.com_id={$com_id}
					and l.proc_record_id={$proc_record_id}
					and l.from_user_id=u1.id 
					and l.to_user_id=u2.id
				order by update_time";
//					and l.state in({$state})";
			
		} else {								//非发起人，查看发起人与自己相关的记录
			$sql = "select l.*, u1.pic_url as from_user_pic, u1.name as from_user_name, u2.pic_url as to_user_pic, u2.name as to_user_name 
				from proc_log l, sc_user u1, sc_user u2
				where l.com_id={$com_id}
					and l.proc_record_id={$proc_record_id}
					and l.from_user_id=u1.id
					and l.from_user_id in({$send_user_id},{$curr_user_id}) 
					and l.to_user_id=u2.id
					and l.to_user_id in({$send_user_id},{$curr_user_id})
				order by update_time";
//					and l.state in({$state})";
		}
		return g('db') -> select($sql);
	}
	
	/**
	 * 获取审批流程(同一步骤中 非催办 的最后一条记录)
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_record_id
	 */
	public function get_process($com_id, $proc_record_id) {
		$com_id = intval($com_id);
		$proc_record_id = intval($proc_record_id);
		
		//取驳回记录的ID
		//只有一条，取所有记录
		//有多条，取最后一次驳回后的记录
		$fields = 'id';
		$cond = array(
			'com_id=' => $com_id,
			'proc_record_id=' => $proc_record_id,
			'state=' => self::$StateBack,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond, 0, 0, '', 'order by id desc');
		$extend_sql = '';
		if ($ret && count($ret) > 1) {
			$extend_sql = " and l.id>{$ret[1]['id']} ";	//倒数第二次驳回的记录 之后的记录
		}
		
		$sql = "select l.*, u1.pic_url as from_pic_url, u1.name as from_user_name, u2.pic_url as to_pic_url, u2.name as to_user_name 
			from proc_log l, sc_user u1, sc_user u2
			where l.com_id={$com_id}
				and l.proc_record_id={$proc_record_id}
				and l.from_user_id=u1.id
				and l.to_user_id=u2.id
				and l.id in(
					select max(id) from proc_log 
					where state != 4
					group by com_id, proc_record_id, step, l.state
				) {$extend_sql}";
		return g('db') -> select($sql);
	}
	
}

// end of file