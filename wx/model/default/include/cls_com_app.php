<?php
/**
 * 企业的应用配置
 * @author yangpz
 * @date 2014-11-14
 *
 */
class cls_com_app {
	/** 对应的库表名称 */
	private static $Table = 'sc_com_app';
	
	/** 企业信息库表名称 */
	private static $com_table = 'sc_company';
	/** 部门映射库表名称 */
	private static $dmap_table = 'sc_dept_map';
	/** 用户信息库表名称 */
	private static $user_table = 'sc_user';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	/** 未初始化菜单 0 */
	private static $NoMenu = 0;
	/** 已初始化菜单 1 */
	private static $HasMenu = 1;

	/**
	 * 获取应用的我方套件id
	 *
	 * @access public
	 * @param integer $app_id 我方定义的应用id
	 * @param integer $com_id 企业id
	 * @return boolean
	 */
	public function get_sie_id($app_id, $com_id=0) {
        return 0;
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			empty($com_id) and $com_id = $_SESSION[SESSION_VISIT_COM_ID];	
		}

		$key_str = __FUNCTION__ . "::{$com_id}:{$app_id}";
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);

		if ($ret !== false) return $ret;
		
		$table = self::$Table;

		$sql = <<<EOF
SELECT sie_id FROM {$table} 
WHERE com_id={$com_id} AND app_id={$app_id} AND state=1 
ORDER BY id DESC
LIMIT 1;
EOF;
		$sie_id = g('db') -> select_first_val($sql);
		!$sie_id and $sie_id = false;
		
		// 缓存1分钟
		g('redis') -> setex($cache_key, $sie_id, 60);
		return $sie_id;
	}
	
	/**
	 * 根据企业ID获取应用列表
	 * @param unknown_type $id	应用ID
	 */
	public function get_by_com($id, $state=NULL, $fields='*') {
		$cond = array(
			'com_id=' => $id,
		);
		!is_null($state) && $cond['state='] = $state;

		$key_str = __FUNCTION__ . "::" . json_encode($cond);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);

		if ($ret !== false) {
			$ret = json_decode($ret, true);
			!is_array($ret) and $ret = array();
			return $ret;
		}

		$ret = g('ndb') -> select(self::$Table, $fields, $cond);

		// 缓存1分钟
		g('redis') -> setex($cache_key, json_encode($ret), 60);

		return $ret;
	}
	
	/**
	 * 获取企业指定应用
	 * @param unknown_type $com_id
	 * @param unknown_type $app_id
	 */
	public function get_by_app($com_id, $app_id, $fields = '*') {
		$cond = array(
			'com_id=' => $com_id,
			'app_id=' => $app_id,
		);

		$key_str = __FUNCTION__ . "::" . json_encode($cond);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);

		if ($ret !== false) {
			$ret = json_decode($ret, true);
			!is_array($ret) and $ret = array();
			return $ret;
		}

		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		!empty($ret) and $ret = array_shift($ret);
		!is_array($ret) and $ret = array();

		// 缓存1分钟
		g('redis') -> setex($cache_key, json_encode($ret), 60);

		return $ret;
	}

	/**
	 * 获取所有已安装的应用信息
	 * @param unknown_type $comapny			企业ID
	 */
	public function get_all_auth_app($comapny, $fields='a.id id,a.`name` `name`'){
		try{
			$sql = "select {$fields} from sc_com_app c , sc_app a where  c.state = 1 and a.id = c.app_id and c.com_id = {$comapny} ";
			$ret = g('db')->select($sql);
			return $ret ? $ret : false;
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 根据token获取企业应用信息
	 * @param unknown_type $token
	 */
	public function get_by_token($token) {
		$fields = '*';
		$cond = array(
			'token=' => $token,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		return $result ? $result[0] : FALSE;
	}
	
	/**
	 * 获取APP配置：包括token，EncodingAESKey
	 */
	public function get_app_conf() {
		$data = array(
			'token' => get_rand_string(32),
			'aes_key' => get_rand_string(43),
		);
		return $data;
	}
	
	/**
	 * 获取所有未初始化应用菜单的应用
	 */
	public function get_unconf_menu_app() {
		$sql = 'select c.id as com_id, c.corp_id, c.corp_secret, c.corp_url, a.name as app_name, ca.wx_app_id, ca.id as com_app_id
			from sc_company c, sc_app a, sc_com_app ca 
			where c.id=ca.com_id and a.id=ca.app_id and ca.conf_menu=0';
		$ret = g('db') -> select($sql);
		return $ret;
	}
	
	/**
	 * 更新应用菜单配置状态
	 * @param unknown_type $com_app_id
	 * @param unknown_type $state
	 * @throws SCException
	 */
	public function change_menu_state($com_app_id, $state=1) {
		$data = array(
			'conf_menu' => $state,
		);
		$ret = g('ndb') -> update(self::$Table, $com_app_id, $data);
		if (!$ret) {
			throw new SCException('更新应用菜单配置状态失败');
		}
		return $ret;
	}
	
	/**
	 * 判断应用是否推送操作指引
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $app_id	应用ID
	 */
	public function is_send_guide($com_id, $app_id) {
		$fields = ' count(id) AS cnt ';
		$cond = array(
			'com_id=' => $com_id,
			'app_id=' => $app_id,
			'send_guide=' => 1
		);
		$cnt = g('ndb') -> select(self::$Table, $fields, $cond);
		return $cnt[0]['cnt'] > 0;
	}
	
	/**
	 * 获取缓存变量名
	 *
	 * @access private
	 * @param string $str 关键字符串
	 * @return string
	 */
	private function get_cache_key($str) {
		$key_str = SYSTEM_COOKIE_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

    /**
     * 更新应用的可见性（私有化使用）
     *
     * @access public
     * @param integer $com_id 企业id
     * @param array $agent_id 授权方应用的微信id
     * @param array $app_info
     * @return void
     */
    public function private_update_visible($com_id, $agent_id, array $app_info, $sc_app_id=0) {
        log_write('更新应用可见范围');

        $visit_arr = array();
        $dept_ids = array();
        $user_ids = array();

        !isset($visit_arr[$agent_id]) and $visit_arr[$agent_id] = array(
            'dept_ids' => array(),
            'user_ids' => array(),
        );

        //log_write('更新应用可见范围，allow_userinfos=' . json_encode($result['allow_userinfos']));

        if (isset($app_info['allow_userinfos']['user']) and is_array($app_info['allow_userinfos']['user'])) {
            foreach ($app_info['allow_userinfos']['user'] as $cval) {
                $tmp_id = $cval['userid'];
                if (empty($tmp_id)) continue;

                $visit_arr[$agent_id]['user_ids'][] = $tmp_id;
                $user_ids[] = $tmp_id;
            }
            unset($cval);

            //log_write('更新应用可见范围，user_ids=' . json_encode($user_ids));
        }

        if (isset($app_info['allow_partys']['partyid']) and is_array($app_info['allow_partys']['partyid'])) {
            foreach ($app_info['allow_partys']['partyid'] as $cval) {
                $tmp_id = intval($cval);
                if (empty($tmp_id)) continue;

                $visit_arr[$agent_id]['dept_ids'][] = $tmp_id;
                $dept_ids[] = $tmp_id;
            }
            unset($cval);
        }

        $map_depts = array();
        !empty($dept_ids) and $map_depts = $this -> get_map_depts($com_id, $dept_ids);

        $map_users = array();
        !empty($user_ids) and $map_users = $this -> get_map_users($com_id, $user_ids);

        $base_con = array(
            'com_id=' => $com_id,
        );
        $time = time();
        foreach ($visit_arr as $key => $val) {
            $tmp_con = $base_con;
            $tmp_con['wx_app_id='] = $key;

            $visit_depts = array();
            $visit_users = array();

            foreach ($val['dept_ids'] as &$cval) {
                $cval = (string)$cval;
                if (!isset($map_depts[$cval])) continue;
                $visit_depts[] = strval($map_depts[$cval]);
            }
            unset($cval);

            foreach ($val['user_ids'] as $cval) {
                $cval = (string)$cval;
                if (!isset($map_users[$cval])) continue;
                $visit_users[] = strval($map_users[$cval]);
            }
            unset($cval);

            $tmp_data = array(
                'visit_depts' => json_encode($visit_depts),
                'visit_users' => json_encode($visit_users),
                'allow_dept' => json_encode($val['dept_ids']),
                'allow_user' => json_encode($val['user_ids']),
                'update_time' => $time,
            );

            g('ndb') -> update_by_condition(self::$Table, $tmp_con, $tmp_data);
            log_write('更新应用可见范围成功，condition=' . json_encode($tmp_con) . ', data=' . json_encode($tmp_data));

            if($sc_app_id == 23){
                //获取子应用进行安装初始化
                $this->install_app($com_id, 24, $visit_depts, $visit_users);
            }

        }
        unset($val);

        return;
    }

    /**
     * 安装子应用
     * @param $com_id
     * @param $app_id
     * @param array $dept_list
     * @param array $user_list
     * @return mixed
     */
    private  function install_app($com_id, $app_id, $dept_list=array(), $user_list=array()){
        $cond = array(
            'com_id=' => $com_id,
            'app_id=' => $app_id,
        );

        $app = g('ndb')->select('sc_com_child_app', 'id',$cond);
        if(empty($app)){
            g('ndb')->insert('sc_com_child_app', array(
                    'com_id'=>$com_id,
                    'app_id'=>$app_id,
                    'state'=>1,
                )
            );
        }
        $data = array(
            'state' => 1,
            'install_time' => time(),
            'visit_depts' => json_encode($dept_list),
            'visit_users' => json_encode($user_list)
        );
        $ret = g('ndb')->update_by_condition('sc_com_child_app',$cond, $data);
        return $ret;
    }

    /**
     * 根据微信方的部门id集合，查询我方的映射部门id集合
     *
     * @access public
     * @param integer $com_id 企业id
     * @param mixed $ids 可为array|string，微信方的部门id集合
     * @return array
     */
    private function get_map_depts($com_id, $ids) {
        $ret = array();

        $root_id = $this -> get_root_id($com_id);
        if (empty($root_id)) return $ret;

        is_array($ids) and $ids = implode(',', $ids);
        $condition = array(
            'root_id=' => $root_id,
            'wx_id IN ' => '(' . $ids . ')',
        );

        $fields = array(
            'wx_id', 'id',
        );
        $fields = implode(',', $fields);

        $data = g('ndb') -> select(self::$dmap_table, $fields, $condition);
        !is_array($data) and $data = array();


        foreach ($data as $val) {
            $ret[$val['wx_id']] = $val['id'];
        }
        unset($val);

        //合并dept表的wx_id
        $d_data = g('ndb') -> select('sc_dept', $fields, $condition);
        !is_array($d_data) and $d_data = array();
        foreach ($d_data as $val) {
            $ret[$val['wx_id']] = $val['id'];
        }
        unset($val);

        return $ret;
    }

    /**
     * 根据微信方的用户acct集合，查询我方的映射用户id集合
     *
     * @access public
     * @param integer $com_id 企业id
     * @param array $accts 微信方的用户accts集合
     * @return array
     */
    private function get_map_users($com_id, $accts) {
        $ret = array();

        $root_id = $this -> get_root_id($com_id);
        if (empty($root_id)) return $ret;

        $ids_str = '';
        foreach ($accts as $val) {
            $ids_str .= '\'' . $val . '\',';
        }
        unset($val);
        !empty($ids_str) and $ids_str = substr($ids_str, 0, -1);

        $condition = array(
            'root_id=' => $root_id,
            'acct IN ' => '(' . $ids_str . ')',
        );

        //log_write('更新应用可见范围，map_users_condition=' . json_encode($condition));

        $fields = array(
            'id', 'acct',
        );
        $fields = implode(',', $fields);

        $data = g('ndb') -> select(self::$user_table, $fields, $condition);
        !is_array($data) and $data = array();

        //log_write('更新应用可见范围，map_users=' . json_encode($data));

        $ret = array();
        foreach ($data as $val) {
            $ret[$val['acct']] = $val['id'];
        }
        unset($val);

        return $ret;
    }

    /**
     * 获取部门的root_id
     *
     * @access private
     * @param integer $com_id 企业id
     * @return integer
     */
    private function get_root_id($com_id) {
        $sql = 'SELECT dept_id FROM ' . self::$com_table . ' WHERE id=' . $com_id . ' LIMIT 1';
        $root_id = g('db') -> select_first_val($sql);
        empty($root_id) and $root_id = 0;
        return $root_id;
    }

}

// end of file