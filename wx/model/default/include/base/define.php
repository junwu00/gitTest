<?php
/** 
 * 全局变量定义文件 
 * 
 * 所有的自定义的全局变量都在这里定义
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED!');
$G_CONF = require (dirname(dirname(SYSTEM_ROOT))) . '/config/env/env.now.config.php';

//定义图片需要缩放处理的默认尺寸集合
$DEFAULT_IMG_SIZES = array(
    'x-large' => '1080x894',
    'large' => '640x512',
    'middle' => '215x152',
    'small' => '62x47',
);
$GLOBALS['DEFAULT_IMG_SIZES'] = $DEFAULT_IMG_SIZES;

//定义html页面的全局信息
$PAGEINFO = array(
	//html页面的title
	'title'			=>		'广州三尺科技管理后台 - '.SYSTEM_VERSION,
	//html页面的ico
	'favicon'		=>		'/static/images/favicon.ico',
	//html页面的关键字
	'keywords'		=>		'三尺科技',
	//html页面的描述
	'description'	=>		'三尺科技',
	//html页面的robots
	'robots'		=>		'all',
	//html页面的作者
	'author'		=>		'三尺科技',
	//html页面的编码
	'charset'		=>		'utf-8',
);
//将数据添加到全局数组中
if(!isset($GLOBALS['PAGEINFO'])) $GLOBALS['PAGEINFO'] = $PAGEINFO;

//-----------------------------多媒体资源服务-----------------------
// 调用者
define('MEDIA_CALLER', $G_CONF['MEDIA_CALLER']);
/** 多媒体资源服务的加密后缀 */
define('MEDIA_URL_KEY', $G_CONF['MEDIA_URL_KEY']);
/** 多媒体资源服务来源标识 */
define('MEDIA_SRC', $G_CONF['MEDIA_SRC']);
/** 多媒体资源服务的host地址 */
define('MEDIA_URL_HOST', $G_CONF['MEDIA_URL_HOST']);
/** 多媒体资源服务的cdn地址 */
define('CDN_MEDIA_URL_HOST', $G_CONF['CDN_MEDIA_URL_HOST']);
/** 多媒体资源url前缀 */
define('MEDIA_URL_PREFFIX', $G_CONF['MEDIA_URL_PREFFIX']);

/** 多媒体预览调用者 */
define('PREVIEW_CALLER', $G_CONF['MEDIA_PREVIEW_CALLER']);
/** 多媒体预览KEY */
define('PREVIEW_URL_KEY', $G_CONF['MEDIA_PREVIEW_KEY']);
/** 多媒体预览链接 */
define('PREVIEW_URL_PREFFIX', $G_CONF['MEDIA_PREVIEW_PREFFIX']);

/** 领签项目地址 */
define('SIGN_URL_HOST', $G_CONF['SIGN_URL_HOST']);
/** 领签项目静态资源地址 */
define('SIGN_MEDIA_URL_HOST', $G_CONF['SIGN_MEDIA_URL_HOST']);

/** 过期的media域名 */
$INVALID_CDN_MEDIA_URL_HOST = array(
);
if(!isset($GLOBALS['INVALID_CDN_MEDIA_URL_HOST'])) $GLOBALS['INVALID_CDN_MEDIA_URL_HOST'] = $INVALID_CDN_MEDIA_URL_HOST;

/** 消息服务交互密钥 */
define('MESSAGER_KEY', $G_CONF['MESSAGER_KEY']);
/** 消息服务订阅前缀 */
define('MESSAGER_PREFFIX', $G_CONF['MESSAGER_PREFFIX']);

//------------------------------------官方技术支持信息----------------------------
define('SUPPORT_INFO',			$G_CONF['MAIN_COPYRIGHT']);
define('SUPPORT_URL',			(isset($G_CONF['IS_PRIVATE']) && $G_CONF['IS_PRIVATE'] == 1) ? '' : 'http://www.easywork365.com');

//--------------------------------------定义JS与CSS的版本时间-------------------
define('CSS_JS_DATE', $G_CONF['DEF_STATIC_VER']);//'2014.08.10123131');

//--------------------------------------定义当前访问的员工的信息---------------
/** 当前访问的ip */
define('SESSION_VISIT_IP',			'session_visit_ip');
/** 当前访问的企业号的corp url */
define('SESSION_VISIT_CORP_URL',	'session_visit_corp_url');
/** 当前访问的企业号的com id */
define('SESSION_VISIT_COM_ID',		'session_visit_com_id');
/** 当前访问的企业号根部门的id */
define('SESSION_VISIT_DEPT_ID',		'session_visit_dept_id');
/** 当前访问的企业号的corp id */
define('SESSION_VISIT_CORP_ID',		'session_visit_corp_id');
/** 当前访问的企业号的corp secret */
define('SESSION_VISIT_CORP_SECRET',	'session_visit_corp_secret');
/** 当前访问的员工的id */
define('SESSION_VISIT_USER_ID',		'session_visit_user_id');
/** 当前访问的员工的姓名 */
define('SESSION_VISIT_USER_NAME',	'session_visit_user_name');
/** 当前访问的员工的头像URL */
define('SESSION_VISIT_USER_PIC',	'session_visit_user_pic');
/** 当前访问的员工的企业号账号 */
define('SESSION_VISIT_USER_WXACCT',	'session_visit_user_wxacct');
/** 当前访问的员工的授权信息 */
define('SESSION_VISIT_USER_WXINFO',	'session_visit_user_wxinfo');
/** 当前访问的员工的微信号 */
define('SESSION_VISIT_USER_WXID',	'session_visit_user_wxid');
/** 当前访问的员工的主部门的id */
define('SESSION_VISIT_USER_DEPT',	'session_visit_user_dept_id');
/** 技术支持的文本内容 */
define('SESSION_VISIT_POWERBY',		'session_visit_powerby');
/** 技术支持的链接地址 */
define('SESSION_VISIT_POWERBY_URL',	'session_visit_powerby_url');
/** 企业的永久授权码 */
define('SESSION_VISIT_AUTH_CODE',	'session_visit_auth_code');
/** 当前访问的员工的直属及上级部门 */
define('SESSION_VISIT_USER_DEPT_PARENT_LIST',	'session_visit_user_dept_parent_list');


/** 最后一次访问的企业号的corp url */
define('COOKIE_VISIT_LAST_CORP_URL','session_visit_last_url');

#---begin------OPEN ID相关-----------------------------
define('PUBLIC_APP_ID', '');
define('PUBLIC_APP_SECRET', '');
#---end--------OPEN ID相关-----------------------------


//-------------------------------访问统计-----------------------------------
/** 统一缓存KEY */
define('REDIS_ACCESS_KEY', $G_CONF['REDIS_ACCESS_KEY']); //MD5(access_key_ew365)
/** 访问端类型 */
define('REDIS_ACCESS_TYPE', $G_CONF['REDIS_ACCESS_TYPE']);

//----------------------------私有化授权相关-------------------------------------
if (isset($G_CONF['IS_PRIVATE']) && $G_CONF['IS_PRIVATE'] == 1) {
    define('API_AUTH_SRC', $G_CONF['API_AUTH_SRC']);
    define('API_AUTH_CALLER', $G_CONF['API_AUTH_CALLER']);
    define('API_AUTH_SECRET', $G_CONF['API_AUTH_SECRET']);
    define('API_AUTH_URL', $G_CONF['API_AUTH_URL']);
}

/* End of file define.php */ 