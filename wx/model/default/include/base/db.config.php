<?php
/** 
 * 数据库连接配置
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

!defined('SYSTEM') && die('ACCESS DENIED');
$db_file = dirname(dirname(dirname(SYSTEM_ROOT))) . '/install_conf/db.config.json';
$DB_CONFIG_LIST = json_decode(file_get_contents($db_file), true);
!isset($DB_CONFIG_LIST['charset']) && $DB_CONFIG_LIST['charset'] = 'utf8';
$DB_CONFIG_LIST = array($DB_CONFIG_LIST);
/*
$DB_CONFIG_LIST = array(
	array(
		'host' 		=> 'localhost',
		'port' 		=> '3306',
		'user' 		=> 'admin',
		'pass' 		=> 'adminpass',
		'name' 		=> 'd_ew365_private',
		'charset' 	=> 'utf8',
	)
);
*/
!isset($GLOBALS['DB_CONFIG_LIST']) && $GLOBALS['DB_CONFIG_LIST'] = $DB_CONFIG_LIST;

/* End of file db.config.php */ 