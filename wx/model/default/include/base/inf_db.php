<?php
/** 
 * 所有的数据库操作基类都必须实现该接口 
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */


interface inf_db{
	//连接数据库
	public function connect();
	//是否连接
	public function is_connect($is_real=FALSE);
	//重新连接数据库
	public function reconnect();
	//关闭连接
	public function close();
	//执行语句
	public function query($sql);
	//选择数据库
	public function select_db($db_name=NULL);
	//选择数据库编码
	public function select_charct($db_chart=NULL);
	//session专用查询方法
	public function get_session_one($sql, $sess_data_name);
}

/* End of file inf_db.php */ 