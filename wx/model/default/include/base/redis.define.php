<?php 
/**
 * 记录redis键值，各项目通用且保持一致
 */
//---------------------------------------redis key----------------------------------

//-------------qy.easywork365.com
/** 记录二维码扫码成功\认证为管理员 的管理员账号  */
define('REDIS_QY_EW365_LOGIN_USER', 				'redis:qy:ew365:login:user');
/** 记录二维码扫码成功\认证为管理员 的管理员ID  */
define('REDIS_QY_EW365_LOGIN_USER_ID', 				'redis:qy:ew365:login:user:id');
/** 记录二维码扫码生成时间  */
define('REDIS_QY_EW365_LOGIN_TIME', 				'redis:qy:ew365:login:time');
/** 记录二维码扫码人员是否为管理员  */
define('REDIS_QY_EW365_LOGIN_STATE', 				'redis:qy:ew365:login:state');
/** 记录接收定位地理位置信息的员工的微信账号 */
define('REDIS_QY_EW365_SEND_LOCAL_USERS',			'redis:qy:ew365:send:local:users');

/** 最新access token  */
define('REDIS_QY_EW365_ACCESS_TOKEN', 				'redis:qy:ew365:access:token');
/** 最新access token的获取时间   */
define('REDIS_QY_EW365_ACCESS_TOKEN_TIME',  		'redis:qy:ew365:access:token:time');

/** 最新套件access token 未md5加密 */
define('REDIS_QY_EW365_SUITE_ACCESS_TOKEN',  		'redis:qy:ew365:suite:access:token');
/** 最新套件access token的获取时间  未md5加密 */
define('REDIS_QY_EW365_SUITE_ACCESS_TOKEN_TIME',  	'redis:qy:ew365:suite:access:token:time');

/** 最新企业access token 未md5加密 */
define('REDIS_QY_EW365_COMPANY_ACCESS_TOKEN',  		'redis:qy:ew365:company:access:token');
/** 最新企业access token的获取时间  未md5加密 */
define('REDIS_QY_EW365_COMPANY_ACCESS_TOKEN_TIME',  'redis:qy:ew365:company:access:token:time');

//------------- m.easywork365.com
/** 员工当前地理位置    */
define('REDIS_M_EW365_SIGN_USER_LOCAL',				'redis:m:ew365:sign:user:local');

// end of file