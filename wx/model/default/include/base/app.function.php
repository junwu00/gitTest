<?php
/** 
 * 全局应用函数，非稳定版 
 * 
 * 可以自行在此文件添加新的全局函数
 * 
 * @author LiangJianMing
 * @create 2014-05-27 
 * @version 1.0
 */


/** 
 * 写入日志文件
 * 
 * @global
 * @param string $content 操作产生的消息内容
 * @param string $type 操作的类型
 * @return mixed 禁止写入日志返回FALSE，否则返回TRUE
 */
function log_write($content='', $type=NULL){
	if(!defined('SYSTEM_SET_ISLOG') || !SYSTEM_SET_ISLOG) return FALSE;
	$log_file_name = SYSTEM_LOG.date('Ymd', time()).'.txt';
	if (PHP_SAPI == 'cli') {
		$log_file_name = SYSTEM_LOG.date('Ymd', time()).'.cron.txt';	//定时任务写的日志
	}
	
	$com_info = '';
	if (isset($_SESSION) && isset($_SESSION[SESSION_VISIT_COM_ID])) {
		$com_info = '[com:' . $_SESSION[SESSION_VISIT_COM_ID] . ']';
		isset($_SESSION[SESSION_VISIT_USER_ID]) && $com_info .= '[us:'.$_SESSION[SESSION_VISIT_USER_ID].']';
	}

	//用作识别是否同一次访问
	static $ident = NULL;
	$ident === NULL and $ident = uniqstr();

	$time 	= time();
	$ip 	= get_ip();
	$sid 	= isset($_SESSION) ? session_id() : 'no session';
	$write_string = '['.date("Y-m-d H:i:s", $time).']['.$ip.']['.$type.']['.$sid.']['.$content.']';
	$write_string = '[ident：' . $ident . ']'.'[time:'.date("Y-m-d H:i:s", $time).'][ip:'.$ip.']['.$type.'][session_id:'.$sid.']'.$com_info.'['.$content.']';
	$write_string .= "\r\n";
	file_put_contents($log_file_name, $write_string, FILE_APPEND);
}

/** 
 * 记录日志
 * 
 * @global
 * @param string $message 需要记录的消息
 * @param string $type 操作的类型
 * @return void
 */
function debug($message, $type=NULL){
	static $static_string_md5 = 'NO';
	$error = array();
	if($message instanceof Exception){
		$error['message'] = $message -> getmessage();
		$error['type'] = $message -> getcode();
		$error['line'] = $message -> getline();
		$error['file'] = $message -> getfile();
	}else{
		log_write($message, $type);
		$error = error_get_last();
	}
	if(!empty($error)){
		$tmp_string_md5 = md5(serialize($error));
		if($tmp_string_md5 != $static_string_md5){
			$message = g('html') -> get_text($error['message']).'<CODE:'.$error['type'].'><LINE:'.$error['line'].'><FILE:'.$error['file'].'>';
			log_write($message, $type);
			$static_string_md5 = $tmp_string_md5;
		}
	}
}

/** 
 * 全部类的非静态方法都必须通过此函数调用
 * 
 * @global
 * @param: string $class_name 类名称[全部小写]
 * @param: string $mark_name 类名对应标记
 * @param: boolean $is_new 是否进行new操作，如果该参数是FALSE，则使用单例模式
 * @return object
 */
function g($class_name, $mark_name=NULL, $is_new=FALSE){
	static $APP_OBJECT_DATA = array();	//存放对象，用于单例模式
	$return = FALSE;
	$class_name = 'cls_'.strtolower(trim($class_name));
	if(!class_exists($class_name)){
    	$object_class_url = SYSTEM_INCLUDE.$class_name.'.php';
    	if(!is_include($object_class_url)){
    		if(!file_exists($object_class_url)){
    			debug('Class File ['.$object_class_url.'] Not Found!', 'function_g');
    		}
    		include_once($object_class_url);
    	}else {
    		debug('Class ['.$class_name.'] not in ['.$object_class_url.']!', 'function_g');
    	}
    	return $return;
    }
	$mark_str = is_string($mark_name) ? $mark_name : json_encode($mark_name);
    $index_str = $class_name . ':' . $mark_str;
	if(!$is_new && isset($APP_OBJECT_DATA[$index_str])){
		$return = $APP_OBJECT_DATA[$index_str];
	}else{
		$APP_OBJECT_DATA[$index_str] = $return = new $class_name($mark_name);
	}
	return $return;
}

/** 
 * 加载扩展应用配置文件(global.config.php)
 * 
 * @global
 * @param array $exts 需要加载的扩展应用数组
 * @return mixed 若$exts为空则返回已加载的应用数组，否则返回TRUE
 */
function load_extensions($exts=NULL){
	static $exts_list = array();
	if(empty($exts)){
		return $exts_list;
	}else{
		!is_array($exts) && $exts = array($exts);
		foreach($exts as $val){
			$val = strval($val);
			if(!in_array($val, $exts_list, TRUE)){
				$tmp_file = SYSTEM_EXTENSIONS.$val.'/global.config.php';
				if(!file_exists($tmp_file)){
					debug('Load Apps ['.$val.'] ERROR, File ['.$tmp_file.'] Not Found!', 'global_function');
				}else{
					include_once($tmp_file);			//载入应用
					array_push($exts_list, $val);
				}
			}
		}
		unset($val);
	}
	return TRUE;
}

/** 
 * 判断访问者是否来自IPhone客户端
 * 
 * @global
 * @return boolean
 */
function is_iphone(){
    if(!isset($_SERVER['HTTP_USER_AGENT'])) return FALSE;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if(strpos($user_agent, 'iPhone') || strpos($user_agent, 'iPad') || strpos($user_agent, 'iPod') || strpos($user_agent, 'iOS')){  //iPhone
        $is_iphone = TRUE;
    }else{  //android and other
        $is_iphone = FALSE;
    }
    return $is_iphone;
}

/** 
 * 加载基础库
 * 
 * @global
 * @return void
 */
function include_base(){
	static $base_list = array();
	!defined('SYSTEM_INCLUDE_BASE') && debug('Base source not found', 'include_base');
	
	$base_dir = SYSTEM_INCLUDE_BASE;
	$base_dir = rtrim($base_dir, '\\/');
	!is_dir($base_dir) && debug('Base source not found', 'include_base');
	!($handler = opendir($base_dir)) && debug('Base directory can not open', 'include_base');
	while (($file = readdir($handler)) !== FALSE) {
		if ($file == '.' || $file == '..') continue;
		if (!preg_match('/\.php$/i', $file)) continue;
		include_once($base_dir . '/' . $file);
	}
	closedir($handler);
}

/** 
 * 初始化全局samrty参数
 * 
 * @global
 * @return void
 */
function smarty_init(){
    g('smarty') -> assign('SYSTEM_HTTP', SYSTEM_HTTP_DOMAIN);
	g('smarty') -> assign('STATIC', SYSTEM_STATIC);
	g('smarty') -> assign('HTTP_DOMAIN', SYSTEM_STATIC_DOMAIN);
	g('smarty') -> assign('HTTP_FILE', MEDIA_URL_PREFFIX);
	g('smarty') -> assign('CSS_JS_DATE', CSS_JS_DATE);
	g('smarty') -> assign('PAGEINFO', $GLOBALS['PAGEINFO']);

    g('smarty') -> assign('SYSTEM_STATIC', rtrim(SYSTEM_STATIC, '/\\'));
    g('smarty') -> assign('SYSTEM_TPL', rtrim(SYSTEM_TPL, '/\\'));
    g('smarty') -> assign('SYSTEM_APPS', rtrim(SYSTEM_APPS, '/\\'));
}

/** 
 * 获取当前页面的url
 * 
 * @global
 * @return string
 */
function get_this_url() {
	$url = SYSTEM_HTTP_TYPE . '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	return $url;
}

/**
 * 对象转数组
 * @param unknown_type $obj
 */
function obj2arr($obj) {
	$result = array();
    $array = is_object($obj) ? get_object_vars($obj) : $obj;
	if (!is_array($array) || count($array) == 0)	return $result;
    
    foreach ($array as $key => $val) {
        $val = (is_array($val) || is_object($val)) ? obj2arr($val) : $val;
        $result[$key] = $val;
    }
	
    return $result;
}

/**
 * 清空数组所有数据的前后空格
 * @param unknown_type $Input
 */
function trim_array($input){
    if (!is_array($input))
        return trim($input);
 
    return array_map('trim_array', $input);
}

/**
 * 对数组中所有中文进行urlencode
 * @param unknown_type $input
 */
function urlencode_array($input) {
    if (!is_array($input))
        return urlencode($input);
 
    return array_map('urlencode_array', $input);
}

/**
 * 对数组中所有值强制转换成int
 * @param unknown_type $input
 */
function intval_array($input) {
    if (!is_array($input))
        return intval($input);
 
    return array_map('intval_array', $input);
}

/**
 * 对数组中所有值强制转换成string
 * @param unknown_type $input
 */
function strval_array($input) {
    if (!is_array($input))
        return strval($input);
 
    return array_map('strval_array', $input);
}

/**
 * 获取指定月份的起止time
 * @param unknown_type $year
 * @param unknown_type $month
 */
function get_month_time($year, $month) {
	$sec_mon_days = 28;
	if (($year % 400 == 0) || ($year % 4 == 0 && $year % 100 != 0)) {
		$sec_mon_days = 29;
	}
	
	$month = intval($month);
	$str_mon = ($month < 10) ? '0'.$month : $month;
	$start = $year.$str_mon.'01000000';	//当月起始时间
	
	switch ($month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			$end = $year.$str_mon.'31235959';
			break;
			
		case 2:
			$end = $year.$str_mon.$sec_mon_days.'235959';
			break;
			
		case 4:
		case 6:
		case 9:
		case 11:
			$end = $year.$str_mon.'30235959';
			break;
	}
	
	return array(strtotime($start), strtotime($end));
}

/**
 * 将数据数组的时间转换成指定日期格式
 * @param unknown_type $arr
 * @param unknown_type $fields	字段数组
 * @param unknown_type $formats	格式数组
 * @param unknown_type $def		为空时的替换字符串
 */
function format_array_time(&$arr, $fields, $formats, $def='') {
	if (!is_array($arr))	return FALSE;
	
	foreach ($arr as &$val) {
		foreach ($fields as $col => $field) {
			if (!isset($val[$field]) || is_null($val[$field]) || empty($val[$field])) {
				$val[$field] = $def;
				continue;	
			}
			
			$val[$field] = date($formats[$col], $val[$field]);
		}
	}
}

/**
 * 获取唯一标识
 */
function uniqstr() {
	$uniq_str = uniqid(SYSTEM_UNIQ_PREFIX, TRUE);
	return md5($uniq_str.time());
}
	
/**
 * 秒数转时长
 * @param unknown_type $times
 */
function sec_to_time($times){
	$result = '0小时0分0秒';
    if ($times>0) {
		$hour = floor($times/3600);
		$minute = floor(($times-3600 * $hour)/60);
  		$second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
  		$result = '';
  		$result .= $hour > 0 ? ($hour.'小时') : '';
  		$result .= $minute > 0 ? ($minute.'分') : '';
    	$result .= $second.'秒';
	}
	return $result;
}

/**
 * 较好的用户体验格式化剩余时间
 *
 * @global
 * @param integer $begin_time 起始时间
 * @return string
 */
function html_time_format($begin_time) {
	$ret = '';
	$inteval = $begin_time - time();

	$day_inteval = 24 * 3600;
	$day = intval($inteval / $day_inteval);
	!empty($day) && $ret .= $day . '天';

	$inteval = $inteval % $day_inteval;

	$hour_inteval = 3600;
	$hour = intval($inteval / $hour_inteval);
	!empty($hour) && $ret .= $hour . '小时';

	$inteval = $inteval % $hour_inteval;

	$min_inteval = 60;
	$min = intval($inteval / $min_inteval);
	!empty($min) && $ret .= $min . '分';

	$inteval = $inteval % $min_inteval;

	$sec = intval($inteval);
	!empty($sec) && $ret .= $sec . '秒';

	return $ret;
}

/**
 * 自动加载对应的类
 *
 * @global
 * @param string $cls_name 类名  
 * @return string 真正的类名
 */
function auto_load($name) {
	$cls_name = $name;
	if (class_exists($cls_name, FALSE)) return;

	$arr = array(
		SYSTEM_INCLUDE_BASE.$cls_name.'.php',
		SYSTEM_INCLUDE.$cls_name.'.php',
		SYSTEM_MODS.$cls_name.'.php',
		SYSTEM_APPS.$cls_name.'.php',
	);

	foreach ($arr as $val) {
		if (file_exists($val)) {
			include_once($val);
			BREAk;
		}
	}
}

/**
 * 判断字符串是否json格式数据
 *
 * @global
 * @param string $str 字符串
 * @return boolean
 */
function is_json($str) {
	if (strpos($str, '[') !== 0 && strpos($str, '{') !== 0) return FALSE;
	if (is_null(json_decode($str))) return FALSE;
	return TRUE;
}

/**
 * 设置响应静态文件时的http头部信息
 *
 * @global
 * @param string $ext 扩展名
 * @return void
 */
function set_static_header($ext='') {
    $ext = strtolower($ext);
    switch($ext) {
        case 'css' : {
            header('Content-Type:text/css; charset=UTF-8');
            BREAK;
        }
        case 'js' : {
            header('Content-Type:application/x-javascript; charset=UTF-8');
            BREAK;
        }
        default : {
            header('Content-Type:text/html; charset=UTF-8');
            BREAK;
        }
    }

    //缓存300天
    $offset = 300 * 24 * 3600;
    
    // $http_domain = SYSTEM_HTTP_DOMAIN;
    // $http_domain = rtrim($http_domain, '\\/');
    // header("Access-Control-Allow-Origin: {$http_domain}");
    // header("Cache-Control: public");
    // header("Pragma: cache");

    $expires_date = gmdate("D, d M Y H:i:s T", time() + $offset);
    $now_date = gmdate("D, d M Y H:i:s T", time());

    header("Cache-Control: max-age={$offset}");
    header("Expires: {$expires_date}");
    header("Last-Modified: {$now_date}");
}

/**
 * 替换的css文件中的相对路径，然后返回文件内容
 *
 * @access public
 * @param string $css_file css文件路径
 * @return string
 */
function replace_css_url($css_file) {
    $ret = '';
    if (!file_exists($css_file)) return $ret;

    $dir_path = dirname($css_file);

    $this_http_url = str_replace(SYSTEM_ROOT, SYSTEM_STATIC_DOMAIN, $dir_path);
    $up_http_url = dirname($this_http_url);

    $ret = file_get_contents($css_file);
    
    $ret = preg_replace('/url\s*\([\s\'"]*([^\.\s\)\'"]{1}[^\)\'"]+)[\'"]*\)/i', ('url(' . $this_http_url . "/$1)"), $ret);
    $ret = str_replace('../../../../', dirname(dirname(dirname($up_http_url))) . '/', $ret);
    $ret = str_replace('../../../', dirname(dirname($up_http_url)) . '/', $ret);
    $ret = str_replace('../../', dirname($up_http_url) . '/', $ret);
    $ret = str_replace('../', $up_http_url . '/', $ret);
    $ret = str_replace('./', $this_http_url . '/', $ret);

    return $ret;
}

/**
 * 将字节数转换为带单位的字符串
 *
 * @access global
 * @param integer $size 字节大小
 * @return string
 */
function prase_humen_size($size) {
    $val = floatval($size);

    $preffix = '';
    if ($val < 0) {
        $preffix = '-';
        $val = abs($val);
    }

    $unit = 1;
    while (($val / 1024) > 1) {
        $val = $val / 1024;
        $unit++;
    }

    $ret = round($val, 2);

    switch ($unit) {
        case 1 : {
            $ret .= 'B';
            break;
        }
        case 2 : {
            $ret .= 'KB';
            break;
        }
        case 3 : {
            $ret .= 'MB';
            break;
        }
        case 4 : {
            $ret .= 'GB';
            break;
        }
        case 5 : {
            $ret .= 'TB';
            break;
        }
    }

    $ret = $preffix . $ret;

    return $ret;
}

/**
 * 将字符串自动裁剪，超过隐藏
 *
 * @access global
 * @param string $content utf8正文
 * @param integer $len 要保留的字符长度
 * @param string $sf 隐藏部分替换符
 * @return string
 */
function string_auto_cut($content, $len, $sf='...') {
    $len = intval($len);
    if ($len == 0) return '';
    if (mb_strlen($content, 'UTF-8') <= $len) return $content;

    $ret = mb_strcut($content, 0, $len, 'UTF-8');
    $ret .= $sf;
    return $ret;
}

/**
 * 转大写金额
 * @param unknown_type $num	数字金额
 */
function moneyToUpper($num) {
	$strOutput = "";
  	$strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
 	$num .= "00";  
  	$intPos = strpos($num, '.');
  	if ($intPos && $intPos >= 0) {
  		$num = substr($num, 0, $intPos) . substr($num, $intPos+1, 2); 
  	} 
    
  	$strUnit = mb_substr($strUnit, (strlen($strUnit)/3 - strlen($num)) * 3); 
  	$len = strlen($num);
  	$tmp_str = '零壹贰叁肆伍陆柒捌玖';
  	for ($i=0; $i<$len; $i++){
  		$strOutput .= mb_substr($tmp_str, substr($num, $i, 1)*3, 1*3) . substr($strUnit, $i*3, 1*3); 
  	}  
  	$ret = $strOutput;
  	$ret = preg_replace("/零角零分$/", "整", $ret);
  	$ret = preg_replace("/零[仟佰拾]/u", "零", $ret);
  	$ret = preg_replace("/零{2,}/u", "零", $ret);
  	$ret = preg_replace("/零([亿|万])/u", "$1", $ret);
  	$ret = preg_replace("/零+元/", "元", $ret);
  	$ret = preg_replace("/亿零{0,3}万/", "亿", $ret);
  	$ret = preg_replace("/^元/", "零元", $ret);
    return $ret;    
}

    /**
     * 将错误的域名转换成正确的域名
     * @param unknown_type $str 要处理的字符串
     * @param array $domain_prefix_list 转换的域名数据  array('旧域名' => '新域名') 备注：旧域名不带http头，新域名带http头
     */
    function trans_static_path($str, $domain_prefix_list=array()) {
        if(empty($domain_prefix_list)){
            return $str;
        }
        foreach ($domain_prefix_list as $old => $val){
            $reg = '/(<img.*src=")(http[s]?:\/\/)'.$old.'(\/.*)"(.*)/isU';
            $str = preg_replace($reg, "$1".$val."$3\"$4", $str);
        }
        return $str;
    }

/* End of file app.function.php */ 