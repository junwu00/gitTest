<?php
/** 
 * 全局应用函数，稳定版
 * 
 * 只有经过长期使用测试，稳定可用的函数才应该放置在该文件中
 * 
 * @author LiangJianMing
 * @create 2014-05-27 
 * @version 1.0
 */

/** 
 * 判断文件是否已经被引入
 * 
 * @global
 * @param string $include_file 引入文件的全路径
 * @return boolean
 */
function is_include($include_file){
    return in_array($include_file, get_included_files(), TRUE) ? TRUE : FALSE;
}

/** 
 * 生成随机字符串
 * 
 * @global
 * @param integer $leng 需要生成的字符串长度
 * @param integer $type 字符串类型,取值：1-15，改值会先转换为2进制，如：14=1110，第一位表示使用特殊符号、第二位表示使用数字、第三位表示使用小写字母、第四位表示不使用大写字母
 * @param boolean $dark 是否去除字符集{O,o,0}
 * @return string
 */
function get_rand_string($leng, $type=7, $dark=FALSE){
    $tmp_array = array(
        '1' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        '2' => 'abcdefghijklmnopqrstuvwxyz',
        '4' => '0123456789',
        '8' => '~!@$&()_+-=,./<>?;\'\\:"|[]{}`'
    );
    $return = $target_string = '';
    $array = array();
    $bin_string = decbin($type);
    $bin_leng  = strlen($bin_string);
    for($i = 0; $i < $bin_leng; $i++) if($bin_string{$i} == 1) $array[] = pow(2, $bin_leng - $i - 1);
    if(in_array(1, $array, TRUE)) $target_string .= $tmp_array['1'];
    if(in_array(2, $array, TRUE)) $target_string .= $tmp_array['2'];
    if(in_array(4, $array, TRUE)) $target_string .= $tmp_array['4'];
    if(in_array(8, $array, TRUE)) $target_string .= $tmp_array['8'];
    $target_leng = strlen($target_string);
    mt_srand((double)microtime()*1000000);
    while(strlen($return) < $leng){
        $tmp_string = substr($target_string, mt_rand(0, $target_leng), 1);
        $dark && $tmp_string = (in_array($tmp_string, array('0', 'O', 'o'))) ? '' : $tmp_string;
        $return .= $tmp_string;
    }
    return $return;
}

/** 
 * 获取当前的时间，精确到微妙
 * 
 * @global
 * @param boolean $is_string TRUE-返回string格式，FALSE-返回数组格式
 * @return 
 */
function get_cur_time($is_string=FALSE){
    $cur_time = microtime();
    return $is_string ? $cur_time : array(doubleval(substr($cur_time, 0, 10)), intval(substr($cur_time, 11, 10)));
}

/** 
 * 获取GET方式提交的参数
 * 
 * @global
 * @param string $var_name 参数名
 * @param boolean $is_filter 是否过滤非安全字符
 * @return mixed
 */
function get_var_get($var_name, $is_filter = FALSE){
    $ret = isset($_GET[$var_name]) ? rawurldecode(trim($_GET[$var_name])) : NULL;
    $is_filter && !empty($ret) && $ret = filter_string($ret);
    return $ret;
}

/** 
 * 获取POST方式提交的参数
 * 
 * @global
 * @param string $var_name 参数名
 * @param boolean $is_filter 是否过滤非安全字符
 * @return mixed
 */
function get_var_post($var_name, $is_filter=FALSE){
    $ret = isset($_POST[$var_name]) ? $_POST[$var_name] : NULL;
    $is_filter && !empty($ret) && $ret = filter_string($ret);
    return $ret;
}

/** 
 * 获取GET和POST方式提交的参数
 * 
 * @global
 * @param string $var_name 参数名
 * @param boolean $is_filter 是否过滤非安全字符
 * @param boolean $post_first 是否优先获取POST参数
 * @return mixed
 */
function get_var_value($var_name, $is_filter = FALSE, $post_first = TRUE){
    $ret = NULL;
    $fir_func = 'get_var_post';
    $sec_func = 'get_var_get';

    if (!$post_first) {
        $fir_func = 'get_var_get';
        $sec_func = 'get_var_post';
    }

    $ret = $fir_func($var_name, $is_filter);
    $ret === NULL && $ret = $sec_func($var_name, $is_filter);
    return $ret;
}

/** 
 * 获取有效的客户端IP地址
 * 
 * @global
 * @return string
 */
function get_ip(){
    static $ip = NULL;
    if ($ip !== NULL) return $ip;

    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        is_array($ip) and $ip = array_shift($ip);
        if (check_data($ip, 'ip')) return $ip;
    }

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
        if (check_data($ip, 'ip')) return $ip;
    }

    if (!empty($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
        if (check_data($ip, 'ip')) return $ip;
    }

    $ip = '';
	return $ip;
}

/** 
 *     获取请求者的ip地址，
 * 如果使用代理服务，这里将是代理地址
 * 
 * @global
 * @return mixed 成功返回string, 否则返回FALSE
 */
function get_remote_ip(){
    $ip = FALSE;

    !empty($_SERVER['REMOTE_ADDR']) and $ip = $_SERVER['REMOTE_ADDR'];
    !check_data($ip, 'ip') and $ip = FALSE;

    return $ip;
}

/** 
 * 十六进制转换为二进制
 * 
 * @global
 * @param string $string 需要转换的字符串
 * @return string
 */
function hex_bin($string){
    $return = '';
    $length = strlen($string);
    for($i = 0; $i < $length; $i += 2) $return .= pack('C', hexdec(substr($string, $i, 2)));
    return $return;
}

/** 
 * 获取文件或远程地址的内容
 * 
 * @global
 * @param: string $url 文件路径或远程地址
 * @param: boolean $is_local 是否是本地文件
 * @return mixed 失败返回FALSE，否则返回string
 */
function get_content($url, $is_local=TRUE){
    /*
    $ret = FALSE;
    if($is_local and !file_exists($url)) return $ret;
    if(!($fp = fopen($url, 'rb'))) return $ret;

    $ret = '';
    while(!feof($fp)){
        $ret .= fgets($fp);
    }

    fclose($fp);
    return $ret;
    */
    $ret = FALSE;
    if($is_local and !file_exists($url)) return $ret;
    if(!($fp = g('http')->quick_get($url,30))) return $ret;
    if ($fp['httpcode'] == '200') {
		return $fp['content'];
    } else {
    	return FALSE;
    }
}

/** 
 * 将长整型转换成ip地址
 * 
 * @global
 * @param integer $ip_long 需要转换的整型数
 * @return string
 */
function long_ip($ip_long){
    return long2ip($ip_long);
}

/** 
 * 将ip地址转换成长整型
 * 
 * @global
 * @param string $ip_string 需要转换的ip地址
 * @return integer
 */
function ip_long($ip_string){
    $return = 0;
    $tmp_array = explode('.', $ip_string);
    foreach($tmp_array as $key => $val) $return += intval($val)*pow(256, abs($key-3));
    return $return;
}

/** 
 * 返回数组的深度
 * 
 * @global
 * @param array $array 需要检测的数组
 * @param integer $i 当前所在深度
 * @return mixed 错误返回FALSE，否则返回integer
 */
function get_array_num($array, $i=1){
    if(!is_type($array, 'array')) return FALSE;
    $i = $i < 1 ? 1 : $i;
    $return = $i;
    if(!empty($array)){
        foreach($array as $val){
            if(is_type($val, 'array')){
                $return = max($i, $return, get_array_num($val, $i+1));
            }
        }
    }
    return $return;
}

/** 
 * 检验数据合法性
 * 
 * @global
 * @param string $string 需要检验的字符串
 * @param type $type 检验类型
 * @return boolean
 */
function check_data($string, $type='email'){
    $return = FALSE;
    
    switch($type){
        //昵称,只能以英文或中文开头,只能包含英文、中文、数字、下划线
        case 'sname'        :{ $return = preg_match("/^[\x{4e00}-\x{9fa5}A-Za-z]{1}[\x{4e00}-\x{9fa5}_A-Za-z0-9]*$/u", $string); BREAK; }
	    //普通账号格式
        case 'account'      :{ $return = preg_match("/^[A-Za-z0-9@_]{6,50}$/", $string); BREAK; }
    	//判断密码格式
        case 'password'     :{ $return = preg_match("/^[A-Za-z0-9~!@#$%^&\*\(\)_]+$/", $string); BREAK; }
        //是否为email
        case 'email'     	:{ $return = preg_match("/^(\w+[-+.]*\w+)*@(\w+([-.]*\w+)*\.\w+([-.]*\w+)*)$/", $string); BREAK; }
        //是否为url
        case 'http'     	:{ $return = preg_match("/^http:\/\/[A-Za-z0-9-]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/", $string); BREAK; }
        //是否为qq号码
        case 'qq'         	:{ $return = preg_match("/^[1-9]\d{4,11}$/", $string); BREAK; }
        //是否为邮政编码
        case 'post'     	:{ $return = preg_match("/^[1-9]\d{5}$/", $string); BREAK; }
        //是否为身份证号码
        case 'idnum'     	:{ $return = preg_match("/^\d{15}(\d{2}[A-Za-z0-9])?$/", $string); BREAK; }
        //是否为全中文
        case 'china'     	:{ $return = preg_match("/^[\x{4e00}-\x{9fa5}]+$/u", $string); BREAK; }
        //是否为全英文
        case 'english'     	:{ $return = preg_match("/^[A-Za-z]+$/", $string); BREAK; }
        //是否为手机号码
        case 'mobile'       :{ $return = preg_match("/^1[34578]\d{9}$/", $string); BREAK; }
        //是否为手机号码(仅验证类型长度)
        case 'mobile_s'     :{ $return = preg_match("/^\d{11}$/", $string); BREAK; }
        //是否为固话号码
        case 'phone'     	:{ $return = preg_match("/^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/", $string); BREAK; }
        //是否为年龄
        case 'age'         	:{ $return = (preg_match("/^(-{0,1}|\+{0,1})[0-9]+(\.{0,1}[0-9]+)$/", $string) && intval($string) <= 130 && intval($string) >= 12) ? TRUE : FALSE; BREAK; }
        //是否仅包含大小写英文字母及数字
        case 'eng_num'     	:{ $return = preg_match("/^[A-Za-z0-9]+$/", $string); BREAK; }
        //是否为datetime
        case 'datetime'    	:{ $return = preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}\s[\d]{1,2}:[\d]{1,2}:[\d]{1,2}$/', $string); BREAK; }
        //是否为date
        case 'date'     	:{ $return = preg_match('/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/', $string); BREAK; }
        //是否为datetime中的time部分
        case 'time'     	:{ $return = preg_match('/^[\d]{1,2}:[\d]{1,2}:[\d]{1,2}$/', $string); BREAK; }
        //是否为ip地址
        case 'ip'         	:{ $return = preg_match("/^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b$/", $string); BREAK; }
        //是否包含中文
        case 'with_chinese'	:{ $return = preg_match('/[\x{4e00}-\x{9fa5}]+/u', $string); BREAK; }
        //是否为域名
        case 'domain' 		:{ $return = preg_match('/^[A-Za-z0-9][A-Za-z0-9-]+(\.[A-Za-z0-9-]+){1,3}$/', $string); BREAK; }
        //是否为中文域名
        case 'cndomain'		:{ $return = preg_match('/^([-a-zA-Z0-9\.]*[\x{4e00}-\x{9fa5}]*[-a-zA-Z0-9\.]*)+\.(中国|公司|网络|CN|COM|NET)$/iu', $string); BREAK; }
        //是否是ipv6地址
        case 'ipv6'			:{ $return = preg_match('/^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/', $string); BREAK;}
    }
    return $return ? TRUE : FALSE;
}

/** 
 * 过滤非安全字符
 * 
 * @global
 * @param mixed $string 被过滤的原字符串或数组
 * @return mixed 过滤后的数据
 */
function filter_string($string = ''){
    if ($string == '') return $string;

    if (is_array($string)) {
        foreach($string as &$val) {
            $val = filter_string($val);
        }
        unset($val);
    }else {
        //过滤所有反斜杠
        $string = str_replace('\\', '', $string);
        //对html的实体转义
        $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }
    return $string;
}

/** 
 * 是否是wap移动设备访问
 * 
 * @global
 * @return boolean
 */
function is_wap(){
    if(isset($_SERVER['HTTP_VIA'])) return TRUE;
    if(isset($_SERVER['HTTP_X_NOKIA_CONNECTION_MODE'])) return TRUE;
    if(isset($_SERVER['HTTP_X_UP_CALLING_LINE_ID'])) return TRUE;
    if(strpos(strtoupper($_SERVER['HTTP_ACCEPT']), 'VND.WAP.WML') > 0) return TRUE;
    $http_user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? trim($_SERVER['HTTP_USER_AGENT']) : '';
    if($http_user_agent == '') return TRUE;
    $mobile_os = array('Google Wireless Transcoder', 'Windows CE', 'WindowsCE', 'Symbian', 'Android', 'armv6l', 'armv5', 'Mobile', 'CentOS', 'mowser', 'AvantGo', 'Opera Mobi', 'J2ME/MIDP', 'Smartphone', 'Go.Web', 'Palm', 'iPAQ');
    $mobile_token = array('Profile/MIDP', 'Configuration/CLDC-', '160×160', '176×220', '240×240', '240×320', '320×240', 'UP.Browser', 'UP.Link', 'SymbianOS', 'PalmOS', 'PocketPC', 'SonyEricsson', 'Nokia', 'BlackBerry', 'Vodafone', 'BenQ', 'Novarra-Vision', 'Iris', 'NetFront', 'HTC_', 'Xda_', 'SAMSUNG-SGH', 'Wapaka', 'DoCoMo', 'iPhone', 'iPod');
    $flag_os = $flag_token = FALSE;
    foreach($mobile_os as $val){
        if(strpos($http_user_agent, $val) > 0){ $flag_os = TRUE; break; }
    }
    foreach($mobile_token as $val){
        if(strpos($http_user_agent, $val) > 0){ $flag_token = TRUE; break; }
    }
    if($flag_os || $flag_token) return TRUE;
    return FALSE;
}

/** 
 * 计算时间差，记录日志比较常用
 * 
 * @access public
 * @param: array $time_form 开始时间
 * @param: array $time_to 结束时间
 * @param: integer $point 取小数点位
 * @return double
 */
function time_diff($time_form, $time_to=NULL, $point=10){
    $return = 0.0;
    empty($time_to) && $time_to = get_cur_time();
    is_string($time_form) && $time_form = time_array($time_form);
    if(empty($time_form) || !is_array($time_form)) return FALSE;
    $return = ($time_to[0]-$time_form[0])+($time_to[1]-$time_form[1]);
    return sprintf("%.".$point."f", $return);
}

/** 
 * 时间字符串转成数组，主要用于记录日志
 * 
 * @access public
 * @param: string $string 时间字符串[microtime]
 * @return array
 */
function time_array($string){
    return array(doubleval(substr($string, 0, 10)), intval(substr($string, 11, 10)));
}

/* End of file global.function.php */