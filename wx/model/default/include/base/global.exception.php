<?php
/** 标记异常处理的方法 */
set_error_handler("err_handler");
set_exception_handler("exp_handler");

/**
 * 记录错误日志
 * @param unknown_type $errno	
 * @param unknown_type $errstr
 * @param unknown_type $errfile
 * @param unknown_type $errline
 */
function err_handler($errno, $errstr, $errfile, $errline){
	if (strpos($errstr, 'mysql_escape_string') !== FALSE)	return;
	if (strpos($errstr, 'mcrypt_cbc') !== FALSE)	return;
	if (strpos($errfile, '/smarty/templates_c') !== FALSE)	return;
	
	$log_file_name = SYSTEM_LOG.'error'.date('Ymd', time()).'.log';
	if (PHP_SAPI == 'cli') {
		$log_file_name = SYSTEM_LOG.'error'.date('Ymd', time()).'.cron.log';	//定时任务写的日志
	}
	
	$date = date('Y/m/d H:i:s');
	$sid 	= isset($_SESSION) ? session_id() : 'no session';
	$errLog = "<$date> [$errno][$sid] $errstr \n" . " Error on line $errline in $errfile \n\n"; 
	file_put_contents($log_file_name, $errLog, FILE_APPEND);
	if (strpos(strtolower($errstr), 'fatal error') !== FALSE) {	//致命错误，提示：系统繁忙
		cls_resp::echo_busy();
	}
	//die();
}

/**
 * 记录异常日志
 * @param unknown_type $errno	
 * @param unknown_type $errstr
 * @param unknown_type $errfile
 * @param unknown_type $errline
 */
function exp_handler($exp_str){
	$log_file_name = SYSTEM_LOG.'exception'.date('Ymd', time()).'.txt';
	if (PHP_SAPI == 'cli') {
		$log_file_name = SYSTEM_LOG.'exception'.date('Ymd', time()).'.cron.txt';	//定时任务写的日志
	}
	
	$date = date('Y/m/d H:i:s');
	$sid 	= isset($_SESSION) ? session_id() : 'no session';
	$errLog = "<$date> [$sid] $exp_str "; 
	file_put_contents($log_file_name, $errLog, FILE_APPEND);
	cls_resp::echo_busy();
	//die();
}