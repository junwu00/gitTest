<?php
/**
 * 访问/请求过滤（验证）器
 * @author yangpz
 * @date 2015-05-11
 *
 */
class cls_filter {
	private $refer_arr = array();							//来源验证
	
	//private $white_list = array();						//白名单
	private $block_list = array();							//黑名单
	private $max_count = 3000;								//每半分钟（每个IP）最多访问3000次
	
	private $strict_arr = array(							//需要以限制两次操作时间间隔的请求(毫秒级[1秒=1000毫秒])
		'register|experience' => 1000
	);
	
	/** 执行过滤 */
	public function run() {
		$module	= get_var_get('m', FALSE);
		$action	= get_var_get('a', FALSE);
		$target = $module.'|'.$action;
		//请求来源限制
		if (isset($this -> refer_arr[$target])) {
			$data = get_var_post('data');
			header("Access-Control-Allow-Origin: http://".$data['host']);

			$this -> check_refer($target);
		}
		
		//时间段内总次数限制
		$this -> check_ip();
		
		//相同访问的时间间隔限制，默认为每秒1次
		if (isset($this -> strict_arr[$target])) {
			$this -> strict_check_ip($target, $this -> strict_arr[$target]);
		}
	}
	
	//---------------private---------------------------
	
	/** 验证来源 */
	private function check_refer($target) {
		$flag = FALSE;										//是否合法来源
		if (isset($_SERVER['HTTP_REFERER'])) {				//有设置来源
			$refer_list = $this -> refer_arr[$target];
			$refer = $_SERVER['HTTP_REFERER'];
			if (in_array($refer, $refer_list)) {			//来源在白名单中
				$flag = TRUE;
				$this -> set_access_header($refer);			//处理跨域访问
			}
		}
		
		if (!$flag) {
			$this -> resp_err('非法请求');
		}
	}
	
	/** 验证IP */
	private function check_ip() {
		$ip = get_ip();
		if (empty($ip)) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		if (in_array($ip, $this -> block_list)) {
			$this -> resp_err('操作太频繁，请稍后重试~');
		}
		
		$redis_key = $this -> get_ip_redis_key($ip);
		$val = g('redis') -> get($redis_key);
		if ($val) {
			g('redis') -> increase($redis_key);
			
		} else {
			$val = 1;
			g('redis') -> set($redis_key, $val, 30);
		}
		if ($val >= $this -> max_count) {
			$this -> resp_err('操作太频繁，请稍后重试。');
		}
	}
	
	/** 严格验证IP （两次操作间隔时间） */
	private function strict_check_ip($target, $time=1) {
		$ip = get_ip();
		$redis_key = $this -> get_strict_ip_redis_key($ip, $target);
		$val = g('redis') -> get($redis_key);
		$now = mtime();
		if ($val) {
			if (floatval($now) - floatval($val) <= $time) {							//两次间隔小于等于限定时间，失败
				$this -> resp_err('操作太频繁，请稍后重试');
			}
		}
		$ret = g('redis') -> set($redis_key, strval($now), 60);						//未请求过 or 两次间隔大于限定时间，更新上次请求时间
	}
	
	/** 响应错误信息 */
	private function resp_err($errmsg) {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			cls_resp::echo_err('-1', $errmsg);
			
		} else {
			cls_resp::show_err_page($errmsg);
		}
	}
	
	/** 处理JS跨域访问 */
	private function set_access_header($refer) {
		$ret = parse_url($refer);
		$host = $ret['host'];
		$ew365 = substr($host, -15, strlen($host));
		if ($ew365 == 'easywork365.com') {
			header("Access-Control-Allow-Origin: http://".$host);
		}
	}
	
	/** 获取IP的redis缓存KEY（时间段内访问次数总数方式） */
	private function get_ip_redis_key($ip) {
		return md5(SYSTEM_HTTP_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':request:'.$ip);
	}
	
	/** 获取IP的redis缓存KEY(时间间隔方式) */
	private function get_strict_ip_redis_key($ip, $target) {
		return md5(SYSTEM_HTTP_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':request:'.$ip.':'.$target);
	}
	
}
//end