<?php
/** 
 * html文件的操作类 
 * 
 * html文件相关的操作方法集合
 * 
 * @author LiangJianMing
 * @create 2014-05-28 
 * @version 1.0
 */

class cls_html{
	/** 
	 * 网页源代码转换成普通文本
	 * 
	 * @access global
	 * @param string $html 网页源代码
	 * @return string
	 */
	public function get_text($html){
		$html = preg_replace('/\s+/is', '', $html);
		if(preg_match('/<body[^>]*?>(.*?)<\/body>/is', $html, $match)){
			$html = $match[1];
		}else if(preg_match('/<body[^>]*?>(.*)/i', $html, $match)){
			$html = $match[1];
		}
		$html = preg_replace('/<title[^>]*?>.*?<\/title>/is', '', $html);
		$html = preg_replace('/<style[^>]*?>.*?<\/style>/is', '', $html);
		$html = preg_replace('/<script[^>]*?>.*?<\/script>/is', '', $html);
		$html = preg_replace('/<noscript[^>]*?>.*?<\/noscript>/is', '', $html);
		$html = preg_replace('/<!--.*?-->/is', '', $html);
		$html = preg_replace('/<[\/\!]*?[^<>]*?>/i', '', $html);
		$html = preg_replace('/<!--[^>]*?>.*?<!\[endif\]-->/is', '', $html);
		$html = preg_replace('/&[a-z]+;/i', '', $html);
		return $html;
	}
}

/* End of file cls_html.php */