<?php
/** 
 * 数据库二次封装操作类
 * 
 * @author LiangJianMing
 * @create 2014-06-04 
 */

class cls_db {
	private $db_type 	= NULL;
	private $mark 		= NULL;
	
	/** 
	 * 构造函数
	 * 
	 * @access public
	 * @param string $object_mark 对象标记
	 * @return void
	 */
	public function __construct($object_mark = NULL) {
		!defined('SYSTEM_DB_TYPE') && define('SYSTEM_DB_TYPE', 'pdo');
		$this -> db_type = SYSTEM_DB_TYPE;
		!empty($object_mark) && $this -> mark = $object_mark;
	}
	
	/** 
	 * 数据写入
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $data 数据数组，字段名称 => 值
	 * @return mixed 成功返回id,失败返回FALSE
	 */
	public function insert($table, $data) {
		if(empty($table) || !is_string($table)) return FALSE;
		if(empty($data) || !is_array($data)) return FALSE;
		
		$sql = 'INSERT INTO '.mysql_escape_string($table);
		$set = ' SET ';
		foreach($data as $key => $val) {
			$set .= mysql_escape_string($key).'=\''.mysql_escape_string($val).'\', ';
		}
		unset($val);
		
		$set = substr($set, 0, -2);
		$sql .= $set;
		$result = g($this -> db_type, $this -> mark) -> exec($sql);
		$result !== FALSE && $result = g($this -> db_type, $this -> mark) -> insert_id();
		
		return $result ? $result : FALSE;
	}
	
	/** 
	 * 数据删除，一般是禁止直接删除数据的，仅适用于条件连接关键字为 AND 的操作
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $condition 条件数组，如：array('id!=' => 0)
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function delete($table, $condition) {
		if(empty($table) || !is_string($table)) return FALSE;
		if(empty($condition) || !is_array($condition)) return FALSE;
		
		$sql = 'DELETE FROM '.mysql_escape_string($table);
		$where = g('ndb') -> compose_where($condition);
		$sql .= $where;
		
		$result = g($this -> db_type, $this -> mark) -> exec($sql);
		return $result !== FALSE ? TRUE : FALSE;
	}
	
	/** 
	 * 更新数据，仅适用于条件连接关键字为 AND 的操作
	 * 
	 * @param string $table 数据表名
	 * @param array $data 数据数组，字段名称 => 值
	 * @param array $condition 条件数组，如：array('id!=' => 0)
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function update($table, $data, $condition) {
		if(empty($table) || !is_string($table)) return FALSE;
		if(empty($data) || !is_array($data)) return FALSE;
		if(empty($condition) || !is_array($condition)) return FALSE;
		
		$sql = 'UPDATE '.mysql_escape_string($table);
		$set = ' SET ';
		foreach($data as $key => $val){
			$set .= mysql_escape_string($key).'=\''.mysql_escape_string($val).'\',';
		}
		unset($val);
		$set = substr($set, 0, -1);
		
		$where = g('ndb') -> compose_where($condition);
		
		$sql .= $set.$where;
		
		$result = g($this -> db_type, $this -> mark) -> exec($sql);
		return $result !== FALSE ? TRUE : FALSE;
	}
	
	/** 
	 * 查询一条记录
	 * 
	 * @access public
	 * @param string $sql SQL语句
	 * @param integer $fetch 查询结果索引类型:1-字段索引、2-数字索引、其他-两种索引
	 * @return mixed 成功返回数组，失败返回FALSE
	 */
	public function select_one($sql, $fetch=1) {
		$fetch = intval($fetch);
		
		$result = g($this -> db_type, $this -> mark) -> select($sql, $fetch);
		return $result && isset($result[0]) ? $result[0] : FALSE;
	}
	
	/** 
	 * 查询并返回全部数据
	 * 
	 * @access public
	 * @param string $sql SQL语句
	 * @param integer $fetch 查询结果索引类型:1-字段索引、2-数字索引、其他-两种索引
	 * @return mixed 成功返回数组，失败返回FALSE
	 */
	public function select($sql, $fetch=1){
		$fetch = intval($fetch);
		
		$result = g($this -> db_type, $this -> mark) -> select($sql, $fetch);
		return $result && is_array($result) ? $result : FALSE;
	}
	
	/** 
	 * 查询并只取结果集中第一行第一列的值
	 * 
	 * @access public
	 * @param string $sql SQL语句
	 * @return mixed 成功返回string，失败返回FALSE
	 */
	public function select_first_val($sql){
		$result = g($this -> db_type, $this -> mark) -> select($sql);
		return $result && is_array($result) && is_array($row = array_shift($result)) ? array_shift($row) : FALSE;
	}
	
	/** 
	 * 检查一条记录是否存在，仅适用于条件连接关键字为 AND 的操作
	 * 
	 * @param string $table 数据表名
	 * @param array $condition 条件数组，如：array('id!=' => 0)
	 * @return string 存在返回Y,不存在返回N,参数错误返回E
	 */
	public function check_exists($table, $condition){
		if(empty($table) || !is_string($table)) return 'E';
		if(empty($condition) || !is_array($condition)) return 'E';
		
		$sql = 'SELECT 1 FROM '.mysql_escape_string($table);
		$where = g('ndb') -> compose_where($condition);
		
		$sql .= $where;
		$result = $this -> select_one($sql);
		return $result ? 'Y' : 'N';
	}
	
	/** 
	 * 切换数据库
	 * 
	 * @access public
	 * @param string $db_name 数据库名
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function change_db($db_name){
		if(empty($db_name) || !is_string($db_name)) return FALSE;
		return g($this -> db_type, $this -> mark) -> select_db(mysql_escape_string($db_name)) ? TRUE : FALSE;
	}
	
	/** 
	 * 切换字符格式
	 * 
	 * @access public
	 * @param string $charset 字符格式
	 * @return boolean 成功返回TRUE，失败返回FALSE
	 */
	public function change_charset($charset){
		if(empty($charset) || !is_string($charset)) return FALSE;
		return g($this -> db_type, $this -> mark) -> select_charct(mysql_escape_string($charset)) ? TRUE : FALSE;
	}
	
	/** 
	 * 可以执行自定义的SQL语句,仅允许执行select、insert、update
	 * 开头的语句，禁止使用自定义的delete,禁止其他特殊操作
	 * 
	 * @access public
	 * @param string $sql SQL语句
	 * @return mixed 返回未整理的执行结果
	 */
	public function query($sql){
		$func_name = 'query';
		if (preg_match('/^[ ]*(insert|update|delete)/i', $sql)) {
			$ret = g($this -> db_type, $this -> mark) -> exec($sql);
			$ret !== FALSE && $ret = TRUE;
			if (preg_match('/^[ ]*insert/i', $sql) && $ret) {
				$ret = g($this -> db_type, $this -> mark) -> insert_id();
			}
		}else {
			$ret = g($this -> db_type, $this -> mark) -> select($sql);
			$ret !== FALSE && $ret = TRUE;
		}
		
		return $ret !== FALSE ? $ret : FALSE;
	}

	/**
	 * 开始事务
	 *
	 * @access public
	 * @return void
	 */
	public function begin_trans() {
		g($this -> db_type, $this -> mark) -> begin_trans();
	}

	/**
	 * 提交事务
	 *
	 * @access public
	 * @return void
	 */
	public function commit() {
		g($this -> db_type, $this -> mark) -> commit();	
	}

	/**
	 * 回滚事务
	 *
	 * @access public  
	 * @return void
	 */
	public function rollback() {
		g($this -> db_type, $this -> mark) -> rollback();	
	}
	
	/** 
	 * 如果使用对象调用无权访问或不存在的函数时，触发该函数
	 * 
	 * @access public
	 * @param string $name 函数名
	 * @return void
	 */
	public function __call($name, $arg){
		echo 'Error function ['.$name.'] in CLASS ['.__CLASS__.']!';
	}
}

/* End of file cls_db.php */ 