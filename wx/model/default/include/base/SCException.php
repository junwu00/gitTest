<?php

/**
 * 异常类
 * @author yangpz
 * @date 2014-12-01
 *
 */
class SCException extends Exception {
	
	public function __construct($message, $code = 99999) {
		log_write('异常: code='.$code.', msg='.$message);
		
//		log_write('异常: code='.$code.', msg='.$message, 'EXP');
//		log_write($this -> getTraceAsString(), 'EXP');
		parent::__construct($message, $code);
	}
	
}

// end of file