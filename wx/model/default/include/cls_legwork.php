<?php
/**
 * 移动外勤数据模型
 *
 * @author LiangJianMing
 * @create 2015-02-09
 */
class cls_legwork {
	private $app_name = 'legwork';

	//公司id
	private $com_id 	= NULL;
	//用户id
	private $user_id 	= NULL;
	//数据库操作基础条件
	private $base_con	= NULL;

	//公司的部门root_id
	private $root_id	= NULL;

	//移动外勤配置表
	public $conf_table 	 = 'legwork_conf';
	//移动外勤计划表
	public $plan_table 	 = 'legwork_plan';
	//移动外勤上报记录表
	public $report_table = 'legwork_record';
	//用户信息表
	public $user_table 	 = 'sc_user';
	//部门信息表
	public $dept_table 	 = 'sc_dept';

	//签到、签退提示消息
	private $wx_mark_tip;

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			$this -> com_id  = $_SESSION[SESSION_VISIT_COM_ID];
			$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$this -> base_con = array(
				'com_id=' => $this -> com_id,
				'user_id=' => $this -> user_id,
			);

			$this -> root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
		}

		$this -> wx_mark_tip = array(
			'title' 		=> '您的下属 %name% 的外出计划%title%已%mark_type%',
			'description' 	=> '请及时查看',
			'url'			=> '',
			'picurl'		=> '',
		);
	}

	/**
	 * 获取当前公司的移动外勤配置
	 *
	 * @access public
	 * @return array
	 */
	public function get_conf() {
		$condition = $this -> base_con;
		unset($condition['user_id=']);

		$fields = array(
			'sign_in_pic', 'sign_out_pic', 'sign_in_tip',
			'sign_out_tip', 'sign_in_tip_time', 'sign_out_tip_time',
			'outgoing_type', 'all_junior', 'source_type'
		);
		$fields = implode(',', $fields);
		$result = g('ndb') -> select($this -> conf_table, $fields, $condition);
		if (!$result) {
			throw new SCException('查询公司移动外勤设置失败!');
		}
		$ret = $result[0];

		$ret['sign_in_tip_time'] = intval($ret['sign_in_tip_time']);
		$ret['sign_out_tip_time'] = intval($ret['sign_out_tip_time']);

		return $ret;
	}

	/**
	 * 新建计划
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function add_plan(array $data) {
		$time = time();
		$data['user_id'] = $this -> user_id;
		$data['com_id'] = $this -> com_id;
		$data['update_time'] = $time;
		$data['create_time'] = $time;

		$result = g('ndb') -> insert($this -> plan_table, $data);

		if (!$result) throw new SCException('写入数据库失败！');
		return $result;
	}

	/**
	 * 编辑计划
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param array $data 更新数据集合
	 * @return boolean
	 */
	public function update_plan($pn_id, array $data) {
		$condition = $this -> base_con;
		$condition['pn_id='] = $pn_id;
		//只有状态为1的记录才能更新
		$condition['state='] = 1;

		$data['update_time'] = time();

		$result = g('ndb') -> update_by_condition($this -> plan_table, $condition, $data);
		if (!$result) throw new SCException('更新计划失败！');
		return TRUE;
	}

	/**
	 * 删除一个计划
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param array $data 更新数据集合
	 * @return boolean
	 */
	public function delete_plan($pn_id) {
		$condition = $this -> base_con;
		$condition['pn_id='] = $pn_id;
		//只有状态为1的计划才允许删除
		$condition['state='] = 1;

		$data = array(
			'update_time' => time(),
			'is_del' => 1,
		);

		$result = g('ndb') -> update_by_condition($this -> plan_table, $condition, $data);
		if (!$result) {
			throw new SCException('删除计划失败！');
		}

		return TRUE;
	}

	/**
	 * 上报一次移动外勤状况
	 *
	 * @access public
	 * @param integer $mark_type 签到类型：1-签到、2-签退、3-上传地理位置
	 * @param array $data 数据集合
	 * @return integer 1-成功，否则失败
	 * @throws SCException
	 */
	public function add_report($mark_type, array $data) {
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data['create_time'] = time();

		unset($data['is_last']);

		//签到签退只能签一次
		if ($mark_type != 3) {
			$check_con = $this -> base_con;
			$check_con['pn_id='] = $data['pn_id'];
			$check_con['mark_type='] = $mark_type;
			$check = g('ndb') -> record_exists($this -> report_table, $check_con);
			if ($check) {
				$str = $mark_type == 1 ? '请勿重复签到!' : '请勿重复签退!';
				throw new SCException($str);
			}
		}

		//--begin-------上报表相关------------------
		$up_con = $this -> base_con;
		$up_con['pn_id='] = $data['pn_id'];
		$up_con['is_last='] = 1;
		$up_data = array('is_last' => 0);
		//--end------------------------------------

		//--begin-------计划表相关------------------
		$plan_con = $this -> base_con;
		$plan_con['pn_id='] = $data['pn_id'];
		$plan_data = array('state' => 2);
		$mark_type == 2 and $plan_data['state'] = 3;
		//--end------------------------------------

		$data['mark_type'] = $mark_type;

		g('db') -> begin_trans();

		//更新该计划的其他上报记录的 is_last = 0
		$result = g('ndb') -> update_by_condition($this -> report_table, $up_con, $up_data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('系统繁忙！');
		}

		//写入上报记录
		$rp_id = $result = g('ndb') -> insert($this -> report_table, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('系统繁忙！');
		}

		//签到、签退后更新计划表的记录状态(state)
		if ($mark_type != 3) {
			$result = g('ndb') -> update_by_condition($this -> plan_table, $plan_con, $plan_data);
			if (!$result) {
				g('db') -> rollback();
				throw new SCException('系统繁忙！');
			}
		}

		g('db') -> commit();

		$img_arr = array();
		!empty($data['image_1']) and $img_arr[] = $data['image_1'];
		!empty($data['image_2']) and $img_arr[] = $data['image_2'];
		!empty($data['image_3']) and $img_arr[] = $data['image_3'];
		!empty($data['image_4']) and $img_arr[] = $data['image_4'];
		!empty($data['image_5']) and $img_arr[] = $data['image_5'];

		if (!empty($img_arr)) {
			foreach ($img_arr as $img_path) {
				//加入容量文件列表
				g('capacity') -> add_list_by_cache($this -> com_id, $img_path, $this -> app_name, $this -> report_table, $rp_id);
			}
			unset($img_path);
		}

		return 1;
	}

	/**
	 * 获取计划详情
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param boolean $with_sign 是否返回签到签退信息
	 * @return mixed
	 */
	public function get_plan_info($pn_id, $with_sign = TRUE) {
		$table = $this -> plan_table . ' AS pl, ' . $this -> user_table . ' AS us';
		$fields = array(
			'pl.pn_id', 'pl.title', 'pl.desc', 'pl.address', 'pl.mobile', 
			'pl.longt', 'pl.lat',
			'pl.begin_time', 'pl.end_time', 'pl.outgoing_name', 'pl.state',
			'pl.sign_type', 'pl.user_id', 'us.name AS user_name', 'us.pic_url'
		);
		$fields = implode(',', $fields);

		$condition['pl.com_id='] = $this -> com_id;
		$condition['pl.pn_id='] = $pn_id;
		$condition['pl.is_del='] = 0;
		//特殊用法，表示两端不加引号
		$condition['^pl.user_id='] = 'us.id';

		$ret = g('ndb') -> select($table, $fields, $condition);

		if ($ret) {
			$ret = $ret[0];

			if ($with_sign) {
				$signs = $this -> get_signs($pn_id);
				!empty($signs) and $ret = array_merge($ret, $signs);
			}
		}

		return $ret ? $ret : FALSE;
	}

	/**
	 * 获取外出计划的签到及签退信息
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @return array
	 */
	public function get_signs($pn_id) {
		$condition = $this -> base_con;
		unset($condition['user_id=']);
		$condition['pn_id='] = $pn_id;
		$condition['is_del='] = 0;

		//只获取签到及签退信息
		$condition['mark_type!='] = 3;

		$fields = array(
			'longt', 'lat', 'address', 'mark',
			'image_1', 'image_2', 'image_3', 'image_4',
			'image_5', 'create_time',
		);
		$fields = implode(',', $fields);

		$order = ' ORDER BY mark_type, id ASC';
		$ret = g('ndb') -> select($this -> report_table, $fields, $condition, 0, 0, '', $order);

		if ($ret) {
			$tmp_ret = array();
			$tmp_ret['sign_in'] = $ret[0];

			$length = count($ret);
			$length > 1 and $tmp_ret['sign_out'] = $ret[$length - 1];
			$ret = $tmp_ret;
		}
		return $ret ? $ret : array();
	}

	/**
	 * 获取计划列表
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param boolean $is_self 是否获取自身的计划列表
	 * @return array
	 */
	public function get_plan_list($user_id = 0, $page, $page_size, $is_self = TRUE) {
		empty($user_id) and $user_id = $this -> user_id;
		$ret = array();

		$ids = array($user_id);
		if (!$is_self) {
			$data = $this -> get_subordinate($user_id);
			$ids = $data['user_ids'];
			if ($ids) {	//去除自己的记录
				$idx = array_search($user_id, $ids);
				if ($idx !== false) unset($ids[$idx]);
			}
		}
		
		if (empty($ids)) return $ret;

		$ids = implode(',', $ids);

		$table = $this -> plan_table . ' AS pl, ' . $this -> user_table . ' AS us';
		$fields = array(
			'pl.pn_id', 'pl.title', 'pl.desc', 'pl.address', 
			'pl.begin_time', 'pl.end_time', 'pl.outgoing_name', 'pl.state', 'pl.sign_type',
			'us.name AS user_name', 'us.pic_url', 'us.dept_tree',
		);
		$fields = implode(',', $fields);

		$condition = array();
		$condition['pl.com_id='] = $this -> com_id;
		$condition['pl.is_del='] = 0;
		$condition['^pl.user_id='] = 'us.id';
		$condition['pl.user_id IN '] = '(' . $ids . ')';
		$condition['__OR_1'] = array(
			'__AND_1' => array(
				'pl.sign_type=' => 1,
				'pl.state!=' => 3,
			),
			'__AND_2' => array(
				'pl.sign_type=' => 2,
				'pl.state=' => 1,
			),
		);

		$order = ' ORDER BY pl.pn_id DESC';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);
		$ret['data'] = $this -> complete_dept_info($ret['data']);

		return $ret;
	}

	/**
	 * 获取下属的用户id
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @return array
	 */
	public function get_subordinate($user_id = 0) {
		empty($user_id) and $user_id = $this -> user_id;
		$key_str = __FUNCTION__ . ':user_id:' . $user_id . ':com_id:' . $this -> com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) {
			$ret = json_decode($ret, TRUE);
			return $ret;
		}

		$ret = array(
			'user_ids' => array(),
			'dept_map' => array(),
		);

		//上下级关系方式
		try {
			$conf = $this -> get_conf();
			
		} catch(SCException $e) {
			return $ret;
		}
		
		$sub_list = g('api_leader_relationship') -> list_sub_by_user_id($this -> user_id);
		$sub_dept_ids = $sub_list['depts'];
		$sub_user_ids = $sub_list['users'];
		if (isset($conf['all_junior']) and $conf['all_junior'] != 0) {	//所有下属：下级成员+下级部门及其子..部门成员
			$compare_field = 'dept_tree';
			
		} else {														//直属下属：下级成员+下级部门的直属成员
			$compare_field = 'dept_list';
		}
		
		$or_con = array();
		foreach ($sub_dept_ids as $key => $val) {
			$or_con['__' . $key . '__' .  $compare_field . ' LIKE '] = '%"' . $val . '"%';
		}
		unset($val);

		$result = array();
		if (!empty($or_con)) {
			$condition = array(
				'state!=' => 0,
				'root_id=' => $this -> root_id,
				'__OR_1' => $or_con,
			);
			$condition['id!='] = $this -> user_id;
			$where = g('ndb') -> compose_where($condition);
			log_write('移动外勤，查找直属下属集合，where=' . $where);
	
			$sql = 'SELECT DISTINCT `id` FROM ' . $this -> user_table . $where;
			$result = g('db') -> select($sql);
		}
		!$result && $result = array();
		
		$ret['user_ids'] = $sub_user_ids;
		foreach ($result as $val) {
			$ret['user_ids'][] = $val['id'];
		}unset($val);
		$ret['user_ids'] = array_unique($ret['user_ids']);
		//缓存60秒
		g('redis') -> setex($cache_key, json_encode($ret), 60);
		return $ret;
	}

	/**
	 * 获取自身负责的部门列表
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @return array
	 */
	public function get_main_dept($user_id=0) {
		empty($user_id) and $user_id = $this -> user_id;
		$ret = array();

		$fields = array(
			'id', 'name', 'boss',
			'boss=' . $user_id . ' AS is_boss',
		);
		$fields = implode(',', $fields);
		$condition = array();
		$condition['__OR_1'] = array(
			'boss=' => $user_id,
			'second_boss LIKE ' => '%"'.$user_id.'"%',
		);

		$where = g('ndb') -> compose_where($condition);
		$sql = <<<EOF
SELECT {$fields} FROM {$this -> dept_table} {$where}
EOF;

		$ret = g('db') -> select($sql);
		//log_write('移动外勤，查找自身负责的部门，info=' . json_encode($ret));
		!is_array($ret) and $ret = array();

		return $ret;
	}

	/**
	 * 获取本身负责部门下的全部子部门
	 *
	 * @access public
	 * @param array $dept_ids 部门id集合
	 * @return array
	 */
	public function get_all_dept(array &$dept_info) {
		$ret = array();
		if (empty($dept_info)) return $ret;

		$dept_ids = array();
		foreach ($dept_info as $val) {
			$dept_ids[] = $val['id'];
		}

		$dept_str = implode(',', $dept_ids);

		$fields = array(
			'id', 'name',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'p_id IN ' => '(' . $dept_str . ')',
			'root_id=' => $this -> root_id,
		);

		$result = g('ndb') -> select($this -> dept_table, $fields, $condition);
		!is_array($result) and $result = array();

		$child = $this -> get_all_dept($result);
		$ret = array_merge($dept_info, $child);

		return $ret;
	}

	/**
	 * 获取已签到的计划及计划最后一次上报情况
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param boolean $is_self 是否获取自身的计划列表
	 * @return array
	 */
	public function get_plan_with_reports($user_id = 0, $page, $page_size, $is_self = TURE) {
		empty($user_id) and $user_id = $this -> user_id;
		$ret = array();

		$ids = array($user_id);
		if (!$is_self) {
			$data = $this -> get_subordinate($user_id);
			$ids = $data['user_ids'];
			if ($ids) {	//去除自己的记录
				$idx = array_search($user_id, $ids);
				if ($idx !== false) unset($ids[$idx]);
			}
		}
		
		if (empty($ids)) return $ret;

		$ids = implode(',', $ids);

		$table = $this -> report_table . ' AS rp, ' . $this -> user_table . ' AS us, ' . $this -> plan_table . ' AS pl';
		$fields = array(
			'rp.id', 'rp.pn_id', 
			'rp.mark_type', 'rp.address', 'rp.create_time', 
			'us.name AS user_name', 'us.pic_url', 
			'us.dept_tree', 'pl.title', 
			'pl.state', 'pl.begin_time', 'pl.end_time',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'rp.com_id=' 	 => $this -> com_id,
			'rp.is_last=' 	 => 1,
			'rp.user_id IN ' => '(' . $ids . ')',
			'^rp.user_id=' 	 => 'us.id',
			'^rp.pn_id=' 	 => 'pl.pn_id',
			'pl.is_del=' 	 => 0,
		);

		empty($order) and $order = ' ORDER BY rp.id DESC';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);
		$ret['data'] = $this -> complete_dept_info($ret['data']);
		return $ret;
	}

	/**
	 * 获取计划的上报列表
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @return array
	 */
	public function get_report_list($pn_id) {
		$table = $this -> report_table;
		$fields = array(
			'id',
			'mark_type', 'longt', 'lat', 'address',
			'mark', 'image_1', 'image_2', 'image_3', 
			'image_4', 'image_5', 'create_time',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'com_id=' => $this -> com_id,
			'pn_id=' => $pn_id,
			'is_del=' => 0,
		);

		$order = ' ORDER BY id ASC';

		$ret = g('ndb') -> select($table, $fields, $condition, 0, 0, '', $order, FALSE);

		return $ret ? $ret : array();
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 根据签退类型获取可操作的计划列表
	 *
	 * @access public
	 * @param integer $mark_type 标记状态：1-签到、2-签退、3-上传地理位置
	 * @return array
	 */
	public function get_plan_by_sign($mark_type = 1) {
		$ret = array();
		if ($mark_type != 1 and $mark_type != 2 and $mark_type != 3) return $ret;

		$condition = $this -> base_con;
		$condition['is_del='] = 0;
		if ($mark_type == 1) {
			$condition['state='] = 1;
		}else {
			$condition['state='] = 2;
			$condition['sign_type='] = 1;
		}

		$fields = array(
			'pn_id', 'title', 'state',
			'sign_type',
		);
		$fields = implode(',', $fields);
		$order = ' ORDER BY pn_id ASC ';

		$result = g('ndb') -> select($this -> plan_table, $fields, $condition, 0, 0, '', $order);
		
		return is_array($result) ? $result : $ret;
	}

	/**
	 * 获取指定用户的直属上司id集合
	 *
	 * @access public
	 * @param integer $user_id 用户id，默认是当前用户
	 * @return array
	 */
	public function get_boss_ids($user_id=0) {
		empty($user_id) and $user_id = $this -> user_id;
		$ret = array();

		$fields = array(
			'dept_list', 'dept_tree',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'id=' => $user_id,
		);
		$result = g('ndb') -> select($this -> user_table, $fields, $condition, 1, 1);
		!is_array($result) and $result = array();
		isset($result[0]) and $result = $result[0];
		// log_write('legwork -> get_boss_ids: result=' . json_encode($result));

		//查询是否查看全部的下级部门人员计划
		$all_check = FALSE;
		try {
			$conf = $this -> get_conf();
			$conf['all_junior'] != 0 and $all_check = TRUE;
		}catch(SCException $e) {}

		$fields_idx = 'dept_list';
		//通知全部上级，包括非直属
		$all_check and $fields_idx = 'dept_tree';
		//log_write('legwork -> get_boss_ids: all_check=' . $all_check);
		
		$result = isset($result[$fields_idx]) ? json_decode($result[$fields_idx], TRUE) : array();
		!is_array($result) and $result = array();

		$dept_ids = $result;
		// log_write('legwork -> get_boss_ids: dept_ids=' . json_encode($dept_ids));

		$tmp_depts = array();
		foreach ($dept_ids as $val) {
			if (!is_array($val)) {
				$tmp_int = intval($val);
				!empty($tmp_int) and $tmp_depts[] = $tmp_int;
				continue;
			}

			foreach ($val as $cval) {
				$tmp_int = intval($cval);
				!empty($tmp_int) and $tmp_depts[] = $tmp_int;
			}
		}
		unset($val);
		$dept_ids = $tmp_depts;

		if (empty($dept_ids)) {
			return $ret;
		}

		$fields = array(
			'boss',
			'second_boss',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'id IN ' => '(' . implode(',', $dept_ids) . ')',
			'boss!=' => $user_id,
		);
		// log_write('legwork -> get_boss_ids: condition=' . json_encode($condition));

		$result = g('ndb') -> select($this -> dept_table, $fields, $condition);
		!is_array($result) and $result = array();

		// log_write('legwork -> get_boss_ids: result=' . json_encode($result));

		$ret = array();
		foreach ($result as $val) {
			$ret[$val['boss']] = 1;

			$tmp_boss = json_decode($val['second_boss'], TRUE);
			!is_array($tmp_boss) and $tmp_boss = array();
			foreach ($tmp_boss as $cval) {
				$tmp_int = intval($cval);
				!empty($tmp_int) and $ret[$tmp_int] = 1;
			}
			unset($cval);
		}
		unset($val);

		unset($ret[$user_id]);
		$ret = array_keys($ret);
		// log_write('legwork -> get_boss_ids: ret=' . json_encode($ret));

		return $ret;
	}

	/**
	 * 发送签到或签退的提醒消息到微信端
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param string $user_id 计划操作者的用户id
	 * @param string $user_name 计划操作者的用户名称
	 * @param string $title 计划标题
	 * @param integer $mark_type 签到类型 1-签到、2-签退、3-上传地理位置
	 * @return boolean
	 */
	public function send_wx_mark_tip($pn_id, $user_id, $user_name, $title, $mark_type) {
		$boss_ids = $this -> get_boss_ids($user_id);
		if (empty($boss_ids)) {
			log_write('移动外勤，发送微信签到、签退提醒消息时，没有找到有效的上司');
			return FALSE;
		}

		$mark_str = '';
		switch($mark_type) {
			case 1 : {
				$mark_str = '签到';
				BREAK;
			}
			case 2 : {
				$mark_str = '签退';
				BREAK;
			}
			default : {
				$mark_str = '更新地理位置';
				BREAK;
			}
		}

		$article = $this -> wx_mark_tip;
		$article['title'] = str_replace('%name%', $user_name, $article['title']);
		$article['title'] = str_replace('%title%', '《'. $title . '》', $article['title']);
		$article['title'] = str_replace('%mark_type%', $mark_str, $article['title']);

		$article['url'] = SYSTEM_HTTP_DOMAIN . '?app=legwork&m=plan&a=info&id=' . $pn_id;
		$art_list = array($article);

		g('nmsg') -> init($this -> com_id, 'legwork');
		$ret = g('nmsg') -> send_news(array(), $boss_ids, $art_list);
		return $ret;
	}

	/**
	 * 发送签到或签退的提醒消息到pc端
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param string $user_id 计划操作者的用户id
	 * @param string $user_name 计划操作者的用户名称
	 * @param string $title 计划标题
	 * @param integer $mark_type 签到类型 1-签到、2-签退、3-上传地理位置
	 * @return boolean
	 */
	public function send_pc_mark_tip($pn_id, $user_id, $user_name, $title, $mark_type) {
		$boss_ids = $this -> get_boss_ids($user_id);
		if (empty($boss_ids)) {
			log_write('移动外勤，发送pc签到、签退提醒消息时，没有找到有效的上司');
			return FALSE;
		}

		$mark_str = '';
		switch($mark_type) {
			case 1 : {
				$mark_str = '签到';
				BREAK;
			}
			case 2 : {
				$mark_str = '签退';
				BREAK;
			}
			default : {
				$mark_str = '更新地理位置';
				BREAK;
			}
		}

		//---pc端消息提醒---
		$tip_config = include(SYSTEM_ROOT . 'tip.config.php');
        if (is_array($tip_config)) {
            $tip_config = $tip_config['legwork'];

            $msg_title = $tip_config['title_model'];
            $url = $tip_config['url_pfx'];
            $app_name = $tip_config['app_name'];

            $msg_title = str_replace('%name%', $user_name, $msg_title);
			$msg_title = str_replace('%title%', '《'. $title . '》', $msg_title);
			$msg_title = str_replace('%mark_type%', $mark_str, $msg_title);

            $data = array(
            	'title' => $msg_title,
            	'url' => $url,
                'app_name' => $app_name,
            );

            g('messager') -> publish_by_ids('pc', array(), $boss_ids, $data);
            return TRUE;
        }

		return FALSE;
	}

	/**
     * 对结果集补全部门信息
     *
     * @access private
     * @param array $data 员工信息数据结果集  
     * @return array
     */
    private function complete_dept_info(array &$data) {
        if (empty($data)) return $data;

        $table = 'sc_dept';

        $ids = array();
        foreach ($data as $val) {
            $tmp_dept = json_decode($val['dept_tree'], TRUE);
            if (!is_array($tmp_dept) or empty($tmp_dept)) continue;

            foreach ($tmp_dept as $cval) {
                if (!is_array($val)) {
                    $ids[] = $val;
                }else {
                    foreach ($cval as $dcval) {
                        $ids[] = $dcval;
                    }
                    unset($dcval);
                }
            }
            unset($cval);
        }
        unset($val);
        $ids = array_unique($ids);
        $ids = implode(',', $ids);

        $fields = array(
            'id', 'name',
        );
        $fields = implode(',', $fields);
        $condition = array(
            'id IN ' => '(' . $ids . ')',
        );

        $depts = g('ndb') -> select($table, $fields, $condition);
        !is_array($depts) and $depts = array();

        $tmp_depts = array();
        foreach ($depts as $val) {
            $tmp_depts[$val['id']] = $val['name'];
        }
        unset($val);
        $depts = $tmp_depts;

        foreach ($data as &$val) {
            $val['dept_desc'] = $dept_desc = array();
            
            $tmp_dept = json_decode($val['dept_tree'], TRUE);
            if (!is_array($tmp_dept) or empty($tmp_dept)) continue;

            foreach ($tmp_dept as $cval) {
                $tmp_short = isset($depts[$cval[0]]) ? $depts[$cval[0]] : '';
                $tmp_long = '';
                
                foreach ($cval as $dcval) {
                    $tmp_suffix = empty($tmp_long) ? '' : ('>' . $tmp_long);
                    $tmp_long = $depts[$dcval] . $tmp_suffix;
                }
                unset($dcval);
                $dept_desc[] = array(
                    'short' => $tmp_short,
                    'long' => $tmp_long,
                );
            }
            unset($cval);

            $val['dept_desc'] = $dept_desc;
        }
        unset($val);

        return $data;
    }
}

/* End of this file */