<?php
/**
 * 通讯录应用数据模型
 *
 * @author LiangJianMing
 * @create 2015-06-26
 */

class cls_app_contact {
    //当前用户id
    private $user_id = 0;
    private $dept_table = 'sc_dept';

    public function __construct() {
        isset($_SESSION[SESSION_VISIT_USER_ID]) and $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
    }

    /**
     * 根据关键字查找员工信息
     *
     * @access public
     * @param string $key_word 关键字
     * @param integer $page 页码
     * @param integer $page_size 每页多少条记录
     * @param string $order 排序方式
     * @return array
     * @throws SCException
     */
    public function search_user($key_word='', $page=1, $page_size=20, $order='') {
        $conf = $this -> get_contact_setting();
        
        $search = $conf['search_list'];
        $secret = $conf['secret_list'];

        $tmp_secret = array();
        foreach ($secret as $val) {
            $tmp = intval($val);
            !empty($tmp) and $tmp_secret[] = $tmp;
        }
        unset($tmp);
        $secret = $tmp_secret;

        //查询虚拟结构条件
        $st_con = $this -> get_struct_condition();
        if (empty($st_con)) {
            return array();
        }

        $root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
        $condition = array(
            'root_id=' => $root_id,
            'state!=' => 0,
            '__OR_123' => $st_con,
        );
        !empty($secret) and $condition['id NOT IN '] = '(' . implode(',', $secret) . ')';

        if (!empty($key_word)) {
            $or_con = array();
            in_array('name', $search) and $or_con['name LIKE '] = '%' . $key_word . '%';
            in_array('acct', $search) and $or_con['acct LIKE '] = '%' . $key_word . '%';
            in_array('position', $search) and $or_con['position LIKE '] = '%' . $key_word . '%';
            in_array('wx_acct', $search) and $or_con['weixin_id LIKE '] = '%' . $key_word . '%';
            in_array('phone', $search) and $or_con['mobile LIKE '] = '%' . $key_word . '%';
            in_array('tel', $search) and $or_con['tel LIKE '] = '%' . $key_word . '%';

            !empty($or_con) and $condition['__OR_1'] = $or_con;
        }

        $key_str = __FUNCTION__ . "::" . json_encode($condition) . "::{$page}::{$page_size}::{$order}";
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('redis') -> get($cache_key);

        if (!empty($ret)) {
            $ret = json_decode($ret, true);
            !is_array($ret) and $ret = array();
            return $ret;
        }
        
        $table = 'sc_user';
        $fields = array(
            'id', 'acct', 'name',
            'all_py',
            'dept_list', 'position', 'mobile',
            'gender', 'tel', 'email',
            'weixin_id', 'ext_attr', 'pic_url',
            'state', 'create_time', 'update_time',
            'root_id', 'work_position', 'dept_tree',
        );
        $fields = implode(',', $fields);
        empty($order) and $order = ' ORDER BY all_py ASC, id ASC ';

        $ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order);
        !is_array($ret) and $ret = array();

        // 缓存30分钟
        g('redis') -> setex($cache_key, json_encode($ret), 1800);
        
        return $ret;
    }

    /**
     * 将虚拟结构部门树整理为适用于前端的数据
     *
     * @access private
     * @param array $childs 需要整理的子集合，为空即可
     * @return array
     */
    private function range_dept_tree(array &$childs=array()) {
        if (empty($childs)) {
            $childs = g('share_struct') -> get_depts_tree();
        }

        $ret = array();
        foreach ($childs as &$val) {
            if (!empty($val['children'])) {
                $this -> range_dept_tree($val['children']);
            }

            $val['childs'] = &$val['children'];
        }
        unset($val);

        $ret = &$childs;
        return $ret;
    }

    /**
     * 获取虚拟结构的权限条件
     *
     * @access private
     * @return array
     */
    public function get_struct_condition() {
        $ret = array();
        
        $dept_ids = g('share_struct') -> get_permission_dept();
        if (empty($dept_ids)) {
            return $ret;
        }

        // 使用正则表达式
        $ret['dept_tree REGEXP '] = '"(' . implode('|', $dept_ids) . ')"';

        return $ret;
    }

    /**
     * 获取通讯录配置
     *
     * @access public  
     * @return array
     * @throws SCException
     */
    public function get_contact_setting() {
        $com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $com = g('company') -> search($com_id);
        $conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
        $conf['search_list'] = json_decode($conf['search_list'], TRUE);
        !is_array($conf['search_list']) and $conf['search_list'] = array();

        $conf['show_list'] = json_decode($conf['show_list'], TRUE);
        !is_array($conf['show_list']) and $conf['show_list'] = array();

        $conf['secret_list'] = json_decode($conf['secret_list'], TRUE);
        !is_array($conf['secret_list']) and $conf['secret_list'] = array();

        $conf['general_list'] = json_decode($conf['general_list'], TRUE);
        !is_array($conf['general_list']) and $conf['general_list'] = array();

        return $conf;
    }

    /**
     * 获取缓存变量名
     *
     * @access private
     * @param string $str 关键字符串
     * @return string
     */
    private function get_cache_key($str) {
        $key_str = SYSTEM_COOKIE_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
        $key = md5($key_str);
        return $key;
    }
}

/* End of this file */