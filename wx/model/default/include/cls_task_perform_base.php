<?php
/**
 * 任务信息
 * @author lijuns
 * @date 2014-12-05
 */
class cls_task_perform_base extends cls_task_base{
	/**
	 * 插入数据
	 * @throws SCException
	 */
	public function save($perform_info) {
		$data = array(
			'taskid' => $perform_info['taskid'],
			'perform_id' => $perform_info['perform_id'],
			'perform_name' => $perform_info['perform_name'],
			'state' => $perform_info['state'],
			'finish_time' => $perform_info['finish_time'],
			'info_state' => parent::$StateOn,
			'create_time' => time()
		);
		$ret = g('ndb') -> insert(parent::$TablePerform, $data);
		if (!$ret) {
			throw new SCException('保存承办人信息失败');
		}
		return $ret;
	}
	
	public function get_perform_info($task_id,$user_id,$fields='*'){
		$task_id = (int)$task_id;
		if(empty($task_id)){
			return false;
		}
		log_write('开始获取perform');
		$cond = array(
			'taskid=' => $task_id,
			'perform_id=' => $user_id
		);
		$ret = g('ndb') -> select(parent::$TablePerform,$fields,$cond, 1, 1, '', ' order by state desc ');
		return $ret ? $ret[0] : false;
	}
	
	
	/**
	 * 获取指定taskid的最高状态的负责人信息
	 * @param unknown_type $task_id
	 * @param unknown_type $fields
	 */
	public function get_running_perform($task_id,$fields ='*'){
		$task_id = (int)$task_id;
		if(empty($task_id)){
			return false;
		}
		$cond = array(
			'taskid=' => $task_id,
			'state !=' => parent::$StateOntime,
			'state!=' => parent::$StateOverdue
		);
		$ret = g('ndb') -> select(parent::$TablePerform,$fields,$cond, 1, 1, '', ' order by state desc ');
		return $ret ? $ret[0] : false;
	}
	
	
	/**
	 * 批量插入承办人信息
	 */
	public  function batch_save($perform_info_arr) {
		$ret = g('ndb') -> batch_insert(parent::$TablePerform,"taskid,perform_id,perform_name,info_state,create_time,state",$perform_info_arr);
		if (!$ret) {
			throw new SCException('批量保存承办人信息失败');
		}
		return $ret;
	}
	
	/**
	 * 删除承办人任务信息
	 */
	public function delete($taskid){
		$data = array(
			'info_state' => parent::$StateOff,
			'info_time' =>time()
		);
		$cond = array(
			'taskid='=>$taskid,
		); 
		$ret = g('ndb') -> update_by_condition(parent::$TablePerform, $cond, $data);
		if (!$ret) {
			throw new SCException('删除承办人信息失败');
		}
		return $ret;
	}
	
	/**
	 * 根据taskid获取承办人信息
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function get_by_com($taskid, $fields='*') {
		$cond = array(
			'taskid=' => $taskid,
			'info_state=' => parent::$StateOn
		);
		return g('ndb') -> select(parent::$TablePerform, $fields, $cond, 0, 0, '', ' order by id asc ');
	}
	
	
	/**
	 * 申请完成 延期 退回 更新承办人状态
	 */
	public function update_apply_info($taskid,$userid,$perform_data){
		if(parent::get_user_perform($taskid, $userid)){
			$cond = array(
				'perform_id='=>$userid,
				'taskid='=>$taskid,
				'info_state='=>parent::$StateOn
			); 
			$ret = g('ndb') -> update_by_condition(parent::$TablePerform, $cond, $perform_data);
			if (!$ret) {
				throw new SCException('更新承办人信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('更新承办人信息失败');
		}
	}
	
	/**
	 * 审批申请完成 延期 退回 更新承办人状态(更新子任务时需要去掉权限控制)
	 */
	public function update_approve_info($taskid,$assign_id,$userid,$perform_data,$is_need_role=TRUE){
	if(!$is_need_role||parent::get_user_assign($taskid, $assign_id)){
			if(empty($userid)){
				$cond = array(
					'taskid=' => $taskid,
				);
			}else{
				$cond = array(
					'perform_id=' => $userid,
					'taskid=' => $taskid,
				); 
			}
			$ret = g('ndb') -> update_by_condition(parent::$TablePerform, $cond, $perform_data);
			if (!$ret) {
				throw new SCException('审批更新承办人信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('审批更新承办人信息失败');
		}
	}
	
	
	
	/**
	 * 退回 更新承办人
	 */
	public function update_callback_perform($taskid,$userid,$perform_id,$perform_data){
		if(parent::get_user_assign($taskid, $userid)){
			$cond = array(
				'taskid='=>$taskid,
				'perform_id='=>$perform_id,
				'info_state='=>parent::$StateOn
			); 
			$ret = g('ndb') -> update_by_condition(parent::$TablePerform, $cond, $perform_data);
			if (!$ret) {
				throw new SCException('更新承办人信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限更新承办人信息');
		}
	}
	
	
	
}

// end of file