<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_workinst {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_workitem';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	
	/**
	 * 人员状态
	 */
	private static $UserDel = 0;//删除用户
	
	private static $UserFollow  = 2;//关注用户
	
	private static $UserFloze = 3;//冻结用户
	
	private static $UserNotFollow  = 4;//未关注
	
	/**
	 * 人员选择范围
	 */
	private static $RecriverFixed = 1;//固定接收人
	
	private static $RecriverCreater = 2;//流程发起人
	
	private static $RecriverCreaterLeader = 3;//流程发起人领导
	
	private static $RecriverCreaterDept = 4;//流程发起人部门
	
	private static $RecriverHandler = 5;//当前处理人
	
	private static $RecriverHandlerLeader = 6;//当前处理人上级领导
	
	private static $RecriverHandlerDept = 7;//当前处理人部门
	
	private static $RecriverInput = 8;//表单具体数据
	
	/**
	 * 比较符号
	 */
	
	private static $Compare_Greater = 1;//大于
	
	private static $Compare_Lesser = 2;//小于
	
	private static $Compare_Equeal = 3;//等于
	
	private static $Compare_Lesser_Equeal = 4;//小于等于
	
	private static $Compare_Greater_Equeal = 5;//大于等于
	
	private static $Compare_Not_Equeal = 6;//不等于
	
	//人员规则
	private static $People_Position = 7;//人员职位
	
	private static $People_Type_Creater =2;//发起人
	
	private static $People_Type_Done =1;//当前处理人
	
	private static $Compare_Contain = 8;//包含
	
	private static $Compare_Not_Contain = 9;//不包含
	
	/**
	 * 工作项状态
	 */
	/** 新收到1 */
	private static $StateNew = 1;
	/** 已阅读 2 */
	private static $StateRead = 2;
	/** 已保存 3 */
	private static $StateSave = 3;
	/** 等待中 4 */
	private static $StateWait = 4;
	/** 回收 5 */
	private static $StateRecovery = 5;
	/** 终止 6 */
	private static $StateStop = 6;
	/** 已处理 7 */
	private static $StateSend = 7;
	/** 已退回 8 */
	private static $StateCallback = 8;
	/** 已通过 9 */
	private static $StateDone = 9;
	/** 退回 10 */
	private static $StateSendback = 10;
	
	/** 是否为开始工作项  1 是 */
	private static $StartWItem = 1;
	/** 是否为开始工作项  0 是 */
	private static $NotStartWItem = 0;
	
	
	/**
	 * 保存工作项new
	 * @param array $work_item 工作项数组
	 */
	public function save_workitem($work_item) {
		$data = array(
			'formsetinit_id' => $work_item["formsetinit_id"],
			'workitem_name' => $work_item["workitem_name"],
			'com_id' => $work_item["com_id"],
			'handler_id' => $work_item["handler_id"],
			'handler_name' => $work_item["handler_name"],
			'handler_dept_id' =>$work_item["handler_dept_id"],
			'handler_dept_name' =>$work_item["handler_dept_name"],
			'receive_time' => time(),
			'receiver_id' => empty($work_item["receiver_id"])?NULL:$work_item["receiver_id"],
			'receiver_name' => empty($work_item["receiver_name"])?NULL:$work_item["receiver_name"],
			'receiver_dept_id' =>empty($work_item["receiver_dept_id"])?NULL:$work_item["receiver_dept_id"],
			'receiver_dept_name' =>empty($work_item["receiver_dept_name"])?NULL:$work_item["receiver_dept_name"],
			'state' => $work_item["state"],
			'creater_id' => $work_item["creater_id"],
			'creater_name' => $work_item["creater_name"],
			'creater_dept_id' =>$work_item["creater_dept_id"],
			'creater_dept_name' =>$work_item["creater_dept_name"],
			'work_node_id' =>$work_item["work_node_id"],
			'pre_work_node_id'=>empty($work_item["pre_work_node_id"])?NULL:$work_item["pre_work_node_id"],
			'is_returnback' =>$work_item["is_returnback"],
			'returnback_node_id' =>$work_item["returnback_node_id"],
			'info_state' =>self::$StateOn,
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存表单实例失败');
		}
		
		return $ret;
		
	}
	
	
	/**
	 * 
	 * 更新工作项内容new
	 * @param int $workitem_id
	 * @param int $com_id
	 * @param string $judgement
	 * @throws SCException
	 */
	public function update_workitem($workitem_id,$com_id,$judgement,$is_linksign=0){
		$cond = array(
			'id=' => $workitem_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'state' => self::$StateSave,
			'judgement' => $judgement,
			'is_linksign' => $is_linksign
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 更新事项的状态new
	 * @param int $work_node_id
	 * @param int $formsetinst_id
	 * @param int $com_id
	 * @param int $state 原来状态
	 * @param int $change_state 修改状态
	 */
	public function update_state_workitem($com_id,$formsetinst_id,$work_node_id,$state,$change_state){
		$cond = array(
			'work_node_id=' => $work_node_id,
			'com_id=' => $com_id,
			'state=' =>$state,
			'formsetinit_id=' =>$formsetinst_id
		);

		$data = array(
			'state' => $change_state,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项状态失败');
		}
		
		return $ret;
	}
	
	
	
	/**
	 * 
	 * 发送流程
	 * @param unknown_type $workitemid
	 * @param unknown_type $com_id
	 * @param unknown_type $work_item
	 * @throws SCException
	 */
	public function send_workitem($workitemid,$com_id,$work_item){
		$cond = array(
			'id=' => $workitemid,
			'com_id=' => $com_id,
		);

		$data = array(
			'complete_time' => $work_item["complete_time"],
			'state' => self::$StateSend,
			'consuming_time' => $work_item["consuming_time"],
			'receiver_info' => $work_item["receiver_info"],
			'target_node' => $work_item["target_node"]
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 退回工作项new
	 */
	public function callback_workitem($workitem_id,$com_id,$work_item){
		$cond = array(
			'id=' => $workitem_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'complete_time' => $work_item["complete_time"],
			'state' => self::$StateCallback,
			'consuming_time' => $work_item["consuming_time"],
			'receiver_info' => $work_item["receiver_info"],
			'target_node' => $work_item["target_node"],
			'judgement' => $work_item["judgement"]
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	
	/**
	 * 
	 * 根据流程实例id删除工作项
	 * @param unknown_type $workitemid
	 * @param unknown_type $com_id
	 * @param unknown_type $work_item
	 * @throws SCException
	 */
	public function delete_workitem($formsetinst_id,$com_id,$user_id){
		$cond = array(
			'formsetinit_id=' => $formsetinst_id,
			'com_id=' => $com_id,
		);

		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_user_id' => $user_id
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('删除工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 根据工作项id更新工作项的状态new
	 * @param int $workitem_id
	 * @param int $com_id
	 * @param int $state
	 * @throws SCException
	 */
	public function update_workitem_state($workitem_id,$com_id,$state){
		$cond = array(
			'id=' => $workitem_id,
			'com_id=' => $com_id,
		);
		$data = array(
			'state' => $state,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项状态失败');
		}
		return $ret;
	}
	
	
	/**
	 * 更新前一工作项id new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param string $pre_work_node_id 前工作项id集合
	 * @param int $com_id
	 */
	public function update_same_handlers($formsetinst_id,$work_node_id,$pre_work_node_id,$com_id){
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn,
			'formsetinit_id=' =>$formsetinst_id,
			'work_node_id=' =>$work_node_id
		);

		$data = array(
			'pre_work_node_id' => $pre_work_node_id,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 更新退回状态的工作项
	 */
	public function update_sendback_workitem($formsetinit_id,$returnback_node_id,$com_id){
		
		$update = "update ".self::$Table." set state=temp_state".
		" where  returnback_node_id=".$returnback_node_id." and info_state=".self::$StateOn.
		" and state=".self::$StateSendback." and com_id= ".$com_id." and formsetinit_id = ".intval($formsetinit_id);
		
		$ret = g('db') -> query($update);
		if (!$ret) {
			throw new SCException('更新工作项状态失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 更新当前同一个节点的工作项的状态new
	 * @param int $workitem_id
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $state
	 * @param int $returnback_node_id 退回工作项id
	 */
	public function update_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,$state,$select_state,$com_id,$returnback_node_id=0){
		$set_str="";
		if($returnback_node_id!=0){
			$set_str .="returnback_node_id=".intval($returnback_node_id).",temp_state=state,";
		}
		$set_str .= "state=".$state;
		$update = "update ".self::$Table." set ".$set_str.
		" where id!=".intval($workitem_id).
		" and formsetinit_id=".intval($formsetinst_id).
		" and work_node_id=".intval($work_node_id).
		" and info_state=".self::$StateOn.
		" and state in(".implode(",",$select_state).")";
		
		$ret = g('db') -> query($update);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 
	 * 更新等待中工作项的状态new
	 * @param unknown_type $workitem_id
	 * @param unknown_type $formsetInst_id
	 * @param unknown_type $work_id
	 */
	public function update_formset_state_item($formsetinst_id,$state,$select_state,$com_id,$returnback_node_id=0){
		$set_str="";
		if($returnback_node_id!=0){
			$set_str .="returnback_node_id=".intval($returnback_node_id).",temp_state=state,";
		}
		$set_str .= "state=".$state;
		$update = "update ".self::$Table." set ".$set_str.
		" where formsetinit_id=".intval($formsetinst_id).
		" and state in(".implode(",",$select_state).")".
		" and info_state=".self::$StateOn;
		
		$ret = g('db') -> query($update);
		if (!$ret) {
			throw new SCException('更新工作项失败');
		}
		return $ret;
	}
	
	/**
	 * 根据工作项id获取表单信息，流程信息new
	 * @param int $workitem_id 工作项id
	 * @param int $com_id
	 * @param int $user_id
	 * @param string $field
	 */
	 
	public function get_curr_workitem($workitem_id,$com_id,$user_id,$field='mpw.*,su.pic_url,mpfm.form_describe,mpwn.is_start,mpwn.is_auto_notify,mpwn.notify_rule,mpf.work_id,mpwn.input_visble_rule,mpwn.input_visit_rule,mpfm.inputs,mpwn.node_type,mpwn.circulation_limit_rule,mpwn.work_node_describe,mpwn.file_type,mpwn.work_type'){
		$sql = "select ".$field." 
		from mv_proc_workitem mpw,mv_proc_formsetinst mpf,mv_proc_form_model mpfm,mv_proc_work_node mpwn,sc_user su 
		where mpfm.id = mpf.form_id and mpw.formsetinit_id = mpf.id and mpwn.id = mpw.work_node_id and su.id = mpw.creater_id 
		and mpw.id = ".intval($workitem_id)." and mpw.com_id = ".$com_id." and mpw.handler_id = ".$user_id;
		$ret = g('db') -> select_one($sql);
		
		return $ret ? $ret : FALSE;
	}
	
/**
	 * 根据节点id获取对应的工作项new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $com_id
	 * @param string $field
	 */
	public function get_workitem_by_nodeid($formsetinst_id,$work_node_id,$com_id,$user_id,$field='mpw.*,su.pic_url,mpwn.is_start,mpwn.is_auto_notify,mpwn.notify_rule,mpwn.input_visble_rule,mpwn.input_visit_rule,mpwn.node_type,mpwn.circulation_limit_rule,mpwn.work_node_describe,mpwn.file_type,mpwn.work_type '){
		$sql = "select ".$field.
		"from mv_proc_workitem mpw,mv_proc_work_node mpwn,sc_user su 
		where mpw.work_node_id = mpwn.id and su.id = mpw.handler_id 
		and mpw.formsetinit_id=".$formsetinst_id.
		" and mpw.work_node_id = ".$work_node_id.
		" and mpw.com_id = ".$com_id.
		" and mpw.handler_id = ".$user_id;
		
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 根据工作项ids获取表单信息，流程信息new
	 * 
	 */
	public function get_item_by_ids($workitemids,$com_id){
		$sql = "select mpw.*,su.pic_url,mpwn.is_start 
		from mv_proc_workitem mpw,mv_proc_formsetinst mpf,mv_proc_form_model mpfm,mv_proc_work_node mpwn,sc_user su 
		where mpfm.id = mpf.form_id and mpw.formsetinit_id = mpf.id and mpwn.id = mpw.work_node_id and su.id = mpw.creater_id 
		and mpw.info_state=".self::$StateOn." and mpw.com_id = ".$com_id;
		
		if(count($workitemids)==0){
			return FALSE;
		}
		$sql .= " and mpw.id in(".implode(",",$workitemids).")";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 获取当前节点指定状态的工作项（除去指定的事项id）new
	 * @param int $workitem_id 工作项id
	 * @param int $formsetinst_id 实例id
	 * @param int $work_node_id 节点id
	 */
	public function get_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,$state,$com_id,$fields='*'){
		$sql = "select ".$fields." from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		" and com_id = ".$com_id.
		" and work_node_id = ".$work_node_id.
		" and id !=".$workitem_id.
		" and info_state=".self::$StateOn .
		" and state in(".implode(",",$state).")";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取当前表单实例指定状态的工作项（除去指定的事项id）new
	 * @param int $workitem_id 工作项id
	 * @param int $formsetinst_id 实例id
	 * @param int $com_id
	 */
	public function get_formset_state_item_nwi($formsetinst_id,$workitem_id,$state,$com_id,$fields='*'){
		$sql = "select ".$fields." from ".self::$Table." 
		where formsetinit_id = ".intval($formsetinst_id).
		" and com_id = ".$com_id.
		" and info_state=".self::$StateOn.
		" and id !=".intval($workitem_id)." 
		and state in(".implode(",",$state).")";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 判断同一节点是否有同一个处理人new
	 * @param int $formsetinst_id
	 * @param int $work_node_id
	 * @param int $handler_id 处理人id
	 * @param int $com_id
	 */
	public function is_same_handlers($formsetinst_id,$work_node_id,$handler_id,$com_id){
		$sql = "select * from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		" and work_node_id = ".$work_node_id.
		" and handler_id = ".$handler_id.
		" and info_state=".self::$StateOn .
		" and ( state =".self::$StateNew.
		" or state =".self::$StateRead.
		" or state =".self::$StateSave.
		" or state =".self::$StateWait.")";
		
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 *  根据节点id获取节点某个状态的事项new
	 *  @param int $com_id
	 *  @param int $formsetinst_id
	 *  @param int $work_node_id 当前节点id
	 *  @param array $state 需要获取的状态
	 */
	public function get_node_state_item($formsetinst_id,$work_node_id,$state,$com_id,$field='*'){
		$sql = "select ".$field." from ".self::$Table." 
		where formsetinit_id=".$formsetinst_id.
		" and com_id = ".$com_id.
		" and work_node_id=".$work_node_id.
		" and info_state = ".self::$StateOn.
		" and state in(".implode(",",$state).")";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	
	/**
	 * 获取当前表单实例指定状态的工作项
	 * @param int $formsetinst_id
	 * @param array $state
	 * @param int $com_id
	 */
	public function get_formset_state_workitem($formsetinst_id,$state,$com_id,$field='*'){
		$sql = "select ".$field." from ".self::$Table." 
		where formsetinit_id=".intval($formsetinst_id).
		" and com_id = ".$com_id.
		" and info_state=".self::$StateOn .
		" and state in(".implode(",",$state).")";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取流程监控数据new
	 */
	public function get_workitem_list($formsetinst_id,$com_id,$page=0,$page_size=0,$fields='mpw.*,su.pic_url,mpwn.work_type',$order_by=" order by complete_time is null,complete_time  ",$with_count = FALSE){
		$state_str = " and mpw.state in(".self::$StateNew.",".self::$StateRead.",".self::$StateSave.",".self::$StateWait.",".self::$StateStop.",".self::$StateSend.",".self::$StateCallback.",".self::$StateDone.")";
		
		$sql = "select ".$fields." from ".self::$Table." mpw, sc_user su,mv_proc_work_node mpwn 
		where mpw.handler_id = su.id and mpw.work_node_id = mpwn.id 
		and mpwn.com_id=".$com_id." and mpw.com_id = ".$com_id." and mpw.info_state =".self::$StateOn." and mpw.formsetinit_id =".$formsetinst_id.$state_str;
		
		
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		
		$ret = g('db') -> select($limit_sql);
		
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
		}
		return $ret ? $ret : FALSE;
	}
	
	/**
	 * 获取待办已办列表
	 */
	public function get_do_list($condition = Array(),$com_id,$user_id,$is_finish=0,$is_proc_type=0,$page=0,$page_size=0,$fields='mpf.formsetinst_name,mpf.create_time,mpfm.form_name,mpf.state formsetinst_state,mpw.*,su.pic_url',$order_by=" order by receive_time desc ",$with_count = FALSE){
		$sql = "select ".$fields." from mv_proc_form_model mpfm,mv_proc_formsetinst mpf,mv_proc_workitem mpw,sc_user su 
		where mpfm.id = mpf.form_id and mpf.id = mpw.formsetinit_id and su.id = mpw.creater_id 
		and mpw.handler_id = ".$user_id." and mpw.com_id = ".$com_id." and mpw.info_state =".self::$StateOn." and mpf.info_state =".self::$StateOn." and mpf.state!=0 and mpf.is_other_proc=".$is_proc_type;
		
		if($is_finish==0){//待办
			$state = " and (mpw.state=".self::$StateNew." or mpw.state=".self::$StateRead." or mpw.state=".self::$StateSave.")";
		}
		else{//已办
			$state = " and (mpw.state=".self::$StateSend." or mpw.state=".self::$StateDone." or mpw.state=".self::$StateCallback.")";
		}
		
		$sql = $sql.$state;
		
		foreach ($condition as $key => $val) {
			if($key=="formsetinst_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.formsetinst_name like '%".mysql_escape_string($val)."%'";
				continue;
			}
			if($key=="creater_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.creater_name like '%".mysql_escape_string($val)."%'";
				continue;
			}
			
			if($key=="s_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time>=".mysql_escape_string($val);
				continue;
			}
			if($key=="e_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time<".mysql_escape_string($val);
				continue;
			}
			if($key=="s_receive_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.receive_time>=".mysql_escape_string($val);
				continue;
			}
			if($key=="e_receive_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.receive_time<".mysql_escape_string($val);
				continue;
			}
			if($key=="state"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpw.state=".mysql_escape_string($val);
				continue;
			}
			if($key=="formsetinst_state"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.state=".mysql_escape_string($val);
				continue;
			}
		}
		unset($val);
		
		
		if($is_finish==1){
			$sql = "SELECT * from (".$sql." order by mpw.formsetinit_id,mpw.id desc) a  GROUP BY a.formsetinit_id ";
		}
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
		
		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') a';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
		}
		return $ret ? $ret : FALSE;
	}
	
	//---------------------------------------内部实现---------------------------------------------------------

	
	
	
	
}

// end of file