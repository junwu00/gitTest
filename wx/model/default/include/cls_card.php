<?php
/**
 * 客户名片数据模型
 *
 * @author LiangJianMing
 * @create 2014-12-19
 */
class cls_card {
	//公司id
	private $com_id 	= NULL;
	//用户id
	private $user_id 	= NULL;
	//微信id
	private $wx_id 		= NULL;

	//名片配置表
	public $conf_table 	= 'card_conf';
	//名片信息表
	public $info_table 	= 'card_info';
	//名片信息备份表
	public $bak_table 	= 'card_info_bak';
	//名片分组表
	public $group_table = 'card_group';
	//名片关系映射表
	public $map_table 	= 'card_map';
	//拍名片队列表
	public $img_table 	= 'card_img';
	//名片应用配置表
	public $app_table 	= 'sc_com_app';
	//公司信息表
	public $company_table = 'sc_company';
	//用户信息表
	public $user_table = 'sc_user';

	//字段对应中文名称映射
	public $name_map = array(
		'cn_name'	=> '姓名',
		'en_name'	=> '英文名',
		'pic_url'	=> '头像',
		'mobile'	=> '手机号码',
		'call'		=> '工作电话',
		'email'		=> '电子邮箱',
		'qq'		=> 'QQ号',
		'wx'		=> '微信号',
		'fax'		=> '传真',
		'com_name'	=> '公司名称',
		'com_job'	=> '公司职务',
		'com_addr'	=> '公司地址',
	);

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			$this -> com_id  = $_SESSION[SESSION_VISIT_COM_ID];
			$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$this -> wx_id 	 = $_SESSION[SESSION_VISIT_USER_WXID];
		}
	}

	/**
	 * 添加名片备份
	 *
	 * @access public
	 * @param array $info_data 名片信息数据集合
	 * @param array $map_data 名片归属映射信息数据集合
	 * @return boolean
	 */
	public function add_card(array $info_data, array $map_data) {
		$time = time();

		$info_data['com_id']  	= $this -> com_id;
		$info_data['user_id']  	= $this -> user_id;
		$info_data['is_copy'] 	= 1;
		$info_data['update_time'] = $time;
		$info_data['create_time'] = $time;

		$map_data['com_id']  	= $this -> com_id;
		$map_data['user_id']  	= $this -> user_id;
		$map_data['update_time'] = $time;
		$map_data['create_time'] = $time;

		g('db') -> begin_trans();
		$result = g('ndb') -> insert($this -> info_table, $info_data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据写入异常!');
		}
		$map_data['card_id'] = $result;
		$result = g('ndb') -> insert($this -> map_table, $map_data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据写入异常!');
		}
		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 更新名片信息
	 *
	 * @access public
	 * @param integer $card_id 名片信息id
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function update_card($card_id, array $data, $group_id = 0) {
		$data['update_time'] = time();
		$condition = array(
			'com_id=' 	=> $this -> com_id,
			'user_id=' 	=> $this -> user_id,
			'card_id=' 	=> $card_id,
			'is_copy=' 	=> 1,
		);

		g('db') -> begin_trans();
		$result = g('ndb') -> update_by_condition($this -> info_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据更新异常!');
		}

		$data = array(
			'group_id' => $group_id,
		);
		$condition = array(
			'com_id=' 	=> $this -> com_id,
			'user_id=' 	=> $this -> user_id,
			'card_id=' 	=> $card_id,
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据更新异常!');
		}

		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 删除一张名片
	 *
	 * @access public
	 * @param integer $card_id 名片信息id
	 * @return boolean
	 */
	public function delete_card($card_id) {
		$info_con = array(
			'com_id='  => $this -> com_id,
			'user_id=' => $this -> user_id,
			'card_id=' => $card_id,
			'is_copy=' => 1,
		);

		$map_con = array(
			'com_id='  => $this -> com_id,
			'user_id=' => $this -> user_id,
			'card_id=' => $card_id,
		);

		g('db') -> begin_trans();
		$result = g('db') -> delete($this -> info_table, $info_con);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据删除异常!');
		}

		$result = g('db') -> delete($this -> map_table, $map_con);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据删除异常!');
		}

		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 添加一个名片分组
	 *
	 * @access public
	 * @param string $name 分组姓名
	 * @param array $member 成员集合
	 * @return boolean
	 */
	public function add_group($name, array $member = array()) {
		$time = time();
		$data = array(
			'com_id' 	=> $this -> com_id,
			'user_id' 	=> $this -> user_id,
			'name' 		=> $name,
			'update_time' => $time,
			'create_time' => $time,
		);

		g('db') -> begin_trans();
		$result = g('ndb') -> insert($this -> group_table, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据写入异常!');
		}
		if (empty($member)) {
			g('db') -> commit();
			return TRUE;
		}

		$data = array(
			'group_id' 		=> $result,
			'update_time' 	=> $time,
		);
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'card_id IN ' 	=> '(' . implode(',', $member) . ')',
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据写入异常!');
		}
		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 更新一个名片分组
	 *
	 * @access public
	 * @param string $group_id 分组id
	 * @param string $name 分组姓名
	 * @param array $member 成员集合
	 * @return boolean
	 */
	public function update_group($group_id, $name, array $member = array()) {
		$time = time();
		$data = array(
			'name' => $name,
			'update_time' => $time,
		);
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'group_id=' 	=> $group_id,
		);

		g('db') -> begin_trans();
		$result = g('ndb') -> update_by_condition($this -> group_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据写入异常!');
		}

		$data = array(
			'group_id' => 0,
			'update_time' => $time,
		);
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'group_id=' 	=> $group_id,
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据更新异常!');
		}

		if (empty($member)) {
			g('db') -> commit();
			return TRUE;
		}

		$data = array(
			'group_id' => $group_id,
		);
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'card_id IN ' 	=> '(' . implode(',', $member) . ')',
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据更新异常!');
		}
		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 删除一个分组
	 *
	 * @access public
	 * @param integer $group_id 分组id
	 * @return boolean
	 */
	public function delete_group($group_id) {
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'group_id='		=> $group_id,
		);

		g('db') -> begin_trans();

		$result = g('db') -> delete($this -> group_table, $condition);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据删除异常!');
		}

		$data = array(
			'group_id' => 0,
			'update_time' => time(),
		);
		$condition = array(
			'com_id=' 		=> $this -> com_id,
			'user_id=' 		=> $this -> user_id,
			'group_id='		=> $group_id,
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据删除异常!');
		}
		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 初始化自身的名片
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function card_init(array $data) {
		$time = time();

		$data['com_id'] 	 = $this -> com_id;
		$data['user_id'] 	 = $this -> user_id;
		$data['self_id'] 	 = $this -> user_id;
		$data['wx_id'] 		 = $this -> wx_id;
		$data['wx_hash'] 	 = md5($this -> wx_id);
		$data['update_time'] = $time;
		$data['create_time'] = $time;

		$result = g('ndb') -> insert($this -> info_table, $data);
		if (!$result) {
			throw new SCException('数据删除异常!');
		}
		return TRUE;
	}

	/**
	 * 更新自身的名片
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function update_self(array $data) {
		g('db') -> begin_trans();

		//先备份历史记录，再更新
		$sql = <<<EOF
SELECT * 
FROM {$this -> info_table} 
WHERE com_id={$this -> com_id} AND self_id={$this -> user_id} 
FOR UPDATE 
EOF;
		$result = g('db') -> select_one($sql);		
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据锁定异常!');
		}

		unset($result['card_id']);
		unset($result['extra']);
		unset($result['mod_key']);
		unset($result['is_share']);
		unset($result['is_expiry']);
		unset($result['is_copy']);
		unset($result['update_time']);

		$time = time();
		$result['create_time'] = $time;

		$bak_data = $result;
		$result = g('ndb') -> insert($this -> bak_table, $bak_data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据备份异常!');
		}

		$data['update_time'] = $time;
		$condition = array(
			'com_id='  => $this -> com_id,
			'user_id=' => $this -> user_id,
			'self_id=' => $this -> user_id,
		);

		$result = g('ndb') -> update_by_condition($this -> info_table, $condition, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('数据更新异常!');
		}

		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 企业内部分享自己的名片
	 *
	 * @access public
	 * @return boolean
	 */
	public function share_inside() {
		$data = array(
			'is_share' => 1,
			'update_time' => time(),
		);
		$condition = array(
			'com_id='  => $this -> com_id,
			'user_id=' => $this -> user_id,
			'self_id=' => $this -> user_id,
		);

		$result = g('ndb') -> update_by_condition($this -> info_table, $condition, $data);
		if (!$result) {
			throw new SCException('数据更新异常!');
		}

		return TRUE;
	}

	/**
	 * 增加映射关系，用作收藏推送名片
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function add_map(array $data) {
		$time = time();
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data['update_time'] = $time;
		$data['create_time'] = $time;

		$condition = array(
			'card_id=' => $data['card_id'],
			'user_id=' => $data['user_id'],
		);
		$check = g('ndb') -> record_exists($this -> map_table, $condition);
		if ($check) {
			throw new SCException('已收藏!');
		}

		$result = g('ndb') -> insert($this -> map_table, $data);
		if (!$result) {			
			throw new SCException('系统繁忙!');
		}

		return TRUE;
	}

	/**
	 * 更新一条映射记录
	 *
	 * @access public
	 * @param integer $card_id 名片信息id
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function update_map($card_id, array $data) {
		$data['update_time'] = time();
		$condition = array(
			'com_id=' 	=> $this -> com_id,
			'user_id=' 	=> $this -> user_id,
			'card_id=' 	=> $card_id,
		);
		$result = g('ndb') -> update_by_condition($this -> map_table, $condition, $data);
		if (!$result) {			
			throw new SCException('数据更新异常!');
		}
		return TRUE;
	}

	/**
	 * 删除一条映射记录
	 *
	 * @access public
	 * @param integer $card_id 名片信息id
	 * @return boolean
	 */
	public function delete_map($card_id) {
		$condition = array(
			'com_id=' 	=> $this -> com_id,
			'user_id=' 	=> $this -> user_id,
			'card_id=' 	=> $card_id,
		);
		$result = g('db') -> delete($this -> map_table, $condition);
		if (!$result) {
			throw new SCException('数据删除异常!');
		}
		return TRUE;
	}

	/**
	 * 获取个人名片信息，非备份名片
	 *
	 * @access public
	 * @param integer $card_id 名片id
	 * @return array
	 */
	public function get_card($card_id) {
		$table = "{$this -> info_table} AS a, {$this -> conf_table} AS b, {$this -> user_table} AS user";
		$fields  = array(
			'a.cn_name', 'a.en_name', 'a.mobile',
			'a.call', 'a.email', 'a.qq',
			'a.wx', 'a.fax', 'a.com_name',
			'a.com_job', 'a.com_addr', 'a.extra',
			'a.mod_key', 'a.is_share',
			'b.p_cname', 'b.p_ename', 'b.p_extra',
			'b.r_mobile', 'b.r_call', 'b.r_qq',
			'b.r_wx', 'b.r_email', 'b.r_fax',
			'b.r_extra', 'b.c_com_name', 'b.c_com_job',
			'b.c_com_addr', 'b.c_com_logo', 'b.c_logo_val',
			'b.c_extra', 'b.n_extra', 'b.mod_list',
			'user.pic_url', 
		);
		$fields = implode(',', $fields);

		$condition = array(
			'a.card_id=' => $card_id,
			'a.is_copy=' => 0,
			'^a.com_id=' => 'b.com_id',
			'^a.self_id=' => 'user.id',
			'user.state=' => 1,
		);

		$result = g('ndb') -> select($table, $fields, $condition);
		if (!$result) {
			throw new SCException('查询失败!');
		}
		$result = $result[0];

		$conf = array(
			'p_cname' 		=> $result['p_cname'],
			'p_ename'		=> $result['p_ename'],
			'p_extra'		=> $result['p_extra'],
			'r_mobile'		=> $result['r_mobile'],
			'r_call'		=> $result['r_call'],
			'r_qq'			=> $result['r_qq'],
			'r_wx'			=> $result['r_wx'],
			'r_email'		=> $result['r_email'],
			'r_fax'			=> $result['r_fax'],
			'r_extra'		=> $result['r_extra'],
			'c_com_name'	=> $result['c_com_name'],
			'c_com_job'		=> $result['c_com_job'],
			'c_com_addr'	=> $result['c_com_addr'],
			'c_com_logo'	=> $result['c_com_logo'],
			'c_logo_val'	=> $result['c_logo_val'],
			'c_extra'		=> $result['c_extra'],
			'n_extra'		=> $result['n_extra'],
		);

		$info = array(
			'cn_name'	=> $result['cn_name'],
			'en_name'	=> $result['en_name'],
			'mobile'	=> $result['mobile'],
			'call'		=> $result['call'],
			'email'		=> $result['email'],
			'qq'		=> $result['qq'],
			'wx'		=> $result['wx'],
			'fax'		=> $result['fax'],
			'com_name'	=> $result['com_name'],
			'com_job'	=> $result['com_job'],
			'com_addr'	=> $result['com_addr'],
			'extra'		=> $result['extra'],
			'mod_key' 	=> $result['mod_key'],
			'pic_url' 	=> $result['pic_url'],
		);

		$conf['p_extra'] = json_decode($conf['p_extra'], TRUE);
		$conf['r_extra'] = json_decode($conf['r_extra'], TRUE);
		$conf['c_extra'] = json_decode($conf['c_extra'], TRUE);
		$conf['n_extra'] = json_decode($conf['n_extra'], TRUE);

		!$conf['p_extra'] && $conf['p_extra'] = array();
		!$conf['r_extra'] && $conf['r_extra'] = array();
		!$conf['c_extra'] && $conf['c_extra'] = array();
		!$conf['n_extra'] && $conf['n_extra'] = array();

		$conf_extra = array_merge($conf['p_extra'], $conf['r_extra']);
		$conf_extra = array_merge($conf_extra, $conf['c_extra']);
		$conf_extra = array_merge($conf_extra, $conf['n_extra']);

		$info['extra'] = json_decode($info['extra'], TRUE);

		empty($info['extra']) && $info['extra'] = array();

		$data = array(
			'personal'	=> array(),
			'contact'	=> array(),
			'company'	=> array(),
			'other'		=> array(),
			'mod_key'	=> $info['mod_key'],
			'is_share'	=> $result['is_share'],
			'mod_list'  => json_decode($result['mod_list'], TRUE),
			'logo_val'	=> $conf['c_com_logo'] == 1 ? MEDIA_URL_PREFFIX.$conf['c_logo_val'] : '',
		);

		$personal = &$data['personal'];
		$contact  = &$data['contact'];
		$company  = &$data['company'];
		$other    = &$data['other'];

		$map = $this -> name_map;
		//个人信息
		$conf['p_cname'] == 1 && $personal['cn_name'] = array('key' => 'cn_name', 'name' => $map['cn_name'], 'val' => $info['cn_name']);
		$conf['p_ename'] == 1 && $personal['en_name'] = array('key' => 'en_name', 'name' => $map['en_name'], 'val' => $info['en_name']);
		$personal['pic_url'] = array('key' => 'pic_url', 'name' => $map['pic_url'], 'val' => $info['pic_url']);
		
		//联系信息
		$conf['r_mobile'] == 1 && $contact['mobile'] = array('key' => 'mobile', 'name' => $map['mobile'], 'val' => $info['mobile']);
		$conf['r_call']   == 1 && $contact['call'] = array('key' => 'call', 'name' => $map['call'], 'val' => $info['call']);
		$conf['r_qq']     == 1 && $contact['qq'] = array('key' => 'qq', 'name' => $map['qq'], 'val' => $info['qq']);
		$conf['r_wx']     == 1 && $contact['wx'] = array('key' => 'wx', 'name' => $map['wx'], 'val' => $info['wx']);
		$conf['r_email']  == 1 && $contact['email'] = array('key' => 'email', 'name' => $map['email'], 'val' => $info['email']);
		$conf['r_fax']    == 1 && $contact['fax'] = array('key' => 'fax', 'name' => $map['fax'], 'val' => $info['fax']);
		//公司信息
		$conf['c_com_name'] == 1 && $company['com_name'] = array('key' => 'com_name', 'name' => $map['com_name'], 'val' => $info['com_name']);
		$conf['c_com_job']  == 1 && $company['com_job'] = array('key' => 'com_job', 'name' => $map['com_job'], 'val' => $info['com_job']);
		$conf['c_com_addr'] == 1 && $company['com_addr'] = array('key' => 'com_addr', 'name' => $map['com_addr'], 'val' => $info['com_addr']);

		if (!empty($conf_extra)) foreach ($conf_extra as $key => $val) {
			if (!isset($info['extra'][$key]) || empty($info['extra'][$key])) continue;

			$index = '';
			if (preg_match('/^p_/', $key)) {
				$index = 'personal';
			}elseif (preg_match('/^r_/', $key)) {
				$index = 'contact';
			}elseif (preg_match('/^c_/', $key)) {
				$index = 'company';
			}else {
				$index = 'other';
			}
			$data[$index][$key] = array('key' => $key, 'name' => $val, 'val' => $info['extra'][$key]);
		}
		unset($val);

		return $data;
	}

	/**
	 * 获取一张备份名片信息
	 *
	 * @access public
	 * @param integer $card_id 名片id
	 * @return boolean
	 */
	public function get_card_bak($card_id) {
		$fields  = array(
			'cn_name', 'en_name', 'mobile',
			'`call`', 'email', 'qq',
			'wx', 'fax', 'com_name',
			'com_job', 'com_addr', 
		);
		$fields = implode(',', $fields);

		$condition = array(
			'card_id=' => $card_id,
		);

		$result = g('ndb') -> select($this -> info_table, $fields, $condition);
		if (!$result) {
			throw new SCException('查询失败!');
		}
		$result = $result[0];

		$info = array(
			'cn_name'	=> $result['cn_name'],
			'en_name'	=> $result['en_name'],
			'mobile'	=> $result['mobile'],
			'call'		=> $result['call'],
			'email'		=> $result['email'],
			'qq'		=> $result['qq'],
			'wx'		=> $result['wx'],
			'fax'		=> $result['fax'],
			'com_name'	=> $result['com_name'],
			'com_job'	=> $result['com_job'],
			'com_addr'	=> $result['com_addr'],
		);
		unset($result);

		$data = array(
			'personal'	=> array(),
			'contact'	=> array(),
			'company'	=> array(),
		);
		$personal = &$data['personal'];
		$contact  = &$data['contact'];
		$company  = &$data['company'];

		$map = $this -> name_map;
		//个人信息
		$personal['cn_name'] = array('key' => 'cn_name', 'name' => $map['cn_name'], 'val' => $info['cn_name']);
		$personal['en_name'] = array('key' => 'en_name', 'name' => $map['en_name'], 'val' => $info['en_name']);
		//联系信息
		$contact['mobile'] = array('key' => 'mobile', 'name' => $map['mobile'], 'val' => $info['mobile']);
		$contact['call'] = array('key' => 'call', 'name' => $map['call'], 'val' => $info['call']);
		$contact['qq'] = array('key' => 'qq', 'name' => $map['qq'], 'val' => $info['qq']);
		$contact['wx'] = array('key' => 'wx', 'name' => $map['wx'], 'val' => $info['wx']);
		$contact['email'] = array('key' => 'email', 'name' => $map['email'], 'val' => $info['email']);
		$contact['fax'] = array('key' => 'fax', 'name' => $map['fax'], 'val' => $info['fax']);
		//公司信息
		$company['com_name'] = array('key' => 'com_name', 'name' => $map['com_name'], 'val' => $info['com_name']);
		$company['com_job'] = array('key' => 'com_job', 'name' => $map['com_job'], 'val' => $info['com_job']);
		$company['com_addr'] = array('key' => 'com_addr', 'name' => $map['com_addr'], 'val' => $info['com_addr']);

		return $data;
	}

	/**
	 * 获取判断状态相关的信息
	 *
	 * @access public
	 * @param integer $card_id 名片id
	 * @return mixed
	 */
	public function get_check($card_id) {
		$fields = array('is_copy', 'user_id', 'self_id');
		$fields =implode(',', $fields);
		$condition = array(
			'card_id=' => $card_id,
		);
		$check = g('ndb') -> select($this -> info_table, $fields, $condition);
		return $check ? $check[0] : FALSE;
	}

	/**
	 * 对于非备份名片，检测当前用户是否拥有这张名片
	 *
	 * @access public
	 * @param integer $card_id 名片id
	 * @return boolean
	 */
	public function check_own($card_id) {
		$condition = array(
			'card_id=' => $card_id,
			'user_id=' => $this -> user_id,
			'com_id='  => $this -> com_id,
		);
		$check = g('ndb') -> record_exists($this -> map_table, $condition);
		return $check;
	}

	/**
	 * 获取名片相关的映射信息
	 *
	 * @access public
	 * @param integer $card_id 名片id
	 * @return mixed
	 */
	public function get_map_info($card_id) {
		$fields = array('group_id, mark');
		$fields =implode(',', $fields);
		$condition = array(
			'card_id=' => $card_id,
			'user_id=' => $this -> user_id,
		);
		$result = g('ndb') -> select($this -> map_table, $fields, $condition);
		return $result ? $result[0] : FALSE;
	}

	/**
	 * 获取分组形式列表
	 *
	 * @access public 
	 * @return array
	 */
	public function get_group_list() {
		$table = "{$this -> map_table} AS a, {$this -> info_table} AS b " .
					"LEFT JOIN {$this -> user_table} AS user ON b.self_id=user.id";
		$fields = array(
			'a.group_id', 'b.card_id', 'b.cn_name',
			'b.com_name', 'user.pic_url',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'a.com_id=' => $this -> com_id,
			'a.user_id=' => $this -> user_id,
			'^a.card_id=' => 'b.card_id',
		);
		$order = ' ORDER BY a.group_id, b.sort_py';
		$result = g('ndb') -> select($table, $fields, $condition, 0, 0, '', $order);
		return $result ? $result : array();
	}

	/**
	 * 获取用户自定义的全部分组
	 *
	 * @access public
	 * @return array
	 */
	public function get_groups() {
		$fields = array(
			'group_id',
			'name',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'user_id=' => $this -> user_id,
		);
		$order = ' ORDER BY group_id ';
		$result = g('ndb') -> select($this -> group_table, $fields, $condition, 0, 0, '', $order);
		return $result ? $result : array();
	}

	/**
	 * 获取联系人形式列表
	 *
	 * @access public
	 * @return array
	 */
	public function get_contact_list() {
		$table = "{$this -> map_table} AS a, {$this -> info_table} AS b " . 
					"LEFT JOIN {$this -> user_table} AS user ON b.self_id=user.id and user.state=1";
		$fields = array(
			'b.card_id', 'b.cn_name', 'b.com_name',
			'b.first_py', 'b.sort_py',
			'user.pic_url',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'a.com_id='  => $this -> com_id,
			'a.user_id='  => $this -> user_id,
			'^a.card_id=' => 'b.card_id',
		);
		$order = ' ORDER BY b.sort_py';
		$result = g('ndb') -> select($table, $fields, $condition, 0, 0, '', $order);
		return $result ? $result : array();
	}

	/**
	 * 获取公司内部分享列表
	 *
	 * @access public
	 * @param integer
	 * @return string
	 */
	public function get_share_list($com_id = 0) {
		$table = $this -> info_table . ' AS info, ' . 
					$this -> user_table . ' AS user';
		$fields = array(
			'info.card_id', 'info.cn_name', 'info.com_name',
			'info.sort_py',
			'user.pic_url',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'info.com_id=' => $this -> com_id,
			'info.is_share=' => 1,
			'info.is_expiry=' => 0,
			'info.user_id!=' => $this -> user_id,
			'^info.self_id=' => 'user.id',
			'user.state=' => 1,
		);
		$order = ' ORDER BY info.sort_py ';
		$result = g('ndb') -> select($table, $fields, $condition, 0, 0, '', $order);
		return $result ? $result : array();
	}

	/**
	 * 获取公司信息
	 *
	 * @access public
	 * @return array
	 */
	public function get_company() {
		$fields = array(
			'id',
			'name',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'id=' => $this -> com_id,
		);
		$result = g('ndb') -> select($this -> company_table, $fields, $condition);
		return $result ? $result[0] : array();
	}

	/**
	 * 获取自身名片的一些基础信息
	 *
	 * @access public
	 * @return mixed
	 */
	public function get_self() {
		$fields = array(
			'card_id',
			'mod_key',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'com_id=' => $this -> com_id,
			'user_id=' => $this -> user_id,
			'self_id=' => $this -> user_id,
		);

		$result = g('ndb') -> select($this -> info_table, $fields, $condition);
		return is_array($result) ? $result[0] : FALSE;
	}

	/**
	 * 获取自身名片的详细基础信息
	 *
	 * @access public
	 * @return mixed
	 */
	public function get_self_detail() {
		$fields = array(
			'cn_name', 'en_name', 'mobile',
			'`call`', 'email', 'qq',
			'wx', 'fax', 'com_name',
			'com_job', 'com_addr', 'extra',
			'mod_key',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'com_id=' => $this -> com_id,
			'user_id=' => $this -> user_id,
			'self_id=' => $this -> user_id,
		);

		$result = g('ndb') -> select($this -> info_table, $fields, $condition);
		if (!is_array($result)) {
			throw new SCException('读取失败!');
		}
		
		$result = $result[0];
		$result['extra'] = json_decode($result['extra'], TRUE);
		!is_array($result['extra']) && $result['extra'] = array();

		return $result;
	}

	/**
	 * 获取公司的名片配置
	 *
	 * @access public
	 * @return mixed
	 */
	public function get_conf() {
		$table = $this -> conf_table;
		$fields  = array(
			'p_cname', 'p_ename', 'p_extra',
			'r_mobile', 'r_call', 'r_qq',
			'r_wx', 'r_email', 'r_fax',
			'r_extra', 'c_com_name', 'c_com_job',
			'c_com_addr', 'c_com_logo', 'c_logo_val',
			'c_extra', 'n_extra', 'mod_list', 
		);
		$fields = implode(',', $fields);

		$condition = array(
			'com_id=' => $this -> com_id,
		);

		$result = g('ndb') -> select($table, $fields, $condition);
		if (!$result) {
			throw new SCException('查询失败!');
		}
		$conf = $result[0];

		$conf['p_extra'] = json_decode($conf['p_extra'], TRUE);
		$conf['r_extra'] = json_decode($conf['r_extra'], TRUE);
		$conf['c_extra'] = json_decode($conf['c_extra'], TRUE);
		$conf['n_extra'] = json_decode($conf['n_extra'], TRUE);

		!$conf['p_extra'] && $conf['p_extra'] = array();
		!$conf['r_extra'] && $conf['r_extra'] = array();
		!$conf['c_extra'] && $conf['c_extra'] = array();
		!$conf['n_extra'] && $conf['n_extra'] = array();

		$conf_extra = array_merge($conf['p_extra'], $conf['r_extra']);
		$conf_extra = array_merge($conf_extra, $conf['c_extra']);
		$conf_extra = array_merge($conf_extra, $conf['n_extra']);

		$data = array(
			'personal'	=> array(),
			'contact'	=> array(),
			'company'	=> array(),
			'other'		=> array(),
			'logo_url'  => '',
			'mod_list'	=> json_decode($conf['mod_list'], TRUE),
		);
		$conf['c_com_logo'] == 1 && !empty($conf['c_logo_val']) && $data['logo_url'] = MEDIA_URL_PREFFIX.$conf['c_logo_val'];

		$personal = &$data['personal'];
		$contact  = &$data['contact'];
		$company  = &$data['company'];
		$other    = &$data['other'];
		
		$user_fields = 'name,position,mobile,email,tel,weixin_id';
		$com_fields = 'name,addr';
		
		$company_info = g('company') -> get_by_id($_SESSION[SESSION_VISIT_COM_ID], $com_fields);
		$user = g('user')-> get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID], $_SESSION[SESSION_VISIT_USER_ID], $user_fields);
		
		$map = $this -> name_map;
		//个人信息
		$conf['p_cname'] == 1 && $personal['cn_name'] = array('key' => 'cn_name', 'name' => $map['cn_name'],'val'=>$user['name']);
		$conf['p_ename'] == 1 && $personal['en_name'] = array('key' => 'en_name', 'name' => $map['en_name']);
		//联系信息
		$conf['r_mobile'] == 1 && $contact['mobile'] = array('key' => 'mobile', 'name' => $map['mobile'],'val'=>$user['mobile']);
		$conf['r_call']   == 1 && $contact['call'] = array('key' => 'call', 'name' => $map['call'],'val'=>$user['tel']);
		$conf['r_qq']     == 1 && $contact['qq'] = array('key' => 'qq', 'name' => $map['qq']);
		$conf['r_wx']     == 1 && $contact['wx'] = array('key' => 'wx', 'name' => $map['wx'],'val'=>$user['weixin_id']);
		$conf['r_email']  == 1 && $contact['email'] = array('key' => 'email', 'name' => $map['email'],'val'=>$user['email']);
		$conf['r_fax']    == 1 && $contact['fax'] = array('key' => 'fax', 'name' => $map['fax']);
		//公司信息
		$conf['c_com_name'] == 1 && $company['com_name'] = array('key' => 'com_name', 'name' => $map['com_name'],'val'=>$company_info['name']);
		$conf['c_com_job']  == 1 && $company['com_job'] = array('key' => 'com_job', 'name' => $map['com_job'],'val'=>$user['position']);
		$conf['c_com_addr'] == 1 && $company['com_addr'] = array('key' => 'com_addr', 'name' => $map['com_addr'],'val'=>$company_info['addr']);

		if (!empty($conf_extra)) foreach ($conf_extra as $key => $val) {
			$index = '';
			$extra = '';
			if (preg_match('/^p_/', $key)) {
				$index = 'personal';
				$extra = 'p_extra';
			}elseif (preg_match('/^r_/', $key)) {
				$index = 'contact';
				$extra = 'r_extra';
			}elseif (preg_match('/^c_/', $key)) {
				$index = 'company';
				$extra = 'c_extra';
			}else {
				$index = 'other';
				$extra = 'n_extra';
			}
			$data[$index][] = array('key' => $key, 'name' => $val, 'extra' => $extra);
		}
		unset($val);
		return $data;
	}
}