<?php
/**
 * 意见反馈数据模型
 *
 * @author LiangJianMing
 * @create 2015-12-31
 */

class cls_advice_trend {
	// 公司id
	private $com_id = 0;
	// 基础查询条件
	private $base_con = array();

	// 反馈信息表
	private static $info_table = 'advice_info';
	// 反馈回复表
	private static $reply_table = 'advice_reply';
	// 反馈分类表
	private static $class_table = 'advice_class';
	// 反馈主题浏览状态表
	private static $state_table = 'advice_visit_state';
	// 反馈动态表
	private static $trend_table = 'advice_trend';
	// 反馈附件表
	private static $file_table = 'advice_file';

	// 用户信息表
	private static $user_table = 'sc_user';
	// 部门信息表
	private static $dept_table = 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;

		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
	}


	/**
	 * 增加一条动态
	 **/
	public function add_trend($advice_id,$user_id,$user_pic,$user_name,$msg){
		 $data = array(
	            'desc' => $msg,
	            'create_time' => time(),
	            'user_id' => $user_id,
	            'user_pic' => $user_pic,
	            'user_name' => $user_name,
	            'adi_id' => $advice_id,
	        );
		$ret = g('ndb') -> insert(self::$trend_table,$data);
		return $ret;
	}

	/**
	 * 更新动态数据（撤回重新发送时用到）
	 * @param   $advice_id
	 * @param   $user_id
	 * @param   $data
	 * @return 
	 */
	public function update_trend($advice_id,$user_id,$data){
		$cond = array(
				'adi_id=' => $advice_id,
				'user_id=' => $user_id,
			);
		$ret = g('ndb') -> update_by_condition(self::$trend_table,$cond,$data,false);
		return $ret;
	}

	/**
	 * 获取指定反馈的动态信息
	 * @param   $advice_id
	 * @return 
	 */
	public function get_trend($advice_id,$fields='*',$page=1,$page_size=20){
		$cond = array(
				'adi_id=' => $advice_id,
			);
		$ret = g('ndb') -> select(self::$trend_table,$fields,$cond,$page,$page_size,'','order by create_time desc',true);
		return $ret;
	}

}

/* End of this file */