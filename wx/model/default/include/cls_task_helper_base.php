<?php
/**
 * 任务评论信息
 * @author lijuns
 * @date 2014-12-05
 */
class cls_task_helper_base extends cls_task_base{
	
	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($helper_info) {
		$is_role = parent::get_user_perform($helper_info['taskid'], $helper_info['apply_id']);
		if($is_role==true){
			$data = array(
				'taskid' => $helper_info['taskid'],
				'apply_id' => $helper_info['apply_id'],
				'apply_name' => $helper_info['apply_name'],
				'apply_content' => $helper_info['apply_content'],
				'apply_time' => time(),
				'helper_id' => $helper_info['helper_id'],
				'helper_name' => $helper_info['helper_name'],
			);
			$ret = g('ndb') -> insert(parent::$TableHelper, $data);
			if (!$ret) {
				throw new SCException('保存协办信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限发起协办');
		}
	}
	
	/**
	 * 更新审批信息
	 */
	public function update($helper_info){
		$is_role = parent::get_user_helper($helper_info['taskid'], $helper_info['helper_id']);
		if($is_role){
			$data = array(
				'is_agree' => $helper_info['is_agree'],
				'helper_content' => $helper_info['helper_content'],
				'helper_time' => time()
			);
			$cond = array(
				'id='=>$helper_info["id"],
			); 
			$ret = g('ndb') -> update_by_condition(parent::$TableHelper, $cond, $data);
			if (!$ret) {
				throw new SCException('确认协办信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限确认协办');
		}
	}
	
	
	/**
	 * 查询协办人信息（根据performid,taskid）
	 */
	public function select_helper_performid($performid,$taskid){
		$sql = 'SELECT * FROM '.parent::$TableHelper.' where apply_id='.$performid." and taskid='".$taskid."' and is_agree=".parent::$AgreeOn;
		return g('db')->select($sql);
	}
	
	/**
	 * 检查协办人是否可以被申请协办（根据helperid,taskid）
	 */
	public function check_helper($helperid,$taskid){
		$sql = 'SELECT * FROM '.parent::$TableHelper.' where helper_id='.$helperid." and taskid='".$taskid."' and ( is_agree = ".parent::$IsAgreeOff." or is_agree = ".parent::$AgreeOn." )";
		return g('db')->select($sql) ? false : true;
	}
	
	/**
	 * 协办列表
	 */
	public function select_helper_is_agree_off($userid,$isagree,$isfinish){
		$sql = "select * from ".parent::$Table." task,".parent::$TableHelper." helper ".
		"where task.id = helper.taskid and helper_id=".$userid." and is_agree=".$isagree." and isfinish=".$isfinish;
		return g('db')->select($sql);
	}
	
	
	
	/**
	 * 查看协办记录
	 */
	public function select_helper_taskid($taskid,$userid){
		$sql = 'SELECT th.*,su.pic_url help_pic_url FROM '.parent::$TableHelper.' th LEFT JOIN sc_user su on helper_id = su.id where apply_id='.$userid." and taskid='".$taskid."' order by apply_time desc";
		return g('db')->select($sql);
	}
	
	/**
	 * 根据协办信息id查找协办信息
	 */
	public function select_helper_id($id,$userid,$fields="*"){
		$cond = array(
			'id=' => $id,
			'helper_id=' => $userid,
		);
		return g('ndb') -> select(parent::$TableHelper, $fields, $cond, 0, 0, '', ' order by id desc ');
	}
	
	
}

// end of file