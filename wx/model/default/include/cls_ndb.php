<?php
/** 
 * 通用数据库操作类
 * 
 * @author LiangJianMing
 * @create 2014-08-11 
 * @version 1.0.0
 */
class cls_ndb {
	/** 
	 * 获取全字段名称
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param boolean $with_info 是否取详细信息
	 * @return mixed 成功返回array，失败返回FALSE
	 */
	public function get_columns($table, $with_info = FALSE) {
		if ($with_info) {
			$sql = 'SHOW FULL COLUMNS FROM ' . $table;
		}else {
			$sql = 'SHOW COLUMNS FROM ' . $table;
		}
		$result = g('db')->select($sql);
		
		$ret = array();
		if ($result) {
			if (!$with_info) {
				foreach ($result as $val) {
					$ret[] = $val['Field'];
				}
			}else {
				$ret = $result;
			}
			return $ret;
		}
		return FALSE;
	}
	
	/** 
	 * 单条记录入库
	 * 
	 * @access public
	 * @param string $table 数据表名称
	 * @param array $data 数据集合数组
	 * @param boolean $use_filter 是否使用参数过滤
	 * @param boolean $char_filt 是否转义特殊字符
	 * @return mixed 写入且成功返回integer,否则返回FALSE
	 */
	public function insert($table, $data, $use_filter = TRUE, $char_filt = TRUE) {
	
		if ($use_filter) {
			$info = $this->filter_data($table, $data, $char_filt);
			
		}else {
			$info = $data;
		}
	  
		if (!$info) {
			return FALSE;
		}
		$result = g('db') -> insert($table, $info);
		
		return $result ? $result : FALSE;
	}
	
	/** 
	 * 批量写入数据库
	 * 
	 * @access public
	 * @param string $table 数据库表名
	 * @param string $fields 写入的对应字段字符串
	 * @param array $data 需要批量写入的数据集合，子元素必须与$fields一一对应
	 * @return boolean
	 */
	public function batch_insert($table, $fields, $data) {
		$ret = TRUE;
		
		$sql = 'INSERT INTO ' . $table . '('. $fields .') VALUES';
		
		$values = '';
		$count = 0;
		foreach ($data as $dval) {
			if ($count == 100) {
				$temp_sql = $sql . substr($values, 0, -1);
				$result = g('db') -> query($temp_sql);
				!$result && $ret = FALSE;

				$values = '';
				$count = 0;
			}
			
			$temp = '(';
			foreach ($dval as $val) {
				$temp .= '\'' . mysql_escape_string($val) . '\',';
			}
			unset($val);
			$temp = substr($temp, 0, -1) . '),';
			$values .= $temp;
			$count++;
		}
		unset($dval);
		if (!empty($values)) {
			$temp_sql = $sql . substr($values, 0, -1);
			$result = g('db')->query($temp_sql);
			!$result && $ret = FALSE;
		}
		
		return $ret;
	}
	
	/** 
	 * 更新一条数据库记录
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param integer $id 主键id
	 * @param array $data 数据集合
	 * @param boolean $use_filter 是否使用参数过滤
	 * @param boolean $char_filt 是否转义特殊字符
	 * @return boolean
	 */
	public function update($table, $id, $data, $use_filter = TRUE, $char_filt = TRUE) {
		if ($use_filter) {
			$info = $this->filter_data($table, $data, $char_filt);
		}else {
			$info = $data;
		}
		
		if (!$info) {
			return FALSE;
		}
		
		$columns = $this->get_columns($table);
		if (!is_array($columns)) {
			return FALSE;
		}
		unset($info[$columns[0]]);
		
		$condition = array(
			$columns[0].'=' => $id,
		);		
		$result = g('db') -> update($table, $info, $condition);
		return $result !== FALSE ? TRUE : FALSE;
	}
	
	/** 
	 * 更新一条数据库记录
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $condition 条件数组
	 * @param array $data 数据集合
	 * @param boolean $use_filter 是否使用参数过滤
	 * @param boolean $char_filt 是否转义特殊字符
	 * @return boolean
	 */
	public function update_by_condition($table, $condition = array(), $data, $use_filter = TRUE, $char_filt = TRUE) {
		if (!is_array($condition)) {
			return FALSE;
		}
		if ($use_filter) {
			$info = $this->filter_data($table, $data, $char_filt);
		}else {
			$info = $data;
		}
			
		if (!$info) {
			return FALSE;
		}
		
		$columns = $this->get_columns($table);
		if (!is_array($columns)) {
			return FALSE;
		}
			
		unset($info[$columns[0]]);
		
		$result = g('db') -> update($table, $info, $condition);
			
		return $result !== FALSE ? TRUE : FALSE;
	}
	
	/** 
	 * 删除一条数据库记录
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param integer $id 主键id
	 * @return boolean
	 */
	public function delete_by_id($table, $id) {
		$columns = $this->get_columns($table);
		if (!is_array($columns)) {
			return FALSE;
		}
		$where = array($columns[0].'='=>$id);
		$result = g('db')->delete($table, $where);
		return $result;
	}
	
	/** 
	 * 根据条件删除数据库记录
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $condition 条件condition
	 * @return boolean
	 */
	public function delete($table, array $condition) {
		$where = $this -> compose_where($condition);
		$sql = 'DELETE FROM `' . $table . '`' . $where;
		$result = g('db') -> query($sql);
		return $result;
	}
	
	/** 
	 * 常用的查询类
	 * 
	 * @access public
	 * @param string $table 数据表名称
	 * @param string $fields 要查询的字段
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数	 
	 * @param string $groubBy 分组方式
	 * @param string $order_by 排序方式
	 * @param boolean $with_count 是否查询limit前的总量
	 * @return mixed
	 */
	public function select($table, $fields, $condition = array(), $page = 0, $page_size = 0, $group_by = '', $order_by = '', $with_count = FALSE) {
		$sql = 'SELECT ' . $fields . ' FROM ' . $table;
		
		$where = $this -> compose_where($condition);
		$sql .= $where . $group_by . $order_by;

		$limit_sql = $sql;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);

		if ($with_count) {
			$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') counta';
			$count = g('db')->select_one($sql);
			$ret = array(
				'data' => $ret ? $ret : array(),
				'count' => $count ? $count['count'] : 0,
			);
			$page_size != 0 and $page != 0 and $this -> get_page_info($page, $page_size, $count ? $count['count'] : 0);
		}
		return $ret ? $ret : FALSE;
	}
	
	/** 
	 * 根据id获取数据记录信息
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param mixed $ids integer或array
	 * @param string $id_name ids对应的哪个字段名
	 * @param string $fields 查询哪些字段
	 * @param string $order 指定排序方式
	 * @param string $key_by 以这个字段作为索引返回数组
	 * @param array $condition 其他条件
	 * @return mixed 查询成功返回array,否则返回FALSE
	 */
	public function get_data_by_ids($table, $ids, $id_name = '', $fields = '*', $order = '', $key_by = '', $condition = array()) {
		if (empty($id_name)) {
			$columns = $this->get_columns($table);
			if (!is_array($columns)) {
				return FALSE;
			}
			$id_name = $columns[0];
		}
		
		!is_array($ids) && $ids = array($ids);
		$ids_str = implode(',', $ids);

		$condition[$id_name . ' IN '] = '(' . $ids_str . ')';
		$where = $this -> compose_where($condition);
		
		$sql = 'SELECT ' . $fields . ' FROM ' . $table . $where . ' ' . $order;
		$result = g('db') -> select($sql);
		if (!empty($key_by) && $result) {
			$temp = array();
			foreach ($result as $val) {
				isset($val[$key_by]) && $temp[$val[$key_by]] = $val;
			}
			unset($val);
			!empty($temp) && $result = $temp;
		}
		return $result;
	}
	
	/** 
	 * 将输入数据过滤为合法的数据
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $data 数据集合
	 * @param boolean $char_filt 是否转义特殊字符
	 * @return mixed 无法转换返回FALSE，否则返回array
	 */
	public function filter_data($table, $data, $char_filt = TRUE) {
		$columns = $this->get_columns($table, TRUE);
		if (!is_array($columns)) {
			return FALSE;
		}
		
		$ret = array();
		foreach ($columns as $val) {
			if (isset($data[$val['Field']])) {
				$temp = $data[$val['Field']];
				$temp = trim(strval($temp));
				
				if (preg_match('/^[^ ]*int\(/', $val['Type'])) {
					if ($temp) {
						$temp = preg_replace('/[^-\d]+/', '', $temp);
					}else {
						//自增的值不能为0
						if (preg_match('/auto_increment/', $val['Extra']) && !$temp) {
							return FALSE;
						}
						$temp = intval($temp);
					}
				}else if (preg_match('/^[^ ]*char\(([\d]+)\)/', $val['Type'], $match)) {
					if ($temp) {
						$len = mb_strlen($temp);
						if ($len > $match[1]) {
							$temp = mb_substr($temp, 0, $match[1], 'UTF-8');
						}
					}
				}else if (preg_match('/^[^ ]*text/', $val['Type'])) {
				}else if (preg_match('/^datetime/', $val['Type'], $match)) {
					if (!preg_match('/^[\d]{4}-[\d]{2}-[\d]{2}( [\d]{2}:[\d]{2}:[\d]{2}|)$/', $temp)) {
						$temp = date('Y-m-d H:i:s');
					}
				}
				$ret['`' . $val['Field'] . '`'] = $temp;
			}
		}
		return $ret;
	}

	/**
	 * 根据数组条件组合成where字符串
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @return string
	 */
	public function compose_where(array $condition) {
		$where = ' WHERE 1';
		foreach ($condition as $key => $val) {
			if (preg_match('/ IN $/', $key)) {
				$where .= ' AND ' . $key . $val;
			}elseif (preg_match('/^[\^]{1}/', $key)) {
				//condition数组中，key在开头使用"^"号表示值不添加引号
				$key = preg_replace('/^[\^]/', '', $key);
				$where .= ' AND ' . $key . mysql_escape_string($val);
			//OR条件，以 '__OR' 开头作为数组元素索引的元素被视为OR条件，该元素必定是数组，方便多个OR或单个OR
			}elseif (preg_match('/^__OR/', $key)) {
				$or_preffix = ' AND (';
				$where .= $or_preffix;
				$tmp_or = '';
				foreach ($val as $ckey => $cval) {
					//or条件中还允许有and子条件
					if (is_array($cval)) {
						$tmp_where = $this -> compose_where($cval);
						$tmp_where = substr($tmp_where, 13);
						$tmp_or .= '(' . $tmp_where . ') OR ';
						continue;
					}
					//允许多个同样字段的or条件
					$tmp_ckey = preg_replace('/^__[\d]+__/', '', $ckey);
					if (preg_match('/ IN $/', $tmp_ckey)) {
						$tmp_or .= $tmp_ckey . $cval . ' OR ';
					}else if (preg_match('/ REGEXP $/', $tmp_ckey)) {
						$tmp_or .= $tmp_ckey . "'{$cval}' OR ";
					}else {
						$tmp_or .= $tmp_ckey . '\'' . mysql_escape_string($cval) . '\' OR ';
					}
				}
				unset($cval);
				!empty($tmp_or) and $tmp_or = substr($tmp_or, 0, -4);
				$where .= $tmp_or . ')';
			}else if (preg_match('/ REGEXP $/', $key)) {
				$where .= ' AND ' . $key . "'{$val}'";
			}else {
				$where .= ' AND ' . $key . '\'' . mysql_escape_string($val) . '\'';
			}
		}
		return $where;
	}
	
	/** 
	 * 验证某条数据库记录是否存在
	 * 
	 * @access public
	 * @param string $table 数据表名
	 * @param array $condition 条件集合，仅适用于全AND条件
	 * @return boolean 存在返回TRUE，否则返回FALSE
	 */
	public function record_exists($table, $condition = array()) {
		$sql = 'SELECT 1 FROM ' . $table;
		$where = $this -> compose_where($condition);
		$sql .= $where;
		$ret = g('db') -> select_one($sql);
		return $ret ? TRUE : FALSE;
	}
	

	/**
	 * 获取分页信息，并传递页码、当前页到smarty
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $total		总记录数
	 * @return 记录起始位置、页码
	 */
	private function get_page_info($page,$page_size, $total) {
		
		$page_count = ($total % $page_size == 0) ? ($total / $page_size) : ($total / $page_size + 1);	//总页数
		if (!$page || $page == NULL) {
			$page = 1;
		}
		$pages = array();	//页码，即1，2，3...
		for ($i=1; $i<=$page_count; $i++) {
			array_push($pages, $i);
		}
		g('smarty') -> assign('curr_page', $page);
		g('smarty') -> assign('pages', $pages);
	}
}