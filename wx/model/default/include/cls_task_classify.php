<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_classify extends cls_task_base{
	
	public function get_task_classify($comid){
		$sql = 'SELECT * FROM '.parent::$TableClassify.' where com_id='.$comid." and info_state=".parent::$StateOn;
		return g('db')->select($sql);
	}
	
	public function get_task_classify_name($classify_id){
		$sql = 'SELECT * FROM '.parent::$TableClassify.' where id='.$classify_id;
		return g('db')->select_one($sql);
	}
}

// end of file