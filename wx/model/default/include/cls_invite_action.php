<?php
/**
 * 邀请加入
 * 
 * @author gch
 * @date 2018-03-08 17:30:50
 *
 */
class cls_invite_action {

	/** 用户表 */
	private static $t_user = 'sc_user';
	/** 部门表 */
	private static $t_dept = 'sc_dept';
	/** 企业表 */
	private static $t_company = 'sc_company';
	/** 邀请用户表 */
	private static $t_invite_user = 'invite_user';
	/** 人员邀请数据表 */
	private static $t_invite_info = 'invite_info';


	/**活动状态, 4为进行中 */
	private static $ActivityState = 4;
	
	/**
	 * 插入用户信息
	 * @access public
	 * @param $data 用户注册信息
	 *
	 * @return bool
	 */
	public function insertUser($data) {
		$table = self::$t_user;

		$data = g('ndb') -> insert($table, $data);

		return $data;
	}

	/**
	 * 更新用户信息
	 * @access public
	 * @param $data 用户注册信息
	 *
	 * @return bool
	 */
	public function updateUser($id, $data) {
		$table = self::$t_user;

		$condition = array(
			'id=' => $id,
		);
		$result = g('ndb') -> update_by_condition($table, $condition, $data);

		return $result;
	}

	/**
	 * 录入邀请数据
	 * @access public 
	 * @param  $data 邀请人和被邀请人的id
	 *
	 * @return bool
	 */
	public function insertInvite($data) {
		$table = self::$t_invite_user;

		$result = g('ndb') -> insert($table, $data);

		return empty($result) ? false : $result;
	}

	/**
	 * 查询用户表中某注册信息的手机号
	 * @access public 
	 * @param  $phone 新用户注册时填写的手机号
	 *
	 * @return 
	 */
	public function checkPhone($phone, $com_id) {
		$table = self::$t_user;
		$cond = array(
			'com_id=' => $com_id,
			'mobile=' => $phone,
			"state!=" => 0,
		);

		$ret = g('ndb') -> record_exists($table, $cond);
		return $ret;
	}

	/**
	 * 获取邀请单数据
	 * @access public
	 * @param  $id 邀请表id
	 *
	 * @return 
	 */
	public function getInvitePageData($id) {
		$table = self::$t_invite_info;
		$field = '*';
		$cond = array(
			'id=' => $id,
		);

		$data = g('ndb') -> select($table, $field, $cond);
		
		return empty($data) ? false : $data[0];
	}

	/**
	 * 获取微信部门id
	 */
	public function get_wx_dept_id($id){
		$table = self::$t_dept;
		$field = 'wx_id';
		$cond = array(
			'id=' => $id,
		);

		$data = g('ndb') -> select($table, $field, $cond);
		return empty($data) ? false : $data[0]['wx_id'];
	}

	/**
	 * 获取微信部门ids
	 */
	public function get_wx_dept_ids($ids){
		if (empty($ids)) {
			return array();
		}
		$table = self::$t_dept;
		$field = 'wx_id';
		$cond = array(
			'id IN ' => "(" . implode('|', $ids) . ")",
		);

		$data = g('ndb') -> select($table, $field, $cond);
		!is_array($data) && $data = array();
		!empty($data) && $data = array_column($data, 'wx_id');
		
		return $data;
	}

	/**
	 * 获取部门ids
	 */
	public function get_dept_ids($ids, $field = '*'){
		if (empty($ids)) {
			return array();
		}
		$table = self::$t_dept;
		$cond = array(
			'id IN ' => "(" . implode('|', $ids) . ")",
		);

		$data = g('ndb') -> select($table, $field, $cond);
		!is_array($data) && $data = array();
		
		return $data;
	}

	/**
	 * 获取通讯录消息推送
	 * @access public
	 * @param  $id 邀请表id
	 *
	 * @return 
	 */
	public function contact_subscribe_data($id) {
		$table = self::$t_invite_info . ' li LEFT JOIN ' . self::$t_company . ' sc ON li.com_id = sc.id';
		$field = 'li.title, li.reply_des, li.create_time, sc.name com_name';
		$cond = array(
			'li.id=' => $id
		);

		$data = g('ndb') -> select($table, $field, $cond);
		return empty($data) ? false : $data[0];
	}

	/**
	 * 根据用户id获取邀请单信息
	 */
	public function invite_page_data_by_uid($uid) {
		$table = self::$t_invite_user . ' lu LEFT JOIN ' . self::$t_invite_info . ' li ON lu.invit_order_id = li.id';
		$field = 'lu.aid, lu.inviter_id, lu.invit_order_id, li.reply_type, li.reply_title, li.reply_des, li.reply_banner, li.reply_card_id, li.is_show, li.notify_user_list, li.notify_group_list, li.dept_list, li.title';
		$cond = array(
			'lu.uid=' => $uid, 
			'info_state !=' => 0,
		);
		$data = g('ndb') -> select($table, $field, $cond);
		return empty($data) ? false : $data[0];
	}
}