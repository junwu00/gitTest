<?php
/**
 * 客户管理数据模型
 *
 * @author chenyihao
 * @create 2015-03-17
 */
class cls_client {
	//公司id
	private $com_id   = NULL;
	//用户id
	private $user_id  = NULL;
	//用户name
	private $user_name  = NULL;
	//基础条件
	private $base_con = NULL;

	//客户信息表
	public $info_table 	 = 'client_client';

	//联系人信息表
	public $linkman_table 	 = 'client_linkman';
	
	//联系人信息表
	public $classify_table 	 = 'client_classify';

	//联系人信息表
	public $business_table 	 = 'client_business';
	
	//客户用户中间表表
	public $client_user_table 	 = 'client_user';
	
	//客户日志表表
	public $client_log_table 	 = 'client_log';
	
	//联系人日志表表
	public $client_linkman_log_table 	 = 'client_linkman_log';
	
	
	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$this -> user_name = $_SESSION[SESSION_VISIT_USER_NAME];
	}

	/**
	 * 检查当前用户是否有指定客户的操作权限(修改)
	 * @param unknown_type $id
	 * @return 有:客户信息; 无:false
	 */
	function check_client($id,$fields ='id'){
		$cond = array(
			'user_id=' => $this-> user_id,
			'client_id=' => $id,
		);
		$ret = g('ndb') ->select($this-> client_user_table,$fields,$cond);
		return $ret ? $ret : false; 
	}
	
	/**
	 * 检查当前用户是否有指定客户的从属用户（删除权限）
	 * @param unknown_type $id
	 * @return 有:客户信息; 无:false
	 */
	function check_own_client($id,$fields ='id'){
		$cond = array(
			'user_id=' => $this-> user_id,
			'id=' => $id,
			'info_state=' => 1,
		);
		$ret = g('ndb') ->select($this-> info_table,$fields,$cond);
		return $ret ? $ret : false; 
	}
	
	/**
	 * 判断是否拥有查看客户的权限
	 * @throws SCException
	 * @return 1:自己的客户，2.下属的客户，3.共享的客户,4，保存的客户
	 */
	function can_see_client($client_id){
		
		$client = $this -> get_client($client_id,'share_state,user_id,share_list,dept_list');
		if($client['user_id']==$this->user_id)
			return 1;
		
		if($this->check_client($client_id))
			return 4;
		
			
		//判断是否为下属的客户
		$depts = g('dept')->get_user_admin_dept($this->user_id);
		$like ='1=0';
		if($depts) foreach($depts as $d){
			$like .=' or dept_list like "%\"'.$d.'\"%" ';
		}
		$sql = <<<EOF
SELECT id 
FROM {$this -> info_table} 
WHERE info_state =1 and id = {$client_id} and ( {$like} ) and user_id != {$this-> user_id} and id not in (select client_id from {$this->client_user_table} where user_id = {$this->user_id})
EOF;
		$ret = g('db')-> select($sql);
		if($ret)
			return 2;
		
		if($this->user_id != $client['user_id'])
			switch($client['share_state']){
				case 0:		//未分享
					echo 1;
					throw new SCException('没有权限');
					break;
				case 1:		//指定分享
					$share_list = json_decode($client['share_list'],true);
					if(!in_array($uid, $share_list)){
						echo 2;
						throw new SCException('没有权限');
					}
					break;
				case 2:		//部门分享
					$user = g('user')-> get_by_id($uid);
					$dept_list = json_decode($client['dept_list'],true);
					$user_dept = json_decode($user['dept_list'],true);
					if(!array_intersect($dept_list,$user_dept)){
						echo 3;
						throw new SCException('没有权限');
					}
					break;
			}
		return 3;	
	}
	
	/**
	 * 判断是否有指派的权限
	 */
	function can_assgin($clinet_id){
		//判断是否为下属的客户
		$depts = g('dept')->get_user_admin_dept($this->user_id);
		$like ='1=0';
		if($depts) foreach($depts as $d){
			$like .=' or dept_list like \'%"'.$d.'"%\' ';
		}
		$sql = <<<EOF
SELECT id 
FROM {$this -> info_table} 
WHERE info_state =1 and  id = {$clinet_id} and ( {$like} ) and user_id != {$this-> user_id} and id not in (select client_id from {$this->client_user_table} where user_id = {$this->user_id})
EOF;
		$ret = g('db')-> select($sql);
		return $ret ? true : false;
	}
	
	/**
	 * 获取商业类型列表
	 */
	function get_business_type_list($fields = '*'){
		$cond = array(
			'com_id=' => $this -> com_id,
			'info_state='=>1,
			'is_enabled=' => 1
		);
		return g('ndb') -> select($this->business_table,$fields,$cond);
	}
	
	/**
	 * 获取客户类型列表
	 */
	function get_classify_type_list($fields = '*'){
		$cond = array(
			'com_id=' => $this -> com_id,
			'info_state='=>1,
			'is_enabled=' => 1
		);
		return g('ndb') -> select($this->classify_table,$fields,$cond);
	}
	
	/**
	 * 获取下属员工
	 */
	function get_subordinate($fields='*'){
		$depts = g('dept') -> get_user_admin_dept($this-> user_id);
		
		if(!$depts)
			return false;
			$like ='1 = 0 ';
		foreach($depts as $d){
			$like .=' or dept_list like \'%"'.$d.'"%\' ';
		}
		$sql = <<<EOF
SELECT {$fields}
FROM sc_user
WHERE ({$like}) and state !=0
EOF;
		$ret = g('db')-> select($sql);
		return $ret ? $ret :false;
	}
	
	/**
	 * 判断指定员工是否为登录员工的下属
	 * @param unknown_type $uid
	 */
	function is_subordinate($uid){
		$depts = g('dept') -> get_user_admin_dept($this-> user_id);
		$user = g('user') -> get_by_id($uid);
		$user_dept = json_decode($user['dept_list'],true);
		if(array_intersect($depts,$user_dept))
			return true;
		else
			return false;
	}
	
	/**
	 * 将指定客户指派给指定给员工 
	 * @param unknown_type $client_id
	 * @param unknown_type $user_id
	 */
	function assign($client_id,$user_id,$user_name=""){
		$client = $this -> get_client($client_id,'user_id');
		$old_user = $client['user_id'];
		$cond = array(
			'id=' => $client_id,
			'com_id=' => $this -> com_id
		);
		$data = array(
			'user_id' => $user_id,
			'user_name' => $user_name
		);
		
		try{
			g('db') -> begin_trans();
			g('ndb') -> update_by_condition($this->info_table,$cond,$data);		//更新客户从属用户
			$this -> delete_client_user($client_id,$old_user);						//删除原从属用户的信息
			$this -> add_client_user($client_id,$user_id);						//添加新从属用户的信息
			g('db') -> commit();
			return true;
		}catch(SCException $e){
			g('db') -> rollback();
			throw $e;
		}
	}
	
	/**
	 * 获取客户的详细信息
	 * @param unknown_type $id
	 * @param unknown_type $withlinkman 是否获取联系人
	 */
	function get_client($id,$fields='*',$withlinkman=false,$linkman_fields ='*'){
			
		$cond = array(
			'com_id=' => $this->com_id,
			'id=' => $id,
			'info_state='=>1
		);
		$ret = g('ndb')->select($this->info_table,$fields,$cond);
		$ret[0]['client_expand'] = json_decode($ret[0]['client_expand'],true);
		if($ret && $withlinkman){
			$this -> get_linkman($ret,false,$linkman_fields);
		}
		return $ret ? $ret[0] : false;
	}
	
	/**
	 * 根据type返回客户列表
	 * @param unknown_type $type
	 */
	public function get_client_list($type,$page=1,$page_size =20,$key=""){
		$fields = 'id,client_name';
		
		if($key)
			$key = " client_name like '%{$key}%' and ";
		
		//计算分页信息
		if ($page > 0 && $page_size > 0)
			$begin = ($page - 1) * $page_size;
		
		switch($type){
			case 1:
				$sql = <<<EOF
SELECT {$fields} 
FROM {$this -> info_table} 
WHERE info_state =1 and {$key}id in (SELECT `client_id` FROM {$this->client_user_table} where user_id = {$this->user_id}) 
LIMIT {$begin},{$page_size} 
EOF;
				break;
			case 2:
				$depts = g('dept')->get_user_admin_dept($this->user_id);
				$like = '(1=0';
				if($depts) foreach($depts as $d){
					$like .=' or dept_list like "%\"'.$d.'\"%" ';
				}
				$like .=')';
				$sql = <<<EOF
SELECT {$fields} 
FROM {$this -> info_table} 
WHERE info_state =1 and{$key}user_id !={$this->user_id} and ( (share_state = 1 and share_list like '%"{$this->user_id}"%') or (share_state = 2 and {$like}))  
LIMIT {$begin},{$page_size} 
EOF;
				break;
			case 3:
				$depts = g('dept')->get_user_admin_dept($this->user_id);
				$like = '(1=0';
				if($depts) foreach($depts as $d){
					$like .=' or dept_list like "%\"'.$d.'\"%" ';
				}
				$like .=')';
				$sql = <<<EOF
SELECT {$fields} 
FROM {$this -> info_table} 
WHERE info_state =1 and{$key}({$like}) and user_id != {$this-> user_id}
LIMIT {$begin},{$page_size} 
EOF;
				break;
		}	
		
		try{
			$ret = g('db')-> select($sql);
			return $ret ? $ret : false;
		}catch(SCException $e){
			throw $e;
		}
	}

	/**
	 * 批量获取客户的联系人
	 * @param unknown_type $clients			//客户信息(数组/id)
	 * @param unknown_type $linkman_id		//指定联系人ID 		若不为空，则第一个参数为客户id
	 * @param unknown_type $fields			//联系人返回字段
	 */
	function get_linkman(&$clients,$linkman_id=false,$fields ="*"){
		
		if(!$linkman_id){
			if(!is_array($clients) || empty($clients)){
				return false;
			}
		}else{
			$clients = array(array('id' => $clients));
		}
		foreach($clients as &$client){
			$cond = array(
				'client_id=' => $client['id'],
				'info_state=' => 1
			);
			$linkman_id && $cond['id='] = $linkman_id;
			$ret = g('ndb') -> select($this-> linkman_table,$fields,$cond);
			$client['linkman'] = $linkman_id ? $ret[0] : $ret;
		}
		return $clients;
	}
	
	/**
	 * 添加客户
	 * @param unknown_type $info	客户信息
	 */
	public function add_client($info){
		$user = g('user') -> get_by_id($this->user_id,'dept_list');
		$info['user_id'] = $this->user_id;
		$info['dept_list'] = $user['dept_list'];
		$info['user_name'] = $this->user_name;
		$info['com_id'] = $this->com_id;
		
		
		try{
			$nid =  g('ndb') -> insert($this->info_table,$info,TRUE);
			self::add_client_user($nid);
			return $nid;
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 修改客户信息
	 * @param unknown_type $info	客户信息
	 */
	public function edit_client($id,$info){
		$cond = array(
			'id=' => $id,
			'com_id=' => $this-> com_id 
		);
		
		try{
			$ret =  g('ndb') -> update_by_condition($this->info_table,$cond,$info,false);
			if($ret)
				return $ret;
			else
				throw new SCException('更新客户信息失败');	
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 修改联系人信息
	 * @param unknown_type $info	联系人信息
	 */
	public function edit_linkman($client_id,$id,$info){
		$cond = array(
			'id=' => $id,
			'client_id=' =>$client_id 
		);
		
		try{
			$ret = g('ndb') -> update_by_condition($this->linkman_table,$cond,$info,false);
			if($ret)
				return $ret;
			else
				throw new SCException('更新联系人信息失败');	
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 删除联系人
	 * @param unknown_type $client_id			客户ID
	 * @param unknown_type $linkman_id			联系人ID，若为空，则删除该客户所有的联系人
	 */
	function delete_linkman($client_id,$linkman_id=false){
		try{
			$cond =array();
			$data = array('info_state'=>2);
			$cond['client_id=']= $client_id;
			$linkman_id && $cond['id='] = $linkman_id;
			
			$ret = g('ndb') -> update_by_condition($this -> linkman_table,$cond,$data,false);
			if(!$ret)
				throw new SCException('删除失败');
		}catch (SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 删除客户信息
	 * @param unknown_type $id
	 */
	function delete_client($id){
		try{
			$cond =array();
			
			$client = $this ->get_client($id,'user_id');
			$is_own = $client['user_id'] == $this -> user_id;
			
			if($is_own){
				$data =array(
					'info_state' => 2
				);
				$cond =array(
					'id=' => $id,
				);
				$ret = g('ndb') -> update_by_condition($this -> info_table,$cond,$data,false);	//删除客户
				$cond =array(
					'client_id=' => $id,
				);
				$ret = g('ndb') -> update_by_condition($this -> linkman_table,$cond,$data,false);	//删除联系人
			}
			$cond =array(
				'client_id=' => $id,
			);
			!$is_own && $cond['user_id='] = $this->user_id; 
			$ret = g('ndb') -> delete($this -> client_user_table,$cond);	//删除用户客户对应关系
			if(!$ret)
				throw new SCException('删除失败');
			else
				$this-> log_clent($id, '删除客户');
		}catch (SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 将指定客户添加给指定用户
	 * @param unknown_type $cid			客户id
	 * @param unknown_type $user_id		用户id
	 */
	public function add_client_user($cid,$user_id = false){
		if(!$user_id)
			$user_id  = $this ->user_id;
		
		$client_user = array(
			'user_id' => $user_id,
			'create_time' => time(),
			'client_id' => $cid, 
		);
		g('ndb') -> insert($this->client_user_table,$client_user,TRUE);
	}
	
	/**
	 * 将指定客户和指定用户移除关系
	 * @param unknown_type $cid			客户id
	 * @param unknown_type $user_id		用户id
	 */
	public function delete_client_user($cid,$user_id){
		$cond = array(
			'user_id=' => $user_id,
			'client_id=' => $cid, 
		);
		g('ndb') -> delete($this->client_user_table,$cond);
	}
	
	/**
	 * 添加联系人
	 * @param unknown_type $cid		客户ID
	 * @param unknown_type $linkmans联系人数组
	 */
	function add_linkman($cid,$lms){
		$fields = 'linkman_name,linkman_phone,linkman_dept,linkman_position,linkman_email,linkman_telautogram,
				  linkman_expand,is_main,create_time,update_time,info_state,client_id';
		foreach($lms as &$lm){
			$lm['client_id'] =$cid;
		}
		try{
			return g('ndb') -> batch_insert($this->linkman_table,$fields,$lms);
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 获取行业类型
	 * @param unknown_type $id
	 */
	function get_classify_type($id,$fields = '*'){
		$cond = array(
			'id=' => $id,
			'com_id=' => $this -> com_id,
			'is_enabled=' => 1,
			'info_state=' => 1
		);
		try{
			$ret = g('ndb') -> select($this-> classify_table,$fields,$cond);
			if($ret)
				return $ret[0];
			else
				throw new SCException('不存在的行业分类');
		}catch(SCException $e){
			throw $e;
		}
	}
	
	/**
	 * 获取客户类型
	 * @param unknown_type $id
	 */
	function get_business_type($id,$fields = '*'){
		$cond = array(
			'id=' => $id,
			'com_id=' => $this -> com_id,
			'is_enabled=' => 1,
			'info_state=' => 1
		);
		try{
			$ret = g('ndb') -> select($this-> business_table,$fields,$cond);
			if($ret)
				return $ret[0];
			else
				throw new SCException('不存在的商业分类');
		}catch(SCException $e){
			throw $e;
		}
	}
	
	
	/**
	 * 客户表操作日志
	 * @param unknown_type $client_id	客户ID
	 * @param unknown_type $text		日志内容
	 */
	function log_client($client_id,$text){
		$data = array(
			'user_id' => $this->user_id,
			'user_name' => $this ->user_name,
			'client_id' => $client_id,
			'log_msg' => $text,
			'create_time' => time()
		);
		g('ndb') -> insert($this-> client_log_table,$data);
	}
	
	/**
	 * 联系人表操作日志
	 * @param unknown_type $linkman_id	客户ID
	 * @param unknown_type $text		日志内容
	 */
	function log_linkman($linkman_id,$text){
		$data = array(
			'user_id' => $this->user_id,
			'user_name' => $this ->user_name,
			'linkman_id' => $linkman_id,
			'log_msg' => $text,
			'create_time' => time()
		);
		g('ndb') -> insert($this-> client_linkman_log_table,$data);
	}
}