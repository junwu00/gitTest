<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_proc_setting {
	/** 对应的库表名称  */
	private static $Table = 'proc_setting';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	/** 冻结 2*/
	private static $StateFrozen = 2;
	/** 删除 3*/
	private static $StateDeleted = 3;
	
	/** 非自定义 0 */
	private static $NotCustom = 0;
	/** 自定义 1 */
	private static $Custom = 1;
	
	/**
	 * 企业自定义审批流程(所有)
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function list_custom($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'is_custom=' => self::$Custom,
			'state!=' => self::$StateDeleted
		);
		//列出所有流程
		return g('ndb') -> select(self::$Table, $fields, $cond, 0, 0, '', ' order by create_time ');
	}
	
	/**
	 * 获取员工可见的 企业自定义审批流程(后台可设置哪些部门可见)
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $fields
	 */
	public function list_user_custom($com_id, $user_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'is_custom=' => self::$Custom,
			'state!=' => self::$StateDeleted
		);
		//列出所有流程
		$list = g('ndb') -> select(self::$Table, $fields, $cond, 0, 0, '', ' order by create_time ');
		if (!$list) 	return $list;
		
		
		//员工可见流程
		$user = g('user') -> get_by_id($user_id);
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$depts = array();
		foreach ($dept_tree as $dept) {
			$depts = array_merge($depts, $dept);
		}
		unset($dept);
		
		$ret = array();
		foreach ($list as $item) {			//遍历所有流程
			$scope = json_decode($item['scope'], TRUE);
			if (count($scope) == 0) {				//企业内可见
				array_push($ret, $item);
				
			} else {
				foreach ($scope as $s) {			//记录可见的流程
					if (in_array($s, $depts)) {
						array_push($ret, $item);
						break;
					}
				}
				unset($s);
			}
		}
		unset($item);
		return count($ret) > 0 ? $ret : FALSE;
	}
	
	/**
	 * 根据ID获取自定义审批流程
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function get_custom_by_id($com_id, $id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
			'is_custom=' => self::$Custom,
			'state!=' => self::$StateDeleted,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret) {
			$this -> decode_setting_info($ret[0]);
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 根据ID获取审批流程
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function get_by_id($com_id, $id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
			'state!=' => self::$StateDeleted,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret) {
			$this -> decode_setting_info($ret[0]);
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	//---------------------------------------内部实现

	/**
	 * 将流程中的JSON格式数据转换成数组格式
	 * @param unknown_type &$rule_list
	 */
	private function decode_setting_info(&$setting) {
		if ($setting == FALSE)	return;

		$scope = json_decode($setting['scope'], TRUE);
		if (count($scope) == 0) {
			$setting['scope'] = array();
		} else {
			$setting['scope'] = g('dept') -> get_dept_name($scope);
		}
		
		$setting['inputs'] = json_decode($setting['inputs'], TRUE);
		foreach ($setting['inputs'] as &$input) {
			if ($input['type'] == 'select')
				$input['opt'] = explode(',', $input['opt']);
		}
	}
}

// end of file