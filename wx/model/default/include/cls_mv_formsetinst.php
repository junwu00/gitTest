<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_formsetinst {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_formsetinst';
	
	/** 禁用 0*/
	private static $StateOff = 0; 
	/** 启用 1*/
	private static $StateOn = 1;
	
	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	private static $StateDraft = 0;
	/** 运行中 */
	private static $StateDone = 1;
	/** 已结束 */
	private static $StateFinish = 2;
	/** 终止 */
	private static $StateStop = 3;
	
	/**
	 * 保存表单实例new
	 * @param int $form_id 表单模板id
	 * @param int $work_id 流程模板id
	 * @param string $formsetinst_name 实例名称
	 * @param int $creater_id 创建人id
	 * @param string $creater_name 创建人名称
	 * @param int $com_id 企业id
	 * @param array $form_vals 表单内容
	 * @param int $is_other_proc 实例模块 0 流程 1请假 2报销
	 * @param int $create_dept_id 创建人部门id
	 * @param string $create_dept_name 创建人部门名称
	 */
	public function save_formsetinst($form_id, $work_id, $formsetinst_name,$creater_id,$creater_name,$com_id,$form_vals,$is_other_proc=0,$create_dept_id,$create_dept_name) {
		$form_vals = json_encode($form_vals);
		$data = array(
			'form_id' => $form_id,
			'work_id' => $work_id,
			'formsetinst_name' => $formsetinst_name,
			'create_time' =>time(),
			'creater_id' => $creater_id,
			'creater_name' => $creater_name,
			'info_state' => self::$StateOn,
			'com_id' => $com_id,
			'form_vals' => $form_vals,
			'state' =>self::$StateDraft,
			'is_other_proc' =>$is_other_proc,
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据ID更新审批表单内容
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function update_formsetinst($formsetinst_id,$com_id,$form_vals) {
		$form_vals = json_encode($form_vals);
		$data = array(
			'form_vals' => $form_vals
		);
		$cond = array(
			'id='=>$formsetinst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新表单实例失败');
		}
		return $ret;
	}
	
	
	
	/**
	 * 根据ID更新审批表单状态new
	 * @param int $com_id
	 * @param int $formsetinst_id
	 * @param int $state
	 */
	public function update_formsetinst_state($formsetinst_id,$com_id,$state) {
		$data = array(
			'state' => $state
		);
		$cond = array(
			'id='=>$formsetinst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据ID更新审批步骤new
	 * @param int $formsetinst_id
	 * @param int $com_id
	 * @param string $workitem_rule 流向字段
	 * @param int $curr_node_id 当前节点id
	 */
	public function update_formsetinst_workitemrule($formsetinst_id,$com_id,$workitem_rule,$curr_node_id) {
		
		$data = array(
			'workitem_rule' => $workitem_rule,
			'curr_node_id' =>	$curr_node_id
		);
		$cond = array(
			'id='=>$formsetinst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('更新表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 删除表单实例
	 */
	public function del_formsetInst($formsetInst_id,$com_id,$user_id) {
		$data = array(
			'info_state' => self::$StateOff,
			'info_time' => time(),
			'info_user_id' => $user_id
		);
		$cond = array(
			'id='=>$formsetInst_id,
			'com_id=' => $com_id,
		); 
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		
		if (!$ret) {
			throw new SCException('删除表单实例失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据id获取表单实例new
	 * @param int $formsetinst_id
	 * @param int $com_id
	 */
	public function get_formsetinst_by_id($formsetinst_id,$com_id,$user_id,$fields='mpf.*,mpfm.form_name,mpfm.form_describe,mpfm.is_linksign '){
		$formsetinstsql = "select ".$fields."  
		from mv_proc_formsetinst mpf,mv_proc_form_model mpfm 
		where mpfm.id = mpf.form_id and mpf.id = ".$formsetinst_id." and mpf.com_id = ".$com_id." 
		and (EXISTS (SELECT 1 from mv_proc_workitem where creater_id = ".$user_id." or handler_id = ".$user_id.") or 
		EXISTS (SELECT 1 from mv_proc_notify where notify_id = ".$user_id."))";
		$formsetinst = g('db') -> select_one($formsetinstsql);
		return $formsetinst;
	}
	
	
	/**
	 * 获取我起草的列表
	 */
	public function get_do_list($condition=Array(),$com_id,$user_id,$is_finish=0,$is_proc_type=0,$page=0,$page_size=0,$fields='mpf.*,mpwn.work_node_name,mpfm.form_name',$order_by=" order by mpf.create_time desc ",$with_count = FALSE){
		$sql = "select ".$fields." from mv_proc_formsetinst mpf,mv_proc_work_node mpwn,mv_proc_form_model mpfm
		where mpf.curr_node_id = mpwn.id and mpfm.id = mpf.form_id 
		and mpf.creater_id = ".$user_id." and mpf.com_id = ".$com_id." and mpf.info_state =".self::$StateOn." and mpf.is_other_proc=".$is_proc_type;
		
		foreach ($condition as $key => $val) {
			if($key=="formsetinst_name"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.formsetinst_name like '%".$val."%'";
				continue;
			}

			if($key=="s_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time>=".$val;
				continue;
			}
			if($key=="e_create_time"&&!empty($val)&&$val!=""){
				$sql = $sql." and mpf.create_time<".$val;
				continue;
			}
			if($key=="state"&&$val!=""){
				log_write($key." ".$val);
				$sql = $sql." and mpf.state=".$val;
				continue;
			}
		}
		unset($val);
		if($is_finish==0){//运行中
			$state = " and mpf.state=".self::$StateDone;
		}
		else if($is_finish==1){//已结束
			$state = " and mpf.state=".self::$StateFinish;
		}
		else{
			$state = " and mpf.state=".self::$StateDraft;
		}
		
		$sql = $sql.$state;
		
		
		$limit_sql = $sql.$order_by;
		if ($page > 0 && $page_size > 0) {
			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);
			$limit_sql .= $limit;
		}
		$ret = g('db') -> select($limit_sql);
		
		return $ret ? $ret : FALSE;
	}
	
	//---------------------------------------内部实现-------------------------------

}

// end of file