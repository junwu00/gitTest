<?php
/**
 * 任务信息
 * @author yangpz
 * @date 2014-12-05
 */
class cls_task_base {
	/** 对应库表名称 */
	protected static $Table = 'task_info';
	
	protected static $TablePerform = 'task_perform';
	
	protected static $TableSuperintendent = 'task_superintendent';
	
	protected static $TableHelper = 'task_helper';
	
	protected static $TableFile = 'task_file';
	
	protected static $TableLog = 'task_log';
	
	protected static $TableTalking = 'task_talking';
	
	protected static $TableReport = 'task_report';
	
	protected static $TableApply = 'task_apply';
	
	protected static $TableClassify = 'task_classify';
	
	protected static $TableUser = 'sc_user';

	/** 禁用 0 */
	protected static $StateOff = 0;
	/** 启用 1 */
	protected static $StateOn = 1;
	
	/** 未完成 */
	protected static $IsFinishOff = 0;
	/** 已完成   */
	protected static $IsFinishOn = 1;
	
	/** 是否同意 未确认 */
	protected static $IsAgreeOff = 0;
	/** 是否同意 不同意 */
	protected static $AgreeOff = 1;
	/** 是否同意 同意 */
	protected static $AgreeOn = 2;

	/** 草稿 0 */
	protected static $StateDraft = 0;
	/** 执行中 1 */
	protected static $StateRunning = 1;
	/** 申请退回 2 */
	protected static $StateApplyCallback = 2;
	/** 申请延期 3 */
	protected static $StateApplyDelay = 3;
	/** 申请完成 4 */
	protected static $StateApplyFinish = 4;
	/** 逾期完成 5 */
	protected static $StateOverdue = 5;
	/** 按时完成 6 */
	protected static $StateOntime = 6;
	/** 终止 7 */
	protected static $StateStop = 7;
	
	
	//查看当前用户是否有权限查看子任务和父任务
	protected function get_user_role($taskid,$userid){
		$flag = FALSE;
		if($this->get_user_perform($taskid, $userid)||$this->get_user_assign($taskid, $userid)||$this->get_user_superintendent($taskid, $userid)||$this->get_user_helper($taskid, $userid)){
			$flag = true;
		}
		return $flag;
	}
	
	
	//是否有承办人权限
	protected function get_user_perform($taskid,$userid){
		$performsql = "select count(1) count from ".self::$TablePerform." where taskid=".$taskid." and perform_id=".$userid;
		$perform_count = g('db') -> select_one($performsql);
		if($perform_count["count"]!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	//是否有分配人权限
	protected function get_user_assign($taskid,$userid){
		$assignsql = "select count(1) cout from ".self::$Table." where id=".$taskid." and assign_id=".$userid;
		$assign_count = g('db') -> select_one($assignsql);
		if($assign_count["cout"]!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	//是否有监督人权限
	protected function get_user_superintendent($taskid,$userid){
		$superintendentsql = "select count(1) cout from ".self::$TableSuperintendent." where taskid=".$taskid." and superintendent_id=".$userid;
		$superintendent_count = g('db') -> select_one($superintendentsql);
		if($superintendent_count["cout"]!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	//是否有协办人权限
	protected function get_user_helper($taskid,$userid){
		$helpersql = "select count(1) cout from ".self::$TableHelper." where taskid=".$taskid." and helper_id=".$userid;
		$helper_count = g('db') -> select_one($helpersql);
		if($helper_count['cout']!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
}

// end of file