<?php 
/**
 * 微信企业员工信息验证处理器
 * @author yangpz
 * @date 2014-12-20
 *
 */
class cls_httpauth {
	//用户信息表
	private $user_table = 'sc_user';

	/**
	 *	可绕过check_user方法的url参数配置
	 *	需要url中带free为参数名的参数，如：http://host/index.php?app=xxx&free。或不带free，默认进行用户身份验证
	 *	如（以下四项不能都为空）：
	 *	array(
	 *		'app' => 'process',	//可为空，如：'app' => '',
	 *		'm' => 'apply',		//可为空，如：'m' => '',
	 *		'a' => 'apply',		//可为空，如：'a' => '',
	 *		'match' => '',		//可为空，如：'match' => ''。正则表达式，用于检测url是否与该表达式匹配
	 *		'lev' => 1,			//放行等级：1=>不检测企业和员工；2=>检测企业和员工，检测失败继续放行，
	 *	),
	 */
	private static $FreeList = array(
		//招聘上传头像//TODO
		array(
			'app'	=> '',
			'm'		=> 'root_ajax',
			'a'		=> '',
			'match' => '/cmd=101/',
			'lev'	=> 2,
		),
		//快速体验
		array(
			'app'	=> '',
			'm'		=> 'quick_try',
			'a'		=> '',
			'match' => '',
			'lev'	=> 1,
		),
		//分享客户名片
		array(
			'app' 	=> 'card',
			'm'		=> 'info',
			'a'		=> 'show',
			'match' => '',
			'lev'	=> 2,
		),
		//下载客户名片到通讯录
		array(
			'app' 	=> 'card',
			'm'		=> 'ajax',
			'a'		=> '',
			'match' => '/cmd=113/',
			'lev'	=> 2,
		),
		//参加调研
		array(
			'app' 	=> 'survey',
			'm'		=> 'detail',
			'a'		=> '',
			'match' => '',
			'lev'	=> 2,
		),
		//提交调研问卷
		array(
			'app' 	=> 'survey',
			'm'		=> 'ajax',
			'a'		=> '',
			'match' => '/cmd=101/',
			'lev'	=> 2,
		),
        //jssdk初始化
        array(
            'app' 	=> '',
            'm'		=> 'root_ajax',
            'a'		=> '',
            'match' => '/cmd=104/',
            'lev'	=> 2,
        ),
		//推荐有赏主页
		array(
			'app' 	=> 'recruit',
			'm'		=> 'recommend',
			'a'		=> '',
			'match' => '',
			'lev'	=> 2,
		),
		//职位详情
		array(
			'app' 	=> 'recruit',
			'm'		=> 're_job',
			'a'		=> '',
			'match' => '',
			'lev'	=> 2,
		),
		//推荐有赏主页
		array(
			'app' 	=> 'recruit',
			'm'		=> 'ajax',
			'a'		=> '',
			//'match' => 'cmd=!(103|104|105|106|110|111|112|113|114|116|117)',
			'lev'	=> 2,
		),
		//邀请单注册页面
		array(
			'app' 	=> 'contact',
			'm'		=> 'index',
			'a'		=> 'jz_contact_register',
			'lev'	=> 2,
		),
		//邀请单关注页面
		array(
			'app' 	=> 'contact',
			'm'		=> 'index',
			'a'		=> 'jz_contact_subscribe',
			'lev'	=> 2,
		),
		//邀请单数据录入接口
		array(
			'app' 	=> 'contact',
			'm'		=> 'ajax',
			'a'		=> 'register',
			'lev'	=> 2,
		),
		//根据手机号获取用户信息接口
		array(
			'app' 	=> 'contact',
			'm'		=> 'ajax',
			'a'		=> 'getChildInfo',
			'lev'	=> 2,
		),

        //通知公告支持家长登录
        array(
            'app' 	=> 'helper',
            'm'		=> 'content',
            'a'		=> 'index',
            'lev'	=> 3,
        ),

        array(
            'app' 	=> 'helper',
            'm'		=> 'news',
            'a'		=> '',
            'lev'	=> 3,
        ),

        array(
            'app' 	=> '',
            'm'		=> 'root_ajax',
            'a'		=> '',
            'match' => '/cmd=(103)/',
            'lev'	=> 2,
        ),
        array(
            'app' 	=> 'helper',
            'm'		=> 'content',
            'a'		=> 'download',
            'lev'	=> 2,
        ),

	);
	
	/**
	 * 验证页面访问权限
	 */
	public function check_auth() {
//		$this -> check_ip();
		$free = $this -> check_free();

		switch ($free) {
			case 1:		//不检测企业和员工
				break;
				
			case 2:		//检测企业和员工，检测失败继续放行
				try {
					$this -> check_com();
                    $this -> check_com_state();
					$this -> check_app();
					$this -> check_user(2);
				} catch (SCException $e) {}
				break;
            case 3:
                //同时支持内部成员与家长登录
                try {
                    $this -> check_com();
                    $this -> check_com_state();
                    $this -> check_app();
                    $this -> check_user_with_parent();
                } catch (SCException $e) {
                    $this -> unset_session_info();
                    cls_resp::show_err_page(array($e -> getMessage()));
                }
                break;
			default:	//检测企业和员工，失败不放行
				try {
					$this -> check_com();
                    $this -> check_com_state();
					$this -> check_app();
					$this -> check_user();	
				} catch (SCException $e) {
					$this -> unset_session_info();
					cls_resp::show_err_page(array($e -> getMessage()));	
				}
				break;
		}
		
		try {
			$this -> check_visit();
			
		} catch (SCException $e) {
			cls_resp::show_err_page(array($e -> getMessage()));	
		}
	}
	
	/**
	 * 当前请求是否可绕过授权
	 * @return boolean
	 */
	private function check_free() {
		if (!isset($_GET['free']))	return FALSE;
		
		$m = get_var_get('m', FALSE);
		$a = get_var_get('a', FALSE);
		$app = get_var_get('app', FALSE);
		
		$flag = FALSE;
		$url = get_this_url();
		foreach (self::$FreeList as $item) {
			//配置的四项都空，若都为空，跳过该配置
			if (empty($item['app']) && empty($item['m']) && empty($item['a']) && empty($item['match']))	continue;		
			
			if (!empty($item['app']) && strcmp($item['app'], $app) != 0)		continue;
			if (!empty($item['m']) && strcmp($item['m'], $m) != 0)				continue;
			if (!empty($item['a']) && strcmp($item['a'], $a) != 0)				continue;
			if (!empty($item['match']) && !preg_match($item['match'], $url))	continue;

			$flag = $item['lev'];
			break;
		}
		unset($item);
		return $flag;
	}
	
	/**
	 * 验证当前访问的IP与session中的ip是否一致，不一致，清除对应的cookie和session
	 */
	private function check_ip() {
		$now_ip = get_ip();
		if (!isset($_SESSION[SESSION_VISIT_IP])) {				//未验证过
			$_SESSION[SESSION_VISIT_IP] = $now_ip;
			return;
		}
		
		if (strcmp($now_ip, $_SESSION[SESSION_VISIT_IP]) != 0) {
			log_write('非法访问，原IP:'.$_SESSION[SESSION_VISIT_IP].', 新IP'.$now_ip);
			$this -> unset_cookie_info();
			$this -> unset_session_info();
			//重定向到当前页面
			$url = get_this_url();
			header('Location:'.$url);
		}
	}
	
	/**
	 * 验证要访问的员工信息
	 */
	private function check_user($lev=3) {
		//获取当前用户所在当前企业号对应的微信账号
		$wxacct = $this -> get_wxacct($lev);

		if (!$wxacct) {
			//获取不到员工信息，终止执行
			throw new SCException('找不到您的信息。可能是管理员未同步您的数据');
		}

		$user = g('user') -> get_by_acct($_SESSION[SESSION_VISIT_DEPT_ID], $wxacct, '*', NULL, FALSE);
		if ($user) {
			if ($user['state'] == 4) {	//未关注，改为已关注
				log_write('开始更新用户关注状态为: 已关注');
				global $sc_app_id;
				g('user') -> change_state($_SESSION[SESSION_VISIT_DEPT_ID], $user['acct'], 1);
				$token = g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $sc_app_id);
				g('user') -> set_pic_url($token, $_SESSION[SESSION_VISIT_DEPT_ID], $user['acct']);
				log_write('更新成功');
			}
			if ($user['state'] == 2) {
				throw new SCException('您的账号已被禁用。请联系管理员');	
			}
			
			$dept_list = $user['dept_list'];
			$dept_list = json_decode($dept_list, TRUE);
			$this -> append_user_cookie($_SESSION[SESSION_VISIT_CORP_URL], $user, $dept_list[0]);
//			log_write('获取成功：user_id='.$_SESSION[SESSION_VISIT_USER_ID]);
		} else {
			throw new SCException('找不到您的信息。可能是管理员未同步您的数据');	
		}
		//log_write('获取员工信息成功: user='.$_SESSION[SESSION_VISIT_USER_ID].', user_dept='.$_SESSION[SESSION_VISIT_USER_DEPT]);
	}

    /**
     * 验证要访问的员工信息
     */
    private function check_user_with_parent($lev=3) {
        //获取当前用户所在当前企业号对应的微信账号
        $wxinfo = $this -> get_wxacct_auth_info($lev);
        !is_array($wxinfo) && $wxinfo = json_decode($wxinfo,true);

        if (!$wxinfo) {
            //获取不到员工信息，终止执行
            throw new SCException('找不到您的信息。可能是管理员未同步您的数据');
        }

        //通过信息判断是否为家长
        if( isset($wxinfo['UserId']) ){
            //内部人员登录
            $wxacct = $wxinfo['UserId'];
            $user = g('user') -> get_by_acct($_SESSION[SESSION_VISIT_DEPT_ID], $wxacct, '*', NULL, FALSE);
            if ($user) {
                if ($user['state'] == 4) {	//未关注，改为已关注
                    log_write('开始更新用户关注状态为: 已关注');
                    global $sc_app_id;
                    g('user') -> change_state($_SESSION[SESSION_VISIT_DEPT_ID], $user['acct'], 1);
                    $token = g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $sc_app_id);
                    g('user') -> set_pic_url($token, $_SESSION[SESSION_VISIT_DEPT_ID], $user['acct']);
                    log_write('更新成功');
                }
                if ($user['state'] == 2) {
                    throw new SCException('您的账号已被禁用。请联系管理员');
                }

                $dept_list = $user['dept_list'];
                $dept_list = json_decode($dept_list, TRUE);
                $this -> append_user_cookie($_SESSION[SESSION_VISIT_CORP_URL], $user, $dept_list[0],$wxinfo);
            } else {
                throw new SCException('找不到您的信息。可能是管理员未同步您的数据');
            }
        }elseif( isset($wxinfo['parent_userid']) ){
            //家长登录
            $parent_userid = $wxinfo['parent_userid'];
            $parent = g('ndb')->select(
                'school_user_parents',
                'id,parent_userid,relation,student_userid,mobile',
                array('info_state='=>1,'com_id='=>$_SESSION[SESSION_VISIT_COM_ID],'parent_userid='=>$parent_userid),
                1,1);
            if($parent){
                $parent = $parent[0];
                $this -> append_parent_cookie($_SESSION[SESSION_VISIT_CORP_URL], $parent,$wxinfo);
            }else{
                throw new SCException('找不到您的信息。可能是管理员未同步您的数据');
            }

        }else{
            throw new SCException('找不到您的信息。可能是管理员未同步您的数据');
        }

    }

	/**
	 * 获取员工对应的微信账号
	 *
	 * @access private
	 * @param integer $lev 放行等级
	 * @return mixed
	 */
	private function get_wxacct($lev=3) {
		$wxacct = NULL;
		if (isset($_SESSION[SESSION_VISIT_USER_WXACCT])) {
			$wxacct = $_SESSION[SESSION_VISIT_USER_WXACCT];
			$user = g('user') -> get_by_acct($_SESSION[SESSION_VISIT_DEPT_ID], $wxacct);
			if(empty($user)){
				$wxacct = '';
			}
		}else {
			//检测cookie中的user信息是否有变更，无变更直接使用，无需授权
			if ($_SESSION[SESSION_VISIT_CORP_URL] != 'e541ab0ecc9408b1f4d32d363c1e1c0b') {
				$wxacct = $this -> check_user_cache($_SESSION[SESSION_VISIT_CORP_URL]);
				$wxacct and log_write('COOKIE缓存可用，无需授权，wxacct=' . $wxacct);
			}
		}

		if ($wxacct) return $wxacct;

		$code = isset($_GET['code']) ? $_GET['code'] : NULL;
		if (empty($code)) {			//未授权
			$url = g('wxqy') -> get_oauth_url($_SESSION[SESSION_VISIT_CORP_ID], get_this_url());
			if ($lev != 1) {
				//非ajax请求才会跳转
				$m = get_var_value('m');
				if ($m != 'ajax' and !preg_match('/ajax_ident=1/', get_this_url())) {
					log_write('跳转到授权页面: url='.$url);
					header('location:' . $url);
					exit();
				}else {
					log_write('ajax请求，不跳转跳转到授权页面: url='.$url);
				}
			}
		//已授权，未记录到session
		} else {
			global $sc_app_id;
			global $app_id;
			log_write('开始获取员工信息: code='.$code);

			$cache_key = $this -> get_cache_key($code);
			$wxacct = g('redis') -> get($cache_key);

			if ($wxacct !== FALSE) {
				log_write('该授权code已被锁定，进入授权等待');
				$count = 0;
				while($wxacct == $cache_key and $count < 50) {
					log_write('休眠等待获取授权用户，已等待' . ($count + 1) . '秒');
					$wxacct = g('redis') -> get($cache_key);
					$count++;
					sleep(1);
				}
				log_write('等待完成，获得授权用户[' . $wxacct . ']');
			}else {
				//锁定授权code，持续60秒
				g('redis') -> setex($cache_key, $cache_key, 60);
				//$wxacct = $this -> get_visit_user_wxacct($code, $app_id, $sc_app_id);
				$wxinfo = $this->get_visit_user_wxinfo($code, $app_id, $sc_app_id);

				if(isset($wxinfo['parent_userid'])){
				    throw new SCException("程序员正在开发中~<br>如需查看通知历史，请先移步到学校通知的消息中查看");
                }elseif(isset($wxinfo['OpenId'])){
				    throw new SCException("无权限");
                }

                $wxacct = isset($wxinfo['UserId'])?$wxinfo['UserId']:'';

				if (!$wxacct) {
					g('redis') -> delete($cache_key);
				}else {
					g('redis') -> set($cache_key, $wxacct);
				}
			}
		}

		return $wxacct ? $wxacct : FALSE;
	}

    /**
     * 获取授权对应的微信账号
     *
     * @access private
     * @param integer $lev 放行等级
     * @return mixed
     */
    private function get_wxacct_auth_info($lev=3) {
        $wxinfo = NULL;
        if (isset($_SESSION[SESSION_VISIT_USER_WXINFO])) {
            $wxinfo = $_SESSION[SESSION_VISIT_USER_WXINFO];
            !is_array($wxinfo) && $wxinfo = json_decode($wxinfo,true);
            //判断是家长 / 内部成员
            if(isset($wxinfo['UserId'])){
                //内部人员
                $wxacct = $wxinfo['UserId'];
                $user = g('user') -> get_by_acct($_SESSION[SESSION_VISIT_DEPT_ID], $wxacct);
                if(empty($user)){
                    $wxinfo = '';
                }
            }elseif( isset($wxinfo['parent_userid']) ){
                //家长登录
                $parent_userid = $wxinfo['parent_userid'];
                $parent = g('ndb')->select(
                    'school_user_parents',
                    'id,parent_userid,relation,student_userid,mobile',
                    array('info_state='=>1,'com_id='=>$_SESSION[SESSION_VISIT_COM_ID],'parent_userid'=>$parent_userid),
                    1,1);
                if(empty($parent)){
                    $wxinfo = '';
                }
            }

        }else {
            //检测cookie中的user信息是否有变更，无变更直接使用，无需授权
            if ($_SESSION[SESSION_VISIT_CORP_URL] != 'e541ab0ecc9408b1f4d32d363c1e1c0b') {
                $wxinfo = $this -> check_user_auth_cache($_SESSION[SESSION_VISIT_CORP_URL]);
                $wxinfo and log_write('COOKIE缓存可用，无需授权，wxinfo=' . $wxinfo);
            }
        }

        if ($wxinfo) return $wxinfo;

        $code = isset($_GET['code']) ? $_GET['code'] : NULL;
        if (empty($code)) {			//未授权
            $url = g('wxqy') -> get_oauth_url($_SESSION[SESSION_VISIT_CORP_ID], get_this_url());
            if ($lev != 1) {
                //非ajax请求才会跳转
                $m = get_var_value('m');
                if ($m != 'ajax' and !preg_match('/ajax_ident=1/', get_this_url())) {
                    log_write('跳转到授权页面: url='.$url);
                    header('location:' . $url);
                    exit();
                }else {
                    log_write('ajax请求，不跳转跳转到授权页面: url='.$url);
                }
            }
            //已授权，未记录到session
        } else {
            global $sc_app_id;
            global $app_id;
            log_write('开始获取员工信息: code='.$code);

            $cache_key = $this -> get_cache_key($code);
            $wxinfo = g('redis') -> get($cache_key);

            if ($wxinfo !== FALSE) {
                log_write('该授权code已被锁定，进入授权等待');
                $count = 0;
                while($wxacct == $cache_key and $count < 50) {
                    log_write('休眠等待获取授权用户，已等待' . ($count + 1) . '秒');
                    $wxacct = g('redis') -> get($cache_key);
                    $count++;
                    sleep(1);
                }
                log_write('等待完成，获得授权用户[' . $wxacct . ']');
            }else {
                //锁定授权code，持续60秒
                g('redis') -> setex($cache_key, $cache_key, 60);
                $wxinfo = $this -> get_visit_user_wxinfo($code, $app_id, $sc_app_id);

                if(isset($wxinfo['OpenId'])){
                    throw new SCException("无权限");
                }

                if (!$wxinfo) {
                    g('redis') -> delete($cache_key);
                }else {
                    g('redis') -> set($cache_key, json_encode($wxinfo));
                }
            }
        }

        return $wxinfo ? $wxinfo : FALSE;
    }

	/**
	 * 检测用户缓存信息是否可用
	 *
	 * @access private
	 * @param string
	 * @return mixed
	 */
	private function check_user_cache($corp_url) {
		$cache_key = $this -> get_cookie_key($corp_url);
		$data = g('cookie') -> get_cookie($cache_key);
		if (!is_array($data) or empty($data)) {
			log_write('员工信息解密失败！，corp_url=' . $corp_url.',url:'.get_this_url());
			return FALSE;
		}

		$condition = array(
			'acct=' => $data['acct'],
			'tel=' => $data['tel'],
			'email=' => $data['email'],
			'weixin_id=' => $data['weixin_id'],
			'state=' => $data['state'],
			'state!=' => 0,
			'root_id=' => $_SESSION[SESSION_VISIT_DEPT_ID],
		);

		$check = g('ndb') -> record_exists($this -> user_table, $condition);
		if (!$check) {
			log_write('检测到员工信息有变更，缓存信息不可用！，corp_url=' . $corp_url);
			return FALSE;
		}
		return $data['acct'];
	}

    /**
     * 检测用户缓存信息是否可用
     *
     * @access private
     * @param string
     * @return mixed
     */
    private function check_user_auth_cache($corp_url) {
        $cache_key = $this -> get_cookie_key($corp_url);
        $wxinfo = g('cookie') -> get_cookie($cache_key);
        if (!is_array($wxinfo) or empty($wxinfo)) {
            log_write('员工信息解密失败！，corp_url=' . $corp_url.',url:'.get_this_url());
            return FALSE;
        }

        //通过信息判断是否为家长
        if( isset($wxinfo['UserId']) ){
            //内部人员登录
            $wxacct = $wxinfo['UserId'];
            $user = g('user') -> get_by_acct($_SESSION[SESSION_VISIT_DEPT_ID], $wxacct, '*', NULL, FALSE);
            if ($user) {
                return $wxinfo;
            }
        }elseif( isset($wxinfo['parent_userid']) ){
            //家长登录
            $parent_userid = $wxinfo['parent_userid'];
            $parent = g('ndb')->select(
                'school_user_parents',
                'id,parent_userid,relation,student_userid,mobile',
                array('info_state='=>1,'com_id='=>$_SESSION[SESSION_VISIT_COM_ID],'parent_userid'=>$parent_userid),
                1,1);
            if($parent){
                return $wxinfo;
            }

        }

        return false;
    }

	/**
	 * 保存用户缓存信息
	 *
	 * @access private
	 * @param string $corp_url 企业唯一标识
	 * @param array $info 用户信息集合
	 * @return void
	 */
	private function set_user_cache($corp_url, $info) {
		$data = array(
		    'UserId' => $info['acct'],
			'acct' => $info['acct'],
			'tel' => $info['tel'],
			'email' => $info['email'],
			'weixin_id' => $info['weixin_id'],
			'state' => $info['state'],
		);
		$cache_key = $this -> get_cookie_key($corp_url);
		g('cookie') -> set_cookie($cache_key, $data);
	}

    /**
     * 保存家长缓存信息
     *
     * @access private
     * @param string $corp_url 企业唯一标识
     * @param array $info 用户信息集合
     * @return void
     */
    private function set_parent_cache($corp_url, $info) {
        $data = array(
            'parent_userid' => $info['parent_userid'],
            'student_userid' => $info['student_userid'],
            'mobile' => $info['mobile'],
            'relation' => $info['relation'],
        );
        $cache_key = $this -> get_cookie_key($corp_url);
        g('cookie') -> set_cookie($cache_key, $data);
    }

	/**
	 * 获取用作cookie的缓存key
	 *
	 * @access public
	 * @param string
	 * @return string
	 */
	private function get_cookie_key($str='') {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' .__FUNCTION__ . ':cookie:' . $str;
		$key = md5($key_str);
		return $key;
	}
	
	/**
	 * 验证要访问的企业
	 */
	private function check_com() {
		$corp_url = get_var_get('corpurl', FALSE);
		
		if (is_null($corp_url) || empty($corp_url)) {							//未指定企业
			//1.尝试访问SESSION中的企业信息
			if (isset($_SESSION[SESSION_VISIT_CORP_URL])) {
                $this -> update_support_info($_SESSION[SESSION_VISIT_CORP_URL]);
				return TRUE;
			}
			
			//2.尝试访问最后次访问的企业信息（cookie中）
			$last_corp_url = g('cookie') -> get_cookie(COOKIE_VISIT_LAST_CORP_URL);
			if ($last_corp_url) {
				$this -> set_com_cookie_session($last_corp_url['corp_url']);
				return TRUE;
			}
			
			//3.提示未指定企业
			log_write('未指定要访问的企业号: '.get_this_url());

			$corp_tip = '未指定要访问的企业号';
			
			global $app;
			if ($app == 'entry') {
				$corp_tip = '授权已失效，请使用 "综合服务" 进行授权登录!';
			}

			throw new SCException($corp_tip);
		}
		
		//指定旧的企业信息
		if (isset($_SESSION[SESSION_VISIT_CORP_URL]) && strcmp($corp_url, $_SESSION[SESSION_VISIT_CORP_URL]) == 0) {
            $this -> update_support_info($_SESSION[SESSION_VISIT_CORP_URL]);
			return TRUE;
		} 

		$this -> unset_session_info();	
		//访问另一个企业号信息
		//1.尝试访问已保存在cookie中
		$cookie = g('cookie') -> get_cookie($corp_url);
		if ($cookie && isset($cookie[SESSION_VISIT_COM_ID]) && $cookie[SESSION_VISIT_COM_ID] != 724) {
			$this -> set_com_cookie_session($corp_url, TRUE);
			$this -> build_session($corp_url);
			//技术支持信息
			$support_info = $this -> get_support_info($corp_url);
			$_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
			$_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
			return TRUE;
		}
		
		//2.通过微信授权
		log_write('开始验证企业url: url='.$corp_url);
		
		if (is_null($corp_url) || empty($corp_url)) {
			throw new SCException('未指定要访问的企业号。');
		}
		
		$this -> set_com_cookie_session($corp_url);
		return TRUE;
	}
	
	/**
	 * 根据corp url保存企业信息cookie和session
	 * @param unknown_type $corp_url
	 * @throws SCException
	 */
	private function set_com_cookie_session($corp_url, $only_last=FALSE) {
		//保存最后一次访问的corp url
		g('cookie') -> set_cookie(COOKIE_VISIT_LAST_CORP_URL, array('corp_url' => $corp_url));

		if ($only_last) {
			return;
		}

		$com = g('company') -> get_by_corp_url($corp_url);
		if (!$com) {
			throw new SCException('系统 繁忙');
		}	
		if ($com['state'] == 3) {
			throw new SCException('企业号应用过期');
		}
		
		$this -> build_com_cookie($com);
		
		//技术支持信息
		$support_info = $this -> get_support_info($corp_url, $com['agt_id']);
		$_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
		$_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
		
		log_write('验证成功：url='.$corp_url.', com='.$_SESSION[SESSION_VISIT_COM_ID]);
	}

    /**
     * 更新技术支持信息
     * @param $corp_url
     */
	private function update_support_info($corp_url) {
        $com = g('company') -> get_by_corp_url($corp_url);
        $support_info = $this -> get_support_info($corp_url, $com['agt_id']);
        $_SESSION[SESSION_VISIT_POWERBY] = $support_info['info'];
        $_SESSION[SESSION_VISIT_POWERBY_URL] = $support_info['url'];
    }
	
	/**
	 * 获取技术支持信息
	 * @param unknown_type $corp_url
	 */
	private function get_support_info($corp_url, $agt_id=NULL) {
		if (is_null($agt_id)) {
			$com = g('company') -> get_by_corp_url($corp_url);
			if (!$com) {
				throw new SCException('指定的企业号不存在或被禁用');
			}	
			if ($com['state'] == 3) {
				throw new SCException('企业号应用过期');
			}
			$agt_id = $com['agt_id'];
		}
		//技术支持信息
		$support_info = g('agency_info') -> get_support_info($agt_id);
		return $support_info;
	}

	/** 验证企业状态（禁用等） */
	private function check_com_state() {
        if (!isset($_SESSION[SESSION_VISIT_CORP_URL]))      return;

        $com = g('company') -> get_by_corp_url($_SESSION[SESSION_VISIT_CORP_URL], 'state');
        if ($com['state'] == 0) {
            throw new SCException('系统 繁忙');
        }
    }

	/**
	 * 验证app有效性和权限
	 * @param unknown_type $app_name
	 */
	private function check_app() {
		global $app;
		
		if (empty($app)) {
//			cls_resp::show_err_page(array('未指定要访问的应用'));
			return TRUE;	//允许没有APP
		}
		
		if (!file_exists(SYSTEM_APPS.$app)) {
			throw new SCException('应用不存在: '.$app);
		}
		
		$app_obj = g('app') -> get_by_name($app);
		if (!$app_obj) {
			throw new SCException('应用不存在: '.$app);
		}
		
		$com_app = g('com_app') -> get_by_app($_SESSION[SESSION_VISIT_COM_ID], $app_obj['id']);
		if (!$com_app || $com_app['state'] == 0) {
			throw new SCException('企业未开启该应用: '.$app);
		}
		
		global $app_id;
		global $sc_app_id;
		global $suite_id;
		$app_id = $com_app['wx_app_id'];
		$sc_app_id = $com_app['app_id'];
		$suite_id = $com_app['sie_id'];
	}
	
	/**
	 * 验证页面访问或请求
	 */
	private function check_visit() {
		global $app;
		global $module;
		global $action;
		global $mod_cls;
		
		if (empty($app)) {
			$mod_file = SYSTEM_MODS.'cls_'.$module.'.php';
		} else {
			$mod_file = SYSTEM_APPS.$app.'/modules/cls_'.$module.'.php';
		}
	
		if (!file_exists($mod_file)) {
			throw new SCException('页面不存在');
		}
	
		include_once($mod_file);
		if (!class_exists($mod_cls) || !method_exists($mod_cls, $action)) {
			throw new SCException('页面不存在');
		}
	}
	
	/**
	 * 清除session中的企业信息
	 */
	private function unset_session_info() {
		session_unset();
		session_destroy();
		session_start();
	}
	
	/**
	 * 清除对应企业的COOKIE信息
	 */
	private function unset_cookie_info() {
		if (isset($_SESSION[SESSION_VISIT_CORP_URL])) {
			g('cookie') -> clean_cookie($_SESSION[SESSION_VISIT_CORP_URL]);
		}
	}
	
	/**
	 * 保存当前访问员工所属企业的企业信息到 cookie和session
	 * @param unknown_type $com_info
	 */
	private function build_com_cookie($com_info) {
		$cookie = array(
			//企业信息
			SESSION_VISIT_CORP_URL => $com_info['corp_url'],
			SESSION_VISIT_COM_ID => $com_info['id'],
			SESSION_VISIT_DEPT_ID => $com_info['dept_id'],
			SESSION_VISIT_CORP_ID => $com_info['corp_id'],
			SESSION_VISIT_CORP_SECRET => $com_info['corp_secret'],
			SESSION_VISIT_AUTH_CODE => $com_info['permanent_code'],
		);
		$ret = g('cookie') -> set_cookie($com_info['corp_url'], $cookie);
		
		//保存企业信息到session
		foreach ($cookie as $key => $val) {
			$_SESSION[$key] = $val;
		}
		unset($val);
		
		if (!$ret) {
			log_write('保存员工cookie失败');
			throw new SCException('系统繁忙');
		}
	}
	
	/**
	 * 增加user信息到cookie，session
	 * @param unknown_type $corp_url
	 * @param unknown_type $user_info
	 * @param unknown_type $detp_id
	 */
	private function append_user_cookie($corp_url, $user_info, $detp_id,$wxinfo=[]) {
		//员工信息
		$user_cookie = array(
			SESSION_VISIT_USER_DEPT => $detp_id,										
			SESSION_VISIT_USER_ID => $user_info['id'],										//记录当前员工ID
			SESSION_VISIT_USER_NAME => $user_info['name'],									//记录当前员工姓名
			SESSION_VISIT_USER_WXID => $user_info['weixin_id'],								//记录当前员工微信ID
			SESSION_VISIT_USER_WXACCT => $user_info['acct'],								//当前员工账号
			SESSION_VISIT_USER_PIC => $user_info['pic_url'],								//当前员工头像
			SESSION_VISIT_IP => get_ip(),
            SESSION_VISIT_USER_WXINFO => $wxinfo
		);
		
		//保存企业信息到session
		foreach ($user_cookie as $key => $val) {
			$_SESSION[$key] = $val;
		}
		unset($val);
		
		$ret = g('cookie') -> set_cookie($corp_url, $_SESSION);
		if (!$ret) {
			log_write('保存员工cookie失败');
			throw new SCException('系统繁忙');
		}

		//log_write('主部门id='.$detp_id);								
		//log_write('用户信息: '.json_encode($user_info));
		$this -> set_user_cache($corp_url, $user_info);
	}

    /**
     * 增加parent信息到cookie，session
     * @param unknown_type $corp_url
     * @param unknown_type $user_info
     * @param unknown_type $detp_id
     */
    private function append_parent_cookie($corp_url, $parent,$wxinfo=[]) {
        //员工信息
        $user_cookie = array(
            SESSION_VISIT_USER_ID => $parent['id'],										//记录当前员工ID
            SESSION_VISIT_USER_WXID => $parent['parent_userid'],								//记录当前员工微信ID
            SESSION_VISIT_USER_WXACCT => $parent['parent_userid'],								//当前员工账号
            SESSION_VISIT_IP => get_ip(),
            SESSION_VISIT_USER_WXINFO => $wxinfo
        );

        //保存企业信息到session
        foreach ($user_cookie as $key => $val) {
            $_SESSION[$key] = $val;
        }
        unset($val);

        $ret = g('cookie') -> set_cookie($corp_url, $_SESSION);
        if (!$ret) {
            log_write('保存员工cookie失败');
            throw new SCException('系统繁忙');
        }

        $this -> set_parent_cache($corp_url, $parent);
    }
	
	/**
	 * 根据cookie中的内容，保存session
	 * @param unknown_type $corp_url
	 */
	private function build_session($corp_url) {
		$cookie = g('cookie') -> get_cookie($corp_url);
		if ($cookie) {
			foreach ($cookie as $key => $val) {
				if ($key == SESSION_VISIT_USER_WXACCT)	continue;	//不保存到session，以便重新获取员工信息
				$_SESSION[$key] = $val;
			}
			unset($val);
		}
	}
	
	/**
	 * 获取用户的企业号账号
	 * @param unknown_type $code		获取员工信息的标识
	 * @param unknown_type $agent_id	企业号中应用的ID
	 */
	private function get_visit_user_wxacct($code, $agent_id=0, $app_id) {
		try {
			$token = g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $app_id);
			return g('wxqy') -> get_oauth_user_id($token, $code, $agent_id);

		} catch (SCException $e) {
			throw $e;
		}
	}

    /**
     * 获取用户账号信息
     * @param unknown_type $code		获取员工信息的标识
     * @param unknown_type $agent_id	企业号中应用的ID
     */
    private function get_visit_user_wxinfo($code, $agent_id=0, $app_id) {
        try {
            $token = g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $app_id);
            return g('wxqy') -> get_oauth_info($token, $code, $agent_id);

        } catch (SCException $e) {
            throw $e;
        }
    }

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}
}

// end of file