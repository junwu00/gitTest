<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 * 
 * 发起时：step=1, proc_record表state=0（审批中）
 * 同意时：step=1, proc_record表state=1 (同意)
 * 驳回时：step=1, proc_record表state=2 (驳回)
 * 撤销时: step=1, proc_record表state=3 (撤销)
 * 催办时：step=x, proc_record表state=x （步骤、状态不变更，只记录到proc_log）
 */
class cls_mv_formsetinst_name {
	/** 对应的库表名称 */
	private static $Table = 'mv_proc_formsetinst_name';
	
	/** 删除状态 */
	private static $StateOff = 0;
	/** 正常状态 */
	private static $StateOn = 1;
	
	/**
	 * 获取表单实例名称格式
	 * @param int $com_id 企业id
	 * @param int $is_other_proc 流程模板0自定义流程 1请假 2报销
	 */
	public function get_recode_by_id($com_id,$is_other_proc=0, $app_version=0){
		$table_name = self::$Table;
		$stateOn = self::$StateOn;
		$sql = <<<EOF
SELECT * from {$table_name} 
where com_id = {$com_id} 
	and is_other_proc = {$is_other_proc} 
	AND app_version = {$app_version} 
	AND info_state={$stateOn} 
EOF;
		$data = g('db') -> select($sql);
		return empty($data) ? false : $data[0];
	}
	
	//---------------------------------------内部实现

}

// end of file