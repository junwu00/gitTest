<?php
/**
 * 民主投票数据模型
 *
 * @author LiangJianMing
 * @create 2015-1-2
 */
class cls_vote {
	//公司id
	private $com_id 	= NULL;
	//用户id
	private $user_id 	= NULL;
	//基本数据库操作条件
	private $base_con	= NULL;

	//投票活动配置表
	public $conf_table 	 = 'vote_conf';
	//投票与部门id的映射关系表
	public $map_table 	 = 'vote_dept_map';
	//投票记录表
	public $record_table = 'vote_record';
	//用户表
	public $user_table = 'sc_user';
	//用户表
	public $group_table = 'sc_group';

	//定义特定投票类型只能拥有固定选项
    public $extra = array(
    	'agree' => array(
			'agree_1' => '赞成',
			'agree_2' => '反对',
    	),
    	'star' => array(
			'star_1' => '1星',
			'star_2' => '2星',
			'star_3' => '3星',
			'star_4' => '4星',
			'star_5' => '5星',
		),
    );

    //定义固有的字段key的中文描述
    public $fields_map = array(
    	'agree_1' 	=> '赞成',
    	'agree_2' 	=> '反对',
    	'star_1'	=> '1星',
    	'star_2'	=> '2星',
    	'star_3'	=> '3星',
    	'star_4'	=> '4星',
    	'star_5'	=> '5星',
    	'star_avg' 	=> '平均分',
    );

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id  = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
			'user_id=' => $this -> user_id,
		);
	}

	/**
	 * 获取单个投票活动信息
	 *
	 * @access public
	 * @param integer $vote_id
	 * @return array
	 */
	public function get_info($vote_id) {
		$fields = array(
			'vote_id', 'type', 'img_type', 'field_name',
			'field_val', 'title', 'description', 'is_single',
			'is_real', 'view_type', 'total', 'status',
			'begin_time', 'end_time', 'create_time', 'maintain_time',
			'is_complete', 'max_select', 'max_type'
		);
		$fields = implode(',', $fields);
		$condition = array(
			'com_id=' => $this -> com_id,
			'vote_id=' => $vote_id,
		);

		$info = g('ndb') -> select($this -> conf_table, $fields, $condition);
		if (!$info) {
			throw new SCException('获取投票内容失败');
		}
		$info = current($info);

		//判断用户是否已经参与该投票
		$info['attended'] = $this -> check_attended($vote_id);
		//是否返回投票结果
		$with_result = $info['attended'];
		$is_complete = $info['is_complete'];

		$info['status'] == 3 && $with_result = TRUE;

		//是否返回投票者列表
		$with_user = FALSE;
		if ($info['is_real'] == 1) {
			if ($info['view_type'] == 3) {
				$with_user = TRUE;
			}elseif ($info['view_type'] == 2 && $info['status'] == 3) {
				$with_user = TRUE;
			}elseif ($info['view_type'] == 1 && $with_result) {
				$with_user = TRUE;
			}
		}

		if (!$with_user) {
			$user_index = '';
			if ($info['is_real'] == 0) {
				$user_index = 'none';
			}else {
				if ($info['view_type'] == 1 && !$with_result) {
					$user_index = 'after';
				}elseif ($info['view_type'] == 2 && $info['end_time'] > time()) {
					$user_index = 'end';
				}
			}
			$info['user_index'] = $user_index;
		}

		$field_name = json_decode($info['field_name'], TRUE);
		$field_name = empty($field_name) ? array() : $field_name;

		if ($info['type'] == 2 && $info['img_type'] != 3) {
			!empty($field_name) && $info['img_show'] = MEDIA_URL_PREFFIX . $field_name[0]['img_url'];
			!empty($field_name) && $info['img_name'] = $field_name[0]['name'];
			if ($info['img_type'] == 1) {
				$field_name = $this -> extra['agree'];
			}elseif ($info['img_type'] == 2) {
				$field_name = $this -> extra['star'];
			}
		}

		$info['field_name'] = $field_name;

		if (!$with_result && !$with_user) {
			unset($info['field_val']);
			return $info;
		}

		$field_val = json_decode($info['field_val'], TRUE);
		$info['field_val'] = empty($field_val) ? array() : $field_val;

		$info['my_vote'] = array();

		$my_vote_info = $this -> get_my_vote_info($vote_id);
		$my_vote_info && $info['my_vote'] = $my_vote_info['field_val'];

		// gch add
		// 支持多选投票可以不选任何选项即可投票
		// empty($info['my_vote']) && $info['my_vote'] = array(0);

		$maintain_time = $info['maintain_time'];

		if ($with_result) {
			//维护时间小于当前用户投票时间
			if ($maintain_time < $my_vote_info['create_time']) {
				foreach ($my_vote_info['field_val'] as $tval) {
					!isset($info['field_val'][$tval]) && $info['field_val'][$tval] = 0;
					$info['field_val'][$tval]++;
				}
				unset($tval);
				$info['total']++;
			}
		}else {
			unset($info['field_val']);
		}

		if ($with_user) {
			$info['user_list'] = array();
			
			$attendant = $this -> get_attendant($vote_id, $maintain_time);
			if (!empty($attendant)) {
				foreach ($attendant as $val) {
					$field_val = json_decode($val['field_val'], TRUE);
					$tmp_arr = array(
						'name' 			=> $val['name'],
						'head_img' 		=> $val['head_img'],
						'field_val' 	=> $field_val,
						'create_time' 	=> $val['create_time'],
					);
					
					$info['user_list'][] = $tmp_arr;
				}
				unset($val);
			}
		}

		/**
		 * 投票结果整理
		 */
		if (isset($info['field_val'])) {
			$info['vote_result'] = $this -> get_html_result($info, FALSE);
			unset($info['field_val']);
		}

		return $info;
	}

	/**
	 * 获取自身对某个投票的投票选择
	 *
	 * @access public
	 * @param integer $vote_id 投票活动id
	 * @return array
	 */
	public function get_my_vote_info($vote_id) {
		$sql = <<<EOF
SELECT field_val, create_time FROM {$this -> record_table} 
WHERE vote_id={$vote_id} AND com_id={$this -> com_id} AND user_id={$this -> user_id} 
LIMIT 1
EOF;
		$ret = g('db') -> select_one($sql);
		$ret && $ret['field_val'] = json_decode($ret['field_val'], TRUE);
		empty($ret) && $ret = array();
		return is_array($ret) ? $ret : FALSE;
	}

	/**
	 * 获取该投票还未维护的结果
	 *
	 * @access public
	 * @param integer $vote_id 投票活动id
	 * @param integer $time 时间条件
	 * @return array
	 */
	public function get_replenish($vote_id, $time) {
		$fields = array(
			'field_val',
		);
		$condition = array(
			'com_id=' => $this -> com_id,
			'vote_id=' => $vote_id,
			'create_time>' => $time,
		);
		$result = g('ndb') -> select($this -> conf_table, $fields, $condition);
		$ret = array(
			'field_val' => array(),
			'count' => 0,
		);

		if ($result) {
			$ret['count'] += count($result);
			foreach ($result as $val) {
				$field_val = json_decode($val['field_val'], TRUE);
				if (!empty($field_val)) foreach ($field_val as $cval) {
					!isset($ret['field_val'][$cval]) && $ret['field_val'][$cval] = 0;
					$ret['field_val'][$cval] += 1;
				}
				unset($cval);
			}
			unset($val);
		}
		return $result;
	}

	/**
	 * 获取投票参与者
	 *
	 * @access public
	 * @param integer $vote_id 投票活动id
	 * @param integer $maintain_time 投票活动最近一次自动维护时间
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @return array
	 */
	public function get_attendant($vote_id, $maintain_time, $page = 1, $page_size = 10) {
		$page < 1 && $page = 1;
		$page_size < 1 && $page_size = 10;
		empty($maintain_time) && $maintain_time = time();

		$str = __FUNCTION__ . ':vote_id:' . $vote_id . ':page:' . $page . ':page_size:' . $page_size;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!$ret) {
			$table = "`{$this -> record_table}` a, `{$this -> user_table}` b";
			$fields = array(
				'a.field_val', 'a.create_time', 'b.id', 'b.name',
				'b.pic_url as head_img',
			);
			$fields = implode(',', $fields);
			$condition = array(
				'a.com_id=' => $this -> com_id,
				'a.vote_id=' => $vote_id,
				'a.create_time<=' => $maintain_time,
				'^a.user_id=' => 'b.id',
			);
			$order = ' ORDER BY a.id ASC';

			$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order);
			if ($ret && count($ret) == $page_size) {
				g('redis') -> set($cache_key, json_encode($ret), 3600);
			}
		}else {
			$ret = json_decode($ret, TRUE);
		}

		//做缓存的同时，排除本身的记录
		foreach ($ret as $key => $val) {
			if ($val['id'] == $this -> user_id) {
				unset($ret[$key]);
				BREAK;
			}
		}

		return $ret;
	}

	/**
	 * 判断用户是否已经参与过投票
	 *
	 * @access public
	 * @param integer $vote_id 投票活动id
	 * @return boolean
	 */
	public function check_attended($vote_id) {
		$condition = $this -> base_con;
		$condition['vote_id='] = $vote_id;
		$check = g('ndb') -> record_exists($this -> record_table, $condition);
		return $check;
	}

	/**
	 * 判断用户是否能够查看该投票
	 *
	 * @access public
	 * @param integer $vote_id
	 * @return boolean
	 */
	public function check_visible($vote_id) {
		$depts = $this -> get_user_dept();
		$groups = $this -> get_user_group();
		if (empty($depts) && empty($groups)) {
			return FALSE;
		}

		$depts = empty($depts) ? 0 : implode(',', $depts);
		$groups = empty($groups) ? 0 : implode(',', $groups);
		$sql = <<<EOF
SELECT COUNT(vote_id) 
FROM `{$this -> map_table}` 
WHERE com_id={$this -> com_id} AND vote_id={$vote_id} AND dept_id IN ({$depts}) OR group_id IN ({$groups})
EOF;

		$count = g('db') -> select_first_val($sql);
		return empty($count) ? FALSE : TRUE;
	}

	/**
	 * 参与投票
	 *
	 * @access public
	 * @param array $data 投票数据集合
	 * @return integer
	 */
	public function attend(array $data) {
		$time = time();
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data['create_time'] = $time;

		$result = g('ndb') -> insert($this -> record_table, $data);
		if (!$result) throw new SCException('写入数据库失败');
		return $result;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取投票列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param string $order 排序方式
	 * @param boolean $with_result 是否要返回投票结果
	 * @return array
	 */
	public function get_list($condition, $page = 1, $page_size = 10, $order = '', $with_result = FALSE) {
		$page < 1 && $page = 1;
		$page_size < 1 && $page_size = 10;

		$str = json_encode($condition) . ':' . $page . ':' . $page_size . ':' . $order;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (empty($ret)) {
			$fields = array(
				'DISTINCT b.vote_id', 'b.type', 'b.img_type', 'b.field_name',
				'b.field_val', 'b.field_percent', 'b.title', 'b.description', 'b.is_single',
				'b.is_real', 'b.view_type', 'b.total', 'b.`status`',
				'b.begin_time', 'b.end_time', 'b.create_time', 'c.id AS attend',
			);
			$fields = implode(',', $fields);

			$depts = $this -> get_user_dept();
			$groups = $this -> get_user_group();
			if (empty($depts) && empty($groups)) {
				throw new SCException("获取用户所在部门或组别失败");
			}

			$depts = empty($depts) ? 0 : implode(',', $depts);
			$groups = empty($groups) ? 0 : implode(',', $groups);

			empty($order) && $order = ' ORDER BY b.vote_id DESC';

			$begin = ($page - 1) * $page_size;
			$limit = ' LIMIT ' . intval($begin) . ', ' . intval($page_size);

			$and = '';
			foreach ($condition as $key => $val) {
				$and .= ' AND b.' . $key . '\'' . mysql_escape_string($val) . '\'';
			}
			unset($val);

			$sql = <<<EOF
SELECT {$fields} 
FROM `{$this -> map_table}` a 
INNER JOIN `{$this -> conf_table}` b ON a.com_id=b.com_id AND a.vote_id=b.vote_id {$and} 
LEFT JOIN `{$this -> record_table}` c ON b.com_id=c.com_id AND b.vote_id=c.vote_id AND c.user_id={$this -> user_id}
WHERE a.com_id={$this -> com_id} AND a.dept_id IN ({$depts}) OR a.group_id IN ({$groups}) 
{$order} 
{$limit} 
EOF;
			$count_sql = <<<EOF
SELECT COUNT(1) 
FROM `{$this -> map_table}` a 
INNER JOIN `{$this -> conf_table}` b ON a.com_id=b.com_id AND a.vote_id=b.vote_id {$and} 
WHERE a.com_id={$this -> com_id} AND a.dept_id IN ({$depts}) OR a.group_id IN ({$groups}) 
EOF;

			$result = g('db') -> select($sql);
			//$count = g('db') -> select_first_val($count_sql);

			$ret = array(
				'data' => array(),
				'total' => 0,
			);

			if ($result) {
				if ($with_result) {
					foreach ($result as &$val) {
						$val['attend'] = empty($val['attend']) ? 0 : 1;
						$val['vote_result'] = $this -> get_html_result($val, TRUE);
						unset($val['field_name']);
						unset($val['field_val']);
					}
					unset($val);
				}else {
					foreach ($result as &$val) {
						$val['attend'] = empty($val['attend']) ? 0 : 1;
						$val['html_begin_time'] = html_time_format($val['begin_time']);
						unset($val['begin_time']);
					}
					unset($val);
				}

				$ret['data'] = $result;
			}

			//$count && $ret['total'] = $count;

			//保存缓存结果10秒
			g('redis') -> set($key, json_encode($ret), 10);
		}else {
			$ret = json_decode($ret, TRUE);
		}
		
		return $ret;
	}

	/**
	 * 获取当前用户的所有归属部门id集合
	 *
	 * @access public
	 * @return array
	 */
	public function get_user_dept() {
		$str = __FUNCTION__.':user_id:'.$this -> user_id;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$sql = <<<EOF
SELECT `dept_tree` 
FROM {$this -> user_table} 
WHERE id={$this -> user_id} 
LIMIT 1 
EOF;
		$result = g('db') -> select_first_val($sql);
		$result = json_decode($result, TRUE);
		$dept_arr = array();
		if (is_array($result) && !empty($result)) {
			foreach ($result as $val) {
				foreach ($val as $cval) {
					$dept_arr[] = intval($cval);
				}
				unset($cval);
			}
			unset($val);
		}
		$dept_arr = array_unique($dept_arr);

		//用户部门信息缓存30秒
		g('redis') -> set($cache_key, json_encode($dept_arr), 30);

		return $dept_arr;
	}

	// gch add
	/**
	 * 获取当前用户的所有归属组别id集合
	 *
	 * @access private
	 * @return array
	 */
	private function get_user_group() {
		$cond = array(
			'com_id=' => $this -> com_id,
			'info_state=' => 1,
			'user_list LIKE ' => '%"'. $this -> user_id .'"%',
		);
	
		$ret = g('ndb') -> select($this -> group_table, '`id`', $cond);
		!is_array($ret) and $ret = array();

		$return = array();
		foreach ($ret as $value) {
			$return[] = $value['id'];
		}

		return $return;
		
	}

	/**
	 * 将投票结果整理为适合前端输出的数据
	 *
	 * @access public
	 * @param array $info 单个投票活动的数据信息集合
	 * @param boolean $get_max 是否只获取选择最多的选项：TRUE-是，FALSE-否
	 * @return array
	 */
	public function get_html_result(array $info, $get_max = FALSE) {
		$ret = array();

		$img_map = array();
		if ($info['type'] == 2 && $info['img_type'] == 2) {
			$field_name = $this -> extra['star'];
		}elseif ($info['type'] == 2 && $info['img_type'] == 1) {
			$field_name = $this -> extra['agree'];
		}else {
			$field_name = is_array($info['field_name']) ? $info['field_name'] : json_decode($info['field_name'], TRUE);
			$tmp = array();
			$field = $info['type'] == 1 ? 'text' : 'name';	//文字投票的选项名为text，图片投票的选项名为name
			foreach ($field_name as $f) {
				$tmp[$f['key']] = $f[$field];
				$info['type'] == 2 && $img_map[$f['key']] = $f['img_url'];
			}unset($f);
			$field_name = $tmp;
			unset($tmp); 
		}

		if ($get_max) {
			$max = array(
				'key' 	=> '',
				'name' 	=> '暂无投票',
				'value' => 0,
			);
			$field_percent = is_array($info['field_percent']) ? $info['field_percent'] : json_decode($info['field_percent'], TRUE);

			if (!is_array($field_percent)) return $max;
			foreach ($field_percent as $key => $val) {
				$max['key'] = $key;
				$max['value'] = $val;
				$max['name'] = $field_name[$key];
				isset($img_map[$key]) && $max['img_url'] = $img_map[$key];
				BREAK;
			}
			unset($val);

			return $max;
		}

		$field_val = is_array($info['field_val']) ? $info['field_val'] : json_decode($info['field_val'], TRUE);

		$length = count($field_name);

//------------------------单选，所有选项的比例和为100%-----------------------
		if ($info['is_single'] == 1) {
			$i = 0;
			$per_total = 0;
			foreach ($field_name as $key => $val) {
				$count = isset($field_val[$key]) ? intval($field_val[$key]) : 0;

				if ($i == $length - 1) {
					//必须有投票才可用
					if ($info['total'] > 0) $value = round(100 - $per_total, 1);
				}else {
					$value = 0;
					!empty($info['total']) && $value = round(floatval($count) / floatval($info['total']), 3) * 100;
				}
				preg_match('/\.0$/', $value) && $value = intval($value);

				$tmp_arr = array(
					'key'	=> $key,
					'name' 	=> $val,
					'count' => $count,
					'value' => $value,
				);
				isset($img_map[$key]) && $tmp_arr['img_url'] = $img_map[$key];

				(!isset($max['count']) || $max['count'] < $count) && $max = $tmp_arr;
				!$get_max && $ret[] = $tmp_arr;

				$i++;
				$per_total += $value;

				//最大只能为100
				$per_total > 100 and $per_total = 100;
			}
			unset($val);
//------------------------多选，每一个选项的比例独立-----------------------
		}else {
			foreach ($field_name as $key => $val) {
				$count = isset($field_val[$key]) ? intval($field_val[$key]) : 0;
				
				$value = 0;
				!empty($info['total']) && $value = round(floatval($count) / floatval($info['total']), 3) * 100;
				
				preg_match('/\.0$/', $value) && $value = intval($value);

				$tmp_arr = array(
					'key'	=> $key,
					'name' 	=> $val,
					'count' => $count,
					'value' => $value,
				);
				isset($img_map[$key]) && $tmp_arr['img_url'] = $img_map[$key];

				(!isset($max['count']) || $max['count'] < $count) && $max = $tmp_arr;
				!$get_max && $ret[] = $tmp_arr;
			}
			unset($val);
		}
		
		if ($info['type'] == 2 && $info['img_type'] == 2) {
			$total_star = 0;
			foreach ($ret as $val) {
				$total_star += $val['count'] * intval(str_replace('star_', '', $val['key']));
			}
			unset($val);

			$avg = 0;
			!empty($info['total']) && $avg = round(floatval($total_star) / floatval($info['total']), 1);
			preg_match('/\.0$/', $avg) && $avg = intval($avg);
			$ret['star_avg'] = array(
				'name' => $this -> fields_map['star_avg'],
				'value' => $avg,
			);
			unset($val);
		}

		return $ret;
	}

	/**
	 * 检测用户是否已经参加了投票
	 *
	 * @access public
	 * @param integer $user_id 用户id
	 * @param array $vote_ids 投票id集合
	 * @return array
	 */
	public function check_voted($user_id = 0, $vote_ids) {
		empty($user_id) && $user_id = $this -> user_id;
		$ids_str = implode(',', $vote_ids);

		$ret = array();

		$fields = 'id';
		$condition = array(
			'user_id=' => $user_id,
			'vote_id IN ' => '('.$ids_str.')',
		);
		$result = g('ndb') -> select($this -> record_table, $fields, $condition);

		if (!$result) return $ret;

		foreach ($result as $val) {
			$ret[$val['vote_id']] = 1;
		}
		unset($val);
		return $ret;
	}
	
	/**
	 * 根据ID获取投票配置信息
	 * @param unknown_type $vote_id	投票配置ID
	 * @param unknown_type $fileds	查找的字段
	 */
	public function get_conf_by_id($vote_id, $fields='*') {
		if (is_array($vote_id)) {
			$cond = array(
				'vote_id IN' => '(' . implode(',', $vote_id) . ')',
				'com_id=' => $this -> com_id,
			);
		} else {
			$cond = array(
				'vote_id=' => $vote_id,
				'com_id=' => $this -> com_id,
			);
		}
		$ret = g('ndb') -> select($this -> conf_table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 查询自身投票项
	 * @param array $vote_ids	投票记录ID
	 */
	public function list_self_vote_result(array $vote_ids) {
		if (empty($vote_ids))	return array();
		
		$vote_ids = intval_array($vote_ids);
		$vote_ids_str = implode(',', $vote_ids);
		
		$sql = <<<EOF
			SELECT r.field_val AS self_val, c.field_name, c.vote_id, c.type, c.img_type
			FROM vote_conf c LEFT JOIN vote_record r
			ON (c.vote_id = r.vote_id AND r.user_id={$this -> user_id})
			WHERE c.com_id={$this -> com_id}
				AND c.vote_id IN ({$vote_ids_str})	
EOF;
		$ret = g('db') -> select($sql);
		log_write($sql);
		return $ret ? $ret : array();
	}
	
}

/* End of this file */