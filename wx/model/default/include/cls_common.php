<?php
/**
 * 公用的基础类
 *
 * @author Liang JianMing
 * @create 2015-05-27
 * @version $Id: cls_common.php 14136 2016-01-29 11:18:42Z zhangp $
 * @lastChangedBy $Author: zhangp $
 * @lastChangedDate $Date: 2016-01-29 19:18:42 +0800 (Fri, 29 Jan 2016) $
 */
class cls_common {
    //公司信息表
    public $company_table = 'sc_company';

    /**
     * 判断是否微信客户端打开
     *
     * @access public
     * @return boolean
     */
    public function is_wx() {    	
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') === FALSE) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * 判断手机平台
     *
     * @access public
     * @return integer 取值：0-其他、1-ios、2-android、3-wp
     */
    public function get_mobile_platid() {
        $agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(iPod|iPad|iPhone)/i', $agent)) {
            return 1;
        }

        if (preg_match('/android/i', $agent)) {
            return 2;
        }

        if (preg_match('/WP/', $agent)) {
            return 3;
        }

        return 0;
    }

    /**
     * 获取该企业的access_token
     *
     * @access public
     * @param integer $combo 套件号
     * @param integer $com_id 企业id
     * @return string
     */
    public function get_access_token($combo, $com_id=0) {
        $info = $this -> get_com_info($com_id);

        $token = '';
        try{
            $token = g('atoken') -> get_access_token($combo, $info['corp_id'], $info['corp_secret'], $info['permanent_code'], $info['permanent_suite_id']);
        }catch(SCException $e) {
        }
        return $token;
    }

    /**
     * 获取企业的信息
     *
     * @access public
     * @param integer $com_id 企业id
     * @return array
     */
    public function get_com_info($com_id=0) {
        empty($com_id) and $com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $key_str = __FUNCTION__ . ':com_id:' . $com_id;
        $cache_key = $this -> get_cache_key($key_str);
        $cache = g('redis') -> get($cache_key);

        if (empty($cache)) {
            $sql = <<<EOF
SELECT corp_id, corp_secret, permanent_code, corp_url, permanent_code, permanent_suite_id 
FROM {$this -> company_table} 
WHERE id={$com_id} 
LIMIT 1
EOF;
            $com_info = g('db') -> select_one($sql);
            !empty($com_info) and g('redis') -> setex($cache_key, json_encode($com_info), 1800);
        }else {
            $com_info = json_decode($cache, TRUE);
        }

        return $com_info;
    }

    /**
     * 获取企业的根部门id
     *
     * @access public
     * @param mixed $com_ids 企业集合
     * @return mixed
     * @throws SCException
     */
    public function get_root_id($com_ids) {
        if (empty($com_ids)) {
            throw new SCException(__FUNCTION__ . ": 企业id为空");
        }
        !is_array($com_ids) and $com_ids = array($com_ids);

        $key_str = __FUNCTION__ . ':' . json_encode($com_ids);
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('redis') -> get($cache_key);

        if (!empty($ret)) {
            $ret = json_decode($ret, TRUE);
            return $ret;
        }

        $fields = array(
            'id AS com_id', 'dept_id AS root_id'
        );
        $fields = implode(',', $fields);

        $condition = array(
            'id IN ' => '(' . implode(',', $com_ids) . ')',
        );

        $ret = g('ndb') -> select('sc_company', $fields, $condition);
        if (!$ret) {
            throw new SCException(__FUNCTION__ . ": 查询结果为空");
        }

        if (count($com_ids) === 1) {
            $ret = array_shift($ret);
            $ret = $ret['root_id'];
        }

        g('redis') -> setex($cache_key, $ret, json_encode($ret));

        return $ret;
    }
    
    /**
     * 获取缓存变量名
     *
     * @access public
     * @param string $str 关键字符
     * @return string
     */
    public function get_cache_key($str = '') {
        $key_str = SYSTEM_COOKIE_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':'.$str;
        $key = md5($key_str);
        return $key;
    }
}