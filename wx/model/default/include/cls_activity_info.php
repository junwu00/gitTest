<?php
/**
 * 工作日程
 * 
 * @author chenyihao
 * @date 2014-12-09
 *
 */
class cls_activity_info {
	
	private static $Table = 'activity_info';
	private static $TableInfoFile = 'activity_info_file';
	private static $TableEnroll = 'activity_enroll';
	private static $TableEnrollCustom = 'activity_enroll_custom';
	private static $TableComment = 'activity_comment';
	private static $TableFollow = 'activity_follow';
	private static $TableUser = 'sc_user';
	private static $TableActivitySee = 'activity_see';
	private static $TableActivityConfig = 'activity_config';
	
	private static $InfoStateOn = 1;
	private static $InfoStateDelete = 0;
	
	/** 签到状态   未签到 */
	private static $NoSign = 0;
	/** 签到状态   已签到 */
	private static $Signed = 1;
	
	
	/**
	 * 查阅活动,增加查阅记录
	 **/
	public function activity_see($user_id,$com_id,$act_id){
		$time = time();
		$sql = "INSERT INTO activity_see(activity_id,user_id,com_id,create_time)VALUES({$act_id},{$user_id},{$com_id},{$time}) ON DUPLICATE KEY UPDATE create_time = create_time;";
		$ret = g('db') -> query($sql);
		return $ret;
	}

	/**
	 * 检查用户是否属于部门
	 * @return 
	 */
	public function check_user($user_id,$scope_dept=array(),$scope_user = array()){
		$user = g('user') -> get_by_id($user_id,'dept_tree');
		$dept_tree = json_decode($user['dept_tree']);
		$dept = array();
		foreach ($dept_tree as $dt){
			foreach ($dt as $d){
    			array_push($dept,$d);
			}
			unset($d);
		}
		unset($dt);
		$dept = array_unique($dept);
		//获取交集，判断是否有权限
    	if(!in_array($user_id,$scope_user) && !array_intersect($dept,$scope_dept)){
    		return false;
    	}
    	return true;
	}

	/**
	 * 新建活动
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $data
	 */
	public function save_activity($com_id,$user_id,$data){
		$tem_data = array(
			'com_id' => $com_id,
			'title' => $data['title'],
			'activity_describe' => $data['desc'],
			'pic_url' => $data['pic_url'],
			'enroll_time' => $data['enroll_time'],
			'start_time' => $data['start_time'],
			'end_time' => $data['end_time'],
			'place' => $data['place'],
			'lng' => $data['lng'],
			'lat' => $data['lat'],
			'scope_user' => $data['user'],
			'scope_dept' => $data['dept'],
			'limited' => $data['limited'],
			'can_family' => $data['can_family'],
			'family_limited' => $data['family_limited'],
			'is_sign' => $data['is_sign'],
			'sign_time' => $data['sign_time'],
			'sign_remind_type' => $data['sign_remind_type'],
			'state' => $data['state'],
			'is_enroll_msg' => $data['is_enroll_msg'],
			'enroll_custom' => $data['enroll_custom'],
			'sign_place' => $data['sign_place'],
			'remind_type' => $data['remind_type'],
			'remind_time' => $data['remind_time'],
			'public_id' => $user_id,
			'public_name' => $data['user_name'],
			'public_time' => time(),
			'info_time' => time(),
			'info_state' => self::$InfoStateOn,
		);
		$ret = g('ndb') -> insert(self::$Table,$tem_data);
		return empty($ret) ? 0 : $ret ;
	}
	
	
	/**
	 * 更新活动 （支持部分更新）
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $data
	 */
	public function edit_activity($com_id,$user_id,$act_id,$data = array()){
		if(empty($data) || !is_array($data)){
			return false;
		}
		
		$cond = array(
			'com_id=' => $com_id,
			'public_id=' => $user_id,
			'id=' => $act_id,
		);
		$tem_data = array(
			'title' => $data['title'],
			'activity_describe' => $data['desc'],
			'pic_url' => $data['pic_url'],
			'enroll_time' => $data['enroll_time'],
			'start_time' => $data['start_time'],
			'end_time' => $data['end_time'],
			'place' => $data['place'],
			'lng' => $data['lng'],
			'lat' => $data['lat'],
			'is_enroll_msg' => $data['is_enroll_msg'],
			'enroll_custom' => $data['enroll_custom'],
			'scope_user' => $data['user'],
			'scope_dept' => $data['dept'],
			'limited' => $data['limited'],
			'can_family' => $data['can_family'],
			'family_limited' => $data['family_limited'],
			'is_sign' => $data['is_sign'],
			'sign_remind_type' => $data['sign_remind_type'],
			'sign_time' => $data['sign_time'],
			'sign_place' => $data['sign_place'],
			'remind_type' => $data['remind_type'],
			'remind_time' => $data['remind_time'],
			'info_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$tem_data);
		return empty($ret) ? 0 : $ret;
	}
	
	/**
	 * 根据条件获取活动列表
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $cond
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $desc
	 */
	public function get_activity_list($table,$cond,$fields='*',$page=1,$page_size=20,$order){
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order);
		if ($ret) {
			foreach ($ret as &$item) {
				$item['activity_describe'] = $this -> _generate_digest($item['activity_describe'], 100);
			}
		}
		return $ret;
	}
	

	public function check_prviage($com_id,$user_id,$fields='*'){
		$cond = array(
				'com_id=' => $com_id,
			);
		$ret = g('ndb') -> select(self::$TableActivityConfig,$fields,$cond);
		
		if(empty($ret)){
			return false;
		}

		if($ret[0]['type'] == 1){
			return true;
		}else if($ret[0]['type'] == 2){
			$users = json_decode($ret[0]['user_list'],true);
			if(in_array($user_id, $users)){
				return true;
			}
			$depts = json_decode($ret[0]['dept_list'],true);
			$user = g('user') -> get_by_id($user_id,'dept_tree');
			$dept_tree = json_decode($user['dept_tree'],true);
			$user_dept = array();
			foreach($dept_tree as $d){
				$user_dept = array_merge($user_dept,$d);
			}
			unset($d);
			if(!empty(array_intersect($user_dept,$depts))){
				return true;
			}

		}
		return false;
 	}

	/**
	 * 获取活动信息
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $deail	是否获取详情
	 */
	public function get_by_id($com_id,$act_id,$fields='*',$deail=false){
		$cond = array(
			'act.com_id=' => $com_id,
			'act.id=' => $act_id
		);
		if($deail){
			$table = self::$Table.' act LEFT JOIN '.self::$TableUser.' u ON u.id = act.public_id';
		}else{
			$table = self::$Table.' act ';
		}
		$ret = g('ndb') -> select($table,$fields,$cond);
		return empty($ret)?false:$ret[0];
	}
	
	/**
	 * 检查活动记录的人数限制//增加行锁
	 */
	public function check_activity($com_id,$act_id,$fields){
		$sql = 'select '.$fields.' from '.self::$Table.' where id='.$act_id.' and com_id = '.$com_id.' for update';
		$ret = g('db') -> select($sql);
		return empty($ret) ? false : $ret[0];
	}
	
	
	/**
	 * 获取封面图片
	 * @param unknown_type $com_id
	 * @param unknown_type $activity_id
	 */
	public function get_cover_img($com_id,$activity_id,$fields='*'){
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' => self::$InfoStateOn,
			'activity_id=' => $activity_id,
		);
		$ret = g('ndb') -> select(self::$TableInfoFile,$fields,$cond);
		return $ret;
	}
	
	
	/**
	 * 保存封面
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type file
	 */
	public function insert_cover_img($com_id,$user_id,$user_name,$act_id,$file){
		$date = time();
		$tem_data = array(
			'com_id' => $com_id,
			'file_name' => $file['file_name'],
			'file_url' => $file['file_url'],
			'file_key' => $file['file_key'],
			'activity_id' => $act_id,
			'info_state' => self::$InfoStateOn,
			'create_time' => $date,
			'create_name' => $user_name,
			'info_time' => $date,
			'create_user_id' => $user_id,
			'extname' => $file['ext']
		);
		$ret = g('ndb') -> insert(self::$TableInfoFile,$tem_data);
		return empty($ret) ? 0 : $ret ;
	}
	
	/**
	 * 删除封面
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 */
	public function delete_cover_img($com_id,$act_id){
		$cond = array(
			'com_id=' => $com_id,
			'activity_id=' => $act_id
		);
		$data = array(
			'info_state' => self::$InfoStateDelete
		);
		$ret = g('ndb') -> update_by_condition(self::$TableInfoFile,$cond,$data);
		return $ret;
	}
	
	/**
	 * 获取报名数据
	 * $user_id = 0,$act_id != 0时，获取指定活动的报名数据； 分页
	 * $user_id != 0,$act_id = 0时，获取指定员工所有的报名数据；分页
	 * $user_id != 0,$act_id != 0时，获取指定员工在指定活动的报名数据；单挑数据
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 * @param unknown_type $fields
	 */
	public function get_enroll_data($com_id,$user_id=0,$act_id=0,$fields=' e.*,u.name,u.pic_url ',$page=0,$page_size = 0,$order=""){
		$witch_detail = TRUE; 
		$cond = array(
			'e.com_id=' => $com_id,
			'e.info_state=' => self::$InfoStateOn,
		);
		if(!empty($user_id)){
			$cond['e.enroll_id='] = $user_id; 
		}
		if(!empty($act_id)){
			$cond['e.activity_id='] = $act_id;
		}
		
		if(!empty($user_id) && !empty($act_id)){
			$witch_detail = false;
		}
		
		$table = self::$TableEnroll. ' e  LEFT JOIN '.self::$TableUser.' u ON u.id = e.enroll_id ';
		
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order);
		return empty($ret) ? false : ($witch_detail ? $ret : $ret[0]);
	}
	
	/**
	 * 获取签到数据
	 * @param    $act_id
	 * @param    $type 0：未签到，1：已签到
	 * @param    $fields
	 * @param    $page
	 * @param    $page_size
	 * @param    $with_count 是否返回总数
	 * @return 
	 */
	public function get_sign_data($act_id, $type, $fields='e.*,u.pic_url,u.name ', $page=1, $page_size=20, $with_count = false){
		$cond = array(
				'e.activity_id=' => $act_id,
				'e.sign_state=' => $type,
				'e.info_state=' => self::$InfoStateOn,
			);
		$order = '';
		if($type == 1){
			$order = ' order by e.sign_time desc ';
		}

		$table = self::$TableEnroll .' e LEFT JOIN '.self::$TableUser.' u ON e.enroll_id = u.id ';
		$ret = g('ndb') -> select($table, $fields, $cond, $page, $page_size, '', $order, $with_count);
		return $ret;
	}


	/**
	 * 添加报名数据
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 */
	public function enroll_activity($com_id,$act_id,$user_id,$custom,$family=array(),$count,$now){
		$data = array(
			'activity_id' => $act_id,
			'com_id' => $com_id,
			'enroll_id' => $user_id,
			'custom' => $custom,
			'family' => $family,
			'family_num' => $count,
			'enroll_time' => $now,
			'info_state' => self::$InfoStateOn,
			'info_time' => $now,
		);
		$ret = g('ndb') -> insert(self::$TableEnroll,$data);
		return $ret;
	}
	
	
	/**
	 * 更新报名数据
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 */
	public function update_enroll($id,$com_id,$act_id,$user_id,$custom,$family=array(),$count,$now=false){
		if(!$now){
			$now = time();
		}
		$data = array(
			'custom' => $custom,
			'family' => $family,
			'family_num' => $count,
			'enroll_time' => $now,
		);
		$cond = array(
			'com_id=' => $com_id,
			'enroll_id=' => $user_id,
			'id=' => $id,
			'activity_id=' => $act_id
		);
		
		$ret = g('ndb') -> update_by_condition(self::$TableEnroll,$cond,$data);
		return $ret;
	}
	
	/**
	 * 删除报名数据
	 * @param unknown_type $com_id
	 * @param unknown_type $enroll
	 * @param unknown_type $act_id
	 */
	public function delete_enroll($com_id,$act_id,$user_id){
		$cond = array(
			'com_id=' => $com_id,
			'enroll_id=' => $user_id,
			'activity_id=' => $act_id,
		);
		$data = array(
			'info_state' => self::$InfoStateDelete,
			'info_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$TableEnroll,$cond,$data);
		return $ret;
	}
	
	
	
	/**
	 * 活动计数字段更新（评论数，点赞数，图片数）
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $update_col
	 */
	public function update_num($com_id,$act_id,$update_col,$type=1,$num=1){
		if(!in_array($update_col,array('comment_num','follow_num','enroll_num','image_num'))){
			return false;
		}
		if($type==1){
			$do_num = '+'.$num;
		}else{
			$do_num = '-'.$num;
		}
		
		
		$sql = 'UPDATE '.self::$Table.' SET '.$update_col.'= '.$update_col.$do_num.' WHERE  com_id ='.$com_id.' AND id='.$act_id;
		log_write($sql);
		$ret = g('db') -> query($sql);
		return $ret;
	}
	
	
	/**
	 * 活动计数字段更新（报名人数）
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $update_col
	 */
	public function update_enroll_num($com_id,$act_id){
		$sql = 'UPDATE '.self::$Table.' SET '.$update_col.'= '.$update_col.'+1 WHERE  com_id ='.$com_id.' AND id='.$act_id;
		$ret = g('db') -> query($sql);
		return $ret;
	}
	
	
	/**
	 * 增加评论
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 * @param unknown_type $pid
	 * @param unknown_type $comment
	 */
	public function add_comment($com_id,$user_id,$act_id,$pid,$comment){
		$now = time();
		$data = array(
			'com_id' => $com_id,
			'comment_id' => $user_id,
			'activity_id' => $act_id,
			'pid' => $pid,
			'comment_content' => $comment,
			'comment_time' => $now,
			'info_state' => self::$InfoStateOn,
			'info_time' => $now,
		);
		$ret = g('ndb') -> insert(self::$TableComment,$data,false);
		return $ret;
	}
	
	/**
	 * 获取评论列表
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $fields
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 */
	public function get_comment_list($com_id,$act_id,$fields=' c.*,u.name,u.pic_url,count(c1.id) count ',$page=0,$page_size=0,$order=""){
		$cond = array(
			'c.com_id=' => $com_id,
			'c.activity_id=' => $act_id,
			'c.info_state=' => self::$InfoStateOn,
			'c.pid=' => 0
		);
		$table = self::$TableComment.' c LEFT JOIN '.self::$TableComment.' c1 ON c.id = c1.pid LEFT JOIN '.self::$TableUser.' u ON c.comment_id = u.id ';
		
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,' group by c.id ',$order);
		return empty($ret)? false:$ret;
	}
	
	/**
	 * 获取评论
	 * @param unknown_type $com_id
	 * @param unknown_type $comm_id
	 */
	public function get_comment($com_id,$comm_id,$fields='*'){
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $comm_id,
			'info_state=' => self::$InfoStateOn
		);
		$ret = g('ndb') -> select(self::$TableComment,$fields,$cond);
		return empty($ret)? false:$ret[0];
	}
	
	/**
	 * 获取评论列表
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $comment_id
	 */
	public function get_comment_detail($com_id,$act_id,$comment_id,$fields,$page,$page_size,$order=''){
		$cond = array(
			'c.com_id=' => $com_id,
			'c.activity_id=' => $act_id,
			'c.info_state=' => self::$InfoStateOn,
			'c.pid=' => $comment_id
		);
		$table = self::$TableComment.' c LEFT JOIN '.self::$TableUser.' u ON c.comment_id = u.id ';
		
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order,TRUE);
		return empty($ret)? false:$ret;
	}
	
	
	/**
	 * 签到
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $user_id
	 * @param unknown_type $lng
	 * @param unknown_type $lat
	 */
	public function enroll_sign($com_id,$act_id,$user_id,$lng,$lat,$family){
		$cond = array(
			'com_id=' => $com_id,
			'enroll_id=' => $user_id,
			'activity_id=' => $act_id,
			'info_state=' => self::$InfoStateOn,
		);
		$data = array(
			'sign_state' => self::$Signed,
			'lng' => $lng,
			'lat' => $lat,
			'family' => $family,
			'sign_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$TableEnroll,$cond,$data);
		return $ret;
	}
	
	/**
	 * 删除活动
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 */
	public function delete_activity($com_id,$act_id){
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $act_id
		);
		$data = array(
			'info_state' => self::$InfoStateDelete,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 改变活动的状态（发布/终止）
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $state
	 */
	public function change_actvity_state($com_id,$act_id,$state){
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $act_id
		);
		$data = array(
			'state' => $state,
		);
		if($state = 1){
			$data['public_time'] = time();
		}
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 获取指定人员的点赞信息
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $user_id
	 */
	public function get_activity_follow($com_id,$act_id,$user_id,$fields='*'){
		$cond = array(
			'com_id=' => $com_id,
			'activity_id=' => $act_id,
			'user_id=' => $user_id, 
		);
		$ret = g('ndb') -> select(self::$TableFollow,$fields,$cond);
		return empty($ret) ? false:$ret[0];
	}
	
	/**
	 * 活动点赞
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $user_id
	 * @param unknown_type $fields
	 */
	public function activity_follow($com_id,$act_id,$user_id){
		$data = array(
			'activity_id' => $act_id,
			'com_id' => $com_id,
			'user_id' => $user_id,
			'is_follow' => 1,
			'create_time' => time()
		);
		$ret = g('ndb') -> insert(self::$TableFollow,$data,false);
		return $ret;
	}
	
	/**
	 * 获取自定义字段
	 * @param unknown_type $com_id
	 */
	public function get_enroll_custom($com_id,$fields = '*'){
		$cond = array(
			'com_id=' => $com_id,
			'info_state=' => self::$InfoStateOn
		);
		$ret = g('ndb') -> select(self::$TableEnrollCustom,$fields,$cond);
		return empty($ret)?false:$ret;
	}

	
	/**
	 * 生成摘要
	 * @param  string  $content='' 	富文本内容源码
	 * @param  integer $length=100 	摘要字数限制
	 * @return [type]
	 */
	private function _generate_digest($text='',$length=50) {
	    mb_regex_encoding("UTF-8"); 
	    $text = strip_tags($text);
	    $text = str_replace('&nbsp;', '', $text);
	    if(mb_strlen($text) <= $length ) return $text;   
	    
	    $Foremost = mb_substr($text, 0, $length); 
	    $re = "<(/?) 
	(P|DIV|H1|H2|H3|H4|H5|H6|ADDRESS|PRE|TABLE|TR|TD|TH|INPUT|SELECT|TEXTAREA|OBJECT|A|UL|OL|LI| 
	BASE|META|LINK|HR|BR|PARAM|IMG|AREA|INPUT|SPAN)[^>]*(>?)"; 
	    $Single = "/BASE|META|LINK|HR|BR|PARAM|IMG|AREA|INPUT|BR/i";     
	
	    $Stack = array(); $posStack = array(); 
	    mb_ereg_search_init($Foremost, $re, 'i'); 
	    while($pos = mb_ereg_search_pos()){ 
	        $match = mb_ereg_search_getregs(); 
	        if($match[1] == "") { 
	            $Elem = $match[2]; 
	            if(mb_eregi($Single, $Elem) && $match[3] !=""){ 
	                continue; 
	            } 
	            array_push($Stack, mb_strtoupper($Elem)); 
	            array_push($posStack, $pos[0]);             
	        } else { 
	            $StackTop = $Stack[count($Stack)-1]; 
	            $End = mb_strtoupper($match[2]); 
	            if(strcasecmp($StackTop,$End)==0){ 
	                array_pop($Stack); 
	                array_pop($posStack); 
	                if($match[3] ==""){ 
	                    $Foremost = $Foremost.">"; 
	                } 
	            } 
	        } 
	    } 
	
	    $cutpos = array_shift($posStack) - 1;     
	    $Foremost =  mb_substr($Foremost,0,$cutpos,"UTF-8"); 
	    return $Foremost; 
	}
}

// end of file