<?php
/**
 * 工作日程
 * 
 * @author chenyihao
 * @date 2014-12-09
 *
 */
class cls_activity_atlas {
	
	private static $Table = 'activity_atlas';
	private static $TableFolder = 'activity_folder';
	private static $TableFile = 'activity_file';
	private static $TableInfo = 'activity_info';
	private static $TableUser = 'sc_user';
	
	private static $InfoStateOn = 1;
	private static $InfoStateDelete = 0;
	
	/** 审核状态 未审核  */
	private static $ApproveNot = 0;
	/** 审核状态 通过  */
	private static $ApproveAccess = 1;
	/** 审核状态 不通过 */
	private static $ApproveNoAccess = 2;
	
	/** 文件类型  文件夹  */
	private static $FileFolder = 1;
	/** 文件类型  文件 */
	private static $FileFile = 2;
	
	/**
	 * 新建文件/文件夹
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $act_id
	 * @param unknown_type $file_name
	 * @param unknown_type $pid
	 * @param unknown_type $file_type	1:文件夹，2：文件
	 */
	public function save_atlas($com_id,$user_id,$act_id,$file_name,$pid,$file_type){
		$tem_data = array(
			'com_id' => $com_id,
			'file_name' => $file_name,
			'file_type' => $file_type,
			'pid' => $pid,
			'activity_id' => $act_id,
			'create_id' => $user_id,
			'create_time' => time(),
			'info_time' => time(),
			'info_state' => self::$InfoStateOn,
		);
		$ret = g('ndb') -> insert(self::$Table,$tem_data);
		return empty($ret) ? 0 : $ret ;
	}
	
	/**
	 * 根据ID获取atlas
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $atlas_id
	 */
	public function get_atlas_by_id($com_id,$atlas_id,$fields='*'){
		$cond=array(
			'com_id=' => $com_id,
			'id=' => $atlas_id
		);
		$ret = g('ndb') -> select(self::$Table,$fields,$cond);
		return empty($ret)?false:$ret[0];
	}
	
	/**
	 * 创建文件夹
	 */
	public function save_folder($id,$is_system,$user_id,$user_name){
		$tem_data = array(
			'id' => $id,
			'is_system' => $is_system,
			'creater_id' => $user_id,
			'creater_name' => $user_name,
			'create_time' => time(),
		);
		$ret = g('ndb') -> insert(self::$TableFolder,$tem_data);
		return empty($ret) ? 0 : $ret ;
	}
	
	/**
	 * 创建文件
	 * @param unknown_type $id
	 * @param unknown_type $file_state	/0：未审核，1：审核通过
	 * @param unknown_type $file_url
	 * @param unknown_type $user_id
	 * @param unknown_type $user_name
	 */
	public function save_file($act_id,$id,$file_state,$file_url,$file_name,$file_key,$file_ext,$user_id,$user_name){
		$tem_data = array(
			'id' => $id,
			'activity_id' =>$act_id,
			'file_state' => $file_state,
			'file_url' => $file_url,
			'file_name' => $file_name,
			'file_key' => $file_key,
			'file_ext' => $file_ext,
			'upload_id' => $user_id,
			'upload_name' => $user_name,
			'upload_time' => time(),
		);
		$ret = g('ndb') -> insert(self::$TableFile,$tem_data);
		return empty($ret) ? 0 : $ret ;
	}
	
	/**
	 * 根据条件获取图册列表
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $cond
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $desc
	 */
	public function get_atlas_list($table,$cond,$fields='*',$page=0,$page_size=0,$order=""){
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'',$order);
		return $ret;
	}
	
	
	/**
	 * 
	 * @param unknown_type $com_id
	 * @param unknown_type $act_id
	 * @param unknown_type $fields
	 */
	public function get_file_by_act_id($com_id,$act_id,$fields='*',$page=0,$page_size=20,$order=""){
		$cond = array(
			'a.com_id=' => $com_id,
			'a.activity_id=' => $act_id,
			'a.info_state=' => self::$InfoStateOn,
			'f.file_state=' => self::$ApproveAccess
		);
		
		$table = self::$Table.' a LEFT JOIN '. self::$TableFile.' f ON a.id = f.id ';
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,"",$order);
		return $ret;
	}
	
	/**
	 * 获得指定文件夹下的文件
	 * @param unknown_type $com_id
	 * @param unknown_type $folder_id	//文件夹ID
	 * @param unknown_type $file_state	//文件类型 1：审核通过，0，审核不通过,2:我上传的
	 */
	public function get_file_by_folder($com_id,$user_id,$folder_id,$file_state,$fields='f.*',$page,$page_size,$order=""){
		$cond = array(
			'a.com_id=' => $com_id,
			'a.pid=' => $folder_id,
			'a.info_state=' => self::$InfoStateOn,
		);
		if($file_state == 2){
			$cond['a.create_id='] = $user_id;
		}else{
			$cond['f.file_state='] = $file_state;
		}
		
		$table = self::$Table.' a LEFT JOIN '. self::$TableFile.' f ON a.id = f.id ';
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,"",$order);
		return $ret;
	}
	
	/**
	 * 获取文件(批量)
	 * @param unknown_type $com_id
	 * @param unknown_type $file_id
	 * @param unknown_type $fields
	 */
	public function get_file_by_id($com_id,$act_id,$file_id,$fields = 'f.*,a2.create_id'){
		$cond = array(
			'a.file_type=' => self::$FileFile,
			'a.info_state=' => self::$InfoStateOn,
			'a.com_id=' => $com_id,
			'a.activity_id=' => $act_id,
		);
		if(is_array($file_id)){
			$cond['f.id IN ']  = '('.implode(',',$file_id).')';
		}else{
			$cond['f.id=']  = $file_id;
		}
		$table = self::$TableFile .' f LEFT JOIN '.self::$Table .' a ON a.id = f.id LEFT JOIN '.self::$Table.' a2 ON a.pid = a2.id';
		$ret = g('ndb') -> select($table,$fields,$cond);
		return empty($ret) ? false : (is_array($file_id) ? $ret : $ret[0]);
	}
	
	/**
	 * 审核文件
	 */
	public function approval_file($file_id,$user_id,$user_name){
		$cond = array();
		if(is_array($file_id)){
			$cond['id IN '] = "(".implode(',', $file_id).")";
		}else{
			$cond['id='] = $file_id;
		}
		$data = array(
			'file_state' => self::$ApproveAccess,
			'approve_id' => $user_id,
			'approve_name' => $user_name,
			'approve_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$TableFile,$cond,$data,false);
		return $ret;
	}
	
	/**
	 *  删除文件
	 */
	public function delete_file($file_id,$user_id){
		$cond = array();
		if(is_array($file_id)){
			$cond['id IN '] = "(".implode(',', $file_id).")";
		}else{
			$cond['id='] = $file_id;
		}
		$data = array(
			'info_state'=> self::$InfoStateDelete,
			'info_user_id' => $user_id,
			'info_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data,false);
		return $ret;
	}
}

// end of file