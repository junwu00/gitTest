<?php
/**
 * 审批流程指派方式
 * @author yangpz
 * @date 2014-12-05
 */
class cls_proc_proc {
	/** 对应的库表名称 */
	private static $Table = 'proc_proc';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	/** 冻结 2 */
	private static $StateFrozen = 2;
	
	/**
	 * 获取企业流程指派详情
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $ver		版本ID
	 * @param unknown_type $step
	 */
	public function get_by_com_proc($com_id, $proc_id, $ver, $step=1, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'ver=' => $ver,
			'step=' => $step,
			'state!=' => self::$StateOff,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		
		if ($ret && count($ret) > 1) {
			throw new SCException('存在多条审批人配置');
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 判断审批流程是否被冻结
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 */
	public function is_proc_frozen($com_id, $proc_id) {
		$fields = '*';		
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'state=' => self::$StateFrozen,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? TRUE : FALSE;
	}

	/**
	 * 获取流程的最新版本号
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 */
	public function get_last_ver($com_id, $proc_id) {
		$fileds = '*';
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'state=' => self::$StateOn,
		);
		$ret = g('ndb') -> select(self::$Table, $fileds, $cond, 1, 1, '', ' ORDER BY ID DESC ');
		if (!$ret) {
			return 0;
			
		} else {
			return $ret[0]['ver'];
		}
	}
}

// end of file