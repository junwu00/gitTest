<?php
/**
 * 邮件操作相关方法
 *
 */
class cls_email_email {
	
	private static $Table = 'email_email';
	
	
	/**
	 * 保存邮件信息
	 * @param unknown_type $data
	 * @param unknown_type $file
	 * @param unknown_type $filename
	 * @throws SCException
	 */
	public function save($data){
		try {
			$result = g('ndb') -> insert(self::$Table, $data, FALSE);
			if($result) 
				return $result;
			else
				throw new SCException('保存邮件失败！');
		} catch (SCException $e) {
			throw $e;
		}
	}

	/**
	 *  通过messageid获取邮箱信息
	 * @param unknown_type $messageID
	 */
	public function get_email_mid($messageID,$fidles ="*"){
		$cond = array(
			'message_id=' =>$messageID
		);
		return g('ndb') -> select(self::$Table,$fidles,$cond);
	}
}