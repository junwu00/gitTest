<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_work_node {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_work_node';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/** 开始节点标示 */
	private static $StateStart = 1;
	/** 非开始节点标示 */
	private static $StateNotStart = 0;
	
	
	/**
	 * 根据ID获取流程节点new
	 * @param unknown_type $com_id
	 * @param unknown_type $id
	 */
	public function get_by_id($com_id, $id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		
		return $ret ? $ret[0] : FALSE;
	}
	
	
	public function get_worknode_by_condition($com_id,$work_id,$cond=array(),$fields='*'){
		$condition = array(
			'com_id='=>$com_id,
			'work_id=' =>$work_id
		);
		
		$cond = array_merge($condition,$cond);
		
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret : FALSE;
	}
	
	
	
	/**
	 * 根据ids获取节点信息new
	 * @param int $com_id
	 * @param string $ids
	 */
	public function get_work_node_by_id($com_id,$ids){
		$sql = "select * from ".self::$Table." where com_id=".$com_id." and id in(".$ids.") and info_state=".self::$StateOn;
		$ret = g('db') -> select($sql);
		return $ret ? $ret : FALSE;
		
	}
	
	/**
	 * 
	 * 查询模板的开始节点new
	 * @param 公司id $com_id
	 * @param 流程模板id $work_id
	 * @param 查询字段 $fields
	 */
	public function get_start_by_workid($com_id,$work_id, $fields='*'){
		$cond = array(
			'work_id=' => $work_id,
			'is_start=' => self::$StateStart,
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	//---------------------------------------内部实现

}

// end of file