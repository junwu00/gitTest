<?php
/**
 * 考勤记录
 * @author yangpz
 * @date 2014-11-11
 */
class cls_cwork_record {
	/** 对应库表名称 */
	private static $Table = 'cwork_record';
	
	/** 非工作日 */
	private static $NoWorkDay = -2;
	/** 没有签到记录 */
	private static $NoSigninRec = -1;
	
	/** 正常 0 */
	private static $SignSuccess = 0;
	/** 迟到 1 */
	private static $SignLate = 1;
	/** 早退 2 */
	private static $SignEarly = 2;
	/** 未记录 3 */
	private static $SignEmpty = 3;
	/** 无签到记录 4 */
	private static $SignNoSignin = 4;
	
	/** 标准工时签到 0 */
	private static $StandardSignIn = 0;
	/** 标准工时签退 1 */
	private static $StandardSignOut = 1;
	/** 弹性工时签到 2 */
	private static $ElasticSignIn = 2;
	/** 弹性工时签退 3 */
	private static $ElasticSignOut = 3;

	/** 标准工时 0 */
	private static $TypeStandard = 0;
	/** 弹性工时 1 */
	private static $TypeElastic = 1;

	/**
	 * 获取指定天的签到记录(未记录除外)
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	指定日期
	 * @param unknown_type $sign_type	签到类型
	 */
	public function get_signin_by_day($com_id, $user_id, $sign_day, $sign_type, $fields='*') {
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$sign_day = mysql_escape_string($sign_day);
		
		$table = self::$Table;
		$type = self::$ElasticSignIn.','.self::$StandardSignIn;
		if ($sign_type == self::$TypeStandard) {
			$type = self::$StandardSignIn;
		} else if ($sign_type == self::$TypeElastic) {
			$type = self::$ElasticSignIn;
		}
		$state_empty = self::$SignEmpty;
		$sql = <<<EOF
		SELECT {$fields} FROM {$table}
		WHERE com_id={$com_id} AND user_id={$user_id} AND day='{$sign_day}' AND sign_type in ({$type}) AND state!={$state_empty}
		ORDER BY id
EOF;
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 获取指定天的签退记录(未记录除外)
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	指定日期
	 * @param unknown_type $sign_type	签到类型
	 */
	public function get_signout_by_day($com_id, $user_id, $sign_day, $sign_type, $fields='*') {
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$sign_day = mysql_escape_string($sign_day);
		
		$table = self::$Table;
		$type = self::$ElasticSignOut.','.self::$StandardSignOut;
		if ($sign_type == self::$TypeStandard) {
			$type = self::$StandardSignOut;
		} else if ($sign_type == self::$TypeElastic) {
			$type = self::$ElasticSignOut;
		}
		$state_empty = self::$SignEmpty;
		$sql = <<<EOF
		SELECT {$fields} FROM {$table}
		WHERE com_id={$com_id} AND user_id={$user_id} AND day='{$sign_day}' AND sign_type in ({$type}) AND state!={$state_empty}
		ORDER BY id
EOF;
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 判断员工是否签到/退过
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	考勤日期，如：20150101
	 * @param unknown_type $sign_group	时间段ID,如：0, 1
	 * @param unknown_type $with_empty	是否包括”未记录“的记录，默认不取
	 * @param unknown_type $sign_type	考勤类型：0标准签到 1标准签退 2弹性签到 3弹性签退
	 */
	public function has_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type, $with_empty=FALSE, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'day=' => $sign_day,
			'sign_group=' => $sign_group,
			'sign_type=' => $sign_type
		);
		!$with_empty && $cond['state!='] = self::$SignEmpty;
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 指定日期是否有保存的考勤记录
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	考勤日期
	 * @param unknown_type $fields
	 */
	public function has_rec($com_id, $user_id, $sign_day, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'day=' => $sign_day
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret : array();
	}
	
	/**
	 * 获取考勤记录（状态非”未记录“）
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	考勤日期
	 * @param unknown_type $sign_group	分段
	 * @param unknown_type $sign_type	考勤类型
	 */
	public function get_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'day=' => $sign_day,
			'sign_group=' => $sign_group,
			'sign_type=' => $sign_type,
			'state!=' => self::$SignEmpty
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 保存签到信息
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		用户ID
	 * @param unknown_type $user_acct	用户账号
	 * @param unknown_type $sign_day	签到日期
	 * @param unknown_type $sign_group	时间段
	 * @param unknown_type $sign_type	考勤类型
	 * @param unknown_type $sign_limit	考勤时间（限制）
	 * @param unknown_type $addr		考勤地点
	 * @param unknown_type $sign_state	状态：迟到早退等
	 * @param unknown_type $rule		考勤规则
	 * @throws SCException
	 */
	public function save_signin($com_id, $user_id, $user_acct, $sign_day, $sign_group, $sign_type, $sign_limit, $addr, $sign_state, array $rule) {
		if ($sign_type == self::$StandardSignIn) {	//标准工时，验证考勤时间范围
			$this -> check_sign_time_limit($rule, $sign_group, $sign_type);
		}
		
		$now = time();
		$data = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'sign_group' => $sign_group,
			'day' => $sign_day,
			'sign_limit' => $sign_limit,
			'sign_type' => $sign_type,
			'addr_id' => $addr['id'],
			'addr_name' => $addr['addr'],
			'state' => $sign_state,
			'update_time' => $now
		);
		$db = g('db');
		try {
			$db -> begin_trans();
			$has_rec = $this -> has_rec($com_id, $user_id, $sign_day);
			//无”未记录“的记录，生成”未记录“的记录，并更新当前保存的记录
			if (empty($has_rec)) {				
				$this -> batch_insert_empty_rec($com_id, $user_id, $user_acct, $sign_day, $rule);
			}
			
			//记录为”未记录“，更新该数据 
			$has_empty = $this -> has_empty_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type);
			if ($has_empty) {
				//更新签到记录
				$this -> update_sign($com_id, $user_id, $has_empty['id'], $data);
				$ret = $has_empty['id'];
				
			} else {
				$rec = $this -> get_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type);
				if (!$rec) {
					log_write("找不到签到记录信息：com={$com_id}, user={$user_id}, day={$sign_day}, type={$sign_type}");
					throw new SCException('找不到签到记录信息，请联系管理员勿频繁修改考勤规则');
				}
				$ret = $rec['id'];
			}
			
			if ($sign_type == self::$ElasticSignIn) {	//弹性签到，记录签退提醒时间
				$group = $rule['detail'][$sign_group];
				g('cwork_warn') -> save_or_update($com_id, $user_id, $user_acct, $now, $group['e_work_hour'], $group['e_work_min']);	//保存弹性签退提醒记录
			}
			$db -> commit();
			return $ret;
			
		} catch (SCException $e) {
			$db -> rollback();
			throw $e;
		}
	}
	
	/**
	 * 保存签退信息
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		用户ID
	 * @param unknown_type $user_acct	用户账号
	 * @param unknown_type $sign_day	签到日期
	 * @param unknown_type $sign_group	时间段
	 * @param unknown_type $sign_type	考勤类型
	 * @param unknown_type $sign_limit	考勤时间（限制）
	 * @param unknown_type $addr		考勤地点
	 * @param unknown_type $sign_state	状态：迟到早退等
	 * @param unknown_type $rule		考勤规则
	 * @throws SCException
	 */
	public function save_signout($com_id, $user_id, $user_acct, $sign_day, $sign_group, $sign_type, $sign_limit, $addr, $sign_state, array $rule) {
		if ($sign_type == self::$StandardSignOut) {	//标准工时，验证考勤时间范围
			$this -> check_sign_time_limit($rule, $sign_group, $sign_type);
		}
		
		$now = time();
		$data = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'sign_group' => $sign_group,
			'day' => $sign_day,
			'sign_limit' => $sign_limit,
			'sign_type' => $sign_type,
			'addr_id' => $addr['id'],
			'addr_name' => $addr['addr'],
			'state' => $sign_state,
			'update_time' => $now
		);
		$db = g('db');
		try {
			$db -> begin_trans();
			$has_rec = $this -> has_rec($com_id, $user_id, $sign_day);
			//无”未记录“的记录，生成”未记录“的记录，并更新当前保存的记录
			if (empty($has_rec)) {				
				$this -> batch_insert_empty_rec($com_id, $user_id, $user_acct, $sign_day, $rule);
			}
			
			//查找”未记录“的记录
			$rec = $this -> has_empty_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type);
			if (empty($rec)) {	//无”未记录“的记录，查找已签退的记录
				$rec = $this -> get_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type);
			}
			if (empty($rec)) {
				log_write("找不到签退记录信息：com={$com_id}, user={$user_id}, day={$sign_day}, type={$sign_type}");
				throw new SCException('找不到签退记录信息，请联系管理员勿频繁修改考勤规则');
			}
			
			//更新签退记录
			$this -> update_sign($com_id, $user_id, $rec['id'], $data);
			$ret = $rec['id'];
			
			$db -> commit();
			return $ret;
			
		} catch (SCException $e) {
			$db -> rollback();
			throw $e;
		}
	}
	
	/**
	 * 验证考勤时间是否在范围内
	 * @param unknown_type $rule		规则内容
	 * @param unknown_type $sign_group	分段
	 * @param unknown_type $sign_type	签到/退类型
	 */
	private function check_sign_time_limit(array $rule, $sign_group, $sign_type) {
		$reset_time = $rule['reset_time'];
		
		$group = $rule['detail'][$sign_group];
		$d_time = date('H:i');
		$d_time = $this -> trans_sign_date($d_time, $reset_time);
		$d_time = strtotime($d_time);
		
		if ($sign_type == self::$StandardSignIn) {
			$type_name = '签到';
			$sign_start = $this -> trans_sign_date($group['signin_start'], $reset_time);
			$sign_end = $this -> trans_sign_date($group['signin_end'], $reset_time);
			$d_start = strtotime($sign_start);
			$d_end = strtotime($sign_end);
			
		} else {
			$type_name = '签退';
			$sign_start = $this -> trans_sign_date($group['signout_start'], $reset_time);
			$sign_end = $this -> trans_sign_date($group['signout_end'], $reset_time);
			$d_start = strtotime($sign_start);
			$d_end = strtotime($sign_end);
		}
		
		if ($d_time < $d_start) {
			throw new SCException($type_name.'时间未到，不能'.$type_name);
		}
		if ($d_time >= $d_end) {
			throw new SCException($type_name.'时间已过，不能'.$type_name);
		}
	}
	
	/**
	 * 将时间转换成Date方便比较
	 * @param unknown_type $time		时间，如：12:59
	 * @param unknown_type $reset_time	新考勤时间
	 */
	private function trans_sign_date($time, $reset_time) {
		$reset_time = str_replace(':', '', $reset_time);
		strlen($time) == 4 && $time = '0'.$time;			//左补0
		$time_str = intval(str_replace(':', '', $time));
		if ($time_str >= $reset_time || $time_str == 0) {	//不跨天
			return date('2015-01-01 '.substr($time, 0, 2).':'.substr($time, 3, 2).':00');
		} else {										//跨天
			return date('2015-01-02 '.substr($time, 0, 2).':'.substr($time, 3, 2).':00');
		}
	}
	
	/**
	 * 是否有未记录考勤情况的记录
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $sign_day	考勤日期
	 * @param unknown_type $sign_group	分段
	 * @param unknown_type $sign_type	考勤类型
	 * @param unknown_type $fields
	 */
	private function has_empty_signed($com_id, $user_id, $sign_day, $sign_group, $sign_type, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'day=' => $sign_day,
			'sign_group=' => $sign_group,
			'sign_type=' => $sign_type,
			'state=' => self::$SignEmpty
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			log_write('考勤：存在多条重复的未记录的记录：'.json_encode($cond));
		}
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 更新考勤信息（对于state=无记录）
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $rec_id		记录ID
	 * @param unknown_type $data		新考勤数据
	 */
	private function update_sign($com_id, $user_id, $rec_id, array $data) {
		$cond = array(
			'id=' => $rec_id,
			'com_id=' => $com_id,
			'user_id=' => $user_id,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新考勤信息失败');
		}
		return $ret;
	}
	
	/**
	 * 保存当天”未记录“的记录，包括签到/退
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $user_acct	员工账号
	 * @param unknown_type $sign_day	考勤日期
	 * @param unknown_type $rule		考勤规则
	 */
	private function batch_insert_empty_rec($com_id, $user_id, $user_acct, $sign_day, array $rule) {
		$weekday = date('w', strtotime($sign_day));		//获取星期N
		$weekday = $weekday == 0 ? 7 : $weekday;
		$group_list = array();
		$detail = $rule['detail'];
		foreach ($detail as $idx => $item) {
			if (in_array($weekday, $item['week'])) {	//有对应分段规则，记录下来
				$group_list[$idx] = $item;
			}
		}
		unset($item);
		
		if (empty($group_list)) {						//无对应分段规则，采用默认规则
			foreach ($detail as $idx => $item) {
				if (in_array($rule['def_day'], $item['week'])) {
					$group_list[$idx] = $item;
				}
			}
			unset($item);
		}
		
		if (empty($group_list)) {						//还是无对应规则，返回
			return;
		}
		
		//插入记录
		$fields = array('com_id', 'user_id', 'day', 'addr_id', 'sign_group', 'sign_limit', 'sign_type', 'state', 'create_time', 'update_time');
		$fields = implode(',', $fields);
		$data = array();
		$now = time();
		foreach ($group_list as $key => $group) {
			$item_in = array(				//签到记录
				'com_id' => $com_id,
				'user_id' => $user_id,
				'day' => $sign_day,
				'addr_id' => 0,
				'sign_group' => $key,
				'sign_limit' => $rule['type'] == self::$TypeStandard ? $group['signin_time'].'~'.$group['signout_time'] : $group['e_work_hour'].'小时'.$group['e_work_min'].'分钟',
				'sign_type' => $rule['type'] == self::$TypeStandard ? self::$StandardSignIn : self::$ElasticSignIn,
				'state' => self::$SignEmpty,
				'create_time' => $now,
				'update_time' => $now
			);
			$item_out = array(				//签退记录
				'com_id' => $com_id,
				'user_id' => $user_id,
				'day' => $sign_day,
				'addr_id' => 0,
				'sign_group' => $key,
				'sign_limit' => $rule['type'] == self::$TypeStandard ? $group['signin_time'].'~'.$group['signout_time'] : $group['e_work_hour'].'小时'.$group['e_work_min'].'分钟',
				'sign_type' => $rule['type'] == self::$TypeStandard ? self::$StandardSignOut : self::$ElasticSignOut,
				'state' => self::$SignEmpty,
				'create_time' => $now,
				'update_time' => $now
			);
			
			array_push($data, $item_in);		//签到
			array_push($data, $item_out);		//签退
		}
		unset($group);
		
		$ret = g('ndb') -> batch_insert(self::$Table, $fields, $data);
		if (!$ret) {
			throw new SCException('保存考勤记录失败');
		}
		return $ret;
	}
	
	/**
	 * 判断签到/退结果
	 * @param unknown_type $com_id			企业ID
	 * @param unknown_type $user_id			用户ID
	 * @param unknown_type $sign_day		考勤日期
	 * @param unknown_type $rule			规则详情
	 * @param unknown_type $sign_group		分段
	 * @param unknown_type $sign_type		签到/退类型
	 * @param unknown_type $sign_time		签到/退时间戳，默认为当前时间
	 */
	public function get_sign_state($com_id, $user_id, $sign_day, array $rule, $sign_group, $sign_type, $sign_time='') {
		empty($sign_time) && $sign_time = time();
		$flag_time = date('H:i', $sign_time);
		$sec = date('s', $sign_time);
		
		$group = $rule['detail'][$sign_group];
		$flag_time = $this -> trans_sign_time($flag_time, $rule['reset_time']);
		$flag_time += $sec;
		
		$state = self::$SignEmpty;
		$diff = 0;	//提前/迟到的秒数
		switch ($sign_type) {
			case 0:		//标准签到
				$compare = $this -> trans_sign_time($group['signin_time'], $rule['reset_time']);
				$limit = $group['signin_limit'] * 60;		//可迟到时长
				$diff = $compare - $flag_time;
				if ($flag_time <= ($compare + $limit)) {	//签到时间 小于等于 规定时间+可迟到时间，计为正常
					$state = self::$SignSuccess;
				} else {									//计为迟到
					$state = self::$SignLate;
				}
				BREAK;
			case 1:		//标准签退
				$compare = $this -> trans_sign_time($group['signout_time'], $rule['reset_time']);
				$limit = $group['signout_limit'] * 60;		//可迟到时长
				$diff = $flag_time - $compare;
				if ($flag_time >= ($compare - $limit)) {		//签退时间 大于 规定时间-可提前时间，计为正常
					$state = self::$SignSuccess;
				} else {									//计为早退
					$state = self::$SignEarly;
				}
				BREAK;
			case 2:		//弹性签到
				$state = self::$SignSuccess;				//都记为正常
				BREAK;
			case 3:		//弹性签退
				$e_sign_in = $this -> has_signed($com_id, $user_id, $sign_day, $sign_group, self::$ElasticSignIn);
				if ($e_sign_in) {
					$work_time = $sign_time - $e_sign_in['create_time'];	//计算时长
						$diff = $work_time - ($group['e_work_hour'] * 3600) - ($group['e_work_min'] * 60);
					if ($work_time >= ($group['e_work_hour'] * 3600) + ($group['e_work_min'] * 60) - ($group['e_work_limit'] * 60)) {	//达到时长，记为正常
						$state = self::$SignSuccess;
					} else {																											//记为早退
						$state = self::$SignEarly;
					}
					
				} else {		//无对应签到信息
					$state = self::$SignNoSignin;
				}
				BREAK;
			default:
				throw new SCException('无效的签到/退类型');
		}
		
		return array('state' => $state, 'time' => $diff);
	}

	/**
	 * 保存签到说说/签退心情
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $record_id
	 * @param unknown_type $mood
	 */
	public function save_mood($com_id, $user_id, $record_id, $mood) {
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $record_id,
			'user_id=' => $user_id,
		);
		$data = array(
			'mood' => $mood,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('保存失败');
		}
	}
	
	/**
	 * 获取用户月度考勤信息
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $year
	 * @param unknown_type $mon
	 * @param unknown_type $gone_days	已过去的天数
	 */
	public function get_user_mon_record($com_id, $user_id, $year, $mon, &$gone_days) {
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$year = intval($year);
		$mon = intval($mon);
		
		$setting = g('cwork_setting') -> get_by_com($com_id);
		if (!$setting)	{	//未配置
			$start_day = 1;
			$end_mon = 0;
			$end_day = 31;
		} else {
			$start_day = $setting['start_day'];
			$end_mon = $setting['end_mon'];
			$end_day = $setting['end_day'];
		}
		
		$time_scope = $this -> get_time_scope($year, $mon, $start_day, $end_day, $end_mon);
		$now = time();
		if ($now > $time_scope['start']) {
			$gone_days = intval((time() - $time_scope['start']) / (24 * 3600));

		} else {
			$time_start = mktime(0, 0, 0, $mon-1, $start_day, $year);
			$gone_days = intval((time() - $time_start) / (24 * 3600));
		}

		$query_start_time = mktime(0, 0, 0, $mon, 1, $year);
		$firstday = date('Y-m-d', $query_start_time);
		$query_end_time = strtotime("{$firstday} +1 month -1 day");
		$query_end_time = $query_end_time + (48 * 3600) - 1;
		
		$fields = '*';
		$sql = sprintf(" SELECT %s FROM %s WHERE com_id=%s AND user_id=%s AND day>='%s' AND day<'%s' ORDER BY day, sign_group ",
			$fields, self::$Table, $com_id, $user_id, date('Ymd', $query_start_time), date('Ymd', $query_end_time));
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 获取员工月度工作时长
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $year
	 * @param unknown_type $mon
	 */
	public function get_mon_report($com_id, $user_id, $year, $mon) {
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$year = intval($year);
		$mon = intval($mon);
		$mon_time = get_month_time($year, $mon);
		
		$rec_cond = " r1.com_id = {$com_id} and r1.user_id = {$user_id} ";
		$mon < 10 && $mon = '0'.$mon;
		$rec_cond .= " and r1.day like '{$year}{$mon}%%' ";
		
		$sql = <<<EOF
		SELECT r1.user_id, r1.day, r1.sign_group, r1.state AS signin_state, r1.update_time AS signin_time, r1.id AS signin_id,
			r2.state AS signout_state, r2.update_time AS signout_time, r2.id AS signout_id
		FROM cwork_record r1
			LEFT JOIN cwork_record r2 ON ( r1.com_id=r2.com_id AND r1.user_id=r2.user_id AND r1.id!=r2.id AND r1.sign_group=r2.sign_group AND r1.day=r2.day )
		WHERE r1.sign_type IN(0,2) AND {$rec_cond}
		ORDER BY day, sign_group
EOF;
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 获取时间范围
	 * @param unknown_type $year		年份
	 * @param unknown_type $mon			月份
	 * @param unknown_type $start_day	起始日期
	 * @param unknown_type $end_day		截止日期(含截止日期)
	 * @param unknown_type $is_next_mon	0当月1次月
	 * 
	 * @return 起止的time数组
	 */
	private function get_time_scope($year, $mon, $start_day, $end_day, $is_next_mon) {		
		$max_day = $this -> get_mon_len($year, $mon);
		$next_year = ($mon == 12) ? $year+1 : $year;
		$next_mon = ($mon == 12) ? 1 : $mon+1;
		$next_max_day = $this -> get_mon_len($next_year, $next_mon);
		$next_mon = strlen($next_mon) < 2 ? '0'.$next_mon : $next_mon; 
		
		$mon = strlen($mon) < 2 ? '0'.$mon : $mon; 
		$start_day = ($start_day > $max_day) ? $max_day : $start_day;
		$start_day = strlen($start_day) < 2 ? '0'.$start_day : $start_day;
		$start_str = $year.$mon.$start_day;
		
		if ($is_next_mon) {
			$end_day = ($end_day > $next_max_day) ? $next_max_day : $end_day;
			$end_day = strlen($end_day) < 2 ? '0'.$end_day : $end_day;
			$end_str = $next_year.$next_mon.$end_day;
			
		} else {
			$end_day = ($end_day > $max_day) ? $max_day : $end_day;
			$end_day = strlen($end_day) < 2 ? '0'.$end_day : $end_day;
			$end_str = $year.$mon.$end_day;
		}
		
		$result = array(
			'start' => strtotime($start_str.'000000'),
			'end' => strtotime($end_str.'235959'),
		);
		return $result;
	}
	
	/**
	 * 获取月份天数
	 * @param unknown_type $year
	 * @param unknown_type $mon
	 */
	private function get_mon_len($year, $mon) {		
		switch ($mon) {
			case 1:		case 3:		case 5:		case 7:		case 8:		case 10:		case 12:
				return 31;
			case 4:		case 6:		case 9:		case 11:		
				return 30;
			case 2:
				if (($year % 4 == 0 && $year % 100 != 0) || ($year % 400 == 0) ) {
					return 29;
				} else {
					return 28;
				}
		}
		return 0;
	}
	
	/**
	 * 将时间转换成Date方便比较
	 * @param unknown_type $time		时间，如：12:59
	 * @param unknown_type $reset_time	新考勤时间
	 */
	private function trans_sign_time($time, $reset_time) {
		$reset_time = intval(str_replace(':', '', $reset_time));
		strlen($time) == 4 && $time = '0'.$time;			//左补0
		$time_str = intval(str_replace(':', '', $time));
		if ($time_str >= $reset_time || $time_str == 0) {	//不跨天
			$date = date('2015-01-01 '.substr($time, 0, 2).':'.substr($time, 3, 2).':00');
		} else {											//跨天
			$date = date('2015-01-02 '.substr($time, 0, 2).':'.substr($time, 3, 2).':00');
		}
		return strtotime($date);
	}
	
}

//end of file