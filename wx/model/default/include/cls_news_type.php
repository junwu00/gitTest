<?php
/**
 * 信息发布类型处理类
 * @author yangpz
 * @date 2015-06-13
 */

class cls_news_type {
	
	private static $TABLE = 'news_type';
	
	/** 删除 0 */
	private static $STATE_DEL 	= 0;
	/** 启用 1 */
	private static $STATE_ON 	= 1;
	/** 禁用 2 */
	private static $STATE_DFF 	= 2;
	
	/** 默认查找的字段 */
	private static $DEF_FIELD = 'id, name';
	
	/**
	 * 根据ID获取信息发布分类 
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $id		分类ID
	 * @param unknown_type $fields	查找的字段
	 * @throws \SCException
	 */
	public function get_by_id($com_id, $id, $fields='') {
		empty($fields) && $fields = self::$DEF_FIELD;
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $id,
			'state!=' => self::$STATE_DEL
		);
		$ret = g('ndb') -> select(self::$TABLE, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
}

//end