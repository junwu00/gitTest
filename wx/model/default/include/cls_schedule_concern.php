<?php
/**
 * 工作日程
 * 
 * @author chenyihao
 * @date 2014-12-09
 *
 */
class cls_schedule_concern {
	
	private static $Table = 'schedule_concern';
	private static $UserTable = 'sc_user';
	private static $ScheduleTable = 'schedule_schedule';
	
	
	/**
	 * 根据ID或者ID数组删除常用联系人
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $group_id
	 * @param unknown_type $group_name
	 */
	public function delete_concern($com_id,$user_id,$conern_id){
		$cond = array(
			'user_id=' => $user_id,
			'com_id=' => $com_id,
		);
		
		if(is_array($conern_id)){
			$cond['__OR'] = array();
			foreach ($conern_id as $key => $id){
				$id = (int)$id;
				!empty($id) && $cond['__OR']['__'.$key.'__concern_id='] = $id; 
			}
		}else{
			$conern_id = (int)$conern_id;
			if(!empty($conern_id)){
				$cond['concern_id='] = $conern_id;
			}
		}
		
		$data = array(
			'info_state' => 0,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	
	/**
	 * 添加常用联系人
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $see_id
	 */
	public function add_concern($com_id,$user_id,$concern_ids){
		$time = time();
		if(!is_array($concern_ids)){
			return false;
		}
		
		$value_sql = '';
		foreach($concern_ids as $id){
			$id = (int)$id;
			if(!empty($id)){
				$value_sql .= " , ({$user_id},{$id},{$com_id},{$time},1)";
			}
		}
		$value_sql = substr($value_sql,3);
		
		$sql = "INSERT INTO ".self::$Table." (user_id,concern_id,com_id,create_time,info_state) VALUES{$value_sql} ON DUPLICATE KEY UPDATE create_time = {$time}, info_state = 1;";
		$ret = g('db') ->query($sql);
		return $ret === false ? false : true; 
	}
	
	/**
	 * 获取我的常用联系人（不分页）
	 * @param unknown_type $user_id
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function get_concern($com_id,$user_id,$fields='*',$detail = FALSE){
		
		$table = self::$Table;
		
		if($detail){
			$table = self::$Table . ' c LEFT JOIN '.self::$UserTable . ' u ON u.id = c.concern_id ';
			$cond = array(
				'c.user_id=' => $user_id,
				'c.com_id=' => $com_id,
				'c.info_state=' => 1,
				'u.state!=' => 0,
			);
		}else{
			$cond = array(
				'user_id=' => $user_id,
				'com_id=' => $com_id,
				'info_state=' => 1,
			);
		}
		$ret = g('ndb') ->select($table,$fields,$cond);
		return empty($ret)? false :$ret;
	}
	
}

// end of file