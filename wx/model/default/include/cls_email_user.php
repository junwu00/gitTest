<?php
/**
 * 企业成员
 * 
 * @author yangpz
 * @date 2014-10-22
 *
 */
class cls_email_user {
	
	private static $Table = 'email_user';
	/**
	 * 查找用户
	 * @return 
	 */
	public function search( $arr="") {
	    $str = " where ";
	    $count = 1;
	    if($arr!=""){
			foreach ($arr as $k => $a){
				$str = $str.self::$Table.'.'.$k.' like "%'.$a.'%" ';
				if ($count<count($arr)){
					$str = $str.' or ';
				}
				$count++;
			}
			$str = $str;
	    }
		
		$sql = 'select * from '.self::$Table.' '.$str;
		$user = g('db') -> select($sql);
		
		return $user ? $user : false;
	}

	/**
	 * 根据user_id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_ids($uid, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $uid, 'user_id', $fields);
	}

	/**
	 * 根据id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_email($id, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $id, 'id', $fields);
	}
	/**
	 * 更新
	 * @param unknown_type $condition  array 条件
	 * @param unknown_type $data	更新哪些字段
	 */
	public function update($id,$email,$password) {
		$data = array(	
			'email' => $email,
			'password' => g('des')->encode($password),
		);
		try{
			$result = g('ndb') -> update(self::$Table, $id, $data);
			if(!$result)throw new SCException('密码或邮箱有误');
	    }catch(SCException $e){
	    	g('db')->rollback();
			throw $e;
	    }
	}
	/**
	 * 插入个人常用联系人
	 * @param unknown_type $dept_id		企业对应的dept_id
	 * @param unknown_type $user_info	用户数据
	 * @return 信息不会或插入失败：FALSE；成功：用户ID
	 */
	public function save($id,$email,$password) {
		$data = array(
			'user_id' => $id,
			'email' => $email,
			'password' => g('des')->encode($password),
			'create_date'=>time(),
		);
		try{
			$result =  g('ndb') -> insert(self::$Table, $data);
			if(!$result)throw new SCException('密码或邮箱有误');
	    }catch(SCException $e){
	    	g('db')->rollback();
			throw $e;
	    }
	}
}

// end of file