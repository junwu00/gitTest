<?php
/**
 * cookie操作类
 * @author yangpz
 * @date 2014-12-20
 */
class cls_cookie {
	/** cookie的有效时长，一周 */
	private $MaxAliveTime = 604800; //有效时间一周，即7*24*3600
	
	//redis_cookie是否可用
	private $use_rcookie = TRUE;
	//使用使用公众号对应的openid作为rcookie_id
	private $use_openid = FALSE;

	//当redis_cookie可用时，是否清空客户端cookie以提高页面响应速度
	private $clear_cookie = false;

	public function __construct() {
		$exts = load_extensions();
		!in_array('redis_cookie', $exts) and $this -> use_rcookie = FALSE;
	}

	/**
	 * cookie初始化操作
	 *
	 * @access public
	 * @return boolean
	 */
	public function cookie_start() {
		if (!$this -> use_rcookie) return TRUE;

		if (!$this -> use_openid) {
			g('rcookie') -> start();
			return TRUE;
		}

		//rcookie_id是否已经被保存在客户端的浏览器中
		$check1 = g('rcookie') -> check_id_exists();
		//是否微信客户端打开
		$check2 = g('common') -> is_wx();
		if ($check1 or !$check2) {
			g('rcookie') -> start();
			return TRUE;
		}

		$open_id = '';
		g('rcookie') -> rcookie_id($open_id);
		//重新跳转到原url，避免code、state参数干扰后面的企业号授权操作
		$this_url = get_this_url();
		$this_url = preg_replace('/code=[^=]*/', '', $this_url);
		$this_url = preg_replace('/state=[^=]*/', '', $this_url);

		header('Location:' . $this_url);
		exit;
	}
	
	/**
	 * 保存cookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function set_cookie($name, array $arr_data, $ttl=NULL) {
		empty($ttl) and $ttl = $this -> MaxAliveTime;

		if ($this -> use_rcookie) {
			$ret = g('rcookie') -> set($name, $arr_data, $ttl);
			$this -> clean_client_data();
		}else {
			$ret = $this -> set_real_cookie($name, $arr_data, $ttl);
		}
		return $ret ? TRUE : FALSE;
	}
	
	/**
	 * 获取cookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return mixed 成功返回array，否则返回FALSE
	 */
	public function get_cookie($name) {
		if ($this -> use_rcookie) {
			$ret = g('rcookie') -> get($name);
		}else {
			$ret = $this -> get_real_cookie($name);
		}

		return is_array($ret) ? $ret : FALSE;
	}

	/**
	 * 清除单个cookie变量
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return boolean
	 */
	public function clean_cookie($name) {
		if ($this -> use_rcookie) {
			$ret = g('rcookie') -> delete($name);
		}else {
			$ret = $this -> clean_real_cookie($name);
		}
		return $ret ? TRUE : FALSE;
	}

	/**
	 * 清除除session_id、rcookie_id外的全部客户端cookie数据
	 *
	 * @access private
	 * @return void
	 */
	private function clean_client_data() {
		if (!$this -> clear_cookie) return;
		if (count($_COOKIE) <= 2) return;
		$sess_id = session_id();
		$rcookie_id = g('rcookie') -> rcookie_id();
		foreach ($_COOKIE as $key => $val) {
			if ($val == $sess_id or $val == $rcookie_id) continue;
			log_write('清除client_cookie变量[' . $key . ']');
			$this -> clean_real_cookie($key);
		}
		unset($val);
	}

	/**
	 * 保存cookie数据到客户端
	 *
	 * @access public
	 * @param string $name 变量名
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function set_real_cookie($name, $data, $ttl=NULL) {
		empty($ttl) and $ttl = $this -> MaxAliveTime;
		$valid_time = time() + $ttl;

		is_array($data) and $data = json_encode($data);
		$data_str = g('des') -> encode($data);

		$ret = setcookie($name, $data_str, $valid_time, '/', SYSTEM_COOKIE_DOMAIN, false, TRUE);
		return $ret ? TRUE : FALSE;
	}
	
	/**
	 * 获取http头信息的cookie数据
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return boolean
	 */
	public function get_real_cookie($name) {
		if (!isset($_COOKIE[$name])) return FALSE;

		$data_str = $_COOKIE[$name];
		$data = g('des') -> decode($data_str);
		$data = json_decode($data, TRUE);
		
		if (!is_array($data)) {
			//获取失败清除cookie
			$this -> clean_real_cookie($name);
			return FALSE;
		}

		return $data;
	}
	
	/**
	 * 清除客户端单个真实的cookie变量
	 *
	 * @access public
	 * @param string $name 变量名
	 * @return boolean
	 */
	public function clean_real_cookie($name) {
		$ret = setcookie($name, '', time() - 3600, '/', SYSTEM_COOKIE_DOMAIN, false, TRUE);
		return $ret ? TRUE : FALSE;
	}
}

/* End of this file */