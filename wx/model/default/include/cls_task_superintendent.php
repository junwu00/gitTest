<?php
/**
 * 任务监督人信息
 * @author lijuns
 * @date 2014-12-05
 */
class cls_task_superintendent extends cls_task_base{
	
	/**
	 * 插入数据
	 * 
	 * @throws SCException
	 */
	public function save($superintendent_info) {
		$data = array(
			'taskid' => $superintendent_info['taskid'],
			'superintendent_id' => $superintendent_info['superintendent_id'],
			'superintendent_name' => $superintendent_info['superintendent_name'],
			'info_state' => parent::$StateOn,
			'create_time' => time()
		);
		$ret = g('ndb') -> insert(parent::$TableSuperintendent, $data);
		if (!$ret) {
			throw new SCException('保存任务监督信息失败');
		}
		
		return $ret;
	}
	
	/**
	 * 批量插入监督人信息
	 */
	public  function batch_save($superintendent_info_arr) {
		$ret = g('ndb') -> batch_insert(parent::$TableSuperintendent,"taskid,superintendent_id,superintendent_name,info_state,create_time",$superintendent_info_arr);
		if (!$ret) {
			throw new SCException('批量保存承办人信息失败');
		}
		return $ret;
	}
	
	
	/**
	 * 删除任务信息
	 */
	public function delete($taskid){
		$data = array(
			'info_state' => parent::$StateOff,
			'info_time' =>time()
		);
		$cond = array(
			'taskid='=>$taskid
		); 
		$ret = g('ndb') -> update_by_condition(parent::$TableSuperintendent, $cond, $data);
		
		if (!$ret) {
			throw new SCException('删除任务监督信息失败');
		}
		
		return $ret;
	}
	
	
	
	
	/**
	 * 根据taskid获取监督人信息
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function get_by_com($taskid, $fields='*') {
		$cond = array(
			'taskid=' => $taskid,
			'info_state=' => parent::$StateOn,
		);
		return g('ndb') -> select(parent::$TableSuperintendent, $fields, $cond, 0, 0, '', ' order by id asc ');
	}
	
	
	
}

// end of file