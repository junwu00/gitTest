<?php
/**
 * 微信卡券类库
 *
 * @author JianMing Liang
 * @create 2016-11-29 16:03:42
 */

class cls_wxcard {
    private $card_table = "pg_funday_wxcard";

    private $com_id;
    private $user_id;
    private $detail_url;
    private $logo_url;

    public function __construct() {
        $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];

        $this -> detail_url = "https://www.easywork365.com?";
        $this -> logo_url = "http://svc.pg.3ruler.com:8501/data/qy/image/resize/405/e34/49a/405e3449aa44adba6b939e97e7e76b27.png";
    }

    // /**
    //  * 增加微信卡券的发布记录
    //  *
    //  * @access public
    //  * @param array $card_arr 卡券信息数组
    //  * @return boolean
    //  */
    // public function add_record(array $card_arr) {
    //     $data = array(
    //         "com_id" => $this -> com_id,
    //         "user_id" => $this -> user_id,
    //         "card_data" => json_encode($card_arr),
    //         "create_time" => time(),
    //     );

    //     $ret = g("ndb") -> insert($this -> card_table, $data);
    //     return $ret;
    // }

    /**
     * 获取全部的卡券签名
     *
     * @access public
     * @param array $card_ids 卡券id集合
     * @return array
     */
    public function get_card_exts(array $card_ids) {
        $ret = array();

        foreach ($card_ids as $val) {
            $ext = $this -> get_single_card_ext($val);

            if ($ext) {
                $ret[] = array(
                    "card_id" => $val,
                    "card_ext" => $ext,
                );
            }
        }

        return $ret;
    }

    /**
     * 批量创建卡券
     *
     * @access public
     * @param array $card_arr 卡券信息数组  
     * @return array
     */
    public function create_cards(array $card_arr) {
        $card_ids = array();

        $card_mods = array();
        foreach ($card_arr["codes"] as $val) {
            $tmp_url = $this -> detail_url . "code={$val}";
            $card_mods[] = $this -> get_card_module($tmp_url);
        }

        foreach ($card_arr["urls"] as $val) {
            $tmp_str = urlencode($val);
            $tmp_url = $this -> detail_url . "url={$tmp_str}";
            $card_mods[] = $this -> get_card_module($tmp_url);
        }

        foreach ($card_mods as $val) {
            $id = $this -> create_single_card($val);
            if ($id) {
                $card_ids[] = $id;
            }
        }

        return $card_ids;
    }

    private function create_single_card(array $card_mod) {
        $token = g("atoken") -> get_access_token_by_com($this -> com_id);
        $api_url = "https://qyapi.weixin.qq.com/cgi-bin/card/create?access_token={$token}";

        $result = cls_http::post($url, json_encode($card_mod));
        
        if (!is_array($result) || $result["httpcode"] != 200) {
            return false;
        }
        
        $content = json_decode($result["content"], TRUE);
        if ($content["errcode"] != 0) {
            return false;
        }
        
        $ret = $content["card_id"];
        return $ret;
    }

    private function get_card_module($url) {
        $card_mod = array(
            "card" => array(
                "card_type" => "GENERAL_COUPON",
                "general_coupon" => array(
                    "base_info" => array(
                        "bind_openid" => false,
                        "brand_name" => "宝洁工会同乐日奖券",
                        "can_give_friend" => true,
                        "can_share" => false,
                        "code_type" => "CODE_TYPE_TEXT",
                        "color" => "Color101",
                        "custom_url" => $url,
                        "custom_url_name" => "奖券使用说明",
                        "custom_url_sub_title" => "查看奖券串码",
                        "date_info" => array(
                            "begin_timestamp" => time(),
                            "end_timestamp" => strtotime("+3 years"),
                            "type" => "DATE_TYPE_FIX_TIME_RANGE",
                        ),
                        "description" => "此处信息无效,请返回查看奖券说明!",
                        "get_limit" => 1,
                        "logo_url" => $this -> logo_url,
                        "notice" => "此处信息无效,请返回查看奖券说明!",
                        "sku" => array(
                            "quantity" => 1,
                        ),
                    ),
                ),
            ),
        );

        $ret = urlencode_array($card_mod);
    }

    // 获取api_ticket
    private function get_api_ticket() {
        $cache_str = sprintf("%s:%s->%s", SYSTEM_HTTP_DOMAIN, __CLASS__, __FUNCTION__);
        $cache_key = hash("sha1", $cache_str);
        $cache = g("redis") -> get($cache_key);
        if ($cache) {
            return $cache;
        }

        $token = g("atoken") -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID]);
        $api_url = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token={$token}&type=wx_card";
        $res = cls_http::get($api_url);
        if ($res["httpcode"] !== 200) {
            return false;
        }

        $data = json_decode($res["content"], true);
        !is_array($data) and $data = array();
        if (!isset($data["errcode"]) or $data["errcode"] !== 0) {
            return false;
        }

        $ticket = isset($data["ticket"]) ? $data["ticket"] : "";
        if ($ticket == "") {
            return false;
        }

        g("redis") -> setex($cache_key, $ticket, 80);
        return $ticket;
    }

    /**
     * 获取卡券的签名信息
     *
     * @access private
     * @param integer  
     * @return string
     */
    private function get_single_card_ext($card_id, $card_ext=array()) {
        $ticket = $this -> get_api_ticket();
        if (empty($ticket)) {
            return false;
        }

        $card_ext["timestamp"] = strval(time());
        $card_ext["nonce_str"] = get_rand_string(8);

        $tmp_arr = array_values($card_ext);
        $tmp_arr[] = $card_id;
        $tmp_arr[] = $ticket;
        sort($tmp_arr);

        $tmp_str = "";
        foreach ($tmp_arr as $val) {
            $tmp_str .= $val;
        }

        $card_ext["signature"] = sha1($tmp_str);

        return json_encode($card_ext);
    }
}