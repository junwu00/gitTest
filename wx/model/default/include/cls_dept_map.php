<?php
/**
 * 部门映射类
 * 
 * @author yangpz
 * @date 2014-10-20
 *
 */
class cls_dept_map {
	/** 对应的库表名称 */
	private static $Table = 'sc_dept_map';
	private static $DeptTable = 'sc_dept';
	
	
	/**
	 * 插入数据
	 * @param unknown_type $did	本地数据库对应的部门ID  $wid 微信数据库对应ID
	 * @return TRUE or false
	 */
	public function insert($did,$wid) {
		//此方法已废弃;
		return false;
		$data = array(
			'id' => $did,
			'wx_id' => $wid
		);
		log_write('insert dept map：'.json_encode($data));
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('添加部门映射关系失败');
		}
		return $ret;
	}
	
	/**
	 * 根据本地部门ID查找微信部门ID
	 * @param unknown_type $did	本地数据库对应的部门ID  $wid 微信数据库对应ID
	 * @return TRUE or false
	 */
	public function find_by_did($did) {
        $fields = '*';
        
		$condition = array(
			'id=' => $did,
			'state!=' => 0 
		);
		$result = g('ndb') -> select(self::$DeptTable, $fields,$condition,'','','',' order by id desc ');
		return $result?$result[0]:false;
	}
	
	/**
	 * 根据微信部门ID查找本地部门ID
	 * @param unknown_type $did	本地数据库对应的部门ID  $wid 微信数据库对应ID
	 * @return TRUE or false
	 */
	public function find_by_wid($wid) {

		$fields = '*';
		$condition = array(
			'wx_id=' => $wid,
		);
		$result = g('ndb') -> select(self::$DeptTable, $fields,$condition,'','','',' order by id desc ');
		return $result?$result[0]:false;
	}
}

// end of file