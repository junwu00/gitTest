<?php
/**
 * 人力招聘数据模型
 *
 * @author LiangJianMing
 * @create 2015-03-12
 */
class cls_recruit {
	private $app_name = 'recruit';

	//公司id
	private $com_id   = NULL;
	//用户id
	private $user_id  = NULL;
	//基础条件
	private $base_con = NULL;

	//职位配置表
	public $dc_table = 'recruit_job_conf';
	//公司信息配置表
	public $cc_table = 'recruit_company_conf';
	//其他配置表
	public $oc_table = 'recruit_other_conf';
	//职位信息表
	public $ji_table = 'recruit_job_info';
	//职位要求表
	public $jd_table = 'recruit_job_demand';
	//职位统计表
	public $jc_table = 'recruit_job_count';
	//简历信息表
	public $ri_table = 'recruit_resume_info';
	//悬赏奖励表
	public $aw_table = 'recruit_award';
	//悬赏提现表
	public $ex_table = 'recruit_extract';
	//用户红包余额表
	public $bal_table = 'recruit_balance';
	//用户转发记录表
	public $ts_table = 'recruit_transpond';

	//用户信息表
	public $user_table 	 = 'sc_user';
	//部门信息表
	public $dept_table 	 = 'sc_dept';
	//公司信息表
	public $com_table 	 = 'sc_company';

	//默认异常信息
	private $default_tips = '系统正忙，请稍后再试！';

	//默认微信端url前缀
	private $recruit_url = '';

	//触发事件，获得红包时的提示消息
	public $event_tip = NULL;

	//hr入口消息
	public $hr_tip = NULL;

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : NULL;
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);

		$this -> recruit_url = SYSTEM_HTTP_DOMAIN . '?app=recruit';

		$this -> event_tip = array(
			'title' 		=> '红包快到兜里来',
			'description' 	=> '恭喜获得1个悬赏红包，您推荐的职位《%job_name%》有小伙伴%event_desc%',
			'url'			=> '',
			'picurl'		=> '',
		);

		$this -> hr_tip = array(
			'title' 		=> '欢迎，点击即可进入HR管理页面',
			'description' 	=> '',
			'url'			=> '',
			'picurl'		=> '',
		);
	}

	/**
	 * 重新初始化相关变量
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @param string $acct 微信端账号
	 * @return void
	 */
	public function reinit($com_id, $acct) {
		$this -> com_id = $com_id;
		try {
			$this -> user_id = $this -> get_id_by_acct($acct);
		}catch(SCException $e) {
			throw $e;
		}
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
	}

	/**
	 * 根据微信端的用户账号获取员工的用户id
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @param string $acct 员工账号
	 * @return integer
	 * @throws SCException
	 */
	public function get_id_by_acct($acct) {
		$sql = 'SELECT id FROM `' . $this -> user_table . '` WHERE acct=\'' . $acct . '\' AND state=1 LIMIT 1';
		$user_id = g('db') -> select_first_val($sql);
		if (!$user_id) {
			throw new SCException('查询用户id失败！');
		}
		return $user_id;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 更新用户余额
	 *
	 * @access public
	 * @param integer $money 增/减量数，包含两位小数
	 * @param integer $type 类型：1-增加、2-减少
	 * @param integer $user_id 用户id
	 * @param integer $com_id 企业id
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_com_balance($money, $type=1, $com_id=0) {
		empty($com_id) and $com_id = $this -> com_id;
		$char = $type == 1 ? '+' : '-';
		$sql = <<<EOF
UPDATE {$this -> oc_table} SET award_balance=award_balance{$char}{$money} 
WHERE com_id={$com_id} 
EOF;
		$result = g('db') -> query($sql);
		if (!$result) {
			throw new SCException('更新企业悬赏余额失败！');
		}

		return TRUE;
	}

	/**
	 * 获取职位列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_job_list(array $condition=array(), $page=1, $page_size=10, $order='') {
		empty($condition['com_id=']) and $condition['com_id='] = $this -> com_id;
		//只获取发布状态的职位
		$condition['state='] = 2;

		$tmp_arr = array();
		foreach ($condition as $key => $val) {
			$tmp_arr['ji.' . $key] = $val;
		}
		unset($val);
		$condition = $tmp_arr;

		$str = __FUNCTION__ . ':condition:' . json_encode($condition) . ':page:' . $page . ':page_size:' . $page_size . ':order:' . $order;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$table = $this -> ji_table . ' AS ji, ' . $this -> jc_table . ' AS jc';
		$fields = array(
			'ji.rji_id', 'ji.name', 'ji.share_type', 
			'ji.share_limit', 'ji.resume_type', 'ji.resume_limit', 
			'ji.hire_type', 'ji.hire_limit', 'ji.end_time', 
			'jc.click_count', 'jc.resume_count',
		);
		$fields = implode(',', $fields);
		$condition['^ji.rji_id='] = 'jc.rji_id';

		empty($order) and $order = ' ORDER BY ji.`sort` DESC, ji.rji_id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order);
		!is_array($ret) and $ret = array();
		$ret = $this -> replace_data($ret);

		g('redis') -> setex($cache_key, json_encode($ret), 10);
		//log_write('job_list=' . json_encode($ret));

		return $ret;
	}

	/**
	 * 获取职位相关的详细信息
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @return array
	 * @throws SCException
	 */
	public function get_job_detail(array $condition) {
		$table = $this -> ji_table . ' AS ji, ' . 
					$this -> jc_table . ' AS jc, ' . 
					$this -> dept_table . ' AS dept, ' . 
					$this -> cc_table . ' AS com';
		
		$more_condition = array(
			'^ji.rji_id=' => 'jc.rji_id',
			'^ji.dept_id=' => 'dept.id',
			'^ji.com_id=' => 'com.com_id',
		);
		$condition = array_merge($condition, $more_condition);

		$str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$fields = array(
			'ji.rji_id', 'ji.name', 'ji.state',
			'ji.create_time', 'ji.number', 'ji.email', 'ji.end_time',
			'ji.work_place', 'ji.treatment', 'ji.job_nature',
			'ji.education_id', 'ji.experience_id', 'ji.treatment_id',
			'ji.education', 'ji.experience', 'ji.with_manage',
			'ji.description', 'ji.share_type', 'ji.share_limit',
			'ji.resume_type', 'ji.resume_limit', 'ji.hire_type',
			'ji.hire_limit', 'dept.name AS dept_name', 
			'jc.click_count', 'com.name AS com_name', 'com.logo',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException('查询失败，该职位可能已被删除！');
		}

		$ret = $ret[0];

		//工作性质可用选项
		$nature_list = g('recruit_dict') -> nature_list;
		//薪资待遇可用选项
		$treatment_list = g('recruit_dict') -> treatment_list;
		//学历可用选项
		$education_list = g('recruit_dict') -> education_list;
		//工作经验可用选项
		$experience_list = g('recruit_dict') -> experience_list;

		if ($ret['treatment_id'] == 99) {
			$ret['treatment'] = json_decode($ret['treatment'], TRUE);
			$ret['treatment_desc'] = '￥' . number_format($ret['treatment']['left']) . '-￥' . number_format($ret['treatment']['right']) . '/月';
		}else {
			$ret['treatment_desc'] = $treatment_list[$ret['treatment_id']];
		}

		if ($ret['experience_id'] == 99) {
			$ret['experience'] = json_decode($ret['experience'], TRUE);
			$ret['experience_desc'] = $ret['experience']['left'] . '-' . $ret['experience']['right'] . '年';
		}else {
			$ret['experience_desc'] = $experience_list[$ret['experience_id']];
		}

		if ($ret['education_id'] == 99) {
			$ret['education_desc'] = $ret['education'];
		}else {
			$ret['education_desc'] = $education_list[$ret['education_id']];
		}
		$ret['nature_desc'] = $nature_list[$ret['job_nature']];

		$ret = $this -> replace_data($ret);

		g('redis') -> setex($cache_key, json_encode($ret), 10);

		return $ret;
	}

	/**
	 * 获取职位简历要求
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @return array
	 * @throws SCException
	 */
	public function get_job_demand(array $condition) {
		$table = $this -> ji_table . ' AS ji, ' . $this -> jd_table . ' AS jd';
		
		$more_condition = array(
			//保证是发布中状态
			'ji.state=' => 2,
			'^ji.rji_id=' => 'jd.rji_id',
		);
		$condition = array_merge($condition, $more_condition);

		$str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$fields = array(
			'ji.rji_id', 'ji.name', 'ji.end_time',
			'jd.use_name', 'jd.use_mobile', 'jd.use_portrait',
			'jd.use_gender', 'jd.use_age', 'jd.use_salary',
			'jd.use_dwell', 'jd.use_experience', 'jd.use_degree',
			'jd.use_job', 'jd.use_email', 'jd.custom_info',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException('查询失败，该职位可能已被删除！');
		}

		$ret = $ret[0];
		$tmp_info = json_decode($ret['custom_info'], TRUE);
		$ret['custom_info'] = is_array($tmp_info) ? $tmp_info : array();

		$ret = $this -> replace_data($ret);

		g('redis') -> setex($cache_key, $ret, 10);

		return $ret;
	}

	/**
	 * 获取简历列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_resume_list(array $condition, $page=1, $page_size=10, $order='') {
		$table = $this -> ri_table . ' AS ri, ' . $this -> user_table . ' AS user, ' . $this -> ji_table . ' AS ji';

		$condition['ri.com_id='] = $this -> com_id;
		$condition['ji.hr_id='] = $this -> user_id;
		$condition['ri.state!='] = 7;
		$condition['^ri.user_id='] = 'user.id';
		$condition['^ri.rji_id='] = 'ji.rji_id';

		$fields = array(
			'ri.id', 'ri.rji_id', 'ri.user_id',
			'ri.name', 'ri.mobile', 'ri.portrait',
			'ri.salary', 'ri.create_time', 'ri.state',
			'ri.is_collected','ri.degree','ri.experience','ri.gender',
			'user.name AS user_name', 'ji.name as job_name',
		);
		$fields = implode(',', $fields);

		empty($order) and $order = ' ORDER BY ri.id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order);
		!is_array($ret) and $ret = array();
		$ret = $this -> replace_data($ret);

		return $ret;
	}

	/**
	 * 更新简历信息，主要用作更改简历的状态
	 *
	 * @access public
	 * @param mixed $ids 支持一个或多个
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_resume($ids, array $data) {
		$condition = $this -> base_con;
		if (is_array($ids)) {
			$condition['id IN '] = '(' . implode(',', $ids) . ')';
		}else {
			$condition['id='] = $ids;
		}

		$result = g('ndb') -> update_by_condition($this -> ri_table, $condition, $data);
		if (!$result) {
			throw new SCException($this -> default_tips);
		}

		return TRUE;
	}

	/**
	 * 获取简历详情
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @return array
	 * @throws SCException
	 */
	public function get_resume_detail(array $condition) {
		$table = $this -> ri_table . ' AS ri, ' . $this -> user_table . ' AS user, ' . $this -> ji_table . ' AS ji';

		$more_condition = array(
			'ri.com_id=' => $this -> com_id,
			'ji.hr_id=' => $this -> user_id,
			'ri.state!=' => 7,
			'^ri.user_id=' => 'user.id',
			'^ri.rji_id=' => 'ji.rji_id',
		);
		$condition = array_merge($condition, $more_condition);

		$fields = array(
			'ri.id', 'ri.rji_id',
			'ri.name', 'ri.mobile', 'ri.portrait',
			'ri.gender', 'ri.age', 'ri.salary',
			'ri.dwell', 'ri.experience', 'ri.degree',
			'ri.job', 'ri.email', 'ri.other_info',
			'ri.create_time', 'ri.state', 'ri.accessory',
			'ri.is_collected',
			'user.name AS user_name', 'ji.name as job_name',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException('找不到该简历，可能无权查看或已被删除！');
		}

		$ret = $ret[0];
		$tmp_arr = json_decode($ret['other_info'], TRUE);
		empty($tmp_arr) and $tmp_arr = array();
		$ret['other_info'] = $tmp_arr;

		//附件集合
		$tmp_arr = json_decode($ret['accessory'], TRUE);
		empty($tmp_arr) and $tmp_arr = array();
		$ret['accessory'] = $tmp_arr;

		$ret = $this -> replace_data($ret);

		return $ret;
	}

	/**
	 * 获取全部推荐者
	 *
	 * @access public
	 * @return array
	 */
	public function get_all_presenters() {
		$table = $this -> ri_table . ' AS ri, ' . $this -> user_table . ' AS user';

		$condition = array(
			'ri.com_id=' => $this -> com_id,
			'ri.state!=' => 7,
			'^ri.user_id=' => 'user.id',
		);

		$str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$fields = array(
			'ri.user_id', 'user.name',
		);
		$fields = implode(',', $fields);
		
		$group_by = ' GROUP BY ri.user_id ';
		$order_by = ' ORDER BY ri.id DESC';

		$ret = g('ndb') -> select($table, $fields, $condition, 0, 0, $group_by, $order_by);
		!is_array($ret) and $ret = array();

		$ret = $this -> replace_data($ret);

		g('redis') -> setex($cache_key, json_encode($ret), 10);

		return $ret;
	}

	/**
	 * 获取全部职位
	 *
	 * @access public
	 * @return array
	 */
	public function get_all_jobs() {
		$condition = array(
			'com_id=' => $this -> com_id,
			'state!=' => 5,
		);

		$fields = array(
			'rji_id', 'name',
		);
		$fields = implode(',', $fields);
		
		$order_by = ' ORDER BY `sort` DESC';

		$ret = g('ndb') -> select($this -> ji_table, $fields, $condition, 0, 0, '', $order_by);
		!is_array($ret) and $ret = array();

		$ret = $this -> replace_data($ret);

		return $ret;
	}

	/**
	 * 获取奖励列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_award_list(array $condition, $page=1, $page_size=10, $order='') {
		$table = $this -> aw_table . ' AS award, ' . $this -> user_table . ' AS user, ' . $this -> ji_table . ' AS ji';

		$condition['award.com_id='] = $this -> com_id;
		$condition['award.user_id='] = $this -> user_id;
		$condition['^award.user_id='] = 'user.id';
		$condition['^award.rji_id='] = 'ji.rji_id';

		$fields = array(
			'award.id', 'award.event_type','award.is_read', 
			'award.type', 'award.money', 'award.create_time', 
			'ji.name as job_name',
		);
		$fields = implode(',', $fields);

		empty($order) and $order = ' ORDER BY award.id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order);
		if (is_array($ret) and !empty($ret)) {
			foreach ($ret as &$val) {
				$val['money'] = round((float)$val['money'] / 100, 2);
			}
			unset($val);
		}

		return $ret;
	}

	/**
	 * 获取部门的信息集合
	 *
	 * @access public
	 * @param array $dept_ids 部门id集合
	 * @return array
	 */
	public function get_depts_info(array $dept_ids) {
		$ids = implode(',', $dept_ids);

		$str = __FUNCTION__ . ':ids:' . $ids;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$condition = array(
			'id IN ' => '(' . $ids . ')',
		);

		$fields = array(
			'id', 'name',
		);
		$fields = implode(',', $fields);

		$result = g('ndb') -> select($this -> dept_table, $fields, $condition);

		$ret = array();
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val['name'];
			}
			unset($val);
		}

		g('redis') -> setex($cache_key, json_encode($ret), 10);

		return $ret;
	}

	/**
	 * 获取提现列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_extract_list(array $condition, $page=1, $page_size=10, $order='') {
		$condition['com_id='] = $this -> com_id;
		$condition['user_id='] = $this -> user_id;

		$fields = array(
			'id', 'money', 'state',
			'create_time',
		);
		$fields = implode(',', $fields);

		empty($order) and $order = ' ORDER BY id DESC ';

		$ret = g('ndb') -> select($this -> ex_table, $fields, $condition, $page, $page_size, '', $order);
		if (is_array($ret) and !empty($ret)) {
			foreach ($ret as &$val) {
				$val['money'] = round((float)$val['money'] / 100, 2);
			}
			unset($val);
		}

		return $ret;
	}

	/**
	 * 获取公司的介绍信息
	 *
	 * @access public
	 * @return array
	 * @throws SCException
	 */
	public function get_company_info($com_id=0) {
		empty($com_id) and $com_id = $this -> com_id;

		$condition = array(
			'com_id=' => $com_id,
		);

		$str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if ($ret) return json_decode($ret, TRUE);

		$fields = array(
			'name', 'simple_desc', 'logo',
			'banner', 'nature_desc', 'nature_id',
			'scale_id', 'local_desc', 'local_key',
			'address', 'description', 'update_time',
			'create_time',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($this -> cc_table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException($this -> default_tips);
		}

		$ret = $ret[0];

		//企业规模可用选项
		$scale_list = g('recruit_dict') -> scale_list;

		$ret['scale_desc'] = isset($ret['scale_id']) ? $scale_list[$ret['scale_id']] : '';

		$ret = $this -> replace_data($ret);

		g('redis') -> setex($cache_key, $ret, 600);

		return $ret;
	}

	/**
	 * 对hr发送管理入口
	 *
	 * @access public
	 * @param inetger $com_id 公司id
	 * @param inetger $user_id 用户id
	 * @param boolean $resume_tip 是否简历提醒
	 * @return boolean
	 * @throws SCException
	 */
	public function send_hr_tip($com_id=0, $user_id=0, $resume_tip=FALSE) {
		empty($com_id) and $com_id = $this -> com_id;
		empty($user_id) and $user_id = $this -> user_id;

		$param = array(
			'm' => 'hr_entry',
			'a' => 'lister',
		);

		$hr_tip = $this -> hr_tip;
		$hr_tip['url'] = $this -> recruit_url . '&' . http_build_query($param);

		$resume_tip and $hr_tip['title'] = '有人投简历啦，快来处理吧~';

		$user_list = array($user_id);
		$art_list = array($hr_tip);
		g('nmsg') -> init($com_id, 'recruit');
		$ret = g('nmsg') -> send_news(array(), $user_list, $art_list);
		return $ret;
	}

	/**
	 * 获取当前可用悬赏余额
	 *
	 * @access public
	 * @param integer $com_id 公司id
	 * @param boolean $is_lock 是否锁定余额
	 * @return float
	 * @throws SCException
	 */
	public function get_now_balance($com_id=0, $is_lock=TRUE) {
		empty($com_id) and $com_id = $this -> com_id;
		$lock_str = $is_lock ? ' FOR UPDATE' : '';

		$sql = 'SELECT award_balance FROM ' . $this -> oc_table . ' WHERE com_id=' . $com_id . ' LIMIT 1' . $lock_str;
		$balance = g('db') -> select_first_val($sql);
		if ($balance === FALSE) {
			throw new SCException('获取悬赏可用余额失败！');
		}
		return $balance;
	}

	/**
	 * 触发奖励事件
	 *
	 * @access public
	 * @param integer $event_type 事件类型：1-分享被查看，2-收到简历，3-被录用
	 * @param integer $user_id 得到奖励的用户
	 * @param array $data 其他参数
	 * @return TRUE
	 * @throws SCException
	 */
	public function trigger_award($event_type, $user_id=0, array $data=array()) {
		if ($event_type == 1) {
			$id = $data['job_id'];
			$is_job_id = TRUE;
		}elseif ($event_type == 2) {
			$id = $data['job_id'];
			$is_job_id = TRUE;
		}else {
			$id = $data['resume_id'];
			$is_job_id = FALSE;
		}

		try {
			$info = $this -> get_simple_job_data($id, $is_job_id);
			$balance = $this -> get_now_balance($info['com_id']);
		}catch(SCException $e) {
			throw $e;
		}


		$rji_id = $id;
		if ($event_type == 3) {
			$user_id = $info['user_id'];
			$rji_id = $info['rji_id'];
		}

		if ($event_type == 1) {
			$index_type = 'share_type';
			$index_limit = 'share_limit';
		}elseif ($event_type == 2) {
			$index_type = 'resume_type';
			$index_limit = 'resume_limit';
		}else {
			$index_type = 'hire_type';
			$index_limit = 'hire_limit';
		}

		//无红包奖励
		if ($balance <= 0 or $info[$index_type] == 3 or $info[$index_limit] == 0) {
			return TRUE;
		}

		$user_award = 0;
		if ($info[$index_type] == 2) {
			$user_award = $info[$index_limit] * 100;
		}else {
			$tmp_limit = $info[$index_limit] * 100;
			$user_award = (float)rand(1, $tmp_limit) / 100;
			$user_award = round($user_award, 2) * 100;
			$user_award = intval($user_award);
		}

		$user_award > $balance and $user_award = $balance;

		$award_data = array(
			'rji_id' => $rji_id,
			'com_id' => $info['com_id'],
			'user_id' => $user_id,
			'event_type' => $event_type,
			'type' => $info[$index_type],
			'money' => $user_award,
		);

		try {
			$this -> add_award($award_data);
			$this -> update_com_balance($user_award, 2, $info['com_id']);
			$this -> update_user_balance($user_award, 1, $user_id, $info['com_id']);
		}catch(SCException $e) {
			throw $e;
		}

		//红包消息推送失败不报错
		$this -> send_award_tip($info['name'], $event_type, $info['com_id'], $user_id);

		return TRUE;
	}

	/**
	 * 更新用户余额
	 *
	 * @access public
	 * @param integer $money 增/减量数，包含两位小数
	 * @param integer $type 类型：1-增加、2-减少
	 * @param integer $user_id 用户id
	 * @param integer $com_id 企业id
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_user_balance($money, $type=1, $user_id=0, $com_id=0) {
		empty($user_id) and $user_id = $this -> user_id;
		empty($com_id) and $com_id = $this -> com_id;
		$char = $type == 1 ? '+' : '-';
		$time = time();

		$sql = <<<EOF
UPDATE {$this -> bal_table} SET balance=balance{$char}{$money}, update_time={$time} 
WHERE user_id={$user_id} AND com_id={$com_id}
EOF;
		$result = g('db') -> query($sql);
		if (!$result) {
			throw new SCException('更新用户余额失败！');
		}

		return TRUE;
	}

	/**
	 * 更新用户对应职位相关的统计信息
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @param integer $event_type 事件类型：1-转发、2-点击、3-投递简历、4-录用
	 * @param integer $user_id 用户id
	 * @param integer $com_id 公司id
	 * @return boolean
	 */
	public function increase_user_counts($rji_id, $event_type=1, $user_id=0, $com_id=0) {
		empty($user_id) and $user_id = $this -> user_id;
		empty($com_id) and $com_id = $this -> com_id;

		$index = 'share_count';
		$event_type == 2 and $index = 'click_count';
		$event_type == 3 and $index = 'resume_count';
		$event_type == 4 and $index = 'hire_count';

		$sql = <<<EOF
UPDATE {$this -> ts_table} SET {$index}={$index}+1
WHERE com_id={$com_id} AND user_id={$user_id} AND rji_id={$rji_id}
EOF;
		$result = g('db') -> query($sql);
		if (!$result) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 检测并完善用户的数据表初始化情况
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return TRUE
	 * @throws SCException
	 */
	public function check_user_init($rji_id=0) {
		$condition = $this -> base_con;
		$condition['user_id='] = $this -> user_id;
		$check = g('ndb') -> record_exists($this -> bal_table, $condition);
		if (!$check) {
			$data = array(
				'com_id' => $this -> com_id,
				'user_id' => $this -> user_id,
				'create_time' => time(),
			);
			$result = g('ndb') -> insert($this -> bal_table, $data);
			if (!$result) {
				throw new SCException('初始化用户余额数据失败！');
			}
		}

		if (!empty($rji_id)) {
			$condition['rji_id='] = $rji_id;
			$check = g('ndb') -> record_exists($this -> ts_table, $condition);
			if (!$check) {
				$data = array(
					'com_id' => $this -> com_id,
					'user_id' => $this -> user_id,
					'rji_id' => $rji_id,
					'create_time' => time(),
				);
				$result = g('ndb') -> insert($this -> ts_table, $data);
				if (!$result) {
					throw new SCException('初始化用户统计数据失败！');
				}
			}
		}

		return TRUE;
	}

	/**
	 * 推送奖励信息
	 *
	 * @access public
	 * @param string $job_name 职位名称
	 * @param integer $event_type 事件类型
	 * @param integer $com_id 公司id
	 * @param integer $user_id 用户id
	 * @return boolean
	 */
	public function send_award_tip($job_name, $event_type, $com_id=0, $user_id=0) {
		empty($com_id) and $com_id = $this -> com_id;
		empty($user_id) and $user_id = $this -> user_id;
		$event_tip = $this -> event_tip;
		$event_desc = '';

		if ($event_type == 1) {
			$event_desc = '查看分享链接';
		}elseif ($event_type == 2) {
			$event_desc = '投递简历';
		}else {
			$event_desc = '成功被录用';
		}

		$event_tip['description'] = str_replace('%job_name%', $job_name, $event_tip['description']);
		$event_tip['description'] = str_replace('%event_desc%', $event_desc, $event_tip['description']);

		$param = array(
			'm' => 're_award',
			'a' => 'show',
		);

		$event_tip['url'] = $this -> recruit_url . '&' . http_build_query($param);

		$user_list = array($user_id);
		$art_list = array($event_tip);

		g('nmsg') -> init($com_id, 'recruit');
		$ret = g('nmsg') -> send_news(array(), $user_list, $art_list);
		return $ret;
	}

	/**
	 * 获取用作分析的统计数据
	 *
	 * @access public  
	 * @return array
	 * @throws SCException
	 */
	public function get_analyze_count() {
		$condition = $this -> base_con;
		$condition['user_id='] = $this -> user_id;
		$condition['click_count!='] = 0;

		$fields = array(
			'COUNT(rji_id) AS job_total', 'SUM(click_count) AS click_total',
			'SUM(resume_count) AS resume_total','SUM(hire_count) AS hire_total',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($this -> ts_table, $fields, $condition);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException('查询失败，可能未初始化数据表：recruit_transpond');
		}

		$ret = $ret[0];

		empty($ret['job_total']) and $ret['job_total'] = 0;
		empty($ret['hire_total']) and $ret['hire_total'] = 0;
		empty($ret['resume_total']) and $ret['resume_total'] = 0;
		empty($ret['click_total']) and $ret['click_total'] = 0;

		return $ret;
	}

	/**
	 * 以职位作为分组条件，获取统计数据
	 *
	 * @access public
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @return array
	 * @throws SCException
	 */
	public function get_job_counts($page=1, $page_size=10) {
		$table = $this -> ts_table . ' AS ts, ' . $this -> ji_table . ' AS ji';

		$condition = array(
			'ts.com_id=' => $this -> com_id,
			'ts.user_id=' => $this -> user_id,
			'^ts.rji_id=' => 'ji.rji_id',
			'ts.click_count!=' => 0,
		);

		$fields = array(
			'ts.share_count', 'ts.click_count', 'ts.resume_count',
			'ts.hire_count',
			'ji.name as job_name','ji.hire_limit as hire_limit'
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size);

		return $ret;
	}

	/**
	 * 更新职位相关的统计信息
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @param integer $event_type 事件类型：1-转发、2-点击、3-投递简历、4-录用
	 * @param integer $com_id 公司id，用作避免权限泄露
	 * @return boolean
	 */
	public function increase_job_count($rji_id, $event_type, $com_id=0) {
		empty($com_id) and $com_id = $this -> com_id;

		$index = 'share_count';
		$event_type == 2 and $index = 'click_count';
		$event_type == 3 and $index = 'resume_count';
		$event_type == 4 and $index = 'hire_count';

		$sql = <<<EOF
UPDATE {$this -> jc_table} SET {$index}={$index}+1
WHERE rji_id={$rji_id} AND com_id={$com_id}
EOF;
		$result = g('db') -> query($sql);
		if (!$result) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 添加录用的相关统计
	 *
	 * @access public
	 * @param integer $id 简历id
	 * @return boolean
	 */
	public function add_hire_count($id) {
		$fields = array(
			'rji_id', 'com_id', 'user_id'
		);
		$fields = implode(',', $fields);

		$condition = array(
			'id=' => $id,
		);

		$ret = g('ndb') -> select($this -> ri_table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) return FALSE;

		$ret = $ret[0];
		$this -> increase_job_count($ret['rji_id'], 4, $ret['com_id']);
		$this -> increase_user_counts($ret['rji_id'], 4, $ret['user_id'], $ret['com_id']);
		return TRUE;
	}

	/**
	 * 提交简历
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function add_resume(array $data) {
		try {
			$this -> check_resume_data($data);
		}catch(SCException $e) {
			throw $e;
		}

		$time = time();
		$data['create_time'] = $time;
		$data['update_time'] = $time;

		$share_user_id = $data['user_id'];
		$share_com_id = $data['com_id'];

		g('db') -> begin_trans();
		$ri_id = $result = g('ndb') -> insert($this -> ri_table, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('提交简历失败');
		}

		$param = array(
			'com_id' => $share_com_id,
			'job_id' => $data['rji_id'],
		);
		try {
			$this -> trigger_award(2, $share_user_id, $param);
		}catch(SCException $e) {
			g('db') -> rollback();
			throw $e;
		}

		$this -> send_resume_tip($data['rji_id']);

		g('db') -> commit();


		$img_arr = array();
        !empty($data['portrait']) and $img_arr[] = $data['portrait'];
        $tmp_arr = json_decode($data['accessory'], TRUE);
        !is_array($tmp_arr) and $tmp_arr = array();
        $img_arr = array_merge($img_arr, $tmp_arr);

        foreach ($img_arr as $val) {
        	//加入容量文件列表
			g('capacity') -> add_list_by_cache($share_com_id, $val, $this -> app_name, $this -> ri_table, $ri_id);
        }
        unset($val);

		return TRUE;
	}

	/**
	 * 有新的简历时，发送提醒
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return boolean
	 */
	public function send_resume_tip($rji_id) {
		try {
			$info = $this -> get_simple_job_data($rji_id);
		}catch(SCException $e) {
			log_write('发送简历提醒失败，职位id=' . $rji_id);
			return FALSE;
		}

		$tip_str = __FUNCTION__ . '::' . $rji_id;
		$cache_key = $this -> get_cache_key($tip_str);
		$check = g('redis') -> get($cache_key);

		if (!empty($check)) {
			return FALSE;
		}

		$this -> send_hr_tip($info['com_id'], $info['hr_id'], TRUE);

		$interval = $info['remind_time'] ? intval($info['remind_time']) : 0;
		if (!empty($interval)) {
			g('redis') -> setex($cache_key, 1, $interval);
		}

		return TRUE;
	}

	/**
	 * 获取用作推送消息的职位数据
	 *
	 * @access public
	 * @param integer $id 职位id或简历id
	 * @param boolean $is_job_id $id是否职位id
	 * @return array
	 * @throws SCException
	 */
	public function get_simple_job_data($id, $is_job_id=TRUE) {
		$key_str = __FUNCTION__ . ':id:' . $id . ':is_job_id:' . $is_job_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$fields = array(
			'name', 'share_type', 'share_limit',
			'resume_type', 'resume_limit', 'hire_type',
			'hire_limit', 'rji_id', 'com_id',
			'hr_id', 'remind_time'
		);

		if ($is_job_id){
			$table = $this -> ji_table;
			$condition = array(
				//过滤删除状态
				'state!=' => 5,
				'rji_id=' => $id,
			);
		}else {
			$table = $this -> ri_table . ' AS ri, ' . $this -> ji_table . ' AS ji';
			foreach ($fields as &$val) {
				$val = 'ji.' . $val;
			}
			unset($val);

			$more_fields = array(
				'ri.user_id',
			);

			$fields = array_merge($fields, $more_fields);

			$condition = array(
				'ri.id=' => $id,
				'ji.state!=' => 5,
				'^ri.rji_id=' => 'ji.rji_id',
			);
		}

		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		if (!is_array($ret) or empty($ret)) {
			throw new SCException('查询失败，该职位可能已被删除！');
		}

		$ret = $ret[0];
		$ret = $this -> replace_data($ret);
		g('redis') -> setex($cache_key, json_encode($ret), 1800);

		return $ret;
	}

	/**
	 * 申请提现
	 *
	 * @access public
	 * @param integer $money 金额，包含两位小数
	 * @return boolean
	 */
	public function apply_extract($money) {
		g('db') -> begin_trans();

		try {
			$balance = $this -> get_user_balance(TRUE);
		}catch(SCException $e) {
			throw $e;
		}

		if ($balance < $money) {
			throw new SCException('余额不足！');
		}

		$data = array(
			'com_id' => $this -> com_id,
			'user_id' => $this -> user_id,
			'money' => $money,
		);

		try {
			$this -> add_extract($data);
			$this -> update_user_balance($money, 2);
		}catch(SCException $e) {
			g('db') -> rollback();
			throw $e;
		}

		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 查询用户的余额
	 *
	 * @access public
	 * @param boolean $is_lock 是否锁定用户余额
	 * @param boolean $with_not_open 是否包含未打开的红包余额
	 * @return array
	 * @throws SCException
	 */
	public function get_user_balance($is_lock=FALSE, $with_not_open=TRUE) {
		$lock_str = $is_lock ? ' FOR UPDATE' : '';
		$sql = <<<EOF
SELECT balance FROM {$this -> bal_table} 
WHERE com_id={$this -> com_id} AND user_id={$this -> user_id} 
LIMIT 1 {$lock_str}
EOF;
		$ret = g('db') -> select_first_val($sql);
		if ($ret === FALSE) {
			throw new SCException('找不到用户余额记录，可能未初始化！');
		}
		if ($with_not_open) return $ret;

		$sql = <<<EOF
SELECT SUM(money) as money_sum FROM {$this -> aw_table} 
WHERE com_id={$this -> com_id} AND user_id={$this -> user_id} AND is_read=0 
LIMIT 1
EOF;
		$not_open = g('db') -> select_first_val($sql);
		empty($not_open) and $not_open = 0;
		$ret -= (int)$not_open;

		return $ret;
	}

	/**
	 * 获取用户未打开的金额数
	 *
	 * @access public
	 * @param integer  
	 * @return boolean
	 */
	public function get_award_balance() {

	}

	/**
	 * 添加提现记录
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return integer
	 * @throws SCException
	 */
	public function add_extract(array $data=array()) {
		$time = time();
		$data['update_time'] = $time;
		$data['create_time'] = $time;
		$result = g('ndb') -> insert($this -> ex_table, $data);
		if (!$result) {
			throw new SCException('写入提现记录失败！');
		}

		return $result;
	}

	/**
	 * 生成一条奖励记录
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function add_award(array $data) {
		$time = time();
		$data['create_time'] = $time;

		$result = g('ndb') -> insert($this -> aw_table, $data);
		if (!$result) {
			throw new SCException('写入奖励记录失败！');
		}

		return $result;
	}

	/**
	 * 更新红包信息，主要用作更改红包的状态
	 *
	 * @access public
	 * @param array $id 红包主键id
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_award($id, array $data) {
		$condition = $this -> base_con;
		$condition['id='] = $id;

		$result = g('ndb') -> update_by_condition($this -> aw_table, $condition, $data);
		if (!$result) {
			throw new SCException($this -> default_tips);
		}

		return TRUE;
	}

	/**
	 * 简历提交后，生成cookie标识，避免重复提交
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return TRUE
	 */
	public function set_finish_cookie($rji_id) {
		$key_str = __FUNCTION__ . ':rji_id:' . $rji_id;
		$cache_key = $this -> get_cookie_key($key_str);
		g('cookie') -> set_cookie($cache_key, array(1));
		return TRUE;
	}

	/**
	 * 检测是否已经提交过该职位的简历
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return void
	 * @throws SCException
	 */
	public function check_resume_submit($rji_id) {
		$key_str = __FUNCTION__ . ':rji_id:' . $rji_id;
		$cache_key = $this -> get_cookie_key($key_str);
		//已提交过
		$check = g('cookie') -> get_cookie($cache_key);
		if (!empty($check)) {
			throw new SCException('该职位已经投过简历了~~');
		}
	}

	/**
	 * 职位查看后，生成cookie标识，避免重复生成奖励
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return TRUE
	 */
	public function set_view_cookie($rji_id) {
		$key_str = 'rji_id:' . $rji_id;
		$cache_key = $this -> get_cookie_key($key_str);
		g('cookie') -> set_cookie($cache_key, array(1));
		return TRUE;
	}

	/**
	 * 检测是否第一次查看该职位信息
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return void
	 */
	public function check_first_view($rji_id) {
		$ret = FALSE;

		$key_str = 'rji_id:' . $rji_id;
		$cache_key = $this -> get_cookie_key($key_str);
		$check = g('cookie') -> get_cookie($cache_key);
		//第一次查看
		empty($check) and $ret = TRUE;
		return $ret;
	}

	/**
	 * 常规检测
	 *
	 * @access public
	 * @return TRUE
	 * @throws SCException
	 */
	public function check_valid() {
		$check = g('common') -> is_wx();
		if (!$check) {
			//throw new SCException('请在微信客户端中打开！');
		}

		return TRUE;
	}

	/**
	 * 检测职位相关的可访问性
	 *
	 * @access public
	 * @param integer $rji_id 职位id
	 * @return TRUE
	 * @throws SCException
	 */
	public function check_job_valid($rji_id) {
		try {
			$this -> check_valid();
		}catch(SCException $e) {
			throw $e;
		}

		$key_str = __FUNCTION__ . ':rji_id:' . $rji_id;
		$cache_key = $this -> get_cache_key($key_str);
		$state = g('redis') -> get($cache_key);

		if (empty($state)) {
			$sql = <<<EOF
SELECT `state` FROM {$this -> ji_table} 
WHERE rji_id={$rji_id} AND state!=5 LIMIT 1
EOF;

			$state = g('db') -> select_first_val($sql);
			g('redis') -> setex($cache_key, $state, 10);
		}

		if ($state === FALSE) {
			throw new SCException('职位查询失败，可能已被删除！');
		}elseif ($state == 1) {
			throw new SCException('该职位尚未发布！');
		}elseif ($state == 3) {
			throw new SCException('该职位已暂停！');
		}elseif ($state == 4) {
			throw new SCException('该职位已结束！');
		}

		return TRUE;
	}

	/**
	 * 检测用户是否拥有hr权限
	 *
	 * @access public
	 * @return TRUE
	 * @throws SCExcetion
	 */
	public function check_hr_valid() {
		try {
			$this -> check_valid();
		}catch(SCException $e) {
			throw $e;
		}

		$condition = array(
			'com_id=' => $this -> com_id,
			'hr_id=' => $this -> user_id,
			'state!=' => 5,
		);
		$key_str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($key_str);
		$cache = g('redis') -> get($cache_key);
		if ($cache !== FALSE) {
			if ($cache == 1) return TRUE;
			throw new SCException('无权查看，可能是所负责的职位已被删除！');
		}

		$check = g('ndb') -> record_exists($this -> ji_table, $condition);
		if (!$check) {
			throw new SCException('无权查看，可能是所负责的职位已被删除！');
		}

		g('redis') -> setex($cache_key, 1, 10);

		return TRUE;
	}

	/**
	 * 构造自定义信息
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return string
	 */
	public function compose_cus_data(array $data=array()) {
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data = json_encode($data);

		$cipher = g('des') -> encode($data);
		return $cipher;
	}

	/**
	 * 还原密文
	 *
	 * @access public
	 * @param string $cipher 密文
	 * @return mixed
	 */
	public function restore_cus_data($cipher='') {
		$data = g('des') -> decode($cipher);
		$data = json_decode($data, TRUE);
		(!is_array($data) or empty($data)) and $data = NULL;

		return $data;
	}

	/**
	 * 验证简历数据的合法性
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	private function check_resume_data(array &$data) {
		try {
			$condition = array(
				'ji.rji_id=' => $data['rji_id'],
			);
			$demand = $this -> get_job_demand($condition);
		}catch(SCException $e) {
			throw $e;
		}

		if ($demand['use_name']) {
			if (!isset($data['name']) or $data['name'] == '') {
				throw new SCException('姓名不能为空');
			}
		}else {
			unset($data['name']);
		}
		if ($demand['use_mobile']) {
			if (!isset($data['mobile']) or $data['mobile'] == '') {
				throw new SCException('手机号码不能为空');
			}
			if (!check_data($data['mobile'], 'mobile')) {
				throw new SCException('手机号码格式不正确');
			}
		}else {
			unset($data['mobile']);
		}
		if ($demand['use_portrait']) {
			if (!isset($data['portrait']) or $data['portrait'] == '') {
				throw new SCException('头像不能为空');
			}
		}else {
			unset($data['portrait']);
		}
		if ($demand['use_gender']) {
			if (!isset($data['gender']) or ($data['gender'] != 1 and $data['gender'] != 2)) {
				throw new SCException('不合法的性别格式');
			}
		}else {
			unset($data['gender']);
		}
		if ($demand['use_age']) {
			if (!isset($data['age']) or $data['age'] < 1 or $data['age'] > 999) {
				throw new SCException('不合法的年龄格式');
			}
		}else {
			unset($data['age']);
		}
		if ($demand['use_salary']) {
			if (!isset($data['salary'])) {
				throw new SCException('期望月薪不能为空');
			}
		}else {
			unset($data['salary']);
		}
		if ($demand['use_dwell']) {
			if (!isset($data['dwell']) or $data['dwell'] == '') {
				throw new SCException('居住地址不能为空');
			}
		}else {
			unset($data['dwell']);
		}
		if ($demand['use_experience']) {
			if (!isset($data['experience']) or $data['experience'] == '') {
				throw new SCException('工作经验不能为空');
			}
		}else {
			unset($data['experience']);
		}
		if ($demand['use_degree']) {
			if (!isset($data['degree']) or $data['degree'] == '') {
				throw new SCException('学历不能为空');
			}
		}else {
			unset($data['degree']);
		}
		if ($demand['use_job']) {
			if (!isset($data['job']) or $data['job'] == '') {
				throw new SCException('现职位不能为空');
			}
		}else {
			unset($data['job']);
		}
		if ($demand['use_email']) {
			if (!isset($data['email']) or $data['email'] == '') {
				throw new SCException('邮箱不能为空');
			}
			if (!check_data($data['email'], 'email')) {
				throw new SCException('邮箱格式不正确');
			}
		}else {
			unset($data['email']);
		}

		$demand_custom = $demand['custom_info'];

		$other_info = json_decode($data['other_info'], TRUE);
		
		$tmp_arr = array();
		foreach ($demand_custom as $val) {
			if (!isset($other_info[$val]) or $other_info[$val] == '') {
				throw new SCException($val . '不能为空');
			}
			$tmp_arr[$val] = $other_info[$val];
		}
		unset($val);

		$data['other_info'] = json_encode($tmp_arr);

		return TRUE;
	}

	/**
	 * 获取cookie的缓存变量名
	 *
	 * @access public
	 * @param string $str 特定字符串
	 * @return string
	 */
	public function get_cookie_key($str='') {
		$key_str = __FUNCTION__ . $str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 检测用户类型
	 *
	 * @access public
	 * @return integer 1-游客，2-其他企业号用户，3-当前企业号内部员工
	 * @throws SCException 当无权访问时，抛出异常
	 */
	public function check_user($ci_data=NULL) {
		try {
			$this -> check_valid();
		}catch(SCException $e) {
			throw $e;
		}

		$this_com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		$this_user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : NULL;

		if (empty($this_com_id) or empty($this_user_id)) {
			if ($ci_data === NULL) {
				throw new SCException('无权查看该链接！');
			}
			return 1;
		}

		if (isset($ci_data['com_id']) and $this_com_id != $ci_data['com_id']) return 2;
		
		return 3;
	}

	/**
	 * 获取当前链接真正的所属公司id及分享者id、该职位的id
	 *
	 * @access public
	 * @return array
	 */
	public function get_real_info() {
		$cipher = get_var_value('cipher');
		$ci_data = $this -> restore_cus_data($cipher);
		$user_type = $this -> check_user($ci_data);
		$com_id = 0;
		$user_type != 3 and $com_id = $ci_data['com_id'];

		$user_id = 0;
		$com_id != 0 and $user_id = $ci_data['user_id'];

		$com_id = $ci_data['com_id'];
		$user_id = $ci_data['user_id'];

		$ret = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'user_type' => $user_type,
		);

		return $ret;
	}

	/**
	 * 获取提现申请最低金额
	 *
	 * @access public
	 * @return integer
	 */
	public function get_extract_limit() {
		$sql = 'SELECT lowest_withdraw FROM ' . $this -> oc_table . ' WHERE com_id=' . $this -> com_id . ' LIMIT 1';
		$limit = g('db') -> select_first_val($sql);
		if ($limit === FALSE) {
			log_write('最低提现金额，查询失败，com_id=' . $this -> com_id);
		}
		empty($limit) and $limit = 0;

		return $limit;
	}

	/**
	 * 转移特殊字符
	 *
	 * @access private
	 * @param array $data 数据集合
	 * @return array
	 */
	private function replace_data(array $data) {
		if (!is_array($data) and !is_array($data['data'])) {
			return $data;
		}

		if (isset($data['data'])) {
			foreach ($data['data'] as &$val) {
				if (!is_array($val)) continue;

				$val = filter_string($val);
			}
			unset($val);
		}else {
			foreach ($data as &$val) {
				if (!is_string($val)) continue;

				$val = filter_string($val);
			}
			unset($val);
		}

		return $data;
	}
}