<?php
/**
 * 上传文件处理类
 * @author yangpz
 * @date 2014-12-16
 */
class cls_upload {
	/** 允许的文件类型 */
	private static $ExtList;
	
	private static $FileExt = array(
		'txt', 'xml', 'pdf', 
		'zip', 'doc', 'ppt', 
		'xls', 'docx', 'pptx', 
		'xlsx', 'rar'
	);
	private static $ImageExt = array(
		'jpeg', 'jpg', 'png',
	);
	private static $videoExt = array(
		'rm', 'rmvb', 'wmv', 
		'avi', 'mpg', 'mpeg', 
		'mp4',
	);
	private static $voiceExt = array(
		'mp3', 'wma', 'wav', 
		'amr',
	);
	
	/** 微信服务器media 有效时长 */
	public static $MaxMediaTime = 400000; //5 * 24 * 3600 - 32000

	public function __construct() {}

	/**
	 * 改写图片文件路径，使用全局定义的图片尺寸
	 *
	 * @access public
	 * @param string $url 图片文件路径
	 * @param string $image_idx 图片规格索引
	 * @return string
	 */
	public function rebuild_image_url($url, $image_idx) {
		$sizes = $GLOBALS['DEFAULT_IMG_SIZES'];
		!is_array($sizes) and $sizes = array();

		$size = isset($sizes[$image_idx]) ? $sizes[$image_idx] : '';
		if (empty($size)) return $url;

		$url = str_replace('/resize/', "/resize/{$size}/", $url);

		return $url;
	}
	
	/**
	 * 获取本地文件存放的真实路径
	 * @param unknown_type $file_hash	文件MD5
	 * @param unknown_type $ext			文件扩展名
	 * @param unknown_type $create		未找到，是否创建，默认为FALSE
	 */
	public static function get_local_file_path($file_hash, $ext, $create=FALSE) {
		$type = $this -> get_file_type($ext);
		
		$dir_1 = substr($file_hash, 0, 2);
		$dir_2 = substr($file_hash, 2, 2);
		$relative_path = "{$type}/{$dir_1}/{$dir_2}/";
		$filename = $file_hash.'.'.$ext;
		
		$path = SYSTEM_DATA.$relative_path.$filename;
		if (!file_exists($path) && !$create) {
			log_write('文件找不到：'.$path);
			throw new SCException('文件找不到');
		}
		if (!file_exists($path) && $create) {
			$ret = touch($path);
			if (!$ret) {
				throw new SCException('创建文件失败');
			}
		}
		return $path;
	}

	/**
	 * 本地上传（data目录）
	 * @param unknown_type $max_size
	 * @param unknown_type $ext_list
	 * 
	 * @return array('hash'=>'', 'rela_path', 'real_path'=>'', 'name'=>'', 'size'=>'', 'ext'=>'');	文件哈希，相对路径，绝对路径，文件名，文件大小，文件类型
	 */
	public static function upload($max_size, $ext_list=NULL) {
		try {
			$ext = '';
			$type = self::verify_file($max_size, $ext, $ext_list);
			
			$md5 = md5_file($_FILES["Filedata"]["tmp_name"]);
			$dir_1 = substr($md5, 0, 2);
			$dir_2 = substr($md5, 2, 2);
			$relative_path = "{$type}/{$dir_1}/{$dir_2}/";
			$upload_path = SYSTEM_DATA.$relative_path;
			
			if (!file_exists($upload_path)) {
				mkdir($upload_path, 0755, TRUE);
			}
			$filename = $md5.'.'.$ext;
			$filepath = $upload_path.$filename;
			
			if (!file_exists($filepath)) {			//已存在MD5、后缀名相同的文件
				$destination = $upload_path.$filename;
				$location =  $_FILES["Filedata"]["tmp_name"];
				move_uploaded_file($location, $destination);
			}
			
			$ret = array(
				'hash' => $md5,
				'name' => $_FILES['Filedata']['name'],
				'size' => $_FILES['Filedata']['size'],
				'ext' => $ext,
				'type' => $type,
				'real_path' => $upload_path.$filename,
				'rela_path' => $relative_path.$filename,
			);
			return $ret;
			
		} catch (SCException $e) {
			throw $e;
		}
	}

	/**
	 * 本地分段断点续传（临时保存在data目录中）
	 *
	 * @access public
	 * @param array $part_data 片段参数
	 * 			=> string $hash 文件内容的哈希值
	 * 			=> string $ext 文件扩展名，不带"."
	 * 			=> string $part 文件的一个片段内容
	 * 			=> boolean $is_end 是最后一个片段
	 * @param array $max_size 最大值集合
	 * 			=> integer $part 每个片段的最大值
	 * 			=> integer $file 片段整合完成后的文件最大值
	 * @param array $ext_list 可上传扩展名列表
	 * @param array $cloud_param 云存储相关的参数
	 * 			=> boolean $finish 默认为TRUE，即上传完成后，自动调用云存储接管文件
	 * 			=> array $sizes 默认值为空，当文件类型为图片时，需要将图片处理为该尺寸集合的额外单位
	 * @return array
	 * @throws SCException
	 */
	public function part_upload(array &$data, array $max_size, array &$ext_list=array(), array $cloud_param=array()) {
		log_write("开始上传文件片段，hash={$data['hash']}");
		empty($ext_list) and $ext_list = array_merge(self::$FileExt, self::$ImageExt, self::$videoExt, self::$voiceExt);
		if (strlen($data['part']) > $max_size['part']) {
			log_write("单个片段大小超出限制，hash={$data['hash']}");
			throw new SCException("单个片段大小超出限制", -1001);
		}

		if (!in_array($data['ext'], $ext_list)) {
			log_write("扩展名不合法，hash={$data['hash']}");
			throw new SCException("扩展名不合法", -1002);
		}

		!is_dir(SYSTEM_PART_DIR) and mkdir(SYSTEM_PART_DIR, 0755, TRUE);
		$file_name = $data['hash'] . '.' . $data['ext'];
		$file_path = SYSTEM_PART_DIR . $file_name;

		$ret = array(
			'status' => 0,
			'offset' => 0,
		);
		$server_offset = $this -> get_offset($file_path);
		if ($offset != $server_offset) {
			log_write("单个片段上传完成，hash={$data['hash']}");
			$ret['offset'] = $server_offset;
			return $ret;
		}

		$status = $this -> save_part($file_path, $data['part']);
		if ($status !== 1) {
			if ($status === 0) {
				log_write("上传片段未知异常，hash={$data['hash']}");
				throw new SCException("系统异常", -1003);
			}elseif ($status === -1002) {
				log_write("其他用户正在上传，hash={$data['hash']}");
				throw new SCException("其他用户正在上传", -1004);
			}
		}

		$server_offset = $this -> get_offset($file_path);
		if ($server_offset > $max_size['file']) {
			log_write("文件大小超出限制，hash={$data['hash']}");
			throw new SCException("文件大小超出限制", -1006);
		}
		if (!$data['is_end']) {
			log_write("单个片段上传完成，hash={$data['hash']}");
			$ret['offset'] = $server_offset;
			return $ret;
		}

		$file_hash = md5_file($file_path);
		if ($file_hash != $data['hash']) {
			unlink($file_path);
			log_write("片段整合完成，但文件已损坏，hash={$data['hash']}");
			throw new SCException("文件已损坏", -1005);
		}

		empty($cloud_param['finish']) and $cloud_param['finish'] = TRUE;
		empty($cloud_param['sizes']) and $cloud_param['sizes'] = array();

		$ret['status'] = 1;

		try {
			$info = $this -> cloud_get_info($data['hash'], $data['ext']);
			$cloud_param['finish'] and $ret['info'] = $info;
			log_write("片段整合完成，但云存储中已经存在相同的记录");

			unlink($file_path);

			return $ret;
		}catch(SCException $e) {
			log_write("片段整合完成，查询异常：" . $e -> getMessage());
		}

		log_write("开始上传整合后的文件到云存储");

		if ($cloud_param['finish']) {
			$type = get_file_type($data['ext']);
			try {
				$info = $this -> cloud_upload($type, $file_path, $cloud_param['sizes']);
				log_write("片段整合完成，并且上传到云存储成功");

				$ret['info'] = $info;
			}catch(SCException $e) {
				log_write("片段整合完成，但上传异常：" . $e -> getMessage());
			}
		}

		return $ret;
	}

	/**
	 * 根据文件内容哈希，获取云存储中的文件信息
	 *
	 * @access public
	 * @param string $file_hash 文件内容哈希
	 * @param string $ext 文件扩展名
	 * @return array
	 * @throws SCException
	 */
	public function cloud_get_info($file_hash, $ext) {
		$url = MEDIA_URL_HOST . '?cmd=105';
		$suffix = MEDIA_URL_KEY;
		$param 	= array(
			'file_hash' => $file_hash,
			'ext' => $ext,
			'time' => time(),
		);

		$sign = cls_sign::get_sign($param, $suffix);
		$param['sign'] = $sign;
		$result = cls_http::curl($url, $param, 3);
		
		if (!is_array($result) || $result['httpcode'] != 200) throw new SCException('网络异常');
		$content = json_decode($result['content'], TRUE);
		if ($content['status'] != 1) throw new SCException($content['errmsg']);
		return $content['data'];
	}
	
	/**
	 * 构造上传云服务的参数
	 * @param unknown_type $max_size
	 * @param unknown_type $ext_list
	 * @throws SCException
	 */
	public static function cloud_build_params($max_size, $ext_list=NULL) {
		try {
			$file = array_shift($_FILES);
			if (empty($file) || !is_array($file)) {
				cls_resp::echo_err(cls_resp::$FileNotFound, '找不到该上传文件，请重新上传！');
			}
			
			$ext = '';
			$type = self::verify_file($max_size, $ext, $ext_list, $file);
			
			return array('type' => $type, 'file' => json_encode($file), 'name' => $file['name'], 'ext' => $ext, 'size' => $file['size']);
		} catch (SCException $e) {
			throw $e;
		}
	}

	/**
	 * 上传到云存储服务
	 *
	 * @access public
	 * @param string $type 文件类别
	 * @param string $file $_FILES中的单个子元素的json字符串，或一个文件的绝对路径
	 * @param array $sizes 当type是image有效，表示需要缩放的尺寸规格集合
	 * @return array
	 * status 	integer 	是 	0-失败，1-成功
	 * errcode 	integer 	是 	错误码
	 * errmsg 	string 		是 	错误消息
	 * data 	array 		否 	自定义数据集合
	 * hash 	string 		是 	文件的hash
	 * path 	string 		是 	文件的相对保存路径
	 * 
	 */
	public static function cloud_upload($type, $file, array $sizes=array()) {
    	self::_verify_capacity();
    	
		$url 	= MEDIA_URL_HOST . '?cmd=101';
		$suffix = MEDIA_URL_KEY;
		$param 	= array(
			'type' 	=> $type,
			'src'  	=> MEDIA_SRC,
			'time'	=> time(),
		);

		($check = (!is_array($file) and is_json($file))) and $param['file'] = $file;

		//仅当文件类型为图片时，需要做额外的尺寸处理
		if ($type == 'image') {
			//通知云存储将图片处理为这些规格
			empty($sizes) and $sizes = $GLOBALS['DEFAULT_IMG_SIZES'];
			!empty($sizes) and $param['sizes'] = json_encode($sizes);
		}

		$sign = cls_sign::get_sign($param, $suffix);
		$param['sign'] = $sign;

		!$check and $param['file'] = "@" . $file;

		$result = cls_http::curl($url, $param, 30);
		
		if (!is_array($result) || $result['httpcode'] != 200) throw new SCException('网络异常');
		$content = json_decode($result['content'], TRUE);
		if ($content['status'] != 1) throw new SCException($content['errmsg']);
		return $content['data'];
	}

	/**
	 * 根据hash获取媒体的media_id
	 *
	 * @access public
	 * @param string $hash 文件索引哈希
	 * @param string $token 调用微信接口的令牌
	 * @param string $type 文件类别
	 * @param string $name 文件重命名
	 * @param string $size 仅当type为图片时有效，代表图片尺寸，如：100x200
	 * @return boolean
	 */
	public static function get_media_id($hash, $token, $type, $name, $size='') {
		$url 	= MEDIA_URL_HOST . '?cmd=102';
		$suffix = MEDIA_URL_KEY;
		$param 	= array(
			'token' => $token,
			'hash' 	=> $hash,
			'filename' => $name,
			'time'	=> time(),
		);

		if ($type == 'image' and empty($size) and isset($GLOBALS['DEFAULT_IMG_SIZES']['x-large'])) {
			$size = $GLOBALS['DEFAULT_IMG_SIZES']['x-large'];
		}
		!empty($size) and $param['size'] = $size;

		$sign = cls_sign::get_sign($param, $suffix);
		$param['sign'] = $sign;
		$result = cls_http::curl($url, $param, 30);
		log_write(json_encode($result));
		
		if (!is_array($result) || $result['httpcode'] != 200) throw new SCException('网络异常');
		$content = json_decode($result['content'], TRUE);
		if ($content['status'] != 1) throw new SCException($content['errmsg']);
		return $content['data'];
	}

	/**
     * 根据文件扩展名获取文件类型
     *
     * @access private
     * @param string $extname 文件扩展名
     * @return string
     */
    private function get_file_type($extname) {
        $type = '';
        $extname = strtolower($extname);
        $file_type = array(
            'file' => self::$FileExt,
            'image' => self::$ImageExt,
            'video' => self::$videoExt,
            'voice' => self::$voiceExt,
        );
        foreach ($file_type as $key => $val) {
            if (in_array($extname, $val)) {
                $type = $key;
                BREAK;
            }
        }
        unset($val);

        return empty($type) ? 'file' : $type;
    }
	
	//------------------------------------内部实现-------------------------------------------------
	
	/**
	 * 验证上传的文件
	 * @param unknown_type $max_size	最大字节数
	 * @param unknown_type $ext			当前文件的后缀名
	 * @param unknown_type $ext_list	允许的格式，数组格式
	 */
	private static function verify_file($max_size, &$ext, $ext_list=NULL, $file=NULL) {
		empty($file) && $file = $_FILES['Filedata'];
		
		if ($file['error'] > 0) {
			$errmsg = '';
			switch($file['error']) {  
			    case 1:    $errmsg = '文件大小超出了服务器的空间大小 ';	break;   
				case 2:    $errmsg = '要上传的文件大小超出浏览器限制';  	break;    
				case 3:    $errmsg = '文件仅部分被上传'; 				break;    
				case 4:    $errmsg = '没有找到要上传的文件'; 				break;    
				case 5:    $errmsg = '服务器临时文件夹丢失'; 				break;    
				case 6:    $errmsg = '文件写入到临时文件夹出错'; 			break;    
				default:	$errmsg = '上传异常';
			}
			throw new SCException($errmsg);
		}
		
		if (empty($ext_list)) {
			self::$ExtList = array_merge(self::$FileExt, self::$ImageExt, self::$videoExt, self::$voiceExt);
			$ext_list = self::$ExtList;		//允许的文件类型
		}
		
		if (!isset($_FILES) || !isset($file)) {						//空文件
			throw new SCException('未找到上传的文件');
		}
		
		if ($file['size'] == 0) {									//空文件
			throw new SCException('空文件');
		}
		
		if ($file['size'] > $max_size) {							//大小超限
			throw new SCException('文件大小超限');
		}
		
		$ext = explode('.',$file['name']);
		$ext = $ext[count($ext) - 1];
		if (!in_array($ext, $ext_list)) {							//错误文件格式
			throw new SCException('仅支持 '.implode(',', $ext_list).' 格式文件的上传');
		}
		
		if (in_array($ext, self::$ImageExt)) {
			return 'image';
		} else if (in_array($ext, self::$voiceExt)) {
			return 'voice';
		} else if (in_array($ext, self::$videoExt)) {
			return 'video';
		} else {
			return 'file';
		}
	}

	/** 
	 * 获取指定文件的上传偏移量
	 * 
	 * @access private
	 * @param string $file 上传的临时文件路径
	 * @return integer
	 */
	private function get_offset($file) {
		$offset = 0;
		clearstatcache();
		if (file_exists($file)) {
			$offset = filesize($file);
		}
		return $offset;
	}

	/**
	 * 保存文件片段
	 * 
	 * @access private
	 * @param string $file 上传的临时文件路径
	 * @param string $part 文件片段内容
	 * @return integer
	 */
	private function save_part($file, $part){
		$fp = fopen($file, 'a+');
		if (!$fp) {
			//服务器异常
			return 0;
		}
		if (!flock($fp, LOCK_EX | LOCK_NB)) {
			fclose($fp);
			//别的用户正在上传
			return 1002;
		}
		if (fwrite($fp, $part) === FALSE) {
			fclose($fp);
			return 0;
		}
		flock($fp, LOCK_UN);
		fclose($fp);

		//保存成功
		return 1;
	}
    
	/** 验证空间容量是否充足、允许上传 */
	private static function _verify_capacity() {
		$cap_info = g('capacity') -> get_capacity_info();
		if ($cap_info['is_full']) {
			$forbit_time = $cap_info['notice_time'] + $cap_info['pre_buffer_time'] + $cap_info['buffer_time'];
			log_write($forbit_time);
			log_write($cap_info['info_time']);
			if ($cap_info['info_time'] >= $forbit_time) {
				throw new SCException('您的企业服务器 <font color="#f74949">空间容量不足！</font><br>图片和文件上传功能暂时无法使用<br>请告知管理员扩容空间喔!');
			}
		}
	}
}

// end of file