<?php
/**
 * 报销记录
 * @author yangpz
 * @date 2015-01-15
 */
class cls_exacct_record {
	/** 对应的库表名称  */
	private static $Table = 'exacct_record';
	
	private static $StateOn = 1;
	
	private static $StateOff = 0;
	
	/**
	 * 保存报销记录
	 * @param unknown_type $com_id				企业 ID
	 * @param unknown_type $type_id				报销主类型
	 * @param unknown_type $reason				事由
	 * @param unknown_type $sum_money			总金额
	 * @param unknown_type $sum_money_capital	大写的总金额
	 * @param unknown_type $detail				明细，数组格式
	 */
	public function save($com_id, $type_id, $reason, $sum_money, $sum_money_capital, $detail,$formsetinst_id) {
		$data = array(
			'com_id' => $com_id,
			'type_id' => $type_id,
			'reason' => $reason,
			'sum_money' => $sum_money,
			'sum_money_capital' => $sum_money_capital,
			'detail' => json_encode($detail),
			'create_time' => time(),
			'update_time' => time(),
			'info_state' =>self::$StateOn,
			'create_id' => $_SESSION[SESSION_VISIT_USER_ID],
			'formsetinst_id' =>$formsetinst_id
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存报销记录失败');
		}
		return $ret;
	}
	
	/**
	 * 更新报销记录
	 * @param unknown_type $id					记录ID
	 * @param unknown_type $com_id				企业ID
	 * @param unknown_type $type_id				报销主类型ID
	 * @param unknown_type $reason				事由
	 * @param unknown_type $sum_money			总金额
	 * @param unknown_type $sum_money_capital	大写的总金额
	 * @param unknown_type $detail				明细，数组格式
	 */
	public function update($id, $com_id, $type_id, $reason, $sum_money, $sum_money_capital, $detail, $check=TRUE) {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
		);
		//验证报销类型
		$check && $cond['type_id='] = $type_id;
		$data = array(
			'reason' => $reason,
			'sum_money' => $sum_money,
			'sum_money_capital' => $sum_money_capital,
			'detail' => json_encode($detail),
			'update_time' => time(),
		);
		//不验证报销类型时直接更新报销类型
		!$check && $data['type_id'] = $type_id;
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新报销记录失败');
		}
		return $ret;
	}
	
	
/**
	 * 根据实例id查询记录（一对一的关系）
	 * Enter description here ...
	 * @param unknown_type $formsetinst_id
	 */
	public function get_record_by_instid($formsetinst_id){
		$formsetinst_id = intval($formsetinst_id);
		$tb = self::$Table;
		$sql = <<<EOF
SELECT * 
FROM {$tb} 
WHERE formsetinst_id = {$formsetinst_id}
EOF;
		$ret = g('db') -> select($sql);
		
		return $ret ? $ret[0] : FALSE;
	}
	
}

// end of file