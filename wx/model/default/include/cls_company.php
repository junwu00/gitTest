<?php
/**
 * 企业信息
 * @author yangpz
 * @date 2014-10-26
 *
 */
class cls_company {
	/** 对应的库表名称 */
	private static $Table = 'sc_company';
	
	/** 禁用0 */
	private static $StateOff = 0;
	/** 启用1 */
	private static $StateOn = 1;
	/** 试用2 */
	private static $StateTest = 2;
	/** 过期3 */
	private static $StateOverTime = 3;
	
	/**
	 * 根据企业ID获取企业信息
	 * @param unknown_type $com_id
	 */
	public function get_by_id($com_id, $fields='*') {
		$coms = g('ndb') -> get_data_by_ids(self::$Table, $com_id, 'id', $fields);
		return $coms ? $coms[0] : FALSE;
	}
	
	/**
	 * 根据企业根部门ID获取企业信息
	 * @param unknown_type $dept_id	企业根部门ID
	 * @param unknown_type $fields	查看的字段
	 */
	public function get_by_dept_id($dept_id, $fields='*') {
		$cond = array(
			'dept_id=' => $dept_id
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 根据企业查找企业信息
	 * @return 
	 */
	public function search($com_id) {
		$cond = array(
			'id=' => $com_id,
		);
		$com = g('ndb') -> select(self::$Table, '*', $cond);
		
		return $com ? $com : FALSE;
	}
	
	/**
	 * 根据企业配置的url，查找对应的企业信息
	 * @param unknown_type $corp_url
	 * @param unknown_type $fields
	 */
	public function get_by_corp_url($corp_url, $fields='*') {
		$cond = array(
			'corp_url=' => $corp_url,
			'state!=' => self::$StateOff,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		return $result ? $result[0] : FALSE;
	}
	
	/**
	 * 根据corpid 获取企业 信息
	 * @param unknown_type $corp_id
	 * @param unknown_type $fields
	 */
	public function get_by_corp_id($corp_id, $fields='*') {
		$cond = array(
			'corp_id=' => $corp_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			throw new SCException('corp id记录重复：'.$corp_id);
		}
		return $ret ? $ret[0] : FALSE;
	}
	
}

// end of file