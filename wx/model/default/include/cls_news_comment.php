<?php
/**
 * 文章评论/回复信息
 * @author yangpz
 * @date 2016-01-26
 *
 */
class cls_news_comment {
	/** 对应的库表名称 */
	private static $Table = 'news_comment';
	
	/** 已删除  0 */
	public static $StateDel = 0;
	/** 已启用 1 */
	public static $StateOn = 1;
	
	/** 当前访问的企业ID */
	private $com_id;
	/** 当前访问的成员ID */
	private $user_id;
	
	public function __construct() {
		isset($_SESSION[SESSION_VISIT_COM_ID]) && $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		isset($_SESSION[SESSION_VISIT_USER_ID]) && $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}
	
	/**
	 * 获取文章的总评论数(不包括回复)
	 * @param unknown_type $news_id	文章ID
	 */
	public function get_total_by_news_id($news_id) {
		$fields = ' count(id) AS cnt ';
		$cond = array(
			'com_id=' => $this -> com_id,
			'news_id=' => $news_id,
			'p_id=' => 0,
			'state=' => self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret[0]['cnt'];
	}
	
	/**
	 * 获取指定的评论信息
	 * @param unknown_type $news_id	文章ID
	 * @param unknown_type $id		评论ID
	 * @param unknown_type $fields	查找的字段
	 */
	public function get_by_id($news_id, $id, $fields='id, com_id, p_id, user_id, content, create_time') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $this -> com_id,
			'news_id=' => $news_id,
			'state=' => self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	/**
	 * 保存评论
	 * @param unknown_type $news_id		文章ID
	 * @param unknown_type $content		评论内容
	 * @param unknown_type $p_id		父级评论ID，默认为0，即第一级评论
	 */
	public function save($news_id, $content, $p_id=0) {
		if ($p_id != 0) {
			$exists = $this -> get_by_id($news_id, $p_id);
			if (empty($exists)) {
				throw new SCException('要回复的评论不存在');
			}
		}
		
		$db = g('db');
		try {
			$db -> begin_trans();
			$data = array(
				'p_id' => $p_id,
				'com_id' => $this -> com_id,
				'news_id' => $news_id,
				'user_id' => $this -> user_id,
				'content' => $content,
				'state' => self::$StateOn,
				'reply_cnt' => 0,
				'create_time' => time()
			);
			$ret = g('ndb') -> insert(self::$Table, $data);
			if (!$ret) {
				throw new SCException('保存评论或回复失败');
			}
			$p_id != 0 && $this -> increase_reply_cnt($news_id, $p_id);
			$db -> commit();
			return $ret;
			
		} catch (SCException $e) {
			$db -> rollback();
			throw $e;
		}
	}
	
	/**
	 * 评论回复数自增（暂不考虑并发情况）
	 * @param unknown_type $news_id		文章ID
	 * @param unknown_type $p_id		评论ID
	 */
	public function increase_reply_cnt($news_id, $id) {
		$news_id = intval($news_id);
		$id = intval($id);
		
		$table = self::$Table;
		$state_on = self::$StateOn;
		$sql = 'UPDATE ';
		$sql .= <<<EOF
{$table} SET `reply_cnt` = `reply_cnt`+1
WHERE id={$id} AND com_id={$this -> com_id} AND news_id={$news_id} AND state={$state_on}
EOF;
		
		$ret = g('db') -> query($sql);
		if (!$ret) {
			throw new SCException('回复评论失败');
		}
		return $ret;
	}
	
	/**
	 * 分页获取评论信息
	 * @param unknown_type $news_id		文章ID
	 * @param unknown_type $p_id		父级评论ID，默认为0
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 */
	public function list_by_news_id($news_id, $p_id=0, $page=1, $page_size=20) {
		$fields = 'id, news_id, user_id, content, create_time, reply_cnt';
		$cond = array(
			'p_id=' => $p_id,
			'com_id=' => $this -> com_id,
			'news_id=' => $news_id,
			'state=' => self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond, $page, $page_size, '', ' order by create_time desc ', TRUE);
		if ($ret['count'] == 0) {
			return $ret;
		}
		
		$user_ids = array();
		foreach ($ret['data'] as $item) {
			$user_ids[] = $item['user_id'];
		}unset($item);
		$users = g('api_user') -> list_by_ids($user_ids, 'id, name, pic_url');
		$sorted_users = array();
		foreach ($users as $u) {
			$sorted_users[$u['id']] = $u;
		}unset($u);
		
		foreach ($ret['data'] as &$item) {
			if (isset($sorted_users[$item['user_id']])) {
				$item['name'] = $sorted_users[$item['user_id']]['name'];
				$item['pic_url'] = $sorted_users[$item['user_id']]['pic_url'];
				
			} else {
				$item['u_name'] = '未知(该成员已被删除)';
				$item['u_pic_url'] = '';
			}
		}
		return $ret;
	}
	
}

//end