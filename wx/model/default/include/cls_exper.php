<?php
/**
 * 快速体验数据模型
 *
 * @author LiangJianMing
 * @create 2015-1-6
 */

class cls_exper {
	//单个体验用的企业号配置
	public $conf = array();

	//用户信息表
	private $user_table = 'sc_user';

	private $access_token = NULL;
	private $token_create_time = 0;
	private $token_valid_time = 5000;

	public function __construct($index = 0) {
		empty($index) && $index = 0;
		$g_exper_confs = $GLOBALS['g_exper_confs'];
		!empty($g_exper_confs[$index]) && $this -> conf = $g_exper_confs[$index];
	}

	public function get_access_token() {
		$time_check = (time() - $this -> token_create_time) > $this -> token_valid_time;
		if (empty($this -> access_token) || $time_check) {
			try {
				$access_token = g('atoken') -> get_access_token_by_com($this -> conf['com_id'], $this -> conf['combo']);
				if (!empty($access_token)) {
					$this -> access_token = $access_token;
					$this -> token_create_time = time();
				}
			}catch(SCException $e) {
				return FALSE;
			}
		}
		return $this -> access_token;
	}

	/**
	 * 添加体验者到企业号作为企业员工
	 *
	 * @access public
	 * @param integer  
	 * @return boolean
	 */
	public function add_member($mobile, $name) {
		if ($this -> check_exists($mobile)) {
			return TRUE;
		}

		if (!$this -> check_max_user()) {
			throw new SCException('当前的体验名额已经用完，请联系我方工作人员!');
		}

		$conf = $this -> conf;
		$root_id = $conf['root_id'];
		if (empty($root_id)) {
			throw new SCException('体验配置已失效，请重新选择!');
		}

		$dept_id = $conf['dept_id'];
		empty($dept_id) && $dept_id = $root_id;

		$acct = get_rand_string(3) . substr($mobile, -4);
		$time = time();
		$data = array(
			'create_time' => $time,
			'acct' 		  => $acct,
			'mobile' 	  => $mobile,
			'name' 		  => $name,
			'root_id'	  => $root_id,
			'dept_list'	  => json_encode(array(strval($dept_id))),
			'dept_tree'	  => $conf['dept_tree'],
			'state'		  => 1,
		);

		g('db') -> begin_trans();
		$result = g('ndb') -> insert($this -> user_table, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException('系统繁忙，请稍候再试!');
		}

		try {
			$access_token = $this -> get_access_token();
			$wx_dept_id = $conf['wx_dept_id'];
			g('wxqy') -> create_user($access_token, $acct, $name, array($wx_dept_id), '', $mobile, 0, '', '', '');
			g('db') -> commit();
		}catch(SCException $e) {
			g('db') -> rollback();
			throw new SCException('系统繁忙，请稍候再试!');
		}
		return TRUE;
	}

	/**
	 * 检验当前企业号是否已经满员
	 *
	 * @access public
	 * @return void
	 */
	public function check_max_user() {
		$conf = $this -> conf;
		if (!is_array($conf)) return FALSE;

		$sql = <<<EOF
SELECT COUNT(id) as user_count
FROM {$this -> user_table} 
WHERE root_id={$conf['root_id']} AND state!=0 
EOF;
		$count = g('db') -> select_first_val($sql);
		if ($count === FALSE) return FALSE;
		
		if ($count >= $conf['max_user']) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * 检测用户体验资格是否已经存在
	 *
	 * @access public
	 * @return boolean
	 */
	public function check_exists($mobile) {
		$conf = $this -> conf;
		$condition = array(
			'mobile=' 	=> $mobile,
			'root_id='	=> $conf['root_id'],
			'state!='	=> 0,
		);
		$check = g('ndb') -> record_exists($this -> user_table, $condition);
		return $check;
	}

	/**
	 * 获取公众号的access_token
	 *
	 * @access public
	 * @param string $app_id 公众号的应用id
	 * @param string $app_secret 公众号的应用密钥
	 * @return string
	 */
	public function get_public_accesstoken($app_id, $app_secret) {
		$cache_key = $this -> get_cache_key(__FUNCTION__);
		$ret = g('redis') -> get($cache_key);
		if (empty($ret)) {
			$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential';
			$url .= '&appid='.$app_id;
			$url .= '&secret='.$app_secret;
			$result = cls_http::quick_get($url);
			if ($result['httpcode'] == 200) {
				$data = json_decode($result['content'], TRUE);
				isset($data['access_token']) && $ret = $data['access_token'];
				$ttl = isset($data['expires_in']) ? ($data['expires_in'] - 1000) : 3600;
				!empty($ret) && g('redis') -> set($cache_key, $ret, $ttl);
			}
		}
		return $ret;
	}

	/**
	 * 获取公众号的js_sdk
	 *
	 * @access public
	 * @param string $app_id 公众号的应用id
	 * @param string $app_secret 公众号的应用密钥
	 * @return string
	 */
	public function get_jsapi_ticket($app_id, $app_secret) {
		$cache_key = $this -> get_cache_key(__FUNCTION__);
		$ret = g('redis') -> get($cache_key);
		if (empty($ret)) {
			$access_token = $this -> get_public_accesstoken($app_id, $app_secret);
			$url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='.$access_token.'&type=jsapi';
			$result = cls_http::quick_get($url);
			if ($result['httpcode'] == 200) {
				$data = json_decode($result['content'], TRUE);
				isset($data['ticket']) && $ret = $data['ticket'];
				$ttl = isset($data['expires_in']) ? ($data['expires_in'] - 1000) : 3600;
				!empty($ret) && g('redis') -> set($cache_key, $ret, $ttl);
			}
		}
		return $ret;
	}

	/**
	 * 获取js_sdk签名
	 *
	 * @access public
	 * @param string $app_id 公众号的应用id
	 * @param string $app_secret 公众号的应用密钥
	 * @param array $data 加密所需的其他信息集合
	 * @return string
	 */
	public function get_jsapi_sign($app_id, $app_secret, array $data) {
		$data['jsapi_ticket'] = $this -> get_jsapi_ticket($app_id, $app_secret);
		if (empty($data['jsapi_ticket'])) return '';

		ksort($data);
		$str = '';
		foreach ($data as $key => $val) {
			$str .= $key.'='.$val.'&';
		}
		$str = substr($str, 0, -1);

		$sign = sha1($str);
		return $sign;
	}

	/**
	 * 获取二维码多媒体资源id
	 *
	 * @access public
	 * @param string $app_id 公众号的应用id
	 * @param string $app_secret 公众号的应用密钥
	 * @return boolean
	 */
	public function get_media_id($app_id, $app_secret) {
		$conf = $this -> conf;
		$com_id = $conf['com_id'];
		$key_str = __FUNCTION__.':com_id:'.$com_id;
		$cache_key = $this -> get_cache_key($key_str);
		if (empty($ret)) {
			$access_token = $this -> get_public_accesstoken($app_id, $app_secret);
			$url = 'http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token='.$access_token.'&type=image';

			$file = SYSTEM_STATIC.'image/try_img/'.$conf['img'];
			if (!file_exists($file)) return FALSE;

			$param = array(
				'img' => "@".$file,
			);

			$result = cls_http::curl($url, $param);
			if ($result['httpcode'] == 200) {
				$data = json_decode($result['content'], TRUE);
				isset($data['media_id']) && $ret = $data['media_id'];
				$ttl = 2 * 24 * 3600;
				!empty($ret) && g('redis') -> set($cache_key, $ret, $ttl);
			}
		}
		return $ret;
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 自定义字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$preffix = 'domain:'.SYSTEM_HTTP_DOMAIN.':class:'.__CLASS__.':';
		$key_str = md5($preffix.$str);
		return $key_str;
	}
}