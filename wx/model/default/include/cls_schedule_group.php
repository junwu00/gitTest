<?php
/**
 * 工作日程
 * 
 * @author chenyihao
 * @date 2014-12-09
 *
 */
class cls_schedule_group {
	
	private static $Table = 'schedule_group';
	private static $UserTable = 'sc_user';
	private static $SeeTable = 'schedule_see';
	private static $ScheduleTable = 'schedule_schedule';
	private static $PartnerTable = 'sc_partner';
	
	
	/**
	 * 创建一个分组
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $group_name
	 */
	public function add_group($com_id,$user_id,$group_name,$dept_list,$user_list){
		$data = array(
			'group_name' => $group_name,
			'user_id' => $user_id,
			'com_id' => $com_id,
			'info_state' => 1,
			'create_time' => time(),
			'group_user_list' => $user_list,
			'group_dept_list' => $dept_list,
		);
		$id = g('ndb') -> insert(self::$Table,$data);
		if(empty($id)){
			return false;
		}else{
			return $id;
		}
	}
	
	/**
	 * 修改组名
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $group_id
	 * @param unknown_type $group_name
	 */
	public function edit_group($com_id,$user_id,$group_id,$group_name,$dept_list,$user_list){
		$cond = array(
			'id=' => $group_id,
			'user_id=' => $user_id,
			'com_id=' => $com_id,
			'info_state=' => 1,
		);
		$data = array(
			'group_name' => $group_name,
			'group_user_list' => $user_list,
			'group_dept_list' => $dept_list,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 根据ID或者ID数组删除分组
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $group_id
	 * @param unknown_type $group_name
	 */
	public function delete_group($com_id,$user_id,$group_id){
		$cond = array(
			'user_id=' => $user_id,
			'com_id=' => $com_id,
		);
		
		if(is_array($group_id)){
			$cond['__OR'] = array();
			foreach ($group_id as $key => $id){
				$id = (int)$id;
				!empty($id) && $cond['__OR']['__'.$key.'__id='] = $id; 
			}
		}else{
			$group_id = (int)$group_id;
			if(!empty($group_id)){
				$cond['id='] = $group_id;
			}
		}
		
		$data = array(
			'info_state' => 0,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 获取组
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $group_id
	 */
	public function get_group($com_id ='' ,$user_id = '',$group_id='',$fields = '*'){
		if(empty($com_id) && empty($user_id) && empty($group_id)){
			return false;
		}
		$cond = array(
			'info_state=' => 1,
		);
		!empty($com_id) && $cond['com_id='] = $com_id;
		!empty($user_id) && $cond['user_id='] = $user_id;
		!empty($group_id) && $cond['id='] = $group_id;
		
		$ret = g('ndb') ->select(self::$Table,$fields,$cond);
		if(!empty($group_id)){
			return empty($ret) ? false:$ret[0];
		}else{
			return empty($ret) ? false:$ret;
		}
	}

	
	/**
	 * 组内添加员工和部门
	 * $user_list $dept_list两者不能同时为空
	 * @param unknown_type $group_id
	 * @param unknown_type $user_list
	 * @param unknown_type $dept_list
	 * 
	 */
	public function add_user_dept($group_id,$user_list,$dept_list){
		if(empty($user_list) && empty($dept_list)){
			return false;
		}
		if((!empty($user_list) && !is_array($user_list)) || (!empty($dept_list) && !is_array($dept_list))){
			return false;
		}
		
		$group = $this -> get_group('','',$group_id,'group_user_list,group_dept_list');
		if(empty($group)){
			return false;
		}
		log_write(json_encode($group));
		$user_array = array();
		$dept_array = array();
		if(!empty($group['group_user_list'])){
			log_write(1);
			$user_array = json_decode($group['group_user_list'],true);
		}
		if(!empty($group['group_dept_list'])){
			$dept_array = json_decode($group['group_dept_list'],true);
		}
		log_write(json_encode($user_array));
		foreach ($user_list as $user){
			$user = (int)$user;
			!empty($user) && !in_array($user,$user_array) && array_push($user_array,$user);
		}
		log_write(json_encode($user_array));
		foreach ($dept_list as $dept){
			$dept = (int)$dept;
			!empty($dept) && !in_array($dept,$dept_array) && array_push($dept_array,$dept);
		}
		
		$cond = array(
			'id=' => $group_id
		);
		$data = array(
			'group_user_list' => json_encode($user_array),
			'group_dept_list' => json_encode($dept_array),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 删除组内员工和部门
	 * @param unknown_type $group_id
	 * @param unknown_type $user_list
	 * @param unknown_type $dept_list
	 */
	public function delete_user_dept($group_id,$user_list,$dept_list){
		if(empty($user_list) && empty($dept_list)){
			return false;
		}
		if((!empty($user_list) && !is_array($user_list)) || (!empty($dept_list) && !is_array($dept_list))){
			return false;
		}
		
		$group = $this -> get_group('','',$group_id,'group_user_list,group_dept_list');
		if(empty($group)){
			return false;
		}
		$user_array = array();
		$dept_array = array();
		if(!empty($group['group_user_list'])){
			$user_array = json_decode($group['group_user_list'],true);
		}
		if(!empty($group['group_dept_list'])){
			$dept_array = json_decode($group['group_dept_list'],true);
		}
		
		foreach ($user_list as $user){
			if( ($key = array_search($user, $user_array)) !== false){
				array_splice($user_array,$key,1);
			}
		}
		unset($user);
		unset($key);
		
		foreach ($dept_list as $dept){
			if( ($key = array_search($dept, $dept_array)) !== false){
				array_splice($dept_array,$key,1);
			}
		}
		unset($dept);
		unset($key);
		
		$cond = array(
			'id=' => $group_id
		);
		$data = array(
			'group_user_list' => json_encode($user_array),
			'group_dept_list' => json_encode($dept_array),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table,$cond,$data);
		return $ret;
	}
	
	/**
	 * 保存最近查看的员工
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $see_id
	 */
	public function save_see($user_id,$see_id){
		$time = time();
		$sql = "INSERT INTO ".self::$SeeTable." (user_id,see_id,see_time,info_state) VALUES({$user_id},{$see_id},{$time},1)ON DUPLICATE KEY UPDATE see_time = {$time}, info_state = 1;";
		log_write($sql);
		$ret = g('db') ->query($sql);
		return $ret === false ? false : true; 
	}
	
	/**
	 * 获取最近查看的人员信息(只获取最近10条)
	 * @param unknown_type $user_id
	 * @param unknown_type $fields
	 */
	public function get_see($user_id,$fields ='u.id,u.name,u.pic_url,u.dept_list'){
		$cond = array(
			'user_id=' => $user_id,
			'info_state=' => 1,
		);
		$ret = g('ndb') -> select(self::$SeeTable." s LEFT JOIN ".self::$UserTable . ' u ON s.see_id = u.id',$fields,$cond,1,10,'','ORDER BY see_time DESC');
		return empty($ret) ? false : $ret;
	}
	
	/**
	 * 根据ID数组获取组信息
	 * @param unknown_type $com_id
	 * @param unknown_type $group_ids
	 * @param unknown_type $fields
	 */
	public function get_group_list($com_id,$group_ids,$fields='*'){
		if(empty($group_ids) || !is_array($group_ids)){
			return false;
		}
		$cond = array('__OR' => array());
		
		foreach( $group_ids as $key => $id){
			$id = (int)$id;
			!empty($id) && $cond['__OR']['__'.$key.'__id='] = $id;
		}
		if(empty($cond['__OR'])){
			return false;
		}
		$cond['info_state='] = 1;
		$ret = g('ndb') -> select(self::$Table,$fields,$cond);
		return $ret;
	}
}

// end of file