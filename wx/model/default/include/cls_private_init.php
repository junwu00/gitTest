<?php
/**
 * 私有化部署初始化类
 *
 * @author JianMing Liang
 * @create 2016-02-27 14:26:57
 */
class cls_private_init {
    private $com_table = 'sc_company';

    public function check_auth() {
        $cache_key = md5(__CLASS__ . ":" . __FUNCTION__);
        $cache = g('redis') -> get($cache_key);

        if (!empty($cache)) {
            $cache_str = g('des') -> decode($cache);
            $cache_arr = json_decode($cache_str, true);
            !is_array($cache_arr) and $cache_arr = array();

            if (!isset($cache_arr['time']) or time() - $cache_arr['time'] > 1800) {
                //重新查询授权情况
            } else {
                return;
            }
        }

        $sql = "select count(1) as corp_count from {$this -> com_table}";
        $corp_count = g('db') -> select_first_val($sql);
        if ($corp_count === false) {
            cls_resp::echo_busy();
            exit;
        }

        $wx_domain = SYSTEM_HTTP_DOMAIN;
        if (empty($wx_domain)) {
            cls_resp::echo_busy();
        }

        $index_md5 = md5_file(dirname(dirname(SYSTEM_ROOT)) . '/index.php');
        $param  = array(
            'caller'        => API_AUTH_CALLER,
            'wx_domain'     => $wx_domain,
            'src'           => API_AUTH_SRC,
            'ver'           => SYSTEM_VERSION,
            'core_md5'      => $index_md5,
            'corp_count'    => $corp_count,
            'time'          => time(),
        );

        $sign = cls_sign::get_sign($param, API_AUTH_SECRET);
        $param['sign'] = $sign;

        $url = API_AUTH_URL . '?' . http_build_query($param);
        $result = cls_http::get($url, array(), 30);

        $res_arr = json_decode($result['content'], TRUE);
        !is_array($res_arr) and $res_arr = array();

        if ($result['httpcode'] != 200 or !isset($res_arr['status']) or $res_arr['status'] != 1 or $res_arr['data'] != $sign) {
            log_write('授权验证失败: params=' . json_encode($param, JSON_UNESCAPED_UNICODE) . ', ret=' . json_encode($result, JSON_UNESCAPED_UNICODE));
            cls_resp::show_err_page("您的私有化版本授权验证失败，请联系移步到微官方进行处理！");
            exit;
        }

        $data = array(
            'token' => get_rand_string(33),
            'time' => time(),
        );
        $cache_val = g('des') -> encode(json_encode($data));

        // 缓存10分钟
        g('redis') -> setex($cache_key, $cache_val, 600);
    }
}

/* End of this file */