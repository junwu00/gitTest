<?php
/**
 * 年假处理类
 * @author yangpz
 * @date 2014-12-22
 */
class cls_rest_year {
	/** 对应的库表名称 */	
	private static $Tabel = 'rest_year';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/**
	 * 企业是否配置为年假信息
	 * @param unknown_type $com_id
	 */
	public function is_com_configed($com_id) {
		$fields = 'count(1) as cnt';
		$cond = array(
			'com_id=' => $com_id,
		);
		$ret = g('ndb') -> select(self::$Tabel, $fields, $cond);
		$cnt = $ret[0]['cnt'];
		return $cnt > 0;
	}
	
	/**
	 * 获取企业年假信息
	 * @param unknown_type $com_id
	 * @param unknown_type $state	
	 * @param unknown_type $fields
	 */
	public function get_by_com($com_id, $state=NULL, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
		);
		!is_null($state) && $cond['state='] = $state;
		return g('ndb') -> select(self::$Tabel, $fields, $cond);
	}
	
	/**
	 * 根据员工ID获取年假配置
	 * @param unknown_type $com_id
	 * @param unknown_type $user_acct
	 */
	public function get_by_user_id($com_id, $user_id, $fields='id') {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
		);
		$ret = g('ndb') -> select(self::$Tabel, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
	
	//------------------------------------
	
}

// end of file