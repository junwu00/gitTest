<?php
/**
 * 流程审批记录
 * @author yangpz
 * @date 2014-12-03
 */
class cls_proc_record {
	/** 对应的库表名称 */
	private static $Table = 'proc_record';
	
	/** 审批中 0 */
	private static $StateWait = 0;
	/** 同意 1 */
	private static $StateAgree = 1;
	/** 驳回 2 */
	private static $StateBack = 2;
	/** 撤销 3 */
	private static $StateCancel = 3;
	/** 催办 4 */
	private static $StateWarn = 4;
	
	/**
	 * 保存审批记录
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $to_user_id
	 * @param unknown_type $inputs
	 * @param unknown_type $step
	 * @param unknown_type $code			唯一标识一条申请
	 * @param unknown_type $state
	 * @param unknown_type $proc_ver		流程版本号
	 */
	public function save($com_id, $proc_id, $from_user_id, $to_user_id, $inputs, $step, $code, $state, $proc_ver, $detail_id=NULL) {
		$data = array(
			'com_id' => $com_id,
			'proc_id' => $proc_id,
			'from_user_id' => $from_user_id,
			'to_user_id' => $to_user_id,
			'step' => $step,
			'inputs' => json_encode($inputs),
			'code' => $code,
			'state' => $state,
			'proc_ver' => $proc_ver,
			'create_time' => time(),
			'update_time' => time(),
		);
		!is_null($detail_id) && $data['detail_id'] = $detail_id;
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存审批记录失败');
		}
		return $ret;
	}
	
	/**
	 * 更新审批记录状态
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $id
	 * @param unknown_type $state
	 * @param unknown_type $state
	 */
	public function change_state($com_id, $proc_id, $id, $state) {
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'id=' => $id,
		);
		$data = array(
			'state' => $state,
			'update_time' => time(),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新审批记录状态失败');
		}
		return $ret;
	}
	
	/**
	 * 更新审批记录状态、经办人信息
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $id
	 * @param unknown_type $state
	 * @param unknown_type $approval_user_id	经办人的user id
	 * @param unknown_type $next_user_id			下一审批人的user id
	 * @param unknown_type $next_step			下一审批步骤
	 */
	public function update($com_id, $proc_id, $id, $state, $approval_user_id, $next_user_id=NULL, $next_step=NULL) {
		$fields = '*';
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'id=' => $id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if (!$ret) {
			throw new SCException('找不到申请记录');
		}
		$to_user_list = $ret[0]['to_user_list'];
		$to_user_list = (is_null($to_user_list) || empty($to_user_list)) ? array() : json_decode($to_user_list, TRUE);
		!in_array($approval_user_id, $to_user_list) && array_push($to_user_list, strval($approval_user_id));
		
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'id=' => $id,
		);
		$data = array(
			'state' => $state,
			'update_time' => time(),
			'to_user_list' => json_encode($to_user_list),
		);
		//($state == self::$StateBack) && $data['to_user_id'] = $ret[0]['from_user_id'];	//若为驳回，下一处理人为发起人
		!is_null($next_user_id) && $data['to_user_id'] = $next_user_id;
		!is_null($next_step) && $data['step'] = $next_step;
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新审批记录失败');
		}
		return $ret;
	}
	
	/**
	 * 重新提交申请表
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $id
	 * @param unknown_type $state
	 * @param unknown_type $to_user_id
	 * @param unknown_type $inputs
	 */
	public function reapply($com_id, $proc_id, $id, $state, $to_user_id, $inputs) {
		$fields = '*';
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'id=' => $id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if (!$ret) {
			throw new SCException('找不到原申请记录');
		}
		$to_user_list = $ret[0]['to_user_list'];
		$to_user_list = (is_null($to_user_list) || empty($to_user_list)) ? array() : json_decode($to_user_list, TRUE);
		if (in_array($to_user_id, $to_user_list)) {
			$idx = array_search(strval($to_user_id), $to_user_list);
			unset($to_user_list[$idx]);	
		}
		
		$cond = array(
			'com_id=' => $com_id,
			'proc_id=' => $proc_id,
			'id=' => $id,
		);
		$data = array(
			'step' => 1,
			'state' => $state,
			'update_time' => time(),
			'inputs' => json_encode($inputs),
			'to_user_id' => $to_user_id,
			'to_user_list' => json_encode($to_user_list),
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新审批记录失败');
		}
		return $ret;
	}
	
	
	/**
	 * 根据code获取审批明细
	 * @param unknown_type $code	
	 * @param unknown_type $com_id
	 * @param unknown_type $proc_id
	 * @param unknown_type $fields		
	 */
	public function get_by_code($code, $com_id, $proc_id, $fields='*') {
		$code = mysql_escape_string($code);
		$com_id = intval($com_id);
		$proc_id = intval($proc_id);
		$fields = mysql_escape_string($fields);
		
		$code = mysql_escape_string($code);
		$sql = " select r.*, u1.name as user_name, u1.pic_url, u2.name as to_user_name from proc_record r, sc_user u1, sc_user u2 
			where u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id={$proc_id} 
				and r.com_id={$com_id} 
				and r.code='{$code}'";
		
		$ret = g('db') -> select($sql);
		if ($ret && count($ret) > 1) {
			throw new SCException('存在多条审批记录');
		}
		if ($ret) {
			$ret = $ret[0];
			$ret['inputs'] = json_decode($ret['inputs'], TRUE);
		} else {
			$ret = FALSE;
		}
		return $ret;
	}
	
	//-------------------------------------申请人查看=》指定单个流程类型----------------------------------------------
	
	/**
	 * 获取员工 审批中 的记录，按创建时间升序
	 * @param unknown_type $proc_id			流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_wait_list($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list($proc_id, $com_id, $user_id, self::$StateWait, $page, $page_size, 'order by create_time asc');
	}
	
	/**
	 * 获取员工 审批通过 的记录，按创建时间降序
	 * @param unknown_type $proc_id			流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_agress_list($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list($proc_id, $com_id, $user_id, self::$StateAgree, $page, $page_size, 'order by create_time desc');
	}
	
	/**
	 * 获取员工 被驳回 的记录，按创建时间降序
	 * @param unknown_type $proc_id			流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_back_list($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list($proc_id, $com_id, $user_id, self::$StateBack, $page, $page_size, 'order by create_time desc');
	}
	
	/**
	 * 获取员工 被驳回/撤销 的记录，按创建时间降序
	 * @param unknown_type $proc_id			流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_back_and_cancel($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		$state = self::$StateBack.','.self::$StateCancel;
		return $this -> get_send_rec_list($proc_id, $com_id, $user_id, $state, $page, $page_size, 'order by create_time desc');
	}
	
	
	//-------------------------------------申请人查看=》指定多个流程类型----------------------------------------------

	/**
	 * 获取员工 审批中 的记录，按创建时间升序
	 * @param unknown_type $proc_id_list			流程类型，数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_wait_list_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list_mulit(implode(',', $proc_id_list), $com_id, $user_id, self::$StateWait, $page, $page_size, 'order by create_time asc');
	}
	
	/**
	 * 获取员工 审批通过 的记录，按创建时间降序
	 * @param unknown_type $proc_id_list			流程类型，数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_agress_list_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list_mulit(implode(',', $proc_id_list), $com_id, $user_id, self::$StateAgree, $page, $page_size, 'order by create_time desc');
	}
	
	/**
	 * 获取员工 被驳回 的记录，按创建时间降序
	 * @param unknown_type $proc_id_list			流程类型，数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $from_user_id
	 * @param unknown_type $fields
	 */
	public function get_back_list_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		return $this -> get_send_rec_list_mulit(implode(',', $proc_id_list), $com_id, $user_id, self::$StateBack, $page, $page_size, 'order by create_time desc');
	}
	
	/**
	 * 获取员工 被驳回/撤销 的记录，按创建时间降序
	 * @param unknown_type $proc_id_list			流程类型，数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_back_and_cancel_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		$state = self::$StateBack.','.self::$StateCancel;
		return $this -> get_send_rec_list_mulit(implode(',', $proc_id_list), $com_id, $user_id, $state, $page, $page_size, 'order by create_time desc');
	}
	
	//-------------------------------------审批人查看=>单个流程类型----------------------------------------------

	/**
	 * 获取待审批(未处理)记录，按创建时间升序
	 * @param unknown_type $proc_id		流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_undeal_list($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		$proc_id = intval($proc_id);
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$page = intval($page);
		$page_size = intval($page_size);
		
		$offset = ($page - 1) * $page_size;
		$state = self::$StateWait;
		$sql = " select r.*, u1.name as from_user_name, u1.pic_url, u2.name as to_user_name from proc_record r, sc_user u1, sc_user u2 
			where u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id={$proc_id} 
				and r.com_id={$com_id} 
				and r.to_user_id={$user_id}
				and r.state={$state} 
			order by r.create_time asc
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}
	
	/**
	 * 获取待审批(已处理)记录，按创建时间降序
	 * @param unknown_type $proc_id		流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_deal_list($proc_id, $com_id, $user_id, $page=1, $page_size=20) {
		$proc_id = intval($proc_id);
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$page = intval($page);
		$page_size = intval($page_size);
		
		$offset = ($page - 1) * $page_size;
		$sql = " select r.*, u1.name as from_user_name, u1.pic_url, u2.name as to_user_name from proc_record r, sc_user u1, sc_user u2 
			where r.to_user_list like '%\"{$user_id}\"%'
				and u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id={$proc_id} 
				and r.com_id={$com_id} 
			order by r.create_time desc
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}
	
	
	//-------------------------------------审批人查看=>多个流程类型----------------------------------------------

	/**
	 * 获取待审批(未处理)记录，按创建时间升序
	 * @param unknown_type $proc_id_list		流程类型,数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_undeal_list_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		if (empty($proc_id_list) || count($proc_id_list) == 0)	return FALSE;
		
		$proc_id_list = mysql_escape_string(implode(',', $proc_id_list));
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$page = intval($page);
		$page_size = intval($page_size);
		
		$offset = ($page - 1) * $page_size;
		$state = self::$StateWait;
		$sql = " select ps.name as proc_name, r.*, u1.name as from_user_name, u1.pic_url, u2.name as to_user_name 
			from proc_setting ps, proc_record r, sc_user u1, sc_user u2 
			where ps.id=r.proc_id
				and u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id in ({$proc_id_list}) 
				and r.com_id={$com_id} 
				and r.to_user_id={$user_id}
				and r.state={$state} 
			order by r.create_time asc
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}
	
	/**
	 * 获取待审批(已处理)记录，按创建时间降序
	 * @param unknown_type $proc_id_list		流程类型,数组格式
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $fields
	 */
	public function get_deal_list_mulit($proc_id_list, $com_id, $user_id, $page=1, $page_size=20) {
		if (empty($proc_id_list) || count($proc_id_list) == 0)	return FALSE;
		
		$proc_id_list = mysql_escape_string(implode(',', $proc_id_list));
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$page = intval($page);
		$page_size = intval($page_size);
		
		$offset = ($page - 1) * $page_size;
		$sql = " select ps.name as proc_name, r.*, u1.name as from_user_name, u1.pic_url, u2.name as to_user_name 
			from proc_setting ps, proc_record r, sc_user u1, sc_user u2 
			where ps.id=r.proc_id
				and r.to_user_list like '%\"{$user_id}\"%'
				and u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id in ({$proc_id_list}) 
				and r.com_id={$com_id} 
			order by r.create_time desc
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}
	
	//--------------------------------------------内部实现-------------------------------------------
	
	/**
	 * 获取提申请的员工、指定审批状态的记录（指定单个流程类型）
	 * @param unknown_type $proc_id			流程类型
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $state
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $order
	 * @param unknown_type $fields
	 */
	private function get_send_rec_list($proc_id, $com_id, $user_id, $state, $page=1, $page_size=20, $order='') {		
		$proc_id = intval($proc_id);
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$state = mysql_escape_string($state);
		$page = intval($page);
		$page_size = intval($page_size);
		$order = mysql_escape_string($order);
		
		$offset = ($page - 1) * $page_size;
		$sql = " select r.*, u1.name as user_name, u1.pic_url, u2.name as to_user_name from proc_record r, sc_user u1, sc_user u2 
			where u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id={$proc_id} 
				and r.com_id={$com_id} 
				and r.from_user_id={$user_id}
				and r.state in ({$state}) 
			{$order} 
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}

	/**
	 * 获取提申请的员工、指定审批状态的记录（指定多个流程类型）
	 * @param unknown_type $proc_ids	流程类型。多个用,分隔
	 * @param unknown_type $com_id
	 * @param unknown_type $user_id
	 * @param unknown_type $state
	 * @param unknown_type $page
	 * @param unknown_type $page_size
	 * @param unknown_type $order
	 * @param unknown_type $fields
	 */
	private function get_send_rec_list_mulit($proc_ids, $com_id, $user_id, $state, $page=1, $page_size=20, $order='') {
		if (empty($proc_ids))	return FALSE;
		
		$proc_ids = mysql_escape_string($proc_ids);
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$state = mysql_escape_string($state);
		$page = intval($page);
		$page_size = intval($page_size);
		$order = mysql_escape_string($order);
				
		$offset = ($page - 1) * $page_size;
		$sql = " select ps.name as proc_name, r.*, u1.name as user_name, u1.pic_url, u2.name as to_user_name 
			from proc_setting ps, proc_record r, sc_user u1, sc_user u2 
			where ps.id=r.proc_id
				and u1.id=r.from_user_id 
				and u2.id=r.to_user_id
				and	r.proc_id in ({$proc_ids}) 
				and r.com_id={$com_id} 
				and r.from_user_id={$user_id}
				and r.state in ({$state}) 
			{$order} 
			limit {$offset}, {$page_size}";
		return g('db') -> select($sql);
	}
}

// end of file