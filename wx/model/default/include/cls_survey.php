<?php
/**
 * 问卷调查数据模型
 *
 * @author LiangJianMing
 * @create 2015-03-05
 */
class cls_survey {
	//公司id
	private $com_id   = NULL;
	//用户id
	private $user_id  = NULL;
	//基础条件
	private $base_con = NULL;
	//用于保存答卷的cookie变量名，只保留最后一张答卷
	private $sheet_key = 'easywork_sheet_key';

	//调研信息表
	public $info_table 	 = 'survey_info';
	//问卷问题表
	public $qs_table 	 = 'survey_question';
	//问卷问题选项表
	public $option_table = 'survey_question_option';
	//用户答题表
	public $ans_table 	 = 'survey_answer';
	//答卷基础信息表
	public $sheet_table  = 'survey_sheet';
	//用户信息表
	public $user_table 	 = 'sc_user';
	//组别表
	public $group_table 	 = 'sc_group';

    //定义特定题目类型只能拥有固定的选项
    public $extra = array(
    	'score' => array(
			'score_1',
			'score_2',
			'score_3',
			'score_4',
			'score_5',
		),
    );

    //定义特殊索引的名称映射
    public $name_map = array(
    	'score_1' => '1分',
		'score_2' => '2分',
		'score_3' => '3分',
		'score_4' => '4分',
		'score_5' => '5分',
    );

    //问卷类型对应的中文名称
    public $stype_list = array(
    	1 => '品牌营销', 2 => '产品测试', 3 => '消费者分析',
    	4 => '满意度调查', 5 => '人力资源', 6 => '学术教育',
    	7 => '社会民意', 8 => '其他',
    );

    //问题类型对应的中文名称
    public $qtype_list = array(
    	1 => '单选', 2 => '多选', 3 => '打分',
    	4 => '填空', 5 => '简答',
    );

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : NULL;
		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : NULL;
		$this -> sheet_key = md5($this -> sheet_key);
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 获取当前用户的所有归属部门id集合
	 *
	 * @access public
	 * @return array
	 */
	public function get_user_dept() {
		$str = __FUNCTION__.':user_id:'.$this -> user_id;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$sql = <<<EOF
SELECT `dept_tree` 
FROM {$this -> user_table} 
WHERE id={$this -> user_id} 
LIMIT 1 
EOF;
		$result = g('db') -> select_first_val($sql);
		$result = json_decode($result, TRUE);
		$dept_arr = array();
		if (is_array($result) && !empty($result)) {
			foreach ($result as $val) {
				foreach ($val as $cval) {
					$dept_arr[] = intval($cval);
				}
				unset($cval);
			}
			unset($val);
		}
		$dept_arr = array_unique($dept_arr);

		//用户部门信息缓存30秒
		g('redis') -> setex($cache_key, json_encode($dept_arr), 30);

		return $dept_arr;
	}

	// gch add
	/**
	 * 获取当前用户的所有归属组别id集合
	 *
	 * @access private
	 * @return array
	 */
	private function get_user_group() {
		$cond = array(
			'com_id=' => $this -> com_id,
			'user_list LIKE ' => '%"'. $this -> user_id .'"%',
		);
	
		$ret = g('ndb') -> select($this -> group_table, '`id`', $cond);
		!is_array($ret) and $ret = array();

		$return = array();
		foreach ($ret as $value) {
			$return[] = $value['id'];
		}

		return $return;
		
	}

	/**
	 * 批量获取问题个数
	 *
	 * @access public
	 * @param mixed $sui_ids 调研信息id集合
	 * @return array
	 * @throws SCException
	 */
	public function get_question_number($sui_ids) {
		!is_array($sui_ids) && $sui_ids = array($sui_ids);

		$ids_str = implode(',', $sui_ids);
		$fields = array(
			'sui_id',
			'COUNT(qs_id) AS qs_count',
		);
		$fields = implode(',', $fields);

		$condition = $this -> base_con;
		$condition['sui_id IN '] = '(' . $ids_str . ')';

		$group_by = ' GROUP BY sui_id ';

		$ret = g('ndb') -> select($this -> qs_table, $fields, $condition, 0, 0, $group_by);
		if (!$ret) {
			throw new SCException('系统繁忙，请稍后再试！');
		}

		$tmp_arr = array();
		foreach ($ret as $val) {
			$tmp_arr[$val['sui_id']] = $val['qs_count'];
		}
		unset($val);
		$ret = $tmp_arr;

		return $ret;
	}

	/**
	 * 获取调研列表
	 *
	 * @access public
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param string $order 排序方式
     * @param int $is_answer 已答卷（0全部；1未答；2已答）
	 * @return array
     * @throws
	 */
	public function get_survey_list($page=1, $page_size=20, $order='',$is_answer=1) {
		$page < 1 && $page = 1;
		$page_size < 1 && $page_size = 20;

		$str = __FUNCTION__ . ':' . $page . ':' . $page_size . ':' . $order;
		$cache_key = $this -> get_cache_key($str);
		// $ret = g('redis') -> get($cache_key);

		// if (empty($ret)) {
			$table = $this -> info_table . ' AS info LEFT JOIN ' . $this -> sheet_table . 
						' AS sht ON info.sui_id=sht.sui_id AND sht.user_id=' . $this -> user_id;
			$fields = array(
				'info.sui_id', 'info.title', 'info.create_time',
				'info.state', 'info.shareable', 'info.type',
				'info.`desc`', 'info.class_name',
			);
			$fields = implode(',', $fields);

			$depts = $this -> get_user_dept();
			$groups = $this -> get_user_group();
			if (empty($depts) && empty($groups)) {
				throw new SCException('获取用户所在部门或组别失败');
			}

			$condition = array(
				'info.com_id=' => $this -> com_id,
				'info.is_del=' => 0,
				// 'info.state IN ' => '(2, 3)',
				// '^sht.st_id' => ' is null',
			);
			switch ($is_answer){
                case 1:
                    $condition['info.state IN '] = '(2)';
                    $condition['^sht.st_id'] = ' is null';
                    break;
                case 2:
                    $condition['^sht.st_id'] = ' is not null';
                    break;
            }

			$tmp_con = array();
			$tmp_con['info.user_list LIKE '] = '%"' . $this -> user_id . '"%';
			// 使用正则表达式
	        $rexp_str = '';
	        foreach ($depts as $val) {
	            $rexp_str .= $val . '|';
	        }
	        if ($rexp_str !== '') {
	            $rexp_str = substr($rexp_str, 0, -1);
	            $tmp_con['info.dept_list REGEXP '] = "\"({$rexp_str})\"";
	        }

	        $rexp_str_group = '';
	        foreach ($groups as $val) {
	            $rexp_str_group .= $val . '|';
	        }
	        if ($rexp_str_group !== '') {
	            $rexp_str_group = substr($rexp_str_group, 0, -1);
	            $tmp_con['info.group_list REGEXP '] = "\"({$rexp_str_group})\"";
	        }
			
			!empty($tmp_con) && $condition['__OR_1'] = $tmp_con;

			empty($order) && $order = ' ORDER BY info.state ASC, info.sui_id ASC ';

			$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);

			if (!empty($ret['data'])) {
				$sui_ids = array();
				foreach ($ret['data'] as $val) {
					$sui_ids[] = $val['sui_id'];
				}
				unset($val);

				try {
					$qs_count = $this -> get_question_number($sui_ids);
				}catch(SCException $e) {
					$qs_count = array();
				}

				foreach ($ret['data'] as &$val) {
					isset($qs_count[$val['sui_id']]) && $val['qs_count'] = $qs_count[$val['sui_id']];
					$val['type_str'] = $val['class_name'];
				}
				unset($val);
			}

			//保存缓存结果10秒
			g('redis') -> setex($cache_key, json_encode($ret), 10);
		// }else {
		// 	$ret = json_decode($ret, TRUE);
		// }

		return $ret;
	}

	/**
	 * 记录一些与答卷信息相关的信息
	 *
	 * @access public
	 * @return TRUE
	 */
	public function sheet_cookie_init($sui_id) {
		g('cookie') -> clean_cookie($this -> sheet_key);

		$time = time();
		$data = array(
			'sui_id' => $sui_id,
			'begin_time' => $time,
		);
		g('cookie') -> set_cookie($this -> sheet_key, $data);
		return TRUE;
	}

	/**
	 * 获取答卷相关的基础信息
	 *
	 * @access public
	 * @return array
	 */
	public function get_sheet_cookie() {
		$data = g('cookie') -> get_cookie($this -> sheet_key);
		return empty($data) ? array() : $data;
	}

	/**
	 * 获取每项调研唯一的cookie变量名
	 *
	 * @access public
	 * @param integer $sui_id 调研id
	 * @return string
	 */
	public function get_sheet_key($sui_id) {
		$key_str = __CLASS__ . ':' . __FUNCTION__ . ':' . $sui_id;
		$key = md5($key_str);
		return $key;
	}

	/**
	 * 答题结束，对外部人员生成cookie标识，避免刷票
	 *
	 * @access public
	 * @param integer $sui_id 调研id
	 * @return TRUE
	 */
	public function set_finish_cookie($sui_id) {
		$key = $this -> get_sheet_key($sui_id);
		g('cookie') -> set_cookie($key, array(1));
		return TRUE;
	}

	/**
	 * 生成外部人员的用户哈希
	 *
	 * @access public
	 * @return string
	 */
	public function get_user_hash() {
		return uniqstr();
	}

	/**
	 * 检测当前用户是否可参加该项调研
	 *
	 * @access public
	 * @param integer $sui_id 调研id
	 * @return boolean
	 * @throws SCException
	 */
	public function check_valid($sui_id) {
		//已经参与过的错误码
		$attend_code = 1001;
		//输错常规警告信息
		$warn_code = 1002;

		//log_write('HTTP_USER_AGENT=' . $_SERVER['HTTP_USER_AGENT']);
		if (!g('common') -> is_wx()) {
			//throw new SCException('请在微信客户端中打开！');
		}
		
		try {
			$info = $this -> get_survey_info($sui_id);
			log_write(json_encode($info));
		}catch(SCException $e) {
			throw new SCException($e -> getMessage());
		}

		//如果是游客或非企业内部员工，将user_id强制重置为空
		if (empty($this -> com_id) || $this -> com_id != $info['com_id']) {
			$this -> com_id = $info['com_id'];
			$this -> user_id = NULL;
		}

		// $time = time();

		// if ($info['state'] == 1) {
		// 	throw new SCException('该调研尚未发布！', $warn_code);
		// }elseif ($info['state'] == 3) {
		// 	throw new SCException('该调研已被暂停！', $warn_code);
		// }elseif ($info['state'] == 4 || $info['end_time'] <= $time) {
		// 	throw new SCException('该调研已结束！', $warn_code);
		// }

		$key = $this -> get_sheet_key($sui_id);
		$sheet_check = g('cookie') -> get_cookie($key);
		if (!empty($sheet_check)) {
			throw new SCException('已经参与过该调研，感谢您的参与！', $attend_code);
		}

		if (empty($this -> user_id)) {
			if ($info['shareable'] != 1) {
				throw new SCException('无权查看该调研！');
			}
		}else {
			$visible = $this -> check_visible($sui_id);
			if (!$visible) {
				throw new SCException('无权查看该调研！');
			}

			$condition = $this -> base_con;
			$condition['sui_id='] = $sui_id;
			$condition['user_id='] = $this ->user_id;
			$check = g('ndb') -> record_exists($this -> sheet_table, $condition);
			if ($check) {
				throw new SCException('已经参与过该调研，感谢您的参与！', $attend_code);
			}
		}

		return TRUE;
	}

	/**
	 * 判断用户是否能够查看该调研
	 *
	 * @access public
	 * @param integer $sui_id
	 * @return boolean
	 */
	public function check_visible($sui_id) {
		$depts = $this -> get_user_dept();
		$groups = $this -> get_user_group();
		if (empty($depts) && empty($groups)) {
			return FALSE;
		}
		$condition = $this -> base_con;

		$tmp_con = array();
		$tmp_con['user_list LIKE '] = '%"' . $this -> user_id . '"%';
		// 使用正则表达式
        $rexp_str = '';
        foreach ($depts as $val) {
            $rexp_str .= $val . '|';
        }

        if ($rexp_str !== '') {
            $rexp_str = substr($rexp_str, 0, -1);
            $tmp_con['dept_list REGEXP '] = "\"({$rexp_str})\"";
        }

        $rexp_str_group = '';
        foreach ($groups as $val) {
            $rexp_str_group .= $val . '|';
        }

        if ($rexp_str_group !== '') {
            $rexp_str_group = substr($rexp_str_group, 0, -1);
            $tmp_con['group_list REGEXP '] = "\"({$rexp_str_group})\"";
        }
		
		!empty($tmp_con) && $condition['__OR_1'] = $tmp_con;
		
		$check = g('ndb') -> record_exists($this -> info_table, $condition);
		return $check;
	}

	/**
	 * 提交答卷
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 * @throws SCException
	 */
	public function submit_survey(array $data) {
		$info = $this -> get_sheet_cookie();

		$time = time();
		$data['end_time'] = $time;
		if (isset($info['begin_time']) && $info['sui_id'] == $data['sui_id']) {
			$data['begin_time'] = $info['begin_time'];
			$data['answer_time'] = $time - $data['begin_time'];
		}else {
			//cookie数据丢失，伪造答题时间数据
			$data['begin_time'] = $time - 60;
			$data['answer_time'] = 60;
		}

		//内部人员user_id必定存在
		$data['user_id'] = !empty($this -> user_id) ? $this -> user_id : 0;
		//外部人员生成唯一hash，暂无大作用，保留
		$data['user_hash'] = empty($this -> user_id) ? $this -> get_user_hash() : '';

		$data['com_id'] = $this -> com_id;
		$data['ip'] = get_ip();

		//上一层处理好的答案集合
		$answer_list = $data['answer_list'];
		unset($data['answer_list']);

		//问题id集合，方便做批量操作
		$qs_ids = array();
		//由上一层处理好的选项id集合，方便做批量操作
		$option_ids = $data['option_ids'];
		$option_ids = implode(',', $option_ids);
		unset($data['option_ids']);

		//异常默认抛出的提示
		$def_tips = '提交失败，请稍后再试！';

		g('db') -> begin_trans();

		//---begin---------------写入survey_sheet表---------------------------
		$result = g('ndb') -> insert($this -> sheet_table, $data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException($def_tips);
		}
		$st_id = $result;
		//---end-----------------写入survey_sheet表---------------------------

		$com_id = $this -> com_id;
		//批量写入的基础数据集合
		$base_arr = array(
			$st_id, $com_id, $data['sui_id'], 
			$data['user_id'], $data['user_hash'],
			$time,
		);

		//批量写入字段名称集合
		$ins_fields = array(
			'st_id', 'com_id',
			'sui_id', 'user_id', 'user_hash',
			'create_time', 'qs_id', 
			'answer', 'ans_desc', 
		);
		$ins_fields = implode(',', $ins_fields);

		//批量写入数据集合，对应上面的字段名称集合
		$ins_data = array();
		foreach ($answer_list as $answer) {
			$tmp_arr = $base_arr;
			$tmp_arr[] = $answer['qs_id'];
			$tmp_arr[] = is_array($answer['answer']) ? json_encode($answer['answer']) : $answer['answer'];
			$tmp_arr[] = $answer['ans_desc'];
		 	$ins_data[] = $tmp_arr;

		 	$qs_ids[] = $answer['qs_id'];
		}
		unset($answer);

		//---begin---------------写入survey_answer表---------------------------
		$result = g('ndb') -> batch_insert($this -> ans_table, $ins_fields, $ins_data);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException($def_tips);
		}
		//---end-----------------写入survey_answer表---------------------------

		$qs_ids = implode(',', $qs_ids);
		//---begin---------------自增统计数据部分-----------------------------------------
		$table = $this -> info_table;
		$condition = array(
			'com_id=' => $com_id,
			'sui_id=' => $data['sui_id'],
		);
		$result = $this -> attend_increase($table, $condition);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException($def_tips);
		}

		$table = $this -> qs_table;
		$condition['qs_id IN '] = '(' . $qs_ids . ')';
		$result = $this -> attend_increase($table, $condition);
		if (!$result) {
			g('db') -> rollback();
			throw new SCException($def_tips);
		}

		if(!empty($option_ids)){
			$table = $this -> option_table;
			$condition['id IN '] = '(' . $option_ids . ')';
			$result = $this -> attend_increase($table, $condition);
			if (!$result) {
				g('db') -> rollback();
				throw new SCException($def_tips);
			}
		}
		//---end-----------------自增统计数据部分-----------------------------------------

		$this -> set_finish_cookie($data['sui_id']);
		g('db') -> commit();
		return TRUE;
	}

	/**
	 * 获取一项调研的基础信息
	 *
	 * @access public
	 * @param integer $sui_id 调研信息id
	 * @param boolean $with_detail 是否返回更详细的信息
	 * @return array
	 * @throws SCException
	 */
	public function get_survey_info($sui_id, $with_detail=FALSE) {
		$condition = array(
			'sui_id=' => $sui_id,
			'is_del=' => 0,
		);

		$str = __FUNCTION__ . ':sui_id:' . $sui_id . ':condition:' . json_encode($condition) . ':with_detail:' . $with_detail;
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

	//	log_write($cache_key);
	//	if (!empty($ret)) return $ret;

		$fields = array(
			'sui_id', 'title', 'type',
			'state', 'in_total','out_total', 'end_time',
			'create_time', 'shareable',
			'visible_desc', 'dept_list',
			'user_list', 'com_id', '`desc`',
			'class_name',
		);
		$fields = implode(',', $fields);
		$ret = g('ndb') -> select($this -> info_table, $fields, $condition, 1, 1);
		if (!$ret) {
			throw new SCException('查询失败，该调研不存在!');
		}

		$ret = $ret[0];
		if ($with_detail) {
			$ret['questions'] = array();
			$questions = $this -> get_question_list($sui_id);
			if (empty($questions)) {
				return $ret;
			}

			$qs_ids = array();
			foreach ($questions as $val) {
				//只有单选和多选题存在选项
				$val['type'] <= 3 && $qs_ids[] = $val['qs_id'];
			}
			unset($val);
			$opt_arr = array();
			if (!empty($qs_ids)) {
				$fields = array(
					'qs_id','id', '`desc`', 
				);
				$fields = implode(',', $fields);
				$condition = array(
					'com_id=' => $this -> com_id,
				);

				$order = ' ORDER BY id ASC ';
				$options = g('ndb') -> get_data_by_ids($this -> option_table, $qs_ids, 'qs_id', $fields,$order,'', $condition);
				if ($options) foreach ($options as $option) {
					!isset($opt_arr[$option['qs_id']]) && $opt_arr[$option['qs_id']] = array();
					$opt_arr[$option['qs_id']][] = $option;
				}
				unset($option);
			}

			foreach ($questions as &$val) {
				$val['type'] <= 3 && isset($opt_arr[$val['qs_id']]) && $val['options'] = $opt_arr[$val['qs_id']];
			}
			unset($val);

			$ret['questions'] = $questions;
		}

		//缓存30分钟
		g('redis') -> setex($cache_key, json_encode($ret), 1800);

		return $ret;
	}
    /**
     * 获取一项调研的基础信息
     *
     * @access public
     * @param integer $sui_id 调研信息id
     * @return array
     * @throws SCException
     */
    public function get_survey_answer_info($sui_id) {
        $condition = array(
            'sui_id=' => $sui_id,
            'is_del=' => 0,
        );

        $str = __FUNCTION__ . ':sui_id:' . $sui_id . ':condition:' . json_encode($condition);
        $cache_key = $this -> get_cache_key($str);
        $ret = g('redis') -> get($cache_key);

        //	log_write($cache_key);
        //	if (!empty($ret)) return $ret;

        $fields = array(
            'sui_id', 'title', 'type',
            'state', 'in_total','out_total', 'end_time',
            'create_time', 'shareable',
            'visible_desc', 'dept_list',
            'user_list', 'com_id', '`desc`',
            'class_name',
        );
        $fields = implode(',', $fields);
        $ret = g('ndb') -> select($this -> info_table, $fields, $condition, 1, 1);
        if (!$ret) {
            throw new SCException('查询失败，该调研不存在!');
        }

        $ret = $ret[0];
        $ret['questions'] = array();
        $questions = $this -> get_question_list($sui_id);
        if (empty($questions)) {
            return $ret;
        }

        $answer = $this->get_answers($sui_id);
        $qs_ids = array();
        foreach ($questions as &$val) {
            //只有单选和多选题存在选项
            $val['type'] <= 3 && $qs_ids[] = $val['qs_id'];
            $val['answer'] = $answer[$val['qs_id']];
        }
        unset($val);
        $opt_arr = array();
        if (!empty($qs_ids)) {
            $fields = array(
                'qs_id','id', '`desc`',
            );
            $fields = implode(',', $fields);
            $condition = array(
                'com_id=' => $this -> com_id,
            );

            $order = ' ORDER BY id ASC ';
            $options = g('ndb') -> get_data_by_ids($this -> option_table, $qs_ids, 'qs_id', $fields,$order,'', $condition);
            if ($options) foreach ($options as $option) {
                !isset($opt_arr[$option['qs_id']]) && $opt_arr[$option['qs_id']] = array();
                $opt_arr[$option['qs_id']][] = $option;
            }
            unset($option);
        }

        foreach ($questions as &$val) {
            $val['type'] <= 3 && isset($opt_arr[$val['qs_id']]) && $val['options'] = $opt_arr[$val['qs_id']];
        }
        unset($val);

        $ret['questions'] = $questions;

        return $ret;
    }
    public function get_answers($sui_id)
    {
        $fields = 'qs_id,answer,ans_desc';
        $condition = [
            'com_id='=>$this->com_id,
            'user_id='=>$this->user_id,
            'sui_id='=>$sui_id
        ];
        $result = g('ndb') -> select($this -> ans_table, $fields, $condition);
        $data = [];
        foreach ($result as $item){
            $data[$item['qs_id']] = $item;
        }
        return $data;
    }
	/**
	 * 获取调研的全部问题信息列表
	 *
	 * @access public
	 * @param integer $sui_id 调研信息id
	 * @return array
	 */
	public function get_question_list($sui_id) {
		$fields = array(
			'qs_id', 'serial', '`desc`', 'astrict',
			'type', 'in_total','out_total', 'update_time',
		);
		$fields = implode(',', $fields);

		$condition = array();
		$condition['com_id='] = $this -> com_id;
		$condition['sui_id='] = $sui_id;

		$order = ' ORDER BY serial ASC ';

		$ret = array();

		$result = g('ndb') -> select($this -> qs_table, $fields, $condition, 0, 0, '', $order);
		if (!is_array($result)) {
			return $ret;
		}

		return $result;
	}

	/**
	 * 增加对应表中的统计人数
	 *
	 * @access public
	 * @param string $table 数据表
	 * @param array $condition 条件
	 * @return boolean
	 */
	public function attend_increase($table, $condition) {
		$field = !empty($this -> user_id) ? 'in_total' : 'out_total';
		$where = g('ndb') -> compose_where($condition);
		$sql = 'UPDATE `' . $table . '` set `' . $field . '`=`' . $field . '` + 1 ' . $where;
		$result = g('db') -> query($sql);
		return $result;
	}

	/**
	 * 获取问题的类型集合
	 *
	 * @access public
	 * @param mixed $sui_id 调研id
	 * @return array
	 * @throws SCException
	 */
	public function get_qs_types($sui_id) {
		$condition = array(
			'com_id=' => $this -> com_id,
			'sui_id= ' => $sui_id,
		);

		$str = __FUNCTION__ . ':sui_id:' . $sui_id . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$fields = array(
			'qs_id', 'type',
		);
		$fields = implode(',', $fields);

		$result = g('ndb') -> select($this -> qs_table, $fields, $condition);
		if (!$result) {
			throw new SCException('获取问题类型失败！');
		}

		$ret = array();
		foreach ($result as $val) {
			$ret[$val['qs_id']] = $val['type'];
		}
		unset($val);

		//结果缓存30分钟
		g('redis') -> setex($cache_key, json_encode($ret), 1800);

		return $ret;
	}

	/**
	 * 获取选项相关的信息
	 *
	 * @access public
	 * @param integer $sui_id 调研id
	 * @return array
	 */
	public function get_options_info($sui_id) {
		$condition = array(
			'com_id=' => $this -> com_id,
			'sui_id= ' => $sui_id,
		);

		$str = __FUNCTION__ . ':condition:' . json_encode($condition);
		$cache_key = $this -> get_cache_key($str);
		$ret = g('redis') -> get($cache_key);

		if (!empty($ret)) return json_decode($ret, TRUE);

		$ret = array();

		$fields = array(
			'id', 'qs_id', '`desc`', 
		);
		$fields = implode(',', $fields);

		$result = g('ndb') -> select($this -> option_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val;
			}
			unset($val);
		}

		//结果缓存30分钟
		g('redis') -> setex($cache_key, json_encode($ret), 1800);

		return $ret;
	}
}