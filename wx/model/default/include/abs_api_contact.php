<?php
/**
 * 与部门/成员查询相关的api接口基础类
 * 仅限与下列相关的操作，其他的禁止继承该类：
 * 1.虚拟组织架构；2.部门；3.成员；4.群组/标签
 * 
 * @author yangpz
 * @date 2015-12-19
 *
 */
abstract class abs_api_contact {	
	/** 当前操作的企业ID */
	protected $com_id;
	/** 当前操作的企业的根部门ID */
	protected $root_id;
	/** 构造当前实例的配置信息 */
	protected $api_conf;
	
	/** 仅过滤成员 1 */
	protected $filter_type_user = 1;
	/** 仅过滤部门 2 */
	protected $filter_type_dept = 2;
	
	/** 是否根据虚拟组织架构过滤掉架构外的部门/成员，默认为FALSE */
	protected $struct_filter = FALSE;
	/** 是否根据通讯录应用过滤部门/成员，默认为FALSE */
	protected $contact_filter = FALSE;
	
	public function __construct(array $conf=array()) {
		$this -> api_conf = $conf;
		//企业信息配置
		$need_logined = !isset($conf['logined']) ? TRUE : $conf['logined'];	//是否为登录状态下调用
		if ($need_logined) {	//登录状态，直接取session
			if (!isset($_SESSION[SESSION_VISIT_COM_ID]) || empty($_SESSION[SESSION_VISIT_COM_ID]))	throw new SCException('登录超时，请重新登录后再操作');
			if (!isset($_SESSION[SESSION_VISIT_DEPT_ID]) || empty($_SESSION[SESSION_VISIT_DEPT_ID]))	throw new SCException('登录超时，请重新登录后再操作');
			$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$this -> root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
			
		} else {				//非登录状态，需要配置企业信息
			if (!isset($conf['com_id']) || empty($conf['com_id']))		throw new SCException('非法请求，未指定企业');
			if (!isset($conf['root_id']) || empty($conf['root_id']))	throw new SCException('非法请求，未指定企业');
			$this -> com_id = intval($conf['com_id']);
			$this -> root_id = intval($conf['root_id']);
		}
		//架构过滤配置
		if (isset($conf['struct_filter']) && $conf['struct_filter']) {
			$this -> struct_filter = TRUE;
		}
		//通讯录应用过滤配置
		if (isset($conf['contact_filter']) && $conf['contact_filter']) {
			$this -> contact_filter = TRUE;
		}
	}
	
	/**
	 * 获取部门及其子..部门ID字符串列表（不做过滤）
	 * @param unknown_type $type		返回的数组的value类型:string：string类型；array:数组类型
	 * @param unknown_type $no_cache	是否不从缓存中取数据，默认为：FALSE
	 * 
	 * @return 如：array(1 => '2,22,3,33')，表示部门ID为'2,22,3,33'的部门是部门ID为1的部门的子..部门
	 */
	protected final function list_all_child_dept_ids_str($type='string', $no_cache=FALSE) {
		$fields = 'id,p_id';
		$cache_key = __FUNCTION__.':'.$this -> root_id.':'.$fields.':'.$type;
		$cache_key = $this -> _get_cache_key($cache_key);

		if ($no_cache) {
			$ret = $this -> list_all_dept_by_lev(0, $fields, FALSE);
			$ret = $this -> _sort_child_ids_str($ret, $type);
			g('redis') -> set($cache_key, json_encode($ret), 300);			//缓存5分钟
			
		} else {
			$ret = g('redis') -> get($cache_key);
			if ($ret) {
				$ret = json_decode($ret, TRUE);
				
			} else {
				$ret = $this -> list_all_dept_by_lev(0, $fields, FALSE);
				$ret = $this -> _sort_child_ids_str($ret, $type);	
				g('redis') -> set($cache_key, json_encode($ret), 300);		//缓存5分钟
			}
		}

		return $ret;
	}

	/**
	 * 获取指定层级的部门列表
	 * @param unknown_type $lev			层级，默认为: 0(表示获取该企业所有层级)
	 * @param unknown_type $fields		查找的字段，默认查找全部
	 * @param unknown_type $auto_filter	是否自动按架构进行过滤，默认为TRUE
	 */
	protected function list_all_dept_by_lev($lev=0, $fields='*', $auto_filter=TRUE) {
		$table = 'sc_dept as t1';
		
		!is_array($fields) && $fields = explode(',', $fields);
		if (empty($fields)) {
			return array();
		}
		array_walk($fields, function(&$v, $k, $prefix) {
			$v = trim($v);
			$v = $prefix . $v;
		}, 't1.');
		$fields = implode(',', $fields);

		$cond = array( 
			't1.root_id=' => $this -> root_id,
			't1.state=' => 1
		);
		$lev > 0 && $cond['t1.lev='] = $lev;

		// gch add contact config
		$hiddenConfig = g('contact_config') -> getHiddenConfig($this -> com_id, $this -> user_id);
		if (!empty($hiddenConfig['dept'])) {
			$cond['t1.id NOT REGEXP '] = '(' . implode('|', $hiddenConfig['dept']) . ')';
		}
		$watchOther = g('contact_config') -> checkWatchOther($this -> com_id, $this -> user_id);
		if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$dept_sql = "(SELECT * FROM sc_dept t2 WHERE t2.id IN (" . implode(',', $watchOther['dept']) . ") AND (t2.p_list REGEXP (t1.id) OR t2.id=t1.id))";
			$cond['^EXISTS '] = $dept_sql;
		}
		
		$order = ' ORDER BY t1.lev, t1.idx ';

		$ret = g('ndb') ->  select($table, $fields, $cond, 0, 0, '', $order);
		$ret = $ret ? $ret : array();
		$auto_filter && $this -> _list_with_struct_filter_info($ret);
		return $ret;
	}
	
    /**
     * 获取缓存变量名
     * @param string $str 关键字符串
     * @return string
     */
    protected function _get_cache_key($str) {}

    //虚拟架构过滤=============================================================================
    
	/**
	 * 执行单条记录的 组织架构过滤，仅ID
	 * @param unknown_type $type	过滤类型（部门/成员），使用类变更赋值
	 * @param unknown_type $ret		要过滤的内容
	 */
	protected function _get_with_struct_filter_id($type, &$info) {
		if (empty($info))				return;
		
		if (!$this -> struct_filter)	return;	//不过滤
		
		//TODO
	}
	
	/**
	 * 执行多条记录的 组织架构过滤，仅ID
	 * @param array $ret		要过滤的内容
	 */
	protected function _list_with_struct_filter_id(array &$depts=array(), array &$users=array()) {
		if (!$this -> struct_filter)				return;	//不过滤

		if (!empty($depts)) {	//过滤部门
		//TODO
		}
		
		if (!empty($users)) {	//过滤成员
		//TODO
		}
	}
	
	/**
	 * 执行单条记录的 组织架构过滤
	 * @param unknown_type $type	过滤类型（部门/成员），使用类变更赋值
	 * @param unknown_type $ret		要过滤的内容
	 */
	protected function _get_with_struct_filter_info($type, &$info) {
		if (empty($info))				return;
		
		if (!$this -> struct_filter)	return;	//不过滤
		
		//TODO
	}
	
	/**
	 * 执行多条记录的 组织架构过滤
	 * @param array $ret		要过滤的内容
	 */
	protected function _list_with_struct_filter_info(array &$depts=array(), array &$users=array()) {
		if (!$this -> struct_filter)				return;	//不过滤
		
		if (!empty($depts)) {	//过滤部门
		//TODO
		}
		
		if (!empty($users)) {	//过滤成员
		//TODO
		}
	}

	/**
	 * 获取部门及其子..部门的ID
	 * @param array $depts				目标部门ID列表
	 * @param array $all_child_depts	部门层级映射
	 */
	protected function merge_child_dept_ids(array $dept_ids, $all_child_depts=array()) {
		if (empty($all_child_depts)) {
			$all_child_depts = $this -> list_all_child_dept_ids_str('array', FALSE);
		}
		
		$ret = array();
		foreach ($dept_ids as $d) {
			if (!isset($all_child_depts[$d])) {
				log_write('部门不存在：root_id='.$this -> root_id.', dept_id='.$d, 'ERROR');
				continue;
			}
			$ret = array_merge($ret, $all_child_depts[$d]);
		}unset($d);
		return array_unique($ret);
	}

	//内部实现 ==============================================================================================================
	
	/**
	 * 获取部门层级关系(部门=》下级部门列表(含自身\子子...部门))
	 * @param unknown_type $dept_list	部门列表
	 * @param unknown_type $type	返回的数组的value类型, string：string类型；array: 数组类型
	 */
	private function _sort_child_ids_str(array $dept_list, $type=TRUE) {
		$sorted_dept = array();
		foreach ($dept_list as $dept) {
			$sorted_dept[$dept['id']] = $dept;
		}unset($dept);
		
		$max_lev = 15;
		foreach ($dept_list as &$dept) {
			$sorted_dept[$dept['id']]['childs'][] = $dept['id'];
			$p_id = $dept['p_id'];
			$curr_lev = 0;
			while ($p_id > 0) {
				if (isset($sorted_dept[$p_id])) {
					$sorted_dept[$p_id]['childs'][] = $dept['id'];
					$p_id = $sorted_dept[$p_id]['p_id'];
					
				} else {
					break;
				}
				if (++$curr_lev > $max_lev)	break;
			}
		}
		
		$ret = array();
		if (strtolower($type) == 'array') {
			foreach ($sorted_dept as $d) {
				$ret[$d['id']] = $d['childs'];
			}unset($d);
			
		} else {
			foreach ($sorted_dept as $d) {
				$ret[$d['id']] = implode(',', $d['childs']);
			}unset($d);
		}
		return $ret;
	}
	
}

//end