<?php
/**
 * 部门类
 * 
 * @author yangpz
 * @date 2014-10-20
 *
 */
class cls_dept {
	/** 对应的库表名称 */
	private static $Table = 'sc_dept';
	
	/** 禁用 */
	private static $StateOff = 0;
	/** 启用 */
	private static $StateOn = 1;
	
	/**
	 * 获取部门层级关系列表
	 * @param unknown_type $dept_id
	 * @param unknown_type $fields
	 */
	public function get_dept_parent_list($dept_id, $fields='*') {
		$list = $this -> get_by_dept($dept_id, 0, $fields);
		$list = $this -> sort_parent_dept($list);
		return $list;
	}
	
	/**
	 * 获取指定部门的上级..部门ID
	 * @param unknown_type $root_id	企业根部门ID
	 * @param unknown_type $dept_id	要查询的部门的ID
	 */
	public function get_single_dept_parent_ids($root_id, $dept_id) {
		$redis_key = $this -> _get_cache_key(__FUNCTION__.':'.$root_id);
		$all_dept = g('redis') -> get($redis_key);
		if ($all_dept) {
			$all_dept = json_decode($all_dept, TRUE);
			
		} else {
			$all_dept = $this -> get_by_dept($root_id);
			$sorted_dept = array();
			foreach ($all_dept as $dept) {
				$sorted_dept[$dept['id']] = array('id' => $dept['id'], 'p_id' => $dept['p_id']);
			}unset($dept);
			unset($all_dept);
			$all_dept = $sorted_dept;
			g('redis') -> set($redis_key, json_encode($all_dept), 60);		//缓存1分钟
		}
		
		$parent_ids = array();
		$p_id = $all_dept[$dept_id]['p_id'];
		$parent_ids[] = $dept_id;
		$max_lev = 15;		//最大部门层级
		$curr_lev = 1;
		while ($p_id > 0) {
			$parent_ids[] = $all_dept[$p_id]['id'];
			$p_id = $all_dept[$p_id]['p_id'];
			
			if (++$curr_lev > $max_lev)	break;
		}
		return $parent_ids;
	}

	/**
	 * 获取企业部门层级列表
	 * @param unknown_type $dept_id	企业对应的部门ID
	 * @return 多维数组，每一维对应一级部门
	 */
	public function list_dept($dept_id, $fields="*") {
		$list = $this -> get_by_dept($dept_id, 0, $fields);
		$list = $this -> sort_dept($list);
		return $list;
	}
	
	/**
	 * 按层级显示部门，html树显示
	 * @param unknown_type &$html 		最终显示的html
	 * @param unknown_type $dept_list
	 * @param unknown_type $with_checkbox	是否还复选框
	 * @param unknown_type $root			是否是根部门
	 */
	public function get_dept_list_tree(&$html, $dept_list, $with_checkbox=FALSE, $root=TRUE) {
		
		$display = '';
		if (!$root) {
			$display = 'style="display:none;"';
		}
		
		if (!is_array($dept_list))	return;
		foreach ($dept_list as $dept) {
			$checkbox = '';
			if ($with_checkbox) {
				$checkbox = '<label>
					<input type="checkbox" p="'.$dept['id'].'">
					<span class="lbl"></span>
				</label>';
			}
			if (isset($dept['childs'])) {
				$html .= '<ul><li '.$display.' p="'.$dept['id'].'"><span><i class="icon-plus-sign gray"></i> <i class="icon-folder-open blue"></i> </span> <a href="javascript:void(0);"><span>'.$dept['name'].'</span>'.$checkbox.'</a>';
				$this -> get_dept_list_tree($html, $dept['childs'], $with_checkbox, FALSE);
			} else {
				$html .= '<ul><li '.$display.' p="'.$dept['id'].'"><span><i class="icon-folder-open blue"></i> </span> <a href="javascript:void(0);"><span>'.$dept['name'].'</span>'.$checkbox.'</a>';
			}
			$html.= '</li></ul>';
		}
	}
	
	/**
	 * 获取企业部门，默认只获取企业的第一级部门列表
	 * @param unknown_type $dept_id	企业对应的部门ID
	 * @param unknown_type $lev		层级，默认为企业的一级部门,若lev = 0，表示获取该企业所有部门
	 */
	public function get_by_dept($dept_id, $lev=0, $fields='*') {
		if($lev == 0){
		   $cond = array( 
			'root_id=' => $dept_id,
			'state=' => self::$StateOn
		   );
		}else{
		    $cond = array( 
			'root_id=' => $dept_id,
			'lev=' => $lev, 
			'state=' => self::$StateOn
		    );
		}

		$order = ' ORDER BY lev, idx, id ';
		
		return g('ndb') ->  select(self::$Table, $fields, $cond, 0, 0, '', $order);
	}
	
	/**
	 * 获取一个员工的管理部门
	 * @param unknown_type $id
	 */
	function get_user_admin_dept($id){
		$sql = <<<EOF
SELECT id 
FROM sc_dept 
WHERE boss = {$id} or second_boss like '%"{$id}"%' 
EOF;
		$ret = g('db') -> select($sql);
		$depts = array();
		foreach($ret as $r){
			$depts[] = $r['id'];
		}
		return $depts;
	}
	
	/**
	 * 根据ID列表获取部门名称列表
	 * @param unknown_type $dept_id_list
	 */
	public function get_dept_name($dept_id_list, $fields='id,`name`') {
		$dept_id_list = implode(',', $dept_id_list);
		$dept_id_list = mysql_escape_string($dept_id_list);
		$sql = " select $fields from ".self::$Table ." where id in(".$dept_id_list.")";
		$result = g('db') -> select($sql);
		return $result;
	}
	
	/**
	 * 按照ID返回部门信息
	 * @param unknown_type $dept_id	部门id
	 * @return 所有子部门的ID
	 */
	public function get_dept_by_id($dept_id,$fields='*') {
        $cond = array(
			'id =' => $dept_id
		);
		$result  = g("ndb")->select(self::$Table,$fields,$cond);
		
		return $result?$result[0]:false;
	}
	
 	//----------------------------------内部实现----------------------------------
	
	/**
	 * 将部门列表，按层级关系排序
	 * @param unknown_type $dept_list	部门列表
	 * @param unknown_type $lev			部门列表的顶级级别，默认为部门列表最上级级别
	 */
	private function sort_dept($dept_list, $target_lev=NULL) {		
		$sorted_dept = array();
		foreach ($dept_list as $dept) {
			$sorted_dept[$dept['id']] = $dept;
		}unset($dept);
		unset($dept_list);
		
		foreach ($sorted_dept as &$dept) {
			if ($dept['p_id'] != 0 && isset($sorted_dept[$dept['p_id']])) {
				$sorted_dept[$dept['p_id']]['childs'][] = &$sorted_dept[$dept['id']];
			}
		}
		
		$ret = array();
		foreach ($sorted_dept as $d) {
			if ($d['p_id'] == 0) {
				$ret[] = $d;
			}
		}unset($d);
		return $ret;
	}

	/**
	 * 获取部门层级关系
	 * @param unknown_type $dept_list	部门列表
	 */
	private function sort_parent_dept($dept_list) {
		$result = array();
		$top_lev = 9999;
		foreach ($dept_list as $dept) {
			if ($dept['lev'] < $top_lev)	$top_lev = $dept['lev'];
		}
		
		foreach ($dept_list as $dept) {
			$str = '';
			$this -> get_parent_dept_str($str, $dept_list, $dept, $top_lev);
			$result[$dept['id']] = substr($str, 0, strlen($str)-1);
		}
		return $result;
	}
	
	/**
	 * 递归，拼接上级ID列表
	 */
	private function get_parent_dept_str(&$parent_dept_str, $dept_list, $curr_dept, $top_lev) {
		$parent_dept_str .= $curr_dept['id'] . ',';
		if ($curr_dept['lev'] == $top_lev)	return;
		
		foreach ($dept_list as $dept) {
			if ($curr_dept['p_id'] == $dept['id']) {
				$this -> get_parent_dept_str($parent_dept_str, $dept_list, $dept, $top_lev);
			}
		}
	}

	/**
	 * 根据deptid数组获取部门信息
	 * @param unknown_type $array_dept		ID数组 ['1','2','3']
	 * @param unknown_type $fields			
	 */
	public function get_dept_list($array_dept,$fields='*'){
		if(empty($array_dept)){
			return array();
		}
		$cond = array();
		foreach($array_dept as $dept){
			array_push($cond, " id = {$dept} ");
		} 
		$cond = implode(' or ',$cond);

		$sql = "select {$fields} from ".self::$Table." where {$cond}";
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 获取缓存KEY
	 * @param array $key_params	生成KEY的参数
	 */
	private function _get_redis_cache_key(array $key_params) {
		$key = SYSTEM_HTTP_DOMAIN.':'.__CLASS__.':'.__FUNCTION__;
		foreach ($key_params as $p) {
			$p .= ':'.strval($p);
		}
		unset($p);
		return md5($key);
	}
	
	/**
	 * 按层级显示部门
	 * @param unknown_type $dept_list
	 * @param unknown_type $lev
	 */
	public function print_dept_list($dept_list, $lev) {
		$tmp = $lev;
		$pre = '';
		while (--$tmp)	$pre.='&nbsp;&nbsp;&nbsp;&nbsp;';
		foreach ($dept_list as $dept) {
			echo $pre.$dept['name'].'<br>';
			if (isset($dept['childs'])) {
				$this -> print_dept_list($dept['childs'], $lev+1);
			}
		}
	}

	 /**
     * 获取缓存变量名
     *
     * @access private
     * @param string $str 关键字符串
     * @return string
     */
    private function _get_cache_key($str) {
        $key_str = SYSTEM_COOKIE_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
        return md5($key_str);
    }
	
}

// end of file