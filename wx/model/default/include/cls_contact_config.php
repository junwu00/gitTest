<?php
/**
 * 通讯录权限配置操作类
 * 隐藏的部门/成员:被隐藏的部门或成员,不会显示在公司通讯录中
 * 限制查看外部门:被限制的部门,只能看到本部门的通讯录
 * 限制查看所有人:被限制的部门或成员,不能看到企业所有通讯录
 * 
 * @author gch
 * @date 2018-02-06 12:34:29
 *
 */
class cls_contact_config {
	
	/** 对应的库表 */
	public static $TABLE = 'contact_config';
	
    // 数据库连接句柄
	private $database;
    // redis连接句柄
	private $redis;
    // 缓存数据时间（秒）
    private static $TTL = 5;
	
    // 默认字段
    private static $FIELD = "power_type, person_type, user_list, dept_list, group_list, `key`";

	/** 删除/禁用 0 */
	private static $STATE_OFF 	= 0;
	/** 启用 1 */
	private static $STATE_ON 	= 1;

	// 权限类型, 1-隐藏的部门/成员 2-限制查看外部门 3-限制查看所有人
	private static $POWER_HIDDEN = 1;
	private static $POWER_OTHER = 2;
	private static $POWER_ALL = 3;
	// 人员类型, 1-人员配置 2-白名单 3-额外配置
	private static $PERSON_LIST = 1;
	private static $PERSON_WHITE = 2;
	private static $PERSON_EXTERNAL = 3;

	public function __construct() {
		$this -> database = g('ndb');
		$this -> redis = g('redis');
	}
	
	/**
	* 获取对指定用户id隐藏的部门/成员/分组
	*   
	* @access public
	* @param int $comId 
	* @param int $userId 
	* @return   
	*/
	public function getHiddenConfig($comId, $userId) {
		$return = array(
			'user' => array(),
			'dept' => array(),
			'group' => array(),
		);
		if (empty($comId) || empty($userId)) {
			return $return;
		}

		$key = sprintf("%s-%s-key:%d:%d", __CLASS__, __FUNCTION__, $comId, $userId);
		$cacheData = $this -> getCacheData($key);
		if (!empty($cacheData)) {
			return $cacheData;
		}

		$condition = array(
			'com_id=' => $comId,
			'power_type=' => self::$POWER_HIDDEN,
			'info_state=' => self::$STATE_ON,
		);
		$rows = $this -> database -> select(self::$TABLE, self::$FIELD, $condition);
		if (!empty($rows)) {
			// 根据key整理同组数据
			$rows = $this -> combineData($rows);

			// 用户通讯录信息
			$userInfo = $this -> getUserContact($comId, $userId);

			$userList = array();
			$deptList = array();
			$groupList = array();
			foreach ($rows as $v) {

				// 白名单判断
				if (in_array($userInfo['user_id'], $v['white']['user_list']) || !empty(array_intersect($userInfo['dept_tree'], $v['white']['dept_list'])) || in_array($userInfo['group_id'], $v['white']['group_list'])) {
					continue;
				}

				$userList = array_merge($userList, $v['list']['user_list']);
				$deptList = array_merge($deptList, $v['list']['dept_list']);
				$groupList = array_merge($groupList, $v['list']['group_list']);
			}
			$return['user'] = array_map('intval', array_unique($userList));
			$return['dept'] = array_map('intval', array_unique($deptList));
			$return['group'] = array_map('intval', array_unique($groupList));
		}

		$this -> setCacheData($key, $return);
		
		return $return;
	}
	
	/**
	* 检测是否被限制查看外部门,若被限制,则同时获取可见部门/成员/分组
	* @access public
	* @param int $comId
	* @param int $userId 用户id
	* @return
	*/
	public function checkWatchOther($comId, $userId) {
		$return = array(
			'flag' => false, // 是否被限制查看外部门 false-不限制 true-限制
			'user' => array(), // 可见成员
			'dept' => array(), // 可见部门
			'group' => array(), // 可见分组
		);
		if (empty($comId) || empty($userId)) {
			return $return;
		}

		$key = sprintf("%s-%s-key:%d:%d", __CLASS__, __FUNCTION__, $comId, $userId);
		$cacheData = $this -> getCacheData($key);
		if (!empty($cacheData)) {
			return $cacheData;
		}

		$condition = array(
			'com_id=' => $comId,
			'power_type=' => self::$POWER_OTHER,
			'info_state=' => self::$STATE_ON,
		);
		$rows = $this -> database -> select(self::$TABLE, self::$FIELD, $condition);
		if (!empty($rows)) {
			// 根据key整理同组数据
			$rows = $this -> combineData($rows);

			// 用户通讯录信息
			$userInfo = $this -> getUserContact($comId, $userId);

			$flag = false;
			$userList = array();
			$deptList = array();
			$groupList = array();
			foreach ($rows as $v) {
				// 人员配置判断
				if (!in_array($userInfo['user_id'], $v['list']['user_list']) && empty(array_intersect($userInfo['dept_tree'], $v['list']['dept_list'])) && !in_array($userInfo['group_id'], $v['list']['group_list'])) {
					continue;
				}

				// 白名单判断
				if (in_array($userInfo['user_id'], $v['white']['user_list']) || !empty(array_intersect($userInfo['dept_tree'], $v['white']['dept_list'])) || in_array($userInfo['group_id'], $v['white']['group_list'])) {
					continue;
				}

				$flag = true;
				// 额外配置
				$userList = array_merge($userList, $v['external']['user_list']);
				$deptList = array_merge($deptList, $v['external']['dept_list']);
				$groupList = array_merge($groupList, $v['external']['group_list']);

			}
			// 主部门
			$deptList[] = $userInfo['main_dept'];
			
			if ($flag) {
				$return['flag'] = true;
				$return['user'] = array_map('intval', array_unique($userList));
				$return['dept'] = array_map('intval', array_unique($deptList));
				$return['group'] = array_map('intval', array_unique($groupList));
			}
		}

		$this -> setCacheData($key, $return);

		return $return;
	}
	
	/**
	* 检测是否被限制查看所有人
	*   
	* @access public
	* @param int $userId 用户id
	* @return boolean true-是  false-否
	*/
	public function checkWatchAll($comId, $userId) {
		$flag = false;

		if (empty($comId) || empty($userId)) {
			return $flag;
		}

		$key = sprintf("%s-%s-key:%d:%d", __CLASS__, __FUNCTION__, $comId, $userId);
		$cacheData = $this -> getCacheData($key);
		if (!empty($cacheData)) {
			return $cacheData;
		}

		$condition = array(
			'com_id=' => $comId,
			'power_type=' => self::$POWER_ALL,
			'info_state=' => self::$STATE_ON,
		);
		$rows = $this -> database -> select(self::$TABLE, self::$FIELD, $condition);
		if (!empty($rows)) {
			// 根据key整理同组数据
			$rows = $this -> combineData($rows);

			// 用户通讯录信息
			$userInfo = $this -> getUserContact($comId, $userId);

			$flag = false;
			foreach ($rows as $v) {
				// 人员配置判断
				if (!in_array($userInfo['user_id'], $v['list']['user_list']) && empty(array_intersect($userInfo['dept_tree'], $v['list']['dept_list'])) && !in_array($userInfo['group_id'], $v['list']['group_list'])) {
					continue;
				}

				$flag = true;
				break;
			}
			
		}

		$this -> setCacheData($key, $flag);

		return $flag;
	}

	// 根据key合并同条数据
	private function combineData($rawData) {
		$return = array();

		foreach ($rawData as $rd) {
			if (!isset($return[$rd['key']])) {
				$return[$rd['key']] = array();
			}
			
			switch ($rd['person_type']) {
				case self::$PERSON_LIST:
					$k = 'list';
					break;

				case self::$PERSON_WHITE:
					$k = 'white';
					break;

				case self::$PERSON_EXTERNAL:
					$k = 'external';
					break;
				
				default:
					$k = '';
					break;
			}
			if (empty($k)) {
				continue;
			}

			$return[$rd['key']][$k] = array(
				'user_list' => json_decode($rd['user_list'], TRUE),
				'dept_list' => json_decode($rd['dept_list'], TRUE),
				'group_list' => json_decode($rd['group_list'], TRUE),
			);
		}
		$return = array_values($return);

		return $return;
	}

	// 获取用户的通讯录信息
	private function getUserContact($comId, $userId) {
		$return = array(
			'user_id' => $userId,
			'main_dept' => 0,
			'dept_list' => array(),
			'dept_tree' => array(),
			'group_id' => array(),
		);

		$userCondition = array(
			'com_id=' => $comId,
			'id=' => $userId,
		);
		$userInfo = $this -> database -> select('sc_user', 'id, dept_list, dept_tree', $userCondition);
		if (!empty($userInfo)) {
			$userInfo = array_shift($userInfo);
			$deptList = json_decode($userInfo['dept_list'], TRUE);
			$userInfo['dept_tree'] = json_decode($userInfo['dept_tree'], TRUE);
			$deptTree = array();
			foreach ($userInfo['dept_tree'] as $v) {
				$deptTree = array_merge($deptTree, $v);
			}
			$deptTree = array_unique($deptTree);

			$return['main_dept'] = reset($deptList);
			$return['dept_list'] = $deptList;
			$return['dept_tree'] = $deptTree;
		}

		$groupCondition = array(
			'user_list like ' => '%"' . $userId . '"%',
			'com_id=' => $comId,
			'info_state=' => self::$STATE_ON,
		);
		$groupInfo = $this -> database -> select('sc_user', 'id', $groupCondition);
		if (!empty($groupInfo)) {
			$return['group_id'] = array_column($groupInfo, 'id');
		}

		return $return;
	}

	private function getCacheData($key) {
		$cacheData = array();

		$key = md5($key);
		$flag = $this -> redis -> exists($key);
		if ($flag) {
			$data = $this -> redis -> get($key);
			$cacheData = json_decode($data, TRUE);
		}
		
		return $cacheData;
	}

	private function setCacheData($key, $rawData) {
		$key = md5($key);		
		$flag = $this -> redis -> exists($key);
		if ($flag) {
			$data = $this -> redis -> delete($key);
		}
		
		$rawData = json_encode($rawData);
		$data = $this -> redis -> set($key, $rawData, self::$TTL);
		
		return true;
	}
	
}

//end