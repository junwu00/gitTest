<?php
/**
 * 意见反馈数据模型
 *
 * @author LiangJianMing
 * @create 2015-12-31
 */

class cls_advice_reply {
	// 公司id
	private $com_id = 0;
	// 基础查询条件
	private $base_con = array();

	// 反馈信息表
	private static $info_table = 'advice_info';
	// 反馈回复表
	private static $reply_table = 'advice_reply';
	// 反馈分类表
	private static $class_table = 'advice_class';
	// 反馈主题浏览状态表
	private static $state_table = 'advice_visit_state';
	// 反馈动态表
	private static $trend_table = 'advice_trend';
	// 反馈附件表
	private static $file_table = 'advice_file';

	// 用户信息表
	private static $user_table = 'sc_user';
	// 部门信息表
	private static $dept_table = 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;

		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
	}


	/**
	 * 添加回复
	 **/
	public function add_reply($data){
		$now = time();
		$data['com_id'] = $this -> com_id;
		$data['create_time'] = $now;
		$data['update_time'] = $now;
		$ret = g('ndb') -> insert(self::$reply_table,$data);
		return $ret;
	}

	/**
	 * 删除回复
	 **/
	public function remove_reply($reply_id){
		$now = time();
		$cond = array(
				'com_id=' => $this -> com_id,
				'update_time=' => $now,
			);
		$data = array(
				'state' => 0,
			);

		$ret = g('ndb') -> update_by_condition(self::$reply_table,$cond,$data);
		return $ret;
	}

	/**
	 * 获取反馈回复列表
	 * @param   $advice_id
	 * @param   $page
	 * @return  array
	 */
	public function get_list($advice_id,$fields='r1.*,count(r2.id) reply_count',$page=1,$page_size=20){
		$cond = array(
				'r1.adi_id=' => $advice_id,
				'r1.com_id=' => $this -> com_id,
				'r1.to_reply_id =' => 0,
			);
		$table = 'advice_reply  r1 LEFT JOIN advice_reply r2 ON r1.id = r2.to_reply_id ';
		$ret = g('ndb') -> select($table,$fields,$cond,$page,$page_size,'group by r1.id ','order by r1.create_time desc',true);
		return $ret;
	}	


	/**
	 * 获取反馈评论的回复信息
	 * @param   $advice_id
	 * @param   $page
	 * @return  array
	 */
	public function get_child_list($advice_id,$reply_id,$page=1,$page_size=20){
		$cond = array(
				'adi_id=' => $advice_id,
				'com_id=' => $this -> com_id,
				'to_reply_id=' => $reply_id,
			);
		$fields = '*';
		$ret = g('ndb') -> select(self::$reply_table,$fields,$cond,$page,$page_size,'','order by to_reply_id asc,create_time desc',true);
		return $ret;
	}	

	/**
	 * 获取指定的评论
	 * @return 
	 */
	public function get_reply($advice_id,$reply_id,$fields='*'){
		$cond = array(
				'adi_id=' => $advice_id,
				'com_id=' => $this -> com_id,
				'id=' => $reply_id,
			);
		$ret = g('ndb') -> select(self::$reply_table,$fields,$cond);
		return !empty($ret) ? $ret[0] : false;
	}

    // ----------------------以下是内部实现-----------------------------------
    
}

/* End of this file */