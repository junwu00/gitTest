<?php
/**
 * 微信access token获取
 * @author yangpz
 * @date 2015-01-05
 *
 */
class cls_atoken {
	/** access token 生存最长时间 */
	private static $MaxTokenAliveTime = 60;

	/**
	 * 根据企业ID，应用所属套餐，获取access token
	 * @param unknown_type $com_id
	 * @param unknown_type $app_combo 若要获取的access token与具体的应用相关，该值不能为空。若与具体应用无关（如员工管理），则可为空
	 * @throws SCException
	 */
	public function get_access_token_by_com($com_id, $app_id=NULL) {
		try {
			$com = g('company') -> get_by_id($com_id);
			if (!$com) {
				throw new SCException('找不到对应的企业信息');
			}
			$token = $this -> get_access_token($app_id, $com['corp_id'], $com['corp_secret'], $com['permanent_code'], $com['permanent_suite_id']);
			return $token;
			
		} catch (SCException $e) {
			throw $e;
		} catch (RedisException $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取access token
	 * @param unknown_type $app_combo 若从/apps目录调用该方法，该值不能为空，需要填写$this -> app_combo，即应用对应的套餐
	 * @throws SCException
	 */
	public function get_access_token($app_id, $corp_id, $corp_secret, $auth_code, $auth_suite) {
		log_write('开始获取access token');
		
		try {
			// if (!is_null($auth_code) && !empty($auth_code)) {
			// 	if (empty($auth_suite) || empty($corp_id)) {
			// 		throw new SCException('请先安装应用后再使用该功能');
			// 	}
				
			// 	$combo_suite_arr = json_decode($auth_suite, TRUE);
			// 	if (!is_array($combo_suite_arr) || count($combo_suite_arr) == 0) {
			// 		throw new SCException('管理员在安装应用时未配置“通讯录管理权限”');
			// 	}
				
			// 	$auth_code_arr = json_decode($auth_code, TRUE);
			// 	if (!is_null($app_combo) && !empty($app_combo)) {
			// 		if (!isset($combo_suite_arr[$app_combo])) {		//指定套餐
			// 			throw new SCException('该应用未授权');
			// 		}
			// 		$suite_id = $combo_suite_arr[$app_combo];
			// 		$auth_code = $auth_code_arr[$app_combo];
					
			// 	} else {											//不指定套餐（通讯录管理时使用）
			// 		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			// 			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			// 		} else {
			// 			$com = g('company') -> get_by_corp_id($corp_id, 'id');
			// 			$com_id = $com['id'];
			// 		}
					
			// 		$auth_apps = g('com_app') -> get_all_auth_app($com_id, 'c.*');
			// 		empty($auth_apps) && $auth_apps = array();
			// 		$sie_ids = array();
			// 		foreach ($auth_apps as $auth_app) {
			// 			$sie_ids[$auth_app['sie_id']] = $auth_app['sie_id'];
			// 		}
			// 		unset($auth_app);
					
			// 		$key_arr = array_keys($combo_suite_arr);
			// 		$key = '';
			// 		foreach ($key_arr as $item) {
			// 			if (in_array($item, $sie_ids)) {
			// 				$key = $item;
			// 				break;
			// 			}
			// 		}
			// 		unset($item);
					
			// 		if (empty($key)) {
			// 			throw new SCException('请先安装应用');
			// 		}
			// 		$suite_id = $combo_suite_arr[$key];
			// 		$auth_code = $auth_code_arr[$key];
			// 	}
			// 	$token = $this -> get_access_token_by_suite($corp_id, $auth_code, $suite_id);
				
			// } else {
			// 	if (empty($corp_secret) || empty($corp_id)) {
			// 		throw new SCException('请先安装应用后再使用该功能。');
			// 	}
				
			// 	$token = $this -> get_access_token_by_secret($corp_id, $corp_secret);
			// }
			
			if(empty($app_id)){
				if (empty($corp_secret) || empty($corp_id)) {
					throw new SCException('请先安装应用后再使用该功能。');
				}
				$token = $this -> get_access_token_by_secret($corp_id, $corp_secret);
			}else{
				$com = g('company') -> get_by_corp_id($corp_id, '*');
				$app_info = g('com_app') -> get_by_app($com['id'], $app_id);
				if(empty($app_info['secret'])){
					$token = $this -> get_access_token_by_secret($corp_id, $corp_secret);
				}
				$app_secret = $app_info['secret'];
				$token = $this -> get_access_token_by_secret($corp_id, $app_secret);
			}
			
			log_write('获取成功，corp='.$corp_id.', token：'.$token);
			return $token;
			
		} catch (SCException $e) {
			throw $e;
		} catch (RedisException $e) {
			throw $e;
		}
	}
	
	//---------------------------------------------内部实现
	
	/**
	 * 根据套件信息获取access token
	 * @param unknown_type $corp_id		企业corp id
	 * @param unknown_type $auth_code	企业永久授权码
	 * @param unknown_type $suite_id	套件ID
	 * @throws SCException
	 */
	private function get_access_token_by_suite($corp_id, $auth_code, $suite_id, $with_redis=TRUE) {
		try{
			$with_redis && $time = g('redis') -> get(md5(REDIS_QY_EW365_COMPANY_ACCESS_TOKEN_TIME.':'.$corp_id.':'.$suite_id));
			(!isset($time) || is_null($time) || empty($time)) && $time = 0;
			$with_redis && $exists = g('redis') -> get(md5(REDIS_QY_EW365_COMPANY_ACCESS_TOKEN.':'.$corp_id.':'.$suite_id));
			
			$is_over_time = (time() - $time >= self::$MaxTokenAliveTime) ? TRUE : FALSE;		//是否过期
			if ($is_over_time || empty($exists)) {
				$suite = g('suite') -> get_by_suite_id($suite_id);
	
				if(!$suite)
					throw new SCException('不存在的套件');
				$suite_token = g('wxqy_suite') -> get_suite_access_token($suite_id, self::$MaxTokenAliveTime);
				log_write('suite_token='.$suite_token);
				
				$token =  g('wxqy_suite') -> update_auth_access_token($suite_id, $suite_token, $corp_id, $auth_code);
				if($token){
					$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_COMPANY_ACCESS_TOKEN.':'.$corp_id.':'.$suite_id), $token, 3600);
					$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_COMPANY_ACCESS_TOKEN_TIME.':'.$corp_id.':'.$suite_id),time(), 3600);
					return $token;
				}
				throw new SCException('获取access token失败');
				
			} else {
				return g('redis') -> get(md5(REDIS_QY_EW365_COMPANY_ACCESS_TOKEN.':'.$corp_id.':'.$suite_id));
			}
			
		} catch(SCException $e) {
			throw $e;
			
		} catch(RedisException $e) {
			if ($with_redis) {
				return $this -> get_access_token_by_suite($corp_id, $auth_code, $suite_id, FALSE);	
			}
			throw $e;
		}
	}
	
	/**
	 * 根据企业id，secret获取access token
	 * @param unknown_type $corpid
	 * @param unknown_type $secret
	 */
	private function get_access_token_by_secret($corpid, $secret, $with_redis=TRUE) {
		try {
			$with_redis && $time = g('redis') -> get(md5(REDIS_QY_EW365_ACCESS_TOKEN_TIME.':'.$corpid.':'.$secret));
			
			(!isset($time) || is_null($time) || empty($time)) && $time = 0;
			$is_over_time = (time() - $time >= self::$MaxTokenAliveTime) ? TRUE : FALSE;		//是否过期
			if ($is_over_time) {
				$new_one = g('wxqy') -> update_access_token($corpid, $secret);
				$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_ACCESS_TOKEN.':'.$corpid.':'.$secret), $new_one['token'], 3600);
				$with_redis && g('redis') -> set(md5(REDIS_QY_EW365_ACCESS_TOKEN_TIME.':'.$corpid.':'.$secret), $new_one['time'], 3600);
				log_write('access token='.$new_one['token']);
				return $new_one['token'];
				
			} else {
				return g('redis') -> get(md5(REDIS_QY_EW365_ACCESS_TOKEN.':'.$corpid.':'.$secret));
			}
			
		} catch(SCException $e) {
			throw $e;
			
		} catch(RedisException $e) {
			if ($with_redis) {
				return $this -> get_access_token_by_secret($corp_id, $secret, FALSE);
			}
			throw $e;
		}
	}
	
}

// end of file
