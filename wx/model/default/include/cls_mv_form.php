<?php

/**
 * 审批流程配置（包括请假、报销等所有流程的配置）
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_mv_form {
	/** 对应的库表名称  */
	private static $Table = 'mv_proc_form_model';
	/** 分类表 */
	private static $TableClassify = 'mv_proc_form_classify';
	/** 流程模板表 */
	private static $TableWorkModel = 'mv_proc_work_model';
	/** 流程节点表 */
	private static $TableWorkNodeModel = 'mv_proc_work_node';
	
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/** 发布版本 */
	private static $StatePublic = 2;
	/** 历史版本 */
	private static $StateHistory = 1;
	
	/**
	 * 根据ID获取审批表单new
	 * @param int $com_id
	 * @param int $id
	 */
	public function get_by_id($id,$com_id , $fields='*') {
		$cond = array(
			'id=' => $id,
			'com_id=' => $com_id,
			'info_state=' =>self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return !$ret||count($ret)==0?FALSE:$ret[0];
	}
	
	/**
	 * 根据历史版本id获取发布版本的表单new
	 * @param int $form_history_id 历史版本id
	 * @param int $com_id 企业id
	 */
	public function get_by_his_id($form_history_id,$com_id,$fields='*'){
		$cond = array(
			'form_history_id=' => $form_history_id,
			'com_id=' => $com_id,
			'version=' => self::$StatePublic,
			'info_state=' =>self::$StateOn
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return !$ret||count($ret)==0?FALSE:$ret[0];
	}
	
	
	/**
	 * 获取有表单的所有分类new
	 */
	public function get_classify($com_id,$user_id){
		$classify_sql ="select classify_id,dominated,classify_name from ".self::$Table." mpfm,".self::$TableClassify." mpfc 
		where mpfm.classify_id = mpfc.id 
		and mpfm.version = ".self::$StatePublic." and mpfm.info_state=".self::$StateOn." and mpfm.com_id=".$com_id.
		" group by classify_id,dominated,classify_name order by dominated";
		
		$classifys = g('db') -> select($classify_sql);
		
		return !$classifys||count($classifys)==0 ? FALSE : $classifys;
	}
	
	/**
	 * 根据分类id获取表单信息new
	 * @param int $com_id
	 * @param int $classify_id
	 */
	public function get_form_by_classify($classify_id,$com_id,$user_id){
		$form_sql ="select * from ".self::$Table."
		where classify_id = ".$classify_id." 
		and version = ".self::$StatePublic." and info_state=".self::$StateOn." and com_id=".$com_id.
		"  order by create_time desc";
		
		$form_list = g('db') -> select($form_sql);
		
		return !$form_list||count($form_list)==0 ? FALSE : $form_list;
	}
	
	
	
	
	/**
	 * 直接获取发布状态表单（请假和报销用）
	 */
	public function get_public_proc($is_other_proc,$com_id){
		$sql ="select mpfm.* from ".self::$Table." mpfm,".self::$TableWorkModel." mpwm 
		where mpwm.form_id = mpfm.id 
		and mpfm.is_other_proc = ".$is_other_proc." and mpfm.com_id = ".$com_id.
		" and mpfm.info_state=".self::$StateOn." and mpfm.version=".self::$StatePublic." order by id desc";
		
		$ret = g('db') -> select_one($sql);
		if (!$ret) {
			$ret = array();
		}
		return $ret;
	}
	
	
	//---------------------------------------内部实现

}

// end of file