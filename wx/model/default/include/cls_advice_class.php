<?php
/**
 * 意见反馈数据模型
 *
 * @author LiangJianMing
 * @create 2015-12-31
 */

class cls_advice_class {
	// 公司id
	private $com_id = 0;
	// 基础查询条件
	private $base_con = array();

	// 反馈信息表
	private static $info_table = 'advice_info';
	// 反馈回复表
	private static $reply_table = 'advice_reply';
	// 反馈分类表
	private static $class_table = 'advice_class';
	// 反馈主题浏览状态表
	private static $state_table = 'advice_visit_state';
	// 反馈动态表
	private static $trend_table = 'advice_trend';
	// 反馈附件表
	private static $file_table = 'advice_file';

	// 用户信息表
	private static $user_table = 'sc_user';
	// 部门信息表
	private static $dept_table = 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;

		$this -> base_con = array(
			'com_id=' => $this -> com_id,
		);
	}


	/**
	 * 根据ID获取反馈分类ID
	 **/
	public function get_class_by_id($id,$fields=""){
		$cond = array(
				'id=' => $id,
				'com_id=' => $this -> com_id,
				'state=' => 1,
			);

		if(empty($fields)){
			$fields = array(
				'id', 'com_id', 'name',
				'dept_ids', 'weight','anonymous',
			);
			$fields = implode(',', $fields);
		}
		$order = ' ORDER BY weight DESC ';

		$ret = g('ndb') -> select(self::$class_table, $fields, $cond);
		return !empty($ret) ? $ret[0] : false;

	}

	/**
	 * 获取反馈分类列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_class_list(array $condition, $page=1, $page_size=20,$order='') {
		$condition['com_id='] = $this -> com_id;
		$condition['state='] = 1;

		if(empty($fields)){
			$fields = array(
				'id', 'com_id', 'name',
				'dept_ids', 'weight', 'staff_anonymous',
				'handler_ids', 'handler_anonymous', 'supervisor_ids',
				'supervisor_anonymous', 'state', 'update_time',
				'create_time','anonymous',
			);
			$fields = implode(',', $fields);
		}

		empty($order) and $order = ' ORDER BY weight ASC ';

		$ret = g('ndb') -> select(self::$class_table, $fields, $condition, $page, $page_size, '', $order, true);

		// $ret['data'] = $this -> complete_class_list($ret['data']);

		return $ret;
	}

	/**
	 * 获取反馈分类详情
	 *
	 * @access public
	 * @param integer $id 反馈分类id
	 * @return array
	 * @throws SCException
	 */
	public function get_class_detail($id) {
		$condition = $this -> base_con;
		$condition['state='] = 1;
		$condition['id='] = $id;

		$fields = array(
			'id', 'com_id', 'name',
			'dept_ids', 'weight', 'staff_anonymous',
			'handler_ids', 'handler_anonymous', 'supervisor_ids',
			'supervisor_anonymous', 'state', 'update_time',
			'create_time',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		!is_array($ret) and $ret = array();

		if (empty($ret)) {
			throw new SCException('查询反馈分类详情失败，该分类可能已被删除！');
		}

		$tmp_data = $this -> complete_class_list(array($ret));
		$ret = array_shift($tmp_data);

		return $ret;
	}
	
    // ----------------------以下是内部实现-----------------------------------
    /**
     * 补全反馈分类的展示信息
     *
     * @access private
     * @param array $data 反馈分类的查询数据结果集
     * @return array
     */
    private function complete_class_list(array $data) {
    	if (empty($data)) {
    		return $data;
    	}

    	$all_dept_ids = array();
    	$all_user_ids = array();

    	foreach ($data as &$val) {
    		$dept_ids = json_decode($val['dept_ids'], true);
    		!is_array($dept_ids) and $dept_ids = array();
    		$all_dept_ids = array_merge($all_dept_ids, $dept_ids);

    		$handler_ids = json_decode($val['handler_ids'], true);
    		!is_array($handler_ids) and $handler_ids = array();
    		$all_user_ids = array_merge($all_user_ids, $handler_ids);

    		$supervisor_ids = json_decode($val['supervisor_ids'], true);
    		!is_array($supervisor_ids) and $supervisor_ids = array();
    		$all_user_ids = array_merge($all_user_ids, $supervisor_ids);

    		$val['dept_ids'] = $dept_ids;
    		$val['handler_ids'] = $handler_ids;
    		$val['supervisor_ids'] = $supervisor_ids;
    	}
    	unset($val);

    	$tmp_dept_ids = array();
    	foreach ($all_dept_ids as $val) {
    		$tmp_int = intval($val);
    		$tmp_int > 0 and $tmp_dept_ids[$val] = 1;
    	}
    	$all_dept_ids = array_keys($tmp_dept_ids);

    	$tmp_user_ids = array();
    	foreach ($all_user_ids as $val) {
    		$tmp_int = intval($val);
    		$tmp_int > 0 and $tmp_user_ids[$val] = 1;
    	}
    	$all_user_ids = array_keys($tmp_user_ids);

    	$dept_map = $this -> get_dept_name($all_dept_ids);
    	$user_map = $this -> get_user_name($all_user_ids);
    	foreach ($data as &$val) {
    		$val['dept_desc'] = '';
    		$val['handler_desc'] = '';
    		$val['supervisor_desc'] = '';

    		foreach ($val['dept_ids'] as $cval) {
    			isset($dept_map[$cval]) and $val['dept_desc'] .= "{$dept_map[$cval]}；";
    		}

    		foreach ($val['handler_ids'] as $cval) {
    			isset($user_map[$cval]) and $val['handler_desc'] .= "{$user_map[$cval]}；";
    		}

    		foreach ($val['supervisor_ids'] as $cval) {
    			isset($user_map[$cval]) and $val['supervisor_desc'] .= "{$user_map[$cval]}；";
    		}

    		$val['dept_desc'] = rtrim($val['dept_desc'], '；');
    		$val['handler_desc'] = rtrim($val['handler_desc'], '；');
    		$val['supervisor_desc'] = rtrim($val['supervisor_desc'], '；');
    	}
    	unset($val);

    	return $data;
    }

    /**
	 * 获取部门id与名称的映射数组
	 *
	 * @access private
	 * @param array $dept_ids 部门id集
	 * @return array
	 */
	private function get_dept_name(array $dept_ids) {
		$ret = array();
		if (empty($dept_ids)) return $ret;

		$dept_ids = implode(',', $dept_ids);
		$condition = array(
			'id IN ' => '(' . $dept_ids . ')',
		);

		$fields = array(
			'id', 'name',
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$dept_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val['name'];
			}
			unset($val);
		}

		return $ret;
	}

	/**
	 * 获取用户id与名称的映射数组
	 *
	 * @access private
	 * @param array $user_ids 用户id集
	 * @return array
	 */
	private function get_user_name(array $user_ids) {
		$ret = array();
		if (empty($user_ids)) return $ret;

		$user_ids = implode(',', $user_ids);
		$condition = array(
			'id IN ' => '(' . $user_ids . ')',
		);

		$fields = array(
			'id', 'name','pic_url',
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val['name'];
			}
			unset($val);
		}

		return $ret;
	}


	/**
	 * 获取用户id与名称的映射数组
	 *
	 * @access private
	 * @param array $user_ids 用户id集
	 * @return array
	 */
	private function get_user_name2(array $user_ids) {
		$ret = array();
		if (empty($user_ids)) return $ret;

		$user_ids = implode(',', $user_ids);
		$condition = array(
			'id IN ' => '(' . $user_ids . ')',
		);

		$fields = array(
			'id', 'name','pic_url',
		);
		$fields = implode(',', $fields);
		$order  = ' ORDER BY id ASC ';

		$result = g('ndb') -> select(self::$user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[$val['id']] = $val;
			}
			unset($val);
		}

		return $ret;
	}
}

/* End of this file */