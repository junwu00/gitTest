<?php

class cls_sc_admin {
	
	private static $Table = 'sc_admin';
	/**
	 * 根据user_id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_ids($cid, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $cid, 'com_id', $fields);
	}
}
