<?php
/**
 * 应用配置
 * @author yangpz
 * @date 2014-11-14
 *
 */
class cls_app {
	/** 对应的库表名称 */
	private static $Table = 'sc_app';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/**
	 * 根据ID获取应用
	 * @param unknown_type $id	应用ID
	 */
	public function get_by_id($id, $fields='*') {
		$cond = array(
			'id=' => $id,
			'state=' => self::$StateOn,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		return $result ? $result[0] : FALSE;
	}
	
	/**
	 * 根据应用名称获取应用信息
	 * @param unknown_type $name
	 * @param unknown_type $fields
	 */
	public function get_by_name($name, $fields='*') {
		$cond = array(
			'name=' => $name,
			'state=' => self::$StateOn,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		return $result ? $result[0] : FALSE;
	}
	
	public function get_all_app($fields='*'){
		$cond = array(
				'state=' => self::$StateOn,
			);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret;
	}
	
	/**
	 * 获取企业指定应用列表
	 * @param unknown_type $ids
	 * @param unknown_type $fields
	 */
	public function get_by_ids($ids, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $ids, 'id');
	}
	
	/**
	 * 根据单个套件号获取套件内的应用信息
	 *
	 * @access public
	 * @param integer $combo_id 套件号
	 * @return array 返回值参考 apps/app.config.php 
	 */
	public function get_by_combo($combo_id) {
		static $ret = NULL;

		if ($ret !== NULL) return $ret;

		$combo_conf = include(SYSTEM_APPS . 'app.config.php');

		$ret = array();
		$combo_data = $combo_conf[$combo_id];
		if (!is_array($combo_data)) {
			return $ret;
		}

		$app_ids = array();
		foreach ($combo_data['data'] as $val) {
			$app_ids[] = $val['id'];
		}
		unset($val);

		//该套件所包含的应用id集合
		$combo_data['combo_inc'] = $app_ids;

		$ret = $combo_data;

		return $ret;
	}

	/**
	 * 根据版本号获取全部套件及应用信息
	 *
	 * @access public
	 * @param integer $version 版本号
	 * @return array 返回值参考 apps/version.config.php 
	 */
	public function get_by_version($version) {
		static $ret = NULL;

		if ($ret !== NULL) return $ret;

		$version_conf = include(SYSTEM_APPS . 'version.config.php');

		$ret = array();
		$version_data = $version_conf[$version];
		if (!is_array($version_data)) {
			log_write("找不到版本对应的应用配置，version={$version}");
			return $ret;
		}

		$version_inc = array();
		foreach ($version_data['combo_list'] as &$val) {
			$app_ids = array();
			foreach ($val['data'] as $cval) {
				$app_ids[] = $cval['id'];
			}
			unset($cval);

			//该套件所包含的应用id集合
			$val['combo_inc'] = $app_ids;
			$version_inc = array_merge($version_inc, $app_ids);
		}
		unset($val);

		//该版本所包含的应用id集合
		$version_data['version_inc'] = $version_inc;

		$ret = $version_data;

		return $ret;
	}
	
	/**
	 * 根据应用id获取所在的套件号
	 *
	 * @access public
	 * @param integer $app_id 应用id
	 * @param integer $version 所在版本
	 * @return mixed 成功返回套件号，否则返回FALSE
	 */
	public function get_combo_by_app($app_id, $version) {
		$ver_data = $this -> get_by_version($version);
		if (empty($ver_data)) {
			log_write("获取版本的应用配置失败，version={$version}");
			return FALSE;
		}

		foreach ($ver_data['combo_list'] as $key => $val) {
			if (in_array($app_id, $val['combo_inc'])) {
				return $key;
			}
		}
		unset($val);

		log_write("获取套件号失败，version={$version}，app_id={$app_id}");
		return FALSE;
	}

	/**
	 * 根据应用id，检测该应用是否包含在某一版本中
	 *
	 * @access public
	 * @param integer $app_id 应用id
	 * @param integer $version 所在版本
	 * @return boolean
	 */
	public function check_in_version($app_id, $version) {
		$ver_data = $this -> get_by_version($version);
		if (empty($ver_data)) {
			log_write("获取版本的应用配置失败，version={$version}");
			return FALSE;
		}

		if (in_array($app_id, $ver_data['version_inc'])) {
			return $key;
		}

		return FALSE;
	}

	/**
	 * 获取应用的我方套件id
	 *
	 * @access public
	 * @param integer $agt_id 挂牌代理商id
	 * @param integer $combo 套件号
	 * @return mixed
	 */
	public function get_sie_id($agt_id=0, $combo) {
		return 0;
		$table = 'sc_suite';

		$sql = <<<EOF
SELECT id FROM {$table} 
WHERE agt_id={$agt_id} AND combo_type={$combo} 
ORDER BY id DESC
LIMIT 1;
EOF;
		$sie_id = g('db') -> select_first_val($sql);
		return $sie_id ? $sie_id : FALSE;
	}
}

// end of file