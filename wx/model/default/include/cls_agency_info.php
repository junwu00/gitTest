<?php
/**
 * 代理商信息表
 * @author yangpz
 * @date 2014-12-19
 */
class cls_agency_info {
	/** 对应的库表名称 */
	private static $Table = 't_agency_info';
	
	/**
	 * 获取技术支持信息
	 * @param unknown_type $agt_id
	 */
	public function get_support_info($agt_id) {
		if ($agt_id == 0) {
			return array('info' => SUPPORT_INFO, 'url' => SUPPORT_URL);
		}
		
		$agt = $this -> get_by_id($agt_id);
		if (!$agt) {
			log_write('找不到代理商信息: '.$agt_id, 'ERROR');
			return array('info' => '', 'url' => 'javascript:void(0);');
		}
		
		$oem_id = $agt['oem_id'];
		if ($oem_id == 0) {
			return array('info' => SUPPORT_INFO, 'url' => SUPPORT_URL);
			
		} 
		
		$agt = $this -> get_by_id($oem_id);
		if (!$agt) {
			log_write('找不到代理商信息: '.$agt_id, 'ERROR');
			return array('info' => '', 'url' => 'javascript:void(0);');
		}
		
		$info = (is_null($agt['oem_support_info']) || empty($agt['oem_support_info'])) ? '' : $agt['oem_support_info'];
		$url = (is_null($agt['oem_support_url']) || empty($agt['oem_support_url'])) ? 'javascript:void(0);' : $agt['oem_support_url'];
		return array('info' => $info, 'url' => $url);
	}
	
	/**
	 * 根据ID获取代理商信息
	 * @param unknown_type $agt_id
	 */
	public function get_by_id($agt_id, $fields='*') {
		$cond = array(
			'agt_id=' => $agt_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
}

// end of file