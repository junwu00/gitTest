<?php
/**
 * 信息发布文章处理类
 * @author yangpz
 * @date 2015-06-17
 */

class cls_news_content {
	
	private static $TABLE = 'news_content';
	
	/** 删除 0 */
	public static $STATE_DEL 	= 0;
	/** 起草 1 */
	public static $STATE_PRE 	= 1;
	/** 未审核 2 */
	public static $STATE_CHECK = 2;
	/** 未发布 3 */
	public static $STATE_OFF 	= 3;
	/** 已发布 4 */
	public static $STATE_ON 	= 4;
	/** 驳回 5 */
	public static $STATE_BACK 	= 5;
	
	/** 默认查找的字段 */
	private static $DEF_FIELD = 'id,source,title,cover_url,read_count,public_time';
	
	/** 当前访问的企业ID */
	private $com_id;
	/** 当前访问的成员ID */
	private $user_id;
	
	public function __construct() {
		isset($_SESSION[SESSION_VISIT_COM_ID]) && $this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		isset($_SESSION[SESSION_VISIT_USER_ID]) && $this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}
	
	/**
	 * 获取发布者列表
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $type_id		分类ID
	 */
	public function list_source($com_id, $type_id) {
		$com_id = intval($com_id);
		$type_id = intval($type_id);
		
		$table = self::$TABLE;
		$state_on = self::$STATE_ON;
		$sql = <<<EOF
		SELECT DISTINCT(source) FROM {$table}
		WHERE com_id={$com_id} AND type_id={$type_id} AND state={$state_on} AND source!=''
EOF;
		$ret = g('db') -> select($sql);
		return $ret ? $ret : array();
	}
	
	/**
	 * 分页查找文章
	 * @param unknown_type $com_id		企业ID
	 * @param array $dept_ids			部门(及其父..级部门)ID列表,验证是否有权限查看
	 * @param unknown_type $user_id		成员ID,验证是否有权限查看
	 * @param unknown_type $type_id		分类ID
	 * @param unknown_type $state		状态
	 * @param unknown_type $title		标题
	 * @param unknown_type $offset		开始分页位置
	 * @param unknown_type $limit		分页大小
	 * @param unknown_type $sort		按哪个字段排序
	 * @param unknown_type $order		升序/降序
	 * @param unknown_type $start		发布开始时间
	 * @param unknown_type $end			发布结束时间
	 * @param unknown_type $source		发布者
	 * @param unknown_type $road		是否已读，默认查找全部
	 * @param unknown_type $fields		查找的字段
	 */
	public function lists($com_id, $user_id, array $dept_ids, $type_id, $title='', $offset=0, $limit=20, $sort='', $order='', $start, $end, $source, $road=NULL, $fields='') {
		empty($fields) && $fields = self::$DEF_FIELD;
		
		$dept_parents = $this -> _get_content_dept($dept_ids);	//直属及所有上级部门ID
		
		$com_id = intval($com_id);
		$user_id = intval($user_id);
		$type_id = intval($type_id);
		$start = intval($start);
		$end = intval($end);
		$source = mysql_escape_string($source);
			
		$title = mysql_escape_string($title);
		$offset = intval($offset);
		$offset = $offset < 0 ? 0 : $offset;
		$limit = intval($limit);
		$limit = $limit < 1 ? 1 : $limit;
		$sort = mysql_escape_string($sort);
		$order = strtoupper($order);
		$order = ($order != "ASC" && $order != "DESC") ? "ASC" : $order;
		
		$scope_sql = " scope_user LIKE '%\"{$user_id}\"%' ";
		
		!empty($dept_parents) && $scope_sql .= ' OR scope REGEXP "(' . implode('|', $dept_parents) . ')" ';
		
		if (!empty($title)) {
			$title_sql = " title LIKE '%{$title}%' ";
		} else {
			$title_sql = ' 1 ';
		}
		
		$date_sql = ' 1 ';
		if ($start > 0) {
			$date_sql .= " AND public_time >= {$start} ";
		}
		if ($end > 0) {
			$date_sql .= " AND public_time <={$end} ";
		}
		
		$source_sql = ' 1 ';
		if (!empty($source)) {
			$source_sql = " source='{$source}' ";
		}
		$state_on = self::$STATE_ON;
	
		$road_sql = '';
		if (!is_null($road) && $road !== '') {
			if ($road == 0) {			//查找未读
				$road_sql = " AND id NOT IN (SELECT news_id FROM news_road WHERE com_id={$com_id} AND info_state=1 AND user_id={$user_id}) ";
			} else if ($road == 1) {	//查找已读
				$road_sql = " AND id IN (SELECT news_id FROM news_road WHERE com_id={$com_id} AND info_state=1 AND user_id={$user_id}) ";
			}
		}
		
		$sort_sql = " ORDER BY public_time DESC ";
		$fields_arr = explode(',', self::$DEF_FIELD);
		if (!empty($sort) && in_array($sort, $fields_arr)) {
			$sort_sql = " ORDER BY {$sort} {$order} ";
		}
		
		//查数据
		$table = self::$TABLE;
		$sql = <<<EOF
		SELECT {$fields} FROM {$table}
		WHERE com_id={$com_id} AND type_id={$type_id} AND state={$state_on} AND {$title_sql} AND ({$scope_sql}) AND {$date_sql} AND {$source_sql} {$road_sql}
		{$sort_sql}
		LIMIT {$offset},{$limit}
EOF;
		$ret = g('db') -> select($sql);
		
		if (!$ret) {
			return array('rows' => array(), 'total' => 0);
		}
		
		//查总数
		$total_sql = <<<EOF
		SELECT count(1) as cnt FROM {$table}
		WHERE com_id={$com_id} AND type_id=({$type_id}) AND state={$state_on} AND {$title_sql} AND ({$scope_sql}) AND {$date_sql} AND {$source_sql} {$road_sql}
EOF;
		$count = g('db') -> select($total_sql);
		return array('rows' => $ret, 'total' => $count[0]['cnt']);
	}
	
	/**
	 * 根据分类、文章ID获取文章信息
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $type_id	分类ID
	 * @param unknown_type $id		文章ID
	 */
	public function get_by_id($com_id, $type_id=NULL, $id=0, $fields='') {
		empty($fields) && $fields = self::$DEF_FIELD;
		
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $id,
			'state=' => self::$STATE_ON
		);
		!is_null($type_id) && $cond['type_id='] = $type_id;
		$ret = g('ndb') -> select(self::$TABLE, $fields, $cond);
		return $ret ? $ret[0] : FALSE;
	}
	
    /**
	 * 更新文章阅读量
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $type_id	分类ID
	 * @param unknown_type $id		文章ID
     * @param $user_type 用户类型  1 内部人员 2学生家长
	 */
	public function update_read_count($com_id, $type_id, $id,$user_id, $user_type=1) {
		$exists = $this -> get_by_id($com_id, $type_id, $id);
		if (!$exists)		throw new SCException('文章不存在');
		
		$cnt = g("news_road")->isexist_user($com_id,$id,$user_id,$user_type);
		
		if($cnt==0){//用户没有阅读过
		g("news_road") -> save($com_id, $user_id, $id,$user_type);
			$cond = array(
				'com_id=' => $com_id,
				'type_id=' => $type_id,
				'id=' => $id,
				'state=' => self::$STATE_ON
			);

			if($user_type==2){
                $data = array(
                    'parent_read_count' => $exists['parent_read_count'] + 1,
                    'update_time' => time()
                );
            }else{
                $data = array(
                    'read_count' => $exists['read_count'] + 1,
                    'update_time' => time()
                );
            }

			$ret = g('ndb') -> update_by_condition(self::$TABLE, $cond, $data);
			if (!$ret) {
				throw new SCException('更新文章阅读量失败');
			}
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

    /**
     * 检测家长权限
     */
	public function check_parent_power($news_id, $fields='title, school_scope_dept, school_scope_parent'){
        if (empty($news_id)) 				throw new SCException('未指定要评论的文章');

        $news = g('news_content') -> get_by_id($this -> com_id, NULL, $news_id, $fields);
        if (empty($news)) 					throw new SCException('文章不存在');

        $scope_parent = json_decode($news['school_scope_parent'], TRUE);
        !is_array($scope_parent) && $scope_parent = array();
        if (in_array($this -> user_id, $scope_parent, FALSE)) {	//有权限
            return $news;
        }

        $scope_dept = json_decode($news['school_scope_dept'], TRUE);
        if (!is_array($scope_dept) || empty($scope_dept)) {
            throw new SCException('无权限访问');
        }

        $parent = g('api_school_user_parent') -> get_by_id($this -> user_id, NULL, 'dept_list');
        if (!$parent) 						throw new SCException('找不到您对应的成员数据');

        $dept_ids = json_decode($parent['dept_list'], TRUE);
        if (!is_array($dept_ids)) 			throw new SCException('无权限访问');

        $comm = array_intersect($dept_ids, $scope_dept);
        if (!empty($comm)) {
            return $news;
        }

        $ids = g('api_school_dept') -> list_all_parent_ids($dept_ids);	//直属及所有上级部门ID

        $ret = array();
        foreach ($ids as $group) {
            foreach ($group as $id) {
                $ret[$id] = $id;
            }unset($id);
        }unset($group);
        $ret = array_values($ret);
        $comm = array_intersect($scope_dept, $ret);

        if (empty($comm)) 					throw new SCException('无权限访问');
        return $news;
    }
	
	/**
	 * 验证当前用户对该文章是否有操作权限 
	 * @param unknown_type $news_id		文章ID
	 */
	public function check_power($news_id, $fields='title, scope, scope_user, scope_group') {
		if (empty($news_id)) 				throw new SCException('未指定要评论的文章');
				
		$news = g('news_content') -> get_by_id($this -> com_id, NULL, $news_id, $fields);
		if (empty($news)) 					throw new SCException('文章不存在');
		
		$scope_user = json_decode($news['scope_user'], TRUE);
		!is_array($scope_user) && $scope_user = array();
		if (in_array($this -> user_id, $scope_user, FALSE)) {	//有权限
			return $news;
		}
		
		// gch add
		$scope_group = json_decode($news['scope_group'], TRUE);
		$cond = array(
				'com_id=' => $this -> com_id,
				'info_state=' => 1,
				'user_list LIKE ' => '%"'.$this -> user_id.'"%',
			);
		$group_id = g('ndb') -> select('sc_group', 'id', $cond);
		$flag = false;
		foreach ($group_id as $value) {
			in_array($value['id'], $scope_group) && $flag = true;
		}
		if ($flag) {
			return $news;
		}
		
		$scope_dept = json_decode($news['scope'], TRUE);
		if (!is_array($scope_dept)) {
			throw new SCException('无权限访问');
		}
	
		$user = g('api_user') -> get_by_id($this -> user_id, NULL, 'dept_list');
		if (!$user) 						throw new SCException('找不到您对应的成员数据');
		
		$dept_ids = json_decode($user['dept_list'], TRUE);
		if (!is_array($dept_ids)) 			throw new SCException('无权限访问');

		$comm = array_intersect($dept_ids, $scope_dept);
		if (!empty($comm)) {
			return $news;
		}
		
		$ids = g('api_dept') -> list_all_parent_ids($dept_ids);	//直属及所有上级部门ID
		$ret = array();
		foreach ($ids as $group) {
			foreach ($group as $id) {
				$ret[$id] = $id;
			}unset($id);
		}unset($group);
		$ret = array_values($ret);
		$comm = array_intersect($scope_dept, $ret);
		
		if (empty($comm)) 					throw new SCException('无权限访问');
		return $news;
	}
	
	/** 获取员工可查看的文章的所有部门ID */
	private function _get_content_dept(array $dept_ids) {
		$ids = g('api_dept') -> list_all_parent_ids($dept_ids);	//直属及所有上级部门ID
		$ret = array();
		foreach ($ids as $group) {
			foreach ($group as $id) {
				$ret[$id] = $id;
			}unset($id);
		}unset($group);
		return array_values($ret);
	}
	
}

//end