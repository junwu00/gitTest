<?php
/**
 * 企业成员
 * 
 * @author yangpz
 * @date 2014-10-22
 *
 */
class cls_user_general {
	
	private static $Table = 'sc_user_general';
	
	/**
	 * 判断是否为企业用户
	 * @param unknown_type $wx_acct	企业号账号
	 * @return 错误码，用户ID
	 */
	public function is_user($wx_acct) {
		$result = $this -> get_qy_acct($wx_acct);
		if ($result[0] != cls_resp::$OK) {
			return $result;
		}
		
		///查找数据库记录
		$cond = array(
			'id=' => $result[1]['id'],
			'acct=' => $result[1]['acct'],
			'state=' => self::$StateSubscribe,
		);
		$is_exists = g('ndb') -> record_exists(self::$Table, $cond);
		if (!$is_exists) {
			return array(cls_resp::$UserNotFound, NULL);
		}
		$data = array(
			'id' => $result[1]['id'],
		);
		return array(cls_resp::$OK, $data);
	}

	/**
	 * 根据id获取
	 * @param unknown_type $ids		integer或array
	 * @param unknown_type $fields	查询哪些字段
	 */
	public function get_by_ids($uid, $fields='*') {
		return g('ndb') -> get_data_by_ids(self::$Table, $uid, 'user_id', $fields);
	}

	/**
	 * 更新
	 * @param unknown_type $condition  array 条件
	 * @param unknown_type $data	更新哪些字段
	 */
	public function update($id, $general_list=array(), $del_list=array()) {
		$general_list = array_values($general_list);
		$del_list = array_values($del_list);
		$condition = array(
			'id=' => $id,
		);
		$data = array(	
			'general_list' => json_encode($general_list),
			'del_list' => json_encode($del_list),
		);
	    $result = g('ndb') -> update_by_condition(self::$Table, $condition, $data);
	    if(!$result) {
		    throw new SCException('更新常用联系人失败');
	    }
	    return $result;
	}
	/**
	 * 插入个人常用联系人
	 * @param unknown_type $dept_id		企业对应的dept_id
	 * @param unknown_type $user_info	用户数据
	 * @return 信息不会或插入失败：FALSE；成功：用户ID
	 */
	public function save($id, $general_list=array(), $del_list=array()) {
		$general_list = array_values($general_list);
		$del_list = array_values($del_list);
		$data = array(
			'user_id' => $id,
			'general_list' => json_encode($general_list),
			'del_list' => json_encode($del_list),
		);
		
	    $result =  g('ndb') -> insert(self::$Table, $data);
	    if(!$result) {
		    throw new SCException('保存常用联系人失败');
	    }
	    return $result;
	}
}

// end of file