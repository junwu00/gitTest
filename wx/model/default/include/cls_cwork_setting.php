<?php
/**
 * 考勤配置
 * @author yangpz
 * @date 2014-11-21
 */
class cls_cwork_setting {
	/** 对应的库表名称 */
	private static $Table = 'cwork_setting';
	
	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/**
	 * 根据企业ID获取考勤配置
	 * @param unknown_type $com_id
	 */
	public function get_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
		);
		$result = g('ndb') -> select(self::$Table, $fields, $cond);
		return $result ? $result[0] : FALSE;
	}
	
}

// end of file