<?php
/**
 * 系统返回信息<br><br>
 * 
 * 系统错误码，所有用到的错误码（除extensions包外）需在此处声明，且系统只能用此处的错误码<br>
 * 一般地，错误码编号自增 1 即可<br>
 * 禁止修改任何已有错误码
 * 
 * @author yangpz
 * @2014-10-20
 *
 */
class cls_resp {
	/** 成功 */
	public static $OK	 				= 0;	
	/** 缺少请求参数 */
	public static $LackReqParam	 		= -99994;	
	/** 二维码过期，请刷新页面 */
	public static $CodeOvertime			= -99995;
	/** 扫码失败 */	
	public static $ScanCodeFail			= -99996;	
	/** 等待扫码 */
	public static $WaitScanCode			= -99997;	
	/** 非法请求 */
	public static $ForbidReq	 		= -99998;	
	/** 未知原因（系统繁忙） */
	public static $Busy					= -99999;	
	
	/** 企业管理员不存在 */
	public static $AdminNotFound	 	= -90001;	
	/** 企业管理员密码错误 */
	public static $AdminPwdErr	 		= -90002;	
	/** 验证码错误 */
	public static $VerifyCodeErr 		= -90003;	
	/** 登录超时 */
	public static $LoginOvertime 		= -90004;	
	/** 企业成员不存在 */
	public static $UserNotFound 		= -90005;	
	/** 非管理员，没有权限 */
	public static $NotAdmin		 		= -90006;	
	/** 没有应用操作权限 */
	public static $HasNoAppPower 		= -90007;	
	/** 文件未找到 或 文件超过10M*/
	public static $FileNotFound 		= -90008;	
	/** 文件大小超限 */
	public static $FileOverSize 		= -90009;	
	/** 文件格式错误 */
	public static $FileTypeErr	 		= -90010;	
	/** 文件内容不符合条件 */
	public static $FileContentErr 		= -90011;	
	/** 找不到指定的素材 */
	public static $ArticleNotFound 		= -90012;	
	
	/** 错误码对应中文解释 */
	public static $map = array(
		0	=> '成功',
		-99994	=> '缺少请求参数',
		-99995	=> '二维码过期，请刷新页面',
		-99996	=> '扫码失败',
		-99997	=> '等待扫码',
		-99998	=> '非法请求',
		-99999	=> '系统繁忙',
		
		-90001	=> '账号不存在',
		-90002	=> '密码错误',
		-90003	=> '验证码错误',
		-90004 	=> '登录超时',
		-90005 	=> '企业成员不存在',
		-90006 	=> '非管理员，没有权限',
		-90007 	=> '没有应用操作权限',
		-90008 	=> '文件未找到',
		-90009 	=> '文件大小超限',
		-90011 	=> '文件内容不符合条件',
		-90012 	=> '找不到指定的素材',
	);
	
	/**
	 * 输出异常到前端
	 * @param unknown_type $e			exception对象
	 * @param unknown_type $data_name	附加信息的Key，字符串
	 * @param unknown_type $data		附加信息的val，数组格式
	 */
	public static function echo_exp($e, $data_name=NULL, $data=NULL) {
		$json = array(
			'errcode' => $e -> getCode(),
			'errmsg' => preg_replace('/[\d]+[,，]/', '', $e -> getMessage()),	//去掉错误码
		);
		!is_array($data) && $data = array();
		if (!is_null($data_name) && !empty($data_name)) {
			$json[$data_name] = $data;
		}
		header('Content-type: application/json');
		echo json_encode($json);
		exit();
	}
	
	/**
	 * 返回操作失败
	 * @param unknown_type $errcode	错误码
	 * @param unknown_type $errmsg	错误描述
	 */
	public static function echo_err($errcode, $errmsg, $data_name=NULL, $data=NULL) {
		$json = array(
			'errcode' => $errcode,
			'errmsg' => $errmsg,
		);
		!is_array($data) && $data = array();
		if (!is_null($data_name) && !empty($data_name)) {
			$json[$data_name] = $data;
		}
		echo json_encode($json);
		exit();
	}
	
	/**
	 * 输入返回信息
	 * @param unknown_type $err_code
	 * @param unknown_type $data_name
	 * @param unknown_type $data
	 */
	public static function echo_resp($err_code, $data_name=NULL, $data=NULL) {
		$json = array(
			'errcode' => $err_code,
			'errmsg' => self::$map[$err_code],
		);
		!is_array($data) && $data = array();
		if (!is_null($data_name) && !empty($data_name)) {
			$json[$data_name] = $data;
		}
		echo json_encode($json);
		exit();
	}
	
	/**
	 * 返回系统繁忙，并终止执行
	 * @param unknown_type $err_code
	 */
	public static function echo_busy($err_code=NULL, $data_name=NULL, $data=NULL) {
		is_null($err_code) && $err_code = self::$Busy;
		self::echo_resp($err_code, $data_name, $data);
	}
	
	/**
	 * 返回系统繁忙，并终止执行
	 * @param unknown_type $err_code
	 */
	public static function echo_ok($err_code=NULL, $data_name=NULL, $data=NULL) {
		is_null($err_code) && $err_code = self::$OK;
		self::echo_resp($err_code, $data_name, $data);
	}
	
	/**
	 * 重定向到错误页面
	 * @param unknown_type $reason_arr
	 */
	public static function show_err_page($reason_arr) {
		if (!is_array($reason_arr)) {
			$reason_arr = array($reason_arr);
		}
		
		g('smarty') -> assign('reason', $reason_arr);
		g('smarty') -> show('error/index.html');
		die();
	}
	
	/**
	 * 显示提示页面
	 */
	public static function show_warn_page($reason_arr, $title=NULL, $extend_html=NULL) {
		if (!is_array($reason_arr)) {
			$reason_arr = array(strval($reason_arr));
		}
		
		g('smarty') -> assign('title', empty($title) ? '提示' : strval($title));
		!empty($extend_html) && g('smarty') -> assign('extend_html', $extend_html);
		g('smarty') -> assign('reason', $reason_arr);
		g('smarty') -> show('warn_index.html');
		die();
	}
	
}

// end of file 