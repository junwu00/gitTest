<?php
/**
 * 部门考勤规则
 * @author yangpz
 * @date 2014-11-22
 */
class cls_cwork_dept_rule {
	/** 对应的库表名称 */
	private static $Table = 'cwork_dept_rule';
	
	/** 禁用 0 */
	private static $StateOff= 0;
	/** 启用 1 */
	private static $StateOn = 1;
	/** 非指定到部门的规则 0 */
	private static $NotDirect = 0;
	/** 指定到部门的规则 1 */
	private static $Direct = 1;
	
	/**
	 * 获取部门考勤规则
	 * @param unknown_type $com_id	所属企业ID
	 * @param unknown_type $root_id	企业根部门ID
	 * @param unknown_type $dept_id	主部门ID
	 * @param unknown_type $dept_tree	部门层级数组
	 */
	public function get_dept_rule($com_id, $root_id, $dept_id, $dept_tree) {
		$com_id = intval($com_id);
		$root_id = intval($root_id);
		$dept_id = intval($dept_id);
		
		$dept_list = array();
		foreach ($dept_tree as $tree) {
			$tree_depts = g('api_dept') -> list_by_ids($tree, 'id, lev');
			$sort_dept = array();
			foreach ($tree_depts as $d) {
				$sort_dept[-1 * $d['lev']] = $d['id'];		//按层级从小到大排列
			}unset($d);
			ksort($sort_dept);
			$sort_dept = array_values($sort_dept);
			
			if (current($sort_dept) == $dept_id) {			//主部门的层级列表
				$dept_list = $sort_dept;
				break;
			}
		}
		unset($tree);
		if (empty($dept_list)) {
			log_write('考勤配置异常: dept_tree='.json_encode($dept_tree));
			throw new SCException('配置异常');
		}
		
		$p_dept_list = implode(',', $dept_list);
		
		$sql = sprintf(" select * from %s where com_id=%s and state=%s and dept_id in(%s)", self::$Table, $com_id, self::$StateOn, $p_dept_list);
		$dept_rule_list = g('db') -> select($sql);
		
		if ($dept_rule_list) {
			foreach ($dept_list as $id) {
				foreach ($dept_rule_list as $dept_rule) {
					if ($id == $dept_rule['dept_id']) {
						$rule = g('cwork_rule') -> get_by_id($com_id, $dept_rule['rule_id']);
						return $rule;
					}
				}
				unset($dept_rule);
			}
			unset($id);
		}
		return FALSE;
	}
	
}

// end of file