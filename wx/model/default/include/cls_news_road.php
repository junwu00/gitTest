<?php
/**
 * 信息发布文章处理类
 * @author yangpz
 * @date 2015-06-17
 */

class cls_news_road {
	
	private static $TABLE = 'news_road';
	
	/** 禁用 0 */
	private static $STATE_OFF 	= 0;
	/** 启用 1 */
	private static $STATE_ON 	= 1;
	
	
	/**
	 * 根据分类、文章ID获取文章信息
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $content_id	文章ID
	 * @param unknown_type $user_id		用户ID
	 */
	public function isexist_user($com_id,$content_id,$user_id,$user_type=1) {
		if (empty($content_id))							throw new SCException('文章不能为空');
		if (empty($user_id))							throw new SCException('id不能为空');
		$info_state = self::$STATE_ON;
		//查数据
		$table = self::$TABLE;
		$sql = <<<EOF
		SELECT count(1) cnt FROM {$table}  
		WHERE info_state = {$info_state} 
		and user_id = {$user_id} and com_id={$com_id}
		and news_id = {$content_id} and user_type={$user_type}
EOF;
		$ret = g('db') -> select_one($sql);
		return $ret['cnt'];
	}
	
	
	/**
	 * 保存阅读信息
	 * @param unknown_type $com_id		企业ID
	 * @param unknown_type $user_id		用户ID
	 * @param unknown_type $id			文章ID
	 */
	public function save($com_id,$user_id, $content_id,$user_type=1 ) {
		$data = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'news_id' => $content_id,
			'road_time' => time(),
            'user_type' => $user_type,
			'info_state' => self::$STATE_ON,
		);
		$ret = g('ndb') -> insert(self::$TABLE, $data);
		if (!$ret) {
			throw new SCException('保存阅读失败');
		}
		return $ret;
	}
	
}

//end