<?php
/**
 * 反馈信息数据模型
 *
 * @author LiangJianMing
 * @create 2015-06-21
 */

class cls_feedback {
	//公司id
	private $com_id = 0;
	//用户id
	private $user_id = 0;

	//反馈信息表
	private $fi_table = 't_feedback_info';
	//反馈回复表
	private $fr_table = 't_feedback_reply';

	//用户信息表
	private $user_table = 'sc_user';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this -> com_id = isset($_SESSION[SESSION_VISIT_COM_ID]) ? $_SESSION[SESSION_VISIT_COM_ID] : 0;
		$this -> user_id = isset($_SESSION[SESSION_VISIT_USER_ID]) ? $_SESSION[SESSION_VISIT_USER_ID] : 0;
	}

	/**
	 * 添加一条反馈信息
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function add_finfo(array $data) {
		$now_time = time();
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data['update_time'] = $now_time;
		$data['create_time'] = $now_time;

		$result = g('ndb') -> insert($this -> fi_table, $data);
		if (!$result) {
			throw new SCException('添加反馈信息失败');
		}

		return TRUE;
	}

	/**
	 * 编辑一条反馈信息
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_finfo(array $condition, array $data) {
		$now_time = time();
		$data['update_time'] = $now_time;

		$condition['com_id='] = $this -> com_id;
		$condition['user_id='] = $this -> user_id;

		$result = g('ndb') -> update_by_condition($this -> fi_table, $condition, $data);
		if (!$result) {
			throw new SCException('更新反馈信息失败');
		}

		return TRUE;
	}

	/**
	 * 删除一条反馈信息
	 *
	 * @access public
	 * @param array condition
	 * @return TRUE
	 * @throws SCException
	 */
	public function delete_finfo(array $condition) {
		$condition['com_id='] = $this -> com_id;
		$condition['user_id='] = $this -> user_id;
		$result = g('ndb') -> delete($this -> fi_table, $condition);
		if (!$result) {
			throw new SCException('删除反馈信息失败');
		}
		g('ndb') -> delete($this -> fr_table, $condition);

		return TRUE;
	}

	/**
	 * 添加一条回复信息
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function add_freply(array $data) {
		$now_time = time();
		$data['com_id'] = $this -> com_id;
		$data['user_id'] = $this -> user_id;
		$data['update_time'] = $now_time;
		$data['create_time'] = $now_time;

		$result = g('ndb') -> insert($this -> fr_table, $data);
		if (!$result) {
			throw new SCException('添加反馈回复失败');
		}

		//有新回复时更新反馈表状态
		$condition = array(
			'fi_id=' => $data['fi_id'],
			'user_id!=' => $this -> user_id,
		);
		$data = array(
			'new_reply' => 1,
		);
		g('ndb') -> update($this -> fi_table, $condition, $data);

		return TRUE;
	}

	/**
	 * 编辑一条回复信息
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param array $data 数据集合
	 * @return TRUE
	 * @throws SCException
	 */
	public function update_freply(array $condition, array $data) {
		$now_time = time();
		$data['update_time'] = $now_time;

		$condition['com_id='] = $this -> com_id;
		$condition['user_id='] = $this -> user_id;

		$result = g('ndb') -> update_by_condition($this -> fr_table, $condition, $data);
		if (!$result) {
			throw new SCException('更新反馈回复失败');
		}

		return TRUE;
	}

	/**
	 * 删除一条反馈回复
	 *
	 * @access public
	 * @param array condition
	 * @return TRUE
	 * @throws SCException
	 */
	public function delete_freply(array $condition) {
		$condition['com_id='] = $this -> com_id;
		$condition['user_id='] = $this -> user_id;
		$result = g('ndb') -> delete($this -> fr_table, $condition);
		if (!$result) {
			throw new SCException('删除反馈回复失败');
		}

		return TRUE;
	}

	/**
	 * 获取反馈信息列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @param boolean $with_replay 是否返回相应的回复信息
	 * @return array
	 */
	public function get_finfo_list(array $condition, $page=1, $page_size=20, $order='', $with_replay=FALSE) {
		$table = $this -> fi_table . ' AS fi, '. $this -> user_table . ' AS user';

		$condition['^fi.user_id='] = 'user.id';
		$condition['fi.user_id='] = $this -> user_id;

		$fields = array(
			'fi.fi_id', 'fi.com_id',
			'fi.title', 'fi.detail', 'fi.img_hashs',
			'fi.update_time', 'fi.create_time',
			'user.id AS user_id', 'user.name AS user_name', 'user.pic_url AS user_img',
		);
		$fields = implode(',', $fields);

		empty($order) and $order = ' ORDER BY fi.fi_id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);

		if ($with_replay) {
			//---begin---返回相应的回复列表
			$tmp_arr = array();
			foreach ($ret['data'] as $val) {
				$tmp_arr[] = $val['fi_id'];
			}
			unset($val);
			$replay = $this -> get_freply_by_ids($tmp_arr);

			if (is_array($replay) and !empty($replay)) {
				foreach ($ret['data'] as &$val) {
					isset($replay[$val['fi_id']]) and $val['replay_list'] = $replay[$val['fi_id']];
				}
				unset($val);
			}
			//---end----返回相应的回复列表
		}

		$ret['data'] = $this -> complete_reply_state($ret['data']);

		return $ret;
	}

	/**
	 * 获取反馈信息详情
	 *
	 * @access public
	 * @param integer $fi_id 反馈信息id
	 * @param boolean $with_replay 是否返回回复信息
	 * @return array
	 */
	public function get_finfo_detail($fi_id, $with_replay=TRUE) {
		$table = $this -> fi_table . ' AS fi, '. $this -> user_table . ' AS user';

		$fields = array(
			'fi.fi_id', 'fi.com_id', 'fi.user_id', 
			'fi.title', 'fi.detail', 'fi.img_hashs', 
			'fi.update_time', 'fi.create_time',
			'user.id AS user_id', 'user.name AS user_name', 'user.pic_url AS user_img',
		);
		$fields = implode(',', $fields);

		$condition = array();
		$condition['fi.fi_id='] = $fi_id;
		$condition['^fi.user_id='] = 'user.id';

		$condition['fi.com_id='] = $this -> com_id;
		$condition['fi.user_id='] = $this -> user_id;

		$ret = g('ndb') -> select($table, $fields, $condition, 1, 1);
		if (!$ret) {
			throw new SCException('找不到该信息，可能是已被删除');
		}
		$ret = $this -> complete_reply_state($ret);
		$ret = $ret[0];

		if ($with_replay) {
			$tmp_arr = $this -> get_freply_by_ids($ret['fi_id']);
			isset($tmp_arr[$ret['fi_id']]) and $ret['replay_list'] = $tmp_arr[$ret['fi_id']];
		}

		$ret['img_hashs'] = json_decode($ret['img_hashs'], TRUE);
		!is_array($ret['img_hashs']) and $ret['img_hashs'] = array();
		if (!empty($ret['img_hashs'])) {
			try {
				$ret['imgs'] = g('media') -> compose_url($ret['img_hashs']);
			}catch(SCException $e) {
				$ret['imgs'] = array();
			}
		}

		return $ret;
	}

	/**
	 * 根据反馈信息id集合，查询对应的回复
	 *
	 * @access public
	 * @param mixed $ids 反馈信息id集合
	 * @return array
	 */
	public function get_freply_by_ids($ids) {
		is_array($ids) and $ids = implode(',', $ids);
		$table = $this -> fr_table . ' AS fr LEFT JOIN ' . $this -> user_table . ' AS user ON fr.user_id=user.id';
		
		$condition = array();
		$condition['fr.com_id='] = $this -> com_id;
		$condition['fr.fi_id IN '] = '(' . $ids . ')';
		$fields = array(
			'fr.id', 'fr.fi_id', 'fr.detail',
			'fr.update_time', 'fr.create_time',
			'user.id AS user_id', 'user.name AS user_name', 'user.pic_url AS user_img',
		);
		$fields = implode(',', $fields);

		$ret = g('ndb') -> select($table, $fields, $condition);
		!is_array($ret) and $ret = array();

		$tmp_ret = array();
		foreach ($ret as $val) {
			!isset($tmp_ret[$val['fi_id']]) and $tmp_ret[$val['fi_id']] = array();
			$tmp_ret[$val['fi_id']][$val['id']] = $val;
		}
		unset($val);

		foreach ($tmp_ret as &$val) {
			ksort($val);
		}
		unset($val);
		$ret = $tmp_ret;

		return $ret;
	}

	/**
	 * 获取反馈回复列表
	 *
	 * @access public
	 * @param array $condition 条件集合
	 * @param integer $page 页码
	 * @param integer $page_size 每页记录数
	 * @param string $order 排序方式
	 * @return array
	 */
	public function get_freply_list(array $condition, $page=1, $page_size=20, $order='') {
		$table = $this -> fr_table . ' AS fr LEFT JOIN ' . $this -> user_table . ' AS user on fr.user_id=user.id';

		$condition['fr.com_id='] = $this -> com_id;

		$fields = array(
			'fr.fi_id', 'fr.com_id', 'fr.user_id',
			'fr.title', 'fr.detail', 'fr.img_hashs',
			'fr.update_time', 'fr.create_time',
			'user.name AS user_name', 'user.pic_url AS user_pic_url',
		);
		$fields = implode(',', $fields);

		empty($order) and $order = ' ORDER BY fr.fi_id DESC ';

		$ret = g('ndb') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);

		return $ret;
	}

	/**
	 * 补全是否已回复状态
	 *
	 * @access public
	 * @param array $data 反馈建议结果集
	 * @return array
	 */
	private function complete_reply_state(array $data) {
		if (empty($data)) return $data;

        $ids = array();
        foreach ($data as $val) {
        	$ids[] = $val['fi_id'];
        }
        unset($val);
        $ids = array_unique($ids);
        $ids = implode(',', $ids);

        $fields = array(
            'fi_id'
        );
        $fields = implode(',', $fields);
        $condition = array(
            'fi_id IN ' => '(' . $ids . ')',
        );

        $result = g('ndb') -> select($this -> fr_table, $fields, $condition);
        !is_array($result) and $result = array();
        $tmp_arr = array();
        foreach ($result as $val) {
        	!isset($tmp_arr[$val['fi_id']]) and $tmp_arr[$val['fi_id']] = 1;
        }
        unset($val);
        $result = $tmp_arr;

        foreach ($data as &$val) {
        	$val['is_reply'] = isset($result[$val['fi_id']]) ? TRUE : FALSE;
        }
        unset($val);

        return $data;
	}
}