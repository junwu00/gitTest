<?php
/**
 * 任务申请信息
 * @author lijuns
 * @date 2014-12-05
 */
class cls_task_apply extends cls_task_base{
	
	
	/**
	 * 插入数据
	 * @throws SCException
	 */
	public function save($apply_info) {
		$is_role = parent::get_user_perform($apply_info['taskid'], $apply_info['applyer_id']);
		if($is_role){
			$data = array(
				'taskid' => $apply_info['taskid'],
				'applyer_id' => $apply_info['applyer_id'],
				'applyer_name' => $apply_info['applyer_name'],
				'apply_state' => $apply_info['apply_state'],
				'apply_time' => time(),
				'apply_content' => isset($apply_info['apply_content'])?$apply_info['apply_content']:"",
				'delayed_time'=>isset($apply_info['delayed_time'])?$apply_info['delayed_time']:"",
				'answer_id' => $apply_info['answer_id'],
				'answer_name' => $apply_info['answer_name'],
				'finish_time'=>isset($apply_info['finish_time'])?$apply_info['finish_time']:""
			);
			$ret = g('ndb') -> insert(parent::$TableApply, $data);
			if (!$ret) {
				throw new SCException('保存申请信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限保存申请信息');
		}
	}
	
	/**
	 * 审批申请信息
	 */
	public function update($apply_info){
		log_write($apply_info['taskid'].'---'.$apply_info['answer_id']);
		$is_role = parent::get_user_assign($apply_info['taskid'], $apply_info['answer_id']);
		if($is_role){
			$data = array(
				'is_agree' => $apply_info['is_agree'],
				'answer_content' => $apply_info['answer_content'],
				'answer_name' => $apply_info['answer_name'],
				'send_back_id' => $apply_info['send_back_id'],
				'send_back_name' => $apply_info['send_back_name'],
				'score' => $apply_info['score'],
				'answer_time' =>time()
			);
			$cond = array(
				'id='=>$apply_info["id"],
			); 
			$ret = g('ndb') -> update_by_condition(parent::$TableApply, $cond, $data);
			if (!$ret) {
				throw new SCException('保存任务附件信息失败');
			}
			return $ret;
		}
		else{
			throw new SCException('没有权限审批该任务');
		}
	}
	
	
	/**
	 * 查询数据列表
	 * @throws SCException
	 */
	public function select_list_taskid($taskid,$userid,$apply_id=0){
		$is_role = parent::get_user_role($taskid, $userid);
		if($is_role){
			$sql = 'SELECT ta.*,su.pic_url pic_url FROM '.parent::$TableApply.' ta LEFT JOIN '.parent::$TableUser.' su on applyer_id = su.id where taskid='.$taskid.(empty($apply_id)?"" :( ' and ta.id = '.$apply_id))." order by id desc";
			$ret = g('db')->select($sql);
			return $ret;
		}
		else{
			throw new SCException('没有权限查看审批信息');
		}
	}
	
	
	/**
	 * 查询数据数量
	 * @throws SCException
	 */
	public function select_count_taskid($taskid,$userid){
		$is_role = parent::get_user_role($taskid, $userid);
		if($is_role){
			$sql = 'SELECT COUNT(1) as count FROM '.parent::$TableApply.' where taskid='.$taskid;
			$count = g('db')->select_one($sql);
			return $count;
		}
		else{
			throw new SCException('没有权限查看审批信息');
		}
	}
	
	/**
	 * 查看审批信息
	 */
	public function get_apply_id($apply_id){
		try{
			$sql = 'SELECT * FROM '.parent::$TableApply.' where id='.intval($apply_id);
			log_write($sql);
			$ret = g('db')->select_one($sql);
			return $ret;
		}catch (SCException $e) {
			throw new SCException('数据异常');
		}
	}
	
}

// end of file