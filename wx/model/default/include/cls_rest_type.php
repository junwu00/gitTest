<?php
/**
 * 请假类型
 * @author yangpz
 * @date 2014-12-05
 */
class cls_rest_type {
	/** 对应库表名称 */
	private static $Table = 'rest_type';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;
	
	/**
	 * 获取企业请假类型
	 * @param unknown_type $com_id
	 * @param unknown_type $fields
	 */
	public function get_by_com($com_id, $fields='*') {
		$cond = array(
			'com_id=' => $com_id,
			'state=' => self::$StateOn,
		);
		return g('ndb') -> select(self::$Table, $fields, $cond, 0, 0, '', ' order by idx asc ');
	}
	
	/**
	 * 获取请假天数上限配置
	 * @param unknown_type $com_id
	 * @param unknown_type $type_id
	 * 
	 */
	public function get_max_day($com_id, $type_id) {
		$fields = '*';
		$cond = array(
			'com_id=' => $com_id,
			'id=' => $type_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if (!$ret) {
			throw new SCException('找不到请假天数上限配置');
		}
		return $ret[0]['max_day'];
	}

	/**
	 * 请假类型是否为年假
	 * @param unknown_type $com_id
	 * @param unknown_type $type_id
	 */
	public function is_year($com_id, $type_id) {
		$fields = 'ori_name';
		$cond = array(
			'id=' => $type_id,
			'com_id=' => $com_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if (!$ret) {
			throw new SCException('找不到请假类型');
		}
		return strcmp($ret[0]['ori_name'], '年假') == 0 ? TRUE : FALSE;
	}
}

// end of file