<?php
/**
 * 弹性工时签退时间记录
 * @author yangpz
 *
 */
class cls_cwork_warn {
	/** 对应的库表名称 */
	private static $Table = 'cwork_warn';
	
	/** 未提醒  0 */
	private static $StateNotWarn = 0;
	/** 已提醒  1 */
	private static $StateWarned = 1;
	
	/**
	 * 保存或更新签退提醒记录
	 * @param unknown_type $com_id		员工所属企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $user_acct	员工账号
	 * @param unknown_type $signin_time	签到时间
	 * @param unknown_type $hours		工作满xx小时
	 * @param unknown_type $min			工作满（xx小时）xx分钟
	 */
	public function save_or_update($com_id, $user_id, $user_acct, $signin_time, $hours, $min) {
		$rec = $this -> get_by_user($user_id);
		$warn_time = strtotime("+{$hours}hour+{$min}min", $signin_time);
		$warn_time = date('H:i', $warn_time);
		try {
			if ($rec) {
				return $this -> update($com_id, $user_id, $user_acct, $warn_time);
			} else {
				return $this -> save($com_id, $user_id, $user_acct, $warn_time);
			}
			
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 获取员工提醒记录
	 * @param unknown_type $user_id
	 * @param unknown_type $fields
	 */
	public function get_by_user($user_id, $fields='*') {
		$cond = array(
			'user_id=' => $user_id,
		);
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		return $ret ? $ret[0] : $ret;
	}
	
	/**
	 * 保存签退提醒记录
	 * @param unknown_type $com_id		员工所属企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $warn_time	提醒时间，如：18:00
	 */
	public function save($com_id, $user_id, $user_acct, $warn_time) {
		$data = array(
			'com_id' => $com_id,
			'user_id' => $user_id,
			'user_acct' => $user_acct,
			'state' => self::$StateNotWarn,
			'warn_time' => $warn_time,
		);
		$ret = g('ndb') -> insert(self::$Table, $data);
		if (!$ret) {
			throw new SCException('保存员工签退提醒记录失败');
		}
		return $ret;
	}
	
	/**
	 * 更新员工签退提醒记录
	 * @param unknown_type $com_id		员工所属企业ID
	 * @param unknown_type $user_id		员工ID
	 * @param unknown_type $warn_time	提醒时间，如：18:00
	 */
	public function update($com_id, $user_id, $user_acct, $warn_time) {
		$cond = array(
			'com_id=' => $com_id,
			'user_id=' => $user_id,
			'user_acct=' => $user_acct,
		);
		$data = array(
			'user_id' => $user_id,
			'state' => self::$StateNotWarn,
			'warn_time' => $warn_time,
		);
		$ret = g('ndb') -> update_by_condition(self::$Table, $cond, $data);
		if (!$ret) {
			throw new SCException('更新员工签退提醒记录失败');
		}
		return $ret;
	}
	
}

// end of file