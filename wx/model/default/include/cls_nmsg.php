<?php
/**
 * 常用便捷的消息类
 *
 * @author LiangJianMing
 * @create 2015-03-22
 */
class cls_nmsg {
	//app的应用配置
	private $app_conf = array();
	//该应用对应的微信应用id
	private $wx_app_id = NULL;
	//公司id
	private $com_id = NULL;
	//公司的access_token
	private $access_token = NULL;
	//企业唯一标识
	private $corp_url = NULL;

	//用户数据表
	public $user_table = 'sc_user';
	//公司信息表
	public $company_table = 'sc_company';
	//公司的应用对应微信应用的映射关系表
	public $app_map_table = 'sc_com_app';
	//我方部门id与微信方部门id的映射关系表
	public $dept_map_table = 'sc_dept_map';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * 使用前必须执行初始化函数
	 *
	 * @access public
	 * @param integer $com_id 公司id，将使用该企业的权限进行消息操作
	 * @param string $app_name 应用名称，仅对该应用进行消息操作
	 * @param string $corp_url 企业唯一标识
	 * @return boolean
	 */
	public function init($com_id = 0, $app_name = '', $corp_url = '') {
		$this -> com_id = $com_id;

		$app_conf_url = SYSTEM_APPS . $app_name . '/config.php';
		file_exists($app_conf_url) && $this -> app_conf = include($app_conf_url);

		$this -> access_token = NULL;
		$this -> wx_app_id = NULL;
		if (empty($corp_url)) {
			try {
				$com_info = $this -> get_com_info();
				$this -> corp_url = $com_info['corp_url'];
			}catch(SCException $e) {}
		}else {
			$this -> corp_url = $corp_url;
		}
	}

	/**
	 * 获取当前应用对应的微信应用id
	 *
	 * @access public
	 * @return integer
	 * @throws SCException
	 */
	public function get_wx_app_id() {
		if (empty($this -> wx_app_id) and (empty($this -> com_id) or empty($this -> app_conf))) {
			throw new SCException('配置项异常！');
		}

		$com_id = $this -> com_id;
		$app_id = $this -> app_conf['id'];

		$key_str = __FUNCTION__ . ':com_id:' . $com_id . ':app_id:' . $app_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			$this -> wx_app_id = $ret;
			return $ret;
		}

		$fields = 'wx_app_id';
		$condition = array(
			'com_id=' => $com_id,
			'app_id=' => $app_id,
		);

		$ret = g('ndb') -> select($this -> app_map_table, $fields, $condition, 1, 1);

		if (empty($ret)) {
			throw new SCException('获取微信应用id失败！');
		}

		$ret = $ret[0]['wx_app_id'];
		g('redis') -> setex($cache_key, $ret, 900);

		$this -> wx_app_id = $ret;

		return $ret;
	}

	/**
	 * 获取该企业对应该应用的access_token
	 *
	 * @access public
	 * @return string
	 * @throws SCException
	 */
	public function get_access_token() {
		if (!empty($this -> access_token)) return $this -> access_token;

		$key_str = __FUNCTION__ . ':com_id:' . $this -> com_id . ':app_conf:' . json_encode($this -> app_conf);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			$this -> access_token = $ret;
			return $ret;
		}

		try{
			$info = $this -> get_com_info();
//			if (!empty($this -> app_conf['combo'])) {
//				$combo = $this -> app_conf['combo'];
//			}else {
//				$combo = g('com_app') -> get_sie_id($this -> app_conf['id'], $this -> com_id);
//			}
			$ret = g('atoken') -> get_access_token($this -> app_conf['id'], $info['corp_id'], $info['corp_secret'], $info['permanent_code'], $info['permanent_suite_id']);
		}catch(SCException $e) {
			throw $e;
		}

		if (empty($ret)) {
			throw new SCException('获取令牌失败！');
		}

		$this -> access_token = $ret;
		g('redis') -> setex($cache_key, $ret, 3600);

		return $ret;
	}

	/**
	 * 获取企业的信息
	 *
	 * @access public
	 * @return array
	 * @throws SCException
	 */
	public function get_com_info() {
		$com_id = $this -> com_id;
		if (empty($com_id)) {
			throw new SCException('获取企业信息失败！');
		}

		$key_str = __FUNCTION__ . ':com_id:' . $com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			$ret = json_decode($ret, TRUE);
			return $ret;
		}

		$fields = array(
			'corp_id', 'corp_secret', 'permanent_code',
			'corp_url', 'permanent_suite_id',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'id=' => $com_id,
		);

		$ret = g('ndb') -> select($this -> company_table, $fields, $condition, 1, 1);
		if (empty($ret)) {
			throw new SCException('获取企业信息失败！');
		}

		$ret = $ret[0];
		g('redis') -> setex($cache_key, json_encode($ret), 1800);

		return $ret;
	}

	/**
	 * 发送图文消息
	 *
	 * @access public
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @param array $art_list 需要发送的图文消息集合
	 * @return boolean
	 */
	public function send_news(array $dept_list=array(), array $user_list=array(), array $art_list=array()) {
		foreach ($art_list as &$val) {
			!empty($val['url']) and $val['url'] .= ('&corpurl=' . $this -> corp_url);
		}
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = !empty($dept_list) ? $this -> get_wx_depts($dept_list) : array();
			$user_list = !empty($user_list) ? $this -> get_wx_accts($user_list) : array();
			g('wxqy') -> send_news($access_token, $user_list, $wx_app_id, $art_list, array(), $dept_list);
		}catch(SCException $e) {
			log_write('发送图文消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送文本消息
	 *
	 * @access public
	 * @param string $content 文本内容
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_text($content, array $dept_list=array(), array $user_list=array()) {
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy') -> send_text($access_token, $user_list, $wx_app_id, $content, array(), $dept_list);
		}catch(SCException $e) {
			log_write('发送文本消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送多种文件
	 *
	 * @access public
	 * @param string $type 微信方资源类型，允许取值：file、image、voice
	 * @param string $media_id 推送到微信方资源id
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_files($type, $media_id, array $dept_list=array(), array $user_list=array()) {
		try {
			$type == 'image' and $type = 'img';
			$func = 'send_' . $type;

			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy') -> $func($access_token, $user_list, $wx_app_id, $media_id, array(), $dept_list);
		}catch(SCException $e) {
			log_write('发送' . $type . '消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 发送视频
	 *
	 * @access public
	 * @param string $media_id 推送到微信方资源id
	 * @param string $title 标题
	 * @param string $desc 简介
	 * @param array $dept_list 需要发送的部门id集合
	 * @param array $user_list 需要发送的用户id集合
	 * @return boolean
	 */
	public function send_video($media_id, $title, $desc, array $dept_list=array(), array $user_list=array()) {
		try {
			$access_token = $this -> get_access_token();
			$wx_app_id = $this -> get_wx_app_id();
			$dept_list = $this -> get_wx_depts($dept_list);
			$user_list = $this -> get_wx_accts($user_list);
			g('wxqy') -> send_video($access_token, $user_list, $wx_app_id, $media_id, $title, $desc, array(), $dept_list);
		}catch(SCException $e) {
			log_write('发送视频消息失败，异常：[' . $e -> getMessage() . ']');
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * 根据部门id集合，获取微信方的部门id集合
	 *
	 * @access public
	 * @param array $dept_list 部门id集合
	 * @return array
	 */
	public function get_wx_depts(array $dept_list) {
		if (empty($dept_list)) return array();

		sort($dept_list);
		$key_str = __FUNCTION__ . ':dept_list:' . json_encode($dept_list);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			return json_decode($ret, TRUE);
		}

		$ids = implode(',', $dept_list);
		$fields = 'wx_id';
		$condition = array(
			'id IN ' => '('.$ids.')',
		);

		$ret = array();
		$result = g('ndb') -> select($this -> dept_map_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[] = $val['wx_id'];
			}
			unset($val);
			return $ret;
		}

		!empty($ret) && g('redis') -> setex($cache_key, json_encode($ret), 1800);
		return $ret;
	}

    /**
     * 根据部门id集合，获取微信方的部门id集合
     *
     * @access public
     * @param array $dept_list 部门id集合
     * @return array
     */
    public function get_school_wx_depts(array $dept_list) {
        if (empty($dept_list)) return array();

        sort($dept_list);
        $key_str = __FUNCTION__ . ':school_dept_list:' . json_encode($dept_list);
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('redis') -> get($cache_key);
        if (!empty($ret)) {
            return json_decode($ret, TRUE);
        }

        $ids = implode(',', $dept_list);
        $fields = 'wx_id';
        $condition = array(
            'id IN ' => '('.$ids.')',
        );

        $ret = array();
        $result = g('ndb') -> select('school_dept', $fields, $condition);
        if ($result) {
            foreach ($result as $val) {
                $ret[] = $val['wx_id'];
            }
            unset($val);
            return $ret;
        }

        !empty($ret) && g('redis') -> setex($cache_key, json_encode($ret), 1800);
        return $ret;
    }

	/**
	 * 根据用户id集合，获取微信方的用户账号集合
	 *
	 * @access public
	 * @param array $user_ids 用户id集合
	 * @return array
	 */
	public function get_wx_accts(array $user_ids) {
		if (empty($user_ids)) return array();
		
		sort($user_ids);
		$key_str = __FUNCTION__ . ':user_ids:' . json_encode($user_ids);
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('redis') -> get($cache_key);
		if (!empty($ret)) {
			return json_decode($ret, TRUE);
		}

		$ids = implode(',', $user_ids);
		$fields = 'acct';
		$condition = array(
			'id IN ' => '('.$ids.')',
		);

		$ret = array();
		$result = g('ndb') -> select($this -> user_table, $fields, $condition);
		if ($result) {
			foreach ($result as $val) {
				$ret[] = $val['acct'];
			}
			unset($val);
			return $ret;
		}
		
		!empty($ret) && g('redis') -> setex($cache_key, json_encode($ret), 1800);
		return $ret;
	}

    /**
     * 根据用户id集合，获取微信方的用户账号集合
     *
     * @access public
     * @param array $user_ids 用户id集合
     * @return array
     */
    public function get_school_wx_parent_userid(array $user_ids) {
        if (empty($user_ids)) return array();

        sort($user_ids);
        $key_str = __FUNCTION__ . ':school_parent_userid:' . json_encode($user_ids);
        $cache_key = $this -> get_cache_key($key_str);
        $ret = g('redis') -> get($cache_key);
        if (!empty($ret)) {
            return json_decode($ret, TRUE);
        }

        $ids = implode(',', $user_ids);
        $fields = 'parent_userid';
        $condition = array(
            'id IN ' => '('.$ids.')',
        );

        $ret = array();
        $result = g('ndb') -> select('school_user_parents', $fields, $condition);
        if ($result) {
            foreach ($result as $val) {
                $ret[] = $val['parent_userid'];
            }
            unset($val);
            return $ret;
        }

        !empty($ret) && g('redis') -> setex($cache_key, json_encode($ret), 1800);
        return $ret;
    }

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符
	 * @return string
	 */
	public function get_cache_key($str = '') {
		$key_str = SYSTEM_COOKIE_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':'.$str;
		$key = md5($key_str);
		return $key;
	}

    //=================================================================================
    /**
     * 发送文本消息
     *
     * @access public
     * @param string $content 文本内容
     * @param array $dept_list 需要发送的部门id集合
     * @param array $user_list 需要发送的用户id集合
     * @return boolean
     */
    public function send_school_text($content, array $user_list=array(), array $dept_list=array()) {
        try {
            $access_token = $this -> get_access_token();
            $wx_app_id = $this -> get_wx_app_id();
            $dept_list = $this -> get_school_wx_depts($dept_list);
            $user_list = $this -> get_school_wx_parent_userid($user_list);
            g('wxqy') -> send_school_text($access_token,  $wx_app_id, $content, $user_list, $dept_list);
        }catch(SCException $e) {
            log_write('发送文本消息失败，异常：[' . $e -> getMessage() . ']');
            return FALSE;
        }

        return TRUE;
    }

    /**
     * 发送多种文件
     *
     * @access public
     * @param string $type 微信方资源类型，允许取值：file、image、voice
     * @param string $media_id 推送到微信方资源id
     * @param array $dept_list 需要发送的部门id集合
     * @param array $user_list 需要发送的用户id集合
     * @return boolean
     */
    public function send_school_files($type, $media_id, array $dept_list=array(), array $user_list=array()) {
        try {
            $type == 'image' and $type = 'img';
            $func = 'send_school_' . $type;

            $access_token = $this -> get_access_token();
            $wx_app_id = $this -> get_wx_app_id();
            $dept_list = $this -> get_school_wx_depts($dept_list);
            $user_list = $this -> get_school_wx_parent_userid($user_list);
            g('wxqy') -> $func($access_token,  $wx_app_id, $media_id, $user_list, $dept_list);
        }catch(SCException $e) {
            log_write('发送' . $type . '消息失败，异常：[' . $e -> getMessage() . ']');
            return FALSE;
        }

        return TRUE;
    }

    /**
     * 发送视频
     *
     * @access public
     * @param string $media_id 推送到微信方资源id
     * @param string $title 标题
     * @param string $desc 简介
     * @param array $dept_list 需要发送的部门id集合
     * @param array $user_list 需要发送的用户id集合
     * @return boolean
     */
    public function send_school_video($media_id, $title, $desc, array $dept_list=array(), array $user_list=array()) {
        try {
            $access_token = $this -> get_access_token();
            $wx_app_id = $this -> get_wx_app_id();
            $dept_list = $this -> get_school_wx_depts($dept_list);
            $user_list = $this -> get_school_wx_parent_userid($user_list);
            g('wxqy') -> send_school_video($access_token,  $wx_app_id, $media_id, $title, $desc, $user_list, $dept_list);
        }catch(SCException $e) {
            log_write('发送视频消息失败，异常：[' . $e -> getMessage() . ']');
            return FALSE;
        }

        return TRUE;
    }


}