<?php
/**
 * 用户标识相关的类
 *
 * @author Liang JianMing
 * @create 2015-04-14
 * @version $Id: cls_user_ident.php 7217 2015-05-27 07:55:03Z liangjm $
 * @lastChangedBy $Author: liangjm $
 * @lastChangedDate $Date: 2015-05-27 15:55:03 +0800 (Wed, 27 May 2015) $
 */
class cls_user_ident {
    /**
     * 获取微信用户的唯一openid
     *
     * @access public
     * @return string
     */
    public function get_open_id() {
        if (isset($_GET['code']) and !empty($_GET['code'])) {
            $param = array(
                'appid' => PUBLIC_APP_ID,
                'secret' => PUBLIC_APP_SECRET,
                'code' => $_GET['code'],
                'grant_type' => 'authorization_code',
            );
            $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?' . http_build_query($param);
            $result = cls_http::quick_get($url);
            if ($result['httpcode'] == 200 and !empty($result['content'])) {
                $data = json_decode($result['content'], TRUE);
                if (is_array($data) and !empty($data['openid'])) return $data['openid'];
            }
            
            return FALSE;
        }

        $this_url = get_this_url();
        $param = array(
            'appid' => PUBLIC_APP_ID,
            'redirect_uri' => $this_url,
            'response_type' => 'code',
            'scope' => 'snsapi_base',
            'state' => '123',
        );

        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?" . http_build_query($param) . "#wechat_redirect";
        header('Location:' . $url);
        exit;
    }
}