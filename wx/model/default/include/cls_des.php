<?php
/** 
 * DES加密类
 * 
 * @author LiangJianMing
 * @create 2014-07-04 
 * @version 1.0
 */
class cls_des{
	//des加密的两组密钥
	public $DES_KEY = array(17, 47, 77, 47, 57, 87, 37, 57);
	public $DES_IV  = array(18, 28, 28, 38, 48, 38, 28, 88);
	//密钥偏移量：0-15，一般更新密钥仅需调整该值
	public $DES_OFFSET = 9;

	//初始化加密密钥和偏移值
	public function __construct(){
		$this -> DES_OFFSET > 15 && $this -> DES_OFFSET = intval($this -> DES_OFFSET % 15);
		foreach ($this -> DES_KEY as &$val) {
			$val = $val + $this -> DES_OFFSET;
		}
		unset($val);
		foreach ($this -> DES_IV as &$val) {
			$val = $val + $this -> DES_OFFSET;
		}
		unset($val);
	}

	/**
	 * 16进制字符串偏移
	 *
	 * @access private
	 * @param string $hex_str 大写16进制字符串
	 * @param integer $offset 偏移量
	 * @param string $type 偏移类型：add-增加偏移，back-恢复偏移
	 * @return boolean
	 */
	private function key_offset($hex_str, $offset = 1, $type = 'add') {
		$offset > 15 && $offset = $offset % 15;
		$count = 0;
		$length = strlen($hex_str);
		while($count < $length) {
			$dec = hexdec($hex_str[$count]);
			if ($type == 'add') {
				($dec += $this -> DES_OFFSET) > 15 && $dec = $dec - 16;
			}else {
				($dec -= $this -> DES_OFFSET) < 0 && $dec = $dec + 16;	
			}
			$hex_str[$count] = strtoupper(dechex($dec));
			$count += 6;
		}
		return $hex_str;
	}

	/** 
	 * 字符串加密
	 * 
	 * @access public
	 * @param string $name 变量名
	 * @param string $str 需要加密的字符串
	 * @return string
	 */
	public function encode($str) {
		$des_string = $this -> encrypt($str, $this -> DES_KEY, $this -> DES_IV);
		$des_string = $this -> key_offset($des_string, $this -> DES_OFFSET, 'add');
		return $des_string;
	}
	
	/** 
	 * 字符串解密
	 * 
	 * @access public
	 * @param string $str 需要解密的字符串
	 * @return string
	 */
	public function decode($str) {
		$str = $this -> key_offset($str, $this -> DES_OFFSET, 'back');
		$ret = g('des') -> decrypt($str, $this -> DES_KEY, $this -> DES_IV);
		return $ret;
	}


	/** 
	 * 加密，返回大写十六进制字符串
	 * 
	 * @access public
	 * @param string $str
	 * @param string $key
	 * @param string $iv
	 * @return string
	 */
	public function encrypt($str, $key, $iv) {
		$key 	= $this -> bytes2str($key);
		$iv 	= $this -> bytes2str($iv);
		$size 	= mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_CBC);
		$str 	= $this->pkcs5_pad($str, $size);
		$str 	= mcrypt_cbc(MCRYPT_DES, $key, $str, MCRYPT_ENCRYPT, $iv);
		return strtoupper(bin2hex($str));
	}

	/** 
	 * 解密
	 * 
	 * @access public
	 * @param string $str
	 * @param string $key
	 * @param string $iv
	 * @return string
	 */
	public function decrypt($str, $key, $iv) {
		$key 	= $this -> bytes2str($key);
		$iv 	= $this -> bytes2str($iv);
		$str_bin = pack("H*", $str);
		$str = mcrypt_cbc(MCRYPT_DES, $key, $str_bin, MCRYPT_DECRYPT, $iv);
		$str = $this->pkcs5_unpad($str);
		return $str;
	}
	
	private function pkcs5_pad($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text.str_repeat(chr($pad), $pad);
	}

	private function pkcs5_unpad($text) {
		$pad = ord($text{strlen($text) - 1});
		if($pad > strlen($text)) return FALSE;
		if(strspn($text, chr($pad), strlen($text) - $pad) != $pad) return FALSE;
		return substr($text, 0, - 1 * $pad);
	}

	/**
	 * bytes2str
	 * @static
	 * @access public
	 * @param array $bytes
	 * @return string
	 */
	public function bytes2str($bytes) {
		$str = '';
		foreach ( $bytes as $ch ) {
			$str .= chr($ch);
		}
		return $str;
	}
}

/* End of file cls_des.php */ 