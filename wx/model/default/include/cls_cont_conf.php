<?php
/**
 * 通讯录配置
 * @author yangpz
 * @date 2014-11-13
 *
 */
class cls_cont_conf {
	/** 对应库表名称 */
	private static $Tabel = 'cont_conf';
	/** 禁用 0*/
	private static $StateOff = 0;
	/** 启用 1*/
	private static $StateOn = 1;
	
	/**
	 * 获取企业通讯录配置
	 * @param unknown_type $dept_id
	 */
	public function get_by_dept($dept_id) {
		$fileds = '*';
		$cond = array(
			'dept_id=' => $dept_id,
			'state=' => self::$StateOn,
		);
		$result = g('ndb') -> select(self::$Tabel, $fileds, $cond);
		return $result ? $result[0] : FALSE;
	}
}

// end of fiel