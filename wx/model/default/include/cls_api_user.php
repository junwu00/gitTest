<?php
/**
 * 企业成员查询类
 * 
 * @author yangpz
 * @date 2015-12-18
 *
 */
class cls_api_user extends abs_api_contact {
	
	private static $Table = 'sc_user';
	/** 已删除 */
	private static $StateDelete = 0;
	/** 已关注 */
	private static $StateSubscribe = 1;
	/** 已冻结 */
	private static $StateFrozen = 2;
	/** 未关注 */
	private static $StateUnsubscribe = 4;
	
	public function __construct($conf=array()) {
		empty($conf) && $conf = array();
		parent::__construct($conf);
	}
	
	/**
	 * 根据员工ID，获取成员信息
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $state		成员状态，为空则查询未删除的成员
	 * @param unknown_type $fields		查找的字段
	 * 
	 * @return	成员信息（数组），找不到则返回FALSE
	 */
	public function get_by_id($id, $state=NULL, $fields='*') {
		$cond = array(
			'root_id=' => $this -> root_id,
			'id=' => $id,
		);
		if (is_null($state)) {
			$cond['state!='] = self::$StateDelete;
		} else if (!empty($state)) {
			$cond['state='] = $state;
		}
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		$ret = $ret ? $ret[0] : FALSE;
		parent::_get_with_struct_filter_info($this -> filter_type_user, $ret);
		return $ret;
	}
	
	/**
	 * 根据账号获取成员信息
	 * @param unknown_type $acct		成员账号
	 * @param unknown_type $state		成员状态，为空则查询未删除的成员
	 * @param unknown_type $fields		查找的字段
	 * 
	 * @return	成员信息（数组），找不到则返回FALSE
	 */
	public function get_by_acct($acct, $state=NULL, $fields='*') {
		$root_id = $this -> root_id;
		$cond = array(
			'root_id=' => $root_id,
			'acct=' => $acct,
		);
		if (is_null($state)) {
			$cond['state!='] = self::$StateDelete;	
			
		} else {
			$cond['state='] = $state;	
		}
		$ret = g('ndb') -> select(self::$Table, $fields, $cond);
		if ($ret && count($ret) > 1) {
			log_write('成员账号重复：root_id='.$root_id.', acct='.$wx_acct, 'ERROR');
		}
		$ret = $ret ? $ret[0] : FALSE;
		parent::_get_with_struct_filter_info($this -> filter_type_user, $ret);
		return $ret;
	}
	
	/**
	 * 获取成员主部门信息/仅ID
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	是否仅获取部门ID，默认为TRUE
	 * 
	 * @return 
	 * $only_dept_id为TRUE时，成员主部门ID，找不到则返回FALSE
	 * $only_dept_id为FALSE时，成员主部门信息，找不到则返回FALSE
	 */
	public function get_main_dept($id, $only_dept_id=TRUE) {
		$dept_list = $this -> list_all_dept($id, TRUE);
		$main_dept_id = empty($dept_list) ? FALSE : array_shift($dept_list);
		parent::_get_with_struct_filter_id($this -> filter_type_dept, $main_dept_id);
		if ($only_dept_id) {
			return $main_dept_id;
		}
		
		if (empty($main_dept_id))		return FALSE;
		
		$api_dept = $this -> _get_api_dept_instance();
		return $api_dept -> get_by_id($main_dept_id);		//前面已进行虚拟架构过滤，此处不再过滤
	}
	
	/**
	 * 根据ID集合获取成员列表
	 * @param array $ids				成员ID数组
	 * @param unknown_type $fields		查找的字段，默认为全部
	 * @param unknown_type $auto_filter	是否自动进行架构过滤，默认为TRUE
	 */
	public function list_by_ids(array $ids, $fields='*', $auto_struct_filter=TRUE) {
		if (empty($ids))		return array();
		
		$fields = mysql_escape_string($fields);
		$ids = intval_array($ids);
		$ids = implode(',', $ids);
		
		$table = self::$Table;
		$state_del = self::$StateDelete;
		$root_id = $this -> root_id;
		$sql = <<<EOF
SELECT {$fields} FROM {$table}
WHERE root_id={$root_id} AND state!={$state_del} AND id IN ({$ids})
EOF;
		$ret = g('db') -> select($sql);
		$ret = $ret ? $ret : array();
		$depts = array();
		$auto_struct_filter && parent::_list_with_struct_filter_info($depts, $ret);
		return $ret;
	}
	
	/**
	 * 获取成员所有部门信息/仅ID
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	是否仅获取部门ID，默认为FALSE
	 * @param unknown_type $auto_filter	是否自动进行架构过滤，默认为TRUE
	 * 
	 * @return 
	 * $only_dept_id为TRUE时，成员所有部门ID，找不到则返回空数组
	 * $only_dept_id为FALSE时，成员所有部门信息，找不到则返回空数组
	 */
	public function list_all_dept($id, $only_dept_id=TRUE, $auto_struct_filter=TRUE) {
		$user = $this -> get_by_id($id, '', 'dept_list');
		if (empty($user))				throw new SCException('成员不存在');
		
		$dept_list = json_decode($user['dept_list'], TRUE);
		!is_array($dept_list) && $dept_list = array();
		$auto_struct_filter && parent::_list_with_struct_filter_id($dept_list);
		if ($only_dept_id) {
			return $dept_list;
		}
		
		if (empty($dept_list))		return array();
		
		$api_dept = $this -> _get_api_dept_instance();
		return $api_dept -> list_by_ids($dept_list, '*', $auto_struct_filter);
	}
	
	/**
	 * 根据ID获取成员的部门及其父..级部门
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_dept_id	仅返回部门ID/返回部门信息
	 */
	public function list_all_parent_dept($id, $only_dept_id=TRUE) {
		$all_dept_ids = $this -> list_all_dept($id, '*', FALSE);				//此处不进行过滤，待下面处理完再过滤
		$api_dept = $this -> _get_api_dept_instance();
		$parent_dept_ids = $api_dept -> list_all_parent_ids($all_dept_ids);		//此处有过滤
		
		if ($only_dept_id)	return $parent_dept_ids;
		
		return $api_dept -> list_by_ids($parent_dept_ids, '*', FALSE);			//上面已过滤，此处不再过滤
	}
	
	/**
	 * 获取成员所有的公共群组
	 * @param unknown_type $id				成员ID
	 * @param unknown_type $only_group_id	仅返回群组ID/返回群组信息，默认为群组信息
	 */
	public function list_group($id, $only_group_id=TRUE) {
		$api_group = $this -> _get_api_group_instance();
		return $api_group -> list_by_user_id($id, $only_group_id);
	}
	
	/**
	 * 根据成员ID，获取直接下属与分管的部门（仅取状态为：启用）
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $only_ids	仅返回成员/部门ID 或 返回成员/部门信息，默认返回ID
	 * @param unknown_type $dept_fields	查找的部门字段
	 * @param unknown_type $user_fields	查找的成员字段
	 */
	public function list_sub_by_id($id, $only_ids=TRUE, $dept_fields='id,name', $user_fields='id,name,pic_url') {
		$api_leader = $this -> _get_api_leader_instance();
		return $api_leader -> list_sub_by_user_id($id, $only_ids, $dept_fields, $user_fields);
	}
	
	/**
	 * 获取指定成员的所有领导
	 * @param unknown_type $id			成员ID
	 * @param unknown_type $only_id		仅返回成员ID/返回成员信息，默认为仅成员ID
	 * @param unknown_type $user_fields	查找的字段（成员）
	 */
	public function list_leader_by_id($id, $only_ids=TRUE, $user_fields='id,name,pic_url') {
		$api_leader = $this -> _get_api_leader_instance();
		return $api_leader -> list_leader_by_user_id($id, $only_ids, $user_fields);
	}
	
	/**
	 * 根据部门ID，获取部门直属成员列表
	 * @param unknown_type $dept_id 	部门ID
	 * @param unknown_type $fields 		查找的字段，默认为：全部字段
	 * @param unknown_type $sort_boss 	是否标识部门负责人并置顶，默认为：TRUE
	 * 
	 * @return 成员信息列表，无记录时返回空数组
	 */
	public function list_direct_by_dept_id($dept_id, $fields='*', $sort_boss=TRUE) {
		$root_id = $this -> root_id;
		$dept_id = intval($dept_id);
		$fields = mysql_escape_string($fields);
		
		$dept = g('dept') -> get_dept_by_id($root_id, $dept_id, 'boss,second_boss');
		if(empty($dept))	return array();

		//部门负责人置顶
		$order = ' state, all_py ';
		if ($sort_boss) {
			$boss_ids = array();
			$second_boss = json_decode($dept['second_boss'], TRUE);
			!is_array($second_boss) && $second_boss = array();
	        !empty($dept['second_boss']) && $boss_ids = array_merge($boss_ids,$second_boss);
			!empty($dept['boss']) && $boss_ids = array_merge($boss_ids, array($dept['boss']));
			!empty($boss_ids) && $order = ' FIELD(id, '.implode(',', $boss_ids).') DESC ,state, all_py ';
		}

		$table = self::$Table;
		$state_del = self::$StateDelete;
		$sql = <<<EOF
		SELECT {$fields} FROM {$table}
		WHERE state!={$state_del} AND root_id={$root_id} AND dept_list LIKE '%%"{$dept_id}"%%' 
		ORDER BY {$order}
EOF;
		$data = g('db') -> select($sql);
		$data = $data ? $data : array();
		
		//添加部门负责人标识
		if ($sort_boss) {
			foreach ($data as &$user) {
				if(!empty($dept['boss']) && $user['id'] == $dept['boss']){
					$user['boss'] = true;
				}
				if(in_array($user['id'], $second_boss)){
					$user['second_boss'] = true;	
				}
			}
		}

		$depts = array();
		parent::_list_with_struct_filter_info($depts, $data);
		return $data;
	}
	
	/**
	 * 分页获取成员信息(含子部门成员)
	 * @param unknown_type $dept_ids	要查找的部门
	 * @param unknown_type $page		页码
	 * @param unknown_type $page_size	分页大小
	 * @param unknown_type $params		要进行匹配搜索的字段信息
	 * @param unknown_type $with_child	是否包含子部门成员，默认为FALSE
	 * @param unknown_type $fields		查找的字段， 默认为全部
	 * @param unknown_type $order		排序
	 */
	public function page_list_by_dept_ids(array $dept_ids, $page=1, $page_size=50, array $params=array(), $fields='*', $order='', $with_count=TRUE) {
		$empty_resp = array(
			'data' => array(),
			'count' => 0,
		);
		
		$has_root = FALSE;	//当前查找的部门是否有根部门（或虚拟根部门），有则需要架构过滤
		foreach ($dept_ids as $id) {
			if ($id == $this -> root_id || $id <= 0) {
				$has_root = TRUE;
				break;
			}
		}unset($id);
		
		if (empty($dept_ids)) {
			return $with_count ? $empty_resp : array();
		}
		
		$cond = array(
			'root_id=' => $this -> root_id,
			'state!=' => self::$StateDelete,
		);

		// gch add contact config
		$hiddenConfig = g('contact_config') -> getHiddenConfig($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
		if (!empty($hiddenConfig['user'])) {
			// $cond['__OR_99'] = array(
			// 	'id NOT REGEXP ' => '(' . implode('|', $hiddenConfig['user']) . ')',
			// );
			$cond['id NOT REGEXP '] = '(' . implode('|', $hiddenConfig['user']) . ')';
		}
		// if (!empty($hiddenConfig['dept'])) {
		// 	// $cond['__OR_99']['dept_tree NOT REGEXP '] = "\"(" . implode('|', $hiddenConfig['dept']) . ")\"";
		// 	$cond['dept_tree NOT REGEXP '] = "\"(" . implode('|', $hiddenConfig['dept']) . ")\"";
		// }
		$watchOther = g('contact_config') -> checkWatchOther($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
		if ($watchOther['flag'] && !empty($watchOther['user'])) {
			$cond['__OR_100'] = array(
				'id REGEXP ' => '(' . implode('|', $watchOther['user']) . ')',
			);
		}
		if ($watchOther['flag'] && !empty($watchOther['dept'])) {
			$cond['__OR_100']['dept_tree REGEXP '] = "\"(" . implode('|', $watchOther['dept']) . ")\"";
		}
		
        //虚拟结构条件
        if ($has_root && $this -> struct_filter) {
	        //查询虚拟结构条件
	        $st_con = g('app_contact') -> get_struct_condition();
	        if (empty($st_con)) {
	            return $empty_resp;
	        }
	        $cond['__OR_123'] = $st_con;
	        
        } else {
			// 使用正则表达式
      		$ret['dept_tree REGEXP '] = '"(' . implode('|', $dept_ids) . ')"';
			$cond['__OR_321'] = $ret;
        }
        
        //通讯录应用过滤
        if ($this -> contact_filter) {
        	$conf = g('app_contact') -> get_contact_setting();
        
	        $search = $conf['search_list'];
	        $secret = $conf['secret_list'];
	
	        $tmp_secret = array();
	        foreach ($secret as $val) {
	            $tmp = intval($val);
	            !empty($tmp) and $tmp_secret[] = $tmp;
	        }
	        unset($tmp);
	        $secret = $tmp_secret;		//保密成员
	        !empty($secret) and $cond['id NOT IN '] = '(' . implode(',', $secret) . ')';
	        
	        if (isset($params['keyword'])) {
	        	$key_word = mysql_escape_string($params['keyword']);
	        	if (!empty($key_word)) {
		            $or_con = array();
		            in_array('name', $search) and $or_con['name LIKE '] = '%' . $key_word . '%';
		            in_array('acct', $search) and $or_con['acct LIKE '] = '%' . $key_word . '%';
		            in_array('position', $search) and $or_con['position LIKE '] = '%' . $key_word . '%';
		            in_array('wx_acct', $search) and $or_con['weixin_id LIKE '] = '%' . $key_word . '%';
		            in_array('phone', $search) and $or_con['mobile LIKE '] = '%' . $key_word . '%';
		            in_array('tel', $search) and $or_con['tel LIKE '] = '%' . $key_word . '%';
		
		            !empty($or_con) and $cond['__OR_1'] = $or_con;
	        	}
	        }
	        
        } else {
        	if (isset($params['keyword'])) {
	        	$key_word = mysql_escape_string($params['keyword']);
	        	if (!empty($key_word)) {
        			$params['__OR_2'] = array(
	        			'name LIKE ' 		=> '%' . $key_word . '%',
	        			'all_py LIKE ' 		=> '%' . $key_word . '%',
	        			'first_py LIKE ' 	=> '%' . $key_word . '%',
        			);
        		}
        	}
        }
        
		$page_size > 500 && $page_size = 500;		//一次最多查询的数据量
		empty($order) && $order = ' all_py ';
		
		if (!empty($params)) {
			foreach ($params as $column => $val) {
				if ($column === 'keyword')	continue;
				
				$cond[$column] = $val;
			}unset($column);unset($val);
		}
		
		$users = g('ndb') -> select(self::$Table, $fields, $cond, $page, $page_size, "", " order by {$order} ");
		$users = $users ? $users : array();
		if (!$with_count) {
			return $users;
		}
		
		$where = g('ndb') -> compose_where($cond);
		$sql = 'SELECT ' . $fields . ' FROM ' . self::$Table . $where;

		$sql = 'SELECT COUNT(*) as count FROM (' . $sql . ') counta';
		$count = g('db') -> select_one($sql);
		$count = $count ? $count['count'] : 0;
		return array(
			'data' => $users,
			'count' => $count,
		);
	}

    /**
     * 获取缓存变量名
     * @param string $str 关键字符串
     * @return string
     */
    protected function _get_cache_key($str) {
    	$filter_str = $this -> struct_filter ? '1' : '0';
        $key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str.':'.$filter_str;
        return md5($key_str);
    }
	
	//内部实现=================================================================================================

	/**
	 * 根据当前信息，获取部门数据库操作类
	 */
	private function _get_api_dept_instance() {
		$conf = $this -> api_conf;
		if ($this -> struct_filter) {	//使用已计算出的架构过滤信息，避免重复计算
			$conf['struct_use_conf'] = TRUE;
			$conf['struct_scope_all'] = $this -> struct_scope_all;
			$conf['struct_scope_read'] = $this -> struct_scope_read;
			$conf['struct_scope_write'] = $this -> struct_scope_write;
		}
		return g('api_dept', $conf);
	}
	
	/**
	 * 根据当前信息，获取公共群组数据库操作类
	 */
	private function _get_api_group_instance() {
		$conf = $this -> api_conf;
		if ($this -> struct_filter) {	//使用已计算出的架构过滤信息，避免重复计算
			$conf['struct_use_conf'] = TRUE;
			$conf['struct_scope_all'] = $this -> struct_scope_all;
			$conf['struct_scope_read'] = $this -> struct_scope_read;
			$conf['struct_scope_write'] = $this -> struct_scope_write;
		}
		return g('api_group', $conf);
	}
	
	/**
	 * 根据当前信息，获取上下级关系数据库操作类
	 */
	private function _get_api_leader_instance() {
		$conf = $this -> api_conf;
		if ($this -> struct_filter) {	//使用已计算出的架构过滤信息，避免重复计算
			$conf['struct_use_conf'] = TRUE;
			$conf['struct_scope_all'] = $this -> struct_scope_all;
			$conf['struct_scope_read'] = $this -> struct_scope_read;
			$conf['struct_scope_write'] = $this -> struct_scope_write;
		}
		return g('api_leader_relationship', $conf);
	}
	
}

//end