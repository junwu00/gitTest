<?php
/**
 * 对内部其他项目提供接口
 */
include_once('global.config.php');

//验证来源IP(仅允许内部IP访问)
$white_ip = array('59.42.237.182', '119.29.33.149', '127.0.0.1');
$ip = get_ip();
if (!in_array($ip, $white_ip))                      cls_resp::echo_err(-1, '非法请求');
//验证来源IP end

//验证sign
$SIGN_KEY = 'wx_easywork365';
$sign = get_var_get('sign');
if (empty($sign))                                   cls_resp::echo_err(-1, '非法请求');
$data = $_POST;
if (empty($data))                                   cls_resp::echo_err(-1, '缺少请求参数');
$sign_ret = cls_sign::check_sign($data, $sign, $SIGN_KEY);
if (!$sign_ret)                                     cls_resp::echo_err(-1, '非法请求');
//验证sign end

$module = get_var_get('m', FALSE);
$action = get_var_get('a', FALSE);
empty($module) && $module = 'index';
empty($action) && $action = 'index';

$mod_cls = 'cls_'.$module;
$cls_module = SYSTEM_ROOT.'api/cls_'.$module.'.php';

if (file_exists($cls_module)) {
    include_once ($cls_module);             //加载对应模块类
    $module = 'cls_'.$module;

    if (class_exists($module) && method_exists($module, $action)) {
        $mod = new $module();
        $mod -> $action();                  //处理业务逻辑
    }
    
} else {
    cls_resp::echo_err(-1, '非法请求');
}

/* End of this file */