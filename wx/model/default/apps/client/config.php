<?php
/**
 * 应用的配置文件_工作日程
 * 
 * @author chenyihao
 * @date 2014-12-03
 * 
 */

return array(
	'id' 				=> 15,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(15),
	'name' => 'client',
	'cn_name' => '客户管理',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Task.png',
	'menu' => array(
		1 => array(
			array('id' => 'bmenu-1', 'name' => '共     享', 'url' => 'javascript:void(0)', 'icon' => ''),
			array('id' => 'bmenu-1', 'name' => '删     除', 'url' => 'javascript:void(0)', 'icon' => ''),
			array('id' => 'bmenu-1', 'name' => '返     回', 'url' => 'javascript:void(0)', 'icon' => ''),
		),
		2 => array(
			array('id' => 'bmenu-1', 'name' => '指     派', 'url' => 'javascript:void(0)', 'icon' => ''),
			array('id' => 'bmenu-1', 'name' => '保     存', 'url' => 'javascript:void(0)', 'icon' => ''),
		),
		3 => array(
			array('id' => 'bmenu-1', 'name' => '保     存', 'url' => 'javascript:void(0);', 'icon' => ''),
		),
		4 => array(
			array('id' => 'bmenu-1', 'name' => '删      除', 'url' => 'javascript:void(0);', 'icon' => ''),
		),
		5 => array(
			array('id' => 'bmenu-1', 'name' => '创建客户', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=add', 'icon' => ''),
		),
	),
);

// end of file