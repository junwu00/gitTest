<?php
/**
 * 微研——我的调研
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_detail extends abs_app_base {
	public function __construct() {
		parent::__construct('survey');
	}
	
	/**
	 * 参与调研页
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$sui_id = (int)get_var_value('id');
		$key_str = get_var_value('key_str');
		$key_str = trim($key_str);

		if (empty($sui_id)) {
			cls_resp::show_err_page('无效的调研对象！');
		}

		if (!empty($key_str)) {
			$key_data = g('des') -> decode($key_str);
			$key_data = json_decode($key_data, TRUE);
		}
		empty($key_data) && $key_data = array();

		if (empty($key_data) && empty($_SESSION[SESSION_VISIT_COM_ID])) {
			cls_resp::show_err_page('链接已失效！');
		}

		try {
			g('survey') -> check_valid($sui_id);
			$info = g('survey') -> get_survey_info($sui_id, TRUE);
			//初始化cookie信息
			g('survey') -> sheet_cookie_init($sui_id);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		//begin---------------分享相关的数据---------------------------
		global $app, $module, $action;

		$share_crop_id = '';
		$share_com_id = 0;
		if (!empty($key_data)) {
			$share_crop_id = $key_data['app_id'];
			$share_com_id = $key_data['com_id'];
		}
		$this -> oauth_jssdk($share_crop_id, $share_com_id);

		$to_share_url = '';
		$share_icon_url = '';
		$share_desc = '请注意，该调研不可分享！';
		$share_title = '请注意，该调研不可分享！';
		
		if ($info['shareable'] == 1) {
			if (empty($key_data)) {
				$key_data = array(
					'app_id' => $_SESSION[SESSION_VISIT_CORP_ID],
					'com_id' => $_SESSION[SESSION_VISIT_COM_ID],
				);
				$key_str = g('des') -> encode(json_encode($key_data));
			}
			$to_share_url = SYSTEM_HTTP_DOMAIN . 'index.php?app=' . 'survey' . '&m=' . $module . '&a=' . $action . '&id=' . $sui_id . '&free=1&key_str=' . $key_str;
			$share_icon_url = SYSTEM_HTTP_APPS_ICON.'Survey.png';
			$share_desc = '期待您的参与！';
			$share_title = '诚挚邀请您参与调研《'. $info['title'] . '》！';
		}
		g('smarty') -> assign('to_share_url', $to_share_url);
		g('smarty') -> assign('share_icon_url', $share_icon_url);
		g('smarty') -> assign('share_desc', $share_desc);
		g('smarty') -> assign('share_title', $share_title);
		//end---------------分享相关的数据-------------------------------------

		//用作提交问卷的接口
		$ajax_submit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.'survey' .'&m=ajax&cmd=101&free=1';
		//用作跳转返回的url
		$back_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.'survey' .'&m=list&a=show';

		g('smarty') -> assign('ajax_submit_url', $ajax_submit_url);
		g('smarty') -> assign('back_url', $back_url);

		$info['type'] = g('survey') -> stype_list[$info['type']];
		g('smarty') -> assign('info', $info);
		g('smarty') -> show($this -> temp_path.'detail/deal.html');
	}
}

// end of file