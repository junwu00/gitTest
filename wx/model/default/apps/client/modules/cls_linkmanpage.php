<?php
/**
 * 客户管理-列表
 * @author 陈chenyihao
 * @date 2015-3-21
 *
 */
class cls_linkmanpage extends abs_app_base {
	public function __construct() {
		parent::__construct('client');
	}
	
	/**
	 * 添加联系人
	 * @access public 
	 * @return void
	 */
	public function add() {
		$client_id = get_var_get('c');
		
		//返回列表URL
		$return_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=lister';
		
		//添加联系人的URL
		$add_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=110';

		if(!g('client')->check_client($client_id))
			cls_resp::show_err_page(array('没有权限'));
		
		$client = g('client') -> get_client($client_id,'client_name');
		
			
		g('smarty') -> assign('add_linkman_url', $add_linkman_url);
		g('smarty') -> assign('return_list_url', $return_list_url);
		
		g('smarty') -> assign('client_id', $client_id);
		g('smarty') -> assign('client_name', $client['client_name']);
		g('smarty') -> assign('APP_MENU', $this -> app_menu[3]);
		g('smarty') -> show($this -> temp_path.'new/new_linkman.html');
	}
	
	/**
	 * 修改联系人
	 * @access public 
	 * @return void
	 */
	public function edit() {
		$changetype = get_var_get("changetype");
		$fieldname = get_var_get("fieldname");
		$cname = get_var_get("cname");
		$isnull = get_var_get("isnull");
		$regex = get_var_get("regex");
		$data = get_var_get("datavalue");
		
		$client_id = get_var_get("cid");
		$linkman_id = get_var_get("lid");
		$refer = get_var_get("refer");
		$id = $client_id;
			
		if(!g('client') -> check_client($client_id))
			cls_resp::show_err_page(array('没有权限'));
		
		$fields = array('name'=>'联系人名称','dept'=>'部门','position'=>'职务','mobile'=>'手机号码','email'=>'Email','expand'=>'扩展字段','telautogram'=>'传真');
		if(!isset($fields[$fieldname]))
			cls_resp::show_err_page(array('请求错误'));
			
		if($fieldname=='expand'){
			$client = g('client') ->get_linkman($client_id,$linkman_id,'linkman_expand');
			$expand = json_decode($client[0]['linkman']['linkman_expand'],true);
			foreach($expand as &$e){
				if($e['name']==$cname){
					$e['edit'] = 1;
				}
			}
			log_write(json_encode($expand));
			g('smarty') -> assign('expand', $expand);
		}
			
		//返回详情页面URL
		if($refer =='linkman')
			$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=detail&c='.$id.'&l='.$linkman_id;
		else
			$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail&id='.$id;
		
		//修改客户AJAX_URL
		$ajax_edit_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=107';
		
		g('smarty') -> assign('return_detail_url', $return_detail_url);
		g('smarty') -> assign('ajax_edit_linkman_url', $ajax_edit_linkman_url);

		g('smarty') -> assign('changetype', $changetype);
		g('smarty') -> assign('fieldname', $fieldname);
		g('smarty') -> assign('datavalue', $data);
		g('smarty') -> assign('list', $list);
		g('smarty') -> assign('client_id', $id);
		g('smarty') -> assign('linkman_id', $linkman_id);
		g('smarty') -> assign('cname', $cname);
		g('smarty') -> assign('isnull', $isnull);
		g('smarty') -> assign('regex', $regex);
		g('smarty') -> show($this -> temp_path.'detail/linkman_fill.html');
	}
	
	/**
	 * 联系人详细页面
	 * @access public 
	 * @return void
	 */
	public function detail() {
		$client_id =get_var_get('c');
		$linkman_id =get_var_get('l');
		
		if(empty($linkman_id) || !intval($linkman_id) || empty($client_id) || !intval($client_id) ){
			cls_resp::show_err_page(array('请求错误'));
		}
		try{
			g('client') -> can_see_client($client_id);
		}catch(SCException $e){
			cls_resp::show_err_page(array('没有权限'));
		}
		
		$client = g('client')->get_client($client_id,'client_name');
		
 		$linkman_fields = "id,linkman_name,linkman_phone,linkman_dept,linkman_position,linkman_email,linkman_telautogram,linkman_expand,is_main";
		$linkman = g('client') -> get_linkman($client_id,$linkman_id,$linkman_fields);

		$linkman[0]['linkman']['linkman_expand'] = json_decode($linkman[0]['linkman']['linkman_expand'],true);
		
		//返回列表URL
		$return_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=lister';
		
		//共享页面URL
		$share_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=share';
		
		//客户修改页面URL
		$edit_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=edit';
		
		//联系人修改页面URL
		$edit_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=edit';
		
		//客户添加扩展字段页面URL
		$add_linkman_expand_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=add_expand';
		
		//删除联系人AJAX_URL
		$remove_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=105';
		
		g('smarty') -> assign('share_client_url',$share_client_url);
		g('smarty') -> assign('return_list_url',$return_list_url);
		g('smarty') -> assign('remove_linkman_url',$remove_linkman_url);
		g('smarty') -> assign('edit_client_url', $edit_client_url);
		g('smarty') -> assign('edit_linkman_url', $edit_linkman_url);
		g('smarty') -> assign('add_linkman_expand_url', $add_linkman_expand_url);

		g('smarty') -> assign('client_name', $client['client_name']);
		g('smarty') -> assign('client_id', $linkman[0]['id']);
		g('smarty') -> assign('linkman',$linkman[0]['linkman']);
		g('smarty') -> show($this -> temp_path.'detail/linkman.html');
	}
	
	/**
	 * 添加联系人扩展字段
	 */
	public function add_expand(){
		$client_id = get_var_get('cid');
		$linkman_id = get_var_get('lid');
		
		if(empty($client_id) || empty($linkman_id))
			cls_resp::show_err_page(array('请求错误'));
			
		if(!g('client') -> check_client($client_id))
			cls_resp::show_err_page(array('没有权限'));
			
		//添加客户扩展字段AJAX_URL
		$add_linkman_expand_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=115';
			
		//返回详情页面URL
		$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=detail&c='.$client_id.'&l='.$linkman_id;
		
		g('smarty') -> assign('return_detail_url', $return_detail_url);
		g('smarty') -> assign('add_linkman_expand_url', $add_linkman_expand_url);
		g('smarty') -> assign('client_id', $client_id);
		g('smarty') -> assign('linkman_id', $linkman_id);
		
		g('smarty') -> show($this -> temp_path.'detail/linkman_other.html');
	}
}

// end of file