<?php
/**
 * 客户管理
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_new extends abs_app_base {
	public function __construct() {
		parent::__construct('client');
	}
	
	/**
	 * 联系人详细信息
	 * 
	 */
	public function new_linkman(){
		g('smarty') -> assign('APP_MENU', $this -> app_menu[2]);
		g('smarty') -> show($this -> temp_path.'new/new_linkman.html');
	}
	
	
	/**
	 * 客户资料详细信息
	 */
	
	public function new_client(){
		g('smarty') -> assign('APP_MENU', $this -> app_menu[2]);
		g('smarty') -> show($this -> temp_path.'new/new_client.html');
	}
	
}

// end of file