<?php
/**
 * 客户管理-列表
 * @author 陈chenyihao
 * @date 2015-3-21
 *
 */
class cls_clientpage extends abs_app_base {
	public function __construct() {
		parent::__construct('client');
	}
	
	/**
	 * 客户列表
	 * @access public 
	 * @return void
	 */
	public function lister() {
		//获取列表的URL
		$ajax_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=101';
		
		//查看联系人页面
		$detail_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=detail';
		//进入客户详情页面
		$detail_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail';
		
		//添加联系人页面
		$add_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=add';
		//创建客户页面
		$add_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=add';
		
		g('smarty') -> assign('APP_MENU', $this -> app_menu[5]);

		g('smarty') -> assign('ajax_list_url', $ajax_list_url);
		g('smarty') -> assign('detail_linkman_url',$detail_linkman_url);
		g('smarty') -> assign('add_linkman_url', $add_linkman_url);
		g('smarty') -> assign('detail_client_url', $detail_client_url);
		g('smarty') -> assign('add_client_url', $add_client_url);

		g('smarty') -> show($this -> temp_path.'lists/list.html');
	}

	
	/**
	 * 添加客户
	 * @access public 
	 * @return void 
	 */
	public function add() {
		
		//添加客户AJAX_URL
		$ajax_add_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=102';
		
		//返回列表URL
		$back_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=lister';
		
		$business = g('client')->get_business_type_list();
		$classify = g('client')->get_classify_type_list();
		
		g('smarty') -> assign('APP_MENU', $this -> app_menu[3]);
		g('smarty') -> assign('ajax_add_client_url', $ajax_add_client_url);
		g('smarty') -> assign('back_list_url', $back_list_url);
		
		g('smarty') -> assign('business', $business);
		g('smarty') -> assign('classify', $classify);
		g('smarty') -> show($this -> temp_path.'new/new_client.html');
	}
	
	/**
	 * 修改客户
	 * @access public 
	 * @return void
	 */
	public function edit() {
		
		$changetype = get_var_get("changetype");
		$fieldname = get_var_get("fieldname");
		$cname = get_var_get("cname");
		$isnull = get_var_get("isnull");
		$regex = get_var_get("regex");
		$data = get_var_get("datavalue");
		
		$client_id = get_var_value("cid");
		
		if(!g('client') -> check_client($client_id))
			cls_resp::show_err_page(array('没有权限'));
		
		$fields = array('name'=>'客户名称','classify'=>'客户类型','business'=>'行业类型','number'=>'公司号码','address'=>'客户地址','postalcode'=>'邮件编码','email'=>'Email','expand'=>'扩展字段','telautogram'=>'传真');
		if(isset($fields[$fieldname]));
			//$cname = $fields[$fieldname];
		else
			cls_resp::show_err_page(array('请求错误'));
			
		if($fieldname=='classify')
			$list = g('client') ->get_classify_type_list('id,client_name');
			
		if($fieldname=='business')
			$list = g('client') ->get_business_type_list('id,client_name');
			
		if($fieldname=='expand'){
			$client = g('client') ->get_client($client_id,'client_expand');
			$expand = $client['client_expand'];
			foreach($expand as &$e){
				if($e['name']==$cname){
					$e['edit'] = 1;
				}
			}
			g('smarty') -> assign('expand', $expand);
		}
			
		//返回详情页面URL
		$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail&id='.$client_id;
		
		//修改客户AJAX_URL
		$ajax_edit_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=104';
		
		g('smarty') -> assign('return_detail_url', $return_detail_url);
		g('smarty') -> assign('ajax_edit_client_url', $ajax_edit_client_url);

		g('smarty') -> assign('changetype', $changetype);
		g('smarty') -> assign('fieldname', $fieldname);
		g('smarty') -> assign('datavalue', $data);
		g('smarty') -> assign('list', $list);
		g('smarty') -> assign('client_id', $client_id);
		g('smarty') -> assign('cname', $cname);
		g('smarty') -> assign('isnull', $isnull);
		g('smarty') -> assign('regex', $regex);
		g('smarty') -> show($this -> temp_path.'detail/client_fill.html');
	}
	
	/**
	 * 客户详细页面
	 * @access public 
	 * @return void
	 */
	public function detail() {
		
		$id =get_var_get('id');
		
		if(empty($id) || !intval($id)){
			cls_resp::show_err_page(array('请求错误'));
		}
		try{
			$curr = g('client') -> can_see_client($id);
		}catch(SCException $e){
			cls_resp::show_err_page(array('没有权限'));
		}
		
		$client_fields = "id,client_name,client_business,client_business_name,client_classify,client_classify_name,client_number,client_postalcode,client_address,client_email,share_state,client_telautogram,client_expand,client_importance";
		$linkman_fields = "id,linkman_name,linkman_phone,linkman_dept,linkman_position,linkman_email,linkman_telautogram,linkman_expand,is_main";
 		$client = g('client')->get_client($id,$client_fields,true,$linkman_fields);
 		foreach ($client['linkman'] as &$l){
 			$l['linkman_expand'] = json_decode($l['linkman_expand'],true);
 		}
 		
 		//返回列表页面URL
		$return_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=lister';
 		
		//共享页面URL
		$share_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=share';
		
		//客户修改页面URL
		$edit_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=edit';
		
		//联系人修改页面URL
		$edit_linkman_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=linkmanpage&a=edit';
		
		//客户添加扩展字段页面URL
		$add_client_expand_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=add_expand';
		
		//客户指派页面URL
		$assign_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=assign';
		
		//删除客户AJAX_URL
		$remove_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=106';
		
		//修改客户AJAX_URL
		$edit_client_ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=104';
		
		//共享客户AJAX_URL
		$ajax_share_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=109';
		
		//保存客户AJAX_URL
		$save_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=116';
		
		g('smarty') -> assign('assign_client_url',$assign_client_url);
		g('smarty') -> assign('save_client_url',$save_client_url);
		g('smarty') -> assign('ajax_share_client_url',$ajax_share_client_url);
		g('smarty') -> assign('return_list_url',$return_list_url);
		g('smarty') -> assign('share_client_url',$share_client_url);
		g('smarty') -> assign('edit_client_ajax_url', $edit_client_ajax_url);
		g('smarty') -> assign('edit_client_url', $edit_client_url);
		g('smarty') -> assign('edit_linkman_url', $edit_linkman_url);
		g('smarty') -> assign('remove_client_url', $remove_client_url);
		g('smarty') -> assign('add_client_expand_url', $add_client_expand_url);

		g('smarty') -> assign('client', $client);
		g('smarty') -> assign('curr', $curr);
		g('smarty') -> assign('APP_MENU', $this -> app_menu[$curr]);
		
		
		g('smarty') -> show($this -> temp_path.'detail/client_info.html');
	}
	
	/**
	 * 客户分享页面
	 */
	public function share(){
		
		$client_id = get_var_get('cid');
		if(!g('client') ->check_own_client($client_id))
				cls_resp::show_err_page(array('没有权限'));
			
		//分享客户AJAX_URL
		$ajax_share_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=109';
		
		//返回详情页面URL
		$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail&id='.$client_id;
		
		g('smarty') -> assign('detail_client', $return_detail_url);
		g('smarty') -> assign('share_client_url', $ajax_share_client_url);
		
		//获取下属员工
		$user = g('client')->get_subordinate('id,name');
		g('smarty') -> assign('user', $user);
		g('smarty') -> assign('client_id', $client_id);
		g('smarty') -> show($this -> temp_path.'detail/client_share.html');
	}
	
	
	/**
	 * 添加客户扩展字段
	 */
	public function add_expand(){
		$client_id = get_var_get('cid');
		if(empty($client_id))
			cls_resp::show_err_page(array('请求错误'));
			
		if(!g('client') -> check_client($client_id))
			cls_resp::show_err_page(array('没有权限'));
			
		//添加客户扩展字段AJAX_URL
		$add_client_expand_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=114';
			
		//返回详情页面URL
		$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail&id='.$client_id;
		
		g('smarty') -> assign('return_detail_url', $return_detail_url);
		g('smarty') -> assign('add_client_expand_url', $add_client_expand_url);
		g('smarty') -> assign('client_id', $client_id);
		
		g('smarty') -> show($this -> temp_path.'detail/client_other.html');
	}
	
	/**
	 * 客户指派页面
	 */
	public function assign(){
		$client_id = get_var_get('c');
		
		if(empty($client_id))
			cls_resp::show_err_page(array('请求错误'));
			
		//指派客户AJAX-URL
		$assign_client_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=ajax&cmd=113';
			
		//返回详情页面URL
		$return_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=client&m=clientpage&a=detail&id='.$client_id;
		
		$user = g('client')->get_subordinate('id,name');
		g('smarty') -> assign('user', $user);
		g('smarty') -> assign('client_id', $client_id);
		g('smarty') -> assign('assign_client_url',$assign_client_url);
		g('smarty') -> assign('return_client_url',$return_detail_url);
		
		g('smarty') -> show($this -> temp_path.'detail/client_appoint.html');
	}
}

// end of file