<?php
/**
 * 客户管理--列表
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_list extends abs_app_base {
	public function __construct() {
		parent::__construct('client');
	}
	
	/**
	 * 展示我的客户列表
	 *
	 * @access public 
	 * @return void
	 */
	public function show_list() {
		global $app, $module, $action;
		/*
		$ajax_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=100';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=detail';

		g('smarty') -> assign('ajax_list_url', $ajax_list_url);
		g('smarty') -> assign('detail_url', $detail_url);
		//*/

		g('smarty') -> show($this -> temp_path.'lists/index.html');
	}
}

// end of file