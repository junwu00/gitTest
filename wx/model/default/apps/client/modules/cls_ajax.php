<?php
/**
 * 全部调研ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2015-03-04
 */
class cls_ajax extends abs_app_base {
	public function __construct() {
		parent::__construct('client');
	}

	//客户可修改字段
	private $client_edit_item = array(
					'name'=>'客户名称',
					'classify'=>'客户类型',
					'business'=>'行业类型',
					'number'=>'公司号码',
					'importance'=>'重要程度',
					'address'=>'客户地址',
					'postalcode'=>'邮件编码',
					'email'=>'Email',
					'expand'=>'扩展字段',
					'telautogram'=>'传真'
	);
	
	//联系人表可修改字段
	private $linkman_edit_item = array(
					'name'=>'联系人名称',
					'dept' => '部门',
					'position'=>'职务',
					'mobile'=>'公司号码',
					'email'=>'Email',
					'expand'=>'扩展字段',
					'telautogram'=>'传真'
	);
	
	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				//获取客户列表
				$this -> get_client_list();
			}
			case 102 :{
				//新增客户
				$this -> add_client();
			}
			case 103 : {
				//获取客户详细信息
				$this -> get_client_detail();
			}
			case 104 : {
				//更新客户信息
				$this -> edit_client();
			}
			case 105 : {
				//删除联系人
				$this -> delete_linkman();
			}
			case 106 : {
				//删除 客户
				$this -> delete_client();
			}
			case 107 : {
				//修改客户联系人信息
				$this -> edit_linkman();
			}
			case 108 : {
				//获取联系人信息
				$this -> get_linkman();
			}
			case 109 : {
				//客户共享
				$this -> share_client();
			}
			case 110 : {
				//添加联系人
				$this -> add_linkman();
			}
			case 111 : {
				//将联系人保存到手机
				$this -> save_linkman_to_phone();
			}
			case 112 : {
				//获取下属
				$this -> get_subordinate();
			}
			case 113 : {
				//指派
				$this -> assign();
			}
			case 114 : {
				//添加客户扩展字段
				$this -> add_client_expand();
			}
			case 115 : {
				//添加联系人扩展字段
				$this -> add_linkman_expand();
			}
			case 116 : {
				//保存共享/下属客户
				$this -> save_client();
			}
			default : {
				//非法的请求
				cls_resp::echo_resp(cls_resp::$ForbidReq);
				BREAK;
			}
		}
	}
	
	/**
	 * 保存共享/下属客户
	 */
	function save_client(){
		try{
			$fields = array(
				'cid'
			);
			$data = parent::get_post_data($fields);
			 
			if(empty($data['cid']))
				throw new SCException('请求错误');

			$curr = g('client')->can_see_client($data['cid']);
			if($curr==1)
				throw new SCException('已经保存');
				
			g('client')->add_client_user($data['cid']);
			g('client') -> log_client($data['cid'],'保存客户');
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 添加联系人扩展字段
	 */
	private function add_linkman_expand(){
		try{
			$fields = array('cid','lid','name','value');
			$data = parent::get_post_data($fields);
			$client_id = $data['cid'];
			if(empty($data['cid']) ||empty($data['lid']) || empty($data['name']) || empty($data['value']))
				throw new SCException('请求错误');
			
			if(!g('client') -> check_client($data['cid']))
				throw new SCException('没有权限');
	

			$client = g('client') -> get_linkman($data['cid'],$data['lid'],'linkman_expand');
			$linkman = $client[0]['linkman'];
			$linkman['linkman_expand'] = json_decode($linkman['linkman_expand'],true);
			$linkman['linkman_expand'][] = array('name'=>$data['name'],'value'=> $data['value']);	
			$linkman['update_time'] = time();
			$linkman['linkman_expand'] = json_encode($linkman['linkman_expand']);
			g('client') -> edit_linkman($client_id,$data['lid'],$linkman);
			
			g('client') -> log_linkman($data['lid'],'修改联系人');
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 添加客户扩展字段
	 */
	private function add_client_expand(){
		try{
			$fields = array('cid','name','value');
			$data = parent::get_post_data($fields);
			
			if(empty($data['cid']) || empty($data['name']) || empty($data['value']))
				throw new SCException('请求错误');
			
			if(!g('client') -> check_client($data['cid']))
				throw new SCException('没有权限');
	

			$client = g('client') -> get_client($data['cid'],'client_expand');
			
			$client['client_expand'][] = array('name'=>$data['name'],'value'=> $data['value']);	
			$client['update_time'] = time();
			$client['client_expand'] = json_encode($client['client_expand']);
			
			
			g('client') -> edit_client($data['cid'],$client);
			g('client') -> log_client($data['cid'],'修改客户');
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 指派
	 */
	private function assign(){
		try{
			$fields = array(
				'cid','uid'
			);
			$data = parent::get_post_data($fields);

//			$data = array(
//				'cid' => 6,
//				'uid' => 488
//			);
			if(empty($data['cid']) || empty($data['uid']))
				throw new SCException('请求错误');
			
			if(!g('client')-> can_assgin($data['cid']))
				throw new SCException('没有权限');
			
			if(!g('client') -> is_subordinate($data['uid']))
				throw new SCException('只能指派给下属员工');
			
			g('client') -> assign($data['cid'],$data['uid']);
			g('client') -> log_client($data['cid'],'将客户'.$data['cid'].'指派给'.$data['uid']);
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取下属员工信息
	 */
	private  function get_subordinate(){
		try{
			$fields = 'id,name';
			$user = g('client') -> get_subordinate($fields);
			cls_resp::echo_ok(cls_resp::$OK,'info',$user);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 将联系人保存到手机
	 */
	private function save_linkman_to_phone(){
		
		try{
		
//			$fields = array(
//				'id','cid'
//			);
//			$data = parent::get_post_data($fields);
			
			$data=  array(
				'id' => 91,
				'cid' => 6
			);
			
			g('client') ->can_see_client($data['cid']);
				
			$linkman = g('client')->get_linkman($data['cid'],$data['id'],'linkman_name,linkman_phone,linkman_email');;
			$linkman = $linkman['linkman'];
			$card = <<<EOF
BEGIN:VCARD
VERSION:2.1
N;LANGUAGE=zh-cn;CHARSET=UTF-8:;{$linkman['linkman_name']}
FN;CHARSET=UTF-8:{$linkman['linkman__name']}
TEL;WORK;VOICE:{$linkman['mobile']}
TEL;CELL;VOICE:{$linkman['mobile']}
EMAIL;PREF;INTERNET:{$linkman['email']}
END:VCARD
EOF;
			header('Content-Type: text/x-vcard; charset=utf-8');
			header('Content-Disposition: attachment; filename='.time().'.vcf;');

			die($card);
		}catch(SCException $e){
			cls_resp::show_err_page(array('没有权限'));
		}
	}
	
	/**
	 * 添加联系人
	 */
	private function  add_linkman(){
		try{
			$fields = array(
				'cid','name','mobile','dept'
			);
			$data = parent::get_post_data($fields);
			
//			$data=array(							//带*号为必填字段，其他选填
//					'cid' => 6,					//*
//					'name' => '联系人000',			//*
//					'mobile' => '手机号码',		//*
//					'dept' => '部门1',			//*
//					'position' => '职位',
//					'email' => '邮箱',
//					'telautogram' => '传真',
//					'main' => 1,				//是否为主要联系人，默认为不是
//			);
			
			if(!g('client') -> check_client($data['cid'])){
				throw new SCException('没有权限');
			}
			
			if(!(isset($data['cid']) && !empty($data['cid'])))
				throw new SCException('参数不正确');
				
			if(!isset($data['name']) || empty($data['name']))
				throw new SCException('名称不能为空');
			
			if(!isset($data['mobile']) || empty($data['mobile']))
				throw new SCException('手机号码不能为空');
			
			if(!isset($data['dept']) || empty($data['dept']))
				throw new SCException('部门不能为空');
				
			if(isset($data['expand']) && !empty($data['expand']) && is_array($data['expand']))
				$expand = json_encode($data['expand']);
			else
				$expand = '[]';
			
			$info = array(
				'linkman_name' => $data['name'],
				'linkman_phone' => $data['mobile'],
				'linkman_dept' => $data['dept'],
				'linkman_position' => isset($data['position']) ? $data['position']:"",
				'linkman_eamil' => isset($data['email']) ? $data['email']:"",
				'linkman_telautogram' => isset($data['telautogram']) ? $data['telautogram']:"",
				'linkman_expand' => $expand,
				'is_main' => isset($data['main']) ? $data['main']:0,
				'create_time' => time(),
				'update_time' => time(),
				'info_state' => 1,
				'client_id' => $data['cid']
			);
			
			g('client') -> add_linkman($data['cid'],array($info));
			g('client') -> log_client($data['cid'],'添加联系人');
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 客户共享/取消共享    3.不共享，1.指定人员共享，2.部门共享	//0在经过empty()之后为false
	 */
	private function share_client(){
		try{
			$fields = array(
				'cid','type'
			);
			$data = parent::get_post_data($fields);
			
//			$data = array(
//				'cid' => 6,
//				'type' => 3,
//				'user_list' => array(
//					'1','2'
//				)
//			);
			
			if(empty($data['cid']) || empty($data['type']))
				throw new SCException('请求错误');
			
			if($data['type'] ==1 && (empty($data['user_list']) || !is_array($data['user_list']) ))
				throw new SCException('请求错误');
				
			if(!g('client') ->check_own_client($data['cid'])){
				throw new SCException('没有权限');
			}
			$info = array();	
			switch($data['type']){
				case 3:
					$info['share_state'] =0;
					$info['share_list'] ='[]';
					break;
				case 1:
					$info['share_state'] =1;
					$info['share_list'] =json_encode($data['user_list']);
					break;
				case 2:
					$info['share_state'] =2;
					$info['share_list'] ='[]';
					break;			
			}
			
			!empty($info) && g('client') -> edit_client($data['cid'],$info);
			g('client') -> log_client($data['cid'],'共享客户');
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取单个联系人信息
	 */
	private function get_linkman(){
		try{
			//必填项
//			$fields = array(
//				'id','cid'
//			);
//			$data = parent::get_post_data($fields);

			$data = array(
				'id' => 81,
				'cid' => 6
			);
			
			if(empty($data['id']) || empty($data['cid'])){
				throw new SCException('请求错误');
			}
			
			g('client') ->can_see_client($data['cid']);
			
			$client = $data['cid'];
			$client = g('client') ->get_linkman($data['cid'],$data['id'],'*');
			cls_resp::echo_ok(cls_resp::$OK,'info',$client[0]);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 修改联系人信息
	 */
	private function edit_linkman(){
			try{
			//必填项
			$fields = array('id','cid');
			$data_data = parent::get_post_data($fields);
			if(isset($this->linkman_edit_item[$data_data['name']]))
				$data = array('id'=>$data_data['id'],$data_data['name']=>$data_data['value'],'cid'=>$data_data['cid']);
			else{
				throw new SCException('没有权限');
			}
//			$data=array(							//带*号为必填字段，其他选填
//					'cid' => 6,
//					'id' => 83,
//					'name' => '联系人0000',			//*
//					'mobile' => '手机号码',		//*
//					'dept' => '部门1',			//*
//					'position' => '职位',
//					'email' => '邮箱',
//					'telautogram' => '传真',
//					'main' => 1,				//是否为主要联系人，默认为不是
//			);
			if(!(isset($data['id']) && !empty($data['id']) && isset($data['cid']) && !empty($data['cid'])))
				throw new SCException('参数不正确');
			
			if(!g('client') -> check_client($data['cid'])){
				throw new SCException('没有权限');
			}
				
			$info = array();
			
			if(isset($data['name']) && !empty($data['name']))
				$info['linkman_name'] = $data['name'];
					
			if(isset($data['mobile']) && !empty($data['mobile']))
				$info['linkman_phone'] = $data['mobile'];
				
			if(isset($data['expand']) && is_array($data['expand'])){
				$expand = json_encode($data['expand']);
				$info['linkman_expand'] = $expand;
			}
			
			if(isset($data['email']) && !empty($data['email'])){
				$info['linkman_email'] = $data['email'];
			}
			
			if(isset($data['position']) && !empty($data['position'])){
				$info['linkman_position'] = $data['position'];
			}
			
			if(isset($data['dept']) && !empty($data['dept'])){
				$info['linkman_dept'] = $data['dept'];
			}
			
			if(isset($data['telautogram']) && !empty($data['telautogram'])){
				$info['linkman_telautogram'] = $data['telautogram'];
			}
			
			if(isset($data['main']) && !empty($data['main'])){
				$info['is_main'] = $data['main'];
			}
			
			$info['update_time'] = time();
			g('client') -> edit_linkman($data['cid'],$data['id'],$info);
			g('client') -> log_linkman($data['id'],'修改联系人');
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除联系人
	 */
	private function delete_linkman(){
		try{
		$fields = array(
				'id','cid'
			);
			$data = parent::get_post_data($fields);
	
//			$data = array(
//				'cid' => 7,
//				'id' => 85
//			);
			
			if(!isset($data['cid']) || empty($data['cid']) || !isset($data['id']) || empty($data['id']))
				throw new SCException('请求错误');
			
			if(!g('client') -> check_client($data['cid']))
				throw new SCException('没有权限');
			
			g('client') -> delete_linkman($data['cid'],$data['id']);
			g('client') -> log_linkman($data['id'],'删除联系人');
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
		
	}
	
	/**
	 * 删除客户信息
	 */
	private function delete_client(){
		try{
			$fields = array(
				'id'
			);
			$data = parent::get_post_data($fields);
	
//			$data = array(
//				'id' =>7
//			);
			
			if( !isset($data['id']) || empty($data['id']) )
				throw new SCException('请求错误');
			
			if(!g('client') -> check_client($data['id']))
				throw new SCException('没有权限');
			
			g('client') -> delete_client($data['id']);
			g('client') -> log_client($data['id'],'删除客户');
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	/**
	 * 获取客户详细信息(包含联系人信息)
	 */
	private function get_client_detail(){
		try{
			//必填项
//			$fields = array(
//				'id'
//			);
//			$data = parent::get_post_data($fields);

			$data = array(
				'id' => 7,
			);
			
			if(!isset($data['id']) || empty($data['id']))
				throw new SCException('请求参数错误');
			$fields = 'id,client_name,client_classify_name,client_business_name,client_number,client_postalcode,
						client_address,client_email,client_telautogram,client_expand,share_state,client_importance';
			$linkman_fields = 'id,linkman_name,linkman_phone,linkman_dept,linkman_position,linkman_email,linkman_telautogram,linkman_expand,is_main';
			
			g('client') ->can_see_client($data['id']);
			
			$client = g('client') -> get_client($data['id'],$fields,true,$linkman_fields);
			foreach($client['linkman'] as &$l)
				$l['linkman_expand'] = json_decode($l['linkman_expand'],true);
				
			cls_resp::echo_ok(cls_resp::$OK,'info',$client);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
				
			
	}
	
	/**
	 * 修改客户信息
	 */
	private function edit_client(){
		try{
			//必填项
			$fields = array('id');
			$data_id = parent::get_post_data($fields);
			if(isset($this->client_edit_item[$data_id['name']]))
				$data = array('id'=>$data_id['id'],$data_id['name']=>$data_id['value']);
			else
				throw new SCException('没有权限');
//			$data=array(							//带*号为必填字段，其他选填
//				'id' => 7,							//*
//				'name' => '超级大客户3',				
////				'classify' => 1,					
////				'business' => 1,					
////				'importance' => 1,					
////				'number' => '公司号码2',
////				'postalcode' => '邮政编码2',
////				'address' => '公司地址1',
////				'email' => ' 邮箱1',
////				'telautogram' => ' 传真1',
////				'expand'=>array(
////					'量级' => '世界五百强1'
////				),
//			);
			if(!(isset($data['id']) && !empty($data['id'])))
				throw new SCException('参数不正确');
			
			if(!g('client') -> check_client($data['id'])){
				throw new SCException('没有权限');
			}
				
			$info = array();
			
			if(isset($data['name']) && !empty($data['name']))
				$info['client_name'] = $data['name'];
					
			if(isset($data['importance']) && ($data['importance']>=1 || $data['importance']<=2))
				$info['client_importance'] = $data['importance'];
				
			if(isset($data['classify']) && !empty($data['classify'])){
				$classify = g('client') -> get_classify_type($data['classify']);
				$info['client_classify_name'] = $classify['client_name'];
				$info['client_classify'] = $classify['id'];
			}
				
			if( isset($data['business']) && !empty($data['business'])){
				$business = g('client') -> get_business_type($data['business']);
				$info['client_business_name'] = $business['client_name'];
				$info['client_business'] = $business['id'];
			}
				
			if(isset($data['expand']) && !empty($data['expand']) && is_array($data['expand'])){
				$expand = json_encode($data['expand']);
				$info['client_expand'] = $expand;
			}
			
			if(isset($data['postalcode']) && !empty($data['postalcode'])){
				$info['client_postalcode'] = $data['postalcode'];
			}
			
			if(isset($data['number']) && !empty($data['number'])){
				$info['client_number'] = $data['number'];
			}
			
			if(isset($data['address']) && !empty($data['address'])){
				$info['client_address'] = $data['address'];
			}
			
			if(isset($data['email']) && !empty($data['email'])){
				$info['client_email'] = $data['email'];
			}
			
			if(isset($data['telautogram']) && !empty($data['telautogram'])){
				$info['client_telautogram'] = $data['telautogram'];
			}
			
			$info['update_time'] = time();
			$nid = g('client') -> edit_client($data['id'],$info);
			g('client') -> log_client($data['id'],'修改客户');
			//创建成功返回ID
			cls_resp::echo_ok();	
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 新增客户
	 */
	private function add_client(){
		try{
			//必填项
			$fields = array(
				'name','classify','business','importance'
			);
			$data = parent::get_post_data($fields);
			
			
//			$data=array(							//带*号为必填字段，其他选填
//				'name' => '超级大客户',				//*
//				'classify' => 1,					//*
//				'business' => 1,					//*
//				'importance' => 1,					//*
//				'number' => '公司号码',
//				'postalcode' => '邮政编码',
//				'address' => '公司地址',
//				'email' => ' 邮箱',
//				'telautogram' => ' 传真',
//				'expand'=>array(
//					'量级' => '世界五百强'
//				),
//				'linkman'=>array(					//若有该字段，以下带*号为必填字段
//					array(
//						'name' => '联系人1',			//*
//						'mobile' => '手机号码',		//*
//						'dept' => '部门1',			//*
//						'position' => '职位',
//						'email' => '邮箱',
//						'telautogram' => '传真',
//						'main' => 1,				//是否为主要联系人，默认为不是
//					),
//					array(
//						'name' => '联系人1',			//*
//						'mobile' => '手机号码',		//*
//						'dept' => '部门1',			//*
//						'position' => '职位',
//						'email' => '邮箱',
//						'telautogram' => '传真',
//						'main' => 0,				
//						'expand' => array(
//							'身高' => '1米八',
//							'体重' => '200KG',
//						)
//					)
//				)
//			);
			
			if(!(isset($data['name']) && !empty($data['name'])))
				throw new SCException('参数不正确');
				
			if(!(isset($data['importance']) && ($data['importance']>=1 || $data['importance']<=2)))
				throw new SCException('参数不正确');
			
			if( !isset($data['classify']) || !empty($data['classify']))
				$classify = g('client') -> get_classify_type($data['classify']);
			else
				throw new SCException('行业类型错误!');
				
			if( !isset($data['business']) || !empty($data['business']))
				$business = g('client') -> get_business_type($data['business']);
			else
				throw new SCException('商业类型错误!');
				
			if(isset($data['expand']) && is_array($data['expand']))
				$expand = json_encode($data['expand']);
			else
				$expand = '[]';
				
			$info = array(
				'client_name' => $data['name'],
				'client_classify' => $data['classify'],
				'client_classify_name' => $classify['client_name'],
				'client_business' => $data['business'],
				'client_business_name' => $business['client_name'],
				'client_importance' => $data['importance'],
				'client_number' => isset($data['number'])?$data['number']:'',
				'client_postalcode' => isset($data['postalcode'])?$data['postalcode']:'',
				'client_address' => isset($data['address'])?$data['address']:'',
				'client_email' => isset($data['email'])?$data['email']:'',
				'client_telautogram' => isset($data['telautogram'])?$data['telautogram']:'',
				'client_expand' => $expand,
				'share_state' => 0,
				'create_time' => time(),
				'update_time' => time(),
				'info_state' => 1,
			);	
			
			$nid = g('client') -> add_client($info);
			
			$has_linkman =FALSE;	//是否包含联系人   //是，验证必填字段
			if(isset($data['linkman']) && !empty($data['linkman']) && is_array($data['linkman'])){
				$linkman = array();
				foreach($data['linkman'] as $lm){
					if(empty($lm))
						continue;
					if(!isset($lm['name']) || empty($lm['name']))
						throw new SCException('联系人姓名不能为空');
					if(!isset($lm['mobile']) || empty($lm['mobile']))
						throw new SCException('联系人手机不能为空');
					if(!isset($lm['dept']) || empty($lm['dept']))
						throw new SCException('联系人部门不能为空');
						
					if(!(isset($lm['expand']) && !empty($lm['expand']) && is_array($lm['expand'])))
						$lm['expand'] = "[]";
					else
						$lm['expand'] = json_encode($lm['expand']);
						
					$linkarr['linkman_name'] = $lm['name'];
					$linkarr['linkman_phone'] = $lm['mobile'];
					$linkarr['linkman_dept'] = $lm['dept'];
					$linkarr['linkman_position'] = isset($lm['position'])?$lm['position']:'';
					$linkarr['linkman_email'] = isset($lm['email'])?$lm['email']:'';
					$linkarr['linkman_telautogram'] = isset($lm['telautogram'])?$lm['telautogram']:'';
					$linkarr['linkman_expand'] = $lm['expand'];
					$linkarr['is_main'] = isset($lm['main'])?$lm['main']:0;
					$linkarr['create_time'] = time();
					$linkarr['update_time'] = time();
					$linkarr['info_state'] = 1;
					
					$linkman[] = $linkarr;
					$has_linkman =TRUE;
				}
			}
			
			$has_linkman && g('client') -> add_linkman($nid,$linkman);
			
			//创建成功返回ID
			cls_resp::echo_ok(cls_resp::$OK,'info',array('id'=>$nid));	
			g('client') -> log_client($nid,'新增客户');
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取客户列表
	 * 1.我的客户
	 * 2.共享客户
	 * 3.下属客户
	 */
	private function get_client_list(){
		$fields = array(
			'type'
		);
		$data = parent::get_post_data($fields);
		
		$key =false;
		if(isset($data['key']) && !empty($data['key']))
			$key = $data['key'];
//		$data = array(
//			'type' =>1			//1.我的  2.共享的，3.下属的
//		); 
		
		$page = isset($data['page']) && !empty($data['page']) ? $data['page'] : 1;
		$page_size = isset($data['page_size']) && !empty($data['page_size']) ? $data['page_size'] : 10;
		
		if( !intval($page) || !intval($page_size))
			throw new SCException('请求参数错误');
			
		if($data['type'] < 1 || $data['type'] > 3 || empty($data['type']))
			throw new SCException('请求类型错误');
		
		try{
			$clients = g('client') -> get_client_list($data['type'],$page,$page_size,$key);
			g('client') -> get_linkman($clients,false,'id,linkman_name,is_main');
			
			cls_resp::echo_ok(cls_resp::$OK,'info',$clients);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
}

/* End of this file */