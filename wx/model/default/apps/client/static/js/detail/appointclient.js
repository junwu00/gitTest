//指派
function appointClient(){
		var shareStr = "";
		$('.input_row').each(function(){
			if($(this).find(".icon-circle-blank").hasClass("icon-circle")){
				shareStr =$(this).attr("i");
				return false;
			}
		});
		if(shareStr==""){
			frame_obj.alert("请选择指定的指派人员!");
			return false;
		}
		
		assign(shareStr);
}

function back_to(){
	location.href=$('#return_client').val();
}

//指派
function assign(shareStr){
	var data = {
			'cid':$('#client_id').val(),
			'uid':shareStr
	};
	$('#ajax-url').val($('#assign_client').val());
	frame_obj.do_ajax_post(
			$(this),
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					frame_obj.alert('指派成功','返回',function(){
						location.href=$('#return_client').val();
					});
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}


$(document).ready(function(){
	//选择指定人员绑定事件
	$('.input_row').click(function(){
		$(".icon-circle-blank").removeClass("icon-circle");
		$(this).find(".icon-circle-blank").toggleClass("icon-circle");
	});
});






