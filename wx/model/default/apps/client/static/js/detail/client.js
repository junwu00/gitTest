//打开修改联系人信息项页面
//changetype：修改方式(0 文本 ；1 选择；2 地址；3 其他信息) fieldname：数据库的字段名 cname：显示名称 isnull：是否为空  regex：正则表达式
function editClientInfo(changetype,fieldname,cname,isnull,regex,datavalue){
	window.location.href=$('#edit_client').val()+"&cid="+$('#client_id').val()+"&changetype="+changetype+"&fieldname="+fieldname+"&cname="+cname+"&isnull="+isnull+"&regex="+regex+"&datavalue="+datavalue;
}

function editLinkmanInfo(changetype,fieldname,cname,isnull,regex,datavalue,lid){
	window.location.href=$('#edit_linkman').val()+"&cid="+$('#client_id').val()+"&changetype="+changetype+"&fieldname="+fieldname+"&cname="+cname+"&isnull="+isnull+"&regex="+regex+"&datavalue="+datavalue+"&lid="+lid;
}

//添加客户其他信息
function addOtherClientInfo(){
	window.location.href = $('#add_client_expand').val()+"&cid="+$('#client_id').val();
}

//共享客户
function shareClient(){
	window.location.href=$('#share_client').val()+"&cid="+$('#client_id').val();
}


//取消共享
function removeShareClient(){
	frame_obj.comfirm('是否取消共享？',function(){
		var data={
				cid:$('#client_id').val(),
				type:3
		};
		$('#ajax-url').val($('#ajax_share_client').val());
		frame_obj.do_ajax_post(
				$(this),
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						frame_obj.alert('取消共享成功','',function(){
							location.reload();
						});
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	});
}

function assignClient(){
	location.href=$('#assign_client').val()+"&c="+$("#client_id").val();
}

function saveClient(){
	var data ={
			cid:$('#client_id').val()	
		};
		$('#ajax-url').val($('#save_client').val());
		frame_obj.comfirm('确认要保存该客户吗?',function(){
			frame_obj.do_ajax_post(
					$(this),
					'',
					JSON.stringify(data),
					function(response){
						if(response['errcode']==0){
							//frame_obj.alert(response['errmsg']);
							frame_obj.alert("保存成功",'',function(){
								location.reload();
							});
						}else{
							frame_obj.alert(response['errmsg']);
						}
					}
			);
		});
}

function delete_client(){
	var data ={
			id:$('#client_id').val()	
		};
		$('#ajax-url').val($('#remove_client').val());
		frame_obj.comfirm('确认要删除该客户吗?',function(){
			frame_obj.do_ajax_post(
					$(this),
					'',
					JSON.stringify(data),
					function(response){
						if(response['errcode']==0){
							frame_obj.alert('删除成功','返回列表',function(){
								location.href=$('#return_list').val();
							});
						}else{
							frame_obj.alert(response['errmsg']);
						}
					}
			);
		});
}
//修改重要程度
function change_state(state){
	var data ={
			id:$('#client_id').val(),
			name:'importance',
			value:state
	};
	$('#ajax-url').val($('#edit_client_ajax_url').val());
	frame_obj.do_ajax_post(
			$(this),
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']!=0)
					frame_obj.alert(response['errmsg']);
			}
	);
}


//返回列表
function return_list(){
	location.href=$('#return_list').val();
}

$(document).ready(function(){
	$('.changelevel').click(function(){
		if($(this).children(".icon-circle-blank").hasClass("icon-circle")){
			$(this).children(".leveltitle").html("一般");
			$(this).children(".icon-circle-blank").removeClass("icon-circle")
			change_state(2);
		}else{
			$(this).children(".icon-circle-blank").addClass("icon-circle")
			$(this).children(".leveltitle").html("重要客户");
			change_state(1);
		}
	});
	
	
	if($('#curr').val()==1){
		if($('#share_state').val()==1 || $('#share_state').val()==2){
			$('#bottom-menu ul>li:first a').html('取消共享');
			$('#bottom-menu ul>li:first').unbind('click').bind('click',function(){
				removeShareClient();
			});
		}else{
			$('#bottom-menu ul>li:first').unbind('click').bind('click',function(){
				shareClient();
			});
		}
		$('#bottom-menu ul').children().eq(1).click(function(){
			delete_client();
		});
		
	}else if($('#curr').val()==2){
		$('#bottom-menu ul>li:first').unbind('click').bind('click',function(){
			assignClient();
		});
		$('#bottom-menu ul').children().eq(1).unbind('click').bind('click',function(){
			saveClient();
		});
	}else if($('#curr').val()==3){
		$('#bottom-menu ul>li:first').unbind('click').bind('click',function(){
			saveClient();
		});
	}else if($('#curr').val()==4){
		$('#bottom-menu ul>li:first').unbind('click').bind('click',function(){
			delete_client();
		});
	}
	
	//返回按钮绑定事件
	$('#bottom-menu ul').children().eq(2).click(function(){
		return_list();
	});
});
