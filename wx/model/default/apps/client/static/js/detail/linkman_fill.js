//打开修改联系人信息项页面
function saveInfo(changetype){
	if(changetype==0){
		if(checkInfo()){
			submit_data($('#edit_item').attr('i'),$('#edit_item').val());
		}
	}
	if(changetype==1){
		if($('.row_black').length == 1){
			submit_data($('.row_black').attr('i'),$('.row_black').attr('v'));
		}
	}
	if(changetype==2){
		if(checkInfo()){
			submit_data('address',$('#place').val()+$('#addressdetail').val());
		}
	}
	if(changetype==3){
		if(checkInfo()){
			var expand = [];
			$('.expand_list').each(function(){
				expand.push({name:$(this).attr('n'),value:$(this).attr('v')});
			});
			expand.push({name:$('.expand_title').val(),value:$('.expand_value').val()});
			
			submit_data('expand',expand);
		}
	}
}
function submit_data(name,value){
	var data = {
			cid:$('#client_id').val(),
			id:$('#linkman_id').val(),
			name : name,
			value : value
		};
		$('#ajax-url').val($('#edit_linkman_url').val());
		frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
					//	frame_obj.alert(response['errmsg']);
						location.href=$('#return_detail_url').val();
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
}


function back_to_detail(){
	location.href=$('#return_detail_url').val();
}

function checkInfo(){
	var ischeck = true;
	$(".input_row input").each(function(){
		if($(this).attr("isNull")=="true"&&$(this).val()==""){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'不能为空!');
			return false;
		}
		if((typeof($(this).attr("regex")) != "undefined"&&$(this).attr("regex")!="")&&$(this).val()!=""&&!check_data($(this).val(),$(this).attr("regex"))){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'格式不正确!');
			return false;
		}
	 });
	
	return ischeck;
}

//清除控件内容
function cancelText(obj){
	obj.parent().parent().children("input").eq(0).val("");
}

//删除其他信息
function delOtherInfo(){
	frame_obj.comfirm('确定要删除该信息吗？',function(){
		var expand = [];
		$('.expand_list').each(function(){
			expand.push({name:$(this).attr('n'),value:$(this).attr('v')});
		});
		
		submit_data('expand',expand);
	});
}


//初始化页面
$(document).ready(function() {
	$('.cancelText').bind('click',function(){
		cancelText($(this));
	});
	
	
	$('.choose_select').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.row_black').removeClass('row_black');
			$(this).addClass('row_black');
		});
	});
	$('#place').mlocal();
	$("#edit_item").focus();
});



