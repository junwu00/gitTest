function checkInfo(){
	var ischeck = true;
	$(".input_row input").each(function(){
		if($(this).attr("isNull")=="true"&&$(this).val()==""){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'不能为空!');
			return false;
		}
		if((typeof($(this).attr("regex")) != "undefined"&&$(this).attr("regex")!="")&&$(this).val()!=""&&!check_data($(this).val(),$(this).attr("regex"))){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'格式不正确!');
			return false;
		}
	 });
	
	return ischeck;
}

//添加
function addOtherInfo(){
	if(checkInfo()){
		var data = {
				cid:$('#client_id').val(),
				name:$('#expand_title').val(),
				value:$('#expand_value').val()
		};
		$('#ajax-url').val($('#add_client_expand').val());
		frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						//frame_obj.alert(response['errmsg']);
						location.href=$('#return_detail_client').val();
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	}
}
//初始化页面
$(document).ready(function() {
	$("#expand_title").focus();
});


//返回详情页面
function back_to_detail(){
	location.href=$('#return_detail_client').val();
}


