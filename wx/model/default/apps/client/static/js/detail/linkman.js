//打开修改联系人信息项页面
//changetype：修改方式(0 文本 ；1 选择；2 地址；3 其他信息) fieldname：数据库的字段名 cname：显示名称 isnull：是否为空  regex：正则表达式

function editLinkmanInfo(changetype,fieldname,cname,isnull,regex,datavalue,lid){
	window.location.href=$('#edit_linkman').val()+"&cid="+$('#client_id').val()+"&refer=linkman"+"&changetype="+changetype+"&fieldname="+fieldname+"&cname="+cname+"&isnull="+isnull+"&regex="+regex+"&datavalue="+datavalue+"&lid="+lid;
}

//添加联系人其他信息
function addotherInfo(lid){
	window.location.href = $('#add_linkman_expand').val()+"&cid="+$('#client_id').val()+"&lid="+lid;
}

$(document).ready(function(){
	$('#remove').unbind('click').bind('click',function(){
		frame_obj.comfirm('确认要删除联系人？',function(){
			var data ={
					cid:$('#client_id').val(),
					id:$('#linkman_id').val(),
			};
			$('#ajax-url').val($('#remove_linkman').val());
			frame_obj.do_ajax_post(
					$(this),
					'',
					JSON.stringify(data),
					function(response){
						if(response['errcode']==0){
							frame_obj.alert('删除成功','返回列表',function(){
								location.href=$('#return_list_url').val();
							});
						}else{
							frame_obj.alert(response['errmsg']);
						}
					}
			);
		});
	});
	
	$("#return").bind('click',function(){
		location.href=$('#return_list_url').val();
	});
});

