//共享
function shareClient(){
	var shareRange = "";//共享范围
	if($("#special").hasClass("icon-circle")){
		shareRange=$("#special").attr("value");
		var shareStr = [];
		$('.input_row').each(function(){
			if($(this).find(".icon-circle-blank").hasClass("icon-circle")){
				shareStr.push($(this).attr("value"));
			}
		});
		if(shareStr==""){
			frame_obj.alert("请选择指定的共享人员!");
			return false;
		}
		submit_data(1,shareStr);
	}else{
		shareRange=$("#all").attr("value");
		submit_data(2);
	}
	
}

function back_to(){
	location.href =$('#detail_client').val();
}

//提交数据
function submit_data(type,share_list){
	var data ="";
	switch(type){
		case 1:
			 data = {
				'cid':$('#client_id').val(),
				'type':type,
				'user_list':share_list
			};
			break;
		case 2:
			data = {
				'cid':$('#client_id').val(),
				'type':type,
			};
			break;
	}
	$('#ajax-url').val($('#share_client').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					frame_obj.alert('分享成功','返回',function(){
						location.href =$('#detail_client').val();
					});
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

$(document).ready(function(){
	//选择共享范围事件绑定
	$('.share').click(function(){
		$('.share').removeClass("icon-circle");
		$(this).addClass("icon-circle");
		if($(this).attr("id")=="all"){
			$("#special_div").css("display","none");
		}
		else{
			$("#special_div").css("display","block");
		}
	});
	
	//选择指定人员绑定事件
	$('.input_row').click(function(){
		$(this).find(".icon-circle-blank").toggleClass("icon-circle");
	});
});






