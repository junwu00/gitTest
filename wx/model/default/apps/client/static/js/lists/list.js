var page =0;
$(document).ready(function(){
	set_side();
	set_get_list();
	get_list();
	init_scroll_load('.container', '.rec_list', get_list);
	
});

//添加联系人跳转
function add_linkman(id){
	location.href = $('#add_linkman_url').val()+'&c='+id;
}
//查看联系人跳转
function to_linkman(cid,cn,id){
	location.href = $('#detail_linkman_url').val()+'&c='+cid+'&cn='+cn+'&l='+id;
}

 //设置查看客户信息按钮
function see_client(){
	$('.see_client').each(function(){
		$(this).unbind('click').bind('click',function(e){
			e.stopPropagation();
			location.href = $('#detail_client_url').val()+'&id='+$(this).attr('i');
		});
	});
}

//设置获取列表相关方法
function set_get_list(){
	$('.tool').each(function(){
		$(this).unbind('click').bind('click',function(){
			if($(this).hasClass('btn-unselected')){
				$('.tool').removeClass('btn-selected');
				$('.tool').addClass('btn-unselected');
				$(this).removeClass('btn-unselected');
				$(this).addClass('btn-selected');
				page=0;
				get_list();
			}
		});
	});
}
//设置下拉隐藏和显示的方法
function set_side(){
	$('.task-side').unbind('click').bind('click',function(){
		$(this).parent().children('.side-content').css('width','100%');
		$(this).parent().children('.side-content').slideToggle();
		$(this).children().children(".icon-plus").toggleClass("icon-minus");
	});
}
//搜索相关方法

function entersearch(){
	var key = $('#sreach_key').val();
	page = 0;
	get_list(key);
	
}

function get_list(key){
	var type = $('.btn-selected').attr('t');
	var data = {
		'type':type,
		'page':++page,
		'key':key
	};
	$('#ajax-url').val($('#ajax-list-url').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				show_list(response);
			}
	);
}
function show_list(data){
	var html = "";
	if(data['info']=="" && page==1){
		html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/logo.png"><span>暂时没有客户</span></div></div>';
		$('.list_content').html(html);
		return;
	}
	for(var i in data['info']){
		var client = data['info'][i];
		html +='<div class="row"><div class="task-side item" >'+
				'<div ><span class="icon-plus" style="padding: 10px"></span>'+client['client_name']+'<span class="right"><a href="javascript:void(0);"  i="'+client['id']+'" class="see_client btn btn-app btn-success management_btn">管理</a></span></div></div>';
		html +='<div class="side-content" >';
		if(client['linkman']!=""){
			for(var j in client['linkman']){
				var linkman = client['linkman'][j];
				var main = "";
				if(linkman['is_main']==1)
					main ='<span style="float:right;">主要联系人</span>';
				html +='<div class="children-report" onclick="to_linkman('+client['id']+",'"+client['client_name']+"',"+linkman['id']+')"><div class="message">'+
						'<div><span class="icon-user"></span>&nbsp'+linkman['linkman_name']+main+'</div></div></div>';
			}
		}
		html +='<div class="children-report"><div class="message"><div onclick="add_linkman('+client['id']+');"><div class="upload glyphicon glyphicon-plus float"></div><div><span class="grey valign">&nbsp;添加联系人</span></div></div>	</div></div>';
		html +='</div></div>';
	}
	if(page==1)
		$('.list_content').html(html);
	else
		$('.list_content').append(html);
	scroll_load_complete('.rec_list',data['info'].length);
	set_side();
	see_client();
}