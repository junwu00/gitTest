/*===============================================联系人其他信息===================================================*/
//打开添加联系人其它信息项页面
function addOtherLinkmanInfo(){
	$("#linkman_other_div input").val("");
	$("#linkman_other_div .top_del").css("display","none");
	$(".container").css("display","none");
	$("#linkman_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$('#linkman_other_div .savebutton').unbind();
	$("#linkman_other_div .savebutton").bind('click', saveOtherLinkmanInfo);
	$("#linkman_other_div input").eq(0).focus();
}

//打开修改联系人其它信息项页面
function editOtherLinkmanInfo(obj){
	var item_title = obj.find(".item_title").first().html();
	var item_content = obj.find(".item_content").first().html();
	$("#linkman_other_div input").val("");
	
	$(".container").css("display","none");
	$("#linkman_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$("#linkman_other_div input").eq(0).val(item_title);
	$("#linkman_other_div input").eq(1).val(item_content);
	$('#linkman_other_div .savebutton').unbind();
	$("#linkman_other_div .savebutton").bind('click', function(){updateOtherLinkmanInfo(obj)});
	$("#linkman_other_div .top_del").css("display","block");
	$('#linkman_other_div .top_del .right').unbind();
	$("#linkman_other_div .top_del .right").bind('click', function(){delLinkmanOtherinfo(obj)});
	$("#linkman_other_div input").eq(0).focus();
}



//保存联系人其他信息项
function saveOtherLinkmanInfo(){
	var linkman_other_title = $("#linkman_other_title").val();
	var linkman_other_content = $("#linkman_other_content").val();
	if(linkman_other_title==""){
		frame_obj.alert($("#linkman_other_title").attr("alertStr")+'不能为空!');
		return false;
	}
	if(linkman_other_content==""){
		frame_obj.alert($("#linkman_other_content").attr("alertStr")+'不能为空!');
		return false;
	}
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","block");
	var linkman_other_html = '<div class="row otherlinkmaninforow " n="'+linkman_other_title+'" v="'+linkman_other_content+'" ><div><div class="item_title">'+linkman_other_title+'</div><div class="right item_edit"><span class="icon-chevron-right"></span></div>'+
	'<div class="right item_div"><div class="item_content">'+linkman_other_content+'</div></div></div></div>';
	$("#linkman_other").before(linkman_other_html);
	$('.otherlinkmaninforow').bind('click',function(){
		editOtherLinkmanInfo($(this));
    });
}

//取消保存联系人其它信息项
function cancelOtherLinkmanInfo(){
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","block");
}

//更新联系人其他信息项
function updateOtherLinkmanInfo(obj){
	var linkman_other_title = $("#linkman_other_title").val();
	var linkman_other_content = $("#linkman_other_content").val();
	if(linkman_other_title==""){
		frame_obj.alert($("#linkman_other_title").attr("alertStr")+'不能为空!');
		return false;
	}
	if(linkman_other_content==""){
		frame_obj.alert($("#linkman_other_content").attr("alertStr")+'不能为空!');
		return false;
	}
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","block");
	obj.find(".item_title").first().html(linkman_other_title);
	obj.find(".item_content").first().html(linkman_other_content);
}

//删除联系人其他信息
function delLinkmanOtherinfo(obj){
	frame_obj.comfirm('确定要删除该信息吗？',function(){
		obj.remove();
		cancelOtherLinkmanInfo();
	});
}
/*===============================================================联系人其他信息=========================================================*/

/*===============================================================联系人基本信息==========================================================*/


//保存联系人所有信息
function saveLinkmanInfo(){
	if(checklinkmanInfo()){
		var expand =[];
		
		$('.otherlinkmaninforow').each(function(){
			expand.push({name:$(this).attr('n'),value:$(this).attr('v')});
		});
		
		var main = $('.is_main:checked').length;
		var data={
			'cid':$('#client_id').val(),
			'name':$('#linkman_name').val(),		//*
			'mobile':$('#linkman_phone').val(),		//*
			'dept':$('#linkman_dept').val(),			//*
			'position':$('#linkman_position').val(),
			'email':$('#linkman_email').val(),
			'telautogram':$("#linkman_tel").val(),
			'main':main,
			'expand':expand
				
		};
		$('#ajax-url').val($('#add_linkman_url').val());
		frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						frame_obj.alert('保存成功','返回列表',function(){
							location.href=$('#return_list_url').val();
						});
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	}
}

//检查联系人信息的必填项
function checklinkmanInfo(){
	var ischeck = true;
	$("#linkman_div input").each(function(){
		if($(this).attr("isNull")=="true"&&$(this).val()==""){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'不能为空!');
			return false;
		}
		if((typeof($(this).attr("regex")) != "undefined"&&$(this).attr("regex")!="")&&$(this).val()!=""&&!check_data($(this).val(),$(this).attr("regex"))){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'格式不正确!');
			return false;
		}
	 });
	return ischeck;
}


/*=========================================联系人基本信息==================================================*/


//初始化页面
$(document).ready(function() {
	$("#linkman_name").focus();
	$("#bottom-menu a").click(function(e){
		e.stopPropagation();
		saveLinkmanInfo();
	});
});
