/*===============================================联系人其他信息===================================================*/
//打开添加联系人其它信息项页面
function addOtherLinkmanInfo(){
	$("#linkman_other_div input").val("");
	$("#linkman_other_div .top_del").css("display","none");
	$(".container").css("display","none");
	$("#linkman_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$('#linkman_other_div .savebutton').unbind();
	$("#linkman_other_div .savebutton").bind('click', saveOtherLinkmanInfo);
	$("#linkman_other_div input").eq(0).focus();
}

//打开修改联系人其它信息项页面
function editOtherLinkmanInfo(obj){
	var item_title = obj.find(".item_title").first().html();
	var item_content = obj.find(".item_content").first().html();
	$("#linkman_other_div input").val("");
	
	$(".container").css("display","none");
	$("#linkman_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$("#linkman_other_div input").eq(0).val(item_title);
	$("#linkman_other_div input").eq(1).val(item_content);
	$('#linkman_other_div .savebutton').unbind();
	$("#linkman_other_div .savebutton").bind('click', function(){updateOtherLinkmanInfo(obj)});
	$("#linkman_other_div .top_del").css("display","block");
	$('#linkman_other_div .top_del .right').unbind();
	$("#linkman_other_div .top_del .right").bind('click', function(){delLinkmanOtherinfo(obj)});
	$("#linkman_other_div input").eq(0).focus();
	
}



//保存联系人其他信息项
function saveOtherLinkmanInfo(){
	var linkman_other_title = $("#linkman_other_title").val();
	var linkman_other_content = $("#linkman_other_content").val();
	if(linkman_other_title==""){
		frame_obj.alert($("#linkman_other_title").attr("alertStr")+'不能为空!');
		return false;
	}
	if(linkman_other_content==""){
		frame_obj.alert($("#linkman_other_content").attr("alertStr")+'不能为空!');
		return false;
	}
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","none");
	var linkman_other_html = '<div class="row otherlinkmaninforow " ><div><div class="item_title">'+linkman_other_title+'</div><div class="right item_edit"><span class="icon-chevron-right"></span></div>'+
	'<div class="right item_div"><div class="item_content">'+linkman_other_content+'</div></div></div></div>';
	$("#linkman_other").before(linkman_other_html);
	$('.otherlinkmaninforow').bind('click',function(){
		editOtherLinkmanInfo($(this));
    });
}

//取消保存联系人其它信息项
function cancelOtherLinkmanInfo(){
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","none");
}

//更新联系人其他信息项
function updateOtherLinkmanInfo(obj){
	var linkman_other_title = $("#linkman_other_title").val();
	var linkman_other_content = $("#linkman_other_content").val();
	$(".container").css("display","none");
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","none");
	obj.find(".item_title").first().html(linkman_other_title);
	obj.find(".item_content").first().html(linkman_other_content);
}

//删除联系人其他信息
function delLinkmanOtherinfo(obj){
	frame_obj.comfirm('确定要删除该信息吗？',function(){
		obj.remove();
		cancelOtherLinkmanInfo();
	});
}
/*===============================================================联系人其他信息=========================================================*/

/*===============================================================联系人基本信息==========================================================*/
//打开联系人编辑页面
function addlinkman(){
	$(".container").css("display",'none');
	$("#linkman_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$(".linkman_del").css("display","none");
	$("#linkman_div input").eq(0).focus();
}

//取消联系人编辑页面
function cancelAddlinkman(){
	$(".container").css("display","none");
	$("#client_div").css("display","block");
	$("#bottom-menu").css("display","block");
}

//保存联系人所有信息
function saveLinkmanInfo(){
	if(checklinkmanInfo()){
		//基本信息
		var html = "";
		var msg ='';
		html += '<div class="row linkman_row top_row" onclick="editLinkmanInfo()">'+
			'<div><div class="item_title" style="max-width:100%"><span class="icon-user"></span>&nbsp;&nbsp;<span id="linkman_name">'+$(".input_row input[alertStr='联系人名称']").val()+'</span></div>'+
			'<div class="right item_edit"><span class="icon-chevron-right"></span></div></div>'+
		'</div>';
		msg += $(".input_row input[alertStr='联系人名称']").attr('i')+'="'+$(".input_row input[alertStr='联系人名称']").val()+'" ';
		var i=0;
		$(".input_row input").each(function(){
			i++;
			if(i==1){//联系人名称样式比较特殊，跳过
				return;
			}
			var display ="";
			if($.trim($(this).val())=="")
				display = 'style="display:none;"';
			html+='<div class="row linkman_row linkman_base_row" '+display+'>'+
				'<div><div class="item_title">'+$(this).attr("alertStr")+'</div>'+
					'<div class="right item_div"><div class="item_content linkman_padding">'+$(this).val()+'</div></div>'+
				'</div>'+
			'</div>';
			msg += $(this).attr('i')+'="'+$(this).val()+'" ';
		});
		//其他信息
		$(".otherlinkmaninforow").each(function(){
			html+='<div class="row linkman_row linkman_expand" n="'+$(this).find(".item_title").first().html()+'" v="'+$(this).find(".item_content").first().html()+'">'+
				'<div><div class="item_title">'+$(this).find(".item_title").first().html()+'</div>'+
					'<div class="right item_div"><div class="item_content linkman_padding">'+$(this).find(".item_content").first().html()+'</div></div>'+
				'</div>'+
			'</div>';
		});
		html ='<div class="other_linkman" '+msg+'>'+html+'</div>';
		if($('.other_linkman').length !=0)
			$(".other_linkman").remove();
		$("#add_linkman").after(html);
		$("#add_linkman").css("display","none");
		cancelAddlinkman();//切换页面
	}
}

//检查联系人信息的必填项
function checklinkmanInfo(){
	var ischeck = true;
	$("#linkman_div input").each(function(){
		if($(this).attr("isNull")=="true" && $(this).val()==""){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'不能为空!');
			return false;
		}
		if((typeof($(this).attr("regex")) != "undefined"&&$(this).attr("regex")!="")&&$(this).val()!=""&&!check_data($(this).val(),$(this).attr("regex"))){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'格式不正确!');
			return false;
		}
	 });
	return ischeck;
}

//修改联系人信息
function editLinkmanInfo(){
	var i=0;
	$(".input_row input").each(function(){
		if(i==0){
			$(this).val($("#linkman_name").html());
		}
		else{
			$(this).val($(".linkman_base_row .item_content").eq(i-1).html());
		}
		i++;
	});
	addlinkman();
	$(".linkman_del").css("display","block");
}

//删除联系人信息
function delLinkmanInfo(){
	frame_obj.comfirm('确定要删除联系人信息吗？',function(){
		//清除联系人编辑页面信息
		$(".otherlinkmaninforow").each(function(){
			$("#this").remove();
		});
		$("#linkman_div input").val("");
		
		$(".linkman_row").remove();
		$("#add_linkman").css("display","block");
		cancelAddlinkman();//切换页面
	});
}
/*=========================================联系人基本信息==================================================*/

/*=========================================客户其他信息===================================================*/
//打开添加联系人其它信息项页面
function addOtherClientInfo(){
	$("#client_other_div input").val("");
	$("#client_other_div .top_del").css("display","none");
	$(".container").css("display","none");
	$("#client_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$('#client_other_div .savebutton').unbind();
	$("#client_other_div .savebutton").bind('click', saveOtherClientInfo);
	$("#client_other_div input").eq(0).focus();
}

//打开修改联系人其它信息项页面
function editOtherClientInfo(obj){
	var item_title = obj.find(".item_title").first().html();
	var item_content = obj.find(".item_content").first().html();
	$("#client_other_div input").val("");
	
	$(".container").css("display","none");
	$("#client_other_div").css("display","block");
	$("#bottom-menu").css("display","none");
	$("#client_other_div input").eq(0).val(item_title);
	$("#client_other_div input").eq(1).val(item_content);
	$('#client_other_div .savebutton').unbind();
	$("#client_other_div .savebutton").bind('click', function(){updateOtherClientInfo(obj)});
	$("#client_other_div .top_del").css("display","block");
	$('#client_other_div .top_del .right').unbind();
	$("#client_other_div .top_del .right").bind('click', function(){delClientOtherinfo(obj)});
	
}

//保存联系人其他信息项
function saveOtherClientInfo(){
	var client_other_title = $("#client_other_title").val();
	var client_other_content = $("#client_other_content").val();
	if(client_other_title==""){
		frame_obj.alert($("#client_other_title").attr("alertStr")+'不能为空!');
		return false;
	}
	if(client_other_content==""){
		frame_obj.alert($("#client_other_content").attr("alertStr")+'不能为空!');
		return false;
	}
	$(".container").css("display","none");
	$("#client_div").css("display","block");
	$("#bottom-menu").css("display","block");
	var client_other_html = '<div class="row otherclientinforow " n="'+client_other_title+'" v="'+client_other_content+'"><div><div class="item_title">'+client_other_title+'</div><div class="right item_edit"><span class="icon-chevron-right"></span></div>'+
	'<div class="right item_div"><div class="item_content">'+client_other_content+'</div></div></div></div>';
	$("#client_other").before(client_other_html);
	$('.otherclientinforow').bind('click',function(){
		editOtherClientInfo($(this));
    });
}

//取消保存联系人其它信息项
function cancelOtherClientInfo(){
	$(".container").css("display","none");
	$("#client_div").css("display","block");
	$("#bottom-menu").css("display","block");
}

//更新联系人其他信息项
function updateOtherClientInfo(obj){
	var client_other_title = $("#client_other_title").val();
	var client_other_content = $("#client_other_content").val();
	$(".container").css("display","none");
	$("#client_div").css("display","block");
	$("#bottom-menu").css("display","block");
	obj.find(".item_title").first().html(client_other_title);
	obj.find(".item_content").first().html(client_other_content);
}

//删除联系人其他信息
function delClientOtherinfo(obj){
	frame_obj.comfirm('确定要删除该信息吗？',function(){
		obj.remove();
		cancelOtherClientInfo();
	});
}




/*=========================================客户其他信息===================================================*/

/*=========================================初始化保存按钮及保存客户信息====================================*/
//保存客户信息
function saveClientInfo(){
	var expand = [];
	if($('.otherclientinforow').length != 0)
		$('.otherclientinforow').each(function(){
			expand.push({name:$(this).attr('n'),value:$(this).attr('v')});
		});
	
	var linkman =[];
	
	if($('.other_linkman').length!=0){
		var  linkman_expand = [];
		if($('.linkman_expand').length !=0){
			$('.linkman_expand').each(function(){
				linkman_expand.push({name:$(this).attr('n'),value:$(this).attr('v')});
			});
		}
		var lm = {
				'name':$('.other_linkman').attr('name'),
				'mobile':$('.other_linkman').attr('phone'),
				'dept':$('.other_linkman').attr('dept'),
				'position':$('.other_linkman').attr('position'),
				'email':$('.other_linkman').attr('email'),
				'telautogram':$('.other_linkman').attr('telautogram'),
				'main':1,
				'expand':linkman_expand
		};
		linkman.push(lm);
	}
	
	if(checkClientInfo()){
		var data = {
				'name':$('#client_name').val(),				
				'classify':$('.classify').val(),					
				'business':$('.business').val(),					
				'importance':$('.icon-circle').attr('i'),		
				'number':$('#client_number').val(),
				'postalcode':$('#client_postalcode').val(),
				'address':$('#place').val()+$('#address_detail').val(),
				'email':$('#client_email').val(),
				'telautogram':$('#client_telautogram').val(),
				'expand':expand,
				'linkman':linkman
		};
		$('#ajax-url').val($('#ajax_add_client_url').val());
		frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						frame_obj.alert('保存成功','返回列表',function(){
							location.href = $('#client_list').val();
						});
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	}
}

//检查客户信息的必填项
function checkClientInfo(){
	var ischeck = true;
	$("#client_div input").each(function(){
		if($(this).attr("isNull")=="true"&& $.trim($(this).val())==""){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'不能为空!');
			return false;
		}
		if((typeof($(this).attr("regex")) != "undefined"&&$(this).attr("regex")!="")&&$(this).val()!=""&&!check_data($(this).val(),$(this).attr("regex"))){
			ischeck = false;
			frame_obj.alert($(this).attr("alertStr")+'格式不正确!');
			return false;
		}
	 });
	if(!ischeck)
		return ischeck
	$("#client_div select").each(function(){
		if($(this).attr("isNull")=="true" && ($(this).val()=="" || $(this).val()==0)){
			ischeck = false;
			frame_obj.alert("请选择"+$(this).attr("alertStr")+'!');
			return false;
		}
	});
	return ischeck;
}

function share_win(){
	$('#win-dlg').modal('show');
	$("#talking_content").focus();
	$('#win-ok').unbind('click').bind('click', function() {
		$('#win-dlg').modal('hide');
		talking_ajax();
	});
}

//返回列表
function return_list(){
	location.href = $('#client_list').val();
}

//初始化页面
$(document).ready(function() {
	$('#place').mlocal();//选择地址控件初始化
	
	$("#client_name").focus();
	
	$("#bottom-menu ul>li:first").click(function(e){
		e.stopPropagation();
		saveClientInfo();
	});
	
	//绑定选择客户重要程度控件初始化
	$(".changeLevel").click(function(e){
		e.stopPropagation();
		$(".changeLevel").removeClass("icon-circle");
		$(this).addClass("icon-circle");
	});
	
	//返回按钮绑定事件
	$('#bottom-menu ul').children().eq(1).click(function(){
		return_list();
	});
});
