<?php
/**
 * 应用的配置文件_通讯录
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 				=> 2,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(2),
	'name' 				=> 'contact',
	'cn_name' 			=> '通讯录',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Ccations.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '通讯录', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=contact', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '常用联系人', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=contact&a=general', 'icon' => ''),
		),
	),
	
	'wx_menu' => array(
		array('name' => '通讯录', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=contact&corpurl=<corpurl>'),
		array('name' => '常用联系', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=contact&a=general&corpurl=<corpurl>'),
	),

	'send_invite_msg'	=> 1,
);

// end of file