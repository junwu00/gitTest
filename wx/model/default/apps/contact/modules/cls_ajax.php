<?php
/**
 * 吉兆通讯录相关ajax接口
 * @author rhk
 *
 * @create 2017-03-02 14:38
 */

class cls_ajax {
  
    //录入注册信息
    // gch edit 2018-03-09 17:33:38
	public function register() {
		$invite_id = get_var_value('invite_id', false);
		$name = get_var_value('name', false);
        $phone = get_var_value('phone', false);

		//获取邀请单信息
		$invite_list_info = g('invite_action') -> getInvitePageData($invite_id);
		//判断邀请信息是否有误
		if (empty($invite_list_info) ||!$invite_id) {
			cls_resp::show_err_page('邀请信息有误，请联系管理员');
		}
        $com_id = $invite_list_info['com_id'];

        //获取企业信息
        $com_info = g('company') -> get_by_id($com_id);
        if (empty($com_info)) {
            cls_resp::show_err_page('企业信息有误');
        }
        $rawDeptTree = array();
		//处理注册表单
        if (empty($name) || empty($phone)) {
            cls_resp::echo_err(cls_resp::$ForbidReq, '注册信息不能留空');
        }
        if (!check_data($phone, "mobile")) {  
            cls_resp::echo_err(cls_resp::$ForbidReq, '手机号格式不正确');
        }
        // $ex = g('invite_action') -> checkPhone($phone, $com_id); 
        $rawUserInfo = g('user') -> getByMobile($phone, 'id, dept_list, `name`, `acct`,dept_tree');
        
        $isRegister = false;
        $rawInviteDeptList = json_decode($invite_list_info['dept_list'], true);
        $wx_dept_list = g('invite_action') -> get_wx_dept_ids($rawInviteDeptList);
        $PyName = $name;
        $saveName = $name;

        if (!empty($rawUserInfo)) {
            $isRegister = true;
            // cls_resp::echo_err(cls_resp::$ForbidReq, '该手机号已注册');
            $rawUserInfo = array_shift($rawUserInfo);

            $rawUserName = explode('/', $rawUserInfo['name']);
            if (in_array($name, $rawUserName)) {
                cls_resp::echo_err(cls_resp::$ForbidReq, "学生[{$name}]不能重复录入");
            }
            
            $PyName = $rawUserInfo['name'] . $name;
            $saveName = $rawUserInfo['name'] . '/' . $name;
            $deptList = json_decode($invite_list_info['dept_list'], true);
            !is_array($deptList) && $deptList = array();
            $rawDeptList = json_decode($rawUserInfo['dept_list'], true);
            !is_array($rawDeptList) && $rawDeptList = array();

            $rawDeptTree = json_decode($rawUserInfo['dept_tree'], true);
            !is_array($rawDeptTree) && $rawDeptTree = array();
            
            $deptList = array_unique(array_merge($rawDeptList, $deptList));
            $invite_list_info['dept_list'] = json_encode($deptList);
        }

        //通过部门列表获取微信部门列表
        // $wx_dept_list = array();
        // $tempArr = json_decode($invite_list_info['dept_list'], true);
        // foreach ($tempArr as $value) {
        // 	$wx_dept_list[] = g('invite_action') -> get_wx_dept_id($value);
        // }

        $all_dept_tree = array();
        // foreach ($tempArr as $dept_id) {
        foreach ($rawInviteDeptList as $dept_id) {
            $dept_tree = g('dept') -> get_single_dept_parent_ids($com_info['dept_id'], $dept_id);
            $all_dept_tree[] = $dept_tree;
        }unset($dept_id);
        $all_dept_tree = array_merge($all_dept_tree,$rawDeptTree);

        //组装用户数据
        $py = g('char') -> cn2py($PyName, true);
        $userInfo = array(
            'acct' => $phone,   //以手机号为账号
            'name' => $saveName, 
            'com_id' => $com_id,
            'all_py' => $py["all_py"],
            'first_py' => $py["first_py"],
            'mobile' => $phone,
            'dept_list' => $invite_list_info['dept_list'],
            'dept_tree' => json_encode($all_dept_tree),
            'wx_dept_list' => json_encode($wx_dept_list),
            'create_time' => time(),
            'root_id' => $com_info['dept_id']
        );

        g('db')->begin_trans(); //开启事务 
        if ($isRegister) {
            $userInfo['acct'] = $rawUserInfo['acct'];
            $uid = g('invite_action') -> updateUser($rawUserInfo['id'], $userInfo);    //更新用户信息到sc_user表
            $uid && $uid = $rawUserInfo['id'];
        } else {
            $uid = g('invite_action') -> insertUser($userInfo);    //插入用户信息到sc_user表

        }
        if (!$uid) {
            g('db') -> rollback();
            cls_resp::echo_err(cls_resp::$ForbidReq, '录入用户数据失败');
        }

        // 插入用户信息到邀请用户表
        $info = array(
            'uid' => $uid, 
            'aid' => 0,
            'inviter_id' => 0,
            'invit_time' => time(),
            'invite_name' => $name,
            'com_id' => $com_id,
            'invit_order_id' => $invite_id
        );
        $res = g('invite_action') -> insertInvite($info);   //插入用户信息到邀请用户表
        if (!$res) {
            g('db') -> rollback();
            cls_resp::echo_err(cls_resp::$ForbidReq, '录入邀请信息失败');
        }

        //信息录入企业号
        try {
            $access_token = g('atoken') -> get_access_token_by_com($com_id); 
            if ($isRegister) {
                g('wxqy') -> update_user($access_token, $userInfo['acct'], $userInfo['name'], $wx_dept_list, "", $userInfo['mobile'], 0, "", "", "");

                $contact_app_conf = include(SYSTEM_APPS.'contact/config.php');
                $app_id = $contact_app_conf["send_invite_msg"];
                $app_handler = app_info::get_handler($app_id, $com_id, $com_info['dept_id']);
                $post_data = array(
                    "ToUserName" => $com_info["corp_id"],
                    "FromUserName" => $userInfo["acct"],
                    "CreateTime" => time(),
                    "MsgType" => "event",
                    "AgentID" => 0,
                    "Event" => "subscribe",
                );
                $app_handler -> contact_send_msg($post_data,$invite_id);

            } else {
                g('wxqy') -> create_user($access_token, $userInfo['acct'], $userInfo['name'], $wx_dept_list, "", $userInfo['mobile'], 0, "", "", "");
            }
        } catch (\Exception $e) {
            cls_resp::echo_err(cls_resp::$Busy, '调用企业号修改成员时失败');
        }
        
        //提交事务
        g('db') -> commit(); 

        cls_resp::echo_ok(cls_resp::$OK, 'info', array($uid));  //返回用户id
	}

    /**
    * 根据手机号获取用户信息
    * gch add invite
    * @access public
    * @param   
    * @return   
    */
    public function getChildInfo() {
		$name = get_var_value('name', false);
        $phone = get_var_value('phone', false);
        
        if (empty($phone) || empty($name)) {
            cls_resp::echo_err(cls_resp::$ForbidReq, '注册信息不能留空');
        }
        if (!check_data($phone, "mobile")) {  
            cls_resp::echo_err(cls_resp::$ForbidReq, '手机号格式不正确');
        }

        $redis_key = __FUNCTION__ . $name . $phone;//这里应选择一个可以标识缓存的相对定值
        $redis_key = md5($redis_key);
        $redis_cache = g('redis') -> get($redis_key);
        if (!empty($redis_cache)) {
            cls_resp::echo_ok(cls_resp::$OK, 'info', json_decode($redis_cache, TRUE));
        }

        $son = array();
        $userInfo = g('user') -> getByMobile($phone, 'dept_list, `name`');
        if (empty($userInfo)) {
            cls_resp::echo_ok(cls_resp::$OK, 'info', $son);
        }

        $deptIdArr = array();
        foreach ($userInfo as $u) {
            $nameArr = explode('/', $u['name']);
            $deptList = json_decode($u['dept_list'], true);
            foreach ($nameArr as $k => $n) {
                $deptId = isset($deptList[$k]) ? $deptList[$k] : $deptList[0];
                $tmp = array(
                    'dept_id' => $deptId,
                    'name' => $n,
                    'class' => $deptId,
                );
                $son[] = $tmp;
                $deptIdArr[] = $deptId;
            }
        }

        // 填充部门名称
        $deptInfo = g('dept') -> get_dept_name($deptIdArr);
        $deptId = array_column($deptInfo, 'id');
        $deptInfo = array_combine($deptId, $deptInfo);
        foreach ($son as &$d) {
            $d['class'] = $deptInfo[$d['dept_id']]['name'];
        }unset($d);
        $son = array_values($son);

        if (!empty($son)) {
            $redis_cache = g('redis') -> set($redis_key, json_encode($son), 10);
        }
        cls_resp::echo_ok(cls_resp::$OK, 'info', $son);
    }

	//图文消息接口
	public function image_text() {
		$invite_id = get_var_value('invite_id', false);
        $invite_info = g('invite_action') -> contact_subscribe_data($invite_id);

        if (!$invite_id || empty($invite_info)) {
            cls_resp::echo_err(cls_resp::$ForbidReq, '邀请信息有误');
        }
        //时间戳转日期
        $invite_info['create_time'] = date('Y-m-d', $invite_info['create_time']);
        cls_resp::echo_ok(cls_resp::$OK, 'info', $invite_info); 
	}

//-------------------------测试部分--------------------------------------

}