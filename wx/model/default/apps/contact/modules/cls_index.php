<?php
/**
 * 通讯录默认页面
 * @author yangpz
 *
 */
class cls_index extends abs_app_base {
	
	public function __construct() {
		parent::__construct('contact');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		parent::oauth_jssdk();
		parent::assign_contact_dept(TRUE);
		$conf = g('cont_conf') -> get_by_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
		g('smarty') -> assign('add_general_url', $this -> app_domain.'&a=add_general');
		g('smarty') -> assign('del_general_url', $this -> app_domain.'&a=del_general');
		$user_info = g('api_user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('user_info', $user_info);
		
		$field_map = array(
			'name'		=> '姓名',
			'acct'		=> '工号',
			'position'	=> '职位',
			'wx_acct'	=> '微信号',
			'phone'		=> '手机号',
			'tel'		=> '电话'
		);
		g('smarty') -> assign('conf', $conf);
		$search_field = json_decode($conf['search_list'], TRUE);
		!is_array($search_field) && $search_field = array();
		$search_field_str = '';
		foreach ($search_field as $f) {
			isset($field_map[$f]) && $search_field_str .= '/'.$field_map[$f];
		}unset($f);
		!empty($search_field_str) && $search_field_str = substr($search_field_str, 1);
		
		g('smarty') -> assign('search_field_str', $search_field_str);
        g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> show($this -> temp_path.'index.html');
	}
	
	/** 添加常用联系人 */
	public function add_general() {
		$this -> _add_general();
	}
	
	/** 删除常用联系人 */
	public function del_general() {
		$this -> _del_general();
	}
	
	/**
	 * 加载常用联系人页面
	 */
	public function general(){
		$act = get_var_value("ajax_act");
		switch($act) {
			case 'save':
				$this -> self_save();
				break;
				
			default:
				parent::oauth_jssdk();
				parent::assign_contact_dept();
	
				$uid = $_SESSION[SESSION_VISIT_USER_ID];
				
				//个人信息-------------------------------
				$user = g('user') -> get_by_id($uid, 'id, name, acct, pic_url, gender, work_position, weixin_id, tel, mobile, email, position, dept_list, state, ext_attr, short_code, inner_code');
				$user['ext_attr'] = ($_SESSION[SESSION_VISIT_COM_ID] == 11951) ? json_decode($user['ext_attr'], TRUE) : array();	//TODO 仅为该企业开启
				$user['dept_list'] = json_decode($user['dept_list'], TRUE);
				$depts = g('api_dept') -> list_by_ids($user['dept_list'], 'id, name');
				$sotred_depts = array();
				foreach ($depts as $d) {
					$sotred_depts[$d['id']] = $d;
				}unset($d);
				unset($depts);
				
				$dept_str = '';
				foreach ($user['dept_list'] as $d) {
					isset($sotred_depts[$d]) && $dept_str .= ','.$sotred_depts[$d]['name'];
				}unset($d);
				$user['dept_str'] = empty($dept_str) ? '' : substr($dept_str, 1);
				g('smarty') -> assign('self', $user);
//				var_dump($user);exit();
					
				//联系人信息-----------------------------
				$user_ids = array();	
				
				//自己配置的常用
				$general_user = g('user_general') -> get_by_ids($uid);
				$dlist = $general_user ? json_decode($general_user[0]['del_list'], TRUE) : array();
				!is_array($dlist) && $dlist = array();
				$glist = $general_user ? json_decode($general_user[0]['general_list'], TRUE) : array();
				!is_array($glist) && $glist = array();
				
				//管理员配置的常用（除去自己移除的常用）
				$conf = g('cont_conf') -> get_by_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
				$general_list = json_decode($conf['general_list'], TRUE);
				!is_array($general_list) && $general_list = array();
				foreach ($general_list as $u){
					if($u != $uid && !in_array($u, $dlist, FALSE) && !empty($u)){
						$user_ids[] = $u;
					}
				}unset($u);
				
				//自己配置的常用
				foreach ($glist as $u){
					if(!in_array($u, $dlist, TRUE)){
						$user_ids[] = $u;
					}
				}unset($u);
				
				$users = array();
				if (!empty($user_ids)) {
					$users = g('user') -> get_by_ids($user_ids, 'id, name, acct, all_py, first_py, pic_url, position, dept_list, state');
				}
				empty($users) && $users = array();
				
				//加载联系人部门信息
				$dept_ids = array();
				foreach ($users as &$u) {
					$u['dept_list'] = json_decode($u['dept_list'], TRUE);
					foreach ($u['dept_list'] as $d) {
						$dept_ids[] = $d;
					}unset($d);
				}
				$depts = g('api_dept') -> list_by_ids($dept_ids, 'id, name');
				$sotred_depts = array();
				foreach ($depts as $d) {
					$sotred_depts[$d['id']] = $d;
				}unset($d);
				unset($depts);
				
				foreach ($users as &$u) {
					$dept_str = '';
					foreach ($u['dept_list'] as $d) {
						isset($sotred_depts[$d]) && $dept_str .= ','.$sotred_depts[$d]['name'];
					}unset($d);
					$u['dept_str'] = empty($dept_str) ? '' : substr($dept_str, 1);
				}
				
				g('smarty') -> assign('users', $users);
				
				$conf = g('cont_conf') -> get_by_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
				g('smarty') -> assign('save_self_url', $this -> app_domain.'&a=general');
				g('smarty') -> assign('add_general_url', $this -> app_domain.'&a=add_general');
				g('smarty') -> assign('del_general_url', $this -> app_domain.'&a=del_general');
				g('smarty') -> assign('conf', $conf);
				g('smarty') -> show($this -> temp_path.'general.html');
		}
	}
	
	/**
	 * 邀请单注册
	 */
	public function jz_contact_register() {
		global $app;
		$invite_id = get_var_value('invite_id', false);
		$invite_info = g('invite_action') -> getInvitePageData($invite_id);
		if (!$invite_id || empty($invite_info)) {
			cls_resp::show_err_page('邀请信息有误');
		}

		// gch add dept_name 2018-04-16 18:23:16
		$deptList = json_decode($invite_info['dept_list'], true);
		!is_array($deptList) && $deptList = array();
		$deptInfo = g('invite_action') -> get_dept_ids($deptList,  '`name`');
		if (!empty($deptInfo)) {
			$deptName = array_column($deptInfo, 'name');
			$deptName = implode(',', $deptName);
		} else {
			$deptName = '';
		}
		g('smarty') -> assign('import_dept_name', $deptName);

		// 验证有效期
		$start_time = $invite_info["start_time"];
		$end_time = $invite_info["end_time"];
		$now = time();
		if ($now < $start_time) {
			cls_resp::show_err_page('该邀请单未开始');
		}
		if ($now > $end_time) {
			cls_resp::show_err_page('该邀请单有效期已过');
		}

		$com_info = g('company') -> get_by_id($invite_info['com_id']);
		if (empty($com_info)) {
			cls_resp::show_err_page('找不到企业信息');
		}
		if ($invite_info['is_show'] == 0) {
			cls_resp::show_err_page('邀请单被禁用，请联系管理员');
		}

		$post_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&a=register&free=1&corpurl='.$com_info['corp_url'];
		$child_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&a=getChildInfo&free=1&corpurl='.$com_info['corp_url'];
		$next_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=index&a=jz_contact_subscribe&free=1&com_id='.$com_info['id'].'&corpurl='.$com_info['corp_url'];
		
		g('smarty') -> assign('post_url', $post_url);
		g('smarty') -> assign('next_url', $next_url);
		g('smarty') -> assign('child_url', $child_url);
		g('smarty') -> assign('title', '注册页面');
		g('smarty') -> assign('invite_id', $invite_id);
		g('smarty') -> assign('com_name', $com_info['name']);
		g('smarty') -> assign('des', $invite_info['des']);
		g('smarty') -> assign('title', $invite_info['title']);
		g('smarty') -> assign('deadline', date('Y-m-d H:i:s', $invite_info['end_time']));

		//g('smarty') -> show($this -> temp_path.'inviteQrcode.html');
		g('smarty') -> show($this -> temp_path.'invitePerson.html');
	}

	/**
	 * 邀请单关注
	 */
	public function jz_contact_subscribe() {
		$com_id = get_var_value('com_id', false);
		$com_info = g('company') -> get_by_id($com_id);
		if (!$com_id || empty($com_info)) {
			cls_resp::show_err_page('找不到企业信息');
		}

		g('smarty') -> assign('title', '关注页面');
		g('smarty') -> assign('com_name', $com_info['name']);
		g('smarty') -> assign('qrcode', SYSTEM_HTTP_QY_DOMAIN . 'data/' . $com_info['corp_qrcode']);
		g('smarty') -> show($this -> temp_path.'inviteQrcode.html');

	}

	/**
	 * 邀请单图文消息
	 */
	public function jz_contact_image_text() {
        global $app;
		$invite_id = get_var_value('invite_id', false);
		$invite_info = g('invite_action') -> getInvitePageData($invite_id);
		if (!$invite_id || empty($invite_info)) {
			cls_resp::show_warn_page('找不到图文消息');
		}
		//获取公司信息
		$com_info = g('company') -> get_by_id($invite_info['com_id']);

		$get_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&a=image_text&corpurl='.$com_info['corp_url'];
		g('smarty') -> assign('title', '图文消息');
		g('smarty') -> assign('invite_id', $invite_id);
		g('smarty') -> assign('get_url', $get_url);

		g('smarty') -> show($this -> temp_path.'inviteText.html');
	}

//-----------------------------细节实现-----------------------------------------------------------	
	
	/** 添加常用联系人 */
	private function _add_general(){
		try {
			$sid = $_SESSION[SESSION_VISIT_USER_ID];
			log_write('开始添加常用联系人: user='.$sid);
			$need = array('id');
			$data = $this -> get_post_data($need);
			$gid = strval($data['id']);			//要增加的联系人
			
			$general_user = g('user_general') -> get_by_ids($sid);
			
			if(!empty($general_user)) {			//已有个人常用配置，更新
				$glist = json_decode($general_user[0]['general_list'], TRUE);
				!is_array($glist) && $glist = array();
				$glist = strval_array($glist);
				
				$dlist = json_decode($general_user[0]['del_list'], TRUE);
				!is_array($dlist) && $dlist = array();
				$dlist = strval_array($dlist);
				
				$glist[] = $gid;
				$glist = array_unique($glist);
				$dlist = array_diff($dlist, array($gid));
				g('user_general') -> update($general_user[0]['id'], $glist, $dlist);
				
			} else {							//未有个人常用配置，新增
				g('user_general') -> save($sid, array($gid), array());
			}
			log_write('添加常用联系人成功');
			cls_resp::echo_resp(cls_resp::$OK);
		
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}		
	}
	
	/** 删除常用联系人 */
	private function _del_general(){
		try {
			$need = array('id');
			$data = $this -> get_post_data($need);
			$gid = strval($data['id']);
			
			$sid = $_SESSION[SESSION_VISIT_USER_ID];
			log_write("员工开始移除常用联系人: user=".$sid);
			$general_user = g('user_general') -> get_by_ids($sid);
			
			if(!empty($general_user)){
				$glist = json_decode($general_user[0]['general_list'], TRUE);
				!is_array($glist) && $glist = array();
				$glist = strval_array($glist);
				
				$dlist = json_decode($general_user[0]['del_list'], TRUE);
				!is_array($dlist) && $dlist = array();
				$dlist = strval_array($dlist);
				
				$dlist[] = $gid;
				$dlist = array_unique($dlist);
				$glist = array_diff($glist, array($gid));
				g('user_general') -> update($general_user[0]['id'], $glist, $dlist);
				
			} else {
				$result = g('user_general') -> save($sid, array(), array($gid));
			}
			
			log_write("员工移除常用联系人成功");
			cls_resp::echo_resp(cls_resp::$OK);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 保存用户修改信息  */
	private  function self_save(){
		try {
			$uid = $_SESSION[SESSION_VISIT_USER_ID];
			log_write("员工开始修改个人信息: ".$uid);
			
			$req = $this -> get_post_data();
			
			$data = array();
			isset($req['weixin_id']) && $data['weixin_id'] = $req['weixin_id'];
			isset($req['tel']) && $data['tel'] = $req['tel'];
			isset($req['short_code']) && $data['short_code'] = $req['short_code'];
			isset($req['inner_code']) && $data['inner_code'] = $req['inner_code'];
			isset($req['work_position']) && $data['work_position'] = $req['work_position'];
			isset($req['mobile']) && $data['mobile'] = $req['mobile'];
			isset($req['email']) && $data['email'] = $req['email'];
			
			if (isset($data['weixin_id']) && empty($data['weixin_id'])) 			throw new SCException('微信号不能为空');
			if (isset($data['tel']) && !check_data($data['tel'], 'phone')) 			throw new SCException('电话格式错误');
			if (isset($data['work_position']) && empty($data['work_position'])) 	throw new SCException('工作地点不能为空');
			if (isset($data['mobile']) && !check_data($data['mobile'], 'mobile')) 	throw new SCException('手机号码格式错误');
			if (isset($data['email']) && !check_data($data['email'], 'email')) 		throw new SCException('邮箱格式错误');
			
			$condition =array(
				'id=' => $uid,
				'root_id=' => $_SESSION[SESSION_VISIT_DEPT_ID]
			);

			$user = g('user') -> get_by_id($uid);
			if(isset($data['weixin']) && $user['state'] == 1 && $user['weixin_id'] !== $req['weixin']) {
				throw new SCException('您已关注，不可以修改微信号');
			}
			
			!isset($data['tel']) && $data['tel'] = $user['tel']; 
			!isset($data['weixin_id']) && $data['weixin_id'] = $user['weixin_id']; 
			!isset($data['mobile']) && $data['mobile'] = $user['mobile']; 
			!isset($data['email']) && $data['email'] = $user['email']; 

			$data['acct'] = $user['acct']; 
			$data['name'] = $user['name']; 
			$data['position'] = $user['position']; 
			$data['gender'] = $user['gender'];
			$data['dept_list'] = $user['dept_list']; 
			g('user') -> update($condition, $data, false);
			
			log_write("员工修改个人信息成功");
			cls_resp::echo_resp(cls_resp::$OK);
		
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 用户详细信息
	 * @param unknown_type $id
	 */
	private  function get_self_detail(){
		$user = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID], '*');
    	$user['dept_list'] = json_decode($user['dept_list'], TRUE);
		$depts = g('api_dept') -> list_by_ids($user['dept_list'], 'id, name');
		$sotred_depts = array();
		foreach ($depts as $d) {
			$sotred_depts[$d['id']] = $d;
		}unset($d);
		unset($depts);
		
		$dept_str = '';
		foreach ($user['dept_list'] as $d) {
			isset($sotred_depts[$d]) && $dept_str .= ','.$sotred_depts[$d]['name'];
		}unset($d);
		$user['dept_str'] = empty($dept_str) ? '' : substr($dept_str, 1);
		g('smarty') -> assign('sre', $user);
		
		g('smarty') -> assign('user', $user);
		g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
		g('smarty') -> show($this -> temp_path.'index/self-detail.html');
	}
	
}

// end of file