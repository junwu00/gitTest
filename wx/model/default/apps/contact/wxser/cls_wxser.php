<?php
/**
 * 企业通讯录
 * 
 * @author yangpz
 * @date 2014-10-21
 *
 */
class cls_wxser extends abs_app_wxser {
	private static $PageSize = 20;
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『通讯录』操作指引',
		'desc' => '欢迎使用通讯录应用，在这里你可以随时随地查看同事的联系方式，一键拨打，省去寻找人员的烦恼，找找Ta在哪里吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970246&idx=3&sn=d869c8f7057003e6d89b5412e415bf0e#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	/** 搜索帮助 */
	protected function reply_click() {
		if ($this -> post_data['EventKey'] !== 'contact_search_helper') {
			return;
		}
		
		$conf = g('cont_conf') -> get_by_dept($this -> dept_id);
		$search_desc = '';
		if ($conf) {
			$search_list = json_decode($conf['search_list'], TRUE);
			in_array('name', $search_list) && $search_desc .= ',姓名';
			in_array('acct', $search_list) && $search_desc .= ',账号';
			in_array('position', $search_list) && $search_desc .= ',职位';
			in_array('wx_acct', $search_list) && $search_desc .= ',微信号';
			in_array('phone', $search_list) && $search_desc .= ',手机号';
			in_array('tel', $search_list) && $search_desc .= ',电话';
			$search_desc = '支持搜索项：'.substr($search_desc, 1);
		}
		$search_desc = "发送关键字可直接搜索哦！\n" . $search_desc;
		echo g('wxqy') -> get_text_reply($this -> post_data, $search_desc);
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
		
		$resp_text = '未找到相关的成员信息';
		$user_info = FALSE;
		//为数字，查找缓存是否有对应数据
		if (preg_match('/^[\d]+$/', $text)) {
			$user_info = $this -> _cache_search($text);
		}
		
		if (!empty($user_info)) {		//有缓存数据
			$resp_text = $this -> _get_single_user_resp($user_info);
			
		} else {						//无缓存数据
			$params = array(
				'keyword' => $text
			);
			
			$api_conf = array(
				'logined'		=> FALSE,
				'com_id'		=> $this -> com_id,
				'root_id'		=> $this -> dept_id,
				'struct_filter'	=> TRUE,
				'contact_filter'=> TRUE,
			);
			$api_user = g('api_user', $api_conf); 
			//部分方法实现需要依赖session，此处进行初始化
			!session_id() && session_start();
			$curr_user = $api_user -> get_by_acct($this -> post_data['FromUserName']);
			if (empty($curr_user)) {
				$resp_text = '找不到您的信息。可能是管理员未同步您的数据';
				
			} else {
		        $_SESSION[SESSION_VISIT_COM_ID] = $this -> com_id;
		        $_SESSION[SESSION_VISIT_DEPT_ID] = $this -> dept_id;
		        $_SESSION[SESSION_VISIT_USER_ID] = $curr_user['id'];
				
				$users = $api_user -> page_list_by_dept_ids(array($this -> dept_id), 1, self::$PageSize, $params, '*', '', TRUE);
				$total = $users['count'];
				$users = $users['data'];
				$this -> _append_user_dept_info($users, $api_conf);
				
				if ($total == 0) {			//未找到
					
				} else if ($total == 1) {	//只找到一个
					$resp_text = $this -> _get_single_user_resp($users[0]);
					
				} else {					//有多个
					$resp_text = $this -> _list_multi_user_resp($users, $total);
				}
			}
		}
		echo g('wxqy') -> get_text_reply($this -> post_data, $resp_text);
		
	}
	
	/** 回复单个成员的联系方式 */
	private function _get_single_user_resp(array $user) {
		$fields = array('name', 'acct', 'dept', 'position', 'wx_acct', 'phone', 'tel', 'email', 'short_code', 'inner_code');
		$conf = g('cont_conf') -> get_by_dept($this -> dept_id);
		$conf && $fields = json_decode($conf['show_list'], TRUE);
		empty($user['position']) && $user['position'] = '(暂无)';
		empty($user['weixin_id']) && $user['weixin_id'] = '(暂无)';
		empty($user['mobile']) && $user['mobile'] = '(暂无)';
		empty($user['tel']) && $user['tel'] = '(暂无)';
		empty($user['email']) && $user['email'] = '(暂无)';
		empty($user['short_code']) && $user['short_code'] = '(暂无)';
		empty($user['inner_code']) && $user['inner_code'] = '(暂无)';
		$text = "账号: {$user['acct']}\n";
		$text .= "姓名: {$user['name']}\n";
		$text .= "部门: {$user['dept_str']}\n";
		$text .= "职位: {$user['position']}\n";
		
		//TODO 仅为该企业开启
		if ($user['com_id'] == 11951) {
			$ext_attr = json_decode($user['ext_attr'], TRUE);
			if (is_array($ext_attr) && isset($ext_attr['attrs'])) {
				foreach ($ext_attr['attrs'] as $val) {
					$text .= "{$val['name']}: {$val['value']}\n";
				}
			}
		}
		//TODO END 仅为该企业开启
		
		in_array('wx_acct', $fields) && $text .= "微信号: {$user['weixin_id']}\n";
		in_array('phone', $fields) && $text .= "手机: {$user['mobile']}\n";
		in_array('tel', $fields) && $text .= "电话: {$user['tel']}\n";
		in_array('email', $fields) && $text .= "邮箱: {$user['email']}\n";
		in_array('short_code', $fields) && $text .= "短号: {$user['short_code']}\n";
		in_array('inner_code', $fields) && $text .= "内线号: {$user['inner_code']}\n";
		return $text;
	}
	
	/** 回复单个成员的联系方式 */
	private function _list_multi_user_resp(array $users, $count) {
		$redis = g('redis') -> get_handler();
		$cache_hash = $this -> _get_cache_hash();
		$text = '';
		if ($count > self::$PageSize) {
			$text = '结果过多，仅列出前'.self::$PageSize."个成员信息\n";
		}
		$text .= "回复序号查看对应成员信息\n";
		$text .= "30分钟内有效!\n";
		foreach ($users as $key => $u) {
			$num = $key + 1;
			$text .= "{$num}.[{$u['dept_str']}] {$u['name']}\n";
			
			//更新缓存
			try {
				$cache_key = $this -> _get_cache_key($num.':keyword');
				$redis -> hSet($cache_hash, $cache_key, json_encode($u));
			} catch (Exception $e) {}
		}unset($u);
		
		try {
			$redis -> expire($cache_hash, 1800);
		} catch (Exception $e) {}
		
		return $text;
	}
	
	/** 加载联系人部门信息 */
	private function _append_user_dept_info(array &$users, array $api_conf) {
		$dept_ids = array();
		foreach ($users as &$u) {
			$u['dept_list'] = json_decode($u['dept_list'], TRUE);
			foreach ($u['dept_list'] as $d) {
				$dept_ids[] = $d;
			}unset($d);
		}
		$depts = g('api_dept', $api_conf) -> list_by_ids($dept_ids, 'id, name');
		$sotred_depts = array();
		foreach ($depts as $d) {
			$sotred_depts[$d['id']] = $d;
		}unset($d);
		unset($depts);
		
		foreach ($users as &$u) {
			$dept_str = '';
			foreach ($u['dept_list'] as $d) {
				isset($sotred_depts[$d]) && $dept_str .= ','.$sotred_depts[$d]['name'];
			}unset($d);
			$u['dept_str'] = empty($dept_str) ? '' : substr($dept_str, 1);
		}
	}
	
	/** 验证接收的序号是否有对应的数据 */
	private function _cache_search($text) {
		$cache_hash = $this -> _get_cache_hash();
		$redis = g('redis') -> get_handler();
		$cache_key = $this -> _get_cache_key($text.':keyword');

		try {
			$user_info = $redis -> hGet($cache_hash, $cache_key);
			if (!empty($user_info)) {
				return json_decode($user_info, TRUE);
			}
			
		} catch(Exception $e) {}
		return FALSE;
	}
	
	/** 获取缓存数组的hash */
	private function _get_cache_hash() {
		return $this -> _get_cache_key(__FUNCTION__.':hash');
	}
	
	/** 获取缓存KEY */
	private function _get_cache_key($str) {
		$user = $this -> post_data['FromUserName'];
		$key = __FUNCTION__ . ':com:' . $this -> com_id . ':user:' . $user . ':str:' . $str;		//各成员权限不同，所以成员间查询结果各自缓存
		return md5($key);
	}
	
}

// end of file