var c_search_cache = {};			//员工列表缓存
var c_search_cache_user = {};		//员工详情缓存
var c_search_cache_search = {};		//员工搜索缓存
var c_search_data = {};				//当前选中的内容
var c_search_count = 0;				//当前选中的个数
var c_search_offset = 0;			//当前查找位置
var c_search_limit = 50;			//每次查找数量
var c_search_form;
var c_search_curr_opt = {};			//当前选择器的配置
var c_search_dept_icon = 'static/image/icon-dept.png';
var c_search_bottom_bar_height = 0;	//底部菜单高度，用于自适应高度时计算

//监听url变更
window.addEventListener('popstate', function(e) {
	var info = e.state;
    if (info) {
    	var pre_page = '';
    	if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//成员详情页面
    		pre_page = 'detail';
		} else if ($('#c-search-user').css('left') == '0px') {													//成员列表页面
			pre_page = 'user';
		} else {																								//部门列表页面
			pre_page = 'dept';
		}
    	
    	switch(info.target) {
	    	case 'user':
	    		if (pre_page == 'detail') {
	    			$('#c-search-back').click();
	    		}
	    		break;
	    		
	    	case 'dept':
    			$('#c-search-back').click();
	    		break;
    	}
        // ...
    }else{
        // 页面初始化的时候state为null，需要做的处理    
		$('#c-search-back').click();
    }
    // 当然也可以直接使用history.state来获取当前对应的state数据。
});

$(document).ready(function(){
	var win_height = $(window).height();
	$('#c-search-container').css({'height': win_height - c_search_bottom_bar_height});
	
	try {c_search_conf_search_list = $.parseJSON(c_search_conf_search_list);} 	catch (e) {c_search_conf_search_list = [];}		//可搜索的字段
	try {c_search_conf_show_list = $.parseJSON(c_search_conf_show_list);} 		catch (e) {c_search_conf_show_list = [];}		//可显示的字段
	try {c_search_conf_general_list = $.parseJSON(c_search_conf_general_list);} catch (e) {c_search_conf_general_list = [];}	//通用常用联系人
	try {c_search_conf_secret_list = $.parseJSON(c_search_conf_secret_list);} 	catch (e) {c_search_conf_secret_list = [];}		//保存联系人
	
	c_search_init_toggle();
	c_search_init_tab_toggle();
	c_search_init_py_position();
	c_search_init_selected_toggle();
	$('#c-search-title').bind('focus', function() {
		$('#c-search-selected').addClass('hide');
	});
});

//显示/隐藏选人控件
function c_search_init_toggle(opt) {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

//点击确定的触发事件、初始化数据
function c_search_init(opt) {
	c_search_curr_opt = opt;
	//预处理事件
	if (typeof(opt.before) === 'function') {
		opt.before();
	}
	//初始化数据
	$('.icheck-cb').each(function() {
		$(this)[0].checked = false;
	});
	$('.icheck-dp').each(function() {
		$(this)[0].checked = false;
	});
	c_search_data = opt.selected || {};
	c_search_count = 0;
	var users = [];	//已选中的员工
	var depts = [];	//已选中的部门
	for (var i in c_search_data) {
		if (c_search_data[i]) {
			if (i.length > 2 && i.substr(0, 2) == 'd-') {
				depts.push(i);
				
			} else {
				users.push(i);
			}
		}
	}
	
	//清空已选	
	$('#c-search-selected').html('');
	$('#c-search-selected-count').html(0);
	//初始化选中员工
	if (users.length > 0) {
		frame_obj.post({
			url		: c_search_init_url,
			data	: {user: users},
			success	: function(resp) {
				if (resp.errcode == 0) {
					var info = resp.info;
					for (var i in info) {
						if (!info[i])	continue;
						
						c_search_data[info[i].id].pic = info[i].pic_url;
						var target = $('.icheck-cb[data-id=' + info[i].id + ']');
						if (target.length > 0) {
							target[0].checked = true;
						}
						c_search_insert_selected(info[i].id, info[i].name, info[i].pic_url);
					}
					
				} else {
					frame_obj.alert(resp.errmsg);
				}
			}
		});
	}
	//初始化选中部门
	if (depts.length > 0) {
		var dept_ids = [];
		for (var i in depts) {
			dept_ids.push(depts[i].substr(2));
		}
		
		frame_obj.post({
			url		: c_search_init_url,
			ajax_key: c_search_init_url + '#' + new Date().getTime(),
			data	: JSON.stringify({user: [], dept: dept_ids}),
			success	: function(resp) {
				if (resp.errcode == 0) {
					var info = resp.info;
					for (var i in info) {
						if (!info[i])	continue;

						var target = $('.icheck-dp[data-id=d-' + info[i].id + ']');
						if (target.length > 0) {
							target[0].checked = true;
						}
						c_search_insert_selected(info[i].id, info[i].name, c_search_dept_icon, 1);
					}
					
				} else {
					frame_obj.alert(resp.errmsg);
				}
			}
		});
	}
	
	var type = opt.type ? Number(opt.type) : 0;
	c_search_curr_opt.type = type;
	var win_height = $(window).height();
	switch (type) {
		case 0:		//部门+成员
			$('.icheck-dp').removeClass('hide');
			$('.c-search-tree .c-search-list').removeClass('hide');
			$('#c-search-head').removeClass('hide');
			$('#c-search-body').css({'margin-top': 50});
			$('#c-search-container').css({'height': win_height - c_search_bottom_bar_height});
			break;
		case 1:		//仅部门
			$('.icheck-dp').removeClass('hide');
			$('#c-search-head').addClass('hide');
			$('.c-search-tree .c-search-list').addClass('hide');
			$('#c-search-body').css({'margin-top': 0});
			
			$('#c-search-dept').removeClass('hide');
			$('#c-search-py').addClass('hide');
			$('#c-search-user').css({left: '100%'});
			$('#c-search-title').attr('data-dept', '0');
			$('#c-search-user').html('');
			$('#c-search-back').addClass('hide');
			$('#c-search-container').css({'height': win_height});		
			break;
		case 2:		//仅成员
			$('.icheck-dp').addClass('hide');
			$('.c-search-tree .c-search-list').removeClass('hide');
			$('#c-search-head').removeClass('hide');
			$('#c-search-body').css({'margin-top': 50});
			$('#c-search-container').css({'height': win_height - c_search_bottom_bar_height});
			break;
	}
	
	//确认事件
	$('#c-search-ok').unbind('click').bind('click', function() {
		$('#c-search-container').css({'display': 'none'});
		if (typeof(opt.ok) === 'function') {
			var ret = c_search_data;
			var split = opt.split || false;
			if (split) {
				var users = {};	//已选中的员工
				var depts = {};	//已选中的部门
				for (var i in ret) {
					if (ret[i]) {
						if (i.length > 2 && i.substr(0, 2) == 'd-') {
							depts[i.substr(2)] = {id: i.substr(2), name: ret[i].name, pic: ret[i].pic};
							
						} else {
							users[i] = {id: i, name: ret[i].name, pic: ret[i].pic};
						}
					}
				}
				ret = {depts: depts, users: users};
			}
			
			opt.ok(ret);
		}
	});
}

//隐藏/显示已选成员
function c_search_init_selected_toggle() {
	$('#c-search-show-selected').unbind('click').bind('click', function() {
		if ($('#c-search-selected').hasClass('hide')) {
			if (c_search_count == 0) {
				frame_obj.tips('当前没有已选择的成员');
				
			} else {
				$('#c-search-selected').removeClass('hide');
			}
			
		} else {
			$('#c-search-selected').addClass('hide');
		}
	});
}

//初始化部门选择事件
function c_search_on_dept_check(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var dept = $(that).attr('data-id');
	var dept_name = $(that).attr('data-name');
	var checked = $(that)[0].checked;
	if (checked) {	//选择部门
		c_search_insert_selected(dept, dept_name, c_search_dept_icon, 1);
		c_search_data[dept] = {id: dept, name: dept_name, pic: c_search_dept_icon};
		
	} else {		//取消选择部门
		$('#c-search-selected img[data-id=' + dept + ']').parent().remove();
		delete(c_search_data[dept]);
		
		$('#c-search-selected-count').html(--c_search_count);
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
	}
}

//加载子部门
function c_search_append_child_dept(d_id, target_li) {
	window.c_search_loading_dept = window.c_search_loading_dept || [];
	if ($.inArray(d_id, window.c_search_loading_dept) !== -1) {	//正在获取中，不再获取
		return;
	}
	window.c_search_loading_dept.push(d_id);
	
	var ul = '<ul class="c-search-tree"><li style="height:48px; line-height: 48px; border-bottom: 1px solid #e7e7e7; color: #9b9b9b; padding-left: 15px;">加载中...</li></ul>';
	$(target_li).append(ul);
	$(target_li).children('ul').slideDown('normal', function() {
		$(target_li).addClass('c-search-down');
		$(target_li).removeClass('c-search-up');
	});
	
	var post_opt = {
		url			: c_search_dept_url,
		data		: {d_id: d_id},
		success		: function(resp) {
			if (resp.errcode == 0) {
				var data = resp.info;
				var html = '';
				var ids = [];
				for (var i in data) {
					var c = data[i];
					ids.push('d-' + c.id);							//记录部门ID，加载后可能需要直接勾选（初始数据中有该部门）
					var cls = c.has_child ? 'c-search-up' : '';
					html += '<li class="' + cls + '" data-id="' + c.id + '">' +
			                '<span onclick="c_search_on_dept_click(this, event);">' +
			                	'<span>' +
			                		'<input type="checkbox" class="icheck-dp ck2" data-id="d-' + c.id + '" data-name="' + c.name + '" onclick="c_search_on_dept_check(this, event);"> ' + c.name +
			                	'</span>' +
			                '</span>' +
			                '<i class="c-search-list" data-id="' + c.id + '" data-name="' + c.name + '"  onclick="c_search_on_dept_ricon_click(this, event);"> ' +
			                	'<span class="icon-angle-right"></span> ' +
			                '</i>' +
						'</li>';
				}
				$(target_li).children('ul').html(html);
				
				for (var i in ids) {
					if (c_search_data[ids[i]]) {
						$('.icheck-dp[data-id=' + ids[i] + ']')[0].checked = true;
					}
				}

				switch (c_search_curr_opt.type) {
					case 0: //部门+成员
						$('.icheck-dp').removeClass('hide');
						break;
					case 1: //仅部门
						$('.icheck-dp').removeClass('hide');
						break;
					case 2: //仅成员
						$('.icheck-dp').addClass('hide');
						break;
				}
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		}
	}
	frame_obj.post(post_opt);
}

//部门点击事件（收缩/展开/叶子部门进入部门成员查看）
function c_search_on_dept_click(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var target_li = $(that).parent();
	if ($(target_li).hasClass('c-search-up')) {				//收缩  => 展开 
		if ($(target_li).children('ul').length == 0) {	//未加载子部门，进行加载
			c_search_append_child_dept($(target_li).attr('data-id'), $(target_li));
			
		} else {
			$(target_li).children('ul').slideDown('normal', function() {
				$(target_li).addClass('c-search-down');
				$(target_li).removeClass('c-search-up');
			});
		}
		
	} else if ($(target_li).hasClass('c-search-down')) {	//展开 => 收缩  
		$(target_li).children('ul').slideUp('normal', function() {
			$(target_li).addClass('c-search-up');
			$(target_li).removeClass('c-search-down');
		});
		
	} else {												//叶子节点
		if (c_search_curr_opt.type && Number(c_search_curr_opt.type) == 1)	return;	//仅部门，不可查看成员
		if ($(target_li).find('.c-search-list').length == 0)	return;	//可能为虚拟部门，不可查看
		
		$(target_li).find('.c-search-list').click();
	}
}

//点击部门列表右侧icon进入成员列表
function c_search_on_dept_ricon_click(that, evt) {
	if (evt) {
		evt.stopPropagation();	
	}
	
	if (c_search_curr_opt.type && Number(c_search_curr_opt.type) == 1)	return;	//仅部门，不可查看成员
	
	var dept = $(that).attr('data-id');
	var dept_name = $(that).attr('data-name');
	
	c_search_dept_select_evt($(that), dept, dept_name, 'dept');
	$('#c-search-back').removeClass('hide');
	if (dept.substr(0, 2) == 'd-') {
		dept = dept.substr(2);
	}
	history.pushState({from: 'dept', target: 'user', handler: dept}, "通讯录_成员信息");
}

//初始化部门、员工、员工详情切换
function c_search_init_tab_toggle() {
	$('#c-search-back').unbind('click').bind('click', function() {
		$('#c-search-head').removeClass('hide');
		$('#c-search-selected').addClass('hide');
		if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				$('#c-search-user-detail').html('');
				$('#c-search-back').removeClass('hide');
			});
			
		} else if ($('#c-search-user').css('left') == '0px') {	//当前在员工选择页面，则返回部门列表页面
			$('#c-search-dept').removeClass('hide');
			$('#c-search-py').addClass('hide');
			$('#c-search-user').animate({left: '100%'}, 'normal', function() {
				$('#c-search-title').attr('data-dept', '0');
				$('#c-search-user').html('');
				$('#c-search-back').addClass('hide');
			});

		} else {												//当前在部门列表页面，则返回上一层
//			$('#c-search-py').addClass('hide');
//			$(c_search_form).css({display: 'block'});
//			$('#c-search-container').css({display: 'none'});
		}
	});
}

//选择部门（显示员工列表）事件
function c_search_dept_select_evt(btn, dept, dept_name, from) {		//from 来源：部门列表的选择 / 员工详情页面的选择
	$('#c-search-selected').addClass('hide');
	$('#c-search-title').attr('data-dept', dept);
	$('#c-search-title').attr('data-dname', dept_name);
	
	function show_cache() {
		var data = [];
		for (var i in c_search_cache[dept]) {
			if (!c_search_cache[dept][i])	continue;
			if (i == 'offset') {
				c_search_offset = c_search_cache[dept][i];	//历史查找位置
				continue;
			}
			if (i == 'complete') {
				$('#c-search-py').removeClass('hide');
				continue;
			}
			
			data = data.concat(c_search_cache[dept][i]);
		}
		$('#c-search-user').html('');
		c_search_build_user_list(data);
	}

	$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	if (c_search_cache[dept]) {								//有本地缓存，取缓存数据
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'normal', function() {
				$('#c-search-dept').addClass('hide');
				show_cache();
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				show_cache();
			});
		}
		
	} else {
		c_search_offset = 0;	//重置查找位置
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'normal', function() {
				$('#c-search-dept').addClass('hide');
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
		}
	}
	//滚动加载
	c_search_scroll_load(function() {
		c_search_list_user(undefined, dept, c_search_offset, c_search_limit);
	});
}

//查找员工
function c_search_on_search() {
	var empty_reg = /^[\S]+$/;
	var name = $('#c-search-title').val();
	if (!empty_reg.test(name)) {							//当前内容为空
		if ($('#c-search-title').attr('data-pre') == '') {	//上次内容也为空，不处理
			return;
		}													//上次内容不为空，处理
	}

	$('#c-search-title').attr('data-pre', $.trim(name));
	$('#c-search-user').html('');
	if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
		$('#c-search-user').removeClass('hide');
		$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
			$('#c-search-user-detail').html('');
		});
		history.pushState({from: 'dept', target: 'user', handler: dept}, "通讯录_成员信息");
		
	} else if ($('#c-search-user').css('left') != '0px') {	//当前在部门列表页面，则跳转员工列表页面
		$('#c-search-user').animate({left: '0'}, 'normal');
		$('#c-search-back').removeClass('hide');
		history.pushState({from: 'dept', target: 'user', handler: dept}, "通讯录_成员信息");
	}
	
	var dept = $('#c-search-title').attr('data-dept');
	if (c_search_cache_search[dept] && c_search_cache_search[dept][name]) {	//有缓存
		var data = [];
		var source = c_search_cache_search[dept][name];
		for (var i in source) {
			if (!source[i])	continue;
			if (i == 'offset') {
				c_search_offset = source[i];	//历史查找位置
				continue;
			}
			if (i == 'complete') {
				$('#c-search-py').removeClass('hide');
				continue;
			}
			
			data = data.concat(source[i]);
		}
		c_search_build_user_list(data);
		
	} else {
		c_search_offset = 0;	//重置查找位置
		$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
		c_search_list_user($(this), dept, c_search_offset, c_search_limit, name);
	}

	var dept_name = $('.c-search-list[data-id=' + dept + ']').attr('data-name');
	//滚动加载
	c_search_scroll_load(function() {
		c_search_list_user(undefined, dept, c_search_offset, c_search_limit, name);
	});
}

//获取员工数据
function c_search_list_user(btn, dept_id, offset, limit, name) {
	var empty_reg = /^[\S]+$/;
	var data = new Object();
	data.dept = String(dept_id);
	data.offset = offset || c_search_offset;
	data.limit = limit || c_search_limit;
	data.name = (empty_reg.test(name)) ? $.trim(name) : '';
	
	frame_obj.post({
		url		: c_search_url,
		data	: data,
		btn		: btn,
		success	: function(resp) {
			if (resp.errcode == 0) {
				var info = resp.info;
				if (info.count == 0) {
					if (data.name == '') {
						$('#c-search-user').html('<p class="c-search-empty"><img src="' + c_search_list_empty_icon + '"><br>当前部门暂无成员</p>');
					} else {
						$('#c-search-user').html('<p class="c-search-empty"><img src="' + c_search_list_empty_icon + '"><br>找不到匹配的成员</p>');
					}

					if ($('#c-search-user #c-search-user-dept').length == 0) {
						$('#c-search-user').prepend('<div id="c-search-user-dept"></div>');
					}
					$('#c-search-user-dept').html($('#c-search-title').attr('data-dname'));
					return;
				}
				
				var info = info.data;
				c_search_scroll_load_complete(info.length);
				c_search_offset += info.length;
				
				//缓存数据
				if (info.length != 0 && data.name == '') {						//部门所有员工
					c_search_cache[dept_id] = c_search_cache[dept_id] || {};
					c_search_cache[dept_id][offset] = info;
					c_search_cache[dept_id]['offset'] = c_search_offset;
					c_search_build_user_list(info);
					
				} else if (info.length != 0 && data.name != '') {				//查找员工
					c_search_cache_search[dept_id] = c_search_cache_search[dept_id] || {};
					c_search_cache_search[dept_id][data.name] = c_search_cache_search[dept_id][data.name] || {};
					c_search_cache_search[dept_id][data.name][offset] = info;
					c_search_cache_search[dept_id][data.name]['offset'] = c_search_offset;
					c_search_build_user_list(info);
				}

				//标识为完成加载
				if (offset == 0 && resp.info.count <= resp.info.data.length) {	//只一页，显示右侧拼音搜索
					$('#c-search-py').removeClass('hide');
					if (data.name == '') {
						c_search_cache[dept_id]['complete'] = 1;
						
					} else if (data.name != '') {
						c_search_cache_search[dept_id][data.name]['complete'] = 1;
					}

				} else {
					$('#c-search-py').addClass('hide');
				}

				if (resp.info.data.length == 0 && data.name == '') {
					c_search_cache[dept_id]['complete'] = 1;
					$('#c-search-py').removeClass('hide');
					
				} else if (resp.info.data.length == 0 && data.name != '') {
					c_search_cache_search[dept_id][data.name]['complete'] = 1;
					$('#c-search-py').removeClass('hide');
				}
				
			} else {
				frame_obj.tips('加载员工数据失败');
			}
		}
	});
}

//生成用户选择列表
function c_search_build_user_list(info) {
	var html = '';
	var py = '';
	var py_list = 'abcdefghijklmnopqrstuvwxyz';
	var py_list_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	for (var i in info) {
		if (!info[i])			continue;
		if (i == 'offset')		continue;
		if (i == 'complete')	continue;
		
		var item = info[i];
		if ($.inArray(Number(item.id), c_search_conf_secret_list) !== -1) continue;
		
		var f_py = item.first_py.substr(0, 1);
		var py_idx = py_list.indexOf(f_py);
		if (item.boss) {
			f_py = '第一负责人';
		} else if (item.second_boss) {
			f_py = '第二负责人';
		} else if (py_idx == -1) {
			f_py = '#';
		} else {
			f_py = py_list_up.substr(py_idx, 1);
		}
		if ($('#c-search-user .c-search-py-tab[data-py=' + f_py + ']').length == 0) {//未加载过该拼音开头的数据
			$('#c-search-user').append(html);
			$('#c-search-user').append('<p class="c-search-py-tab" data-py=' + f_py + '>' + f_py + '</p>');
			html = '';
		}
		html += '<p class="c-search-py-item" data-id="' + item.id + '" onclick="c_search_show_detail(this, event);">' + 
			'<input type="checkbox" class="icheck-cb ck2" data-id="' + item.id + '" data-name="' + item.name + '">' +
			'<img class="c-search-user-pic" data-id="' + item.id + '" src="' + item.pic_url + '" onerror="this.src=\'static/image/face.png\'" onclick="c_search_show_detail(this, event);">' +
			'<span class="c-search-user-name" data-id="' + item.id + '" onclick="c_search_show_detail(this, event);">' + item.name + '</span>' +
		'</p>';
	}
	$('#c-search-user').append(html);
	//显示当前进入的部门
	if ($('#c-search-user #c-search-user-dept').length == 0) {
		$('#c-search-user').prepend('<div id="c-search-user-dept"></div>');
	}
	$('#c-search-user-dept').html($('#c-search-title').attr('data-dname'));
	
	c_search_init_selected();
}

//初始化员工详情页面展示
function c_search_show_detail(that, evt) {
	evt.stopPropagation();

	$('#c-search-head').addClass('hide');
	$('#c-search-selected').addClass('hide');
	var that = $(that);
	var user = $(that).attr('data-id');
	$('#c-search-user-detail').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	$('#c-search-user-detail').animate({left: '0'}, 'normal', function() {
		$('#c-search-user').addClass('hide');
		c_search_load_user_detail(that, user);
	});

	history.pushState({from: 'user', target: 'detail'}, "通讯录_部门列表");
}

//加载员工详情信息
function c_search_load_user_detail(btn, user) {
	var use_detail_cache = false;
	if (use_detail_cache && c_search_cache_user[user]) {
		c_search_build_user_detail(c_search_cache_user[user]);
		
	} else {
		var map = {'wx_acct': 'weixin_id', 'phone': 'mobile', 'tel': 'tel', 'email': 'email'};
		var ext_attr = [];
		for (var i in c_search_conf_show_list) {
			if (map[c_search_conf_show_list[i]]) ext_attr.push(map[c_search_conf_show_list[i]]);
		}
		frame_obj.post({
			url		: c_search_detail_url,
			data	: {user: user, ext_attr: ext_attr, chk_general: 1},
			success	: function(resp) {
				if (resp.errcode == 0) {
					c_search_cache_user[user] = resp.info;
					c_search_build_user_detail(resp.info);
					
				} else {
					frame_obj.tips('加载员工详情信息失败');
				}
			}
		});
	}
}

//生成员工详情页面
function c_search_build_user_detail(data) {
	var depts = '<span>';
	for (var i in data.dept_list) {
		if (!data.dept_list[i])		continue;
		
		var dept = data.dept_list[i];
		var dept_name = dept.name;
		dept = '<span class="c-search-detail-dept" data-id="' + dept.id + '" data-name="' + dept_name + '">' + dept_name + '</span>';
		depts += dept;
	}
	depts += '</span>';
	
	var gender_icon = '';
	if (data.gender == 1) {
		gender_icon = '<img src="' + c_search_detail_male_icon + '" class="c-search-detail-gender">';
	} else if (data.gender == 2) {
		gender_icon = '<img src="' + c_search_detail_famale_icon + '" class="c-search-detail-gender">';
	}
	var set_general = '';
	var show_general_flag = 'hide';
	if (c_search_op_user_id != data.id) {
		if (data.general) {
			set_general = '<span class="c-search-detail-set-general" data-state="1" data-id="' + data.id + '" onclick="c_search_set_general(this);">移除常用联系人</span>';
			show_general_flag = '';
		} else {
			set_general = '<span class="c-search-detail-set-general" data-state="0" data-id="' + data.id + '" onclick="c_search_set_general(this);">加为常用联系人</span>';
		}
	}
	
	//自定义信息
	var ext_attr = [];
	var ext_attr_html = '';
	try {
		if (data.ext_attr != '') {
			data.ext_attr = $.parseJSON(data.ext_attr);
			ext_attr = data.ext_attr.attrs;
		}
		for (var i in ext_attr) {
			ext_attr_html += '<p class="c-search-detail-line">' +
				'<label>' + ext_attr[i].name + '</label>' + ext_attr[i].value +
			'</p>';
		}
	} catch(e) {}
	
	var html = '<div>' +
		'<p id="c-search-detail-banner">' +
			'<img src="' + data.pic_url + '" onerror="this.src=\'static/image/face.png\'" class="c-search-detail-pic">' +
			'<span>' +
				data.name + gender_icon +
			'</span>' +
		'</p>' +
		'<p id="c-search-detail-title">' +
			'<span>' +
				'<span id="c-search-detail-name">' + data.name + '<span class="' + show_general_flag + '">常用联系人</span></span>' +
				'<span id="c-search-detail-depts">' + depts + '</span>' +
				'<span id="c-search-detail-position">' + data.position + '</span>' +
			'</span>' +
			set_general +
			'<span class="c-search-detail-set-general-wait hide"><i><b></b></i> 数据提交中...</span>' + 
		'</p>' +
		'<p class="c-search-detail-line c-search-detail-line-first">' +
			'<label>微信号</label>' + (data.weixin_id === undefined ? '******' : data.weixin_id || '<font color="#9b9b9b">(暂无)</font>') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>手机号码</label>' + (data.mobile === undefined ? '******' : data.mobile || '<font color="#9b9b9b">(暂无)</font>') +
			(data.mobile ? '<a href="tel:' + data.mobile + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>工作电话</label>' + (data.tel === undefined ? '******' : data.tel || '<font color="#9b9b9b">(暂无)</font>') +
			(data.tel ? '<a href="tel:' + data.tel + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		// mrc add st
		'<p class="c-search-detail-line">' +
			'<label>短号</label>' + (data.short_code === undefined ? '******' : data.short_code || '<font color="#9b9b9b">(暂无)</font>') +
			(data.short_code ? '<a href="tel:' + data.short_code + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>内线号</label>' + (data.inner_code === undefined ? '******' : data.inner_code || '<font color="#9b9b9b">(暂无)</font>') +
			(data.inner_code ? '<a href="tel:' + data.inner_code + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		// mrc add end
		'<p class="c-search-detail-line">' +
			'<label>邮箱</label>' + (data.email === undefined ? '******' : data.email || '<font color="#9b9b9b">(暂无)</font>') +
		'</p>' +
		ext_attr_html +
		'<button type="button" class="btn btn-success c-search-send-msg" data-acct="' + data.acct + '" onclick="c_search_send_msg(this);">发消息</button>' +
	'</div>';
	$('#c-search-user-detail').html(html);
//	c_search_detail_user_list();
}

/*由员工详情页面的部门列表点击部门，进入部门员工列表页面
function c_search_detail_user_list() {
	$('.c-search-detail-dept').each(function() {
		$(this).unbind('click').bind('click', function() {
			c_search_dept_select_evt($(this), $(this).attr('data-id'), $(this).attr('data-name'), 'detail');
		});
	});
}
*/

//初始化选中的员工
function c_search_init_selected() {
	for (var i in c_search_data) {
		if (!c_search_data[i])								continue;
		if ($('.icheck-cb[data-id=' + i + ']').length == 0)	continue;
		
		$('.icheck-cb[data-id=' + i + ']')[0].checked = true;
	}
}

//更新人员选择事件
function c_search_on_user_check(that, evt) {
	if (evt) {
		evt.stopPropagation();
	}
	
	var target = ''; 
	if ($(that).hasClass('c-search-py-item')) {	//非点了checkbox，变更选中状态
		target = $(that).children('.icheck-cb');
		var checked = $(target)[0].checked ? true : false;
		$(target)[0].checked = !checked;
		
	} else {									//点了checkbox，不变更选中状态
		target = $(that);
	}
	var checked = $(target)[0].checked;
	
	if (checked) {		//选中
		var user = $(target).attr('data-id');
		var pic = $(target).parents('.c-search-py-item').children('img').attr('src');
		var name = $(target).attr('data-name');
		c_search_data[$(target).attr('data-id')] = {id: user, name: name, pic: pic};
		c_search_insert_selected(user, name, pic);
		
	} else {			//非选中
		var user = $(target).attr('data-id');
		$('#c-search-selected img[data-id=' + user + ']').parent().remove();
		delete(c_search_data[user]);
		
		$('#c-search-selected-count').html(--c_search_count);
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
	}
}

//选择列表中插入新选择成员/部门
function c_search_insert_selected(id, name, pic, is_dept) {
	is_dept = typeof(is_dept) === undefined ? 0 : is_dept;
	if (is_dept ==1 && id.substr(0, 2) != 'd-')		id = 'd-' + id;
	
	var item = '<span class="c-search-selected-item">' + 
		'<img src="' + pic + '" data-id="' + id + '" onclick="c_search_delete_evt(this, ' + is_dept + ')" onerror="this.src=\'static/image/face.png\'">' + 
		'<span class="c-search-selected-name">' + name + '</span>' + 
	'</span>';
	$('#c-search-selected').append(item);
	$('#c-search-selected-count').html(++c_search_count);
}

//更新删除部门/员工事件
function c_search_delete_evt(that, is_dept) {
	var id = $(that).attr('data-id');
	$('#c-search-selected img[data-id=' + id + ']').parent().remove();
	delete(c_search_data[id]);
	$('#c-search-selected-count').html(--c_search_count);
	if (c_search_count == 0) {
		$('#c-search-selected').addClass('hide');
	}
	
	if (is_dept == 1) {												//删除部门
		var target = $('.icheck-dp[data-id=' + id + ']');
		if (target.length > 0) {			//选中部门在当前列表中
			target[0].checked = false;		
		}
		
	} else {														//删除成员
		var target = $('.icheck-cb[data-id=' + id + ']');
		if (target.length > 0) {			//选中成员在当前列表中
			target[0].checked = false;
		}
	}
}

//滚动加载
var c_search_curr_scroll_top = 0;
var c_search_is_scroll_end = false;
var c_search_is_scroll_loading = false;
function c_search_scroll_load(callback) {
	var $container = $('#c-search-user');
	var sTimer;
	
	$container[0].scrollTop = c_search_curr_scroll_top;
	$container.unbind('scroll').scroll(function scrollHandler(){
	    clearTimeout(sTimer);
	    sTimer = setTimeout(function() {
	    	var c = $container[0].scrollHeight;		//总高度
	    	var t = $container.scrollTop();			//滚动高度
	    	var h = $container[0].clientHeight;
	    	c_search_curr_scroll_top = t;

	        if(window.loaded == 1){$(window).unbind("scroll", scrollHandler);}
	        if(t + h + 100 > c && !c_search_is_scroll_end && !c_search_is_scroll_loading){
	        	if ($('#c-search-user').children('#c_search_loading').length == 0) {
	        		$('#c-search-user').append('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	        	}
	        	if (typeof(callback) === 'function') {
	        		callback();
	        	};
	        }
	    }, 100);
	});
}

//加载完成回调事件
function c_search_scroll_load_complete(load_length) {
	c_search_is_scroll_loading = false;
	if (load_length == 0) {
		c_search_is_scroll_end = true;
		
	} else {
		c_search_is_scroll_end = false;
	}
	$('#c-search-user').children('#c_search_loading').remove();
}

//初始化拼音定位
function c_search_init_py_position() {
	$('#c-search-py > span').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.py-big').addClass('hide');
			$(this).children('.py-big').removeClass('hide');
		});
	});
	$('.py-big > span').each(function() {
		$(this).unbind('click').bind('click', function(evt) {
			$(this).parent().addClass('hide');
			var py = $(this).html();
			
			var target = $('.c-search-py-tab[data-py=' + py + ']');
			if (target.length == 0) {
				frame_obj.tips('无字母' + py + '开头的成员');
				
			} else {
				$('#c-search-user').animate({scrollTop: target[0].offsetTop});
			}
			evt.stopPropagation();
		});
	});
}

//加为/移除常用联系人
function c_search_set_general(that) {
	var is_general = $(that).attr('data-state');
	var id = $(that).attr('data-id');
	var target_url = '';
	if (is_general == 0) {	//添加为常用
		target_url = add_general_url;
	} else {				//移除常用
		target_url = del_general_url;
	}
	$('.c-search-detail-set-general-wait').removeClass('hide');
	$(that).addClass('hide');
	frame_obj.post({
		url		: target_url,
		data	: {id: id},
		success	: function(resp) {
			$('.c-search-detail-set-general-wait').addClass('hide');
			$(that).removeClass('hide');
			if (resp.errcode == 0) {
				if (is_general == 0) {	//修改后为常用
					$(that).attr('data-state', 1);
					$(that).html('移除常用联系人');
					$('#c-search-detail-name span').removeClass('hide');
					
				} else {
					$(that).attr('data-state', 0);
					$(that).html('添加常用联系人');
					$('#c-search-detail-name span').addClass('hide');
				}
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		}
	});
}

//建立会话
function c_search_send_msg(that){
	var msg = '未知错误';
	var acct = $(that).attr('data-acct');

	if (acct == undefined || acct == null || !acct) {
		acct = '';
	}

	acct = acct.replace(/(^\s*)|(\s*$)/g, "");
	if (acct == '') {
		msg = '无效的参数';
		frame_obj.tips(msg);
		return false;
	}

	wx.openEnterpriseChat({
		userIds: acct, 
		groupName: '',
		success: function(res){
		},
	    fail: function(res){
			if(res.errMsg.indexOf('function not exist') > 0){
                frame_obj.tips('微信版本过低，请升级后重试');
            } else {
            	switch (res.errCode) {
            		case '10001': msg = 'appid无效';				break;
            		case '10002': msg = '用户未关注企业号';		break;
            		case '10003': msg = '消息服务未开启';			break;
            		case '10004': msg = '用户不在消息服务可见范围';	break;
            		case '10005': msg = '存在无效的消息会话成员';	break;
            		case '10006': msg = '消息会话成员数不合法';		break;
            	}
                frame_obj.tips(msg);
            }
		}
	});
}