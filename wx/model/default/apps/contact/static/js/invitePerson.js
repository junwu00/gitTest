/**
 * Created by hdh on 2017/3/2.
 */
var next_url=$('#next_url').val();
var invite_id =$('#invite_id').val();
var post_url =$('#post_url').val();
var media_url=$('#media-url').val();
var child_url = $('#child_url').val();
$(function(){
    $(document).on('click','#btn_next',function(){
        var inviteId = $('#inviteId').val();
        var name=$('#name').val();
        var phone =$('#phone').val();
        if(name.length!=''&&phone.length!=''){
            if((/^1[34578]\d{9}$/.test(phone))){
                $.ajax({
                    type: "POST",
                    url: child_url,
                    data: {"name":name,"phone":phone,"invite_id":invite_id},
                    success: function (msg) {
                        var info = JSON.parse(msg)['info'];
                        var  msgErrCode=JSON.parse(msg).errcode;
                        var errMsg=JSON.parse(msg).errmsg;
                        if(msgErrCode == 0){
                            var data = info;
                            if(data.length != 0){
                                var html = '';
                                html += '<table border="1" style="border:1px solid #eee; width:100%"><thead style="background: #ebebeb;"><td style="padding:5px;">班级</td><td style="padding:5px;">姓名</td></thead>';  
                                for(var i = 0; i<data.length;i++){
                                    // html += '<p style="text-align:left; margin-left:10%;">班级: '+resq.data[i].class+'</p>';
                                    // html += '<p style="text-align:left; margin-left:10%;">姓名: '+resq.data[i].name+'</p>';    
                                    html += '<tr><td style="padding:5px;">'+data[i].class+'</td>';
                                    html += '<td>'+data[i].name+'</td></tr>';
                                }
                                html += '</table>';
                                $.alert({
                                    title:  '有孩子已经就读',
                                    text: html,
                                    onOK: function () {
                                        register_post();
                                    }
                                });
                                $('.weui-dialog__bd').css({'max-height':'400px','overflow':'scroll'})
                            }else{
                                //$.alert('注册成功！', function(){
                                    register_post();
                                //});
                            }
                        } else {
                            if(errMsg == '该手机号已注册'){
                                //$.alert('该手机号已注册,点击确定到扫码关注！', function(){
                                    register_post();
                                //});
                            }else{
                                $.alert(errMsg);
                            }
                        }
                    }
                });
            }else{
                $.alert('手机号码有误，请重填！');
                return false;
            }
        }else{
            if($('#name').val() == ''){
                $.alert('请填写学生姓名');
            }else if($('#phone').val() == ''){
                 $.alert('请输入家长电话');
            }
        }
        return false;
    });
    $('#powerby').find('a').before('<img src="../apps/contact/static/images/copyright.png" style="width:25%"><br>');
}); 
function register_post(){
    var inviteId = $('#inviteId').val();
    var name=$('#name').val();
    var phone =$('#phone').val();
    $.ajax({
        type: "POST",
        url: post_url,
        data: {"name":name,"phone":phone,"invite_id":invite_id},
        success: function (msg) {
            var info = JSON.parse(msg)['info'];
            var  msgErrCode=JSON.parse(msg).errcode;
            var errMsg=JSON.parse(msg).errmsg;
            if(msgErrCode == 0){
                $.alert('注册成功！', function(){
                    window.location.href = next_url;
                });
            } else {
                if(errMsg == '该手机号已注册'){
                    $.alert('该手机号已注册,点击确定到扫码关注！', function(){
                        window.location.href = next_url;
                    });
                }else{
                    $.alert(errMsg);
                }
            }
        }
    });
}