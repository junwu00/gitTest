var c_search_bottom_bar_height = 0;	//底部菜单高度，用于自适应高度时计算

//监听url变更
window.addEventListener('popstate', function(e) {
	var info = e.state;
	if (info) {
	  	var pre_page = '';
	  	if ($('#c-search-user-detail').css('left') == '0px') {	//成员详情页面
	  		pre_page = 'detail';
		} else if ($('#c-search-edit').css('left') == '0px') {	//编辑页面
			pre_page = 'edit';
		} else {												//主页面(user)
			pre_page = 'user';
		}
	  	
	  	switch(info.target) {	    		
	    	case 'user':
	    		$('#c-search-back').click();
	    		break;
	  	}
      // ...
	} else {
		// 页面初始化的时候state为null，需要做的处理   
		$('#c-search-back').click();
	}
	// 当然也可以直接使用history.state来获取当前对应的state数据。
});

$(document).ready(function(){
	var win_height = $(window).height();
	$('#c-search-container').css({'height': win_height - c_search_bottom_bar_height});

	try {c_search_conf_show_list = $.parseJSON(c_search_conf_show_list);} 		catch (e) {c_search_conf_show_list = [];}		//可显示的字段
	
	c_search_init_tab_toggle();
	$('#c-search-title').bind('focus', function() {
		$('#c-search-selected').addClass('hide');
	});
});

//初始化主页面、编辑、常用联系人详情切换
function c_search_init_tab_toggle() {
	$('#c-search-back').unbind('click').bind('click', function() {
		$('#c-search-head').removeClass('hide');
		$('#c-search-selected').addClass('hide');
		if ($('#c-search-user-detail').css('left') == '0px') {		//当前在常用详情页面，则返回主页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'normal', function() {
				$('#c-search-user-detail').html('');
				$('#c-search-back').removeClass('hide');
			});
			
		} else if ($('#c-search-edit').css('left') == '0px') {		//当前在编辑页面，则返回主页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-edit').animate({left: '100%'}, 'normal', function() {
				$('#c-search-back').addClass('hide');
			});

		} else {													//当前在主页面，不处理
		}
	});
}

//编辑个人信息
function c_search_show_self_edit(that, evt) {
	evt.stopPropagation();

	$('#c-search-head').addClass('hide');
	$('#c-search-selected').addClass('hide');
	$('#c-search-edit').animate({left: '0'}, 'normal', function() {
		$('#c-search-user').addClass('hide');
	});
	
	history.pushState({from: 'user', target: 'edit'}, "通讯录_编辑个人信息");
}

//初始化员工详情页面展示
function c_search_show_detail(that, evt) {
	evt.stopPropagation();

	$('#c-search-head').addClass('hide');
	$('#c-search-selected').addClass('hide');
	var that = $(that);
	var user = $(that).attr('data-id');
	$('#c-search-user-detail').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	$('#c-search-user-detail').animate({left: '0'}, 'normal', function() {
		$('#c-search-user').addClass('hide');
		c_search_load_user_detail(that, user);
	});

	history.pushState({from: 'user', target: 'detail'}, "通讯录_常用联系人详情");
}

//加载员工详情信息
function c_search_load_user_detail(btn, user) {
	var map = {'wx_acct': 'weixin_id', 'phone': 'mobile', 'tel': 'tel', 'email': 'email'};
	var ext_attr = [];
	for (var i in c_search_conf_show_list) {
		if (map[c_search_conf_show_list[i]]) ext_attr.push(map[c_search_conf_show_list[i]]);
	}
	frame_obj.post({
		url		: c_search_detail_url,
		data	: {user: user, ext_attr: ext_attr, chk_general: 1},
		success	: function(resp) {
			if (resp.errcode == 0) {
				c_search_build_user_detail(resp.info);
				
			} else {
				frame_obj.alert('加载员工详情信息失败');
			}
		}
	});
}

//生成员工详情页面
function c_search_build_user_detail(data) {
	var depts = '<span>';
	for (var i in data.dept_list) {
		if (!data.dept_list[i])		continue;
		
		var dept = data.dept_list[i];
		var dept_name = dept.name;
		dept = '<span class="c-search-detail-dept" data-id="' + dept.id + '" data-name="' + dept_name + '">' + dept_name + '</span>';
		depts += dept;
	}
	depts += '</span>';
	
	var gender_icon = '';
	if (data.gender == 1) {
		gender_icon = '<img src="' + c_search_detail_male_icon + '" class="c-search-detail-gender">';
	} else if (data.gender == 2) {
		gender_icon = '<img src="' + c_search_detail_famale_icon + '" class="c-search-detail-gender">';
	}
	var set_general = '';
	var show_general_flag = 'hide';
	if (data.general) {
		set_general = '<span class="c-search-detail-set-general" data-state="1" data-name="' + data.name + '" data-apy="' + data.all_py + '" data-py="' + data.first_py + '" data-id="' + data.id + '" onclick="c_search_set_general(this);">移除常用联系人</span>';
		show_general_flag = '';
	} else {
		set_general = '<span class="c-search-detail-set-general" data-state="0" data-name="' + data.name + '" data-apy="' + data.all_py + '" data-py="' + data.first_py + '" data-id="' + data.id + '" onclick="c_search_set_general(this);">加为常用联系人</span>';
	}
	
	//自定义信息
	var ext_attr = [];
	var ext_attr_html = '';
	try {
		if (data.ext_attr != '') {
			data.ext_attr = $.parseJSON(data.ext_attr);
			ext_attr = data.ext_attr.attrs;
		}
		for (var i in ext_attr) {
			ext_attr_html += '<p class="c-search-detail-line">' +
			'<label>' + ext_attr[i].name + '</label>' + ext_attr[i].value +
			'</p>';
		}
	} catch(e) {}
	
	var html = '<div>' +
		'<p id="c-search-detail-banner">' +
			'<img src="' + data.pic_url + '" onerror="this.src=\'static/image/face.png\'" class="c-search-detail-pic">' +
			'<span>' +
				data.name + gender_icon +
			'</span>' +
		'</p>' +
		'<p id="c-search-detail-title">' +
			'<span>' +
				'<span id="c-search-detail-name">' + data.name + '<span class="' + show_general_flag + '">常用联系人</span></span>' +
				'<span id="c-search-detail-depts">' + depts + '</span>' +
				'<span id="c-search-detail-position">' + data.position + '</span>' +
			'</span>' +
			set_general +
			'<span class="c-search-detail-set-general-wait hide"><i><b></b></i> 数据提交中...</span>' + 
		'</p>' +
		'<p class="c-search-detail-line c-search-detail-line-first">' +
			'<label>微信号</label>' + (data.weixin_id === 'undefined' ? '******' : data.weixin_id || '<font color="#9b9b9b">(暂无)</font>') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>手机号码</label>' + (data.mobile === 'undefined' ? '******' : data.mobile || '<font color="#9b9b9b">(暂无)</font>') +
			(data.mobile ? '<a href="tel:' + data.mobile + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>工作电话</label>' + (data.tel === 'undefined' ? '******' : data.tel || '<font color="#9b9b9b">(暂无)</font>') +
			(data.tel ? '<a href="tel:' + data.tel + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		// mrc add
		'<p class="c-search-detail-line">' +
			'<label>短号</label>' + (data.short_code === 'undefined' ? '******' : data.short_code || '<font color="#9b9b9b">(暂无)</font>') +
			(data.short_code ? '<a href="tel:' + data.short_code + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		'<p class="c-search-detail-line">' +
			'<label>内线号</label>' + (data.inner_code === 'undefined' ? '******' : data.inner_code || '<font color="#9b9b9b">(暂无)</font>') +
			(data.inner_code ? '<a href="tel:' + data.inner_code + '" class="c-search-detail-call"><img src="' + c_search_detail_call_icon + '"></a>' : '') +
		'</p>' +
		// mrc end
		'<p class="c-search-detail-line">' +
			'<label>邮箱</label>' + (data.email === 'undefined' ? '******' : data.email || '<font color="#9b9b9b">(暂无)</font>') +
		'</p>' +
		ext_attr_html +
		'<button type="button" class="btn btn-success c-search-send-msg" data-acct="' + data.acct + '" onclick="c_search_send_msg(this);">发消息</button>' +
	'</div>';
	$('#c-search-user-detail').html(html);
}

//加为/移除常用联系人
function c_search_set_general(that) {
	var is_general = $(that).attr('data-state');
	var id = $(that).attr('data-id');
	var target_url = '';
	if (is_general == 0) {	//添加为常用
		target_url = add_general_url;
	} else {				//移除常用
		target_url = del_general_url;
	}
	$('.c-search-detail-set-general-wait').removeClass('hide');
	$(that).addClass('hide');
	frame_obj.post({
		url		: target_url,
		data	: {id: id},
		success	: function(resp) {
			$('.c-search-detail-set-general-wait').addClass('hide');
			$(that).removeClass('hide');
			if (resp.errcode == 0) {
				if (is_general == 0) {	//修改后为常用
					$(that).attr('data-state', 1);
					$(that).html('移除常用联系人');
					$('#c-search-detail-name span').removeClass('hide');
					
					var pic_url = $('#c-search-detail-banner img').attr('src');
					var user_name = $(that).attr('data-name');
					var all_py = $(that).attr('data-apy');
					var pinyin = $(that).attr('data-py');
					var dept_str = '';
					$('#c-search-detail-depts .c-search-detail-dept').each(function() {
						dept_str += ',' + $(this).html();
					});
					if (dept_str != '')		dept_str = dept_str.substr(1);
					var position = $('#c-search-detail-position').html();
					var new_item = '<p class="c-search-py-item" data-id="' + id + '" data-name="' + user_name + '" data-apy="' + all_py + '" data-py="' + pinyin + '" onclick="c_search_show_detail(this, event);">' + 
						'<img class="c-search-user-pic" data-id="' + id + '" src="' + pic_url + '" onerror="this.src=\'static/image/face.png\'" onclick="c_search_show_detail(this, event);">' +
						'<span class="c-search-user-name" data-id="' + id + '" onclick="c_search_show_detail(this, event);">' +
							user_name + '<br>' +
							'<span>' + dept_str + ' ' + position + '</span>' +
						'</span>' +
					'</p>';
					$('#c-search-general-list').prepend(new_item);
					
				} else {
					$(that).attr('data-state', 0);
					$(that).html('添加常用联系人');
					$('#c-search-detail-name span').addClass('hide');
					
					$('.c-search-py-item[data-id=' + id + ']').remove();
				}
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		}
	});
}

//建立会话
function c_search_send_msg(that){
	var msg = '未知错误';
	var acct = $(that).attr('data-acct');

	if (acct == undefined || acct == null || !acct) {
		acct = '';
	}

	acct = acct.replace(/(^\s*)|(\s*$)/g, "");
	if (acct == '') {
		msg = '无效的参数';
		frame_obj.tips(msg);
		return false;
	}

	wx.openEnterpriseChat({
		userIds: acct, 
		groupName: '',
		success: function(res){
		},
	    fail: function(res){
			if(res.errMsg.indexOf('function not exist') > 0){
              frame_obj.tips('微信版本过低，请升级后重试');
          } else {
          	switch (res.errCode) {
          		case '10001': msg = 'appid无效';				break;
          		case '10002': msg = '用户未关注企业号';		break;
          		case '10003': msg = '消息服务未开启';			break;
          		case '10004': msg = '用户不在消息服务可见范围';	break;
          		case '10005': msg = '存在无效的消息会话成员';	break;
          		case '10006': msg = '消息会话成员数不合法';		break;
          	}
              frame_obj.tips(msg);
          }
		}
	});
}

//编辑个人信息项
function c_search_detail_edit_item(that, evt) {
	var field = $(that).attr('data-field');
	var field_name = $(that).attr('data-fname');
	var field_val = $(that).attr('data-val');
	var placeholder = $(that).attr('data-demo') || '';
	
	$('#c-search-detail-edit-bar label').html(field_name);
	$('#c-search-detail-edit-bar input').val(field_val);
	$('#c-search-detail-edit-bar input').attr('data-field', field);
	$('#c-search-detail-edit-bar input').attr('placeholder', placeholder);
	
	$('#c-search-detail-edit-main').addClass('hide');
	$('#c-search-detail-edit-bar').removeClass('hide');
	$('#c-search-detail-edit-bar input').focus();
}

//取消个人信息编辑
function c_search_detail_edit_cancel(that, evt) {
	$('#c-search-detail-edit-main').removeClass('hide');
	$('#c-search-detail-edit-bar').addClass('hide');
}

//保存个人具信息编辑
function c_search_detail_edit_ok(that ,evt) {
	var input = $(that).parent().find('input[type=text]');
	var data = new Object();
	var field = $(input).attr('data-field');
	var val = $(input).val();
	
	switch (field) {
		case 'tel':
			if (!check_data(val, 'phone')) {
				frame_obj.tips('请输入正确的电话<br>如: 020-12345678');	return;
			}
			break;
		case 'work_position':
			if (!check_data(val, 'notnull')) {
				frame_obj.tips('请填写工作地点');	return;
			}
			break;
		case 'weixin_id':
			if (!check_data(val, 'notnull')) {
				frame_obj.tips('请填写微信号');	return;
			}
			break;
		case 'mobile':
			if (!check_data(val, 'mobile_s')) {
				frame_obj.tips('请填写11位手机号码');	return;
			}
			break;
		case 'email':
			if (!check_data(val, 'email')) {
				frame_obj.tips('请填写正确的邮箱<br>如：123456@qq.com');	return;
			}
			break;
	}
	
	data[field] = val;
	frame_obj.lock_screen('数据提交中');
	frame_obj.post({
		url			: save_self_url,
		ajax_act	: 'save',
		data		: data,
		success		: function(resp) {
			frame_obj.unlock_screen();
			if (resp.errcode == 0) {
				frame_obj.tips('保存成功');
				c_search_detail_edit_cancel();
				$('.c-search-detail-line span[data-field=' + field + ']').html(val);
				$('.c-search-detail-line span[data-field=' + field + ']').parent('.c-search-detail-line').attr('data-val', val);
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		}
	});
}

//清空当前编辑数据
function c_search_detail_edit_clean(that, evt) {
	$(that).parent().find('input[type=text]').val('');
}

//搜索常用联系人
function c_search_on_general_search(that, evt) {
	var val = $(that).children('input[type=search]').val();
	
	val = val.toLowerCase();
	var reg = eval('/^[\\S]*' + val + '[\\S]*$/');
	$('.c-search-py-item').each(function() {
		var name = $(this).attr('data-name');
		var apy = $(this).attr('data-apy');
		var py = $(this).attr('data-py');
		if (reg.test(name) || reg.test(apy) || reg.test(py)) {
			$(this).removeClass('hide');
		} else {
			$(this).addClass('hide');
		}
	});
}