
$(document).ready(function() {
	$("ul.two").first().css('display','inline');
	$(".contact-tog").each(function(){
		$(this).bind('click',function(){
			var url = $("#durl").val();
			window.location.href = url;
		});
	});
    $(".general-contact").each(function(){
    	$(this).bind('click',function(){
    		var url = $("#durl").val();
			window.location.href = url + '&a=general';
    	});
    });
    
    $("#right-menu li").bind('click',function(e){
		 e.stopPropagation();
	});
	init_load_contact();
	init_load_search();
	init_save_change();
	init_save_or_del_general();
	init_show_tip();
	to_code();
});

function to_code() {
	$('#right-menu a').each(function() {
		$(this).unbind('click').bind('click', function() {
			var id = $(this).attr('href');
			var top = $(id)[0].offsetTop;
			$('.container').scrollTop(Number(top));
			return false;
		});
	});
}

function func(a){
	
	if($(a).find('i').attr('class')!='icon-caret-down'){
		$(".float-opearte").hide();
		$(".all-list").find("i").each(function(){
			$(this).attr('class','icon-caret-left');
		});
		$(a).find('i').removeClass('icon-caret-left');
		$(a).find('i').addClass('icon-caret-down');
		$(a).find('.float-opearte').show();
	}else{
		$(".float-opearte").hide();
		$(a).find('i').removeClass('icon-caret-down');
		$(a).find('i').addClass('icon-caret-left');
	}
	
}

function show_msg(){
	frame_obj.alert('该员工信息不可查看');
}
function init_load_contact(){
	
	$(".contact-all").bind('click',function(){
		$(".all").show();
		$(".dept_all").hide();
		$('.search-result').hide()
		$(this).addClass('contact-select');
		$('.depart-contact').removeClass('contact-select');
	});

	$('.depart-contact').bind('click',function(){
		$('.all').hide();
		$('.dept_all').show();
		$('.search-result').hide();
		$(this).addClass('contact-select');
		$('.contact-all').removeClass('contact-select');
	});
}

function entersearch(){
	if($(".search1").val()!=""){
		$('.del_search_i').css({'display':'inline-block'});
		//$('.del_search_i').show();
		var url = $("#durl").val()+"&act=search";
		$.post(
				url,
				{
					"text":$(".search1").val(),
				},
				function(data){
					if(data['state'] ==0){
						var str = "<ul class='search-list'>";
					    var arr = data['data'];
						for(var i in arr){
							if(arr[i]['pic_url']!=null){
								var m = arr[i]['pic_url']+'64';
							}else{
								var m = $("#url").val()+'images/face.png';
							}
							if(arr[i]['id']!= data['sid']){
								str += '<li><a href="'+$("#durl").val()+'&uid='+arr[i]['id']+'"><div style="width:34%;float:left;padding-left:4%"><img src="'+m+'" style="width:30px;height:30px;margin-bottom:3px;"/><span style="padding-left:5px;font-size:13px;">'+arr[i]['name']+'</span></div><div style="width:65%;float:left;padding-right:2%">';
							}else{
								str += '<li><a href="'+$("#durl").val()+'&a=general&uid='+arr[i]['id']+'"><div style="width:34%;float:left;padding-left:4%"><img src="'+m+'" style="width:30px;height:30px;"/><span style="padding-left:5px;font-size:13px;">'+arr[i]['name']+'</span></div><div style="width:65%;float:left;padding-right:2%">';
							}
							var dept = arr[i]['dept_name'];
							for(var j in dept){
								str += '<span style="float:right;height:12px;font-size:13px;padding-right:3px;">'+dept[j]+'</span>';
							}
							str += '</div></a></li>';
						}
						str += "</ul>";
						$(".search-result").html(str);
						$(".search-result").show();
						$(".dept_all").hide();
						$(".all").hide();
						$(".user-msg-detail").hide();
						$(".contain-detail").hide();
						$(".general-list").hide();
						
					}else{
						var str = "<p style='text-align:center'>"+data['msg']+"</p>";
						$(".search-result").html(str);
						$(".search-result").show();
						$(".dept_all").hide();
						$(".all").hide();
						$(".user-msg-detail").hide();
						$(".contain-detail").hide();
						$(".general-list").hide();
						
					}
				},'json'
		);
	}else{
		cancel_search();
	}
}

function init_load_search(){
	
	$('.search1')[0].addEventListener('input',function(){
		entersearch();
	});
	
	$("#search_btn").bind('click',function(){
		entersearch();
	});
	
}

function change(a){
	var sid = $("#sid").val();
	
	if(a != sid){
		window.location.href= $("#durl").val()+'&uid='+a;
	}else{
		window.location.href = $("#durl").val()+'&a=general&uid='+sid;
	}
}

function init_save_change(){
	$(".self-change").bind("click",function(){
		var btn = $(this);
		btn != undefined && btn.attr('disabled', 'disabled');
		var tel = $("#tel").val();
		var work = $("#work").val();
		var mobile = $("#mobile").val();
		var email = $("#email").val();
		var weixin = $("#weixin").val();
		
		if(mobile!=""){
			if(!check_data(mobile,'mobile')){
				frame_obj.alert("手机号格式有误!");
				btn != undefined && btn.attr('disabled', false);
				return;
			}
		}
		
		
		if(email!=""){
			if(!check_data(email,'email')){
				frame_obj.alert("邮箱格式有误!");
				btn != undefined && btn.attr('disabled', false);
				return;
			}
		}
		if(weixin==""&&email==""&&mobile==""){
			frame_obj.alert("微信、手机号、邮箱至少要填一项！");	
			btn != undefined && btn.attr('disabled', false);
			return;
		}
		
		var url = $("#durl").val()+'&a=general&act=save';
		frame_obj.post({
			msg		: '数据提交中...',
			url		: url,
			data	: {
				"id":$("#uid").val(),
				"tel":tel,
				"mobile":mobile,
				"work":work,
				"weixin":weixin,
				"email":email,
			},
			success	: function(data) {
				frame_obj.unlock_screen();
				btn != undefined && btn.attr('disabled', false);
				if(data['errcode']==0){
					window.location.href = $("#durl").val()+'&a=general';
					
				}else{
					frame_obj.alert(data['errmsg']);
				}
			}
		});
	});
}

function init_save_or_del_general(){
	
	$(".add-general").bind('click',function(){
		var id = $("#uid").val();
		var url = $("#durl").val()+'&a=general&act=add';
		frame_obj.lock_screen('加入常用联系人……');
		$.post(
				url,
				{
					"id":$("#uid").val(),
				},
				function(data){
					frame_obj.unlock_screen();
					if(data['errcode']==0){
						window.location.href = $("#durl").val()+'&uid='+id;
					}else{
						frame_obj.alert(data['errmsg']);
					}
				},
				"json"
		);
	});
	$(".delete-general").bind('click',function(){
		var id = $("#uid").val();
		var url = $("#durl").val()+'&a=general&act=del';
		frame_obj.lock_screen('移除常用联系人……');
		$.post(
				url,
				{
					"id":$("#uid").val(),
				},
				function(data){
					frame_obj.unlock_screen();
					if(data['errcode']==0){
						window.location.href = $("#durl").val()+'&uid='+id;
					}else{
						frame_obj.alert(data['errmsg']);
					}
				},
				"json"
		);
	});

	$(".send-msg").unbind('click').bind('click',function(){
		var acct = $("#real_user_acct").val();
		sendMessage(acct);
	});
}

function addGeneral(a){
	var id = $(a).attr('uid');
	var url = $("#durl").val()+'&a=general&act=add';
	$.post(
			url,
			{
				"id":id
			},
			function(data){
				if(data['errcode']==0){
					window.location.href = $("#durl").val()+'&a=general';
				}else{
					frame_obj.alert(data['errmsg']);
				}
			},
			"json"
	);
}
function delGeneral(a){
	var id = $(a).attr('uid');
	var url = $("#durl").val()+'index/general?act=del';
	$.post(
			url,
			{
				"id":id
			},
			function(data){
				if(data['errcode']==0){
					window.location.href = $("#durl").val()+'&a=general';
				}else{
					frame_obj.alert(data['errmsg']);
				}
			},
			"json"
	);
}

function sendMessage(acct){
	var msg = '未知错误';

	if (acct == undefined || acct == null || !acct) {
		acct = '';
	}

	acct = acct.replace(/(^\s*)|(\s*$)/g, "");
	if (acct == '') {
		msg = '无效的参数';
		frame_obj.tips(msg);
		return false;
	}

	wx.openEnterpriseChat({
		userIds: acct, 
		groupName: '',
		success: function(res){
		},
	    fail: function(res){
			if(res.errMsg.indexOf('function not exist') > 0){
                frame_obj.tips('微信版本过低，请升级后重试');
            } else {
            	switch (res.errCode) {
            		case '10001': msg = 'appid无效';				break;
            		case '10002': msg = '用户未关注企业号';		break;
            		case '10003': msg = '消息服务未开启';			break;
            		case '10004': msg = '用户不在消息服务可见范围';	break;
            		case '10005': msg = '存在无效的消息会话成员';	break;
            		case '10006': msg = '消息会话成员数不合法';		break;
            	}
                frame_obj.tips(msg);
            }
		}
	});
}

function init_show_tip(){
	$("#right-menu li").bind('click',function(){
		$("#divbox").html($(this).find('a').html());
		$("#divbox").fadeIn();
		setTimeout("codefans()",800);//3秒
	});
}
function codefans(){
	$("#divbox").fadeOut();
}

function cancel_search(){
	//$('.del_search_i').hide();
	$('.del_search_i').css({'display':'none'});
	$(".search1").val("");
	var top_select = $('.contact-select').html();
	if("部门" == top_select){
		$(".search-result").hide();
		$(".all").hide();
		$(".dept_all").show();
	}else{
		$(".search-result").hide();
		$(".dept_all").hide();
		$(".all").show();
	}
}