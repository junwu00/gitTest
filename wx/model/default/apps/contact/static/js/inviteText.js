/**
 * Created by hdh on 2017/3/2.
 */
var invite_id=$('#invite_id').val();
var get_url = $('#get_url').val();
var ajax_url = $('#ajax_url').val();
var media_url = $('#media-url').val();
$(function () {
    $.ajax({
        type: "POST",
        url: get_url,
        data: {'invite_id': invite_id},
        success: function (msg) {
            //加载中
            $.showLoading();
            var msgErrCode = JSON.parse(msg).errcode;
            var errMsg = JSON.parse(msg).errmsg;
            if (msgErrCode == 0) {
                var info = JSON.parse(msg)['info'];
                var inviteInfo = '';
                var html = '';
                if (msg = '') {
                    html += '<div class="weui-loadmore weui-loadmore_line"><span class="weui-loadmore__tips">暂无数据</span> </div>';
                    html += '<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot"><span class="weui-loadmore__tips"></span></div>';
                }
                if ((typeof info) == "object") {
                    html += '<h2 class="active_title">' + info["title"] + '</h2>';
                    html += '<em class="rich_media_meta rich_media_meta_text">' + info["create_time"] + '</em><em class="rich_media_meta rich_media_meta_text rich_media_meta_color"> ' + info["com_name"] + '</em>';
                    html += '<div class="describe">' + info["reply_des"] + '<input type="hidden" id="activityId"' + 'value=' + info["id"] + ' />' + '</div>';
                }
            }
            else if (msgErrCode != 0) {
                $.alert(errMsg);
            } else if ((typeof info) != "object") {
                $.alert("系统繁忙！");
            }
            $("#main").html(html);
            $.hideLoading();
        }
    });
   $('#powerby').find('a').before('<img src="../apps/contact/static/images/copyright.png" style="width:25%"><br>');
});
//获取链接参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
};
