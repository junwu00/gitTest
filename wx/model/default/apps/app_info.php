<?php
/**
 * 已启用的应用
 * @author yangpz
 * @date 2014-10-21
 *
 */
class app_info {
	/** 应用ID与应用名映射 */
	public static $AppMap = array(
		1	=> array('id' => 1,		'name' => 'helper', 		'cn_name' => '通知公告',		'md5' => 'fde5d67bfb6dc4b598291cc2ce35ee4a'),
		2	=> array('id' => 2,		'name' => 'contact', 		'cn_name' => '通讯录',		'md5' => '2f8a6bf31f3bd67bd2d9720c58b19c9a'),
		3	=> array('id' => 3,		'name' => 'checkwork', 		'cn_name' => '掌上考勤',		'md5' => '971e90222be24bf0331bfb3c4cae5624'),
		4	=> array('id' => 4,		'name' => 'card', 	    	'cn_name' => '客户名片',		'md5' => '5dd2199ad68327cc76d583b057aee7d5'),
		5	=> array('id' => 5,		'name' => 'task', 	    	'cn_name' => '任务协作',		'md5' => '478f3a4c51824ad23cb50c1c60670c0f'),
		6	=> array('id' => 6,		'name' => 'email', 	    	'cn_name' => '企业邮箱',		'md5' => '0c83f57c786a0b4a39efab23731c7ebc'),
		7	=> array('id' => 7,		'name' => 'rest', 	    	'cn_name' => '快速请假',		'md5' => '65e8800b5c6800aad896f888b2a62afc'),
		8	=> array('id' => 8,		'name' => 'schedule', 		'cn_name' => '工作日程',		'md5' => '799855594adc0f2bd7302c69d3234b5a'),
		9	=> array('id' => 9,		'name' => 'vote', 			'cn_name' => '民主投票',		'md5' => '4ca5d171acaac2c5ca261c97b0d40383'),
		10	=> array('id' => 10,	'name' => 'exaccount', 		'cn_name' => '费用报销',		'md5' => '8c2272a9cd61e2a50a3c79fe0bb4c019'),
		11 	=> array('id' => 11,	'name' => 'process', 		'cn_name' => '流程审批',		'md5' => '5075140835d0bc504791c76b04c33d2b'),
		12 	=> array('id' => 12,	'name' => 'survey', 		'cn_name' => '问卷调查',		'md5' => '2fa7b041eea9ee875e012bac495a639c'),
		13 	=> array('id' => 13,	'name' => 'legwork', 		'cn_name' => '移动外勤',		'md5' => 'c3d5600153b80ce176fe4553793a3257'),
		14 	=> array('id' => 14,	'name' => 'cabinet', 		'cn_name' => '南湖云盘',		'md5' => '6f7961832ac93809ec77b7db837779c7'),
		15	=> array('id' => 15, 	'name' => 'client', 		'cn_name' => '客户管理',		'md5' => 'cda7994ff4c19062aa935c681823eb1b'),
		16 	=> array('id' => 16, 	'name' => 'market', 		'cn_name' => '销售管理',		'md5' => '1d0c5b5df3f64c68e83de80a1bd985ba'),
		17 	=> array('id' => 17,	'name' => 'recruit', 		'cn_name' => '人力招聘',		'md5' => 'c5a5adb4becbec9d63074dcb70a2fa03'),
		18 	=> array('id' => 18, 	'name' => 'advice', 		'cn_name' => '意见反馈',		'md5' => 'fd99cadea9d8ef6a1ffcc52a2e3e8017'),
		19 	=> array('id' => 19, 	'name' => 'entry',	 		'cn_name' => '综合服务',		'md5' => '1043bfc77febe75fafec0c4309faccf1'),
		22 	=> array('id' => 22, 	'name' => 'activity',	 	'cn_name' => '组织活动',		'md5' => '69a256025f66e4ce5d15c9dd7225d357'),
	);
	
	/**
	 * 根据app_id获取应用控制类
	 * @param unknown_type $app_id
	 * @param unknown_type $com_id	企业ID
	 * @param unknown_type $dept_id	企业根部门的ID
	 */
	public static function get_handler($app_id, $com_id, $dept_id) {
		$app_name = self::$AppMap[$app_id]['name'];
		$app_path = SYSTEM_APPS.$app_name.'/wxser/cls_wxser.php';
		if (!file_exists($app_path)) {
			log_write('应用文件不存在: '.$app_name, 'error');
			exit();
		}
		
		include_once($app_path);
		$cls_ser = 'cls_wxser';
		if (!class_exists($cls_ser)) {
			log_write('应用处理类不存在: '.$app_name, 'error');
			exit();
		}
		
		$ser = new $cls_ser();
		$ser -> init($app_name, $com_id, $dept_id);
		return $ser;
	}
	
	/**
	 * 根据应用的token（应用英文名的md5）获取应用信息
	 * @param unknown_type $token
	 */
	public static function get_by_token($token) {
		$flag = FALSE;
		foreach (self::$AppMap as $app) {
			if (strcmp($app['md5'], $token) == 0) {
				$flag = $app;
				break;
			}
		}
		unset($app);
		return $flag;
	}
	
	//-------------------------------------内部实现----------------------------------------

}

// end of file