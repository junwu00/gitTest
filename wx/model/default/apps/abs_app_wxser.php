<?php
/**
 * 应用基础类
 * @author yangpz
 * @date 2014-10-21
 *
 */
abstract class abs_app_wxser {
	protected $post_data;
	/** 企业ID	*/
	protected $com_id;
	/** 企业根部门的 ID */
	protected $dept_id;
	/** 应用 ID */
	protected $app_id;
	/** 应用英文名称 */
	protected $app_name;
	/** 应用所属套餐的类型 */
	protected $app_combo;
	
	/** 不推送操作指引的企业的com_id */
	private static $NotSendGuideComList = array(2252, 3459);
	
	public function init($app_name, $com_id, $dept_id) {
		$app_conf = include(SYSTEM_APPS.$app_name.'/config.php');
		
		$this -> app_id = $app_conf['id'];
		$this -> app_name = $app_name;
		$this -> app_combo = $app_conf['combo'];
		$this -> com_id = $com_id;
		$this -> dept_id = $dept_id;
	}
	
	public function do_reply($post_data) {
		$this -> post_data = $post_data;
		
		//根据消息类型进行回复
		switch($this -> post_data['MsgType']) {
			case 'text':
				$this -> reply_text();
				break;
			case 'image':
				$this -> reply_image();
				break;
			case 'event':
				$this -> reply_event();
				break;
			default:
				break;
		}
	}
    
    // gch add invite
    //邀请单消息推送
    public function contact_send_msg($post_data = array(),$invite_id=0) {
        !empty($post_data) && $this -> post_data = $post_data;

        // 指定发送到通知公告
        $contact_app_conf = include(SYSTEM_APPS.'contact/config.php');
        $app_id = $contact_app_conf["send_invite_msg"];

        log_write('邀请单消息推送');
        //跳过回调的信息不是指定应用的应用
        if ($this -> app_name != app_info::$AppMap[$app_id]['name']) {
            log_write('跳过回调的信息不是指定应用的应用');
            return;
        }
        //获取相关参数
        $corp_id = $this -> post_data['ToUserName'];
        $acct = $this -> post_data['FromUserName'];

        //获取企业数据
        $com_info = g('company') -> get_by_corp_id($corp_id);
        if (empty($com_info)) {
            log_write('通讯录回调时，找不到企业信息');
            return;
        }
        //获取用户数据
        $user_info = g('user') -> get_by_acct($com_info['dept_id'], $acct);
        if (empty($user_info)) {
            log_write('通讯录回调时，找不到用户信息');
            return;
        }
        //获取邀请单数据
        //lyc
        if($invite_id){
            $invite_page_info = g('invite_action') -> getInvitePageData($invite_id);
            $invite_page_info['invit_order_id'] = $invite_id;
        }else{
            $invite_page_info = g('invite_action') -> invite_page_data_by_uid($user_info['id']);
        }
        if (empty($invite_page_info)) {
            log_write('通讯录回调时，找不到邀请单信息');
            return;
        }
        //判断邀请单状态
        if ($invite_page_info['is_show'] == 0) {
            log_write('该邀请单已被禁用，不做相关消息推送');
            return;
        }
        //消息推送必要参数
        $access_token = g('atoken') -> get_access_token_by_com($com_info['id']);
        // $agent_id = $this -> post_data['AgentID'];
        $appInfo = g('com_app') -> get_by_app($com_info['id'], app_info::$AppMap[$app_id]['id'], 'wx_app_id');
        if (!isset($appInfo['wx_app_id']) || empty($appInfo['wx_app_id'])) {
            log_write('缺少通知应用wx_app_id');
            return;
        }
        $agent_id = $appInfo["wx_app_id"];

        //推送相关消息给用户
        if ($invite_page_info['reply_type'] == 1) {
            log_write('通讯录文本消息推送');
            $user_list[] = $acct;
            $text = strip_tags($invite_page_info['reply_des']);
            g('wxqy') -> send_text($access_token, $user_list, $agent_id, $text, $tag_list=array(), $party_list=array(), $is_safe=0);

        } else if ($invite_page_info['reply_type'] == 2) {
            log_write('通讯录卡券消息推送');
            //组装相关数据
            $url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={$access_token}";
            $data['touser'] = $acct;
            $data['agentid'] = $agent_id;
            $data['msgtype'] = 'card';
            $data['card']['card_id'] = $invite_page_info['reply_card_id'];
            $data = json_encode($data);
            //发送接口请求
            $res = cls_http::post($url, $data);
            log_write(json_encode($res));

        } else if ($invite_page_info['reply_type'] == 3) {
            log_write('通讯录图文消息推送');
            $image_text = array(
             'title' => $invite_page_info['reply_title'],
             'description' => strip_tags($invite_page_info['reply_des']),
             'picurl' => MEDIA_URL_PREFFIX . $invite_page_info['reply_banner'],
             'url' => SYSTEM_HTTP_DOMAIN . '?app=contact&m=index&a=jz_contact_image_text&corpurl=' . $com_info['corp_url'] . '&invite_id=' . $invite_page_info['invit_order_id'],
            );
            $user_list[] = $acct;
            $art_list[] = $image_text; 
            $res = g('wxqy') -> send_news($access_token, $user_list, $agent_id, $art_list, $tag_list=array(), $party_list=array());
        }

        //推送消息给指定人员和部门
        $notify_group_list = json_decode($invite_page_info['notify_group_list'], true);
        $notify_user_list = json_decode($invite_page_info['notify_user_list'], true);
        //获取需要推送消息的微信部门id
        $to_party_list = [];
        foreach ($notify_group_list as $value) {
            $to_party_info = g('dept') -> get_dept_by_id($value);
            $to_party_list[] = $to_party_info['wx_id'];
        }
        //获取指定推送人员的微信账号
        $to_user_list = [];

        foreach ($notify_user_list as $value) {
            $to_user_info = g('user') -> get_by_id($value);
            $to_user_list[] = $to_user_info['acct'];
        }
        //处理指定人员的推送消息
        $dept_list = json_decode($invite_page_info['dept_list'], true);
        $user_info['name'] = explode('/', $user_info['name']);
        $user_info['name'] = array_pop($user_info['name']);
        $text = $user_info['name'] . '的家长通过邀请单[' . $invite_page_info['title'] . ']加入到班级[';
        foreach ($dept_list as $key => $value) {
           $dept_info = g('dept') -> get_dept_by_id($value);
           if ($key < count($dept_list) - 1 ) {
               $text = $text . $dept_info['name'] . '、';
           }else {
                $text = $text . $dept_info['name'];
           }    
        }
        $text .= ']';
        if($to_user_list || $to_party_list)
            g('wxqy') -> send_text($access_token, $to_user_list, $agent_id, $text, $tag_list=array(), $party_list=$to_party_list, $is_safe=0);

        // 更新用户关注状态为关注状态
        $condition = array(
            'id=' => $user_info['id'],
        );
        $updateData = array(
            'state' => 1,
        );
        g('ndb') -> update_by_condition('sc_user', $condition, $updateData);
    }

	//--------------------------------------abstract-------------------------------------------
	
	/**
	 * 响应文本请求
	 */
	protected function reply_text() {}

	/**
	 * 响应图片请求
	 */
	protected function reply_image() {}
	
	/**
	 * 处理地理位置上报
	 */
	protected function reply_location() {}

	/**
	 * 对指定用户回复文本请求
	 *
	 * @access protected
	 * @return void
	 */
	protected function reply_click() {}
	
	/**
	 * 扫码等待事件
	 */
	protected function reply_scancode_waitmsg() {}
	
	/**
	 * 进入应用
	 */
	protected function enter_advice() {}
	
	/** 关注事件 */
	protected function reply_subscribe() {}

	//---------------------------------------------内部实现-----------------------------------
	
	/**
	 * 响应事件请求
	 */
	private function reply_event() {
		switch ($this -> post_data['Event']) {
            case 'subscribe':{
                // 关注
                log_write('关注回调');
                // gch add invite
                $this -> contact_send_msg();
                
				if (in_array($this -> com_id, self::$NotSendGuideComList))	return;
                
                
				$need_send_guide = g('com_app') -> is_send_guide($this -> com_id, $this -> app_id);
				if ($need_send_guide) {				//配置为推送时才推送
	            	$this -> reply_subscribe();
				}	
				
            	break;
            }			
            case 'unsubscribe':			//取消关注
            	break;
            case 'LOCATION':			//地理位置
            	$this -> reply_location();
            	break;
            case 'enter_agent':			//进入应用
            	$this -> enter_advice();
            	break;
            	
            case 'click':				//点击菜单
            	$this -> reply_click();
            	break;
            case 'VIEW':				//点击菜单链接
            	break;
            case 'scancode_push':		//扫码
            	break;
            case 'scancode_waitmsg': 	//扫码推事件且弹出“消息接收中”提示框
            	$this -> reply_scancode_waitmsg();
            	break;
            case 'pic_sysphoto':		//弹出系统拍照发图
            	break;
            case 'pic_photo_or_album':	//弹出拍照或者相册发图
            	break;
            case 'pic_weixin':			//弹出微信相册发图器
            	break;
            case 'location_select':		//弹出地理位置选择器
            	break;
		}
	}
	
} 

// end of file