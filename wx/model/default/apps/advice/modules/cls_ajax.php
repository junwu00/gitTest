<?php
/**
 * 全部反馈建议ajax行为的交互类
 *
 * @author Chenyihao
 * @create 2016-01-23
 */
class cls_ajax extends abs_app_base {
    public function __construct() {
        parent::__construct('advice');
    }

    private $Page_Size = 20;
    private static $Session_file = 'SESSION_FILE';
    private static $FileTable = 'advice_file';


    /**
     * ajax行为统一调用方法
     *
     * @access public
     * @return void
     */
    public function index() {
        $cmd = (int)get_var_value('cmd', FALSE);
        switch($cmd) {
            case 101 : {
                //获取反馈分类
                $this -> get_classify_list();
                BREAK;
            }
            case 102 : {
                //提交反馈
                $this -> add_advice();
                BREAK;
            }
            case 103 : {
                //我的反馈列表
                $this -> get_my_advice_list();
                BREAK;
            }
            case 104 : {
                //评论
                $this -> comment();
                BREAK;
            }
            case 105 : {
                //获得我处理的反馈列表
                $this -> get_my_deal_list();
                BREAK;
            }
            case 106 : {
                //获得我监督的反馈列表
                $this -> get_my_super_list();
                BREAK;
            }
            case 115 : {
                //催办
                $this -> press();
                BREAK;
            }
            case 107: {
                //草稿状态发布
                $this -> submit();
                BREAK;
            }
            case 108: {
                //处理人关闭反馈
                $this -> finish();
                BREAK;
            }
            case 109: {
                //处理人主导
                $this -> primary();
                BREAK;
            }
            case 110: {
                //获取反馈动态列表
                $this -> get_trend();
                BREAK;
            }
            case 111: {
                //获取反馈评论列表
                $this -> get_reply();
                BREAK;
            }
            case 112: {
                //撤回反馈
                $this -> withdraw_advice();
                BREAK;
            }
            case 113: {
                //删除反馈
                $this -> delete_advice();
                BREAK;
            }
            case 114: {
                //激活反馈
                $this -> resend_advice();
                BREAK;
            }
            case 116: {
                //附件下载
                $this -> download_file();
                BREAK;
            }
            case 117: {
                //关闭反馈
                $this -> close_advice();
                BREAK;
            }
            case 118: {
                //取消主导
                $this -> cancel_primary();
                BREAK;
            }
            case 119: {
                //获取评论的所有回复
                $this -> get_child_reply();
                BREAK;
            }
            default : {
                //非法的请求
                cls_resp::echo_resp(cls_resp::$ForbidReq);
                BREAK;
            }
        }
    }

    /**
     * 下载文件
     *
     * @access private
     * @return void
     */
    private function download_file() {
        $advice_id = (int)get_var_value('advice_id');
        $cf_id = (int)get_var_value('id');
        $origin = (int)get_var_value('origin');
        $is_push = (int)get_var_value('is_push');

        if (empty($cf_id)) {
            cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
        }

        $size = '';
        $origin and $size = '-1';
        $is_push = $is_push === 0 ? false : true;

        parent::log('开始下载文件');
        try {
            $result = g('advice_info') -> download_file($advice_id,$cf_id, $size, $is_push);
        }catch(SCException $e) {
            parent::log('下载文件失败，异常：[' . $e -> getMessage() . ']');
            cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
        }

        parent::log('下载文件成功');
        cls_resp::echo_ok();
    }

    public function resend_advice(){
        try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            if (empty($advice_id)) {
               throw new SCException('反馈不存在！');
            }
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }
            if($advice['user_id'] != $user_id){
                throw new SCException('没有权限！');
            }
            if($advice['title_state']!=2){
                throw new SCException('当前状态不能激活！');
            }
            $classify = g('advice_class') -> get_class_by_id($advice['adc_id'],'*');
            if(empty($classify)){
                throw new SCException('对于反馈分类不存在！');
            }

            $ret = g('advice_info') -> resend($advice_id);
            if(!$ret){
                throw new SCException('激活失败！');
            }

            if($advice['ays'] == 1){
                $user_pic = '';
                $user_name = '发起人';
                

            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '激活反馈';
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);

            $handler = array($advice['primary_id']);
            $this -> remind($handler,$advice_id,2,'您有一条处理过的反馈激活了，请及时处理！');


            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 删除反馈
     */
    public function delete_advice(){
        try{
            g('db') -> begin_trans();
        	$fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            if (empty($advice_id)) {
               throw new SCException('反馈不存在！');
            }
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('删除失败，反馈不存在');
            }
            if($advice['user_id'] != $user_id){
                throw new SCException('没有权限！');
            }
            if($advice['title_state']!=0){
                throw new SCException('当前状态不能删除！');
            }

            $ret = g('advice_info') -> delete($advice_id);
            if(!$ret){
                throw new SCException('删除失败！');
            }
            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 撤回反馈
     */
    public function withdraw_advice(){
        try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
	        if (empty($advice_id)) {
	           throw new SCException('反馈不存在！');
	        }
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }
            if($advice['user_id'] != $user_id){
                throw new SCException('没有权限！');
            }
            if($advice['state']!=0){
                throw new SCException('当前状态不能撤回！');
            }

            $ret = g('advice_info') -> withdraw($advice_id,$user_id);
            if(!$ret){
                throw new SCException('撤回失败！');
            }
            
            if($advice['ays'] == 1){
                $user_pic = '';
                $user_name = '发起人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '撤回反馈';
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);
            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 获取反馈评论列表
     */
    public function get_reply(){
        try{
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $page = isset($data['page']) ? (int)$data['page'] : 1;
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 1;

            $ret = g('advice_reply') -> get_list($advice_id,'r1.*,count(r2.id) reply_count',$page,$page_size);
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret);
        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }
    }

    public function get_child_reply(){
        try{
            $fields = array('id','reply_id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $reply_id = (int)$data['reply_id'];
            $page = isset($data['page']) ? (int)$data['page'] : 1;

            $ret = g('advice_reply') -> get_child_list($advice_id,$reply_id,$page);
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret);
        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }   
    }

    /**
     * 获取反馈动态（不分页）
     */
    public function get_trend(){
        try{
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $page = isset($data['page']) ? (int)$data['page'] : 1;
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;

            $ret = g('advice_trend') -> get_trend($advice_id,'*',$page,$page_size);
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret);
        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 处理人主导反馈
     */
    public function primary(){
       try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }

            if(!empty($advice['primary_id'])){
                throw new SCException('该反馈已经存在主导人！');
            }

            $classify = g('advice_info') -> get_class_by_id($advice['adc_id'],'*');
            if(empty($classify)){
                throw new SCException('对于反馈分类不存在！');
            }

            $handler = json_decode($classify['handler_ids'],true);
            if(!in_array($user_id,$handler)){
                throw new SCException('没有权限主导！');
            }

            $ret = g('advice_info') -> primary($advice_id,$user_id);
            if(!$ret){
                throw new SCException('主导失败！');
            }

            if($advice['handler_anonymous'] == 1){
                $user_pic = '';
                $user_name = '处理人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '主导了该反馈';
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);

            
            $this -> remind(array($advice['user_id']),$advice_id,1,'您有一条反馈已经被主导，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 处理人取消主导反馈
     */
    public function cancel_primary(){
       try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }

            if($advice['primary_id'] != $user_id){
                throw new SCException('没有权限取消主导');
            }

            $ret = g('advice_info') -> primary($advice_id,$user_id,2);
            if(!$ret){
                throw new SCException('取消主导失败！');
            }

            if($advice['handler_anonymous'] == 1){
                $user_pic = '';
                $user_name = '处理人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '取消主导反馈';
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);
            // $this -> remind(array($advice['user_id']),$advice_id,1,'您有一条反馈已经被主导，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 草稿反馈提交
     */
    public function submit(){
        try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }

            $classify = g('advice_info') -> get_class_by_id($advice['adc_id'],'*');
            if(empty($classify)){
                throw new SCException('反馈分类不存在！');
            }
            $ret = g('advice_info') -> submit($advice_id,$user_id);
            if(!$ret){
                throw new SCException('提交失败！');
            }
            if($advice['ays'] == 1){
                $user_pic = '';
                $user_name = '发起人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '提交反馈';
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);

            $handler = json_decode($classify['handler_ids'],true);
            empty($handler) && $handler = array();
            $supervisor = json_decode($classify['supervisor_ids'],true);
            empty($supervisor) && $supervisor = array();
            $this -> remind($handler,$advice_id,2,'您收到一条新的反馈，请及时处理！');
            $this -> remind($supervisor,$advice_id,3,'您收到一条新的反馈，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 将反馈置为关闭状态
     */
    public function close_advice(){
        try{
            g('db') -> begin_trans();
            $fields = array('id','comment');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $comment = $data['comment'];
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }
            if($advice['primary_id'] != $user_id){
                throw new SCException('没有权限！');
            }

            $ret = g('advice_info') -> close_advice($advice_id,$user_id);
            if(!$ret){
                throw new SCException('关闭失败！');
            }

            if($advice['handler_anonymous'] == 1){
                $user_pic = '';
                $user_name = '处理人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '关闭反馈，备注：'.$comment;
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);

            $this -> remind(array($advice['user_id']),$advice_id,1,'您发起的反馈已经被关闭，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 将反馈置为完成状态
     */
    public function finish(){
        try{
            g('db') -> begin_trans();
            $fields = array('id','comment');
            $data = $this -> get_post_data($fields);
            $advice_id = (int)$data['id'];
            $comment = $data['comment'];
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $advice = g('advice_info') -> get_advice_by_id($advice_id);
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }
            if($advice['primary_id'] != $user_id){
                throw new SCException('没有权限！');
            }

            $ret = g('advice_info') -> finish($advice_id,$user_id);
            if(!$ret){
                throw new SCException('完成失败！');
            }

            if($advice['handler_anonymous'] == 1){
                $user_pic = '';
                $user_name = '处理人';
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '完成了反馈，备注：'.$comment;
            $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);

            $this -> remind(array($advice['user_id']),$advice_id,1,'您发起的反馈已经完成，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 获取分类列表
     **/
    private function get_classify_list(){
        try{
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $user = g('user') -> get_by_id($user_id,'dept_tree');
            if(empty($user)){
                throw new SCException('用户不存在，或者已禁用！');
            }
            $dept_tree = json_decode($user['dept_tree']);
            $all_dept = array();
            foreach ($dept_tree as $d) {
                $all_dept = array_merge($all_dept,$d);
            }unset($d);

            $all_dept = array_unique($all_dept);

            $cond = array();
            if(!empty($all_dept)){
                $cond['__OR'] = array();
                foreach ($all_dept as $key => $d) {
                    $cond['__OR']['__'.$key.'__dept_ids like '] = '%"'.$d.'"%';
                }
            }
            $cond['version='] = 0;
            $ret = g('advice_class') -> get_class_list($cond,0,0);
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret['data']);

        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 新增反馈
     * 'classify_id' 分类ID
     * 'title'  反馈标题
     * 'content' 反馈内容
     * 'ays'    是否是匿名反馈,1是，0否
     * 'state'  状态，0-草稿 1-进行中
     * file_hash 附件 格式： array(
     *                               array('file_name'=>'xx','file_ext' => 'xx','file_size' => 'xx','file_path' => 'xxx'),
     *                               array('file_name'=>'xx','file_ext' => 'xx','file_size' => 'xx','file_path' => 'xxx'),
     *                       )
     **/
    private function add_advice(){
        try{
            g('db') -> begin_trans();
            $fields = array('title');
            $data = $this -> get_post_data($fields);
            // $data = array(
            //         'classify_id' => 4,
            //         'title' => '我的反馈',
            //         'content' => '反馈内容',
            //         'ays' => 0,
            //         'state' => 1,
            //     );

            $classify_id = isset($data['classify_id']) ? (int)$data['classify_id'] : 0;
            $title = $data['title'];
            $content = isset($data['content']) ? $data['content'] : "";
            $file_hash = isset($data['file_hash']) ? $data['file_hash'] : array() ;
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $ays = isset($data['ays']) ? (int)$data['ays'] : 0;
            $state = empty($data['state']) ? 0 : (int)$data['state'];
            $id = isset($data['id']) ? (int)$data['id'] : 0;
            $com_id = $_SESSION[SESSION_VISIT_COM_ID];
            $all_file = array();
            if($state >1 || $state < 0){
                throw new SCException('反馈状态错误');
            }
            if($state == 1){
                if(empty($classify_id)){
                    throw new SCException('请选择反馈分类！');
                }
                if(empty($content)){
                    throw new SCException('反馈内容不能为空！');
                }

                $classify = g('advice_info') -> get_class_by_id($classify_id,'*');
                if(empty($classify)){
                    throw new SCException('反馈分类不存在，请重新选择分类！');
                }
                if($classify['anonymous'] == 0){
                    $ays = 0;
                }
            }else{
                $classify = array(
                        'staff_anonymous' => 0,
                        'handler_anonymous' => 0,
                        'supervisor_anonymous' => 0,
                    );
            }
            if(empty($title)){
                throw new SCException('反馈标题不能为空！');
            }

            $data = array(
                    'user_id' => $user_id,
                    'title' => $title,
                    'detail' => $content,
                    'adc_id' => $classify_id,
                    'staff_anonymous' => $classify['staff_anonymous'],
                    'handler_anonymous' => $classify['handler_anonymous'],
                    'supervisor_anonymous' => $classify['supervisor_anonymous'],
                    'upgrade_time' => time(),
                    'title_state' => $state,
                    'ays' => $ays,
                );

            if(!empty($id)){
                $advice = g('advice_info') -> get_advice_by_id($id);
                if($advice['title_state'] == 1){
                    throw new SCException('当前状态无法修改！');
                }
                $ret = g('advice_info') -> update_advice($id,$data);
                if(!$ret){
                    throw new SCException('更新反馈失败！');
                }

                g('advice_info') -> delete_file($id);

                if(is_array($file_hash) && !empty($file_hash)){
                    $all_file = $this -> save_file($id,$file_hash);
                    if(!$all_file){
                        throw new SCException('保存附件失败！');
                    }
                }

                if($ays == 1){
                    $user_pic = '';
                    $user_name = '发起人';
                    $data = array(
                        'user_pic' => '',
                        'user_name' => '发起人',
                    );
                    g('advice_trend') -> update_trend($id,$user_id,$data);
                }else{
                    $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                    $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
                }
                $msg = '修改反馈';
                $ret = g('advice_trend') -> add_trend($id,$user_id,$user_pic,$user_name,$msg);
                $advice_id = $id;
            }else{
                $advice_id = g('advice_info') -> add_advice($data);
                if(!$advice_id){
                    throw new SCException('提交反馈失败！');
                }

                if(is_array($file_hash) && !empty($file_hash)){
                    $all_file = $this -> save_file($advice_id,$file_hash);
                    if(!$all_file){
                        throw new SCException('保存附件失败！');
                    }
                }

                if($ays == 1){
                    $user_pic = '';
                    $user_name = '发起人';
                }else{
                    $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                    $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
                }
                $msg = '创建反馈';
                $ret = g('advice_trend') -> add_trend($advice_id,$user_id,$user_pic,$user_name,$msg);
            }
            if($state == 1){
                $handler_id = json_decode($classify['handler_ids'],true);
                $supervisor = json_decode($classify['supervisor_ids'],true);

                $this -> remind($handler_id,$advice_id,2,'您收到一个新的反馈信息，请及时处理！');
                $this -> remind($supervisor,$advice_id,3,'您收到一个新的反馈信息，请及时查看！');

            }
            //计算容量
            foreach ($all_file as $file) {
                try{
                    g('capacity') -> add_list_by_cache($com_id, $file['file_hash'], $this -> app_name, self::$FileTable, $file['id']);
                }catch(SCException $e){
                    log_write('上传附件计入容量文件列表失败');
                }                
            }
            
            g('db') -> commit();
            cls_resp::echo_ok(cls_resp::$OK,'data',array('id'=>$advice_id));
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 获得我的反馈列表
     * state : 反馈状态 0：草稿，1:处理中，2:已完成
     * title
     * time 
     * classify_id
     **/
    private function get_my_advice_list(){
        try{
            $fields = array('state');
            $data = $this -> get_post_data($fields);

            // $data = array(
            //         'state' => 1,
            //         'time' => '2011-10-12 12:00 - 2018-10-12 12:22',
            //         'title' => '匿名',
            //         'classify_id' => 4,
            //     );

            $title = isset($data['title']) ? $data['title'] : "";
            $time = isset($data['time']) ? $data['time'] : "";
            $classify_id = isset($data['classify_id']) ? (int)$data['classify_id'] : "";
            $page = isset($data['page']) ? (int)$data['page'] : 1;
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $state = (int)$data['state'];

            if($state > 2 || $state < 0){
                throw new SCException('请选择反馈状态！');
            }

            $cond = array(
                    'i.user_id=' => $user_id,
                    'i.info_state=' => 1,
                );
            if($state == 2){
                 $cond['__OR'] = array(
                        'i.title_state=' => $state,
                        'i.state=' => 4,
                    );
            }else if($state == 1){
                $cond['i.title_state='] = $state;
                $cond['i.state!='] = 4;
            }else{
                $cond['i.title_state='] = 0;
            }

            if(!empty($title)){
                $cond['i.title like '] = '%'.$title.'%';
            }
            if(!empty($time)){
                $time = explode(' - ', $time);
                if(!empty($time) && 2 == count($time)){
                    $cond['i.create_time >='] = strtotime($time[0].' 00:00:00');
                    $cond['i.create_time <='] = strtotime($time[1].' 23:59:59');
                }
            }
            if(!empty($classify_id)){
                $cond['i.adc_id ='] = $classify_id;
            }

            $ret = g('advice_info') -> get_list($cond,'i.*,c.name class_name ',$page,$page_size,true);
            if(!$ret){
                throw new SCException($e);
            }
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret);

        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }
    }

    /**
     * 评论
     **/
    private function comment(){
        try{
            g('db') -> begin_trans();
            $fields = array('ad_id','comment');
            $data = $this -> get_post_data($fields);

            // $data = array(
            //         'ad_id' => 50,
            //         'comment' => '哈哈哈哈哈哈哈',
            //     );

            $ad_id = (int)$data['ad_id'];
            $comment = $data['comment'];
            $reply_id = isset($data['reply_id']) ? (int)$data['reply_id'] : 0;
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            if(empty($ad_id)){
                throw new SCException('请指定要评论的反馈！');
            }
            if(empty($comment)){
                throw new SCException('评论不能为空！');
            }

            $advice = g('advice_info') -> get_advice_by_id($ad_id,'id,title_state,adc_id,user_id,ays,handler_anonymous,supervisor_anonymous');
            if(empty($advice)){
                throw new SCException('反馈不存在!');
            }
            if($advice['title_state'] ==2){
                throw new SCException('该反馈已经完成或关闭，无法评论！');
            }

            $class = g('advice_class') -> get_class_by_id($advice['adc_id'],'id,handler_ids,supervisor_ids');
            if(empty($class)){
                throw new SCException('反馈分类不存在！');
            }

            $handler = json_decode($class['handler_ids'],true);
            $supervisor = json_decode($class['supervisor_ids'],true);
            $ids = array_merge($handler, $supervisor);
            $ids = array_unique($ids);
           
            $user_state = 0; //当前用户类型 1，发起人，2，处理人，3，监督人
            if($user_id == $advice['user_id']){
                $user_state = 1;
            }else if(!empty($handler) && in_array($user_id, $handler)){
                $user_state = 2;
            }else if(!empty($supervisor) && in_array($user_id, $supervisor)){
                $user_state = 3;
            }
            if($user_state == 0){
                throw new SCException('没有权限评论！');
            }

            $user_pic = "";
            if($user_state == 1 && $advice['ays'] == 1){
                $user_name = '发起人';
            }else if($user_state == 2 && $advice['handler_anonymous']==1){
                $user_name = '处理人';
            }else if($user_state == 3 && $advice['supervisor_anonymous']==1){
                $user_name = '监督人';
            }else{
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
            }
            $data = array(
                    'to_reply_id' => $reply_id,
                    'adi_id' => $ad_id,
                    'text' => $comment,
                    'user_id' => $user_id,
                    'user_name' => $user_name,
                    'user_pic' => $user_pic,
                    'type' => 0,
                );
            $ret = g('advice_reply') -> add_reply($data);
            if(!$ret){
                throw new SCException('评论失败');
            }

            $this -> remind($ids,$ad_id,2,'您收到一条评论信息，请及时查看！');

            //评论和回复不添加动态
            // if(($user_state == 1&& $advice['ays'] == 1) || ($user_state == 2&& $advice['handler_anonymous']==1) || ($user_state ==3 && $advice['supervisor_anonymous']==1)){
            //     $user_pic = '';
            //     $user_name = $user_name;
            // }else{
            //     $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
            //     $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            // }
            // if($reply_id !=0){
            //     $msg = '回复评论';
            // }else{
            //     $msg = '评论反馈';
            // }
            // $ret = g('advice_trend') -> add_trend($ad_id,$user_id,$user_pic,$user_name,$msg);
            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }

    }


    /**
     * 获取我处理的反馈
     * state: 1,未完成，2:已完成,空为所有
     * title : 搜索关键字
     * time: 时间段
     * classify_id:分类ID
     **/
    private function get_my_deal_list(){
        $fields = array();
        $data = $this -> get_post_data($fields);
        $title = isset($data['title']) ? $data['title'] : "";
        $time = isset($data['time']) ? $data['time'] : "";
        $classify_id = isset($data['classify_id']) ? (int)$data['classify_id'] : "";
        $state = isset($data['state']) ? (int)$data['state'] : "";
        $page = isset($data['page']) ? (int)$data['page'] : 1; 
        $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20; 
        $user_id = $_SESSION[SESSION_VISIT_USER_ID];

        $cond = array();
        if(!empty($classify_id)){
            $class = g('advice_class') -> get_class_by_id($classify_id,'supervisor_ids');
            if(empty($class)){
                throw new SCException('分类不存在！');
            }else{
                $supervisor_ids = json_decode($class['supervisor_ids'],true);
                if(!in_array($user_id,$supervisor_ids)){
                    throw new SCException('没有权限！');
                }
            }
            $cond['i.adc_id ='] = $classify_id;
        }else{
            $class_cond = array(
                    'handler_ids like ' => '%"'.$user_id.'"%',
                    'state=' => 1,
                );

            $ret = g('advice_class') -> get_class_list($class_cond,0,0);
            $classes = $ret['data'];
            if(empty($classes)){
                $return = array(
                        'data' => array(),
                        'count' => 0,
                    );
                cls_resp::echo_ok(cls_resp::$OK,'data',$return);
            }
            $cond['__OR'] = array();
            foreach ($classes as $key => $class) {
                $cond['__OR']['__'.$key.'__i.adc_id = '] = $class['id'];
            }unset($class);
        }

        if($state !== ""){
            if($state == 2){
                 $cond['__OR1'] = array(
                        'i.title_state=' => $state,
                        'i.state=' => 4,
                    );
            }else if($state == 1){
                $cond['i.title_state='] = 1;
            }
        }else{
            $cond['i.title_state!='] = 0;
        }

        if(!empty($title)){
            $cond['i.title like '] = '%'.$title.'%';
        }
        if(!empty($time)){
            $time = explode(' - ', $time);
            if(!empty($time) && 2 == count($time)){
                $cond['i.create_time >='] = strtotime($time[0].' 00:00:00');
                $cond['i.create_time <='] = strtotime($time[1].' 23:59:59');
            }
        }
        
        $cond['i.info_state='] = 1;

        $ret = g('advice_info') -> get_other_list($cond,'i.*,c.name class_name,c.weight,s.create_time see_time',$user_id,$page,$page_size,true);

        cls_resp::echo_ok(cls_resp::$OK,'data',$ret);
    }

    /**
     * 获取我监督的反馈
     * state: 1,未完成，0:已完成,空为所有
     * title : 搜索关键字
     * time: 时间段
     * classify_id:分类ID
     **/
    private function get_my_super_list(){
        try{
            $fields = array();
            $data = $this -> get_post_data($fields);
            $title = isset($data['title']) ? $data['title'] : "";
            $time = isset($data['time']) ? $data['time'] : "";
            $classify_id = isset($data['classify_id']) ? (int)$data['classify_id'] : "";
            $state = isset($data['state']) ? (int)$data['state'] : 1;
            $page = isset($data['page']) ? (int)$data['page'] : 1; 
            $page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20; 
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            $cond = array();
            if(!empty($classify_id)){
                $class = g('advice_class') -> get_class_by_id($classify_id,'supervisor_ids');
                if(empty($class)){
                    throw new SCException('分类不存在！');
                }else{
                    $supervisor_ids = json_decode($class['supervisor_ids'],true);
                    if(!in_array($user_id,$supervisor_ids)){
                        throw new SCException('没有权限！');
                    }
                }
                $cond['i.adc_id ='] = $classify_id;
            }else{
                $class_cond = array(
                        'supervisor_ids like ' => '%"'.$user_id.'"%',
                        'state=' => 1,
                    );

                $ret = g('advice_class') -> get_class_list($class_cond,0,0);
                $classes = $ret['data'];
                if(empty($classes)){
                    $return = array(
                            'data' => array(),
                            'count' => 0,
                        );
                    cls_resp::echo_ok(cls_resp::$OK,'data',$return);
                }
                $cond['__OR'] = array();
                foreach ($classes as $key => $class) {
                    $cond['__OR']['__'.$key.'__i.adc_id = '] = $class['id'];
                }unset($class);
            }

            $cond['i.title_state=']  = $state;

            if(!empty($title)){
                $cond['i.title like '] = '%'.$title.'%';
            }
            if(!empty($time)){
                $time = explode(' - ', $time);
                if(!empty($time) && 2 == count($time)){
                    $cond['i.create_time >='] = strtotime($time[0].' 00:00:00');
                    $cond['i.create_time <='] = strtotime($time[1].' 23:59:59');
                }
            }
            
            $cond['i.info_state='] = 1;
            $ret = g('advice_info') -> get_other_list($cond,'i.*,c.name class_name,c.weight,s.create_time see_time',$user_id,$page,$page_size,true);
            cls_resp::echo_ok(cls_resp::$OK,'data',$ret);
        }catch(SCException $e){
            cls_resp::echo_exp($e);
        }
    }    

    /**
     * 催办
     * id :反馈ID
     **/
    private function press(){
        try{
            g('db') -> begin_trans();
            $fields = array('id');
            $data = $this -> get_post_data($fields);

            $id = isset($data['id']) ? (int)$data['id'] : 0;
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];

            if(empty($id)){
                throw new SCException('未指定反馈');
            }
            
            $advice = g('advice_info') -> get_advice_by_id($id,'adc_id,user_id,ays,primary_id,supervisor_anonymous');
            if(empty($advice)){
                throw new SCException('反馈不存在！');
            }

            $class = g('advice_class') -> get_class_by_id($advice['adc_id'],'id,handler_ids,supervisor_ids');
            if(empty($class)){
                throw new SCException('对应反馈分类不存在！');
            }
            if(!empty($advice['primary_id'])){
                $handler = array($advice['primary_id']);
            }else{
                $handler = json_decode($class['handler_ids'],true);
            }

            $supervisor_ids = json_decode($class['supervisor_ids'],true);

            if(!($advice['user_id'] == $user_id || in_array($user_id, $supervisor_ids))){
                throw new SCException('没有权限');
            }

            if(($advice['user_id'] == $user_id && $advice['ays'] == 1) || (in_array($user_id, $supervisor_ids) && $advice['supervisor_anonymous'] ==1)){
                $user_pic = '';
                if($advice['user_id'] == $user_id){
                    $user_name = '发起人';
                }else{
                    $user_name = '监督人';
                }
            }else{
                $user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
                $user_name = $_SESSION[SESSION_VISIT_USER_NAME];
            }
            $msg = '催办反馈';
            $ret = g('advice_trend') -> add_trend($id,$user_id,$user_pic,$user_name,$msg);
            if(!$ret){
                throw new SCException('催办失败！');
            }

            $this -> remind($handler,$id,2,'您收到一条催办信息，请及时查看！');

            g('db') -> commit();
            cls_resp::echo_ok();
        }catch(SCException $e){
            g('db') -> rollback();
            cls_resp::echo_exp($e);
        }
    }

//-----------------------------内部实现--------------------------------

    /**
     * 信息推送
     **/
    private function remind($user_ids,$act_id,$type,$content){
        try{
            log_write('反馈提醒:'.json_encode($user_ids));
            $com_id = $_SESSION[SESSION_VISIT_COM_ID];

            if($type==1){
                $menu_active = 3;
            }else if($type==2){
                $menu_active = 1;
            }else if($type==3){
                $menu_active = 2;
            }

            $wx_title = '反馈提醒';
            $wx_desc = $content;

            $wx_url = SYSTEM_HTTP_DOMAIN.'index.php?app=advice&a=show_detail&type='.$type.'&id='.$act_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
            $pc_title = '意见反馈：'.$content;        
            $pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app=advice&m=apply&a=show_detail&type='.$type.'&menu_active='.$menu_active.'&id='.$act_id;
            $data = array(
                'title' => $pc_title,
                'url' => $pc_url,
                'app_name' => 'advice',
            );
           
            if(!empty($user_ids)){ 
                $users = g('user') -> get_by_ids($user_ids,'id,acct');
                $accts = array();
                foreach($users as $s){
                    array_push($accts,$s['acct']);
                }unset($s);
            }
            if(empty($accts)){
                log_write('接收人为空，无法推送消息');
                return false;
            }
            parent::send_single_news($accts, $wx_title, $wx_desc, $wx_url);
            
            //PC端提醒
            //g('messager') -> publish_by_ids('pc', array(),$user_ids,$data);
            
            log_write('反馈信息推送成功');
        }catch(SCException $e){
            log_write('反馈信息发送失败');
        }
    }

    /**
     * 保存附件
     **/
    private function save_file($ad_id,$file_array){
        if(empty($file_array) || !is_array($file_array)){
            return false;
        }
        $now_time = time();
        $all_file = array();
        foreach ($file_array as $file) {
            $file_data = array(
                    'ad_id' => $ad_id,
                    'file_name' => $file['file_name'],
                    'file_ext' => $file['file_ext'],
                    'file_size' => $file['file_size'],
                    'file_path' => isset($file['file_path']) ? $file['file_path'] : "",
                    'file_hash' => $file['file_hash'],
                    'info_state' => 1,
                    'update_time' => $now_time,
                    'create_time' => $now_time,

                );
            $ret = g('advice_info') -> save_file($file_data);
            if($ret){
                $tmp = array(
                        'id' => $ret,
                        'file_hash' => $file_data['file_hash'],
                    );
                array_push($all_file, $tmp);
            }
        }

        return $all_file;
    }

}

/* End of this file */
