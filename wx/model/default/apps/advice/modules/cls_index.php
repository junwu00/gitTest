<?php
/**
 * 发表意见
 * @author yangpz
 * @date 2014-10-23
 *
 */
class cls_index extends abs_app_base {
	public function __construct() {
		parent::__construct('advice');
	}
	
	public function add_advice() {
		$id = get_var_value('id');
		//获取分类列表
		$get_list_url = $this -> app_domain.'&m=ajax&cmd=101';
		//保存反馈信息
		$save_advice_url = $this-> app_domain.'&m=ajax&cmd=102';
		//转调添加结果页面
		$save_succese_url = $this-> app_domain.'&a=save_succese';
		//我的反馈列表
		$mine_list_url = $this -> app_domain.'&a=mine_list';
		//删除uurl
		$del_advice_url = $this -> app_domain.'&m=ajax&cmd=113';
		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		$user_pic = $_SESSION[SESSION_VISIT_USER_PIC];
		log_write($user_pic);
		$user_dept = g('dept') -> get_dept_by_id($_SESSION[SESSION_VISIT_USER_DEPT]);
		$user_dept_name = $user_dept['name'];
		g('smarty') -> assign('del_advice_url', $del_advice_url);
		
		g('smarty') -> assign('id', $id);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('save_advice_url', $save_advice_url);
		g('smarty') -> assign('save_succese_url', $save_succese_url);
		g('smarty') -> assign('mine_list_url', $mine_list_url);
		g('smarty') -> assign('user_name', $user_name);
		g('smarty') -> assign('user_pic', $user_pic);
		g('smarty') -> assign('user_dept_name', $user_dept_name);
		if(empty($id)){
			g('smarty') -> assign('menu_name', '发起反馈');
			g('smarty') -> assign('title', '发起反馈');
		}else{
			g('smarty') -> assign('menu_name', '编辑反馈');
			g('smarty') -> assign('title', '编辑反馈');
			$info = g('advice_info') -> get_advice_detail($id,1);
			g('smarty') -> assign('info', $info);
		}
		g('smarty') -> show($this -> temp_path.'add_advice.html');
	}
	
	
	public function save_succese(){
		$id = (int)get_var_value('id');
		$anonymous = (int)get_var_value('anonymous');
		//我的反馈列表
		$mine_list_url = $this -> app_domain.'&a=mine_list&state=1';
		//反馈详情
		$show_detail_url = $this -> app_domain.'&a=show_detail&type=1&id='.$id;
		//撤回反馈信息
		$backout_url = $this -> app_domain.'&m=ajax&cmd=112&id='.$id;
		//反馈对象
		$info = g('advice_info') -> get_advice_detail($id,1);
		
		g('smarty') -> assign('id', $id);
		g('smarty') -> assign('info', $info);
		g('smarty') -> assign('anonymous', $anonymous);
		g('smarty') -> assign('mine_list_url', $mine_list_url);
		g('smarty') -> assign('show_detail_url', $show_detail_url);
		g('smarty') -> assign('backout_url', $backout_url);
		g('smarty') -> assign('menu_name', '发起反馈成功');
		g('smarty') -> assign('title', '发起反馈成功');
		g('smarty') -> show($this -> temp_path.'add_advice_success.html');
		
	}
	
	public function mine_list() {
		$state = (int)get_var_value('state');
		if(empty($state) && $state != 0){
			$state = 1;
		}
		//获取列表数据
		$get_list_url = $this -> app_domain.'&m=ajax&cmd=103';
		//反馈详情
		$show_detail_url = $this -> app_domain.'&a=show_detail';
		//this_url
		$mine_list_url = $this -> app_domain.'&a=mine_list';
		//编辑url
		$edit_advice_url = $this-> app_domain.'&a=add_advice';
		
		
		g('smarty') -> assign('state', $state);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('show_detail_url', $show_detail_url);
		g('smarty') -> assign('mine_list_url', $mine_list_url);
		g('smarty') -> assign('edit_advice_url', $edit_advice_url);
		g('smarty') -> assign('menu_name', '我发起的');
		g('smarty') -> assign('title', '我发起的');
		g('smarty') -> show($this -> temp_path.'advice_mine_list.html');
	}
	
	public function deal_list() {
		$state = (int)get_var_value('state');
		if(empty($state)){
			$state = 1;
		}
		//获取列表数据
		$get_list_url = $this -> app_domain.'&m=ajax&cmd=105';
		//反馈详情
		$show_detail_url = $this -> app_domain.'&a=show_detail';
		//this_url
		$deal_list_url = $this -> app_domain.'&a=deal_list';
		
		g('smarty') -> assign('state', $state);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('show_detail_url', $show_detail_url);
		g('smarty') -> assign('deal_list_url', $deal_list_url);
		g('smarty') -> assign('menu_name', '我处理的');
		g('smarty') -> assign('title', '我处理的');
		g('smarty') -> show($this -> temp_path.'advice_deal_list.html');
	}
	
	public function supervise_list() {
		$state = (int)get_var_value('state');
		if(empty($state)){
			$state = 1;
		}
		//获取列表数据
		$get_list_url = $this -> app_domain.'&m=ajax&cmd=106';
		//反馈详情
		$show_detail_url = $this -> app_domain.'&a=show_detail';
		//this_url
		$supervise_list = $this -> app_domain.'&a=supervise_list';
		
		g('smarty') -> assign('state', $state);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('show_detail_url', $show_detail_url);
		g('smarty') -> assign('supervise_list', $supervise_list);
		g('smarty') -> assign('menu_name', '我监督的');
		g('smarty') -> assign('title', '我监督的');
		g('smarty') -> show($this -> temp_path.'advice_supervise_list.html');
	}
	
	public function show_detail() {
		$id = (int)get_var_value('id');
		$type = (int)get_var_value('type');
		$info = g('advice_info') -> get_advice_detail($id,$type);
		$comment_url = $this -> app_domain.'&m=ajax&cmd=111';
		$deal_dynamic_url = $this -> app_domain.'&m=ajax&cmd=110';
		$add_comment_url = $this -> app_domain.'&m=ajax&cmd=104';
		$comment_list_url = $this-> app_domain.'&a=comment_list&id='.$id;
		$edit_advice_url = $this-> app_domain.'&a=add_advice';
		$publish_advice_url = $this -> app_domain.'&m=ajax&cmd=107';
		$del_advice_url = $this -> app_domain.'&m=ajax&cmd=113';
		$back_out_advice_url = $this -> app_domain.'&m=ajax&cmd=112';
		$press_deal_advice_url = $this -> app_domain.'&m=ajax&cmd=115';
		$active_advice_url = $this -> app_domain.'&m=ajax&cmd=114';
		$deal_advice_url = $this -> app_domain.'&m=ajax&cmd=109';
		$complete_url = $this -> app_domain.'&m=ajax&cmd=108';
		$mine_list_url = $this -> app_domain.'&a=mine_list';
		$deal_list_url = $this -> app_domain.'&a=deal_list';
		$down_load_url = $this -> app_domain.'&m=ajax&cmd=116';
		$colose_advice_url = $this -> app_domain.'&m=ajax&cmd=117';
		$cancel_deal_advice = $this -> app_domain.'&m=ajax&cmd=118';
		$to_reply_url = $this -> app_domain.'&a=show_reply';
		//取值：0-其他、1-ios、2-android、3-wp
        $plat_id = g('common') -> get_mobile_platid();
        
        $info['detail'] = str_replace('\n', '<br>', json_encode($info['detail']));
	    $info['detail'] = json_decode($info['detail']);

        g('smarty') -> assign('qy_media_url', MEDIA_URL_PREFFIX);
		g('smarty') -> assign('id', $id);
		g('smarty') -> assign('type', $type);
		g('smarty') -> assign('info', $info);  
		g('smarty') -> assign('json_info', json_encode($info));
		g('smarty') -> assign('comment_url', $comment_url);
		g('smarty') -> assign('deal_dynamic_url', $deal_dynamic_url);
		g('smarty') -> assign('add_comment_url', $add_comment_url);
		g('smarty') -> assign('comment_list_url', $comment_list_url);
		g('smarty') -> assign('edit_advice_url', $edit_advice_url);
		g('smarty') -> assign('this_user_id', $_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('publish_advice_url', $publish_advice_url);
		g('smarty') -> assign('del_advice_url', $del_advice_url);
		g('smarty') -> assign('back_out_advice_url', $back_out_advice_url);
		g('smarty') -> assign('press_deal_advice_url', $press_deal_advice_url);
		g('smarty') -> assign('active_advice_url', $active_advice_url);
		g('smarty') -> assign('deal_advice_url', $deal_advice_url);
		g('smarty') -> assign('complete_url', $complete_url);
		g('smarty') -> assign('mine_list_url', $mine_list_url);
		g('smarty') -> assign('deal_list_url', $deal_list_url);
		g('smarty') -> assign('down_load_url', $down_load_url);
		g('smarty') -> assign('colose_advice_url', $colose_advice_url);
		g('smarty') -> assign('cancel_deal_advice_url', $cancel_deal_advice);
		g('smarty') -> assign('plat_id', $plat_id);
		g('smarty') -> assign('to_reply_url', $to_reply_url);
		
		g('smarty') -> assign('menu_name', '反馈信息');
		g('smarty') -> assign('title', '反馈信息');
		g('smarty') -> show($this -> temp_path.'advice_detail.html');
	}
	
	public function comment_list() {
		$id = (int)get_var_value('id');
		//获取列表数据
		$get_list_url = $this -> app_domain.'&m=ajax&cmd=111';
		//评论
		$add_comment_url = $this -> app_domain.'&m=ajax&cmd=104';
		//回复评论列表
		$to_reply_url = $this -> app_domain.'&a=show_reply';
		//当前反馈
		$info = g('advice_info') -> get_advice_by_id($id);
		//是否隐藏评论回复
		$hide_comment = false;
		if($info['state'] == 4 || $info['state'] == 5){
			$hide_comment = true;
		}
		
		g('smarty') -> assign('id', $id);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('add_comment_url', $add_comment_url);
		g('smarty') -> assign('to_reply_url', $to_reply_url);
		g('smarty') -> assign('hide_comment', $hide_comment);
		g('smarty') -> assign('menu_name', '评论列表');
		g('smarty') -> assign('title', '评论列表');
		g('smarty') -> show($this -> temp_path.'advice_comment_list.html');
	}
	
	public function show_reply() {
		$id = (int)get_var_value('id');
		$advice_id = (int)get_var_value('advice_id');
		//获取列表数据
		$get_reply_url = $this -> app_domain.'&m=ajax&cmd=119';
		//评论
		$ajax_to_reply_url = $this -> app_domain.'&m=ajax&cmd=104';
		//当前评论
		$comment = g('advice_reply') -> get_reply($advice_id,$id);
		//当前反馈
		$info = g('advice_info') -> get_advice_by_id($advice_id);
		//是否隐藏评论回复
		$hide_comment = false;
		if($info['state'] == 4 || $info['state'] == 5){
			$hide_comment = true;
		}
		g('smarty') -> assign('id', $id);
		g('smarty') -> assign('advice_id', $advice_id);
		g('smarty') -> assign('get_reply_url', $get_reply_url);
		g('smarty') -> assign('ajax_to_reply_url', $ajax_to_reply_url);
		g('smarty') -> assign('comment', $comment);
		g('smarty') -> assign('hide_comment', $hide_comment);
		g('smarty') -> assign('menu_name', '回复');
		g('smarty') -> assign('title', '回复');
		g('smarty') -> show($this -> temp_path.'advice_reply_list.html');
	}
	
	public function advice_test() {
		g('smarty') -> show($this -> temp_path.'advice_test.html');
	}
}
