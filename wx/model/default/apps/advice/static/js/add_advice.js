var mupload;
var mediaUrl = $('#media-url').val();
$(document).ready(function () {
	init_advice_classify();
	init_file_download_pre();
	init_upload();
	midImg();
});

function init_file_download_pre() {
	$('.file-list').children().unbind().click(function () {
		var opt = {};
		opt.data = {};
		opt.data.el = $(this);
		opt.data.name = $(this).attr('data-name');
		opt.data.key = $(this).attr('data-key');
		opt.data.ext = $(this).attr('data-ext');
		opt.data.path = $(this).attr('data-path');
		console.log(opt);
		opt.menus = [];
		var m1 = {};
		m1.id = 1;
		m1.name = '删除';
		m1.fun = function (el, m, o) {
			o.el.remove();
			bottom_menu_action.hide();
		}
		opt.menus.push(m1);
		bottom_menu_action.init(opt);
	})
}

function init_advice_classify() {
	var opt = {};
	opt.url = $('#get_list_url').val();
	opt.success = function (response) {
		if (response.errcode != 0) {
			frame_obj.alert(response.errmsg);
			return;
		} else {
			if (response.data.length == 0) {
				$('.container').addClass('hide');
				$('#bottom-menu').children().addClass('hide');
				$('#bottom-menu').addClass('hide');
				$('.empty_classify').removeClass('hide');
				return;
			}
			var html = '';
			for (var i = 0; i < response.data.length; i++) {
				if ($('#class_id').val().length > 0 && response.data[i].id == $.trim($('#class_id').val())) {
					html += '<option selected="selected"  value="' + response.data[i].id + '" data-anonymous="' + response.data[i].anonymous + '">' + response.data[i].name + '</option>';
				} else {
					html += '<option class="options" value="' + response.data[i].id + '" data-anonymous="' + response.data[i].anonymous + '">' + response.data[i].name + '</option>';
				}
			}
			$('#type').append(html);
			bind_type_change();
			if ($('#class_id').val().length > 0) {
				bind_type_change($('#type')[0]);
				init_edit_open_anonymous();
			}
			$('#type').change(function () {
				bind_type_change(this);
			});
		}
	}
	frame_obj.post(opt);
}

function init_edit_open_anonymous() {
	if ($('#class_id').val().length > 0 && $('#ays').val() == 1) {
		$('.anonymous_action').children('input').attr('checked', 'checked');
		$('.anonymous_action').parent().find('p[class="anonymous-user"]').find('.un_open_anonymous').addClass('hide');
		$('.anonymous_action').parent().find('p[class="anonymous-user"]').find('.open_anonymous').removeClass('hide');
	}
}

function bind_type_change(that) {
	var anonymous = $(that).find("option:selected").attr('data-anonymous');
	if (anonymous == 0) {
		$('.unanonymous').removeClass('hide');
		$('.anonymous').addClass('hide');
	} else if (anonymous == 1) {
		$('.unanonymous').addClass('hide');
		$('.anonymous').removeClass('hide');
		$('.anonymous_action').children('input').removeAttr('checked');
		$('.anonymous_action').parent().find('p[class="anonymous-user"]').find('.un_open_anonymous').removeClass('hide');
		$('.anonymous_action').parent().find('p[class="anonymous-user"]').find('.open_anonymous').addClass('hide');
	} else {
		$('.unanonymous').addClass('hide');
		$('.anonymous').addClass('hide');
	}
}

function open_anonymous(that) {
	if (that.checked) {
		$(that).parent().parent().find('p[class="anonymous-user"]').find('.un_open_anonymous').addClass('hide');
		$(that).parent().parent().find('p[class="anonymous-user"]').find('.open_anonymous').removeClass('hide');
	} else {
		$(that).parent().parent().find('p[class="anonymous-user"]').find('.un_open_anonymous').removeClass('hide');
		$(that).parent().parent().find('p[class="anonymous-user"]').find('.open_anonymous').addClass('hide');
	}
}


function init_upload() {
	// var media = 'http://svc.nh.order.3ruler.com:9111/data/'
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form: '#formToUpload',
		uploadUrl: unify_upload_url,
		checkUrl: unify_check_url,
		mediaUrl: $('#media-url').val(),
		scaning: function (filePath, fileName, isImg) {
			$('#bottom-menu').children().attr('disabled', 'disabled');
			$('button.btn.btn-green').attr('disabled', 'disabled');
			$('button.btn.btn-white').attr('disabled', 'disabled');
			$('button.btn.btn-green').addClass('btn-green-dis');
			$('button.btn.btn-green').removeClass('btn-green');
			$('button.btn.btn-white').addClass('btn-white-dis');
			$('button.btn.btn-white').removeClass('btn-white');
			var ext = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();
			if (ext == 'txt' || ext == 'xml') {
				file_icon_css = 'file-type-txt';
			} else if (ext == 'doc' || ext == 'docx') {
				file_icon_css = 'file-type-doc';
			} else if (ext == 'xls' || ext == 'xlsx') {
				file_icon_css = 'file-type-xls';
			} else if (ext == 'ppt' || ext == 'pptx') {
				file_icon_css = 'file-type-ppt';
			} else if (ext == 'pdf') {
				file_icon_css = 'file-type-pdf';
			} else if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
				file_icon_css = 'file-type-img';
			} else if (ext == 'zip' || ext == 'rar') {
				file_icon_css = 'file-type-zip';
			} else if (ext == 'rm' || ext == 'rmvb' || ext == 'wmv' || ext == 'avi' || ext == 'mpg' || ext == 'mpeg' || ext == 'mp3' || ext == 'wma' || ext == 'wav' || ext == 'amr') {
				file_icon_css = 'file-type-mp3';
			} else if (ext == 'mp4') {
				file_icon_css = 'file-type-mp4';
			} else {
				file_icon_css = 'file-type-others';
			}
			var html = '';
			html += '<div data-draft="true" class="file-item file-type-icon ' + file_icon_css + '">';
			html += '<span class="file-name">' + fileName + '</span>';
			html += '<span class="file-size">扫描文件</span>';
			html += '</div>';
			if ($('.file-list').children().length > 0) {
				$($('.file-list').children()[0]).before(html);
			} else {
				$('.file-list').append(html);
			}
			return true;
		},
		scaned: function () {
			if ($('.file-list').children().lenght > 0 && $($('.file-list').children()[0]).find('span[class=file-size]').attr('data-draft') == 'true') {
				$($('.file-list').children()[0]).find('span[class=file-size]').html('扫描完成');
			}
		},
		startUpload: function () {
			$($('.file-list').children()[0]).find('span[class=file-size]').html('上传中');
		},
		process: function (percent) {
		},
		success: function (data, isImg) {
			$('button.btn.btn-green-dis').removeAttr('disabled');
			$('button.btn.btn-white-dis').removeAttr('disabled');
			$('button.btn.btn-green-dis').addClass('btn-green');
			$('button.btn.btn-green-dis').removeClass('btn-green-dis');
			$('button.btn.btn-white-dis').addClass('btn-white');
			$('button.btn.btn-white-dis').removeClass('btn-white-dis');
			$($('.file-list').children()[0]).find('span[class=file-size]').html('上传完成');
			$($('.file-list').children()[0]).remove();
			var name = data.file_name;
			var size = data.filesize;
			var ext = data.file_ext;
			var hash = data.hash;
			var path = data.path;
			var size_desc = size / 1024 / 1024 > 1 ? (size / 1024 / 1024).toFixed(2) + 'MB' : (size / 1024).toFixed(2) + 'KB'
			var file_icon_css = "";
			if (ext == 'txt' || ext == 'xml') {
				file_icon_css = 'file-type-txt';
			} else if (ext == 'doc' || ext == 'docx') {
				file_icon_css = 'file-type-doc';
			} else if (ext == 'xls' || ext == 'xlsx') {
				file_icon_css = 'file-type-xls';
			} else if (ext == 'ppt' || ext == 'pptx') {
				file_icon_css = 'file-type-ppt';
			} else if (ext == 'pdf') {
				file_icon_css = 'file-type-pdf';
			} else if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
				file_icon_css = 'file-type-img';
			} else if (ext == 'zip' || ext == 'rar') {
				file_icon_css = 'file-type-zip';
			} else if (ext == 'rm' || ext == 'rmvb' || ext == 'wmv' || ext == 'avi' || ext == 'mpg' || ext == 'mpeg' || ext == 'mp3' || ext == 'wma' || ext == 'wav' || ext == 'amr') {
				file_icon_css = 'file-type-mp3';
			} else if (ext == 'mp4') {
				file_icon_css = 'file-type-mp4';
			} else {
				file_icon_css = 'file-type-others';
			}
			var html = '';
			html += '<div data-new="true" class="file-item file-type-icon ' + file_icon_css + '" data-path="' + path + '" data-size="' + size + '" data-ext="' + ext + '" data-key="' + hash + '" data-name="' + name + '">';
			html += '<span class="file-name">' + name + '</span>';
			html += '<span class="file-size">' + size_desc + '</span>';
			if (file_icon_css == 'file-type-img') {
				html += '<div id="imgmid"  />'
				setTimeout(function () {
					$('#imgmid').css({
						'background-image': 'url(' + mediaUrl + path + ')',
						"width": "28px",
						"height": "28px",
						"position": "absolute",
						"top": "2px",
						"left": "3px",
					})
				}, 500)
			}
			html += '</div>';
			if ($('.file-list').children().length > 0) {
				$($('.file-list').children()[0]).before(html);
			} else {
				$('.file-list').append(html);
			}
			init_file_download_pre();
		},
		error: function (resp) {
			$($('.file-list').children()[0]).remove();
			$('button.btn.btn-green-dis').removeAttr('disabled');
			$('button.btn.btn-white-dis').removeAttr('disabled');
			$('button.btn.btn-green-dis').addClass('btn-green');
			$('button.btn.btn-green-dis').removeClass('btn-green-dis');
			$('button.btn.btn-white-dis').addClass('btn-white');
			$('button.btn.btn-white-dis').removeClass('btn-white-dis');
			fail_screen(resp.errmsg);
		}
	});
}

function get_data(type) {
	var data = {};
	data.state = type;
	if ($('#id').val().length > 0) {
		data.id = $('#id').val();
	}
	data.classify_id = $('#type').val();
	data.title = $('#name').val();
	data.content = $('#desc').val();
	if ($('.anonymous').attr('class').indexOf('hide') == -1) {
		data.ays = $('.anonymous').find('div').find('input')[0].checked ? 1 : 0;
	} else {
		data.ays = 0;
	}
	data.file_hash = [];
	$('.file-list').children().each(function () {
		var item = new Object();
		item.file_name = $(this).attr("data-name");
		item.file_hash = $(this).attr("data-key");
		item.file_path = $(this).attr("data-path");
		item.file_ext = $(this).attr("data-ext");
		item.file_size = $(this).attr("data-size");
		data.file_hash.push(item);
	});
	return data;
}

// 确认弹窗
function check(data) {
	if (data.classify_id == '' && data.state == 1) {
		var opt = { desc: '请选择反馈类型' };
		alert(opt);
		return false;
	}
	if (!check_data(data.title, 'notnull')) {
		var opt = { desc: '请输入反馈标题' };
		alert(opt);
		return false;
	}
	if (!check_data(data.content, 'notnull') && data.state == 1) {
		var opt = { desc: '请输入反馈内容' };
		alert(opt);
		return false;
	}
	return true;
}

function save(type) {
	var data = get_data(type);
	if (check(data)) {
		if (type == 0) {
			lock_screen('保存中');
		} else {
			lock_screen('提交中');
		}
		var opt = {};
		opt.url = $('#save_advice_url').val();
		opt.data = JSON.stringify(data);
		opt.success = function (response) {
			lock_screen();
			if (response.errcode != 0) {
				if (type == 0) {
					fail_screen(response.errmsg)
				} else {
					fail_screen(response.errmsg)
				}
				return;
			} else {
				if (type == 0) {
					success_screen('保存成功')
					location.href = $('#mine_list_url').val() + '&state=' + 0;
				} else {
					success_screen('提交成功')
					var id = $('#id').val().length > 0 ? $('#id').val() : response.data.id;
					location.href = $('#save_succese_url').val() + '&anonymous=' + data.ays + '&id=' + id;
				}
			}
		}
		frame_obj.post(opt);
	}
}


function del_advice(id) {
	var data = {};
	data.desc = "确认删除该反馈？";
	data.ok_callback = function () {
		lock_screen('删除中');
		var opt = {};
		opt.url = $('#del_advice_url').val();
		opt.data = JSON.stringify({ id: $('#id').val() });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				success_screen('删除成功');
				location.href = $('#mine_list_url').val() + '&state=0';
			} else {
				alert({ title: '删除失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm(data);
}


// 缩略图
function midImg() {
	$('.file-list div').each(function (index) {
		if ($(this).attr('data-ext') == 'jpeg' || $(this).attr('data-ext') == 'png'
			|| $(this).attr('data-ext') == 'jpg') {
			var urlImg = $(this).attr('data-img')
			var imgDom = '<div class="imgmid" id="mid' + index + '"/>'
			$(this).append(imgDom)
			$('#mid' + index).addClass('abc' + index).css('background-image', 'url(' + mediaUrl + urlImg + ')')
			// $('.imgmid').css('background-image', 'url(' + urlImg + ')')
		}
	})
}