$(document).ready(function () {
	init_deal_comment_change();
	init_deal_data();
	init_comment_data();
	init_file_download_pre();
	midImg();
});

function init_file_download_pre() {
	$('.file-list').children().click(function () {
		var opt = {};
		opt.data = {};
		opt.data.name = $(this).attr('data-name');
		opt.data.key = $(this).attr('data-key');
		opt.data.ext = $(this).attr('data-ext');
		opt.data.path = $(this).attr('data-path');
		opt.data.file_id = $(this).attr('data-id');
		opt.menus = [];
		if (MS_document_pre.is_pre(opt.data.ext)) {
			var m1 = {};
			m1.id = 1;
			m1.name = '预览';
			m1.icon_html = '<span><i class="icon icon-eye-open"></i></span>';
			m1.fun = function (el, m, o) {
				MS_document_pre.init_pre(o.ext, o.key, o.name);
			}
			opt.menus.push(m1);
			var m2 = {};
			m2.id = 2;
			m2.name = '下载';
			m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			m2.fun = function (el, m, o) {
				file_down(o.file_id);
			}
			opt.menus.push(m2);
			bottom_menu_action.init(opt);
		} else if (image_pre.is_pre(opt.data.ext)) {
			image_pre.init_pre(opt.data.ext, opt.data.key);
		} else {
			var m2 = {};
			m2.id = 1;
			m2.name = '下载';
			m2.icon_html = '<span><i class="icon icon-download-alt"></i></span>';
			m2.fun = function (el, m, o) {
				file_down(o.file_id);
			}
			opt.menus.push(m2);
			bottom_menu_action.init(opt);
		}
	})
}

function file_down(f_id) {
	bottom_menu_action.hide();
	var id = $("#id").val();
	var url = '';
	if ($('#plat_id').val() == 1) {
		url = $('#down_load_url').val() + '&is_push=1&id=' + f_id + '&advice_id=' + $("#id").val();
	} else {
		url = $('#down_load_url').val() + '&id=' + f_id + '&advice_id=' + $("#id").val();
	}
	lock_screen('下载中……');
	$.ajax({
		url: url,
		type: 'post',
		data: '',
		dataType: 'json',
		success: function (data) {
			unlock_screen();
			if (data.errcode != 0) {
				alert({ title: data.errmsg });
				return;
			}
			alert({ desc: '附件成功推送到信息框！' });
		},
		error: function (data, status, e) {
			unlock_screen();
			location.href = $('#down_load_url').val() + '&id=' + f_id + '&advice_id=' + $("#id").val();
		}
	});
}

function init_deal_comment_change() {
	$('.advice-comment-msg').children('ul').find('li').click(function () {
		$(this).parent().children().removeClass('active');
		$(this).parent().parent().children('div').addClass('hide');
		$(this).addClass('active');
		$(this).parent().parent().children('div[class^="' + $(this).attr('data-show') + '"]').removeClass('hide');
	});
}

function init_deal_data() {
	$('.deal-msg').html('');
	var opt = {};
	opt.url = $('#deal_dynamic_url').val();
	opt.data = JSON.stringify({ id: $('#id').val() });
	opt.success = function (response) {
		var html = '';
		for (var i = 0; i < response.data.data.length; i++) {
			var ob = response.data.data[i];
			var name = ob.user_name == "" ? '匿名' : ob.user_name;
			var pic = ob.user_pic == "" ? '/static/image/face.png' : ob.user_pic;
			var desc = ob.desc.slice(ob.desc.indexOf("，") + 1);
			var time_desc = frame_obj.recent_time(ob.create_time); 
			html += '<div class="item">';
			html += '<span class="deal-face-icon"><img src="' + pic + '" onerror="this.src=\'/static/image/face.png\'"></span>';
			html += '<p class="deal-desc">' + name + ob.desc.split("，备注")[0]; +'</p>';
			html += '<p class="deal-time">' + time_desc + '</p>';
			if (desc !== undefined && desc.length !== 3) {
				html += '<p class="beizhu">' + desc + '</p>';
			}
			html += '</div>';
		}
		$('.deal-msg').html(html);
	};
	frame_obj.post(opt);

}

function init_comment_data() {
	$('#comment-list').html('');
	var opt = {};
	opt.url = $('#comment_url').val();
	opt.data = JSON.stringify({ id: $('#id').val(), page: 1, page_size: 5 });
	opt.success = function (response) {
		if (response.data.count > 5) {
			$('#comment-more').removeClass('hide');
		}
		var html = '';
		for (var i = 0; i < response.data.data.length; i++) {
			var ob = response.data.data[i];
			var create_time = frame_obj.recent_time(ob.create_time);
			var reply_count = ob.reply_count;
			html += '<div class="comment-item" data-reply-count="' + reply_count + '" onclick="to_reply(' + ob.id + ')" data-reply="' + reply_count + '">';
			html += '<img src="' + ob.user_pic + '" onerror="this.src=\'static/image/face.png\'">';
			html += '<p class="comment-name">' + ob.user_name + '<span class="time">' + create_time + '</span></p>';
			html += '<p class="comment-text">' + ob.text + '</p>';
			//html += '<p class="comment-time hide"><span class="time">'+ create_time +'</span><span class="reply hide"><span class="icon icon-comment"></span>回复&nbsp;('+ reply_count +')</span></p>';
			html += '</div>';
		}
		if (html == '') {
			html += '<div class="comment-item empty-comment">';
			html += '<div><img src="/apps/advice/static/images/content_no.png" class="commentImg"></div>';
			html += '<p>暂时没有内容</p>';
			html += '</div>';
		}
		$('#comment-list').html(html);
		// add start
		let htmlText = $('.advice-comment-msg').children('ul').find('li[data-show="comment-block"]').html();
		$('.advice-comment-msg').children('ul').find('li[data-show="comment-block"]').html('评论信息' + '<span class="commentCount">(' + response.data.count + ')</span>');
		console.log($('.advice-comment-msg').children('ul').find('li[data-show="comment-block"]'));
		// add end
	};
	frame_obj.post(opt);
}

function add_comment() {
	if ($.trim($('#comment-text').val()).length == 0) {
		alert({ desc: '请输入评论!' });
		return false;
	}
	lock_screen('提交评论');
	var opt = {};
	opt.url = $('#add_comment_url').val();
	opt.data = JSON.stringify({ ad_id: $('#id').val(), comment: $.trim($('#comment-text').val()) });
	opt.success = function (response) {
		unlock_screen();
		if (response.errcode == 0) {
			success_screen('评论成功');
			$('#comment-text').val('');
			init_comment_data();
			hideCommentDiv();
			//init_deal_data();
		} else {
			alert({ title: '评论失败', desc: response.errmsg })
		}
	};
	frame_obj.post(opt);
}

function edit_advice(id) {
	location.href = $('#edit_advice_url').val() + '&id=' + id;
}

function back_out_advice(id) {
	var data = {};
	data.desc = "确认撤回该反馈？";
	data.ok_callback = function () {
		lock_screen('撤回中');
		var opt = {};
		opt.url = $('#back_out_advice_url').val();
		opt.data = JSON.stringify({ id: $('#id').val() });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				success_screen('撤回成功');
				location.href = $('#mine_list_url').val() + '&state=1';
			} else {
				alert({ title: '撤回失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm(data);
}

function press_deal_advice(id) {
	lock_screen('催办中');
	var opt = {};
	opt.url = $('#press_deal_advice_url').val();
	opt.data = JSON.stringify({ id: $('#id').val() });
	opt.success = function (response) {
		unlock_screen();
		if (response.errcode == 0) {
			success_screen('催办成功');
		} else {
			alert({ title: '催办失败', desc: response.errmsg })
		}
	};
	frame_obj.post(opt);
}

function active_advice(id) {
	var data = {};
	data.desc = "确认激活该反馈？";
	data.ok_callback = function () {
		lock_screen('激活中');
		var opt = {};
		opt.url = $('#active_advice_url').val();
		opt.data = JSON.stringify({ id: $('#id').val() });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				success_screen('激活成功');
				location.reload();
			} else {
				alert({ title: '激活失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm(data);
}

function deal_advice(id) {
	var data = {};
	data.title = '确认主导处理？';
	data.desc = "主导处理后，反馈将被锁定，其他处理人将无法对反馈进行处理操作！";
	data.ok_callback = function () {
		lock_screen('主导中');
		var opt = {};
		opt.url = $('#deal_advice_url').val();
		opt.data = JSON.stringify({ id: $('#id').val() });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				success_screen('主导成功');
				location.reload();
			} else {
				alert({ title: '主导失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm(data);
}

function complete_advice(id) {
	var c_opt = {};
	c_opt.placeholder = "请填写处理意见";
	c_opt.ok_callback = function (value) {
		lock_screen('数据提交中');
		var opt = {};
		opt.url = $('#complete_url').val();
		opt.data = JSON.stringify({ id: $('#id').val(), comment: value });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				confirm_input.hide();
				success_screen('完成');
				location.href = $('#deal_list_url').val() + '&state=1';
			} else {
				alert({ title: '完成失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm_input.init(c_opt);
}

function colose_advice(id) {
	var c_opt = {};
	c_opt.placeholder = "备注一下吧";
	c_opt.ok_callback = function (value) {
		lock_screen('关闭中');
		var opt = {};
		opt.url = $('#colose_advice_url').val();
		opt.data = JSON.stringify({ id: $('#id').val(), comment: value });
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode == 0) {
				success_screen('关闭成功');
				location.href = $('#deal_list_url').val();
			} else {
				alert({ title: '关闭失败', desc: response.errmsg })
			}
		};
		frame_obj.post(opt);
	}
	confirm_input.init(c_opt);
}

function cancel_deal_advice(id) {
	lock_screen('取消主导中');
	var opt = {};
	opt.url = $('#cancel_deal_advice_url').val();
	opt.data = JSON.stringify({ id: $('#id').val() });
	opt.success = function (response) {
		unlock_screen();
		if (response.errcode == 0) {
			success_screen('取消主导成功');
			location.reload();
		} else {
			alert({ title: '取消主导失败', desc: response.errmsg })
		}
	};
	frame_obj.post(opt);
}

function to_reply(id) {
	return;
	location.href = $('#to_reply_url').val() + '&id=' + id + '&advice_id=' + $('#id').val();
}

function showCommentDiv(params) {
	$('#commentDiv').removeClass('hide')
}

function hideCommentDiv(params) {
	$('#commentDiv').addClass('hide')
}

// 缩略图
function midImg() {
	$('.file-list div').each(function (index) {
		if ($(this).attr('data-ext') == 'jpeg' || $(this).attr('data-ext') == 'png'
			|| $(this).attr('data-ext') == 'jpg') {
			var urlImg = $(this).attr('data-img')
			//console.log(urlImg, 'url');
			var imgDom = '<div class="imgmid" id="mid' + index + '"/>'
			$(this).append(imgDom)
			$('#mid' + index).addClass('abc' + index).css('background-image', 'url(' + urlImg + ')')
			// $('.imgmid').css('background-image', 'url(' + urlImg + ')')
		}
	})
}