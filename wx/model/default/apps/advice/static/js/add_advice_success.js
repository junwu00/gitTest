$(document).ready(function() {
	var html_height=$('html').height();
	var main_height=$('.main').height()-6;
	var top = (html_height - main_height)/2;
	$('.main').css({'margin-top':top+'px'});
});

function backout(that){
	var data = {};
	data.desc = "确认撤回该反馈？";
	data.ok_callback = function(){
		var opt = {};
		opt.url = $(that).attr('href');
		opt.data = JSON.stringify({id:$('#id').val()});
		opt.success = function(response){
			if(response.errcode != 0){
				var opt = {};
				opt.title = '撤回失败';
				opt.desc = response.errmsg;
				alert(opt);
			}else{
				success_screen('撤回成功');
				location.href = $('#mine_list_url').val();
			}
		}
		frame_obj.post(opt);
	}
	confirm(data);
}

function go_detail(that){
	location.href = $(that).attr('data-url');
}