
$(document).ready(function() {
	load_comment_list(true);
	app_auto_hight();
	init_scroll_load();
});

function load_comment_list(is_first){
	if(is_first){
		$('.comment-list').attr('data-page',0);
		$('.comment-list').children('div.comment-item').remove();
	}
	var page = $('.comment-list').attr('data-page')-0;
	var data = {};
	data.page = page+1;
	data.page_size = 20;
	data.id = $('#id').val();
	var opt = {};
	opt.url = $('#get_list_url').val();
	opt.data = JSON.stringify(data);
	opt.success = function(response){
		if(response.data.data.length < 20){
			scroll_load_obj.scroll_load_end($('.container'));
		}
		scroll_load_obj.remove_scroll_waiting($('.comment-list'));
		scroll_load_obj.set_is_scroll_loading_false($('.container'));
		$('.comment-list').attr('data-page',data.page);
		if(data.page = 1){
			$('#comment-count').html(response.data.count);
		}
		var html = '';
		for(var i = 0; i < response.data.data.length; i++){
			var ob = response.data.data[i];
			var create_time = frame_obj.recent_time(ob.create_time);
			var reply_count = ob.reply_count;
			html += '<div class="comment-item" data-reply-count="'+ reply_count +'" onclick="to_reply(this,'+ob.id+')" data-reply="'+reply_count+'">';
			html += '<img src="'+ ob.user_pic +'" onerror="this.src=\'static/image/face.png\'">';
			html += '<p class="comment-name">'+ ob.user_name +'<span class="time">'+ create_time +'</span></p>';
			html += '<p class="comment-text">'+ ob.text +'</p>';
			//html += '<p class="comment-time hide"><span class="time">'+ create_time +'</span><span class="reply hide"><span class="icon icon-comment"></span>回复&nbsp;('+ reply_count +')</span></p>';
			html += '</div>';
		}
		$('.comment-list').append(html);
	}
	frame_obj.post(opt);
}

function init_scroll_load(){
	scroll_load_obj.init({scroll_target:$('.container'),target:$('.comment-list'),fun:load_comment_list});
}

function add_comment(){
	if($.trim($('#comment-text').val()).length == 0){
		alert({title:'请输入评论'});
		return false;
	}
	var opt = {};
	opt.url = $('#add_comment_url').val();
	opt.data = JSON.stringify({ad_id:$('#id').val(),comment:$.trim($('#comment-text').val())});
	opt.success = function(response){
		if(response.errcode == 0){
			success_screen('评论成功');
			$('#comment-text').val('');
			load_comment_list(true);
		}else{
			alert({title:'评论失败',desc:response.errmsg})
		}
	};
	frame_obj.post(opt);
}

function to_reply(that,id){
	return;
	location.href=$('#to_reply_url').val() + '&id='+id + '&advice_id='+$('#id').val();
}