$(document).ready(function() {
	init_comment_reply_list();
});

function init_comment_reply_list(){
	$('.comment-reply-list').children('div.comment-reply-item').remove();;
	var opt = {};
	opt.url = $('#get_reply_url').val();
	opt.data = JSON.stringify({id:$('#advice_id').val(),reply_id:$('#id').val()});
	opt.success = function(response){
		var html = '';
		for(var i = 0; i < response.data.data.length; i++){
			var ob = response.data.data[i];
			var pic = ob.user_pic ? ob.user_pic + '64' : 'static/image/face.png';
			var name = ob.user_name;
			var time = frame_obj.recent_time(ob.create_time);
			var text = ob.text;
			html += '<div class="comment-reply-item" data-name="'+ name +'" onclick="reply(this)">';
			html += '<img src="'+ pic +'" onerror="this.src=\'static/image/face.png\'">';
			html += '<p class="name-time">';
			html += '<span class="name">'+ name +'</span>';
			html += '<span class="time">'+ time +'</span>';
			html += '</p>';
			html += '<p class="text">'+ text +'</p>';
			html += '</div>';
		}
		$('.comment-reply-list').append(html);
	};
	frame_obj.post(opt);
}


function reply_comment(text){
	if($.trim($('#reply-comment-text').val()).length == 0){
		alert({title:'请输入评论回复信息'});
		return false;
	}
	lock_screen('提交评论回复');
	var opt = {};
	opt.url = $('#ajax_to_reply_url').val();
	opt.data = JSON.stringify({ad_id:$('#advice_id').val(),reply_id:$('#id').val(),comment:$.trim($('#reply-comment-text').val())});
	opt.success = function(response){
		unlock_screen();
		if(response.errcode == 0){
			success_screen('评论回复成功');
			$('#reply-comment-text').val('');
			init_comment_reply_list();
		}else{
			alert({title:'评论回复失败',desc:response.errmsg})
		}
	};
	frame_obj.post(opt);
}

function reply(that){
	var text = "回复 "+ $(that).attr('data-name') +"：";
	$("#reply-comment-text").focus().val(text);
}
