$(document).ready(function() {
	init_head();
	load_list();
	app_auto_hight();
	init_scroll_load();
});

function init_head(){
	var opt = {};
	opt.menu = {'list':[{'name':'未完成','activekey':1},{'name':'已完成','activekey':2},{'name':'草稿','activekey':0}],'fun':menu_switch};
	head_obj.init_head(opt);
	head_obj.active_head_menu($('#activekey').val());
}

function menu_switch(obj,el){
	location.href = $('#mine_list_url').val() + '&state=' + $(el).attr('data-activekey');
} 

function load_list(){
	var page = $('.advice_list').attr('data-page')-0;
	var data = {};
	data.page = page+1;
	data.state = $('#activekey').val();
	var opt = {};
	opt.url = $('#get_list_url').val();
	opt.data = JSON.stringify(data);
	opt.success = function(response){
		if(data.page == 1 && response.data.count == 0){
			no_data_css();
		}
		if(response.data.data.length < 20){
			scroll_load_obj.scroll_load_end($('.container'));
		}
		scroll_load_obj.remove_scroll_waiting($('.advice_list'));
		scroll_load_obj.set_is_scroll_loading_false($('.container'));
		$('.advice_list').attr('data-page',data.page);
		var html = '';
		for(var i = 0; i < response.data.data.length; i++){
			var ob = response.data.data[i];
			var state = {};
			if(ob.state == 0){
				state.state_desc = '未阅读';
				state.cl = 'state state-red';
			}
			if(ob.state == 1){
				state.state_desc = '已阅读';
				state.cl = 'state state-yellow';
			}
			if(ob.state == 2){
				state.state_desc = '处理中';
				state.cl = 'state state-green';
			}
			if(ob.state == 4){
				state.state_desc = '已完成';
				state.cl = 'state state-grey-green';
			}
			if(ob.state == 5){
				state.state_desc = '已关闭';
				state.cl = 'state state-white';
			}
			var class_name =  ob.class_name == undefined ||  ob.class_name == null ? "" : ob.class_name;
			html += '<div onclick="go_detail('+ ob.id +')" class="item">';
			html += '<p class="title title3-1">'+ '<span class="_title">'+ ob.title+ '</span>';
			
			if(ob.title_state != 0 &&　state.state_desc　!= undefined){
				html += '<span class="'+ state.cl +'">'+ state.state_desc +'</span>';
			}
			html += '</p>';
			html += '<p class="other-msg">';
			html += '<span class="time text2-2">'+ time2date(ob.create_time,'yyyy-MM-dd HH:mm') +'</span>';
			html += '<span class="type text2-2">'+ class_name +'</span>';
			
			html += '</p>';
			html += '</div>';
		}
		$('.advice_list').append(html);
	}
	frame_obj.post(opt);
}

function go_detail(id){
	if($('#activekey').val() == 0){
		location.href = $('#edit_advice_url').val() + '&id=' + id ;
	}else{
		location.href = $('#show_detail_url').val() + '&type=1&id=' + id ;
	}
}

function init_scroll_load(){
	scroll_load_obj.init({scroll_target:$('.container'),target:$('.advice_list'),fun:load_list});
}

function no_data_css(){
	$('#powerby').addClass('hide');
	$('body').css({'background-color':'#fff'});
	var opt = {};
	opt.target = $('.advice_list');
	opt.img = $('#app_static').val() + 'images/content_no.png';
	if($('#activekey').val() == 1){
		opt.title = '暂无数据';
		opt.desc_list = [' ',' '];
	}
	if($('#activekey').val() == 2){
		opt.title = '暂无数据';
		opt.desc_list = [' ',' '];
	}
	if($('#activekey').val() == 0){
		opt.title = '暂无数据';
		opt.desc_list = [' ',' '];
	}
	has_no_record(opt);
	var html_height=$('html').height()-90;
	var main_height=$('.has-no-record').height();
	var top = (html_height - main_height)/2;
	$('.has-no-record').css({'margin-top':top+'px'});
}
