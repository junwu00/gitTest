<?php
/**
 * 考勤
 * 
 * @author yangpz
 * @date 2014-10-21
 *
 */
class cls_wxser extends abs_app_wxser {
	/*
	public function enter_advice() {
		$wxacct = $this -> post_data['FromUserName'];
		$redis_key = md5(REDIS_QY_EW365_LOGIN_USER.':advice:'.$this -> com_id . ':' . $wxacct);
		if (g('redis') -> get($redis_key)) {
			return;
		}
		g('redis') -> set($redis_key, 'done', 24*3600);	//一天提醒一次
		echo  g('wxqy') -> get_text_reply($this -> post_data, "用得不爽[发怒]？有BUG？快来吐槽[咒骂]吧！支持文字或截图哦~");
	}

	public function reply_text() {
		//记录意见
		$wxacct = $this -> post_data['FromUserName'];
		$text = $this -> post_data['Content'];
		g('adv_apply') -> save_text($this -> com_id, $wxacct, $text);
		echo  g('wxqy') -> get_text_reply($this -> post_data, "感谢您的支持！");
	}
	
	public function reply_image() {
		//记录意见
		$wxacct = $this -> post_data['FromUserName'];
		$image = $this -> post_data['PicUrl'];
		g('adv_apply') -> save_image($this -> com_id, $wxacct, $image);
		echo  g('wxqy') -> get_text_reply($this -> post_data, "感谢您的支持！");
	}
	
	//--------------------------------------内部实现---------------------------
	*/
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『意见反馈』操作指引',
		'desc' => '欢迎使用意见反馈应用，在这里你可以匿名发建议给公司，说不定就能升职加薪迎娶白富美，给领导发一下你的心声吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971786&idx=3&sn=d3d02fafa5dfff8def52820f5f9dc12a&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
}

// end of file