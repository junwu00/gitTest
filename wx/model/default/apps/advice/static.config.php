<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'advice_apply_detail.css' => array(
		SYSTEM_ROOT . 'apps/advice/static/css/apply_index.css'
	),
    'advice_apply_detail.js' => array(
        SYSTEM_APPS . 'advice/static/js/apply_detail.js'
    ),
	'advice_apply_index.css' => array(
		SYSTEM_ROOT . 'apps/advice/static/css/apply_index.css'
	),
    'advice_apply_index.js' => array(
        SYSTEM_APPS . 'advice/static/js/apply_index.js'
    ),
	'advice_apply_lists.css' => array(
		SYSTEM_ROOT . 'apps/advice/static/css/apply_lists.css'
	),
    'advice_apply_lists.js' => array(
        SYSTEM_APPS . 'advice/static/js/apply_lists.js'
    ),
);

return $arr;

/* End of this file */