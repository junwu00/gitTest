<?php
/**
 * 任务通用父类
 * @author yangpz
 * @date 2014-12-09
 *
 */
class abs_task extends abs_app_base {
	
	protected static $AppName = 'task';
	
	/** 上传类型 ： 1 ，task_info*/
	private static $task_info = 1;
	/** 上传类型 ： 2 ，task_report*/
	private static $task_report = 2;
	
	protected static $TASKSTATE = array("任务草稿","执行中","申请退回","申请延期","申请完成","逾期完成","按时完成 ","终止");

	protected static $TaskButton = array(
										101=>array(
												0 =>"true,true,true",
												1 => "remind_page,stop_task",
												2 => "remind_page,approval_page,stop_task",
												3 => "remind_page,approval_page,stop_task",
												4 => "remind_page,stop_task",
												5 => "true,true,true",
												6 => "true,true,true",
												7 =>"true,true,true"
										),
										102=>array(
												0 =>"true,true,true",
												1 =>"finish",
												2 =>"true,true,true",
												3 =>"true,true,true",
												4 =>"true,true,true",
												5 =>"true,true,true",
												6 =>"true,true,true",
												7 =>"true,true,true"
										),
										103=>array(
												0 =>"true,true,true",
												1 =>"remind_page",
												2 =>"remind_page",
												3 =>"remind_page",
												4 =>"remind_page",
												5 =>"true",
												6 =>"true",
												7 =>"true"
										)
										);
	
	/** 草稿 0 */
	protected static $StateDraft = 0;
	/** 执行中 1 */
	protected static $StateRunning = 1;
	/** 申请退回 2 */
	protected static $StateApplyCallback = 2;
	/** 申请延期 3 */
	protected static $StateApplyDelay = 3;
	/** 申请完成 4 */
	protected static $StateApplyFinish = 4;
	/** 逾期完成 5 */
	protected static $StateOverdue = 5;
	/** 按时完成 6 */
	protected static $StateOntime = 6;
	/** 终止 7 */
	protected static $StateStop = 7;
	
	/**附件tab标示*/
	private static $ListFileTab = 0;
	/**子任务tab标示*/
	private static $ListSonTab = 1;
	/**汇报tab标示*/
	private static $ListReportTab = 2;
	/**评论tab标示*/
	private static $ListTalkingTab = 3;
	/**申请tab标示*/
	private static $ListApplyTab = 4;
	
	/** 禁用 0 */
	protected static $StateOff = 0;
	/** 启用 1 */
	protected static $StateOn = 1;
	
	//未完成
	protected static $IsFinishOff = 0;
	//已完成
	protected static $IsFinishOn = 1;
	
	//协办列表标示
	/**是否同意 未确认*/
	protected static $IsAgreeOff = 0;
	/**是否同意 不同意*/
	protected static $AgreeOff = 1;
	/**是否同意 同意*/
	protected static $AgreeOn = 2;
	
	//审批任务标示
	/**是否同意 未确认*/
	protected static $IsTaskAgreeOff = 0;
	/**是否同意 不同意*/
	protected static $TaskAgreeOff = 1;
	/**是否同意 同意*/
	protected static $TaskAgreeOn = 2;
	
	public function __construct($app_name) {
		$app_conf = include(SYSTEM_APPS.$app_name.'/config.php');
		parent::__construct($app_name);
	}
	
	//--------------------------------------protected--------------------------------

	/**
	 * 获取任务基本信息
	 */
	protected function get_task_info($userid,$taskid,$fields='*',$states=false) {
		try {
			//获取任务信息表数据
			$task_info = g("task_info")->get_by_com($taskid,$userid,$fields,$states);
			if(!$task_info)
				return false;
			
			//获取承办人(协办人)信息
			$perform_infos = g("task_perform_base")->get_by_com($taskid,'perform_id,perform_name,state');
			$perform_str="";
			$perform_id_str="";
			foreach ($perform_infos as $key => $perform_info)
			{
				$perform_id_str .=($perform_info["perform_id"].",");
				$perform_str .=$perform_info["perform_name"].'('.self::get_task_state($perform_info['state']).') ';
			}
			$perform_id_str = substr($perform_id_str,0,-1);
			$perform_str=substr($perform_str,0,-1);
			$task_info["perform_name"] = $perform_str;
			$task_info["perform_id"] = $perform_id_str;
			$task_info["perform"] = $perform_infos;

			//获取监督人信息
			$superintendent_infos = g('task_superintendent')->get_by_com($taskid,'superintendent_id,superintendent_name');
			$superintendent_str = "";
			if($superintendent_infos!=FALSE){
				foreach ($superintendent_infos as $superintendent_info)
				{
					$superintendent_str .=$superintendent_info["superintendent_name"].",";
				}
				$superintendent_str=substr($superintendent_str,0,-1);
			}
			$task_info["superintendent_name"] = $superintendent_str;
			$task_info["superintendent"] = $superintendent_infos;
			
			
			//封装分类名称
			$task_classify_id = $task_info["task_type"];
			$task_classify = g("task_classify")->get_task_classify_name($task_classify_id);
			$task_classify_name =$task_classify["classify_name"];
			 
			$task_info["task_classify_name"] = $task_classify_name;
			
			//封装任务状态名称
			$task_state = $task_info["state"];
			$task_state_name = self::$TASKSTATE[$task_state];
			$task_info["task_state_name"] = $task_state_name;
			$task_info["start_date"] = $task_info["starttime"]==0?"":date('Y-m-d H:i',$task_info["starttime"]);
			$task_info["end_date"] = $task_info["endtime"]==0?"":date('Y-m-d H:i',$task_info["endtime"]);
			return $task_info;
		} catch (SCException $e) {
			throw new SCException('查询任务信息失败');
		}
	}
	
		/**
	 * 上传文件
	 *
	 * @access public
	 * @return void
	 */
	protected function upload_file() {
		$file = array_shift($_FILES);
		log_write(json_encode($file));
		if (empty($file) || !is_array($file)) {
			cls_resp::echo_err(cls_resp::$FileNotFound, '找不到该上传文件，请重新上传！');
		}

		
		$type = get_var_post('type');
		$table = '';
		switch($type){
			case self::$task_info :$table = 'task_info';break;
			case self::$task_report :$table = 'task_report';break;
			default:throw new SCException('上传错误');
		}
		
		parent::log('开始上传文件');

		try {
			$data = g('task_file') -> upload_file($file);
		}catch(SCException $e) {
			parent::log('上传文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('上传文件成功');

		
		log_write(json_encode($data));
		$file_data = array(
			'objectid' => 0,
			'upload_table'=>$table,
			'file_name' => $data['file_name'],
			'file_url' => $data['show_url'],
			'file_key' => $data['file_key'],
			'file_type' => $data['extname'],
			'upload_id' => $_SESSION[SESSION_VISIT_USER_ID],
			'upload_name' => $_SESSION[SESSION_VISIT_USER_NAME],
			'upload_time' => time(),
			'info_state' => parent::$StateOn
		);
		$file_id = g('task_file') -> save($file_data);
		$info = array(
			'rec' => $data,
			'file' => $file_id
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 下载文件
	 */
	protected function download_file(){
		log_write('开始下载文件');
		$data = parent::get_post_data(array('id','objid','type','key'));
		$file_id = g('des')->decode($data['id']);
		$objid = g('des')->decode($data['objid']);
		$type = $data['type'];
		$key = $data['key'];
		
		if (empty($file_id) || empty($objid) || empty($type) || empty($key)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		parent::log('开始下载文件');
		try {
			$result = g('task_file') -> download_file($_SESSION[SESSION_VISIT_COM_ID],$_SESSION[SESSION_VISIT_USER_ID],$file_id,$objid,$type,$key,$this->app_combo);
		}catch(SCException $e) {
			parent::log('下载文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		
//		$user = g('user') -> get_by_id($this->UserId);
//		
//		$msg_user = array($user['acct']);
//				
//		$msg_title="重新打开详情页面";
//		
//		if($data['deal_type']==1){//待办
//			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
//		}
//		else if($data['deal_type']==2){//起草
//			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
//		}
//		else{//知会
//			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=notify_detail&notify_id='.$data['notify_id'];//TODO
//		}
//		try{
//			parent::send_single_news($msg_user, $msg_title, '重新打开详情页面', $msg_url);
//		}catch(SCException $e){
//			$fail_user .=$this->UserId.' ';
//		}
		parent::log('下载文件成功');
		cls_resp::echo_ok();
	}
	
	/**
	 * 详细页面信息展开
	 */
	protected function information_open($taskid,$userid,$open_type){
		if($open_type==self::$ListFileTab){
			$file = g("task_file")->select_list_taskid($taskid,"task_info",$userid);
//			if(!empty($file)){
//				foreach($file as &$f){
//					$f['id'] = g('des') -> encode($f['id']);
//				}
//			}
			return $file;
		}
		else if($open_type==self::$ListSonTab){
			$son_list = g("task_info")->get_son_list($taskid,$userid);
			
			$task_list = array();
			if ($son_list) {
				foreach ($son_list as $key => $son_info)
				{
					if($son_info['state']==self::$StateDraft)
						continue;
					$state_name = self::$TASKSTATE[$son_info["state"]];
					$son_info["state_name"]=$state_name;
					$son_info["starttime"] = date('Y-m-d H:i',$son_info["starttime"]);
					array_push($task_list,$son_info);
				}
				unset($son_info);
			}
			return $task_list; 
		}
		else if($open_type==self::$ListReportTab){
			$report_list = g("task_report")->select_list_taskid($taskid,$userid);
			$count = 0;
			if ($report_list) {
				foreach ($report_list as $report_info)
				{
					$report_info["report_time"] = date('Y-m-d H:i',$report_info["report_time"]);
					//查询附件信息
					$report_file = g("task_file")->select_list_taskid($report_info["id"],"task_report",$userid);
					if($report_file!=FALSE){
						
						foreach($report_file as &$f){
							$f['id'] = g('des')->encode($f['id']);
							$f['objectid'] = g('des')->encode($f['objectid']);
						}
						
						$report_info["report_file"] = $report_file;
					}
					$report_list[$count] = $report_info;
					$count++;
				}
				unset($report_info);
			}
			return  $report_list;
		}
		else if($open_type==self::$ListTalkingTab){
			$talking_list = g("task_talking")->select_list_taskid($taskid,$userid);
			$count = 0;
			if($talking_list){
				foreach ($talking_list as $talking_info)
				{
					$talking_info["talking_time"] = date('Y-m-d H:i',$talking_info["talking_time"]);
					$talking_list[$count] = $talking_info;
					$count++;
				}
				unset($talking_info);
			}
			return  $talking_list;
		}
		else if($open_type==self::$ListApplyTab){
			$apply_list = g("task_apply")->select_list_taskid($taskid,$userid); 
			$count = 0;
			if($apply_list){
				foreach ($apply_list as $apply_info)
				{
					$apply_info["apply_time"] = date('Y-m-d H:i',$apply_info["apply_time"]);
					$apply_info["answer_time"] = date('Y-m-d H:i',$apply_info["answer_time"]);
					$apply_list[$count] = $apply_info;
					$count++;
				}
				unset($apply_info);
			}
			return  $apply_list;
		}
	}
	
	
	/**
	 * 评论任务
	 */
	protected function task_talking($taskid,$talking_content){
		try {			
			parent::log('开始评论');
			g('db') -> begin_trans();
			$userid = $_SESSION[SESSION_VISIT_USER_ID];
			$talking_info = array(
				"taskid"=> $taskid,
				"talking_id"=>$userid,
				"talking_name"=>$_SESSION[SESSION_VISIT_USER_NAME],
				"talking_content"=>$talking_content
			);
			
			g("task_talking")->save($talking_info);
			
			$log_info = array(
				"taskid"=> $taskid,
				"loger_id"=>$userid,
				"loger_name"=>$_SESSION[SESSION_VISIT_USER_NAME],
				"log_content"=>"评论任务"
			);
			
			g("task_log")->save($log_info);
			
			g('db') -> commit();
			parent::log('评论成功');
			cls_resp::echo_ok();
		}catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 
	 */
	protected function error_page(){
		g('smarty') -> assign('title', '错误');
		g('smarty') -> show($this -> temp_path.'lists/index.html');
	}
	
	
	
	//------------------------------------------------通讯录-------------------------------------------------------------
/**
	 * 展示通讯录成员相关方法
	 */
	protected function show_contact(){
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$com = g("company")-> search($com_id);
			$user = g("user")-> search($com[0]['dept_id'],'');
			$dept_list = g('dept') -> list_dept($com[0]['dept_id']);
			$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
			$arr = array();
			$secret = array();
			$ar = array();
		    foreach ($user as $k=>$u){
		    	
		    		if(!in_array($u['id'], $secret)){
			    		$a = g('pinyin') -> get_first_charter(mb_substr($u['name'], 0, 1, 'UTF-8'));
				    	if(!in_array($a, $arr)){
				    	  $arr[]=$a;
				    	}
			        	$user[$k]['ch_name']= $a;	        	
		        		$ar[] = $user[$k];
		    		}	 
		    	    		
	        }
	      
	        sort($arr);
	        $sid= $_SESSION[SESSION_VISIT_USER_ID];
	      	g('smarty') -> assign('sid',$sid);
	        
	        $users = $this -> get_user_dept($ar);
	      	$this -> get_list_tree($tree,$dept_list,$ar);
	       	
	      	$result = $this -> user_general();
	      	if($result){
	      		g('smarty') -> assign('gen',$result);
	      	}
	      
		    g('smarty') -> assign('ch',$arr);
			g('smarty') -> assign('tree',$tree);
			g('smarty') -> assign('dept',$dept_list);
			g('smarty') -> assign('user',$users);
			g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
	}
	
	/**
	 * 获取用户的部门信息
	 * Enter description here ...
	 * @param unknown_type $user 本公司的所有员工
	 */
	private function get_user_dept($user){
		foreach ($user as $key=>$u){
			$dept_id = (Array)json_decode($u['dept_list']);
			$arr = array();
			
			foreach ($dept_id as $id){
				$dept = g('dept')-> get_dept_by_id($id);
				if($dept){
					$arr[] = $dept['name'];
				}
			}
			$user[$key]['dept_name'] = $arr;
		}
		return $user;
	}
	
	/**
	 *返回员工常用联系人
	 */
	private function user_general(){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$com = g("company")-> search($com_id);
		$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
		$general_list = (Array)json_decode($conf['general_list']);
			
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		$result = $this -> get_user_dept(g('user')-> get_by_ids($sid));
		$general_user = g('user_general') -> get_by_ids($sid);
		$dlist = (Array)json_decode($general_user[0]['del_list']);
		
		$user = array();	
		foreach ($general_list as $d){
			if($d != $sid && !in_array($d, $dlist)){
				$u = g('user')-> get_by_ids($d);
				if($u){
					$user[] = $u[0];
				}
			}
		}
		if($general_user){
			$list = (Array)json_decode($general_user[0]['general_list']);
			foreach ($list as $d){
					$u = g('user')-> get_by_ids($d);
					if($u && !in_array($u[0], $user)){
						$user[] = $u[0];
					}
			}
		}
		$arr = array();
		foreach ($user as $u){
			if($u['email']!=""){
				$arr[]=$u;
			}
		}
		$users=array();
		if($arr){
			$users = $this -> get_user_dept($arr);
		}
			
		return $users;
	}
	
/**
	 * 加载公司部门与全部员工
	 * Enter description here ...
	 * @param unknown_type $html
	 * @param unknown_type $dept_list 本公司所有部门
	 * @param unknown_type $user	本公司所有员工
	 */
	private function get_list_tree(&$html, $dept_list, $user, $root=FALSE) {
		$display = '';
		if (!$root) {
			$display = 'style="display:none;"';
		}
		
		if (!is_array($dept_list)){$html .='</ul>'; return;}
		foreach ($dept_list as $dept) {
			if (isset($dept['childs'])) {
				$html .= '<li ><div class="dlabel" >'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
				foreach ($user as $u){
					$arr = (Array)json_decode($u['dept_list']);
					 if(in_array($dept['id'], $arr)){
					 	if($u['pic_url']!=""){
					 		$img=$u['pic_url'];
					 	}else{
					 		$img=SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}

					 	$html .= '<li  style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" m="'.$img.'"  n="'.$u['name'].'" d="'.$u['id'].'"></i></li>';
					 }
				}
			
				$this -> get_list_tree($html, $dept['childs'],$user);
				$html .= '</ul>';
			} else {
				$html .= '<li ><div class="dlabel">'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
			    foreach ($user as $u){
			    	$arr = (Array)json_decode($u['dept_list']);
					 if(in_array($dept['id'], $arr)){
						 if($u['pic_url']!=""){
					 		$img = $u['pic_url'];
					 	}else{
					 		$img = SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}
					 	$html .= '<li style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" m="'.$img.'"  n="'.$u['name'].'" d="'.$u['id'].'"></i></li>';
					 }
				}
				$html .= '</ul>';
			}
			$html .= '</li>';
		}
	}
	
	/**
	 * 根据state值获取对应状态名
	 * @param unknown_type $type
	 */
	protected function get_task_state($type){
		$type = (int)$type;
		if(empty($type)){
			return false;
		}
		$type_name = '';
		switch($type){
			case self::$StateRunning : $type_name = '执行中';break;
			case self::$StateApplyCallback : $type_name = '申请退回';break;
			case self::$StateApplyDelay : $type_name = '申请延期';break;
			case self::$StateApplyFinish : $type_name = '申请完成';break;
			case self::$StateOverdue : $type_name = '逾期完成';break;
			case self::$StateOntime : $type_name = '按时完成';break;
			case self::$StateStop : $type_name = '终止';break;
		}
		return $type_name;
	}
}

// end of file