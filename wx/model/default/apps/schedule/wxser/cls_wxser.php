<?php
/**
 * 工作日程
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『工作日程』操作指引',
		'desc' => '欢迎使用工作日程应用，在这里你可以轻松安排每天的工作计划，合理管理自己的计划，不再担心遗忘你的日程啦！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970246&idx=2&sn=09ee694b89fbf92b8822a037111aaa7d#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	//--------------------------------------内部实现---------------------------
	
}

// end of file