<?php
/**
 * 日程记录查询
 * @author chenyh
 * @date 2015-09-14
 *
 */
class cls_ajax extends abs_app_base {
	
	private static $State = array('0','一般','重要','非常重要');
	private static $Repeat = array('不重复','每天','每周','每月','每年');
	private static $remind = array('不提醒','开始时','开始前10分钟','开始前一个小时','开始前1天','开始前30分钟');
	
	private static $repeat_time = array(
			'1' => '+1 day',
			'2' => '+1 week',
			'3' => '+1 month',
			'4' => '+1 year',
		);

	public function __construct() {
		parent::__construct('schedule');
	}
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'load_count':
				$this ->load_count();
				break;
			case 'load'://获取指定日期的日程
				$this ->load();
				break;
			case 'add_group':
				$this ->add_group();
				break;
			case 'load_group':
				$this ->get_group();
				break;
			case 'edit_group':
				$this ->edit_group();
				break;
			case 'delete_group':
				$this ->delete_group();
				break;
			case 'add_concern':
				$this ->add_concern();	
				break;
			case 'load_partner':
				$this ->load_partner();	
				break;
			case 'delete_concern':
				$this ->delete_concern();	
				break;
			case 'get_concern':
				$this ->get_concern();	
				break;
			case 'get_see':
				$this ->get_see();	
				break;
			case 'get_group_number':
				$this -> get_group_number();
				break;
			case 'detail'://获取日程详情
				$this ->detail();
				break;
			case 'save'://添加/更新日程
				$this ->save();
				break;
			case 'remove_schedule'://删除日程
				$this ->remove();
				break;
			default:
				cls_resp::show_err_page(array('请求错误'));
		}
	}
	
	/**
	 * 获取组成员
	 */
	private function get_group_number(){
		try{
			$data = parent::get_post_data(array('group_id'));
			$group_id = $data['group_id'];
			if(empty($group_id)){
				throw new SCException('请求错误');
			}
			
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$ret = g('schedule_group') -> get_group($com_id,$user_id,$group_id);
			if(empty($ret)){
				throw new SCException('组信息不存在');
			}
			$dept = json_decode($ret['group_dept_list'],true);
			$user = json_decode($ret['group_user_list'],true);
			if(empty($dept)){
				$dept = array();
			}else{
				$dept = g('dept') -> get_dept_name($dept);
			}
			if(empty($user)){
				$user = array();
			}else{
				$user =  g('user') -> get_by_ids($user,'id,name,pic_url,dept_list');
			}
			$info = array(
				'group'=> $ret['group_name'],
				'dept' => $dept,
				'user' => $user
			);
			cls_resp::echo_ok(cls_resp::$OK,'info',$info);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	private function get_group(){
		try{
			$data = parent::get_post_data(array());
			$group_id = '';
			if(isset($data['id']) && !empty($data['id'])){
				$group_id = $data['id'];
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$ret = g('schedule_group') -> get_group($com_id,$user_id,$group_id,'id,group_name');
			cls_resp::echo_ok(cls_resp::$OK,'list',$ret);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 添加常用联系人
	 * @throws SCException
	 */
	private function add_concern(){
		try{
			$data = parent::get_post_data(array('concern_id'));
			$concern_id = $data['concern_id'];
			if(empty($concern_id) || !is_array($concern_id)){
				throw new SCException();
			}
			
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			if(in_array($user_id,$concern_id)){
				$key = array_search($user_id,$concern_id);
				if($key !== false){
					array_splice($concern_id,$key,1);
				}
			}
			
			$ret = g('schedule_concern') -> add_concern($com_id,$user_id,$concern_id);
			if(!$ret){
				throw new SCException('添加失败');
			}
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取常用联系人
	 */
	private function get_concern(){
		try{
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$ret = g('schedule_concern') -> get_concern($com_id,$user_id,'u.id,u.name,u.pic_url,u.dept_list',TRUE);
			if(!empty($ret)){
				$dept_array =array();
				foreach ($ret as &$r){
					$dept_list = json_decode($r['dept_list'],true);
					$dept = array_shift($dept_list);
					array_push($dept_array,$dept);
					$r['dept_list'] = $dept;
				}
				unset($r);
				$dept_list =  g('dept') -> get_dept_name($dept_array);
				
				$ret_copy = $ret;
				foreach ($ret_copy as $key => $r){
					foreach ($dept_list as $d){
						if($r['dept_list']  == $d['id']){
							$ret[$key]['dept_list'] = $d['name'];
							unset($ret_copy[$key]);
							continue;
						}
					}
				}
			}
			
			cls_resp::echo_ok(cls_resp::$OK,'info',$ret);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取最近查看员工信息
	 */
	private function get_see(){
		try{
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$ret = g('schedule_group') -> get_see($user_id);
			cls_resp::echo_ok(cls_resp::$OK,'info',$ret);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除常用联系人
	 * @throws SCException
	 */
	private function delete_concern(){
		try{
			$data = parent::get_post_data(array('concern_id'));
			$concern_id = (int)$data['concern_id'];
			if(empty($concern_id)){
				throw new SCException('请求错误');
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$ret = g('schedule_concern') -> delete_concern($com_id,$user_id,$concern_id);
			if(!$ret){
				throw new SCException('删除失败');
			}
			cls_resp::echo_ok();
			
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 增加一个分组
	 */
	private function add_group(){
		try{
			$data = parent::get_post_data(array('group_name'));
			if(empty($data['group_name'])){
				throw new SCException('分组名称不能为空！');
			}
			
			if(!is_array($data['user_list'])){
				$data['user_list'] = array();
			}
			if(!is_array($data['dept_list'])){
				$data['dept_list'] = array();
			}
			
			$data['user_list'] = json_encode($data['user_list'],TRUE);
			$data['dept_list'] = json_encode($data['dept_list'],TRUE);
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$group_id = g('schedule_group') -> add_group($com_id,$user_id,$data['group_name'],$data['dept_list'],$data['user_list']);
			if(empty($group_id)){
				throw new SCException('分组创建失败！');
			}
			$group = array(
				'id' => $group_id,
				'group_name' => $data['group_name'],
			);
			cls_resp::echo_ok(cls_resp::$OK,'group',$group);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 修改一个分组
	 */
	private function edit_group(){
		try{
			$data = parent::get_post_data(array('group_id','group_name'));
			if(empty($data['group_id'])){
				throw new SCException('请求错误！');
			}
			if(empty($data['group_name'])){
				throw new SCException('分组名称不能为空！');
			}
			
			if(!is_array($data['user_list'])){
				$data['user_list'] = array();
			}
			if(!is_array($data['dept_list'])){
				$data['dept_list'] = array();
			}
			
			$data['user_list'] = json_encode($data['user_list'],TRUE);
			$data['dept_list'] = json_encode($data['dept_list'],TRUE);
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$ret = g('schedule_group') -> edit_group($com_id,$user_id,$data['group_id'],$data['group_name'],$data['dept_list'],$data['user_list']);
			if(!$ret){
				throw new SCException('分组创建失败！');
			}
			cls_resp::echo_ok();
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除一个分组
	 */
	private function delete_group(){
		try{
			$data = parent::get_post_data(array('group_id'));
			if(empty($data['group_id'])){
				throw new SCException('请求错误！');
			}
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$group = g('schedule_group') -> get_group($com_id,$user_id,$data['group_id']);
			if(empty($group)){
				throw new SCException('没有权限');
			}
			$ret = g('schedule_group') -> delete_group($com_id,$user_id,$data['group_id']);
			if(!$ret){
				throw new SCException('分组删除失败！');
			}else{
				g('schedule') -> delete_group($com_id,$user_id,$data['group_id']);
			}
			cls_resp::echo_ok();
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 删除日程
	 */
	private function remove(){
		try{
			$data = parent::get_post_data(array('sid'));
			$data['sid'] = (int)$data['sid'];
			if(empty($data['sid'])){
				throw new SCException('请求错误');
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$ret = g('schedule') -> get_by_id($com_id,$data['sid']);
			if(empty($ret)){
				throw new SCException('日程不存在');
			}
			$remove_p = FALSE;
			$sid = $ret['id'];
			if($ret['repeat_type']!=0){
				$remove_p = TRUE;
				if($ret['p_id'] != 0){
					$sid = $ret['p_id'];
				}
			}
			$ret = g('schedule') -> delete_schedule($com_id,$user_id,$sid,$remove_p);
			if(!$ret){
				throw new SCException('删除失败');
			}
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
		
	}
	
	/**
	 * 获取时间范围内的日程数目
	 */
	private function load_count(){
		try{
			$data = parent::get_post_data(array());
			
			isset($data['id']) && $user_id = (int)$data['id'];
			
			if( empty($user_id)){
				$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			}
			$partner_id = '';
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			
			if($user_id != $_SESSION[SESSION_VISIT_USER_ID]){
				$partner_id = $_SESSION[SESSION_VISIT_USER_ID];
			}
			
			log_write('开始查询伙伴日程记录');
			$ret = g('schedule')-> load_count($user_id,$com_id,$partner_id);
			
			$sc_count = array();
			$repeat = array();
			foreach ($ret as $sc){
			if($sc['repeat_type'] != 0){
					switch ($sc['repeat_type']) {
						case '1':
							$s_day = 0;
							break;
						case '2':
							$s_day = date("w",strtotime($sc['startdate']));
							break;
						case '3':
							$s_day = date("d",strtotime($sc['startdate']));
							break;
						case '4':
							$s_day = date("m-d",strtotime($sc['startdate']));
							break;
						default:
							break;
					}
					$second = strtotime($sc['startdate']) - strtotime($sc['enddate']);
					$day = $second/(24*60*60);
					$day += ($second%(24*60*60) == 0) ? 0 : 1;
					if($day==0) $day = 1;
					$tmp_sc = array(
							'repeat_type' => $sc['repeat_type'],
							'start' => $s_day,
							'state' => $sc['state'],
							'y' => date("Y",strtotime($sc['startdate'])),
							'm' => date("m",strtotime($sc['startdate'])),
							'd' => date("d",strtotime($sc['startdate'])),
							'num' => $day,
						);
					array_push($repeat,$tmp_sc);
					continue;
				}


				if($sc['day']==1){
					$this -> set_count($sc_count,$sc['startdate'],$sc['state']);
				}else{
					$start = strtotime($sc['startdate']);
					$end = strtotime($sc['enddate']);
					while($start <= $end){
						$this -> set_count($sc_count,$sc['startdate'],$sc['state']);
						$start = strtotime("+1 day", $start); 
						$sc['startdate'] = date('Y-m-d',$start);
					}
				}
			}
			$return = array(
					'data' => $sc_count,
					'repeat' => $repeat,
				);
			cls_resp::echo_ok(cls_resp::$OK,'data',$return);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	private function set_count(&$sc_count,$date,$state){
		if(!isset($sc_count[$date])){
			$sc_count[$date] = array($state); 
		}else{
			array_push($sc_count[$date], $state);
		}
	} 
	
	
	/**
	 * 删除伙伴
	 */
	private function remove_partner(){
		try{
			$data = parent::get_post_data(array('user_id'));
			
			if(empty($data['user_id'])){
				throw new SCException('参数错误');
			}
			
			$ret = g('schedule') -> remove_partner($_SESSION[SESSION_VISIT_COM_ID],$_SESSION[SESSION_VISIT_USER_ID],$data['user_id']);
			if(!$ret){
				throw new SCException('删除失败');
			}
			cls_resp::echo_ok();
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
		
	}
	
	/**
	 * 处理申请  2：接受，3：拒绝
	 */
	private function do_apply(){
		try{
			$data = parent::get_post_data(array('user_id','state'));
			if(empty($data['user_id']) || empty($data['state'])){
				throw new SCException('请求错误');
			}
			
			if($data['state'] !=2 && $data['state'] !=3){
				throw new SCException('操作错误');
			}
			log_write('开始处理申请');
			g('schedule')->deal_apply($_SESSION[SESSION_VISIT_COM_ID],$_SESSION[SESSION_VISIT_USER_ID],$data['user_id'],$data['state']);
			log_write('申请处理完成');
			cls_resp::echo_ok();
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 加载我的伙伴（同时加载申请中和已确立关系的伙伴）
	 * type,1:我关注的，2，最近查看的，3以上两者
	 */
	private function load_partner(){
		try{
			$data = parent::get_post_data(array('type'));
			$type = (int)$data['type'];
			if(empty($type) || $type < 1 || $type > 3){
				throw new SCException('请求错误');
			}
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			
			switch($type){
				case 1:
					$ret = g('schedule_concern')-> get_concern($user_id,$com_id,'u.name,u.id,u.pic_url,u.dept_list',TRUE);
					break;
				case 2:
					$ret = g('schedule_group') -> get_see($user_id);
					break;
				case 3:
					$concern = g('schedule_concern')-> get_concern($com_id,$user_id,'u.name,u.id,u.pic_url,u.dept_list',TRUE);
					$see = g('schedule_group') -> get_see($user_id);
					$ret = array(
						'concern' => $concern,
						'see' => $see,
					);
					break;
			}
			
			cls_resp::echo_ok(cls_resp::$OK,'info',$ret);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 添加伙伴
	 */
	private function add_partern(){
		try{
			$data = parent::get_post_data(array('partern')); 
			if(empty($data['partern']) || !is_array($data['partern'])){
				throw new SCException('参数错误');
			}
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$parterns = array();
			foreach($data['partern'] as $partern){
				if(isset($partern['id']) && isset($partern['name']) && !empty($partern['id']) && !empty($partern['name'])){
					if(!g('schedule')->check_apply($_SESSION[SESSION_VISIT_COM_ID],$user_id,$partern['id'])){
						array_push($parterns,$partern);
					}
				}
			}
			if(!empty($parterns)){
				g('schedule') -> apply_partern($user_id,$com_id,$parterns);
				try{
					
					$user_array = array();
					foreach ($parterns as $user){
						array_push($user_array, $user['id']);
					}
					$to_users = g('user') -> get_by_ids($user_array);
					
					$user_accts = array();
					foreach ($to_users as $t){
						array_push($user_accts, $t['acct']);
					}
					
					$msg_super_title = '伙伴申请';
					$msg_super_desc = '有一位同事申请与您成为伙伴，快点去看看吧！';
					$msg_url = SYSTEM_HTTP_WX_DOMAIN.'?app=schedule';
					if(!empty($user_accts)){
						parent::send_single_news($user_accts, $msg_super_title, $msg_super_desc, $msg_url);
					}
					
					$pc_super_msg_desc = '工作日程:您有一条伙伴申请，请及时处理！';
					$pc_url = SYSTEM_HTTP_DOMAIN.'?app=schedule&m=record';
					foreach($to_users as $to_user){
						//发送PC提醒
						$data = array(
							'url' => $pc_url,
							'title' => $pc_super_msg_desc,
							'app_name' => parent::$app_name
						);
						g("messager")->publish_by_user('pc', $to_user['id'],$data);
					}
					
				}catch(SCException $e){
					log_write();
				}
			}
			cls_resp::echo_ok();
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 保存和更新 
	 * 注：share_partern格式为：['id1':'name1',"id2":"name2"]
	 */
	private function save(){
		try{
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$data = parent::get_post_data(array('title','startdate','starttime','enddate','endtime','state','allday','repeat','share_type','remind'));
			
			isset($data['title']) && $data['title'] = filter_string($data['title']);
			isset($data['desc']) && $data['desc'] = filter_string($data['desc']);
	
			$data['allday'] = empty($data['allday']) ? 0 : $data['allday'];
			
			if($data['allday'] != 1){
				$data['start_date'] = strtotime($data['startdate'] . ' ' .$data['starttime']);  
				$data['end_date'] = strtotime($data['enddate'] . ' ' .$data['endtime']);
			}else{
				$data['start_date'] = strtotime($data['startdate']);
				$data['end_date'] = strtotime($data['enddate'].' '.'23:59:59');
			}
			
			if($data['start_date'] > $data['end_date']){
				throw new SCException('开始时间不能小于结束时间！');
			}
			
			if(empty($data['state'])){
				throw new SCException('请选择重要程度！');
			}
			
			$data['repeat'] = empty($data['repeat']) ? 0 :$data['repeat'];
			$data['remind'] = empty($data['remind']) ? 0 :$data['remind'];
	
			$data['share_group'] = (int)$data['share_group'];
			
			if($data['share_type'] == 0){
				$data['share_user'] = array();
				$data['share_dept'] = array();
				$data['share_group'] = 0;
				$data['remind_share'] = 0;
			}else if( $data['share_type'] == 1){
				$data['share_user'] = array();
				$data['share_dept'] = array();
				$data['share_group'] = 0;
				$data['remind_share'] = empty($data['remind_share']) ? 0 :$data['remind_share'];
			}else if($data['share_type'] == 2){
				$data['share_dept'] = array();
				if(!isset($data['share_user']) || empty($data['share_user']) || !is_array($data['share_user'])){
					$data['share_user'] = array();
				}else{
					$data['share_user'] = array_filter($data['share_user']);
				}
				if(!isset($data['share_group']) || empty($data['share_group'])){
					$data['share_group'] = 0;
				}
				$data['remind_share'] = empty($data['remind_share']) ? 0 :$data['remind_share'];
			}else{
				throw new SCException('提交参数错误！');
			}
			$data['share_user'] = json_encode($data['share_user']);
			$data['share_dept'] = json_encode($data['share_dept']);
			
			$data['schedule_id'] = (int)$data['schedule_id'];
			$schedule_time = $data['start_date'];
			$remind_time = array(0=>0,1=>0,2=>60*10,3=> 60*60,4=>60*60*24,5=>60*30);
			$data['remind_time'] = $data['start_date'] - $remind_time[$data['remind']];

			$curr_time = time();
			if($data['repeat'] != 0){
				while($data['remind_time'] < $curr_time){
					$data['remind_time'] = strtotime(self::$repeat_time[$data['repeat']],$data['remind_time']);
				}
			}
			if(empty($data['schedule_id'])){
				$ret = g('schedule')->save($user_id,$_SESSION[SESSION_VISIT_COM_ID],$data);
				if($ret){
					if($data['create_remind'] == 1){
						$this ->remind_share($ret);
					}
				}else{
					throw new SCException('创建失败');
				}
			}else{
				if($data['remind_time'] <= time()){
					$data['remind_state'] = 1;
				}else{
					$data['remind_state'] = 0;
				}
				g('schedule')->update($user_id,$_SESSION[SESSION_VISIT_COM_ID],$data['schedule_id'],$data);
			}
		 	cls_resp::echo_ok();
		}catch(SCException $e){
		 	cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 提醒被分享人
	 * @param unknown_type $sid
	 */
	public function remind_share($sid){
		try{
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$schedule = g('schedule') -> get_by_id($com_id,$sid);
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			$wx_title = '日程分享提醒';		
			$wx_desc = $user_name.'创建了日程【'.$schedule['title'].'】并分享给您，请注意查看！';
			$wx_url = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&uid='.$schedule['user_id'].'&sid='.$schedule['id'].'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
			$pc_title = '工作日程：'.$user_name.'创建了日程【'.$schedule['title'].'】并分享给您，请注意查看！';		
			$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app=schedule&m=record'.'&sid='.$schedule['id'].'&user_id='.$schedule['user_id'].'&default_date='.date('Y-m-d',$schedule['start_date']);;
			$data = array(
            	'title' => $pc_title,
            	'url' => $pc_url,
                'app_name' => 'schedule',
            );
			
	 		switch ($schedule['share_type']){
				case 0://私有
					break;
				case 1://分享所有
					parent::send_single_news(array('@all'), $wx_title, $wx_desc, $wx_url);
					$root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
					g('messager') -> publish_by_ids('pc', array($root_id),array(),$data);
					break;
				case 2://分享指定
					$user = empty($schedule['user_list']) ? array() : json_decode($schedule['user_list'],TRUE);
					$group_user = empty($schedule['group_user_list']) ? array() : json_decode($schedule['group_user_list'],TRUE);
					$user_list = array_merge($user,$group_user);
					$user_list = array_filter($user_list);
					if(empty($user_list)){
						log_write('分享人员为空');
						return false;
					}
					$users = g('user') -> get_by_ids($user_list,'id,acct');
					$acct_array = array();
					if(empty($users)) return false;
					foreach ($users as $u){
						array_push($acct_array, $u['acct']);
					}
					parent::send_single_news($acct_array, $wx_title, $wx_desc, $wx_url);
					$root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
					g('messager') -> publish_by_ids('pc', array(),$user_list,$data);
					break;
			}
		}catch(SCException $e){
			throw $e;
		}
	}
	
	
	/**
	 * 获取日程详情
	 */
	private function detail(){
		try{
			$data = parent::get_post_data(array('i'));	//日程ID
			$id = $data['i'];
			if(empty($id)){
				cls_resp::show_err_page(array('请求错误'));
			}
			$uid = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$schedult = g('schedule')-> get_by_id($com_id,$id);
			if(empty($schedult)){
				throw new SCException('找不到日程');
			}
			if($schedult['user_id'] != $uid){
				if($schedult['share_type'] == 0){
					cls_resp::echo_exp(new SCException('没有权限'));
				}else if($schedult['share_type'] == 2){	//因未实现分享至部门，先不做部门判断
					$partern_List = json_decode($schedult['user_list'],true);
					$group_user_List = json_decode($schedult['group_user_list'],true);
					if(!in_array($uid, $partern_List) && !in_array($uid, $group_user_List)){
						cls_resp::echo_exp(new SCException('没有权限'));
					}
				}
			}
			$user = g('user') -> get_by_id($schedult['user_id'],'name,pic_url,position');
			if($schedult){
				if($schedult['share_type']==0){
					$schedult['share_name'] = "";
				}else if($schedult['share_type'] == 1){
					$schedult['share_name'] = "所有";
				}else if($schedult['share_type'] == 2){
					if(empty($schedult['group_list']) || $schedult['group_info_state'] == 0){
						$schedult['group_list'] = null;
						$schedult['group_user_list'] = array();
						$schedult['group_name'] = null;
					}
					
					$schedult['user_list'] = json_decode($schedult['user_list'],true);
					$schedult['group_user_list'] = json_decode($schedult['group_user_list'],true);
					if(empty($schedult['group_user_list'])){
						$schedult['group_user_list'] = array();
					}
					
					$user_list = array_merge($schedult['user_list'],$schedult['group_user_list']);
					if(empty($user_list)){
						$schedult['user_list'] = array();
						$schedult['group_user_list'] = array();
					}else{
						$ret = g('user') -> get_by_ids($user_list,'id,name');
						log_write(json_encode($ret));
						$u_list = array();
						$g_list = array();
						foreach ($ret as $r){
							if(in_array($r['id'],$schedult['user_list'])){
								array_push($u_list, $r);
								continue;
							}
							if(in_array($r['id'],$schedult['group_user_list'])){
								array_push($g_list, $r);
								continue;
							}
						}
						$schedult['user_list'] = $u_list;
						$schedult['group_user_list'] = $g_list;
						$schedult['all_user'] = $ret;
					}
				}
				
				$schedult['user_name'] = $user['name'];
				
				$schedult['create_date'] = date('Y-m-d H:i',$schedult['create_date']);
				if($schedult['all_day']==1){
					$schedult['start_date'] = date('Y-m-d',$schedult['start_date']);
					$schedult['end_date'] = date('Y-m-d',$schedult['end_date']);
				}else{
					$schedult['start_date'] = date('Y-m-d H:i',$schedult['start_date']);
					$schedult['end_date'] = date('Y-m-d H:i',$schedult['end_date']);
				}
				
				$schedult['remind'] = self::$remind[$schedult['remind_type']];
				$schedult['state_name'] = self::$State[$schedult['state']];
				$schedult['repeat'] = self::$Repeat[$schedult['repeat_type']];
			}else{
				cls_resp::show_err_page(array('找不到日程'));
			}
			
			cls_resp::echo_ok(cls_resp::$OK,'info',$schedult);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取员工指定日期内的日程（只支持单天）
	 */
	private function load() {
		try{
			$data = parent::get_post_data(array());
			$date = $data['date'];
			empty($date) && $date = date('Y-m-d',time());
			
			$start = strtotime($date.' 00:00');
			$end = strtotime($date.' 23:59:59');
			if(empty($start) || empty($end)){
				throw new SCException('请求错误！');
			}
			if(isset($data['id'])){
				$user_id = $data['id'];	//当前被查看伙伴的ID，查看自己日程时留空
			}else{
				$user_id = '';
			}
			
			$partner_id = '';
			
			empty($user_id) && $user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			if($user_id != $_SESSION[SESSION_VISIT_USER_ID]){
				$partner_id = $_SESSION[SESSION_VISIT_USER_ID];
				g('schedule_group')-> save_see($partner_id,$user_id);
			}
			$ret = g('schedule') ->get_by_time($_SESSION[SESSION_VISIT_COM_ID],$user_id,$start,$end,$partner_id);
			$result = array();
			if(!empty($ret)){
				foreach ($ret as $key => $item) {
					$s_time = '';
					$e_time = '';
					
					if($item['start_date'] >= $start && $item['end_date'] <= $end){
						$s_time =  date('H:i',$item['start_date']);
						$e_time =  date('H:i',$item['end_date']);
					}else if($item['start_date'] >= $start && $item['start_date'] <= $end){
						$s_time =  date('H:i',$item['start_date']);
						$e_time = '24:00';
					}else if($item['end_date'] >= $start && $item['end_date'] <= $end){
						$s_time = '00:00';
						$e_time = date('H:i',$item['end_date']);
					}else if($item['start_date'] < $start && $item['end_date'] > $end){
						$s_time = '00:00';
						$e_time = '24:00';
					}
					
					$item['state_name'] = self::$State[$item['state']];
					$item['repeat_name'] = self::$Repeat[$item['repeat_type']];
					$item['remind_name'] = self::$remind[$item['remind_type']];
					
					
					$start_date = date('Y-m-d',$item['start_date']);
					$start_time = date('H:i',$item['start_date']);
					$end_date = date('Y-m-d',$item['end_date']);
					$end_time = date('H:i',$item['end_date']);
					
					$result[] = array(
						'id' => $item['id'],
						'title' => $item['title'],
						'desc' => $item['content'],
						's_time' => $s_time,
						'e_time' => $e_time,
						'start_time' => $item['start_date'],
						'end_time' => $item['end_date'],
						'all_day' => $item['all_day'],
						'state' => $item['state']
					);
				}unset($item);
			}
			cls_resp::echo_ok(cls_resp::$OK,'data',$result);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
}

// end of file