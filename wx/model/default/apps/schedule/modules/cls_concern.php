<?php
/**
 * 工作日程伙伴管理
 * @author zhangpeng
 *
 */
class cls_concern extends abs_app_base {
	
	public function __construct() {
		parent::__construct('schedule');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		parent::assign_contact_dept();
		$post_url = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&m=ajax';
		g('smarty') -> assign('post_url',$post_url);
		
		g('smarty') -> assign('title', '工作日程');
		g('smarty') -> show($this -> temp_path.'partner/index.html');
	}
}
// end of file