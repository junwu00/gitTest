<?php
/**
 * 工作日程默认页面
 * @author yangpz
 *
 */
class cls_new extends abs_app_base {
	
	public function __construct() {
		parent::__construct('schedule');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		$schedule_id = get_var_value("schedule_id");
		
		parent::assign_contact_dept();
		$partner_list_ajax = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&m=ajax&ajax_act=load_partner';
		$save_ajax = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&m=ajax&ajax_act=save';
		$post_url = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&m=ajax';
		$list = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule';
		$get_edit_data = SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&m=ajax&ajax_act=detail';
		g('smarty') -> assign('post_url', $post_url);
		g('smarty') -> assign('save_ajax', $save_ajax);
		g('smarty') -> assign('partner_list_ajax', $partner_list_ajax);
		g('smarty') -> assign('list', $list);
		g('smarty') -> assign('time', date("Y-m-d H:i",time()));
		if(empty($schedule_id)){
			g('smarty') -> assign('title', '新建日程');
		}else{
			g('smarty') -> assign('title', '修改日程');
			g('smarty') -> assign('schedule_id', $schedule_id);
			g('smarty') -> assign('get_edit_data', $get_edit_data);
		}
		g('smarty') -> show($this -> temp_path.'new/index.html');
	}

}
// end of file