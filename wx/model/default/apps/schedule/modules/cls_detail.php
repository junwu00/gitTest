<?php
/**
 * 日程详细页面
 * @author yangpz
 *
 */
class cls_detail extends abs_app_base {
	
	private static $State = array('0','不紧急','不重要','紧急','重要');
	private static $remind = array('不提醒','开始时','开始前10分钟','开始前一个小时','开始前1天');
	
	public function __construct() {
		parent::__construct('schedule');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act) {
			case 'finish':
				$this ->finish();break;
			case 'delete':
				$this ->delete();break;
			default:
				$id = get_var_get('sid');
				$schedule = $this -> get_schedule($user_id,$id);
				if(!$schedule){
					echo '找不到日程！';
					return;
				}				
				g('smarty') -> assign('schedule',$schedule);
				g('smarty') -> show($this -> temp_path.'detail/index.html');
		}
	}
	/**
	 *  获取日程详细信息
	 * Enter description here ...
	 */
	private function get_schedule($uid,$id){
		if(!$uid || !$id)
			return false;
		$schedult = g('schedule')-> get_by_id($uid,$id);
		if($schedult){
			$schedult['schedule_date'] = date('Y-m-d H:i',$schedult['schedule_date']);
			$schedult['remind_type'] = self::$remind[$schedult['remind_type']];
			$schedult['state'] = self::$State[$schedult['state']];
		}
		return $schedult; 
	}
	
	private function finish(){
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$data = get_var_post('data');
		$sids = explode(',', $data['schedule_id']);
		try{
			g('schedule')->finish($user_id,$sids);
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	private function delete(){
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$data = get_var_post('data');
		$id = $data['schedule_id'];
		try{
			g('schedule')->delete($user_id,$id);
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
}
// end of file