<?php
/**
 * 工作日程默认页面
 * @author yangpz
 *
 */
class cls_index extends abs_app_base {
	
	public function __construct() {
		parent::__construct('schedule');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act) {
			case 'finish':
				$this ->finish();break;
			case 'json_date':
				$data = get_var_post('data');
				echo json_encode($this -> getAllSchedule($data['date'])); break;
			default:
				$date = get_var_get('date');
				if(empty($date))
					$date = date("Y-m-d",time());
				$my_id = $_SESSION[SESSION_VISIT_USER_ID];
				parent::assign_contact_dept();
				
				$show_schedule_id = get_var_get('sid');
				$user_id = get_var_get('uid');
				if(!empty($user_id)){
					$user = g('user')->get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID],$user_id,'id,name,pic_url');
					if(empty($user)){
						cls_resp::show_err_page(array('没有权限'));
					}
					$count = json_encode($this ->load_count($user_id));
				}else{
					$user = array();
					$count = json_encode($this ->load_count());
				}
				
				g('smarty') -> assign('create_url','index.php?app=schedule&m=new');
				g('smarty') -> assign('post_url','index.php?app=schedule&m=ajax');
				g('smarty') -> assign('alldate',$count);
				g('smarty') -> assign('user',$user);
				g('smarty') -> assign('my_id',$my_id);
				g('smarty') -> assign('show_schedule_id',$show_schedule_id);
				g('smarty') -> assign('date',$date);
				g('smarty') -> show($this -> temp_path.'index/index.html');
		}
	}
	
	/**
	 * 获取时间范围内的日程数目
	 */
	private function load_count($see_id = false){
		if(!empty($see_id)){
			$user_id = $see_id;
			$partner_id = $_SESSION[SESSION_VISIT_USER_ID];	
		}else{
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$partner_id = '';
		}
		
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		
		$ret = g('schedule')-> load_count($user_id,$com_id,$partner_id);
		$sc_count = array();
		$repeat = array();
		if(!empty($ret)){
			foreach ($ret as $sc){
				if($sc['repeat_type'] != 0){
						switch ($sc['repeat_type']) {
							case '1':
								$s_day = 0;
								break;
							case '2':
								$s_day = date("w",strtotime($sc['startdate']));
								break;
							case '3':
								$s_day = date("d",strtotime($sc['startdate']));
								break;
							case '4':
								$s_day = date("m-d",strtotime($sc['startdate']));
								break;
							default:
								break;
						}
						$second = strtotime($sc['startdate']) - strtotime($sc['enddate']);
						$day = $second/(24*60*60);
						$day += ($second%(24*60*60) == 0) ? 0 : 1;
						if($day==0) $day = 1;
						$tmp_sc = array(
								'repeat_type' => $sc['repeat_type'],
								'start' => $s_day,
								'state' => $sc['state'],
								'y' => date("Y",strtotime($sc['startdate'])),
								'm' => date("m",strtotime($sc['startdate'])),
								'd' => date("d",strtotime($sc['startdate'])),
								'num' => $day,
							);
						array_push($repeat,$tmp_sc);
						continue;
					}

				if($sc['day']==1){
					$this->set_count($sc_count,$sc['startdate'],$sc['state']);
				}else{
					$start = strtotime($sc['startdate']);
					$end = strtotime($sc['enddate']);
					while($start <= $end){
						$this->set_count($sc_count,$sc['startdate'],$sc['state']);
						$start = strtotime("+1 day", $start); 
						$sc['startdate'] = date('Y-m-d',$start);
					}
				}
			}unset($sc);
		}

		$return = array(
				'data' => $sc_count,
				'repeat' => $repeat,
			);

		return $return;
	}
	
	private function set_count(&$sc_count,$date,$state){
		if(!isset($sc_count[$date])){
			$sc_count[$date] = array($state); 
		}else{
			array_push($sc_count[$date], $state);
		}
	} 
	
	/**
	 * 获取该日期的所有日程
	 * Enter description here ...
	 * @param unknown_type $date
	 */
	private function getAllSchedule($date){
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	
		$result = g('schedule')-> get_by_date($user_id,strtotime($date),strtotime($date)+60*60*24);
		if($result){
			$wait_schedule = array();
			$finsh_schedule = array();
			foreach($result as $sc){
				$sc['schedule_date'] = date('H:i',$sc['schedule_date']);
				if($sc['isfinish']==1)
					array_push($finsh_schedule, $sc);
				else
					array_push($wait_schedule, $sc);
			}
			return array('finish'=>$finsh_schedule,'wait'=>$wait_schedule);
		}else
			return false;
	}
	/**
	 * 获取所有含有日程记录的日期
	 * Enter description here ...
	 */
	private function get_all_date(){
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		return g('schedule')->get_all_date($user_id);
	}
	private function finish(){
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$data = get_var_post('data');
		$sids = explode(',', $data['schedule_id']);
		try{
			g('schedule')->finish($user_id,$sids);
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
}
// end of file