<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'schedule_detail_index.css' => array(
		SYSTEM_APPS . 'schedule/static/css/bootstrap-datetimepicker.min.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/green.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/red.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/blue.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/orange.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/grey.css'
	),
    'schedule_detail_index.js' => array(
        SYSTEM_APPS . 'schedule/static/js/icheck.min.js',
        SYSTEM_APPS . 'schedule/static/js/detail/index.js'
    ),
	'schedule_index_index.css' => array(
		SYSTEM_APPS . 'schedule/static/css/bootstrap-datetimepicker.min.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/green.css',
//		SYSTEM_APPS . 'schedule/static/css/contact.css',
//		SYSTEM_ROOT . 'static/js/icheck/skins/square/green.css'
	),
    'schedule_index_index.js' => array(
//		SYSTEM_ROOT . 'static/js/icheck/icheck.js',
//		SYSTEM_APPS . 'schedule/static/js/contact.js',
        SYSTEM_APPS . 'schedule/static/js/icheck.min.js',
        SYSTEM_APPS . 'schedule/static/js/index/index.js',
        SYSTEM_APPS . 'schedule/static/js/bootstrap-datetimepicker.min.js'
    ),
	'schedule_new_index.css' => array(
		SYSTEM_APPS . 'schedule/static/css/bootstrap-datetimepicker.min.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/green.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/red.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/blue.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/orange.css',
		SYSTEM_APPS . 'schedule/static/css/icheck/grey.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
	),
    'schedule_new_index.js' => array(
        SYSTEM_APPS . 'schedule/static/js/icheck.min.js',
        SYSTEM_APPS . 'schedule/static/js/new/index.js',
        SYSTEM_ROOT . 'static/js/storage.js',
        SYSTEM_ROOT . 'static/js/format.js',
        SYSTEM_APPS . 'schedule/static/js/bootstrap-datetimepicker.min.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js'
    ),
    'schedule_index_contact.css' => array(
		SYSTEM_APPS . 'task/static/css/contact.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/square/green.css'
	),
    'schedule_index_contact.js' => array(
        SYSTEM_ROOT . 'static/js/icheck/icheck.js',
        SYSTEM_APPS . 'schedule/static/js/index/contact.js'
    ),
);

return $arr;

/* End of this file */