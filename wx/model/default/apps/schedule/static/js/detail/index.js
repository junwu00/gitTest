
$(document).ready(function(){
	$('.finish').click(function(){
		frame_obj.do_ajax_post($(this),'finish',
				{
					'schedule_id':$(this).attr('i')
				},
				reload,undefined,undefined,undefined,'数据提交中……');
	});
	
	$('.delete').click(function(){
		frame_obj.comfirm('确认要删除？',function(){
			frame_obj.do_ajax_post(
					$(this),'delete',
					{
						'schedule_id':$('.delete').attr('i')
					},
					delete_reload,undefined,undefined,undefined,'删除中……');
		});
		
	});
});

function reload(data){
	if(data['errcode']==0){
		 location.reload();
	}else{
		frame_obj.alert(data['errmsg']);
	}
}
function delete_reload(data){
	if(data['errcode']==0){
		 location.href=$('#app-url').val();
	}else{
		frame_obj.alert(data['errmsg']);
	}
}