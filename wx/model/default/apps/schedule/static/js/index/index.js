var _move=false;
var _move_top =0;
var _top_top=0;
var height;
var inner;
var startX,//触摸时的坐标
startY,
 x, //滑动的距离
 y,
 aboveY=0; //设一个全局变量记录上一次内部块滑动的位置 
var first = 1;
var activetop=0;//当前日期的高度
$(document).ready(function(){
	$('#see-list').parent().slideUp();
	$('.user_side').click(function(){
		if($(this).parent().children('.side-content').hasClass('side_active')){
			$('.side_active').slideUp();
			$('.side_active').removeClass('side_active');
			return;
		}
		$('.side_active').slideUp();
		$('.side_active').removeClass('side_active');
		$(this).parent().children('.side-content').slideDown();
		$(this).parent().children('.side-content').addClass('side_active');
	});	
	
	$('#ajax-url').val($('#ajax-url').val()+'&m=ajax');
	var json_date = jQuery.parseJSON(alldate);
	init_datetimepicker(json_date);//初始化日期时间选择器
	$('#checkdate').datetimepicker().on('changeDate', function(ev){
		var chooseday = ev.date.getFullYear()+"-"+(ev.date.getMonth()+1)+"-"+ev.date.getDate();
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(null,'load', JSON.stringify({'date':chooseday,'id':$('#uid').val()}), load,undefined,undefined,undefined,'加载日程中……');
		$('#today').val(chooseday);
		activetop = $('.active').offset().top;//重新设置当前日期的高度
		if($('#container').attr('i') == 1) {
			get_today();
		}
	});
	//修改月份标题样式
    $('#checkdate').datetimepicker().on('show', function(evt) {
    	var html = $('.datepicker-days thead tr:eq(0)').html();
    	var mon = $('.datepicker-days thead tr:eq(0) .switch').html();
    	var new_html = '<th class="prev"><a class="black icon-arrow-left"></a></th>' + 
    		'<th  colspan="2" style="font-size:11px;" class="switch">' + mon + '</th>' + 
    		'<th class="next"><a class="black icon-arrow-right"></a></th>' +
    		'<th colspan="2" class="back_btn"><div onclick="back_my_schedule()" style="color: #80C269;"><i class="glyphicon glyphicon-user"></i>&nbsp;我的日程</div></th>' + 
    		'<th><i class="glyphicon glyphicon-plus" style="font-size:20px;line-height:20px;" id="icon-add-schedule"></i></th>';
    	$('.datepicker-days thead tr:eq(0)').html(new_html);
    	var height = $('.bootstrap-datetimepicker-widget').height();
    	$('.container').css('padding-top', height);
    	$('#icon-add-schedule').unbind('click').bind('click',function(){
    		location.href=$('#create_url').val();
    	});
    	$('.bootstrap-datetimepicker-widget').css({'min-height': height});
    	
        //添加伙伴
//        init_add_partner();
    });
    $('#checkdate').datetimepicker('show');
    
    //查看伙伴日程
    init_partner_panel();
    
    $('.dropdown-menu').css('position','absolute');
    $('.dropdown-menu').css('top',"-2px");
    $('.dropdown-menu').css('box-shadow',"none");
    $('.dropdown-menu').css('width','100%');
    $('.dropdown-menu').css('z-index','0');
    
    height = $('.dropdown-menu').offset().top+$(".dropdown-menu").height();
    aboveY = height;
	//$('.container').height($('.container').height()-height-45);
   
    activetop = $('.active').offset().top;
    init_data();//数据加载完成之后初始化方法
    c_search_init_toggle();
    
});


//显示/隐藏选人控件
function c_search_init_toggle() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
		var curr_selected = {};
		var type = '';
		if($(this).hasClass('leader-add')){
			$('.group_item.leader').each(function() {
				var selected_id = $(this).attr('i');
				var selected_name = $(this).children('.group_item_name').html();
				curr_selected[selected_id] = {id: selected_id, name: selected_name};
			});
			type="leader";
		}else{
			$('.group_item.jiandu').each(function() {
				var selected_id = $(this).attr('i');
				var selected_name = $(this).children('.group_item_name').html();
				curr_selected[selected_id] = {id: selected_id, name: selected_name};
			});
			type="jiandu";
		}
		//初始化选人控件
		c_search_init({
			select_num:1,
			type:2,
			selected: curr_selected,
			before	: function() {
				$('#add-type').val(type);
			},
			ok	: function(selected_user) {
			    $('#checkdate').datetimepicker('show');
			    $('.dropdown-menu').css('position','absolute');
			    $('.dropdown-menu').css('top',"-2px");
			    $('.dropdown-menu').css('box-shadow',"none");
			    $('.dropdown-menu').css('width','100%');
			    $('.dropdown-menu').css('z-index','0');
		    	$('.container').css('padding-top', 295);
				
				$('.container').css('display','block');
				$(".contact-content").css('display','none');
				var html="";
				var uid = 0; 
				var type = $('#add-type').val();
				for (var i in selected_user) {
					if (!selected_user[i])		continue;
					var user = selected_user[i];
					html = '<span n="'+user['name']+'" p="'+user['pic']+'" ></span>'
					uid = i;
				}
				if(uid !=0 && uid !="" && uid != null){
					show_partner_data($(html), uid);
				}
			}
		});
		
	});
}



//返回我的日程
function back_my_schedule(){
	$('#uid').val('');
	$('#partner-handler').html('<b>我</b> 的日程<i class="icon-angle-right"></i>')
	
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(null,'load_count',JSON.stringify({}),function(data){
		$('#checkdate').datetimepicker('setHaveDate', data['data']['data']);
		$('#checkdate').datetimepicker('setHaveRepeat', data['data']['repeat']);
		$('#checkdate').datetimepicker('update');
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(null,'load', JSON.stringify({'date':$('#today').val(),'id':$('#uid').val()}), load,undefined,undefined,undefined,'加载日程中……');
	});
}

function get_partner(){
//	if(first != 1) return;
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(null,'load_partner', JSON.stringify({'type':3}), function(data){
		if(data['errcode']==0){
			var html = '';
			var see_html = '';
			if(data['info']['concern']==''){
				$('.concern_count').html(0);
				html = '<div class="partner-item" style="text-align:center;"> 您还没有关注任何人，<a href="index.php?app=schedule&&m=concern">立即添加</a></div>';
			}else{
				$('.concern_count').html(data['info']['concern'].length);
				for(var i = 0;i<data['info']['concern'].length;i++){
					var partner = data['info']['concern'][i];
					var pic_url = '';
					if(partner.pic_url == null || partner.pic_url== ""){
						pic_url = 'static/image/face.png';
					}else{
						pic_url = partner.pic_url+'64';
					}
					html += '<div class="partner-item" p="'+partner['pic_url']+'64" n="'+partner['name']+'" onclick="show_partner_data(this, '+partner['id']+')";>'+
								'<span class="status icon-schedule"></span>'+
								'<img src="'+pic_url+'"> '+partner['name']+
							'</div>';
				}
			}
			
			if(data['info']['see']==''){
				$('.see_count').html(0);
				see_html = '<div class="partner-item" style="text-align:center;"> 无记录 </div>';
			}else{
				$('.see_count').html(data['info']['see'].length);
				for(var i = 0;i<data['info']['see'].length;i++){
					var partner = data['info']['see'][i];
					var pic_url = '';
					if(partner.pic_url == null || partner.pic_url== ""){
						pic_url = 'static/image/face.png';
					}else{
						pic_url = partner.pic_url+'64';
					}
					see_html += '<div class="partner-item" p="'+partner['pic_url']+'64" n="'+partner['name']+'" onclick="show_partner_data(this, '+partner['id']+')";>'+
								'<span class="status icon-schedule"></span>'+
								'<img src="'+pic_url+'"> '+partner['name']+
							'</div>';
				}
			}
			$('#see-list').html(see_html);
			$('#partner-list').html(html);
//			first = 0;
		}else{
		}
	});
    
}

/*初始化选择伙伴控件
function init_add_partner() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		//初始化现有伙伴 TODO
//		$(this).parents('.side-content').find('.select_people').each(function() {
//			var selected_id = $(this).attr('i');
//			var selected_name = $(this).attr('n');
//			selected[selected_id] = {id: selected_id, name: selected_name};
//		});
		var instance_id = $(this).attr("id");
		var opt = {
			selected	: selected,
			before		: function() {
//				$("#sure_div").val(instance_id);
			},
			ok			: function(selected) {
				console.log(selected);

				$(c_search_form).css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
				$('.bootstrap-datetimepicker-widget').css({display: 'block'});
				$('#bottom-menu').css({display: 'block'});
			}
		}
		console.log(opt);
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
		$('.bootstrap-datetimepicker-widget').css({display: 'none'});
		$('#bottom-menu').css({display: 'none'});
	});
}
*/

//查看伙伴日程
function init_partner_panel() {
	var win_height = $(window).height();
	$('#partner-panel').css({height: win_height});
	$('#partner-list').css({height: win_height-(88+97)});
	$('#see-list').css({height: win_height-(88+97)});
	
	//显示
	$('#partner-handler').unbind('click').bind('click', function() {
		var from = $(this).attr('data-from');
		var to = $(this).attr('data-to');
		
		$(to).css({display: 'block'});
		$('#partner-panel > .row').animate({left: 0}, 'normal');
		$('#partner-bottom-bar').animate({left: 0}, 'normal');
		$('#partner-panel').animate({left: 0}, 'normal', function() {
			$(from).css({display: 'none'});
			$('.bootstrap-datetimepicker-widget').css({display: 'none'});
			$('#bottom-menu').css({display: 'none'});
		});
		get_partner();
	});
	//隐藏
	$('#partner-back').unbind('click').bind('click', function() {
		var from = $(this).attr('data-from');
		var to = $(this).attr('data-to');

		$(to).css({display: 'block'});
		$('.bootstrap-datetimepicker-widget').css({display: 'block'});
		$('#bottom-menu').css({display: 'block'});
		$('#partner-bottom-bar').animate({left: '100%'}, 'normal');
		$('#partner-panel > .row').animate({left: '100%'}, 'normal');
		$('#partner-panel').animate({left: '100%'}, 'normal', function() {
			$(from).css({display: 'none'});
		});
	});
}

//显示伙伴日程
function show_partner_data(el, user) {
	$('.partner-item .status').removeClass('icon-partner-ok');
	$(el).find('.status').addClass('icon-partner-ok');
	
	$('#uid').val(user);
	$('#partner-handler').html('<img src="'+$(el).attr('p')+'" > <b>'+$(el).attr('n')+'</b> 的日程<i class="icon-angle-right"></i>');
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(null,'load_count',JSON.stringify({id:user}),function(data){
		$('#checkdate').datetimepicker('setHaveDate', data['data']['data']);
		$('#checkdate').datetimepicker('setHaveRepeat', data['data']['repeat']);
		$('#checkdate').datetimepicker('update');
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(null,'load', JSON.stringify({'date':$('#today').val(),'id':$('#uid').val()}), load,undefined,undefined,undefined,'加载日程中……');
	});
	
	
	$('#container').css({display: 'block'});
	$('.bootstrap-datetimepicker-widget').css({display: 'block'});
	$('#bottom-menu').css({display: 'block'});
	$('#partner-bottom-bar').animate({left: '100%'}, 'normal');
	$('#partner-panel > .row').animate({left: '100%'}, 'normal');
	$('#partner-panel').animate({left: '100%'}, 'normal', function() {
		$('#partner-panel').css({display: 'none'});
	});
}

//显示日程详情
function show_partner_detail(el, s_id) {
	//显示
	$('#partner-detail-panel').css({display: 'block'});
	$('#partner-detail-bottom-bar').animate({left: 0}, 'normal');
	$('#partner-detail-panel').animate({left: 0}, 'normal', function() {
		$('#container').css({display: 'none'});
		$('.bootstrap-datetimepicker-widget').css({display: 'none'});
		$('#bottom-menu').css({display: 'none'});
	});
	$('#ajax-url').val($('#post_url').val());
	clear_data();
	frame_obj.do_ajax_post(null,'detail',JSON.stringify({i:s_id}),function(data){
		if(data['errcode']==0){
			init_detail(data['info']);
		}else{
			$('html').html(get_err_content(data['errmsg']));
			setTimeout(function(){
				location.href="index.php?app=schedule";
			},2500);
		}
	},undefined,undefined,undefined,'数据加载中……');
	
	//隐藏
	$('#partner-detail-back').unbind('click').bind('click', function() {
		var from = $(this).attr('data-from');
		var to = $(this).attr('data-to');
		
		$(to).css({display: 'block'});
		$('.bootstrap-datetimepicker-widget').css({display: 'block'});
		$('#bottom-menu').css({display: 'block'});
		$('#partner-detail-bottom-bar').animate({left: '100%'}, 'normal');
		$('#partner-detail-panel').animate({left: '100%'}, 'normal', function() {
			$(from).css({display: 'none'});
		});
	});
}

function clear_data(){
	$('.detail-panel-head').html("");
	$('.detail-panel-title').html("");
	$('.detail-panel-warn').html("");
	$('.detail-panel-main').html("");
	$('.detail-panel-creater').html("");
}

function init_detail(info){
	clear_data();
	
	
	$('.detail-panel-head').html(info['title']);
	
	var html = '';
	if(info['all_day']==1){
		if(info['start_date'] == info['end_date'] ){
			html = '<span><b>全天</b><br></span><span class="font-light">'+info['start_date']+' '+get_week(new Date(info['start_date']).getDay())+'</span>';
		}else{
			html = '<span><b>全天</b><br></span>';
			html += '<span class="font-light">开始时间：</span><span><b>'+info['start_date']+' '+get_week(new Date(info['start_date']).getDay())+'</b></span><br>';
			html += '<span class="font-light">结束时间：</span><span><b>'+info['end_date']+' '+get_week(new Date(info['end_date']).getDay())+'</b></span>';
		}
	}else{
		html = '<span class="font-light">开始时间：</span><span><b>'+info['start_date']+' '+get_week(new Date(info['start_date']).getDay())+'</b></span><br>';
		html += '<span class="font-light">结束时间：</span><span><b>'+info['end_date']+' '+get_week(new Date(info['end_date']).getDay())+'</b></span>';
	}
	$('.detail-panel-title').html(html);
	
	var main_html = '';
	if(info['content']!=""){
		main_html = ' <b>'+info['content']+'</b><br>';
	}
	if(info['place']!=''){
		main_html += '<span class="font-light detail-panel-addr">'+info['place']+'</span><br>';
	}
	if(info['share_type']!=0){
		if(info['share_type'] == 1){
			main_html += '<span>谁可以查看：<b>所有</b></span><br>';
		}else{
			var user_name = '';
			var group_name = '';
			if(info['group_name'] !="" && info['group_name']!=null){
				group_name = info['group_name'];
			}
			for(var i in info['user_list']){
				var user = info['user_list'][i];
				user_name += ' '+user['name'];
			}
			if(user_name!="" && group_name !=''){
				main_html += '<span>谁可以查看：<b>'+group_name+';'+user_name+'</b></span><br>';
			}else if(user_name=="" && group_name ==''){
				main_html += '<span>谁可以查看：<b>无</b></span><br>';
			}else{
				main_html += '<span>谁可以查看：<b>'+group_name+user_name+'</b></span><br>';
			}
		}
	}
	$('.detail-panel-main').html(main_html);
	
	var warn_html = '';
	if($('#currid').val() == info['user_id']){
		warn_html += '<span class="font-light">提醒：</span> <b>'+info['remind']+'</b><br>';
		warn_html += '<span class="font-light">重复：</span> <b>'+info['repeat']+'</b>';
		$('.detail-panel-warn').html(warn_html);
		$('.detail-panel-warn').removeClass('hide');
	}else{
		$('.detail-panel-warn').addClass('hide');
	}
	
	var create_html = '';
	create_html += '<span class="lev"><i class="lev'+info['state']+'"></i> '+info['state_name']+'</span>';
	create_html += '<span class="creater"><span class="font-light">创建人: </span> <b>'+info['user_name']+'</b></span>';
	$('.detail-panel-creater').html(create_html);
	
	if($('#currid').val() == info['user_id']){
		$('.edit_btn').css('display','inline');
		$('.delete_schedule').css('display','inline');
		$('.edit_btn').attr('href','?app=schedule&m=new&a=index&schedule_id='+info['id']);
		$('.delete_schedule').unbind('click').bind('click',function(){
			frame_obj.comfirm('确定要删除该日程？',function(){
				frame_obj.do_ajax_post(null,'remove_schedule',JSON.stringify({sid:info['id']}),function(data){
					if(data['errcode']==0){
						$('#partner-detail-back').click();
						$('#ajax-url').val($('#post_url').val());
						frame_obj.do_ajax_post(null,'load_count',JSON.stringify({id:info['user_id']}),function(data){
							$('#checkdate').datetimepicker('setHaveDate', data['data']['data']);
							$('#checkdate').datetimepicker('setHaveRepeat', data['data']['repeat']);
							$('#checkdate').datetimepicker('update');
							$('#ajax-url').val($('#post_url').val());
							frame_obj.do_ajax_post(null,'load', JSON.stringify({'date':$('#today').val(),'id':$('#uid').val()}), load,undefined,undefined,undefined,'加载日程中……');
						});
					}else{
						frame_obj.alert(data['errmsg']);
					}
				});
			});
		});
	}else{
		$('.edit_btn').css('display','none');
		$('.delete_schedule').css('display','none');
	}
}

function get_week(i){
	var week ='';
	switch(i){
		case 0: week = '（星期日）';break;
		case 1: week = '（星期一）';break;
		case 2: week = '（星期二）';break;
		case 3: week = '（星期三）';break;
		case 4: week = '（星期四）';break;
		case 5: week = '（星期五）';break;
		case 6: week = '（星期六）';break;
	}
	return week;
}


function init_data(){
	var uid = $('#uid').val();
	if(uid != ''){
		var pic_url = $('#pic_url').val();
		var name = $('#name').val();
		$('#partner-handler').html('<img src="'+pic_url+'" > <b>'+name+'</b> 的日程<i class="icon-angle-right"></i>');
	}else{
		$('#partner-handler').html(' <b>我</b> 的日程<i class="icon-angle-right"></i>');
	}
}


function load(data){
	var html='';
	if(data['errcode']==0){
		if(data['data'] ==''){
			html +='<div class="partner-detail detail-empty"><span class="detail-body">今天没有日程安排</span></div>';
		}else{
			for(var i=0;i<data['data'].length;i++){
				var schedule = data['data'][i];
				var allday = '';
				if(schedule['all_day']==1){
					allday = '<span class="detail-head">全天</span>';
				}else{
					allday = '<span class="detail-head time-head">'+schedule['s_time']+'<br>~<br>'+schedule['e_time']+'</span>';
				}
				
				html += '<div class="partner-detail detail-lev'+schedule['state']+'" onclick="show_partner_detail(this, '+schedule['id']+')">'+
							allday+
							'<span class="detail-body">'+schedule['title']+'</span>'+
						'</div>';
			}
		}
		$('#partner-body').html(html);
	}else{
		frame_obj.alert(data['errmsg']);
	}
	
   if($('#show_schedule_id').val() !=0 && $('#show_schedule_id').val() !=""){
    	show_partner_detail(undefined,$('#show_schedule_id').val());
    	$('#show_schedule_id').val(0);
    }
	
}
//完成后刷新
function reload(data){
	frame_obj.alert(data['errmsg'],'确定',function(){
		if(data['errcode']==0){
			location.reload(); 
		}
	});
}
function init_datetimepicker(data){
		$('#checkdate').datetimepicker({
		      pickTime: false,
		      language:'cn',
		      haveDate:{data:data['data']},
		      haveRepeat:{data:data['repeat']},
		});
		$('#ajax-url').val($('#post_url').val());
		
		if($('#uid').val() != ''){
			
		}else{
			
		}
		frame_obj.do_ajax_post(null,'load', JSON.stringify({'date':$('#today').val(),'id':$('#uid').val()}), function(data){
			load(data);
		},undefined,undefined,undefined,'加载日程中……');
//      greyDate:{cls:'grey0',data:grey},
//      blueDate:{cls:'blue0',data:blue},
//      orangeDate:{cls:'orange0',data:orange},
//      redDate:{cls:'red0',data:red},
//      
//      grey1Date:{cls:'grey1',data:grey1},
//      blue1Date:{cls:'blue1',data:blue1},
//      orange1Date:{cls:'orange1',data:orange1},
//      red1Date:{cls:'red1',data:red1},
 
}


function get_err_content(errmsg){
	var html = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="description" content="三尺科技"/>  <meta http-equiv="X-UA-Compatible" content="IE=edge">  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" /><meta name="apple-mobile-web-app-capable" content="yes"> <meta name="apple-mobile-web-app-status-bar-style" content="black"> <meta name="format-detection" content="telephone=no"><title>错误</title><style type="text/css">* {font-family: "Microsoft YaHei", "微软雅黑", Helvetica, "黑体",Arial,Tahoma; color: #333; font-weight: normal;}html, body {margin: 0; padding: 0;}	#img-bar {width: 100%; text-align: center; background: url("static/image/err_bg.png") no-repeat; background-size: 100% 100%; padding: 15px 0;}h3 {width: 90%; margin: 0 auto; border-bottom: 1px solid #888; text-align: center; padding: 15px 0;}#main {width: 90%; margin: 0 auto; padding-top: 15px;}#main p {text-indent: 2em;}</style></head><body><div id="img-bar"><img src="/static/image/err.png"></div><h3>哦噢，出错了！</h3><div id="main">可能的原因：<p>'+errmsg+'</p></div></body></html>';
	return html;
}
