partner_list_load = false;
var curr_selected = {};
$(document).ready(function(){
	init_date();
	c_search_init_toggle();
	var win_height = $(window).height();
	$('.group_user_list').css({height:win_height-137});
	
	if($('#schedule_id').val() != ""){
		$('.create_remind').css('display','none');
		init_edit_data();
		$('#save').html('保存');
	}else{
		$storage.init({
			key : 'new_schedule_storage',
			ckey:['#title','#repeat','input[name=level]','#remind','#detail','#addr','#is_all_day','#s_time','#e_time'],
		});
		init_time();
	}

});

//创建并通知
function create_remind(){
	frame_obj.comfirm('日程创建完成之后，将立即通知被分享人，确定要继续创建吗？',function(){
		creat_save(1);
	});
}


function init_time(){
	var myDate = new Date();
	var time = time2date(myDate.getTime(),'yyyy-MM-dd HH:mm');
	$('#s_time').val(time);
	$('#e_time').val(time);
	var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	$('#s_time').next().html(weekDay[myDate.getDay()]);
	$('#e_time').next().html(weekDay[myDate.getDay()]);
}

function init_edit_data(){
	$('#ajax-url').val($('#get_edit_data').val());
	frame_obj.do_ajax_post(null,'detail',JSON.stringify({i:$('#schedule_id').val()}), function(data){
		if(data['errcode']!=0){
			frame_obj.alert(data['errmsg']);
			return;
		}else{
			var info = data.info;
				//标题
				$('#title').val(info.title);
				//是否全天以及开始结束时间
				if(info.all_day == 1){
					$('#is_all_day').attr('checked','checked');
					
					$('#s_time').val(info.start_date.substr(0,10));
					var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
					var dateStr = info.start_date;
					var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
					$('#s_time').next().html(weekDay[myDate.getDay()]);
					
					$('#e_time').val(info.end_date.substr(0,10));
					dateStr = info.end_date;
					myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
					$('#e_time').next().html(weekDay[myDate.getDay()]);
				}else{
					$('#s_time').val(info.start_date);
					var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
					var dateStr = info.start_date;
					var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
					$('#s_time').next().html(weekDay[myDate.getDay()]);
					
					$('#e_time').val(info.end_date);
					dateStr = info.end_date;
					myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
					$('#e_time').next().html(weekDay[myDate.getDay()]);
				}
				//重复
				$('#repeat').val(info.repeat_type);
				//重要程度
				$('input[type="radio"][name="level"][value="'+info.state+'"]').attr('checked','checked');
				//提醒
				$('#remind').val(info.remind_type);
				//详情
				$('#detail').val(info.content);
				//地点
				$('#addr').val(info.place);
				//是否分享//分享人
				if(info.share_type > 0){
					if(info.remind_share = 1){
						$('#remind_share').attr('checked','checked');
						$('.remind_share_div').removeClass('hide');
					}
					
					$('#is_share').attr('checked','checked');
					$('.partner_class').removeClass('hide');
					$('#is_share').parent().removeClass('others_last_div');
					if(info.share_type == 2){
						var user_html = '';
						var share_names = "";
						var group_name = "";
						var user_name = "";
						var choose_html = '';
						if(info.group_list !="" && info.group_list !=null){
							user_html += '<input type="hidden" class="share_group" n="'+info.group_name+'" value="'+info.group_list+'">';
							group_name = info.group_name;
						}
						for(var i = 0; i<info.user_list.length; i++){
							user_html += '<input type="hidden" class="share_user" n="'+info.user_list[i].name+'" value="'+info.user_list[i].id+'">';
							user_name += ' ' + info.user_list[i].name;
							choose_html += '<li i="'+info.user_list[i].id+'" n="'+info.user_list[i].name+'" ><span>'+info.user_list[i].name+'<i onclick="$(this).parent().parent().remove();" class="glyphicon glyphicon-remove"></i></span></li>';
						}
						if(group_name == "" || user_name == ""){
							$('#partner').html(group_name + user_name);
						}else{
							$('#partner').html(group_name +'；'+ user_name);
						}
						$('.user_list').html(choose_html);
						$('#partner').append(user_html);
						$('.special_partner').click();
						$('.special_partner').attr('d',info.group_list);
					}else{
						$('#partner').html('所有');
					}
				}else{
					$('.partner_class').addClass('hide');
					$('#is_share').parent().addClass('others_last_div');
				}
		}
	},undefined,undefined,undefined,'数据加载中……');
}



function group_delete(){
	var id = $('#group_id').val();
	frame_obj.do_ajax_post(null,'delete_group',JSON.stringify({group_id:id}), function(data){
		if(data['errcode']==0){
			group_cancel();
			$('.special_partner').click();
		}else{
			frame_obj.alert(data['errmsg']);
		}
	},undefined,undefined,undefined,'删除分组信息……');
}

//修改分组信息
function edit_group(id){
	$('.delete_li').removeClass('hide');
	$('#group_id').val(id);
	$('.group_msg').each(function(){
		$(this).removeClass('hide');
	});
	$('.select_partner').each(function(){
		$(this).addClass('hide');
	});
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(null,'get_group_number',JSON.stringify({group_id:id}), function(data){
		var info = data.info;
		var list_html = "";
		$('#group_name').val(info['group']);
		if(info['user'] !="" ){
			for(var i = 0; i < info['user'].length; i++){
				if(info['user'][i]['pic_url'] == null ){
					info['user'][i]['pic_url'] = 'static/image/face.png';
				}else{
					info['user'][i]['pic_url'] = info['user'][i]['pic_url']+'64';
				}
				list_html += '<div  onclick="$(this).remove();" i="'+info['user'][i]['id']+'" n="'+info['user'][i]['name']+'" p="'+info['user'][i]['pic_url']+'" ><span class="glyphicon glyphicon-minus-sign delete_btn"></span><img src="'+info['user'][i]['pic_url']+'" ><br>'+info['user'][i]['name']+'</div>';
			}
		}
		$('.group_user_list').html(list_html);
		var partner_list_load = true;
	},undefined,undefined,undefined,'加载分组信息……');
	
}
//添加分组
function add_group(){
	$('#group_id').val(0);
	$('.group_msg').each(function(){
		$(this).removeClass('hide');
	});
	$('.select_partner').each(function(){
		$(this).addClass('hide');
	});
	
	$('.delete_li').addClass('hide');
	$('#group_name').val("");
	$('.group_user_list').html("");
}

//显示/隐藏选人控件
function c_search_init_toggle() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
		var curr_selected = [];
		var type = $(this).attr('t');
		if( type == 'add_user'){
			$('.user_list li').each(function(){
				var selected_id = $(this).attr('i');
				var selected_name = $(this).attr('n');
				curr_selected[selected_id] = {id: selected_id, name: selected_name};
			});
		}else if( type == 'add_group_user'){
			$('.group_user_list div').each(function(){
				var selected_id = $(this).attr('i');
				var selected_name = $(this).attr('n');
				var selected_pic = $(this).attr('p');
				curr_selected[selected_id] = {id: selected_id, name: selected_name,pic:selected_pic};
			});
		}
		//初始化选人控件
		c_search_init({
			type:2,
			selected: curr_selected,
			ok	: function(selected_user) {
				if(type == 'add_user'){
					$('.select_partner,#bottom-menu').css('display','block');
					var html ="";
					for (var i in selected_user) {
						if (!selected_user[i])		continue;
						html += '<li i="'+selected_user[i]['id']+'" n="'+selected_user[i]['name']+'" p="'+selected_user[i]['pic']+'"><span>'+selected_user[i]['name']+'<i onclick="$(this).parent().parent().remove();" class="glyphicon glyphicon-remove"></i></span></li>';
					}
					$('.user_list').html(html);
				}else if(type == 'add_group_user'){
					$('.group_msg,#bottom-menu').css('display','block');
					var html ="";
					for (var i in selected_user) {
						if (!selected_user[i])		continue;
						html += '<div onclick="$(this).remove();" i="'+selected_user[i]['id']+'" n="'+selected_user[i]['name']+'" p="'+selected_user[i]['pic']+'" ><span class="glyphicon glyphicon-minus-sign delete_btn"></span><img src="'+selected_user[i]['pic']+'" ><br>'+selected_user[i]['name']+'</div>';
					}
					$('.group_user_list').html(html);
				}
			}
		});
		
	});
}





function init_date(){
	init_datetime_scroll('#s_time');
	init_datetime_scroll('#e_time');
}

function set_time_day(that){
	var weekDay = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	var dateStr = $(that).val();
	var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
	$(that).next().html(weekDay[myDate.getDay()]);
	if($('#is_all_day')[0].checked){
		$(that).val($(that).val().substr(0,10));
	}
}

function all_day_change(that){
	if($(that)[0].checked){
		if($.trim($('#s_time').val()).length > 0){
			$('#s_time').val($('#s_time').val().substr(0,10));
		}
		if($.trim($('#e_time').val()).length > 0){
			$('#e_time').val($('#e_time').val().substr(0,10));
		}
	}else{
		$('#s_time').val($('#s_time').val() + ' 00:00');
		$('#e_time').val($('#e_time').val() + ' 23:59');
		//$('#s_time').next().html('');
		//$('#e_time').next().html('');
	}
}

function share_change(that){
	if($(that)[0].checked){
		$('.partner_class').removeClass('hide');
		$('.remind_share_div').removeClass('hide');
		$(that).parent().removeClass('others_last_div');
	}else{
		$('.partner_class').addClass('hide');
		$('.remind_share_div').addClass('hide');
		$(that).parent().addClass('others_last_div');
	}
}

function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};
	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

function select_partner(){
	$('.creat_new').each(function(){
		$(this).addClass('hide');
	})
	$('.select_partner').each(function(){
		$(this).removeClass('hide');
	})
}

function group_cancel(){
	$('.select_partner').each(function(){
		$(this).removeClass('hide');
	})
	$('.group_msg').each(function(){
		$(this).addClass('hide');
	})
}

function group_ok(){
	var data = {};
	data.group_name = $('#group_name').val();
	data.user_list = [];
	$('.group_user_list div').each(function(){
		var id = $(this).attr('i');
		if(id !="" ){
			data.user_list.push(id);
		}
	});
	
	if($('#group_id').val() == 0){
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(null,'add_group',JSON.stringify(data), function(reponse){
			if(reponse['errcode'] == 0){
				group_cancel();
				var group = reponse['group'];
				var html = '<li><label class="group_label"><input type="checkbox" n="'+group['group_name']+'" value="'+group['id']+'" class="ck3" name="group_name" >&nbsp;&nbsp;&nbsp;&nbsp;'+group['group_name']+'</label><span onclick="edit_group('+group['id']+')" class="glyphicon glyphicon-pencil"></span></li>';
				$('.group_list').append(html);
				change_choose();
			}else{
				frame_obj.alert(reponse['errmsg']);
			}
			var partner_list_load = true;
		},undefined,undefined,undefined,'创建分组信息……');
	}else{
		data.group_id = $('#group_id').val();
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(null,'edit_group',JSON.stringify(data), function(reponse){
			if(reponse['errcode'] == 0){
				group_cancel();
				var group = $('input[name=group_name][value='+data.group_id+']').parent();
				group.html('<input type="checkbox" n="'+$('#group_name').val()+'" value="78" class="ck3" name="group_name">&nbsp;&nbsp;&nbsp;&nbsp;'+$('#group_name').val());
				//group.attr('n',$('#group_name').val());
				//group.parent().children('span').html('&nbsp;'+$('#group_name').val());
				change_choose();
			}else{
				frame_obj.alert(reponse['errmsg']);
			}
			var partner_list_load = true;
		},undefined,undefined,undefined,'保存分组信息……');
	}
}

function partner_cancel(){
	$('.creat_new').each(function(){
		$(this).removeClass('hide');
	})
	$('.select_partner').each(function(){
		$(this).addClass('hide');
	})
}

function partner_ok(){
	partner_data();
	partner_cancel();
}

function partner_data(){
	if($('input[name=partner_type]:checked').val() == 1){
		$('#partner').html('所有');
	}else{
		var user = '';
		var user_id = '';
		var share_html = "";
		$('.user_list li').each(function(){
			user += ' ' + $(this).attr('n');
			share_html += '<input type="hidden" class="share_user" n="'+$(this).attr('n')+'" value="'+$(this).attr('i')+'" > ';
		});
		var group_name = $('input[name=group_name]:checked').attr('n');
		var group_id = $('input[name=group_name]:checked').val();
		if($('input[name=group_name]:checked')){
			share_html += '<input type="hidden" class="share_group" n="'+$('input[name=group_name]:checked').attr('n')+'" value="'+$('input[name=group_name]:checked').val()+'" > ';
		}
		if(group_name==undefined ){
			group_name= '';
		}
		if(user != '' && group_name !=''){
			$('#partner').html(group_name +'；'+ user);
		}else if(user == '' && group_name ==''){
			$('#partner').html('');
		}else{
			$('#partner').html(group_name + user);
		}
		
		$('#partner').append(share_html);
	}
}

function check_partner_type(that){ 
	$(that).find('input').attr('checked','checked');
	if($(that).attr('data-v') == 'special'){
		$('.partner_list').removeClass('hide');
		if(!partner_list_load){
			$('#ajax-url').val($('#partner_list_ajax').val());
			frame_obj.do_ajax_post(null,'load_group',JSON.stringify({}), function(data){
				var list = data.list;
				var list_html = "";
				if(list != "")
				for(var i = 0; i < list.length; i++){
					list_html += '<li>';
					list_html += '<label class="group_label">';		
					list_html += '<input type="checkbox" n="'+list[i].group_name+'" value="'+list[i]['id']+'" class="ck3" name="group_name" >';
					list_html += '&nbsp;&nbsp;&nbsp;&nbsp;'+list[i].group_name;
					list_html += '</label>';
					list_html += '<span onclick="edit_group('+list[i]['id']+')" class="glyphicon glyphicon-pencil"></span>';
					list_html += '</li>';
				}
				$('.group_list').html(list_html);
				change_choose();
				if($(that).attr('d') != ''){
					$('input[name=group_name][value='+$(that).attr('d')+']').click();
					$(that).attr('d',"");
				}
				var partner_list_load = true;
			},undefined,undefined,undefined,'加载分组列表……');
		}
	}else{
		$('.partner_list').addClass('hide');
		$('.partner_list').find('li').each(function(){
			$(this).find('input[type="checkbox"]').removeAttr('checked');
		});
	}
}

function change_choose(){
	$('.group_label input[type=checkbox]').each(function(){
		$(this).unbind('change').bind('change',function(event){
			event.stopPropagation();
			var input = $(this);
			$('.group_label input[type=checkbox]').each(function(){
				if($(this).val() != input.val()){
					$(this)[0].checked = false;
				}
			});
			return false;
		});
	});
}


function partner_li_click(that){
	if($(that).find('input[type="checkbox"]')[0].checked){
		$(that).find('input[type="checkbox"]').removeAttr('checked');
	}else{
		$(that).find('input[type="checkbox"]').attr('checked','checked');
	}
} 

function creat_cancel(){
	if(history.length <= 1){
		WeixinJSBridge.call('closeWindow');
	}else{
		var schedule_id = $('#schedule_id').val();
		if(schedule_id !="" && schedule_id !=0){
			location.href = 'index.php?app=schedule&sid='+schedule_id;
		}else{
			history.go(-1);
		}
	}
}

function creat_save(create_remind){
	var data = get_data();
	if(check_data(data)){
		var tmp_data = {};
		var schedule_id = data.id;
		tmp_data.schedule_id = data.id;
		tmp_data.title = data.title;
		tmp_data.startdate = data.s_time.substr(0,10);
		tmp_data.starttime = data.s_time.substr(10,15);
		tmp_data.enddate = data.e_time.substr(0,10);
		tmp_data.endtime = data.e_time.substr(10,15);
		tmp_data.state = data.level;
		tmp_data.allday = data.is_all_day == true ? 1 : 0;
		tmp_data.repeat = data.repeat;
		tmp_data.remind = data.remind;
		if(data.is_share){
			if(data.share_all){
				tmp_data.share_type = 1;
			}else{
				tmp_data.share_type = 2;
				tmp_data.share_user = data.user_list;
				tmp_data.share_group = data.group;
				tmp_data.share_dept = "";
			}
			if(data.remind_share == 1){
				tmp_data.remind_share = 1;
			}else{
				tmp_data.remind_share = 0;
			}
		}else{
			tmp_data.share_type = 0;
		}
		if(create_remind == 1){
			tmp_data.create_remind = 1;
		}else{
			tmp_data.create_remind = 0;
		}
		
		
		tmp_data.place = data.addr;
		tmp_data.desc = data.detail;
		$('#ajax-url').val($('#save_ajax').val());
		frame_obj.do_ajax_post(null,'save',JSON.stringify(tmp_data), function(data){
			if(data['errcode']!=0){
				frame_obj.alert(data['errmsg']);
				$('#create_remind').val(0);
				return;
			}else{
				$storage.clear();
				location.href = $('#list').val()+'&sid='+schedule_id;
			}
		},undefined,undefined,undefined,'数据提交中……');
	}
}

function get_data(){
	var data = {};
	if($('#schedule_id').val() !=""){
		data.id = $('#schedule_id').val(); 
	}else{
		data.id = 0;
	}
	data.title = $('#title').val();
	data.is_all_day = $('#is_all_day')[0].checked;
	data.s_time = $('#s_time').val();
	data.e_time = $('#e_time').val();
	data.repeat = $('#repeat').val();
	data.level = $('input[type="radio"][name="level"]:checked').val();
	data.remind = $('#remind').val();
	
	data.detail = $('#detail').val();
	data.addr = $('#addr').val();
	data.is_share = $('#is_share')[0].checked;
	if(data.is_share){
		var share_choose = $('input[type="radio"][name="partner_type"]:checked').parent().attr('data-v');
		if(share_choose == "all"){
			if($('#remind_share:checked').val() == 1){
				data.remind_share = 1; 
			}else{
				data.remind_share = 0;
			}
			data.share_all = true;
		}else{
			if($('#remind_share:checked').val() == 1){
				data.remind_share = 1; 
			}else{
				data.remind_share = 0;
			}
			data.share_all = false;
			var user_list = [];
			$('#partner>.share_user').each(function(){
				user_list.push($(this).val());
			});
			var group = $('#partner>.share_group').val();
			if(group == "undefined"){
				group = '';
			}
			data.user_list = user_list;
			data.group = group;
		}
	}
	return data;
}

function check_data(data){
	if($.trim(data.title).length == 0){
		frame_obj.alert('请输入标题');
		return false;
	}
	if($.trim(data.s_time).length == 0){
		frame_obj.alert('请输入开始时间');
		return false;
	}
	if($.trim(data.s_time).length == 0){
		frame_obj.alert('请输入结束时间');
		return false;
	}
	if($.trim(data.level).length == 0){
		frame_obj.alert('请选择日程重要程度');
		return false;
	}
	if(data.is_share){
		if(!data.share_all){
			console.log(data);
			if(data.user_list == '' && data.group == ''){
				frame_obj.alert('请选择分享范围');	
				return false;
			}
		}
	}
	return true;
}
