<?php
/**
 * 应用的配置文件_工作日程
 * 
 * @author chenyihao
 * @date 2014-12-03
 * 
 */

return array(
	'id' 				=> 8,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(8),
	'name' 				=> 'schedule',
	'cn_name' 			=> '工作日程',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Calendar.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '我的日程', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&&m=index', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '新建日程', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&&m=new', 'icon' => '')
		),
		1 => array(
			array('id' => 'bmenu-1', 'name' => '我的日程2', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&&m=index', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '新建日程2', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=schedule&&m=new', 'icon' => '')
		),
	),
);

// end of file