<?php
/**
 * 全部组织活动ajax行为的交互类
 *
 * @author Chnegyh
 * @create 2015-10-19
 */
class cls_ajax extends abs_app_base {
    public function __construct() {
        parent::__construct('activity');
    }

    /** 分页大小 20 */
    private static $PageSize = 100;
    /** 记录状态  */
    private static $StateOn = 1;
    private static $StateDelete = 0;
    
    private static $FaceTable = 'activity_info_file';
    private static $FileTable = 'activity_file';
    
    /** 活动状态  草稿 */
    private static $ActivityDraft = 0;
    /** 活动状态  进行中 */
    private static $ActivityIn = 1;
    /** 活动状态  终止状态 */
    private static $ActivityStop = 2;
    
    private static $RemindType = array(
    		1 => 0,			//开始时提醒
    		2 =>600,		//提前10分钟提醒
    		3 =>1800,		//提前30分钟提醒
    		4 =>3600,		//提前1小时提醒
    		5 =>86400,		//提前一天提醒
    );
    
    /**
     * ajax行为统一调用方法
     *
     * @access public
     * @return void
     */
    public function index() {
        $ajax_act = get_var_value('ajax_act', FALSE);
        switch($ajax_act) {
            case 'save_activity':{
                //活动创建和修改
                $this -> save();
                BREAK;
            }
        	case 'activity_list':{
                //获取活动列表（分页）
                $this -> get_activity_list();
                BREAK;
            }
        	case 'atlas_list':{
                //获取图册列表
                $this -> get_atlas_list();
                BREAK;
            }
      		case 'image_list':{
                //获取指定相册的图片列表（分页）
                $this -> get_atlas_image();
                BREAK;
            }
        	case 'activity_detail':{
                //获取活动详情
                $this -> get_activity_detail();
                BREAK;
            }
        	case 'activity_state':{
        		//修改活动状态（发布/终止/删除）
        		$this -> activity_state();
        		BREAK;
        	}
        	case 'get_some':{
                //获取活动相关评论，报名人，上传图片
                $this -> get_some();
                BREAK;
            }
        	case 'enroll':{
                //活动报名
                $this -> enroll_activity();
                BREAK;
            }
        	case 'cancel_enroll':{
                //取消报名
                $this -> cancel_enroll();
                BREAK;
            }
        	case 'enroll_list':{
                //获取报名详情列表（分页）
                $this -> enroll_list();
                BREAK;
            }
        	case 'add_comment':{
                //发表评论
                $this -> add_comment();
                BREAK;
            }
        	case 'comment_list':{
                //获取评论详情列表(分页)
                $this -> comment_list();
                BREAK;
            }
        	case 'comment_detail':{
                //获取指定评论的详情列表(分页)
                $this -> comment_detail_list();
                BREAK;
            }
        	case 'upload_image':{
                //上传图片
                $this -> upload_image();
                BREAK;
            }
        	case 'approval_image':{
                //审核图片
                $this -> approval_image();
                BREAK;
            }
        	case 'delete_image':{
                //删除图片
                $this -> delete_image();
                BREAK;
            }
     	   case 'sign':{
                //签到
                $this -> sign();
                BREAK;
            }
        	case 'follow':{
                //点赞（暂无取消点赞功能）
                $this -> activity_follow();
                BREAK;
            }
        	case 'get_enroll_custom':{
                //获得报名信息自定义字段
                $this -> get_enroll_custom();
                BREAK;
            }
        	case 'transfer': {
        		//GPS转地图坐标
        		$this -> _transfer();
        		BREAK;
        	}
        	case 'activity_see':{
        		$this -> activity_see();
        		BREAK;
        	}
        	case 'get_sign_list':{
        		$this -> get_sign_list();
        		BREAK;
        	}
            default : {
                //非法的请求
                cls_resp::echo_resp(cls_resp::$ForbidReq);
                BREAK;
            }
        }
    }
    
    /**
     * 获取签到列表
     * @return 
     */
    private function get_sign_list(){
    	try{
	    	$data = parent::get_post_data(array('act_id', 'type'));
	    	$id = (int)$data['act_id'];
	    	$type = (int)$data['type'];			//1签到，0未签到
	    	$page = !empty($data['page']) ? (int)$data['page'] : 1;
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];

	    	if(empty($id)){
	    		throw new SCException('请求错误！');
	    	}

	    	if($type > 1 || $type < 0){
	    		throw new SCException('请求错误！');
	    	}

	    	$fields = ' public_id, lat, lng ';
	    	$act = g('activity_info') -> get_by_id($com_id, $id, $fields, false);

	    	if(empty($act)){
	    		throw new SCException('活动不存在！');
	    	}

			if($user_id != $act['public_id']){
	    		throw new SCException('没有权限！');
	    	}
	    	
	     	if($type == 1){
	     		$fields = 'e.sign_time, e.family_num, e.lng, e.lat, u.pic_url,u.name ';
	     	}else{
	     		$fields = 'e.family_num, u.pic_url,u.name ';
	     	}

	     	$act_lng = $act['lng'];
	     	$act_lat = $act['lat'];
	    	$ret = g('activity_info') -> get_sign_data($id, $type, $fields, $page, self::$PageSize, TRUE);
	    	if(!empty($ret['data']) && $type == 1){
	    		foreach ($ret['data'] as &$data) {
	    			$data['sign_time'] = date('Y-m-d H:i', $data['sign_time']);
	    			$data['distance'] = g('bdmap') -> get_distance($act_lng, $act_lat, $data['lng'], $data['lat']);
	    		}
	    	}

	    	cls_resp::echo_ok(cls_resp::$OK, 'info', $ret);

    	}catch(SCException $e){
    		cls_resp::echo_exp($e);
    	}
    }


    /**
	 * 查阅活动
     **/
    private function activity_see(){
    	try{
	    	$data = parent::get_post_data(array('act_id'));
	    	if(empty($data['act_id'])){
	    		throw new SCException('请求错误');
	    	}

	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$ret = g('activity_info') -> activity_see($user_id,$act_id);
	    	if(!$ret){
	    		throw new SCException('请求失败');
	    	}
    	}catch(SCException $e){
    		cls_resp::echo_exp($e);
    	}

    }

    /**
     * 活动的创建与更新（id为空为创建）
     */
    private function save(){
    	try{
//	    	$data = array(
//				'id' => 24,
//				'title' => '标题4',
//				'desc' => '描述4',
//				'pic_url' => '头像4',
//	    		'face_pic' => array(
//	    						'file_key' => 'sad4sa5d45sa45d4as4d5sa45d4as5',
//	    						'file_url' => 'sad4sa5d45sa45d4as4d5sa45d4as5',
//	    						'file_name' =>'我的封面2',
//	    						'ext' => 'jpg'
//	    					),
//				'enroll_time' => '1427427369',
//				'start_time' => '1427427370',
//				'end_time' => '1427427371',
//				'place' => '地球4',
//				'location' => array(
//								'lng' => '113.364269',
//								'lat' => '23.133866'		
//	    					),
//				'dept' => array("1"),
//				'user' => array("80144"),
//				'is_all' =>0,			//是否是邀请所有人
//				'remind_type' => 2,
//				'limited' => 10,
//				'is_enroll_msg' => 0	//报名是否需要填写表单 1：是 0否
//				'enroll_custom' => array('手机','姓名'),		//报名字段
//				'is_sign' => 1,			//是否需要签到 0不签到，1要签到
//				"sign_remind_type" => 1, //  1.开始时，2提前10分钟，3.提前30分钟，4.提前1个小时，5.提前一天
//				'sign_place' => '广州三尺KTV',
//	    		'state' => 0	//0：草稿，1：发布
//				'remind' => 0 	// 1 ：创建并通知    0：不通知
//				'can_family' => 1	   		是否可携带家属
//    			'family_limited' => 3		家属人数上限
//			);
			$trans =FALSE;
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		    $priviage = g('activity_info') -> check_prviage($com_id,$user_id);
			if(!$priviage){
				throw new SCException('您没有权限发起活动，请联系管理员配置权限！');
			}
	    	$data = parent::get_post_data(array('title','enroll_time','start_time','end_time','place','state','location','limited'));
	    	

    		if(empty($data['title'])){
	    		throw new SCException('标题不能为空！');
	    	}
	    	$data['title'] = filter_string($data['title']);
	    	$data['desc'] = filter_string($data['desc']);
	    	$data['enroll_time'] = strtotime($data['enroll_time']);
	    	$data['start_time'] = strtotime($data['start_time']);
	    	$data['end_time'] = strtotime($data['end_time']);
	    	
    		if(empty($data['enroll_time'])){
	    		throw new SCException('报名时间错误！');
	    	}
    		if(empty($data['start_time'])){
	    		throw new SCException('开始时间错误！');
	    	}
    		if(empty($data['end_time'])){
	    		throw new SCException('结束时间错误！');
	    	}
    		if( $data['start_time'] <= $data['enroll_time']){
	    		throw new SCException('报名时间要小于开始时间！');
	    	}
    		if( $data['start_time'] >= $data['end_time']){
	    		throw new SCException('结束时间要大于开始时间！');
	    	}
	    	
    		if(empty($data['place'])){
	    		throw new SCException('活动地点不能为空！');
	    	}
    		if(empty($data['state'])){
    			$data['state'] = 0;
	    	}
	    	if(!isset($data['location']['lng']) || empty($data['location']['lng']) || !isset($data['location']['lat']) || empty($data['location']['lat'])){
	    		throw new SCException('无法获取地点信息');
	    	}
	    	
	    	!isset($data['desc']) && $data['desc'] = "";
	    	!isset($data['can_family']) && $data['can_family'] = 0;
	    	!isset($data['family_limited']) && $data['family_limited'] = 0;
	    	!isset($data['limited']) && $data['limited'] = 0;
	    	!isset($data['is_sign']) && $data['is_sign'] = 0;
	    	!isset($data['sign_remind_type']) && $data['sign_remind_type'] = 1;
	    	!isset($data['sign_place']) && $data['sign_place'] = "";
	    	!isset($data['remind_type']) && $data['remind_type'] = 0;
	    	!isset($data['remind']) && $data['remind'] = 0;
	    	
	    	
	    	if($data['limited'] <=0){
	    		throw new SCException('参与人数限制错误');
	    	}
	    	if($data['can_family']==1){
	    		if($data['family_limited'] <=0){
		    		throw new SCException('携带家属数量上限错误');
		    	}
	    	}
	    	$data['remind_type'] = (int)$data['remind_type'];
	    	if($data['remind_type'] <0 || $data['remind_type']>5){
	    		throw new SCException('提醒类型选择错误');
	    	}
    		if($data['state'] <0 || $data['state']>1){
	    		throw new SCException('保存类型错误');
	    	}
	    	$data['lng'] = $data['location']['lng'];
	    	$data['lat'] = $data['location']['lat'];
	    	$data['user_name'] = $_SESSION[SESSION_VISIT_USER_NAME];
	    	if(isset($data['face_pic']['file_url'])){
	    		$data['pic_url'] = $data['face_pic']['file_url'];
	    	}else{
	    		$data['pic_url'] = '';
	    	}
	    	//获取提醒时间
	    	$data['remind_time'] = empty($data['remind_type']) ? $data['start_time'] : ( $data['start_time'] - self::$RemindType[$data['remind_type']] );
	    	$data['sign_time'] = empty($data['sign_remind_type']) ? $data['start_time'] : ( $data['start_time'] - self::$RemindType[$data['sign_remind_type']] );
	    	$data['is_enroll_msg'] = empty($data['is_enroll_msg']) ? 0 : 1;
	    	if($data['is_enroll_msg'] == 1){
	    		if(empty($data['enroll_custom']) || !is_array($data['enroll_custom'])){
		    		throw new SCException('报名字段不能为空！');
	    		}
	    	}else{
		    	$data['enroll_custom'] = array();
	    	}  
    		$data['enroll_custom'] = json_encode($data['enroll_custom']);
	    	
	    	if(empty($data['user']) || !is_array($data['user'])){
	    		$data['user'] = array();
	    	}
    		if(empty($data['dept']) || !is_array($data['dept'])){
	    		$data['dept'] = array();
	    	}
	    	if(empty($data['dept']) && empty($data['user'])){
	    		throw new SCException('邀请人不能为空');
	    	}
	    	$data['user'] = json_encode($data['user']);
	    	$data['dept'] = json_encode($data['dept']);
	    	$ret = 0;
	    	g('db') -> begin_trans();
	    	$trans = TRUE;
	    	if(!isset($data['id']) || empty($data['id'])){
	    		$ret = g('activity_info') -> save_activity($com_id,$user_id,$data);
	    		if($ret){
	    			if(!empty($data['face_pic']) && is_array($data['face_pic'])){
		    			//保存封面图片
		    			$r = g('activity_info') -> insert_cover_img($com_id,$user_id,$data['user_name'],$ret,$data['face_pic']);
		    			if(!$r){
			    			throw new SCException('封面图片保存失败');
		    			}else{
		    				//加入容量文件列表
		    				try{
								g('capacity') -> add_list_by_cache($com_id, $data['face_pic']['file_key'], $this -> app_name, self::$FaceTable, $r);
		    				}catch(SCException $e){
		    					log_write('上传活动封面计入容量文件列表失败');
		    				}
		    			}
	    			}
	    			$atlas_id = g('activity_atlas') -> save_atlas($com_id,$user_id,$ret,$data['title'],0,1);
	    			if(empty($atlas_id)){
	    				throw new SCException('创建失败！');
	    			}
	    			$folder = g('activity_atlas') -> save_folder($atlas_id,1,$user_id,$data['user_name']);
	    			if(empty($folder)){
	    				throw new SCException('创建失败！');
	    			}
	    			if($data['remind'] ==1){
		    			$this ->  remind($data['title'],$ret,json_decode($data['user'],true),json_decode($data['dept'],TRUE));
	    			}
	    		}
	    	}else{
	    		$ret = g('activity_info') -> edit_activity($com_id,$user_id,$data['id'],$data);
	    		if($ret){
	    			$r_insert = true;
	    			//删除封面图片
	    			$r_delete = g('activity_info') -> delete_cover_img($com_id,$data['id']);
		    		if(!empty($data['face_pic']) && is_array($data['face_pic'])){
		    			//保存新的封面图片
		    			$r_insert = g('activity_info') -> insert_cover_img($com_id,$user_id,$data['user_name'],$data['id'],$data['face_pic']);
		    		}
		    		
		    		if(!$r_insert || !$r_delete){
			    		throw new SCException('封面图片更新失败');
		    		}else{
		    			try{
							g('capacity') -> add_list_by_cache($com_id, $data['face_pic']['file_key'], $this -> app_name, self::$FaceTable, $r_insert);
	    				}catch(SCException $e){
	    					log_write('上传活动封面计入容量文件列表失败');
	    				}
		    		}
	    		}
	    	}
	    	if(!$ret){
	    		throw new SCException('操作失败'); 
	    	}
	    	g('db') -> commit();
		    cls_resp::echo_ok(cls_resp::$OK,'info',array('id' => $ret));
    	}catch (SCException $e){
    		if($trans){
	    		g('db') -> rollback();
    		}
    		cls_resp::echo_exp($e);
    	}
    	
    }
    
    
    /**
     * 获取活动列表 
     * 	type, 1:最新活动(当前自己可报名的活动)，2：所有活动，3：我参与的，4：我发布的，5,：草稿
     * 	is_finish:（当查询 当type=2|3时 ，用来区分  进行中[0] 和  已结束[1]）
     * 	search_key 关键字
     * 	desc 是否按照创建顺序倒序  1：倒序，0：顺序
     * 	page 页码
     */
    private function get_activity_list(){
    	try{
	    	$data = parent::get_post_data(array('type'));
	    	$type = (int)$data['type'];
	    	$is_finish = (!isset($data['is_finish'])|| empty($data['is_finish'])) ? 0 : (int)$data['is_finish'];
	    	$search_key = (!isset($data['search_key'])) ? "" : $data['search_key'];
	    	$desc = (!isset($data['desc'])|| empty($data['desc'])) ? 1 : (int)$data['desc'];
	    	
	    	$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
	    	$page_size = self::$PageSize;
	    	if(empty($type) || $type<1 || $type>5 ){
	    		throw new SCException('请求错误！');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$order = ' ORDER BY a.is_stick desc, a.public_time DESC ';
	    	if($desc != 1){
	    		$order = ' ORDER BY a.is_stick desc, a.public_time ASC ';	
	    	}
	    	$fields = 'a.*,u.name,u.pic_url user_pic';
	    	$now = time();
	    	$cond = array(
	    		'a.com_id=' => $com_id,
	    		'a.info_state=' => self::$StateOn,
	    	);
	    	if($search_key!==""){
	    		$cond['__OR1'] = array(
	    			'a.title like ' => '%'.$search_key.'%',
	    			'a.activity_describe like ' => '%'.$search_key.'%',
	    		);
	    	}
	    	$table = 'activity_info a LEFT JOIN sc_user u ON a.public_id = u.id';
	    	switch($type){
	    		case 1:{
		    			$user = g('user') -> get_by_id($user_id,'dept_tree');
		    			$dept_tree = json_decode($user['dept_tree']);
		    			$dept = array();
		    			foreach ($dept_tree as $dt){
		    				foreach ($dt as $d){
			    				array_push($dept,$d);
		    				}
		    				unset($d);
		    			}
		    			unset($dt);
		    			$dept = array_unique($dept);
		    			$cond['__OR'] = array();
		    			foreach ($dept as $key => $d){
		    				$cond['__OR']['__'.$key.'__a.scope_dept like '] = '%"'.$d.'"%';
		    			}
		    			unset($d);
		    			$cond['__OR']['a.scope_user like '] = '%"'.$user_id.'"%';
		    			$cond['a.enroll_time >'] = $now;
		    			$cond['a.state='] = self::$ActivityIn;
		    			break;
	    		}
	    		case 2:{
		    			if($is_finish == 0){
		    				$cond['a.end_time>'] = $now;
			    			$cond['a.state='] = self::$ActivityIn;
		    			}else{
		    				$cond['__OR'] = array();
		    				$cond['__OR'][0]= array(
		    					'a.end_time<=' => $now,
		    					'a.state=' => self::$ActivityIn
		    				);
			    			$cond['__OR']['a.state='] = self::$ActivityStop;
		    			}
		    			break;
	    		}
	    		case 3:{
		    			$fields = 'a.*,u.pic_url user_pic,u.name';
		    			$table = 'activity_enroll e LEFT JOIN activity_info a ON e.activity_id = a.id LEFT JOIN sc_user u ON a.public_id = u.id';
		    			$cond['e.enroll_id='] = $user_id;
		    			$cond['e.info_state='] = 1;
		    			if($is_finish == 0){
		    				$cond['a.end_time>'] = $now;
			    			$cond['a.state='] = self::$ActivityIn;
		    			}else{
		    				$cond['__OR'] = array();
		    				$cond['__OR'][0]= array(
		    					'a.end_time<=' => $now,
		    					'a.state=' => self::$ActivityIn
		    				);
			    			$cond['__OR']['a.state='] = self::$ActivityStop;
		    			}
		    			break;
	    		}
	    		case 4:{
		    			$cond['a.public_id='] = $user_id;
		    			$cond['a.state!='] = self::$ActivityDraft;
		    			break;
	    		}
	    		case 5:{
		    			$cond['a.public_id='] = $user_id;
		    			$cond['a.state='] = self::$ActivityDraft;
		    			break;
		    			break;
	    		}default : {
	                //非法的请求
	                cls_resp::echo_resp(cls_resp::$ForbidReq);
	                BREAK;
	            }
	    	}
	    	$ret = g('activity_info') -> get_activity_list($table,$cond,$fields,$page,$page_size,$order);
	    	empty($ret) && $ret = array();
	    	$return = array();
	    	$now = time();
	    	foreach($ret as $r){
	    		if($now < $r['enroll_time']){
	    			$activity_state = '报名中';
	    		}else if($now < $r['start_time']){
	    			$activity_state = '即将开始'; 
	    		}else if($now > $r['end_time']){
	    			$activity_state = '已结束';
	    		}else{
	    			$activity_state = '进行中';
	    		}
	    		if($r['state']==0){
	    			$state = '草稿';
	    		}else if($r['state'] == 1){
	    			$state = '已发布';
	    		}else{
	    			$state = '已终止';
	    		}
	    		log_write();
	    		if($r['is_admin'] == 1){
	    			$r['name'] = '管理员';
	    			$r['public_pic'] = '';
	    			$r['user_pic'] = '';
	    		}
	    		
	    		$return[] = array(
	    			'act_id' => $r['id'],
	    			'title' => $r['title'],
	    			'desc' => $r['activity_describe'],
	    			'activity_pic' => $r['pic_url'],
	    			'public_name' => $r['name'],
	    			'public_pic' => $r['user_pic'],
	    			'state' => $state,
	    			'activity_state' => $activity_state,
	    			'public_time' => date('Y-m-d H:i',$r['public_time']), 
	    		);
	    	}
	    	unset($r);
	    	cls_resp::echo_ok(cls_resp::$OK,'info',$return);
    	}catch(SCException $e){
    		cls_resp::echo_exp($e);
    	}
    }
    

    /**
     * 增加报名
     */
    private function add_enroll(){
    
    }
    
    /**
     * 取消报名接口
     * act_id 活动ID
     * @throws SCException
     */
    private function cancel_enroll(){
    	try{
    		$trans = false;
    		$data = parent::get_post_data(array('act_id'));
    		$act_id = (int)$data['act_id'];
    		$cancel_family = $data['cancel_family'];
    		if(empty($act_id)){
    			throw new SCException('请求错误');
    		}
    		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
    		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
    		
    		$act = g('activity_info') -> check_activity($com_id,$act_id,'id,enroll_num');
    		
    		$enroll = g('activity_info') ->get_enroll_data($com_id,$user_id,$act_id);
    		if(empty($enroll)){
    			throw new SCException('未报名，无法取消');
    		}
    		if($enroll['sign_state']==1){
    			throw new SCException('已经签到，无法取消报名');
    		}
    		g('db') -> begin_trans();
    		$trans = TRUE;
    		
    		$count = (int)$enroll['family_num'];
    		//取消报名
    		$ret1 = g('activity_info') -> delete_enroll($com_id,$act_id,$user_id);
    		
    		//更新活动报名数
    		$ret2 = g('activity_info') -> update_num($com_id,$act_id,'enroll_num',0,$count);
    		
    		if(!$ret1 || !$ret2){
    			throw new SCException('取消报名失败');
    		}
    		g('db') -> commit();
    		cls_resp::echo_ok();
    	}catch (SCException $e){
    		if($trans){
    			g('db') -> rollback();
    		}
    		cls_resp::echo_exp($e);
    	}
    }
    
    
    /**
     * 报名活动
     * id 活动ID
     * custom_msg 报名信息 array(
     * 							array('name' => '姓名','val'=>"XXXX")
     * 							array('name' => '手机','val'=>"15915882145")	
     * 						)
     * family_msg 家属信息 array(											//custom格式同custom_msg
     * 							array('name'=>'家属名','custom' => array(array('name'=>'姓名','val'=>'XXX'))),
     * 						)
     */
    private function enroll_activity(){
    	try{
    		$trans = false;
	    	$data = parent::get_post_data(array('id'));
	    	$id = (int)$data['id'];
			$custom_msg = isset($data['custom_msg']) ? $data['custom_msg'] : '';
			$family_msg = isset($data['family_msg']) ? $data['family_msg'] : '';
	    	
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$now = time();
	    	if(empty($id)){
	    		throw new SCException('请求错误！');
	    	}
	    	//开启事务
	    	g('db')->begin_trans();
	    	$trans = TRUE;
	    	$act = g('activity_info') -> check_activity($com_id,$id,'id,enroll_time,state,limited,enroll_num,is_enroll_msg,enroll_custom,scope_user,scope_dept,can_family,family_limited');
	    	if(!$act){
	    		throw new SCException('活动不存在！');
	    	}
	    	$custom = array();
    		$act_custom = json_decode($act['enroll_custom'],TRUE);
	    	if($act['is_enroll_msg'] ==1){
	    		if( empty($custom_msg) || !is_array($custom_msg) ){
		    		throw new SCException('请填写报名信息');
	    		}else{
	    			$custom = $this -> tally_with($act_custom, $custom_msg);
	    			if(!$custom){
		    			throw new SCException('报名信息不完整！');
	    			}
	    			
	    		}
	    	}
	    	$enroll_family = array();
	    	if($act['can_family'] == 1){
	    		if(!empty($family_msg) && is_array($family_msg)){
	    			if(count($family_msg) > $act['family_limited']){
	    				throw new SCException('家属人数超过限制！');
	    			}else{
	    				foreach($family_msg as $f){
	    					$family_custom = array();
		    				if($act['is_enroll_msg'] ==1){
					    		if( empty($f['custom']) || !is_array($f['custom']) ){
						    		throw new SCException('请填写报名信息');
					    		}else{
					    			$family_custom = $this -> tally_with($act_custom,$f['custom']);
					    			if(!$family_custom){
						    			throw new SCException('报名信息不完整！');
					    			}
					    			
					    		}
					    	}
					    	if(empty($f['name'])){
					    		throw new SCException('家属名不能为空！');
					    	}
	    					array_push($enroll_family,array('name'=>$f['name'],'custom'=>$family_custom,'enroll_time'=>$now,'sign_state'=>0,'sign_time'=>0,));
	    				}
	    				unset($f);
	    			}
	    		}
	    	}
	    	
	    	$custom = json_encode($custom);
	    	$count = count($enroll_family)+1;
	    	$enroll_family = json_encode($enroll_family);
	    	$now = time();
	    	$scope_user = json_decode($act['scope_user'],true);
	    	$scope_dept = json_decode($act['scope_dept'],true);
	    	$user = g('user') -> get_by_id($user_id,'dept_tree');
    		$dept_tree = json_decode($user['dept_tree']);
    		$dept = array();
    		foreach ($dept_tree as $dt){
    			foreach ($dt as $d){
	    			array_push($dept,$d);
    			}
    			unset($d);
    		}
    		unset($dt);
    		$dept = array_unique($dept);
    		//获取交集，判断是否有权限
	    	if(!in_array($user_id,$scope_user) && !array_intersect($dept,$scope_dept)){
	    		throw new SCException('没有权限报名');
	    	}
	    	
	    	
	    	if($act['state'] == self::$ActivityStop){
	    		throw new SCException('该活动已终止，不能报名！');
	    	}
    		if($act['state'] == self::$ActivityDraft){
	    		throw new SCException('该活动未发布，不能报名！');
	    	}
	    	if($now > $act['enroll_time']){
	    		throw new SCException('报名时间已结束！');
	    	}
	    	if($act['limited'] <  ($act['enroll_num'] + $count)){
	    		throw new SCException('已经达到报名人数限制！');
	    	}
	    	
	    	$ret = g('activity_info') -> get_enroll_data($com_id,$user_id,$id);
	    	$result = false;
	    	$type = 1;
	    	$num = 1;
	    	if(!empty($ret)){
	    		if($count < $ret['family_num']){
		    		$num = ((int)$ret['family_num']) - $count;
		    		$type = 0;
	    		}else{
	    			$num = $count - ((int)$ret['family_num']);
	    			$type = 1;
	    		}
	    		$result = g('activity_info') -> update_enroll($ret['id'],$com_id,$id,$user_id,$custom,$enroll_family,$count,$now);
	    	}else{
	    		$num = $count;
	    		$type = 1;
		    	$result = g('activity_info') -> enroll_activity($com_id,$id,$user_id,$custom,$enroll_family,$count,$now);
	    	}
	    	if(!$result){
	    		throw new SCException('报名失败');
	    	}
	    	g('activity_info') -> update_num($com_id,$id,'enroll_num',$type,$num);
	    	
	    	g('db') -> commit();
	    	cls_resp::echo_ok();
    	}catch (SCException $e){
    		if($trans){
	    		g('db') -> rollback();
    		}
    		cls_resp::echo_exp($e);
    	}
    	
    }
    
    /**
     * 获取活动相关的评论，图片，报名人
     * @throws SCException
     */
    private function get_some(){
	    try{
	    	$data = parent::get_post_data(array('id'));
	    	$id = (int)$data['id'];
	    	if(empty($id)){
	    		throw new SCException('请求错误');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	
	    	$fields = 'act.*,u.name,u.pic_url';
	    	$act = g('activity_info') -> get_by_id($com_id,$id,$fields,true);
	    	if(!$act){
	    		throw new SCException('活动不存在');
	    	}
	    	$comment = g('activity_info') -> get_comment_list($com_id,$id,' c.*,u.name,u.pic_url,count(c1.id) count',1,3,' order by c.comment_time desc ');
	    	$enroll = g('activity_info') -> get_enroll_data($com_id,0,$id,' u.name,u.pic_url ',1,5);
	    	$image = g('activity_atlas') -> get_file_by_act_id($com_id,$id,' f.file_url ',1,3);
	    	
	    	$cond = array(
	    		'pid=' => 0,
	    		'file_type=' => 1,
	    		'activity_id=' => $id,
	    		'info_state=' => self::$StateOn,
	    	);
	    	$table = ' activity_atlas ';
	    	$atlas = g('activity_atlas') -> get_atlas_list($table,$cond,'id');
	    	$info = array(
	    		'comment' => empty($comment) ? array() : $comment,
	    		'enroll' => empty($enroll) ? array() : $enroll,
	    		'image' => empty($image) ? array() : $image,
	    		'atlas_id' => $atlas[0]['id'],
	    	);
	    	
	    	cls_resp::echo_ok(cls_resp::$OK,'info',$info);
    	}catch (SCException $e){
    		cls_resp::echo_exp($e);
    	}
    }
    
    /**
     * 获得活动详情
     * id 指定活动ID
     * 
     * 注意：
     * act_state 标识活动进行状态  0：报名阶段，1：报名已结束，但活动未开始，2：活动进行中，3：活动结束
     * enroll_state 标识我是否已报名
     * follow_state 标识我是否已点赞
     */
    private function get_activity_detail(){
    	try{
	    	$data = parent::get_post_data(array('id'));
	    	$id = (int)$data['id'];
	    	if(empty($id)){
	    		throw new SCException('请求错误');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$fields = 'act.*,u.name,u.pic_url';
	    	$act = g('activity_info') -> get_by_id($com_id,$id,$fields,true);
	    	if(!$act){
	    		throw new SCException('活动不存在');
	    	}
	    	//若活动进行中，返回相关状态
	    	if($act['state'] == self::$ActivityIn){
		    	$now = time();
		    	if($now <= $act['enroll_time']){
		    		$act['act_state'] = 0;
		    	}else if($now < $act['start_time'] && $now > $act['enroll_time']){
		    		$act['act_state'] = 1;
		    	}else if($now >= $act['start_time'] && $now < $act['end_time']){
		    		$act['act_state'] = 2;
		    	}else{
		    		$act['act_state'] = 3;
		    	}
		    	$act['sign_state'] = 0;
		    	
		    	$enroll = g('activity_info') -> get_enroll_data($com_id,$user_id,$id);
		    	if(!empty($enroll)){
		    		$act['enroll_state'] = 1;
		    		$act['sign_state'] = $enroll['sign_state'];
		    	}else{
		    		$act['enroll_state'] = 0;
		    	}
		    	
	    		$follow = g('activity_info') -> get_activity_follow($com_id,$id,$user_id);
		    	if(!empty($follow)){
		    		$act['follow_state'] = 1;
		    	}else{
		    		$act['follow_state'] = 0;
		    	}
	    	}
	    	
	    	//格式化数据
	    	$act['scope_user'] = json_decode($act['scope_user'],true);
	    	$act['scope_dept'] = json_decode($act['scope_dept'],true);
	    	$act['enroll_time'] = date('Y-m-d H:i',$act['enroll_time']);
	    	$act['start_time'] = date('Y-m-d H:i',$act['start_time']);
	    	$act['end_time'] = date('Y-m-d H:i',$act['end_time']);
	    	$act['public_time'] = date('Y-m-d H:i',$act['public_time']);
	    	$act['enroll_custom'] = json_decode($act['enroll_custom'],TRUE);
	    	
	    	
	    	cls_resp::echo_ok(cls_resp::$OK,'info',$act);
    	}catch (SCException $e){
    		cls_resp::echo_exp($e);
    	}
    }
    
    /**
     * 获取报名列表
     * id 活动ID
     */
    private function enroll_list(){
    	try{
	    	$data = parent::get_post_data(array('id'));
	    	$id = (int)$data['id'];
	    	$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
	    	$page_size = 100;
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	if(empty($id)){
	    		throw new SCException('请求错误');
	    	}
    		$ret = g('activity_info') -> get_enroll_data($com_id,0,$id,' e.*,u.name,u.pic_url ',$page,$page_size);
    		cls_resp::echo_ok(cls_resp::$OK,'info',$ret);
    	}catch (SCException $e){
    		cls_resp::echo_exp($e);
    	}
    }

    /**
     * 评论功能
     * aid 活动ID
     * pid 回复评论ID，若直接回复活动则为0
     * comment 评论内容
     * 
     */
	private function add_comment(){
		try{
			$trans = false;
			$data = parent::get_post_data(array('aid','pid','comment'));
			$id = (int)$data['aid'];
			$comment = filter_string($data['comment']);
			$pid = (int)$data['pid'];
			
			if(empty($id)){
				throw new SCException('请求错误');	
			}
			if(!trim($comment)){
				throw new SCException('评论内容不能为空');
			}
			empty($pid) && $pid = 0; 
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			g('db') -> begin_trans();
			$trans = true;
			if($pid !=0 ){
				$p_comm = g('activity_info') -> get_comment($com_id,$pid,'comment_id');
				if(!$p_comm){
					throw new SCException('您回复的评论不存在!');
				}
			}
			
			$ret = g('activity_info') -> add_comment($com_id,$user_id,$id,$pid,$comment);
			if(!$ret){
				throw new SCException('评论失败!');
			}
			if($pid ==0){
				g('activity_info') -> update_num($com_id,$id,'comment_num');
			}
			//获取本人头像
			$user = g('user') -> get_by_id($user_id,'pic_url');
			
			$new_comment = array(
				'id' => $ret,
				'pid' => $pid,
				'pic_url' => $user['pic_url'],
				'comment' => $comment,
				'comment_time' => '刚刚',
			);
			//返回回复的信息
			g('db') -> commit();
			cls_resp::echo_ok(cls_resp::$OK,'comment',$new_comment);
		}catch (SCException $e){
			if($trans){
				g('db') -> rollback();
			}
			cls_resp::echo_exp($e);
		}
	}

	/**
	 * 获取评论详情列表
	 * act_id 活动ID
	 * page 页码
	 */
	private function comment_list(){
		try{
			$data = parent::get_post_data(array('act_id'));
			$id = (int)$data['act_id'];
			
			$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
		    $page_size = self::$PageSize;
			
			if(empty($id)){
				throw new SCException('请求错误');
			}
		    
		    $com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$activity = g('activity_info') -> get_by_id($com_id,$id,'id,title,comment_num');
			if(empty($activity)){
				throw new SCException('活动不存在');
			}
			$order = ' order by c.comment_time desc ';
		    $fields=' c.*,u.name,u.pic_url,count(c1.id) count ';
		    
		    $ret = g('activity_info') -> get_comment_list($com_id,$id,$fields,$page,$page_size,$order);
		    cls_resp::echo_ok(cls_resp::$OK,'info',array('data'=> $ret,'title'=>$activity['title'],'count'=> $activity['comment_num']));
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}

	/**
	 * 获取指定评论的详情列表
	 * act_id 活动ID
	 * page 页码
	 */
	private function comment_detail_list(){
		try{
			$data = parent::get_post_data(array('act_id','comment_id'));
			$id = (int)$data['act_id'];
			$comment_id = (int)$data['comment_id'];
			
			$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
		    $page_size = self::$PageSize;
			
			if(empty($id)){
				throw new SCException('请求错误');
			}
		    
		    $com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$activity = g('activity_info') -> get_by_id($com_id,$id,'id,title,comment_num');
			if(empty($activity)){
				throw new SCException('活动不存在');
			}
			$order = ' order by c.comment_time desc ';
		    $fields=' c.*,u.name,u.pic_url ';
		    
		    $ret = g('activity_info') -> get_comment_detail($com_id,$id,$comment_id,$fields,$page,$page_size,$order);
		    
		    cls_resp::echo_ok(cls_resp::$OK,'info',array('data'=> $ret['data'],'count'=> $ret['count']));
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 获取相册列表
	 *  type 1:所有相册，2：我发布的，3：我参与的
	 *  search_key 关键字
	 *  page 页码
	 */
	private function get_atlas_list(){
		try{
			$data = parent::get_post_data(array('type'));
			$type = (int)$data['type'];

			if(empty($type)){
				throw new SCException('请求错误');
			}
			
			$search_key = (!isset($data['search_key'])|| empty($data['search_key'])) ? "" : $data['search_key'];
	    	$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
	    	$page_size = self::$PageSize;
	    	$desc = (!isset($data['desc'])|| empty($data['desc'])) ? 1 : (int)$data['desc'];
	    	
			$order = ' ORDER BY i.is_stick desc, i.public_time DESC ';
	    	if($desc != 1){
	    		$order = ' ORDER BY i.is_stick desc, i.public_time ASC ';	
	    	}
	    	
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	
	    	$cond = array(
	    		'a.file_type=' => 1,	//文件夹类型
	    		'a.pid =' => 0,		//根目录
	    		'a.com_id=' => $com_id,
	    		'a.info_state=' =>self::$StateOn,
	    		'i.state =' => self::$ActivityIn,
	    	);
	    	if(!empty($search_key)){
	    		$cond['i.title like '] = '%'.$search_key.'%';
	    	}
	    	
	    	$table = 'activity_info i LEFT JOIN activity_atlas a ON a.activity_id = i.id ';
	    	$fields = 'DISTINCT a.*,i.pic_url,i.image_num,i.title,i.public_time';
	    	switch($type){
	    		case 1:{
	    			break;
	    		}
	    		case 2:{
	    			$cond['a.create_id='] = $user_id;
	    			break;
	    		}
	    		case 3:{
	    			$table = 'activity_file f LEFT JOIN activity_atlas a ON f.activity_id = a.activity_id LEFT JOIN activity_info i ON i.id = a.activity_id ';
	    			$cond['f.upload_id ='] = $user_id;
	    			break;
	    		}default : {
	                //非法的请求
	                cls_resp::echo_resp(cls_resp::$ForbidReq);
	                BREAK;
	            }
	    	}
	    	$ret = g('activity_atlas') -> get_atlas_list($table,$cond,$fields,$page,$page_size,$order);
	    	$return = array();
	    	foreach($ret as $r){
	    		$return[] = array(
	    			'id' => $r['id'],
	    			'act_id' => $r['activity_id'],
	    			'pic_url' => $r['pic_url'],
	    			'title' => $r['title'],
	    			'image_num' => $r['image_num'],
	    			'activity_pic' => $r['pic_url'],
	    			'public_time' => date('Y-m-d H:i',$r['public_time']), 
	    		);
	    	}
	    	unset($r);
	    	cls_resp::echo_ok(cls_resp::$OK,'info',$return);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}

	
	/**
	 * 获得相册内图片（分页）
	 * type 1：审核通过，0：未审核，2：我发布的
	 * atlas_id 相册ID
	 * in_time 是否按照时间进行图片归类
	 */
	private function get_atlas_image(){
		try{
			$data = parent::get_post_data(array('type','atlas_id'));
			$type = (int)$data['type'];
			$atlas_id = (int)$data['atlas_id'];
			$in_time = isset($data['in_time']) ? (int)$data['in_time'] : 0; 
			
			if( $type<0 || $type>2 ){
				throw new SCException('请求错误');
			}
	    	$page = (!isset($data['page'])|| empty($data['page'])) ? 1 : (int)$data['page'];
	    	$page_size = self::$PageSize;
	    	
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	
    		$atlas = g('activity_atlas')->get_atlas_by_id($com_id,$atlas_id);
    		if(empty($atlas)){
    			throw new SCException('相册不存在！');
    		}
	    	if($type == 2){	//判断当前用户是否为发布人
	    		if($atlas['file_type'] !=1 || $atlas['create_id'] != $user_id){
	    			throw new SCException('没有权限');
	    		}
	    	}else{
	    		$activity = g('activity_info') -> get_by_id($com_id,$atlas['activity_id'],'act.title,act.pic_url');
	    	}
	    	$order = ' order by f.approve_time desc,a.create_time desc';
	    	$ret = g('activity_atlas') -> get_file_by_folder($com_id,$user_id,$atlas_id,$type,'f.*,a.create_time',$page,$page_size,$order);
	    	empty($ret) && $ret = array();
	    	
	    	$return = array();
	    	foreach($ret as $r){
		    	$time = empty($r['approve_time']) ? $r['create_time'] : $r['approve_time'];
	    		$return[] = array(
	    			'image_id' => $r['id'],
	    			'url' => $r['file_url'],
	    			'key' => $r['file_key'],
	    			'ext' => $r['file_ext'],
	    			'time' => date('Y-m-d',$time), 
	    		);
	    	}
	    	unset($r);
	    	if(!empty($in_time)){
	    		$ret = array();
	    		foreach($return as $r){
	    			if(!isset($ret[$r['time']])){
	    				$ret[$r['time']] = array($r);
	    			}else{
	    				array_push($ret[$r['time']], $r);
	    			}
	    		}
	    		unset($r);
	    		$return = $ret;
	    	}
	    	
	    	cls_resp::echo_ok(cls_resp::$OK,'info',array('data' => $return,'atlas_pic'=> $activity['pic_url'],'title'=>$activity['title']));
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 审批图片通过
	 * image_id 被审批图片
	 */
	private function approval_image(){
		try{
			$trans = false;
			$data = parent::get_post_data(array('act_id','image_id'));
			$image_id = $data['image_id'];
			$act_id = (int)$data['act_id'];
			if(empty($image_id) || !is_array($image_id)){
				throw new SCException('请求错误');
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			
			//过滤空ID
			$image_id = array_filter($image_id);
			
			if(empty($image_id)){
				throw new SCException('找不到图片');
			}
			$images = g('activity_atlas') -> get_file_by_id($com_id,$act_id,$image_id);
			if(empty($images)){
				throw new SCException('图片不存在');
			}
			
			$image_arr = array();
			foreach ($images as $image){
				if($image['create_id'] == $user_id && $image['file_state'] == 0){
					array_push($image_arr,$image['id']);
				}
			}
			unset($image);
			
			if(empty($image_arr)){
				throw new SCException('没有权限审批');
			}
			
			g('db') -> begin_trans();
			$trans = true;
			$ret = g('activity_atlas') -> approval_file($image_arr,$user_id,$user_name);
			$num = count($image_arr);
			g('activity_info') -> update_num($com_id,$act_id,'image_num',1,$num);
			
			if(!$ret){
				throw new SCException('审核失败');
			}
			g('db') -> commit();
			cls_resp::echo_ok();
		}catch (SCException $e){
			if($trans){
				g('db')-> rollback();
			}
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除已审核的图片
	 * image_id 被删除图片ID
	 */
	public function delete_image(){
		try{
			$trans = false;
			$data = parent::get_post_data(array('act_id','image_id'));
			$image_id = $data['image_id'];
			$act_id = (int)$data['act_id'];
			if(empty($image_id) || !is_array($image_id)){
				throw new SCException('请求错误');
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			
			$image_id = array_filter($image_id);
			if(empty($image_id)){
				throw new SCException('找不到图片');
			}
			
			$images = g('activity_atlas') -> get_file_by_id($com_id,$act_id,$image_id);
			if(empty($images)){
				throw new SCException('图片不存在');
			}
			$image_arr = array();
			foreach ($images as $image){
				if($image['create_id'] == $user_id){
					array_push($image_arr, $image['id']);
				}
			}
			unset($image);
			if(empty($image_arr)){
				throw new SCException('没有权限');
			}
			
			g('db') -> begin_trans();
			$trans = true;
			$ret = g('activity_atlas') -> delete_file($image_arr,$user_id);
			$num = count($image_arr);
			g('activity_info') -> update_num($com_id,$act_id,'image_num',0,$num);
			if(!$ret){
				throw new SCException('删除失败');
			}
			g('db') -> commit();
			cls_resp::echo_ok();
		}catch (SCException $e){
			if($trans){
				g('db') -> rollback();
			}
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 上传图片
	 * file 图片信息  array('file_name'=>'','file_url' => '','file_key'=>'','file_ext' => '')
	 * atlas_id 图册ID
	 */
	private function upload_image(){
		try{
			$trans = FALSE;
			$data = parent::get_post_data(array('file','atlas_id'));
			$atlas_id = (int)$data['atlas_id'];
			$file = $data['file'];
			
			if(empty($atlas_id)){
				throw new SCException('请求错误');
			}
			if(empty($file) || !is_array($file)){
				throw new SCException('上传文件错误');
			}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			
			$atlas = g('activity_atlas') -> get_atlas_by_id($com_id,$atlas_id,'activity_id,file_type');
			if(empty($atlas)){
				throw new SCException('相册不存在！');
			}
			if($atlas['file_type'] != 1){
				throw new SCException('相册类型错误');
			}
			
			
			g('db') ->begin_trans();
			$trans = TRUE;
			
			$id = g('activity_atlas') -> save_atlas($com_id,$user_id,$atlas['activity_id'],$file['file_name'],$atlas_id,2);
			if(empty($id)){
				throw new SCException('上传错误');
			}
			$f_id = g('activity_atlas') -> save_file($atlas['activity_id'],$id,0,$file['file_url'],$file['file_name'],$file['file_key'],$file['file_ext'],$user_id,$user_name);
			if(empty($f_id)){
				throw new SCException('上传错误');
			}else{
				try{
					g('capacity') -> add_list_by_cache($com_id, $file['file_key'], $this -> app_name, self::$FileTable, $f_id);
    			}catch(SCException $e){
    				log_write('上传活动封面计入容量文件列表失败');
    			}
			}
			// $ret = g('activity_info')-> update_num($com_id,$atlas['activity_id'],'follow_num');
			// if(!$ret){
			// 	throw new SCException('上传错误');
			// }
			
			g('db') ->commit();
			cls_resp::echo_ok();
		}catch(SCException $e){
			if($trans){
				g('db') -> rollback();
			}
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 签到
	 * act_id 活动ID
	 * location 位置信息 ：  array('lng'=>'' , 'lat' =>"")
	 *  family_sign 签到家属 ： array('家属名1','家属名2');
	 */
	private function sign(){
		try{
			$data = parent::get_post_data(array('act_id','location'));
			$act_id = (int)$data['act_id'];
			$location = $data['location'];
			$family_sign = isset($data['family_sign']) ? $data['family_sign'] : array();
			if(empty($act_id)){
				throw new SCException('请求错误！');
			}
			if(empty($location) || !is_array($location)){
				throw new SCException('无法获取地理位置！');
			}
			
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$now = time();
			$activity = g('activity_info') -> get_by_id($com_id,$act_id,'is_sign,sign_time,end_time');
			if(empty($activity)){
				throw new SCException('活动不存在！');
			}
			if($activity['is_sign'] !=1){
				throw new SCException('该活动无需签到！');
			}
			if($activity['sign_time'] > $now){
				throw new SCException('签到时间还没到哦！');
			}
			if($activity['end_time'] < $now){
				throw new SCException('活动已经结束！');
			}
			$enroll = g('activity_info') -> get_enroll_data($com_id,$user_id,$act_id);
	    	if(empty($enroll)){
	    		throw new SCException('未报名，无法签到！');
	    	}else if($enroll['sign_state'] == 1){
	    		throw new SCException('已经签到，无法重复签到！');
	    	}
	    	
	    	$family = json_decode($enroll['family'], true);
	    	if(!empty($enroll['family'])){
	    		foreach ($family as &$f){
	    			if(in_array($f['name'], $family_sign)){
	    				$f['sign_state'] = 1;
	    				$f['sign_time'] = time();
	    			}
	    		}
	    		unset($f);
	    	}
	    	$family = json_encode($family);
			$ret = g('activity_info')-> enroll_sign($com_id,$act_id,$user_id,$location['lng'],$location['lat'],$family);
			if(!$ret){
				throw new SCException('签到失败');
			}
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 改变活动状态
	 * state  1:发布，2.终止，3.删除
	 * act_id 活动ID
	 */
	public function activity_state(){
		try{
			$data = parent::get_post_data(array('act_id','state'));
			$act_id = (int)$data['act_id'];
			$state = (int)$data['state'];
			if(empty($act_id)){
				throw new SCException('请求错误！');
			}
			if($state < 1 || $state > 3){
				throw new SCException('请求错误！');
			}
			
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			
			$act = g('activity_info')-> get_by_id($com_id,$act_id);
			if(empty($act)){
				throw new SCException('活动不存在！');
			}
			if($act['public_id'] != $user_id){
				throw new SCException('没有权限！');
			}
			if($state == 3){
				$ret = g('activity_info') -> delete_activity($com_id,$act_id);
			}else{
				$ret = g('activity_info') -> change_actvity_state($com_id,$act_id,$state);
			}
			if(!$ret){
				throw new SCException('操作失败！');
			}
			if($state == 1){
				$this ->  remind($act['title'],$act_id,json_decode($act['scope_user'],true),json_decode($act['scope_dept'],TRUE));
			}
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 活动点赞（暂无取消点赞功能）
	 * act_id 活动点赞
	 * 
	 */
	public function activity_follow(){
		try{
			$trans = FALSE;
			$data = parent::get_post_data(array('act_id'));
			$act_id = (int)$data['act_id'];
			
			
			if(empty($act_id)){
				throw new SCException('请求错误！');
			}
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			g('db') -> begin_trans();
			$trans = true;
			$ret = g('activity_info')->get_activity_follow($com_id,$act_id,$user_id);
			if(empty($ret)){
				$ret = g('activity_info') -> activity_follow($com_id,$act_id,$user_id);
				if(!$ret){
					throw new SCException('点赞失败！');
				}
				$ret1 = g('activity_info')-> update_num($com_id,$act_id,'follow_num');
				if(!$ret1){
					throw new SCException('点赞失败！');
				}
			}
			g('db') ->commit();
			cls_resp::echo_ok();
		}catch (SCException $e){
			if($trans){
				g('db') -> rollbak();
			}
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 获得报名自定义字段信息
	 */
	private function get_enroll_custom(){
		try{
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$ret = g('activity_info') -> get_enroll_custom($com_id);
			$custom = array();
			if(!empty($ret)){
				foreach ($ret as $r) {
					array_push($custom, $r['custom_name']);
				}
				unset($r);
			}
			cls_resp::echo_ok(cls_resp::$OK,'list',$custom);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 提醒
	 * @param unknown_type $user
	 * @param unknown_type $dept
	 */
	private function remind($title,$act_id,$user=array(),$dept=array()){
		try{
			log_write('开始提醒活动'.json_encode($user).'-'.json_encode($dept));
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			$wx_title = '活动发布通知';
			$wx_desc = $user_name.'发布了活动【'.$title.'】并邀请您参加，赶快报名吧！';
			$wx_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=detail&act_id='.$act_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
			$pc_title = '组织活动：'.$user_name.'发布了活动【'.$title.'】并邀请您参加，赶快报名吧！';		
			$pc_url = SYSTEM_HTTP_PC_DOMAIN;
			$data = array(
            	'title' => $pc_title,
            	'url' => $pc_url,
                'app_name' => 'activity',
            );
            $wx_dept = array();
            if(!empty($dept)){
            	$depts = g('dept') -> get_dept_list($dept);
            	if(!empty($depts)){
            		foreach ($depts as $d) {
            			array_push($wx_dept,$d['wx_id']);
            		}
            	}
            }
            if(!empty($user)){ 
	            $users = g('user') -> get_by_ids($user,'id,acct');
	            $accts = array();
	            foreach($users as $s){
	            	array_push($accts,$s['acct']);
	            }unset($s);
            }
            if(empty($accts) && empty($wx_dept)){
            	log_write('接收人为空，无法推送消息');
            	return false;
            }

            $activity = g('activity_info') -> get_by_id($com_id, $act_id, 'pic_url');
            $pic_url = $activity['pic_url'];
            $wx_pic_url = $pic_url ? MEDIA_URL_PREFFIX.$pic_url : NULL;

            parent::send_single_news($accts, $wx_title, $wx_desc, $wx_url, $wx_pic_url,$wx_dept);
            
            //PC端提醒先屏蔽
//          g('messager') -> publish_by_ids('pc', array(),$user,$data);
            
            log_write('提醒活动成功');
		}catch(SCException $e){
			log_write('发送通知失败');
		}
	}
	
	/** GPS坐标转高德地图坐标 */
	private function _transfer() {
		try {
			$need = array('lng', 'lat');
			$data = $this -> get_post_data($need);
			
			$point = g('bdmap') -> trans2gd($data['lng'], $data['lat']);
			cls_resp::echo_ok(cls_resp::$OK, 'info', $point);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 判断报名信息是否完整
	 * @param unknown_type $act_custom
	 * @param unknown_type $custom_msg
	 * @throws SCException
	 */
	private function tally_with($act_custom,$custom_msg){
		$custom_name = array();
    	$custom = array();
    	foreach($custom_msg as $c){
    		if(!empty($c['val'])){
    			$c['val'] = filter_string($c['val']);
    			array_push($custom,array('name'=>$c['name'],'val'=>$c['val']));
	    		array_push($custom_name, $c['name']);
    		}
    	}
    	unset($c);
    	//获取差集，判定信息是否完整
    	$diff = array_diff($act_custom,$custom_name);
    	if(!empty($diff)){
    		return false;
    	}
    	return $custom;
	}
	
}

/* End of this file */