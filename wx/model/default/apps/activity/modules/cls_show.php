<?php
/**
 * 组织活动
 * @author yangpz
 * @date 2015-10-17
 *
 */
class cls_show extends abs_app_base {
	
	/** 最新活动	 */
	private static $NewActivity = 1;
	/** 所有活动	 */
	private static $AllActivity = 2;
	/** 我参与的	 */
	private static $ActionActivity = 3;
	/** 我发布的活动	 */
	private static $MyActivity = 4;
	/** 草稿活动 */
	private static $DeaftActivity = 5;
	
	/** 所有相册	 */
	private static $AllAtlas = 1;
	/** 我发布的活动	 */
	private static $MyAtlas = 2;
	/** 我参与的	 */
	private static $ActionAtlas = 3;
	
	/** 我发布的详情	 */
	private static $AtlasDetail = 2;
	/** 未审核的相册详情	 */
	private static $NoApprovalAtlas = 0;
	/** 审核通过的相册详情 */
	private static $ApprovalAtlas = 1;
	
	
	
	/** 活动状态  草稿 */
    private static $ActivityDraft = 0;
    /** 活动状态  进行中 */
    private static $ActivityIn = 1;
    /** 活动状态  终止状态 */
    private static $ActivityStop = 2;
	
    
    
	public function __construct() {
		parent::__construct('activity');
	}

	/**签到列表页面	 */
	public function sign_list(){
		global $app;
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$id = (int)get_var_get('id');
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$fields = 'public_id, enroll_num, sign_time';
		$act = g('activity_info') -> get_by_id($com_id, $id, $fields, false);
		if(empty($act)){
			cls_resp::show_err_page('活动不存在！');
		}
		if($user_id != $act['public_id']){
			cls_resp::show_err_page('没有权限！');	
		}
		if(time() < $act['sign_time']){
			cls_resp::show_warn_page('活动的签到时间还没到<br><span style="color: #9b9b9b; font-size: 12px;">由于活动签到时间未到，暂时无法查看签到信息</span>');
		}

		//报名人数
		$enroll_num = $act['enroll_num'];
		//已签到人数
		$sign_num = 0;
		if($enroll_num != 0){
			$ret = g('activity_info') -> get_sign_data($id, 1, 'sum(e.family_num) count');
			if(!empty($ret)){
				if(!is_null($ret[0]['count'])){
					$sign_num = $ret[0]['count'];
				}
			}
		}

		g('smarty') -> assign('act_id', $id);
		g('smarty') -> assign('enroll_num', $enroll_num);
		g('smarty') -> assign('sign_num', $sign_num);
		g('smarty') -> assign('unsign_num', $enroll_num - $sign_num);
		g('smarty') -> assign('list_url', $this -> app_domain . '&m=ajax');
		g('smarty') -> show($this -> temp_path.'activity_sign_list.html');
	}
	
	/** 所有活动 */
	public function all() {
		global $app;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=detail';
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=all';
		$is_finish = get_var_get('is_finish');
		if(empty($is_finish) ||$is_finish<0 || $is_finish>1){
			$is_finish = 0;
		}
		g('smarty') -> assign('title', '所有活动');
		g('smarty') -> assign('is_finish', $is_finish);
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$AllActivity);
		g('smarty') -> show($this -> temp_path.'activity_list_all.html');
	}
	
	/** 我参与的活动 */
	public function action() {
		global $app;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=detail';
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=action';
		$is_finish = get_var_get('is_finish');
		if(empty($is_finish) ||$is_finish<0 || $is_finish>1){
			$is_finish = 0;
		}
		g('smarty') -> assign('title', '我参与的活动');
		g('smarty') -> assign('is_finish', $is_finish);
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$ActionActivity);
		g('smarty') -> show($this -> temp_path.'activity_list_all.html');
	}
	
	/** 最新活动 */
	public function new_activity() {
		global $app;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=detail';
		g('smarty') -> assign('title', '最新活动');
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$NewActivity);
		g('smarty') -> show($this -> temp_path.'activity_list_new.html');
	}
	

	/** 已发布的 */
	public function publiced() {
		global $app;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=detail';
		g('smarty') -> assign('title', '我发起的');
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$MyActivity);
		g('smarty') -> assign('draft_list_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=draft&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_list_publiced.html');
	}
	
	/** 草稿 */
	public function draft() {
		global $app;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=edit';
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('edit_url', $edit_url);
		
		g('smarty') -> assign('title', '我发起的');
		g('smarty') -> assign('list_type', self::$DeaftActivity);
		g('smarty') -> assign('publiced_list_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_list_draft.html');
	}
	
	/** 活动详情 */
	public function detail() {
		try {
			$self = (int)get_var_get('self');
			$id = (int)get_var_get('act_id');
	    	if(empty($id)){
	    		throw new SCException('请求错误');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$fields = 'act.*,u.name,u.pic_url public_pic,scope_dept,scope_user';
	    	$act = g('activity_info') -> get_by_id($com_id,$id,$fields,true);
	    	if(!$act){
	    		throw new SCException('活动不存在');
	    	}
	    	
	    	g('activity_info') -> activity_see($user_id,$com_id,$id);

	    	$act['activity_describe'] = str_replace('\n', '<br>', json_encode($act['activity_describe']));
	    	$act['activity_describe'] = json_decode($act['activity_describe']);

	    	//检查用户是否有权限报名
	    	$scope_dept = json_decode($act['scope_dept'],true);
	    	$scope_user = json_decode($act['scope_user'],true);
	    	$act['enroll_privirage'] = g('activity_info') -> check_user($user_id,$scope_dept,$scope_user);


	    	//若活动进行中，返回相关状态
	    	$act['can_enroll'] = 0;
	    	$act['can_sign'] = 0;
	    	$act['enroll_limit'] = 0;
	    	if($act['state'] == self::$ActivityIn){
		    	$now = time();
		    	if($now <= $act['enroll_time']){
		    		$act['can_enroll'] = 1;		//报名时间
		    	}
		    	
		    	if($act['limited'] <= $act['enroll_num']){
		    		$act['enroll_limit'] = 1;	//报名人数限制是否达到上限
		    	}
		    	
		    	if($now <= $act['sign_time'] ){
		    		$act['can_sign'] = 0;		//未到签到时间
		    	}else if($now >= $act['sign_time']){
		    		$act['can_sign'] = 1;		//签到时间
		    	}
		    	
		    	if($now < $act['start_time']){
		    		$act['act_state'] = 0;		//活动未开始
		    	}else if($now >= $act['start_time'] && $now < $act['end_time']){
		    		$act['act_state'] = 1;		//活动进行中
		    	}else{
		    		$act['act_state'] = 2;		//活动结束
		    	}
		    	
		    	$act['sign_state'] = 0;
		    	$act['famil_num'] = 0;
		    	$enroll = g('activity_info') -> get_enroll_data($com_id,$user_id,$id);
		    	if(!empty($enroll)){
			    	$act['famil_num'] = $enroll['family_num'];
		    		$act['enroll_state'] = 1;
		    		$act['sign_state'] = $enroll['sign_state'];
		    	}else{
		    		$act['enroll_state'] = 0;
		    	}
		    	
	    	}
    		$follow = g('activity_info') -> get_activity_follow($com_id,$id,$user_id);
	    	if(!empty($follow)){
	    		$act['follow_state'] = 1;
	    	}else{
	    		$act['follow_state'] = 0;
	    	}
	    	
	    	//格式化数据
	    	$act['scope_user'] = json_decode($act['scope_user'],true);
	    	$act['scope_dept'] = json_decode($act['scope_dept'],true);
	    	$act['enroll_time'] = date('Y-m-d H:i',$act['enroll_time']);
	    	$act['start_time'] = date('Y-m-d H:i',$act['start_time']);
	    	$act['public_time'] = date('Y-m-d H:i',$act['public_time']);
	    	$act['end_time'] = date('Y-m-d H:i',$act['end_time']);
	    	$act['sign_time'] = date('Y-m-d H:i',$act['sign_time']);
	    	$act['enroll_custom'] = json_decode($act['enroll_custom'],TRUE);
			
			$self = 0;
	    	if($act['is_admin']==1){
	    		$act['public_name'] = '管理员';
	    		$act['public_pic'] = '';
	    		$act['pic_url'] = '';
	    	}else if($act['public_id'] == $user_id){
	    		$self = 1;
	    	}
			global $app;
			$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
			$follow_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&ajax_act=follow';
			$comment_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&ajax_act=add_comment';
			$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=edit&act_id='.$id;
			$comment_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=comments&act_id='.$id;
			$album_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=album_detail&&act_id='.$id;
			$user_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=users&&act_id='.$id;
			$enroll_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&ajax_act=enroll';
			$enroll_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=enroll&act_id='.$id;
			$sign_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=sign&act_id='.$id;
			$reply_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=replys&act_id='.$id;
			$sign_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=sign_list&id='.$id;
			
			g('smarty') -> assign('ajax_url', $ajax_url);
			g('smarty') -> assign('edit_url', $edit_url);
			g('smarty') -> assign('follow_url', $follow_url);
			g('smarty') -> assign('comment_url', $comment_url);
			g('smarty') -> assign('comment_list_url', $comment_list_url);
			g('smarty') -> assign('album_list_url', $album_list_url);
			g('smarty') -> assign('user_list_url', $user_list_url);
			g('smarty') -> assign('enroll_url', $enroll_url);
			g('smarty') -> assign('enroll_page_url', $enroll_page_url);
			g('smarty') -> assign('sign_page_url', $sign_page_url);
			g('smarty') -> assign('reply_page_url', $reply_page_url);
			g('smarty') -> assign('sign_list_url', $sign_list_url);
			
			$user_info = g('user') -> get_by_id($user_id);
			g('smarty') -> assign('user_info', $user_info);
			g('smarty') -> assign('self', $self);
			g('smarty') -> assign('activity', $act);
			g('smarty') -> assign('title', '活动详情');
			g('smarty') -> show($this -> temp_path.'activity_detail.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}
	}
	
	/** 评论列表 */
	public function comments() {
		$act_id = get_var_get('act_id');
		if (empty($act_id))			cls_resp::show_err_page(array('未指定活动，无法查看评论信息'));
		$act_id = intval($act_id);
		if (empty($act_id))			cls_resp::show_err_page(array('未指定活动，无法查看评论信息'));
		
		$comment_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=comment_list';
		$comment_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=add_comment';
		$reply_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=replys&act_id='.$act_id;
		g('smarty') -> assign('comment_list_url', $comment_list_url);
		g('smarty') -> assign('comment_url', $comment_url);
		g('smarty') -> assign('reply_page_url', $reply_page_url);
		
		$user_info = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('user_info', $user_info);
		g('smarty') -> assign('act_id', $act_id);
		g('smarty') -> assign('title', '评论列表');
		g('smarty') -> show($this -> temp_path.'activity_comment_list.html');
	}

	/** 回复评论 */
	public function replys() {
		$act_id = get_var_get('act_id');
		if (empty($act_id))			cls_resp::show_err_page(array('未指定活动，无法查看评论信息'));
		$comm_id = get_var_get('comm_id');
		if (empty($comm_id)) 		cls_resp::show_err_page(array('未指定要回复的评论'));
		$comm = g('activity_info') -> get_comment($_SESSION[SESSION_VISIT_COM_ID], $comm_id);
		if (empty($comm))			cls_resp::show_err_page(array('评论信息不存在'));
		if ($comm['pid'] != 0)		cls_resp::show_err_page(array('评论信息不存在'));
		
		$comm_user = g('user') -> get_by_id($comm['comment_id'], '*', TRUE);
		$comm['user_name'] = empty($comm_user) ? '' : $comm_user['name'];
		$comm['user_pic'] = empty($comm_user) ? '' : $comm_user['pic_url'];
		$comment_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=comment_list';
		$comment_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=add_comment';
		g('smarty') -> assign('comment_list_url', $comment_list_url);
		g('smarty') -> assign('comment_url', $comment_url);
		
		$user_info = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('user_info', $user_info);
		g('smarty') -> assign('comm_info', $comm);
		g('smarty') -> assign('act_id', $act_id);
		g('smarty') -> assign('comm_id', $comm_id);
		
		g('smarty') -> assign('title', '回复');
		g('smarty') -> show($this -> temp_path.'activity_reply_list.html');
	}
	
	/** 报名人员列表 */
	public function users() {
		try{
			$id = (int)get_var_get('act_id');
	    	if(empty($id)){
				throw new SCException('请求错误');
	    	}
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$fields = 'act.*,u.name,u.pic_url public_pic';
			$act = g('activity_info') -> get_by_id($com_id, $id, $fields, TRUE);
	    	if(!$act){
				throw new SCException('活动不存在');
	    	}
	    	
			$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=enroll_list';
			g('smarty') -> assign('list_url', $list_url);
			
			g('smarty') -> assign('title', '报名列表');
			g('smarty') -> assign('activity', $act);
			g('smarty') -> show($this -> temp_path.'activity_user_list.html');
			
		}catch(SCException $e){
			cls_resp::show_err_page(array($e->getMessage()));
		}
	}
	
	/** 活动签到 */
	public function sign() {
		try {
			parent::oauth_jssdk();
			$act_id = get_var_get('act_id');
			if(empty($act_id)){
				cls_resp::show_err_page(array('请求错误'));
			}
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$act = g('activity_info') -> get_enroll_data($com_id,$user_id,$act_id);
			
			$user = g('user') -> get_by_id($user_id,'name,dept_list,pic_url');
			$dept_list = json_decode($user['dept_list'],true);
			$dept_id = array_shift($dept_list);
			$dept = g('dept') -> get_dept_by_id($dept_id,'name');
			$user['dept_name'] = $dept['name'];
			$family = json_decode($act['family'],true);
			g('smarty') -> assign('user', $user);
			g('smarty') -> assign('activity', $act);
			g('smarty') -> assign('family', $family);
			g('smarty') -> assign('is_sign', $act['sign_state']);
			g('smarty') -> assign('sign_time', $act['sign_time']);
			
			$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=sign';
			$transfer_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=transfer';
			$activity_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=detail&act_id='.$act_id;
			
			g('smarty') -> assign('sign_url', $sign_url);
			g('smarty') -> assign('transfer_url', $transfer_url);
			g('smarty') -> assign('activity_page_url', $activity_page_url);
			
			g('smarty') -> assign('title', '活动签到');
			g('smarty') -> show($this -> temp_path.'activity_sign.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}
	}
	
	/** 所有相册 */
	public function all_album() {
		global $app;
		g('smarty') -> assign('title', '所有相册');
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=album_detail';
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$AllAtlas);
		g('smarty') -> show($this -> temp_path.'activity_album_all.html');
	}
	
	/** 我的相册(我发布的) */
	public function publiced_album() {
		global $app;
		g('smarty') -> assign('title', '我的相册');
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=album_wait';
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$MyAtlas);
		g('smarty') -> assign('partake_album_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=partake_album&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_album_publiced.html');
	}
	
	/** 我的相册(我参与的) */
	public function partake_album() {
		global $app;
		g('smarty') -> assign('title', '我的相册');
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=show&a=album_detail';
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('list_type', self::$ActionAtlas);
		g('smarty') -> assign('publiced_album_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced_album&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_album_partake.html');
	}
	
	/** 待审批图片 */
	public function album_wait() {
		global $app;
		parent::oauth_jssdk();	//用于预览大图
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$a_id = get_var_get('aid');
		$act_id = get_var_get('act_id');
		
		$pass_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&ajax_act=approval_image';
		g('smarty') -> assign('pass_url', $pass_url);
		
		g('smarty') -> assign('aid', $a_id);
		g('smarty') -> assign('act_id', $act_id);
		g('smarty') -> assign('title', '我的相册');
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('post_url', $ajax_url);
		g('smarty') -> assign('list_type', self::$NoApprovalAtlas);
		g('smarty') -> assign('album_pass_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=album_pass&act_id='.$act_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_album_wait.html');
	}
	
	/** 已通过图片 */
	public function album_pass() {
		global $app;
		parent::oauth_jssdk();	//用于预览大图
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		$a_id = get_var_get('aid');
		$act_id = get_var_get('act_id');
		
		$delete_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&ajax_act=delete_image&act_id='.$act_id;
		g('smarty') -> assign('delete_url', $delete_url);
		
		g('smarty') -> assign('aid', $a_id);
		g('smarty') -> assign('act_id', $act_id);
		g('smarty') -> assign('title', '我的相册');
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('post_url', $ajax_url);
		g('smarty') -> assign('list_type', self::$ApprovalAtlas);
		g('smarty') -> assign('album_wait_url', SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=album_wait&act_id='.$act_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL]);
		g('smarty') -> show($this -> temp_path.'activity_album_pass.html');
	}
	
	/** 相册详情 */
	public function album_detail() {
		global $app;
		$atlas_id = get_var_get('aid');
		if(empty($atlas_id))				cls_resp::show_err_page(array('指定相册不存在'));
		
		parent::oauth_jssdk();
		g('smarty') -> assign('title', '相册详情');
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';
		
		g('smarty') -> assign('atlas_id', $atlas_id);
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('list_type', self::$ApprovalAtlas);
		g('smarty') -> show($this -> temp_path.'activity_album_detail.html');
	}
	

	/** 添加活动 */
	public function add() {
		parent::assign_contact_dept();
		parent::oauth_jssdk();
		
		$priviage = g('activity_info') -> check_prviage($_SESSION[SESSION_VISIT_COM_ID],$_SESSION[SESSION_VISIT_USER_ID]);
		if(!$priviage){
			cls_resp::show_warn_page(array('您没有权限发起活动，请联系管理员配置权限！'));
		}

		$save_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=save_activity';
		$custom_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=get_enroll_custom';
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced';
		g('smarty') -> assign('save_url', $save_url);
		g('smarty') -> assign('custom_field_url', $custom_url);
		g('smarty') -> assign('list_url', $list_url);
		
		$storage_key = $this -> app_name .'_'.__FUNCTION__;
		g('smarty') -> assign('storage_key', $storage_key);
		g('smarty') -> assign('title', '发起活动');
		g('smarty') -> show($this -> temp_path.'activity_add.html');
	}
	
	/** 编辑活动 */
	public function edit() {
		try{
			parent::assign_contact_dept();
			parent::oauth_jssdk();
			
			$id = (int)get_var_get('act_id');
	    	if(empty($id)){
				throw new SCException('请求错误');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$fields = 'act.*,u.name,u.pic_url public_pic';
	    	$act = g('activity_info') -> get_by_id($com_id,$id,$fields,true);
	    	if(!$act){
				throw new SCException('活动不存在');
	    	}
	    	if($act['public_id'] != $_SESSION[SESSION_VISIT_USER_ID]){
				throw new SCException('没有权限');
	    	}
	    	
	    	//格式化数据
	    	$act['scope_user'] = json_decode($act['scope_user'],true);
	    	if(!empty($act['scope_user'])){
	    		$act['scope_user'] = g('user') -> get_by_ids($act['scope_user'],'id,name');
	    	}
	    	$act['scope_dept'] = json_decode($act['scope_dept'],true);
	    	if(!empty($act['scope_dept'])){
	    		$act['scope_dept'] = g('dept') -> get_dept_name($act['scope_dept'],'id,name');
	    	}
	    	$act['enroll_time'] = date('Y-m-d H:i',$act['enroll_time']);
	    	$act['start_time'] = date('Y-m-d H:i',$act['start_time']);
	    	$act['public_time'] = date('Y-m-d H:i',$act['public_time']);
	    	$act['end_time'] = date('Y-m-d H:i',$act['end_time']);
	    	$act['sign_time'] = date('Y-m-d H:i',$act['sign_time']);
	    	$act['enroll_custom'] = json_decode($act['enroll_custom'],TRUE);
	    	
			$save_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=save_activity';
			$custom_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=get_enroll_custom';
			$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced';
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('custom_field_url', $custom_url);
			g('smarty') -> assign('list_url', $list_url);
			
			$act['file_info'] = array();
	    	$img = g('activity_info') -> get_cover_img($com_id,$id,' file_key,file_name,file_url,extname ');
	    	if(!empty($img)){
	    		$act['file_info'] = $img[0];
	    	}
	    	
			g('smarty') -> assign('title', '编辑活动');
			g('smarty') -> assign('activity', $act);
			g('smarty') -> show($this -> temp_path.'activity_edit.html');
			
		}catch(SCException $e){
			cls_resp::show_err_page(array($e->getMessage()));
		}
	}
	
	/** 报名活动 */
	public function enroll() {
		try{
			$id = (int)get_var_get('act_id');
	    	if(empty($id)){
				throw new SCException('请求错误');
	    	}
	    	$com_id = $_SESSION[SESSION_VISIT_COM_ID];
	    	$user_id = $_SESSION[SESSION_VISIT_USER_ID];
	    	$fields = 'act.*,u.name,u.pic_url public_pic';
	    	$act = g('activity_info') -> get_by_id($com_id, $id, $fields, TRUE);
	    	if(!$act || $act['state'] != 1){
				cls_resp::show_err_page(array('活动不存在或未发布'));
	    	}
	    	$act['enroll_custom'] = json_decode($act['enroll_custom'], TRUE);
	    	/* 有带家属也需要到该页面
	    	if (empty($act['enroll_custom'])) {
				cls_resp::show_err_page(array('没有需要填写的报名信息，请返回活动详情页面报名'));
	    	}
	    	*/
	    	
	    	$user = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID],'name');
	    	$user_name = $user['name'];

	    	$enroll = g('activity_info') -> get_enroll_data($com_id,$user_id,$id);
	    	if(empty($enroll)){
	    		$enroll = array();
	    	}else{
	    		$enroll['family'] = json_decode($enroll['family'],true);
	    		$enroll['custom'] = json_decode($enroll['custom'],true);
	    	}
	    	
			$enroll_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=enroll';
			$cancel_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=ajax&ajax_act=cancel_enroll';
			$active_url = SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=detail&act_id='.$id;
			g('smarty') -> assign('cancel_url', $cancel_url);
			g('smarty') -> assign('enroll_url', $enroll_url);
			g('smarty') -> assign('active_url', $active_url);
			
			g('smarty') -> assign('activity', $act);
			g('smarty') -> assign('user_name', $user_name);
			g('smarty') -> assign('enroll', $enroll);
			g('smarty') -> assign('title', '活动报名');
			g('smarty') -> show($this -> temp_path.'activity_enroll.html');
			
		}catch(SCException $e){
			cls_resp::show_err_page(array($e->getMessage()));
		}
	}

}

// end of file