<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	//新建/编辑活动
	'activity_edit_union.css' => array(
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		SYSTEM_APPS . 'activity/static/css/activity_edit.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'activity_edit_union.js' => array(
        SYSTEM_ROOT . 'static/js/gdmap.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
		SYSTEM_APPS . 'activity/static/js/activity_edit.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js',
		SYSTEM_ROOT . 'static/js/static/js/jweixin-1.1.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
	),
	'activity_add_union.js' => array(
        SYSTEM_ROOT . 'static/js/gdmap.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
		SYSTEM_APPS . 'activity/static/js/activity_add.js',
		//本地缓存
		SYSTEM_ROOT . 'static/js/storage.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js',
		SYSTEM_ROOT . 'static/js/static/js/jweixin-1.1.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
	),
	//上传相册图片
	'album_detail_union.css' => array(
		SYSTEM_APPS . 'activity/static/css/activity_album_detail.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'album_detail_union.js' => array(
		SYSTEM_APPS . 'activity/static/js/activity_album_detail.js',
		//上传
        SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.2.0.js',
        /*
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js',
		*/
	),
);

return $arr;

/* End of this file */