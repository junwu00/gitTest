<?php
/**
 * 应用的配置文件_意见反馈
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id'       => 22,                                          //对应sc_app表的id
    //此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
    'combo' => g('com_app') -> get_sie_id(22),
    'name'      => 'activity',
    'cn_name'   => '组织活动',
    'icon'      => SYSTEM_HTTP_APPS_ICON.'Activity.png',

    'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '最新活动', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=new_activity'),
			array('id' => 'bmenu-2', 'name' => '所有活动', 
				'childs' => array(
					array('id' => 'bmenu-2-1', 'name' => '活动列表', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=all'),
					array('id' => 'bmenu-2-2', 'name' => '活动相册', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=all_album'),
				)
			),
			array('id' => 'bmenu-3', 'name' => '我的活动', 
				'childs' => array(
					array('id' => 'bmenu-3-1', 'name' => '发起活动', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=add'),
					array('id' => 'bmenu-3-2', 'name' => '我参与的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=action'),
					array('id' => 'bmenu-3-3', 'name' => '我发起的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced'),
					array('id' => 'bmenu-3-4', 'name' => '我的相册', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=activity&m=show&a=publiced_album'),
				)
			),
		),
    ),
);

// end of file