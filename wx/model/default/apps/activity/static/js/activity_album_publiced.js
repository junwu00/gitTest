var page = 0;

$(document).ready(function() {
	$('#rec-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	
	$(document).click(function(evt){
		 if($(evt.toElement)[0].id == 'sort'){
			 return false;
		 }
		$("#sort_div").hide();
		$("#sort_finish_div").hide();
	});
	
	$("#search_input").unbind('input').bind('input', function() {
		page = 0;
		load_lists();
	});
	
});

//加载相册列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.is_finish = $('.active').attr('t');
	data.page = page;
	data.search_key = $("#search_input").val();
	data.type = $('#list_type').val();
	data.desc = $('#sort').attr('data-sort') == 'down' ? 1 : -1;	//1降序 -1升序
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'atlas_list', JSON.stringify(data), show_list);	//TODO
}

//显示相册列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  

	var media_url = $('#media-url').val();
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	if(list != "") {
		for (var i in list) {
			var atlas = list[i];
			html += '<div class="list-item" onclick="to_detail('+atlas['id']+','+atlas['act_id']+');">'+
						'<img src="'+media_url+atlas['pic_url']+'" onerror="this.src=\'' + $('#def_img_url').val() + '\'">'+
						'<p>'+
							'<span class="title">'+atlas['title']+'</span>'+
							'<span class="count">('+atlas['image_num']+')</span>'+
							'<span class="time">'+atlas['public_time']+'</span>'+
						'</p>'+
						'<i class="icon-angle-right"></i>'+
					'</div>';
		}
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

//跳转到相册详情
function to_detail(id, act_id){
	location.href = $('#detail_url').val() + '&aid=' + id + '&act_id=' + act_id;
}

//排发布时间升序/降序
function sort_div(obj){
	var sort = $(obj).attr('data-sort');
	if(sort == 'down') {	//当前为时间降序
		$(obj).find('span').removeClass('glyphicon-arrow-down');
		$(obj).find('span').addClass('glyphicon-arrow-up');
		$(obj).attr('data-sort', 'up');
		
	} else {				//当前为时间升序
		$(obj).find('span').removeClass('glyphicon-arrow-up');
		$(obj).find('span').addClass('glyphicon-arrow-down');
		$(obj).attr('data-sort', 'down');
	}

	page = 0;
	$('#rec-list').html('');
	$('#rec-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
}