jsApiList = ['getLocation'];
$(document).ready(function() {
	$('#powerby').remove();
	if ($('input.family-member').length > 0 && $('#is-sign').val() == 0) {	//有携带家属且未签到
		$('#sign-member-count').html($('input.family-member').length + 1);
	}
	init_family_select();
	init_sign();
	
	auto_ticket_info_top();
	$(window).resize(function() {
		auto_ticket_info_top();
	});
});

//自适应窗口
function auto_ticket_info_top() {
	var ticket_w = $('#ticket-body').width();
	var ticket_top = parseInt(ticket_w * 0.26);
	var info_top = parseInt(ticket_w * 0.2);
	$('#sign-info').css({top: ticket_top});
	$('#sign-user').css({'margin-top': info_top});
}

//打票动画
function show_ticket() {
	var target_top = 35;
	$('#ticket-body').animate({top: 35}, 3000);
}

//选择家属
function init_family_select() {
	$('input.family-member').each(function() {
		$(this).unbind('change').bind('change', function() {
			var count = 0;
			$('input.family-member').each(function() {
				if ($(this)[0].checked) {
					count ++;
				}
			});
			$('#sign-member-count').html(count + 1);
		});
	});
}

//执行签到
function init_sign() {
	var signed = ($('#is-sign').val() == 0) ? false : true;
	
	if ($('#member-bar').length > 0) {	//要点击按钮才签到
		var member_h = $('#member-bar').height();
		$('#result-bar').css({top: member_h});
		
		if (signed) {	//已签到
			$('#result-bar').css({top: 0});
			$('#ticket-body').css({top: 35});
			$('#member-bar').addClass('hide');
			$('#result-bar').removeClass('hide');
		}
		
		$('#do-sign').unbind('click').bind('click', function() {
			frame_obj.lock_screen('签到中...', true);
			wx.ready(function(){
				get_local(function() {
					var member_h = $('#member-bar').height();
					$('#member-bar').animate({top: -member_h}, 'slow', function() {
						$('#member-bar').addClass('hide');
					});
					$('#result-bar').removeClass('hide');
					$('#result-bar').animate({top: 0}, 'slow', function() {
						auto_ticket_info_top();
						show_ticket();
					});
				});
			});
		});
		
	} else {							//不需要点击按钮签到
		if (signed) {	//已签到
			$('#ticket-body').css({top: 35});
			
		} else {		//未签到
			frame_obj.lock_screen('签到中...', true);
			wx.ready(function(){
				get_local();
			});
		}
	}
}

function get_local(callback) {
/*test------------------	
	$('#ajax-url').val(transfer_url);
	var req_data = {
		lng: 113.31525,
		lat: 23.164963
	};
	frame_obj.do_ajax_post(undefined, 'transfer', req_data, function(resp) {
		if (resp.errcode == 0) {
			var data = resp.info;
			do_sign(undefined, data, callback);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
		frame_obj.unlock_screen();
	});
	return;
//.test------------------*/	
	
	wx.getLocation({
		success: function (res) {
			$('#ajax-url').val(transfer_url);
			var req_data = {
				lng: res.longitude,
				lat: res.latitude
			};
			frame_obj.do_ajax_post(undefined, 'transfer', req_data, function(resp) {
				if (resp.errcode == 0) {
					var data = resp.info;
					do_sign(undefined, data, callback);
					
				} else {
					frame_obj.tips(resp.errmsg);
				}
				frame_obj.unlock_screen();
			});
			
		},
		cancel:function() {
			frame_obj.alert('要打开地理位置才能定位哦', '我知道了', get_local);
			frame_obj.unlock_screen();
		},
		complete:function() {
		},
	});
}

//执行签到
function do_sign(that, location, callback) {
	$('#ajax-url').val(sign_url);
	var data = new Object();
	data.location = location;
	data.act_id = $('#act-id').val();
	data.family_sign = new Array();
	var count = 0;	//家属人数
	$('input.family-member').each(function() {
		if ($(this)[0].checked) {
			data.family_sign.push($(this).attr('data-name'));
			count ++;
		}
	});
	
	frame_obj.do_ajax_post($(that), 'sign', data, function(resp) {
		frame_obj.unlock_screen();
		if (resp.errcode == 0) {
			var date = new Date();
			var html = '时间: ';
			html += (date.getHours() < 10) ? ('0' + date.getHours()) : date.getHours();
			html += ':';
			html += (date.getMinutes() < 10) ? ('0' + date.getMinutes()) : date.getMinutes();
			html += ':';
			html += (date.getSeconds() < 10) ? ('0' + date.getSeconds()) : date.getSeconds();
			$('#sign-time').html(html);
			$('#sign-title').html('签到成功');
			if (typeof(callback) === 'function') {
				if (count > 0) {
					$('#user-family').html('携带' + count + '名家属');
				}
				callback();

			} else {
				show_ticket();
				$('#user-family').html('');
			}
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}