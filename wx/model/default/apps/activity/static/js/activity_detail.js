$(document).ready(function() {
	load_detail();
	init_timer();
	init_sign_timer();
});

//签到倒计时
function init_sign_timer() {
	if ($('.before-sign-time').length == 0)	return;
	
	var sign_time = $('.before-sign-time').attr('data-time');
	sign_time = sign_time.replace(/[\s,\-,:]/g, ',');
	t_arr = sign_time.split(',');
	sign_time = new Date(t_arr[0], t_arr[1]-1, t_arr[2], t_arr[3], t_arr[4], 0);
	sign_timer(sign_time);

	function sign_timer(date) {
		if ((date - new Date()) > 0) {	//未到时间
			setTimeout(function() {
				sign_timer(date);
			}, 1000);
			
		} else {						//到时间
			var html = '<button type="button" class="btn btn-cancel" onclick="cancel_enroll(this);">取消报名</button>' +
				'<button type="button" class="btn btn-success" onclick="location.href=\'' + sign_url + '\'">我要签到</button>';
			$('#activity-sign-bar').html(html);
		}
	}
}

//初始化倒计时
function init_timer() {
	if ($('#timer').length > 0) {	//需要倒计时
		var enroll_time = $('#timer').attr('data-time');
		enroll_time = enroll_time.replace(/[\s,\-,:]/g, ',');
		t_arr = enroll_time.split(',');
		enroll_time = new Date(t_arr[0], t_arr[1]-1, t_arr[2], t_arr[3], t_arr[4], 0);
		frame_obj.timer('#timer', enroll_time, false, function() {
			$('#activity-sign-bar').html('<div style="text-align:center;">活动已截止报名</div>');
		});
	}
}

//终止活动
function do_state(that){
	var that_html = $(that).html();
	frame_obj.comfirm('确定要'+that_html+'吗？',function(){
		var id = $(that).attr('data-id');
		var state_name = '';
		var data = {};
		data.act_id = id;
		data.state = 2;
		state_name = '终止活动';
		
		frame_obj.do_ajax_post(undefined, 'activity_state', JSON.stringify(data), function(response){
			if(response['errcode']==0){
				frame_obj.tips(that_html+'成功');
				$(that).html(state_name);
				$(that).attr('data-state', data.state);
				setTimeout(function() {
					location.reload();
				}, 500);
				
			}else{
				frame_obj.tips(response['errmsg']);
			}
		});
	});
}

//加载评论等其他信息
function load_detail() {
	var data = new Object();
	data.id = $('#act-id').val();
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'get_some', JSON.stringify(data), show_list);
}

//加载评论等其他信息后的处理
function show_list(data){
	if(data['errcode'] == 0){
		var media_url = $('#media-url').val();
		var comment = data['info']['comment'];
		var image = data['info']['image'];
		var enroll = data['info']['enroll'];
		var comment_html = '';
		var enroll_html = '';
		var image_html = '';
		var album_url = $('#album-head').attr('data-url');
		album_url = album_url + '&aid=' + data['info']['atlas_id'];
		$('#upload-album-link').attr('href', album_url + '&aid=' + data['info']['atlas_id']);
		$('#album-head').unbind('click').bind('click', function() {
			location.href = album_url;
		});
		$('#album-icon').unbind('click').bind('click', function() {
			location.href = album_url;
		});
		
		if(comment != ""){
			for(var i =0;i<comment.length;i++){
				var comm = comment[i];
				comment_html += '<div class="comment-item" onclick="to_reply('  + comm['id'] + ')">'+
					'<img src="'+comm['pic_url']+'64" onerror="this.src=\'static/image/face.png\'">'+
					'<span class="body">'+
						'<span>'+comm['name']+'</span><br>'+
						'<span>'+comm['comment_content']+'</span>'+
					'</span>'+
					'<span class="reply">' +
						'<span class="time">' + frame_obj.recent_time(comm['comment_time']) + '</span>' + 
						'回复(' + comm['count'] + ')' +
					'</span>' +
				'</div>';
			}
		}
		
		if(enroll !=''){
			for(var j=0;j<enroll.length;j++){
				var en = enroll[j];
				enroll_html +='<img src="'+en['pic_url']+'64" onerror="this.src=\'static/image/face.png\'">';
			}
		}
		if(image !=''){
			for(var k=0;k<image.length;k++){
				var img = image[k];
				image_html +='<div class="img-pre-wrap"><div class="img-pre-bar"><img src="'+media_url+img['file_url']+'"></div></div>';
			}
			if (image.length < $('#album-total-count').html()) {
				image_html += '<div class="img-pre-wrap" style="position: absolute;top:15px;">' +
					'<div style="width: 100%; height:100%;display: inline-block; padding-top: 90px; text-align: center;">' +
						'<a href="' + album_url + '" style="color: #9b9b9b;">查看更多</a>' +
					'</div>' +
				'</div>';
			}
			$('#album-body').html(image_html);
		}
		
		$('#comment-list').html(comment_html);
		$('#partake-user>span').html(enroll_html);
	}else{
		frame_obj.alert(data['errmsg']);
	}
}

//跳转到回复页面
function to_reply(comm_id) {
	location.href = reply_url + '&comm_id=' + comm_id;
}

//发表评论
function do_comment(that) {
	$('#ajax-url').val(comment_url);
	var data = new Object();
	data.pid = 0;
	data.aid = $('#act-id').val();
	data.comment = $('#comment-text').val();
	
	if (!check_data(data.comment, 'notnull')) {
		frame_obj.tips('请填写评论内容');
		return false;
	}

	$(that).attr('style', 'background-color: #f4f4f4 !important');
	frame_obj.do_ajax_post($(that), 'add_comment', data, function(resp) {
		$(that).attr('style', 'background-color: #ffffff !important');
		if (resp.errcode == 0) {
			$('#comment-text').val('');
			var count = $('.comment-item').length;
			var html = '<div class="comment-item">' +
				'<img src="' + $('#user-pic').val() + '" onerror="this.src=\'static/image/face.png\'">' +
				'<span class="body">' + 
					'<span>' + $('#user-name').val() + '</span><br>' + 
					'<span>' + data.comment + '</span>' + 
				'</span>' + 
				'<span class="reply">' +
					'<span class="time">刚刚</span>' +
					'回复(0)' +
				'</span>' +
			'</div>';
			if (count == 0) {				//没有记录，直接加入一条
				$('#comment-list').html(html);
				
			} else if (count < 3) {			//有记录，但小于三条，直接加入一条
				$('#comment-list .comment-item:first-child').before(html);
				
			} else {						//超过三条，增加后要删除最后一条
				$('#comment-list .comment-item:first-child').before(html);
				$('#comment-list .comment-item:last-child').remove();
			}
			var comment_count = $('.comment-count').html();
			$('.comment-count').html(Number(comment_count) + 1);
			
			if (Number(comment_count) + 1 > 3) {	//评论后总数超过3条，显示"更多评论"
				$('#comment-more').removeClass('hide');
			}
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//点赞
var is_following = false;	//当前是否已经点了（防止快速多次点击）
function do_follow(that) {
	if (is_following || $(that).find('.glyphicon-heart').hasClass('active'))	return;	//已点赞，不处理
	
	is_following = true;
	$('#ajax-url').val(follow_url);
	frame_obj.do_ajax_post($(that), 'follow', {act_id: $('#act-id').val()}, function(resp) {
		if (resp.errcode == 0) {
			//动画
			$('#heart-count-plus').animate({opacity: 1, top: '-20px'}, 'normal', function() {
				$('#heart-count-plus').animate({opacity: 0}, 'fast', function() {
					$('#heart-count-plus').css({top: '-15px'});			
				});
				
				//更新数量
				$(that).find('.glyphicon-heart').addClass('active');
				var heart_count = $('.heart-count').html();
				$('.heart-count').html(Number(heart_count) + 1);
				is_following = false;
			});
			
		} else {
			frame_obj.tips(resp.errmsg);
			is_following = false;
		}
	});
}

//报名
function enroll_activity(that) {
	var custom_cnt = $(that).attr('data-custom');
	var can_family = $(that).attr('data-family');
	if (custom_cnt > 0) {			//有自定义报名字段，跳转到报名页面
		location.href = enroll_page_url;
		
	} else if (can_family == 1) {	//可带家属
		location.href = enroll_page_url;
		
	} else {						//无自定义报名字段，且不能带家属，当前页面直接报名
		$('#ajax-url').val(enroll_url);
		frame_obj.do_ajax_post($(that), 'enroll', {id: $('#act-id').val()}, function(resp) {
			if (resp.errcode == 0) {
				frame_obj.tips('报名成功');
				setTimeout(function() {
					window.location.href = window.location.href + "&v="+new Date().getTime();
				}, 100);
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		});
	}
}

//取消报名
function cancel_enroll(that) {
	frame_obj.comfirm('确定要取消报名吗？', function(){
		$('#ajax-url').val(enroll_url);
		frame_obj.do_ajax_post($(that), 'cancel_enroll', {act_id: $('#act-id').val()}, function(resp) {
			if (resp.errcode == 0) {
				frame_obj.tips('取消报名成功');
				setTimeout(function() {
					window.location.href = window.location.href + "&v="+new Date().getTime();
				}, 100);
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		});
	});
}