$(document).ready(function() {
	$('#powerby').remove();
	init_family_sel();
});

//报名
function do_enroll(that) {
	var data = get_enroll_data();
	if (!check_enroll_data(data))	return;
	
	$('#ajax-url').val(enroll_url);
	data.id = $('#act-id').val();
	frame_obj.do_ajax_post($(that), 'enroll', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('报名成功');
			setTimeout(function() {
				location.href = activity_url;
			}, 500);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//获取报名数据
function get_enroll_data() {
	var data = new Object();
	var arr = new Array();
	$('#self-info .custom-field').each(function() {
		arr.push({name: $(this).attr('data-field'), val: $(this).val()});
	});
	data.custom_msg = arr;
	
	if ($('#family-container').length > 0) {	//可带家属
		if ($('#family-container .family-item').length > 0) {	//要带家属
			var family = new Array();
			$('#family-container .family-item').each(function() {
				var item = new Object();
				$(this).find('.const-field').each(function() {	//固定字段
					item[$(this).attr('data-field')] = $(this).val();
				});
				
				item.custom = new Array();
				$(this).find('.custom-field').each(function() {	//自定义字段
					item.custom.push({name: $(this).attr('data-field'), val: $(this).val()});
				})
				family.push(item);
			});
			data.family_msg = family;
		}
	}
	return data;
}

//验证报名数据(全部必填)
function check_enroll_data(data) {
	var arr = data.custom_msg;
	for (var i in arr) {
		if (!check_data(arr[i].val, 'notnull')) {
			frame_obj.tips(arr[i].name + '不能为空');	return false;
		}
	}
	if (data.family_msg) {	//有携带家属
		for (var i in data.family_msg) {
			var item = data.family_msg[i];
			if (!check_data(item.name, 'notnull')) {
				frame_obj.tips('第' + (Number(i)+1) + '名家属的姓名不能为空');	return false;
			}
			var custom = item.custom;
			for (var j in custom) {
				if (!check_data(custom[j].val, 'notnull')) {
					frame_obj.tips('第' + (Number(i)+1) + '名家属的' + custom[j].name + '不能为空');			return false;
				}
			}
		}
	}
	return true;
}

//选择家属人数
function init_family_sel() {
	var count = $('#family-count').attr('data-count');
	var html = '';
	for (var i=1; i<=count; i++) {
		html += '<option value="' + i + '">' + i + '名</option>';
	}
	$('#family-count').append(html);
	$('#family-count').val($('#family-container .family-item').length);
	
	$('#family-count').unbind('change').bind('change', function() {
		var count = $(this).val();
		if (count == 0) {
			$('#family-container').html('');
			return;
		}
		
		var curr_count = $('#family-container .family-item').length;
		var html = $('#family-html-modal').html();
		var diff = count - curr_count;
		if (diff > 0) {			//从最后增加记录
			for (var i=0; i<(diff); i++) {
				$('#family-container').append(html);
			}
			
		} else if (diff < 0) {	//从最后删除记录
			for (var i=diff; i<0; i++) {
				$('#family-container .family-item:last-child').remove();
			}
		}
		
		if ($('#family-container .family-item').length == 1) {
			$('#family-container .family-item').find('.form-label').html('家属信息');
			
		} else {
			var idx = 0;
			$('#family-container .family-item').each(function() {
				$(this).find('.form-label').html('第' + (++idx) + '名家属信息');
			});
		}
		
	});
}

//取消报名
function cancel_enroll(that) {
	frame_obj.comfirm('确定要取消报名吗？', function(){
		$('#ajax-url').val(cancel_url);
		frame_obj.do_ajax_post($(that), 'cancel_enroll', {act_id: $('#act-id').val()}, function(resp) {
			if (resp.errcode == 0) {
				frame_obj.tips('取消报名成功');
				setTimeout(function() {
					location.href = activity_url;
				}, 500);
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		});
	});
}