var page = 0;

$(document).ready(function() {
	$('#rec-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	
	$(document).click(function(evt){
		 if($(evt.toElement)[0].id == 'sort'){
			 return false;
		 }
		$("#sort_div").hide();
	});
	
	$("#search_input").unbind('input').bind('input', function() {
		page = 0;
		load_lists();
	});
});

//加载活动列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.type = $('#list_type').val();
	data.page = page;
	data.search_key = $("#search_input").val();
	data.desc = $('#sort').attr('data-sort') == 'down' ? 1 : -1;	//1降序 -1升序
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'activity_list', JSON.stringify(data), show_list);
}

//显示活动列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		var pic_html = '';
		if(item['activity_pic'] != null && item['activity_pic'] != ""){
			pic_html = '<p class="face-bar">' +
				'<span class="active-face-wrap">' +
					'<img src="'+$('#media-url').val()+item['activity_pic']+'" class="active-face" onerror="$(this).parents(\'.face-bar\').remove();">' +
				'</span>' +
			'</p>';
		}

		if (item['activity_state'] == '报名中') {
			tag_cls = 'tag-orange';
		} else if (item['activity_state'] == '即将开始') {
			tag_cls = 'tag-blue';
		} else if (item['activity_state'] == '进行中') {
			tag_cls = 'tag-green';
		} else if (item['activity_state'] == '已结束') {
			tag_cls = 'tag-gray';
		}
		html += '<div class="list-item" onclick="show_detail('+item['act_id']+');">'+
					'<div class="item-head">'+
						'<img src="'+item['public_pic']+'64" class="user-pic" onerror="this.src=\'static/image/face.png\'">'+
						'<p>'+
							'<span class="title">'+item['title']+'</span><br>'+
							'<span class="creater">发布人：'+item['public_name']+'</span>'+
							'<span class="time">'+item['public_time']+'</span>'+
						'</p>'+
					'</div>'+
					'<div class="item-body">'+
						'<span class="' + tag_cls + '">' + item['activity_state'] + '</span>' +
						pic_html+
						'<p class="abstract">'+
							item['desc']+
						'</p>'+
					'</div>'+
				'</div>';
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

//跳转到活动详情
function show_detail(id) {
	location.href = $('#detail_url').val() + '&act_id=' + id;
}

//排发布时间升序/降序
function sort_div(obj){
	var sort = $(obj).attr('data-sort');
	if(sort == 'down') {	//当前为时间降序
		$(obj).find('span').removeClass('glyphicon-arrow-down');
		$(obj).find('span').addClass('glyphicon-arrow-up');
		$(obj).attr('data-sort', 'up');
		
	} else {				//当前为时间升序
		$(obj).find('span').removeClass('glyphicon-arrow-up');
		$(obj).find('span').addClass('glyphicon-arrow-down');
		$(obj).attr('data-sort', 'down');
	}

	page = 0;
	$('#rec-list').html('');
	$('#rec-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
}