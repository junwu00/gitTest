var page = 0;

$(document).ready(function() {
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	
	auto_img_size();
	$(window).resize(function() {
		auto_img_size();
	});
});

//自计算图片大小
function auto_img_size() {
if ($('.list-item').length > 0) {
	var per_w = $('.list-item').css('width');
	per_w = Number(per_w.substr(0, per_w.length-2));
	$('.list-item').css({'height': per_w-10, 'min-height': per_w-10});
}
}

//加载报名列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.page = page;
	data.id = $('#act-id').val();
	$('#ajax-url').val(list_url);
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'enroll_list', JSON.stringify(data), show_list);
}

//显示报名列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var family_html = '';
		if (list[i].family_num > 1) {
			family_html = '<span class="family-count">' + (Number(list[i].family_num) - 1) + '</span>';
		}
		html += '<div class="list-item col-xs-2 col-sm-1 col-md-1 col-lg-1">' +
			'<img src="' + list[i].pic_url + '64" onerror="this.src=\'static/image/face.png\'">' +
			family_html +
		'</div>';
	}
	$('#rec-list').append(html);
	auto_img_size();
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}