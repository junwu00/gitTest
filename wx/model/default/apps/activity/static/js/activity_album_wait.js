var page = 0;

jsApiList = ['previewImage'];
$(document).ready(function() {
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	
	$('#do-cancel').unbind('click').bind('click', function() {
		$('#op-menu').css({display: 'none'});
	});
	
	auto_img_size();
	$(window).resize(function() {
		auto_img_size();
	});
});

//自计算图片大小
function auto_img_size() {
	if ($('.list-item').length > 0) {
		var per_w = $('.list-item').css('width');
		per_w = Number(per_w.substr(0, per_w.length-2));
		$('.list-item').css({'height': per_w-10, 'min-height': per_w-10});
	}
}

function do_cancel(){
	history.go(-1);
}

function select_list_item(that) {
	$('#op-menu').css({display: 'block'});
}

//加载图片列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.is_finish = $('.active').attr('data-state');
	data.page = page;
	data.type = $('#list_type').val();
	data.atlas_id = $('#aid').val();
	is_scroll_loading = true;
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(undefined, 'image_list', JSON.stringify(data), show_list);
}

//显示图片列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	
	var media_url = $('#media-url').val();
	var list = data.info.data;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	if(list != '') {
		for (var i in list) {
			var image = list[i];
			html += '<div class="list-item col-xs-4 col-sm-3 col-md-3 col-lg-2">'+
						'<img src="'+media_url+image['url']+'" onclick="image_pre.init_pre(\'' + image.ext + '\', \'' + image.key + '\');">'+
						'<input type="checkbox" value="'+image['id']+'" class="ck2 wait-img" data-id="' + image.image_id + '">'+
					'</div>';
		}
	}
	$('#rec-list').append(html);
	auto_img_size();
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

//通过图片
function do_pass(that) {
	var arr = new Array();
	$('.wait-img').each(function() {
		if ($(this)[0].checked) {
			arr.push($(this).attr('data-id'));
		}
	});
	if (arr.length == 0) {
		frame_obj.tips('未选择图片');
		return;
	}
	
	var data = new Object();
	data.act_id = $('#act-id').val();
	data.image_id = arr;
	$('#ajax-url').val($('#post_url').val());
	frame_obj.do_ajax_post(undefined, 'approval_image', JSON.stringify(data), function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('操作成功');
			setTimeout(function() {
				location.reload();
			}, 500);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//删除图片
function do_delete(that) {
	var arr = new Array();
	$('.wait-img').each(function() {
		if ($(this)[0].checked) {
			arr.push($(this).attr('data-id'));
		}
	});
	if (arr.length == 0) {
		frame_obj.tips('未选择图片');
		return;
	}

	frame_obj.comfirm('确定要删除所选图片吗？', function(){
		var data = new Object();
		data.act_id = $('#act-id').val();
		data.image_id = arr;
		$('#ajax-url').val($('#post_url').val());
		frame_obj.do_ajax_post(undefined, 'delete_image', JSON.stringify(data), function(resp) {
			if (resp.errcode == 0) {
				frame_obj.tips('操作成功');
				setTimeout(function() {
					location.reload();
				}, 500);
				
			} else {
				frame_obj.tips(resp.errmsg);
			}
		});
	});
}