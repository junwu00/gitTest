var page = 0;
var img_file_name = '';
var img_list = [];	//相册列表
var req_url = $('#ajax-url').val();

$(document).ready(function() {
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	init_uploader();

	auto_img_size();
	$(window).resize(function() {
		auto_img_size();
	});
});

//自计算图片大小
function auto_img_size() {
	if ($('.list-item').length > 0) {
		var per_w = $('.list-item').css('width');
		per_w = Number(per_w.substr(0, per_w.length-2));
		$('.list-item').css({'height': per_w-10, 'min-height': per_w-10});
	}
}

//加载图片列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.type = $('#list_type').val();
	data.atlas_id = $('#atlas_id').val();
	data.in_time = 1;
	data.page = page;
	is_scroll_loading = true;
	$('#ajax-url').val(req_url)
	frame_obj.lock_screen('图片加载中');
	frame_obj.do_ajax_post(undefined, 'image_list', JSON.stringify(data), show_list);
}

//显示图片列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}

	var media_url = $('#media-url').val();
	var list = data.info.data;
	scroll_load_complete('#rec-list', list.length || Object.keys(list).length);	//in scroll-load.html  
	
	if (data.info.atlas_pic != null && data.info.atlas_pic != '') {
		$('#face-img-bar > img').attr('src', media_url + data.info.atlas_pic);
	}
	$('#face-img-bar > span').html(data.info.title);
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	if(list != '')
		for (var i in list) {
			var image_list = list[i];
			var image_html = '';
			for(var j in image_list){
				var image = image_list[j];
				img_list.push(media_url + image['url']);
				image_html += '<div class="list-item col-xs-3 col-sm-3 col-md-2 col-lg-2" onclick="activity_album_preview(this);" data-group="' + i + '">'+
									'<img src="'+media_url+image['url']+'">'+
							  '</div>';
			}
			if($('div[data-time='+i+']').html()){
				$('div[data-time='+i+']').append(image_html);
				image_html = '';
				
			}else{
				html += '<div data-time="'+i+'"><h4 class="col-xs-12">'+i+'</h4>'+image_html+'</div>';
				image_html = '';
			}
		}
	$('#rec-list').append(html);
	$('#rec-list > div').each(function() {
		var cnt = $(this).find('.list-item').length;
		$(this).find('h4').html($(this).attr('data-time') + '<span>' + cnt + '张</span>');
	});
	auto_img_size();
	
	if ($('.list-item').length == 0) {
		var ext_html = '<img src="' + empty_ext_img_url + '"/>'
		has_no_record_2('#rec-list', empty_img_url, '<font style="font-size: 18px">相册暂无图片</font><br>您可以点击下面按钮上传图片或者<br>进入我的相册审核已上传的图片', ext_html);
	}
	frame_obj.unlock_screen();
}

//初始化上传控件
function init_uploader() {
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	var opt = {
		 media_url		: media_url,
		 upload_url		: jssdk_upload_url,
		 per_count		: 9,
		 per_success	: function(data, idx, total) {
			 insert_img(data, idx, total);
		 },
		 per_fail		: function(data, idx, total) {
			 //暂不处理（因为取不到文件名）
		 },
		 upload_success : function(data) {},
		 upload_success_part : function(data, fail_cnt) {}
	};
	$('#file_upload_file').wximgupload(opt);
}

//保存图片到相册中（未审核）
function insert_img(file_info, idx, total) {
	var data = new Object();
	var file = new Object();
	file.file_url = file_info.path;
	file.file_name = file_info.name;
	file.file_key = file_info.hash;
	file.file_ext = file_info.ext;
	data.file = file;
	data.atlas_id = $('#atlas_id').val();
	
	$('#ajax-url').val(req_url + '&t=' + new Date().getTime());
	frame_obj.do_ajax_post(undefined, 'upload_image', JSON.stringify(data), function(resp) {
		if (resp.errcode == 0) {
			if (idx <= total) {
				frame_obj.lock_screen('上传中 ' + idx + '/' + total);
			}
			if (idx == total) {
				setTimeout(function() {
					frame_obj.tips('图片上传成功，待管理员审核通过，即会放入相册');
				}, 1000);
			}
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//预览图片
function activity_album_preview(that) {
	var curr_url = $(that).find('img').attr('src');
	jssdk_obj.preview_img(curr_url, img_list);
}