var img_file_name = '';
var map_inited = false;

$(document).ready(function() {
	init_scope();
	init_contact_selector();
	
	init_custom_field(init_custom_selected);
	
	init_datetime_scroll('#start-time');
	init_datetime_scroll('#end-time');
	init_datetime_scroll('#apply-end-time');
	init_place_sel();
	init_uploader();
	init_sign_sel();
	init_family_sel();
});

//初始化选中参与人
function init_scope() {
	var html_val = '';
	var id_val = '';
	if ($('#rec-scope-dept option').length > 0) {
		$('#rec-scope-dept option').each(function() {
			html_val += '; ' + $(this).html();
			id_val += ',' + $(this).val();
		});
	}
	if ($('#rec-scope-user option').length > 0) {
		$('#rec-scope-user option').each(function() {
			html_val += '; ' + $(this).html();
			id_val += ',' + $(this).val();
		});
	}
	if (html_val != '')	html_val = html_val.substr(2)
	else html_val = '请选择';
	if (id_val != '')	id_val = id_val.substr(1);
	$('#scope').html(html_val);
	$('#scope').attr('data-scope', id_val);
}

//初始化选中的自定义字段
function init_custom_selected() {
	if ($('#rec-custom option').length > 0) {
		var html = '';
		$('#rec-custom option').each(function() {
			var val = $(this).val();
			$('#custom-menu input[type=checkbox]').each(function() {
				if ($(this).attr('data-name') == val) {
					$(this)[0].checked = true;
					html += '<div class="form-line custom-item" data-name="' + val + '">' +
						'<i class="icon-minus" onclick="cancel_sel_custom(this);"></i>' +
						'<span>' + val + '</span>' +
					'</div>';
				}
			});
		});
		$('.custom-item').remove();
		$('#add-custom').before(html);
	}
}

//初始化自定义报名字段
function init_custom_field(callback) {
	$('#ajax-url').val(custom_field_url);
	frame_obj.do_ajax_post(undefined, 'get_enroll_custom', {}, function(resp) {
		if (resp.errcode == 0) {
			var html = '';
			for (var i in resp.list) {
				html += '<label><input type="checkbox" class="ck2" data-name="' + resp.list[i] + '"><span>' + resp.list[i] + '</span></label>';
			}
			html += '<label id="custom-ok">确定</label>';
			$('#custom-menu').html(html);
			$('#custom-menu').attr('data-bottom', -(40 * resp.list.length));
			$('#custom-menu').css({bottom: -(40 * resp.list.length)});
			init_add_custom_field();
			if (typeof(callback) === 'function') {
				callback();
			}
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//初始化添加自定义报名字段事件
function init_add_custom_field() {
	$('#add-custom').unbind('click').bind('click', function() {
		$('#custom-menu-wrap').css({display: 'block'});
		$('#custom-menu').animate({bottom: 0});
		$('html, body').css({overflow: 'hidden'});
	});
	$('#custom-ok').unbind('click').bind('click', function() {
		var html = '';
		$('#custom-menu input[type=checkbox]').each(function() {
			if ($(this)[0].checked) {
				html += '<div class="form-line custom-item" data-name="' + $(this).attr('data-name') + '">' +
					'<i class="icon-minus" onclick="cancel_sel_custom(this);"></i>' +
					'<span>' + $(this).attr('data-name') + '</span>' +
				'</div>';
			}
		});
		
		$('.custom-item').remove();
		$('#add-custom').before(html);
		$('#custom-menu').animate({bottom: $('#custom-menu').attr('data-bottom')}, 'normal', function() {
			$('#custom-menu-wrap').css({display: 'none'});
			$('html, body').css({overflow: 'auto'});
		});
	});
}

//取消某个自定义报名字段
function cancel_sel_custom(that) {
	var name = $(that).parents('.custom-item').attr('data-name');
	$(that).parents('.custom-item').remove();
	$('#custom-menu input[type=checkbox][data-name=' + name + ']')[0].checked = false;
}

//初始化签到按钮事件
function init_sign_sel() {
	$('#is-sign').unbind('change').bind('change', function() {
		if ($(this)[0].checked) {
			$('#sign-warn-time').parents('.form-line').removeClass('hide');
			
		} else {
			$('#sign-warn-time').parents('.form-line').addClass('hide');
			$('#sign-warn-time').val(0);
		}
	});
}

//初始化家属按钮事件
function init_family_sel() {
	$('#is-family').unbind('change').bind('change', function() {
		if ($(this)[0].checked) {
			$('#family-max-count').parents('.form-line').removeClass('hide');
			
		} else {
			$('#family-max-count').parents('.form-line').addClass('hide');
			$('#family-max-count').val('');
		}
	});
}

//初始化上传控件
function init_uploader() {
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: unify_check_url,
		uploadUrl	: unify_upload_url,
		type		: ['jpg', 'jpeg', 'png'],
		scaning		: function(filePath, fileName, isImg) {
			$('#uploader-msg-wrap').html('扫描中...');
			$('#uploader-msg-wrap').removeClass('hide');
			img_file_name = fileName;
			return true;
		},
		scaned		: function() {
			$('#uploader-msg-wrap').html('扫描完成');
			$('#uploader-msg-wrap').removeClass('hide');
		},
		preview		: function(fileName, url) {},
		startUpload	: function() {
			$('#uploader-msg-wrap').html('上传中...');
			$('#uploader-msg-wrap').removeClass('hide');
		},
		process		: function(percent) {},
		success		: function(data, isImg) {
			$('#uploader-msg-wrap').html('上传完成');
			setTimeout(function() {
				$('#uploader-msg-wrap').addClass('hide');
			}, 500);
			
			$('#face-img-wrap .face-img').remove();
			var min_img = '<span class="face-img" onclick="javascript:face_preview(\'' + data.ext + '\',\'' + data.hash + '\');">' +
				'<img src="' + mediaUrl + data.path + '" data-key="' + data.hash + '" data-url="' + data.path + '" data-name="' + img_file_name + '" data-ext="' + data.ext + '">' +
				'<i class="icon-remove" onclick="javascript:mv_face_img(event, this);"></i>' +
			'</span>';
			$('#face-img-wrap .face-img-uploader-wrap').before(min_img);

        	$('#file_upload_file').val("");
		},
		error		: function(resp) {
			$('#uploader-msg-wrap').html('');
			$('#uploader-msg-wrap').addClass('hide');
			
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});
}

//预览封面图片
function face_preview(ext, hash) {
	image_pre.init_pre(ext, hash);
}

//初始化(多个)选人控件
function init_contact_selector() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		var selected_ids = $('#scope').attr('data-scope').split(',');
		var selected_name = [];
		if ($('#scope').val() != '请选择') {
			selected_name = $('#scope').html().split(';');
		}
		for (var i in selected_ids) {
			if ($.trim(selected_ids[i]) == '')	continue;
			
			selected[selected_ids[i]] = {id: selected_ids[i], name: $.trim(selected_name[i])};
		}

		var instance_id = $(this).attr("id");
		var opt = {
			selected	: selected,
			before		: function() {
			},
			ok			: function(selected) {
				var name_str = '';
				var ids = '';
				for (var i in selected) {
					if (selected[i] == undefined)	continue;
					
					name_str += '; ' + selected[i].name;
					ids += ',' + selected[i].id;
				}
				if (name_str == '') {
					$('#scope').html('请选择');
					$('#scope').attr('data-scope', '');
					
				} else {
					name_str = name_str.substr(2);
					ids = ids.substr(1);
					$('#scope').html(name_str);
					$('#scope').attr('data-scope', ids);
				}
				
				$(c_search_form).css({display: 'block'});
				$('#new-bottom-menu').css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
			}
		}
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#new-bottom-menu').css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

//删除封面图
function mv_face_img(evt, that) {
	$(that).parents('.face-img').remove();
	evt.stopPropagation();
}

//初始化时间选择器
function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	var curr_val = $(element).val();
	$(element).val(curr_val).scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

//初始化地点选择器
function init_place_sel() {
	$('#place').unbind('focus').bind('focus', function() {
		set_title_val($('#place').val());
		show_map();
		init_map();
	});
	
	$('#map-ok').unbind('click').bind('click', function() {
		curr_point = $('#bd-map').gdmap('getCenter');
		$('#place').val($('#map-title-text').val());
		$('#place').attr('data-lng', curr_point.lng);
		$('#place').attr('data-lat', curr_point.lat);
		$('#place').blur();
		hide_map();
	});
	
	$('#map-back').unbind('click').bind('click', function() {
		$('#place').blur();
		hide_map();
	});
}

//显示地图
function show_map() {
	$('#main-panel').addClass('hide');
	$('#powerby').addClass('hide');
	$('#new-bottom-menu').addClass('hide');
	
	$('#main-map').removeClass('hide');
}

//隐藏地图
function hide_map() {
	$('#main-panel').removeClass('hide');
	$('#powerby').removeClass('hide');
	$('#new-bottom-menu').removeClass('hide');
	
	$('#main-map').addClass('hide');
}

//初始化地图
function init_map() {
	$('#bd-map').css('height', $(window).height());
	if (!map_inited) {
		map_inited = true;
		$('#bd-map').gdmap({
			zoom:				14,
			controller:			-1,
			defMarkerIcon:		$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',
			defMarkerIconSize:	[21,37],
			centerIcon:			$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',		//中心图片ICON
			centerIconSize:		[21,37],
			searchInputEl:		$('#suggest_id'),																	//搜索输入框
			searchListPanel:	$('#search_result_panel'),															//搜索结果显示页面
			searchBtn:			$('#map-search'),
			afterMapLoaded:		function() {
				$('#bd-map').gdmap('initSearch');
				$('#bd-map').gdmap('setCenter', curr_point);
			},
			afterSetCenter:		function() {
				var center = $('#bd-map').gdmap('getCenter');
				if (curr_point == undefined || center.lng != curr_point.lng || center.lat != curr_point.lat) {
					$('#bd-map').gdmap('getInfoByPosition', undefined, function(addrInfo) {
						set_title_val(addrInfo.regeocode.formattedAddress);
					});
				}
			}
		});
	} else {
		$('#bd-map').gdmap('setCenter', curr_point);
	}
}

function set_title_val(val) {
	$('#map-title-text').val(val);
}

//通用保存/发布调用方法
function _do_save(that, is_public, is_warn, success_msg) {
	var data = get_apply_data();
	if (!check_apply_data(data)) return;
	
	data.state = is_public ? 1 : 0;	//是否发布
	data.remind = is_warn ? 1 : 0;	//是否提醒
	data.id = $('#activity-id').val();
	
	$('#ajax-url').val(save_url);
	frame_obj.do_ajax_post($(that), 'save_activity', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips(success_msg);
			setTimeout(function() {
				location.href = list_url;
			}, 500);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//保存
function do_save(that) {
	_do_save(that, false, false, '保存成功');
}

//发布
function do_public(that) {
	_do_save(that, true, false, '发布成功');
}

//发布并提醒
function do_public_and_warn(that) {
	_do_save(that, true, true, '发布并提醒成功');
}

//更新（活动已发布）
function do_update(that) {
	_do_save(that, false, false, '更新成功');
}

//获取活动数据
function get_apply_data() {
	var data = new Object();
	data.title = $('#title').val();
	data.desc = $('#abstract').val();
	if ($('.face-img').length > 0) {	//有封面
		data.face_pic = new Object();
		data.face_pic.file_key = $('.face-img img').attr('data-key');
		data.face_pic.file_url = $('.face-img img').attr('data-url');
		data.face_pic.file_name = $('.face-img img').attr('data-name');
		data.face_pic.ext = $('.face-img img').attr('data-ext');
	}
	data.start_time = $('#start-time').val();
	data.end_time = $('#end-time').val();
	data.enroll_time = $('#apply-end-time').val();
	data.remind_type = $('#warn-time').val();
	data.place = $('#place').val();
	data.location = new Object();
	data.location.lng = $('#place').attr('data-lng');
	data.location.lat = $('#place').attr('data-lat');
	
	var scope = $('#scope').attr('data-scope');
	data.dept = new Array();
	data.user = new Array();
	if (scope != '') {
		var scope_list = scope.split(',');
		for (var i in scope_list) {
			var item = scope_list[i];
			if (item.length > 2 && item.substr(0, 2) == 'd-') {	//部门
				data.dept.push(item.substring(2));
			} else {											//人员
				data.user.push(item);
			}
		}
	}
	var custom = new Array();
	$('.custom-item').each(function() {
		custom.push($(this).attr('data-name'));
	});
	data.is_enroll_msg = (custom.length > 0) ? 1 : 0;
	data.enroll_custom = custom;
	
	data.limited = $('#limit').val();
	data.is_sign = $('#is-sign')[0].checked ? 1 : 0;
	data.sign_remind_type = $('#sign-warn-time').val();

	data.can_family = $('#is-family')[0].checked ? 1 : 0;
	data.family_limited = $('#family-max-count').val();
	return data;
}

//验证活动数据是否合法
function check_apply_data(data) {
	if (!check_data(data.title, 'notnull')) {
		frame_obj.tips('请输入活动标题');	return false;
	}
	if (!check_data(data.desc, 'notnull')) {
		frame_obj.tips('请输入活动详情');	return false;
	}
	if (!check_data(data.start_time, 'notnull')) {
		frame_obj.tips('请选择活动开始时间');	return false;
	}
	if (!check_data(data.end_time, 'notnull')) {
		frame_obj.tips('请选择流程结束时间');	return false;
	}
	if (!check_data(data.enroll_time, 'notnull')) {
		frame_obj.tips('请选择报名截止时间');	return false;
	}
	if (!check_data(data.place, 'notnull') || !check_data(data.location.lng, 'notnull') || !check_data(data.location.lat, 'notnull')) {
		frame_obj.tips('请选择活动地点');	return false;
	}
	if (data.dept.length == 0 && data.user.length == 0) {
		frame_obj.tips('请选择参与对象');	return false;
	}
	if (!check_data(data.limited, 'notnull')) {
		frame_obj.tips('请填写参与人数上限');	return false;
	}
	if (data.limited <= 0) {
		frame_obj.tips('参与人数上限必须大于0');	return false;
	}
	if (data.can_family == 1 && data.family_limited <= 0) {
		frame_obj.tips('携带家属人数必须大于0');	return false;
	}
	return true;
}