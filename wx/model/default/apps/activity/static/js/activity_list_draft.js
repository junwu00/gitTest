var page = 0;
var curr_activity = 0;
var curr_activity_bar;

$(document).ready(function() {
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	
	$('#do-cancel').unbind('click').bind('click', function() {
		$('#op-menu').css({display: 'none'});
	});
});


function select_list_item(that) {
	curr_activity = $(that).attr('data-id');
	curr_activity_bar = $(that);
	$('#op-menu').css({display: 'block'});
}

//加载活动列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.type = $('#list_type').val();
	data.page = page;
	data.search_key = $("#search_input").val();
	data.desc =$("#sort_val").val();
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'activity_list', JSON.stringify(data), show_list);
}

//显示活动列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		html += '<div class="list-item" onclick="javascript:select_list_item(this);" data-id="' + item['act_id'] + '">' +
					'<div class="item-head">' +
					'<p>' +
						'<span class="title">' + item['title'] + '</span>' +
						'<span class="time">' + item['public_time'] + '</span>' +
					'</p>' +
					'<i class="icon-angle-down"></i>' +
				'</div>' +
				'<div class="item-body">' +
					item['desc'] +
				'</div>' +
			'</div>';
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

//发布活动
function do_public() {
	var data = {
		act_id: curr_activity,
		state: 1
	}
	frame_obj.do_ajax_post(undefined, 'activity_state', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('发布成功');
			setTimeout(function() {
				$('#op-menu').css({display: 'none'});
				$(curr_activity_bar).remove();
			}, 500);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

//编辑活动
function do_edit() {
	var target_url = edit_url + '&act_id=' + curr_activity;
	location.href = target_url;
}

//删除活动
function do_delete() {
	var data = {
		act_id: curr_activity,
		state: 3
	}
	frame_obj.do_ajax_post(undefined, 'activity_state', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('删除成功');
			setTimeout(function() {
				$('#op-menu').css({display: 'none'});
				$(curr_activity_bar).remove();
			}, 500);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}