var page = 0;
var type = 0;			//0未签到/1已签到
var sign_list = [];		//相册列表
var unsign_list = [];	//相册列表

$(document).ready(function() {
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
	init_change_tabs();
	
	$('.container').on('scroll', function(){
		var scroll_top = $('.container')[0].scrollTop;
		if (scroll_top == 0) {	//回到顶部，显示顶部，tabs取消fixed
			$('#title-bar').removeClass('title-bar-fixed');
//			$('#title-bar').css({top: 0});
//			$('#rec-list').css({'padding-top' : '132px'});
			
		} else {				//滚动加载，隐藏顶部，tabs启用fixed
			$('#title-bar').addClass('title-bar-fixed');
//			$('#title-bar').css({top: '-73px'});
//			$('#rec-list').css({'padding-top' : '50px'});
		}
	});
});

//切换签到/未签到的tab事件
function init_change_tabs() {
	$('#type-tabs .type-tabs-item').each(function() {
		$(this).on('click', function() {
			$('#type-tabs .type-tabs-item').removeClass('active');
			$(this).addClass('active');
			
			var this_type = $(this).attr('data-type');
			page = 0;
			type = this_type;
			$('#rec-list').html('');
			load_lists();
		});
	});
}


//加载签到/未签到列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.type = $('#list_type').val();
	data.atlas_id = $('#atlas_id').val();
	data.in_time = 1;
	data.page = page;
	is_scroll_loading = true;
	var load_opt = {
		url:		list_url,
		ajax_act:	'get_sign_list',
		data: 		{act_id: act_id, page: page, type: type},
		msg:		page == 1 ? '加载中...' : '',
		success:	show_list,
	};
	frame_obj.post(load_opt);
}

//显示图片列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.tips(data.errmsg);
		return
	}
	
	var total = data.info.count;
	
	var html = '';
	if (total == 0) {
		html = type == 0 ? '全部成员已签到完成' : '目前还未有成员签到';
		if (Number($('#enroll-total-cnt').html()) == 0) {
			html = '该活动暂无成员报名';
		}
		html = '<span class="sign-info-empty">' + html + '</span>';
		
	} else {
		var list = data.info.data;
		scroll_load_complete('#rec-list', list.length || Object.keys(list).length);
		list = list || [];
		if (type == 0) {	//未签到
			for (var i in list) {
				var item = list[i];
				var family_html = item.family_num > 1 ? '<span class="unsign-info-item-cnt">' + (Number(item.family_num) - 1) + '</span>' : '';
				html += '<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1 unsign-info-item">' +
				'<img src="' + item.pic_url + '64" onerror="this.src=\'static/image/face.png\'">' +
				family_html + '<br>' +
				'<span class="unsign-info-item-name">' + item.name + '</span>' +
				'</div>';
			}
			
		} else {			//已签到
			for (var i in list) {
				var item = list[i];
				var family_html = item.family_num > 1 ? '<span class="sign-info-item-cnt">' + (Number(item.family_num) - 1) + '</span>' : '';
				var distance = parseInt(item.distance);
				if (distance > 1000000) {
					distance = '超出范围';
				} else if (distance > 1000) {
					distance = parseInt(distance / 100) / 10 + '千米';
				} else {
					distance = distance + '米';
				}
				html += '<div class="sign-info-item">' +
				'<span class="sign-info-item-img">' +
				'<img src="' + item.pic_url + '64" onerror="this.src=\'static/image/face.png\'">' +
				family_html +
				'</span>' +
				'<span class="sign-info-item-middle">' +
				'<span class="sign-info-item-name">' +
				item.name + ' <span class="sign-info-item-status">(已签到)</span>' +
				'</span><br>' +
				'<span class="sign-info-item-time">' + item.sign_time + '</span>' +
				'</span>' +
				'<span class="sign-info-item-distance">' +
				'<span class="sign-info-item-meter">' + distance + '</span><br>' +
				'<span class="sign-info-item-meter-desc">距离活动地点</span>' +
				'</span>' +
				'</div>';
			}
		}
	}

	$('#rec-list').append(html);
}