<?php
/**
 * 组织活动
 * 
 * @author yangpz
 * @date 2014-10-21
 *
 */
class cls_wxser extends abs_app_wxser {
	
	//--------------------------------------内部实现---------------------------

	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『组织活动』操作指引',
		'desc' => '欢迎使用组织活动应用，在这里你可以发起活动，组织同事一起娱乐，一起运动，给无趣的生活增添一丝有趣的经历！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=6&sn=7289e587de51c2cf1141a7756c6db3ba#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
}

// end of file