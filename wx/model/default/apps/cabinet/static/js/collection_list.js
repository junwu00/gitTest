var page = 0;
$(document).ready(function(){
	load_list();
	init_scroll_load('.container', '#search-item', load_list);
	init_search();
});

function load_list(){
	page += 1;
	var data = new Object();
	data.page = page;
	$('#ajax-url').val($('#ajax_collection_list').val() + '&page=' + page);
	frame_obj.do_ajax_post(undefined, '', JSON.stringify(data), show_list);
}

function show_list(data){
	var type = $('#type').val(); 
	if(data['errcode']!=0){
		frame_obj.alert(data['errmsg']);
		return;
	}
	var file_list = data.info;
	var file_html = '';
	scroll_load_complete('#search-item', file_list.length);
	for(var i = 0; i < file_list.length; i++){
		var file = file_list[i];
		var name = file.name;
		var ext = file.extname;
		var full_name = name + '.' + ext;
		var id = file.cf_id;
		var dl_id = file.dl_id || file.cf_id;
		var update_time = time2date(file.create_time);
		var filesize = file.filesize/1024/1024 > 1 ? ((file.filesize/1024/1024).toFixed(2)+'Mb'):((file.filesize/1024).toFixed(2)+'Kb');
		var icon_class = get_icon_class(ext);
		var is_new = file.is_new == 1? true :false;
		file_html += '<div class="file_cabinet" onclick="file_detail(' + id + ')">';
		if(is_new){
			file_html += '<div class="file_cabinet_text file_cabinet_text_new ' + icon_class + '">';
			file_html += '<p><span class="file_cabeinet_text_name">' + full_name + '<span class="new_icon"></span></span></p>';
		}else{
			file_html += '<div class="file_cabinet_text ' + icon_class + '">';
			file_html += '<p>' + full_name + '</p>';
		}
		file_html += '<p class="cabinet_time">' + update_time + '<span style="float: right;">' + filesize + '</span></p>';
		file_html += '<div class="clear"></div>';
		file_html += '<span class="glyphicon glyphicon-trash remove_icon" onclick="remove_collection(' + dl_id + ')"></span>';
		file_html += '</div></div>';
	}
	$('#search-item').append(file_html);
	if(is_scroll_end == true && $.trim($('#search-item').html()).length == 0){
		var empty_html = '';
		empty_html += '<div class="empty_data">';
		empty_html += '<img alt="" src="' + $('#app_static').val() + 'images/new/file_collection_nothing_1.png">';
		empty_html += '<p class="empty_title">暂无收藏文档</p>';
		empty_html += '<p class="empty_desc">文档收藏，可以快速查看你的常用文档，进入</p>';
		empty_html += '<p class="empty_desc">文件详情收藏你的常用文档信息</p>';
		empty_html += '</div>';
		$('#search-item').append(empty_html);
	}
}

function remove_collection(id){
	var type = $('#type').val();
	var actionDesc = type == 1 ? '取消收藏……':'删除下载记录';
	event.stopPropagation();
	var tip = $('#del_tip').val();
	frame_obj.comfirm(tip,function(){
		$('#ajax-url').val($('#uncollect_file').val()+ '&id=' + id);
		frame_obj.do_ajax_post(undefined, '', {}, function(data){
			if(data['errcode']!=0){
				frame_obj.alert(data['errmsg']);
				return;
			}
			page = 0;
			$('#search-item').html('');
			load_list();
		},undefined,undefined,undefined,actionDesc);
	});
}

function file_detail(id){
	location.href=$('#file_url').val() + '&id=' + id;
}

function search(){
	var search_key = $.trim($('#search_key').val());
	if(search_key.length == 0){
		return false;
	}
	location.href=$('#search_url').val() + '&search_key=' + search_key;
	return false;
}

function remove_search_key(){
	$('#search_key').val('');
}

function init_search(){
	$("#search_key").unbind('input').bind('input', function() {
		if($.trim($(this).val()).length > 0){
			$('.remove_search_key').removeClass('hide');
		}else{
			$('.remove_search_key').addClass('hide');
		}
	});
}

function get_icon_class(ext){
	var c ='';
	if(ext == 'mp4'){
		c = 'file_cabinet_mp4';
	}else if(ext == 'doc' || ext == 'docx'){
		c = 'file_cabinet_doc';
	}else if(ext == 'pdf'){
		c = 'file_cabinet_pdf';
	}else if(ext == 'ppt' || ext == 'pptx'){
		c = 'file_cabinet_ppt';
	}else if(ext == 'xls' || ext == 'xlsx'){
		c = 'file_cabinet_xls';
	}else if(ext == 'txt'){
		c = 'file_cabinet_txt';
	}else if(ext == 'zip' || ext == 'rar'){
		c = 'file_cabinet_zip';
	}else if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){
		c = 'file_cabinet_img';
	}else if(ext == 'mp3' || ext == 'wma'|| ext == 'wav'|| ext == 'ram'){
		c = 'file_cabinet_mp3';
	}else{
		c = 'file_cabinet_others';
	}
	return c;
}