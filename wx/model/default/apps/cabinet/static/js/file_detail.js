$(document).ready(function(){
	init_file();
});

function init_file(){
	$('#ajax-url').val($('#ajax_file_info').val() + '&id=' + $('#id').val());
	var data = new Object();
	data.id = $('#id').val();
	frame_obj.do_ajax_post(undefined, '', JSON.stringify(data), function(data){
		if(data['errcode']!=0){
			frame_obj.alert(data['errmsg'],undefined,function(){
				location.href = $('#root_url').val();
			});
			return;
		}
		
		var file = data.info.rec;
		var name = file.name;
		var ext = file.extname;
		var full_name = name + '.' + ext;
		var mask = file.mark;
		var create_time = time2date(file.create_time,'yyyy-MM-dd HH:mm');
		var update_time = time2date(file.update_time,'yyyy-MM-dd HH:mm');
		var collection_count = Number(file.count);
		var download_count = Number(file.downloads);
		var hash = file.hash;
		var filesize = file.filesize/1024/1024 > 1 ? ((file.filesize/1024/1024).toFixed(2)+'Mb'):((file.filesize/1024).toFixed(2)+'Kb');
		var icon_class = get_icon_class(ext);
		var is_new = file.is_new == 1? true :false;
		var is_collected = file.collected;
		var dload_able = file.dload_able;
		if(is_collected){
			$('.file_action_uncollection').removeClass('hide');
			$('.file_action_collection').addClass('hide');
		}else{
			$('.file_action_collection').removeClass('hide');
			$('.file_action_uncollection').addClass('hide');
		}
		if(is_new){
			full_name  += '<span class="new_icon"></span>';
			$('.file_name').html(full_name);
			$('.file_name').addClass('file_name_new');
		}else{
			$('.file_name').html(full_name);
		}
		if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){
			$('.file_action_down_img').removeClass('hide');
			$('.file_action_down').addClass('hide');
		}else{
			$('.file_action_down').removeClass('hide');
			$('.file_action_down_img').addClass('hide');
		}
		$('.file_name').html(full_name);
		$('.file_icon').addClass(icon_class);
		$('#file_size').html(filesize);
		$('.file_msg_mask').html(mask);
		$('.file_msg_creat_time').html(create_time);
		$('.file_msg_update_time').html(update_time);
		$('#down_count').html(download_count);
		$('#collect_count').html(collection_count);
		if(MS_document_pre.is_pre(ext)){
			$('.file_pre').removeClass('hide');
			$('.file_pre').attr('data-ext',file.extname);
			$('.file_pre').attr('data-hash',file.hash);
			$('.file_pre').unbind().bind('click',function(){
				MS_document_pre.init_pre($(this).attr('data-ext'),$(this).attr('data-hash'), full_name);
			})
		}else if(image_pre.is_pre(ext)){
			$('.file_pre').removeClass('hide');
			$('.file_pre').attr('data-ext',file.extname);
			$('.file_pre').attr('data-hash',file.hash);
			$('.file_pre').unbind().bind('click',function(){
				image_pre.init_pre($(this).attr('data-ext'),$(this).attr('data-hash'));
			})
		}else{
			$('.file_pre').addClass('hide');
		}
		if(dload_able != 1){
			$('.file_action_down_img').remove();
			$('.file_action_down').remove();
		}
		$('.file_detail').removeClass('hide');
	},undefined,undefined,undefined,'加载中……');
}

function ios_down_replace_pre(){
	var id = $("#id").val();
	$.ajax({
		url : $('#download_url').val()+ '&id=' + id,
		type : 'post',
		data : '',	
		dataType : 'json',
		success : function(data){
			if(data.errcode != 0){
				frame_obj.alert(data.errmsg);
				return;
			}
		},
		error : function(data, status, e){
			location.href =  $('#download_url').val()+ '&id=' + id;
		}
	});
}

function file_collection(){
	$('.file_action_collection').addClass('hide');
	$('.file_action_collectioning').removeClass('hide');
	var id = $("#id").val();
	$('#ajax-url').val($('#collect_file').val()+ '&id=' + id);
	frame_obj.do_ajax_post(undefined, '', {}, function(data){
		$('.file_action_collectioning').addClass('hide');
		if(data['errcode'] != 0){
			frame_obj.alert(data['errmsg']);
			$('.file_action_collection').removeClass('hide');
			return;
		}
		$('.file_action_uncollection').removeClass('hide');
		$('#collect_count').html(Number($('#collect_count').html()) + 1);
	});
}

function file_uncollection(){
	$('.file_action_uncollection').addClass('hide');
	$('.file_action_collectioning').removeClass('hide');
	var id = $("#id").val();
	$('#ajax-url').val($('#uncollect_file').val()+ '&id=' + id);
	frame_obj.do_ajax_post(undefined, '', {}, function(data){
		$('.file_action_collectioning').addClass('hide');
		if(data['errcode'] != 0){
			frame_obj.alert(data['errmsg']);
			$('.file_action_uncollection').removeClass('hide');
			return;
		}
		$('.file_action_collection').removeClass('hide');
		$('#collect_count').html(Number($('#collect_count').html()) - 1);
	});
}

function file_down(){
	var id = $("#id").val();
	var url = '';
	if($('#plat_id').val() == 1){
		url = $('#download_url').val()+ '&increase=1&is_push=1&id=' + id;
	}else{
		url = $('#download_url').val()+ '&increase=1&id=' + id;
	}
	frame_obj.lock_screen('下载中……');
	$.ajax({
		url : url,
		type : 'post',
		data : '',	
		dataType : 'json',
		success : function(data){
			frame_obj.unlock_screen();
			if(data.errcode != 0){
				frame_obj.alert(data.errmsg);
				return;
			}
			frame_obj.alert('附件已成功推送到信息框！');
			$('#down_count').html(Number($('#down_count').html()) + 1);
			close_download_choose();
		},
		error : function(data, status, e){
			frame_obj.unlock_screen();
			$('#down_count').html(Number($('#down_count').html()) + 1);
			location.href =  $('#download_url').val()+ '&id=' + id;
			close_download_choose();
		}
	});
}

function file_img_down(){
	var id = $("#id").val();
	if($('#plat_id').val() == 1){
		url = $('#download_url').val()+ '&origin=1&is_push=1&id=' + id;
	}else{
		url = $('#download_url').val()+ '&origin=1&id=' + id;
	}
	frame_obj.lock_screen('下载中……');
	$.ajax({
		url : url,
		type : 'post',
		data : '',	
		dataType : 'json',
		success : function(data){
			frame_obj.unlock_screen();
			if(data.errcode != 0){
				frame_obj.alert(data.errmsg);
				return;
			}
			frame_obj.alert('附件已成功推送到信息框！');
			$('#down_count').html(Number($('#down_count').html()) + 1);
			close_download_choose();
		},
		error : function(data, status, e){
			frame_obj.unlock_screen();
			location.href =  $('#download_url').val()+ '&id=' + id;
			close_download_choose();
		}
	});
}

function get_icon_class(ext){
	var c ='';
	if(ext == 'mp4'){
		c = 'file_icon_mp4';
	}else if(ext == 'doc' || ext == 'docx'){
		c = 'file_icon_doc';
	}else if(ext == 'pdf'){
		c = 'file_icon_pdf';
	}else if(ext == 'ppt' || ext == 'pptx'){
		c = 'file_icon_ppt';
	}else if(ext == 'xls' || ext == 'xlsx'){
		c = 'file_icon_xls';
	}else if(ext == 'txt'){
		c = 'file_icon_txt';
	}else if(ext == 'zip' || ext == 'rar'){
		c = 'file_icon_zip';
	}else if(ext == 'jpg' || ext == 'jpeg' || ext == 'png'){
		c = 'file_icon_img';
	}else if(ext == 'mp3' || ext == 'wma'|| ext == 'wav'|| ext == 'ram'){
		c = 'file_icon_mp3';
	}else{
		c = 'file_icon_others';
	}
	return c;
}

function close_download_choose(){
	$('.img-download-choose-wrap').css({'display':'none'})
}

function open_download_choose(){
	$('.img-download-choose-wrap').css({'display':'block'})
}