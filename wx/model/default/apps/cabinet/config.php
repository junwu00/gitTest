<?php
/**
 * 应用的配置文件_问卷调查
 * @author zpeng
 * @date 2015-02-04
 * 
 */

return array(
	'id' 		=> 14,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(14),
	'name' 		=> 'cabinet',
	'cn_name' 	=> '南湖云盘',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'File.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '南湖云盘', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=lister', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '快捷操作', 
				'childs' => array(
					array('id' => 'bmenu-2-1', 'name' => '我的收藏',  'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=collect_list', 'icon' => ''),
					array('id' => 'bmenu-2-2', 'name' => '我的下载',  'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=download_list', 'icon' => ''),
				)
			),
		),
	),
);

// end of file