<?php
/**
 * 企业云盘消息处理类
 * 
 * @author yangpz
 * @date 2015-01-21
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『企业云盘』操作指引',
		'desc' => '欢迎使用企业云盘应用，在这里你可以随时随地打开公司的文档，想查就查，不用担心外出忘记带文件去见客户，不信你试试看！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970829&idx=2&sn=7146151398405fd87d9a725e100eadc6&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (in_array($this -> com_id, array(190,32)))	return;
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	/**
	 * 企业云盘普通消息回复入口
	 *
	 * @access protected
	 * @return void
	 */
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			if (in_array($this -> com_id, array(190,32)))	return;
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
			return;
		}

		log_write('企业云盘开始响应文本消息');
		try {
			g('cabinet') -> reinit($this -> com_id, $this -> post_data['FromUserName']);
			g('cabinet') -> discern_reply($this -> post_data['Content']);
		}catch(SCException $e) {
			log_write('企业云盘响应文本消息失败，异常：[' . $e -> getMessage() . ']');
			return;
		}
		log_write('企业云盘响应文本消息完成');
	}

	/**
	 * 企业云盘点击事件回复入口
	 *
	 * @access protected
	 * @return void
	 */
	protected function reply_click() {
		log_write('企业云盘开始回复点击事件');
		if ($this -> post_data['EventKey'] != g('cabinet') -> help_key) {
			log_write('企业云盘无匹配的按钮key，退出');
			return;
		}
		try {
			g('cabinet') -> reinit($this -> com_id, $this -> post_data['FromUserName']);
			g('cabinet') -> send_help_info();
			log_write('企业云盘成功推送帮助消息');
		}catch(SCException $e) {
			log_write('企业云盘推送帮助消息失败，异常：[' . $e -> getMessage() . ']');
			return;
		}
	}
}

// end of file