<?php
/**
 * 全部企业云盘ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2015-03-14
 */
class cls_ajax extends abs_app_base {
	//命令对应函数名的映射表
	private $cmd_map = array(
		//获取目录列表
		100 => 'get_dir_list',
		//获取文件列表
		101 => 'get_file_list',
		//获取文件详情
		102 => 'get_file_detail',
		//收藏文件
		103 => 'collect_file',
		//取消收藏文件
		104 => 'uncollect_file',
		//下载文件
		105 => 'download_file',
		//获取收藏文件列表
		106 => 'get_collected_list',
		//获取用作搜索关键字的全部文件列表
		107 => 'get_search_idx_list',
		//进行搜索，查询文件列表
		108 => 'get_search_list',
		//获取当前目录的导航路径
		109 => 'get_nav_path',
		//获取已下载文件列表
		110 => 'get_downloaded_list',
		//删除下载
		111 => 'del_download_file',
	);

	public function __construct() {
		parent::__construct('cabinet');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		empty($cmd) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$map = $this -> cmd_map;
		!isset($map[$cmd]) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$this -> $map[$cmd]();
	}

	/**
	 * 根据父目录id获取子目录列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_dir_list() {
		//父目录id，默认是 0-根目录
		$cd_id = (int)get_var_value('id');
		$page = (int)get_var_value('page');
		//默认每页10条记录
		$page_size = 10;
		$cd_id < 0 and $cd_id = 0;
		$page < 1 and $page = 1;

		//不返回页码相关信息
		$list = g('cabinet') -> get_dir_list($cd_id, array(), $page, $page_size, '', false);

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 根据目录id获取文件列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_file_list() {
		//目录id，默认是 0-根目录
		$cd_id = (int)get_var_value('id');
		$page = (int)get_var_value('page');

		//默认每页10条记录
		$page_size = 10;
		$cd_id < 0 and $cd_id = 0;
		$page < 1 and $page = 1;

		//不返回页码相关信息
		$list = g('cabinet') -> get_file_list($cd_id, array(), $page, $page_size, '', false);
		if (!empty($list)) {
			foreach ($list as &$val) {
				//今天内上传的文件标记为new
				if (date('Y-m-d', $val['create_time']) == date('Y-m-d')) {
					$val['is_new'] = 1;
				}else {
					$val['is_new'] = 0;
				}
			}
			unset($val);
		}

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 获取文件详情
	 *
	 * @access public
	 * @return void
	 */
	public function get_file_detail() {
		//文件id
		$cf_id = (int)get_var_value('id');

		if (empty($cf_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		try {
			$info = g('cabinet') -> get_file_info($cf_id);
			$info['collected'] = g('cabinet') -> check_collected($cf_id);
			$collect_count = g('cabinet') -> get_collect_count(array($cf_id));
			$info['count'] = isset($collect_count[$cf_id]) ? $collect_count[$cf_id] : 0;

			//今天内上传的文件标记为new
			if (date('Y-m-d', $info['create_time']) == date('Y-m-d')) {
				$info['is_new'] = 1;
			}else {
				$info['is_new'] = 0;
			}
		}catch(SCException $e) {
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}

		$info = array(
			'rec' => $info,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 收藏文件
	 *
	 * @access private
	 * @return void
	 */
	private function collect_file() {
		//文件id
		$cf_id = (int)get_var_value('id');

		if (empty($cf_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		parent::log('开始收藏文件');
		try {
			$result = g('cabinet') -> collect_file($cf_id);
		}catch(SCException $e) {
			parent::log('收藏文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('收藏文件成功');
		cls_resp::echo_ok();
	}

	/**
	 * 取消收藏文件
	 *
	 * @access private
	 * @return void
	 */
	private function uncollect_file() {
		//文件id
		$cf_id = (int)get_var_value('id');

		if (empty($cf_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		parent::log('开始取消收藏文件');
		try {
			$result = g('cabinet') -> uncollect_file($cf_id);
		}catch(SCException $e) {
			parent::log('取消收藏文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('取消收藏文件成功');
		cls_resp::echo_ok();
	}

	/**
	 * 下载文件
	 *
	 * @access private
	 * @return void
	 */
	private function download_file() {
		$cf_id = (int)get_var_value('id');
		$origin = (int)get_var_value('origin');
		$is_push = (int)get_var_value('is_push');
		//是否增加下载数，默认不增加
		$increase = (int)get_var_value('increase');

		if (empty($cf_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		$size = '';
		$origin and $size = '-1';
		$is_push = $is_push === 0 ? false : true;
		$increase = $increase ? true : false;

		parent::log('开始下载文件');
		try {
			$result = g('cabinet') -> download_file($cf_id, $size, $is_push, $increase);
		}catch(SCException $e) {
			parent::log('下载文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}

		parent::log('下载文件成功');
		cls_resp::echo_ok();
	}

	/**
	 * 获取收藏文件列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_collected_list() {
		$page = (int)get_var_value('page');

		//默认每页10条记录
		$page_size = 10;
		$page < 1 && $page = 1;

		$list = g('cabinet') -> get_collected_list($page, $page_size, '', false);
		if (!empty($list)) {
			foreach ($list as &$val) {
				//今天内上传的文件标记为new
				if (date('Y-m-d', $val['file_create_time']) == date('Y-m-d')) {
					$val['is_new'] = 1;
				}else {
					$val['is_new'] = 0;
				}
			}
			unset($val);
		}

		cls_resp::echo_ok(NULL, 'info', $list);
	}

	/**
	 * 获取用作搜索关键字的全部文件列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_search_idx_list() {
		$list = g('cabinet') -> get_search_list(array(), 0, 0, '', false);

		$info = array(
			'rec' => $list,
		);

		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 进行搜索，查询文件列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_search_list() {
		$key_word = get_var_value('key_word');
		$page = (int)get_var_value('page');

		//默认每页10条记录
		$page_size = 10;
		$page < 1 and $page = 1;

		if (empty($key_word)) {
			cls_resp::echo_err(cls_resp::$Busy, '关键字不能为空!');
		}

		$ext = '';
		$ext_list = g('media') -> ext_list;
		if (preg_match('/\.([^\.]+)$/', $key_word, $match)) {
			$ext = strtolower($match[1]);
			//如果是合法的扩展名，则自动分离扩展名进行搜索
			if (in_array($ext, $ext_list)) {
				$key_word = preg_replace('/\.([^\.]+)$/', '', $key_word);
			}else {
				$ext = '';
			}
		}

		$condition = array(
			'__OR_19' => array(
				'name LIKE ' => '%' . $key_word . '%',
				'extname LIKE ' => '%' . $key_word . '%',
				'name_py LIKE ' => '%' . $key_word . '%',
				'name_fpy LIKE ' => '%' . $key_word . '%',
			),
		);
		!empty($ext) and $condition['extname LIKE '] = '%' . $ext . '%';

		//不返回页码相关信息
		$list = g('cabinet') -> get_search_list($condition, $page, $page_size, '', false);
		if (!empty($list)) {
			foreach ($list as &$val) {
				//今天内上传的文件标记为new
				if (date('Y-m-d', $val['create_time']) == date('Y-m-d')) {
					$val['is_new'] = 1;
				}else {
					$val['is_new'] = 0;
				}
			}
			unset($val);
		}

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 获取当前目录的导航路径
	 * 注意：返回结果遵循顺序层级关系，如当前所在目录id为2，其层级关系为：0 -> 1 -> 2
	 *
	 * @access private
	 * @return string json字符串，参数说明如下：
	 * 	  name: 目录名称
	 * 	  cd_id: 目录id
	 * 	  cd_pid: 目录父id
	 * 	  dir_url: 进入该目录的url
	 */
	private function get_nav_path() {
		//当前的目录id
		$cd_id = (int)get_var_value('id');

		$data = array(
			array(
				'name' => '根文件夹',
				'cd_id' => '0',
			)
		);

		$list = g('cabinet') -> get_parent_by_reverse($cd_id);
		$data += $list;

		foreach ($data as &$val) {
			$val['dir_url'] = g('cabinet') -> compose_dir_url($val['cd_id']);
		}
		unset($val);

		$info = array(
			'rec' => $data,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 获取已下载文件列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_downloaded_list() {
		$page = (int)get_var_value('page');

		$page_size = 10;
		$page < 1 && $page = 1;

		$list = g('cabinet') -> get_downloaded_list($page, $page_size, '', false);

		cls_resp::echo_ok(NULL, 'info', $list);
	}

	/**
	 * 删除下载文件
	 *
	 * @access private
	 * @return void
	 */
	private function del_download_file() {
		//下载记录id
		$dl_id = (int)get_var_value('id');

		if (empty($dl_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的下载记录对象！');
		}

		parent::log('开始删除下载记录');
		try {
			$result = g('cabinet') -> del_download_file($dl_id);
		} catch (SCException $e) {
			parent::log('删除下载记录失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('删除下载记录成功');
		cls_resp::echo_ok();
	}
}

/* End of this file */