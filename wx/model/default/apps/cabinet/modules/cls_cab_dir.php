<?php
/**
 * 企业云盘目录控制器
 *
 * @author LiangJianMing
 * @create 2015-03-27
 */
class cls_cab_dir extends abs_app_base {
	public function __construct() {
		parent::__construct('cabinet');
	}

	/**
	 * 列表展示
	 *
	 * @access public
	 * @return void
	 */
	public function lister() {
		$folder_id = (int)get_var_value('id');
		$folder_name = '南湖云盘';

		if ($folder_id !== 0) {
			$info = g('cabinet') -> get_dir_info($folder_id);
			!empty($info) and $folder_name = $info['name'];
		}

		//获取目录列表接口地址
		$ajax_dir_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=100';
		//获取文件列表接口地址, 获取完全部目录后，进行文件获取
		$ajax_file_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=101';
		//获取用作搜索关键字的全部文件列表
		$ajax_search_idx_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=107';
		//进行搜索，查询文件列表
		$ajax_search_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=108';
		//获取当前目录的导航路径接口地址
		$ajax_nav_path_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=109';

		//当前url
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=lister';
		//跳转到文件详情页面的url
		$to_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=detail';
		//转跳搜索页面url
		$to_search_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=search_lister';
		
		g('smarty') -> assign('ajax_dir_list_url', $ajax_dir_list_url);
		g('smarty') -> assign('ajax_file_list_url', $ajax_file_list_url);
		g('smarty') -> assign('ajax_search_idx_list_url', $ajax_search_idx_list_url);
		g('smarty') -> assign('ajax_search_list_url', $ajax_search_list_url);
		g('smarty') -> assign('ajax_nav_path_url', $ajax_nav_path_url);
		
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('to_detail_url', $to_detail_url);
		g('smarty') -> assign('to_search_url', $to_search_url);

		g('smarty') -> assign('folder_id', $folder_id);

		g('smarty') -> assign('title', $folder_name);
		g('smarty') -> show($this -> temp_path.'index/index.html');
	}
	
	/**
	 * 列表展示
	 *
	 * @access public
	 * @return void
	 */
	public function search_lister() {
		$search_key = get_var_value('search_key');

		//获取用作搜索关键字的全部文件列表
		$ajax_search_idx_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=107';
		//进行搜索，查询文件列表
		$ajax_search_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=108';

		//当前url
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=search_lister';
		//跳转到文件详情页面的url
		$to_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=detail';
		
		g('smarty') -> assign('ajax_search_idx_list_url', $ajax_search_idx_list_url);
		g('smarty') -> assign('ajax_search_list_url', $ajax_search_list_url);
		
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('to_detail_url', $to_detail_url);

		g('smarty') -> assign('search_key', $search_key);

		g('smarty') -> assign('title', '搜索结果');
		g('smarty') -> show($this -> temp_path.'index/search.html');
	}
}

// end of file