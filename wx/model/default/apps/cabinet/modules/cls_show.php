<?php
/**
 * 旧的企业云盘目录控制器
 *
 * @author LiangJianMing
 * @create 2015-03-27
 */
class cls_show {
    /**
     * 列表展示
     *
     * @access public
     * @return void
     */
    public function index() {
        include_once(SYSTEM_APPS . 'cabinet/modules/cls_cab_dir.php');
        g('cab_dir') -> lister();
    }
    
    /**
     * 收藏列表
     *
     * @access public
     * @return void
     */
    public function collection() {
        include_once(SYSTEM_APPS . 'cabinet/modules/cls_cab_file.php');
        g('cab_file') -> collect_list();
    }
}

// end of file