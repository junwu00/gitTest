<?php
/**
 * 企业云盘文件控制器
 *
 * @author LiangJianMing
 * @create 2015-03-27
 */
class cls_cab_file extends abs_app_base {
    public function __construct() {
        parent::__construct('cabinet');
    }

    /**
     * 文件详情展示
     *
     * @access public
     * @return void
     */
    public function detail() {
    	$file_id = (int)get_var_value('id');

        //取值：0-其他、1-ios、2-android、3-wp
        $plat_id = g('common') -> get_mobile_platid();

        //获取文件详情接口地址
        $ajax_file_detail = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=102';
        //收藏文件接口地址
        $ajax_collect_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=103';
        //取消收藏接口地址
        $ajax_uncollect_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=104';
        //下载文件接口地址
        $ajax_download_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=105';

        //获取用作搜索关键字的全部文件列表
        $ajax_search_idx_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=107';
        //进行搜索，查询文件列表
        $ajax_search_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=108';
        //获取当前目录的导航路径接口地址
        $ajax_nav_path_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=109';
        //增加文件下载数
        $ajax_increase_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=110';
        //root_url
        $root_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=lister';

        //当前url
        $this_url = get_this_url();
        
        g('smarty') -> assign('ajax_file_detail', $ajax_file_detail);
        g('smarty') -> assign('ajax_collect_url', $ajax_collect_url);
        g('smarty') -> assign('ajax_uncollect_url', $ajax_uncollect_url);
        g('smarty') -> assign('ajax_download_url', $ajax_download_url);
        g('smarty') -> assign('ajax_search_idx_list_url', $ajax_search_idx_list_url);
        g('smarty') -> assign('ajax_search_list_url', $ajax_search_list_url);
        g('smarty') -> assign('ajax_nav_path_url', $ajax_nav_path_url);
        g('smarty') -> assign('ajax_increase_url', $ajax_increase_url);
        
        g('smarty') -> assign('this_url', $this_url);
        g('smarty') -> assign('file_id', $file_id);
        g('smarty') -> assign('root_url', $root_url);

        g('smarty') -> assign('plat_id', $plat_id);

        g('smarty') -> assign('title', '文件详情');
        g('smarty') -> show($this -> temp_path.'index/file_detail.html');
    }

    /**
     * 收藏文件列表
     *
     * @access public
     * @return void
     */
    public function collect_list() {
        //获取收藏列表接口地址
        $ajax_collected_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=106';
        //取消收藏接口地址
        $ajax_uncollect_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=104';
        //跳转到文件详情页面的url
		$to_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=detail';
		//转跳搜索页面url
		$to_search_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=search_lister';

        //当前url
        $this_url = get_this_url();
        
        g('smarty') -> assign('ajax_collected_list_url', $ajax_collected_list_url);
        g('smarty') -> assign('ajax_uncollect_url', $ajax_uncollect_url);
        g('smarty') -> assign('to_detail_url', $to_detail_url);
        g('smarty') -> assign('to_search_url', $to_search_url);
        g('smarty') -> assign('this_url', $this_url);
        g('smarty') -> assign('del_tip', '确认取消收藏文件？');
        g('smarty') -> assign('type', 1);

        g('smarty') -> assign('title', '我的收藏');
        g('smarty') -> show($this -> temp_path.'index/collection_list.html');
    }
    
    
    
    /**
     * 下载文件列表
     *
     * @access public
     * @return void
     */
    public function download_list() {
    	//获取下载列表接口地址
        $ajax_collected_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=110';
        //取消下载记录接口地址
        $ajax_uncollect_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=ajax&cmd=111';
        //跳转到文件详情页面的url
		$to_detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_file&a=detail';
		//转跳搜索页面url
		$to_search_url = SYSTEM_HTTP_DOMAIN.'index.php?app=cabinet&m=cab_dir&a=search_lister';

        //当前url
        $this_url = get_this_url();
        
        g('smarty') -> assign('ajax_collected_list_url', $ajax_collected_list_url);
        g('smarty') -> assign('ajax_uncollect_url', $ajax_uncollect_url);
        g('smarty') -> assign('to_detail_url', $to_detail_url);
        g('smarty') -> assign('to_search_url', $to_search_url);
        g('smarty') -> assign('this_url', $this_url);
        g('smarty') -> assign('del_tip', '确认删除该下载记录？');
        g('smarty') -> assign('type', 0);
    
    	g('smarty') -> assign('title', '我的下载');
    	g('smarty') -> show($this -> temp_path.'index/collection_list.html');
    }
}

// end of file