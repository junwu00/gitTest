<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
    'cabinet_index_file_detail.js' => array(
        SYSTEM_ROOT . 'static/js/document-pre/js/MS-document-pre.js',
		SYSTEM_ROOT . 'static/js/jweixin-1.1.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
		SYSTEM_APPS . 'cabinet/static/js/file_detail.js',
    )
);
return $arr;

/* End of this file */