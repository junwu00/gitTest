var date_format_arr = new Array();
date_format_arr["date"] = "yyyy-mm-dd";
date_format_arr["time"] = "hh:ii";
date_format_arr["datetime"] = "yyyy-mm-dd hh:ii";

var money_type = new Array();
money_type[0]="人民币";
money_type[1]="美元";
money_type[2]="欧元";
money_type[3]="港币";

//default默认 email邮箱地址 phone电话 telephone手机
var default_text_format = "default";
var number_text_format = "number";
var email_text_format = "email";
var phone_text_format = "phone";
var telephone_text_format = "telephone";

function init_people(){
	$(".js_input_people").unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		var obj_names = $(this).attr("names");
		var obj_ids = $(this).attr("ids");
		var select_max = $(this).attr("select_max");
		if(obj_names!==undefined&&obj_names!=""){
			obj_names = obj_names.split(",");
			obj_ids = obj_ids.split(",");
		}
		for (var i in obj_names) {
			var selected_id = obj_ids[i];
			var selected_name = obj_names[i];
			selected[selected_id] = {id: selected_id, name: selected_name};
		}
		var that = $(this);
		var opt = {
			select_num  : select_max,
			type		: 2,
			split		: false,
			selected	: selected,
			ok			: function(selected) {
				var obj_vals ="";
				var obj_ids = "";
				var obj_imgs = "";
				for (var i in selected) {
					obj_ids+=selected[i]["id"]+",";
					obj_vals+=selected[i]["name"]+",";
					obj_imgs+=selected[i]["pic"]+",";
				}
				if(obj_vals!=""){
					obj_vals = obj_vals.substring(0,obj_vals.length-1);
					obj_ids = obj_ids.substring(0,obj_ids.length-1);
					obj_imgs = obj_imgs.substring(0,obj_imgs.length-1);
				}
				that.attr("names",obj_vals);
				that.attr("ids",obj_ids);
				that.attr("imgs",obj_imgs);
				var default_html ='<span class="css_edit_label">请选择'+that.attr("input_name")+'</span>';
				if(that.attr("placeholder")!=""){
					default_html = '<span class="css_edit_label">'+that.attr("placeholder")+'</span>'
				}
				if(obj_vals!=""){
					default_html = obj_vals;
				}
				that.html(default_html);
				$(c_search_form).css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
				
			}
		}
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

function init_dept(){
	$(".js_input_dept").unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		var obj_names = $(this).attr("names");
		var obj_ids = $(this).attr("ids");
		var select_max = $(this).attr("select_max");
		if(obj_names!==undefined&&obj_names!=""){
			obj_names = obj_names.split(",");
			obj_ids = obj_ids.split(",");
		}
		for (var i in obj_names) {
			var selected_id = 'd-'+obj_ids[i];
			var selected_name = obj_names[i];
			selected[selected_id] = {id: selected_id, name: selected_name};
		}
		var that = $(this);
		var opt = {
			select_num  : select_max,
			type		: 1,
			split		: false,
			selected	: selected,
			ok			: function(selected) {
				var obj_vals ="";
				var obj_ids = "";
				for (var i in selected) {
					var d_id = selected[i]["id"];
					var dept_id = d_id.substring(2, d_id.length);
					obj_ids+=dept_id+",";
					obj_vals+=selected[i]["name"]+",";
				}
				if(obj_vals!=""){
					obj_vals = obj_vals.substring(0,obj_vals.length-1);
					obj_ids = obj_ids.substring(0,obj_ids.length-1);
				}
				that.attr("names",obj_vals);
				that.attr("ids",obj_ids);
				var default_html ='<span class="css_edit_label">请选择'+that.attr("input_name")+'</span>';
				if(that.attr("placeholder")!=""){
					default_html = '<span class="css_edit_label">'+that.attr("placeholder")+'</span>'
				}
				if(obj_vals!=""){
					default_html = obj_vals;
				}
				that.html(default_html);
				$(c_search_form).css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
				
			}
		}
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

function init_date() {
	$('.js_input_date').each(function(){
		var format = $(this).attr("format");
		var opt = {};
		opt.datetime = { preset : format, stepMinute: 1};
		$(this).val($(this).val()).scroller('destroy').scroller(
			$.extend(opt['datetime'], 
			{ 
				theme: 'android-ics light', 
				mode: 'scroller',
				display: 'bottom', 
				lang: 'zh',
			})
		);
	});
}

function init_money(){
	$(".js_money_input").bind("input",function(){		
		var val = Number($(this).val());
		if(val != 0 && !check_data(val,'money')){
			if(this.value.indexOf('.')>0){
				this.value = Number(Number(this.value).toFixed(2));
			}else if(val.toString()=='NaN' || val.toString()=='Infinity'){
				this.value = '';
			}else{
				frame_obj.alert('输入非法金额，请重新输入!');
				this.value = this.getAttribute('tmpvalue');
				return;
			}
		}
		this.setAttribute('tmpvalue',this.value);
		if($(this).val()==""||$(this).val()==0){
			$(this).parent().parent().find(".js_capitalized_input").html("");
		}
		else{
			$(this).parent().parent().find(".js_capitalized_input").html(DX(this.value));
		}
	});
}

function init_other(){
	$('.js_other_cb').unbind("click").bind("click",function(){
		if($(this).is(':checked')==true){
			$(this).parent().parent().find(".js_other_text").css("display","");
		}
		else{
			$(this).parent().parent().find(".js_other_text").val('');
			$(this).parent().parent().find(".js_other_text").css("display","none");
		}
	});
	$(".js_opts_radio").unbind("click").bind("click",function(){
		$(this).parent().parent().parent().find(".js_other_text").val('');
		$(this).parent().parent().parent().find(".js_other_text").css("display","none");
	});
}

function init_table(){
	$(".js_detail_add").each(function() {
		$(this).unbind('click').bind('click', function() {
			var input_key = $(this).parent().attr("input_key");
			get_table_td_edit(input_key,-1)
		});
	});
}

function get_table_td_edit(input_key,row_num){
	var table_obj = dataObj[input_key];
	var table_arr = undefined;
	var size = 0;//确定第几条明细
	if(table_obj.rec===undefined){
		table_arr = $.extend(true, {}, table_obj.table_arr);
	}
	else if(table_obj.rec[row_num]===undefined){
		table_arr = $.extend(true, {}, table_obj.table_arr);
		size = table_obj.rec.length;//确定第几条明细
	}
	else{
		table_arr = table_obj.rec[row_num];
		size = row_num;
	}
	
	var htmlstr="";
	$.each(table_arr,function(key,th_obj){
		if(th_obj.type=="text"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			if(th_obj.val===undefined){
				th_obj.val="";
			}
			var placeholder = '请输入'+th_obj.name;
			if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){placeholder=th_obj.placeholder}
			htmlstr +='<div class="css_input_info"><input type="text" th_key = "'+th_obj.th_key+'" class="js_input_val js_func_input js_'+th_obj.format+'" placeholder="'+placeholder+'" value="'+th_obj.val+'"/></div>';
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			htmlstr +='</div></div>';
		}
		else if(th_obj.type=="textarea"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_link"><div class="css_input_title"><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			htmlstr +='<div class="css_input_describe" style="margin:0 0 10px"></div></div>';
			if(th_obj.val===undefined){th_obj.val="";}
			var placeholder = '请输入'+th_obj.name;
			if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){placeholder=th_obj.placeholder}
			htmlstr +='<div class="css_input_all_info"><textarea th_key = "'+th_obj.th_key+'"  class="js_input_val " placeholder="'+placeholder+'">'+th_obj.val+'</textarea></div>';
			htmlstr +='<div style="clear: both;"></div>';
			htmlstr +='</div>';
		}
		else if(th_obj.type=="radio"){
			var opts_nums = th_obj.opts_nums;
			opts_nums = opts_nums.split(",");
			var opts = th_obj.opts;
			opts = opts.split(",");
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			htmlstr +='<div class="css_input_info">';
			for(var i=0;i<opts.length;i++){
				if(th_obj.val!==undefined && th_obj.val !=""){
					var val = th_obj.val.split(";");
					if($.inArray(opts[i], val)>-1){//存在
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_opts_radio rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" checked></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
					else{
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_opts_radio rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" ></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
				}
				else{
					if(th_obj.default_val!=""){
						var default_val = th_obj.default_val.split(",");
						if($.inArray(opts_nums[i], default_val)>-1){//存在
							htmlstr +='<div class="css_input_div">'+
							'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_opts_radio rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" checked></div>'+
							'<div class="css_input_html">'+opts[i]+'</div>'+
							'<div style="clear: both;"></div>'+
							'</div>';
						}
						else{
							htmlstr +='<div class="css_input_div">'+
							'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_opts_radio rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" ></div>'+
							'<div class="css_input_html">'+opts[i]+'</div>'+
							'<div style="clear: both;"></div>'+
							'</div>';
						}
					}
					else{
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_opts_radio rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" ></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
				}
			}
			if(th_obj.is_other==1){
				if(th_obj.c_other===undefined || th_obj.c_other==0){
					htmlstr +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_other_cb rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="is_other" ></div>'+
					'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" style="display:none" placeholder="请输入其它内容"/></div>'+
					'<div style="clear: both;"></div>'+
					'</div>';
				}
				else{
					var val = th_obj.val.split(",");
					htmlstr +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_other_cb rd1" name="'+th_obj.th_key+'_'+(size+1)+'"  value="is_other" checked></div>'+
					'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" placeholder="请输入其它内容" value="'+val[val.length-1]+'"/></div>'+
					'<div style="clear: both;"></div>'+
					'</div>';
				}
			}
			htmlstr +='</div>';
			htmlstr +='<div style="clear: both;"></div>';
			htmlstr +='</div>';
		}
		else if(th_obj.type=="checkbox"){
			var opts_nums = th_obj.opts_nums;
			opts_nums = opts_nums.split(",");
			var opts = th_obj.opts;
			opts = opts.split(",");
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			htmlstr +='<div class="css_input_info">';
			for(var i=0;i<opts.length;i++){
				if(th_obj.val!==undefined && th_obj.val !=""){
					var val = th_obj.val.split(";");
					if($.inArray(opts[i], val)>-1){//存在
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_opts_checkbox ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" checked></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
					else{
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_opts_checkbox ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'"></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
				}
				else{
					if(th_obj.default_val!=""){
						var default_val = th_obj.default_val.split(",");
						if($.inArray(opts_nums[i], default_val)>-1){//存在
							htmlstr +='<div class="css_input_div">'+
							'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_opts_checkbox ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'" checked></div>'+
							'<div class="css_input_html">'+opts[i]+'</div>'+
							'<div style="clear: both;"></div>'+
							'</div>';
						}
						else{
							htmlstr +='<div class="css_input_div">'+
							'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_opts_checkbox ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'"></div>'+
							'<div class="css_input_html">'+opts[i]+'</div>'+
							'<div style="clear: both;"></div>'+
							'</div>';
						}
					}
					else{
						htmlstr +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_opts_checkbox ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="'+opts[i]+'"></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'<div style="clear: both;"></div>'+
						'</div>';
					}
				}
			}
			if(th_obj.is_other==1){
				if(th_obj.c_other===undefined || th_obj.c_other==0){
					htmlstr +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="checkbox" th_key = "'+th_obj.th_key+'" class="js_other_cb ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="is_other" ></div>'+
					'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" style="display:none" placeholder="请输入其它内容"/></div>'+
					'<div style="clear: both;"></div>'+
					'</div>';
				}
				else{
					var val = th_obj.val.split(",");
					htmlstr +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" th_key = "'+th_obj.th_key+'" class="js_other_cb ck2" name="'+th_obj.th_key+'_'+(size+1)+'"  value="is_other" checked></div>'+
					'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" placeholder="请输入其它内容" value="'+val[val.length-1]+'"/></div>'+
					'<div style="clear: both;"></div>'+
					'</div>';
				}
			}
			htmlstr +='</div>';
			htmlstr +='<div style="clear: both;"></div>';
			htmlstr +='</div>';
		}
		else if(th_obj.type=="select"){
			var opts_nums = th_obj.opts_nums;
			opts_nums = opts_nums.split(",");
			var opts = th_obj.opts;
			opts = opts.split(",");
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs ">';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			htmlstr +='<div class="css_input_info"><select th_key = "'+th_obj.th_key+'" class="js_input_val  css_input_select">';
			for(var i=0;i<opts.length;i++){
				if(th_obj.val!==undefined && th_obj.val !=""){
					var val = th_obj.val.split(";");
					if($.inArray(opts[i], val)>-1){//存在
						htmlstr +='<option value="'+opts[i]+'" selected>'+opts[i]+'</option>';
					}
					else{
						htmlstr +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
					}
				}
				else{
					if(th_obj.default_val!=""){
						var default_val = th_obj.default_val.split(",");
						if($.inArray(opts_nums[i], default_val)>-1){//存在
							htmlstr +='<option value="'+opts[i]+'" selected>'+opts[i]+'</option>';
						}
						else{
							htmlstr +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
						}
					}
					else{
						htmlstr +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
					}
				}
			}
			htmlstr +='</select><span class="right_"></span></div>';
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			htmlstr +='</div></div>';
		}
		else if(th_obj.type=="date"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			if(th_obj.val===undefined||th_obj.val==""){
				if(th_obj.default_val!==undefined&&th_obj.default_val!=""){
					th_obj.val = getNowFormatDate(th_obj.format);
				}
				else{
					th_obj.val="";
				}
			}
			var placeholder = '请选择'+th_obj.name;
			if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){placeholder=th_obj.placeholder}
			htmlstr +='<div class="css_input_info"><input type="text" th_key = "'+th_obj.th_key+'" class="js_input_val css_input_date js_input_date" format="'+th_obj.format+'" value="'+th_obj.val+'" placeholder="'+placeholder+'" readonly="readonly"/><span class="right_"></span></div>';
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			
			htmlstr +='</div></div>';
		}
		else if(th_obj.type=="money"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			htmlstr +='</div>';
			if(th_obj.val===undefined){
				th_obj.val="";
			}
			if(th_obj.capitalized_val===undefined){
				th_obj.capitalized_val = "";
			}
			var placeholder = '请输入'+th_obj.name;
			if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){placeholder=th_obj.placeholder}
			htmlstr +='<div class="css_input_info"><input type="number" th_key = "'+th_obj.th_key+'" class="js_input_val js_money_input js_func_input" tmpvalue="'+th_obj.val+'" value="'+th_obj.val+'" placeholder="'+placeholder+'"/>('+money_type[th_obj.money_type]+')</div>';
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			
			htmlstr +='</div>';
			if(th_obj.capitalized==1){
				htmlstr +='<div class="css_money_link"></div><div class="css_input_label"><span>大写</span></div><div class="css_input_info" style="min-height: 32px ;padding:10px 15px 0"><span class="js_capitalized_input" >'+th_obj.capitalized_val+'</span></div><div style="clear: both;"></div><div class="css_input_describe"></div>';
			}
			htmlstr +='</div>';
		}
		else if(th_obj.type=="people"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit">';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			if(th_obj.select_max === undefined){th_obj.select_max=0;}
			htmlstr +='</div>';
			if(th_obj.placeholder===undefined){th_obj.placeholder='';}
			if(th_obj.ids!==undefined){
				if(th_obj.ids!=""){
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+th_obj.val+'" ids="'+th_obj.ids+'" imgs="'+th_obj.imgs+'" class="js_input_val css_input_people js_input_people"  data-form="#table_add_div">'+th_obj.val+'</div><span class="right_"></span></div>';
				}
				else{
					var default_html ='<span class="css_edit_label">请选择'+th_obj.name+'</span>';
					if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){default_html='<span class="css_edit_label">'+th_obj.placeholder+'</span>'}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="" ids="" imgs="" class="js_input_val css_input_people js_input_people"  data-form="#table_add_div">'+default_html+'</div><span class="right_"></span></div>';
				}
			}
			else{
				if(th_obj.default_type==1){
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+$("#create_name").val()+'" ids="'+$("#create_id").val()+'" imgs="'+$("#create_url").val()+'" class="js_input_val css_input_people js_input_people" data-form="#table_add_div">'+$("#create_name").val()+'</div><span class="right_"></span></div>';
				}
				else if(th_obj.default_type==2&&th_obj.default_val!==undefined&&th_obj.default_val!=""&&th_obj.default_val.length>0){
					var names = "";
					var ids = "";
					var imgs = "";
					for(var i=0;i<th_obj.default_val.length;i++){
						var user = th_obj.default_val[i];
						names+=user['name']+",";
						ids+=user['id']+",";
						imgs+=user['pic']+",";
					}
					if(names!=""){
						names = names.substring(0,names.length-1);
						ids = ids.substring(0,ids.length-1);
						imgs = imgs.substring(0,imgs.length-1);
					}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+names+'" ids="'+ids+'" imgs="'+imgs+'" class="js_input_val css_input_people js_input_people" data-form="#table_add_div">'+names+'</div><span class="right_"></span></div>';
				}
				else{
					var default_html ='<span class="css_edit_label">请选择'+th_obj.name+'</span>';
					if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){default_html='<span class="css_edit_label">'+th_obj.placeholder+'</span>'}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="" ids="" imgs="" class="js_input_val css_input_people js_input_people"  data-form="#table_add_div">'+default_html+'</div><span class="right_"></span></div>';
				}
			}
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			htmlstr +='</div></div>';
		}
		else if(th_obj.type=="dept"){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				htmlstr +='<div class="js_inputs css_inputs " >';
			}
			else{
				htmlstr +='<div class="js_inputs css_inputs css_not_visit" >';
			}
			if(th_obj.edit_input===undefined||th_obj.edit_input==0){
				htmlstr +='<div class="css_hidden_div"></div>';
			}
			htmlstr +='<div class="css_input_label" ><span>'+th_obj.name+'</span>';
			if(th_obj.edit_input!==undefined&&th_obj.edit_input==1&&th_obj.must == 1){htmlstr +='<em>*</em>';}
			if(th_obj.select_max === undefined){th_obj.select_max=0;}
			htmlstr +='</div>';
			if(th_obj.placeholder===undefined){th_obj.placeholder='';}
			if(th_obj.ids!==undefined){
				if(th_obj.ids!=""){
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+th_obj.val+'" ids="'+th_obj.ids+'" class="js_input_val css_input_dept js_input_dept"  data-form="#table_add_div">'+th_obj.val+'</div><span class="right_"></span></div>';
				}
				else{
					var default_html ='<span class="css_edit_label">请选择'+th_obj.name+'</span>';
					if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){default_html='<span class="css_edit_label">'+th_obj.placeholder+'</span>'}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="" ids="" imgs="" class="js_input_val css_input_dept js_input_dept"  data-form="#table_add_div">'+default_html+'</div><span class="right_"></span></div>';
				}
			}
			else{
				if(th_obj.default_type==1){
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+$("#create_dept_name").val()+'" ids="'+$("#create_dept_id").val()+'" class="js_input_val css_input_dept js_input_dept" data-form="#table_add_div">'+$("#create_dept_name").val()+'</div><span class="right_"></span></div>';
				}
				else if(th_obj.default_type==2&&th_obj.default_val!==undefined&&th_obj.default_val!=""&&th_obj.default_val.length>0){
					var names = "";
					var ids = "";
					var imgs = "";
					for(var i=0;i<th_obj.default_val.length;i++){
						var dept = th_obj.default_val[i];
						names+=dept['name']+",";
						ids+=dept['id']+",";
					}
					if(names!=""){
						names = names.substring(0,names.length-1);
						ids = ids.substring(0,ids.length-1);
					}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="'+names+'" ids="'+ids+'" class="js_input_val css_input_dept js_input_dept" data-form="#table_add_div">'+names+'</div><span class="right_"></span></div>';
				}
				else{
					var default_html ='<span class="css_edit_label">请选择'+th_obj.name+'</span>';
					if(th_obj.placeholder!==undefined&&th_obj.placeholder!=""){default_html='<span class="css_edit_label">'+th_obj.placeholder+'</span>'}
					htmlstr +='<div class="css_input_info"><div select_max="'+th_obj.select_max+'" input_name="'+th_obj.name+'" placeholder="'+th_obj.placeholder+'" th_key = "'+th_obj.th_key+'" names="" ids=""  class="js_input_val css_input_dept js_input_dept"  data-form="#table_add_div">'+default_html+'</div><span class="right_"></span></div>';
				}
			}
			htmlstr +='<div style="clear: both;"></div><div class="css_input_describe">';
			htmlstr +='</div></div>';
		}
	});
	var detail_div='<div class="js_detailed'+input_key+' detailed" style="border-top: 1px dashed #ddd"><div class="css_inputs js_inputs" style="background-color: #f4f4f4;">'+
				'<input type="hidden" id="table_input_key" value="'+input_key+'"/>'+
				'<input type="hidden" id="table_edit_num" value="'+row_num+'"/>'+
				'<div class="css_table_title">'+
				'<div style="padding-top: 8px;">'+
					'<span style="" class="detailed-title-div'+input_key+'">'+table_obj.name+'详情'+(size+1)+'</span>'+
					'<span class="glyphicon glyphicon-remove" style="font-size:20px;color:#979797;float:right" onclick="cancelDiv()"></span>&nbsp;&nbsp;'+
				'</div>'+
			'</div>'+
		'</div>';
	if(table_obj.describe!==undefined&&table_obj.describe!=""){
		detail_div+='<div class="css_table_describe_div"><div class="css_table_describe">'+table_obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	
	var btn_html = "";
	if(row_num==-1){//新建
		btn_html = '<div id="css_bottom_menu"><ul class="nav nav-pills" role="tablist"><li style="width:100%;" id="new_save_table_tr" onclick="saveDiv()"><div class="css_b_but"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;保存</div></li></ul></div>';
	}
	else{
		btn_html = '<div id="css_bottom_menu"><ul class="nav nav-pills" role="tablist"><li style="width:50%;" id="del_table_tr" onclick="removeDiv()"><div class="css_s_but_d"><span class="glyphicon glyphicon-trash"></span>&nbsp;删除</div></li><li style="width:50%;" id="save_table_tr" onclick="saveDiv()"><div class="css_b_but"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;保存</div></li></ul></div>';
	}
	
	htmlstr = detail_div+htmlstr+"</div><div style='margin-bottom:55px'></div>"+btn_html;
	var height = $(".container").height();
	$("#table_add_div").css("top",height);
	$("#table_add_div").html(htmlstr);
	$("#table_add_div").animate({top:'0px'},"slow",function(){
		$(".container").css("display","none");
		scroll(0,0);
	});

	var obj = calc.returnTrClassObj($("#table_add_div"));
	
	calc.bindObjInputClick(obj,1);
	
	init_date();
	init_money();
	init_other();
	init_dept();
	init_people();
	
}


//删除
function removeDiv(){
	frame_obj.comfirm('确定要删除明细？', function() {
		var table_input_key = $("#table_input_key").val();
		var table_edit_num = $("#table_edit_num").val();
		var table_obj = dataObj[table_input_key];
		var rec = table_obj.rec;
		rec.splice(table_edit_num,1);
		cancelDiv(function(){
			set_table_html(table_input_key);
		});
	});
}
//取消
function cancelDiv(func){
	var table_input_key = $("#table_input_key").val();
	$(".container").css("display","");
	//scroll(0,0);
	$("#table_add_div").animate({top:'100%'},"slow",function(){
		$("#table_add_div").html('');
		$(window).scrollTop($("div[input_key="+table_input_key+"]").offset().top);
		if(func!==undefined){
			func();
		}
	});
}
//保存
function saveDiv(){
	var table_input_key = $("#table_input_key").val();
	var table_edit_num = $("#table_edit_num").val();
	var table_obj = dataObj[table_input_key];
	var table_arr = table_obj.table_arr;
	var temp_obj = undefined;
	if(table_edit_num==-1){
		temp_obj = $.extend(true, {}, table_obj.table_arr);
	}
	else{
		temp_obj = table_obj.rec[table_edit_num];
		if(table_obj.rec[table_edit_num]===undefined){
			temp_obj = $.extend(true, {}, table_obj.table_arr);
		}
	}
	$.each(temp_obj,function(th_key,th_obj){
		var th_key = th_obj.th_key;
		var th_type = th_obj.type;
		if(th_type=="text"){
			var th_val = $("#table_add_div").find("input[th_key="+th_key+"]").val();
			th_obj.val = th_val;
		}
		else if(th_type=="textarea"){
			var th_val = $("#table_add_div").find("textarea[th_key="+th_key+"]").val();
			th_obj.val = th_val;
		}
		else if(th_type=="date"){
			var th_val = $("#table_add_div").find("input[th_key="+th_key+"]").val();
			th_obj.val = th_val;
		}
		else if(th_type=="select"){
			var th_val = $("#table_add_div").find("select[th_key="+th_key+"]").val();
			th_obj.val = th_val;
		}
		else if(th_type=="radio"){
			var th_val = "";
			var th_c_other = 0;
			$("#table_add_div").find("input[th_key="+th_key+"]:checked").each(function(){
				if($(this).hasClass("js_opts_radio")){
					th_val+=$(this).val()+";";
				}
				if($(this).hasClass("js_other_cb")){
					th_val+=$(this).parent().parent().find(".js_other_text").val()+";";
					th_c_other = 1;
				}
			});
			if(th_val!=""){
				th_val = th_val.substring(0,th_val.length-1)
			}
			th_obj.val = th_val;
			th_obj.c_other = th_c_other;
		}
		else if(th_type=="checkbox"){
			var th_val = "";
			var th_c_other = 0;
			$("#table_add_div").find("input[th_key="+th_key+"]:checked").each(function(){
				if($(this).hasClass("js_opts_checkbox")){
					th_val+=$(this).val()+";";
				}
				if($(this).hasClass("js_other_cb")){
					th_val+=$(this).parent().parent().find(".js_other_text").val()+";";
					th_c_other = 1;
				}
			});
			if(th_val!=""){
				th_val = th_val.substring(0,th_val.length-1)
			}
			th_obj.val = th_val;
			th_obj.c_other = th_c_other;
		}
		else if(th_type=="money"){
			var th_val = $("#table_add_div").find("input[th_key="+th_key+"]").val();
			th_obj.val = th_val;
			if(th_obj.capitalized==1){
				th_obj.capitalized_val = $("#table_add_div").find("input[th_key="+th_key+"]").parent().parent().find(".js_capitalized_input").html();
			}
		}
		else if(th_type=="people"){
			var th_input_obj = $("#table_add_div").find("div[th_key="+th_key+"]");
			var obj_vals = th_input_obj.attr("names");
			var obj_ids = th_input_obj.attr("ids");
			var obj_imgs = th_input_obj.attr("imgs");
			th_obj.val = obj_vals;
			th_obj.ids = obj_ids;
			th_obj.imgs = obj_imgs;
		}
		else if(th_type=="dept"){
				var th_input_obj = $("#table_add_div").find("div[th_key="+th_key+"]");
				var obj_vals = th_input_obj.attr("names");
				var obj_ids = th_input_obj.attr("ids");
				th_obj.val = obj_vals;
				th_obj.ids = obj_ids;
		}
	});
	
	
	if(!checkDiv(temp_obj,table_obj)){
		return false;
	}
	
	if(table_edit_num ==-1){//新增
		if(table_obj.rec===undefined){
			table_obj.rec = new Array();
		}
		table_obj.rec.push(temp_obj);
	}
	else{
		table_obj.rec[table_edit_num]=temp_obj;
	}
	cancelDiv(function(){
		set_table_html(table_input_key);
	});
	
}
//子表插入数据检查
function checkDiv(items,table_obj){
	var is_check = true;
	$.each(items,function(key,item){
		if (table_obj.visit_input==1&&item.visit_input==1&&item.edit_input==1 && item.must == 1 && item.type != 'table' && !check_data(item.val, 'notnull')) {
			frame_obj.alert('请填写' + item.name);
			is_check = false;
			return false;
		}
		if( item.type == 'text' && item.val!="" && item.format !=default_text_format ){
			if(item.format == number_text_format && !check_data(item.val, 'double')){
				frame_obj.alert(item.name+"需要填写数字");
				is_check = false;
				return false;
			}
			else if(item.format == email_text_format && !check_data(item.val, 'email')){
				frame_obj.alert(item.name+"需要填写邮箱");
				is_check = false;
				return false;
			}
			else if(item.format == phone_text_format && !check_data(item.val, 'phone')){
				frame_obj.alert(item.name+"需要填写电话号码");
				is_check = false;
				return false;
			}
			else if(item.format == telephone_text_format && !check_data(item.val, 'mobile')){
				frame_obj.alert(item.name+"需要填写手机号码");
				is_check = false;
				return false;
			}
			
		}
	});
	return is_check;
}
//插入子表数据
function set_table_html(table_input_key){
	var table_obj = dataObj[table_input_key];
	var table_html = $(".js_inputs").find("table[input_key="+table_input_key+"]");
	var temp_html = "<tr>";
	$.each(table_obj.table_arr,function(key,th_obj){
		if(th_obj.visit_input===undefined||th_obj.visit_input==1){
			temp_html +='<th>'+th_obj["name"];
			if(th_obj.edit_input==1&&th_obj.must==1){temp_html +='<em>*</em>';}
			temp_html +='</th>';
		}
	});
	temp_html +="<td style='min-width:80px'></td>";
	temp_html += "</tr>";
	for(var i=0;i<table_obj.rec.length;i++){
		var items = table_obj.rec[i];
		temp_html += "<tr class='js_tr'>";
		var table_arr = table_obj.table_arr;
		$.each(items,function(key,item){
			var th_obj = table_arr[key];
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				if(item.type=="money"){
					temp_html +="<td>";
					if(item.val!=""){
						temp_html +=item.val+"("+money_type[item.money_type]+")";
					}
					temp_html +="<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/>";
					if(item.capitalized==1&&item.capitalized_val!=""){temp_html +="<br>("+item.capitalized_val+")";}
					temp_html +="</td>";
				}
				else if(item.type=="textarea"){
					temp_html +="<td>"+item.val.replace(/\n/g,'<br>')+"</td>";
				}
				else if(item.type=="text"){
					temp_html +="<td>"+item.val+"<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+item.val+"'/></td>";
				}
				else{
					temp_html +="<td>"+item.val+"</td>";
				}
			}
		});
		temp_html +="<td style='min-width:80px;text-align:center;color:#80c269' onclick='get_table_td_edit(\""+table_input_key+"\","+i+")'>编辑</td>";
		temp_html += "</tr>";
	}
	table_html.html(temp_html);
	calc.bindObjInputClick(table_html);
	table_html.find(".js_func_input").trigger("input");
}

var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			/*
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			*/
			
			currUploadDiv = $('<div class="css_file_div js_file_div " data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info css_file_name">' + fileName + '</div>'+
							'<span class="mupload-notice" data-new=1>开始扫描文件</span>'+
						'</div>');
			$("#file_describe").after(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('.mupload-notice[data-new=1]').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('.mupload-notice[data-new=1]').html('正在上传');
		},
		process		: function(percent) {
			$('.mupload-notice[data-new=1]').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	
    		var is_exist = false;
        	var file_key = data.hash;
        	$('.js_file_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
        	
        	
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('filekey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('fext', data.file_ext);
			
			var file_ext = data.file_ext;
    		
			var optBtn = '<span class="css_delete_btn hide" onclick="removeFile($(this).parents(\'.js_file_div\'));">删除</span>';
    		if (isImg == 1) {
    			$(currUploadDiv).find('.file_info').before('<img style="width: 33px;height: 23px" src="'+mediaUrl+data.path+'">');
    			//optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		else{
    			$(currUploadDiv).find('.file_info').before('<div style="width: 32px;height: 32px;display: inline-block;position: relative;top: 5px;background-color: #91bef3;color: #fff;text-align: center;">'+file_ext[0].toUpperCase( )+'</div>');
    		}
    		currUploadDiv.append('<span class="file_action_open" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="open_action(this)"><span class="glyphicon glyphicon-chevron-right"></span></span>');
    		currUploadDiv.append('<span class="file_action_close hide" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="close_action(this)"><span class="glyphicon glyphicon-chevron-down"></span></span>');
    		$("#file_num").html('(' + $('.file_info').length + ')');
    		$('.mupload-notice[data-new=1]').html(optBtn);
    		$('.mupload-notice[data-new=1]').attr('data-hash',file_key);
    		$('.mupload-notice[data-new=1]').attr('data-new',0);
    		currUploadDiv.after('<div class="css_file_div js_file_div file_action file_del hide" data-name="'+data.file_name+'" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="del(\'mupload-notice\',\''+file_key+'\')"><span>删除</span></div>')
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});
}

function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_del[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

function DX(num) {
	var strOutput = "";  
  	var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';  
 	num += "00";  
  	var intPos = num.indexOf('.');  
  	if (intPos >= 0) {
  		num = num.substring(0, intPos) + num.substr(intPos + 1, 2);  
  	} 
    	
  	strUnit = strUnit.substr(strUnit.length - num.length);  
  	for (var i=0; i < num.length; i++){
  		strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i,1),1) + strUnit.substr(i,1); 
  	}  
    return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");  
}

function getNowFormatDate(format) {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    var hour = date.getHours();
    var minute = date.getMinutes();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    if(hour>=0 && hour <= 9){
    	hour = "0" + hour;
    }
    if(minute>=0 && minute <= 9){
    	minute = "0" + minute;
    }
    var currentdate = "";
    if(format=="date"){
    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    }
    else if(format=="time"){
    	currentdate = hour + seperator2 + minute;
    }
    else{
    	currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + hour + seperator2 + minute;
    }
    return currentdate;
}

function del(class_key,hash) {
	$('.' + class_key +'[data-hash='+hash+']').find('span')[0].click();
	$(".file_action[data-hash='"+hash+"']").remove();
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
	$("#file_num").html('(' + $('.file_info').length + ')');
}


//初始化页面控件
function inst_apply_html(){
	$.each(dataObj,function(key,obj){
		var input_html = inst_input_html(obj);
		$("#js_inputs_div").append(input_html);
	});
	
}

function inst_input_html(obj){

	var html = ''
	
	if(obj.type=="text"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		var placeholder = '请输入'+obj.name;
		if(obj.placeholder!==undefined&&obj.placeholder!=""){placeholder=obj.placeholder}
		html +='<div class="css_input_info"><input type="text" input_key = "'+obj.input_key+'" class="js_input_val js_func_input js_'+obj.format+'" placeholder="'+placeholder+'" value="'+obj.val+'"/></div>';
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	else if(obj.type=="textarea"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_link"><div class="css_input_title"><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +='<div class="css_input_describe" style="margin:0 0 10px">'+obj.describe.replace(/\n/g,'<br>')+'</div></div>';
		if(obj.val===undefined){
			obj.val="";
		}
		var placeholder = '请输入'+obj.name;
		if(obj.placeholder!==undefined&&obj.placeholder!=""){placeholder=obj.placeholder}
		html +='<div class="css_input_all_info"><textarea input_key = "'+obj.input_key+'"  class="js_input_val " placeholder="'+placeholder+'">'+obj.val+'</textarea></div>';
		html +='<div style="clear: both;"></div>';
		html +='</div>';
	}
	else if(obj.type=="radio"){
		var opts_nums = obj.opts_nums;
		opts_nums = opts_nums.split(",");
		var opts = obj.opts;
		opts = opts.split(",");
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		html +='<div class="css_input_info">';
		for(var i=0;i<opts.length;i++){
			if(obj.val!==undefined && obj.val !=""){
				var val = obj.val.split(",");
				if($.inArray(opts[i], val)>-1){//存在
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_opts_radio rd1" name="'+obj.input_key+'"  value="'+opts[i]+'" checked></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
				else{
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_opts_radio rd1" name="'+obj.input_key+'"  value="'+opts[i]+'" ></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
			}
			else{
				if(obj.default_val!=""){
					var default_val = obj.default_val.split(",");
					if($.inArray(opts_nums[i], default_val)>-1){//存在
						html +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_opts_radio rd1" name="'+obj.input_key+'"  value="'+opts[i]+'" checked></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'</div>';
						obj.val = opts[i];
					}
					else{
						html +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_opts_radio rd1" name="'+obj.input_key+'"  value="'+opts[i]+'" ></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'</div>';
					}
				}
				else{
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_opts_radio rd1" name="'+obj.input_key+'"  value="'+opts[i]+'" ></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
			}
		}
		if(obj.is_other==1){
			if(obj.c_other===undefined || obj.c_other==0){
				html +='<div class="css_input_div">'+
				'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_other_cb rd1" name="'+obj.input_key+'"  value="is_other" ></div>'+
				'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" style="display:none" placeholder="请输入其它内容"/></div>'+
				'</div>';
			}
			else{
				var val = obj.val.split(",");
				html +='<div class="css_input_div">'+
				'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_other_cb rd1" name="'+obj.input_key+'"  value="is_other" checked></div>'+
				'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" placeholder="请输入其它内容" value="'+val[val.length-1]+'"/></div>'+
				'</div>';
			}
		}
		html +='</div>';
		html +='<div style="clear: both;"></div>';
		if(obj.describe===undefined||obj.describe==""){
			obj.describe = "";
		}
		else{
			html +='<div class="css_input_describe">'+obj.describe.replace(/\n/g,'<br>')+'</div>';
		}
		html +='</div>';
	}
	else if(obj.type=="checkbox"){
		var opts_nums = obj.opts_nums;
		opts_nums = opts_nums.split(",");
		var opts = obj.opts;
		opts = opts.split(",");
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		html +='<div class="css_input_info">';
		var tmp_default_val ="";
		for(var i=0;i<opts.length;i++){
			if(obj.val!==undefined && obj.val !=""){
				var val = obj.val.split(";");
				if($.inArray(opts[i], val)>-1){//存在
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_opts_checkbox ck2" name="'+obj.input_key+'"  value="'+opts[i]+'" checked></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
				else{
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_opts_checkbox ck2" name="'+obj.input_key+'"  value="'+opts[i]+'"></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
			}
			else{
				if(obj.default_val!=""){
					var default_val = obj.default_val.split(",");
					if($.inArray(opts_nums[i], default_val)>-1){//存在
						html +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_opts_checkbox ck2" name="'+obj.input_key+'"  value="'+opts[i]+'" checked></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'</div>';
						tmp_default_val += opts[i]+";";
					}
					else{
						html +='<div class="css_input_div">'+
						'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_opts_checkbox ck2" name="'+obj.input_key+'"  value="'+opts[i]+'"></div>'+
						'<div class="css_input_html">'+opts[i]+'</div>'+
						'</div>';
					}
				}
				else{
					html +='<div class="css_input_div">'+
					'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_opts_checkbox ck2" name="'+obj.input_key+'"  value="'+opts[i]+'"></div>'+
					'<div class="css_input_html">'+opts[i]+'</div>'+
					'</div>';
				}
			}
		}
		if(tmp_default_val!=""){
			tmp_default_val = tmp_default_val.substring(0,tmp_default_val.length-1);
			obj.val = tmp_default_val;
		}
		if(obj.is_other==1){
			if(obj.c_other===undefined || obj.c_other==0){
				html +='<div class="css_input_div">'+
				'<div class="css_input_rc"><input type="checkbox" input_key = "'+obj.input_key+'" class="js_other_cb ck2" name="'+obj.input_key+'"  value="is_other" ></div>'+
				'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" style="display:none" placeholder="请输入其它内容"/></div>'+
				'</div>';
			}
			else{
				var val = obj.val.split(",");
				html +='<div class="css_input_div">'+
				'<div class="css_input_rc"><input type="radio" input_key = "'+obj.input_key+'" class="js_other_cb ck2" name="'+obj.input_key+'"  value="is_other" checked></div>'+
				'<div class="css_input_html">其它<input type="text" class="css_other_text js_other_text" placeholder="请输入其它内容" value="'+val[val.length-1]+'"/></div>'+
				'</div>';
			}
		}
		html +='</div>';
		html +='<div style="clear: both;"></div>';
		if(obj.describe===undefined||obj.describe==""){
			obj.describe = "";
		}
		else{
			html +='<div class="css_input_describe">'+obj.describe.replace(/\n/g,'<br>')+'</div>';
		}
		html +='</div>';
	}
	else if(obj.type=="select"){
		var opts_nums = obj.opts_nums;
		opts_nums = opts_nums.split(",");
		var opts = obj.opts;
		opts = opts.split(",");
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		html +='<div class="css_input_info"><select input_key = "'+obj.input_key+'" class="js_input_val  css_input_select">';
		for(var i=0;i<opts.length;i++){
			if(obj.val!==undefined && obj.val !=""){
				var val = obj.val.split(",");
				if($.inArray(opts[i], val)>-1){//存在
					html +='<option value="'+opts[i]+'" selected>'+opts[i]+'</option>';
				}
				else{
					html +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
				}
			}
			else{
				if(obj.default_val!=""){
					var default_val = obj.default_val.split(",");
					if($.inArray(opts_nums[i], default_val)>-1){//存在
						html +='<option value="'+opts[i]+'" selected>'+opts[i]+'</option>';
						obj.val = opts[i];
					}
					else{
						html +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
					}
				}
				else{
					html +='<option value="'+opts[i]+'">'+opts[i]+'</option>';
				}
			}
		}
		html +='</select><span class="right_"></span></div>';
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	else if(obj.type=="date"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.val===undefined||obj.val==""){
			if(obj.default_val!==undefined&&obj.default_val!=""){
				obj.val = getNowFormatDate(obj.format);
			}
			else{
				obj.val="";
			}
		}
		var placeholder = '请选择'+obj.name;
		if(obj.placeholder!==undefined&&obj.placeholder!=""){placeholder=obj.placeholder}
		html +='<div class="css_input_info"><input type="text" input_key = "'+obj.input_key+'" class="js_input_val css_input_date js_input_date" format="'+obj.format+'" value="'+obj.val+'" placeholder="'+placeholder+'" readonly="readonly"/><span class="right_"></span></div>';
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	else if(obj.type=="money"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		if(obj.capitalized_val===undefined){
			obj.capitalized_val = "";
		}
		var placeholder = '请输入'+obj.name;
		if(obj.placeholder!==undefined&&obj.placeholder!=""){placeholder=obj.placeholder}
		html +='<div class="css_input_info"><input type="number" input_key = "'+obj.input_key+'" class="js_input_val js_money_input js_func_input" tmpvalue="'+obj.val+'" value="'+obj.val+'" placeholder="'+placeholder+'"/>('+money_type[obj.money_type]+')</div>';
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div>';
		if(obj.capitalized==1){
			html +='<div class="css_money_link"></div><div class="css_input_label"><span>大写</span></div><div class="css_input_info" style="min-height: 32px ;padding:10px 15px 0"><span class="js_capitalized_input" >'+obj.capitalized_val+'</span></div><div style="clear: both;"></div><div class="css_input_describe"></div>';
		}
		html +='</div>';
	}
	else if(obj.type=="table"){
		var table_obj = dataObj[obj.input_key];
		var table_arr = table_obj.table_arr;
		
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div id="adddiv'+obj.input_key+'" input_key = "'+obj.input_key+'" class="css_button_div css_inputs js_inputs" >';
		}
		else{
			html +='<div id="adddiv'+obj.input_key+'" input_key = "'+obj.input_key+'" class="css_button_div css_inputs js_inputs css_not_visit" >';
		}
		
		html +='<div input_key="'+obj.input_key+'" id="addbutton'+obj.input_key+'" class=" css_detail_add">';
		html +='<span style="position: relative;top: 7px">'+obj.name;
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</span>';
		if(obj.edit_input==1){
			html +='<span class="glyphicon glyphicon-plus-sign js_detail_add" style="font-size: 26px;padding-top: 3px;float:right;color:rgb(100,195,169)"></span>';
		}
		html +='</div>';
		html +='<div style="overflow-x:auto;z-index:9999;width: ' + (s_width - 11) +'px ;" class="son_div"><table class="css_son_table" input_key="'+obj.input_key+'"><tr>';//table
		$.each(table_arr,function(key,th_obj){
			if(th_obj.visit_input===undefined||th_obj.visit_input==1){
				html +='<th>'+th_obj["name"];
				if(th_obj.edit_input==1&&th_obj.must==1){html +='<em>*</em>';}
				html +='</th>';
			}
		});
		html +='<th style="min-width:80px"></th>';
		html +='</tr>';
		var htmlstr = "";
		if(obj.rec!==undefined && obj.rec.length>0){
			var table_arr = obj.table_arr===undefined?new Array():obj.table_arr;
			for(var j =0;j<obj.rec.length;j++){
				var size = j;
				htmlstr +="<tr>";
				$.each(obj.rec[j],function(key,th_obj){
					var table_obj = table_arr[th_obj.th_key]===undefined?new Object():table_arr[th_obj.th_key];
					
					if(th_obj.type=="text"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val+"<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+th_obj.val+"'/>";
						htmlstr +='</td>';
					}
					else if(th_obj.type=="textarea"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						var tmp_val = th_obj.val.replace(/\n/g,'<br>');
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=tmp_val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="radio"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="checkbox"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="select"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="date"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="money"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val+"<input type='hidden' class='js_func_input' th_key='"+ key +"' value='"+th_obj.val+"'/>";
						if(th_obj.val!=''){
							htmlstr +='('+money_type[th_obj.money_type]+')';
						}
						if(th_obj.capitalized==1&&(th_obj.capitalized_val!==undefined&&th_obj.capitalized_val!='')){
							htmlstr +="<br>"+th_obj.capitalized_val+""
						}
						htmlstr +='</td>';
					}
					else if(th_obj.type=="people"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
					else if(th_obj.type=="dept"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(table_obj.visit_input===undefined||table_obj.visit_input==1){
							htmlstr+="<td>";
						}
						else{
							htmlstr+="<td class='css_not_visit'>";
						}
						htmlstr +=th_obj.val;
						htmlstr +='</td>';
					}
				});
				if(obj.edit_input==1){
					htmlstr +="<td style='min-width:80px;text-align:center;color:#80c269' onclick=\"get_table_td_edit('"+obj.input_key+"',"+j+")\">编辑";
				}
				htmlstr +="</td>";
				htmlstr +="</tr>";
			}
		}
		html +=htmlstr;
		html +='</table></div></div>';
	}
	else if(obj.type=="people"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.select_max ===undefined){obj.select_max=0;}
		if(obj.placeholder===undefined){obj.placeholder="";}
		if(obj.ids!==undefined){
			if(obj.ids!=""){
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+obj.val+'" ids="'+obj.ids+'" imgs="'+obj.imgs+'" class="js_input_val css_input_people js_input_people"  data-form=".container">'+obj.val+'</div><span class="right_"></span></div>';
			}
			else{
				var default_html = '<span class="css_edit_label">请选择'+obj.name+'</span>';
				if(obj.placeholder!==undefined&&obj.placeholder!=""){default_html='<span class="css_edit_label">'+obj.placeholder+'</span>'}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" class="js_input_val css_input_people js_input_people"  data-form=".container">'+default_html+'</div><span class="right_"></span></div>';
			}
		}
		else{
			if(obj.default_type==1){
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+$("#create_name").val()+'" ids="'+$("#create_id").val()+'" imgs="'+$("#create_url").val()+'" class="js_input_val css_input_people js_input_people" data-form=".container">'+$("#create_name").val()+'</div><span class="right_"></span></div>';
				obj.val = $("#create_name").val();
				obj.ids = $("#create_id").val();
				obj.imgs = $("#create_url").val();
			}
			else if(obj.default_type==2&&obj.default_val!==undefined&&obj.default_val!=""&&obj.default_val.length>0){
				var names = "";
				var ids = "";
				var imgs = "";
				
				for(var i=0;i<obj.default_val.length;i++){
					var user = obj.default_val[i];
					names+=user['name']+",";
					ids+=user['id']+",";
					imgs+=user['pic']+",";
				}
				if(names!=""){
					names = names.substring(0,names.length-1);
					ids = ids.substring(0,ids.length-1);
					imgs = imgs.substring(0,imgs.length-1);
				}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+names+'" ids="'+ids+'" imgs="'+imgs+'" class="js_input_val css_input_people js_input_people"  data-form=".container">'+names+'</div><span class="right_"></span></div>';
				obj.val = names;
				obj.ids = ids;
				obj.imgs = imgs;
			}
			else{
				var default_html ='<span class="css_edit_label">请选择'+obj.name+'</span>';
				if(obj.placeholder!==undefined&&obj.placeholder!=""){default_html='<span class="css_edit_label">'+obj.placeholder+'</span>'}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" class="js_input_val css_input_people js_input_people"  data-form=".container">'+default_html+'</div><span class="right_"></span></div>';
			}
		}
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	else if(obj.type=="dept"){
		if(obj.visit_input===undefined||obj.visit_input==1){
			html +='<div class="js_inputs css_inputs" >';
		}
		else{
			html +='<div class="js_inputs css_inputs css_not_visit" >';
		}
		if(obj.edit_input==0){
			html +='<div class="css_hidden_div"></div>'
		}
		html +='<div class="css_input_label" ><span>'+obj.name+'</span>';
		if(obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		if(obj.select_max ===undefined){obj.select_max=0;}
		if(obj.placeholder===undefined){obj.placeholder="";}
		if(obj.ids!==undefined){
			if(obj.ids!=""){
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+obj.val+'" ids="'+obj.ids+'" class="js_input_val css_input_dept js_input_dept"  data-form=".container">'+obj.val+'</div><span class="right_"></span></div>';
			}
			else{
				var default_html = '<span class="css_edit_label">请选择'+obj.name+'</span>';
				if(obj.placeholder!==undefined&&obj.placeholder!=""){default_html='<span class="css_edit_label">'+obj.placeholder+'</span>'}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'"  class="js_input_val css_input_dept js_input_dept"  data-form=".container">'+default_html+'</div><span class="right_"></span></div>';
			}
		}
		else{
			if(obj.default_type==1){
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+$("#create_dept_name").val()+'" ids="'+$("#create_dept_id").val()+'" class="js_input_val css_input_dept js_input_dept" data-form=".container">'+$("#create_dept_name").val()+'</div><span class="right_"></span></div>';
				obj.val = $("#create_dept_name").val();
				obj.ids = $("#create_dept_id").val();
			}
			else if(obj.default_type==2&&obj.default_val!==undefined&&obj.default_val!=""&&obj.default_val.length>0){
				var names = "";
				var ids = "";
				var imgs = "";
				for(var i=0;i<obj.default_val.length;i++){
					var dept = obj.default_val[i];
					names+=dept['name']+",";
					ids+=dept['id']+",";
				}
				if(names!=""){
					names = names.substring(0,names.length-1);
					ids = ids.substring(0,ids.length-1);
				}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'" names="'+names+'" ids="'+ids+'" class="js_input_val css_input_dept js_input_dept"  data-form=".container">'+names+'</div><span class="right_"></span></div>';
				obj.val = names;
				obj.ids = ids;
			}
			else{
				var default_html ='<span class="css_edit_label">请选择'+obj.name+'</span>';
				if(obj.placeholder!==undefined&&obj.placeholder!=""){default_html='<span class="css_edit_label">'+obj.placeholder+'</span>'}
				html +='<div class="css_input_info"><div select_max="'+obj.select_max+'" input_name="'+obj.name+'" placeholder="'+obj.placeholder+'" input_key = "'+obj.input_key+'"  class="js_input_val css_input_dept js_input_dept"  data-form=".container">'+default_html+'</div><span class="right_"></span></div>';
			}
		}
		html +='<div style="clear: both;"></div><div class="css_input_describe">';
		if(obj.describe===undefined){
			obj.describe = "";
		}
		html +=obj.describe.replace(/\n/g,'<br>')+'</div></div>';
	}
	return html;

}

function check_apply_data(data) {
	var inputs = data.form_vals;
	var is_check = true ;
	$.each(inputs,function(key,input){
		if (input.edit_input==1&&input.must == 1 && input.type != 'table' && !check_data(input.val, 'notnull')) {
			frame_obj.alert('请填写' + input.name);
			is_check =  false;
			return false;
		}
		if(input.edit_input==1&&input.type == 'text' && input.val!="" && input.format !=default_text_format ){
			if(input.format == number_text_format && !check_data(input.val, 'double')){
				frame_obj.alert(input.name+"需要填写数字");
				is_check = false;
				return false;
			}
			else if(input.format == email_text_format && !check_data(input.val, 'email')){
				frame_obj.alert(input.name+"需要填写邮箱");
				is_check = false;
				return false;
			}
			else if(input.format == phone_text_format && !check_data(input.val, 'phone')){
				frame_obj.alert(input.name+"需要填写电话号码");
				is_check = false;
				return false;
			}
			else if(input.format == telephone_text_format && !check_data(input.val, 'mobile')){
				frame_obj.alert(input.name+"需要填写手机号码");
				is_check = false;
				return false;
			}
			
		}
		//*
		if(input.edit_input==1&&input.type == 'table'){
			var rec = input.rec;
			if(input.must == 1 && (rec===undefined||rec.length==0)){
				frame_obj.alert(input.name+"需要添加明细");
				is_check = false;
				return false;
			}
		}
		if(input.type == 'table'&&input.rec!==undefined){
			var rec = input.rec;
			var table_arr = input.table_arr;
			for(var i=0;i<rec.length;i++){
				var items = rec[i];
				$.each(items,function(key,item){
					var table_obj = table_arr[key];
					if (input.visit_input==1&&table_obj.visit_input==1&&table_obj.edit_input==1&&item.must == 1 && item.type != 'table' && !check_data(item.val, 'notnull')) {
						frame_obj.alert('请填写'+input.name+"详情"+(i+1)+"的" + item.name);
						is_check = false;
						return false;
					}
					if( item.type == 'text' && item.val!="" && item.format !=default_text_format ){
						if(item.format == number_text_format && !check_data(item.val, 'double')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写数字");
							is_check = false;
							return false;
						}
						else if(item.format == email_text_format && !check_data(item.val, 'email')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写邮箱");
							is_check = false;
							return false;
						}
						else if(item.format == phone_text_format && !check_data(item.val, 'phone')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写电话号码");
							is_check = false;
							return false;
						}
						else if(item.format == telephone_text_format && !check_data(item.val, 'mobile')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写手机号码");
							is_check = false;
							return false;
						}
						
					}
				});
				if(is_check == false){
					return false;
				}
			}
		}
		//*/
	});
	if(is_check == false){
		return false;
	}
	
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	return true;
}

function init_cancel() {
	$('#cancel').unbind('click').bind('click', function() {
		window.location.href=$("#app-url").val()+"&m=mv_list&a=get_mine_list&is_finish=2";
	});
}

function get_input_data(){
	$.each(dataObj,function(key,obj){
		var obj_type = obj.type;
		var obj_key = obj.input_key;
		var edit_input = obj.edit_input;
		var visit_input = obj.visit_input===undefined?1:obj.visit_input;
		if(obj_type=="text"){
			var obj_val = $("input[input_key="+obj_key+"]").val();
			obj.val = obj_val;
		}
		else if(obj_type=="textarea"){
			var obj_val = $("textarea[input_key="+obj_key+"]").val();
			obj.val = obj_val;
		}
		else if(obj_type=="date"){
			var obj_val = $("input[input_key="+obj_key+"]").val();
			obj.val = obj_val;
		}
		else if(obj_type=="select"){
			var obj_val = $("select[input_key="+obj_key+"]").val();
			obj.val = obj_val;
		}
		else if(obj_type=="radio"){
			var obj_val = "";
			var obj_c_other = 0;
			$("input[input_key="+obj_key+"]:checked").each(function(){
				if($(this).hasClass("js_opts_radio")){
					obj_val+=$(this).val()+";";
				}
				if($(this).hasClass("js_other_cb")&&$(this).parent().parent().find(".js_other_text").val()!=""){
					obj_val+=$(this).parent().parent().find(".js_other_text").val()+";";
					obj_c_other = 1;
				}
			});
			if(obj_val!=""){
				obj_val = obj_val.substring(0,obj_val.length-1);
			}
			obj.val = obj_val;
			obj.c_other = obj_c_other;
		}
		else if(obj_type=="checkbox"){
			var obj_val = "";
			var obj_c_other = 0;
			$("input[input_key="+obj_key+"]:checked").each(function(){
				if($(this).hasClass("js_opts_checkbox")){
					obj_val+=$(this).val()+";";
				}
				if($(this).hasClass("js_other_cb")&&$(this).parent().parent().find(".js_other_text").val()!=""){
					obj_val+=$(this).parent().parent().find(".js_other_text").val()+";";
					obj_c_other = 1;
				}
			});
			if(obj_val!=""){
				obj_val = obj_val.substring(0,obj_val.length-1);
			}
			obj.val = obj_val;
			obj.c_other = obj_c_other;
		}
		else if(obj_type=="money"){
			var obj_val = $("input[input_key="+obj_key+"]").val();
			obj.val = obj_val;
			if(obj.capitalized==1){
				obj.capitalized_val = $("input[input_key="+obj_key+"]").parent().parent().find(".js_capitalized_input").html();
			}
		}
		else if(obj_type=="people"){
			var obj_vals ="";
			var obj_ids = "";
			var obj_imgs = "";
			obj_vals = $("div[input_key="+obj_key+"]").attr("names");
			obj_ids = $("div[input_key="+obj_key+"]").attr("ids");
			obj_imgs = $("div[input_key="+obj_key+"]").attr("imgs");
			obj.val = obj_vals;
			obj.ids = obj_ids;
			obj.imgs = obj_imgs;
		}
		else if(obj_type=="dept"){
			var obj_vals ="";
			var obj_ids = "";
			obj_vals = $("div[input_key="+obj_key+"]").attr("names");
			obj_ids = $("div[input_key="+obj_key+"]").attr("ids");
			obj.val = obj_vals;
			obj.ids = obj_ids;
		}
	});
}

function init_func_input(){
	calc.init(dataObj);
}