var work_type = $("#work_type").val();

$(document).ready(function() {
	inst_apply_html();
	$('span.js_a_cnt').html('(' + $('.process-appoval .js_a_div').length + ')');
	$('span.js_n_cnt').html('(' + $('.process-appoval .js_n_div').length + ')');
	$('span.js_w_cnt').html('(' + $('.process-appoval .js_w_div').length + ')');
	
	$('.file').css('max-width',(s_width*0.7)-20);
	init_table();
	init_upload();
	init_money();
	init_func_input();
	
	init_back_dlg();
	//init_edit_dlg();
	init_do_apply();
	init_send_back();
	init_call_back();
	init_do_del();
	init_do_press();
	init_do_notify();
	inst_approval();
	init_info_button();
	
	
	$('.writtenbut').unbind('click').bind('click', function() {
		$("#judgement").val($(this).val());
	});
	
	$('.select_work').click(function (){
		$('.select_work').addClass("icon-circle-blank");
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});
	
	$('.select_callback').click(function (){
		$('.select_callback').addClass("icon-circle-blank");
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});
	
	$(".workitem_list").unbind('click').bind('click', function() {
		var url = $("#app-url").val()+'&m=mv_open&a=get_mv_pic&formsetinst_id='+$("#formsetinst_id").val()+"&deal_type="+$("#deal_type").val();
		window.location.href=url;
	});
	
});


function inst_approval(){
	$('#pre_back').unbind('click').bind('click', function() {
		$("#disagreelabel").css("display","inline-block");
		$("#agreelabel").css("display","none");
		
		$("#pre_agree").css("display","none");
		$("#pre_back").css("display","none");
		$("#linksign_b").css("display","none");
		$("#back").css("display","block");
		$("#cancel").css("display","block");
		
		$('#cancel').unbind('click').bind('click', function() {
			$("#back").css("display","none");
			$("#agree").css("display","none");
			$("#cancel").css("display","none");
			$("#pre_agree").css("display","block");
			$("#pre_back").css("display","block");
			$("#linksign_b").css("display","block");
			$(".process-appoval").css("display","block");
			$(".process-words").css("display","block");
			$(".process-info").css("display","block");
			$(".process-judgement").css("display","none");
			$(".process-judgement").css("top","100%");
			$("#powerby").css("display","block");
		});
		
		$(".process-judgement").css("display","block");
		$("#powerby").css("display","none");
		$(".process-judgement").css("top","0");
		$(".process-appoval").css("display","none");
		$(".process-words").css("display","none");
		$(".process-info").css("display","none");
		$("#judgement").val('');
		if(work_type==0){
			$("#judgement").attr('placeholder','请输入退回意见……');
			$("#judgement").focus().val("不同意");
		}
		else{
			$("#judgement").attr('placeholder','请输入备注信息(选填)');
		}
		/*
		$('.process-judgement').animate({top: '0'}, 'slow', function() {
			$(".process-appoval").css("display","none");
			$(".process-words").css("display","none");
			$(".process-info").css("display","none");
			$("#judgement").attr('placeholder','请输入退回意见……');
			$("#judgement").val('');
			setTimeout("$('#judgement').focus()", 3000 );
			
		});
		*/
		//$("#judgement").focus();
	});
	$('#pre_agree').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if(!check_apply_data(data,1)){
			return false;
		}
		$("#disagreelabel").css("display","none");
		$("#agreelabel").css("display","inline-block");
		
		$("#pre_agree").css("display","none");
		$("#pre_back").css("display","none");
		$("#agree").css("display","block");
		$("#cancel").css("display","block");
		$('#cancel').unbind('click').bind('click', function() {
			$("#back").css("display","none");
			$("#agree").css("display","none");
			$("#cancel").css("display","none");
			$("#pre_agree").css("display","block");
			$("#pre_back").css("display","block");
			$(".process-appoval").css("display","block");
			$(".process-words").css("display","block");
			$(".process-info").css("display","block");
			$(".process-judgement").css("display","none");
			$(".process-judgement").css("top","100%");
			$("#powerby").css("display","block");
		});
		$(".process-judgement").css("display","block");
		$("#powerby").css("display","none");
		$(".process-judgement").css("top","0");
		$(".process-appoval").css("display","none");
		$(".process-words").css("display","none");
		$(".process-info").css("display","none");
		$("#judgement").val('');
		if(work_type==0){
			$("#judgement").attr('placeholder','请输入同意意见……');
			$("#judgement").focus().val("同意");
		}
		else{
			$("#judgement").attr('placeholder','请输入备注信息(选填)');
		}
		/*
		$('.process-judgement').animate({top: '0'}, 'slow', function() {
			$(".process-proc").css("display","none");
			$(".process-words").css("display","none");
			$(".process-info").css("display","none");
			$("#judgement").attr('placeholder','请输入同意意见……');
			$("#judgement").val('');
			setTimeout("$('#judgement').focus()", 3000 );
			//$("#judgement").focus();
		});
		*/
		
	});
	$('#linksign_b').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if(!check_apply_data(data,1)){
			return false;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), to_linksign,undefined,undefined,undefined,'保存中……');
	});
	
}

function to_linksign(data){
	var data = new Object();
	data.workitem_id = $('#workitem_id').val();
	data.formsetinst_id = $('#formsetinst_id').val();
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	frame_obj.do_ajax_post(undefined, 'open_linksign', JSON.stringify(data),get_redirect_url ,undefined,undefined,undefined,'保存中……');
	
	function get_redirect_url(data){
		if(data["errcode"]===0){
			location.href=data["redirect_url"];
		}
		else{
			frame_obj.alert(data.errmsg);
		}
	}
}

function read_uploaded() {
	$('.old-img-upload-item').each(function() {
		$(this).wximgupload(wx_img_upload_opt)
			.wximgupload('init');
		var img_paths = $(this).children(".edit_img_path").val();
		if(img_paths!=""){
			img_paths = img_paths.split(",");
			$(this).find(".wx-img-list").eq(0).append('<span class="wx-img-list-item"></span>');
			for(var i=0;i<img_paths.length;i++){
				var img_path = img_paths[i];
				var media_url = $("#media-url").val();
				$(this).find(".wx-img-list-item").eq(0).append('<img  src="'+media_url+img_path+'" />');
			}
		}
		
		$(this).wximgupload(wx_img_upload_opt)
			.wximgupload('read')
			.wximgupload('show_icon')
			.wximgupload('bind_preview');
		$(this).removeClass('old-img-upload-item');
	});
	$('.wx-img-add-img').parent().css('display', 'inline');
}

function init_edit_dlg() {
	$('#edit').unbind('click').bind('click', function() {
		window.location.href=$('#app-url').val() + '&m=mv_open&a=detail&workitem_id='+$("#workitem_id").val()+"&is_edit=true";
	});
}


function init_back_dlg() {
	$('#back').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if (check_back_data(data)) {
			$('#back-dlg').modal('show');
		}
	});
}

function init_send_back() {
	$('#back-ok').unbind('click').bind('click', function() {
		var send_type=0;
		$(".select_work").each(function(){
			if(!$(this).hasClass("icon-circle-blank")){
				send_type = $(this).attr("val");
			}
		});
		if(send_type==1){
			var msg = "开始步骤";
			var method = 'callback_start';
		}
		else{
			var msg = "上一步骤";
			var method = 'callback_pre';
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		if (check_back_data(data)) {
			var btn = $(this);
			$('#back-dlg').modal('hide');
			//frame_obj.comfirm('确定要驳回当前工作项到'+msg+'吗？', function() {});
			
			frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback_complete,undefined,undefined,undefined,'退回中……')
				}
			);
		}
	});
}



function init_call_back() {
	$('#callback-ok').unbind('click').bind('click', function() {
		var send_type=0;
		$(".select_callback").each(function(){
			if(!$(this).hasClass("icon-circle-blank")){
				send_type = $(this).attr("val");
			}
		});
		if(send_type==1){
			var msg = "退回步骤";
			var method = 'sendback';
			var callback = sendback_complete;
		}
		else{
			var msg = "下一步骤";
			var method = 'save_workitem';
			var callback = apply_complete;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		$('#callback-dlg').modal('hide');
		if($("#is_start").val()==0){//开始步骤不用填审批信息
			if (!check_apply_data(data)) {
				return false;
			}
			var btn = $(this);
			//frame_obj.comfirm('确定要发送当前工作项到'+msg+'吗？', function() {});
			if(send_type==1){
				frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……')
				});
			}
			else{
				frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……');
			}
		}
		else{
			if (!check_apply_data(data,1)) {
				return false;
			}
			var btn = $(this);
			//frame_obj.comfirm('确定要发送当前工作项到'+msg+'吗？', function() {});
			if(send_type==1){
				frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……')
				});
			}
			else{
				frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……');
			}
		}
		
	});
}


//申请
function init_do_apply() {
	$('#agree').unbind('click').bind('click', function() {
		var is_returnback = $("#is_returnback").val();
		if(is_returnback==1){
			$('#callback-dlg').modal('show'); 
		}
		else{
			$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
			var data = get_apply_data();
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要发送该工作项到下一步吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'发送中……');
			}
		}
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		//alert($("#app-url").val()+"&m=mv_send&a=send_page&workitemid="+data.workitem_id);
		location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id;
	} else {
		frame_obj.alert(data.errmsg);
	}
}

//删除
function init_do_del() {
	$('#del').unbind('click').bind('click', function() {
		var data = new Object();
		data.workitem_id = $("#workitem_id").val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.comfirm('确定要删除该工作项？', function() {
			frame_obj.do_ajax_post(btn, 'del_workitem', JSON.stringify(data), del_complete,undefined,undefined,undefined,'删除中……');
		});
	});
}

function del_complete(data) {
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_mine_list);
		to_mine_list();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}



function get_apply_data() {
	var data = new Object();
	
	data.form_id = "";
	data.work_id = "";
	data.form_name = "";
	data.judgement = $("#judgement").val()?$("#judgement").val():"";
	data.workitem_id = $('#workitem_id').val();
	data.formsetInst_id = $('#formsetinst_id').val();
	data.file_upload_type = $("#file_upload_type").val();//显示附件的状态
	
	data.form_vals = dataObj;
	
	//附件信息
	data.files = new Array();
	var handler_id = $("#handler_id").val();
	$('.js_file_div').each(function() {
		var file_node_id = $(this).attr("file_node_id");
		if(file_node_id == $("#work_node_id").val()&&$(this).attr("file_create_id")==handler_id){
			var item = new Object();
			item.file_name = $(this).attr("file_name");
			item.file_key = $(this).attr("file_key");
			item.file_url = $(this).attr("file_url");
			item.file_ext = $(this).attr("file_ext");
			item.node_id = $(this).attr("file_node_id");
			data.files.push(item);
		}
	});
	
	return data;
}

function check_apply_data(data,is_cj) {
	var inputs = data.form_vals;
	var is_check = true ;
	$.each(inputs,function(key,input){
		if (input.edit_input==1&&input.must == 1 && input.type != 'table' && !check_data(input.val, 'notnull')) {
			frame_obj.alert('请填写' + input.name);
			is_check =  false;
			return false;
		}
		if(input.edit_input==1&&input.type == 'text' && input.val!="" && input.format !=default_text_format ){
			if(input.format == number_text_format && !check_data(input.val, 'double')){
				frame_obj.alert(input.name+"需要填写数字");
				is_check = false;
				return false;
			}
			else if(input.format == email_text_format && !check_data(input.val, 'email')){
				frame_obj.alert(input.name+"需要填写邮箱");
				is_check = false;
				return false;
			}
			else if(input.format == phone_text_format && !check_data(input.val, 'phone')){
				frame_obj.alert(input.name+"需要填写电话号码");
				is_check = false;
				return false;
			}
			else if(input.format == telephone_text_format && !check_data(input.val, 'mobile')){
				frame_obj.alert(input.name+"需要填写手机号码");
				is_check = false;
				return false;
			}
			
		}
		//*
		if(input.edit_input==1&&input.type == 'table'){
			var rec = input.rec;
			if(input.must == 1 &&(rec===undefined||rec.length==0)){
				frame_obj.alert(input.name+"需要添加明细");
				is_check = false;
				return false;
			}
		}
		if(input.type == 'table'&&input.rec!==undefined&&input.rec.length>0){
			var rec = input.rec;
			var table_arr = input.table_arr;
			for(var i=0;i<rec.length;i++){
				var items = rec[i];
				$.each(items,function(key,item){
					var table_obj = table_arr[key];
					if (input.visit_input==1&&table_obj.visit_input==1&&table_obj.edit_input==1&&item.must == 1 && item.type != 'table' && !check_data(item.val, 'notnull')) {
						frame_obj.alert('请填写'+input.name+"详情"+(i+1)+"的" + item.name);
						is_check = false;
						return false;
					}
					if( item.type == 'text' && item.val!="" && item.format !=default_text_format ){
						if(item.format == number_text_format && !check_data(item.val, 'double')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写数字");
							is_check = false;
							return false;
						}
						else if(item.format == email_text_format && !check_data(item.val, 'email')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写邮箱");
							is_check = false;
							return false;
						}
						else if(item.format == phone_text_format && !check_data(item.val, 'phone')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写电话号码");
							is_check = false;
							return false;
						}
						else if(item.format == telephone_text_format && !check_data(item.val, 'mobile')){
							frame_obj.alert(input.name+"详情"+(i+1)+"的" + item.name+"需要填写手机号码");
							is_check = false;
							return false;
						}
						
					}
				});
				if(is_check == false){
					return false;
				}
			}
		}
		//*/
	});
	if(is_check == false){
		return false;
	}
	
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	
	if($("#is_start").val()==0&&is_cj===undefined){//开始步骤不用填审批信息
		if(!check_data(data.judgement, 'notnull')&&work_type==0){
			frame_obj.alert('请填写审批意见');
			return false;
		}
	}
	return true;
}


function check_file_data(data){
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	return true;
}

function check_back_data(data) {
	if (!check_data(data.judgement, 'notnull')&&work_type==0) {
		frame_obj.alert('请填写审批意见');
		return false;
	}
	return true;
}


//退回后返回，多个步骤的时候，弹出选择退回
function callback_complete(data){
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_list);
		to_list();
	}
	else if (data.errcode == 1) {//弹出选择退回
		//alert($('#callback-cancel').attr("class"));
		$('#back-dlg').modal('hide');
		
		$("#callback_select-dlg").modal('show');
		var callback_workitem = data.callback_workitem;
		var html="";
		for(var i=0;i<callback_workitem.length;i++){
			var date = new Date(callback_workitem[i]["complete_time"]*1000);
			var year = date.getFullYear();
			var month = (date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : "0"+ (date.getMonth() + 1);
			var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
			var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			var datestr = year+"-"+month+"-"+day+" "+hour+":"+minute
			html+="<span class='icon-ok-sign icon-circle-blank select_people' style='font-size: 25px;color: green' workitem_id='"+callback_workitem[i]["id"]+"' workitem_name='"+callback_workitem[i]["workitem_name"]+"' handler_name='"+callback_workitem[i]["handler_name"]+"'></span>&nbsp;&nbsp;["+callback_workitem[i]["workitem_name"]+"]"+callback_workitem[i]["handler_name"]+"于"+datestr+"发送<br>";
		}
		
		$("#callback_select-dlg .modal-body").html(html);
		
		$('.select_people').click(function (){
			$('.select_people').addClass("icon-circle-blank");
			$(this).toggleClass("icon-circle-blank");
			$(this).css("color","green");
		});
		$('#callback_select-ok').unbind('click').bind('click', function() {
			var handler_name ="";
			var workitem_name = "";
			var workitem_id = "";
			var count = 0;
			$(".select_people").each(function(){
				if(!$(this).hasClass("icon-circle-blank")){
					workitem_id = $(this).attr("workitem_id");
					workitem_name = $(this).attr("workitem_name");
					handler_name = $(this).attr("handler_name");
					count++;
				}
			});
			if(count!=1){
				frame_obj.alert('请选择退回的工作项');
				return false;
			}
			var data = new Object();
			data.workitem_id = $("#workitem_id").val();
			data.callback_workitem_id = workitem_id;
			data.judgement = $("#judgement").val()?$("#judgement").val():"";
			var btn = $(this);
			$("#callback_select-dlg").modal('hide');
			//frame_obj.comfirm('确定要发送当前工作项到上一步骤 '+handler_name+' 吗？', function() {});
			frame_obj.do_ajax_post(btn, "callback_select", JSON.stringify(data), sendback_complete,undefined,undefined,undefined,'发送中……');
		});
	}
	else{
		frame_obj.alert(data.errmsg,'');
	}
}


function sendback_complete(data){
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_list);
		to_list();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}


//催办
function init_do_press() {
	$('#press').unbind('click').bind('click', function() {
		var data = new Object();
		data.formsetinst_id = $("#formsetinst_id").val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		//frame_obj.comfirm('确定要催办该工作项？', function() {});
		frame_obj.do_ajax_post(btn, 'press_workitem', JSON.stringify(data), press_complete,undefined,undefined,undefined,'催办中……');
	});
}

function press_complete(data) {
	frame_obj.alert(data.errmsg);
}


//知会
function init_do_notify() {
	$('#notify').unbind('click').bind('click', function() {
		//frame_obj.comfirm('确定要知会该工作项？', function() {});
		location.href=$("#app-url").val()+"&m=mv_open&a=notify_page&formsetinst_id="+$("#formsetinst_id").val()+"&workitem_id="+$('#workitem_id').val();
	});
}


function to_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_do_list';
}

function to_mine_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_mine_list';
}

function save_workitem(){
	if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
		mupload.mupload('_showTips', '当前还有文件正在上传');
		return;
	}
	$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
	var data = get_apply_data();
	frame_obj.do_ajax_post(undefined, 'save_workitem', JSON.stringify(data), function(){},undefined,undefined,undefined);
}

