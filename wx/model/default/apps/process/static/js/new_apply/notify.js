
$(document).ready(function(){
	/*
	$('.send-add').unbind('click').bind('click',function(){
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		$(".surebutton").css("display","block");
		
		$("#sure_div").val($(this).parent().attr("id"));
		
		init_contact();
		
		
		$(this).parent().parent().find('.select_people').each(function(){
			var people_id = $(this).attr("i");
			$(".icon-circle-blank[d='"+people_id+"']").eq(0).click();
		});
		
	});
	//*/
	init_contact_selector();
	
	$(".surebutton").css("display","none");
	
	$(".next_button").click(function (){
		var count = 0;
		var data = new Object();
		data.formsetinst_id = $("#formsetinst_id").val();
		data.receivers = new Array();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		
		var receiver_name = "";
		$(".side-content").each(function(){
			$(this).find(".select_people").each(function(){
				if(!$(this).hasClass("icon-circle-blank")){
					var receiver = new Object();
					receiver.receiver_id = $(this).attr("i");
					receiver.receiver_name = $(this).attr("n");
					data.receivers.push(receiver);
					count++;
					receiver_name = receiver_name+($(this).attr("n")+",");
				}
				
			});
		});
		
		if(receiver_name!=""){
			receiver_name = receiver_name.substring(0,receiver_name.length-1);
		}
		
		if(count==0){
			frame_obj.alert('至少选择一个知会人！');
			return false;
		}
		
		frame_obj.comfirm('确定要将该流程知会给 '+receiver_name+" ?",function(){
			//$(".load_img").css("display","block");
			frame_obj.do_ajax_post($(this), 'send_notify', JSON.stringify(data),return_msg,undefined,undefined,undefined,'知会中……');
		});
	});
	
	$("#pre").click(function (){
		window.location.href=$('#page_url').val() + '&a=detail&workitem_id='+$("#workitem_id").val();
	});
	
});


//初始化(多个)选人控件
function init_contact_selector() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		$(this).parents('.side-content').find('.select_people').each(function() {
			var selected_id = $(this).attr('i');
			var selected_name = $(this).attr('n');
			selected[selected_id] = {id: selected_id, name: selected_name};
		});
		var instance_id = $(this).attr("id");
		var opt = {
			type		: 2,
			split		: false,
			selected	: selected,
			before		: function() {
				$("#sure_div").val(instance_id);
			},
			ok			: function(selected) {
				var sure_div = $("#sure_div").val();
				var html="";
				var type = $('#add-type').val();
				
				for (var i in selected) {
					if (!selected[i])	continue;
					
					html +='<div class="children-report">' + 
							'<div class=face>' +
								'<img src="' + selected[i].pic + '" onerror="this.src=\'static/image/face.png\'">' +
							'</div>' +
							'<div class="message">' + 
								'<div style="display: inline-block;">'+
									selected[i].name + '<span style="color: green;font-size: 12px">(可选)</span>' +
								'</div>' +
								'<div style="text-align: right;float: right;">' +
									'<div style="vertical-align: middle;">' +
										'<div style="display: inline-block;vertical-align: middle;">' + 
											'<span class="icon-ok-sign select_people is_select" i="' + selected[i].id + '" n="' + selected[i].name + '" style="font-size: 25px;color: green"></span>' +
										'</div>' +
										'<div style="font-size: 12px;height: 100%;display: inline-block;vertical-align: middle;height: 30px;padding-left: 10px"></div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>';
				}

				$('#'+sure_div).parent().children(".children-report").remove();
				$('#'+sure_div).before(html);
				$(c_search_form).css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
				
				$('.is_select').unbind('click').bind('click',function(){
					$(this).toggleClass("icon-circle-blank");
				});
			}
		}
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

function return_msg(data){
	//$(".load_img").css("display","none");
	//frame_obj.alert(data.errmsg,'',to_list);
	to_list()
}

function to_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_do_list&is_finish=1';
}

function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}











	
	

