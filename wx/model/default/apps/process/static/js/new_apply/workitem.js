var page = 1;
$(document).ready(function() {
	//初始化列表的外边距
	var head_height = $(".head").height();
	$("#rec-list").css("margin-top",(head_height+12)+"px");
	init_scroll_load('.container', '#rec-list', load_lists);
});

//加载列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.is_finish = $('.active').attr('t');
	data.page = page;
	data.formsetinit_id = $('#formsetinit_id').val();
	
	$('#ajax-url').val($('#get_list_post').val());
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'get_workitem_list', JSON.stringify(data), show_list);
}


//显示审批列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		var time_text = '';
		var finish_text = '';
		var name = '';
		var finish_class = '';
		var time_div_class = 'time_div';
		
		if(item["complete_time"]=="" || item["complete_time"] == null){
			time_text = '<span style="color: red">未完成</span>';
		}else{
			time_text = time2date(item["complete_time"],'yyyy-MM-dd HH:mm');
		}
		
		if(item['receiver_name'] != "" && item["receiver_name"] != null){
			name = '<p style="font-size: 12px;"> 发送人:<span style="font-weight: bold;color:#333;">'+item['receiver_name']+'</span></p>';
			time_div_class += ' time_div_3';
		}
		
		switch(Number(item['state'])){
			case 1:
				finish_text = '新收到';
				finish_class = 'tag-orange';
				break;
			case 2:
				finish_text = '已阅读';
				finish_class = 'tag-blue';
				break;
			case 3:
				finish_text = '已保存';
				finish_class = 'tag-green';
				break;
			case 4:
				finish_text = '等待中';
				finish_class = 'tag-grep';
				break;
			case 6:
				finish_text = '已终止';
				finish_class = 'tag-orange';
				break;
			case 7:
				finish_text = '已处理';
				finish_class = 'tag-green';
				break;
			case 8:
				finish_text = '已退回';
				finish_class = 'tag-blue';
				break;
			case 9:
				finish_text = '已通过';
				finish_class = 'tag-grep';
				break;
		}
		
		if(item["target_node"]==null||item["target_node"]==""||item["target_node"]=="null"){
			item["target_node"]="";
		}
		
		html += '<div class="list-item"> ' +
					'<div class="item-head">'+
						'<span class="type"> '+item["workitem_name"]+' </span>'+
					'</div>'+
					'<div class="item-body">'+
					'<div class="info">'+
						'<img src="'+item['pic_url']+'">'+
						'<div class="item-detail">'+
							'<p style="font-size: 12px;"> 耗时:<span style="color: red;">'+item["consuming_time"]+'</span>  </p>'+
							name+
							'<p style="font-size: 12px;"> 处理人:<span style="font-weight: bold;color:#333;">'+item['handler_name']+'</span></p>'+
						'</div>'+
						'<span class="item-state '+finish_class+'">'+
						finish_text+
						'</span>'+
					'</div>'+
					'<div  class="'+ time_div_class +'">'+
						'<span>'+
						time_text+
						'</span>'+
					'</div>' +
					'</div>'+
					'</div>';
	
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}


function open_mv_pic(){
	var url = $("#app-url").val()+'&m=mv_open&a=get_mv_pic&formsetinst_id='+$("#formsetinit_id").val()+"&deal_type="+$("#deal_type").val();
	if($("#deal_type").val()==1){
		url+="&workitem_id="+$("#workitem_id").val();
	}
	if($("#deal_type").val()==3){
		url+="&notify_id="+$("#notify_id").val();
	}
	window.location.href=url;
} 

function return_deal(){
	var deal_type = $("#deal_type").val();
	var url = $("#app-url").val()+'&m=mv_open&a=mine_detail&formsetinit_id='+$("#formsetinit_id").val();
	if(deal_type==1){
		url = $("#app-url").val()+'&m=mv_open&a=detail&workitem_id='+$("#workitem_id").val();
	}
	if(deal_type==3){
		url = $("#app-url").val()+'&m=mv_open&a=notify_detail&notify_id='+$("#notify_id").val();
	}
	window.location.href=url;
}

