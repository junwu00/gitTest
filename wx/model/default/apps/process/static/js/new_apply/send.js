
$(document).ready(function(){
	var is_send = $("#is_send").val();
	
	
	$('.classify_tab').click(function(){
		if($(this).find(".icon-hand-right").eq(0).hasClass("node_css")){
			return false;
		}
		else{
			$('.side-content').css("display","none");
			$(this).parent().children('.side-content').css('width','100%');
			$(this).parent().children('.side-content').slideToggle();
			$(".icon-hand-right").removeClass("node_css");
			$(this).find(".icon-hand-right").eq(0).toggleClass("node_css");
		}
		//get_open_list($(this).attr("classify_id"),$(this)); 
	});
	/*
	$('.send-add').parent().unbind('click').bind('click',function(){
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		$(".surebutton").css("display","block");
		
		$("#sure_div").val($(this).attr("id"));
		
		init_contact();
		
		
		$('.send-add').parent().parent().find('.select_people').each(function(){
			var people_id = $(this).attr("i");
			$(".icon-circle-blank[d='"+people_id+"']").eq(0).click();
		});
		
	});
	//*/
	init_contact_selector();
	
	$(".surebutton").css("display","none");
	
	
	$('.is_select').click(function (){
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});
	
	$(".next_button").click(function (){
		var count = 0;
		var data = new Object();
		data.workitemid = $("#workitemid").val();
		data.work_nodes = new Array();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		
		var receiver_name = "";
		var is_end = $(".node_css").parent().eq(0).attr("e");
		
		$(".node_css").parent().parent().parent().parent().find(".side-content").eq(0).each(function(){
			var work_node = new Object();
			work_node.id = $(this).attr("w_id");
			work_node.receiver_arr = new Array();
			if(is_end!=1){
				$(this).find(".select_people").each(function(){
					if(!$(this).hasClass("icon-circle-blank")){
						var receiver = new Object();
						receiver.receiver_id = $(this).attr("i");
						receiver.receiver_name = $(this).attr("n");
						work_node.receiver_arr.push(receiver);
						count++;
						receiver_name = receiver_name+($(this).attr("n")+",");
					}
					
				});
			}
			if(work_node.receiver_arr.length>0||is_send=="TRUE"||is_end==1){
				data.work_nodes.push(work_node);
			}
			
		});
		
		if(receiver_name!=""){
			receiver_name = receiver_name.substring(0,receiver_name.length-1);
		}
		
		if(count==0&&is_send=="FALSE"&&is_end!=1){
			frame_obj.alert('至少选择下一步接收人！');
			return false;
		}
		
		if(is_send=="FALSE"){
			var content = '确定要将该流程发送到 '+$(".node_css").parent().attr('n')+" 步骤,接收人为 "+receiver_name+" ?";
			if(is_end==1){
				content = '确定要将该流程发送到 '+$(".node_css").parent().attr('n')+" 步骤?";
			}
			frame_obj.comfirm(content,function(){
				//$(".load_img").css("display","block");
				frame_obj.do_ajax_post($(this), 'send_workitem', JSON.stringify(data),return_msg,undefined,undefined,undefined,'发送中……');
			});
		}
		else{
			//$(".load_img").css("display","block");
			frame_obj.do_ajax_post($(this), 'send_workitem', JSON.stringify(data),return_msg,undefined,undefined,undefined,'发送中……');
		}
		
		
	});
	
	if(is_send=="TRUE"){
		$(".next_button").click();
	}
	
	$("#pre").click(function (){
		if($("#list").val()==2){
			window.location.href=$('#page_url').val() + '&a=mine_detail&formsetinit_id='+$("#formsetInst_id").val();
		}
		else if($("#list").val()==1){
			window.location.href=$('#page_url').val() + '&a=mine_detail&formsetinit_id='+$("#formsetInst_id").val();
		}
		else{
			window.location.href=$('#page_url').val() + '&a=detail&workitem_id='+$("#workitemid").val();
		}
	});
});


//初始化(多个)选人控件
function init_contact_selector() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		var selected = {};
		$(this).parents('.side-content').find('.select_people').each(function() {
			var selected_id = $(this).attr('i');
			var selected_name = $(this).attr('n');
			selected[selected_id] = {id: selected_id, name: selected_name};
		});
		var instance_id = $(this).attr("id");
		var opt = {
			type		: 2,
			split		: false,
			selected	: selected,
			before		: function() {
				$("#sure_div").val(instance_id);
			},
			ok			: function(selected) {
				var sure_div = $("#sure_div").val();
				var html="";
				var type = $('#add-type').val();
				
				for (var i in selected) {
					if (!selected[i])	continue;
					
					html +='<div class="children-report">' + 
							'<div class=face>' +
								'<img src="' + selected[i].pic + '" onerror="this.src=\'static/image/face.png\'">' +
							'</div>' +
							'<div class="message">' + 
								'<div style="display: inline-block;">'+
									selected[i].name + '<span style="color: green;font-size: 12px">(可选)</span>' +
								'</div>' +
								'<div style="text-align: right;float: right;">' +
									'<div style="vertical-align: middle;">' +
										'<div style="display: inline-block;vertical-align: middle;">' + 
											'<span class="icon-ok-sign select_people is_select" i="' + selected[i].id + '" n="' + selected[i].name + '" style="font-size: 25px;color: green"></span>' +
										'</div>' +
										'<div style="font-size: 12px;height: 100%;display: inline-block;vertical-align: middle;height: 30px;padding-left: 10px"></div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>';
				}

				$('#'+sure_div).parent().children(".children-report").remove();
				$('#'+sure_div).before(html);
				$(c_search_form).css({display: 'block'});
				$('#c-search-container').css({display: 'none'});
				
				$('.is_select').unbind('click').bind('click',function(){
					$(this).toggleClass("icon-circle-blank");
				});
			}
		}
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

function return_msg(data){
	//$(".load_img").css("display","none");
	//frame_obj.alert(data.errmsg,'',to_list);
	to_list();
}

function to_list(){
	if($("#list").val()==2){
		//window.location.href=$('#page_url').val();
		window.location.href=$('#open_url').val() + '&a=get_mine_list';
	}
	else if($("#list").val()==1){
		window.location.href=$('#open_url').val() + '&a=get_mine_list';
	}
	else{
		window.location.href=$('#open_url').val() + '&a=get_do_list&is_finish=0';
	}
}
//window.location.href=$('#app-url').val() + '&m=mv_list&a=get_do_list&is_finish=0';

function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}
