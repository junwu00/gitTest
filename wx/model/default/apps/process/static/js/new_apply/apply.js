

$(document).ready(function() {
	var pid = getQueryString('pid');
	if(pid){
		var storage_key = 'apply_process_'+pid;
		$storage.init({
			key:storage_key,
			init_data : function(data){
				if(data != null){
					var tmp_dataObj = JSON.parse(data);
					var proc_form_id = tmp_dataObj["form_id"];
					if(proc_form_id!==undefined&&proc_form_id==$("#proc_form_id").val()){
						dataObj = tmp_dataObj;
					}
					$('#js_inputs_div').html("");
				}
			},
			save_data : function(){
				get_input_data();
				dataObj["form_id"] = $("#proc_form_id").val();
				return dataObj;
			}
		});
	}

	inst_apply_html();		
	init_date();
	init_money();
	init_other();
	init_table();
	init_people();
	init_dept();
	init_func_input();
	
	init_cancel();
	init_do_apply();
	init_upload();
});

//获取URL参数
function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
	$("#file_num").html('(' + $('.file_info').length + ')');
}





//申请
function init_do_apply() {
	$('#apply').unbind('click').bind('click', function() {
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		if(cheak_formset_name()){
			var data = get_apply_data();
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要提交该报申请吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'发送中……');
			}
		}
	});
	$('#save').unbind('click').bind('click', function() {
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		//if (check_apply_data(data)) {
		if(cheak_formset_name()){
			var data = get_apply_data();
			var btn = $(this);
			//frame_obj.comfirm('确定要提交该报申请吗？', function() {});
			frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), save_complete,undefined,undefined,undefined,'保存中……');
		}
		//}
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		$storage.clear();
		window.location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id+"&formsetInst_id="+data.formsetInst_id+"&list=2";
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function save_complete(data) {
	if (data.errcode == 0) {
		$storage.clear();
		//frame_obj.alert("保存成功",'',to_open);
		to_open();
		
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function to_open(){
	window.location.href=$("#mine_list_url").val();
}

function to_lists() {
	location.href = $('#app-url').val() + '&m=mv_list&a=get_mine_list';
}

function get_apply_data() {
	var data = new Object();
	
	data.form_id = $('#proc_form_id').val();
	data.work_id = $('#proc_work_id').val();
	data.form_name = get_formset_name();
	data.judgement = "";
	data.workitem_id = "";
	data.formsetInst_id = "";
	
	get_input_data();
	
	data.form_vals = dataObj;
	
	//附件信息
	data.files = new Array();
	$('.js_file_div .file_info').each(function() {
		var item = new Object();
		item.file_name = $(this).attr("fname");
		item.file_key = $(this).attr("filekey");
		item.file_url = $(this).attr("furl");
		item.file_ext = $(this).attr('fext');
		data.files.push(item);
	});
	
	return data;
}



function init_cancel() {
	$('#cancel').unbind('click').bind('click', function() {
		window.location.href=$("#app-url").val()+"&m=mv_open";
	});
}

function formset_name_change(obj){
	var input_val = $(obj).val();
	$("input[forset_key=7]").val(input_val);
}

function cheak_formset_name(){
	if($("#forset_input")!==undefined&&$("#forset_input").val()==""){
		frame_obj.alert("请补全流程名称");
		return false;
	}
	return true;
}
function get_formset_name(){
	var form_name="";
	$(".js_formset_val").each(function(){
		form_name+=$(this).val()+"-";
	});
	if(form_name!=""){
		form_name = form_name.substring(0,form_name.length-1);
	}
	return form_name;
}