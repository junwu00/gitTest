
$(document).ready(function() {
	inst_apply_html();
	
	init_date();
	init_money();
	init_other();
	init_table();
	init_people();
	init_dept();
	init_func_input();
	
	init_cancel();
	init_do_apply();
	init_do_del();
	init_upload();
	
});



//申请
function init_do_apply() {
	$('#apply').unbind('click').bind('click', function() {
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		var is_returnback = $("#is_returnback").val();
		if(is_returnback==1){//退回
			var data = get_apply_data();
			if (check_apply_data(data)) {
				$('#callback-dlg').modal('show'); 
			}
		}
		else{
			$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
			var data = get_apply_data();
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要发送该流程到下一步吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'发送中……');
			}
		}
	});
	$('#save').unbind('click').bind('click', function() {
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		//if (check_apply_data(data)) {
		var btn = $(this);
			//if(confirm('确定要保存该表单吗？')){}
		frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), to_return_back_list,undefined,undefined,undefined,'保存中……');
		//}
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		window.location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id+"&formsetInst_id="+data.formsetInst_id+"&list=1";
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function to_list() {
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_do_list&is_finish=0';
}


function to_return_back_list(data){
	if (data.errcode == 0) {
		var url = $('#return_list_url').val();
		if(url.indexOf("get_mine_list")>-1){
			url+="&is_finish=2";
		}
		window.location.href=url;
	}else {
		frame_obj.alert(data.errmsg);
	}
}

function to_return_back_list_not_data(){
	var url = $('#return_list_url').val();
	if(url.indexOf("get_mine_list")>-1){
		url+="&is_finish=2";
	}
	window.location.href=url;
}

function get_apply_data() {
	var data = new Object();
	
	data.form_id = $('#proc_form_id').val();
	data.work_id = $('#proc_work_id').val();
	data.form_name = $('#form_name').val();
	data.workitem_id = $('#workitem_id').val();
	data.formsetInst_id = $('#formsetInst_id').val();
	data.judgement = "";
	
	get_input_data();
	
	data.form_vals = dataObj;
	
	//附件信息
	data.files = new Array();
	$('.js_file_div .file_info').each(function() {
		var item = new Object();
		item.file_name = $(this).attr("fname");
		item.file_key = $(this).attr("filekey");
		item.file_url = $(this).attr("furl");
		item.file_ext = $(this).attr('fext');
		data.files.push(item);
	});
	
	return data;
}

//删除
function init_do_del() {
	$('#del').unbind('click').bind('click', function() {
		var data = new Object();
		data.workitem_id = $('#workitem_id').val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.comfirm('确定要删除该工作项？', function() {
			frame_obj.do_ajax_post(btn, 'del_workitem', JSON.stringify(data), del_complete,undefined,undefined,undefined,'删除中……');
		});
	});
}

function del_complete(data) {
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_return_back_list_not_data);
		to_return_back_list_not_data();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}




function download_file(id,url){
	/*
	var u = url.toLowerCase();
	if(u.indexOf('.jpg') >0 || u.indexOf('.jpeg') >0 || u.indexOf('.png') >0){
		frame_obj.show_img($('#media-url').val()+url);
	}else{
		var formsetinst_id = $("#formsetInst_id").val();
		var data = new Object();
		data.id = id;
		data.formsetinst_id = formsetinst_id;
		data.workitem_id = $("#workitem_id").val();
		data.notify_id = "";
		data.deal_type = 4;
		$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
		$(".load_img").css("display","block");
		frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);
	}*/
	var formsetinst_id = $("#formsetInst_id").val();
	var data = new Object();
	data.id = id;
	data.formsetinst_id = formsetinst_id;
	data.workitem_id = $("#workitem_id").val();
	data.notify_id = "";
	data.deal_type = 4;
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	$(".load_img").css("display","block");
	frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);

	//$("#download_div").attr("src",url);
}

function download_complete(data){
	//$(".load_img").css("display","none");
	if (data.errcode == 0) {
		close_win();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}

function close_win(){
	wx.closeWindow();
}

