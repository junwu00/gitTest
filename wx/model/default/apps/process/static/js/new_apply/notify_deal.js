var date_format_arr = new Array();
date_format_arr["date"] = "yyyy-mm-dd";
date_format_arr["time"] = "hh:ii";
date_format_arr["datetime"] = "yyyy-mm-dd hh:ii";

var money_type = new Array();
money_type[0]="人民币";
money_type[1]="美元";
money_type[2]="欧元";
money_type[3]="港币";

//default默认 email邮箱地址 phone电话 telephone手机
var default_text_format = "default";
var number_text_format = "number";
var email_text_format = "email";
var phone_text_format = "phone";
var telephone_text_format = "telephone";

$(document).ready(function() {
	inst_apply_html();
	$('span.js_a_cnt').html('(' + $('.process-appoval .js_a_div').length + ')');
	$('span.js_n_cnt').html('(' + $('.process-appoval .js_n_div').length + ')');
	$('span.js_w_cnt').html('(' + $('.process-appoval .js_w_div').length + ')');
	
	init_info_button();
	
	$(".workitem_list").unbind('click').bind('click', function() {
		var url = $("#app-url").val()+'&m=mv_open&a=get_mv_pic&formsetinst_id='+$("#formsetinst_id").val()+"&deal_type="+$("#deal_type").val();
		window.location.href=url;
	});
	
});

function to_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_notify_list';
}


function download_file(id,url){
	var u = url.toLowerCase();
	
	var formsetinst_id = $("#formsetinst_id").val();
	var data = new Object();
	data.id = id;
	data.formsetinst_id = formsetinst_id;
	data.workitem_id = $("#workitem_id").val()?$("#workitem_id").val():"";
	data.notify_id = $("#notify_id").val();
	data.deal_type = $("#deal_type").val();
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	$(".load_img").css("display","block");
	frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);
}

function download_complete(data){
	//$(".load_img").css("display","none");
	if (data.errcode == 0) {
		wx.closeWindow();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}


function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

//初始化页面控件
function inst_apply_html(){
	$.each(dataObj,function(key,obj){
		var input_html = inst_input_html(obj);
		$("#input_content").append(input_html);
	});
	
}

function inst_input_html(obj){
	var html = '';
	if(obj.type=="table"){
		html = '<div  id="'+obj.input_key+'">';
	}
	else{
		html = '<div class="css_row" id="'+obj.input_key+'">';
	}
	if(obj.type=="text"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		
		if(obj.val===undefined){
			obj.val="";
		}
		
		html +='<div class="css_input_deal_div">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="textarea"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		if(obj.describe===undefined){
			obj.describe = "";
		}
		var textarea_val = obj.val.replace(/\n/g,'<br>');
		html +='<div class="css_input_deal_div">'+textarea_val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	} 
	else if(obj.type=="radio"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		html +='<div class="css_input_deal_div">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="checkbox"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		html +='<div class="css_input_deal_div">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
		
	}
	else if(obj.type=="select"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		html +='<div class="css_input_deal_div">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="date"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		html +='<div class="css_input_deal_div">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="money"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		if(obj.capitalized_val===undefined){
			obj.capitalized_val = "";
		}
		
		html +='<div class="css_input_deal_div">'+obj.val;
		if(obj.val!=""){
			html +='('+money_type[obj.money_type]+')';
		}
		if(obj.capitalized==1&&obj.capitalized_val!=""){
			html +='<br><span class="js_input_val js_capitalized_input"  style="width: 20%" title="'+obj.capitalized_val+'" >('+obj.capitalized_val+')</span>';
		}
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="table"){
		html +='<div class=" css_table_title"><span style="position: relative;top: 7px">'+obj.name;
		html +='</span>';
		html +='</div>';
		html +='<div style="overflow-x:auto;width: '+(s_width-11)+'px ;" class="son_div">';
		
		html +='<table class="css_son_table" input_key = "'+obj.input_key+'" >';
		//表头
		var th_html = '<tr>';
		$.each(obj.table_arr,function(th_key,th_obj){
			th_html+='<th>'+th_obj.name;
			if(obj.edit_input==1&&(state==1||state==2||state==3)&&th_obj.must == 1){th_html +='<em>*</em>';}
			th_html+='</th>';
		});
		th_html+='</tr>';
		html +=th_html;
		
		if(obj.rec!==undefined && obj.rec.length>0){
			var htmlstr= "";
			for(var j =0;j<obj.rec.length;j++){
				htmlstr+="<tr class='js_tr'>";
				$.each(obj.rec[j],function(key,th_obj){
					var type = th_obj.type;
					htmlstr+="<td>";
					if(type=="text"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						htmlstr+=th_obj.val;
					}
					else if(type=="textarea"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						th_obj.val = th_obj.val.replace(/\n/g,'<br>');
						htmlstr+=th_obj.val;
						
					}
					else if(type=="date"){
						if(th_obj.val===undefined||th_obj.val==""){
							if(th_obj.default_val!==undefined&&th_obj.default_val!=""){
								th_obj.val = getNowFormatDate(th_obj.format);
							}
							else{
								th_obj.val="";
							}
						}
						htmlstr+=th_obj.val;
					}
					else if(type=="select"){
						htmlstr+=th_obj.val;
					}
					else if(type=="radio"){
						htmlstr+=th_obj.val;
					}
					else if(type=="checkbox"){
						htmlstr+=th_obj.val;
					}
					else if(type=="money"){
						if(th_obj.val===undefined){
							th_obj.val="";
						}
						if(th_obj.capitalized_val===undefined){
							th_obj.capitalized_val = "";
						}
						htmlstr+=th_obj.val;
						if(th_obj.val!=""){
							htmlstr +='('+money_type[th_obj.money_type]+')';
						}
						if(th_obj.capitalized_val!=""&&th_obj.capitalized==1){
							htmlstr+='<br>'+th_obj.capitalized_val+'';
						}
					}
					else if(type=="people"){
						htmlstr+=th_obj.val;
					}
					else if(type=="dept"){
						htmlstr+=th_obj.val;
					}
					htmlstr+="</td>";
				});
				htmlstr+="</tr>";
			}
			
			html +=htmlstr;
			
		}
		
		html +='</table></div>';
	}
	else if(obj.type=="people"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.ids!==undefined){
			if(obj.ids!=""){
				html +='<div class="css_input_deal_div">'+obj.val;
			}
			else{
				html +='<div class="css_input_deal_div">';
			}
		}
		else{
			if(obj.default_type==1){
				html +='<div class="css_input_deal_div">'+$("#create_name").val();
			}
			else if(obj.default_type==2&&obj.default_val!==undefined&&obj.default_val!=""&&obj.default_val.length>0){
				var user_name="";
				for(var i=0;i<obj.default_val.length;i++){
					var user = obj.default_val[i];
					user_name+=user['name']+",";
				}
				if(user_name!=""){
					user_name = user_name.substring(0,user_name.length-1);
				}
				html +='<div class="css_input_deal_div">'+user_name;
			}
			else{
				var input_edit_val = "";
				html +='<div class="css_input_deal_div">'+input_edit_val;
			}
		}
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="dept"){
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		if(obj.val===undefined){
			obj.val="";
		}
		if(obj.ids!==undefined){
			if(obj.ids!=""){
				html +='<div class="css_input_deal_div">'+obj.val;
			}
			else{
				html +='<div class="css_input_deal_div">';
			}
		}
		else{
			if(obj.default_type==1){
				html +='<div class="css_input_deal_div">'+$("#create_dept_name").val();
			}
			else if(obj.default_type==2&&obj.default_val!==undefined&&obj.default_val!=""&&obj.default_val.length>0){
				var dept_name="";
				for(var i=0;i<obj.default_val.length;i++){
					var dept = obj.default_val[i];
					dept_name+=dept['name']+",";
				}
				if(dept_name!=""){
					dept_name = dept_name.substring(0,dept_name.length-1);
				}
				html +='<div class="css_input_deal_div">'+dept_name;
			}
			else{
				var input_edit_val = "";
				html +='<div class="css_input_deal_div">'+input_edit_val;
			}
		}
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}		
		
	html +='</div>';
		
	return html;

}

function init_merge_workitem(){
	var proc_items = $('.proc-item');
	if(proc_items.length < 2 ){
		return;
	}
	var work_node = $(proc_items[0]).find('.workitem-name');
	var work_node_id = $(proc_items[0]).find('.workitem-name').attr('data-work_node_id');
	var handle_id = $(proc_items[0]).find('.workitem-name').attr('data-handler_id');
	var state = $(proc_items[0]).find('.workitem-name').attr('data-state');
	for(var i = 1; i < proc_items.length; i++){
		var work_node_tmp = $(proc_items[i]).find('.workitem-name');
		var work_node_id_tmp = $(proc_items[i]).find('.workitem-name').attr('data-work_node_id');
		var handle_id_tmp = $(proc_items[i]).find('.workitem-name').attr('data-handler_id');
		var state_tmp = $(proc_items[i]).find('.workitem-name').attr('data-state');
		if(work_node_id == work_node_id_tmp&&handle_id_tmp!=handle_id){
			$(proc_items[i]).find('.workitem-name').css({'display':'none'});
			var point_line = $(proc_items[i]).find('.point_line')[0];
			if(point_line != undefined){
				point_line = $(point_line);
				point_line.height(point_line.height() + 16);
				point_line.css({'top':'6px'});
			}
		}else{
			work_node = work_node_tmp;
			work_node_id = work_node_id_tmp;
			handle_id = handle_id_tmp;
		}
		if(state_tmp==8){
			work_node.find(".js_icon").removeClass("glyphicon-ok-sign green");
			work_node.find(".js_icon").addClass("glyphicon-remove-sign red");
			work_node = work_node_tmp;
			work_node_id = work_node_id_tmp;
			handle_id = handle_id_tmp;
		}
		
	}
	
	var work_node_id = $(proc_items[proc_items.length - 1]).find('.workitem-name').attr('data-work_node_id');
	for(var i = proc_items.length - 1; i > -1; i--){
		var work_node_id_tmp = $(proc_items[i]).find('.workitem-name').attr('data-work_node_id');
		var state_tmp = $(proc_items[i]).find('.workitem-name').attr('data-state');
		if(state_tmp!=8&&work_node_id == work_node_id_tmp){
			var point_line = $(proc_items[i]).find('.point_line')[0];
			if(point_line != undefined){
				point_line = $(point_line);
				point_line.css({'display':'none'});
			}
		}else{
			return;
		}
	}
}

function init_info_button(){
	$(".js_title_btn").unbind("click").bind("click",function(){
		var s_div = $(this).attr("s_div");
		$(".js_title_btn").removeClass("css_title_active");
		$(this).addClass("css_title_active");
		$(".js_a_divs").css("display","none");
		$(".js_n_divs").css("display","none");
		$(".js_w_divs").css("display","none");
		$("."+s_div).css("display","block");
	});
}
