<?php
include (SYSTEM_APPS.'process/modules/cls_mv_send_server.php');
/**
 * 发起申请
 * @author yangpz
 * @date 2014-12-25
 */
class cls_mv_send extends cls_mv_send_server {
	
	public function __construct() {
		parent::__construct('process');
	}
	
	/**
	 * 主页面
	 */
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'save_workitem':
				$this -> save_workitem();
				break;
			case 'send_workitem':
				$this -> send_workitem();
				break;
			case 'callback_start':
				$this -> callback_start();
				break;
			case 'callback_pre':
				$this -> callback_pre();
				break;
			case 'sendback':
				$this -> sendback_workitem();
				break;
			case 'callback_select':
				$this -> callback_select();
				break;
			case 'del_workitem':
				$this -> del_workitem();
				break;
			case 'press_workitem':
				$this -> press_workitem();
				break;
			case 'send_notify':
				$this -> notify_workitem();
				break;
			default:
				cls_resp::show_err_page('非法请求');
		}
	}
	
	/**
	 * 推送二维码消息
	 */
	public function send_pc_info(){
		$workitem_id = get_var_get("workitem_id");
		$workitem_id = intval($workitem_id);
		$cur_workitem = parent::get_curr_workitem($workitem_id);
		
		$md5_key = get_var_get("key");
		$func = get_var_get("func");
		
		$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_send&a=send_linksign_info&key=".$md5_key."&workitem_id=".$workitem_id;
		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		try{
			$linksign = g("sign_api") -> get_sign_url($user_name,$redirect);
			if(!empty($linksign)){
				$redirect_url = $linksign["redirect"];
				header("Location:".$redirect_url);
			}
		}
		catch (SCException $e){
			cls_resp::show_err_page(array("扫码失败，请刷新页面重新扫码!"));
		}
		parent::send_pc_info_parent($md5_key, $func);
	}

	/**
	 * 签字完成后，保存签字并关闭微信页面
	 */
	public function send_linksign_info(){
		$workitem_id = get_var_get("workitem_id");
		$workitem_id = intval($workitem_id);
		$cur_workitem = parent::get_curr_workitem($workitem_id);
		
		$md5_key = get_var_get("key");
		$signatureId = get_var_value("signatureId");
		$cancel = get_var_value("cancel");
		if($cancel=="yes"){
			$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=close_qrcode";
			header("Location:".$redirect);
		}
		else{
			try{
				$img_url = g("sign_api") -> get_image($signatureId);
				if(!empty($img_url)){
					parent::save_linksign($workitem_id, $img_url);
					parent::send_pc_info_parent($md5_key, 'linksign_finish');
					$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=close_qrcode";
					header("Location:".$redirect);
				}
			}catch (SCException $e){
				cls_resp::show_err_page($e);
			}
		}
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
	
	/**
	 * 知会流程
	 */
	private function notify_workitem(){
		try {
			parent::log('开始保存知会流程');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('formsetinst_id','receivers'));
			
			parent::notify_workitem_parent($data);
			
			g('db') -> commit();
			echo json_encode(array('errcode'=>0,'errmsg'=>"知会成功"));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 催办流程
	 */
	private function press_workitem(){
		try {
			parent::log('开始催办流程');
			$data = parent::get_post_data(array('formsetinst_id'));
			
			parent::press_workitem_parent($data);
			
			echo json_encode(array('errcode'=>0,'errmsg'=>"催办成功"));
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除工作项
	 */
	private function del_workitem(){
		try {
			parent::log('开始删除工作项');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id'));
			
			$ret = parent::del_workitem_parent($data);
			
			g('db') -> commit();
			echo json_encode(array('errcode'=>$ret["errcode"],'errmsg'=>$ret["errmsg"]));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 发送到退回步骤
	 */
	private function sendback_workitem(){
		try {
			parent::log('开始发送到退回节点');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id','judgement'));
			
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::sendback_workitem_parent($data);
			
				g('db') -> commit();
				echo json_encode(array('errcode'=>0,'errmsg'=>'流程发送成功，发送步骤为'.$ret["workitem_name"].'步骤，接收人为'.$ret["receiver_name"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 驳回指定步骤流程
	 */
	private function callback_select(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('callback_workitem_id','workitem_id','judgement'));
			
			$ret = parent::callback_select_parent($data);
			
			g('db') -> commit();
			
			echo json_encode(array('errcode'=>0,'errmsg'=>'流程退回成功，退回步骤为'.$ret["workitem_name"].'步骤，接收人为'.$ret["receiver_name"]));
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 驳回开始步骤流程
	 */
	private function callback_start(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id','judgement'));
			
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::callback_start_parent($data);
				g('db') -> commit();
				echo json_encode(array('errcode'=>0,'errmsg'=>'流程退回成功，退回步骤为开始步骤，接收人为'.$ret["receiver_name"]));
			}
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 退回上一步骤
	 */
	private function callback_pre(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			
			$data = parent::get_post_data(array('workitem_id','judgement'));
			
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::callback_pre_parent($data);
				g('db') -> commit();
				echo json_encode(array('errcode'=>$ret["errcode"],'errmsg'=>$ret["errmsg"],'callback_workitem'=>$ret["callback_workitem"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 保存流程new
	 */
	private function save_workitem(){
		try {			
			parent::log('开始保存信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('form_id','form_name','work_id', 'workitem_id','formsetInst_id','form_vals','judgement','files'));
			
			
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::save_workitem_parent($data);
				g('db') -> commit();
				parent::log('保存流程信息成功');
				echo json_encode(array('errcode'=>0,'errmsg'=>'保存流程信息成功','workitem_id'=>$ret["workitem_id"],'formsetInst_id'=>$ret["formsetInst_id"]));
			}
			
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
		
	}
	
	
	
	
	
	/**
	 * 发送流程new
	 */
	private function send_workitem(){
		try {	
			parent::log('开始发送流程');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitemid','work_nodes'));
			$ret = parent::send_workitem_parent($data);
			g('db') -> commit();
			$msg = "";
			if($ret["return_arr"][0]=="TRUE"){
				$msg = '流程流转结束';
			}
			else{
				$msg = '流程发送成功，下一步骤为'.$ret["return_arr"][1].' 接收人为'.$ret["return_arr"][2];
			}
			
			parent::log($msg);
			echo json_encode(array('errcode'=>0,'errmsg'=>$msg));
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
}

//end of file