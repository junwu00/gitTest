<?php
/**
 * 发起申请
 * @author yangpz
 * @date 2014-12-25
 */
class cls_mv_open_server extends abs_new_proc {
	
	public function __construct() {
		parent::__construct('process');
		
	}
	private static $FormUserCount =5;
	
	/**
	 * 获取用户的可发起的流程分类new
	 */
	public function classify_list(){
		$classify_list = g("mv_form")->get_classify($this->ComId,$this->UserId);
		if(empty($classify_list)){return FALSE;}
		
		foreach ($classify_list as $key => &$classify){
			$form_list = g("mv_form")->get_form_by_classify($classify["classify_id"],$this->ComId,$this->UserId);
			
			if(!$form_list||count($form_list)==0){
				unset($classify_list[$key]);
				continue;
			}
			
			//员工可见流程
			$user = g('api_user') -> get_by_id($this->UserId);
			$dept_tree = json_decode($user['dept_tree'], TRUE);
			
			$depts = array();
			foreach ($dept_tree as $dept) {
				$depts = array_merge($depts, $dept);
			}
			unset($dept);
			
			foreach ($form_list as $form_key=>$form_model) {			//遍历所有流程
				$scope = json_decode($form_model['scope'], TRUE);
				if (count($scope) == 0) {				//企业内可见
					unset($form_list[$form_key]);
				} else {
					$is_contain = FALSE;
					foreach ($scope as $s) {			//记录可见的流程
						if (in_array($s, $depts)) {
							$is_contain = TRUE;
							break;
						}
					}
					unset($s);
					if(!$is_contain){
						unset($form_list[$form_key]);
					}
				}
			}
			unset($form_model);
			
			$classify["count"] = count($form_list);
			if($classify["count"]!=0){
				$classify["form"] = $form_list;
			}
			else{
				unset($classify_list[$key]);
			}
		}
		unset($classify);
		return $classify_list;
	}
	
	/**
	 * 获取用户常用表单new
	 */
	public function get_user_form(){
		$form_user = g("mv_form_user")->get_common_form($this->UserId,$this->ComId);
		if(empty($form_user)){
			return array();
		}
		$tmp_form_user = array();
		foreach ($form_user as $fu){
			$version = $fu["version"];
			if($version==parent::$StateHistory){//如果是历史版本，则更新为最新版本的form_id
				$public_form = g('mv_form') -> get_by_his_id($fu["form_history_id"],$this->ComId);
				if(!empty($public_form)){
					$new_form_id = $public_form["id"];
					$new_form_name = $public_form["form_name"];
					$new_form_describe = $public_form["form_describe"];
					$new_form_scope = $public_form["scope"];
					$fu["form_id"] = $new_form_id;
					$fu["form_name"] = $new_form_name;
					$fu["form_describe"] = $new_form_describe;
					$fu["scope"] = $new_form_scope;
					
					//判断新版本是否有权限发起
					$new_form_scope = json_decode($new_form_scope, TRUE);
					if (count($new_form_scope) == 0) {
						$dept_arr = array();
					} else {
						$dept_arr = g('api_dept') -> list_by_ids($new_form_scope,"*",FALSE);
					}
					$user = g("api_user")->get_by_id($this->UserId);
					$is_apply = FALSE;
					if(count($dept_arr)==0){
						$is_apply = TRUE;
					}
					else{
						foreach ($dept_arr as $dept){
							if(strstr($user["dept_tree"],'"'.$dept["id"].'"')){
								$is_apply = TRUE;
								break;
							}
						}
						unset($dept);
					}
					if($is_apply==FALSE){
						g("mv_form_user")->del_common_form($fu["form_history_id"],$this->UserId,$this->ComId);
					}
					else{
						//更新本来旧的表单id
						g("mv_form_user")->update_common_form_id($new_form_id,$fu["form_history_id"],$this->UserId,$this->ComId);
						$tmp_form_user[] = $fu;
					}
					
				}
			}
			else{
				$tmp_form_user[] = $fu;
			}
			
			if(count($tmp_form_user)>=self::$FormUserCount){
				break;
			}
		}
		unset($fu);
		$form_user = $tmp_form_user;
		return $form_user;
	}
	
	/**
	 * 根据表单id获取表单信息new
	 */
	public function classify_proc($proc_id){
		$proc = g('mv_form') -> get_by_id($proc_id,$this->ComId);
		
		if(!$proc){
			cls_resp::show_err_page(array('该流程模板已经被删除！'));
		}
		
		if($proc['version']==self::$StateHistory){//获取最新版本的流程
			$proc = g('mv_form') -> get_by_his_id($proc["form_history_id"],$this->ComId);
		}
		
		if(!$proc){
			cls_resp::show_err_page(array('该流程模板已经被删除！'));
		}
		
		$scope = json_decode($proc['scope'], TRUE);
		if (count($scope) == 0) {
			$proc['scope'] = array();
		} else {
			$proc['scope'] = g('api_dept') -> list_by_ids($scope);
		}
		
		$user = g("api_user")->get_by_id($this->UserId);
		
		//判断是否有权限发起该流程
		$dept_arr = $proc["scope"];
		$is_apply = FALSE;
		foreach ($dept_arr as $dept){
			if(strstr($user["dept_tree"],'"'.$dept["id"].'"')){
				$is_apply = TRUE;
				break;
			}
		}
		unset($dept);
		if(!$is_apply){
			cls_resp::show_err_page(array('没权限发起该流程'));
		}
		
		return $proc;
	}
	

}

//end of file