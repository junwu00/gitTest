<?php
include (SYSTEM_APPS.'process/modules/cls_mv_open_server.php');
/**
 * 发起申请
 * @author yangpz
 * @date 2014-12-25
 */
class cls_mv_open extends cls_mv_open_server {
	
	public function __construct() {
		parent::__construct('process');
	}

	
	/**
	 * 主页面
	 */
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'download_file':
				$this->download_file();
				break;
			case 'open_linksign':
				$this->open_linksign();
				break;
			default:
				$classify_list = parent::classify_list();
				$form_user = parent::get_user_form();
				$apply_page_url = $this -> app_domain.'&m=mv_open&a=apply&pid=';
				
				g('smarty') -> assign('classify_list', $classify_list);
				g('smarty') -> assign('form_user', $form_user);
				g('smarty') -> assign('apply_page_url', $apply_page_url);
				g('smarty') -> assign('title', '发起申请');
				g('smarty') -> show($this -> temp_path.'new_apply/index.html');
		}
	}
	
	/**
	 * 获取领签得到图片url并保存把结果保存到数据库重定向发送页面
	 */
	public function get_save_linksign(){
		$workitem_id= get_var_value("workitemid");
		$workitem_id= intval($workitem_id);
		$formsetinst_id = get_var_value("formsetinst_id");
		$formsetinst_id = intval($formsetinst_id);
		$signatureId = get_var_value("signatureId");
		$cancel = get_var_value("cancel");
		if($cancel=="yes"){
			$send_url = "index.php?app=".$this->AppName."&m=mv_open&a=detail&workitem_id=".$workitem_id;
			header("Location:".$send_url);
		}
		else{
			try{
				$img_url = g("sign_api") -> get_image($signatureId);
				if(!empty($img_url)){
					parent::save_linksign($workitem_id, $img_url);
					$send_url = "index.php?app=".$this->AppName."&m=mv_open&a=send_page&workitemid=".$workitem_id."&formsetInst_id=".$formsetinst_id;
					header("Location:".$send_url);
				}
			}catch (SCException $e){
				cls_resp::show_err_page($e);
			}
		}
	}
	
	/**
	 * 扫码后关闭扫码页面，pc扫码专用
	 */
	public function close_qrcode(){
		parent::oauth_jssdk();
		g('smarty') -> show($this -> temp_path.'new_apply/close_qrcode.html');
	}
	
	/**
	 * 弹出选人页面new
	 */
	public function send_page(){
		try {
			$workitem_id= get_var_value("workitemid");
			$workitem_id= intval($workitem_id);
			$formsetinst_id = get_var_value("formsetInst_id");
			$formsetinst_id = intval($formsetinst_id);
			
			$next_node_id_arr = parent::get_next_workitem($workitem_id);
			$is_send = parent::is_send($next_node_id_arr);
			$list = intval(get_var_value("list"));
			
			g('smarty') -> assign('list', $list);
			g('smarty') -> assign('formsetInst_id', $formsetinst_id);
			g('smarty') -> assign('workitemid', $workitem_id);
			g('smarty') -> assign('is_send', $is_send);
			g('smarty') -> assign('next_node_id', $next_node_id_arr);
			
			parent::assign_contact_dept();
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$page_url = $this -> app_domain.'&m=mv_open';
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('page_url', $page_url);
			
			g('smarty') -> assign('title', '选择发送人');
			g('smarty') -> show($this -> temp_path.'new_apply/send.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	} 
	
	/**
	 * 知会发送列表
	 */
	public function notify_page(){
		try {
			$formsetinst_id= get_var_value("formsetinst_id");
			$formsetinst_id = intval($formsetinst_id);
			$workitem_id = get_var_value("workitem_id"); 
			$workitem_id = intval($workitem_id);
			
			parent::get_formsetinst_by_id($formsetinst_id);
			
			parent::assign_contact_dept();
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$page_url = $this -> app_domain.'&m=mv_open';
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('page_url', $page_url);
			
			g('smarty') -> assign('formsetinst_id', $formsetinst_id);
			g('smarty') -> assign('workitem_id', $workitem_id);
			g('smarty') -> assign('title', '选择知会人');
			g('smarty') -> show($this -> temp_path.'new_apply/notify.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	
	/**
	 * 具体的流程审批申请页面
	 */
	public function apply() {
		try {
			$proc_id = intval(get_var_get('pid'));	
			
			//新建申请单
			$proc = parent::classify_proc($proc_id);
			$start_node = parent::get_start_node($proc["work_id"]);
			//查看那些控件可编辑
			$this->get_form_edit($proc["inputs"], $start_node);
			g('smarty') -> assign('proc', $proc);
			g('smarty') -> assign('curr_node', $start_node);
			
			//获取申请人信息
			$create_info = g("api_user")->get_by_id($this->UserId);
			g('smarty') -> assign('create_info', $create_info);
			$create_m_dept = g("api_user")->get_main_dept($this->UserId,FALSE);
			g('smarty') -> assign('create_m_dept', $create_m_dept);
			
			//获取表单实例格式
			$is_special = 0;
			$formset_name = parent::get_formset_by_num($proc["form_name"],$is_special);
			$formset_name_html = implode(" - ", $formset_name);
			g('smarty') -> assign('is_special', $is_special);
			g('smarty') -> assign('formset_name_html', $formset_name_html);
			g('smarty') -> assign('formset_name', $formset_name);
			
			parent::assign_contact_dept();
			
			g('smarty') -> assign('title', '发起申请');
			$mine_list_url = $this -> app_domain.'&m=mv_list&a=get_mine_list&is_finish=2';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$page_url = $this -> app_domain.'&m=mv_open';
			g('smarty') -> assign('storagr_key','process'.'-'.$this->UserId.'_'.$proc_id);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('mine_list_url', $mine_list_url);
			
			g('smarty') -> show($this -> temp_path.'new_apply/apply.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('没权限发起该流程'));
		}
	}
	
	
	
	/**
	 * 根据workitem_id获取审批明细
	 */
	public function detail() {
		try {
			parent::oauth_jssdk();
			$workitem_id = intval(get_var_get("workitem_id"));
			
			$cur_workitem = parent::get_curr_workitem($workitem_id);
			$formsetinst_id = $cur_workitem["formsetinit_id"];
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id);
			//查看那些控件可编辑
			$this->get_form_edit($formsetinst["form_vals"], $cur_workitem);
			g('smarty') -> assign('formsetInst', $formsetinst);
			g('smarty') -> assign('cur_workitem', $cur_workitem);
			
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$formsetinst_id);
			g('smarty') -> assign('files', $files);
			
			parent::assign_contact_dept();
			
			if($cur_workitem["state"]==parent::$StateNew){
				//更新工作项状态
				g("mv_workinst")->update_workitem_state($cur_workitem["id"],$this->ComId,parent::$StateRead);
			}
			
			//获取申请人信息
			$create_m_dept = g("api_user")->get_main_dept($cur_workitem["creater_id"],FALSE);
			g('smarty') -> assign('create_m_dept', $create_m_dept);
			
			//获取已经审批的步骤
			$workitems =parent::get_workitem_list_parent($formsetinst_id);
			g('smarty') -> assign('workitems', $workitems);
			
			//知会信息
			$notifys = g("mv_notify") ->get_notify_by_formset_id($formsetinst_id,$this->ComId);
			g('smarty') -> assign('notifys', $notifys);
			g('smarty') -> assign('deal_type', 1);
			
			//是否电子签署
			$is_linksign = $formsetinst["is_linksign"];
			// 检查领签是否过期，若过期则把当前节点更改为普通审批模式
			if ($is_linksign == 1) {
				try {
					$check = g('sign_api') -> check_privirage();
				} catch (SCException $e) {
					$check = FALSE;
				}
				!$check && $is_linksign = 0;
			}
			g('smarty') -> assign('is_linksign', $is_linksign);
			
			g('smarty') -> assign('title', "查看流程");
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			if($cur_workitem['state']==parent::$StateNew||$cur_workitem['state']==parent::$StateRead||$cur_workitem['state']==parent::$StateSave){
				$return_list_url = $this -> app_domain.'&m=mv_list&a=get_do_list';
			}
			else{
				$return_list_url = $this -> app_domain.'&m=mv_list&a=get_do_list&is_finish=1';
			}
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('return_list_url', $return_list_url);
			g('smarty') -> assign('linksign_url', $this->LinksignUrl);
			
			g('smarty') -> show($this -> temp_path.'new_apply/deal.html');
				
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	/**
	 * 根据formsetinst_id获取审批明细
	 */
	public function mine_detail() {
		try {
			parent::oauth_jssdk();
			$formsetinit_id = intval(get_var_get("formsetinit_id"));
			$formsetinst = parent::get_formsetinst_by_id($formsetinit_id);
			$start_workitem = parent::get_start_item($formsetinst["work_id"], $formsetinit_id);
			
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$formsetinit_id);
			g('smarty') -> assign('files', $files);
			
			parent::assign_contact_dept();
			
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$return_list_url = $this -> app_domain.'&m=mv_list&a=get_mine_list';
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('return_list_url', $return_list_url);
			
			//*
			if($formsetinst["state"]==parent::$StateFormDraft){
				
				//查看那些控件可编辑
				$this->get_form_edit($formsetinst["form_vals"], $start_workitem);
				//获取申请人信息
				$create_m_dept = g("api_user")->get_main_dept($this->UserId,FALSE);
				g('smarty') -> assign('create_m_dept', $create_m_dept);
				
				g('smarty') -> assign('formsetInst', $formsetinst);
				g('smarty') -> assign('cur_workitem', $start_workitem);
				g('smarty') -> show($this -> temp_path.'new_apply/edit.html');
			}
			
			else{
			//*/
				if($start_workitem["state"]==parent::$StateNew){
					//更新工作项状态
					g("mv_workinst")->update_workitem_state($start_workitem["id"],$this->ComId,parent::$StateRead);
				}
				//查看那些控件可编辑
				$this->get_form_edit($formsetinst["form_vals"], $start_workitem);
				//获取已经审批的步骤
				$workitems =parent::get_workitem_list_parent($formsetinit_id);
				g('smarty') -> assign('workitems', $workitems);
				//知会信息
				$notifys = g("mv_notify") ->get_notify_by_formset_id($formsetinit_id,$this->ComId);
				g('smarty') -> assign('notifys', $notifys);
				
				//获取申请人信息
				$create_m_dept = g("api_user")->get_main_dept($this->UserId,FALSE);
				g('smarty') -> assign('create_m_dept', $create_m_dept);
				g('smarty') -> assign('deal_type', 2);
				g('smarty') -> assign('cur_workitem', $start_workitem);
				g('smarty') -> assign('formsetInst', $formsetinst);
				g('smarty') -> assign('workitems', $workitems);
				
				g('smarty') -> assign('linksign_url', $this->LinksignUrl);
				
				g('smarty') -> show($this -> temp_path.'new_apply/deal.html');
			}
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	/**
	 * 根据notify_id获取审批明细
	 */
	public function notify_detail() {
		try {
			parent::oauth_jssdk();
			$notify_id = get_var_get("notify_id");
			$notify_id = intval($notify_id);
			$notify = parent::get_notify_item($notify_id);
			//查看那些控件可编辑
			$this->get_form_edit($notify["form_vals"], NULL);
			g('smarty') -> assign('notify', $notify);
			
			if ($notify["state"]==parent::$StateReading){
				parent::update_notify_state_parent($notify_id);
			}
			
			//获取已经审批的步骤
			$workitems =parent::get_workitem_list_parent($notify['formsetinst_id']);
			g('smarty') -> assign('workitems', $workitems);
			//知会信息
			$notifys = g("mv_notify") ->get_notify_by_formset_id($notify['formsetinst_id'],$this->ComId);
			g('smarty') -> assign('notifys', $notifys);
			
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$notify['formsetinst_id']);
			g('smarty') -> assign('files', $files);
			
			//获取申请人信息
			$create_m_dept = g("api_user")->get_main_dept($notify["creater_id"],FALSE);
			g('smarty') -> assign('create_m_dept', $create_m_dept);
			g('smarty') -> assign('deal_type', 3);
			
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$return_list_url = $this -> app_domain.'&m=mv_list&a=get_notify_list';
			g('smarty') -> assign('return_list_url', $return_list_url);
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			
			g('smarty') -> assign('linksign_url', $this->LinksignUrl);
			
			g('smarty') -> show($this -> temp_path.'new_apply/notify_deal.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	
	/**
	 * 获取流程图
	 * Enter description here ...
	 */
	public function get_mv_pic(){
		try {
			$formsetinst_id = intval(get_var_get("formsetinst_id"));
			$deal_type= get_var_get('deal_type');
			
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id);
			$work_model = parent::get_work_node($formsetinst["work_id"]);
			$worknodes_arr = parent::get_pic_rule($formsetinst);
			
			if($deal_type==1){//待办详情
				$workitem_id = get_var_get('workitem_id');
				g('smarty') -> assign('workitem_id', $workitem_id);
			}
			if($deal_type==3){//知会详情
				$notify_id = get_var_get('notify_id');
				g('smarty') -> assign('notify_id', $notify_id);
			}
			$open_url = $this -> app_domain.'&m=mv_list';
			
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('deal_type', $deal_type);
			g('smarty') -> assign('formsetinst', $formsetinst);
			g('smarty') -> assign('work_model', $work_model);
			g('smarty') -> assign('worknodes_arr', $worknodes_arr);
			g('smarty') -> show($this -> temp_path.'new_apply/mv_pic.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	/**
	 * 下载文件
	 *
	 * @access public
	 * @return void
	 */
	public function download_file() {
		$data = parent::get_post_data(array('id','formsetinst_id','workitem_id','notify_id','deal_type'));
		$cf_id = (int)$data['id'];
		$formsetinst_id = (int)$data['formsetinst_id'];
		$workitem_id = (int)$data['workitem_id'];
		
		if (empty($cf_id)||empty($formsetinst_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		parent::log('开始下载文件');
		try {
			$result = g('mv_file') -> download_file($cf_id,$this->ComId,$formsetinst_id,$this->app_combo,'process',$this->UserId);
		}catch(SCException $e) {
			parent::log('下载文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		
		$user = g('api_user') -> get_by_id($this->UserId);
		
		$msg_user = array($user['acct']);
				
		$msg_title="重新打开详情页面";
		
		if($data['deal_type']==1){//待办
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		}
		else if($data['deal_type']==2){//起草
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
		}
		else if($data['deal_type']==3){//知会
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=notify_detail&notify_id='.$data['notify_id'];//TODO
		}
		else if($data['deal_type']==4){//编辑状态
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=process&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
		}
		try{
			parent::send_single_news($msg_user, $msg_title, '重新打开详情页面', $msg_url);
		}catch(SCException $e){
			$fail_user .=$this->UserId.' ';
		}
		parent::log('下载文件成功');
		cls_resp::echo_ok();
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
	/**
	 * 打开领签页面
	 */
	private function open_linksign(){
		$data = parent::get_post_data(array('workitem_id','formsetinst_id'));
		$workitem_id= intval($data['workitem_id']);
		$formsetinst_id = intval($data['formsetinst_id']);
		$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=get_save_linksign&workitemid=".$workitem_id."&formsetinst_id=".$formsetinst_id;
		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		try{
			$linksign = g("sign_api") -> get_sign_url($user_name,$redirect);
			if(!empty($linksign)){
				$redirect_url = $linksign["redirect"];
				echo json_encode(array('errcode'=>0,'redirect_url'=>$redirect_url));
			}
			else{
				echo json_encode(array('errcode'=>999998,'errmsg' => "链接为空"));
			}
		}
		catch (SCException $e){
			echo json_encode(array('errcode'=>999999,'errmsg' => $e ->getMessage()));
		}
	}
	
/**
	 * 
	 * 获取该节点中那些控件不可编辑
	 * @param string $form_vals 表单(字符串)
	 * @param array $work_node 步骤节点信息
	 */
	private function get_form_edit(&$form_vals,$work_node){
		if(empty($work_node)){
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}
				$form_val["visit_input"] = parent::$VisitInput;
				$form_val["edit_input"] = parent::$NotEditInput;
				if($form_val["type"]=="table"){
					$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
					foreach ($table_arr as $table_key=>&$table_td){
						$table_td["edit_input"] = parent::$NotEditInput;
						$table_td["visit_input"] = parent::$VisitInput;
					}
					$form_val["table_arr"]=$table_arr;
				}
			}
			unset($form_val);
			$form_vals = json_encode($form_vals);
		}
		else{
			
			$input_visble_rule = $work_node["input_visble_rule"];
			$input_visble_rule = json_decode($input_visble_rule, TRUE);
			
			$input_visit_rule = $work_node["input_visit_rule"];
			$input_visit_rule = json_decode($input_visit_rule, TRUE);
			
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			
			$is_start = $work_node["is_start"];
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}
				if(!empty($input_visit_rule)&&in_array($input_key,$input_visit_rule)){
					$form_val["edit_input"] = parent::$NotEditInput;
					$form_val["visit_input"] = parent::$NotVisitInput;
				}
				else{
					$form_val["visit_input"] = parent::$VisitInput;
					if($is_start == parent::$StartNode){//开始节点，如果存在，则为不可编辑控件
						if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = parent::$NotEditInput;
						}
						else{
							$form_val["edit_input"] = parent::$EditInput;
						}
						if($form_val["type"]=="table"){//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = parent::$NotVisitInput;
									$table_td["edit_input"] = parent::$NotEditInput;
								}
								else{
									$table_td["visit_input"] = parent::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = parent::$NotEditInput;
									}
									else{
										$table_td["edit_input"] = parent::$EditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;
						}
					}
					else{//其他节点，如果存在，则为可编辑控件
						if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = parent::$EditInput;
						}
						else{
							$form_val["edit_input"] = parent::$NotEditInput;
						}
						if($form_val["type"]=="table"){//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = parent::$NotVisitInput;
									$table_td["edit_input"] = parent::$NotEditInput;
								}
								else{
									$table_td["visit_input"] = parent::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = parent::$EditInput;
									}
									else{
										$table_td["edit_input"] = parent::$NotEditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;
						}
					}
				}
			}
			unset($form_val);
			$form_vals = json_encode($form_vals);
		}
		
		$escapers = array("'","\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
		$replacements = array("‘","\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
		$form_vals = str_replace($escapers, $replacements, $form_vals);
	}
	
}

//end of file