<?php
/**
 * 审批流程
 * 
 * @author yangpz
 * @date 2014-12-25
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『流程审批』操作指引',
		'desc' => '欢迎使用流程审批应用，在这里你可以随时随地发起任何审批，让你的申请快速得到领导的审批，赶快发起申请吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213969656&idx=5&sn=24d0c38d70883ecc51d22f39d6a0acc1&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file