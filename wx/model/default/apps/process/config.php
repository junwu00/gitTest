<?php
/**
 * 应用的配置文件_流程审批
 * 
 * @author yangpz
 * @date 2014-12-03
 * 
 */

return array(
	'id' 				=> 11,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(11),
	'name' 				=> 'process',
	'cn_name' 			=> '流程审批',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Process.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '流程管理', 
				'childs' => array(
					array('id' => 'bmenu-1-1', 'name' => '待办/已办', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=process&m=mv_list&a=get_do_list'),
					array('id' => 'bmenu-1-2', 'name' => '我发起的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=process&m=mv_list&a=get_mine_list'),
					array('id' => 'bmenu-1-3', 'name' => '我的知会', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=process&m=mv_list&a=get_notify_list')
				) 
			),
			array('id' => 'bmenu-2', 'name' => '流程发起', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=process&m=mv_open')
		),
	),
);

// end of file