<?php
/**
 * 全部pc入口ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2015-03-14
 */
class cls_ajax extends abs_app_base {
	//命令对应函数名的映射表
	private $cmd_map = array(
		//扫描成功
		101 => 'qr_ready',
		//确认授权
		102 => 'auth_confirm',
		//创建PC登录账号
		103 => 'create_pc_acct',
		//更新PC登录密码
		104 => 'update_pc_pwd',
	);

	public function __construct() {
		parent::__construct('entry');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		empty($cmd) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$map = $this -> cmd_map;
		!isset($map[$cmd]) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$this -> $map[$cmd]();
	}

	/**
	 * 扫描成功
	 *
	 * @access public
	 * @return void
	 */
	private function qr_ready() {
		$token = get_var_value('token');
		$token = trim($token);

		if (empty($token)) {
			cls_resp::echo_err(cls_resp::$Busy, '不合法的二维码');
		}

		$data = array(
			//扫码成功
			'ident' => 1,
		);
		$result = g('messager') -> publish($token, $data);
		$result and cls_resp::echo_ok();

		cls_resp::echo_err(cls_resp::$Busy, '系统正忙，请稍后再试');
	}
	
	/**
	 * 确认授权
	 *
	 * @access public
	 * @return void
	 */
	private function auth_confirm() {
		$token = get_var_value('token');
		$token = trim($token);

		if (empty($token)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的访问');
		}

		$data = array(
			//确认授权
			'ident' => 2,
		);

		$sess_id_key = $token . ":sess_id";
		$result = g('redis') -> setex($sess_id_key, session_id(), 60);
		if ($result) {
			$result = g('messager') -> publish($token, $data);

			$result and cls_resp::echo_ok();
		}
		
		cls_resp::echo_err(cls_resp::$Busy, '系统正忙，请稍后再试');
	}
	
	/** 创建PC登录账号 */
	private function create_pc_acct() {
		try {
			log_write('开始创建个人账号');
			$need = array('account', 'psw');
			$data = $this -> get_post_data($need);
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$acct = g('user_acct') -> get_by_user_id($user_id);
			if (!empty($acct))															throw new SCException('您已有账号，无需创建');
			
			if (!check_data($data['account'], 'account'))								throw new SCException('请输入6位以上长度的账号（支持数字、字母、下划线、@）');
			if (strlen($data['account']) > 50)											throw new SCException('账号过长，最长支持50位');
			if (!check_data($data['psw'], 'password') || strlen($data['psw']) < 6)		throw new SCException('请输入6位以上长度的密码（支持数字、字母、下划线）');
			
			g('user_acct') -> create($user_id, $data['account'], $data['psw']);
			log_write('成功创建个人账号');
			cls_resp::echo_ok();
				
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 更新PC登录密码 */
	private function update_pc_pwd() {
		try {
			log_write('开始更新个人密码');
			$need = array('psw');
			$data = $this -> get_post_data($need);
			
			if (!check_data($data['psw'], 'password') || strlen($data['psw']) < 6)		throw new SCException('请输入6位长度的密码（支持数字、字母、下划线）');
			
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$acct = g('user_acct') -> get_by_user_id($user_id);
			if (empty($acct))	throw new SCException('未找到对应的账号，请先创建账号');
			g('user_acct') -> update_pwd($user_id, $acct['acct'], $data['psw']);
			log_write('成功更新个人密码');
			cls_resp::echo_ok();
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
}

/* End of this file */