<?php
/**
 * 扫描登录控制器
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_index extends abs_app_base {
	public function __construct() {
		parent::__construct('entry');
	}
	
	/**
	 * 扫描页面
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$token = get_var_value('token');

		$result = FALSE;
		//是否跳过扫码阶段，0-否、1-是
		$skip_qr = 1;
		if (!empty($token)) {
			$data = array(
				//扫码成功
				'ident' => 1,
			);
			$result = g('messager') -> publish($token, $data);
		}

		!$result and $skip_qr = 0;
		
		//begin---------------js_sdk---------------------------
		parent::oauth_jssdk();

		global $app, $module, $action;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax';

		g('smarty') -> assign('skip_qr', $skip_qr);
		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('token', $token);
		g('smarty') -> show($this -> temp_path.'index.html');
	}
	
	/**
	 * PC账号信息维护
	 *
	 * @access public
	 * @return void
	 */
	public function account() {
		$user = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		$acct = g('user_acct') -> get_by_user_id($_SESSION[SESSION_VISIT_USER_ID]);
		$acct && $user['account'] = $acct['acct'];
		
		$ajax_add_account = $this -> app_domain.'&m=ajax&cmd=103';
		$ajax_edit_psw = $this -> app_domain.'&m=ajax&cmd=104';
		g('smarty') -> assign('user', $user);
		g('smarty') -> assign('title', '账号设置');
		g('smarty') -> assign('ajax_add_account', $ajax_add_account);
		g('smarty') -> assign('ajax_edit_psw', $ajax_edit_psw);
		g('smarty') -> show($this -> temp_path.'personal.html');
	}
}

// end of file