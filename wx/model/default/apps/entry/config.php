<?php
/**
 * 应用的配置文件
 * @author zpeng
 * @date 2015-02-04
 * 
 */

return array(
	'id' 		=> 19,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
    'combo' => g('com_app') -> get_sie_id(19),
	'name' 		=> 'entry',
	'cn_name' 	=> '综合服务',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'allservice.png',

	'menu' => array(
	),
);

// end of file