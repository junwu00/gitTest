
$(document).ready(function(){
	init_add_accout();
	init_edit_psw();
});

function init_add_accout(){
	$('.psw_add_icon').bind('click',function(){
		$(this).addClass('hide');
		$('#psw_add').addClass('hide');
		$('.psw_add_show_icon').removeClass('hide');
		$('#psw_add_show').removeClass('hide');
	});
	
	$('.psw_add_show_icon').bind('click',function(){
		$(this).addClass('hide');
		$('#psw_add_show').addClass('hide');
		$('.psw_add_icon').removeClass('hide');
		$('#psw_add').removeClass('hide');
	});
	
	$('#psw_add').bind('keyup',function(){
		var value = $(this).val();
		$('#psw_add_show').val(value);
	});
	
	$('#psw_add_show').bind('keyup',function(){
		var value = $(this).val();
		$('#psw_add').val(value);
	});
	
	$('.add_acount').bind('click',function(){
		var data = {};
		data.account = $.trim($('#account_add').val());
		data.psw = $.trim($('#psw_add').val());
		if(data.account == ""){
			frame_obj.tips('请输入账号');
			return;
		}
		if(data.psw == ""){
			frame_obj.tips('请输入密码');
			return;
		}
		if(data.psw.length < 6){
			frame_obj.tips('密码长度不能小于6位');
			return;
		}
		$('#ajax-url').val($('#ajax_add_account').val());
		frame_obj.lock_screen('数据提交中...');
		frame_obj.do_ajax_post($(this), '', data, function(data){
			frame_obj.unlock_screen();
			if(data['errcode'] != 0){
				frame_obj.tips(data['errmsg']);
				return;
			}
			frame_obj.tips('PC登录账号设置成功');
			setTimeout(function(){
				location.replace(location)
			},1000);
		});
	});
}

function init_edit_psw(){
	$('.edit_psw').bind('click',function(){
		$(this).parent().addClass('hide');
		$('.edit_personal').removeClass('hide');
	})
	
	$('.psw_edit_icon').bind('click',function(){
		$(this).addClass('hide');
		$('#psw_edit').addClass('hide');
		$('.psw_edit_show_icon').removeClass('hide');
		$('#psw_edit_show').removeClass('hide');
	});
	
	$('.psw_edit_show_icon').bind('click',function(){
		$(this).addClass('hide');
		$('#psw_edit_show').addClass('hide');
		$('.psw_edit_icon').removeClass('hide');
		$('#psw_edit').removeClass('hide');
	});
	
	$('#psw_edit').bind('keyup',function(){
		var value = $(this).val();
		$('#psw_edit_show').val(value);
	});
	
	$('#psw_edit_show').bind('keyup',function(){
		var value = $(this).val();
		$('#psw_edit').val(value);
	});
	
	$('.cancel_edit_psw').bind('click', function() {
		$(this).parent().addClass('hide');
		$('.personal_msg').removeClass('hide');
	});
	
	$('.save_edit_psw').bind('click',function(){
		var data = {};
		data.psw = $.trim($('#psw_edit').val());
		if(data.psw == ""){
			frame_obj.tips('请输入密码');
			return;
		}
		if(data.psw.length < 6){
			frame_obj.tips('密码长度不能小于6位');
			return;
		}
		$('#ajax-url').val($('#ajax_edit_psw').val());
		frame_obj.lock_screen('数据提交中...');
		frame_obj.do_ajax_post($(this), '', data, function(data){
			frame_obj.unlock_screen();
			if(data['errcode'] != 0){
				frame_obj.tips(data['errmsg']);
				return;
			}
			frame_obj.tips('成功修改密码');
			setTimeout(function(){
				$('#psw_edit').val('');
				$('#psw_add_show').val('');
				$('.save_edit_psw').parent().addClass('hide');
				$('.personal_msg').removeClass('hide');
			},1000);
		});
	});
}