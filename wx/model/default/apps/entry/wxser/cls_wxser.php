<?php
/**
 * PC入口
 * 
 * @author yangpz
 * @date 2015-07-24
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『综合服务』操作指引',
		'desc' => '欢迎使用综合服务应用，在这里你可以通过扫描二维码一键登录pc端，不用记住一大堆密码了，快快扫描下！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=5&sn=1671723e8b060127681677c1883ee6e6&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		/*
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
		*/
		
		echo  g('wxqy') -> get_text_reply($this -> post_data, "手机打字太慢？在电脑浏览器输入： pc.easywork365.com \n点击手机底部登录按钮即可扫码登录！");
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	public function enter_advice() {
		/*
		$wxacct = $this -> post_data['FromUserName'];
		$redis_key = $this -> get_redis_key($this -> com_id, $wxacct);
		if (g('redis') -> get($redis_key)) {
			return;
		}
		g('redis') -> set($redis_key, 'done', 24*3600);	//一天提醒一次
		echo  g('wxqy') -> get_text_reply($this -> post_data, "手机打字太慢？在电脑浏览器输入： pc.easywork365.com \n点击手机底部登录按钮即可扫码登录！");
		*/
	}
	
	//--------------------------------------内部实现---------------------------
	
	private function get_redis_key($com_id, $wxacct) {
		$key = md5(SYSTEM_HTTP_DOMAIN.':'.__CLASS__.':'.__FUNCTION__.':entry:'.$com_id.':'.$wxacct);
		return $key;
	}
	
}

// end of file