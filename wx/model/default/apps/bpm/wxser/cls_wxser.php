<?php
/**
 * 流程专家
 * 
 * @author yangpz
 * @date 2014-10-21
 *
 */
class cls_wxser extends abs_app_wxser {
	
	//--------------------------------------内部实现---------------------------

	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '您好，我是流程专家，很高兴为您服务！',
		'desc' => '您的满意，我们的动力！',
		'pic_url' => '/apps/bpm/static/images/guide.png',
		'url' => 'http://mp.weixin.qq.com/s/Du3_VfM3riRMuZlVFJqipA'
	);

    public function __construct() {
        $this -> help_art['pic_url'] = SYSTEM_STATIC_DOMAIN . $this -> help_art['pic_url'];
    }

    protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
}

// end of file