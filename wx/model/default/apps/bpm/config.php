<?php
/**
 * 应用的配置文件_意见反馈
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id'       => 23,                                          //对应sc_app表的id
    //此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
    'combo' => g('com_app') -> get_sie_id(23),
    'name'      => 'bpm',
    'cn_name'   => '流程专家',
);

// end of file