<?php
/**
 * 应用的配置文件_工作日程
 * 
 * @author chenyihao
 * @date 2014-12-03
 * 
 */

return array(
	'id' 				=> 5,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(5),
	'name' => 'task',
	'cn_name' => '任务管理',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Task.png',
	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '新建', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=create_page', 'icon' => '')
		),
		1 => array(
			array('id' => 'bmenu-1', 'name' => '任务添加', 
				'childs' => array(
					array('id' => 'bmenu-1-1', 'name' => '新建任务', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=create_page'),
					array('id' => 'bmenu-1-2', 'name' => '我的草稿', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=create_list'),
				)
			),
			array('id' => 'bmenu-1', 'name' => '我的任务', 
				'childs' => array(
					array('id' => 'bmenu-1-1', 'name' => '我负责的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=perform_list'),
					array('id' => 'bmenu-1-2', 'name' => '我分配的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=assign_list'),
					array('id' => 'bmenu-1-2', 'name' => '我相关的', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=task&m=task&a=superintendent_list'),
				)
			),
		),
		102 => array(//我承办的任务菜单
			array('id' => 'bmenu-3', 'name' => '完成', 'url' => 'javascript:void(0)', 'icon' => '')
		),
		101 => array(//我分配的任务菜单
			array('id' => 'bmenu-1', 'name' => '提醒', 'url' => 'javascript:void(0)', 'icon' => ''),
			array('id' => 'bmenu-3', 'name' => '终止', 'url' => 'javascript:void(0)', 'icon' => '')
		),
		103 => array(//我监督的任务菜单
			array('id' => 'bmenu-1', 'name' => '提醒', 'url' => 'javascript:void(0)', 'icon' => '')
		)
	),
);

// end of file