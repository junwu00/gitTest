<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'task_assign_index.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/assign/index.css'
	),
    'task_assign_index.js' => array(
        SYSTEM_APPS . 'task/static/js/index/assign.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js'
    ),
	'task_detail_task_detail.css' => array(
		SYSTEM_APPS . 'task/static/css/bootstrap-datetimepicker.min.css',
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/icheck/red.css',
		SYSTEM_APPS . 'task/static/css/icheck/blue.css',
		SYSTEM_APPS . 'task/static/css/icheck/orange.css',
		SYSTEM_APPS . 'task/static/css/icheck/grey.css',
		SYSTEM_APPS . 'task/static/css/detail/index.css'
	),
    'task_detail_task_detail.js' => array(
        SYSTEM_APPS . 'task/static/js/icheck.min.js',
        SYSTEM_APPS . 'task/static/js/detail/index.js'
    ),
	'task_helper_helper.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/red.css',
		SYSTEM_APPS . 'task/static/css/icheck/blue.css',
		SYSTEM_APPS . 'task/static/css/helper/helper.css'
	),
    'task_helper_helper.js' => array(
        SYSTEM_APPS . 'task/static/js/icheck.min.js',
        SYSTEM_APPS . 'task/static/js/index/helper_detail.js'
    ),
	'task_helper_index.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/helper/index.css'
	),
    'task_helper_index.js' => array(
        SYSTEM_APPS . 'task/static/js/index/helper.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js'
    ),
	'task_index_approval.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/red.css',
		SYSTEM_APPS . 'task/static/css/icheck/blue.css',
		SYSTEM_APPS . 'task/static/css/index/approval.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_approval.js' => array(
        SYSTEM_APPS . 'task/static/js/icheck.min.js',
        SYSTEM_APPS . 'task/static/js/index/approval.js',
        SYSTEM_APPS . 'task/static/js/bootstrap-datetimepicker.min.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_callback.css' => array(
		SYSTEM_APPS . 'task/static/css/index/callback.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_callback.js' => array(
        SYSTEM_APPS . 'task/static/js/index/callback.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_delay.css' => array(
		SYSTEM_APPS . 'task/static/css/index/delay.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_delay.js' => array(
        SYSTEM_APPS . 'task/static/js/index/delay.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_finish.css' => array(
		SYSTEM_APPS . 'task/static/css/index/finish.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_finish.js' => array(
        SYSTEM_APPS . 'task/static/js/index/finish.js',
        SYSTEM_ROOT . 'static/js/storage.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_index.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
	),
    'task_index_index.js' => array(
        SYSTEM_APPS . 'task/static/js/index/index.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js'
    ),
    'task_index_log.css' => array(
		SYSTEM_APPS . 'task/static/css/index/log.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_log.js' => array(
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_remind.css' => array(
		SYSTEM_APPS . 'task/static/css/index/remind.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_remind.js' => array(
        SYSTEM_APPS . 'task/static/js/index/remind.js',
        SYSTEM_ROOT . 'static/js/storage.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_report.css' => array(
		SYSTEM_APPS . 'task/static/css/index/report.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_report.js' => array(
        SYSTEM_APPS . 'task/static/js/index/report.js',
        SYSTEM_ROOT . 'static/js/jquery.form.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_index_upload.css' => array(
		SYSTEM_APPS . 'task/static/css/index/upload.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_index_upload.js' => array(
        SYSTEM_APPS . 'task/static/js/index/upload.js',
        SYSTEM_ROOT . 'static/js/jquery.form.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
    ),
    'task_new_contact.css' => array(
		SYSTEM_APPS . 'task/static/css/contact.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/square/green.css'
	),
    'task_new_contact.js' => array(
        SYSTEM_ROOT . 'static/js/icheck/icheck.js',
        SYSTEM_APPS . 'task/static/js/new/contact.js'
    ),
    'task_new_list.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/new/list.css'
	),
    'task_new_list.js' => array(
        SYSTEM_APPS . 'task/static/js/new/list.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js'
    ),
    'task_new_new.css' => array(
		SYSTEM_APPS . 'task/static/css/bootstrap-datetimepicker.min.css',
		SYSTEM_APPS . 'task/static/css/new/new.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
    'task_new_new.js' => array(
        SYSTEM_APPS . 'task/static/js/new/index.js',
        SYSTEM_APPS . 'task/static/js/bootstrap-datetimepicker.min.js',
        SYSTEM_ROOT . 'static/js/jquery.form.js',
        SYSTEM_ROOT . 'static/js/storage.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
    ),
    'task_perfrom_helper.css' => array(
		SYSTEM_APPS . 'task/static/css/perform/helper.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'task_perfrom_helper.js' => array(
        SYSTEM_APPS . 'task/static/js/index/perform_helper.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js'
    ),
    'task_perfrom_index.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/perform/index.css'
	),
    'task_perfrom_index.js' => array(
        SYSTEM_APPS . 'task/static/js/index/perform.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js?'
    ),
    'task_perfrom_report.css' => array(
		SYSTEM_APPS . 'task/static/css/perform/report.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		 //上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
    'task_perfrom_report.js' => array(
        SYSTEM_APPS . 'task/static/js/index/report.js',
        SYSTEM_ROOT . 'static/js/storage.js',
        SYSTEM_ROOT . 'static/js/jquery.form.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
       //上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
    ),
    'task_superintendent_index.css' => array(
		SYSTEM_APPS . 'task/static/css/icheck/green.css',
		SYSTEM_APPS . 'task/static/css/superintendent/index.css'
	),
    'task_superintendent_index.js' => array(
        SYSTEM_APPS . 'task/static/js/index/superintendent.js',
        SYSTEM_APPS . 'task/static/js/icheck.min.js'
    ),
);

return $arr;

/* End of this file */