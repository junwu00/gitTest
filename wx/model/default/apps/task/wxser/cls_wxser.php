<?php
/**
 * 工作日程
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『任务协作』操作指引',
		'desc' => '欢迎使用任务协作应用，在这里你可以随时随地安排任务，处理任务，任务进展一目了然，发个任务刷下你的存在感吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970246&idx=4&sn=caef27a0685eb3ffb35c11c5f6ebcc6e#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	//--------------------------------------内部实现---------------------------
	
}

// end of file