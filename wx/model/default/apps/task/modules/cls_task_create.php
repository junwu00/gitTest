<?php
/**
 * 任务协作--任务列表
 * @author lijuns
 * @date 2014-12-27
 *
 */
class cls_task_create extends abs_task {
	
	public function __construct() {
		parent::__construct('task');
	}
	
	private static $States = 101;
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'save_task':
				$this -> save_task();
				break;
			case 'delete_task':
				$this -> delete_task();
				break;
			case 'upload':
				$this -> upload_form();
				break;
			case 'check_time':
				$this -> check_time();
				break;
			default:
				 parent::error_page();
				
		}
	}
	
	//----------------------------------内部实现----------------------------------

	
	
	/**
	 * 保存更新任务信息
	 * @throws SCException
	 */
	private function save_task(){
		try {			
			parent::log('开始保存任务信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('id','task_title', 'task_describe',
			'pid', 'state', 'task_type', 'starttime', 'endtime', 'isfinish',"degree",
			'file_list_str','perform_id','perform_name','superintendent_id','superintendent_name'));
			
			$starttime = strtotime($data['starttime']);
			$endtime = strtotime($data['endtime']);
			if($starttime > $endtime)
				throw new SCException('开始时间不能大于结束时间！');
			
				
			$task_p_id = g('des')->decode($data['pid']);
			if($task_p_id){
				$p_task = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $task_p_id,'*');
				if($p_task['endtime']<strtotime($data['endtime'])){
					throw new SCException('子任务时间不允许超出父任务时间');
				}
			}
				
				
			//更新任务
			$data['writer_id'] = $_SESSION[SESSION_VISIT_USER_ID];
			$data['writer_name'] = $_SESSION[SESSION_VISIT_USER_NAME];
			$data['writer_com_id'] = $_SESSION[SESSION_VISIT_COM_ID];
			
			log_write($_SESSION[SESSION_VISIT_COM_ID]);
			
			//发送微信推送人员
			$perform = array();
			$superintendent = array();
			if(!empty($data['id'])){
				$data['id'] = g('des') -> decode($data['id']);
				$ret = $data['id'];
				
				$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $data['id'],'*',self::$States);
				
				
				//更新附件
				g('task_file') -> delete_file_taskid($data['id'],'task_info');
				if(!empty($data["file_list_str"]) && is_array($data['file_list_str'])){
					$files = $data["file_list_str"];
					g("task_file") -> save($files,$data['id'],'task_info',$_SESSION[SESSION_VISIT_USER_ID],$_SESSION[SESSION_VISIT_USER_NAME]);
//					g("task_file")->update_file_taskid($data['id'],$data["file_list_str"]);
				}
				
				//草稿任务修改才需要修改承办人，监督人信息，执行中修改不能修改承办人，监督人信息
				if($task_info["state"]== parent::$StateDraft){
					//封装承办人数组
					g('task_perform_base')->delete($data['id']);
					$perform_ids_arr = explode(",",$data["perform_id"]);
					$perform_names_arr = explode(",",$data["perform_name"]);
					$count = 0;
					$perform_info_arr = array();
					foreach ($perform_ids_arr as $perform_id)
					{
						if($perform_id){
							array_push($perform,$perform_id);
							$temp_arr = array($data["id"],$perform_id,$perform_names_arr[$count],parent::$StateOn,time(),parent::$StateRunning);
							$perform_info_arr[$count] = $temp_arr;
							$count++;
						}
					}
					g('task_perform_base')->batch_save($perform_info_arr);
					
					
					
					//封装监督人数组
					g('task_superintendent')->delete($data['id']);
					$superintendent_ids_arr = explode(",",$data["superintendent_id"]);
					$superintendent_names_arr = explode(",",$data["superintendent_name"]);
					$count = 0;
					$superintendent_info_arr = array();
					foreach ($superintendent_ids_arr as $superintendent_id)
					{
						if($superintendent_id){
							array_push($superintendent,$superintendent_id);
							$temp_arr = array($data["id"],$superintendent_id,$superintendent_names_arr[$count],parent::$StateOn,time());
							$superintendent_info_arr[$count] = $temp_arr;
							$count++;
						}
					}
					g('task_superintendent')->batch_save($superintendent_info_arr);
				}
				else{//执行中封装发送人
					$perform_ids_arr = explode(",",$task_info["perform_id"]);
					foreach ($perform_ids_arr as $perform_id)
					{
						array_push($perform,$perform_id);
					}
					$superintendent_ids_arr = explode(",",$task_info["superintendent_id"]);
					foreach ($superintendent_ids_arr as $superintendent_id)
					{
						array_push($superintendent,$superintendent_id);
					}
				}
				
				if($data['state']!=0){
					//任务日志
					$log_info = array(
						"taskid"=>$data["id"],
						"loger_id"=>$_SESSION[SESSION_VISIT_USER_ID],
						"loger_name"=>$_SESSION[SESSION_VISIT_USER_NAME],
						"log_content"=>"修改任务"
					);
					g('task_log')->save($log_info);
				}
				
				g('task_info')->update($data);
				
				//消息提醒内容
				if($task_info['state']==0){
					$msg_title = '任务分配提醒';
					$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'分配给您一条任务，请及时处理！';
					$pc_msg_desc = '任务协作：您有一条新的任务了！';
					$msg_super_title = '任务相关提醒';
					$msg_super_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'发布了一条与您相关的任务，请及时查看！';
					$pc_super_msg_desc = '任务协作：有一条与您相关的任务发布了，请及时查看！';
				}
				else{
					$msg_title = '任务修改提醒';
					$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'修改《'.$data["task_title"].'》任务，请及时查看！';
					$pc_msg_desc = '任务协作：您有一条任务更新了！';
				}
			}
			else{//插入任务
				$data["assign_id"]=$_SESSION[SESSION_VISIT_USER_ID];
				$data["assign_name"]=$_SESSION[SESSION_VISIT_USER_NAME];
				$data["com_id"]=$_SESSION[SESSION_VISIT_COM_ID];
				$data['pid'] = g('des')->decode($data['pid']);
				
				$ret = g('task_info')->save($data);
				//更新附件taskid
				if(!empty($data["file_list_str"]) && is_array($data["file_list_str"])){
					$files = $data["file_list_str"];
					g("task_file") -> save($files,$ret,'task_info',$_SESSION[SESSION_VISIT_USER_ID],$_SESSION[SESSION_VISIT_USER_NAME]);
				}
//				g("task_file")->update_file_taskid($ret,$data["file_list_str"]);
				//封装承办人数组
				$perform_ids_arr = explode(",",$data["perform_id"]);
				$perform_names_arr = explode(",",$data["perform_name"]);
				$count = 0;
				$perform_info_arr = array();
				$perform = array();
				foreach ($perform_ids_arr as $perform_id)
				{
					if(!$perform_id)
						continue;
					array_push($perform,$perform_id);
					$temp_arr = array($ret,$perform_id,$perform_names_arr[$count],parent::$StateOn,time(),parent::$StateRunning);
					$perform_info_arr[$count] = $temp_arr;
					$count++;
				}
				g('task_perform_base')->batch_save($perform_info_arr);
				//封装监督人数组
				$superintendent_ids_arr = explode(",",$data["superintendent_id"]);
				$superintendent_names_arr = explode(",",$data["superintendent_name"]);
				$count = 0;
				$superintendent_info_arr = array();
				$superintendent = array();
				foreach ($superintendent_ids_arr as $superintendent_id)
				{
					if(!$superintendent_id)
						continue; 
					array_push($superintendent,$superintendent_id);
					$temp_arr = array($ret,$superintendent_id,$superintendent_names_arr[$count],parent::$StateOn,time());
					$superintendent_info_arr[$count] = $temp_arr;
					$count++;
				}
				g('task_superintendent')->batch_save($superintendent_info_arr);
				//任务日志
				$log_info = array(
					"taskid"=>$ret,
					"loger_id"=>$_SESSION[SESSION_VISIT_USER_ID],
					"loger_name"=>$_SESSION[SESSION_VISIT_USER_NAME],
					"log_content"=>"新增任务"
				);
				g('task_log')->save($log_info);
				
				//消息提醒内容
				$msg_title = '任务分配提醒';
				$msg_super_title = '任务相关提醒';
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'分配给您一条任务，请及时处理！';
				$msg_super_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'发布了一条与您相关的任务，请及时查看！';
				$pc_msg_desc = '任务协作：您有一条新任务需要处理！';
				$pc_super_msg_desc = '任务协作：有一条跟您相关的任务发布了，请及时查看！';
			}
			$msg = '保存成功';
			//如果任务保存为草稿，不发送信息
			if($data["state"]!=self::$StateDraft){
				//发送申请提醒
				$msg = '发布成功';
					
				$to_users = g('user') -> get_by_ids($perform);
				$to_users_superintendent = g('user') -> get_by_ids($superintendent);
				if (!$to_users) {
					throw new SCException('找不到发送人信息');
				}
				
				$taskid =g('des') ->encode($ret);
				$fail_user = '';
				foreach ($to_users as $to_user)
				{
					$user_id = $to_user['id'];
					$msg_user = array($to_user['acct']);
					$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$taskid.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
					$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_perform&a=detail&f=0&i='.$taskid;//TODO
					try{
						//发送PC提醒
						$data = array(
							'url' => $pc_url,
							'title' => $pc_msg_desc,
							'app_name' => parent::$AppName
						);
						g("messager")->publish_by_user('pc', $user_id,$data);
						parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
					}catch(SCException $e){
						$fail_user .=$to_user['name'].' ';
					}
				}
				if( isset($pc_super_msg_desc) && isset($msg_super_title) && isset($msg_super_desc))
				foreach ($to_users_superintendent as $to_user)
				{
					$user_id = $to_user['id'];
					$msg_user = array($to_user['acct']);
					$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=103&taskid='.$taskid.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
					$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task&a=superientfent_page&f=0&i='.$taskid;//TODO
					try{
						//发送PC提醒
						$data = array(
							'url' => $pc_url,
							'title' => $pc_super_msg_desc,
							'app_name' => parent::$AppName
						);
						g("messager")->publish_by_user('pc', $user_id,$data);
						parent::send_single_news($msg_user, $msg_super_title, $msg_super_desc, $msg_url);
					}catch(SCException $e){
						$fail_user .=$to_user['name'].' ';
					}
				}
				if($fail_user != ''){
					$msg = '发布成功<br>以下成员无法接收信息<br>'.$fail_user.'<br>失败原因：未关注/被禁用/网络异常';
				}
			}
			g('db') -> commit();
			parent::log('保存任务信息成功');
			echo json_encode(array('errcode'=>0,'errmsg'=>$msg));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 检查时间
	 */
	private function check_time(){
		try{
			$data = get_var_post('data');
			
			cls_resp::echo_ok();
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
/**
	 * 删除任务信息
	 */
	private function delete_task(){
		try {
			parent::log('开始删除任务信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('ids'));
			$ids_arr = explode(",",$data["ids"]);
			foreach ($ids_arr as $id)
			{
				if(!$id)
					continue;
				$task_info = array(
					"id"=>g('des') -> decode($id),
					"writer_id"=>$_SESSION[SESSION_VISIT_USER_ID]
				);
				g('task_info')->delete($task_info);
			}
			g('db') -> commit();
			parent::log('删除任务信息成功');
			echo json_encode(array('errcode'=>0,'errmsg'=>'删除成功'));
		}catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 上传文件
	 */
	private function upload_form(){
		log_write('开始上传文件');
		try {
			$a = get_var_post('type');
			$file_type = g('media') -> get_file_type($a);
			if($file_type == 'image'){
				$type = 1;
			}elseif ($file_type == 'voice'){
				$type = 2;
			}elseif ($file_type == 'video'){
				$type = 3;
			}else {
				$type = 5;
			}
			
			$max_size = 0;
			switch ($type) {
				case 1:	//图片 1M
					$max_size = 1024000;
					break;
				case 2:	//音频 2M
					$max_size = 2048000;
					break;
				case 3:	//视频 10M
					$max_size = 10240000;
					break;
				case 5:	//文件 10M
					$max_size = 10240000;
					break;
			}
			$params = g('media') -> cloud_build_params($max_size);
			$file = g('media') -> cloud_upload($params['type'], $params['file']);
			
			$file_info = array(
				'objectid' => '',
				'upload_table'=>'',
				'file_name' => '',
				'file_url' => '',
				'file_type' => '',
				'upload_id' => '',
				'upload_name' => '',
				'upload_time' => '',
				'info_state' => parent::$StateOn
			);
			
			//g('task_file') ->save($file_info);
			
			$url = $file['path'];
			$hash = $file['hash'];
			$data = array(
				'url' => $url,
				'title' => '',
				'desc' => '',
			);
			log_write('上传素材成功');
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('art' => $ret,'hash' => $hash, 'url' => $url, 'name' => $params['name'], 'ext' => $params['ext'], 'size' => $params['size']/1024));
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
}

// end of file