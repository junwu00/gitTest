<?php
/**
 * 任务协作--任务列表
 * @author lijuns
 * @date 2014-12-27
 *
 */
class cls_task_helper extends abs_task {
	
	private static $States = 104;
	
	public function __construct() {
		parent::__construct('task');
	}
	
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'checkout_task':
				$this -> checkout_task();
				break;
			default:
				 cls_resp::show_err_page(array('找不到页面'));
				
		}
	}
	
	/**
	 * 确认协办页面
	 */
	public function checkout_page() {
		try {			
			parent::log('开始确认协办');
			$helpinfoid = get_var_value("helpinfoid");
			$helpinfoid = g('des') -> decode($helpinfoid);
			$task_helper = g("task_helper_base")->select_helper_id($helpinfoid,$_SESSION[SESSION_VISIT_USER_ID]);
			if(empty($task_helper)){
				throw new SCException('没有权限确认协办');
			}
			if($task_helper[0]['is_agree']==2){
				header("Location:index.php?app=task&m=task&a=task_page&states=104&taskid=".g('des')->encode($task_helper[0]['taskid']));
				return;
			}else if($task_helper[0]['is_agree']==1){
				cls_resp::show_err_page(array('您没有权限查看该任务信息！'));
				return;
			}
			
			$task_helper[0]["id"] = g('des') -> encode($task_helper[0]["id"]);
			
			$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $task_helper[0]['taskid'],"*",self::$States);
			$user_info = g("user")->get_by_id($task_helper[0]["apply_id"]);
			$task_info["pic_url"] = $user_info["pic_url"];
			$task_info["id"] = g('des') -> encode($task_info["id"]);
			
			parent::log('确认协办成功');
			g('smarty') -> assign('task_helper', $task_helper[0]);
			g('smarty') -> assign('task_info', $task_info);
				
			g('smarty') -> assign('title', '确认协办任务');
			g('smarty') -> show($this -> temp_path.'helper/helper.html');
		} catch (SCException $e) {
			g('smarty') -> assign('title', '错误');
			g('smarty') -> show($this -> temp_path.'lists/index.html');
		}
	}
	
	
	//----------------------------------内部实现----------------------------------

	/**
	 * 确认协办信息
	 */
	private function checkout_task(){
		try {
			parent::log('开始确认协办信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('id','taskid','is_agree','helper_content'));
			
			$id = g('des') -> decode($data['id']);
			$task_id = g('des') -> decode($data['taskid']);
			$helper_info = array(
				'id'=>$id,
				'is_agree' => $data['is_agree'],
				'helper_content' => $data['helper_content'],
				'helper_id'=>$_SESSION[SESSION_VISIT_USER_ID],
				'taskid'=>$task_id
			);
			g("task_helper_base")->update($helper_info);
			
			$task = g('task_info') -> get_by_com($task_id,$_SESSION[SESSION_VISIT_USER_ID],'*',self::$States);
			
			if($data['is_agree']==2){
				$log_content = "确认协办";
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'同意了任务《'.$task['task_title'].'》的协办！';
			}
			else{
				$log_content = "驳回协办";
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'拒绝了任务《'.$task['task_title'].'》的协办！';
			}
			//任务日志
			$log_info=array(
				'taskid' => $taskid,
				'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'log_content' => $log_content
			);
			g("task_log")->save($log_info);
			
			
			//消息提醒内容
			$msg_title = '确认协办提醒';
			
			
			$perform = g('task_perform_base') -> get_by_com($task_id,'perform_id');
			$perform_array = array(); 
			foreach($perform as $p){
				array_push($perform_array, $p['perform_id']);
			}
		
			$to_users = g('user') -> get_by_ids($perform_array);
			
			if (!$to_users) {
				throw new SCException('找不到发送人信息');
			}
			foreach ($to_users as $to_user)
			{
				$to_user = $to_user['acct'];
				$msg_user = array($to_user);
				$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$data['taskid'].'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
				try{
					parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
				}catch(SCException $e){
				}
			}
			
			
			g('db') -> commit();
			parent::log('确认协办信息成功');
			cls_resp::echo_ok();
		}catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
}

// end of file