<?php
/**
 * 任务协作--任务列表
 * @author lijuns
 * @date 2014-12-27
 *
 */
class cls_task_assign extends abs_task {
	
	private static $States = 101;
	public function __construct() {
		parent::__construct('task');
	}
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'approval_task':
				$this -> approval_task();
			case 'stop_task':
				$this -> stop_task();
			case 'remind_task':
				$this -> remind_task();
			case 'upload_file':
				$this -> upload();
			default:
				 cls_resp::show_err_page(array('找不到页面'));
				
		}
	}
	
	/**
	 * 任务审批页面
	 */
	public function approval_page(){
		$taskid = g('des') -> decode(get_var_value("taskid"));
		$id = g('des') -> decode(get_var_value("id"));
		$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $taskid,"*",self::$States);
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		$task_applys = g("task_apply")->select_list_taskid($taskid,$_SESSION[SESSION_VISIT_USER_ID],$id);
		log_write(json_encode($task_applys));
		$task_applys[0]['id'] =g('des') -> encode($task_applys[0]['id']);
		$task_applys[0]['apply_time'] = date('Y-m-d H:i',$task_applys[0]['apply_time']);			//转换申请时间
		$task_applys[0]['delayed_time'] = date('Y-m-d H:i',$task_applys[0]['delayed_time']);		//转换申请延时时间
		$task_applys[0]['finish_time'] = date('Y-m-d H:i',$task_applys[0]['finish_time']);			//转换申请完成时间
		$state_name = parent::$TASKSTATE[$task_info["state"]];										//获取状态名
		
		g('smarty') -> assign('title', '任务审批');
		g('smarty')	-> assign('taskid', get_var_value("taskid"));
		g('smarty')	-> assign('task_apply', $task_applys[0]);
		g('smarty')	-> assign('state', $task_info["state"]);
		g('smarty')	-> assign('state_name', $state_name);
		g('smarty') -> show($this -> temp_path.'index/approval.html');
	}
	
	/**
	 * 任务提醒页面
	 */
	public function remind_page(){
		$states = get_var_value("states");
		$des_id = get_var_value("taskid");
		$taskid = g('des') -> decode($des_id);
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}

		//$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $taskid,'*',self::$States);
		//$task_msg = "您的承办的任务《".$task_info["task_title"]."》快到期了，请及时处理！";
		g('smarty') -> assign('storagr_key',__FUNCTION__.$_SESSION[SESSION_VISIT_USER_ID].$des_id);
		g('smarty') -> assign('title', '任务提醒');
		g('smarty')	-> assign('states', $states);
		g('smarty')	-> assign('task_id', get_var_value("taskid"));
		//g('smarty')	-> assign('task_msg', $task_msg);
		g('smarty') -> show($this -> temp_path.'index/remind.html');
	}
	
	/**
	 * 上传附件
	 */
	public function upload_page(){
		$taskid = get_var_value("taskid");
		$states = get_var_value("states");
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		g('smarty') -> assign('title', '上传附件');
		g('smarty')	-> assign('taskid', $taskid);
		g('smarty')	-> assign('states', $states);
		g('smarty') -> show($this -> temp_path.'index/upload.html');
	}
	
	//----------------------------------内部实现----------------------------------
	
	/**
	 * 上传附件
	 */
	private function upload(){
		parent::log('开始上传附件');
		try{
			$data = parent::get_post_data(array('taskid','file_info_str'));
			$taskid = g('des') -> decode($data['taskid']);
			$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $taskid,'*',self::$States);
			g("task_file")->update_file_taskid($taskid,$data["file_info_str"]);
			log_write('上传成功');
			cls_resp::echo_ok();
		}catch(SCException $e){
			log_write('上传失败');
			cls_resp::echo_exp($e);
		}
		
	}
	
	private function approval_task(){
		try {
			parent::log('开始审批任务');
			
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('id','taskid','state','is_agree','answer_content','send_back_id','send_back_name','score'));
			
			//解码taskid和apply_id
			$data['id'] = $taskid = g('des') -> decode($data["id"]);
			$data['taskid'] = $taskid = g('des') -> decode($data["taskid"]);
			//查询神品记录信息
			$apply = g("task_apply")->get_apply_id($data['id']);
			if($apply['is_agree']!=0){
				throw new SCException('您已经审批过该申请了！');
			}
			
			//申请表
			$apply_info=array(
				'id'=>$data['id'],
				'taskid' => $data['taskid'],
				'is_agree' => $data['is_agree'],
				'answer_content' => $data['answer_content'],
				'answer_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'answer_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'send_back_id' => $data['send_back_id'],
				'send_back_name' => $data['send_back_name'],
				'score' => $data['score']
			);
			g("task_apply")->update($apply_info);
			
			$task_info = g('task_info') -> get_by_com($data['taskid'],$_SESSION[SESSION_VISIT_USER_ID],'*',self::$States);
			
			$state = $data["state"];
			$is_agree = $data["is_agree"];
			$send_id=0;
			$msg_desc="";
			//不同意申请
			if($is_agree==parent::$TaskAgreeOff){
				//更新承办人表状态
				$perform_data=array(
					"state"=>parent::$StateRunning,
					"finish_time"=>""
				);
				g("task_perform_base")->update_approve_info($data["taskid"],$_SESSION[SESSION_VISIT_USER_ID],$apply['applyer_id'],$perform_data);
				
				//更新任务信息状态
				g("task_info")->update_approve_info($data["taskid"],$_SESSION[SESSION_VISIT_USER_ID],$perform_data);
				
				$send_id = $apply["applyer_id"];
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'不同意您对《'.$task_info["task_title"].'》'.parent::$TASKSTATE[$state].'，请及时查看！';
				$pc_msg_desc = '任务协作：您的申请完成已被拒绝！';
				//任务日志
				$log_info=array(
					'taskid' => $data["taskid"],
					'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
					'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
					'log_content' => "不同意".parent::$TASKSTATE[$state]
				);
				g("task_log")->save($log_info);
				//发送申请提醒
				$to_users = g('user') -> get_by_ids($send_id);
				if (!$to_users) {
					throw new SCException('找不到发送人信息');
				}
				$taskid = g('des')-> encode($data['taskid']);
				foreach ($to_users as $to_user)
				{
					$user_id = $to_user['id'];
					$msg_user = array($to_user['acct']);
					$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$taskid.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//todo
					$msg_title = '任务审批提醒';
					$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_perform&a=detail&f=0&i='.$taskid;//TODO
					try{
						//发送PC提醒
						$data = array(
							'url' => $pc_url,
							'title' => $pc_msg_desc,
							'app_name' => parent::$AppName
						);
						g("messager")->publish_by_user('pc', $user_id,$data);
						parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
					}catch(SCException $e){
						log_write($e->getMessage());
					}
				}
				
			}
			else{//同意申请
				if($state==parent::$StateApplyFinish){
					
					if($data['is_agree']==2){
						if(!is_numeric($data['score'])){
							throw new SCException('评分必须为数字');
						}
						if( 0 > $data['score'] || $data['score'] >100){
							throw new SCException('评分范围必须为0-100');
						}
						
					}
					
					//查询任务基本信息
					$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $data["taskid"],"*",self::$States);
					$endtime=$task_info["endtime"];
					$finishtime = $task_info["finish_time"];
					$temp_state = parent::$StateOntime;
					if($finishtime>$endtime){
						$temp_state = parent::$StateOverdue;
					}
					//更新承办人表状态
					$perform_data = array(
						"state"=>$temp_state
					);
					g("task_perform_base")->update_approve_info($data["taskid"],$_SESSION[SESSION_VISIT_USER_ID],$apply['applyer_id'],$perform_data);
					
					$running_perform = g('task_perform_base') -> get_running_perform($data["taskid"]);
					if(empty($running_perform)){
						//更新任务信息
						$task_data = array(
							"state"=>$temp_state,
							"isfinish"=>parent::$IsFinishOn
						);
						g("task_info")->update_approve_info($data["taskid"],$_SESSION[SESSION_VISIT_USER_ID],$task_data);
					}else{
						$task_data = array(
							"state" =>$running_perform['state'],
						);
						g("task_info")->update_approve_info($data["taskid"],$_SESSION[SESSION_VISIT_USER_ID],$task_data);
					}
					
					$send_id = $apply["applyer_id"];
					$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'同意您对《'.$task_info["task_title"].'》'.parent::$TASKSTATE[$state].'，请及时查看！';
					$pc_msg_desc = '任务协作：您的申请完成已经通过！';
					
					//任务日志
					$log_info=array(
						'taskid' => $data["taskid"],
						'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
						'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
						'log_content' => "同意".parent::$TASKSTATE[$state]."，任务完成状态为".parent::$TASKSTATE[$temp_state]
					);
					g("task_log")->save($log_info);
					
					//发送申请提醒
					$to_users = g('user') -> get_by_ids($send_id);
					if (!$to_users) {
						throw new SCException('找不到发送人信息');
					}
					$taskid = g('des')-> encode($data['taskid']);
					foreach ($to_users as $to_user)
					{
						$user_id = $to_user['id'];
						$to_user = $to_user['acct'];
						$msg_user = array($to_user);
						$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$taskid.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
						$msg_title = '任务审批提醒';
						$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_perform&a=detail&f=0&i='.$taskid;//TODO
						try{
							//发送PC提醒
							$data = array(
								'url' => $pc_url,
								'title' => $pc_msg_desc,
								'app_name' => parent::$AppName
							);
							g("messager")->publish_by_user('pc', $user_id,$data);
							parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
						}catch(SCException $e){
							log_write($e->getMessage());
						}
					}
					
					//更新子任务状态（不包括已完成的任务）
					$son_task_ids = array();
					$son_task_ids = $this->get_next_son_task($data["taskid"], $son_task_ids);
					for ($j=0;$j<count($son_task_ids);$j++){
						$son_task_id = $son_task_ids[$j];
						g("task_perform_base")->update_approve_info($son_task_id,$_SESSION[SESSION_VISIT_USER_ID],$perform_data,FALSE);
						g("task_info")->update_approve_info($son_task_id,$_SESSION[SESSION_VISIT_USER_ID],$task_data,FALSE);
						//任务日志
						$log_info=array(
							'taskid' => $son_task_id,
							'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
							'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
							'log_content' => "父任务已经结束，该任务状态更新为".parent::$TASKSTATE[$temp_state]
						);
						g("task_log")->save($log_info);
					}
				}
			}
			
			g('db') -> commit();
			parent::log('审批任务信息成功');
			cls_resp::echo_ok();
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 终止任务
	 * Enter description here ...
	 * @throws SCException
	 */
	private function stop_task(){
		try {
			parent::log('开始终止任务');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('taskid'));
			
			$taskid = g('des') -> decode($data['taskid']);
			$son_task_ids = array($taskid);
			$son_task_ids = $this->get_next_son_task($taskid, $son_task_ids);
			
			
			for ($j=0;$j<count($son_task_ids);$j++){
				if($son_task_ids[$j]==$taskid){
					$is_role=true;
				}
				else{
					$is_role=false;
				}
				$son_task_id = $son_task_ids[$j];
				$task_data=array(
					"state"=>parent::$StateStop,
					"isfinish"=>parent::$IsFinishOn
				);
				//更新任务信息状态
				g("task_info")->update_approve_info($son_task_id,$_SESSION[SESSION_VISIT_USER_ID],$task_data,$is_role);
				
				//更新承办人表状态
				$perform_data=array(
					"state"=>parent::$StateStop
				);
				g("task_perform_base")->update_approve_info($son_task_id,$_SESSION[SESSION_VISIT_USER_ID],'',$perform_data,$is_role);
				
				//任务日志
				$log_info=array(
					'taskid' => $son_task_id,
					'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
					'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
					'log_content' => parent::$TASKSTATE[parent::$StateStop]."任务",
				);
				g("task_log")->save($log_info);
			}
			
			g('db') -> commit();
			parent::log('终止任务成功');
			
			cls_resp::echo_ok();
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 提醒任务
	 * Enter description here ...
	 */
	private function remind_task(){
	try {
			parent::log('开始提醒任务');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('taskid','msg_desc'));
			
			if(!trim($data['msg_desc']))
				throw new SCException('提醒内容不能为空！');
			
			//查询任务基本信息
			$taskid= g('des')-> decode($data['taskid']); 
			$perform_infos = g("task_perform_base")->get_by_com($taskid);
			foreach ($perform_infos as $perform_info)
			{
				$to_users = g('user') -> get_by_ids($perform_info["perform_id"]);
				if (!$to_users) {
					throw new SCException('找不到发送人信息');
				}
				foreach ($to_users as $to_user)
				{
					$user_id = $to_user['id'];
					$to_user = $to_user['acct'];
					$msg_user = array($to_user);
					$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$data['taskid'].'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
					$msg_title = '任务提醒';
					$msg_desc = $_SESSION[SESSION_VISIT_USER_NAME].'：'.$data["msg_desc"];
					$pc_msg_desc = "任务协作：" .$data["msg_desc"].'('.$_SESSION[SESSION_VISIT_USER_NAME].')';
					$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_perform&a=detail&f=1&i='.$data['taskid'];//TODO
					try{
						//发送PC提醒
						$msg = array(
							'url' => $pc_url,
							'title' => $pc_msg_desc,
							'app_name' => parent::$AppName
						);
						g("messager")->publish_by_user('pc', $user_id,$msg);
						parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
					}catch(SCException $e){
						log_write($e->getMessage());
					}
				}	
			}
			
			g('db') -> commit();
			parent::log('提醒任务成功');
			cls_resp::echo_ok();
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	//递归查询子任务
	private function get_next_son_task($taskid,$son_task_ids){
		$son_list = g("task_info")->get_son_list_not_role($taskid);
		if($son_list!=FALSE){
			for($i=0;$i<count($son_list);$i++){
				$son_task_id = $son_list[$i]["id"];
				$son_task_isfinish = $son_list[$i]["isfinish"];
				if($son_task_isfinish==0){
					array_push($son_task_ids,$son_task_id);
				}
				return $this->get_next_son_task($son_task_id, $son_task_ids);
			}
		}
		else{
			return $son_task_ids;
		}
	}
}

// end of file