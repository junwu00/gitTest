<?php
/**
 * 任务协作--任务列表
 * @author lijuns
 * @date 2014-12-27
 *
 */
class cls_task extends abs_task {
	
	public function __construct() {
		parent::__construct('task');
	}	
	
	private static $States = 101;
	private static $page_size = 20;
	
	/** 上传类型 ： 1 ，task_info*/
	private static $task_info = 1;
	/** 上传类型 ： 2 ，task_report*/
	private static $task_report = 2;
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'download_file':
				$this -> download_file();
				break;
			case 'talking_task':
				$this -> talking_task();
				break;
			case 'open_list':
				$this->get_detail_list();
				break;
			case 'publish':
				self::publish();
				break;
			case 'delete':
				self::delete();
				break;
			case 'upload':
				self::upload_form();
				break;
			case 'task':
				self::get_task();
				break;
			case 'load_create_task':
				self::load_create_task();
				break;
			case 'load_perform_task':
				self::load_perform_task();
				break;
			case 'load_superintendent_task':
				self::load_superintendent_task();
				break;
			case 'load_assign_task':
				self::load_assign_task();
				break;
			case 'load_helper_task':
				self::load_helper_task();
				break;
			default:
				 cls_resp::show_err_page(array('找不到页面'));
				
		}
	}
	
	/**
	 * 草稿状态的任务列表
	 */
	public function create_list() {
		try {			
			parent::log('开始查询草稿列表');
			$task_list = g('task_info')->get_draft_list($_SESSION[SESSION_VISIT_USER_ID],'*','order by createtime',"desc",1,self::$page_size,false);
			if ($task_list){
				foreach ($task_list as $key => $task_info)
				{
					$task_list[$key]["id"] = g('des') -> encode($task_info['id']);
					$task_list[$key]["classify_name"] =g("task_classify")->get_task_classify_name($task_info["task_type"]);
					$task_list[$key]["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_list[$key]["perform"] = g('task_perform_base') -> get_by_com($task_info['id'],'perform_name');
					$task_list[$key]["superintendent"] = g('task_superintendent') -> get_by_com($task_info['id'],'superintendent_name');
					if($task_info["starttime"]!=0){
						$task_list[$key]["starttime"]=date('Y-m-d H:i',$task_info["starttime"]);
					}
					else{
						$task_list[$key]["starttime"]="待定";
					}
					if($task_info["endtime"]!=0){
						$task_list[$key]["endtime"]=date('Y-m-d H:i',$task_info["endtime"]);
					}
					else{
						$task_list[$key]["endtime"]="待定";
					}
					$task_title = $task_info["task_title"];
					if(mb_strlen($task_title,'utf-8')>10){
						$task_list[$key]["task_title"] = mb_substr($task_title,0,10,'utf-8').'…';
					}
				}
				
			}
			parent::log('查询草稿列表成功');
			g('smarty') -> assign('task_list', $task_list);
			g('smarty') -> assign('ajax_url', 'index.php?app=task&m=task');
			g('smarty') -> assign('title', '草稿任务');
			g('smarty') -> show($this -> temp_path.'new/list.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 分页加载草稿列表
	 */
	private function load_create_task(){
		log_write('开始加载草稿列表');
		try{
			$data = parent::get_post_data(array('page'));
			
			$task_list = g('task_info')->get_draft_list($_SESSION[SESSION_VISIT_USER_ID],'*','order by createtime',"desc",$data['page'],self::$page_size,false);
			if ($task_list){
				foreach ($task_list as $key => $task_info)
				{
					$task_list[$key]["id"] = g('des') -> encode($task_info['id']);
					$task_list[$key]["classify_name"] =g("task_classify")->get_task_classify_name($task_info["task_type"]);
					$task_list[$key]["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_list[$key]["perform"] = g('task_perform_base') -> get_by_com($task_info['id'],'perform_name');
					$task_list[$key]["superintendent"] = g('task_superintendent') -> get_by_com($task_info['id'],'superintendent_name');
					if($task_info["starttime"]!=0){
						$task_list[$key]["starttime"]=date('Y-m-d H:i',$task_info["starttime"]);
					}
					else{
						$task_list[$key]["starttime"]="待定";
					}
					if($task_info["endtime"]!=0){
						$task_list[$key]["endtime"]=date('Y-m-d H:i',$task_info["endtime"]);
					}
					else{
						$task_list[$key]["endtime"]="待定";
					}
					$task_title = $task_info["task_title"];
					if(mb_strlen($task_title,'utf-8')>10){
						$task_list[$key]["task_title"] = mb_substr($task_title,0,10,'utf-8').'…';
					}
				}
			}
			cls_resp::echo_ok(0,'data',array('data'=>$task_list,'count'=>$task_list?count($task_list):0,'page'=>$data['page']));
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 发布任务（将草稿任务设置为发布状态）
	 */
	private function publish(){
		$data = get_var_post('data');
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$task_id = explode(',',$data['task_id']);
		g('db') -> begin_trans();
		try{
			foreach($task_id as $id){
				if(!$id)
					continue;
				parent::log('开始发布任务task_id:'.$id);
				$id = g('des')-> decode($id);
				$task = g('task_info') -> get_by_com($id,$user_id,'*',self::$States);
				if(!$task)
					throw new SCException('不存在的任务'); 
				$task['state'] =parent::$StateRunning;
				$task['writer_id'] = $user_id;
				$task['writer_name'] = $_SESSION[SESSION_VISIT_USER_NAME];
				$task['writer_com_id'] = $_SESSION[SESSION_VISIT_COM_ID];
				if($task['starttime']==0){
					throw new SCException('发布失败,原因：计划开始时间未填！');
				}
				$task['starttime'] = date('Y-m-d H:i',$task['starttime']);
				if($task['endtime']==0){
					throw new SCException('发布失败,原因：计划结束时间未填！');
				}
				$task['endtime'] = date('Y-m-d H:i',$task['endtime']);
				
				$perform = g('task_perform_base') -> get_by_com($id,'perform_id');
				//log_write(json_encode($perform));
				if($perform==FALSE){
					throw new SCException('发布失败,原因：任务承办人未选！');
				}
				
				if(!g('task_info') -> update($task,""))
					throw new SCException(' 发布失败');
					
				//消息提醒内容
				$msg_title = '任务分配提醒';
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'分配给您一条任务，请及时处理！';
				$pc_msg_desc = '任务协作：您有一条新任务需要处理';
				
				$perform_array = array(); 
				foreach($perform as $p){
					array_push($perform_array, $p['perform_id']);
				}
			
				$to_users = g('user') -> get_by_ids($perform_array);
				
				if (!$to_users) {
					throw new SCException('找不到发送人信息');
				}
				
				$taskid =g('des')-> encode($id);
				
				foreach ($to_users as $to_user)
				{
					$user_id = $to_user['id'];
					$to_user = $to_user['acct'];
					$msg_user = array($to_user);
					$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=102&taskid='.$taskid.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
					$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_perform&a=detail&f=0&i='.$taskid;//TODO
					try{
						//发送PC提醒
						$data = array(
							'url' => $pc_url,
							'title' => $pc_msg_desc,
							'app_name' => parent::$AppName
						);
						g("messager")->publish_by_user('pc', $user_id,$data);
						parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
					}catch(SCException $e){
					}
				}
			}
			g('db') -> commit();
			echo json_encode(array('errcode'=>0,'errmsg'=>'发布成功'));
		}catch(SCException $e){
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	
	private function get_detail_list(){
		try{
			$data = parent::get_post_data(array('listtype'));
			$taskid = $data['listtype']["taskid"];
			//taskid解密
			$taskid = g('des') -> decode($taskid);
			$open_type = intval($data['listtype']["open_type"]);
			$userid = $_SESSION[SESSION_VISIT_USER_ID];
			$ret_list = parent::information_open($taskid,$userid,$open_type);
			if($ret_list){
				foreach($ret_list as $key =>$task){
					$ret_list[$key]['id'] = g('des')-> encode($task['id']);
				}
			}
			cls_resp::echo_ok(cls_resp::$OK, 'ret_list', $ret_list);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 删除任务（将草稿任务删除）
	 */
	private function delete(){
		$data = get_var_post('data');
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$task_id = explode(',',$data['task_id']);
		g('db') -> begin_trans();
		try{
			foreach($task_id as $id){
				if(!$id)
					continue;
				parent::log('开始删除任务task_id:'.$id);
				$task = g('task_info') -> get_by_com($id,$user_id,'*',self::$States);
				if(!$task)
					throw new SCException('不存在的任务'); 
				$task['info_state'] =parent::$StateOff;
				$task['writer_id'] = $user_id;
				if(!g('task_info') -> delete($task))
					throw new SCException('删除失败');
			}
			g('db') -> commit();
			echo json_encode(array('errcode'=>0,'errmsg'=>'删除成功'));
		}catch(SCException $e){
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取指定taskid的大概任务信息
	 */
	private function get_task(){
		$data = get_var_post('data');
		$taskid = g('des')->decode($data['taskid']);
		if($taskid){
			$task = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID],$taskid,'*');
			cls_resp::echo_ok(0,'data',$task);
		}else
			cls_resp::echo_exp(new SCException('找不到任务'));
		
	}
	
	/**
	 * 我承办的任务列表
	 */
	public function perform_list() {
		try {			
			parent::log('开始查询承办列表');
			$isfinish = intval(get_var_value('is_finish'));
			$perform_condition=array("task_perform","perform_id",array("perform_id="=>$_SESSION[SESSION_VISIT_USER_ID],"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft,"other.info_state="=>parent::$StateOn));
			$task_list = g('task_info')->get_other_task($perform_condition,"task.*,other.state task_state,su.pic_url,tc.classify_name,other.perform_id,other.perform_name",'order by other.state',"asc",1,self::$page_size,false);
			if ($task_list!=FALSE){
				
				$count=0;
				foreach ($task_list as $task_info)
				{
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$assign = g('user') ->get_by_id($task_info['assign_id'],'*',true);
					if(!$assign)
						continue;
					$task_info['pic_url'] = $assign['pic_url'];
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["task_state"]];
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			//var_dump($task_list[0]);
			parent::log('查询承办列表成功');
			g('smarty') -> assign('isfinish',$isfinish);
			g('smarty') -> assign('APP_MENU', $this -> app_menu[1]);
			g('smarty') -> assign('title', '我负责的任务');
			g('smarty') -> show($this -> temp_path.'perform/index.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 分页加载我承办的列表
	 */
	private function load_perform_task(){
			try{
				$isfinish = intval(get_var_value('isfinish'));
				$data = get_var_value('data');
				
				$perform_condition=array("task_perform","perform_id",array("perform_id="=>$_SESSION[SESSION_VISIT_USER_ID],"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft,"other.info_state="=>parent::$StateOn));
				$task_list = g('task_info')->get_other_task($perform_condition,"task.*,other.state task_state,su.pic_url,tc.classify_name,other.perform_id,other.perform_name",'order by other.state',"asc",$data['page'],self::$page_size,false);
				if ($task_list!=FALSE){
					$count=0;
					foreach ($task_list as $task_info)
					{
						$s = $task_info["endtime"]-time();
						$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
						$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
						$assign = g('user') ->get_by_id($task_info['assign_id']);
						if(!$assign)
							continue;
						$task_info['pic_url'] = $assign['pic_url'];
						$task_info["id"] = g('des') -> encode($task_info["id"]);
						$task_info["state_name"] = parent::$TASKSTATE[$task_info["task_state"]];
						$task_info["starttime"] = date('Y-m-d H:i',$task_info["starttime"]);
						$task_info["endtime"] = date('Y-m-d H:i',$task_info["endtime"]);
						$task_list[$count] = $task_info;
						$count++;
					}
				}
				cls_resp::echo_ok(0,'data',array('data'=>$task_list,'count' => $task_list ? count($task_list):0,'page'=> $data['page']));
			}catch(SCException $e){
				cls_resp::echo_exp($e);
			}
	}
	
	
	
	
	/**
	 * 我监督的任务列表
	 */
	public function superintendent_list() {
		try {			
			parent::log('开始查询监督列表');
			$isfinish = intval(get_var_value('is_finish'));
			
			$superintendent_condition=array("task_superintendent","superintendent_id",array("superintendent_id="=>$_SESSION[SESSION_VISIT_USER_ID],"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft,"other.info_state="=>parent::$StateOn));
			
			$task_list = g('task_info')->get_other_task($superintendent_condition,"task.*,su.pic_url,tc.classify_name,other.superintendent_id,other.superintendent_name",'order by task.state',"desc",1,self::$page_size,false);
			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$assign = g('user') ->get_by_id($task_info['assign_id']);
					if(!$assign)
						continue;
					$task_info['pic_url'] = $assign['pic_url'];
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			parent::log('查询监督列表成功');
			g('smarty') -> assign('isfinish',$isfinish);
			g('smarty') -> assign('APP_MENU', $this -> app_menu[1]);
			g('smarty') -> assign('title', '我相关的任务');
			g('smarty') -> show($this -> temp_path.'superintendent/index.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 分页加载我监督的列表
	 */
	private function load_superintendent_task(){
	try {			
			$isfinish = intval(get_var_value('isfinish'));
			$data = get_var_value('data');
			$superintendent_condition=array("task_superintendent","superintendent_id",array("superintendent_id="=>$_SESSION[SESSION_VISIT_USER_ID],"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft,"other.info_state="=>parent::$StateOn));
			
			$task_list = g('task_info')->get_other_task($superintendent_condition,"task.*,su.pic_url,tc.classify_name,other.superintendent_id,other.superintendent_name",'order by task.state',"desc",$data['page'],self::$page_size,false);
			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$assign = g('user') ->get_by_id($task_info['assign_id']);
					if(!$assign)
						continue;
					$task_info['pic_url'] = $assign['pic_url'];
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_info["starttime"] = date('Y-m-d H:i',$task_info["starttime"]);
					$task_info["endtime"] = date('Y-m-d H:i',$task_info["endtime"]);
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			cls_resp::echo_ok(0,'data',array('data'=>$task_list,'count' => $task_list ?count($task_list):0,'page'=>$data['page']));
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 我分配的任务列表
	 */
	public function assign_list() {
		try {			
			parent::log('开始查询分配列表');
			$isfinish = intval(get_var_value('is_finish'));
			$task_list = g('task_info')->get_assign_list($_SESSION[SESSION_VISIT_USER_ID],$isfinish,"task.*,su.pic_url,tc.classify_name",'order by state',"desc",1,self::$page_size,false);
			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			parent::log('查询分配列表成功');
			g('smarty') -> assign('isfinish',$isfinish);
			g('smarty') -> assign('APP_MENU', $this -> app_menu[1]);
			g('smarty') -> assign('title', '我分配的任务');
			g('smarty') -> show($this -> temp_path.'assign/index.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 分页加载我分配的列表
	 * Enter description here ...
	 */
	private function load_assign_task(){
	try {			
			parent::log('开始分页查询分配列表');
			$isfinish = intval(get_var_value('isfinish'));
			$data = get_var_value('data');
			$task_list = g('task_info')->get_assign_list($_SESSION[SESSION_VISIT_USER_ID],$isfinish,"task.*,su.pic_url,tc.classify_name",'order by state',"desc",$data['page'],self::$page_size,false);
			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_info["starttime"] = date('Y-m-d H:i',$task_info["starttime"]);
					$task_info["endtime"] = date('Y-m-d H:i',$task_info["endtime"]);
					$task_list[$count] = $task_info;
					$count++;
				}
			}
			cls_resp::echo_ok(0,'data',array('data'=>$task_list,'count' => $task_list ?count($task_list):0,'page'=>$data['page']));
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	
	
	public function helper_list(){
	try {			
			parent::log('开始查询协办列表');
			$isfinish = intval(get_var_value('is_finish'));
			$is_agree = intval(get_var_value('is_agree'));
			if($is_agree!=parent::$IsAgreeOff&&$is_agree!=parent::$AgreeOn){
				$is_agree=parent::$IsAgreeOff;
			}
			
			$superintendent_condition=array("task_helper","apply_id",array("helper_id="=>$_SESSION[SESSION_VISIT_USER_ID],"is_agree="=>$is_agree,"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft));
			$task_list = g('task_info')->get_other_task($superintendent_condition,"task.*,su.pic_url,tc.classify_name,other.apply_name,other.apply_time,other.apply_content,other.id helpinfoid",'order by task.state',"desc",1,self::$page_size,false);			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
					$task_info["helpinfoid"] = g('des') -> encode($task_info["helpinfoid"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			parent::log('查询协办列表成功');
			g('smarty') -> assign('isfinish',$isfinish);
			g('smarty') -> assign('is_agree',$is_agree);
			g('smarty') -> assign('APP_MENU', $this -> app_menu[1]);
			g('smarty') -> assign('title', '我协助的任务');
			g('smarty') -> show($this -> temp_path.'helper/index.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 分页加载我协办的列表
	 */
	private function load_helper_task(){
	try {			
			$isfinish = intval(get_var_value('is_finish'));
			$is_agree = intval(get_var_value('is_agree'));
			$data = get_var_value('data');
			
			if($is_agree!=parent::$IsAgreeOff&&$is_agree!=parent::$AgreeOn){
				$is_agree=parent::$IsAgreeOff;
			}
			
			$superintendent_condition=array("task_helper","apply_id",array("helper_id="=>$_SESSION[SESSION_VISIT_USER_ID],"is_agree="=>$is_agree,"task.isfinish="=>$isfinish,"task.info_state="=>parent::$StateOn,"task.state<>"=>parent::$StateDraft));
			$task_list = g('task_info')->get_other_task($superintendent_condition,"task.*,su.pic_url,tc.classify_name,other.apply_name,other.apply_time,other.apply_content,other.id helpinfoid",'order by task.state',"desc",$data['page'],self::$page_size,false);			
			if ($task_list!=FALSE){
				$count=0;
				foreach ($task_list as $task_info)
				{
					$s = $task_info["endtime"]-time();
					$task_info["stime_state"] = $s<0?'已经超时':'剩余时间';
					$task_info["stime"] = g('format') -> second_to_date($s<0?abs($s):$s,'d天H小时i分');
					$task_info["id"] = g('des') -> encode($task_info["id"]);
			//		$task_info["helpinfoid"] = g('des') -> encode($task_info["helpinfoid"]);
					$task_info["state_name"] = parent::$TASKSTATE[$task_info["state"]];
					$task_info["starttime"] = date('Y-m-d H:i',$task_info["starttime"]);
					$task_info["endtime"] = date('Y-m-d H:i',$task_info["endtime"]);
					$task_info["apply_time"] = date('Y-m-d H:i',$task_info["apply_time"]);
					$task_list[$count] = $task_info;
					$count++;
				}
				g('smarty') -> assign('task_list', $task_list);
			}
			cls_resp::echo_ok(0,'data',array('data'=>$task_list,'count' => $task_list ?count($task_list):0,'page'=>$data['page']));
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 新增修改页面
	 */
	public function create_page(){
		try {			
			parent::log('开始打开新增修改页面');
			$taskid =  get_var_value("taskid");
			$pid = get_var_value("pid");
			
			
			$userid = $_SESSION[SESSION_VISIT_USER_ID];
			$task_classify = g('task_classify') -> get_task_classify($_SESSION[SESSION_VISIT_COM_ID]);
			$files = array();
			$task_info = array();
			if(empty($taskid)){
//				$task_info["assign_id"]=$userid;
//				$task_info["assign_name"]=$_SESSION[SESSION_VISIT_USER_NAME];
				if(!empty($pid)){
					g('smarty') -> assign('storagr_key',__FUNCTION__.'p'.$_SESSION[SESSION_VISIT_USER_ID].$pid);
					$ptask = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID],g('des')->decode($pid),'*');
					g('smarty') -> assign('pid', $pid);
					g('smarty') -> assign('p_title', $ptask['task_title']);
					g('smarty') -> assign('p_endtime', date('Y-m-d H:i',$ptask['endtime']));
				}else{
					g('smarty') -> assign('storagr_key',__FUNCTION__.$_SESSION[SESSION_VISIT_USER_ID]);
				}
				g('smarty') -> assign('title', '新增任务');
			}
			else{
				//解密taskid
				$taskid = g('des') -> decode($taskid);
				$task_info = parent::get_task_info($userid, $taskid,'*',self::$States);
				$task_info['id'] = g('des')->encode($task_info['id']);
				if($task_info['pid']!=0){
					g('smarty') -> assign('pid', $task_info['pid']);
					$p_task = parent::get_task_info($userid, $task_info['pid'],'*');
					g('smarty') -> assign('p_title', $p_task['task_title']);
					g('smarty') -> assign('p_endtime', date('Y-m-d H:i',$p_task['endtime']));
				}
				
				$files =  g('task_file') -> select_list_taskid($taskid,'task_info',$_SESSION[SESSION_VISIT_USER_ID]);
				g('smarty') -> assign('file', $files);
				g('smarty') -> assign('title', '修改任务');
			}
			
			parent::assign_contact_dept();
			g('smarty') -> assign('APP_MENU', $this -> app_menu[0]);
			g('smarty') -> assign('task_info', $task_info);
			g('smarty') -> assign('create_url', 'index.php?app=task&m=task_create');
			g('smarty') -> assign('upload_url', 'index.php?app=task&m=task&a=upload');
			g('smarty')	-> assign('task_classify', $task_classify);
			g('smarty')	-> assign('task_file', $files);
			g('smarty') -> show($this -> temp_path.'new/new.html');
		} catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	/**
	 * 查看任务
	 */
	public function task_page(){
		try {			
			parent::log('开始查看任务');
			$taskid = get_var_value("taskid");
			$states = intval(get_var_value("states"));
			if($states==0){
				$states=101;
			}
			if(!empty($taskid)){
				//$taskid = g('des') -> encode($taskid);
				$userid = $_SESSION[SESSION_VISIT_USER_ID];
				$username = $_SESSION[SESSION_VISIT_USER_NAME];
				$user_info = g("user")->get_by_id($userid);
				//解密taskid
				log_write($taskid);
				$taskid = g('des') -> decode($taskid);
				$task_info = parent::get_task_info($userid, $taskid,"*",$states);
				
				
				$task_info["id"] = get_var_value("taskid");
				$task_info["pid"] = $task_info['pid']!=0 ? g('des')->encode($task_info['pid']):false;
				
				$task_info["pic_url"] = $user_info["pic_url"];
				$task_info["curr_name"] = $user_info["name"];
				$task_info["states"] = $states;
				//获取任务类型
				$task_classify = g('task_classify') -> get_task_classify_name($task_info['task_type']);
				$task_info['classify_name'] = $task_classify['classify_name'];
				//查看任务子任务列表 汇报列表等列表条数
				$apply_count = g("task_apply")->select_count_taskid($taskid,$userid);
				
				$sontask_count = g("task_info")->get_son_task_count($taskid,$userid);
				
				$talking_count = g("task_talking")->select_count_taskid($taskid,$userid);
				
				$report_count = g("task_report")->select_count_taskid($taskid,$userid);
				
				$file_count = g("task_file")->select_count_taskid($taskid,$userid);
				
				$list_count=array(
					"apply_count"=>$apply_count["count"],
					"sontask_count"=>$sontask_count["count"],
					"talking_count"=>$talking_count["count"],
					"report_count"=>$report_count["count"],
					"file_count"=>$file_count["count"]
				);
				
				
				if($states!=104){
					if($states == 102){
						$perform_info = g('task_perform_base') -> get_perform_info($taskid,$userid,'state');
						$task_button = parent::$TaskButton[$states][$perform_info['state']];
						g('smarty') -> assign('APP_MENU', $this -> app_menu[$states]);
					}else{
						$task_button = parent::$TaskButton[$states][$task_info["state"]];
						g('smarty') -> assign('APP_MENU', $this -> app_menu[$states]);
					}
					
				}
				g('smarty') -> assign('states', $states);
				g('smarty') -> assign('title', '查看任务详情');
				g('smarty') -> assign('task_button', $task_button);
				g('smarty') -> assign('task_info', $task_info);
				g('smarty')	-> assign('list_count', $list_count);
				g('smarty') -> show($this -> temp_path.'detail/task_detail.html');
			}
			else{
				cls_resp::show_err_page(array('输入的参数无效'));
			}
		} catch (SCException $e) {
			log_write($e->getMessage());
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	
	
	/**
	 * 查看任务日志
	 */
	public function task_log(){
		try {
			$taskid = get_var_value("taskid");
			$userid = $_SESSION[SESSION_VISIT_USER_ID];
			$taskid = g('des') -> decode($taskid);
			$ret = g("task_log")->get_task_log($taskid,$userid);
			g('smarty') -> assign('title', '查看任务日志');
			g('smarty') -> assign('log_list', $ret);
			g('smarty') -> show($this -> temp_path.'index/log.html');
		}
		catch (SCException $e) {
			cls_resp::show_err_page(array('找不到页面'));
		}
	}
	/**
	 * 添加提醒
	 */
	public function remind(){
		$task_id = get_var_get('taskid');
		g('smarty') -> assign('title', '提醒');
		g('smarty') -> assign('task_id', $task_id);
		g('smarty') -> show($this -> temp_path.'index/remind.html');
	}
	
	
	/**
	 * 添加延期
	 */
	public function delay(){
		$task_id = get_var_get('taskid');
		g('smarty') -> assign('task_id', $task_id);
		g('smarty') -> assign('title', '延期');
		g('smarty') -> show($this -> temp_path.'index/delay.html');
	}
	
	/**
	 * 添加协办
	 */
	public function helper(){
			parent::show_contact();
			g('smarty') -> assign('title', '协办');
			g('smarty') -> show($this -> temp_path.'index/helper.html');
	}
	
	/**
	 * 添加完成
	 */
	public function finish(){
			g('smarty') -> assign('title', '完成');
			g('smarty') -> show($this -> temp_path.'index/finish.html');
	}
	

	/**
	 * 添加退回
	 */
	public function callback(){
			g('smarty') -> assign('title', '退回');
			g('smarty') -> show($this -> temp_path.'index/callback.html');
	}
	
	
	/**
	 * 上传文件
	 *
	 * @access public
	 * @return void
	 */
	public function upload() {
		$file = array_shift($_FILES);
		log_write(json_encode($file));
		if (empty($file) || !is_array($file)) {
			cls_resp::echo_err(cls_resp::$FileNotFound, '找不到该上传文件，请重新上传！');
		}

		
		$type = get_var_post('type');
		$table = '';
		switch($type){
			case self::$task_info :$table = 'task_info';break;
			case self::$task_report :$table = 'task_report';break;
			default:throw new SCException('上传错误');
		}
		
		parent::log('开始上传文件');

		try {
			$data = g('task_file') -> upload_file($file);
		}catch(SCException $e) {
			parent::log('上传文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('上传文件成功');

		
//		log_write(json_encode($data));
//		$file_data = array(
//			'objectid' => 0,
//			'upload_table'=>$table,
//			'file_name' => $data['file_name'],
//			'file_url' => $data['show_url'],
//			'file_key' => $data['file_key'],
//			'file_type' => $data['extname'],
//			'upload_id' => $_SESSION[SESSION_VISIT_USER_ID],
//			'upload_name' => $_SESSION[SESSION_VISIT_USER_NAME],
//			'upload_time' => time(),
//			'info_state' => parent::$StateOn
//		);
//		$file_id = g('task_file') -> save($file_data);
		$info = array(
			'rec' => $data,
			'file' => $file_id
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	//----------------------------------内部实现----------------------------------
	
	/**
	 * 评论任务
	 */
	private function talking_task(){
		$data = parent::get_post_data(array('taskid', 'talking_content'));
		$taskid = $data["taskid"];
		$taskid = g('des') -> decode($taskid);
		
		if(!trim($data['talking_content'])){
			cls_resp::echo_exp(new SCException('评论内容不能为空！'));
			return;
		}
		
		parent::task_talking($taskid,$data['talking_content']);
	}

	/**
	 * 素材上传(已过时)
	 */
	private function upload_form(){
		log_write('开始上传素材');
		try {
			$a = get_var_post('type');
			$table = get_var_post('table');
			if(empty($table))
				$table = 'task_info';
			$file_type = g('media') -> get_file_type($a);
			if($file_type == 'image'){
				$type = 1;
			}elseif ($file_type == 'voice'){
				$type = 2;
			}elseif ($file_type == 'video'){
				$type = 3;
			}else {
				$type = 5;
			}
			
			$max_size = 0;
			switch ($type) {
				case 1:	//图片 1M
					$max_size = 1024000;
					break;
				case 2:	//音频 2M
					$max_size = 2048000;
					break;
				case 3:	//视频 10M
					$max_size = 10240000;
					break;
				case 5:	//文件 10M
					$max_size = 10240000;
					break;
			}
			$params = g('media') -> cloud_build_params($max_size);
			$file = g('media') -> cloud_upload($params['type'], $params['file']);
			
			$url = $file['path'];
			$hash = $file['hash'];
			$data = array(
				'url' => $url,
				'title' => '',
				'desc' => '',
			);
			log_write('上传素材成功');
			
			$file_data = array(
				'objectid' => 0,
				'upload_table'=>$table,
				'file_name' => $params['name'],
				'file_url' => $url,
				'file_type' => $params['ext'],
				'upload_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'upload_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'upload_time' => time(),
				'info_state' => parent::$StateOn
			);
			$file = g('task_file') -> save($file_data);
			log_write('素材ID：'.$file);
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('file' => $file, 'url' => $url, 'name' => $params['name'], 'ext' => $params['ext'], 'size' => $params['size']/1024));
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
}

// end of file