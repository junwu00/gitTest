<?php
/**
 * 任务详情页面
 * @author chenyihao
 *
 */
class cls_detail extends abs_app_base {
	
	public function __construct() {
		parent::__construct('task');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act) {
			default:
				g('smarty') -> show($this -> temp_path.'detail/index.html');
		}
	}
}
// end of file