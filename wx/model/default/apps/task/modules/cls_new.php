<?php
/**
 * 任务添加
 * @author yangpz
 *
 */
class cls_new extends abs_app_base {
	
	public function __construct() {
		parent::__construct('task');
	}
	
	/**
	 * 新建任务页面
	 */
	public function newtask() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act){
			case 'upload':
				$this -> upload_form(); break;
			default:
				parent::show_contact();
				g('smarty') -> assign('time', date("Y-m-d H:i",time()));
				g('smarty') -> show($this -> temp_path.'new/new.html');
		}
	}
	/**
	 * 任务列表（我的草稿）
	 */
	public  function listtask(){
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act){
			default:
				g('smarty') -> assign('APP_MENU', $this -> app_menu[0]);
				g('smarty') -> assign('time', date("Y-m-d H:i",time()));
				g('smarty') -> show($this -> temp_path.'new/list.html');
		}
	}
	
//----------------------------------------------内部实现------------------------------
	private function show_contact(){
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$com = g("company")-> search($com_id);
			$user = g("user")-> search($com[0]['dept_id'],'');
			$dept_list = g('dept') -> list_dept($com[0]['dept_id']);
			$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
			$arr = array();
			$secret = json_decode($conf['secret_list'],true);
			$ar = array();
		    foreach ($user as $k=>$u){
		    	$email_user = g("email_user")-> get_by_ids($u['id']);
		    	if($email_user){
		    		$user[$k]['qy_email'] = $email_user[0]['email'];
		    		$a = g('pinyin') -> get_first_charter(mb_substr($u['name'], 0, 1, 'UTF-8'));
			    	if(!in_array($a, $arr)){
			    	  $arr[]=$a;
			    	}
		        	$user[$k]['ch_name']= $a;	
		        	$ar[] = $user[$k];
		    	}else{
		    		if(!in_array($u['id'], $secret)&&$u['email']!=""){
			    		$a = g('pinyin') -> get_first_charter(mb_substr($u['name'], 0, 1, 'UTF-8'));
				    	if(!in_array($a, $arr)){
				    	  $arr[]=$a;
				    	}
			        	$user[$k]['ch_name']= $a;	        	
		        		$ar[] = $user[$k];
		    		}	 
		    	}       		
	        }
	      
	        sort($arr);
	        $sid= $_SESSION[SESSION_VISIT_USER_ID];
	      	g('smarty') -> assign('sid',$sid);
	        
	        $users = $this -> get_user_dept($ar);
	      	$this -> get_list_tree($tree,$dept_list,$ar);
	       	
	      	$result = $this -> user_general();
	      	if($result){
	      		g('smarty') -> assign('gen',$result);
	      	}
	      
		    g('smarty') -> assign('ch',$arr);
			g('smarty') -> assign('tree',$tree);
			g('smarty') -> assign('dept',$dept_list);
			g('smarty') -> assign('user',$users);
			g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
	}
	
	/**
	 * 获取用户的部门信息
	 * Enter description here ...
	 * @param unknown_type $user 本公司的所有员工
	 */
	private function get_user_dept($user){
		foreach ($user as $key=>$u){
			$dept_id = (Array)json_decode($u['dept_list']);
			$arr = array();
			
			foreach ($dept_id as $id){
				$dept = g('dept')-> get_dept_by_id($id);
				if($dept){
					$arr[] = $dept['name'];
				}
			}
			$user[$key]['dept_name'] = $arr;
		}
		return $user;
	}
	
	/**
	 *返回员工常用联系人
	 */
	private function user_general(){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$com = g("company")-> search($com_id);
		$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
		$general_list = (Array)json_decode($conf['general_list']);
			
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		$result = $this -> get_user_dept(g('user')-> get_by_ids($sid));
		$general_user = g('user_general') -> get_by_ids($sid);
		$dlist = (Array)json_decode($general_user[0]['del_list']);
		
		$user = array();	
		foreach ($general_list as $d){
			if($d != $sid && !in_array($d, $dlist)){
				$u = g('user')-> get_by_ids($d);
				if($u){
					$user[] = $u[0];
				}
			}
		}
		if($general_user){
			$list = (Array)json_decode($general_user[0]['general_list']);
			foreach ($list as $d){
					$u = g('user')-> get_by_ids($d);
					if($u && !in_array($u[0], $user)){
						$user[] = $u[0];
					}
			}
		}
		$arr = array();
		foreach ($user as $u){
			if($u['email']!=""){
				$arr[]=$u;
			}
		}
		$users=array();
		if($arr){
			$users = $this -> get_user_dept($arr);
		}
			
		return $users;
	}
	
/**
	 * 加载公司部门与全部员工
	 * Enter description here ...
	 * @param unknown_type $html
	 * @param unknown_type $dept_list 本公司所有部门
	 * @param unknown_type $user	本公司所有员工
	 */
	private function get_list_tree(&$html, $dept_list, $user, $root=FALSE) {
		$display = '';
		if (!$root) {
			$display = 'style="display:none;"';
		}
		
		if (!is_array($dept_list)){$html .='</ul>'; return;}
		foreach ($dept_list as $dept) {
			if (isset($dept['childs'])) {
				$html .= '<li ><div class="dlabel" >'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
				foreach ($user as $u){
					$arr = (Array)json_decode($u['dept_list']);
					 if(in_array($dept['id'], $arr)){
					 	if($u['pic_url']!=""){
					 		$img=$u['pic_url'];
					 	}else{
					 		$img=SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}

					 	$html .= '<li  style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" m="'.$img.'"  n="'.$u['name'].'" d="'.$u['id'].'"></i></li>';
					 }
				}
			
				$this -> get_list_tree($html, $dept['childs'],$user);
			} else {
				$html .= '<li ><div class="dlabel">'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
			    foreach ($user as $u){
			    	$arr = (Array)json_decode($u['dept_list']);
					 if(in_array($dept['id'], $arr)){
						 if($u['pic_url']!=""){
					 		$img = $u['pic_url'];
					 	}else{
					 		$img = SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}
					 	$html .= '<li style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" m="'.$img.'"  n="'.$u['name'].'" d="'.$u['id'].'"></i></li>';
					 }
				}
				$html .= '</ul>';
			}
			$html .= '</li>';
		}
	}
	
	
	/**
	 * 上传文件
	 */
	private function upload_form(){
		log_write('开始上传文件');
		try {
			$a = get_var_post('type');
			$file_type = g('media') -> get_file_type($a);
			if($file_type == 'image'){
				$type = 1;
			}elseif ($file_type == 'voice'){
				$type = 2;
			}elseif ($file_type == 'video'){
				$type = 3;
			}else {
				$type = 5;
			}
			
			$max_size = 0;
			switch ($type) {
				case 1:	//图片 1M
					$max_size = 1024000;
					break;
				case 2:	//音频 2M
					$max_size = 2048000;
					break;
				case 3:	//视频 10M
					$max_size = 10240000;
					break;
				case 5:	//文件 10M
					$max_size = 10240000;
					break;
			}
			$params = g('media') -> cloud_build_params($max_size);
			$file = g('media') -> cloud_upload($params['type'], $params['file']);
			
			$url = $file['path'];
			$hash = $file['hash'];
			$data = array(
				'url' => $url,
				'title' => '',
				'desc' => '',
			);
			log_write('上传素材成功');
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('art' => $ret,'hash' => $hash, 'url' => $url, 'name' => $params['name'], 'ext' => $params['ext'], 'size' => $params['size']/1024));
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
}
// end of file