<?php
/**
 * 任务协作--任务列表
 * @author lijuns
 * @date 2014-12-27
 *
 */
class cls_task_perform extends abs_task {
	
	public function __construct() {
		parent::__construct('task');
	}
	
	private static $States = 102;
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'delay_task':
				$this -> delay_task();
				break;
			case 'callback_task':
				$this -> callback_task();
				break;
			case 'finish_task':
				$this -> finish_task();
				break;
			case 'report_task':
				$this -> report_task();
				break;
			case 'helper_task':
				$this -> helper_task();
				break;
			default:
				 cls_resp::show_err_page(array('找不到页面'));
				
		}
	}
	
	/**
	 * 申请延期页面
	 */
	public function delay_page(){
		$taskid = g('des') -> decode(get_var_value("taskid"));
		$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID],$taskid,'*',self::$States);
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		g('smarty') -> assign('title', '申请延期');
		g('smarty')	-> assign('taskid', get_var_value("taskid"));
		g('smarty')	-> assign('endtime', date('Y-m-d H:i',$task_info["endtime"]));
		g('smarty') -> show($this -> temp_path.'index/delay.html');
	}
	
	/**
	 * 申请退回页面
	 */
	public function callback_page(){
		$taskid = get_var_value("taskid");
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		g('smarty') -> assign('title', '申请退回');
		g('smarty')	-> assign('taskid', get_var_value("taskid"));
		g('smarty') -> show($this -> temp_path.'index/callback.html');
	}
	
	/**
	 * 申请完成页面
	 */
	public function finish_page(){
		$taskid = get_var_value("taskid");
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		g('smarty') -> assign('storagr_key',__FUNCTION__.$_SESSION[SESSION_VISIT_USER_ID].$taskid);
		g('smarty') -> assign('title', '申请完成');
		g('smarty')	-> assign('taskid', $taskid);
		g('smarty')	-> assign('nowtime', date('Y-m-d H:i',time()));
		g('smarty') -> show($this -> temp_path.'index/finish.html');
	}
	
	/**
	 * 汇报页面
	 */
	public function report_page(){
		$taskid = get_var_value("taskid");
		$states = get_var_value("states");
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		g('smarty') -> assign('storagr_key',__FUNCTION__.$_SESSION[SESSION_VISIT_USER_ID].$taskid);
		g('smarty') -> assign('title', '任务汇报');
		g('smarty') -> assign('upload_url', 'index.php?app=task&m=task&a=upload');
		g('smarty')	-> assign('taskid', $taskid);
		g('smarty')	-> assign('states', $states);
		g('smarty') -> show($this -> temp_path.'perform/report.html');
	}
	
	
	/**
	 * 协办页面
	 */
	public function helper_page(){
		$taskid = g('des') -> decode(get_var_value("taskid"));
		if(empty($taskid)){
			cls_resp::show_err_page(array('找不到页面'));
		}
		parent::show_contact();
		$userid = $_SESSION[SESSION_VISIT_USER_ID];
		$helper_list = g("task_helper_base")->select_helper_taskid($taskid,$userid);
		g('smarty') -> assign('title', '任务协办');
		g('smarty')	-> assign('taskid', get_var_value("taskid"));
		g('smarty')	-> assign('helper_list', $helper_list);
		g('smarty') -> show($this -> temp_path.'perform/helper.html');
	}
	
	
	
	//----------------------------------内部实现----------------------------------
	
	/**
	 * 申请完成
	 * Enter description here ...
	 * @throws SCException
	 */
	private function finish_task(){
		try {
			parent::log('开始完成任务');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('taskid','apply_content','finish_time'));
			
			if(!trim($data['apply_content']))
				throw new SCException('完成申请信息不能为空！');
			else if(!trim($data['finish_time']))
				throw new SCException('完成时间不能为空！');
			$des_id = $data['taskid'];
			$data['taskid'] = g('des')->decode($data['taskid']);
			$taskid = $data['taskid'];
			//查询任务基本信息
			$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $data['taskid'],'*',self::$States);
			
//			if($task_info["state"]!=parent::$StateRunning){
//				throw new SCException('任务状态已更改，请重新刷新任务');
//			}
			
			//申请表
			$apply_info=array(
				"taskid"=> $data['taskid'],
				"applyer_id"=>$_SESSION[SESSION_VISIT_USER_ID],
				"applyer_name"=>$_SESSION[SESSION_VISIT_USER_NAME],
				"apply_state"=>parent::$StateApplyFinish,
				"apply_content"=>$data["apply_content"],
				"answer_id"=>$task_info["assign_id"],
				"answer_name"=>isset($task_info["answer_name"])?$task_info["answer_name"]:"",
				"finish_time"=>strtotime($data['finish_time'])
			);
			g("task_apply")->save($apply_info);
			
			//更新承办人表状态
			$perform_data=array(
				"state"=>parent::$StateApplyFinish,
				"finish_time"=>strtotime($data['finish_time'])
			);
			g("task_perform_base")->update_apply_info($taskid,$_SESSION[SESSION_VISIT_USER_ID],$perform_data);
			
			//更新任务信息状态
			$running_perform = g('task_perform_base') -> get_running_perform($taskid);
			if(!empty($running_perform)){
				$task_data=array(
					"state"=> $running_perform['state'],
					"finish_time"=>strtotime($data['finish_time'])
				);
				g("task_info")->update_apply_info($taskid,$_SESSION[SESSION_VISIT_USER_ID],$perform_data);
			}
			
			//任务日志
			$log_info=array(
				'taskid' => $taskid,
				'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'log_content' => parent::$TASKSTATE[parent::$StateApplyFinish],
			);
			g("task_log")->save($log_info);
			
			//发送申请提醒
			$to_users = g('user') -> get_by_ids($task_info["assign_id"]);
			if (!$to_users) {
				throw new SCException('找不到发送人信息');
			}
			foreach ($to_users as $to_user)
			{
				$user_id = $to_user['id'];
				$to_user = $to_user['acct'];
				$msg_user = array($to_user);
				$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=101&taskid='.$des_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
				$msg_title = '任务申请完成提醒';
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'对您分配《'.$task_info["task_title"].'》任务申请完成，请及时处理！';
				$pc_msg_desc = '任务协作：您分配的任务有新的申请需要处理！';
				$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_assign&a=detail&f=0&i='.$des_id;//TODO
				try{
					//发送PC提醒
					$msg = array(
						'url' => $pc_url,
						'title' => $pc_msg_desc,
						'app_name' => parent::$AppName
					);
					g("messager")->publish_by_user('pc', $user_id,$msg);
					parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
				}catch(SCException $e){
					log_write($e -> getMessage());
				}
			}
			
			g('db') -> commit();
			parent::log('完成任务信息成功');
			cls_resp::echo_ok();
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	private function report_task(){
		try {
			parent::log('开始汇报任务');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('taskid','report_content','file_info_str'));
			
			if(!trim($data['report_content']))
				throw new SCException('汇报信息不能为空！');
			
			$taskid = g('des') -> decode($data["taskid"]);
			//查询任务基本信息//隐藏了states参数
			$task_info = parent::get_task_info($_SESSION[SESSION_VISIT_USER_ID], $taskid,'*');
			
			//申请表
			$report_info=array(
				'taskid' => $taskid,
				'reporter_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'reporter_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'report_content' => $data['report_content']
			);
			$ret = g("task_report")->save($report_info);
			
			
			//保存上传附件
			if(!empty($data["file_info_str"]) && is_array($data["file_info_str"])){
				$files = $data["file_info_str"];
				g("task_file") -> save($files,$ret,'task_report',$_SESSION[SESSION_VISIT_USER_ID],$_SESSION[SESSION_VISIT_USER_NAME]);
			}
//			g("task_file")->update_file_taskid($ret,$data["file_info_str"]);
			
			//任务日志
			$log_info=array(
				'taskid' => $taskid,
				'loger_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'loger_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'log_content' => "任务汇报",
			);
			g("task_log")->save($log_info);
			
			//发送申请提醒
			$to_users = g('user') -> get_by_ids($task_info["assign_id"]);
			if (!$to_users) {
				throw new SCException('找不到发送人信息');
			}
			foreach ($to_users as $to_user)
			{
				$user_id = $to_user['id'];
				$to_user = $to_user['acct'];
				//$to_user = '002'; //TODO
				$msg_user = array($to_user);
				$msg_url = SYSTEM_HTTP_DOMAIN.'?app=task&m=task&a=task_page&states=101&taskid='.$data["taskid"].'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
				$msg_title = '任务汇报提醒';
				$msg_desc = '您好！'.$_SESSION[SESSION_VISIT_USER_NAME].'对您分配《'.$task_info["task_title"].'》任务进行了汇报，请及时查看！';
				
				$pc_msg_desc = '任务协作：您发布的任务有新的汇报！';
				$pc_url = SYSTEM_HTTP_PC_DOMAIN.'index.php?app='.parent::$AppName.'&m=task_assign&a=detail&f=0&i='.$data["taskid"];//TODO
				try{
					//发送PC提醒
					$data = array(
						'url' => $pc_url,
						'title' => $pc_msg_desc,
						'app_name' => parent::$AppName
					);
					g("messager")->publish_by_user('pc', $user_id,$data);
					parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
				}catch(SCException $e){
					
				}
			}
			
			
			g('db') -> commit();
			parent::log('汇报任务信息成功');
			cls_resp::echo_ok();
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	} 
	
}

// end of file