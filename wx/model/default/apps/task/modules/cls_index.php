<?php
/**
 * 工作日程默认页面
 * @author yangpz
 *
 */
class cls_index extends abs_app_base {
	
	public function __construct() {
		parent::__construct('task');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$visit = $_SESSION[SESSION_VISIT_USER_WXACCT];
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("ajax_act");
		switch($act) {
			case 'finish':
				$this ->finish();break;
			case 'json_date':
				$data = get_var_post('data');
				echo json_encode($this -> getAllSchedule($data['date'])); break;
			default:
				g('smarty') -> assign('APP_MENU', $this -> app_menu[1]);		
				g('smarty') -> show($this -> temp_path.'index/index.html');
		}
	}
	
}
// end of file