var page = 1;
$(document).ready(function(){
	
	init_scroll_load('.container', '.wait', function(){
		var data={'page':page+1};
		frame_obj.do_ajax_post($(this),'load_create_task',
				JSON.stringify(data),
				function(data){
					if(data['errcode']!=0){ 	//加载失败，不再加载
						scroll_load_complete('.wait',0);
					}else{
						scroll_load_complete('.wait',data['data']['count']);
						if(data['data']['count']==0)
							return;
						page=data['data']['page'];
						var html="";
						for(var i in data['data']['data']){
							var task = data['data']['data'][i];
							var degree ='';
							if(task['degree']=='0'){
								degree = '低';
							}else if(task['degree']=='1'){
								degree = '中';
							}else if(task['degree']=='2'){
								degree = '高';
							}
							var perform_name ='待定';
							if(task['perform']){
								perform_name='';
								for(var j in task['perform']){
									var perform = task['perform'][j];
									perform_name +=perform['perform_name'] + '&nbsp;'; 
								}
							}
							
							var superintendent_name ='无';
							if(task['superintendent']){
								for(var j in task['superintendent']){
									var superintendent = task['superintendent'][j];
									superintendent_name +=superintendent['superintendent_name'] + '&nbsp;'; 
								}
							}
							
							html += '<div class="row" >'+
									'<div class="row_item" i="'+task['id']+'">'+
									'<div class="level level-'+task['degree']+'">'+degree+'</div>'+
									'<div class="first_item">'+
									'<div style="display: inline-block; width:90%;vertical-align: top;">'+task['task_title']+'</div></div>'+
									'<div class="item">'+
									'<div class="date">时间：</div><div>'+task['starttime']+'~'+task['endtime']+'</div>'+
									'</div>'+
									'<div class="item">'+
									'<div class="date">承办人：</div><div>'+
									perform_name+
									'</div></div></div>'+
									'<div class="buttonrow">'+
									'<div class="buttonleft"><span></span>发  布</div>'+
										'<div class="middle"></div>'+
										'<div class="buttonright"><span></span>删  除</div>'+
									'</div>'+
									'</div>';
						}
						
						$('.wait').before(html);
					}
					$('.row_item').unbind('click').bind('click',function(){
						location.href=$('#app-url').val()+"&m=task&a=create_page&taskid="+$(this).attr('i');
					});
				}
		);
	});
	
	$('.row_item').click(function(){
		location.href=$('#app-url').val()+"&m=task&a=create_page&taskid="+$(this).attr('i');
	});
	
	$('.buttonleft').click(function(){
		var task_id=$(this).attr("i");
		frame_obj.comfirm('确认要发布选择的任务?',function(){
			frame_obj.do_ajax_post(
					$(this),
					'publish',{
						'task_id':task_id
					},function(data,btn,prams){
						if(data['errcode']==0){
							location.reload();
						}else{
							frame_obj.alert(data['errmsg']);
						}
					},
					'',
					'json',
					'publish',
					'发布中……'
			);
		});
	});
	
	$('.buttonright').click(function(){
		var task_id=$(this).attr("i");
		frame_obj.comfirm('确认要删除选择的任务?',function(){
			$('#ajax-url').val('index.php?app=task&m=task_create');
			var data ={
					'ids':task_id
			};
			frame_obj.do_ajax_post(
					$(this),
					'delete_task',
					JSON.stringify(data),
					function(data,btn,prams){
						if(data['errcode']==0){
							location.reload();
						}else{
							frame_obj.alert(data['errmsg']);
						}
					},
					'',
					'json',
					'delete_task',
					'删除中……'
			);
		});
	});
	
	
//	$('input[type="checkbox"]').iCheck({
//	    checkboxClass: 'icheckbox_square-green check',
//	    radioClass: 'iradio_square-green',
//	    increaseArea: '20%' // optional
//	 });
	
//	$('.nav-pills >li:first').click(function (){
//		if($('.checked:checked').length==0){
//			frame_obj.alert('请选择要发布的任务！');
//		}else{
//			var task_id="";
//			$('.checked:checked').each(function (){
//				task_id +=","+$(this).val();
//			});
//			frame_obj.do_ajax_post(
//					$(this),
//					'publish',{
//						'task_id':task_id
//					},function(data,btn,prams){
//						frame_obj.alert(data['errmsg'],'确定',function(){
//							if(data['errcode']==0)
//								location.reload();
//						});
//					},
//					'',
//					'json',
//					'publish'
//			);
//		}
//	});
//	$('.nav-pills >li:nth-child(2)').click(function (){
//		if($('.checked:checked').length==0){
//			frame_obj.alert('请选择要删除的任务！');
//		}else{
//			frame_obj.comfirm('确认要删除选择的任务?',function(){
//				$('#ajax-url').val('index.php?app=task&m=task_create');
//				var task_id="";
//				$('.checked:checked').each(function (){
//					task_id +=","+$(this).val();
//				});
//				var data ={
//						'ids':task_id
//				};
//				frame_obj.do_ajax_post(
//						$(this),
//						'delete_task',
//						JSON.stringify(data),
//						function(data,btn,prams){
//							frame_obj.alert(data['errmsg'],'确定',function(){
//								if(data['errcode']==0)
//									location.reload();
//							});
//						},
//						'',
//						'json',
//						'delete_task'
//				);
//			});
//		}
//	});
});
