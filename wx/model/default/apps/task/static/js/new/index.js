var a =['高','中','低'];
var state='';
$(document).ready(function(){
	init_upload();
	$storage.init({
		key:$('#storage_key').val(),
		ckey:['.title-input','.content','.task_type'],
		time:2000,
	});

	$('.select_task_type').click(function(){
		$('.task_type').click();
	});
	/*
	$('.leader-add').unbind('click').bind('click',function(){
		$('#add-type').val('leader');
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		init_contact();
	});
	*/
	
	
	/*
	 	$('.jiandu-add').unbind('click').bind('click',function(){
		$('#add-type').val('jiandu');
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		init_contact();
	});
	*/
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});
	init_datetime_scroll('#time_start');
	init_datetime_scroll('#time_end');
	
	$('.youxian_state .degree').click(function (){
		var i = $(this).attr('i');
		$('.youxian_state').attr('i',i);
		$('.youxian').val(i);
		$('.youxian_state .degree').each(function(){
			$(this).addClass('icon-circle-blank');
			$(this).css("color","#777");
		});
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});

	state = $('#state').val()?$('#state').val():0;
	//提交
	$('.submit_task').click(function(){
		if(state==0)
			$('#state').val(1);
		$('.submit-button').click();
	});
	
	//确认
	$('.submit-button').click(function(){
		if (mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		
		$(this).attr("disabled","disabled");
		
		var wait_msg = $('#state').val() == 1 ? '发布中……' : '保存中……';
		
		if($('#state').val() == 1 && $('.submit-button').html() == "保存"){
			wait_msg = '保存中……';
		}
		
		if($('.title-input').val()=='' || $.trim($('.title-input').val())==''){
			frame_obj.alert('请输入任务标题！');
			reset($(this));
			return;
		}
		if($('.task_type').val()==0){
			frame_obj.alert('请选择任务类型！');
			reset($(this));;
			return;
		}
		if(!$('#time_start').val()&&$('#state').val()!=0){
			frame_obj.alert('请选择开始时间！');
			reset($(this));;
			return;
		}
		if(!$('#time_end').val()&&$('#state').val()!=0){
			frame_obj.alert('请选择结束时间！');
			reset($(this));;
			return;
		}
		if($('#leader>.leader').size()==0&&$('#state').val()!=0){
			frame_obj.alert('请选择承办人！');
			reset($(this));;
			return;
		}
		
		var leader="";
		var leader_id="";
		var jiandu="";
		var jiandu_id="";
		$('#leader > .leader').each(function(){
			if($(this).attr('i')==0)
				return;
			leader_id +=$(this).attr('i')+',';
			leader += $(this).children('.group_item_name').html()+',';
		});
		$('#jiandu > .jiandu').each(function(){
			if($(this).attr('i')==0)
				return;
			jiandu_id +=$(this).attr('i')+',';
			jiandu += $(this).children('.group_item_name').html()+',';
		});
		var file = [];
		$('.file_div > .file_info').each(function(){
			file.push({
					'file_name' : $(this).attr('fname'),
					'file_type' : $(this).attr('fext'),
					'file_key'  : $(this).attr('filekey'),
					'file_url'  : $(this).attr('furl'),
			});
		});

			var data = {
					'id':$('#id').val(),
					'task_title':$('.title-input').val(), 
					'task_describe':$('.content').val(),
					'pid':$('#pid').val(),
					'state':$('#state').val(),
					'task_type':$('.task_type').val(),
					'starttime':$('#time_start').val(),
					'endtime':$('#time_end').val(),
					'isfinish':$('#isfinish').val(),
					"degree":$('.youxian').val(),
					'file_list_str':file,
					'perform_id':leader_id,
					'perform_name':leader,
					'superintendent_id':jiandu_id,
					'superintendent_name':jiandu
				};
			$('#ajax-url').val($('#create_url').val());
			frame_obj.do_ajax_post($(this),'save_task',
					JSON.stringify(data),
					function(data){
						if(data['errcode']==0){
							$storage.clear();
						    if($('#state').val()==0){
						    	location.href="index.php?app=task&m=task&a=create_list";
						    }else{
						    	location.href="index.php?app=task&m=task&a=assign_list";
						    }
						}else{
							frame_obj.alert(data['errmsg']);
							reset($(this));
						}
					},
					'',
					'json',
					'save_task',
					wait_msg
			);
	});
	init_data();
	
	c_search_init_toggle();
	
});


//显示/隐藏选人控件
function c_search_init_toggle() {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
		var curr_selected = {};
		var type = '';
		if($(this).hasClass('leader-add')){
			$('.group_item.leader').each(function() {
				var selected_id = $(this).attr('i');
				var selected_name = $(this).children('.group_item_name').html();
				curr_selected[selected_id] = {id: selected_id, name: selected_name};
			});
			type="leader";
		}else{
			$('.group_item.jiandu').each(function() {
				var selected_id = $(this).attr('i');
				var selected_name = $(this).children('.group_item_name').html();
				curr_selected[selected_id] = {id: selected_id, name: selected_name};
			});
			type="jiandu";
		}
		//初始化选人控件
		c_search_init({
			type:2,
			selected: curr_selected,
			before	: function() {
				$('#add-type').val(type);
			},
			ok	: function(selected_user) {
				$('.container').css('display','block');
				$(".contact-content").css('display','none');
				var html="";
				var type = $('#add-type').val();
//				$('.choose').each(function(){
//					if($(this).attr('n')){
//						if($('.'+type+'_choose_list').children('li[i="'+$(this).attr('i')+'"]').size()==0)
//							html +='<li class="group_item '+type+'" i="'+$(this).attr('i')+'"><span class="group_item_name">'+$(this).attr('n')+'</span><button class="close" type="button" onclick="remove_'+type+'('+$(this).attr('i')+')">×</button></li>';
//					}
//				});
				for (var i in selected_user) {
					if (!selected_user[i])		continue;
					
					var item = selected_user[i];
					html +='<li class="group_item '+type+'" i="'+item.id+'"><span class="group_item_name">'+item.name+'</span><span style="font-size:14px;color:rgb(142,142,142)" onclick="del_group_item(this)">&nbsp;x&nbsp;</span></li>';
				}
				$('.group_item.'+type).remove();
				if($('#add-type').val()=='leader'){
					$('.leader_choose_list > .token-input-input-token').before(html);
				}else if($('#add-type').val()=='jiandu'){
					$('.jiandu_choose_list > .token-input-input-token').before(html);
				}
			}
		});
		
	});
}



function reset(obj){
	if(state==0){
		$('#state').val(0);
	}
	obj.removeAttr("disabled");
}

function init_data(){
	$('#time_start').val($('#time_start').attr('i')==0?"":$('#time_start').attr('i'));
	$('#time_end').val($('#time_end').attr('i')==0?"":$('#time_end').attr('i'));
	var degree = $('.youxian_state').attr('i');
	if(degree!="")
		$('.youxian_state .degree'+degree).click();
	else
		$('.youxian_state .degree'+0).click();
	
}
function delete_file(id){
	$('.uploadfile[i="'+id+'"]').remove();
}

function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}

var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			/*
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			*/
			
			currUploadDiv = $('<div class="file_div" data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info file_name">' + fileName + '</div>'+
							'<span id="mupload-notice">开始扫描文件</span>'+
						'</div>');
			$("#file_button_div").after(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('#mupload-notice').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('#mupload-notice').html('正在上传');
		},
		process		: function(percent) {
			$('#mupload-notice').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	
    		var is_exist = false;
        	var file_key = data.hash;
        	$('#files_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('filekey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('fext', data.file_ext);
    		
    		var optBtn = '<span class="delete_btn" onclick="removeFile($(this).parents(\'.file_div\'));">删除</span>';
    		if (isImg == 1) {
    			optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		$('#mupload-notice').html(optBtn);
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});

	//插入预览图
	$('.preview_btn').each(function() {
		var target = $(this).parents('.file_div').find('.file_info');
		var html = '<div class="swiper-slide" data-file="' + $(target).attr('fname') + '">' +
			'<img src="' + mediaUrl + ($(target).attr('furl'))+ '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
		'</div>';
		mupload.mupload('appendPreview', $(html));		//插入到前面
	});
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
}

//滚动到目标预览图
function slideTotarget(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	mupload.mupload('slideToPreview', targetIdx, 1000, fileName);
}

function remove_leader(id){
	$('.leader_choose_list').children('li[i="'+id+'"]').remove();
	$('#add-type').val('leader');
}
function remove_jiandu(id){
	$('.jiandu_choose_list').children('li[i="'+id+'"]').remove();
	$('#add-type').val('jiandu');
}

function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
}

function del_group_item(that){
	var id = $(that).parent().attr('i');
	c_search_delete_evt($('<span>',{'data-id':id}));
	$(that).parent().remove();
}