
$(document).ready(function(){
	init_icheck();
	$(window).resize(function(){   
		$('.value input').each(function(){
			var id = document.activeElement.id;
			var input = $("#"+id);
			$("html,body").stop().animate({scrollTop:input.offset().top},50);
		});
	});
	
	$('.fail').click(function(){
		history.go(-1);
	});
	init_datetime_scroll('#schedule_time');
	
	$('#schedule_time').val($('#schedule_time').attr('i'));
	
	$('.submit-button').click(function(){
		if($('.content').val()==''){
			frame_obj.alert('请输入日程内容！');
			return;
		}
		frame_obj.do_ajax_post($(this),'save',
				{
					'schedule_id':$('#schedule_id').val(),
					'content':$('.content').val(),
					'date':$('#schedule_time').val(),
					'remind':$('#schedule_remind').attr('i'),
					'state':$('input[name="state"]:checked').val(),
					'place':$('#schedule_place').val()
				},
				isfinish
		);
	});
	$('#schedule_remind').click(function(e){
		var id = $(this).attr('i');
		if(id!==0){
			$('input[name="remind"]').each(function(){
				if($(this).val()==id){
					$(this).attr('checked','checked');
					$(this).parent().addClass('checked');
				}
			});
		}
	});
	$('.choose-remind').click(function(){
//		alert($('input[name="remind"]:checked').val());
		$('#schedule_remind').val($('input[name="remind"]:checked').attr('m'));
		$('#schedule_remind').attr('i',$('input[name="remind"]:checked').val());
	});
});
function isfinish(data){
	if(data['errcode']==0){
		frame_obj.alert(data['errmsg']);
		location.href = 'index.php?app=schedule&&m=index';
	}
	else
		frame_obj.alert(data['errmsg']);
}
function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};
function init_icheck(){
	$('.red').iCheck({
	    radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	 });
	$('.orange').iCheck({
	    radioClass: 'iradio_square-orange',
	    increaseArea: '20%' // optional
	 });
	$('.blue').iCheck({
	    radioClass: 'iradio_square-blue',
	    increaseArea: '20%' // optional
	 });
	$('.grey').iCheck({
	    radioClass: 'iradio_square-grey',
	    increaseArea: '20%' // optional
	 });
}