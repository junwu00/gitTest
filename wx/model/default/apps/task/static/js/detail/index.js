
$(document).ready(function(){
	var button_style = $("#button_style").val().split(",");
	var i=0;
	$("#bottom-menu a").each(
		function(){
			if(button_style[i]=="true"){
				$(this).css("color","#ccc");
			}
			else{
				if(button_style[i]=='remind_page'){
					$(this).click(function(e){
						e.stopPropagation();
						remind_page();
					});
				}else if('stop_task'==button_style[i]){
					$(this).click(function(e){
						e.stopPropagation();
						stop_task();
					});
				}else if('delay'==button_style[i]){
					$(this).click(function(e){
						e.stopPropagation();
						delay();
					});
				}else if('callback'==button_style[i]){
					$(this).click(function(e){
						e.stopPropagation();
						callback();
					});
				}else if('finish'==button_style[i]){
					$(this).click(function(e){
						e.stopPropagation();
						finish();
					});
				}else if('approval_page'==button_style[i]){
					$(this).click(function(e){
						e.stopPropagation();
						approval();
					});
				}
			}
			i++;
		}
	);
	
	$('.task-side').click(function(){
			$(this).parent().children('.side-content').css('width','100%');
			$(this).parent().children('.side-content').slideToggle();
	});
	$('.add_attach').click(function(e){
		e.stopPropagation();
		upload_page();
	});
	$('.add_children_task').click(function(e){//添加子任务
		e.stopPropagation();
		var taskid = $("#taskid").val();
		location.href = $("#list_url").val()+"&m=task&a=create_page&pid="+taskid;
	});
	$('.add_report').click(function(e){//汇报
		e.stopPropagation();
		report_page();
	});
	$('.add_comment').click(function(e){//评论
		e.stopPropagation();
		talking_win();
	});
	$('.attach').click(function(){
		$(this).attr('i'); //附件标示符
		alert('开始下载附件：'+$(this).html());
	});
	$('.add_helper').click(function(e){//发起协办
		e.stopPropagation();
		helper_page();
	});
	$('.task_log').click(function(e){//任务日志
		e.stopPropagation();
		log_page();
	});
	
	//展开不同的页面列表
	$('#file_tab').click(function(){
		get_open_list(0,set_file_list); 
	});
	$('#son_tab').click(function(){
		get_open_list(1,set_son_list); 
	});
	$('#report_tab').click(function(){
		get_open_list(2,set_report_list); 
	});
	$('#talking_tab').click(function(){
		get_open_list(3,set_talking_list); 
	});
	$('#apply_tab').click(function(){
		get_open_list(4,set_apply_list); 
	});
	$('.comment').click(function(e){
		e.stopPropagation();
	});
	$('#apply_tab').click();
	$('#file_tab').click();
});


function hide(id){
	$('#'+id).modal('hide');
}
//发起协办页面
function helper_page(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	window.location.href = tempurl+"&m=task_perform&a=helper_page&taskid="+taskid;
}

//汇报页面
function report_page(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	var states = $("#states").val();
	window.location.href = tempurl+"&m=task_perform&a=report_page&states="+states+"&taskid="+taskid;
}

//上传附件页面
function upload_page(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	var states = $("#states").val();
	window.location.href = tempurl+"&m=task_assign&a=upload_page&states="+states+"&taskid="+taskid;
}

$('#talking_content').bind('input propertychange', function() {
    if($('#talking_content').val().length >0)
    	$('#win-ok').removeAttr('disabled');
    else
    	$('#win-ok').attr('disabled','disabled');
});

function talking_win(){
	$('#win-dlg').modal('show');
	$("#talking_content").focus();
	$('#win-ok').unbind('click').bind('click', function() {
		$('#win-dlg').modal('hide');
		talking_ajax();
	});
	//frame_obj.comfirm('', talking_ajax);
}

function talking_ajax(){
	var tempurl = $("#list_url").val();
	tempurl = tempurl+"&m=task";
	$("#ajax-url").val(tempurl);
	var taskid = $("#taskid").val();
	var talking_content = $("#talking_content").val();
	if(talking_content==""){
		frame_obj.alert('评论内容不能为空','',talking_win);
		return;
	}
	var data = new Object();
	data.taskid = taskid;
	data.talking_content = talking_content;
	frame_obj.do_ajax_post($(this), 'talking_task', JSON.stringify(data),refalshwin,"评论成功",undefined,undefined,'提交中……');
}

function log_page(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task&a=task_log&taskid="+taskid;
}

function remind_page(){
	var states = $("#states").val();
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task_assign&a=remind_page&taskid="+taskid+"&states="+states;
}

function approval(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task_assign&a=approval_page&taskid="+taskid;
}

function stop_task(){
	var tempurl = $("#list_url").val();
	tempurl = tempurl+"&m=task_assign";
	$("#ajax-url").val(tempurl);
	var taskid = $("#taskid").val();
	var data = new Object();
	data.taskid = taskid;
	frame_obj.comfirm('确定要终止该任务吗？',function(){
		frame_obj.do_ajax_post($(this), 'stop_task', JSON.stringify(data),do_result,"终止成功",undefined,undefined,'终止中……');
	});
}
function delay(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task_perform&a=delay_page&taskid="+taskid;
}
function finish(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task_perform&a=finish_page&taskid="+taskid;
}
function callback(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task_perform&a=callback_page&taskid="+taskid;
}
function do_result(data,content){
		if(data['errcode']==0){
			var tempurl = $("#list_url").val();
			location.href = tempurl+"&m=task&a=assign_list";
		}else{
			frame_obj.alert(data['errmsg']);
		}
}
function get_open_list(open_type,fun_name){
	var tempurl = $("#list_url").val();
	tempurl = tempurl+"&m=task";
	$("#ajax-url").val(tempurl);
	var data = get_taskid_data(open_type);
	frame_obj.do_ajax_post($(this), 'open_list', JSON.stringify(data),fun_name);
}

function get_taskid_data(open_type){
	var data = new Object();
	data.listtype = new Object();
	data.listtype.taskid = $("#taskid").val();
	data.listtype.open_type = open_type;
	return data;
}

//子任务列表
function set_son_list(data){
	var htmlStr ="";
	for(var i=0;i<data['ret_list'].length;i++){
		htmlStr += '<div class="children-task"><div class="sitem"><div  style="padding-top:8px;line-height: 25px;">'+data['ret_list'][i]["task_title"]+'<span class="right time">'
		+data['ret_list'][i]["starttime"]+'</span><div class="clear"></div></div></div>'
		+'<div class="item"><div>'+data['ret_list'][i]["state_name"]+'<button onclick="javascript:to_task(\''+data['ret_list'][i]['id']+'\');" type="button" class="btn  red right easy-button btn-default" >查看详情</button></div></div></div>';
	}
	$("#son_list").html(htmlStr);
}

//附件列表
function set_file_list(data){
	var htmlStr="";
	for(var i=0;i<data['ret_list'].length;i++){
		if(i==0){
			htmlStr=htmlStr+"<div class='children-task'>";
		}
		htmlStr += '<div class="item"><div style="overflow:hidden;"><span class="attach" onclick="download_file(\''+data['ret_list'][i]["id"]+'\',\''+data['ret_list'][i]["file_url"]+'\' ,\''+data['ret_list'][i]["file_key"]+'\',\'task_info\',\''+$('#taskid').val()+'\')" i="'+data['ret_list'][i]["id"]+'">'+data['ret_list'][i]["file_name"]+'</span></div></div>';
		if(i==data['ret_list'].length-1){
			htmlStr+="</div>";
		}
	}
	$("#file_list").html(htmlStr);
}

function download_file(id,url,key,type,objid){
//	frame_obj.show_img($('#media-url').val()+url);
//	return;
	var u = url.toLowerCase();
	if(u.indexOf('.jpg') >0 || u.indexOf('.jpeg') >0 || u.indexOf('.png') >0){
		frame_obj.show_img($('#media-url').val()+url);
	}else{
		var data = new Object();
		data.id = id;
		data.objid = objid;
		data.key = key;
		data.type = type;
		$(".load_img").css("display","block");
		frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);
	}

	//$("#download_div").attr("src",url);
}
function download_complete(data){
	$(".load_img").css("display","none");
	if (data.errcode == 0) {
		close_win();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}

function close_win(){
	wx.closeWindow();
}


function show_img(url){
	frame_obj.show_img($('#media-url').val()+url);
}


//评论列表
function set_talking_list(data){
	var htmlStr ="";
	for(var i=0;i<data['ret_list'].length;i++){
		htmlStr = htmlStr+'<div class="children-report">'
			+'<div class=face><img src="'+data['ret_list'][i]['pic_url']+'64"></div>'
			+'<div class="message">'
			+'<div>'+data['ret_list'][i]['talking_name']+'<span class="right time">'+data['ret_list'][i]['talking_time']+'</span></div>'
			+'<div>'+data['ret_list'][i]['talking_content']+'</div>'
			+'</div>'
		+'</div>';
	}
	$("#talking_list").html(htmlStr);
}

//审批列表
function set_apply_list(data){
	var htmlStr ="";
	for(var i=0;i<data['ret_list'].length;i++){
		var state = data['ret_list'][i]['apply_state']
		var temphtml = '';
		if(state==2){
			temphtml = '<span style="color:blue">申请退回</span>';
		}
		if(state==3){
			temphtml = '<span style="color:blue">申请延期</span>';
		}
		if(state==4){
			temphtml = '<span style="color:blue">申请完成</span>';
		}
		var agree_state = 'agreeed';
		if(data['ret_list'][i]['is_agree']!=1 && data['ret_list'][i]['is_agree']!=2){
			var agree_state = 'non_agreeed';
		}
		
		htmlStr += '<div class="children-report '+agree_state+'" i="'+data['ret_list'][i]['id']+'" >'
			+'<div class=face><img src="'+data['ret_list'][i]['pic_url']+'64"></div>'
			+'<div class="message">'
				+'<div>'+data['ret_list'][i]['applyer_name']+'<span class="right time">'+data['ret_list'][i]['apply_time']+'</span></div>'
				+'<div>'+temphtml+'：'+data['ret_list'][i]['apply_content']+'</div>'
			+'</div>';
			
			if(data['ret_list'][i]['is_agree']!=0){
				var is_agree = data['ret_list'][i]['is_agree'];
				temphtml = '&nbsp;<span style="color:green">同意</span>';
				if(is_agree==1){
					temphtml = '&nbsp;<span style="color:red">不同意</span>';
				}
				htmlStr=htmlStr+'<div class="j"></div><div class="message-apply" >'
					+'<div style="margin:0 10px">'+data['ret_list'][i]['answer_name']+temphtml+' <span class="right time">'+data['ret_list'][i]['answer_time']+'</span></div>'
					+'<div style="margin:0 10px">'+data['ret_list'][i]['answer_content']+'</div>'
				+'</div>';
			}
		htmlStr=htmlStr+'</div>';
	}
	$("#apply_list").html(htmlStr);
	if($('#states').val()==101){
		$('.non_agreeed').unbind('click').bind('click',function(){
			var tempurl = $("#list_url").val();
			var taskid = $("#taskid").val();
			location.href = tempurl+"&m=task_assign&a=approval_page&taskid="+taskid+"&id="+$(this).attr('i');
		});
	}
}

//汇报列表
function set_report_list(data){
	var htmlStr ="";
	for(var i=0;i<data['ret_list'].length;i++){
		htmlStr += '<div class="children-report">'
			+'<div class=face><img src="'+data['ret_list'][i]['pic_url']+'64"></div>'
			+'<div class="message">'
			+'<div>'+data['ret_list'][i]['reporter_name']+'<span class="right time">'+data['ret_list'][i]['report_time']+'</span></div>'
			+'<div>'+data['ret_list'][i]['report_content']+'</div>';
		htmlStr+='</div><div style="clear:both;"></div>';
		if(data['ret_list'][i]['report_file']!=undefined){
			for(var j=0;j<data['ret_list'][i]['report_file'].length;j++){
				htmlStr+='<div style="background-color:#ebebeb;margin:5px 0;"><span class="attach" onclick="download_file(\''+data['ret_list'][i]['report_file'][j]["id"]+'\',\''+data['ret_list'][i]['report_file'][j]["file_url"]+'\' ,\''+data['ret_list'][i]['report_file'][j]["file_key"]+'\',\'task_report\',\''+data['ret_list'][i]['report_file'][j]["objectid"]+'\')" i="'+data['ret_list'][i]['report_file'][j]["id"]+'">'+data['ret_list'][i]['report_file'][j]["file_name"]+'</span></div>';
			}
		}
		htmlStr+='</div>';
	}
	$("#report_list").html(htmlStr);
}


function refalshwin(data,content){
	window.location.reload();
}
/**
 * 跳转到父/子任务
 * @param id 加密ID
 * @param type 1.查看父任务，2.查看子任务
 */
function to_task(id){
	$('#ajax-url').val($("#app-url").val()+"&m=task");
	frame_obj.do_ajax_post($(this),'task',
			{
				'taskid':id
			},
			function(data){
				$('.t_title').html(data['data']['task_title']);
				$('.t_content').html(data['data']['task_describe']);
				$('.t_start_time').html(data['data']['start_date']);
				$('.t_end_time').html(data['data']['end_date']);
				$('.t_perform').html(data['data']['perform_name']);
				$('.t_assign').html(data['data']['assign_name']);
				$('.t_jiandu').html(data['data']['superintendent_name']);
				
				$('#task-dlg').modal('show');
			},
			'',
			'json',
			'task'
	);
}


function create_page(){
	var tempurl = $("#list_url").val();
	var taskid = $("#taskid").val();
	location.href = tempurl+"&m=task&a=create_page&taskid="+taskid;
}

