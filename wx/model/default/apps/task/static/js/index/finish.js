var disable = 0;
$(document).ready(function(){
	$storage.init({
		key:$('#storage_key').val(),
		ckey:['.content'],
		time:2000,
	});
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});
	init_datetime_scroll('#time_finish');
	$('#time_finish').val($('#time_finish').attr('i'));

	//提交
	$('.submit-button').click(function(){
		if(disable == 1){
			return;
		}else{
			disable = 1;
		}
		if($('.content').val()==''){
			frame_obj.alert('请输入申请完成内容！');
			disable = 0;
			return;
		}
		var data={
				'taskid':$('#taskid').val(),
				'apply_content':$('.content').val(),
				'finish_time':$('#time_finish').val()
		}
		$('#ajax-url').val($('#app-url').val()+'&m=task_perform');
		
		frame_obj.do_ajax_post($(this),'finish_task', JSON.stringify(data),do_result,"申请完成成功",undefined,undefined,'提交中……');
		
	});
});

function do_result(data,content){
	disable = 0;
	if(data['errcode']==0){
		$storage.clear();
		var tempurl = $("#app-url").val();
		location.href = tempurl+"&m=task&a=perform_list";
	}else{
		frame_obj.alert(data['errmsg']);
	}
}


function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};
