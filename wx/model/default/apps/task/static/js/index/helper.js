var page=1;
$(document).ready(function(){
	
	init_scroll_load('.container', '.wait', function(){
		var data={'page':Number(page)+1};
		$('#ajax-url').val('index.php?app=task&m=task&is_finish='+$('#isfinish').val()+'&is_agree='+$('#isagree').val());
		frame_obj.do_ajax_post($(this),'load_helper_task',
				data,
				function(data){
					if(data['errcode']!=0){ 	//加载失败，不再加载
						scroll_load_complete('.wait',0);
					}else{
						scroll_load_complete('.wait',data['data']['count']);
						if(data['data']['count']==0)
							return;
						page=data['data']['page'];
						var html="";
						for(var i in data['data']['data']){
							var task = data['data']['data'][i];
							
							var degree ='';
							if(task['degree']=='0'){
								degree = '低';
							}else if(task['degree']=='1'){
								degree = '中';
							}else if(task['degree']=='2'){
								degree = '高';
							}
							
							html +=
								'<div class="top_row"><span class="tclass">任务分类：'+task['classify_name']+'</span><span class="start_time right">'+task['starttime']+'</span></div>'+
								'<div class="row" onclick="open_task_detail(\''+task['id']+'\')">'+
								'<div class="face"><img src="'+task['pic_url']+'64" /></div>'+
								'<div class="task_content">'+
								'<div class="right_state"><div class="level level-'+(Number(task['degree'])+1)+'">'+degree+'</div><div style="margin-top:-13px">'+task['state_name']+'</div></div>'+
								'<div class="task_title" >'+task['task_title']+'</div>';
								if(task['state']<4){
									html += '<div>'+task['stime_state']+'：'+'<span class="red">'+task['stime']+'</span></div>';
								}else{
									html += '<div>已经结束</div>';
								}
								html += '<div>计划结束时间：<span>'+task['endtime']+'</span></div></div>	</div>';
							if($('#isagree').val()==0){
								html +=
								'<div class="row-helper">'+
								'<div>申请人：'+task['apply_name']+'</div>'+
								'<div>申请时间：'+task['apply_time']+'</div>'+
								'</div>';
							}
						}
						$('.wait').before(html);
					}
				}
		);
	});
	
	
	$('.leader-add').unbind('click').bind('click',function(){
		$('#add-type').val('leader');
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		init_contact();
	});
	
	$('#is_agree_off').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=0&is_agree=0";
		window.location.href=tempurl;
	});
	
	$('#unfinish').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=0&is_agree=2";
		window.location.href=tempurl;
	});
	
	$('#finish').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=1&is_agree=2";
		window.location.href=tempurl;
	});
	
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});

	//提交
	$('.submit-button').click(function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task_perform";
		$("#ajax-url").val(tempurl);
		if($('.content').val()==''){
			frame_obj.alert('请输入申请协办内容！');
			return;
		}
		if($('#leader>.leader').size()==0){
			frame_obj.alert('请选择协办人！');
			return;
		}
		var leader="";		//协办人
		var leader_id="";	//协办人ID
		$('#leader > .group_item').each(function(){
			if($(this).attr('i')==0)
				return;
			leader_id +=$(this).attr('i')+',';
			leader += $(this).children('.group_item_name').html()+',';
		});
		
		if(leader_id!=""){
			leader_id = leader_id.substring(0, leader_id.length-1);
			leader = leader.substring(0, leader.length-1);
		}
		//封装提交数据
		var data = new Object();
		data.taskid = $('#taskid').val();
		data.apply_content = $('.content').val();
		data.helper_id = leader_id;
		data.helper_name = leader;
		frame_obj.do_ajax_post($(this),'helper_task',
				JSON.stringify(data),
				function(data){
					if(data['errcode']==0){
						frame_obj.alert("申请协办成功","",go_perform_page);
					}
				},
				'',
				'json'
				
		);
	});
});

function go_perform_page(){
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+$('#taskid').val()+"&states=102";
}

function answer_helper(helpinfoid){
	var tempurl = $("#list_url").val();
	location.href = tempurl+"&m=task_helper&a=checkout_page&helpinfoid="+helpinfoid;
}



function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}


function remove_leader(id){
	$('.leader_choose_list').children('li[i="'+id+'"]').remove();
	$('#add-type').val('leader');
}

function open_task_detail(taskid){
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+taskid+"&states=104";
}

