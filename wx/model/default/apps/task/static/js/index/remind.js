var disable = 0;
$(document).ready(function(){
	
	$storage.init({
		key:$('#storage_key').val(),
		ckey:['.content'],
		time:2000,
	});
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});

	//提交
	$('.submit-button').click(function(){
		if(disable == 1){
			return;
		}else{
			disable = 1;
		}
		if($('.content').val()==''){
			frame_obj.alert('请输入提醒内容！');
			return;
		}
		$('#ajax-url').val('index.php?app=task&m=task_assign');
		
		var data ={
				'taskid':$('#taskid').val(),
				'msg_desc':$('.content').val()
			};
		frame_obj.do_ajax_post($(this),'remind_task', JSON.stringify(data),do_result,"提醒成功",undefined,undefined,'提交中……');
	});
});


function do_result(data,content){
	disable = 0;
	if(data['errcode']==0){
		$storage.clear();
		var states = $("#states").val();
		var tempurl = $("#app-url").val();
		var taskid = $("#taskid").val();
		location.href = tempurl+"&m=task&a=task_page&states="+states+"&taskid="+taskid;
	}else{
		frame_obj.alert(data['errmsg']);
	}
}

