var disable= 0;
$(document).ready(function(){
	
	$storage.init({
		key:$('#storage_key').val(),
		ckey:['.content'],
		time:2000,
	});
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});

	//提交
	$('.submit-button').click(function(){
		if(disable == 1){
			return;
		}else{
			disable = 1;
		}
		if($('.content').val()==''){
			frame_obj.alert('请输入汇报内容！');
			return;
		}
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task_perform";
		$("#ajax-url").val(tempurl);
//		var data ={
//			"taskid":$('#taskid').val(),
//			"report_content":$('.content').val(),
//			"file_info_str":$("#file_info_str").val()
//		}
		var file=[];
		$('.file_div > .file_info').each(function(){
			file.push({
				'file_name' : $(this).attr('fname'),
				'file_type' : $(this).attr('ftype'),
				'file_key'  : $(this).attr('fkey'),
				'file_url'  : $(this).attr('furl'),
			});
		});
		var data = new Object();
		data.taskid = $('#taskid').val();
		data.report_content = $('.content').val();
		data.file_info_str = file;
		frame_obj.do_ajax_post($(this),'report_task', JSON.stringify(data),do_result,"汇报成功");
		
	});
	init_upload();
});

var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			
			currUploadDiv = $('<div class="file_div" data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info file_name">' + fileName + '</div>'+
							'<span id="mupload-notice">开始扫描文件</span>'+
						'</div>');
			$(".file_content").append(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('#mupload-notice').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('#mupload-notice').html('正在上传');
		},
		process		: function(percent) {
			$('#mupload-notice').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	
    		var is_exist = false;
        	var file_key = data.hash;
        	$('#files_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('fkey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('ftype', data.file_ext);
    		
    		var optBtn = '<span class="delete_btn" onclick="removeFile($(this).parents(\'.file_div\'));">删除</span>';
    		if (isImg == 1) {
    			optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		$('#mupload-notice').html(optBtn);
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});

	//插入预览图
	$('.preview_btn').each(function() {
		var target = $(this).parents('.file_div').find('.file_info');
		var html = '<div class="swiper-slide" data-file="' + $(target).attr('fname') + '">' +
			'<img src="' + mediaUrl + ($(target).attr('furl'))+ '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
		'</div>';
		mupload.mupload('appendPreview', $(html));		//插入到前面
	});
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
}

//滚动到目标预览图
function slideTotarget(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	mupload.mupload('slideToPreview', targetIdx, 1000, fileName);
}


function go_task_page(){
	var states = $("#states").val();
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+$('#taskid').val()+"&states="+states;
}
function delete_file(id){
	$('.uploadfile[i="'+id+'"]').remove();
}

function do_result(data,content){
	disable= 0;
	if(data['errcode']==0){
		$storage.clear();
		frame_obj.alert(content,'确定',function(){
			var states = $("#states").val();
			var tempurl = $("#app-url").val();
			var taskid = $("#taskid").val();
			location.href = tempurl+"&m=task&a=task_page&states="+states+"&taskid="+taskid;
		});
	}else{
		frame_obj.alert(data['errmsg']);
	}
	
}

