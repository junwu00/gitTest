var disable = 0;
$(document).ready(function(){
	$('.leader-add').unbind('click').bind('click',function(){
		$('#add-type').val('leader');
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		init_contact();
	});
	$('.red').iCheck({
	    radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	 });
	$('.blue').iCheck({
	    radioClass: 'iradio_square-blue',
	    increaseArea: '20%' // optional
	 });
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});
	//提交
	$('.submit-button').click(function(){
		if(disable == 1){
			return;
		}else{
			disable = 1;
		}
		if(!$('.agree:checked').val()){
			frame_obj.alert('请选择同意与否!');
			return;
		}
		
		send_back_id="";
		send_back_name ="";
		if($('#approval-type').val()==2){
			$('#leader > .group_item').each(function(){
				if($(this).attr('i')==0)
					return;
				send_back_id +=$(this).attr('i')+',';
				send_back_name += $(this).children('.group_item_name').html()+',';
			});
		}
		if(send_back_id!=""){
			send_back_id = send_back_id.substring(0,send_back_id.length-1);
			send_back_name = send_back_name.substring(0,send_back_name.length-1);
		}
		var data = {
			'id':$('#id').val(),
			'taskid':$('#taskid').val(),
			'state':$('#approval-type').val(),
			'is_agree':$('.agree:checked').val(),
			'answer_content':$('.content').val(),
			'send_back_id':send_back_id,
			'send_back_name':send_back_name,
			'score':$('#score').val()
		};
		$('#ajax-url').val('index.php?app=task&m=task_assign');
		
		frame_obj.do_ajax_post($(this),'approval_task', JSON.stringify(data),do_result,"审批成功");
		
//		frame_obj.do_ajax_post($(this),'approval_task',
//				JSON.stringify(data),
//				function(data){
//					frame_obj.alert(data['errmsg'],'确定',function(){
//						if(data['errcode']==0)
//							location.href="index.php?app=task&m=task&a=assign_list";
//					});
//				},
//				'',
//				'json',
//				'approval_task'
//		);
	});
	init_data();
	
	$('#agreeoff').on('ifChecked', function(event){
		hide();
	});
	
	$('#agreeon').on('ifChecked', function(event){
		show();
	});
	
});

function do_result(data){
	disable = 0;
	if(data['errcode']==0){
		var states = $("#states").val();
		var tempurl = $("#app-url").val();
		var taskid = $("#taskid").val();
		frame_obj.alert('审批成功','确定',function(){
			location.href = tempurl+"&m=task&a=assign_list";
		});
	}else{
		frame_obj.alert(data['errmsg'],'确定',function(){
		});
	}
}


function show(){
	if($('#approval-type').val()==2){//退回
		$('.perform').css('display','block');
	}else if($('#approval-type').val()==4){//完成
		$('.score').css('display','block');
	}
	$('.answer').css('display','none');
}

function hide(){
	$('.perform').css('display','none');
	$('.answer').css('display','block');
	$('.score').css('display','none');
}
function init_data(){
	if($('#approval-type').val()==3){
		$('.start_time').css('display','block');
	}else if($('#approval-type').val()==4){
		$('.finish_time').css('display','block');
	}
}

function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}


function remove_leader(id){
	$('.leader_choose_list').children('li[i="'+id+'"]').remove();
	$('#add-type').val('leader');
}

function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};
