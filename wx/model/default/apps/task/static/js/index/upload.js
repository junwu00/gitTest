$(document).ready(function(){
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});

	//提交
	$('.submit-button').click(function(){
		var tempurl = $("#app-url").val();
		tempurl = tempurl+"&m=task_assign";
		$("#ajax-url").val(tempurl);
//		var data ={
//			"taskid":$('#taskid').val(),
//			"report_content":$('.content').val(),
//			"file_info_str":$("#file_info_str").val()
//		}
		var file="";
		$('.uploadfiles > .uploadfile').each(function(){
			if(!$(this).attr('i'))
				return;
			file +=$(this).attr('i')+',';
		});
		if(!$.trim(file)){
			frame_obj.alert('没有要上传的附件');
			return;
		}
			
		var data = new Object();
		data.taskid = $('#taskid').val();
		data.file_info_str = file;
		
		frame_obj.do_ajax_post($(this),'upload_file',
				JSON.stringify(data),
				function(data){
					if(data['errcode']==0){
						frame_obj.alert("保存成功","",function(){
							location.href="index.php?app=task&m=task&a=task_page&states=101&taskid="+$('#taskid').val();
						});
					}
				},
				'',
				'json'
		);
	});
	
	frame_obj.ajax_upload(
			$('#file'),
			$('#formid'),
			'index.php?app=task&m=task',
			function(){
				var val = $("#file").val();
				var k = val.substr(val.lastIndexOf(".")+1);
				if(!(k == 'jpeg' || k == 'jpg')){
					frame_obj.alert('请上传JPG格式的图片');
					return false;
				}else{
					$('#type').val(k);
					return true;
				}
			},
			function(data, callback_params){
				data = $.parseJSON(data);
				if(data.errcode == 0){
					var html ='<div class="uploadfile" i="'+data.info.file+'"><img src="'+$('#media-url').val()+data.info.url+'" onclick="delete_file('+data.info.file+')" ><i class="icon-remove-sign"></i></div>';
					$('.uploadfiles').prepend(html);
				}else{
					frame_obj.alert(data.errmsg);
				}
			},
			'',
			30000
	);
	
});


function go_task_page(){
	var states = $("#states").val();
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+$('#taskid').val()+"&states="+states;
}
function delete_file(id){
	$('.uploadfile[i="'+id+'"]').remove();
}
