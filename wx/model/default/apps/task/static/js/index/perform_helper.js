var page=1;
$(document).ready(function(){
	
	$('.leader-add').unbind('click').bind('click',function(){
		$('#add-type').val('leader');
		$('.container').css('display','none');
		$('.contact-content').css('display','block');
		init_contact();
	});
	
	$('#is_agree_off').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=0&is_agree=0";
		window.location.href=tempurl;
	});
	
	$('#unfinish').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=0&is_agree=2";
		window.location.href=tempurl;
	});
	
	$('#finish').unbind('click').bind('click',function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task&a=helper_list&is_finish=1&is_agree=2";
		window.location.href=tempurl;
	});
	
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});

	//提交
	$('.submit-button').click(function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task_perform";
		$("#ajax-url").val(tempurl);
		if($('.content').val()==''){
			frame_obj.alert('请输入申请协办内容！');
			return;
		}
		if($('#leader>.leader').size()==0){
			frame_obj.alert('请选择协办人！');
			return;
		}
		var leader="";		//协办人
		var leader_id="";	//协办人ID
		$('#leader > .group_item').each(function(){
			if($(this).attr('i')==0)
				return;
			leader_id +=$(this).attr('i')+',';
			leader += $(this).children('.group_item_name').html()+',';
		});
		
		if(leader_id!=""){
			leader_id = leader_id.substring(0, leader_id.length-1);
			leader = leader.substring(0, leader.length-1);
		}
		//封装提交数据
		var data = new Object();
		data.taskid = $('#taskid').val();
		data.apply_content = $('.content').val();
		data.helper_id = leader_id;
		data.helper_name = leader;
		frame_obj.do_ajax_post($(this),'helper_task', JSON.stringify(data),do_result,"申请协办成功");
	});
});

function go_perform_page(){
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+$('#taskid').val()+"&states=102";
}

function do_result(data,content){
	frame_obj.alert(content,'确定',function(){
		if(data['errcode']==0){
			location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+$('#taskid').val()+"&states=102";
		}
	});
}


function answer_helper(helpinfoid){
	var tempurl = $("#list_url").val();
	location.href = tempurl+"&m=task_helper&a=checkout_page&helpinfoid="+helpinfoid;
}



function init_contact(){
	$('.bottom').children('div').each(function(){
		$(this).click();
		$('.icon-ok-circle').addClass('dcir icon-circle-blank');
		$('.icon-ok-circle').removeClass('icon-ok-circle');
	});
}


function remove_leader(id){
	$('.leader_choose_list').children('li[i="'+id+'"]').remove();
	$('#add-type').val('leader');
}

function open_task_detail(taskid){
	window.location.href = $("#list_url").val()+"&m=task&a=task_page&taskid="+taskid+"&states=104";
}

