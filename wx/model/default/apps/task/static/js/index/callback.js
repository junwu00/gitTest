$(document).ready(function(){
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});
	

	//提交
	$('.submit-button').click(function(){
		if($('.content').val()==''){
			frame_obj.alert('请输入申请退回内容！');
			return;
		}
		var data={
				'taskid':$('#taskid').val(),
				'apply_content':$('.content').val()
			};
		$('#ajax-url').val($('#app-url').val()+'&m=task_perform');
		
		frame_obj.do_ajax_post($(this),'callback_task', JSON.stringify(data),do_result,"申请退回成功");
		
	});
});


function do_result(data,content){
	frame_obj.alert(content,'确定',function(){
		if(data['errcode']==0){
			var tempurl = $("#app-url").val();
			location.href = tempurl+"&m=task&a=perform_list";
		}
	});
}

