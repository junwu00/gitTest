$(document).ready(function(){

	$('.fail').click(function(){
		history.go(-1);
	});
	
	$('.red').iCheck({
	    radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	 });
	$('.blue').iCheck({
	    radioClass: 'iradio_square-blue',
	    increaseArea: '20%' // optional
	 });
	
	$('#agreeoff').on('ifChecked', function(event){
		textarea_show(1);
	});
	
	$('#agreeon').on('ifChecked', function(event){
		textarea_show(2);
	});
	
	//提交
	$('.submit-button').click(function(){
		var tempurl = $("#list_url").val();
		tempurl = tempurl+"&m=task_helper";
		$("#ajax-url").val(tempurl);
		//封装提交数据
		var data = new Object();
		data.id = $('#id').val();
		data.is_agree = $('input:radio:checked').val();
		if(data.is_agree==1){
			if($.trim($("#agreeoff_text").val())==''){
				frame_obj.alert('请输入驳回信息');
				return;
			}
		}
		data.helper_content = $("#agreeoff_text").val();
		data.taskid = $('#taskid').val();
		
		frame_obj.do_ajax_post($(this),'checkout_task',
				JSON.stringify(data),
				function(data){
					frame_obj.alert('协办确认成功','确定',helper_list_page);
				},
				'',
				'json'
				
		);
	});
});

function helper_list_page(){
	window.location.href = $("#list_url").val()+"&m=task&a=helper_list&is_finish=0&is_agree=0";
}

function textarea_show(is_agree){
	if(is_agree==1){
		$("#agreeoff_content").css("display","block");
	}
	else{
		$("#agreeoff_content").css("display","none");
	}
}


