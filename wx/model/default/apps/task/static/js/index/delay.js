$(document).ready(function(){
	
	
	//取消返回
	$('.fail').click(function(){
		history.go(-1);
	});
	init_datetime_scroll('#time_delay');
	$('#time_delay').val($('#time_delay').attr('i'));

	//提交
	$('.submit-button').click(function(){
		if($('.content').val()==''){
			frame_obj.alert('请输入延期申请内容！');
			return;
		}
		var data={
				'taskid':$('#taskid').val(),
				'apply_content':$('.content').val(),
				'delayed_time':$('#time_delay').val()
			};
		$('#ajax-url').val('index.php?app=task&m=task_perform');
		
		frame_obj.do_ajax_post($(this),'delay_task', JSON.stringify(data),do_result,"申请延期成功");
	});
});

function do_result(data,content){
	frame_obj.alert(content,'确定',function(){
		if(data['errcode']==0){
			var tempurl = $("#app-url").val();
			window.location.href = tempurl+"&m=task&a=perform_list";
		}
	});
}


function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};
