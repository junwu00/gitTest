<?php
/**
 * 审批流程通用父类
 * @author lijs
 * @date 2016-01-22
 * @version 1.1
 */
class abs_new_proc extends abs_app_base {
	
	protected static $PageSize = 10;
	/**
	 * 人员状态
	 */
	protected static $UserDel = 0;//删除用户
	
	protected static $UserFollow  = 2;//关注用户
	
	protected static $UserFloze = 3;//冻结用户
	
	protected static $UserNotFollow  = 4;//未关注
	
	/**
	 * 工作项状态
	 */
	/** 新收到1 */
	protected static $StateNew = 1;
	/** 已阅读 2 */
	protected static $StateRead = 2;
	/** 已保存 3 */
	protected static $StateSave = 3;
	/** 等待中 4 */
	protected static $StateWait = 4;
	/** 回收 5 */
	protected static $StateRecovery = 5;
	/** 终止 6 */
	protected static $StateStop = 6;
	/** 已处理 7 */
	protected static $StateSend = 7;
	/** 已退回 8 */
	protected static $StateCallback = 8;
	/** 已通过 9 */
	protected static $StateDone = 9;
	/** 退回 10 */
	protected static $StateSendback = 10;
	
	/**
	 * 是否退回状态
	 */
	/** 退回*/
	protected static $Callback = 1;
	/** 不是退回 */
	protected static $Not_Callback = 0;
	
	/**
	 * 是否开始节点
	 * 
	/** 是*/
	protected static $StartNode = 1;
	/** 不是*/
	protected static $NotStartNode = 0;
	
	
	/** 知会未读 1*/
	protected static $StateReading = 1;
	/** 知会已读 2*/
	protected static $StateRoad = 2;
	
	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	protected static $StateFormDraft = 0;
	/** 运行中 */
	protected static $StateFormDone = 1;
	/** 已结束 */
	protected static $StateFormFinish = 2;
	/** 终止 */
	protected static $StateFormStop = 3;
	
	/**
	 * 退回状态
	 */
	/** 退回上一步骤*/
	protected static $CallBackPer = 1;
	/** 退回开始步骤*/
	protected static $CallBackStart = 2;
	
	/**
	 * 流程图状态
	 */
	/** 已经完成的节点标示*/
	protected static $PicStateDone = 1;
	
	/** 正在进行的节点标示*/
	protected static $PicStateDoing = 2;
	
	
	/**
	 * 是否自动知会
	 */
	/** 开始时知会*/
	protected static $NotifyAutoStart = 2;
	/** 结束时知会*/
	protected static $NotifyAutoEnd = 1;
	/** 否*/
	protected static $NotNotifyAuto = 0;
	
	/**
	 * 知会标示
	 */
	/** 知会上级  */
	protected static $StateNotifyLeader = 3;
	/** 知会上级的上级  */
	protected static $StateNotifyILeader = 4;
	/** 知会发起人  */
	protected static $StateNotifySelf = 5;
	/** 知会固定人  */
	protected static $StateNotifyPeople = 1;
	/** 知会表单控件  */
	protected static $StateNotifyInput = 2;
	
	/**
	 * 控件是否可编辑
	 */
	/** 是*/
	protected static $EditInput = 1;
	/** 否*/
	protected static $NotEditInput = 0;
	
	/**
	 * 控件是否可见
	 */
	/** 是*/
	protected static $VisitInput = 1;
	/** 否*/
	protected static $NotVisitInput = 0;
	
	
	protected static $IsProc = 0;
	
	protected static $IsRest = 1;
	
	protected static $IsExaccount = 2;
	
	/** 申请人主部门 */
	protected static $ApplyerDept = 1;
	/** 申请人名称 */
	protected static $ApplyerName = 2;
	/** 申请人日期（年-月） */
	protected static $ApplyDateM = 3;
	/** 申请人日期（年-月-日） */
	protected static $ApplyDateD = 4;
	/** 申请人日期（年-月-日 时：分） */
	protected static $ApplyDateMin = 5;
	/** 表单名称 */
	protected static $FormName = 6;
	/** 自定义控件 */
	protected static $Custom = 7;
	
	/** 发布版本 */
	protected static $StatePublic = 2;
	/** 历史版本 */
	protected static $StateHistory = 1;
	
	/** 禁用 0*/
	protected static $StateOff = 0; 
	/** 启用 1*/
	protected static $StateOn = 1;
	
	/** 是否为开始工作项  1 是 */
	protected static $StartWItem = 1;
	/** 是否为开始工作项  0 是 */
	protected static $NotStartWItem = 0;
	
	
	/**
	 * 人员选择范围
	 */
	protected static $RecriverFixed = 1;//固定接收人
	
	protected static $RecriverCreater = 2;//流程发起人
	
	protected static $RecriverCreaterLeader = 3;//流程发起人领导
	
	protected static $RecriverCreaterDept = 4;//流程发起人部门
	
	protected static $RecriverHandler = 5;//当前处理人
	
	protected static $RecriverHandlerLeader = 6;//当前处理人上级领导
	
	protected static $RecriverHandlerDept = 7;//当前处理人部门
	
	protected static $RecriverInput = 8;//表单具体数据
	
	/**
	 * 比较符号
	 */
	
	protected static $Compare_Greater = 1;//大于
	
	protected static $Compare_Lesser = 2;//小于
	
	protected static $Compare_Equeal = 3;//等于
	
	protected static $Compare_Lesser_Equeal = 4;//小于等于
	
	protected static $Compare_Greater_Equeal = 5;//大于等于
	
	protected static $Compare_Not_Equeal = 6;//不等于
	
	protected static $Compare_Contain = 8;//包含
	
	protected static $Compare_Not_Contain = 9;//不包含
	
	//人员规则
	protected static $People_Position = 7;//人员职位
	
	protected static $People_Type_Creater =2;//发起人
	
	protected static $People_Type_Done =1;//当前处理人
	
	/**
	 * 是否是领签标示
	 */
	protected static $N_Linksign =0;//否
	
	protected static $Linksign =1;//是
	
	protected $ComId;
	
	protected $UserId;
	
	protected $UserName;
	
	protected $IsProcType;
	
	protected $PcDomain;
	
	protected $WxDomain;
	
	protected $AppName;
	
	protected $AppCName;
	
	protected $LinksignUrl;
	
	
	public function __construct($app_name) {
		$app_conf = include(SYSTEM_APPS.$app_name.'/config.php');
		
		parent::__construct($app_name);
		
		if($app_name=='rest'){
			$this->IsProcType = self::$IsRest;
		}
		else if($app_name=='exaccount'){
			$this->IsProcType = self::$IsExaccount;
		}
		else{
			$this->IsProcType = self::$IsProc;
		}
		$this->ComId = $_SESSION[SESSION_VISIT_COM_ID];
		$this->UserId = $_SESSION[SESSION_VISIT_USER_ID];
		$this->UserName = $_SESSION[SESSION_VISIT_USER_NAME];
		$this->PcDomain = SYSTEM_HTTP_PC_DOMAIN;
		$this->WxDomain = SYSTEM_HTTP_DOMAIN;
		$this->AppName = $app_name;
		$this->AppCName = $app_conf["cn_name"];
		$this->LinksignUrl = SIGN_MEDIA_URL_HOST;
	}
	
	
	
	/**
	 * 推送二维码消息
	 * @param string $md5_key this_url md5
	 * @param string $func 回调js
	 */
	public function send_pc_info_parent($md5_key,$func){
		try{
			//发送PC提醒
			$data = array(
				'ident' => 5,
				'ajax_url' => $this->PcDomain.'index.php?&key='.$md5_key.'&fun='.$func
			);
			g('messager') -> publish_by_user('pc', $this->UserId,$data);
		}catch(SCException $e){
			log_write("fail_user_id:".$user_id);
		}
	}
	
	public function save_linksign($workitem_id,$img_url){
		g("mv_workinst")->update_workitem($workitem_id,$this->ComId,$img_url,self::$Linksign);
	}
	
	
	/**
	 * 获取下一步骤new
	 * @param int $workitemid 工作项id
	 * @throws SCException
	 */
	public function get_next_workitem($workitem_id){
		$field = ' mpw.*,mpwn.* ';
		$workitem = $this->get_curr_workitem($workitem_id,$field);
		
		$circulation_rule = $workitem["circulation_rule"];//流转规则
		
		$formsetinst_id = $workitem['formsetinit_id'];
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
		
		$next_node_id_arr = array();
		if(empty($circulation_rule)){//流转规则为空
			$next_node_ids = $workitem["next_node_id"];
			$next_node_ids = json_decode($next_node_ids, TRUE);
			
			$this->get_receiver_rule($next_node_ids, $next_node_id_arr,$workitem,$formsetinst,$this->ComId);
		}
		else{//如果存在流转规则
			$circulation_rule = json_decode($circulation_rule, TRUE);
			$formsetinst_val = $formsetinst["form_vals"];
			$formsetinst_val = json_decode($formsetinst_val, TRUE);
			$next_node_ids = $workitem["next_node_id"];
			$next_node_ids = json_decode($next_node_ids, TRUE);
			$tmp_next_node_ids = $next_node_ids;
			$work_result_id = array();
			foreach ($circulation_rule as $val){
				//格式：XXXX#XXX#XXX;XXXX#XXX#XXX;XXXX#XXX#XXX@work_node_id
				$circulation_rule_arr = explode("@", $val);
				$work_id = $circulation_rule_arr[1];
				$rule_arr = explode(";",$circulation_rule_arr[0]);
				//验证表单规则
				$this->is_circulation_rule($rule_arr, $formsetinst_val, $work_id, $work_result_id,$workitem,$next_node_ids);
				
			}
			if(count($work_result_id)>0){
				$next_node_ids = $work_result_id; 
			}
			else{
				if(count($next_node_ids)==0){
					$next_node_ids = $tmp_next_node_ids;
				}
			}
			$this->get_receiver_rule($next_node_ids, $next_node_id_arr,$workitem,$formsetinst,$this->ComId);
		}
		
		if(!is_array($next_node_id_arr)){
			throw new SCException('找不到下一步接收人，请联系管理员');
		}
		return $next_node_id_arr;
	}
	
	
	/**
	 * 判断是否直接发送new
	 * @param array $next_node_id_arr
	 */
	public function is_send($next_node_id_arr){
		$is_send = "TRUE";//是否直接发送
		if(count($next_node_id_arr)!=1){
			$is_send = "FALSE";
		}
		else{
			$next_node_id = $next_node_id_arr[0];
			
			if((count($next_node_id["select_array"])!=0)||((count($next_node_id["receive_array"])==0)&&(!empty($next_node_id['next_node_id'])))){
				$is_send = "FALSE";
			}
		}
		return $is_send;
	}
	
	
	/**
	 * 获取当前实例new
	 * @param int $formsetinst_id 实例id
	 */
	public function get_formsetinst_by_id($formsetinst_id){
		$formsetinst = g("mv_formsetinst")->get_formsetinst_by_id($formsetinst_id,$this->ComId,$this->UserId);
		
		if(is_array($formsetinst)){
			if($formsetinst["info_state"]==self::$StateOff){
				cls_resp::show_err_page(array("该实例已被删除!"));
			}
		}
		else{
			cls_resp::show_err_page(array("没有权限查看该步骤!"));
		}
		
		return $formsetinst;
	}
	
	
	/**
	 * 获取开始节点的信息new
	 * @param int $work_id 流程模板id
	 */
	public function get_start_node($work_id){
		$start_node = g('mv_work_node') -> get_start_by_workid($this->ComId, $work_id);
		if(!$start_node){
			cls_resp::show_err_page(array("流程有误，找不到开始节点，请联系管理员!"));
		}
		return $start_node;
	}
	
	/**
	 * 获取当前步骤new
	 * @param int $workitem_id
	 */
	public function get_curr_workitem($workitem_id,$fields=NULL){
		if(empty($fields)){
			$cur_workitem = g("mv_workinst")->get_curr_workitem($workitem_id,$this->ComId,$this->UserId);
		}
		else{
			$cur_workitem = g("mv_workinst")->get_curr_workitem($workitem_id,$this->ComId,$this->UserId,$fields);
		}
		if(is_array($cur_workitem)){
			if($cur_workitem["info_state"]==self::$StateOff){
				cls_resp::show_err_page(array("该工作项已被发起人删除!"));
			}
		}
		else{
			cls_resp::show_err_page(array("没有权限访问该工作项!"));
		}
		return $cur_workitem;
	}
	
	
	
	/**
	 * 获取开始节点工作项new
	 * @param string $workitem_rule
	 * @param int $formsetinit_id
	 */
	public function get_start_item($workitem_rule,$formsetinit_id){
		if(empty($workitem_rule)){
			cls_resp::show_err_page(array("找不到开始节点，请联系管理员!"));
		}
		
		$workitem_rule = explode(",", $workitem_rule);
		$start_node_id = $workitem_rule[0];
		//开始节点的工作项有可能有多个
		$start_workitems = g("mv_workinst")->get_workitem_by_nodeid($formsetinit_id,$start_node_id,$this->ComId,$this->UserId);
		
		if(!is_array($start_workitems)){
			cls_resp::show_err_page(array("找不到开始节点的工作项，请联系管理员!"));
		}
		$start_workitem = end($start_workitems);
		return $start_workitem;
	}
	
	
	/**
	 * 获取知会详情new
	 * @param int $notify_id
	 */
	
	public function get_notify_item($notify_id){
		$notify = g("mv_notify")->get_notify_by_id($notify_id,$this->ComId,$this->UserId);
		
		if(is_array($notify)){
			if($notify["info_state"]==self::$StateOff){
				cls_resp::show_err_page(array("该表单实例已被发起人删除!"));
			}
		}
		else{
			cls_resp::show_err_page(array("没有权限访问该知会信息!"));
		}
		
		return $notify;
	}
	
	/**
	 * 更新知会状态new
	 * @param int $notify_id 知会id
	 */
	public function update_notify_state_parent($notify_id){
		g("mv_notify")->update_notify_state($notify_id,$this->ComId,$this->UserId);
	}
	
	
	/**
	 * 根据节点ID获取模板信息new
	 * @param int $work_id 流程模板ID 
	 */
	public function get_work_node($work_id){
		$work_model = g("mv_work")->get_by_id($this->ComId,$work_id);
			
		if(!is_array($work_model)){
			cls_resp::show_err_page('找不到对应的流程，请联系管理员');
		}	
		
		return $work_model;
	}
	
	
	/**
	 * 获取流程图的信息new
	 * @param array $formsetinst 实例对象
	 */
	public function get_pic_rule($formsetinst){
		
		$workitem_rule = $formsetinst["workitem_rule"];
		$curr_node_id = $formsetinst["curr_node_id"];
		
		if(strripos($workitem_rule,"@")>-1){
			$workitem_rule_arr = explode("@",$workitem_rule);
			$workitem_rule = array_shift($workitem_rule_arr);
			$workitem_rule = explode(',', $workitem_rule);
			$workitem_back_arr = $workitem_rule_arr;
			foreach ($workitem_back_arr as $wb_arr){
				$back_key = 0;
				foreach ($workitem_rule as $w_key=>$w_rule){
					if($w_rule==$wb_arr){
						$back_key = $w_key;
					}
				}
				$workitem_rule = array_slice($workitem_rule, 0, $back_key+1);
			}
			
			$workitem_rule = implode(',', $workitem_rule);
		}
		
		$worknodes = g("mv_work_node")->get_work_node_by_id($this->ComId,$workitem_rule);
		
		$workitem_rule = explode(',', $workitem_rule);
		$workitem_rule = array_unique($workitem_rule);
		
		if(!is_array($worknodes)||count($workitem_rule)!=count($worknodes)){
			cls_resp::show_err_page('找不到对应的节点，请联系管理员');
		}
		
		$worknodes_arr = array();
		foreach($workitem_rule as $w_rule){
			$cur_worknode=NULL;
			foreach ($worknodes as $worknode){
				if($w_rule == $worknode["id"]){
					$cur_worknode = $worknode;
					break;
				}
			}
			unset($worknode);
			if($curr_node_id==$cur_worknode["id"]&&$formsetinst["state"]==self::$StateFormDone){
				$worknodes_arr[$cur_worknode["node_key"]] = self::$PicStateDoing;
			}
			else{
				$worknodes_arr[$cur_worknode["node_key"]] = self::$PicStateDone;
			}
		}
		$worknodes_arr = json_encode($worknodes_arr);
		return $worknodes_arr;
	}
	
	
	/**
	 * 知会流程
	 */
	public function notify_workitem_parent($data){
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($data["formsetinst_id"]);
		
		foreach($data["receivers"] as $receiver){
			$receiver_id = $receiver["receiver_id"];
			$receiver_name = $receiver["receiver_name"];
			$notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);
			
			$user = g('api_user') -> get_by_id($receiver_id);
			if(!is_array($user)){throw new SCException('找不到知会的人员！');}
			$msg_user = array($user['acct']);
			$msg_title=$this->AppCName."知会提醒";
			$msg_desc = $this->AppCName."：".$this->UserName."将流程《".$formsetinst["formsetinst_name"]."》知会给您，请及时了解流程详情。";
			$msg_url = $this->WxDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id.'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;//TODO
			
			try{
				//发送PC提醒
				$data = array(
					'url' => $pc_url,
					'title' => $msg_desc,
					'app_name' => $this->AppName
				);
				g('messager') -> publish_by_user('pc', $receiver_id,$data);
				//发送微信提醒
				parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
			}catch(SCException $e){
				log_write("fail_user ".$user["name"]);
			}
		}
		unset($receiver);
	}
	
	/**
	 * 催办流程
	 */
	public function press_workitem_parent($data){
		
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($data["formsetinst_id"]);
	
		//获取当前正在进行的工作项
		$curr_node_id = $formsetinst["curr_node_id"];
		$doing_workitems = g("mv_workinst")->get_node_state_item($data["formsetinst_id"],$curr_node_id,array(self::$StateNew,self::$StateRead,self::$StateSave),$this->ComId);
		if(!is_array($doing_workitems)){throw new SCException('找不到催办的工作项！');}
		
		foreach ($doing_workitems as $workitem){
			$handler_id = $workitem["handler_id"];
			$user = g('api_user') -> get_by_id($handler_id);
			if(!is_array($user)){throw new SCException('找不到催办的人员！');}
			
			$msg_user = array($user['acct']);
			$msg_title=$this->AppCName."催办提醒";
			$msg_desc = $this->AppCName."：".$this->UserName."对您需要处理的流程《".$formsetinst["formsetinst_name"]."》进行了催办，请及时审批流程。";
			$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem["id"].'&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];//TODO
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem["id"];//TODO
			
			try{
				$data = array(
					'url' => $pc_url,
					'title' => $msg_desc,
					'app_name' => $this->AppName
				);
				g("messager")->publish_by_user('pc', $handler_id,$data);
				
				//发送微信提醒
				parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
			}catch(SCException $e){
				log_write("fail_user ".$user["name"]);
			}
		}
		unset($workitem);
	}
	
	/**
	 * 删除工作项
	 */
	public function del_workitem_parent($data){
		$ret = array();
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		if($curr_workitem["is_start"]==0){throw new SCException('没有权限删除工作项！');}
		
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		
		//获取未完成的工作项
		$not_finish_items = g("mv_workinst")->get_formset_state_item_nwi($formsetinst["id"],$data["workitem_id"],array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId);
		$count = empty($not_finish_items)?0:count($not_finish_items);
		if($count==0&&($curr_workitem["state"]==self::$StateNew||$curr_workitem["state"]==self::$StateRead||$curr_workitem["state"]==self::$StateSave)){
			g("mv_formsetinst")->del_formsetInst($formsetinst["id"],$this->ComId,$this->UserId);
			g("mv_workinst")->delete_workitem($formsetinst["id"],$this->ComId,$this->UserId);
			g("mv_file")->del_file_by_formsetid($this->ComId,$this->UserId,$formsetinst["id"]);
			g("mv_notify")->delete_notify($formsetinst["id"],$this->ComId,$this->UserId);
			$errmsg = '流程删除成功！';
			$errcode = 0;
		}
		else{
			$errmsg = '删除失败，流程已经在流转或者已经结束，没有权限删除工作项！';
			$errcode = 1;
		}
		$ret["errmsg"] = $errmsg;
		$ret["errcode"] = $errcode;
		
		return $ret;
	}
	
	/**
	 * 发送到退回步骤
	 */
	public function sendback_workitem_parent($data){
		$ret = array();
			
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		
		if($curr_workitem["state"]!=self::$StateNew&&$curr_workitem["state"]!=self::$StateRead&&$curr_workitem["state"]!=self::$StateSave){
			throw new SCException('该工作项已经处理，请返回代办列表');
		}
		
		$returnback_node_id = $curr_workitem["returnback_node_id"];
		if(empty($returnback_node_id)){throw new SCException('找不到退回节点！');}
		
		$returnback_workitem = g("mv_workinst")->get_item_by_ids(array($returnback_node_id),$this->ComId);
		$returnback_workitem = $returnback_workitem[0];
		
		//将同级退回状态状态转成原来的状态
		g("mv_workinst")->update_sendback_workitem($curr_workitem["formsetinit_id"],$returnback_node_id,$this->ComId);
		
		$handle_id = $returnback_workitem["handler_id"];
		$create_id = $returnback_workitem["creater_id"];
		$receiver_id = $curr_workitem["handler_id"];
		$handle_m_dept = g("api_user")->get_main_dept($handle_id,FALSE);
		if($handle_m_dept){
			$handle_dept_id = $handle_m_dept["id"];
			$handle_dept_name = $handle_m_dept["name"];
		}
		else{
			$handle_dept_id = -1;
			$handle_dept_name = "无部门";
		}
		$create_m_dept = g("api_user")->get_main_dept($create_id,FALSE);
		if($create_m_dept){
			$create_dept_id = $create_m_dept["id"];
			$create_dept_name = $create_m_dept["name"];
		}
		else{
			$create_dept_id = -1;
			$create_dept_name = "无部门";
		}
		$receiver_m_dept = g("api_user")->get_main_dept($receiver_id,FALSE);
		if($receiver_m_dept){
			$receiver_dept_id = $receiver_m_dept["id"];
			$receiver_dept_name = $receiver_m_dept["name"];
		}
		else{
			$receiver_dept_id = -1;
			$receiver_dept_name = "无部门";
		}
		
		//保存工作项信息
		$work_item = array(
			'formsetinit_id' => $curr_workitem["formsetinit_id"],
			'workitem_name' => $returnback_workitem["workitem_name"],
			'com_id' => $this->ComId,
			'handler_id' => $returnback_workitem["handler_id"],
			'handler_name' => $returnback_workitem["handler_name"],
			'handler_dept_id' =>$handle_dept_id,
			'handler_dept_name' =>$handle_dept_name,
			'state' => self::$StateNew,
			'creater_id' => $returnback_workitem["creater_id"],
			'creater_name' => $returnback_workitem["creater_name"],
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name,
			'receiver_id' => $curr_workitem["handler_id"],
			'receiver_name' => $curr_workitem["handler_name"],
			'receiver_dept_id' =>$receiver_dept_id,
			'receiver_dept_name' =>$receiver_dept_name,
			'work_node_id' =>$returnback_workitem["work_node_id"],
			'pre_work_node_id'=>$returnback_workitem["pre_work_node_id"],
			'is_returnback' =>self::$Not_Callback,
			'returnback_node_id' =>0,
		);
		$workitem_id = g("mv_workinst")->save_workitem($work_item);
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["judgement"] = $data["judgement"];
		$curr_workitem["target_node"] = "送".$returnback_workitem["workitem_name"];
		g("mv_workinst")->send_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
		
		//更新流程流转
		$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		$workitem_rule = $formsetinst["workitem_rule"];
		$workitem_rule = substr($workitem_rule,0,strripos($workitem_rule, "@".$curr_workitem["work_node_id"]));
		g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$returnback_workitem["work_node_id"]);
		
		//消息提醒
		$user_id = $returnback_workitem["handler_id"];
		$user = g('api_user') -> get_by_id($user_id);
		$msg_user = array($user['acct']);
		$msg_title=$this->AppCName."待办提醒";
		$msg_desc = $this->AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》待办事项，请及时处理。";
		$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		try{
			$data = array(
				'url' => $pc_url,
				'title' => $msg_desc,
				'app_name' => $this->AppName
			);
			g("messager")->publish_by_user('pc', $user_id,$data);
			//发送微信提醒
			parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
		}catch(SCException $e){
			log_write("fail_user ".$user["name"]);
		}

		$ret["workitem_name"] = $returnback_workitem["workitem_name"];
		$ret["receiver_name"] = $user["name"];
		
		return $ret;
	}
	
	
	/**
	 * 驳回指定步骤流程
	 */
	public function callback_select_parent($data){
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		
		//将同级工作项状态转成退回状态
		g("mv_workinst")->update_node_state_item_nwi($curr_workitem["formsetinit_id"],$curr_workitem["id"],$curr_workitem["work_node_id"],self::$StateSendback,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$curr_workitem["id"]);
		//将等待中的工作项状态转成退回状态
		g("mv_workinst")->update_formset_state_item($curr_workitem["formsetinit_id"],self::$StateSendback,array(self::$StateWait),$this->ComId,$curr_workitem["id"]);
		
		$callback_workitem = g("mv_workinst")->get_item_by_ids(array($data["callback_workitem_id"]),$this->ComId);
		if(!is_array($callback_workitem)){throw new SCException('退回步骤不存在，请联系管理员');}
		$callback_workitem = $callback_workitem[0];
		
		$handle_id = $callback_workitem["handler_id"];
		$create_id = $curr_workitem["creater_id"];
		$receiver_id = $this->UserId;
		$handle_m_dept = g("api_user")->get_main_dept($handle_id,FALSE);
		if($handle_m_dept){
			$handle_dept_id = $handle_m_dept["id"];
			$handle_dept_name = $handle_m_dept["name"];
		}
		else{
			$handle_dept_id = -1;
			$handle_dept_name = "无部门";
		}
		$create_m_dept = g("api_user")->get_main_dept($create_id,FALSE);
		if($create_m_dept){
			$create_dept_id = $create_m_dept["id"];
			$create_dept_name = $create_m_dept["name"];
		}
		else{
			$create_dept_id = -1;
			$create_dept_name = "无部门";
		}
		$receiver_m_dept = g("api_user")->get_main_dept($receiver_id,FALSE);
		if($receiver_m_dept){
			$receiver_dept_id = $receiver_m_dept["id"];
			$receiver_dept_name = $receiver_m_dept["name"];
		}
		else{
			$receiver_dept_id = -1;
			$receiver_dept_name = "无部门";
		}
		
		//保存工作项信息
		$work_item = array(
			'formsetinit_id' => $curr_workitem["formsetinit_id"],
			'workitem_name' => $callback_workitem["workitem_name"],
			'com_id' => $this->ComId,
			'handler_id' => $callback_workitem["handler_id"],
			'handler_name' => $callback_workitem["handler_name"],
			'handler_dept_id' =>$handle_dept_id,
			'handler_dept_name' =>$handle_dept_name,
			'state' => self::$StateNew,
			'creater_id' => $curr_workitem["creater_id"],
			'creater_name' => $curr_workitem["creater_name"],
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name,
			'receiver_id' => $this->UserId,
			'receiver_name' => $this->UserName,
			'receiver_dept_id' =>$receiver_dept_id,
			'receiver_dept_name' =>$receiver_dept_name,
			'work_node_id' =>$callback_workitem["work_node_id"],
			'pre_work_node_id' => $callback_workitem["pre_work_node_id"],
			'is_returnback' =>self::$Callback,
			'returnback_node_id' =>$curr_workitem["id"],
		);
		$workitem_id = g("mv_workinst")->save_workitem($work_item);			
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["judgement"] = $data["judgement"];
		$curr_workitem["target_node"] = "退回".$callback_workitem["workitem_name"];
		g("mv_workinst")->callback_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
		//更新流程流转
		$formsetInst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		$workitem_rule = $formsetInst["workitem_rule"];
		$workitem_rule.="@".$callback_workitem["work_node_id"];
		g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$callback_workitem["work_node_id"]);
		
		//消息提醒
		$user_id = $callback_workitem["handler_id"];
		$user = g('api_user') -> get_by_id($user_id);
		$msg_user = array($user['acct']);
		$msg_title=$this->AppCName."退回提醒";
		$msg_desc = $this->AppCName."："."您收到了《".$formsetInst["formsetinst_name"]."》退回事项，请及时处理。";
		$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
			
		try{
			//发送PC提醒
			$data = array(
				'url' => $pc_url,
				'title' => $msg_desc,
				'app_name' => $this->AppName
			);
			g("messager")->publish_by_user('pc', $user_id,$data);
			//发送微信提醒
			parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
		}catch(SCException $e){
			log_write("fail_user ".$user["name"]);
		}
		$ret = array();
		$ret["workitem_name"] = $callback_workitem["workitem_name"];
		$ret["receiver_name"] = $user["name"];
		return $ret;
	}
	
	
	/**
	 * 驳回开始步骤流程
	 */
	public function callback_start_parent($data){
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		
		//将同级工作项状态转成退回状态
		g("mv_workinst")->update_node_state_item_nwi($curr_workitem["formsetinit_id"],$curr_workitem["id"],$curr_workitem["work_node_id"],self::$StateSendback,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$curr_workitem["id"]);
		
		//将等待中的工作项状态转成退回状态
		g("mv_workinst")->update_formset_state_item($curr_workitem["formsetinit_id"],self::$StateSendback,array(self::$StateWait),$this->ComId,$curr_workitem["id"]);
		
		$return_name="开始";
		//获取开始步骤的节点id
		$start_work_node =$this->get_start_node($curr_workitem["work_id"]);
		
		$handle_id = $curr_workitem["creater_id"];
		$create_id = $curr_workitem["creater_id"];
		$receiver_id = $this->UserId;
		$handle_m_dept = g("api_user")->get_main_dept($handle_id,FALSE);
		if($handle_m_dept){
			$handle_dept_id = $handle_m_dept["id"];
			$handle_dept_name = $handle_m_dept["name"];
		}
		else{
			$handle_dept_id = -1;
			$handle_dept_name = "无部门";
		}
		$create_m_dept = g("api_user")->get_main_dept($create_id,FALSE);
		if($create_m_dept){
			$create_dept_id = $create_m_dept["id"];
			$create_dept_name = $create_m_dept["name"];
		}
		else{
			$create_dept_id = -1;
			$create_dept_name = "无部门";
		}
		$receiver_m_dept = g("api_user")->get_main_dept($receiver_id,FALSE);
		if($receiver_m_dept){
			$receiver_dept_id = $receiver_m_dept["id"];
			$receiver_dept_name = $receiver_m_dept["name"];
		}
		else{
			$receiver_dept_id = -1;
			$receiver_dept_name = "无部门";
		}
		
		//保存工作项信息
		$work_item = array(
			'formsetinit_id' => $curr_workitem["formsetinit_id"],
			'workitem_name' => $start_work_node["work_node_name"],
			'com_id' => $this->ComId,
			'handler_id' => $curr_workitem["creater_id"],
			'handler_name' => $curr_workitem["creater_name"],
			'handler_dept_id' =>$handle_dept_id,
			'handler_dept_name' =>$handle_dept_name,
			'state' => self::$StateNew,
			'creater_id' => $curr_workitem["creater_id"],
			'creater_name' => $curr_workitem["creater_name"],
			'creater_dept_id' =>$create_dept_id,
			'creater_dept_name' =>$create_dept_name,
			'receiver_id' => $this->UserId,
			'receiver_name' => $this->UserName,
			'receiver_dept_id' =>$receiver_dept_id,
			'receiver_dept_name' =>$receiver_dept_name,
			'work_node_id' =>$start_work_node["id"],
			'is_returnback' =>self::$Callback,
			'returnback_node_id' =>$curr_workitem["id"],
		);
		$workitem_id = g("mv_workinst")->save_workitem($work_item);			
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["judgement"] = $data["judgement"];
		$curr_workitem["target_node"] = "退回".$return_name;
		g("mv_workinst")->callback_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
		
		//更新流程流转
		$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
		
		$workitem_rule = $formsetinst["workitem_rule"];
		$workitem_rule.="@".$start_work_node["id"];
		g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$start_work_node["id"]);
		
		//消息提醒
		$user_id = $curr_workitem["creater_id"];
		$user = g('api_user') -> get_by_id($user_id);
		$msg_user = array($user['acct']);
		$msg_title=$this->AppCName."退回提醒";
		$msg_desc = $this->AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》退回事项，请及时处理。";
		$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		
		try{
			//发送PC提醒
			$data = array(
				'url' => $pc_url,
				'title' => $msg_desc,
				'app_name' => $this->AppName
			);
			g("messager")->publish_by_user('pc', $user_id,$data);
			//发送微信提醒
			parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
		}catch(SCException $e){
			log_write("fail_user ".$user["name"]);
		}
		
		$ret = array();
		$ret["receiver_name"] = $user["name"];
		return $ret;
	}
	
	/**
	 * 退回上一步骤
	 */
	public function callback_pre_parent($data){

		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($data["workitem_id"]);
		$pre_work_node_id = json_decode($curr_workitem["pre_work_node_id"],TRUE);
		
		$errcode=0;
		$errmsg="";
		if(count($pre_work_node_id)==1){//如果只有一个节点，则直接发送
			//将同级工作项处理中状态转成退回状态
			g("mv_workinst")->update_node_state_item_nwi($curr_workitem["formsetinit_id"],$curr_workitem["id"],$curr_workitem["work_node_id"],self::$StateSendback,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$curr_workitem["id"]);
			//将等待中的工作项状态转成退回状态
			g("mv_workinst")->update_formset_state_item($curr_workitem["formsetinit_id"],self::$StateSendback,array(self::$StateWait),$this->ComId,$curr_workitem["id"]);
			$callback_workitemid = $pre_work_node_id[0];
			$callback_workitem = g("mv_workinst")->get_item_by_ids(array($callback_workitemid),$this->ComId);
			if(!is_array($callback_workitem)){throw new SCException('查询工作项失败');}
			$callback_workitem = $callback_workitem[0];
			
			$handle_id = $callback_workitem["handler_id"];
			$create_id = $callback_workitem["creater_id"];
			$receiver_id = $this->UserId;
			$handle_m_dept = g("api_user")->get_main_dept($handle_id,FALSE);
			if($handle_m_dept){
				$handle_dept_id = $handle_m_dept["id"];
				$handle_dept_name = $handle_m_dept["name"];
			}
			else{
				$handle_dept_id = -1;
				$handle_dept_name = "无部门";
			}
			$create_m_dept = g("api_user")->get_main_dept($create_id,FALSE);
			if($create_m_dept){
				$create_dept_id = $create_m_dept["id"];
				$create_dept_name = $create_m_dept["name"];
			}
			else{
				$create_dept_id = -1;
				$create_dept_name = "无部门";
			}
			$receiver_m_dept = g("api_user")->get_main_dept($receiver_id,FALSE);
			if($receiver_m_dept){
				$receiver_dept_id = $receiver_m_dept["id"];
				$receiver_dept_name = $receiver_m_dept["name"];
			}
			else{
				$receiver_dept_id = -1;
				$receiver_dept_name = "无部门";
			}
			
			//保存工作项信息
			$work_item = array(
				'formsetinit_id' => $curr_workitem["formsetinit_id"],
				'workitem_name' => $callback_workitem["workitem_name"],
				'com_id' => $this->ComId,
				'handler_id' => $callback_workitem["handler_id"],
				'handler_name' => $callback_workitem["handler_name"],
				'handler_dept_id' =>$handle_dept_id,
				'handler_dept_name' =>$handle_dept_name,
				'state' => self::$StateNew,
				'creater_id' => $callback_workitem["creater_id"],
				'creater_name' => $callback_workitem["creater_name"],
				'creater_dept_id' =>$create_dept_id,
				'creater_dept_name' =>$create_dept_name,
				'receiver_id' => $this->UserId,
				'receiver_name' => $this->UserName,
				'receiver_dept_id' =>$receiver_dept_id,
				'receiver_dept_name' =>$receiver_dept_name,
				'work_node_id' =>$callback_workitem["work_node_id"],
				'pre_work_node_id'=>$callback_workitem["pre_work_node_id"],
				'is_returnback' =>self::$Callback,
				'returnback_node_id' =>$curr_workitem["id"],
			);
			$workitem_id = g("mv_workinst")->save_workitem($work_item);
			
			//更新当前事项的状态
			$curr_workitem["complete_time"] = time();
			if($curr_workitem["receive_time"]!=0){
				$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
			}
			$curr_workitem["judgement"] = $data["judgement"];
			$curr_workitem["target_node"] = "退回".$callback_workitem["workitem_name"];
			g("mv_workinst")->callback_workitem($data["workitem_id"],$this->ComId,$curr_workitem);
			$errmsg = '流程退回成功，退回步骤为 '.$callback_workitem["workitem_name"].' 步骤，接收人为'.$callback_workitem["handler_name"];
			
			//更新流程流转
			$formsetinst = $this->get_formsetinst_by_id($curr_workitem["formsetinit_id"]);
			$workitem_rule = $formsetinst["workitem_rule"];
			$workitem_rule.="@".$callback_workitem["work_node_id"];
			g("mv_formsetinst")->update_formsetinst_workitemrule($curr_workitem["formsetinit_id"],$this->ComId,$workitem_rule,$callback_workitem["work_node_id"]);
			
			//消息提醒
			$user_id = $callback_workitem["handler_id"];
			$user = g('api_user') -> get_by_id($user_id);
			$msg_user = array($user['acct']);
			$msg_title=$this->AppCName."退回提醒";
			$msg_desc = $this->AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》退回事项，请及时处理。";
			$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
			$pc_menu = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_list&a=get_do_list';//TODO
			
			$fail_user="";
			try{
				//发送PC提醒
				$data = array(
					'url' => $pc_url,
					'title' => $msg_desc,
					'app_name' => $this->AppName
				);
				g("messager")->publish_by_user('pc', $user_id,$data);
				//发送微信提醒
				parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
			}catch(SCException $e){
				log_write("fail_user ".$user["name"]);
			}
		}
		else{//弹出退回的人
			$callback_workitem = g("mv_workinst")->get_item_by_ids($pre_work_node_id,$this->ComId);
			$errcode = 1;
			$errmsg = '选择流程退回步骤';
		}
		
		$ret = array();
		$ret["errcode"] = $errcode;
		$ret["errmsg"] = $errmsg;
		$ret["callback_workitem"] = $callback_workitem;
		return $ret;
	}
	
	
	/**
	 * 保存流程new
	 * @param array $data
	 */
	public function save_workitem_parent($data){
		if(empty($data["formsetInst_id"])){//如果为空，是新发起流程，需要增加forsetInst数据
			$is_other_proc = empty($data["is_other_proc"])?0:$data["is_other_proc"];
			$create_name = $this->UserName;
			$formsetinst_name = $data["form_name"];
			$create_m_dept = g("api_user")->get_main_dept($this->UserId,FALSE);
			if($create_m_dept){
				$creater_dept_id = $create_m_dept["id"];
				$creater_dept_name = $create_m_dept["name"];
			}
			else{
				$creater_dept_id = -1;
				$creater_dept_name = "无部门";
			}
			
			$formsetinst_id = g("mv_formsetinst")->save_formsetinst($data["form_id"],$data["work_id"],$formsetinst_name,$this->UserId,$create_name,$this->ComId,$data["form_vals"],$is_other_proc,$creater_dept_id,$creater_dept_name);
			
			//获取开始步骤的节点id
			$start_work_node =$this->get_start_node($data["work_id"]);
			
			//保存工作项信息
			$work_item = array(
				'formsetinit_id' => $formsetinst_id,
				'workitem_name' => "开始",
				'com_id' => $this->ComId,
				'handler_id' => $this->UserId,
				'handler_name' => $create_name,
				'handler_dept_id' =>$creater_dept_id,
				'handler_dept_name' =>$creater_dept_name,
				'state' => self::$StateNew,
				'creater_id' => $this->UserId,
				'creater_name' => $create_name,
				'creater_dept_id' =>$creater_dept_id,
				'creater_dept_name' =>$creater_dept_name,
				'work_node_id' =>$start_work_node["id"],
				'is_returnback' =>self::$Not_Callback,
				'returnback_node_id' =>0,
			);
			$workitem_id = g("mv_workinst")->save_workitem($work_item);
			//更新流程规则
			g("mv_formsetinst")->update_formsetinst_workitemrule($formsetinst_id,$this->ComId,$start_work_node["id"],$start_work_node["id"]);
			g("mv_file")->add_file($data["files"],$this->ComId,$this->UserId,$create_name,$formsetinst_id,$start_work_node["id"],$this->IsProcType);
			
			//通用流程添加常用记录
			if($this->IsProcType == self::$IsProc){
				$form_model = g('mv_form') -> get_by_id($data["form_id"],$this->ComId);
				$is_exist = g("mv_form_user")->is_exist_user_form($form_model["form_history_id"],$this->UserId,$this->ComId);
				if(!$is_exist){
					g("mv_form_user")->add_common_form($form_model["id"],$form_model["form_history_id"],$this->UserId,$this->ComId);
				}
				else{
					g("mv_form_user")->update_common_form_time($form_model["form_history_id"],$this->UserId,$this->ComId);
				}
			}
		}
		else{//有formsetinst，则为正在跑的流程
			if(!empty($data["workitem_id"])){
				$formsetinst_id = $data["formsetInst_id"];
				//更新表单实例
				g("mv_formsetinst")->update_formsetinst($data["formsetInst_id"],$this->ComId,$data["form_vals"]);
				//更新工作项
				g("mv_workinst")->update_workitem($data["workitem_id"],$this->ComId,$data["judgement"]);
				$workitem_id = $data["workitem_id"];
				$cur_workitem = $this->get_curr_workitem($workitem_id);
				$work_node_id = $cur_workitem["work_node_id"];
			}
			else{
				throw new SCException('数据丢失，请联系管理员');//这种情况不应该存在
			}
			g("mv_file")->del_file_by_node_id($this->ComId,$this->UserId,$work_node_id,$data["formsetInst_id"]);
			if(!empty($data["files"])){
				g("mv_file")->add_file($data["files"],$this->ComId,$this->UserId,$this->UserName,$data["formsetInst_id"],$work_node_id,$this->IsProcType);
			}
		}
		
		$ret = array();
		$ret["workitem_id"] = $workitem_id;
		$ret["formsetInst_id"] = $formsetinst_id;
		
		return $ret;
	}
	
	/**
	 * 发送流程new
	 * @param array $data
	 */
	public function send_workitem_parent($data){
		$workitem_id = $data["workitemid"];
		$work_nodes = $data["work_nodes"];
		
		if(empty($work_nodes)||count($work_nodes)==0){throw new SCException('非法请求');}
		
		//获取当前步骤的信息
		$curr_workitem = $this->get_curr_workitem($workitem_id);
		if($curr_workitem["state"]!=self::$StateNew&&$curr_workitem["state"]!=self::$StateRead&&$curr_workitem["state"]!=self::$StateSave){
			throw new SCException('该工作项已经处理，请返回代办列表');
		}
		$work_node_id = $curr_workitem["work_node_id"];
		$formsetinst_id = $curr_workitem["formsetinit_id"];
		
		//流转限制条件（全部审批到达 2 一个审批到达 1）
		$circulation_limit_rule = $curr_workitem["circulation_limit_rule"];
		$same_level_workitems =  g("mv_workinst")->get_node_state_item_nwi($formsetinst_id,$workitem_id,$work_node_id,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId);
		if(is_array($same_level_workitems)){//如果存在同级工作项
			if($circulation_limit_rule ==2){//一个审批到达 2
				//把未完成的事项更新成已通过
				g("mv_workinst")->update_node_state_item_nwi($formsetinst_id,$curr_workitem["id"],$work_node_id,self::$StateDone,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId,$this->ComId);
				$return_arr = $this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,TRUE);
				$this->notify_process($curr_workitem,$formsetinst_id,$work_nodes);
			}
			else{//全部审批到达
				$return_arr =$this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,FALSE);
			}
		}
		else{
			$return_arr =$this->save_new_workitem($work_nodes, $formsetinst_id, $curr_workitem,TRUE);
			$this->notify_process($curr_workitem,$formsetinst_id,$work_nodes);
		}
		
		
		//更新当前事项的状态
		$curr_workitem["complete_time"] = time();
		if($curr_workitem["receive_time"]!=0){
			$curr_workitem["consuming_time"] = time()-$curr_workitem["receive_time"];
		}
		$curr_workitem["target_node"] = "送".$return_arr[1];
		g("mv_workinst")->send_workitem($workitem_id,$this->ComId,$curr_workitem);
		
		$ret = array();
		$ret["return_arr"] = $return_arr;
		
		return $ret;
	}
	
	
	/**
	 * 判断工作项的状态是否更变
	 * @param int $workitem_id
	 */
	
	public function is_finish($workitem_id){
		$is_finish = false;
		
		$cur_workitem = $this->get_curr_workitem($workitem_id);
		
		if($cur_workitem["state"]==self::$StateSend||$cur_workitem["state"]==self::$StateCallback||$cur_workitem["state"]==self::$StateDone){
			$is_finish=true;
		}
		return $is_finish;
	}
	
	/**
	 * 根据格式的数字字符串获取实例名new
	 * @param int $num_format
	 */
	public function get_formset_by_num($form_name,&$is_special){
		$formset_format = g('mv_formsetinst_name')->get_recode_by_id($this->ComId,$this->IsProcType);
		$num_format = "6;2;5";
		if(!empty($formset_format)){
			$num_format = $formset_format["format"];
		}
		if(strstr($num_format,self::$Custom."")){
			$is_special = 1;
		}
		
		$formset_name = array();
		$num_format = explode(";",$num_format);
		foreach ($num_format as $n_format){
			$temp_str = "";
			if($n_format==self::$ApplyerDept){
				$create_m_dept = g("api_user")->get_main_dept($this->UserId,FALSE);
				if(empty($create_m_dept)){
					$depts = g("api_user")->list_all_dept($this->UserId,FALSE,FALSE);
					$formset_name[$n_format] = $depts[0]["name"];
				}
				else{
					$formset_name[$n_format] = $create_m_dept["name"];
				}
			}
			else if($n_format == self::$ApplyerName){
				$temp_str = $this->UserName;
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateM){
				$temp_str = date("Ym",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateD){
				$temp_str = date("Ymd",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$ApplyDateMin){
				$temp_str = date("YmdHi",time());
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$FormName){
				$temp_str = $form_name;
				$formset_name[$n_format] = $temp_str;
			}
			else if($n_format == self::$Custom){
				$temp_str = "<span class='css_forsetinst_input'>&nbsp;&nbsp;&nbsp;&nbsp;?&nbsp;&nbsp;&nbsp;&nbsp;</span>";
				$formset_name[$n_format] = $temp_str;
			}
		}
		return $formset_name;
	}
	
	
	/**
	 * 根据实例id获取事项信息new
	 * @param int $formsetinit_id
	 */
	public function get_workitem_list_parent($formsetinit_id){
		$state_f = 0;
		$state_b = 1;
		$state_d = 2;
		$state_w = 3;
		$res_list = array();
		$list = g("mv_workinst")->get_workitem_list($formsetinit_id,$this->ComId);
		if(!empty($list)){
			$t_node =  $list[0];
			$temp_state_list = array();
			$temp_state_list[] = $t_node["state"];
			$temp_item_list = array();
			$temp_item_list[] = $t_node;
			$temp_item_name = $t_node["workitem_name"];
			for ($i=1;$i<count($list);$i++){
				$c_node = $list[$i];
				if($c_node["work_node_id"]==$t_node["work_node_id"]){
					if($t_node["state"]==self::$StateNew||$t_node["state"]==self::$StateRead||$t_node["state"]==self::$StateSave||$t_node["state"]==self::$StateWait){
						$temp_item_list[] = $c_node;
						$temp_state_list[] = $c_node["state"];
					}
					else{
						if($c_node["handler_id"]!=$t_node["handler_id"]){
							$temp_item_list[] = $c_node;
							$temp_state_list[] = $c_node["state"];
						}
					}
				}
				else{
					$temp_item_arr = array();
					$temp_state = $state_f;
					if(in_array(self::$StateCallback, $temp_state_list)){
						$temp_state = $state_b;
					}
					else if(in_array(self::$StateNew, $temp_state_list)||in_array(self::$StateRead, $temp_state_list)||in_array(self::$StateSave, $temp_state_list)){
						$temp_state = $state_d;
					}
					else if(in_array(self::$StateWait, $temp_state_list)){
						$temp_state = $state_w;
					}
					$temp_item_arr["workitem_name"] = $temp_item_name;
					$temp_item_arr["state"] = $temp_state;
					$temp_item_arr["workitem_list"] = $temp_item_list;
					$res_list[] = $temp_item_arr;
					$t_node = $c_node;
					$temp_state_list = array();
					$temp_state_list[] = $c_node["state"];
					$temp_item_list = array();
					$temp_item_list[] = $c_node;
					$temp_item_name = $c_node["workitem_name"];
				}
			}
			$temp_item_arr = array();
			$temp_state = $state_f;
			if(in_array(self::$StateCallback, $temp_state_list)){
				$temp_state = $state_b;
			}
			else if(in_array(self::$StateNew, $temp_state_list)||in_array(self::$StateRead, $temp_state_list)||in_array(self::$StateSave, $temp_state_list)){
				$temp_state = $state_d;
			}
			else if(in_array(self::$StateWait, $temp_state_list)){
				$temp_state = $state_w;
			}
			$temp_item_arr["workitem_name"] = $temp_item_name;
			$temp_item_arr["state"] = $temp_state;
			$temp_item_arr["workitem_list"] = $temp_item_list;
			$res_list[] = $temp_item_arr;
		}
		return $res_list;
	}
	
	public function get_linksign_state(){
		$linksign = g('mv_linksign')->select_linksign($this->ComId,$this->IsProcType);
		return $linksign;
	}
	
	//--------------------------------------内部实现--------------------------------
	
	
	/**
	 * 获取接收人规则 new
	 * @param array $next_node_ids节点id数组
	 * @param array $next_node_id_arr 结果集
	 * @param array $workitem 工作项对象
	 * @param array $formsetinst 实例对象
	 * @param int $com_id 企业id
	 * @throws SCException
	 */
	private function get_receiver_rule($next_node_ids,&$next_node_id_arr,$workitem,$formsetinst,$com_id){
		if(count($next_node_ids)==0){
			throw new SCException('找不到下一步的流程节点');
		}
		else{
			foreach ($next_node_ids as $next_node_id){
				$next_work = g("mv_work_node")->get_by_id($com_id,$next_node_id);
				if(is_array($next_work)){
					$receive_array = array();
					$receive_rules = $next_work["receive_rule"];//下一步接收人规则
					
					//结束节点标示
					$is_end = FALSE;
					if(empty($next_work["next_node_id"])){//结束节点
						$is_end = TRUE;
					}
					
					if(empty($receive_rules)){//如果接收人规则为空，则显示选人按钮，使用地址本选择人
						//这里可以添加选择地址本的参数，用地址本的参数控制人员选择范围
					}
					else{
						$receive_rule_arr = explode(",",$receive_rules);
						
						foreach ($receive_rule_arr as $receive_rule){
							if($receive_rule==self::$RecriverFixed){
								$receive_user = $next_work["receive_user"];
								$user_ids = array();
								if(!empty($receive_user)){
									$user_ids = explode(",",$receive_user);
								}
								$receive_group = $next_work["receive_group"];
								if(!empty($receive_group)){
									$group_ids = explode(",",$receive_group);
									$group_user_ids = g("api_group")->get_user_by_ids($com_id,$group_ids);
									$tmp_user_ids = array_column($group_user_ids, 'id');
									$user_ids = array_merge($user_ids, $tmp_user_ids);
								}
								
								$users = g("api_user")->list_by_ids($user_ids,'*',FALSE);
								if(is_array($users)){//如果用户不存在，则使用地址本
									foreach ($users as $user){
										$this->set_receiver($receive_array, $user);
									}
									unset($user);
								}
							}
							if($receive_rule==self::$RecriverCreater){
								$user_id = $workitem["creater_id"];
								$this->get_user_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverCreaterLeader){
								$user_id = $workitem["creater_id"];
								$this->get_user_leader_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverHandler){
								$user_id = $workitem["handler_id"];
								$this->get_user_receiver_list($receive_array, $user_id);
							}
							else if($receive_rule==self::$RecriverHandlerLeader){
								$user_id = $workitem["handler_id"];
								$this->get_user_leader_receiver_list($receive_array, $user_id);
							}
						}
						unset($receive_rule);
					}
					
					$select_array = array();
					$select_rules = $next_work["select_rule"];//下一步选择人员规则
					$select_group = $next_work["select_group"];//下一步选择组别规则
					
					$select_users = array();
					if(!empty($select_rules)){
						$select_users = explode(",",$select_rules);
					}
					
					if(!empty($select_group)){
						$select_group = explode(",",$select_group);
						$group_user_ids = g("dao_group")->get_user_by_ids($com_id,$select_group);
						$tmp_user_ids = array_column($group_user_ids, 'id');
						$select_users = array_merge($select_users, $tmp_user_ids);
					}
					
					$select_users = g("api_user")->list_by_ids($select_users,'*',FALSE);
					
					$temp_users = array();
					//将固定接收人和选择人员重复者去掉
					foreach ($select_users as $user){
						$is_receiver = false;
						foreach ($receive_array as $receiver){
							if($user["id"]==$receiver["id"]){
								$is_receiver = true;
								break;
							}
						}
						if(!$is_receiver){
							$temp_users[]=$user;
						}
					}
					unset($user);
					$select_array = $temp_users;
					
					$next_work["is_end"] = $is_end;
					$next_work["select_array"] = $select_array;
					$next_work["receive_array"] = $receive_array;
					array_push($next_node_id_arr, $next_work);
				}
			}
		}
	}
	
	
	
	/**
	 * 流转规则规则判断，返回TRUE FALSE
	 * @param array $rule_arr
	 * @param array $formsetinst_val
	 * @param int $work_id
	 * @param int $work_result_id
	 * @param int $workitem
	 * @param int $next_node_ids
	 */
	private function is_circulation_rule(&$rule_arr,$formsetinst_val,$work_id,&$work_result_id,$workitem,&$next_node_ids){
		if(count($rule_arr)==0){
			throw new SCException('流转规则保存有误，请联系管理员！流程节点ID为:'.$work_id);
		}
		$is_true=FALSE;
		
		foreach ($rule_arr as $key=>$rule){
			$rule = explode("#", $rule);
			$input_key = $rule[0];
			$symbol = $rule[1];
			$compare_val = $rule[2];//对于人员控件和部门控件新的应该有ids这个参数
			$compare_ids = "";
			if(stripos($compare_val,"&")){
				$compare_val = explode("&", $compare_val);
				$compare_ids = $compare_val[1];
				$compare_val = $compare_val[0];
			}
			$input_val = "";
			$input_ids = "";
			$input_type = "";
			if($symbol!=self::$People_Position){
				foreach ($formsetinst_val as $form_val){//获取表单某个控件的值
					if($form_val["input_key"]==$input_key){
						if(!isset($form_val["val"])){
							$form_val["val"]="";
						}
						$input_val = $form_val["val"];
						$input_type = $form_val["type"];
						if(isset($form_val["ids"])){$input_ids=$form_val["ids"];}
						break;
					}
				}
			}
			if($symbol==self::$Compare_Equeal){
				if($input_type=="checkbox"){
					$input_val = str_replace(";",",",$input_val);
				}
				
				if($input_val==$compare_val||($input_val==""&&$compare_val=="null")){
					if(!empty($compare_ids)&&!empty($input_ids)){//人员部门控件比较
						if($compare_ids==$input_ids){
							$is_true = TRUE;
						}
						else{
							$is_true = FALSE;
							break;
						}
					}
					else{
						$is_true = TRUE;
					}
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Not_Equeal){
				if($input_type=="checkbox"){
					$input_val = str_replace(";",",",$input_val);
				}
				if(($input_val!=""&&$input_val!=$compare_val)||($input_val!=""&&$compare_val=="null")){
					if(!empty($compare_ids)&&!empty($input_ids)){//人员部门控件比较
						if($compare_ids!=$input_ids){
							$is_true = TRUE;
						}
						else{
							$is_true = FALSE;
							break;
						}
					}
					else{
						$is_true = TRUE;
					}
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Greater){
				if($input_val>$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Greater_Equeal){
				if($input_val>=$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Lesser){
				if($input_val<$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Lesser_Equeal){
				if($input_val<=$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$People_Position){//职位规则
				$user_id="";
				if($input_key==self::$People_Type_Creater){//发起人
					$user_id = $workitem["creater_id"];
				}
				else if($input_key==self::$People_Type_Done){//当前处理人
					$user_id =$workitem["handler_id"]; 
				}
				$user_id = intval($user_id);
				$user = g('user') -> get_by_id($user_id);
				
				if($user["position"]==$compare_val){
					$is_true = TRUE;
				}
				else{
					$is_true = FALSE;
					break;
				}
			}
			else if($symbol==self::$Compare_Contain){
				if($input_type=="dept"||$input_type=="people"){//人员部门控件是用逗号隔开的
					$tmp_input_val = explode(",", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					if(!empty($compare_ids)&&!empty($input_ids)){
						$tmp_compare_ids = explode(",", $compare_ids);
						$tmp_input_ids = explode(",", $input_ids);
						foreach ($tmp_input_ids as $tmp_id){
							if(in_array($tmp_id, $tmp_compare_ids)){
								$is_contain = TRUE;
								break;
							}
						}
						unset($tmp_id);
					}
					else{
						foreach ($tmp_input_val as $tmp_val){
							if(in_array($tmp_val, $tmp_compare_val)){
								$is_contain = TRUE;
								break;
							}
						}
						unset($tmp_val);
					}
					if($is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
				else{
					$tmp_input_val = explode(";", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					foreach ($tmp_input_val as $tmp_val){
						if(in_array($tmp_val, $tmp_compare_val)){
							$is_contain = TRUE;
							break;
						}
					}
					unset($tmp_val);
					if($is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
			}
			else if($symbol==self::$Compare_Not_Contain){
				if($input_type=="dept"||$input_type=="people"){//人员部门控件是用逗号隔开的
					$tmp_input_val = explode(",", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					if(!empty($compare_ids)&&!empty($input_ids)){
						$tmp_compare_ids = explode(",", $compare_ids);
						$tmp_input_ids = explode(",", $input_ids);
						foreach ($tmp_input_ids as $tmp_id){
							if(in_array($tmp_id, $tmp_compare_ids)){
								$is_contain = TRUE;
							}
						}
						unset($tmp_id);
					}
					else{
						foreach ($tmp_input_val as $tmp_val){
							if(in_array($tmp_val, $tmp_compare_val)){
								$is_contain = TRUE;
							}
						}
						unset($tmp_val);
					}
					if(!$is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
				else{
					$tmp_input_val = explode(";", $input_val);
					$tmp_compare_val = explode(",", $compare_val);
					$is_contain = FALSE;
					foreach ($tmp_input_val as $tmp_val){
						if(in_array($tmp_val, $tmp_compare_val)){
							$is_contain = TRUE;
						}
					}
					unset($tmp_val);
					if(!$is_contain){
						$is_true = TRUE;
					}
					else{
						$is_true = FALSE;
						break;
					}
				}
			}
			
		}
		unset($rule);
		foreach( $next_node_ids as $key=>$value) {//删除存在表单流转规则的节点
	   		if($work_id == $value) unset($next_node_ids[$key]);
		}
		unset($value);
		if($is_true==TRUE){
			$work_result_id[] = $work_id;
		}
		
	}
	
	
	/**
	 * 新增下一步的工作项new
	 * @param array $work_nodes
	 * @param int $formsetinst_id
	 * @param array $curr_workitem 当前工作项对象
	 */
	private function save_new_workitem($work_nodes,$formsetinst_id,$curr_workitem,$is_last){
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
		$workitem_rule = $formsetinst["workitem_rule"];
		//如果流转规则存在@,截取去掉@后面的字符串
		if(strripos($workitem_rule,"@")>-1){
			$workitem_rule_arr = explode("@",$workitem_rule);
			$workitem_rule = $workitem_rule_arr[0];
			$workitem_rule_last = $workitem_rule_arr[count($workitem_rule_arr)-1];
			$workitem_rule = substr($workitem_rule,0,strripos($workitem_rule, $workitem_rule_last)+strlen($workitem_rule_last));
		}
		
		//返回的步骤名 和接收人名称
		$return_arr = array();
		$msg_send_arr = array();
		$receiver_str = array();
		$curr_workitem_id = $curr_workitem["id"];
		$work_node = $work_nodes[0];
		$work_node_id = $work_node['id'];
		$receiver_arr = $work_node['receiver_arr'];
		$work_node = g("mv_work_node")->get_by_id($this->ComId,$work_node_id);
		if(!is_array($work_node)){
			throw new SCException("流程发送失败，找不到该节点信息，节点id为".$work_node_id);
		}
		$work_node_name = $work_node["work_node_name"];
		$is_finish = "FALSE";//
		if(!empty($work_node["next_node_id"])){//不是结束步骤
			if($is_last){
				//流转规则字段更新
				$workitem_rule.=",".$work_node_id;
				$formsetinst["curr_node_id"] = $work_node_id;
				//获取当前节点等待中的事项
				$waiting_item = g("mv_workinst")->get_node_state_item($formsetinst_id,$work_node["id"],array(self::$StateWait),$this->ComId);
				if(is_array($waiting_item)){
					//把本来等待中状态的事项更新成新收到
					g("mv_workinst")->update_state_workitem($this->ComId,$formsetinst_id,$work_node["id"],self::$StateWait,self::$StateNew);
					//消息提醒数组插入
					foreach ($waiting_item as $item){
						$msg_send = array();
						$msg_send["receiver_id"] = $item["handler_id"];
						$msg_send["workitem_id"] = $item["id"];
						$msg_send_arr[] =$msg_send;
					}
					unset($item);
				}
			}
			foreach ($receiver_arr as $receiver){
				$receiver_str[]=$receiver["receiver_name"];
				//判断下一节点是否存在同一个处理人
				$is_same_handler = g("mv_workinst")->is_same_handlers($formsetinst_id,$work_node["id"],$receiver["receiver_id"],$this->ComId);
				
				if(!$is_same_handler){//不存在，直接新建
					$pre_work_node_id = array();
					$pre_work_node_id[] = $curr_workitem["id"];
					
					$handle_id = $receiver["receiver_id"];
					$create_id = $curr_workitem["creater_id"];
					$receiver_id = $curr_workitem["handler_id"];
					$handle_m_dept = g("api_user")->get_main_dept($handle_id,FALSE);
					if($handle_m_dept){
						$handle_dept_id = $handle_m_dept["id"];
						$handle_dept_name = $handle_m_dept["name"];
					}
					else{
						$handle_dept_id = -1;
						$handle_dept_name = "无部门";
					}
					$create_m_dept = g("api_user")->get_main_dept($create_id,FALSE);
					if($create_m_dept){
						$create_dept_id = $create_m_dept["id"];
						$create_dept_name = $create_m_dept["name"];
					}
					else{
						$create_dept_id = -1;
						$create_dept_name = "无部门";
					}
					$receiver_m_dept = g("api_user")->get_main_dept($receiver_id,FALSE);
					if($receiver_m_dept){
						$receiver_dept_id = $receiver_m_dept["id"];
						$receiver_dept_name = $receiver_m_dept["name"];
					}
					else{
						$receiver_dept_id = -1;
						$receiver_dept_name = "无部门";
					}
					
					$new_work_item = array(
						'formsetinit_id' => $formsetinst_id,
						'workitem_name' => $work_node["work_node_name"],
						'com_id' => $this->ComId,
						'handler_id' => $receiver["receiver_id"],
						'handler_name' => $receiver["receiver_name"],
						'handler_dept_id' =>$handle_dept_id,
						'handler_dept_name' =>$handle_dept_name,
						'receiver_id' => $curr_workitem["handler_id"],
						'receiver_name' => $curr_workitem["handler_name"],
						'receiver_dept_id' =>$receiver_dept_id,
						'receiver_dept_name' =>$receiver_dept_name,
						'creater_id' => $curr_workitem["creater_id"],
						'creater_name' => $curr_workitem["creater_name"],
						'creater_dept_id' =>$create_dept_id,
						'creater_dept_name' =>$create_dept_name,
						'work_node_id' =>$work_node["id"],
						'pre_work_node_id'=>json_encode($pre_work_node_id),
						'is_returnback' =>self::$Not_Callback,
						'returnback_node_id' =>0,
					);
					if($is_last){
						$new_work_item["state"] = self::$StateNew;
					}
					else{
						$new_work_item["state"] = self::$StateWait;
					}
					$new_workitem_id = g("mv_workinst")->save_workitem($new_work_item);
					if($is_last){
						$msg_send = array();
						$msg_send["receiver_id"] = $receiver["receiver_id"];
						$msg_send["workitem_id"] = $new_workitem_id;
						$msg_send_arr[] =$msg_send;
					}
				}
				else{//存在，合并工作项
					$temp_workitem = $is_same_handler[0];
					$temp_pre_work_node_id = json_decode($temp_workitem["pre_work_node_id"],TRUE);
					if(!in_array($curr_workitem["id"],$temp_pre_work_node_id)){
						$temp_pre_work_node_id[] = $curr_workitem["id"];
						g("mv_workinst")->update_same_handlers($formsetinst_id,$work_node["id"],json_encode($temp_pre_work_node_id),$this->ComId);
					}
					
				}
			}
			unset($receiver);
			g("mv_formsetinst")->update_formsetinst_state($formsetinst_id,$this->ComId,self::$StateFormDone);
		}
		else{//结束节点
			//判断是否还有工作项没有结束
			$not_finish_items = g("mv_workinst")->get_formset_state_item_nwi($formsetinst_id,$curr_workitem_id,array(self::$StateNew,self::$StateRead,self::$StateSave,self::$StateWait),$this->ComId);
			$count = empty($not_finish_items)?0:count($not_finish_items);
			
			if($count==0){//没有更新表单实例状态
				g("mv_formsetinst")->update_formsetinst_state($formsetinst_id,$this->ComId,self::$StateFormFinish);
				$workitem_rule.=",".$work_node_id;
				$formsetinst["curr_node_id"] = $work_node_id;
				$user_id = $formsetinst["creater_id"];
				$user = g('api_user') -> get_by_id($user_id);
				$msg_user = array($user['acct']);
				$msg_title=$this->AppCName."通过提醒";
				$msg_desc = $this->AppCName."："."您发起的《".$formsetinst["formsetinst_name"]."》流程已审批通过。";
				$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
				$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
				try{
					//发送PC提醒
					$data = array(
						'url' => $pc_url,
						'title' => $msg_desc,
						'app_name' => $this->AppName
					);
					g("messager")->publish_by_user('pc', $user_id,$data);
					//发送微信提醒
					parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
				}catch(SCException $e){
					log_write("fail_user ".$user["name"]);
				}
				$is_finish = "TRUE";
			}
			else{
				//获取所有等待中的事项
				$waiting_items = g("mv_workinst")->get_formset_state_workitem($formsetinst_id,array(self::$StateWait),$this->ComId);
				
				if(is_array($waiting_items)&&$count==count($waiting_items)){
					$waiting_item = $waiting_items[0];
					$work_node_name = $waiting_item["workitem_name"];
					$workitem_rule.=",".$waiting_item["work_node_id"];
					$formsetinst["curr_node_id"] = $work_node_id;
					//获取某一个等待中节点的事项
					$new_waiting_item = g("mv_workinst")->get_node_state_item($formsetinst_id,$waiting_item["work_node_id"],array(self::$StateWait),$this->ComId);
					g("mv_workinst")->update_state_workitem($this->ComId,$formsetinst_id,$waiting_item["work_node_id"],self::$StateWait,self::$StateNew);
					foreach ($new_waiting_item as $item){
						$msg_send = array();
						$msg_send["receiver_id"] = $item["handler_id"];
						$msg_send["workitem_id"] = $item["id"];
						$msg_send_arr[] =$msg_send; 
						$receiver_str[] = $item["handler_name"];
					}
					unset($item);
				}
				else{
					$is_finish = "TRUE";
				}
			}
		}
		if(count($receiver_str)!=0){
			$receiver_str = implode(",",$receiver_str);
		}
		
		//更新流程规则
		g("mv_formsetinst")->update_formsetInst_workitemrule($formsetinst_id,$this->ComId,$workitem_rule,$formsetinst["curr_node_id"]);
		
		
		//消息提醒
		foreach ($msg_send_arr as $msg_send){
			$user_id = $msg_send["receiver_id"];
			$workitem_id = $msg_send["workitem_id"];
			$user = g('api_user') -> get_by_id($user_id);
			$msg_user = array($user['acct']);
			$msg_title=$this->AppCName."待办提醒";
			$msg_desc = $this->AppCName."："."您收到了《".$formsetinst["formsetinst_name"]."》待办事项，请及时处理。";
			$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
			$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
			try{
				//发送PC提醒
				$fail_user ="";
				$data = array(
					'url' => $pc_url,
					'title' => $msg_desc,
					'app_name' => $this->AppName
				);
				g("messager")->publish_by_user('pc', $user_id,$data);
				//发送微信提醒
				parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
			}catch(SCException $e){
				log_write("fail_user ".$user["name"]);
			}
		}
		unset($msg_send);
		
		$return_arr[0] = $is_finish;
		$return_arr[1] = $work_node_name;
		$return_arr[2] = $receiver_str;
		return $return_arr;
	}
	
	/**
	 * 自动知会流程new
	 * @param array $curr_workitem
	 * @param int $formsetinst_id
	 * @param array $work_nodes
	 */
	private function notify_process($curr_workitem,$formsetinst_id,$work_nodes){
		//获取表单流程实例
		$formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
		
		if(is_array($formsetinst)){
			$is_end_notify = $curr_workitem["is_auto_notify"];
			$notify_people = array();
			
			$end_node_id = $curr_workitem["work_node_id"];
			$end_work_node = g("mv_work_node")->get_by_id($this->ComId,$end_node_id);
			$end_notify_people = Array();
			if($is_end_notify==self::$NotifyAutoEnd&&!empty($end_work_node["notify_rule"])){//结束后知会
				$notify_rules = $end_work_node["notify_rule"];
				$notify_rule_arr = explode(",",$notify_rules);
				$end_notify_people = $this->set_notify_user($notify_rule_arr,$curr_workitem,$end_work_node);
			}
					
			$work_node = $work_nodes[0];
			$start_node_id = $work_node['id'];
			$start_work_node = g("mv_work_node")->get_by_id($this->ComId,$start_node_id);
			$start_notify_people = Array();
			
			if(is_array($start_work_node)&&$start_work_node["is_auto_notify"]==self::$NotifyAutoStart&&!empty($start_work_node["notify_rule"])){
				$notify_rules = $start_work_node["notify_rule"];
				$notify_rule_arr = explode(",",$notify_rules);
				$start_notify_people = $this->set_notify_user($notify_rule_arr,$curr_workitem,$start_work_node);
			}
			
			$notify_people = array_merge($end_notify_people, $start_notify_people); 
			
			$send_user = array();
			foreach($notify_people as $user){
				$receiver_id = $user["id"];
				if(in_array($receiver_id,$send_user)){//如果已经发送过该人员，不再发送
					continue;
				}
				$receiver_name = $user["name"];
				$notify_id = g("mv_notify")->save_notify($formsetinst,$this->UserId,$this->UserName,$receiver_id,$receiver_name,$this->ComId);
				$msg_user = array($user['acct']);
				$msg_title=$this->AppCName."知会提醒";
				$msg_desc = $this->AppCName."：".$this->UserName."将流程《".$formsetinst["formsetinst_name"]."》知会给您，请及时了解流程详情。";
				$msg_url = $this->WxDomain.'?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;//TODO
				$pc_url = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_open&a=notify_detail&notify_id='.$notify_id;//TODO
				$pc_menu = $this->PcDomain.'index.php?app='.$this->AppName.'&m=mv_list&a=get_notify_list';//TODO
					
				try{
					$fail_user='';
					//发送PC提醒
					$data = array(
						'url' => $pc_url,
						'title' => $msg_desc,
						'app_name' => $this->AppName
					);
					g("messager")->publish_by_user('pc', $receiver_id,$data);
					//发送微信提醒
					parent::send_single_news($msg_user, $msg_title, $msg_desc, $msg_url);
					$send_user[] = $receiver_id;
				}catch(SCException $e){
					log_write("fail_user ".$user["name"]);
				}
			}
			unset($user);
		}
		else{
			throw new SCException('没有权限知会该流程！');
		}
	}
	
	/**
	 * 根据知会规则和当前工作项封装知会人信息new
	 * @param array $notify_rule_arr
	 * @param array $curr_workitem
	 */
	private function set_notify_user($notify_rule_arr,$curr_workitem,$next_work){
		$notify_people = Array();

		foreach ($notify_rule_arr as $notify_rule){
			if($notify_rule ==self::$StateNotifyPeople){
				$notify_user = $next_work["notify_user"];
				$user_ids = array();
				if(!empty($notify_user)){
					$user_ids = explode(",",$notify_user);
				}
				$notify_group = $next_work["notify_group"];
				if(!empty($notify_group)){
					$group_ids = explode(",",$notify_group);
					$group_user_ids = g("api_group")->get_user_by_ids($this->ComId,$group_ids);
					$tmp_user_ids = array_column($group_user_ids, 'id');
					$user_ids = array_merge($user_ids, $tmp_user_ids);
				}
				$users = g("api_user")->list_by_ids($user_ids,'*',FALSE);
				if(is_array($users)){
					foreach ($users as $user){
						$this->set_receiver($notify_people, $user);
					}
					unset($user);
				}
				
			}
			else if ($notify_rule==self::$StateNotifySelf){
				$user_id = $curr_workitem["creater_id"];
				$user = g("api_user")->get_by_id($user_id);
				if(is_array($user)){
					$this->set_receiver($notify_people, $user);
				}
			}
			else if($notify_rule==self::$StateNotifyLeader){
				$user_id = $curr_workitem["creater_id"];
				$user_leader = g("api_user")->list_leader_by_id($user_id,FALSE,'*');
				if(is_array($user_leader)){
					foreach ($user_leader as $user){
						if(is_array($user)){
							$this->set_receiver($notify_people, $user);
						}
					}
					unset($user);
				}
			}
			else if($notify_rule==self::$StateNotifyILeader){
				$user_id = $curr_workitem["handler_id"];
				$user_leader = g("api_user")->list_leader_by_id($user_id,FALSE,'*');
				if(is_array($user_leader)){
					foreach ($user_leader as $user){
						if(is_array($user)){
							$this->set_receiver($notify_people, $user);
						}
					}
					unset($user);
				}
			}
			
		}
		unset($notify_rule);
		
		return $notify_people;
	}
	
	
	
	
	
	
	/**
	 * 判断接收人数组中是否有有当前需要添加的人员
	 * $receive_array数组结果集 $user将要加入的用户
	 */
	private function set_receiver(&$receive_array,$user){
		$is_same = FALSE;
		foreach ($receive_array as $receiver){
			if($receiver["id"]==$user["id"]){
				$is_same = TRUE;
				break;
			}
		}
		if(!$is_same){
			$receive_array = array_merge($receive_array, array($user));
		}
	}
	
	/**
	 * 获取用户接收人信息
	 */
	private function get_user_receiver_list(&$receive_array,$userid){
		$user = g("api_user")->get_by_id($userid);
		if(is_array($user)){//如果用户不存在，则使用地址本
			$this->set_receiver($receive_array, $user);
		}
	}
	
	/**
	 * 获取用户上级领导接收人信息
	 */
	private function get_user_leader_receiver_list(&$receive_array,$userid){
		$user_leader = g("api_user")->list_leader_by_id($userid,FALSE,'*');
		
		if(is_array($user_leader)){//如果用户不存在，则使用地址本
			foreach ($user_leader as $user){
				if(is_array($user)){
					$this->set_receiver($receive_array, $user);
				}
			}
		}
	}
	
	/**
	 * 获取用户部门接收人信息
	 */
	private function get_user_dept_receiver_list(&$receive_array,$userid){
		$user = g("api_user")->get_by_id($userid);
		$dept_list = $user["dept_list"];
		if(empty($dept_list)){
			throw new SCException('创建人不存在部门，请设置创建人的部门');
		}
		$depts = json_decode($dept_list, TRUE);
		foreach ($depts as $dept_id){
			$dept_user = g("api_user")->list_direct_by_dept_id($dept_id,'*',FALSE);
			if(is_array($dept_user)){
				foreach ($dept_user as $user){
					$this->set_receiver($receive_array, $user);
				}
			}
		}
	}

}

// end of file