<?php
/**
 * 应用处理类的基础类
 * @author yangpz
 * @date 2014-11-17
 *
 */
abstract class abs_app_base {
	protected $temp_path;
	/** 应用的公共模板存放路径 */
	protected static $CommonTempPath;
	/** 企业号中应用的ID号 */
	protected $app_id;
	/** 当前应用所属的套餐 */
	protected $app_combo;
	/** app英文名称，对应config中的name字段 */
	protected $app_name;
	/** app中文名称，对应config中的cn_name字段 */
	protected $app_cn_name;
	/** app访问域 */
	protected $app_domain;
	/** 应用底部的菜单 */	
	protected $app_menu;
	
	/** 不显示技术支持的企业的com_id */
	private static $NoPowerByComList = array(190, 32, 3459, 2252);
	
	public function __construct($app_name) {
		global $app_id;
		$this -> app_id = $app_id;
		
		//网站信息
		smarty_init();
		self::$CommonTempPath = SYSTEM_APPS.'common/templates/';
		
		$app_conf = include(SYSTEM_APPS.$app_name.'/config.php');
		$this -> temp_path = SYSTEM_APPS.$app_name.'/templates/';
		$this -> app_name = $app_conf['name'];
		$this -> app_cn_name = $app_conf['cn_name'];
		$this -> app_combo = $app_conf['combo'];
		$this -> app_domain = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app_name;
		
		g('smarty') -> assign('CALCULATE_URL', SYSTEM_HTTP_DOMAIN.'index.php?m=calculate');		//统计应用使用频率
		
		g('smarty') -> assign('APP_DOMAIN', SYSTEM_HTTP_DOMAIN.'index.php?app='.$app_name);
		g('smarty') -> assign('APP_STATIC', SYSTEM_STATIC_DOMAIN.'apps/'.$app_name.'/static/');
		g('smarty') -> assign('APP_COMMON', SYSTEM_STATIC_DOMAIN.'apps/common/');
		g('smarty') -> assign('APP_ICON', $app_conf['icon']);
		g('smarty') -> assign('APP_NAME', $app_conf['cn_name']);
		$power_by_str = isset($_SESSION[SESSION_VISIT_POWERBY]) ? $_SESSION[SESSION_VISIT_POWERBY] : '';
		if (isset($_SESSION[SESSION_VISIT_COM_ID]) && in_array($_SESSION[SESSION_VISIT_COM_ID], self::$NoPowerByComList)) {
			$power_by_str = '';
		}
		g('smarty') -> assign('POWERBY_VAL', $power_by_str);
		g('smarty') -> assign('POWERBY_URL', isset($_SESSION[SESSION_VISIT_POWERBY_URL]) ? $_SESSION[SESSION_VISIT_POWERBY_URL] : 'javascript:void(0);');
		
		$this -> app_menu = $app_conf['menu'];
		g('smarty') -> assign('APP_MENU', isset($app_conf['menu'][0]) ? $app_conf['menu'][0] : array());
		g('smarty') -> assign('title', $app_conf['cn_name']);
		g('smarty') -> assign('this_url', get_this_url());
		g('smarty') -> assign('ajax_url', get_this_url());
		//统一上传路径
		g('smarty') -> assign('unify_upload_url', SYSTEM_HTTP_DOMAIN.'index.php?m=root_ajax&cmd=101');
		//统一检查文件是否已经上传路径
		g('smarty') -> assign('unify_check_url', SYSTEM_HTTP_DOMAIN.'index.php?m=root_ajax&cmd=102');
		
		if (isset($_SESSION[SESSION_VISIT_CORP_URL])) {
			g('smarty') -> assign('corp_url', $_SESSION[SESSION_VISIT_CORP_URL]);
		} else {
			log_write('未配置corp url, url='.get_this_url(), 'ERROR');
		}
		
		g('smarty') -> assign('jssdk_upload_url', SYSTEM_HTTP_DOMAIN.'index.php?m=root_ajax&utype=2&cmd=101');

		//统计应用使用信息
		g('app_visit') -> check_increase($app_conf['id']);

		//检查企业容量，一旦容量已满，即跳转到错误页面
		g('capacity') -> check_balance();
	}
	
	/** 员工选择控件，分页查找人员 */
	public function contact_list_user() {
		try {
			// gch add contact config
			$watchAll = g('contact_config') -> checkWatchAll($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
			if ($watchAll) {
				cls_resp::show_warn_page('您没有权限查看通讯录');
				// throw new \SCException("您没有权限查看通讯录");
			}
			
			$need = array('name', 'dept', 'offset', 'limit');
			$data = $this -> get_post_data($need);
			
			$cond = array();
			if ($this -> app_name === 'contact') {	//通讯录应用，使用过滤
				$cond = array(
					'struct_filter' => TRUE,		//是否用虚拟架构过滤
					'contact_filter' => TRUE,		//是否用通讯录应用配置过滤
				);
			}
			
			$fields = array(
	            'root_id', 'id', 'name', 'all_py', 'first_py', 'pic_url'
	        );
	        $fields = implode(',', $fields);
			$page = $data['offset'] == 0 ? 1 : (ceil($data['offset'] / $data['limit']) + 1);
			$params = array(
				'keyword' => $data['name']
			);
			$data['dept'] == 0 && $data['dept'] = $_SESSION[SESSION_VISIT_DEPT_ID];
			$ret = g('api_user', $cond) -> page_list_by_dept_ids(array($data['dept']), $page, $data['limit'], $params, $fields, ' all_py ');
			cls_resp::echo_ok(cls_resp::$OK, 'info', $ret);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 员工选择控件：加载员工详情信息 */
	public function contact_load_user_detail() {
		try {
			$need = array('user');
			$data = $this -> get_post_data($need);

			// gch add
			$fields = 'id,name,acct,pic_url,gender,position,dept_list,all_py,first_py,ext_attr';
			$conf = g('cont_conf') -> get_by_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
			$conf && $fields_conf = json_decode($conf['show_list'], TRUE);
			in_array('short_code', $fields_conf) && $fields .= ",short_code";
			in_array('inner_code', $fields_conf) && $fields .= ",inner_code";

			if (isset($data['ext_attr']) && !empty($data['ext_attr'])) {
				$fields .= ','.implode(',', $data['ext_attr']);
			}
			$ret = g('user') -> get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID], $data['user'], $fields);
			if ($ret) {
				$ret['dept_list'] = json_decode($ret['dept_list'], TRUE);
				$ret['dept_list'] = g('api_dept') -> list_by_ids($ret['dept_list'], 'id, name');
			}
			
			if (isset($data['chk_general']) && $data['chk_general'] == 1) {	//判断是否为常用联系人
				$general = g('user_general') -> get_by_ids(array($_SESSION[SESSION_VISIT_USER_ID]), 'general_list');
				$general = $general ? json_decode($general[0]['general_list'], TRUE) : array();
				$ret['general'] = in_array($ret['id'], $general, FALSE);
			}
			//TODO 仅为该企业开启
			if ($_SESSION[SESSION_VISIT_COM_ID] != 11951) {
				$ret['ext_attr'] = '';
			}
			//TODO END仅为该企业开启
			cls_resp::echo_ok(cls_resp::$OK, 'info', $ret);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 加载已选择的员工的数据（用于初始化数据） */
	public function contact_load_by_ids() {
		try {
			$need = array('user');
			$data = $this -> get_post_data($need);
			
			$type = isset($data['dept']) ? 'dept' : 'user';
			$ret = array();
			switch($type) {
				case 'user':
					!is_array($data['user']) && $data['user'] = json_decode($data['user'], TRUE);
					$ret = g('user') -> get_by_ids($data['user'], 'id,name,pic_url');
					break;
					
				case 'dept':
					!is_array($data['dept']) && $data['dept'] = json_decode($data['dept'], TRUE);
					$ret = g('api_dept') -> list_by_ids($data['dept'], 'id,name');
					break;
			}
			
			cls_resp::echo_ok(cls_resp::$OK, 'info', $ret);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 加载子部门 */
	public function contact_load_sub_dept() {
		try {
			// gch add contact config
			$watchAll = g('contact_config') -> checkWatchAll($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
			if ($watchAll) {
				cls_resp::show_warn_page('您没有权限查看通讯录');
				// throw new \SCException("您没有权限查看通讯录");
			}

			$need = array('d_id');
			$data = $this -> get_post_data($need);
			$dept_list = g('api_dept') -> list_direct_child($data['d_id']);
			
			if (!empty($dept_list)) {			
				$d_ids = array();
				foreach ($dept_list as $d) {
					$d_ids[] = $d['id'];
				}unset($d);
				$has_childs = g('api_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
				
				$root_dept['childs'] = $dept_list;
				foreach ($dept_list as &$dept) {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
			}
			
			cls_resp::echo_ok(cls_resp::$OK, 'info', $dept_list);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}

    /**
     * 判断是否为家长访问
     * @return bool
     */
	protected function is_parent(){
	    if(!empty($_SESSION[SESSION_VISIT_USER_WXINFO]['parent_userid']))
	        return true;

	    return false;
    }
	
	/**
	 * 获取请求数据(带验证参数名/个数)
	 * @param unknown_type $fields 必有的字段
	 */
	protected final function get_post_data($fields=array()) {
		$data = get_var_value('data');
		if (is_null($data)) {
			throw new SCException('不合法的请求参数1');
		}
		!is_array($data) && $data = json_decode($data, TRUE);
		if (!is_array($data)) {
			throw new SCException('不合法的请求参数2');
		}
		
		$keys = array_keys($data);
		!is_array($fields) && $fields = array();
		foreach ($fields as $field) {	//验证必备字段
			if (!in_array($field, $keys)) {
				log_write('缺少请求参数:'.$field);
				throw new SCException('缺少请求参数');
			}
		}
		return $data;
	}
	
	/**
	 * 根据当前登录信息，获取access token
	 */
	protected function get_access_token() {
		try {
            global $sc_app_id;
			return g('atoken') -> get_access_token_by_com($_SESSION[SESSION_VISIT_COM_ID], $sc_app_id);
			
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 记录到日志文件
	 * @param unknown_type $info 记录的内容
	 */
	protected function log($info) {
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			log_write('com_id='.$_SESSION[SESSION_VISIT_COM_ID].', wxacct='.$_SESSION[SESSION_VISIT_USER_WXACCT].', '.$info);

		} else {
			log_write('企业号外部人员操作, '.$info);
		}
	}

	/**
	 * 发送单图文消息（主要用于提醒员工）
	 * @param unknown_type $user_list	接收人列表，数组格式，如：array('001', '002');
	 * @param unknown_type $title
	 * @param unknown_type $desc
	 * @param unknown_type $url
	 * @param unknown_type $pic_url
	 */
	protected function send_single_news($user_list, $title, $desc, $url, $pic_url='',$dept_list=array()) {
		log_write('发送信息给：'.json_encode($user_list));
		try { 
			$art = array(
				'title' => $title,
				'description' => $desc,
				'url' => $url."&corpurl={$_SESSION[SESSION_VISIT_CORP_URL]}",
				'picurl' => $pic_url,
			);
			$token = $this -> get_access_token();
			g('wxqy') -> send_news($token, $user_list, $this -> app_id, array($art),array(),$dept_list);
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	/**
	 * 在smarty中定义企业号的js_sdk配置参数
	 *
	 * @access protected
	 * @param string $app_id 指定使用该企业号的微信app_id
	 * @param string $com_id 公司id，指定使用该公司的accesson_token
	 * @return void
	 */
	protected function oauth_jssdk($app_id = '', $com_id = 0) {
		//企业号方式
		g('js_sdk') -> assign_jssdk_sign($app_id, $com_id);
		//g('js_sdk') -> auth_assign_jssdk_sign();
		
		//公众号方式
		/*
		$app_id = 'wx992bf08e8c72348e';
		$app_secret = '4ab5d27592ba2843a49b33a9097d7f3e';

		$time = time();
		$nonce_str = get_rand_string(10);
		$this_url = get_this_url();

		$data = array(
			'noncestr' => $nonce_str,
			'timestamp' => $time,
			'url' => $this_url,
		);

		$sign = g('exper', 0) -> get_jsapi_sign($app_id, $app_secret, $data);

		g('smarty') -> assign('app_id', $app_id);
		g('smarty') -> assign('time', $time);
		g('smarty') -> assign('nonce_str', $nonce_str);
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('sign', $sign);
		*/
	}
	
	/**
	 * 获取分页数据（数组）
	 * @param unknown_type $curr_page
	 * @param unknown_type $page_size
	 * @param unknown_type $total_cout
	 * @param unknown_type $curr_count
	 */
	protected function get_pageinfo($curr_page, $page_size, $total_count, $curr_count) {
		$info = array(
			'curr_page' => $curr_page,
			'page_size' => $page_size,
			'total_count' => $total_count,
			'curr_count' => $curr_count,
			'total_page' => ceil($total_count / $page_size),
		);
		return $info;
	}
	
	/** 设置选人控件部门 */
	protected function assign_contact_dept($st_filter=FALSE) {	//默认不执行架构过滤
		// gch add contact config
		$watchAll = g('contact_config') -> checkWatchAll($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
		if ($watchAll) {
			cls_resp::show_warn_page('您没有权限查看通讯录');
			// g('smarty') -> assign('c_search_user_dept', array());
			// g('smarty') -> assign('c_search_dept_url', get_this_url().'&a=contact_load_sub_dept');
			// g('smarty') -> assign('c_search_user_url', get_this_url().'&a=contact_list_user');
			// g('smarty') -> assign('c_search_user_init_url', get_this_url().'&a=contact_load_by_ids');
			// g('smarty') -> assign('c_search_user_detail_url', get_this_url().'&a=contact_load_user_detail');
			// g('smarty') -> assign('c_search_base_url', get_this_url());
			// g('smarty') -> assign('c_search_dept', array());
			// return;
		}
		
		if ($st_filter) {
			$root_dept = g('share_struct') -> get_top_depts();

			$d_ids = array();
			foreach ($root_dept as $d) {
				$d['id'] > 0 && $d_ids[] = $d['id'];
			}unset($d);
			$has_childs = g('api_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
		
			foreach ($root_dept as &$dept) {
				if ($dept['id'] <= 0) {
					$dept['has_child'] = TRUE;
					$dept['virtual'] = TRUE;
					
				} else {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
				if (isset($dept['children']) && !empty($dept['children'])) {
					$dept['childs'] = $dept['children'];
					
					$c_ids = array();
					foreach ($dept['children'] as $d) {
						$c_ids[] = $d['id'];
					}unset($d);
					$c_has_childs = g('api_dept') -> list_has_child_by_ids($c_ids);		//是否有子部门
					
					foreach ($dept['children'] as $key => $c) {
						$dept['childs'][$key]['has_child'] = $c_has_childs[$c['id']];
					}unset($key);unset($c);
					unset($dept['children']);
				}
			}
			
		} else {
			$root_dept = g('api_dept') -> list_by_lev(1, 'id, p_id, lev, name, idx');
			!empty($root_dept) && $root_dept = reset($root_dept);

			$dept_list = g('api_dept') -> list_by_lev(2, 'id, p_id, lev, name, idx');
			if (!empty($dept_list)) {			
				$d_ids = array();
				foreach ($dept_list as $d) {
					$d_ids[] = $d['id'];
				}unset($d);
				$has_childs = g('api_dept') -> list_has_child_by_ids($d_ids);	//是否有子部门
				
				$root_dept['childs'] = $dept_list;
				foreach ($dept_list as &$dept) {
					$dept['has_child'] = $has_childs[$dept['id']];
				}
				$root_dept['childs'] = $dept_list;
			}
			!empty($root_dept) && $root_dept = array($root_dept);
		}
		$user_dept = g('api_dept') -> get_by_id($_SESSION[SESSION_VISIT_USER_DEPT], 'id,name');
		
		g('smarty') -> assign('c_search_user_dept', $user_dept);
		g('smarty') -> assign('c_search_dept_url', get_this_url().'&a=contact_load_sub_dept');
		g('smarty') -> assign('c_search_user_url', get_this_url().'&a=contact_list_user');
		g('smarty') -> assign('c_search_user_init_url', get_this_url().'&a=contact_load_by_ids');
		g('smarty') -> assign('c_search_user_detail_url', get_this_url().'&a=contact_load_user_detail');
		g('smarty') -> assign('c_search_base_url', get_this_url());
		g('smarty') -> assign('c_search_dept', $root_dept);
	}
	
	/**
	 * 通用下载方法
	 * @param array $file			文件信息，如：array('name' => 'name', 'extname' => 'ext', 'hash' => 'hash');
	 * @param unknown_type $is_push		是否强制通过微信推送
	 * @param unknown_type $size		为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
	 */ 
	protected function common_download(array $file, $is_push=FALSE, $size='') {
		$this -> log('开始下载文件');
		try {
			$platid = g('common') -> get_mobile_platid();
			$ad_download = g('media') -> android_download;
	
			$ext = strtolower($file['extname']);
			//非推送
			if (!$is_push) {
				//如果是ios平台支持的文件，或者是安卓平台下支持直接下载的文件类型，则直接跳转下载
				if (($platid === 1 and $ext != 'rar' and $ext != 'zip') or ($platid === 2 and in_array($ext, $ad_download))) {
					$url = g('media') -> cloud_rename_download($file['name'], $file['hash'], $size, FALSE);
					$this -> log('下载文件成功（非推送）');
					cls_resp::echo_ok(cls_resp::$OK, 'info', array('url' => $url));
				}
			}
			
			//推送
			//1.获取media_id用于推送
			try {
				$media_info = g('media') -> get_media_id($_SESSION[SESSION_VISIT_COM_ID], $this -> app_combo, $file['hash'], $file['name'], $size);
				$media_id = $media_info['media_id'];
				
			}catch(SCException $e) {
				throw new SCException('系统正忙，请稍后再试！');
			}
	
			//2.区分文件类型进行推送
			$file_type = g('media') -> get_file_type($ext);
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			g('nmsg') -> init($_SESSION[SESSION_VISIT_COM_ID], $this -> app_name);
			try {
				if ($file_type == 'video') {
					g('nmsg') -> send_video($media_id, '', '', array(), array($user_id));
					
				}else {
					g('nmsg') -> send_files($file_type, $media_id, array(), array($user_id));
				}
				$this -> log('下载文件成功（推送）');
				cls_resp::echo_ok();
				
			}catch(SCException $e) {
				$err_tip = '系统正忙，推送失败！';
				try {
					g('nmsg') -> send_text($err_tip, array(), array($user_id));
				}catch(SCException $tmpe) {
					throw $tmpe;
				}
	
				throw $e;
			}
			
		}catch(SCException $e) {
			$this -> log('下载文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
	}

    /**
     * 家校通用下载方法
     * @param array $file			文件信息，如：array('name' => 'name', 'extname' => 'ext', 'hash' => 'hash');
     * @param unknown_type $is_push		是否强制通过微信推送
     * @param unknown_type $size		为图片时有效，代表图片尺寸，如：100x200，如果取值是-1，则表示下载原图
     */
    protected function school_common_download(array $file, $is_push=FALSE, $size='') {
        $this -> log('开始下载文件');
        try {
            $platid = g('common') -> get_mobile_platid();
            $ad_download = g('media') -> android_download;

            $ext = strtolower($file['extname']);
            //非推送
            if (!$is_push) {
                //如果是ios平台支持的文件，或者是安卓平台下支持直接下载的文件类型，则直接跳转下载
                if (($platid === 1 and $ext != 'rar' and $ext != 'zip') or ($platid === 2 and in_array($ext, $ad_download))) {
                    $url = g('media') -> cloud_rename_download($file['name'], $file['hash'], $size, FALSE);
                    $this -> log('下载文件成功（非推送）');
                    cls_resp::echo_ok(cls_resp::$OK, 'info', array('url' => $url));
                }
            }

            //推送
            //1.获取media_id用于推送
            try {
                $media_info = g('media') -> get_media_id($_SESSION[SESSION_VISIT_COM_ID], $this -> app_combo, $file['hash'], $file['name'], $size);
                $media_id = $media_info['media_id'];

            }catch(SCException $e) {
                throw new SCException('系统正忙，请稍后再试！');
            }

            //2.区分文件类型进行推送
            $file_type = g('media') -> get_file_type($ext);
            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            g('nmsg') -> init($_SESSION[SESSION_VISIT_COM_ID], $this -> app_name);
            try {
                if ($file_type == 'video') {
                    g('nmsg') -> send_school_video($media_id, '', '', array(), array($user_id));

                }else {
                    g('nmsg') -> send_school_files($file_type, $media_id, array(), array($user_id));
                }
                $this -> log('下载文件成功（推送）');
                cls_resp::echo_ok();

            }catch(SCException $e) {
                $err_tip = '系统正忙，推送失败！';
                try {
                    g('nmsg') -> send_school_text($err_tip, array(), array($user_id));
                }catch(SCException $tmpe) {
                    throw $tmpe;
                }

                throw $e;
            }

        }catch(SCException $e) {
            $this -> log('下载文件失败，异常：[' . $e -> getMessage() . ']');
            cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
        }
    }
	
	//--------------------------------内部实现---------------------------------
}

// end of file