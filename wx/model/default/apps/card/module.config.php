<?php
/**
 * 模板列表配置文件
 *
 * @author LiangJianMing
 * @create 2015-1-7
 */

return array(
	'a_1' => array(
		'key'  	=> 'a_1',
		'img'  	=> 'images/cardmodel/a_1.jpg',
		'file' 	=> 'business_card_model/model1.html',
		'class'	=> 'blue',
	),
	'b_1' => array(
		'key'  	=> 'b_1',
		'img'  	=> 'images/cardmodel/b_1.jpg',
		'file' 	=> 'business_card_model/model2.html',
		'class'	=> 'blue',
	),
	'c_1' => array(
		'key'  	=> 'c_1',
		'img'  	=> 'images/cardmodel/c_1.jpg',
		'file' 	=> 'business_card_model/model3.html',
		'class'	=> 'blue',
	),
	'd_1' => array(
		'key'  	=> 'd_1',
		'img'  	=> 'images/cardmodel/d_1.jpg',
		'file' 	=> 'business_card_model/model4.html',
		'class'	=> 'blue',
	),
	'e_1' => array(
		'key'  	=> 'e_1',
		'img'  	=> 'images/cardmodel/e_1.jpg',
		'file' 	=> 'business_card_model/model5.html',
		'class'	=> 'bubble',
	),
);

// end of file