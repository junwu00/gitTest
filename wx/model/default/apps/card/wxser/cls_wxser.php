<?php
/**
 * 微名称
 * 
 * @author yangpz
 * @date 2015-01-21
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『客户名片』操作指引',
		'desc' => '欢迎使用客户名片应用，在这里你可以创建个性化名片，通过微信传给潜在用户，也能快速保存客户名片，不再再带着一大堆纸质名片啦！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970350&idx=3&sn=79a069c45be33aaf982f6e8fb1fab3f3&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file