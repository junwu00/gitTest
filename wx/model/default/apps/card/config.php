<?php
/**
 * 应用的配置文件_客户名片
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 		=> 4,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(4),
	'name' 		=> 'card',
	'cn_name' 	=> '客户名片',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'Bcard.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '名片夹', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=card&m=list&a=show', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '我的名片',  'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=card&m=info&a=show', 'icon' => ''),
		),
	),
		
	'wx_menu' => array(
		//array('name' => '名片夹', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=card&m=list&a=group&corpurl=<corpurl>'),
		//array('name' => '我的名片', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=card&m=self&corpurl=<corpurl>'),
	),
);

// end of file