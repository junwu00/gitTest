<?php
/**
 * 全部客户名片ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2014-12-12
 */
class cls_ajax extends abs_app_base {
	public function __construct() {
		parent::__construct('card');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		$func = '';
		switch($cmd) {
			case 101 : {
				//添加备份名片
				$this -> add_card();
				BREAK;
			}
			case 102 : {
				//更新备份名片
				$this -> update_card();
				BREAK;
			}
			case 103 : {
				//删除名片
				$this -> delete_card();
				BREAK;
			}
			case 104 : {
				//添加分组
				$this -> add_group();
				BREAK;
			}
			case 105 : {
				//更新分组
				$this -> update_group();
				BREAK;
			}
			case 106 : {
				//删除分组
				$this -> delete_group();
				BREAK;
			}
			case 107 : {
				//初始化自身名片
				$this -> card_init();
				BREAK;
			}
			case 108 : {
				//更新自身名片
				$this -> update_self();
				BREAK;
			}
			case 109 : {
				//收藏别人的名片
				$this -> collect_card();
				BREAK;
			}
			case 110 : {
				//更新收藏名片
				$this -> collect_update();
				BREAK;
			}
			case 111 : {
				//删除收藏名片
				$this -> collect_delete();
				BREAK;
			}
			case 112 : {
				//企业内部分享自己的名片
				$this -> share_inside();
				BREAK;
			}
			case 113 : {
				//导入到本地联系人
				$this -> educe_card();
				BREAK;
			}
			default : {
				//非法的请求
				cls_resp::echo_resp(cls_resp::$ForbidReq);
				BREAK;
			}
		}
	}

	/**
	 * 添加一张备份名片
	 *
	 * @access public
	 * @return void
	 */
	public function add_card() {
		$fields = array(
			'cn_name',
			'mobile',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$cn_name 	= trim($data['cn_name']);
		$mobile 	= trim($data['mobile']);
		if (empty($cn_name) || empty($mobile)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$map = g('card') -> name_map;

		if (!check_data($cn_name, 'sname')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['cn_name'].'只能以中文、字母开头，且只能包含中文、字母、数字、下划线。');
		}

		if (!check_data($mobile, 'mobile_s')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['mobile'] . '格式不正确');
		}

		$share_id 	= isset($data['share_id']) ? intval($data['share_id']) : 0;
		$en_name 	= isset($data['en_name']) ? trim($data['en_name']) : '';
		$call 		= isset($data['call']) ? trim($data['call']) : '';
		$email 		= isset($data['email']) ? trim($data['email']) : '';
		$qq 		= isset($data['qq']) ? trim($data['qq']) : '';
		$wx 		= isset($data['wx']) ? trim($data['wx']) : '';
		$fax 		= isset($data['fax']) ? trim($data['fax']) : '';
		$com_name 	= isset($data['com_name']) ? trim($data['com_name']) : '';
		$com_job 	= isset($data['com_job']) ? trim($data['com_job']) : '';
		$com_addr 	= isset($data['com_addr']) ? trim($data['com_addr']) : '';

		$info_data = array(
			'cn_name' => $cn_name,
			'mobile'  => $mobile,
		);
		!empty($en_name) && $info_data['en_name'] = $en_name;
		!empty($call) && $info_data['`call`'] = $call;
		!empty($email) && $info_data['email'] = $email;
		!empty($qq) && $info_data['qq'] = $qq;
		!empty($wx) && $info_data['wx'] = $wx;
		!empty($fax) && $info_data['fax'] = $fax;
		!empty($com_name) && $info_data['com_name'] = $com_name;
		!empty($com_job) && $info_data['com_job'] = $com_job;
		!empty($com_addr) && $info_data['com_addr'] = $com_addr;

		$sort_py = g('char') -> cn2fpy($cn_name);
		$first_py = '';
		!empty($sort_py) && $first_py = mb_substr($sort_py, 0, 1, 'utf-8');
		$info_data['sort_py'] = $sort_py;
		$info_data['first_py'] = $first_py;

		$map_data = array(
			'share_id' => $share_id,
		);
		if (isset($data['group_id'])) {
			!empty($data['group_id']) && $map_data['group_id'] = $data['group_id'];
			unset($data['group_id']);
		}

		try {
			g('card') -> add_card($info_data, $map_data);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 更新一张备份名片
	 *
	 * @access public
	 * @return void
	 */
	public function update_card() {
		$fields = array(
			'card_id',
			'cn_name',
			'mobile',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$card_id 	= preg_replace('/[^\d]+/', '', $data['card_id']);
		$cn_name 	= trim($data['cn_name']);
		$mobile 	= trim($data['mobile']);
		if (empty($card_id) || empty($cn_name) || empty($mobile)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$map = g('card') -> name_map;

		if (!check_data($cn_name, 'sname')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['cn_name'].'只能以中文、字母开头，且只能包含中文、字母、数字、下划线。');
		}

		if (!check_data($mobile, 'mobile_s')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['mobile'] . '格式不正确');
		}

		$group_id = empty($data['group_id']) ? 0 : (int)$data['group_id'];

		$en_name 	= isset($data['en_name']) ? trim($data['en_name']) : '';
		$call 		= isset($data['call']) ? trim($data['call']) : '';
		$email 		= isset($data['email']) ? trim($data['email']) : '';
		$qq 		= isset($data['qq']) ? trim($data['qq']) : '';
		$wx 		= isset($data['wx']) ? trim($data['wx']) : '';
		$fax 		= isset($data['fax']) ? trim($data['fax']) : '';
		$com_name 	= isset($data['com_name']) ? trim($data['com_name']) : '';
		$com_job 	= isset($data['com_job']) ? trim($data['com_job']) : '';
		$com_addr 	= isset($data['com_addr']) ? trim($data['com_addr']) : '';

		$info_data = array(
			'cn_name' 	=> $cn_name,
			'mobile'  	=> $mobile,
			'en_name'  	=> $en_name,
			'call'  	=> $call,
			'qq'  		=> $qq,
			'wx'  		=> $wx,
			'fax'  		=> $fax,
			'com_name'  => $com_name,
			'com_job'  	=> $com_job,
			'com_addr'  => $com_addr,
		);

		$sort_py = g('char') -> cn2fpy($cn_name);
		$first_py = '';
		!empty($sort_py) && $first_py = mb_substr($sort_py, 0, 1, 'utf-8');
		$info_data['sort_py'] = $sort_py;
		$info_data['first_py'] = $first_py;

		try {
			g('card') -> update_card($card_id, $info_data, $group_id);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 删除一张备份名片
	 *
	 * @access public
	 * @return void
	 */
	public function delete_card() {
		$fields = array(
			'card_id',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		
		$card_id = preg_replace('/[^\d]+/', '', $data['card_id']);
		if (empty($card_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		try {
			g('card') -> delete_card($card_id);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 添加一个分组
	 *
	 * @access public
	 * @return void
	 */
	public function add_group() {
		$fields = array(
			'name',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		
		$name = trim($data['name']);
		if (empty($name)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$member = isset($data['member']) ? $data['member'] : array();
		!is_array($member) && $member = array();
		foreach ($member as &$val) {
			$val = intval($val);
		}
		unset($val);

		try {
			g('card') -> add_group($name, $member);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 更新一个分组
	 *
	 * @access public
	 * @return void
	 */
	public function update_group() {
		$fields = array(
			'group_id',
			'name',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		
		$group_id = (int)$data['group_id'];
		$name = trim($data['name']);
		if (empty($group_id) || empty($name)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$member = isset($data['member']) ? $data['member'] : array();
		!is_array($member) && $member = array();
		foreach ($member as &$val) {
			$val = intval($val);
		}
		unset($val);

		try {
			g('card') -> update_group($group_id, $name, $member);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 删除一个分组
	 *
	 * @access public
	 * @return void
	 */
	public function delete_group() {
		$fields = array(
			'group_id',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		
		$group_id = (int)$data['group_id'];
		if (empty($group_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		try {
			g('card') -> delete_group($group_id);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 初始化自身名片
	 *
	 * @access public
	 * @return void
	 */
	public function card_init() {
		$fields = array(
			'cn_name',
			'mobile',
			'com_name',
			'com_job',
			'com_addr',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$cn_name 	= trim($data['cn_name']);
		$mobile 	= trim($data['mobile']);
		$com_name 	= trim($data['com_name']);
		$com_job 	= trim($data['com_job']);
		$com_addr 	= trim($data['com_addr']);
		if (empty($cn_name) || empty($mobile) || empty($com_name) || empty($com_job) || empty($com_addr)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$map = g('card') -> name_map;

		if (!check_data($cn_name, 'sname')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['cn_name'].'只能以中文、字母开头，且只能包含中文、字母、数字、下划线。');
		}

		if (!check_data($mobile, 'mobile_s')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['mobile'] . '格式不正确');
		}

		$en_name 	= isset($data['en_name']) ? trim($data['en_name']) : '';
		$call 		= isset($data['call']) ? trim($data['call']) : '';
		$email 		= isset($data['email']) ? trim($data['email']) : '';
		$qq 		= isset($data['qq']) ? trim($data['qq']) : '';
		$wx 		= isset($data['wx']) ? trim($data['wx']) : '';
		$fax 		= isset($data['fax']) ? trim($data['fax']) : '';
		$mod_key 	= isset($data['mod_key']) ? trim($data['mod_key']) : '';
		$extra 		= isset($data['extra']) ? $data['extra'] : array();

		$extra = (!empty($extra) && is_array($extra)) ? json_encode($extra) : '';

		$info_data = array(
			'cn_name' 	=> $cn_name,
			'mobile' 	=> $mobile,
			'com_name' 	=> $com_name,
			'com_job' 	=> $com_job,
			'com_addr' 	=> $com_addr,
			'en_name'  	=> $en_name,
			'`call`'  	=> $call,
			'qq'  		=> $qq,
			'wx'  		=> $wx,
			'fax'  		=> $fax,
			'mod_key'  	=> $mod_key,
			'extra'  	=> $extra,
		);

		$sort_py = g('char') -> cn2fpy($cn_name);
		$first_py = '';
		!empty($sort_py) && $first_py = mb_substr($sort_py, 0, 1, 'utf-8');
		$info_data['sort_py'] = $sort_py;
		$info_data['first_py'] = $first_py;

		try {
			g('card') -> card_init($info_data);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 更新自身名片
	 *
	 * @access public
	 * @return void
	 */
	public function update_self() {
		$fields = array(
			'cn_name',
			'mobile',
			'com_name',
			'com_job',
			'com_addr',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$cn_name 	= trim($data['cn_name']);
		$mobile 	= trim($data['mobile']);
		$com_name 	= trim($data['com_name']);
		$com_job 	= trim($data['com_job']);
		$com_addr 	= trim($data['com_addr']);
		if (empty($cn_name) || empty($mobile) || empty($com_name) || empty($com_job) || empty($com_addr)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$map = g('card') -> name_map;

		if (!check_data($cn_name, 'sname')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['cn_name'].'只能以中文、字母开头，且只能包含中文、字母、数字、下划线。');
		}

		if (!check_data($mobile, 'mobile_s')) {
			cls_resp::echo_err(cls_resp::$Busy, $map['mobile'] . '格式不正确');
		}

		$en_name 	= isset($data['en_name']) ? trim($data['en_name']) : '';
		$call 		= isset($data['call']) ? trim($data['call']) : '';
		$email 		= isset($data['email']) ? trim($data['email']) : '';
		$qq 		= isset($data['qq']) ? trim($data['qq']) : '';
		$wx 		= isset($data['wx']) ? trim($data['wx']) : '';
		$fax 		= isset($data['fax']) ? trim($data['fax']) : '';
		$mod_key 	= isset($data['mod_key']) ? trim($data['mod_key']) : '';
		$extra 		= isset($data['extra']) ? $data['extra'] : array();

		$extra = (!empty($extra) && is_array($extra)) ? json_encode($extra) : '';

		$info_data = array(
			'cn_name' 	=> $cn_name,
			'mobile' 	=> $mobile,
			'com_name' 	=> $com_name,
			'com_job' 	=> $com_job,
			'com_addr' 	=> $com_addr,
			'en_name' 	=> $en_name,
			'call' 		=> $call,
			'email' 	=> $email,
			'qq' 		=> $qq,
			'wx' 		=> $wx,
			'fax' 		=> $fax,
			'mod_key' 	=> $mod_key,
			'extra' 	=> $extra,
		);

		$sort_py = g('char') -> cn2fpy($cn_name);
		$first_py = '';
		!empty($sort_py) && $first_py = mb_substr($sort_py, 0, 1, 'utf-8');
		$info_data['sort_py'] = $sort_py;
		$info_data['first_py'] = $first_py;

		try {
			g('card') -> update_self($info_data);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 收藏别人的名片
	 *
	 * @access public
	 * @return void
	 */
	public function collect_card() {
		$card_id = get_var_value('card_id');
		$from_id = get_var_value('share_id');

		$card_id = preg_replace('/[^\d]+/', '', $card_id);
		$from_user_id = preg_replace('/[^\d]+/', '', $from_id);
		if (empty($card_id) || empty($from_user_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$group_id = isset($data['group_id']) ? preg_replace('/[^\d]+/', '', $data['group_id']) : 0;
		$mark 	  = isset($data['mark']) ? trim($data['mark']) : '';
		empty($group_id) && $group_id = 0;

		$info_data = array(
			'card_id' 		=> $card_id,
			'from_user_id' 	=> $from_user_id,
		);
		!empty($group_id) && $info_data['group_id'] = $group_id;
		!empty($mark) && $info_data['mark'] = $mark;

		try {
			g('card') -> add_map($info_data);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_err($e -> getCode(), $e -> getMessage());
		}
		cls_resp::echo_busy();
	}

	/**
	 * 更新一张收藏的名片
	 *
	 * @access public
	 * @return void
	 */
	public function collect_update() {
		$fields = array(
			'card_id',
		);

		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$card_id = preg_replace('/[^\d]+/', '', $data['card_id']);
		if (empty($card_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		$group_id = isset($data['group_id']) ? preg_replace('/[^\d]+/', '', $data['group_id']) : 0;
		$mark 	  = isset($data['mark']) ? trim($data['mark']) : '';
		empty($group_id) && $group_id = 0;

		$info_data = array(
			'group_id' 	=> $group_id,
			'mark' 		=> $mark,
		);

		try {
			g('card') -> update_map($card_id, $info_data);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 删除一张收藏的名片
	 *
	 * @access public
	 * @return void
	 */
	public function collect_delete() {
		$fields = array(
			'card_id',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$card_id = preg_replace('/[^\d]+/', '', $data['card_id']);
		if (empty($card_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '参数不完整');
		}

		try {
			g('card') -> delete_map($card_id);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 企业内部分享自己的名片
	 *
	 * @access public
	 * @return void
	 */
	public function share_inside() {		
		try {
			g('card') -> share_inside();
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		cls_resp::echo_busy();
	}

	/**
	 * 导出为vcf文件
	 *
	 * @access public
	 * @return void
	 */
	public function educe_card() {
		$card_id = get_var_value('card_id');

		$card_id = preg_replace('/[^\d]+/', '', $card_id);
		empty($card_id) && cls_resp::echo_err(cls_resp::$Busy, '参数不完整');

		$table = g('card') -> info_table;
		$fields = array(
			'cn_name',
			'mobile',
			'`call`',
			'email',
		);
		$fields = implode(',', $fields);
		$condition = array(
			'card_id=' => $card_id,
		);
		$info = g('ndb') -> select($table, $fields, $condition);

		!is_array($info) && cls_resp::echo_busy();
		$info = $info[0];

		$data = <<<EOF
BEGIN:VCARD
VERSION:2.1
N;LANGUAGE=zh-cn;CHARSET=UTF-8:;{$info['cn_name']}
FN;CHARSET=UTF-8:{$info['cn_name']}
TEL;WORK;VOICE:{$info['call']}
TEL;CELL;VOICE:{$info['mobile']}
EMAIL;PREF;INTERNET:{$info['email']}
END:VCARD

EOF;
		header('Content-Type: text/x-vcard; charset=utf-8');
		header('Content-Disposition: attachment; filename='.time().'.vcf;');

		die($data);
	}
}