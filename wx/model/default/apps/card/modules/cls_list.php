<?php
/**
 * 名片夹控制器
 *
 * @author LiangJianMing
 * @create 2014-12-20
 */

class cls_list extends abs_app_base {
	public function __construct() {
		parent::__construct('card');
	}
	
	/**
	 * 分组列表展示
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		$list = g('card') -> get_group_list();
		$share_list = g('card') -> get_share_list();
		$groups = g('card') -> get_groups();
		$com_info = g('card') -> get_company();
		$contact_list = g('card') -> get_contact_list();

		$contact_total = count($contact_list);

		if (!empty($contact_list)) {
			$tmp_arr = array();
			foreach ($contact_list as $val) {
				$first_py = ($val['first_py'] < 'A' || $val['first_py'] > 'Z') ? '#' : $val['first_py'];
				!isset($tmp_arr[$first_py]) && $tmp_arr[$first_py] = array();
				$tmp_arr[$first_py][] = $val;
			}
			unset($val);
			$contact_list = $tmp_arr;
			unset($tmp_arr);
			ksort($contact_list);
		}

		$data = array(
			'0' => array(
				'group_name' => '默认分组',
				'childs' => array(),
			),
		);

		$tmp_groups = array();
		foreach($groups as $val) {
			$tmp_groups[$val['group_id']] = $val;
			$data[$val['group_id']] = array(
				'group_id'	=> $val['group_id'],
				'group_name' => $val['name'],
				'childs' => array(),
			);
		}
		unset($val);
		$groups = $tmp_groups;

		$ex_list = array();
		foreach($list as $val) {
			$ex_list[$val['card_id']] = 1;

			$index = $val['group_id'];
			!isset($data[$val['group_id']]) && $index = 0;
			$data[$index]['childs'][] = $val;
		}
		unset($val);

		foreach ($data as &$val) {
			$val['count'] = count($val['childs']);
		}
		unset($val);

		$com_list = array(
			'group_name' => isset($com_info['name']) ? $com_info['name'] : '企业内部分享',
			'childs' => array(),
		);
		foreach ($share_list as $val) {
			!isset($ex_list[$val['card_id']]) && $com_list['childs'][] = $val;
		}
		unset($val);

		$com_list['count'] = count($com_list['childs']);

		$default_list = array(
			'default' => $data[0],
			'company' => $com_list,
		);
		unset($data[0]);
		$custom_list = array_values($data);

		global $app, $module, $action;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=list&a=show';
		$view_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show';
		$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=edit';
		$add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=bak_add';
		$group_add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=104';
		$group_edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=105';
		$group_del_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=106';

		g('smarty') -> assign('default_list', $default_list);
		g('smarty') -> assign('custom_list', $custom_list);

		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('view_url', $view_url);
		g('smarty') -> assign('edit_url', $edit_url);
		g('smarty') -> assign('add_url', $add_url);

		g('smarty') -> assign('group_add_url', $group_add_url);
		g('smarty') -> assign('group_edit_url', $group_edit_url);
		g('smarty') -> assign('group_del_url', $group_del_url);

		g('smarty') -> assign('contact_list', $contact_list);
		g('smarty') -> assign('contact_total', $contact_total);

		$filename = str_replace('cls_', '', __CLASS__).'_'.__FUNCTION__.'.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}
}