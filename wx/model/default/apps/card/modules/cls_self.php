<?php
/**
 * 我的名片控制器
 *
 * @author LiangJianMing
 * @create 2014-12-20
 */

class cls_self extends abs_app_base {
	//模板列表
	public $mod_list = array();

	public function __construct() {
		$app_name = 'card';
		parent::__construct($app_name);

		$conf_file = SYSTEM_APPS.$app_name.'/module.config.php';

		if (file_exists($conf_file)) {
			$this -> mod_list = include($conf_file);
			!is_array($this -> mod_list) && $this -> mod_list = array();
		}
	}
	
	/**
	 * 自身名片初始化页面
	 *
	 * @access public
	 * @return void
	 */
	public function init() {
		if (($self_info = g('card') -> get_self())) {
			global $app, $module, $action;
			$show_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show';
			header('Location:'.$show_url);
			exit;
		}

		try {
			$conf = g('card') -> get_conf();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}

		$logo_url = $conf['logo_url'];
		$mod_list = $conf['mod_list'];
		unset($conf['logo_url']);
		unset($conf['mod_list']);

		$useful_mods = array();
		$this_mod_list = $this -> mod_list;

		!is_array($mod_list) && $mod_list = array();

		foreach ($mod_list as $val) {
			isset($this_mod_list[$val]) && $useful_mods[] = $this_mod_list[$val];
		}
		$mod_list = $useful_mods;

		$mod_key = $self_info['mod_key'];

		$map = g('card') -> name_map;

		global $app, $module, $action;
		$ajax_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=107';
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show';

		g('smarty') -> assign('conf', $conf);
		g('smarty') -> assign('logo_url', $logo_url);
		g('smarty') -> assign('mod_key', $mod_key);

		g('smarty') -> assign('map', $map);
		g('smarty') -> assign('mod_list', $mod_list);

		g('smarty') -> assign('ajax_url', $ajax_url);
		g('smarty') -> assign('info_url', $info_url);

		$filename = 'self_init.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}

	/**
	 * 编辑自身名片
	 *
	 * @access public
	 * @return void
	 */
	public function edit() {
		if (!($self_info = g('card') -> get_self())) {
			global $app, $module, $action;
			$init_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=self&a=init';
			header('Location:'.$init_url);
			exit;
		}
		
		try{
			$conf = g('card') -> get_conf();
			$info = g('card') -> get_self_detail();
		}catch (SCException $e) {
			cls_resp::echo_busy();
		}
		$logo_url = $conf['logo_url'];
		$mod_list = $conf['mod_list'];
		unset($conf['logo_url']);
		unset($conf['mod_list']);

		$useful_mods = array();
		$this_mod_list = $this -> mod_list;

		!is_array($mod_list) && $mod_list = array();

		foreach ($mod_list as $val) {
			isset($this_mod_list[$val]) && $useful_mods[] = $this_mod_list[$val];
		}
		$mod_list = $useful_mods;

		$mod_key = $self_info['mod_key'];

		$map = g('card') -> name_map;

		global $app, $module, $action;
		$save_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=108';
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show';

		g('smarty') -> assign('mod_list', $mod_list);
		g('smarty') -> assign('mod_key', $mod_key);
		g('smarty') -> assign('info', $info);
		g('smarty') -> assign('conf', $conf);
		g('smarty') -> assign('map', $map);

		g('smarty') -> assign('save_url', $save_url);
		g('smarty') -> assign('info_url', $info_url);

		$filename = 'self_edit.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}

	public function add_pre() {
		global $app, $module, $action;
		$init_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=self&a=init';

		g('smarty') -> assign('init_url', $init_url);

		$filename = 'self_add_card.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}
}