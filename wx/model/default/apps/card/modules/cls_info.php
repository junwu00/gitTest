<?php
/**
 * 名片信息控制器
 *
 * @author LiangJianMing
 * @create 2014-12-20
 */

class cls_info extends abs_app_base {
	public function __construct() {
		parent::__construct('card');
	}
	
	/**
	 * 收藏名片
	 *
	 * @access public
	 * @return void
	 */
	public function card_add() {
		$card_id = get_var_value('card_id');
		$share_id = get_var_value('share_id');
		if (empty($card_id) || empty($share_id)) {
			die('参数缺失!');
		}

		//标识当前浏览者是否名片拥有者
		$is_owner = FALSE;
		//标识名片是否备份名片
		$is_bak = FALSE;

		$check = g('card') -> get_check($card_id);
		!$check && die('该名片不存在!');

		$check['is_copy'] == 1 && die('非法访问');

		!empty($check['self_id']) && $check['self_id'] == $_SESSION[SESSION_VISIT_USER_ID] && die('非法访问!');

		if ($check['user_id'] == $_SESSION[SESSION_VISIT_USER_ID]) {
			$is_owner = TRUE;
		}else {
			$check = g('card') -> check_own($card_id);
			if (!$is_bak && $check) {
				$is_owner = TRUE;
			}
		}
		$is_owner && die('非法访问');

		try{
			$info = g('card') -> get_card($card_id);
		}catch (SCException $e) {
			die('名片不存在!');
		}
		$groups = g('card') -> get_groups();

		$tmp_groups = array();
		if (is_array($groups)) foreach ($groups as $val) {
			$tmp_groups[$val['group_id']] = $val;
		}
		unset($val);
		$groups = $tmp_groups;

		unset($info['mod_key']);
		unset($info['is_share']);
		unset($info['mod_list']);
		unset($info['mod_list']);

		global $app, $module, $action;
		$add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=109';

		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show&card_id='.$card_id;

		g('smarty') -> assign('card_id', $card_id);
		g('smarty') -> assign('share_id', $share_id);
		g('smarty') -> assign('groups', $groups);

		g('smarty') -> assign('info', $info);

		g('smarty') -> assign('add_url', $add_url);
		g('smarty') -> assign('info_url', $info_url);

		$filename = 'card_add.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}

	/**
	 * 添加备份名片
	 *
	 * @access public
	 * @return void
	 */
	public function bak_add() {
		$share_id = (int)get_var_value('share_id');
		$info = get_var_value('info');
		if (!empty($info)) {
			$info = g('des') -> decode($info);
			$info = json_decode($info, TRUE);
			$info = is_array($info) ? $info : array();
		}else {
			$info = array();
		}

		$groups = g('card') -> get_groups();

		$tmp_groups = array();
		if (is_array($groups)) foreach ($groups as $val) {
			$tmp_groups[$val['group_id']] = $val;
		}
		unset($val);
		$groups = $tmp_groups;

		$map = g('card') -> name_map;

		$fields = array(
			'sel_msg'	=>
				array(
					'cn_name'	=> $map['cn_name'],
					'en_name'	=> $map['en_name'],
				),
			'msg'  		=>	
				array(
					'mobile'	=> $map['mobile'],
					'call'		=> $map['call'],
					'email'		=> $map['email'],
					'qq'		=> $map['qq'],
					'wx'		=> $map['wx'],
					'fax'		=> $map['fax'],
				),	
			'com_msg'		=>
				array(
					'com_name'	=> $map['com_name'],
					'com_job'	=> $map['com_job'],
					'com_addr'	=> $map['com_addr'],
				),
			'other_msg'		=>
				array(),
		);
		
		global $app, $module, $action;
		$add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=101';
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=list&a=show';

		g('smarty') -> assign('add_url', $add_url);
		g('smarty') -> assign('list_url', $list_url);

		g('smarty') -> assign('info', $info);
		g('smarty') -> assign('fields', $fields);
		g('smarty') -> assign('map', $map);
		g('smarty') -> assign('groups', $groups);

		g('smarty') -> assign('share_id', $share_id);

		$filename = 'bak_add.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}

	/**
	 * 编辑名片
	 *
	 * @access public
	 * @return void
	 */
	public function edit() {
		$card_id = get_var_value('card_id');
		empty($card_id) && die('参数缺失!');

		//标识当前浏览者是否名片拥有者
		$is_owner = FALSE;
		//标识名片是否备份名片
		$is_bak = FALSE;

		$check = g('card') -> get_check($card_id);
		!$check && die('该名片不存在!');

		$check['is_copy'] == 1 && $is_bak = TRUE;

		!empty($check['self_id']) && $check['self_id'] == $_SESSION[SESSION_VISIT_USER_ID] && die('非法访问!');

		if ($check['user_id'] == $_SESSION[SESSION_VISIT_USER_ID]) {
			$is_owner = TRUE;
		}else {
			$check = g('card') -> check_own($card_id);
			if (!$is_bak && $check) {
				$is_owner = TRUE;
			}
		}
		!$is_owner && die('非法访问');

		$func = $is_bak ? 'get_card_bak' : 'get_card';
		try{
			$info = g('card') -> $func($card_id);
		}catch (SCException $e) {
			die('名片不存在!');
		}
		$groups = g('card') -> get_groups();
		$map_info = g('card') -> get_map_info($card_id);

		$tmp_groups = array();
		if (is_array($groups)) foreach ($groups as $val) {
			$tmp_groups[$val['group_id']] = $val;
		}
		unset($val);
		$groups = $tmp_groups;

		$this_group_id = is_array($map_info) ? $map_info['group_id'] : 0;

		global $app, $module, $action;
		
		$save_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102';
		$delete_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=103';
		
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show&card_id='.$card_id;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=list&a=show';

		$map = g('card') -> name_map;

		g('smarty') -> assign('card_id', $card_id);
		g('smarty') -> assign('this_group_id', $this_group_id);
		g('smarty') -> assign('groups', $groups);

		g('smarty') -> assign('info', $info);

		g('smarty') -> assign('save_url', $save_url);
		g('smarty') -> assign('delete_url', $delete_url);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('list_url', $list_url);

		g('smarty') -> assign('map', $map);

		$filename = 'bak_edit.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}

	/**
	 * 查看名片
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		$des_key = $key = get_var_value('key');
		$is_wx_state = get_var_value('is_wx');
		if (empty($key)) {
			if (!isset($_SESSION[SESSION_VISIT_USER_ID])) die('拒绝访问!');
			$card_id = get_var_value('card_id');

			global $app, $module, $action;

			if (empty($card_id)) {
				$self_info = g('card') -> get_self();
				$self_add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=self&a=add_pre';
				if (!$self_info) {
					header('Location:'.$self_add_url);
					exit;
				}
				$card_id = $self_info['card_id'];
			}

			$data = array(
				'card_id'  => $card_id,
				'share_id' => $_SESSION[SESSION_VISIT_USER_ID],
				'app_id'   => $_SESSION[SESSION_VISIT_CORP_ID],
				'com_id'   => $_SESSION[SESSION_VISIT_COM_ID],
			);
			$json = json_encode($data);
			$key = g('des') -> encode($json);

			$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m='.$module.'&a='.$action;
			$card_url = $this_url.'&free=1&is_wx=2&key='.$key;

			header('Location:'.$card_url);
		}

		$key = g('des') -> decode($key);
		$data = json_decode($key, TRUE);
		!is_array($data) && die('系统繁忙!');
		$card_id 	= $data['card_id'];
		$share_id 	= $data['share_id'];
		if (empty($card_id) || empty($share_id)) die('参数异常!');

		//标识当前浏览者是系统用户还是游客
		$is_user = isset($_SESSION[SESSION_VISIT_USER_ID]) ? TRUE : FALSE;
		//标识当前浏览者是否名片拥有者
		$is_owner = FALSE;
		//标识当前名片是否浏览者自身的名片
		$is_self = FALSE;
		//标识名片是否备份名片
		$is_bak = FALSE;
		//标识名片名片是否已分享到公司内部
		$is_share = FALSE;
		//标识是否在微信客户端打开
		$is_wx = FALSE;

		$check = g('card') -> get_check($card_id);
		!$check && die('名片不存在!');

		$check['is_copy'] == 1 && $is_bak = TRUE;
		!empty($check['self_id']) && $is_user && $check['self_id'] == $_SESSION[SESSION_VISIT_USER_ID] && $is_self = TRUE;

		if ($is_user) {
			if ($check['user_id'] == $_SESSION[SESSION_VISIT_USER_ID]) {
				$is_owner = TRUE;
			}else {
				$check = g('card') -> check_own($card_id);
				if (!$is_bak && $check) {
					$is_owner = TRUE;
				}
			}
		}

		$func = $is_bak ? 'get_card_bak' : 'get_card';
		
		try{
			$info = g('card') -> $func($card_id);
		}catch (SCException $e) {
			die('名片不存在!');
		}
		$mod_key = isset($info['mod_key']) ? $info['mod_key'] : '';
		isset($info['is_share']) && (int)$info['is_share'] != 0 && $is_share = TRUE;

		unset($info['mod_key']);
		unset($info['is_share']);
		unset($info['mod_list']);

		if (isset($info['logo_val'])) {
			$logo_val = $info['logo_val'];
			unset($info['logo_val']);
			g('smarty') -> assign('logo_val', $logo_val);
		}

		if ($is_owner && !$is_self) {
			$map_info = g('card') -> get_map_info($card_id);

			$mark = '';
			$this_group_id = 0;

			if (is_array($map_info)) {
				!$is_bak and $mark = $map_info['mark'];
				$this_group_id = $map_info['group_id'];
			}

			$groups = g('card') -> get_groups();

			$tmp_groups = array();
			if (is_array($groups)) foreach ($groups as $val) {
				$tmp_groups[$val['group_id']] = $val;
			}
			unset($val);
			$groups = $tmp_groups;

			g('smarty') -> assign('mark', $mark);
			g('smarty') -> assign('this_group_id', $this_group_id);
			g('smarty') -> assign('groups', $groups);
		}

		if(empty($is_wx_state)){
			$is_wx = g('common') -> is_wx()?2:1;
		}else{
			$is_wx = $is_wx_state;
		}


		global $app, $module, $action;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=list&a=show';
		$share_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=112';
		$educe_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=113&free=1&card_id='.$card_id;
		$delete_collect_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=111';

		if ($is_self) {
			$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=self&a=edit';
		}else {
			$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m='.$module.'&a=edit&card_id='.$card_id;
		}

		if (!$is_bak) {
			$collect_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=109&share_id='.$share_id.'&card_id='.$card_id;

			if ($is_owner) {
				$collect_update_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=110';
				g('smarty') -> assign('collect_update_url', $collect_update_url);
			}
		}else {
			$tmp_arr = array(
				'cn_name' => $info['personal']['cn_name']['val'],
				'en_name' => $info['personal']['en_name']['val'],
				'mobile' => $info['contact']['mobile']['val'],
				'call' => $info['contact']['call']['val'],
				'qq' => $info['contact']['qq']['val'],
				'wx' => $info['contact']['wx']['val'],
				'email' => $info['contact']['email']['val'],
				'fax' => $info['contact']['fax']['val'],
				'com_name' => $info['company']['com_name']['val'],
				'com_job' => $info['company']['com_job']['val'],
				'com_addr' => $info['company']['com_addr']['val'],
			);
			$str = g('des') -> encode(json_encode($tmp_arr));
			$collect_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=bak_add&share_id='.$share_id.'&info='.$str;
		}

		g('smarty') -> assign('card_id', $card_id);
		g('smarty') -> assign('share_id', $share_id);

		g('smarty') -> assign('is_user', $is_user);
		g('smarty') -> assign('is_owner', $is_owner);
		g('smarty') -> assign('is_bak', $is_bak);
		g('smarty') -> assign('is_self', $is_self);
		g('smarty') -> assign('is_share', $is_share);
		g('smarty') -> assign('is_wx', $is_wx);
		g('smarty') -> assign('mod_key', $mod_key);
		
		log_write(json_encode($info));
		g('smarty') -> assign('info', $info);
		
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('edit_url', $edit_url);
		g('smarty') -> assign('share_url', $share_url);
		g('smarty') -> assign('collect_url', $collect_url);
		g('smarty') -> assign('educe_url', $educe_url);
		g('smarty') -> assign('delete_collect_url', $delete_collect_url);

		//begin---------------分享相关的数据---------------------------
		if ($is_wx) {
			$share_crop_id = $is_user ? '' : $data['app_id'];
			$share_com_id = $is_user ? 0 : $data['com_id'];
			parent::oauth_jssdk();

			$to_share_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m='.$module.'&a='.$action.'&free=1&is_wx=2&key='.$des_key;
			$share_icon_url = SYSTEM_HTTP_APPS_ICON.'Bcard.png';
			$share_desc = '';
			$card_per_name = isset($info['personal']['cn_name']['val']) ? $info['personal']['cn_name']['val'] : '';
			$share_title = '您收到一张来自 '.$card_per_name.' 的名片哦，快藏起来吧！';

			g('smarty') -> assign('to_share_url', $to_share_url);
			g('smarty') -> assign('share_icon_url', $share_icon_url);
			g('smarty') -> assign('share_desc', $share_desc);
			g('smarty') -> assign('share_title', $share_title);
		}

		//end---------------分享相关的数据-------------------------------------

		$filename = str_replace('cls_', '', __CLASS__).'_'.__FUNCTION__.'.html';
		if (!$is_bak) {
			$mod_list = array();
			$conf_file = SYSTEM_APPS.'card/module.config.php';

			if (file_exists($conf_file)) {
				$mod_list = include($conf_file);
				!is_array($mod_list) && $mod_list = array();
			}

			if (empty($mod_key)) {
				log_write('用户模板丢失，user_id='.$_SESSION[SESSION_VISIT_USER_ID], 'card:'.__CLASS__.':'.__FUNCTION__);
				die('模板已失效!');
			}
			if (!isset($mod_list[$mod_key])) {
				$this_module = array_shift($mod_list);
				log_write('系统模板丢失，mod_key='.$mod_key, 'card:'.__CLASS__.':'.__FUNCTION__);
				//die('模板已失效!');
			}else {
				$this_module = $mod_list[$mod_key];
			}

			$filename = $this_module['file'];
			g('smarty') -> assign('module_class', $this_module['class']);
		}

		$update_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102';
		g('smarty') -> assign('update_url', $update_url);
		g('smarty') -> show($this -> temp_path.$filename);
	}
}