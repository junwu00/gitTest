<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'card_list_show.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/list.css'
	),
    'card_list_show.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/list.js'
    ),
	'card_business_card_model_model1.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card.css',
		SYSTEM_ROOT . 'apps/card/static/css/model1.css'
	),
	'card_business_card_model_model1.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_business_card_model_model2.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card2.css',
		SYSTEM_ROOT . 'apps/card/static/css/model2.css'
	),
	'card_business_card_model_model2.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_business_card_model_model3.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card3.css',
		SYSTEM_ROOT . 'apps/card/static/css/model3.css'
	),
	'card_business_card_model_model3.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_business_card_model_model4.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card4.css',
		SYSTEM_ROOT . 'apps/card/static/css/model4.css'
	),
	'card_business_card_model_model4.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_business_card_model_model5.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card5.css',
		SYSTEM_ROOT . 'apps/card/static/css/model5.css'
	),
	'card_business_card_model_model5.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_business_card_model_model6.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/model_card.css',
		SYSTEM_ROOT . 'apps/card/static/css/card5.css',
		SYSTEM_ROOT . 'apps/card/static/css/model5.css'
	),
	'card_business_card_model_model6.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/model.js'
    ),
	'card_bak_add.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_bak_add.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/bak.js'
    ),
	'card_bak_edit.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_bak_edit.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/bak.js'
    ),
	'card_card_add.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_card_add.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js'
    ),
	'card_card_edit.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_card_edit.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js'
    ),
	'card_info_show.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_info_show.js' => array(
        SYSTEM_APPS . 'card/static/js/cardholder/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/bak.js'
    ),
	'card_self_add_card.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_self_add_card.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/index.js'
    ),
	'card_self_edit.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_self_edit.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/bak.js'
    ),
	'card_self_init.css' => array(
		SYSTEM_ROOT . 'apps/card/static/css/index.css'
	),
	'card_self_init.js' => array(
        SYSTEM_APPS . 'card/static/js/businesscard/index.js',
        SYSTEM_APPS . 'card/static/js/cardholder/bak.js'
    )

);

return $arr;

/* End of this file */