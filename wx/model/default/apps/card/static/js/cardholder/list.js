$(document).ready(function() {
	init_create_new_group();
	init_show_tip();
	plus_minus_people();
	init_delete();
});

function init_delete(){
	$('.delete_group').unbind('click').bind('click',function(e){
		e.stopPropagation();
		var btn =$(this);
		frame_obj.comfirm('你确定要删除该分组？',function(){
			$('#ajax-url').val($('#group_del_url').val());
			var data ={
					group_id:btn.attr('i')
			};
			frame_obj.do_ajax_post(btn,'',JSON.stringify(data),function(response){
				if(response['errcode']==0){
					location.reload();
				}else{
					frame_obj.alert(response['errmsg']);
				}
			},undefined,undefined,undefined,'删除中……');
		});
	});
}

function init_create_new_group(){
	$('.new').unbind('click').bind('click',function(){
		$('.exit').click();
		$("#group_name").val("");
		$("#group_mark").val("");
		$("#group_people #content").find('.list').remove();
		$(".create_group").show();
		$(".middle").hide();
		$("#bottom-menu").hide();
	});
	$("#create_new_group").unbind('click').bind('click',function(){
		$("#group_name").val("");
		$("#group_mark").val("");
		$("#group_people #content").find('.list').remove();
		$(".create_group").show();
		$(".middle").hide();
		$("#bottom-menu").hide();
	});
	$(".cancel_create_new_group").unbind('click').bind('click',function(){
		turn_select_off();
		$(".create_group").hide();
		$(".middle").show();
		$('#wrap').show();
		$("#bottom-menu").show();
	});
}
function init_show_tip(){
	$(".right-menu li").bind('click',function(){
		$(this).find(".divbox").fadeIn();
		setTimeout("codefans()",800);
	});
	$(".right-menu li").bind('touchstart',function(){
		$(this).find(".divbox").show();
	});
	$(".right-menu li").bind('touchend',function(){
		$(this).find(".divbox").hide();
	});
}
function codefans(){
	$(".divbox").fadeOut();
}
function plus_minus_people(){
	$("#group_people #content .list").each(function(){
		$(this).bind('click',function(){
			if($(this).find('i').css('display')!='none'){
				var id = $(this).attr('i');
				$(".group-list li").each(function(){
					if($(this).find('i').attr('i')==id){
						$(this).find("i").attr('class','icon-circle');
						$(this).find("i").attr('s','');
					}
				});
				$(this).remove();
			}
		});
	});
	$(".minus").unbind('click').bind('click',function(){
		if($(this).parent().find('.list i').css('display')=='none'){
			$(this).parent().find(".list i").show();
		}else{
			$(this).parent().find(".list i").hide();
		}
	});
	$(".plus").unbind('click').bind('click',function(){
		$('.float-group').hide();
		$('.list-contact').show();
	});
}

function turn_select_off(){
	  var choose_btn = $('.icon-ok-circle');
	  choose_btn.attr('s','');
	  choose_btn.addClass('icon-circle');
	  choose_btn.removeClass('icon-ok-circle');
}

