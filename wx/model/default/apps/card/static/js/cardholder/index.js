$(document).ready(function() {

	$("ul#nav ul").hide();
	$('li.down:has(ul)').click(function(event){
        if ($(this).hasClass('ul-show')) {
        	$(this).removeClass('ul-show')
	        $(this).children("ul:eq(0)").slideUp(400);
        	$(this).find("i:first").attr('class','card-icon');
        } else {
        	$(this).addClass('ul-show')
	        $(this).children("ul:eq(0)").slideDown(400);
        	$(this).find("i:first").attr('class','card-icon-hover');
        }
    });
	$("ul#nav ul li").bind('click',function(e){
		 e.stopPropagation();
	});
	init_selected();
	init_group_people();
	$(function(){
		$('body').click(function(evt) {  
			 if($(evt.target).parents(".contain-bottom").length==0){
				 $(".float-group").hide();
			 }
		});
	});
	init_edit();
	choose_ground();
	if($('#is_wx').val()==1){
		show_choose_content();
	}
});

function show_choose_content(){
	$('.choose_ground_content').css('display','block');
	$('.container').css('display','none');
}

function init_edit(){
	$('.k_value').unbind('click').bind('click',function(){
		if($(this).hasClass('be_write')){
			$('.e_key').html($(this).attr('n'));
			$('.e_value').val("");
		}else{
			$('.e_key').html($(this).attr('n'));
			$('.e_value').val($(this).html());
		}
		$('.e_value').attr('v',$(this).attr('k'));
		
		$('.edit_content').css('display','block');
		$('.container').css('display','none');
		$('.e_value').focus();
	});
	
	$('.exit').unbind('click').bind('click',function(){
		turn_select_off();
		$('.edit_content').css('display','none');
		$('.choose_ground_content').css('display','none');
		$('.group_manage').css('display','none');
		$('.container').css('display','block');
	});
	
	$('.save').unbind('click').bind('click',function(){
		if($(this).attr('t')==1){
			var edit_value = $('.k_value[k='+$('.e_value').attr('v')+']');
			edit_value.html($('.e_value').val());
			edit_value.removeClass('be_write');
			edit_value.addClass('write');
		}else if($(this).attr('t')==2){
			$('.ground_msg').attr('i',$('.option_active').attr('i'));
			$('.ground_msg').html($('.option_active').html());
		}
		
		var data = get_data();
		var btn = $(this);
		$('#ajax-url').val($('#update_url').val());
		frame_obj.do_ajax_post(btn, '', JSON.stringify(data), save_back,undefined,undefined,undefined,'保存中……');
	});
}

function save_back(data){
	if(data['errcode']==0){
		$('.exit').click();
	}else{
		frame_obj.alert(data['errmsg'],'',function(){
			location.reload();
		});
	}
}

function get_data(){
	var data = new Object();
	data.card_id = $('#card_id').val();
	data.cn_name = $('.write.k_value[k=cn_name]').html();
	data.en_name = $('.write.k_value[k=en_name]').html();
	data.mobile = $('.write.k_value[k=mobile]').html();
	data.call = $('.write.k_value[k=call]').html();
	data.qq = $('.write.k_value[k=qq]').html();
	data.email = $('.write.k_value[k=email]').html();
	data.wx = $('.write.k_value[k=wx]').html();
	data.fax = $('.write.k_value[k=fax]').html();
	data.com_name = $('.write.k_value[k=com_name]').html();
	data.com_job = $('.write.k_value[k=com_job]').html();
	data.com_addr = $('.write.k_value[k=com_addr]').html();
	data.group_id = $('.ground_msg').attr('i');
	
	return data;
}

function init_selected(){
	$(".btn-group").unbind('click').bind('click',function(){
		$('#wrap').show();
		$("#all").hide();
		$(this).addClass('btn-select');
		$(".btn-all").removeClass('btn-select');
	});
	$(".btn-all").unbind('click').bind('click',function(){
		$('#wrap').hide();
		$("#all").show();
		$(this).addClass('btn-select');
		$(".btn-group").removeClass('btn-select');
	});
	$("#select-group").unbind('click').bind('click',function(){
		$('.container').css('display','none');
		$('.choose_ground_content').css('display','block');
	});
	$("#cancel").unbind('click').bind('click',function(){
		$("#group-menu").hide();
		$(".float-group").hide();
	});
}

function select($group_id, $group_name){
	$("#group_id").val($group_id);
	$("#group_name").html($group_name);
	$("#group-menu").hide();
	$(".float-group").hide();
}
function init_group_people(){
	$(".group-list li").unbind('click').bind('click',function(){
		if($(this).find("i").attr('class')=='icon-circle'){
			$(this).find("i").attr('class','icon-ok-circle');
			$(this).find("i").attr('s','select');
		}else{
			$(this).find("i").attr('class','icon-circle');
			$(this).find("i").attr('s','');
		}
	});
}

function show_tips($num) {
	if ($num == 1) {
		$(".float-tip").show();
	}else if ($num == 2) {
		$(".float-tip2").show();
	}
}

function choose_ground(){
	$('.ground_msg').unbind('click').bind('click',function(){
		
	});
	$('.ground-option>span').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.option_active').removeClass('option_active');
			$(this).addClass('option_active');
		});
	});
}

function turn_select_off(){
	  var choose_btn = $('.icon-ok-circle');
	  choose_btn.attr('s','');
	  choose_btn.addClass('icon-circle');
	  choose_btn.removeClass('icon-ok-circle');
}
