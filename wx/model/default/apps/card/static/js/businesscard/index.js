$(document).ready(function(){
	var count=$(".img div .card-img").length;
	$('.img div:first').css('width',count*280+10+'px');
	init_del_save();
});

function init_del_save(){
	$(".btn-share").unbind('click').bind('click',function(){
		$(".float-tip").show();
	});
	$(".float-tip").unbind('click').bind('click',function(){
		$(".float-tip").hide();
	});
	$(".del_card").unbind('click').bind('click',function(){
		
	});
	$(".img div  img").unbind('click').bind('click',function(){
		$(".img div .card-img").each(function(){$(this).find("i").hide();$(this).find('.select-img').hide();});
		$(this).parent().find("i").show();
		$(this).parent().find(".select-img").show();
	});
	$(".save_card").unbind('click').bind('click',function(){
		var data = get_conf_data();
//		frame_obj.do_ajax_post($(this), 'save', JSON.stringify(data));
	});
}
function get_conf_data(){
	var data = new Object();
	var name = $("#name").val();
	var en_name = $("#en_name").val();
	var phone = $("#phone").val();
	var tel = $("#tel").val();
	var email = $("#email").val();
	var fax = $("#fax").val();
	var com_name = $("#com_name").val();
	var position = $("#position").val();
	var address = $("#address").val();
	if(name=="" || phone==""){
		frame_obj.alert('姓名和手机号码不能为空');
		return;
	}
	data.name = name;
	data.en_name = en_name;
	data.phone = phone;
	data.tel = tel;
	data.email = email;
	data.fax = fax;
	data.com_name = com_name;
	data.position = position;
	data.address = address;
}