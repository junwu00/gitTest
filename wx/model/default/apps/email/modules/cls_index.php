<?php
/**
 * 企业邮箱默认页面
 * @author yangpz
 *
 */
class cls_index extends abs_app_base {
	/** 消息类型 */
	protected static $MsgType = array(
		1 => 'image',
		2 => 'voice',
		3 => 'video',
		4 => 'file',
	);
	
	public function __construct() {
		parent::__construct('email');
	}
	
	/**
	 * 默认页面
	 */
	public function index() {
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		$act =  get_var_value("act");
		$act1 = get_var_post('ajax_act');	
			if($act1=='save'){
				$this -> save_email($sid);
				return;
			}
			
			switch($act) {
				case 'search':
					$this -> user_search();
					break;
				case 'contact':
					$this -> show_contact1();
					break;
				case 'login':
					$this -> user_login($sid);
					break;
				case 'upload':
					$this -> upload_form();
					break;
				default:
					$service = g("email_service")-> get_by_root($_SESSION[SESSION_VISIT_DEPT_ID]);
					if (!$service) 
						cls_resp::show_err_page(array('管理员未配置企业邮箱','EMAIL'));
						
					$result = g('user') -> get_by_ids($sid);
					$email = g('email_user') -> get_by_ids($sid);
					if(!$email || !$this -> check_email($service[0],$email[0])){
						g('smarty') -> assign('user',$result[0]);
						g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
						g('smarty') -> show($this -> temp_path.'index/login.html');
					}else{
						self::show_contact();
						g('smarty') -> assign('uid', $result[0]['id']);
						g('smarty') -> assign('email_user', $email[0]);
				        g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
						g('smarty') -> show($this -> temp_path.'index/index.html');
					}
			}
	}
	
	
	/**
	 * 显示收件人（通讯录列表）
	 */
	public function show_contact(){
		$user = g("user") -> search($_SESSION[SESSION_VISIT_DEPT_ID], '');
		$dept_list = g('dept') -> list_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
		$conf = g('cont_conf') -> get_by_dept($_SESSION[SESSION_VISIT_DEPT_ID]);
		
		$secret = json_decode($conf['secret_list'], TRUE); 	//获取保密区的员工
		$arr = array();		//首字母列表
		
		$dept_user = array();
	    foreach ($user as $key => &$u){
    		if(!in_array($u['id'], $secret)) {
	    		$a = g('pinyin') -> get_first_charter(mb_substr($u['name'], 0, 1, 'UTF-8'));
		    	if(!in_array($a, $arr)){
		    	  $arr[] = $a;
		    	}
	        	$user[$key]['ch_name']= $a;	    
	        	$dept_user[] = $user[$key];   	
    		}	 
        }
        sort($arr);
        $users = $this -> get_user_dept($dept_user);
      	$this -> get_list_tree($tree, $dept_list, $dept_user);
      	$result = $this -> user_general($secret);
      	$result && g('smarty') -> assign('gen',$result);
      	g('smarty') -> assign('sid', $_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('tree',$tree);
		g('smarty') -> assign('ch',$arr);
		g('smarty') -> assign('dept',$dept_list);
		g('smarty') -> assign('user',$users);
		g('smarty') -> assign('wxacct', $_SESSION[SESSION_VISIT_USER_WXACCT]);
	//	g('smarty') -> show($this -> temp_path.'index/contact.html');
	}
	
	/**
	 * 显示邮件
	 */
	public function show_email(){	
		$act1 = get_var_post('ajax_act');	
		if($act1 == 'file'){
			$this -> send_file();
		}
		log_write('员工id='.$_SESSION[SESSION_VISIT_USER_ID].'开始查看邮件','EMAIL');
		
		$messageID = get_var_value('messageID');
		$_SESSION['me_id']=$messageID;
		$result = g('email_email') -> get_email_mid($messageID);
		log_write($result);
		$email_email_id = $result[0]['id'];
		$email_user = g('email_user') ->  get_by_email($result[0]['email_user_id']);
		$user = g('user')-> get_by_id($email_user[0]['user_id']); 
		$user['email'] =$email_user[0]['email'];
		$email_attach = g('email_attach') -> get_email_mid($email_email_id);
		if(!empty($email_attach)) foreach($email_attach as $key => $attach){
			$email_attach[$key]['attach_url'] = MEDIA_URL_PREFFIX.$attach['attach_url']; 
		}
		if($result[0]['receive_user']){
			$receive = json_decode($result[0]['receive_user'], TRUE);
			g('smarty') -> assign('receive',$receive);
		}
	    if($result[0]['cc_user']) {
	    	$cc_user = json_decode($result[0]['cc_user'], TRUE);
	    	g('smarty') -> assign('cc_user',$cc_user);
	   	}
		if($result[0]['bcc_user']) {
			$bcc_user = json_decode($result[0]['bcc_user'], TRUE);
			g('smarty') -> assign('bcc_user', $bcc_user);
		}
		
		$result[0]['time']=date('Y年m月d日  H:i:s',$result[0]['send_date']);
	    g('smarty') -> assign('email_user',$user);
		g('smarty') -> assign('email',$result[0]);
		g('smarty') -> assign('email_attach',$email_attach);
		g('smarty') -> show($this -> temp_path.'show_email.html');
	}
	/**
	 * 回复或转发邮件
	 */
	public function reply_email(){
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		$email = g('email_user')-> get_by_ids($sid);
		if($email[0]['email']==""||$email[0]['password']==""){
			$_SESSION['url']=SYSTEM_HTTP_DOMAIN.'index.php?app=email&a=reply_email&count=1';
			$this->login();
		}else{
			$com_id = $_SESSION[SESSION_VISIT_DEPT_ID];
			$service = g("email_service")-> get_by_root($com_id);
			$from_user = array();
			$from_user['email']=$email[0]['email'];
			$from_user['password']= g('des')->decode($email[0]['password']);
			if(!g('email')->test($service[0],$from_user)){
				$_SESSION['url']=SYSTEM_HTTP_DOMAIN.'index.php?app=email&a=reply_email&count=1';
				$this->login();
			}
		}
	    $act1 = get_var_post('ajax_act');	
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		if($act1=='save'){$this -> save_email($sid);}
		
		log_write('开始回复邮件','EMAIL');
		$act = get_var_value("act");
		
		$messageID = $_SESSION['me_id'];
		$result = g('email_email') -> get_email_mid($messageID);
		
		$email_email_id = $result[0]['id'];
		$email_user = g('email_user') ->  get_by_email($result[0]['email_user_id']);
		$user = g('user')-> get_by_id($email_user[0]['user_id']); 
		$email_user[0]['user_name'] = $user['name'];
		$email_user[0]['id'] = $user['id'];
        if(get_var_value("count") ==1){
        	$arr=$_SESSION["email"]="";
        }else{
			$arr = $_SESSION["email"];
        }
       
		$arr1 = $this -> get_sesseion_user($arr,$email_user,$result,$act);
		if($result[0]['receive_user']){
			$receive = json_decode($result[0]['receive_user'], TRUE);
			g('smarty') -> assign('receive',$receive);
		}
	    if($result[0]['cc_user']) {
	    	$cc_user = json_decode($result[0]['cc_user'], TRUE);
	    	g('smarty') -> assign('cc_user',$cc_user);
	    }
		if($result[0]['bcc_user']) {
			$bcc_user = json_decode($result[0]['bcc_user'], TRUE);
			g('smarty') -> assign('bcc_user',$bcc_user);
		}
		
		self::show_contact();
		g('smarty') -> assign('act',$act);
		g('smarty') -> assign('email_user',$email_user[0]);
		g('smarty') -> assign('email',$result[0]);
		g('smarty') -> assign('arr',$arr1);
		g('smarty') -> show($this -> temp_path.'reply_email.html');
	}
	
//-----------------------------细节实现----------------------------------------------------------- TODO
	/**
	 * 验证员工是否已验证过邮箱，或更新过邮箱密码
	 */
	private function check_email($service,$email) {
		if(!$email || empty($email['email']) || empty($email['password'])) {
			return false;
		} else {
			$from_user = array();
			$from_user['email'] = $email['email'];
			$from_user['password'] = g('des') -> decode($email['password']);
			if (!g('email') -> test($service, $from_user)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 验证要发送到哪个邮箱（包括企业内外）
	 * @param unknown_type $user_list	邮箱ID列表
	 */
	private function check_recive_user($user_list) {
		$ret = array();
		foreach ($user_list as $user) {
			if(!empty($user)) {
				$result1 = g('user') -> get_by_ids($user);
				if($result1) {		//发送到企业内员工
					$result2 = g('email_user') -> get_by_ids($user);
					if($result2){	//优先发到企业邮箱
						$result1[0]['qy_email'] = $result2[0]['email']; 
					}
					$ret[] = $result1[0];
					
				}else{				//发送到企业外员工
					$ret[] = $user;
				}
			}
		}
		unset($user);
		return $ret;
	}
	
	/**
	 * 返回下载链接
	 * Enter description here ...
	 */
	private function send_file(){
		log_write('返回下载链接给用户','EMAIL');
		try {
			$data = parent::get_post_data(array('id'));
			$attach_id = $data['id'];
			$result = g('email_attach') -> get_by_ids($attach_id);
			$attach_name = $result[0]['attach_name'];
			$hash = $result[0]['hash'];
			$a = substr(strrchr($attach_name,'.'), 1); 
			
			$user = g('user') -> get_by_ids($_SESSION[SESSION_VISIT_USER_ID]);
			$acct = $user[0]['acct'];
	        $user_list = array($acct);
			$token = parent::get_access_token();
			
			if((is_null($result[0]['media_id']) || empty($result[0]['media_id']))		//未上传过微信服务器
				|| ((time() - $result[0]['media_time']) > cls_upload::$MaxMediaTime)) {	//上传过期
				log_write('开始获取media id');
				$media = cls_upload::get_media_id($hash, $token, $attach_name);
				$con = array('id=' => $result[0]['id']);
				$email_attach = array(
					'media_id' => $media['media_id'],
					'media_time' => $media['created_at'],
				);
			    g('email_attach') -> update($con, $email_attach);
			    $media_id = $media['media_id'];
			    log_write('获取media id成功','EMAIL');
			    
	        } else {
	        	$media_id = $result[0]['media_id'];
	        }
		
			$type = '0';
			if(in_array($a, cls_upload::$ImageExt)) {
				$type = '1';
			} elseif (in_array($a, cls_upload::$voiceExt)) {
				$type = '2';
			} elseif (in_array($a, cls_upload::$videoExt)) {
				$type = '3';
			} elseif (in_array($a, cls_upload::$FileExt)) {
				$type = '4';
			}
			
			$agent_id = $this -> app_id;
			switch ($type) {
				case 1:	//图片
					g('wxqy') -> send_img($token, $user_list, $agent_id, $media_id);
					cls_resp::echo_ok();
					break;
					
				case 2:	//音频
					g('wxqy') -> send_voice($token, $user_list, $agent_id,$media_id);
					cls_resp::echo_ok();
					break;
					
				case 3:	//视频
					$this -> send_video($art_id, $token, $user_list, $agent_id,$media_id);
					cls_resp::echo_ok();
					break;
					
				case 4:	//文件
					g('wxqy') -> send_file($token, $user_list, $agent_id, $media_id);
					cls_resp::echo_ok();
					break;
					
				default:
					throw new SCException('文件类型不支持','EMAIL');
					
			}
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取S_session接收人的信息
	 * @param unknown_type $arr
	 */
	private function get_sesseion_user($arr,$email_user,$email,$act){
		if(!empty($arr)){
			$re = explode(',', $arr['re']);		//收件人ID列表
			$co = explode(',', $arr['co']);		//抄送人
			$se = explode(',', $arr['se']);		//密送人
			
	        $filehash = explode(',', $arr['filehash']);
			$filename = explode(',', $arr['filename']);
			$filepath = explode(',', $arr['filepath']);
			
		} else {
			$re = array();$co = array(); $se = array();
			$filehash = array();$filename = array(); $filepath = array();
		}
		
		if(get_var_value('act') == 'send_order' && get_var_value("count") == 1){
			$email_attach = g('email_attach') -> get_email_mid($email[0]['id']);
			foreach ($email_attach as $e){
				$filehash[] = g('des')->encode($e['id']);		//hash字段对应ID
				$filename[] = $e['attach_name'];
				$filepath[] = MEDIA_URL_PREFFIX.$e['attach_url']; 
			}
			unset($e);
			g('smarty') -> assign('filename', $filename);
			g('smarty') -> assign('filepath', $filepath);
			g('smarty') -> assign('filehash',$filehash);
		}
		
		$result = $this -> get_email_user($email);
		$count = get_var_value("count");
		
		$a=array();
		if($count == 1 && get_var_value('act') != "send_order"){
			$a[] = array('name'=> $email_user[0]['user_name'],'id'=>$email_user[0]['id'],'email'=>$email_user[0]['email']);
		}
		
		if(!empty($re)){
			foreach ($re as $r){
				if($r !=""){
					$result1 = g('user') -> get_by_ids($r);
					if($result1){
						$result2 = g('email_user') -> get_by_ids($r);
						if($result2){
							$result1[0]['qy_email'] = $result2[0]['email']; 
						}
						$a[]=$result1[0];
					}else{
						$a[]=$r;
					}
				}
			}	
		}
		
		$arr['re']=$a;
		if($count==1 && get_var_value("act")=='reply_all'){
			$arr['re']=$a+$result[0]['re'];
		}
		$a=array();
		if(!empty($co)){
			foreach ($co as $r){
				if($r !=""){
					$result1 = g('user') -> get_by_ids($r);
					if($result1){
						$result2 = g('email_user') -> get_by_ids($r);
						if($result2){
							$result1[0]['qy_email'] = $result2[0]['email']; 
						}
						$a[]=$result1[0];
					}else{
						$a[]=$r;
					}
				}
			}
		}
		$arr['co'] = $a;
		if($count==1 && get_var_value("act")=='reply_all'){
			$arr['co']=$result[0]['co'];
		}
		$ar3=array();
		if(!empty($se)){
			foreach ($se as $r){
				if($r !=""){
					$result1 = g('user') -> get_by_ids($r);
					if($result1){
						$result2 = g('email_user') -> get_by_ids($r);
						if($result2){
							$result1[0]['qy_email'] = $result2[0]['email']; 
						}
						$ar3[]=$result1[0];
					}else{
						$ar3[] = $r;
					}
				}
			}
		}
		$arr['se'] = $ar3;

		$label = "";
		if($result[0]['re']!=""){
			foreach($result[0]['re'] as $e){
				if(!is_string($e)){
					$label = $label.$e['name'].'('.$e['email'].')&nbsp;';
				}else{
					$label = $label.$e.'&nbsp;';
				}
			}
		}
		$lab = "";
		if($result[0]['co']!=""){
			$lab ='<p>抄送人：';
			foreach($result[0]['co'] as $e){
				if(!is_string($e)){
					$lab = $lab.$e['name'].'('.$e['email'].')&nbsp;';
				}else{
					$lab = $lab.$e.'&nbsp;';
				}
			}
			'</p>';
		}
		$str = '<br><div class="reply_text"  style="margin:8px 10px;">'
					.'<p style="font-size:11px;">----原始邮件------</p>'
					.'<div class="reply_main" style="background-color:#eee ;font-size:11px;-moz-border-radius:4px;-wekit-border-radius:4px; border-radius:4px; border:1px solid #eee; width:95%;margin:8px 0;padding:3px 12px;">'
					.'<p>发件人：'.$email_user[0]['user_name'].'('.$email_user[0]['email'].')</p>'
					.'<p>发送时间：'.$result[0]['time'].'</p>'
					.'<p>收件人：'.$label.'</p>'
					.$lab
					.'</div>'
					.'<p>'.$result[0]['content'].'</p></div>';
		$arr['time']=$result[0]['time']=date('Y年m月d日  H:i:s',$result[0]['send_date']);
		if($count==1){
			$arr['text'] = $str;
			if(get_var_value('act') == "send_order")
				$arr['theme'] = '转发：'.$result[0]['subject'];
			else  
				$arr['theme'] = '回复：'.$result[0]['subject'];
		}
		return $arr;
	}
	
	
	/**
	 * 获取邮件里接收人的信息
	 * Enter description here ...
	 * @param unknown_type $result
	 */
	private function get_email_user($result){
	
		if($result[0]['receive_user']){
				$receive = json_decode($result[0]['receive_user'], TRUE);
				$re = array();
				foreach ($receive as $k=>$r){
					if($k==$r){
						$re[] = $k;
					}else{
						$condition = array(
							'email' => $k,
							'name' => $r,
						);
						$re[] = $condition;
					}
				}
				
				$result[0]['re']=$re;
			}else{$result[0]['re']="";}
			if($result[0]['cc_user']){
				$cc_user = json_decode($result[0]['cc_user'], TRUE);
				$co = array();
				foreach ($cc_user as $k=>$r){
					if($k==$r){
						$co[] = $k;
					}else{
						$condition = array(
							'email' => $k,
							'name' => $r,
						);
						$co[] = $condition;
					}
				}
				$result[0]['co'] = $co;
			}else{$result[0]['co']="";}
			if($result[0]['bcc_user']){
				$bcc_user = json_decode($result[0]['bcc_user'], TRUE);
				$se = array();
				foreach ($bcc_user as $k=>$r){
					if($k==$r){
						$se[] = $k;
					}else{
						$condition = array(
								'email' => $k,
								'name' => $r,
						);
						$se = $condition;
					}
				}
				$result[0]['se'] = $se;
			}else{$result[0]['se']="";}
			$result[0]['time']=date('Y年m月d日  H:i:s',$result[0]['send_date']);
			return $result;
	}
	/**
	 * 保存并发送邮件
	 */
	private function save_email($id){
		log_write('员工id='.$id.'开始发送邮件','EMAIL');
		$email_user = g('email_user') -> get_by_ids($id);
		$user_id = $email_user[0]['id'];
		
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$com = g("email_service")-> get_by_root($com_id);
		
		$fields = array('re_id', 'co_id', 'se_id','text','theme','filehash','filename','filepath');	
		g('db') -> begin_trans();
		try {
			
			$data = parent::get_post_data($fields);
			$re_id = explode(',', $data['re_id']);
			$co_id = explode(',', $data['co_id']);
			$se_id = explode(',', $data['se_id']);
			$theme = $data['theme'];
			$filehash = explode(',', $data['filehash']);
			$filename = explode(',', $data['filename']);
			$text = $data['text'];
			
			$user_list = array();
			$arr2 = $this -> re_arr($re_id,$user_list);
			$receive_user = $arr2[0];
	
			$arr2 = $this -> re_arr($co_id,$user_list);
			$cc_user = $arr2[0];
			
			$arr2 = $this -> re_arr($se_id,$user_list);
			$bcc_user = $arr2[0];
		
			$message_id = uniqstr();
		
			$arr = array(
				"email_user_id " => $user_id,
				"content" => $text,
				"subject" => $theme,
				"message_id" => $message_id,
				"send_date" => time(),
				"receive_user" => json_encode($receive_user),
				"cc_user" => json_encode($cc_user),
				"bcc_user" => json_encode($bcc_user),
			);
			log_write('保存邮件信息','EMAIL');
			$new_eamil_id = g('email_email') -> save($arr);
			log_write('更新附件信息','EMAIL');
			$attach = g('email_attach') -> update_attach($new_eamil_id,$filehash);
			
			$all_attach = array();
			if($data['filepath']!=""){
				$ar1 = array();
				foreach ($attach as $file){
					$ar1['path']=SYSTEM_DATA.$file['attach_rela_path'];
					$ar1['name']=$file['attach_name'];
					$all_attach[]= $ar1;
				}
			}	
			log_write('开始发送邮件'.json_encode($all_attach),'EMAIL');
			if(!g('email')->send($com[0],$email_user[0],$receive_user,$cc_user,$bcc_user,$theme,$text,$all_attach,$message_id)){
					throw new SCException('发送不成功');
			}
			log_write('发送成功','EMAIL');
			$url = SYSTEM_HTTP_DOMAIN.'index.php?app=email&m=index&a=show_email&messageID='.$message_id;
			if($user_list){
				if(!$this ->tmp($theme, $user_list, $url)){log_write('提醒不成功','EMAIL');}
			}
			$_SESSION['email']="";
			log_write('提醒成功','EMAIL');
			g('db')->commit();
			cls_resp::echo_ok();	
		} catch (SCException $e) {
			g('db')-> rollback();
			$_SESSION['email']="";
			cls_resp::echo_exp($e);	
		}
		
	}
	/**
	 * 
	 */
	private  function re_arr($re_id,&$user_list){
		$receive_user = array();
		foreach ($re_id as $r){
			if($r!=""){
				$u = g('user') -> get_by_ids($r);
				if($u){
					$result2 = g('email_user') -> get_by_ids($u[0]['id']);
					if($result2){
						$receive_user[$result2[0]['email']]=$u[0]['name'];
						$user_list[]=$u[0]['acct'];
					}else{
						if($u[0]['email'])
							$receive_user[$u[0]['email']]=$u[0]['name'];
						else
							$receive_user[] = $u[0]['name'];	
						$user_list[]=$u[0]['acct'];
					}
				}else{
					$result3 = g('email_user') -> search(array('email'=>$r));
					if($result3){
						$re = g('user') -> get_by_ids($result3[0]['user_id']);
						$receive_user[$r] =$re[0]['name'];
						$user_list[]=$re[0]['acct'];
					}else{
						$re = g('user') -> search($_SESSION[SESSION_VISIT_DEPT_ID],array('email'=>$r));
						if($re){
							$receive_user[$r] =$re[0]['name'];
							$user_list[]=$re[0]['acct'];
						}else{
							$receive_user[$r] =$r;
						}
					}
				}
			}
		}
		$arr = array();
		$arr[]=$receive_user;
		return $arr;
	}
	
	/**
	 * 加载公司员工
	 */
	private function show_contact1(){
			$re_id = get_var_value("re_id");
			$co_id = get_var_value("co_id");
			$se_id = get_var_value("se_id");
			$theme = get_var_value("theme");
			$filehash = get_var_value("filehash");
			$text = get_var_value("text");
			$plus = get_var_value("plus");
			$filename = get_var_value("filename");
			$filepath = get_var_value("filepath");
			$btn = get_var_value('btn');
			$act1 = get_var_value('act1');
			log_write('filehash='.$filehash);
			$_SESSION["email"]=array('act1'=>$act1,'btn'=>$btn,'re'=>$re_id,'co'=>$co_id,'se'=>$se_id,'theme'=>$theme,'filehash'=>$filehash,'text'=>$text,'plus'=>$plus,'filename'=>$filename,'filepath'=>$filepath);
			
			echo json_encode(array('state'=>'1','s' => json_encode($_SESSION)));
	}

	/**
	 * 返回员工常用联系人
	 * @param unknown_type $secret	//保密员工ID数组
	 */
	private function user_general($secret){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$com = g("company")-> search($com_id);
		$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
		$general_list = json_decode($conf['general_list'], TRUE);
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		$result = $this -> get_user_dept(g('user')-> get_by_ids($sid));
		$general_user = g('user_general') -> get_by_ids($sid);
		$dlist = json_decode($general_user[0]['del_list'], TRUE);	
		$user = array();	
		foreach ($general_list as $d){
			if($d != $sid && !in_array($d, $secret) && !($dlist && in_array($d, $dlist))){
				$u = g('user')-> get_by_ids($d);
				if($u){
					$user[] = $u[0];
				}
			}
		}
		if($general_user){
			$list = json_decode($general_user[0]['general_list'], TRUE);
			foreach ($list as $d){
				if(!in_array($d, $secret)){
					$u = g('user')-> get_by_ids($d);
					if($u && !in_array($u[0], $user)){
						$user[] = $u[0];
					}
				}
			}
		}
		$users = $this -> get_user_dept($user);
		return $users;
	}
	
	/**
	 * 模糊查询
	 */
	private function user_search(){
		$text = get_var_value("text");
		
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$com = g("company")-> search($com_id);
		
		$text = get_var_value("text");
		$arr = array();
		$u_re = g('user') -> search($com[0]['dept_id'],array('name'=>$text));
		if($u_re){
			foreach ($u_re as $k => $r){
				$re = g("email_user") -> get_by_ids($r['id']);
				if($re){
					$u_re[$k]['qy_email']=$re[0]['email'];
				}
				$u_re[$k]['qy_email']="";
				$arr[]=$u_re[$k];
			}
		}
		$q_re = g('email_user') -> search(array('email'=>$text));
		if($q_re != ""){
			foreach ($q_re as $k => $q){
				$re = g("user") -> get_by_ids($q['user_id']);
				if($re){
					$re[0]['qy_email'] = $q['email'];
					if(in_array($re[0], $arr)){
						foreach ($arr as $a){
							if($a.id==$re[0]['id']){$arr[$k]=$re[0];}
						}
					}else{
						$arr[]=$re[0];
					}
				}
			}
		}else{
			$a_re = g('user') -> search($com[0]['dept_id'],array('email'=>$text));
			if(isset($arr)){
				if($a_re !=""){
					foreach ($a_re as $k=>$a){
						$a['qy_email']="";
						if(!in_array($a, $arr)){
							$arr[]=$a;
						}
					}
				}
			}else{
				foreach ($a_re as $k=>$a){
					$a['qy_email']="";
					if(!in_array($a, $arr)){
						$arr[]=$a;
					}
				}
			}
		}
		
		$conf = g('cont_conf') -> get_by_dept($com[0]['dept_id']);
		$ar = array();
		$secret = json_decode($conf['secret_list'], TRUE);
		if(isset($arr)){
			foreach ($arr as $a){
				if(!in_array($a['id'], $secret)){
					$ar[]=$a;
				}
			}
		}
		$sid= $_SESSION[SESSION_VISIT_USER_ID];
		echo json_encode($ar?array('state'=>0,"data"=>$ar):array('state'=>1,'msg'=>'没有相关数据'));
	}
	
	/**
	 * 验证邮箱
	 * Enter description here ...
	 */
	private function user_login($id){
		log_write('开始验证邮箱','EMAIL');
		$email = get_var_value("email");
		$password = get_var_value("password");
		$re = g('email_user')-> get_by_ids($id);
		
		$root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
		$service = g('email_service')->get_by_root($root_id);
		$from_user = array();
		$from_user['email']=$email;
		$from_user['password']=$password;
		
		try{
			g('email')->test($service[0],$from_user);
			log_write('邮箱验证成功,开始插入本地数据库','EMAIL');
			if(!empty($re)){	
				$result = g('email_user') -> update($re[0]['id'],$email,$password);
			}else{
				$result = g('email_user') -> save($id,$email,$password);
			}
			log_write("插入数据成功");
			$_SESSION['email']="";
			cls_resp::echo_ok(cls_resp::$OK);
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
		
	}

/**
	 * 加载公司部门与全部员工
	 * Enter description here ...
	 * @param unknown_type $html
	 * @param unknown_type $dept_list 本公司所有部门
	 * @param unknown_type $user	本公司所有员工
	 */
	private function get_list_tree(&$html, $dept_list, $user, $root=FALSE) {
		$display = '';
		if (!$root) {
			$display = 'style="display:none;"';
		}
		
		if (!is_array($dept_list)){$html .='</ul>'; return;}
		foreach ($dept_list as $dept) {
			if (isset($dept['childs'])) {
				$html .= '<li ><div class="dlabel" >'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
				foreach ($user as $u){
					$arr = json_decode($u['dept_list'], TRUE);
					 if(in_array($dept['id'], $arr)){
					 	if($u['pic_url']!=""){
					 		$img=$u['pic_url'];
					 	}else{
					 		$img=SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}

					 	$html .= '<li  style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" n="'.$u['name'].'" m="'.$img.'" d="'.$u['id'].'"></i></li>';
					 }
				}
				unset($u);
			
				$this -> get_list_tree($html, $dept['childs'],$user);
				$html .= '</ul>';
				
			} else {
				$html .= '<li ><div class="dlabel">'.$dept['name'].'<i class="icon-circle-blank dcir"></i></div><ul class="two">';
			    foreach ($user as $u){
			    	$arr = json_decode($u['dept_list'], TRUE);
					 if(in_array($dept['id'], $arr)){
						 if($u['pic_url']!=""){
					 		$img = $u['pic_url'];
					 	}else{
					 		$img = SYSTEM_HTTP_DOMAIN."apps/contact/static/images/face.png";
					 	}
					 	$html .= '<li style="padding-left:30px;" id="last" uid="'.$u['id'].'"><img src="'.$img.'" style="width:28px;height:28px;"/><span style="padding-left:10px;">'.$u['name'].'</span><i class=" icon-circle-blank dcir" n="'.$u['name'].'" m="'.$img.'" d="'.$u['id'].'"></i></li>';
					 }
				}
				unset($u);
				$html .= '</ul>';
			}
			$html .= '</li>';
		}
	}
	/**
	 * 获取用户的部门信息
	 * Enter description here ...
	 * @param unknown_type $user 本公司的所有员工
	 */
	private function get_user_dept($user){
		foreach ($user as $key=>$u){
			$dept_id = json_decode($u['dept_list'], TRUE);
			$arr = array();
			
			foreach ($dept_id as $id){
				$dept = g('dept')-> get_dept_by_id($id);
				if($dept){
					$arr[] = $dept['name'];
				}
			}
			$user[$key]['dept_name'] = $arr;
		}
		return $user;
	}
	
	/**
	 * 发送提醒信息
	 * @param unknown_type $theme
	 * @param unknown_type $user_list
	 * @param unknown_type $url
	 */
	private function tmp($theme,$user_list,$url) {
		log_write('发送提醒信息'.json_encode($user_list),'EMAIL');
		try{
			parent::send_single_news($user_list, $theme, "", $url);
			return true;
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	/**
	 * 素材上传
	 */
	private function upload_form(){
		log_write('开始上传素材','EMAIL');
		try {
			$a = get_var_post('type');
			if(in_array($a, cls_upload::$ImageExt)){
				$type = 1;
			}elseif (in_array($a, cls_upload::$voiceExt)){
				$type = 2;
			}elseif (in_array($a, cls_upload::$videoExt)){
				$type = 3;
			}elseif (in_array($a, cls_upload::$FileExt)){
				$type = 5;
			}
			
			$max_size = 0;
			switch ($type) {
				case 1:	//图片 1M
					$max_size = 1024000;
					break;
				case 2:	//音频 2M
					$max_size = 2048000;
					break;
				case 3:	//视频 10M
					$max_size = 10240000;
					break;
				case 5:	//文件 10M
					$max_size = 10240000;
					break;
			}
			$local_file = cls_upload::upload($max_size);		//上传到服务器 ，发送邮件需要用到
			log_write('上传到服务器成功：','EMAIL');

			$file = cls_upload::cloud_local_upload($local_file['type'],$local_file['real_path']);	//将服务器文件上传到云存储
			log_write('上传到云存储成功','EMAIL');
			
			log_write('上传素材成功','EMAIL');
			$file_id = g('email_attach') -> save_attach($file['hash'],$local_file['name'],$file['path'],$local_file['rela_path']);
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('hash' => g('des')->encode($file_id), 'url' => MEDIA_URL_PREFFIX.$file['path'], 'name' => $local_file['name'], 'ext' => $local_file['ext'], 'size' => $local_file['size']/1024));
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}

	
	
	
}
// end of file