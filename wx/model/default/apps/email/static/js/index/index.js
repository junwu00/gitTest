$(document).ready(function(){
	$("ul.two").first().css('display','inline');
	$(".btn-submit").unbind('click').bind('click',function(){
		var btn = $(this);
		btn != undefined && btn.attr('disabled', 'disabled');
		var email = $("#email").val();
		var psw = $("#psw").val();
		if(email!=""){
			if(!check_data(email,'email')){
				frame_obj.alert("邮箱格式有误!");
				btn != undefined && btn.attr('disabled', false);
				return;
			}
		}
		if(psw==""||email==""){
			frame_obj.alert("密码或邮箱不能为空");
			btn != undefined && btn.attr('disabled', false);
			return;
		}
		
		var url = $("#url").val()+'&act=login';
		$.post(
				url,
				{
					"email":email,
					"password":psw,
				},
				function(data){
					btn != undefined && btn.attr('disabled', false);
					if(data['errcode']==0){
						window.location.href=$("#url").val()+"&count=1";
					}else{
						frame_obj.alert('验证失败');
					}
				},
				"json"
		);
	});
	$(".lab_right").unbind('click').bind('click',function(){
		if($(this).html()=='收起'){
			$(this).parent().parent().find('.show_list').hide();
			$(this).html('展开');
		}else{
			$(this).parent().parent().find('.show_list').show();
			$(this).html('收起');
		}
	});
	
	init_upload();
	init_select_contact();
	init_select();
	init_sent_contact();
	init_show_tip();
	var text = $("#content .test_box").html();
	if(text=="<br>"){$("#content .test_box").append('<div id="tips">邮件内容</div>');}
	$("#content .test_box").bind('click',function(){
		$(this).find("#tips").remove();
	});
	
	$('.content').click(function(){
		$(this).children('.content-input').focus();
		$(this).children('.content-input').css('width','90%');
//		$('#receive-name').focus();
//		$('#receive-name').css('width','90%');
	});
	$('.content-input').blur(function(){
		$(this).css('width','1px');
	});
});


function init_upload(){
	var url = $("#url").val() + '&act=upload';
	frame_obj.ajax_upload($('#file'), $('#file').parent(), url, before_fun, upload_img_complete);
}
function before_fun(){
	var val = $("#file").val();
	var k = val.substr(val.indexOf(".")+1);
	if(!(k == 'jpeg' || k == 'jpg')){
		frame_obj.alert('暂不支持此类型');
		return false;
	}else{
		$("#type").val(k);
		$("#filename").html(val);
		frame_obj.lock_screen('上传中 ...');
		return true;
	}
}

function upload_img_complete(response){
	var resp = $.parseJSON(response);
	if(resp['errcode']==0){
	var str ='<div><img style="width:35px;height:35px;" class="filename"  h="'+resp['info']['hash']+'" p="'+resp['info']['url']+'" src="'+resp['info']['url']+'"/><span>'+resp['info']['name']+'</span><span class="token-input-delete-token group_item_close" onclick="javascript:$(this).parent().remove();"></span></div>';	
			$(".img-list").append(str);
	}else{
		frame_obj.alert(resp['errmsg']);
	}
}
function in_array(a,arr){
	for(var i in arr){
		if(arr[i]==a){
			return true;
		}
	}
	return false;
}

function editText(a){
	var str ="<label class='label-name re-name' i='"+$(a).attr('i')+"' e='"+$(a).attr('e')+"' n='"+$(a).attr('n')+"' onclick='javascript:$(this).remove();'>"+$(a).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
		$("#receive").find("input").before(str);
		$("#receive-name").val("");
		$(".float-receive-name").hide();
}

function copy_change(text){
	if(text!=""){
		var list = '';
		$('.re-name').each(function() {	
				list += ',' + $(this).attr('n');
		});
		if (list != '')	list = list.substr(1);
		s=list.split(",");
		
		var url = $("#url").val()+'&act=search';
		$.post(
				url,
				{
					"text":text,
				},
				function(data){
					if(data['state']==0){
						var str="";
						var arr = data['data'];
						for(var i in arr){
							if(!in_array(arr[i]['name'],s)){
								if(arr[i]['qy_email']!=""){var e=arr[i]['qy_email'];}else{var e=arr[i]['email']; }
								str += "<div onclick='edit_copy(this)' i='"+arr[i]['id']+"' n='"+arr[i]['name']+"' e='"+e+"'><p>"+arr[i]['name']+"</p><p>"+e+"</p></div>" ;
							}
						}
						$(".float-copy-name").show();
						$(".float-copy-name").html(str);
					}
				},
				"json"
		);
	}
}

function edit_copy(a){
	var str ="<label class='label-name co-name' i='"+$(a).attr('i')+"' e='"+$(a).attr('e')+"' n='"+$(a).attr('n')+"' onclick='javascript:$(this).remove();'>"+$(a).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
	$("#copyed").find("input").before(str);
	$("#copy-name").val("");
	$(".float-copy-name").hide();
}
function secret_change(text){
	if(text!=""){
		var list = '';
		$('.re-name').each(function() {	
				list += ',' + $(this).attr('n');
		});
		if (list != '')	list = list.substr(1);
		s=list.split(",");
		
		var url = $("#url").val()+'&act=search';
		$.post(
				url,
				{
					"text":text,
				},
				function(data){
					if(data['state']==0){
						var str="";
						var arr = data['data'];
						for(var i in arr){
							if(!in_array(arr[i]['name'],s)){
								if(arr[i]['qy_email']!=""){var e=arr[i]['qy_email'];}else{var e=arr[i]['email']; }
								str += "<div onclick='edit_secret(this)' i='"+arr[i]['id']+"' n='"+arr[i]['name']+"' e='"+e+"'><p>"+arr[i]['name']+"</p><p>"+e+"</p></div>" ;
							}
						}
					
						$(".float-secret-name").show();
						$(".float-secret-name").html(str);
					}
				},
				"json"
		);
	}
}

function edit_secret(a){
	var str ="<label class='label-name se-name' i='"+$(a).attr('i')+"' e='"+$(a).attr('e')+"' n='"+$(a).attr('n')+"' onclick='javascript:$(this).remove();'>"+$(a).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
	$("#secret-list").find("input").before(str);
	$("#secret-name").val("");
	$(".float-secret-name").hide();
}

function init_select_contact(){
	$(".dlabel i").bind('click',function(e){
		 e.stopPropagation();
	});
	$("#last i").bind('click',function(e){
		 e.stopPropagation();
	});
	$(".btn-dept").bind('click',function(){
		$(".dept_all").show();
		$(".general").hide();
		$(".all").hide();
		$(this).addClass('btn-selected');
		$('.btn-all').removeClass('btn-selected');
		$('.btn-general').removeClass('btn-selected');
	});
	$(".btn-all").bind('click',function(){
		$(".dept_all").hide();
		$(".general").hide();
		$(".all").show();
		$(this).addClass('btn-selected');
		$('.btn-dept').removeClass('btn-selected');
		$('.btn-general').removeClass('btn-selected');
	});
	$(".btn-general").bind('click',function(){
		$(".dept_all").hide();
		$(".general").show();
		$(".all").hide();
		$(this).addClass('btn-selected');
		$('.btn-all').removeClass('btn-selected');
		$('.btn-dept').removeClass('btn-selected');
	});
	
	$(".dlabel i").each(function(){
		$(this).bind('click',function(e){
			
			if($(this).attr('class')!='icon-ok-circle dcir'&& $(this).attr('class')!='dcir icon-ok-circle'){
				$(this).parent().parent().find('.dcir').removeClass('icon-circle-blank');
				$(this).parent().parent().find('.dcir').addClass('icon-ok-circle');
				$("#last i").each(function(){
					if($(this).attr('class')=='icon-ok-circle dcir' || $(this).attr('class')=='dcir icon-ok-circle'){
						$(this).parent().addClass('selected');
						var u = $(this).parent().attr('uid');
						var id = $(this).attr('d');
						var m = $(this).attr('m');
						var n = $(this).attr('n');

						var img = $(".bottom_sure img").map(function(){if($(this).attr("i")=="") over=1;return $(this).attr('i')}).get().join(",");
						var imgs = img.split(',');
						if(!in_array(id,imgs)){
							var str = "<div onclick='init_remove(this);'><img class='choosed' src='"+m+"' n='"+n+"' i='"+id+"' /><i class='icon-remove-sign'></i></div>";
							$(".bottom_sure").append(str);
							$('.bottom_sure .count').html($('.bottom_sure >div').size());
						}
						$('#last i').each(function(){
							$(this).parent().addClass('selected');
							if($(this).parent().attr('uid')==u){
								 $(this).addClass('icon-ok-circle');
								$(this).removeClass('icon-circle-blank');
							}
						});
					}
				});
			}else{
				$(this).parent().parent().find('.dcir').removeClass('icon-ok-circle');
				$(this).parent().parent().find('.dcir').addClass('icon-circle-blank');
				$(this).parent().parent().parent().parent().find('.dcir:first').removeClass('icon-ok-circle');
				$(this).parent().parent().parent().parent().find('.dcir:first').addClass('icon-circle-blank');
				$("#last i").each(function(){
					if($(this).attr('class')=='icon-circle-blank dcir' || $(this).attr('class')=='dcir icon-circle-blank'){
						$(this).parent().removeClass('selected');
						var u = $(this).parent().attr('uid');
						$('.bottom_sure img[i="'+u+'"]').parent().remove();
						$('.bottom_sure .count').html($('.bottom_sure >div').size());
						$('#last i').each(function(){
							$(this).parent().removeClass('selected');
							if($(this).parent().attr('uid')==u){
								 $(this).addClass('icon-circle-blank');
								$(this).removeClass('icon-ok-circle');
							}
						});
					}
				});
			}
		});
	});

	$(" #last i").each(function(){
		$(this).unbind('click').bind('click',function(e){
			if($(this).attr('class')!='icon-ok-circle dcir'&& $(this).attr('class')!='dcir icon-ok-circle'){
				$(this).removeClass('icon-circle-blank');
				$(this).addClass('icon-ok-circle');
				var u =$(this).parent().attr('uid');
				
				var str = "<div onclick='init_remove(this);'><img class='choosed' src='"+$(this).attr('m')+"' n='"+$(this).attr('n')+"' i='"+$(this).attr('d')+"'/><i class='icon-remove-sign'></i></div>";
				$(".bottom_sure").append(str);
				$('.bottom_sure .count').html($('.bottom_sure >div').size());
				$('#last i').each(function(){
					$(this).parent().addClass('selected');
					if($(this).parent().attr('uid')==u){
						 $(this).addClass('icon-ok-circle');
						$(this).removeClass('icon-circle-blank');
					}
				});
				
			}else{
			    $(this).addClass('icon-circle-blank');
				$(this).removeClass('icon-ok-circle');
				
					$(this).parent().parent().parent().find('.dcir:first').removeClass('icon-ok-circle');
					$(this).parent().parent().parent().find('.dcir:first').addClass('icon-circle-blank');
					
					var u =$(this).parent().attr('uid');
					$('.bottom_sure img[i="'+u+'"]').parent().remove();
					$('.bottom_sure .count').html($('.bottom_sure >div').size());
					$('#last i').each(function(){
						$(this).parent().addClass('selected');
						if($(this).parent().attr('uid')==u){
							 $(this).addClass('icon-circle-blank');
							$(this).removeClass('icon-ok-circle');
						}
					});
			}
			 e.stopPropagation();
		});
	});
}
function init_remove(a){
	var num = Number($('.bottom_sure .count').html());
	$(a).remove();
	$('.bottom_sure .count').html(--num);
}
function turn(a){
	var obj = $(a).find('li i.dcir');
	obj.addClass('icon-ok-circle dcir');
	obj.removeClass('icon-circle-blank dcir');
	if($(obj).parent().parent().children('ul')!=""){turn($(obj).parent().parent().children('ul'));}
}
function rturn(a){
	var obj = $(a).find('li i.dcir');
	obj.addClass('icon-circle-blank dcir');
	obj.removeClass('icon-ok-circle dcir');
	if($(obj).parent().parent().children('ul')!=""){rturn($(obj).parent().parent().children('ul'));}
}
function in_array(a,arr){
	for(var i in arr){
		if(a== arr[i]){return true;}
	}
	return false;
}
function init_select(){
	$("#receive-name").blur(function(){
		if($.trim($(this).val())!="" && check_data($(this).val(),'email')){
			var str ="<label class='label-name re-name'  onclick='javascript:$(this).remove();' i='"+ $("#receive-name").val()+"'>"+ $("#receive-name").val()+"<span class='token-input-delete-token group_item_close'></span></label>";
			$("#receive-name").before(str);
	    }else if($(this).val()!=""){
	    	frame_obj.alert('邮箱格式错误');
	    }
		$("#receive-name").val("");
	});
	
	$("#copy-name").blur(function(){
		if($.trim($(this).val())!="" && check_data($(this).val(),'email')){
			var str ="<label class='label-name co-name'  onclick='javascript:$(this).remove();' i='"+ $("#copy-name").val()+"'>"+ $("#copy-name").val()+"<span class='token-input-delete-token group_item_close'></span></label>";
			$("#copy-name").before(str);
	    }else if($(this).val()!=""){
	    	frame_obj.alert('邮箱格式错误');
	    }
		$("#copy-name").val("");
	});
	
	$("#secret-name").blur(function(){
		if($.trim($(this).val())!="" && check_data($(this).val(),'email')){
			var str ="<label class='label-name se-name'  onclick='javascript:$(this).remove();' i='"+ $("#secret-name").val()+"'>"+ $("#secret-name").val()+"<span class='token-input-delete-token group_item_close'></span></label>";
			$("#secret-name").before(str);
		}else  if($(this).val()!=""){
			frame_obj.alert('邮箱格式错误');
		}
		$("#secret-name").val("");
	});
	
	
	$(".icon-plus").each(function(){
		$(this).bind('click',function(){
			$('.email-container').css('display','block');
			$('.container-eamil').css('display','none');
			$('.bottom_sure').children('div').each(function(){
				$(this).click();
				$('.icon-ok-circle').addClass('dcir icon-circle-blank');
				$('.icon-ok-circle').removeClass('icon-ok-circle');
			});
		});
	});
	
	$("#receive-plus").bind('click',function(){
		$('#addtype').val(1);
	});
	$("#secret-plus").bind('click',function(){
		$('#addtype').val(3);
	});
	$("#copy-plus").bind('click',function(){
		$('#addtype').val(2);
	});
	
}


function init_sent_contact(){
	$(".btn-sure").unbind('click').bind('click',function(){
		
		var html ="";
		if($('#addtype').val()==1){
			$('.choosed').each(function(){
				if($('.receive-content').children('.re-name[i="'+$(this).attr('i')+'"]').size()==0)
					html +="<label class='label-name re-name' i='"+$(this).attr('i')+"' onclick='javascript:$(this).remove();'>"+$(this).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
			});
			$('#receive-name').before(html);
		}else if($('#addtype').val()==2){
			$('.choosed').each(function(){
				if($('.copy-content').children('.co-name[i="'+$(this).attr('i')+'"]').size()==0)
					html +="<label class='label-name co-name' i='"+$(this).attr('i')+"' onclick='javascript:$(this).remove();'>"+$(this).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
			});
			$('#copy-name').before(html);
		}else if($('#addtype').val()==3){
			$('.choosed').each(function(){
				if($('.secret-content').children('.se-name[i="'+$(this).attr('i')+'"]').size()==0)
					html +="<label class='label-name se-name' i='"+$(this).attr('i')+"' onclick='javascript:$(this).remove();'>"+$(this).attr('n')+"<span class='token-input-delete-token group_item_close' ></span></label>";
			});
			$('#secret-name').before(html);
		}
		$('.email-container').css('display','none');
		$('.container-eamil').css('display','block');
	});
	$(".btn-send").bind('click',function(){
		$("#content .test_box #tips").remove();
		var data = get_conf_data();
		if(data !=""){
			frame_obj.lock_screen('发送中');
			frame_obj.do_ajax_post($(this), 'save', JSON.stringify(data),send_email_complete);
		}
	});
}

function send_email_complete(data) {
	frame_obj.unlock_screen();
	if (data.errcode == 0) {
		frame_obj.alert('发送成功','',close);
	} else {
		frame_obj.alert(data.errmsg);
	}
}
function close(){frame_obj.close_browser();}

function get_conf_data(){
	var data = new Object();
	var  re_id = $(".re-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var co_id = $(".co-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var se_id = $(".se-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var theme = $('#theme').val();
	var text = $(".test_box").html();
	var filehash = $(".filename").map(function(){if($(this).attr("h")!="") over=1;return $(this).attr('h')}).get().join(",");
	var filename = $(".filename").map(function(){if($(this).html()!="") over=1;return $(this).html()}).get().join(",");
	var filepath = $(".filename").map(function(){if($(this).attr("p")!="") over=1;return $(this).attr('p')}).get().join(",");
	if(re_id=="" && co_id=="" && se_id ==""){
		frame_obj.alert('收件人、抄送人、密送人至少要填写一个');
		return data="";
	}
	if($.trim(theme)=="" || $.trim(text)==""){
		frame_obj.alert('主题、内容不能为空');
		return data="";
	}
	data.re_id=re_id;
	data.co_id=co_id;
	data.se_id=se_id;
	data.theme=theme;
	data.text=text;
	data.filehash=filehash;
	data.filename=filename;
	data.filepath=filepath;
	return data;
}
function send(id){
	var data = new Object();
	data.id = id;
	frame_obj.do_ajax_post($(this), 'file', JSON.stringify(data),send_email_complete1);
}
function send_email_complete1(data) {
	if (data.errcode == 0) {
		frame_obj.close_browser();
	} else {
		frame_obj.alert(data.errmsg);
	}
}


function init_show_tip(){
	$("#right-menu li").bind('click',function(){
		$("#divbox").html($(this).find('a').html());
		$("#divbox").fadeIn();
		setTimeout("codefans()",800);//3秒
	});
}
function codefans(){
	$("#divbox").fadeOut();
}