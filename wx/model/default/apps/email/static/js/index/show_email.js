
$(document).ready(function(){
	$(function(){
		$('body').click(function(evt) {  
			 if($(evt.target).parents(".float-receive-name").length==0&&evt.target.name !="receive-name"){
				   $(".float-receive-name").hide(); 
				   if($("#receive-name").val()!=""){
						var str ="<label class='label-name re-name' i='"+ $("#receive-name").val()+"'>"+ $("#receive-name").val()+"<span class='token-input-delete-token group_item_close' onclick='javascript:$(this).parent().remove();'></span></label>";
						$("#receive-name").before(str);
						$("#receive-name").val("");
				   }
			 }
			 if($(evt.target).parents(".float-secret-name").length==0&&evt.target.name !="copy-name"){
				   $(".float-copy-name").hide(); 
				   if($("#copy-name").val()!=""){
						var str ="<label class='label-name co-name' i='"+ $("#copy-name").val()+"'>"+ $("#copy-name").val()+"<span class='token-input-delete-token group_item_close' onclick='javascript:$(this).parent().remove();'></span></label>";
						$("#copy-name").before(str);
						$("#copy-name").val("");
				   }
			 }
			 if($(evt.target).parents(".float-secret-name").length==0&&evt.target.name !="secret-name"){
				   $(".float-secret-name").hide();
				   if($("#secret-name").val()!=""){
						var str ="<label class='label-name se-name' i='"+ $("#secret-name").val()+"'>"+ $("#secret-name").val()+"<span class='token-input-delete-token group_item_close' onclick='javascript:$(this).parent().remove();'></span></label>";
						$("#secret-name").before(str);
						$("#secret-name").val("");
				   }
			 }
		 });
	 });
	init_send_email();
});
function init_send_email(){
	$(".btn-send1").bind('click',function(){
		var data = get_conf_data();
		if(!data)
			return;
		frame_obj.lock_screen('发送中');
		frame_obj.do_ajax_post($(this), 'save', JSON.stringify(data),send_email_complete);
	});
}
function send_email_complete(data) {
	frame_obj.unlock_screen();
	if (data.errcode == 0) {
		frame_obj.alert('发送成功','',close);
	} else {
		frame_obj.alert(data.errmsg);
	}
}
function close(){frame_obj.close_browser();}
function get_conf_data(){
	var data = new Object();
	var  re_id = $(".re-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var co_id = $(".co-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var se_id = $(".se-name").map(function(){if($(this).attr("i")!="") over=1;return $(this).attr('i')}).get().join(",");
	var theme = $('#theme').val();
	var filehash = $(".filename").map(function(){if($(this).attr("h")!="") over=1;return $(this).attr('h')}).get().join(",");
	var filename = $(".filename").map(function(){if($(this).html()!="") over=1;return $(this).html()}).get().join(",");
	var filepath = $(".filename").map(function(){if($(this).attr("p")!="") over=1;return $(this).attr('p')}).get().join(",");
	var text = $(".test_box").html();
	
	if(re_id=="" && co_id=="" && se_id){
		frame_obj.alert('收件人、抄送人、密送人至少要填写一个');
		return;
	}
	if(theme=="" || text==""){
		frame_obj.alert('主题、内容不能为空');
		return;
	}
	data.re_id=re_id;
	data.co_id=co_id;
	data.se_id=se_id;
	data.theme=theme;
	data.text=text;
	data.filehash=filehash;
	data.filename=filename;
	data.filepath=filepath;
	data.text = text;
	return data;
}