<?php
/**
 * 应用的配置文件_通讯录
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 				=> 6,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(6),
	'name' 				=> 'email',
	'cn_name' 			=> '企业邮箱',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Email.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '回复', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=email&a=reply_email&act=reply_one&count=1', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '回复全部', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=email&a=reply_email&act=reply_all&count=1', 'icon' => ''),
			array('id' => 'bmenu-3', 'name' => '转发', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=email&a=reply_email&act=send_order&count=1', 'icon' => ''),
			array('id' => 'bmenu-4', 'name' => '写邮件', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=email&count=1', 'icon' => ''),
		),
	),
	
	'wx_menu' => array(
		array('name' => '写邮件', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=email&corpurl=<corpurl>&count=1'),
	),
);

// end of file