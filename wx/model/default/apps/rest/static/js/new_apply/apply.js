var curr_type = 1;

$(document).ready(function() {
	
	$storage.init({
		key:'apply_rest',
		ckey:['#type','#reason','#start-time','#end-time','#rest-days','#rest-hours'],
	});
	
	//list_approval();
	init_start_time();
	init_end_time();
	init_type_change();
	
	init_cancel();
	init_do_apply();
	
	init_upload();
	var url=$("#page_url").val()+"&a=upload_file";
	$("#formToUpload").attr("action",url);
});

var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			/*
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			*/
			
			currUploadDiv = $('<div class="file_div" data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info file_name">' + fileName + '</div>'+
							'<span class="mupload-notice" data-new=1>开始扫描文件</span>'+
						'</div>');
			$("#file_describe").after(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('.mupload-notice[data-new=1]').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('.mupload-notice[data-new=1]').html('正在上传');
		},
		process		: function(percent) {
			$('.mupload-notice[data-new=1]').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	
    		var is_exist = false;
        	var file_key = data.hash;
        	$('.file_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
        	
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('filekey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('fext', data.file_ext);
			
			var file_ext = data.file_ext;
    		
			var optBtn = '<span class="delete_btn hide" onclick="removeFile($(this).parents(\'.file_div\'));">删除</span>';
    		if (isImg == 1) {
    			$(currUploadDiv).find('.file_info').before('<img style="width: 33px;height: 23px" src="'+mediaUrl+data.path+'">');
    			//optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		else{
    			$(currUploadDiv).find('.file_info').before('<div style="width: 32px;height: 32px;display: inline-block;position: relative;top: 5px;background-color: #91bef3;color: #fff;text-align: center;">'+file_ext[0].toUpperCase( )+'</div>');
    		}
    		currUploadDiv.append('<span class="file_action_open" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="open_action(this)"><span class="glyphicon glyphicon-chevron-right"></span></span>');
    		currUploadDiv.append('<span class="file_action_close hide" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="close_action(this)"><span class="glyphicon glyphicon-chevron-down"></span></span>');
    		$("#file_num").html('(' + $('.file_info').length + ')');
    		$('.mupload-notice[data-new=1]').html(optBtn);
    		$('.mupload-notice[data-new=1]').attr('data-hash',file_key);
    		$('.mupload-notice[data-new=1]').attr('data-new',0);
    		currUploadDiv.after('<div class="file_div file_action file_del hide" data-name="'+data.file_name+'" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="del(\'mupload-notice\',\''+file_key+'\')"><span>删除</span></div>')
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
	$("#file_num").html('(' + $('.file_info').length + ')');
}

function del(class_key,hash) {
	console.log('del');
	$('.' + class_key +'[data-hash='+hash+']').find('span')[0].click();
	$(".file_action[data-hash='"+hash+"']").remove();
}

function init_type_change() {
	$('#type').unbind('change').bind('change', function() {
		$('#start-time').val('');
		$('#end-time').val('');
		$('#rest-days input').val(0);
		$('#rest-hours input').val(0);
		
		if ($(this).val() == 0)	return;
		
		var t = $('option[value=' + $(this).val() + ']').attr('t');
		curr_type = t;
		if (t == 0) {		//最小一天
			$('#rest-hours').addClass('hide');
			$('#rest-hours').val('0');
			
		} else {			//最小一小时
			$('#rest-hours').removeClass('hide');
		}
	});
}

function init_start_time() {
	init_datetime_scroll('#start-time');
	$('#start-time').unbind('change').bind('change', function() {
		get_length();
	});
}

function init_end_time() {
	init_datetime_scroll('#end-time');
	$('#end-time').unbind('change').bind('change', function() {
		get_length();
	});
}


function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

function init_date_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'date', stepMinute: 1};

	$(element).val('').scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

function get_length() {
	start_time = $('#start-time').val();
	end_time = $('#end-time').val();

	$('#rest-days input').val(0);
	$('#rest-hours input').val(0);
	
	if (start_time != '' && end_time != '') {
		var start_day = new Date(Date.parse(start_time.replace(/-(\d*)-/, '/$1/')));
		var end_day = new Date(Date.parse(end_time.replace(/-(\d*)-/, '/$1/')));
		var hours = Math.ceil((end_day.getTime() - start_day.getTime()) / (1000 * 60 * 60));			//整数小时
		
		if (hours > 0) {
			var days = parseInt(hours / 24);	//整数天
			hours -= (24 * days);
			if (curr_type == 0 && hours > 0) {	//最小一天
				days++;
				hours=0;
			}
			
			//$('#rest-days input').val(days);
			//$('#rest-hours input').val(hours);
			
		} else {
			frame_obj.alert('结束时间不能小于开始时间');
			$('#end-time').val('');
		}
	}
}



//申请
function init_do_apply() {
	$('#apply').unbind('click').bind('click', function() {
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		if(cheak_formset_name()){
			var data = get_apply_data();
			data.save_type = 1;
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要提交该报申请吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'提交中……');
			}
		}
	});
	$('#save').unbind('click').bind('click', function() {
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		if(cheak_formset_name()){
			var data = get_apply_data();
			data.save_type = 0;
			var btn = $(this);
			//frame_obj.comfirm('确定要保存该表单吗？', function() {});
			frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), save_complete,undefined,undefined,undefined,'保存中……');
		}
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		$storage.clear();
		window.location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id+"&formsetInst_id="+data.formsetInst_id+"&list=2";
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function save_complete(data) {
	if (data.errcode == 0) {
		$storage.clear();
		//frame_obj.alert("保存成功",'',to_open);
		to_open();
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function to_open(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_mine_list&is_finish=2';
}

function to_lists() {
	location.href = $('#app-url').val() + '&m=mv_list&a=get_mine_list';
}


function get_apply_data() {
	var data = new Object();
	
	data.form_id = $('#proc_form_id').val();
	data.work_id = $('#proc_work_id').val();
	data.form_name = get_formset_name();
	data.judgement = "";
	data.workitem_id = "";
	data.formsetInst_id = "";
	
	data.reason = $('#reason').val();
	data.type = $('#type').val();
	data.type_name = $("#type  option:selected").text();
	data.start_time = $('#start-time').val();
	data.end_time = $('#end-time').val();
	data.days = $('#rest-days input').val();
	data.hours = $('#rest-hours input').val();
	
	//附件信息
	data.files = new Array();
	$('.file_div .file_info').each(function() {
		var item = new Object();
		item.file_name = $(this).attr("fname");
		item.file_key = $(this).attr("filekey");
		item.file_url = $(this).attr("furl");
		item.file_ext = $(this).attr("fext");
		data.files.push(item);
	});
	
	return data;
}

function check_apply_data(data) {
	if (!check_data(data.reason, 'notnull')) {
		frame_obj.alert('请填写请假理由');
		return false;
	}
	if (data.type == 0) {
		frame_obj.alert('请选择请假类型');
		return false;
	}
	if (!check_data(data.start_time, 'notnull')) {
		frame_obj.alert('请填写开始时间');
		return false;
	}
	if (!check_data(data.end_time, 'notnull')) {
		frame_obj.alert('请填写结束时间');
		return false;
	}
	if (!check_data(data.days, 'number') 
			|| !check_data(data.hours, 'number')
			|| (curr_type == 0 && data.days == 0)
			|| (curr_type == 1 && data.days == 0 && data.hours == 0)) {
		
		frame_obj.alert('请填写请假时长');
		return false;
	}
	
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	
	return true;
}

function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_del[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

function init_cancel() {
	$('#cancel').unbind('click').bind('click', function() {
		window.location.href=$("#app-url").val()+"&m=mv_list&a=get_mine_list";
	});
}

function formset_name_change(obj){
	var input_val = $(obj).val();
	$("input[forset_key=7]").val(input_val);
}

function cheak_formset_name(){
	if($("#forset_input")!==undefined&&$("#forset_input").val()==""){
		frame_obj.alert("请补全流程名称");
		return false;
	}
	return true;
}
function get_formset_name(){
	var form_name="";
	$(".js_formset_val").each(function(){
		form_name+=$(this).val()+"-";
	});
	if(form_name!=""){
		form_name = form_name.substring(0,form_name.length-1);
	}
	return form_name;
}

