$(document).ready(function() {
	inst_apply_html();
	$('span.js_a_cnt').html('(' + $('.process-appoval .js_a_div').length + ')');
	$('span.js_n_cnt').html('(' + $('.process-appoval .js_n_div').length + ')');
	$('span.js_w_cnt').html('(' + $('.process-appoval .js_w_div').length + ')');
	
	init_info_button();
	
	$(".workitem_list").unbind('click').bind('click', function() {
		var url = $("#app-url").val()+'&m=mv_open&a=get_mv_pic&formsetinst_id='+$("#formsetinst_id").val()+"&deal_type="+$("#deal_type").val();
		window.location.href=url;
	});
	
});



function to_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_notify_list';
}


function download_file(id,url){
	var u = url.toLowerCase();
	var formsetinst_id = $("#formsetinst_id").val();
	var data = new Object();
	data.id = id;
	data.formsetinst_id = formsetinst_id;
	data.workitem_id = $("#workitem_id").val()?$("#workitem_id").val():"";
	data.notify_id = $("#notify_id").val();
	data.deal_type = $("#deal_type").val();
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	//$(".load_img").css("display","block");
	frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete,undefined,undefined,undefined,'下载中……');
	//$("#download_div").attr("src",url);
}

function download_complete(data){
	//$(".load_img").css("display","none");
	if (data.errcode == 0) {
		wx.closeWindow();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}


function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

//初始化页面控件
function inst_apply_html(){
	$.each(dataObj,function(key,obj){
		var input_html = inst_input_html(obj);
		$("#input_content").append(input_html);
	});
	
	
}

function inst_input_html(obj){
	var html = '';
	if(obj.input_key=="input1"){
		html = '<div class="css_row" id="'+obj.input_key+'">';
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		
		if(obj.val===undefined){
			obj.val="";
		}
		var input_val = "";
		if(obj.days!=0){
			input_val+=obj.days+"天";
		}
		if(obj.hours!=0){
			input_val+=obj.hours+"小时";
		}
		
		html +='<div class="css_input_deal_div">'+input_val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
			
		html +='</div>';
	}
	else{
		html = '<div class="css_row" id="'+obj.input_key+'">';
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		
		if(obj.val===undefined){
			obj.val="";
		}
		var input_val = obj.val.replace(/\n/g,'<br>');
		html +='<div class="css_input_deal_div">'+input_val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
		html +='</div>';
	}
		
	return html;

}

function init_info_button(){
	$(".js_title_btn").unbind("click").bind("click",function(){
		var s_div = $(this).attr("s_div");
		$(".js_title_btn").removeClass("css_title_active");
		$(this).addClass("css_title_active");
		$(".js_a_divs").css("display","none");
		$(".js_n_divs").css("display","none");
		$(".js_w_divs").css("display","none");
		$("."+s_div).css("display","block");
	});
}

