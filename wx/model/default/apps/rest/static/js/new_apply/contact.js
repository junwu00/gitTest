var c_search_cache = {};			//员工列表缓存
var c_search_cache_user = {};		//员工详情缓存
var c_search_cache_search = {};		//员工搜索缓存
var c_search_data = {};				//当前选中的内容
var c_search_count = 0;				//当前选中的个数
var c_search_offset = 0;			//当前查找位置
var c_search_limit = 50;			//每次查找数量
var c_search_form;
$(document).ready(function(){
	var win_height = $(window).height();
	$('#c-search-container').css({'height': win_height-40});	//40为底部按钮高度
	
	c_search_init_search();
	c_search_init_toggle();
	c_search_init_tab_toggle();
	c_search_init_dept_toggle();
	c_search_init_py_position();
	c_search_init_selected_toggle();
	$('#c-search-title').bind('focus', function() {
		$('#c-search-selected').addClass('hide');
	});
});

//显示/隐藏选人控件
function c_search_init_toggle(opt) {
	$('.c-search-handler').unbind('click').bind('click', function() {
		c_search_form = $(this).attr('data-form');		//data-form: 显示选人控件时，要对应隐藏的内容（如表单）的id或class（如：#id或.class）
		if (!c_search_form)	return;
		
		c_search_init(opt);
		
		$(c_search_form).css({display: 'none'});
		$('#c-search-container').css({display: 'block'});
	});
}

//点击确定的触发事件、初始化数据
function c_search_init(opt) {
	//预处理事件
	if (typeof(opt.before) === 'function') {
		opt.before();
	}
	//初始化数据
	$('.icheck-cb').iCheck('uncheck');
	c_search_data = opt.selected || {};
	c_search_count = 0;
	var users = [];
	for (var i in c_search_data) {
		if (c_search_data[i]) {
			users.push(i);
		}
	}
	
	//清空已选	
	$('#c-search-selected').html('');
	$('#c-search-selected-count').html(0);

	if (users.length > 0) {
		$('#ajax-url').val(c_search_init_url);
		frame_obj.do_ajax_post(undefined, '', {user: users}, function(resp) {
			if (resp.errcode == 0) {
				var info = resp.info;
				for (var i in info) {
					if (!info[i])	continue;
					
					if ($('.icheck-cb[data-id=' + info[i].id + ']').length == 0) {
						c_search_insert_selected(info[i].id, info[i].name, info[i].pic_url);
						
					} else {
						$('.icheck-cb[data-id=' + info[i].id + ']').iCheck('check');
					}
				}
				
			} else {
				frame_obj.alert(resp.errmsg);
			}
		});
	}
	
	//确认事件
	$('#c-search-ok').unbind('click').bind('click', function() {
		$('#c-search-container').css({'display': 'none'});
		if (typeof(opt.ok) === 'function') {
			opt.ok(c_search_data);
		}
	});
}

//隐藏/显示已选成员
function c_search_init_selected_toggle() {
	$('#c-search-show-selected').unbind('click').bind('click', function() {
		if ($('#c-search-selected').hasClass('hide')) {
			if (c_search_count == 0) {
				frame_obj.alert('当前没有已选择的成员');
				
			} else {
				$('#c-search-selected').removeClass('hide');
			}
			
		} else {
			$('#c-search-selected').addClass('hide');
		}
	});
}

//初始化部门隐藏/展开
function c_search_init_dept_toggle() {
	init_show();
	init_hide();
	
	function init_show() {
		$('.c-search-up > span').unbind('click').bind('click', function() {
			var that = this;
			$(this).parent().children('ul').slideDown('normal', function() {
				$(that).parent('.c-search-up').addClass('c-search-down');
				$(that).parent('.c-search-up').removeClass('c-search-up');
				init_hide();
			});
		});
	}
	function init_hide() {
		$('.c-search-down > span').unbind('click').bind('click', function() {
			var that = this;
			$(this).parent().children('ul').slideUp('normal', function() {
				$(that).parent('.c-search-down').addClass('c-search-up');
				$(that).parent('.c-search-down').removeClass('c-search-down');
				init_show();
			});
		});
	}
}

//初始化部门、员工、员工详情切换
function c_search_init_tab_toggle() {
	$('.c-search-list').each(function() {
		$(this).unbind('click').bind('click', function(evt) {
			var dept = $(this).attr('data-id');
			var dept_name = $(this).attr('data-name');
			
			c_search_dept_select_evt($(this), dept, dept_name, 'dept');
			$('#c-search-back').removeClass('hide');
			
			evt.stopPropagation();
		});
	});
	
	$('.c-search-tree li').each(function() {
		if (!$(this).hasClass('c-search-down') && !$(this).hasClass('c-search-up')) {	//叶子部门，可点击进入
			$(this).unbind('click').bind('click', function() {
				$(this).find('.c-search-list').click();
			});
		}
	});
	
	$('#c-search-back').unbind('click').bind('click', function() {
		$('#c-search-selected').addClass('hide');
		if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'slow', function() {
				$('#c-search-user-detail').html('');
				$('#c-search-back').removeClass('hide');
			});
			
		} else if ($('#c-search-user').css('left') == '0px') {	//当前在员工选择页面，则返回部门列表页面
			$('#c-search-dept').removeClass('hide');
			$('#c-search-py').addClass('hide');
			$('#c-search-user').animate({left: '100%'}, 'slow', function() {
				$('#c-search-title').attr('placeholder', '搜索：姓名/拼音');
				$('#c-search-title').attr('data-dept', '0');
				$('#c-search-user').html('');
				$('#c-search-back').addClass('hide');
			});

		} else {												//当前在部门列表页面，则返回上一层
//			$('#c-search-py').addClass('hide');
//			$(c_search_form).css({display: 'block'});
//			$('#c-search-container').css({display: 'none'});
		}
	});
}

//选择部门（显示员工列表）事件
function c_search_dept_select_evt(btn, dept, dept_name, from) {		//from 来源：部门列表的选择 / 员工详情页面的选择
	$('#c-search-selected').addClass('hide');
	$('#c-search-title').attr('placeholder', '在' + dept_name + '搜索: 姓名/拼音');
	$('#c-search-title').attr('data-dept', dept);
	
	function show_cache() {
		var data = [];
		for (var i in c_search_cache[dept]) {
			if (!c_search_cache[dept][i])	continue;
			if (i == 'offset') {
				c_search_offset = c_search_cache[dept][i];	//历史查找位置
				continue;
			}
			if (i == 'complete') {
				$('#c-search-py').removeClass('hide');
				continue;
			}
			
			data = data.concat(c_search_cache[dept][i]);
		}
		$('#c-search-user').html('');
		c_search_build_user_list(data);
	}

	$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	if (c_search_cache[dept]) {								//有本地缓存，取缓存数据
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'slow', function() {
				$('#c-search-dept').addClass('hide');
				show_cache();
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'slow', function() {
				show_cache();
			});
		}
		
	} else {
		c_search_offset = 0;	//重置查找位置
		if (from == 'dept') {								//来源：部门列表
			$('#c-search-user').animate({left: 0}, 'slow', function() {
				$('#c-search-dept').addClass('hide');
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
			
		} else if (from == 'detail') {						//来源：员工详情
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'slow', function() {
				c_search_list_user(btn, dept, c_search_offset, c_search_limit);
			});
		}
	}
	//滚动加载
	c_search_scroll_load(function() {
		c_search_list_user(undefined, dept, c_search_offset, c_search_limit);
	});
}

//查找员工
function c_search_init_search() {
	var empty_reg = /^[\S]+$/;
	$('#c-seaerch-do-search').unbind('click').bind('click', function() {
		var name = $('#c-search-title').val();
		if (!empty_reg.test(name)) {							//当前内容为空
			if ($('#c-search-title').attr('data-pre') == '') {	//上次内容也为空，不处理
				return;
			}													//上次内容不为空，处理
		}

		$('#c-search-title').attr('data-pre', $.trim(name));
		$('#c-search-user').html('');
		if ($('#c-search-user').css('left') == '0px' && $('#c-search-user-detail').css('left') == '0px') {		//当前在员工详情页面，则返回员工列表页面
			$('#c-search-user').removeClass('hide');
			$('#c-search-user-detail').animate({left: '100%'}, 'slow', function() {
				$('#c-search-user-detail').html('');
			});
			
		} else if ($('#c-search-user').css('left') != '0px') {	//当前在部门列表页面，则跳转员工列表页面
			$('#c-search-user').animate({left: '0'}, 'slow');
			$('#c-search-back').removeClass('hide');
		}
		
		var dept = $('#c-search-title').attr('data-dept');
		if (c_search_cache_search[dept] && c_search_cache_search[dept][name]) {	//有缓存
			var data = [];
			var source = c_search_cache_search[dept][name];
			for (var i in source) {
				if (!source[i])	continue;
				if (i == 'offset') {
					c_search_offset = source[i];	//历史查找位置
					continue;
				}
				if (i == 'complete') {
					$('#c-search-py').removeClass('hide');
					continue;
				}
				
				data = data.concat(source[i]);
			}
			c_search_build_user_list(data);
			
		} else {
			c_search_offset = 0;	//重置查找位置
			$('#c-search-user').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
			c_search_list_user($(this), dept, c_search_offset, c_search_limit, name);
		}

		var dept_name = $('.c-search-list[data-id=' + dept + ']').attr('data-name');
		//滚动加载
		c_search_scroll_load(function() {
			c_search_list_user(undefined, dept, c_search_offset, c_search_limit, name);
		});
	});
}

//获取员工数据
function c_search_list_user(btn, dept_id, offset, limit, name) {
	$('#ajax-url').val(c_search_url);
	var empty_reg = /^[\S]+$/;
	var data = new Object();
	data.dept = String(dept_id);
	data.offset = offset || c_search_offset;
	data.limit = limit || c_search_limit;
	data.name = (empty_reg.test(name)) ? $.trim(name) : '';
		
	frame_obj.do_ajax_post(btn, '', data, function(resp) {
		if (resp.errcode == 0) {
			var info = resp.info;
			if (info.count == 0) {
				if (data.name == '') {
					$('#c-search-user').html('<p class="c-search-empty">当前部门暂无成员</p>');
				} else {
					$('#c-search-user').html('<p class="c-search-empty">找不到匹配的成员</p>');
				}
				return;
			}
			
			var info = info.data;
			c_search_scroll_load_complete(info.length);
			c_search_offset += info.length;
			
			//缓存数据
			if (info.length != 0 && data.name == '') {						//部门所有员工
				c_search_cache[dept_id] = c_search_cache[dept_id] || {};
				c_search_cache[dept_id][offset] = info;
				c_search_cache[dept_id]['offset'] = c_search_offset;
				c_search_build_user_list(info);
				
			} else if (info.length != 0 && data.name != '') {				//查找员工
				c_search_cache_search[dept_id] = c_search_cache_search[dept_id] || {};
				c_search_cache_search[dept_id][data.name] = c_search_cache_search[dept_id][data.name] || {};
				c_search_cache_search[dept_id][data.name][offset] = info;
				c_search_cache_search[dept_id][data.name]['offset'] = c_search_offset;
				c_search_build_user_list(info);
			}

			//标识为完成加载
			if (offset == 0 && resp.info.count <= resp.info.data.length) {	//只一页，显示右侧拼音搜索
				$('#c-search-py').removeClass('hide');
				if (data.name == '') {
					c_search_cache[dept_id]['complete'] = 1;
					
				} else if (data.name != '') {
					c_search_cache_search[dept_id][data.name]['complete'] = 1;
				}

			} else {
				$('#c-search-py').addClass('hide');
			}

			if (resp.info.data.length == 0 && data.name == '') {
				c_search_cache[dept_id]['complete'] = 1;
				$('#c-search-py').removeClass('hide');
				
			} else if (resp.info.data.length == 0 && data.name != '') {
				c_search_cache_search[dept_id][data.name]['complete'] = 1;
				$('#c-search-py').removeClass('hide');
			}
			
		} else {
			frame_obj.alert('加载员工数据失败');
		}
	});
}

//生成用户选择列表
function c_search_build_user_list(info) {
	var html = '';
	var py = '';
	var py_list = 'abcdefghijklmnopqrstuvwxyz';
	var py_list_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	for (var i in info) {
		if (!info[i])			continue;
		if (i == 'offset')		continue;
		if (i == 'complete')	continue;
		
		var item = info[i];
		var f_py = item.first_py.substr(0, 1);
		var py_idx = py_list.indexOf(f_py);
		if (item.boss) {
			f_py = '第一负责人';
		} else if (item.second_boss) {
			f_py = '第二负责人';
		} else if (py_idx == -1) {
			f_py = '#';
		} else {
			f_py = py_list_up.substr(py_idx, 1);
		}
		if ($('#c-search-user .c-search-py-tab[data-py=' + f_py + ']').length == 0) {//未加载过该拼音开头的数据
			$('#c-search-user').append(html);
			$('#c-search-user').append('<p class="c-search-py-tab" data-py=' + f_py + '>' + f_py + '</p>');
			html = '';
		}
		html += '<p class="c-search-py-item">' + 
			'<input type="checkbox" class="icheck-cb" data-id="' + item.id + '" data-name="' + item.name + '">' +
			'<img class="c-search-user-pic" data-id="' + item.id + '" src="' + item.pic_url + '64" onerror="this.src=\'static/image/face.png\'">' +
			'<span class="c-search-user-name" data-id="' + item.id + '">' + item.name + '</span>' +
		'</p>';
	}
	$('#c-search-user').append(html);
	c_search_init_selected();
	c_search_init_detail();
	c_search_update_select_evt();
}

//初始化员工详情页面展示
function c_search_init_detail() {
	$('.c-search-user-pic').each(function() {	//点击头像
		$(this).unbind('click').bind('click', function(evt) {
			show_detail($(this), evt);
		});
	});
	$('.c-search-user-name').each(function() {	//点击姓名
		$(this).unbind('click').bind('click', function(evt) {
			show_detail($(this), evt);
		});
	});
	function show_detail(that, evt) {
		$('#c-search-selected').addClass('hide');
		var that = $(that);
		var user = $(that).attr('data-id');
		$('#c-search-user-detail').html('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
		$('#c-search-user-detail').animate({left: '0'}, 'slow', function() {
			$('#c-search-user').addClass('hide');
			c_search_load_user_detail(that, user);
		});
		evt.stopPropagation();
	}
}

//加载员工详情信息
function c_search_load_user_detail(btn, user) {
	if (c_search_cache_user[user]) {
		c_search_build_user_detail(c_search_cache_user[user]);
		
	} else {
		$('#ajax-url').val(c_search_detail_url);
		frame_obj.do_ajax_post(btn, '', {user: user}, function(resp) {
			if (resp.errcode == 0) {
				c_search_cache_user[user] = resp.info;
				c_search_build_user_detail(resp.info);
				
			} else {
				frame_obj.alert('加载员工详情信息失败');
			}
		});
	}
}

//生成员工详情页面
function c_search_build_user_detail(data) {
	var depts = '<span>';
	for (var i in data.dept_list) {
		if (!data.dept_list[i])		continue;
		
		var dept = data.dept_list[i];
		var dept_name = $('.c-search-list[data-id=' + dept + ']').attr('data-name');
		dept = '<span class="c-search-detail-dept" data-id="' + dept + '" data-name="' + dept_name + '">' + dept_name + '</span>';
		depts += dept;
	}
	depts += '</span>';
	
	var html = '<div>' +
		'<p id="c-search-detail-title">' +
			'<img src="' + data.pic_url + '64" onerror="this.src=\'static/image/face.png\'">' +
			'<span>' +
				'<span id="c-search-detail-name">' + data.name + '</span>' +
				'<span id="c-search-detail-acct">企业号账号 : ' + data.acct + '</span>' +
			'</span>' +
		'</p>' +
		'<p id="c-search-detail-position">' +
			'<label>职位</label>' + data.position + '' +
		'</p>' +
		'<p id="c-search-detail-depts">' +
			'<label>部门</label>' + depts +
		'</p>' +
	'</div>';
	$('#c-search-user-detail').html(html);
	c_search_detail_user_list();
}

//由员工详情页面的部门列表点击部门，进入部门员工列表页面
function c_search_detail_user_list() {
	$('.c-search-detail-dept').each(function() {
		$(this).unbind('click').bind('click', function() {
			c_search_dept_select_evt($(this), $(this).attr('data-id'), $(this).attr('data-name'), 'detail');
		});
	});
}

//初始化选中的员工
function c_search_init_selected() {
	for (var i in c_search_data) {
		if (!c_search_data[i])	continue;
		
		$('.icheck-cb[data-id=' + i + ']').iCheck('check');
	}
}

//更新人员选择事件
function c_search_update_select_evt() {
	$('.c-search-py-item').each(function() {
		$(this).unbind('click').bind('click', function() {
			$(this).find('.icheck-cb').iCheck('toggle');
		});
	});
	$('.icheck-cb').iCheck({
		checkboxClass: 'icheckbox_square-green',
		increaseArea: '20%'
	});
	$('.icheck-cb').on('ifChecked', function(event){
		var user = $(this).attr('data-id');
		var pic = $(this).parents('.c-search-py-item').children('img').attr('src');
		var name = $(this).attr('data-name');
		c_search_data[$(this).attr('data-id')] = {id: user, name: name, pic: pic};
		c_search_insert_selected(user, name, pic)
	});
	
	$('.icheck-cb').on('ifUnchecked', function(event){
		var user = $(this).attr('data-id');
		$('#c-search-selected img[data-id=' + user + ']').parent().remove();
		delete(c_search_data[user]);
		
		c_search_count--;
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
		$('#c-search-selected-count').html(c_search_count);
	});
}

//选择列表中插入新选择成员
function c_search_insert_selected(user, name, pic) {
	var item = '<span class="c-search-selected-item">' + 
		'<img src="' + pic + '" data-id="' + user + '" onclick="c_search_delete_evt(this)" onerror="this.src=\'static/image/face.png\'">' + 
		'<span class="c-search-selected-name">' + name + '</span>' + 
	'</span>';
	$('#c-search-selected').append(item);
	$('#c-search-selected-count').html(++c_search_count);
}

//更新删除员工事件
function c_search_delete_evt(that) {
	var user = $(that).attr('data-id');
	$('#c-search-selected img[data-id=' + user + ']').parent().remove();
	delete(c_search_data[user]);
	
	if ($('.icheck-cb[data-id=' + user + ']').length == 0) {		//选中人不在当前列表中
		c_search_count--;
		if (c_search_count == 0) {
			$('#c-search-selected').addClass('hide');
		}
		$('#c-search-selected-count').html(c_search_count);
		
} else {															//选中人在当前列表中
		$('.icheck-cb[data-id=' + user + ']').iCheck('uncheck');
	}
}

//滚动加载
var c_search_curr_scroll_top = 0;
var c_search_is_scroll_end = false;
var c_search_is_scroll_loading = false;
function c_search_scroll_load(callback) {
	var $container = $('#c-search-user');
	var sTimer;
	
	$container[0].scrollTop = c_search_curr_scroll_top;
	$container.unbind('scroll').scroll(function scrollHandler(){
	    clearTimeout(sTimer);
	    sTimer = setTimeout(function() {
	    	var c = $container[0].scrollHeight;		//总高度
	    	var t = $container.scrollTop();			//滚动高度
	    	var h = $container[0].clientHeight;
	    	c_search_curr_scroll_top = t;

	        if(window.loaded == 1){$(window).unbind("scroll", scrollHandler);}
	        if(t + h + 100 > c && !c_search_is_scroll_end && !c_search_is_scroll_loading){
	        	if ($('#c-search-user').children('#c_search_loading').length == 0) {
	        		$('#c-search-user').append('<div id="c_search_loading"><img src="static/image/wait.gif">加载中...</div>');
	        	}
	        	if (typeof(callback) === 'function') {
	        		callback();
	        	};
	        }
	    }, 100);
	});
}

//加载完成回调事件
function c_search_scroll_load_complete(load_length) {
	c_search_is_scroll_loading = false;
	if (load_length == 0) {
		c_search_is_scroll_end = true;
		
	} else {
		c_search_is_scroll_end = false;
	}
	$('#c-search-user').children('#c_search_loading').remove();
}

//初始化拼音定位
function c_search_init_py_position() {
	$('#c-search-py > span').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.py-big').addClass('hide');
			$(this).children('.py-big').removeClass('hide');
		});
	});
	$('.py-big > span').each(function() {
		$(this).unbind('click').bind('click', function(evt) {
			$(this).parent().addClass('hide');
			var py = $(this).html();
			
			var target = $('.c-search-py-tab[data-py=' + py + ']');
			if (target.length == 0) {
				frame_obj.alert('无字母' + py + '开头的成员');
				
			} else {
				$('#c-search-user').animate({scrollTop: target[0].offsetTop});
			}
			evt.stopPropagation();
		});
	});
}