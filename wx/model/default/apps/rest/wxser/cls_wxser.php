<?php
/**
 * 快速请假
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『快速请假』操作指引',
		'desc' => '欢迎使用快速请假应用，在这里你可以随时随地跟领导请假，审批结果立马通知你，大胆跟领导请个假去约会吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213969656&idx=4&sn=a00864fb9c38b6553f09a060ec4e5a12&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	//--------------------------------------内部实现---------------------------
	
}

// end of file