<?php
include (SYSTEM_APPS.'rest/modules/cls_mv_send_server.php');
/**
 * 发起申请
 * @author yangpz
 * @date 2014-12-25
 */
class cls_mv_send extends cls_mv_send_server {
	
	private static $HoursInOneDay = 8;	//一天8小时制 TODO
	
	public function __construct() {
		parent::__construct('rest');
	}
	
	/**
	 * 主页面
	 */
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'save_workitem':
				$this -> save_workitem();
				break;
			case 'send_workitem':
				$this -> send_workitem();
				break;
			case 'callback_start':
				$this -> callback_start();
				break;
			case 'callback_pre':
				$this -> callback_pre();
				break;
			case 'sendback':
				$this -> sendback_workitem();
				break;
			case 'callback_select':
				$this -> callback_select();
				break;
			case 'del_workitem':
				$this -> del_workitem();
				break;
			case 'press_workitem':
				$this -> press_workitem();
				break;
			case 'send_notify':
				$this -> notify_workitem();
				break;
			case 'update_notify_state':
				$this -> update_notify_state();
				break;
			default:
				cls_resp::show_err_page('非法请求');
		}
	}
	
	
	/**
	 * 推送二维码消息
	 */
	public function send_pc_info(){
		$workitem_id = get_var_get("workitem_id");
		$workitem_id = intval($workitem_id);
		$cur_workitem = parent::get_curr_workitem($workitem_id);
		
		$md5_key = get_var_get("key");
		$func = get_var_get("func");
		
		$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_send&a=send_linksign_info&key=".$md5_key."&workitem_id=".$workitem_id;
		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		try{
			$linksign = g("sign_api") -> get_sign_url($user_name,$redirect);
			if(!empty($linksign)){
				$redirect_url = $linksign["redirect"];
				header("Location:".$redirect_url);
			}
		}
		catch (SCException $e){
			cls_resp::show_err_page(array("扫码失败，请刷新页面重新扫码!"));
		}
		parent::send_pc_info_parent($md5_key, $func);
	}

	/**
	 * 签字完成后，保存签字并关闭微信页面
	 */
	public function send_linksign_info(){
		$workitem_id = get_var_get("workitem_id");
		$workitem_id = intval($workitem_id);
		$cur_workitem = parent::get_curr_workitem($workitem_id);
		
		$md5_key = get_var_get("key");
		$signatureId = get_var_value("signatureId");
		$cancel = get_var_value("cancel");
		if($cancel=="yes"){
			$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=close_qrcode";
			header("Location:".$redirect);
		}
		else{
			try{
				$img_url = g("sign_api") -> get_image($signatureId);
				if(!empty($img_url)){
					parent::save_linksign($workitem_id, $img_url);
					parent::send_pc_info_parent($md5_key, 'linksign_finish');
					$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=close_qrcode";
					header("Location:".$redirect);
				}
			}catch (SCException $e){
				cls_resp::show_err_page($e);
			}
		}
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
	/**
	 * 更新知会状态
	 */
	private function update_notify_state(){
		try {
			parent::log('开始更新知会流程');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('notify_id'));
			
			parent::update_notify_state_parent($data["notify_id"]);
			//g("mv_notify")->update_notify_state($data["notify_id"],$this->ComId,$this->UserId);
				
			g('db') -> commit();
			echo json_encode(array('errcode'=>0,'errmsg'=>"更新知会状态成功"));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 知会流程
	 */
	private function notify_workitem(){
		try {
			parent::log('开始保存知会流程');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('formsetinst_id','receivers'));
			
			parent::notify_workitem_parent($data);
			
			g('db') -> commit();
			echo json_encode(array('errcode'=>0,'errmsg'=>"知会成功"));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 催办流程
	 */
	private function press_workitem(){
		try {
			parent::log('开始催办流程');
			$data = parent::get_post_data(array('formsetinst_id'));
			
			parent::press_workitem_parent($data);
			
			echo json_encode(array('errcode'=>0,'errmsg'=>"催办成功"));
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 删除工作项
	 */
	private function del_workitem(){
		try {
			parent::log('开始删除工作项');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id'));
			
			$ret = parent::del_workitem_parent($data);
			
			g('db') -> commit();
			echo json_encode(array('errcode'=>$ret["errcode"],'errmsg'=>$ret["errmsg"]));
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 发送到退回步骤
	 */
	private function sendback_workitem(){
		try {
			parent::log('开始发送到退回节点');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id','judgement'));
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::sendback_workitem_parent($data);
				
				g('db') -> commit();
				echo json_encode(array('errcode'=>0,'errmsg'=>'流程发送成功，发送步骤为'.$ret["workitem_name"].'步骤，接收人为'.$ret["receiver_name"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 驳回指定步骤流程
	 */
	private function callback_select(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('callback_workitem_id','workitem_id','judgement'));
			
			$ret = parent::callback_select_parent($data);
			
			g('db') -> commit();
			
			echo json_encode(array('errcode'=>0,'errmsg'=>'流程退回成功，退回步骤为'.$ret["workitem_name"].'步骤，接收人为'.$ret["receiver_name"]));
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	/**
	 * 驳回开始步骤流程
	 */
	private function callback_start(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitem_id','judgement'));
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::callback_start_parent($data);
			
				g('db') -> commit();
				echo json_encode(array('errcode'=>0,'errmsg'=>'流程退回成功，退回步骤为开始步骤，接收人为'.$ret["receiver_name"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 退回上一步骤
	 */
	private function callback_pre(){
		try {
			parent::log('开始驳回意见信息');
			g('db') -> begin_trans();
			
			$data = parent::get_post_data(array('workitem_id','judgement'));
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$ret = parent::callback_pre_parent($data);
				
				g('db') -> commit();
				echo json_encode(array('errcode'=>$ret["errcode"],'errmsg'=>$ret["errmsg"],'callback_workitem'=>$ret["callback_workitem"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 保存流程
	 */
	private function save_workitem(){
		try {			
			parent::log('开始保存信息');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('form_id','form_name','work_id', 'workitem_id','formsetInst_id','judgement','reason','type','type_name','start_time','end_time','days','hours'));
			
			
			$is_finish = false;
			if(!empty($data["workitem_id"])){
				$is_finish = parent::is_finish($data["workitem_id"]);
			}
			if($is_finish==true){
				parent::log('流程状态已变更');
				echo json_encode(array('errcode'=>99,'errmsg'=>'流程状态已变更，请返回待办列表'));
			}
			else{
				$temp_formsetInst_id = 0;
				if(!empty($data["formsetInst_id"])){//修改流程
					$temp_formsetInst_id = $data["formsetInst_id"];
				}
				if(!isset($data["save_type"])){$data["save_type"]=1;}
				if($data["save_type"]==1){
					$this -> check_apply_times($data['type'], $data['days'], $data['hours'], $data['start_time'], $data['end_time'], $data['days'], $data['hours'],$temp_formsetInst_id);
				}
				$data['form_vals'] = $this->to_form_vals($data['reason'], $data['type'], $data['type_name'], $data['start_time'], $data['end_time'], $data['days'], $data['hours']);
				
				$data['is_other_proc'] = parent::$IsRest;
				
				$ret = parent::save_workitem_parent($data);
				
				//保存请假记录
				
				$formsetinst_id = $ret["formsetInst_id"];
				
				$record = g('rest_record') ->get_record_by_instid($formsetinst_id);
				
				if(!is_array($record)){
					g('rest_record') ->save($data['reason'], $data['type'], $data['start_time'], $data['end_time'], $data['days'], $data['hours'],$formsetinst_id);
				}
				else{
					g('rest_record') ->update($record["id"], $data['reason'], $data['type'], $data['start_time'], $data['end_time'], $data['days'], $data['hours']);
				}
				
				g('db') -> commit();
				parent::log('保存流程信息成功');
				echo json_encode(array('errcode'=>0,'errmsg'=>'保存流程信息成功','workitem_id'=>$ret["workitem_id"],'formsetInst_id'=>$ret["formsetInst_id"]));
			}
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
		
	}
	
	
	
	
	
	/**
	 * 发送流程
	 */
	private function send_workitem(){
		try {	
			parent::log('开始发送流程');
			g('db') -> begin_trans();
			$data = parent::get_post_data(array('workitemid','work_nodes'));
			
			$ret = parent::send_workitem_parent($data);
			
			g('db') -> commit();
			
			$msg = "";
			if($ret["return_arr"][0]=="TRUE"){
				$msg = '流程流转结束';
			}
			else{
				$msg = '流程发送成功，下一步骤为'.$ret["return_arr"][1].' 接收人为'.$ret["return_arr"][2];
			}
			
			parent::log($msg);
			echo json_encode(array('errcode'=>0,'errmsg'=>$msg));
			
		} catch (SCException $e) {
			g('db') -> rollback();
			cls_resp::echo_exp($e);
		}
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
/**
	 * 验证请假天数上限
	 * @param unknown_type $type
	 */
	private function check_apply_times($type, $days, $hours, $start_time, $end_time, $this_days, $this_hours,$formsetinst_id) {
		$start_time = strtotime($start_time);
		$end_time = strtotime($end_time);
		
		try {
			//*验证时间先后
			if ($end_time <= $start_time) {
				throw new SCException('结束时间必须大于开始时间');
			}
			//*/
			
			//时间段内是否请假过
			$is_applyed = g('rest_record') -> is_date_applyed($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $start_time, $end_time,$formsetinst_id);
			if ($is_applyed) {
				throw new SCException('该时间段内已请假过');
			}
			
			$is_year = g('rest_type') -> is_year($_SESSION[SESSION_VISIT_COM_ID], $type);
			if ($is_year) {	//年假
				$user_year = g('rest_year') -> get_by_user_id($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $fields='days, hours');
				if ($user_year) {	//使用配置的员工年假
					$max_days = $user_year['days'];
					$max_hours = $user_year['hours'];
				} else {			//使用通用的员工年假
					$max_days = g('rest_type') -> get_max_day($_SESSION[SESSION_VISIT_COM_ID], $type);
					$max_hours = 0;
				}
				
				$applyed = g('rest_record') -> get_year_times($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $type, $start_time, $end_time);
				$this -> verify_year_apply($max_days, $max_hours, $applyed['total_days'], $applyed['total_hours'], $this_days, $this_hours);
				
			} else {		//非年假
				$max_days = g('rest_type') -> get_max_day($_SESSION[SESSION_VISIT_COM_ID], $type);
				if ($hours > 0) $days++;	//有小时，多计一天
				if ($days > $max_days) {
					throw new SCException('请假天数超过限制，最多'.$max_days.'天');
				}
			}
			
		} catch (SCException $e) {
			throw $e;
		}
	}
	
	
	/**
	 * 验证年假时长
	 * @param unknown_type $conf_days		配置的天数
	 * @param unknown_type $cond_hours		配置的小时数
	 * @param unknown_type $applyed_days	已申请的天数
	 * @param unknown_type $applyed_hours	已申请的小时数
	 * @param unknown_type $this_days		本次申请的天数
	 * @param unknown_type $this_hours		本次申请的小时数
	 * @throws SCException
	 */
	private function verify_year_apply($conf_days, $cond_hours, $applyed_days, $applyed_hours, $this_days, $this_hours) {
		empty($applyed_days) && $applyed_days = 0;
		empty($applyed_hours) && $applyed_hours = 0;
		
		$total_hours = $conf_days * self::$HoursInOneDay + $cond_hours;
		$total_applyed_hours = $applyed_days * self::$HoursInOneDay + $applyed_hours;
		$total_this_hours = $this_days * self::$HoursInOneDay + $this_hours;
		
		if ($total_applyed_hours + $total_this_hours > $total_hours) {
			throw new SCException("超过年假申请时长。<br>您的年假为{$conf_days}天{$cond_hours}小时，已申请了{$applyed_days}天{$applyed_hours}小时");
		}
	}
	
	
	private function to_form_vals($reason,$type,$type_name,$start_time,$end_time,$days,$hours){
		$form_vals = array();
		$e_c = array(
			'reason' => array('type'=>'textarea','name'=>'请假事由', 'must'=>1, 'id'=>'reason','input_key'=>'input3'),
			'type' => array('type'=>'select','name'=>'请假类型', 'must'=>1, 'id'=>'type' ,'input_key'=>'input2'),
			'start_time'	=> array('type'=>'datetime','name'=>'开始时间', 'must'=>1, 'id'=>'start_time','input_key'=>'input4'),
			'end_time'	=> array('type'=>'datetime','name'=>'结束时间', 'must'=>1, 'id'=>'end_time','input_key'=>'input5'),
			'days_hours'	=> array('type'=>'text','name'=>'请假时长', 'must'=>1, 'id'=>'days_hours' ,'input_key'=>'input1'),
		);
		$ret_arr = $e_c['reason'];
		$ret_arr['val'] = $reason;
		$form_vals[$ret_arr['input_key']]=$ret_arr;
		
		$ret_arr = $e_c['type'];
		$ret_arr['type_num'] = $type;
		$ret_arr['val'] = $type_name;
		$form_vals[$ret_arr['input_key']]=$ret_arr;
		
		$ret_arr = $e_c['start_time'];
		$ret_arr['val'] = $start_time;
		$form_vals[$ret_arr['input_key']]=$ret_arr;
		
		$ret_arr = $e_c['end_time'];
		$ret_arr['val'] = $end_time;
		$form_vals[$ret_arr['input_key']]=$ret_arr;
		
		$ret_arr = $e_c['days_hours'];
		$val=0;
		if(empty($days)){
			$days = 0;
		}
		if(empty($hours)){
			$hours = 0;
		}
		
		$hour_num = sprintf("%.2f",substr(sprintf("%.3f", $hours/self::$HoursInOneDay), 0, -1));
		
		if($hour_num<$hours/self::$HoursInOneDay){
			$hour_num=$hour_num+0.01;
		}
		
		$val = $days+$hour_num;
		$ret_arr['val'] = $val;
		$ret_arr['days'] = $days;
		$ret_arr['hours'] = $hours;
		$form_vals[$ret_arr['input_key']]=$ret_arr;
		
		return $form_vals;
	}
}

//end of file