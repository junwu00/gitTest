<?php
include (SYSTEM_APPS.'rest/modules/cls_mv_open_server.php');
/**
 * 发起申请
 * @author yangpz
 * @date 2014-12-25
 */
class cls_mv_open extends cls_mv_open_server {
	
	public function __construct() {
		parent::__construct('rest');
	}

	
	/**
	 * 主页面
	 */
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'download_file':
				$this->download_file();
				break;
			case 'open_linksign':
				$this->open_linksign();
				break;
			default:
				$rest_type = g('rest_type') -> get_by_com($this->ComId);
				$form = g("mv_form") ->get_public_proc(parent::$IsRest,$this->ComId);
				$start_node = parent::get_start_node($form["work_id"]);
				g('smarty') -> assign('rest_type', $rest_type);
				g('smarty') -> assign('rest_form', $form);
				g('smarty') -> assign('curr_node', $start_node);
				
				//获取表单实例格式
				$is_special = 0;
				$formset_name = parent::get_formset_by_num($form["form_name"],$is_special);
				$formset_name_html = implode(" - ", $formset_name);
				g('smarty') -> assign('is_special', $is_special);
				g('smarty') -> assign('formset_name_html', $formset_name_html);
				g('smarty') -> assign('formset_name', $formset_name);
				
				g('smarty') -> assign('title', '请假申请');
				$open_url = $this -> app_domain.'&m=mv_list';
				$save_url = $this -> app_domain.'&m=mv_send';
				$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
				$page_url = $this -> app_domain.'&m=mv_open';
				g('smarty') -> assign('open_url', $open_url);
				g('smarty') -> assign('save_url', $save_url);
				g('smarty') -> assign('send_url', $send_url);
				g('smarty') -> assign('page_url', $page_url);
				g('smarty') -> assign('storagr_key','rest'.$_SESSION[SESSION_VISIT_USER_ID]);
				//新建申请单
				g('smarty') -> show($this -> temp_path.'new_apply/apply.html');
		}
	}
	
	/**
	 * 获取领签得到图片url并保存把结果保存到数据库重定向发送页面
	 */
	public function get_save_linksign(){
		$workitem_id= get_var_value("workitemid");
		$workitem_id= intval($workitem_id);
		$formsetinst_id = get_var_value("formsetinst_id");
		$formsetinst_id = intval($formsetinst_id);
		$signatureId = get_var_value("signatureId");
		$cancel = get_var_value("cancel");
		if($cancel=="yes"){
			$send_url = "index.php?app=".$this->AppName."&m=mv_open&a=detail&workitem_id=".$workitem_id;
			header("Location:".$send_url);
		}
		else{
			try{
				$img_url = g("sign_api") -> get_image($signatureId);
				if(!empty($img_url)){
					log_write($img_url);
					parent::save_linksign($workitem_id, $img_url);
					$send_url = "index.php?app=".$this->AppName."&m=mv_open&a=send_page&workitemid=".$workitem_id."&formsetInst_id=".$formsetinst_id;
					header("Location:".$send_url);
				}
			}catch (SCException $e){
				cls_resp::show_err_page($e);
			}
		}
	}
	
	
	/**
	 * 扫码后关闭扫码页面，pc扫码专用
	 */
	public function close_qrcode(){
		parent::oauth_jssdk();
		g('smarty') -> show($this -> temp_path.'new_apply/close_qrcode.html');
	}
	
	/**
	 * 弹出选人页面
	 */

	public function send_page(){
		try {
			$workitemid= get_var_value("workitemid");
			$workitemid = intval($workitemid);
			$formsetinst_id = get_var_value("formsetInst_id");
			$formsetinst_id = intval($formsetinst_id);
			$next_node_id_arr = parent::get_next_workitem($workitemid);
			$is_send = parent::is_send($next_node_id_arr);
			$list = get_var_value("list");
			
//			parent::show_contact();
			parent::assign_contact_dept();
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$page_url = $this -> app_domain.'&m=mv_open';
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('page_url', $page_url);
			
			g('smarty') -> assign('list', $list);
			g('smarty') -> assign('formsetInst_id', $formsetinst_id);
			g('smarty') -> assign('workitemid', $workitemid);
			g('smarty') -> assign('is_send', $is_send);
			g('smarty') -> assign('next_node_id', $next_node_id_arr);
			g('smarty') -> assign('title', '选择发送人');
			g('smarty') -> show($this -> temp_path.'new_apply/send.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	} 
	
	/**
	 * 知会发送列表
	 */
	public function notify_page(){
		try {
			$formsetinst_id= get_var_value("formsetinst_id");
			$formsetinst_id = intval($formsetinst_id);
			$workitem_id= get_var_value("workitem_id");
			$workitem_id = intval($workitem_id);
			
			parent::get_formsetInst_by_id($formsetinst_id);
			
//			parent::show_contact();
			parent::assign_contact_dept();
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$page_url = $this -> app_domain.'&m=mv_open';
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('page_url', $page_url);
			
			g('smarty') -> assign('formsetinst_id', $formsetinst_id);
			g('smarty') -> assign('workitem_id', $workitem_id);
			g('smarty') -> assign('title', '选择知会人');
			g('smarty') -> show($this -> temp_path.'new_apply/notify.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	
	/**
	 * 具体的流程审批申请页面
	 */
	public function apply() {}
	
	
	
	/**
	 * 根据formsetinst_id获取审批明细
	 */
	public function detail() {
		try {
			parent::oauth_jssdk();
			$workitem_id = get_var_get("workitem_id");
			$workitem_id = intval($workitem_id);
			$cur_workitem = parent::get_curr_workitem($workitem_id);
			$formsetInst_id = $cur_workitem["formsetinit_id"];
			$formsetInst = parent::get_formsetInst_by_id($formsetInst_id);
			//查看那些控件可编辑
			$this->get_form_edit($formsetInst["form_vals"], $cur_workitem);
			g('smarty') -> assign('deal_type', 1);
			g('smarty') -> assign('cur_workitem', $cur_workitem);
			g('smarty') -> assign('formsetInst', $formsetInst);
			
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$formsetInst_id);
			g('smarty') -> assign('files', $files);
			
			//获取已经审批的步骤
			$workitems =parent::get_workitem_list_parent($formsetInst_id);
			g('smarty') -> assign('workitems', $workitems);
			
			//知会信息
			$notifys = g("mv_notify") ->get_notify_by_formset_id($formsetInst_id,$this->ComId);
			g('smarty') -> assign('notifys', $notifys);
			
			$rest_type = g('rest_type') -> get_by_com($this->ComId);
			g('smarty') -> assign('rest_type', $rest_type);
			
			//是否电子签署
			$is_linksign = parent::get_linksign_state();
			// 检查领签是否过期，若过期则把当前节点更改为普通审批模式
			if ($is_linksign == 1) {
				try {
					$check = g('sign_api') -> check_privirage();
				} catch (SCException $e) {
					$check = FALSE;
				}
				!$check && $is_linksign = 0;
			}
			g('smarty') -> assign('is_linksign', $is_linksign);
						
			if($cur_workitem["state"]==parent::$StateNew){
				//更新工作项状态
				g("mv_workinst")->update_workitem_state($cur_workitem["id"],$this->ComId,parent::$StateRead);
			}
			
			
			g('smarty') -> assign('title', "查看流程");
			if($cur_workitem['state']==parent::$StateNew||$cur_workitem['state']==parent::$StateRead||$cur_workitem['state']==parent::$StateSave){
				$return_list_url = $this -> app_domain.'&m=mv_list&a=get_do_list';
			}
			else{
				$return_list_url = $this -> app_domain.'&m=mv_list&a=get_do_list&is_finish=1';
			}
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('return_list_url', $return_list_url);
			
			g('smarty') -> assign('linksign_url', $this->LinksignUrl);
			
			g('smarty') -> show($this -> temp_path.'new_apply/deal.html');
				
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	/**
	 * 根据formsetinst_id获取审批明细
	 */
	public function mine_detail() {
		try {
			parent::oauth_jssdk();
			$formsetinit_id = get_var_get("formsetinit_id");
			$formsetinit_id = intval($formsetinit_id);
			$formsetInst = parent::get_formsetInst_by_id($formsetinit_id);
			$start_workitem = parent::get_start_item($formsetInst["work_id"], $formsetinit_id);
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$formsetinit_id);
			g('smarty') -> assign('files', $files);
			
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$return_list_url = $this -> app_domain.'&m=mv_list&a=get_mine_list';
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			g('smarty') -> assign('return_list_url', $return_list_url);
			
			if($start_workitem["is_start"]==1&&($start_workitem["is_returnback"]==parent::$Not_Callback)&&$start_workitem["creater_id"]==$this->UserId&&($start_workitem["state"]==parent::$StateNew||$start_workitem["state"]==parent::$StateSave||$start_workitem["state"]==parent::$StateRead)){
				//获取表单信息
				$rest_type = g('rest_type') -> get_by_com($this->ComId);
				g('smarty') -> assign('rest_type', $rest_type);
				g('smarty') -> assign('title', '编辑假单');
				
				$html_vals = $this->to_html_vals($formsetInst['form_vals']);
				
				g('smarty') -> assign('html_vals', $html_vals);
				g('smarty') -> assign('formsetInst', $formsetInst);
				g('smarty') -> assign('cur_workitem', $start_workitem);
				g('smarty') -> show($this -> temp_path.'new_apply/edit.html');
			}
			
			else{
			//*/
				if($start_workitem["state"]==parent::$StateNew){
					//更新工作项状态
					g("mv_workinst")->update_workitem_state($start_workitem["id"],$this->ComId,parent::$StateRead);
				}
				//查看那些控件可编辑
				$this->get_form_edit($formsetInst["form_vals"], $start_workitem);
				//获取已经审批的步骤
				$workitems =parent::get_workitem_list_parent($formsetinit_id);
				g('smarty') -> assign('workitems', $workitems);
				//知会信息
				$notifys = g("mv_notify") ->get_notify_by_formset_id($formsetinit_id,$this->ComId);
				g('smarty') -> assign('notifys', $notifys);
				
				$rest_type = g('rest_type') -> get_by_com($this->ComId);
				g('smarty') -> assign('rest_type', $rest_type);
				g('smarty') -> assign('deal_type', 2);
				g('smarty') -> assign('cur_workitem', $start_workitem);
				g('smarty') -> assign('formsetInst', $formsetInst);
				
				g('smarty') -> assign('linksign_url', $this->LinksignUrl);
				
				g('smarty') -> show($this -> temp_path.'new_apply/deal.html');
			}
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	/**
	 * 根据notify_id获取审批明细
	 */
	public function notify_detail() {
		try {
			$notify_id = get_var_get("notify_id");
			$notify_id = intval($notify_id);
			$notify = parent::get_notify_item($notify_id);
			//查看那些控件可编辑
			$this->get_form_edit($notify["form_vals"], NULL);
			g('smarty') -> assign('notify', $notify);
			
			if ($notify["state"]==parent::$StateReading){
				parent::update_notify_state_parent($notify_id);
			}
			
			//获取已经审批的步骤
			$workitems =parent::get_workitem_list_parent($notify['formsetinst_id']);
			g('smarty') -> assign('workitems', $workitems);
			//知会信息
			$notifys = g("mv_notify") ->get_notify_by_formset_id($notify['formsetinst_id'],$this->ComId);
			g('smarty') -> assign('notifys', $notifys);
			
			$files = g("mv_file")->get_file_by_formsetid($this->ComId,$notify['formsetinst_id']);
			g('smarty') -> assign('files', $files);
			
			g('smarty') -> assign('deal_type', 3);
			
			$page_url = $this -> app_domain.'&m=mv_open';
			$open_url = $this -> app_domain.'&m=mv_list';
			$save_url = $this -> app_domain.'&m=mv_send';
			$send_url = $this -> app_domain.'&m=mv_open&a=send_page';
			$return_list_url = $this -> app_domain.'&m=mv_list&a=get_notify_list';
			g('smarty') -> assign('return_list_url', $return_list_url);
			g('smarty') -> assign('page_url', $page_url);
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('save_url', $save_url);
			g('smarty') -> assign('send_url', $send_url);
			
			g('smarty') -> assign('linksign_url', $this->LinksignUrl);
			
			g('smarty') -> show($this -> temp_path.'new_apply/notify_deal.html');
			
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
		
	}
	
	
	/**
	 * 获取流程图
	 * Enter description here ...
	 */
	public function get_mv_pic(){
		try {
			$formsetinst_id = get_var_get("formsetinst_id");
			$formsetinst_id = intval($formsetinst_id);
			$deal_type= get_var_get('deal_type');
			
			$formsetinst = parent::get_formsetInst_by_id($formsetinst_id);
			$work_model = parent::get_work_node($formsetinst["work_id"]);
			$worknodes_arr = parent::get_pic_rule($formsetinst);
			
			if($deal_type==1){//待办详情
				$workitem_id = get_var_get('workitem_id');
				g('smarty') -> assign('workitem_id', $workitem_id);
			}
			if($deal_type==3){//知会详情
				$notify_id = get_var_get('notify_id');
				g('smarty') -> assign('notify_id', $notify_id);
			}
			$open_url = $this -> app_domain.'&m=mv_list';
			
			g('smarty') -> assign('open_url', $open_url);
			g('smarty') -> assign('deal_type', $deal_type);
			g('smarty') -> assign('formsetinst', $formsetinst);
			g('smarty') -> assign('work_model', $work_model);
			g('smarty') -> assign('worknodes_arr', $worknodes_arr);
			g('smarty') -> show($this -> temp_path.'new_apply/mv_pic.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	
	
/**
	 * 上传文件
	 *
	 * @access public
	 * @return void
	 */
	public function upload_file() {
		$file = array_shift($_FILES);
		if (empty($file) || !is_array($file)) {
			cls_resp::echo_err(cls_resp::$FileNotFound, '找不到该上传文件，请重新上传！');
		}

		parent::log('开始上传文件');

		try {
			$data = g('mv_file') -> upload_file($file);
		}catch(SCException $e) {
			parent::log('上传文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('上传文件成功');

		$info = array(
			'rec' => $data,
		);

		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
/**
	 * 打开领签页面
	 */
	private function open_linksign(){
		$data = parent::get_post_data(array('workitem_id','formsetinst_id'));
		$workitem_id= intval($data['workitem_id']);
		$formsetinst_id = intval($data['formsetinst_id']);
		$redirect=SYSTEM_HTTP_DOMAIN."index.php?app=".$this->AppName."&m=mv_open&a=get_save_linksign&workitemid=".$workitem_id."&formsetinst_id=".$formsetinst_id;
		$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
		try{
			$linksign = g("sign_api") -> get_sign_url($user_name,$redirect);
			if(!empty($linksign)){
				$redirect_url = $linksign["redirect"];
				echo json_encode(array('errcode'=>0,'redirect_url'=>$redirect_url));
			}
			else{
				echo json_encode(array('errcode'=>999998,'errmsg' => "链接为空"));
			}
		}
		catch (SCException $e){
			echo json_encode(array('errcode'=>999999,'errmsg' => $e ->getMessage()));
		}
	}
	
/**
	 * 下载文件
	 *
	 * @access public
	 * @return void
	 */
	private function download_file() {
		
		$data = parent::get_post_data(array('id','formsetinst_id','workitem_id','notify_id','deal_type'));
		$cf_id = (int)$data['id'];
		
		$formsetinst_id = (int)$data['formsetinst_id'];
		
		$workitem_id = (int)$data['workitem_id'];
		
		if (empty($cf_id)||empty($formsetinst_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的文件对象！');
		}

		parent::log('开始下载文件');
		try {
			$result = g('mv_file') -> download_file($cf_id,$this->ComId,$formsetinst_id,$this->app_combo,'rest',$this->UserId);
		}catch(SCException $e) {
			parent::log('下载文件失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		
		$user = g('user') -> get_by_id($this->UserId);
		
		$msg_user = array($user['acct']);
				
		$msg_title="重新打开详情页面";
		
		if($data['deal_type']==1){//待办
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=rest&m=mv_open&a=detail&workitem_id='.$workitem_id;//TODO
		}
		else if($data['deal_type']==2){//起草
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=rest&m=mv_open&a=mine_detail&formsetinit_id='.$formsetinst_id;//TODO
		}
		else if($data['deal_type']==3){//知会
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=rest&m=mv_open&a=notify_detail&notify_id='.$data['notify_id'];//TODO
		}
		else if($data['deal_type']==4){//编辑状态
			$msg_url = SYSTEM_HTTP_DOMAIN.'?app=rest&m=mv_open&a=detail&workitem_id='.$workitem_id.'&is_edit=true';//TODO
		}
		try{
			parent::send_single_news($msg_user, $msg_title, '重新打开详情页面', $msg_url);
		}catch(SCException $e){
			$fail_user .=$this->UserId.' ';
		}
		parent::log('下载文件成功');
		cls_resp::echo_ok();
	}
	
	
	/**
	 * 获取当前编号的值
	 */
	private function get_cur_reference(){
		try{
			$data = parent::get_post_data(array('id'));
			
			$ret = g("mv_reference")->get_cur_reference($data["id"],$this->UserId,'use_date,dept_ids');
			
			if(!$ret){
				throw new SCException('没有权限查看该文号');
			}
			
			cls_resp::echo_ok(cls_resp::$OK, 'ret', $ret);
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 根据分类信息获取
	 */
	private function get_form(){
		try{
			$data = parent::get_post_data(array('classify_id'));
			
			$ret_list = g("mv_form")->get_form_by_classify($this->ComId,$data["classify_id"],$this->UserId);
			
			cls_resp::echo_ok(cls_resp::$OK, 'ret_list', $ret_list);
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	/**
	 * 根据实例form_vals转化编辑的值
	 * Enter description here ...
	 * @param unknown_type $form_vals
	 */
	
	private function to_html_vals($form_vals){
		$html_vals = array();
		
		if(!is_array($form_vals)){
			$form_vals = json_decode($form_vals,TRUE);
		}
		
		foreach ($form_vals as $form_val){
			if($form_val["id"]=='reason'){
				$html_vals[$form_val["id"]] = $form_val['val'];
				continue;
			}
			if($form_val["id"]=='type'){
				$html_vals[$form_val["id"]] = $form_val['type_num'];
				continue;
			}
			if($form_val["id"]=='start_time'){
				$html_vals[$form_val["id"]] = $form_val['val'];
				continue;
			}
			if($form_val["id"]=='end_time'){
				$html_vals[$form_val["id"]] = $form_val['val'];
				continue;
			}
			if($form_val["id"]=='days_hours'){
				$html_vals["days"] = $form_val['days'];
				$html_vals["hours"] = $form_val['hours'];
				continue;
			}
		}
		
		return $html_vals;
	}
	
/**
	 * 
	 * 获取该节点中那些控件不可编辑
	 * @param 表单 $form
	 * @param 步骤节点信息 $work_node
	 */
	private function get_form_edit(&$form_vals,$work_node){
		if(empty($work_node)){
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			foreach ($form_vals as $input_key => &$form_val){
				$form_val["edit_input"] = parent::$NotEditInput;
			}
			unset($form_val);
			$form_vals = json_encode($form_vals);
		}
		else{
			$input_visble_rule = $work_node["input_visble_rule"];
			$input_visble_rule = json_decode($input_visble_rule, TRUE);
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			
			$is_start = $work_node["is_start"];
			foreach ($form_vals as $input_key => &$form_val){
				if($is_start == parent::$StartNode){//开始节点，如果存在，则为不可编辑控件
					if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
						$form_val["edit_input"] = parent::$NotEditInput;
					}
					else{
						$form_val["edit_input"] = parent::$EditInput;
					}
				}
				else{//其他节点，如果存在，则为可编辑控件
					if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
						$form_val["edit_input"] = parent::$EditInput;
					}
					else{
						$form_val["edit_input"] = parent::$NotEditInput;
					}
				}
			}
			unset($form_val);
			$form_vals = json_encode($form_vals);
			
		}
		$escapers = array("'","\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
			$replacements = array("‘","\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
			$form_vals = str_replace($escapers, $replacements, $form_vals);
	}
	
}

//end of file