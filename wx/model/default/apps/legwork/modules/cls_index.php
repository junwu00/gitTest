<?php
/**
 * 移动外勤默认控制器
 *
 * @author LiangJianMing
 * @create 2015-2-9
 */
class cls_index extends abs_app_base {
	public function __construct() {
		parent::__construct('legwork');
	}
	
	public function index(){
		$filename = 'index.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}
}