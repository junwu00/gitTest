<?php
/**
 * 移动外勤计划相关控制器
 *
 * @author LiangJianMing
 * @create 2015-2-9
 */
class cls_plan extends abs_app_base {
	public function __construct() {
		parent::__construct('legwork');
	}

	/**
	 *展示所有计划列表
	 *
	 * @access public
	 * @return void
	 */
	public function lister() {
		$is_self = (int)get_var_value('is_self');
		$is_self = $is_self ? 1 : 0;

		try {
			$conf = g('legwork') -> get_conf();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}

		//是否查看全部下级（包含直属）的计划列表
		$under_check = FALSE;
		$is_self == 0 and !empty($conf['all_junior']) and $under_check = TRUE;

		global $app, $module, $action;
		$plan_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=lister';
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=info';
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign';
		$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=edit';
		$load_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=106';
		$del_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=103';

		g('smarty') -> assign('title', '移动外勤计划');
		g('smarty') -> assign('is_self', $is_self);
		g('smarty') -> assign('plan_list_url', $plan_list_url);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('edit_url', $edit_url);
		g('smarty') -> assign('load_url', $load_url);
		g('smarty') -> assign('del_url', $del_url);

		g('smarty') -> assign('under_check', $under_check);

		g('smarty') -> show($this -> temp_path.'plan/index.html');
	}

	/**
	 *展示计划详细信息
	 *
	 * @access public
	 * @return void
	 */
	public function info() {
		$pn_id = (int)get_var_value('id');
		if (empty($pn_id)) {
			cls_resp::show_err_page('无效的参数!');
		}

		$info = g('legwork') -> get_plan_info($pn_id);
		if (!$info) {
			cls_resp::show_err_page('找不到该计划!');
		}
		
		//企业号js_sdk支持
		$this -> oauth_jssdk();

		$is_self = FALSE;
		$info['user_id'] == $_SESSION[SESSION_VISIT_USER_ID] && $is_self = TRUE;

		global $app, $module, $action;
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=info&id=' . $pn_id;
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign&id=' . $pn_id . '&mark_type=3';
		$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=edit&id=' . $pn_id;
		$delete_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=103&id=' . $pn_id;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=lister';
		$trail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=show&id=' . $pn_id;

		g('smarty') -> assign('title', '计划详情');
		g('smarty') -> assign('rec_info', $info);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('edit_url', $edit_url);
		g('smarty') -> assign('delete_url', $delete_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('trail_url', $trail_url);

		g('smarty') -> assign('is_self', $is_self);
		if ($is_self) {
			g('smarty') -> assign('APP_MENU', $this -> app_menu['plan_self']);
		} else {
			g('smarty') -> assign('APP_MENU', $this -> app_menu['plan_sub']);
		}

		g('smarty') -> show($this -> temp_path.'plan/view.html');
	}

	/**
	 * 添加计划
	 *
	 * @access public
	 * @return void
	 */
	public function add() {
		$plan_conf = g('legwork') -> get_conf();

		$out_type = array();
		if (!empty($plan_conf)) {
			$tmp_arr = json_decode($plan_conf['outgoing_type'], TRUE);
			is_array($tmp_arr) && $out_type = $tmp_arr;
		}
		$user = g('user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);

		global $app, $module, $action;
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=add';
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=lister';
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign&mark_type=1';
		$ajax_add_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=101';
		
		g('smarty') -> assign('title', '创建计划');
		g('smarty') -> assign('user_info', $user);
		g('smarty') -> assign('APP_MENU', $this -> app_menu['add']);

		g('smarty') -> assign('storagr_key',__FUNCTION__.'legwork'.$_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('ajax_add_url', $ajax_add_url);

		//外出类型集合
		g('smarty') -> assign('out_type', $out_type);

		g('smarty') -> show($this -> temp_path.'plan/add.html');
	}

	/**
	 * 编辑计划
	 *
	 * @access public
	 * @return void
	 */
	public function edit() {
		$pn_id = (int)get_var_value('id');
		if (empty($pn_id)) {
			cls_resp::show_err_page('无效的参数!');
		}

		$info = g('legwork') -> get_plan_info($pn_id, FALSE);
		if (!$info) {
			cls_resp::show_err_page('找不到该计划!');
		}
		
		if ($info['user_id'] != $_SESSION[SESSION_VISIT_USER_ID]) {
			cls_resp::show_err_page('找不到该计划!');
		}

		$plan_conf = g('legwork') -> get_conf();

		$out_type = array();
		if (!empty($plan_conf)) {
			$tmp_arr = json_decode($plan_conf['outgoing_type'], TRUE);
			is_array($tmp_arr) && $out_type = $tmp_arr;
		}

		$is_self = FALSE;

		global $app, $module, $action;
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=edit&id=' . $pn_id;
		$update_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102&pn_id=' . $pn_id;
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=info&id=' . $pn_id;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=lister';
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign&mark_type=1';

		g('smarty') -> assign('title', '编辑计划');
		g('smarty') -> assign('rec_info', $info);
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('update_url', $update_url);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('pn_id', $pn_id);

		g('smarty') -> assign('APP_MENU', $this -> app_menu['edit']);

		//外出类型集合
		g('smarty') -> assign('out_type', $out_type);

		g('smarty') -> show($this -> temp_path.'plan/edit.html');
	}
}