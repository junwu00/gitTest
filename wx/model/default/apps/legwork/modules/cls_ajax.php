<?php
/**
 * 移动外勤全部ajax交互类
 *
 * @author LiangJianMing
 * @create 2015-2-9
 */
class cls_ajax extends abs_app_base {
	public function __construct() {
		parent::__construct('legwork');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		$func = '';
		switch($cmd) {
			case 101 : {
				//新建外出计划
				$this -> add_plan();
				BREAK;
			}
			case 102 : {
				//更新移动外勤计划
				$this -> update_plan();
				BREAK;
			}
			case 103 : {
				//删除移动外勤计划
				$this -> delete_plan();
				BREAK;
			}
			case 104 : {
				//添加一次上报信息
				$this -> add_report();
				BREAK;
			}
			case 105 : {
				//使用media_id上传图片
				$this -> upload();
				BREAK;
			}
			case 106 : {
				//获取计划列表
				$this -> get_plan_list();
				BREAK;
			}
			case 107 : {
				//获取单个计划的上报信息列表
				$this -> get_report_list();
				BREAK;
			}
			case 108 : {
				//获取单个计划的详细信息
				$this -> get_plan_info();
				BREAK;
			}
			case 109 : {
				//获取查岗列表
				$this -> get_plan_with_reports();
				BREAK;
			}
			case 110 : {
				//坐标转换
				$this -> trans_gps2gd();
				BREAK;
			}
			default : {
				//非法的请求
				cls_resp::echo_resp(cls_resp::$ForbidReq);
				BREAK;
			}
		}
	}

	/**
	 * 创建计划
	 *
	 * @access public
	 * @return void
	 */
	public function add_plan() {
		$fields = array(
			'title', 'address', 'mobile',
			'begin_time', 'end_time', 'outgoing_name',
			'sign_type', 'longt', 'lat',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			$this -> log('创建移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_exp($e);
		}

		$title 			= trim($data['title']);
		$address 		= trim($data['address']);
		$mobile 		= trim($data['mobile']);
		$begin_time 	= intval(strtotime($data['begin_time']));
		$end_time 		= intval(strtotime($data['end_time']));
		$outgoing_name 	= trim($data['outgoing_name']);
		$sign_type 		= intval($data['sign_type']);
		$longt 			= floatval($data['longt']);
		$lat 			= floatval($data['lat']);

		$desc = isset($data['desc']) ? trim($data['desc']) : '';

		if (empty($title) || empty($address) || empty($mobile) || empty($begin_time) || empty($end_time) || empty($outgoing_name) || empty($sign_type)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数！');
		}

		if (!check_data($mobile, 'mobile')) {
			cls_resp::echo_err(cls_resp::$Busy, '手机号码格式不正确！');
		}

		if ($sign_type != 1 && $sign_type != 2) {
			cls_resp::echo_err(cls_resp::$Busy, '签到方式只能选择两种中的一种！');
		}

		$time = time();
		if ($end_time <= $time || $end_time <= $begin_time) {
			cls_resp::echo_err(cls_resp::$Busy, '结束时间必须大于开始时间，并大于当前时间！');
		}

		$data = array(
			'title' 		=> $title,
			'desc' 			=> $desc,
			'address' 		=> $address,
			'longt' 		=> $longt,
			'lat' 			=> $lat,
			'mobile' 		=> $mobile,
			'begin_time' 	=> $begin_time,
			'end_time' 		=> $end_time,
			'outgoing_name' => $outgoing_name,
			'sign_type' 	=> $sign_type,
			//默认不提醒
			'sign_in_tip_time'  => 0,
			'sign_out_tip_time' => 0,
		);

		$this -> log('开始创建移动外勤计划');

		try {
			$conf = g('legwork') -> get_conf();
			$conf['sign_in_tip'] && $data['sign_in_tip_time'] = $begin_time + $conf['sign_in_tip_time'];
			$conf['sign_out_tip'] && $data['sign_out_tip_time'] = $end_time + $conf['sign_out_tip_time'];

			$ret = g('legwork') -> add_plan($data);
			$this -> log('创建移动外勤计划成功');
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('plan' => $ret));
		}catch(SCException $e) {
			$this -> log('创建移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy();
		}
	}

	/**
	 * 更新计划
	 *
	 * @access public
	 * @return void
	 */
	public function update_plan() {
		$fields = array(
			'title', 'address', 'mobile',
			'begin_time', 'end_time', 'outgoing_name',
			'sign_type', 'longt', 'lat',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			$this -> log('更新移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_exp($e);
		}

		$pn_id 			= (int)get_var_value('pn_id');
		$title 			= trim($data['title']);
		$address 		= trim($data['address']);
		$mobile 		= trim($data['mobile']);
		$begin_time 	= intval(strtotime($data['begin_time']));
		$end_time 		= intval(strtotime($data['end_time']));
		$outgoing_name 	= trim($data['outgoing_name']);
		$sign_type 		= intval($data['sign_type']);
		$longt 			= floatval($data['longt']);
		$lat 			= floatval($data['lat']);

		$desc = isset($data['desc']) ? trim($data['desc']) : '';

		if (empty($pn_id) || empty($title) || empty($address) || empty($mobile) || empty($begin_time) || empty($end_time) || empty($outgoing_name) || empty($sign_type)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数！');
		}

		if (!check_data($mobile, 'mobile')) {
			cls_resp::echo_err(cls_resp::$Busy, '手机号码格式不正确！');
		}

		if ($sign_type != 1 && $sign_type != 2) {
			cls_resp::echo_err(cls_resp::$Busy, '签到方式只能选择两种中的一种！');
		}

		$time = time();
		if ($end_time <= $time || $end_time <= $begin_time) {
			cls_resp::echo_err(cls_resp::$Busy, '结束时间必须大于开始时间，并大于当前时间！');
		}

		$data = array(
			'title' 		=> $title,
			'desc' 			=> $desc,
			'address' 		=> $address,
			'longt' 		=> $longt,
			'lat' 			=> $lat,
			'mobile' 		=> $mobile,
			'begin_time' 	=> $begin_time,
			'end_time' 		=> $end_time,
			'outgoing_name' => $outgoing_name,
			'sign_type' 	=> $sign_type,
			'sign_in_tip_time'  => 0,
			'sign_out_tip_time' => 0,
		);

		$this -> log('开始更新移动外勤计划');

		try {
			$conf = g('legwork') -> get_conf();
			$conf['sign_in_tip'] && $data['sign_in_tip_time'] = $begin_time + $conf['sign_in_tip_time'];
			$conf['sign_out_tip'] && $data['sign_out_tip_time'] = $end_time + $conf['sign_out_tip_time'];

			g('legwork') -> update_plan($pn_id, $data);
			$this -> log('更新移动外勤计划成功');
			cls_resp::echo_ok();
		}catch(SCException $e) {
			$this -> log('更新移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy();
		}
	}

	/**
	 * 删除计划
	 *
	 * @access public
	 * @return void
	 */
	public function delete_plan() {
		$id = (int)get_var_value('id');

		if (empty($id)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数！');
		}

		$this -> log('开始删除移动外勤计划');

		try {
			g('legwork') -> delete_plan($id);
			$this -> log('删除移动外勤计划成功');
			cls_resp::echo_ok();
		}catch(SCException $e) {
			$this -> log('删除移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy();
		}
	}

	/**
	 * 上报实时状态
	 *
	 * @access public
	 * @return void
	 */
	public function add_report() {
		$fields = array(
			'pn_id', 'longt', 'lat',
			'address', 'mark', 'mark_type',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$pn_id 		= preg_replace('/[^\d]+/', '', $data['pn_id']);
		$longt 		= preg_replace('/[^\d\.]+/', '', $data['longt']);
		$lat 		= preg_replace('/[^\d\.]+/', '', $data['lat']);
		$address 	= trim($data['address']);
		$mark 		= trim($data['mark']);
		$mark_type  = (int)$data['mark_type'];

		if ($mark_type < 1 || $mark_type > 3) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数！');
		}

		if (empty($address)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数！');
		}

		$info = g('legwork') -> get_plan_info($pn_id, FALSE);
		if (empty($info)) {
			cls_resp::echo_err(cls_resp::$Busy, '找不到该计划');
		}

		if ($info['user_id'] != $_SESSION[SESSION_VISIT_USER_ID]) {
			cls_resp::echo_err(cls_resp::$Busy, '无权进行该操作');
		}

		if ($info['state'] == 3) {
			cls_resp::echo_err(cls_resp::$Busy, '该计划已完成签退');
		}
		if ($mark_type == 1 and $info['state'] != 1) {
			cls_resp::echo_err(cls_resp::$Busy, '请勿重复签到');
		}
		if ($mark_type == 2 and $info['state'] != 2) {
			cls_resp::echo_err(cls_resp::$Busy, '签退前请先签到');
		}
		if ($mark_type == 3 and $info['state'] != 2) {
			cls_resp::echo_err(cls_resp::$Busy, '请先签到');
		}
		if ($mark_type != 1 and $info['sign_type'] == 2) {
			cls_resp::echo_err(cls_resp::$Busy, '该计划仅支持签到');
		}

		$conf = g('legwork') -> get_conf();
		if (empty($conf)) {
			cls_resp::echo_err(cls_resp::$Busy, '移动外勤配置出错');
		}

		$image_1 = isset($data['image_1']) ? trim($data['image_1']) : '';
		$image_2 = isset($data['image_2']) ? trim($data['image_2']) : '';
		$image_3 = isset($data['image_3']) ? trim($data['image_3']) : '';
		$image_4 = isset($data['image_4']) ? trim($data['image_4']) : '';
		$image_5 = isset($data['image_5']) ? trim($data['image_5']) : '';

		$imgs = array();
		!empty($image_1) && $imgs[] = $image_1;
		!empty($image_2) && $imgs[] = $image_2;
		!empty($image_3) && $imgs[] = $image_3;
		!empty($image_4) && $imgs[] = $image_4;
		!empty($image_5) && $imgs[] = $image_5;

		if ($mark_type == 1) {
			if ($conf['sign_in_pic'] && empty($imgs)) cls_resp::echo_err(cls_resp::$Busy, '必须拍照才能签到！');
		}

		if ($mark_type == 2) {
			if ($conf['sign_out_pic'] && empty($imgs)) cls_resp::echo_err(cls_resp::$Busy, '必须拍照才能签退！');
		}

		$data = array(
			'pn_id'		=> $pn_id,
			'mark_type' => $mark_type,
			'longt' 	=> $longt,
			'lat' 		=> $lat,
			'address' 	=> $address,
			'mark' 		=> $mark,
			'image_1' 	=> $image_1,
			'image_2' 	=> $image_2,
			'image_3' 	=> $image_3,
			'image_4' 	=> $image_4,
			'image_5' 	=> $image_5,
		);

		$this -> log('移动外勤计划开始上报地理位置');

		try {
			g('legwork') -> add_report($mark_type, $data);
			$this -> log('移动外勤计划上报地理位置成功');
		}catch(SCException $e) {
			$this -> log('移动外勤计划上报地理位置失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}

		if ($mark_type != 3) {
			//发送wx端提醒
			g('legwork') -> send_wx_mark_tip($info['pn_id'], $info['user_id'], $info['user_name'], $info['title'], $mark_type);
			//发送pc端提醒
			g('legwork') -> send_pc_mark_tip($info['pn_id'], $info['user_id'], $info['user_name'], $info['title'], $mark_type);
		}

		cls_resp::echo_ok();
	}

	/**
	 * 获取计划列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_plan_list() {
		$data = $this -> get_post_data();

		$page = isset($data['page']) ? (int)$data['page'] : 1;
		$self = isset($data['self']) ? (int)$data['self'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$is_self = $self ? TRUE : FALSE;

		$list = g('legwork') -> get_plan_list(0, $page, $page_size, $is_self);

		cls_resp::echo_ok(NULL, 'info', isset($list['data']) ? $list['data'] : array());
	}

	/**
	 * 获取报告列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_report_list() {
		$pn_id = (int)get_var_value('id');

		empty($pn_id) && cls_resp::echo_err(cls_resp::$Busy, '非法的访问！');

		$this -> log('开始获取报告列表');

		try {
			$list = g('legwork') -> get_report_list($pn_id);
			$this -> log('获取报告列表成功');
			cls_resp::echo_ok(NULL, 'data', $list);
		}catch(SCException $e) {
			$this -> log('获取报告列表失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy();
		}
	}

	/**
	 * 获取计划详细信息
	 *
	 * @access public  
	 * @return void
	 */
	public function get_plan_info() {
		$pn_id 		= (int)get_var_value('id');
		$with_sign 	= (int)get_var_value('with_sign');

		empty($pn_id) && cls_resp::echo_err(cls_resp::$Busy, '非法的访问！');

		$with_sign = $with_sign ? 1 : 0;

		$this -> log('开始获取移动外勤计划详细信息');

		try {
			$info = g('legwork') -> get_plan_info($pn_id, $with_sign);
			$this -> log('获取移动外勤计划详细信息成功');
			cls_resp::echo_ok(NULL, 'data', $info);
		}catch(SCException $e) {
			$this -> log('获取移动外勤计划详细信息失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy();
		}
	}

	/**
	 * 获取查岗列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_plan_with_reports() {
		$data = $this -> get_post_data();

		$page = isset($data['page']) ? (int)$data['page'] : 1;
		$self = isset($data['self']) ? (int)$data['self'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$is_self = $self ? TRUE : FALSE;

		$list = g('legwork') -> get_plan_with_reports(0, $page, $page_size, $is_self);

		cls_resp::echo_ok(NULL, 'info', isset($list['data']) ? $list['data'] : array());
	}
	
	private function trans_gps2gd() {
		$data = $this -> get_post_data('lng', 'lat');
		try {
			$ret = g('bdmap') -> trans2gd($data['lng'], $data['lat']);
			cls_resp::echo_ok(0, 'info', $ret);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
}