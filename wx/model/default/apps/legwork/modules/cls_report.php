<?php
/**
 * 移动外勤上报相关控制器
 *
 * @author LiangJianMing
 * @create 2015-2-9
 */
class cls_report extends abs_app_base {
	public function __construct() {
		parent::__construct('legwork');
	}

	/**
	 * 展示查岗列表
	 *
	 * @access public
	 * @return void
	 */
	public function lister() {
		$is_self = (int)get_var_value('is_self');
		$is_self = $is_self ? 1 : 0;

		try {
			$conf = g('legwork') -> get_conf();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
		//是否查看全部下级（包含直属）的计划列表
		$under_check = FALSE;
		$is_self == 0 and !empty($conf['all_junior']) and $under_check = TRUE;

		global $app, $module, $action;
		$reports_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=show';
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=info';
		$report_lister_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=lister';
		$load_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=109';
		$trail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=show';

		g('smarty') -> assign('title', '考勤查看');
		g('smarty') -> assign('is_self', $is_self);
		g('smarty') -> assign('reports_url', $reports_url);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('report_lister_url', $report_lister_url);
		g('smarty') -> assign('load_url', $load_url);
		g('smarty') -> assign('trail_url', $trail_url);

		g('smarty') -> assign('under_check', $under_check);

		g('smarty') -> show($this -> temp_path.'report/index.html');
	}
	
	/**
	 * 展示单个计划的上报信息列表
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		$pn_id = (int)get_var_value('id');
		if (empty($pn_id)) {
			cls_resp::show_err_page('无效的参数!');
		}
	
		$info = g('legwork') -> get_plan_info($pn_id);
		if (!$info) {
			cls_resp::show_err_page('找不到该计划!');
		}

		$list = g('legwork') -> get_report_list($pn_id);
		if (!$list) {
			g('smarty') -> assign('has_report', 0);
		} else {
			g('smarty') -> assign('has_report', 1);
			g('smarty') -> assign('report_list', $list);
		}

		global $app, $module, $action;
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=lister&id=' . $pn_id;
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign&id=' . $pn_id;
		$edit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=edit&id=' . $pn_id;
		$delete_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=103&id=' . $pn_id;

		g('smarty') -> assign('title', '查看轨迹');
		g('smarty') -> assign('rec_info', $info);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('edit_url', $edit_url);
		g('smarty') -> assign('delete_url', $delete_url);

		g('smarty') -> show($this -> temp_path.'view/trail.html');
	}

	/**
	 * 展示计划详细信息
	 *
	 * @access public
	 * @return void
	 */
	public function info() {
		$pn_id = (int)get_var_value('id');
		if (empty($pn_id)) {
			cls_resp::show_err_page('无效的参数!');
		}

		$info = g('legwork') -> get_plan_info($pn_id);
		if (!$info) {
			cls_resp::show_err_page('找不到该计划!');
		}

		//企业号js_sdk支持
		$this -> oauth_jssdk();

		$is_self = FALSE;
		$info['user_id'] == $_SESSION[SESSION_VISIT_USER_ID] && $is_self = TRUE;

		global $app, $module, $action;
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=info&id=' . $pn_id;
		$sign_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=add_sign&id=' . $pn_id;
		$list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=lister';
		$trail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=report&a=show&id=' . $pn_id;

		g('smarty') -> assign('title', '计划详情');
		g('smarty') -> assign('rec_info', $info);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('sign_url', $sign_url);
		g('smarty') -> assign('list_url', $list_url);
		g('smarty') -> assign('trail_url', $trail_url);

		g('smarty') -> assign('is_self', $is_self);
		if ($is_self) {
			g('smarty') -> assign('APP_MENU', $this -> app_menu['report_self']);
			
		} else {
			g('smarty') -> assign('APP_MENU', $this -> app_menu['report_sub']);
		}

		g('smarty') -> show($this -> temp_path.'report/view.html');
	}

	/**
	 * 展示签到、签退、实时定位页面
	 *
	 * @access public
	 * @return void
	 */
	public function add_sign() {
		$pn_id = (int)get_var_value('id');
		$mark_type = (int)get_var_value('mark_type');

		if ($mark_type < 1 || $mark_type > 3) {
			cls_resp::show_err_page('无效的参数!');
		}
		
		$conf = g('legwork') -> get_conf();
		if (empty($conf)) {
			cls_resp::show_err_page('移动外勤配置出错!');
		}
		
		if (!empty($pn_id)) {
			$info = g('legwork') -> get_plan_info($pn_id, FALSE);
			if (empty($info)) {
				cls_resp::show_err_page('找不到该计划!');
			}
	
			if ($info['user_id'] != $_SESSION[SESSION_VISIT_USER_ID]) {
				cls_resp::show_err_page('无权操作该页面!');
			}
	
			if ($info['state'] == 3) {
				cls_resp::show_err_page('该计划已完成签退!');
			}
			if ($mark_type == 1 && $info['state'] != 1) {
				cls_resp::show_err_page('请勿重复签到!');
			}
			if ($mark_type == 2 && $info['state'] != 2) {
				cls_resp::show_err_page('签退前请先签到!');
			}
			if ($mark_type == 3 && $info['state'] != 2) {
				cls_resp::show_err_page('请先签到!');
			}
			if ($mark_type != 1 && $info['sign_type'] == 2) {
				cls_resp::show_err_page('该计划仅支持签到!');
			}
	
			g('smarty') -> assign('rec_info', $info);
		}

		$plan_sel = g('legwork') -> get_plan_by_sign($mark_type);

		//企业号js_sdk支持
		$this -> oauth_jssdk();

		global $app, $module, $action;
		$plan_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=plan&a=lister&is_self=1';
		$report_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=104&id=' . $pn_id;
		$upload_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=105';
		$trans_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=110';

		g('smarty') -> assign('title', '签到/退');
		g('smarty') -> assign('plan_list_url', $plan_list_url);
		g('smarty') -> assign('report_url', $report_url);
		g('smarty') -> assign('upload_url', $upload_url);
		g('smarty') -> assign('trans_url', $trans_url);
		g('smarty') -> assign('now_time', time());
		g('smarty') -> assign('APP_MENU', $this -> app_menu['sign']);
		g('smarty') -> assign('mark_type', $mark_type);
		
		//可选的计划列表
		g('smarty') -> assign('plan_sel', $plan_sel);
		//上传图片来源：0相册+拍照； 1仅拍照；2仅相册(暂无该值)
		g('smarty') -> assign('source_type', $conf['source_type']);

		$filename = 'operation/sign-in-out.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}
}