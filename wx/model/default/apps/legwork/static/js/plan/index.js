var proc_type = 1;
var page = 0;
var plan_state = ['', '未签到', '已签到', '已签退'];

$(document).ready(function() {
	init_type_change();
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists_scroll);
});

//切换
function init_type_change() {
	$('.head ul li').each(function() {
		$(this).unbind('click').bind('click', function() {
			proc_type = $(this).attr('t'); 	
			$('.head ul li').removeClass('active');
			$(this).addClass('active');
			page = 0;
			load_lists();
		});
	});
}

//加载列表
function load_lists_scroll() {
	page += 1;
	var data = new Object();
	data.self = (proc_type == 0) ? 0 : 1;
	data.page = page;
	frame_obj.do_ajax_post(undefined, '', JSON.stringify(data), show_list);
}

//加载列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.self = (proc_type == 0) ? 0 : 1;
	data.page = page;
	frame_obj.do_ajax_post(undefined, '', JSON.stringify(data), show_list,undefined,undefined,undefined,'加载中……');
}

function show_list(data) {
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	
	var html = '';
	if (proc_type == 0) {
		html = get_sub_list(list);
	} else {
		html = get_self_list(list);
	}
	$('#rec-list').append(html);
	init_links();

	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

function get_sub_list(data) {
	var html = '';
	for (var i in data) {
		var item = data[i];
		
		if (item.state == 3) {							//已签退，不显示
			continue;
		}
		if (item.sign_type == 0 && item.state == 2) {	//仅签到，已签到，不显示
			continue;
		}
		
		var dept_desc = '';
		if (show_all == 1) {
			for (var j in item.dept_desc) {
				var dept_name = item.dept_desc[j];
				dept_desc += dept_name.short + ', ';
			}
			dept_desc = dept_desc.substr(0, dept_desc.length-2);
			dept_desc = '<span style="color: #888888; font-size: 12px; display: inline-block; margin-left: 10px;">' + dept_desc + '</p>';
		}
		html += '<div class="list-item" p="' + item.pn_id + '">' +
					'<div class="item-body">' +
						'<img src="' + item.pic_url + '64" alt="">' +
						'<div class="item-detail">' +
							'<p>' + item.user_name + dept_desc + '</p>' +
							'<p>' + item.title + '</p>' +
							'<p>' + time2date(item.begin_time).substr(0, 16) + '~' + time2date(item.end_time).substr(0, 16) + '</p>' +
						'</div>' +
					'</div>' +
					'<div class="item-loction">' +
						'<span><img alt="地址Icon" src="/apps/legwork/static/img/icon_location.png"> ' + item.address + '</span>' +	
					'</div>' +
				'</div>';
	}
	return html;
}

function get_self_list(data) {
	var html = '';
	for (var i in data) {
		var item = data[i];
		
		if (item.state == 3) {							//已签退，不显示
			continue;
		}
		if (item.sign_type == 0 && item.state == 2) {	//仅签到，已签到，不显示
			continue;
		}
		
		var opera = '';
		var state_cls = '';
		if (item.state == 1) {			//未签到
			state_cls = 'tag-gray';
			opera = '<div class="item-body-mine-operation">' +
			'<span class="signin">签到</span>	' +
			'<span class="edit">编辑</span>' +	
			'<span class="del">删除</span>' +	
			'</div>';
			
		} else if (item.state == 2) {	//已签到
			state_cls = 'tag-green';
			opera = '<div class="item-body-mine-operation-issignin">' +
			'<span class="signout">签退</span>' +
			'<span class="location"><img alt="地址Icon" src="/apps/legwork/static/img/icon_location.png">实时定位</span>' +	
			'</div>';
		}
		
		html += '<div class="list-item" p="' + item.pn_id + '">' +
				'<div class="item-detail-mine">' +
					'<p class="name">' + item.title + '</p>' +
					'<p class="time">时间：' + time2date(item.begin_time).substr(0, 16) + '~' + time2date(item.end_time).substr(0, 16) + '</p>' +
					'<p class="location">' + item.address + '</p>' +
					'<span class="item-state ' + state_cls + '">' + plan_state[item.state] + '</span>' +
				'</div>';
		
		html += opera;
		html += '</div>';
	}
	return html;
}

function init_links() {
	var info_base = $('#info-url').val();
	var edit_base = $('#edit-url').val();
	var sign_base = $('#sign-url').val();
	var del_base = $('#del-url').val();
	
	$('.list-item').each(function() {
		var id = $(this).attr('p');
		$(this).find('.item-detail-mine').unbind('click').bind('click', function() {
			location.href = info_base + '&id=' + id;
		});
		
		if ($(this).find('.item-body').length > 0) {
			$(this).unbind('click').bind('click', function() {
				location.href = info_base + '&id=' + id;
			});
		}
		
		$(this).find('.signin').unbind('click').bind('click', function() {
			location.href = sign_base + '&id=' + id + '&mark_type=1';
		});
		
		$(this).find('.edit').unbind('click').bind('click', function() {
			location.href = edit_base + '&id=' + id;
		});
		
		$(this).find('.del').unbind('click').bind('click', function() {
			var btn = $(this);
			var url = del_base + '&id=' + id;
			frame_obj.comfirm('确定要删除该计划吗？', function() {
				frame_obj.simple_ajax_post(btn, url, '', 'json', function(data) {
					if (data.errcode == 0) {
						//frame_obj.alert('删除成功', '确定', function() {});
						page = 0;
						load_lists();
						
					} else {
						frame_obj.alert(data.errmsg);
					}
				},undefined,undefined,undefined,'删除中……');
			});
		});
		
		$(this).find('.signout').unbind('click').bind('click', function() {
			location.href = sign_base + '&id=' + id + '&mark_type=2';
		});
		
		$(this).find('.location').unbind('click').bind('click', function() {
			location.href = sign_base + '&id=' + id + '&mark_type=3';
		});
	});
}