var curr_type = 1;
var map_inited = false;
var apply_type = 0;

$(document).ready(function() {
	map_local_icon_url = $('#http-domain').val() + 'apps/legwork/static/img/local.png';
	if ($('#type option').length == 0) {
		frame_obj.alert('未配置外出类型');
	}
	
	init_start_time();
	init_end_time();
	init_add();
	init_add_sign();
	init_sign_type();
	init_addr_sel();
});

function init_map() {
	$('#bd-map').css('height', $(window).height());

	if (!map_inited) {
		$('#bd-map').gdmap({
			zoom:				14,
			controller:			-1,
			defMarkerIcon:		$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',
			defMarkerIconSize:	[21,37],
			centerIcon:			$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',		//中心图片ICON
			centerIconSize:		[21,37],
			searchInputEl:		$('#suggest_id'),																	//搜索输入框
			searchListPanel:	$('#search_result_panel'),															//搜索结果显示页面
			searchBtn:			$('#map-search'),
			afterMapLoaded:		function() {
				map_inited = true;
				$('#bd-map').gdmap('setCenter', curr_point);
				$('#bd-map').gdmap('initSearch');
			},
			afterSetCenter:		function() {
				$('#bd-map').gdmap('getInfoByPosition', undefined, function(addrInfo) {
					set_title_val(addrInfo.regeocode.formattedAddress);
				});
			}
		});
	} else {
		$('#bd-map').gdmap('setCenter', curr_point);
	}
}

function selected_addr(val, point) {
	if (point != undefined) {
		curr_point = point;
	}
	set_title_val(val);
}

function init_sign_type() {
	$('input[type=radio]').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
          increaseArea: '20%'
    });
}

function init_addr_sel() {
	$('#addr').unbind('focus').bind('focus', function() {
		show_map();
		init_map();
	});
	
	$('#map-ok').unbind('click').bind('click', function() {
		curr_point = $('#bd-map').gdmap('getCenter');
		$('#addr').val($('#map-title').html());
		$('#addr').blur();
		hide_map();
	});
	
	$('#map-back').unbind('click').bind('click', function() {
		$('#addr').blur();
		hide_map();
	});
}

function show_map() {
	$('#main-panel').addClass('hide');
	$('#powerby').addClass('hide');
	$('#bottom-menu').addClass('hide');
	
	$('#main-map').removeClass('hide');
}

function hide_map() {
	$('#main-panel').removeClass('hide');
	$('#powerby').removeClass('hide');
	$('#bottom-menu').removeClass('hide');
	
	$('#main-map').addClass('hide');
}

function set_title_val(val) {
	$('#map-title').html(val);
}

function init_start_time() {
	init_datetime_scroll('#start-time');
}

function init_end_time() {
	init_datetime_scroll('#end-time');
}

function init_datetime_scroll(element) {
    var opt = {};
	opt.datetime = { preset : 'datetime', stepMinute: 1};

	$(element).val($(element).val()).scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

function init_add() {
	$('#bottom-menu a:eq(0)').attr('href', 'javascript:void(0);');
	$('#bottom-menu a:eq(0)').unbind('click').bind('click', function() {
		apply_type = 0;
		var data = get_apply_data();
		if (check_apply_data(data)) {
			//frame_obj.comfirm('确定更新该计划吗？', send_add);
			send_add();
		}
	});
}

function init_add_sign() {
	$('#bottom-menu a:eq(1)').attr('href', 'javascript:void(0);');
	$('#bottom-menu a:eq(1)').unbind('click').bind('click', function() {
		apply_type = 1;
		var data = get_apply_data();
		if (check_apply_data(data)) {
			//frame_obj.comfirm('确定更新该计划吗？', send_add);
			send_add();
		}
	});
}

function send_add() {
	$('#bottom-menu a:eq(0)').attr('disabled', 'disabled');
	$('#bottom-menu a:eq(1)').attr('disabled', 'disabled');
	var data = get_apply_data();
	frame_obj.do_ajax_post($(this), '', JSON.stringify(data), send_add_complete,undefined,undefined,undefined,'更新中……');
}

function send_add_complete(data) {
	if (data.errcode == 0) {
		if (apply_type == 0) {
			//frame_obj.alert('更新成功', '确定', function() {});
			location.href = $('#list-url').val();
			
		} else {
			var url = $('#sign-url').val() + '&id=' + $('#pn-id').val();
			//frame_obj.alert('更新成功', '确定', function() {});
			location.href = url;
		}
		return;
	}

	$('#bottom-menu a:eq(0)').attr('disabled', false);
	$('#bottom-menu a:eq(1)').attr('disabled', false);
	frame_obj.alert(data.errmsg);
}

function get_apply_data() {
	var data = new Object();
	data.title = $('#title').val();
	data.address = $('#addr').val();
	data.mobile = $('#mobile').val();
	data.begin_time = $('#start-time').val();
	data.end_time = $('#end-time').val();
	data.outgoing_name = $('#type').val();
	$('input[name=sign_type]').each(function() {
		if ($(this).attr('checked') != undefined) {
			data.sign_type = $(this).val();
		}
	});
	data.desc = $('#desc').val();
	
	if (curr_point != undefined) {
		data.longt = curr_point.lng; 
		data.lat = curr_point.lat; 
	}
	return data;
}

function check_apply_data(data) {
	if (!check_data(data.begin_time, 'notnull')) {
		frame_obj.alert('请填写开始时间');
		return false;
	}
	if (!check_data(data.end_time, 'notnull')) {
		frame_obj.alert('请填写结束时间');
		return false;
	}
	if (!check_data(data.outgoing_name, 'notnull')) {
		frame_obj.alert('请选择外出类型');
		return false;
	}
	if (!check_data(data.address, 'notnull')) {
		frame_obj.alert('请选择外出地点');
		return false;
	}
	if (!check_data(data.mobile, 'mobile')) {
		frame_obj.alert('请填写手机号码');
		return false;
	}
	if (!check_data(data.title, 'notnull')) {
		frame_obj.alert('请填写外出标题');
		return false;
	}
	if (!check_add_date(data)) {
		return false;
	}
	return true;
}

function check_add_date(data) {
	start_time = data.begin_time;
	end_time = data.end_time;

	if (start_time != '' && end_time != '') {
		var start_day = new Date(Date.parse(start_time.replace(/-(\d*)-/, '/$1/')));
		var end_day = new Date(Date.parse(end_time.replace(/-(\d*)-/, '/$1/')));
		
		if (start_day >= end_day) {
			frame_obj.alert('结束时间不能小于开始时间');
			return false;
		}
	}
	return true;
}