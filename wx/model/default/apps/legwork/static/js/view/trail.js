var map_inited = false;

$(document).ready(function() {
	if ($('#has-report').val() == 0) {
		frame_obj.alert('还没有上报过位置');
	} else {
		$('.main').removeClass('hide');
	}

	init_map();
	init_btn_list();
	init_btn_map();
});

function init_btn_list() {
	$('.btn-list').unbind('click').bind('click', function() {
		if ($('#has-report').val() == 0) {
			frame_obj.alert('还没有上报过位置');
		} else {
			$('#map-panel').addClass('hide');
			$('#list-panel').removeClass('hide');
		}
	});
}

function init_btn_map() {
	$('.btn-map').unbind('click').bind('click', function() {
		$('#map-panel').removeClass('hide');
		$('#list-panel').addClass('hide');
	});
}

function init_map() {
	$('#powerby').addClass('hide');
	$('#bd-map').css('height', $(window).height()-40);
	var reported = $('#has-report').val() == 1 ? true : false;

	var infoData = [];
	$('.rep-list li').each(function() {
		var lng = $(this).find('.lng').html();
		var lat = $(this).find('.lat').html();
		infoData.push({'lng':lng, 'lat':lat});
	});
	$('#bd-map').gdmap({
		zoom:				14,
		autoMarkCenter:		-1,
		autoFixedCenter:	-1,
		controller:			-1,
		afterMapLoaded:		function() {
			var icon = $('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png';
			var icon_walk = $('#http-domain').val() + 'apps/legwork/static/img/outworking_map_walking.png';
			if (infoData.length > 0) {
				$('#bd-map').gdmap('drawTrail', infoData, $('#start-trail'), $('#stop-trail'), {icon: icon_walk, iconSize: [50, 43]});
			}
			
			if (reported) {	//有上报过
				var idx = 1;
				var length = $('.rep-list li').length;
				
				$('.rep-list li').each(function() {
					var img = '';
					if (idx == length) {
						label = '当前位置';
						img = $('#http-domain').val() + 'apps/legwork/static/img/icon-location-min.png';
						
					} else {
						label = false;
					}
					
					var lng = $(this).find('.lng').html();
					var lat = $(this).find('.lat').html();
					var point = {lng: lng, lat: lat};
					$('#bd-map').gdmap('addMarker', {position: point, icon: icon, iconSize: [21, 37]});
					if (idx == 1) {
						$('#bd-map').gdmap('setCenter', point);
					}
//					
//					idx ++;
				});
				
			} else {		//未上报过
//				var img_url = $('#http-domain').val() + 'apps/legwork/static/img/icon-end.png';
//				$('#bd-map').gdmap('addMarker', {position: curr_point, icon: img_url, iconSize: [60, 100]});
			}
		}
	});
//	$('#bd-map').setCenter(curr_point);
}