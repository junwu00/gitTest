var position_point = undefined;
var position_loading = false;
var max_img_len = 5;
var img_list = new Array();
var rmabled = false;

$(document).ready(function() {
	$('body').gdmap({init: false});
	
	init_clock();
	init_sign();

	wx.ready(function(){
		get_position();
		$('#img-upload-item').wximgupload(wx_img_upload_opt).wximgupload('init');
		$('#img-upload-item').wximgupload('show_icon').wximgupload('set_val', img_list).wximgupload('bind_preview');
	});
	init_reposition();
});

function init_reposition() {
	$('.reposition').unbind('click').bind('click', function() {
		if (position_loading)	return;
		
		get_position();
	});
}

function get_position() {
	position_loading = true;
	$('.position').html('定位中...');
	jssdk_obj.get_location(function(res) {
		position_point = new Object();
		position_point.lng = res.longitude;
		position_point.lat = res.latitude;
		
		var point = position_point;
		$('#ajax-url').val(trans_url);
		frame_obj.do_ajax_post(undefined, '', point, function(data) {
			if (data.errcode == 0) {
				point = {lng: data.info.lng, lat: data.info.lat};
				$('body').gdmap('getInfoByPosition', point, function(ret) {
					if (ret.info == 'OK') {
						ret = ret.regeocode;
						position_point = new Object();
						position_point.name = ret.formattedAddress;
						position_point.lng = point.lng;
						position_point.lat = point.lat;
						$('.position').html(ret.formattedAddress);
						
					} else {
						$('.position').html('获取失败，请重试');
					}
					position_loading = false;
				});
				
			} else {
				$('.position').html('获取失败，请重试');
				position_loading = false;
			}
		});
	});
}

function init_sign() {
	$('#sign').unbind('click').bind('click', function() {
		var data = get_form_data();
		if (!check_form_data(data))	return;
		
		var btn = $(this);
		frame_obj.comfirm('确定上报该信息吗?', function() {
			$('#ajax-url').val(report_url);
			frame_obj.do_ajax_post(btn, '', JSON.stringify(data), function(data) {
				if (data.errcode == 0) {
					//frame_obj.alert('上报成功', '确定', function() {});
					location.href = $('#list-url').val();
					
				} else {
					frame_obj.alert(data.errmsg);
				}
			},undefined,undefined,undefined,'上报位置……');
		});
	});
}

function get_form_data() {
	var data = new Object();
	data.pn_id = $('#plan').val();
	if (position_point != undefined) {
		data.longt = position_point.lng;
		data.lat = position_point.lat;
		data.address = position_point.name;
	}
	data.mark_type = mark_type;
	data.mark = $('#mark').val();
	img_list = $('#img-upload-item').wximgupload('get_val');
	for (var i in img_list) {
		var key = 'image_' + (Number(i) + 1);
		data[key] = img_list[i];
	}
	return data;
}

function check_form_data(data) {
	if (data.pn_id == 0) {
		frame_obj.alert('请选择移动外勤计划');
		return false;
	}
	if (!check_data(data.address, 'notnull')) {
		frame_obj.alert('请先定位');
		return false;
	}
	return true;
}
	
function init_clock() {
	var curr_time = new Date().getTime();
	var diff = Number(curr_time) - Number(page_start_time);
	if (diff > 1000) {
		diff = parseInt(diff / 1000);
	} else {
		diff = 0;
	}
	system_time += diff;
	setTimeout(function() {
		update_clock();
	}, 1000);
}

function update_clock() {
	system_time++;
	$('.position-time').html(time2date(system_time));
	setTimeout(update_clock, 1000);
}