var proc_type = 1;
var page = 0;
var plan_state = ['', '未签到', '已签到', '已签退'];

$(document).ready(function() {
	init_type_change();
	load_lists();
	init_scroll_load('.container', '#rec-list', load_lists);
});

//切换
function init_type_change() {
	$('.head ul li').each(function() {
		$(this).unbind('click').bind('click', function() {
			proc_type = $(this).attr('t'); 	
			$('.head ul li').removeClass('active');
			$(this).addClass('active');
			page = 0;
			load_lists();
		});
	});
}

//加载列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.self = (proc_type == 0) ? 0 : 1;
	data.page = page;
	frame_obj.do_ajax_post(undefined, '', JSON.stringify(data), show_list,undefined,undefined,undefined,'加载中……');
}

function show_list(data) {
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	
	var html = '';
	if (proc_type == 0) {
		html = get_sub_list(list);
	} else {
		html = get_self_list(list);
	}
	$('#rec-list').append(html);
	init_links();

	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

function get_sub_list(data) {
	var html = '';
	var trail_base = $('#trail-url').val();
	for (var i in data) {
		var item = data[i];

		if (item.state == 1) {								//未签到，不显示
			continue;
		}

		if (item.state == 2) {								//已签到
			state_cls = 'tag-gray';

		} else if (item.state == 3) {						//已签到/签退
			state_cls = 'tag-green';
		}

		var dept_desc = '';
		if (show_all == 1) {
			for (var j in item.dept_desc) {
				var dept_name = item.dept_desc[j];
				dept_desc += dept_name.short + ', ';
			}
			dept_desc = dept_desc.substr(0, dept_desc.length-2);
			dept_desc = '<span style="color: #888888; font-size: 12px; display: inline-block; margin-left: 10px;">' + dept_desc + '</p>';
		}
		var url = trail_base + '&id=' + item.pn_id;
		html += '<div class="list-item" p="' + item.pn_id + '">' +
					'<div class="item-body">' +
						'<img src="' + item.pic_url + '64" alt="">' +
						'<div class="item-detail">' +
							'<p>' + item.user_name + dept_desc + '</p>' +
							'<p>' + item.title + '</p>' +
							'<p>' + time2date(item.create_time).substr(0, 16) + '</p>' +
						'</div>' +
					'</div>' +
					'<div class="item-loction">' +
						'<span><img alt="地址Icon" src="/apps/legwork/static/img/icon_location.png"> ' + item.address + '</span>' +	
					'</div>' +
					'<span class="item-state ' + state_cls + '">' + plan_state[item.state] + '</span>' +
					'<a href="' + url + '" class="btn btn-success item-trail">移动外勤轨迹</a>' +
				'</div>';
	}
	return html;
}

function get_self_list(data) {
	var html = '';
	for (var i in data) {
		var item = data[i];
		
		if (item.state == 1) {								//未签到，不显示
			continue;
		}
		
		var state_cls = '';
		if (item.state == 2) {								//已签到
			state_cls = 'tag-gray';

		} else if (item.state == 3) {						//已签到/签退
			state_cls = 'tag-green';
		}
		
		html += '<div class="list-item" p="' + item.pn_id + '">' +
				'<div class="item-detail-mine">' +
					'<p class="name">' + item.title + '</p>' +
					'<p class="time">时间：' + time2date(item.begin_time).substr(0, 16) + '~' + time2date(item.end_time).substr(0, 16) + '</p>' +
					'<p class="location">地点：' + item.address + '</p>' +
					'<span class="item-state ' + state_cls + '">' + plan_state[item.state] + '</span>' +
				'</div>';
		
		html += '</div>';
	}
	return html;
}

function init_links() {
	var info_base = $('#info-url').val();
	$('.list-item').each(function() {
		var id = $(this).attr('p');
		$(this).unbind('click').bind('click', function() {
			location.href = info_base + '&id=' + id;
		});
	});
}