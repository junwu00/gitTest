<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'legwork_operation_sign-in-out.css' => array(
		SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.css'
	),
    'legwork_operation_sign-in-out.js' => array(
        SYSTEM_ROOT . 'static/js/gdmap.js',
        SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.js',
        SYSTEM_APPS . 'legwork/static/js/operation/sign.js'
    ),
	'legwork_plan_add.css' => array(
		SYSTEM_APPS . 'legwork/static/css/plan/add.css',
		SYSTEM_ROOT . 'static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_ROOT . 'static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'legwork_plan_add.js' => array(
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
        SYSTEM_ROOT . 'static/js/gdmap.js',
        SYSTEM_ROOT . 'static/js/icheck/icheck.js',
        SYSTEM_APPS . 'legwork/static/js/plan/add.js'
    ),
	'legwork_plan_edit.css' => array(
		SYSTEM_APPS . 'legwork/static/css/plan/edit.css',
		SYSTEM_ROOT . 'static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_ROOT . 'static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css'
	),
    'legwork_plan_edit.js' => array(
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
        SYSTEM_ROOT . 'static/js/gdmap.js',
        SYSTEM_ROOT . 'static/js/icheck/icheck.js',
        SYSTEM_APPS . 'legwork/static/js/plan/edit.js'
    ),
);

return $arr;

/* End of this file */