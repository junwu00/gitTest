<?php
/**
 * 应用的配置文件_移动外勤
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 		=> 13,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(13),
	'name' 		=> 'legwork',
	'cn_name' 	=> '移动外勤',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'outsidework.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '创建计划', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=legwork&m=plan&a=add', 'icon' => ''),
		),
		'add' => array(
			array('id' => 'bmenu-1', 'name' => '提交', 'url' => '', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '提交并签到', 'url' => '', 'icon' => ''),
		),
		'edit' => array(
			array('id' => 'bmenu-1', 'name' => '更新', 'url' => '', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '更新并签到', 'url' => '', 'icon' => ''),
		),
		'plan_self' => array(
			array('id' => 'bmenu-1', 'name' => '查看轨迹', 'url' => '', 'icon' => 'icon-eye-open'),
			array('id' => 'bmenu-2', 'name' => '实时定位', 'url' => '', 'icon' => 'icon-map-marker'),
		),
		'plan_sub' => array(
			array('id' => 'bmenu-1', 'name' => '直接通话', 'url' => '', 'icon' => 'icon-phone'),
			array('id' => 'bmenu-2', 'name' => '查看轨迹', 'url' => '', 'icon' => 'icon-eye-open'),
		),
		'report_self' => array(
			array('id' => 'bmenu-1', 'name' => '查看轨迹', 'url' => '', 'icon' => 'icon-eye-open'),
		),
		'report_sub' => array(
			array('id' => 'bmenu-1', 'name' => '直接通话', 'url' => '', 'icon' => 'icon-phone'),
			array('id' => 'bmenu-2', 'name' => '查看轨迹', 'url' => '', 'icon' => 'icon-eye-open'),
		),
		'sign' => array(
			array('id' => 'bmenu-1', 'name' => '签到', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=legwork&m=report&a=add_sign&mark_type=1', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '签退', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=legwork&m=report&a=add_sign&mark_type=2', 'icon' => ''),
			array('id' => 'bmenu-3', 'name' => '定位', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=legwork&m=report&a=add_sign&mark_type=3', 'icon' => ''),
		),
	),
);

// end of file