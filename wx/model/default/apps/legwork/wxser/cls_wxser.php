<?php
/**
 * 移动外勤
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『移动外勤』操作指引',
		'desc' => '欢迎使用移动外勤应用，在这里你可以大胆告诉领导我去了哪里？不用解释一大堆，你也能快速向领导反映商机，来试一下吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213970350&idx=2&sn=2e81ba71706ea01a9565fe430772630d&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file