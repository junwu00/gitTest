<?php
/**
 * 应用的配置文件_掌上考勤
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 				=> 3,									//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(3),
	'name' 				=> 'checkwork',
	'cn_name' 			=> '掌上考勤',
	'icon' 				=> SYSTEM_HTTP_APPS_ICON.'Checkwork.png',

	'menu' => array(
		0 => array(
			array('id' => 'bmenu-1', 'name' => '签到', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=signin', 'icon' => ''),
			array('id' => 'bmenu-2', 'name' => '签退', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=signout', 'icon' => ''),
			array('id' => 'bmenu-3', 'name' => '记录查询', 
				'childs' => array(
					array('id' => 'bmenu-3-1', 'name' => '考勤记录', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=record'),
					array('id' => 'bmenu-3-2', 'name' => '月度工作时长', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=duration'),
				)
			),
		),
	),
	
	'wx_menu' => array(
		array('name' => '签到', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=signin&corpurl=<corpurl>'),
		array('name' => '签退', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=signout&corpurl=<corpurl>'),
		array('name' => '记录查询', 'sub_button' => array(
				array('name' => '考勤记录', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=record&corpurl=<corpurl>'),
				array('name' => '工作时长', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=duration&corpurl=<corpurl>'),
			),
		),
	),
);

// end of file