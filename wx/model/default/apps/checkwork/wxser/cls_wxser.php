<?php
/**
 * 考勤
 * 
 * @author yangpz
 * @date 2014-10-21
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『掌上考勤』操作指引',
		'desc' => '欢迎使用掌上考勤应用，在这里你不用排队刷卡，可以优雅用手机考勤，还能查看自己每天的考勤记录，不用担心被扣工资会不明真相啦！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213969656&idx=2&sn=446dabd9e1136495c357e5a6ac0bfce7&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	/**
	 * 处理地理位置上报
	 */
	protected function reply_location() {
		$this -> reply_point_local();
		//记录访问用户考勤位置，保存300秒
		$wxacct = $this -> post_data['FromUserName'];
		$point = g('bdmap') -> trans2gd($this -> post_data['Longitude'], $this -> post_data['Latitude']);
		$redis_key = REDIS_M_EW365_SIGN_USER_LOCAL.':'.$this -> com_id.':'.$wxacct;
		g('redis') -> set(md5($redis_key), json_encode($point), 300);
		log_write('记录员工地理位置成功：wxacct='.$wxacct.', point='.json_encode($point));
	}
	
	//--------------------------------------内部实现---------------------------
	
	/**
	 * 管理员定位
	 */
	private function reply_point_local() {
		$redis_key = md5(REDIS_QY_EW365_SEND_LOCAL_USERS.':'.$this -> com_id);
		if (!g('redis') -> exists($redis_key))	return;
		
		$user_list = g('redis') -> get($redis_key);
		$user_list = json_decode($user_list, TRUE);
		$curr = $this -> post_data['FromUserName'];
		if (!in_array($curr, $user_list))	return;
		
		unset($user_list[array_search($curr, $user_list)]);
		g('redis') -> set($redis_key, json_encode($user_list));
		
		$result = g('bdmap') -> trans2gd($this -> post_data['Longitude'], $this -> post_data['Latitude']);
		$lng = $result['lng'];
		$lat = $result['lat'];
		echo  g('wxqy') -> get_text_reply($this -> post_data, "您当前的位置为\n经度：{$lng}\n纬度：{$lat}\n请将位置坐标填写到移步到微后台，以便实现精准定位");
	}
	
}

// end of file