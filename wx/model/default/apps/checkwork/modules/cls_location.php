<?php
/**
 * 考勤地点定位
 * @author yangpz
 * @date 2015-10-27
 *
 */
class cls_location extends abs_app_base {
	
	public function __construct() {
		parent::__construct('checkwork');
	}
	
	public function index() {
		parent::oauth_jssdk();
		$transfer_url = SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=location&a=transfer';
		g('smarty') -> assign('transfer_url', $transfer_url);
			
		g('smarty') -> assign('title', '考勤地点定位');
		g('smarty') -> show($this -> temp_path.'location.html');
	}
		/** GPS坐标转高德地图坐标 */
	public function transfer() {
		try {
			$need = array('lng', 'lat');
			$data = $this -> get_post_data($need);
			
			$point = g('bdmap') -> trans2gd($data['lng'], $data['lat']);
			cls_resp::echo_ok(cls_resp::$OK, 'info', $point);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
		
}

// end of file