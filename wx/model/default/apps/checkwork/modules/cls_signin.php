<?php
/**
 * 签到页面
 * @author yangpz
 * @date 2014-11-22
 *
 */
class cls_signin extends abs_app_base {
	private $com_id;
	private $user_id;
	
	public function __construct() {
		parent::__construct('checkwork');
		
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'signin':
				$this -> signin();
				break;
				
			case 'mood':
				$this -> save_words();
				break;
				
			default:
				//查找当前适用规则
				$user = g('user') -> get_by_id($this -> user_id);
				$rule = $this -> get_user_rule($user);
				if ($rule) {	//有配置考勤信息
					parent::oauth_jssdk();
					
					$sign_day = $this -> get_sign_day($rule);
					g('smarty') -> assign('sign_day', $sign_day);																		//获取考勤日期
					$sign_week = date('w', strtotime($sign_day));
					g('smarty') -> assign('sign_week', $sign_week == 0 ? 7 : $sign_week);												//获取考勤星期N
					g('smarty') -> assign('def_week', $rule['def_day'] == 0 ? 7 : $rule['def_day']);									//默认考勤星期N
					
					$sign_rec = g('cwork_record') -> get_signin_by_day($this -> com_id, $this -> user_id, $sign_day, $rule['type']);	//签到记录
					$rec_list = array();																								//以group作为key
					foreach ($sign_rec as $rec) {	
						$rec['sign_desc'] = g('format') -> http_time_format($rec['update_time'], FALSE);
						$rec_list[$rec['sign_group']] = $rec;	
					}
					unset($rec);
					g('smarty') -> assign('sign_rec', $rec_list);													
					
					$user = g('user') -> get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID], $this -> user_id);
					if (empty($user['pic_url'])) {
						global $suite_id;
						$access_token = g('atoken') -> get_access_token_by_com($this -> com_id, $suite_id);
						g('user') -> set_pic_url($access_token, $_SESSION[SESSION_VISIT_DEPT_ID], $user['acct']);
						$user = g('user') -> get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID], $this -> user_id);
					}
					g('smarty') -> assign('user_info', $user);
					g('smarty') -> assign('rule', $rule);
					
					g('smarty') -> assign('title', '签到');
					g('smarty') -> assign('words_url', SYSTEM_HTTP_DOMAIN.'index.php?app=checkwork&m=signin&a=words');
					if ($rule['type'] == 0) {
						g('smarty') -> show($this -> temp_path.'signin/index.html');	//标准工时
					} else {
						g('smarty') -> show($this -> temp_path.'signin/e_index.html');	//弹性工时
					}
					
				} else {
					cls_resp::show_warn_page('管理员未配置适用您部门的考勤规则');
				}
		}
	}
	
	//--------------------------内部实现--------------------------------
	
	/** 获取员工对应的考勤规则 */
	private function get_user_rule(array $user=array()) {
		empty($user) && $user = g('user') -> get_by_id($this -> user_id);
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		return g('cwork_dept_rule') -> get_dept_rule($this -> com_id, $_SESSION[SESSION_VISIT_DEPT_ID], $_SESSION[SESSION_VISIT_USER_DEPT], $dept_tree);
	}
	
	/**
	 * 员工签到
	 */
	private function signin() {
		parent::log('开始员工签到');
		try {
			$need = array('rule', 'rule_time', 'group', 'location', 'sign_day');
			$data = parent::get_post_data($need);
			
			$rule = $this -> verify_rule($data);
			$point = $this -> verify_location($data);
			$addr = $this -> verify_addr($data, $rule, $point);
			$resp = $this -> save_signin($rule, $addr, $data);
				
			parent::log('签到成功');
			cls_resp::echo_resp(cls_resp::$OK, 'info', $resp);	
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 验证规则是否变更
	 * @param unknown_type $data	请求的数据
	 */
	private function verify_rule($data) {
		$rule = $this -> get_user_rule();
		if (!$rule)										throw new SCException('考勤规则变更，请刷新页面后重试');
		if ($data['rule'] != $rule['id'])				throw new SCException('考勤规则变更，请刷新页面后重试');
		if ($data['rule_time'] != $rule['update_time'])	throw new SCException('考勤规则变更，请刷新页面后重试');
		return $rule;
	}
	
	/**
	 * 验证当前是否已配置考勤地点，地点是否合法
	 * @param unknown_type $data	请求的数据
	 * @param unknown_type $rule	考勤规则
	 * @param unknown_type $point	用户当前地理位置
	 */
	private function verify_addr($data, $rule, $point) {
		$group = $data['group'];
		$detail = $rule['detail'][$group];
		if (empty($detail))		throw new SCException('该时间段的考勤规则不存在');
		
		$min_scope = 99999;
		if ($detail['addr'] == 0) {		//查找所有考勤地点
			$addr_list = g('cwork_addr') -> lists($this -> com_id);
			if (empty($addr_list))	{
				throw new SCException('未配置考勤地点');
			}
			
			foreach ($addr_list as $addr) {			
				$ret = $this -> verify_dist($point, $addr, $min_scope);
				if (is_array($ret)) {		//有合法地点
					$target_addr = $ret;	BREAK;
					
				} else {					//非合法地点，查找下一下
					$min_scope = $ret;
				}
			}
			unset($addr);
			
		} else {						//查找指定考勤地点
			$addr = g('cwork_addr') -> get_by_id($this -> com_id, $detail['addr']);
			if (empty($addr)) {
				throw new SCException('配置的考勤地点不存在，请联系管理员');
			}
			
			$ret = $this -> verify_dist($point, $addr, $min_scope);
			if (is_array($ret)) {
				$target_addr = $ret;	
			} else {
				$min_scope = $ret;
			}
		}
		
		if (empty($target_addr)) {
			throw new SCException("在公司附近{$min_scope}米才可以签到哦");
		}
		
		return $target_addr;
	}
	
	/**
	 * 验证考勤距离是否合法
	 * @param unknown_type $point		用户位置
	 * @param unknown_type $addr		考勤地点配置
	 * @param unknown_type $min_scope	最小距离
	 * @return	合法，返回信息；非法，返回距离
	 */
	private function verify_dist($point, $addr, $min_scope) {
		$min_scope = $addr['scope'] < $min_scope ? $addr['scope'] : $min_scope;
		
		$dist = g('bdmap') -> get_distance($point['lng'], $point['lat'], $addr['lng'], $addr['lat']);
		parent::log("考勤距离：距{$addr['addr']}{$dist} 米，要求{$addr['scope']}米");
		if ($dist <= 200 || $dist <= $addr['scope']) {	//小于等于200M 或 小于配置的范围，成功
			return $addr;
		}
		if ($addr['scope'] == 0) {						//0，不限距离，一定成功
			return $addr;
		}
		
		return $min_scope;
	}

	/**
	 * 验证获取到的地理位置信息
	 * @param unknown_type $data	请求的数据
	 */
	private function verify_location($data) {
		$point = g('redis') -> get(md5(REDIS_M_EW365_SIGN_USER_LOCAL.':'.$_SESSION[SESSION_VISIT_COM_ID].':'.$_SESSION[SESSION_VISIT_USER_WXACCT]));
		$location = $data['location'];
		if (!empty($point)) {
			$point = json_decode($point, TRUE);
			log_write('签到信息：lng='.$point['lng'].', lat='.$point['lat']);
			
		} else if (isset($location)) {
			$point = $location;
			$point['lng'] = floatval($point['lng']);
			$point['lat'] = floatval($point['lat']);
			$point = g('bdmap') -> trans2gd($point['lng'], $point['lat']);
			log_write('签到信息_js：lng='.$point['lng'].', lat='.$point['lat']);
			
		} else {
			throw new SCException('获取不到您的位置，请返回掌上考勤首页重试。<br>(若未开启<span class="text-blue">提供位置信息</span>功能，请点击考勤首页右上角<i class="icon-user"></i>，开启该功能');
		}
		return $point;
	}
	
	/** 获取当前时间是哪天的考勤（因为可能跨天） */
	private function get_sign_day($rule) {
		$time = date('Hi');
		$time = intval(date('Hi'));
		$reset_time = str_replace(':', '', $rule);
		$reset_time = intval($reset_time);
		
		return $time > $reset_time ? date('Ymd') : date('Ymd', strtotime('-1 day'));
	}
	
	/**
	 * 保存签到结果
	 */
	private function save_signin($rule, $target_addr, $data) {
		$sign_day = $this -> get_sign_day($rule);
		if ($data['sign_day'] != $sign_day) {
			throw new SCException('已到新一天考勤时间，新刷新页面后再签到');
		}
		
		$sign_type = $rule['type'] == 0 ? 0 : 2;	//0：标准工时签到；2：弹性工时签到
		$signed = g('cwork_record') -> has_signed($this -> com_id, $this -> user_id, $sign_day, $data['group'], $sign_type, FALSE);	//是否签到过
		if ($signed) {
			return array(
				'rec' => $signed['id'],
				'signed' => TRUE, 
				'sign_time' => date('Y-m-d H:i:s', $signed['update_time']), 
				'time_desc' => g('format') -> http_time_format($signed['update_time'], FALSE),
				'is_late' => $signed['state'] == 1 ? TRUE : FALSE,
				'diff' => 0
			);
			
		} else {
			$sign_group = $rule['detail'][$data['group']];
			if ($rule['type'] == 0) {
				$sign_limit = $sign_group['signin_time'] . '~' . $sign_group['signout_time'];
			} else {
				$sign_limit = $sign_group['e_work_hour'].'小时'.$sign_group['e_work_min'].'分钟';
			}
			$sign_ret = g('cwork_record') -> get_sign_state($this -> com_id, $this -> user_id, $sign_day, $rule, $data['group'], $sign_type);
			$sign_state = $sign_ret['state'];
			$result = g('cwork_record') -> save_signin($this -> com_id, $this -> user_id, $_SESSION[SESSION_VISIT_USER_WXACCT], $sign_day, $data['group'], $sign_type, $sign_limit, $target_addr, $sign_state, $rule);
			if ($result) {
				return array(
					'rec' => $result,
					'signed' => FALSE,
					'sign_time' => date('Y-m-d H:i:s', time()),
					'time_desc' => '刚刚',
					'is_late' => $sign_state == 1 ? TRUE : FALSE,
					'diff' => $sign_ret['time']
				);
			}
		}

		throw new SCException('签到失败：系统繁忙');
	}
	
	/**
	 * 保存签到心情
	 */
	private function save_words() {
		parent::log('开始保存签到说说');
		try {
			$data = parent::get_post_data(array('signin', 'words'));
			g('cwork_record') -> save_mood($this -> com_id, $this -> user_id, $data['signin'], $data['words']);
			parent::log('保存签到说说成功');
			cls_resp::echo_ok();	
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
}

// end of file