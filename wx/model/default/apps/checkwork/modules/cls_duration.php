<?php
/**
 * 工作时长查询
 * @author yangpz
 * @date 2014-11-25
 *
 */
class cls_duration extends abs_app_base {
	
	public function __construct() {
		parent::__construct('checkwork');
	}
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'report':
				$this -> get_mon_report();
				break;
			
			default:
				g('smarty') -> assign('title', '月度工作时长');
				g('smarty') -> show($this -> temp_path.'duration/index.html');
		}
	}
	
	//----------------------------------内部实现----------------------------------
	
	/**
	 * 获取员工月工作时长信息
	 */
	private function get_mon_report() {
		try {
			$data = parent::get_post_data(array('year', 'mon'));
			$year = empty($data['year']) ? date('Y') : $data['year'];
			$mon = empty($data['mon']) ? date('m') : $data['mon'];
			
			$info = g('cwork_record') -> get_mon_report($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $year, $mon);
			$days = 0;
			$total_time = 0;
			$per_time = 0;
			$late_times = 0;
			$early_times = 0;
			$empty_times = 0;
			$signin_times = 0;
			$signout_times = 0;
			
			if (!empty($info)) {
				$effect_state = array(0, 1, 2);	//有效的状态：0正常 1迟到 2早退
				$pre_day = $info[0]['day'];
				
				foreach ($info as $item) {
					$curr_day = $item['day'];
					if ($curr_day != $pre_day) {	//记录完一天
						$pre_day = $curr_day;
						$days++;
					}
					if (in_array($item['signin_state'], $effect_state) && in_array($item['signout_state'], $effect_state)) {	//签到/退均有效，才记录
						$diff = $item['signout_time'] - $item['signin_time'];
						$total_time += ($diff > 0) ? $diff : 0;
					}
					if ($item['signin_state'] == 1)		$late_times++;
					if ($item['signout_state'] == 2)	$early_times++;
					if ($item['signin_state'] == 3) {
						$empty_times++;	
					} else if (!is_null($item['signin_state'])) {
						$signin_times++;
					}
					if ($item['signout_state'] == 3) {
						$empty_times++;	
					} else if (!is_null($item['signout_state'])) {
						$signout_times++;
					}
				}
				unset($item);
				$days++;
				$per_time = ceil($total_time / $days);
			}
			$ret = array(
				'days' => $days,
				'total_time' => sec_to_time($total_time),
				'pre_time' => sec_to_time($per_time),
				'late_times' => $late_times,
				'early_times' => $early_times,
				'empty_times' => $empty_times,
				'signin_times' => $signin_times,
				'signout_times' => $signout_times
			);
			
			cls_resp::echo_ok(cls_resp::$OK, 'info', $ret);
		
		} catch (SCException $e) {
			cls_resp::echo_exp($e);	
		}
	}
	
}

// end of file