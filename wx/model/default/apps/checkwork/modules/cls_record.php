<?php
/**
 * 考勤记录查询
 * @author yangpz
 * @date 2014-11-25
 *
 */
class cls_record extends abs_app_base {
	
	public function __construct() {
		parent::__construct('checkwork');
	}
	
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {
			case 'load_mon_rec':
				$data = parent::get_post_data(array('year', 'mon'));
				$info = $this -> get_user_mon_record($data['year'], $data['mon']);
				cls_resp::echo_ok(cls_resp::$OK, 'info', $info);
				break;
			
			default:
				$setting = g('cwork_setting') -> get_by_com($_SESSION[SESSION_VISIT_COM_ID]);
				if ($setting) {
					g('smarty') -> assign('setting', $setting);
				}
				$user = g('user') -> get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID], $_SESSION[SESSION_VISIT_USER_ID]);
				g('smarty') -> assign('user_info', $user);
				
				g('smarty') -> assign('title', '考勤记录');
				g('smarty') -> show($this -> temp_path.'record/index.html');
		}
	}
	
	//----------------------------------内部实现-----------------------------------
	
	/**
	 * 获取员工月度考勤信息
	 */
	private function get_user_mon_record($year, $mon) {
		$curr_year = date('Y');
		$curr_mon = date('m');
		$gone_days = 0;
		$rec_list = g('cwork_record') -> get_user_mon_record($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $year, $mon, $gone_days);
		
		$signin_count = 0;
		$signout_count = 0;
		$signin_day = array();
		$signout_day = array();
		$detail = array();
		
		$firstday = date('Y-m-d', mktime(0, 0, 0, intval($mon), 1, intval($year)));
		$end_time = strtotime("{$firstday} +1 month -1 day") + (24 * 3600) - 1;		//最迟签到时间
		
		$resp_data = array();		//以day做为key的数据，用于响应请求
		$full_days = array();		//无异常的日期
		$exp_days = array();		//有异常的日期
		$signin_times = 0;			//签到次数
		$signout_times = 0;			//签退次数
		$late_times = 0;			//迟到
		$early_times = 0;			//早退
		$empty_times = 0;			//未考勤
		
		if (!empty($rec_list)) {
			$pre_day = $rec_list[0]['day'];		//第一天
			$has_exp = FALSE;					//默认没有异常记录
			
			foreach ($rec_list as $rec) {
				$day = substr($rec['day'], 6, 2);
				$day_int = intval($day);
				if ($rec['sign_type'] == 0 || $rec['sign_type'] == 2) {	//签到
					if (!is_null($rec['state']) && $rec['state'] != 3)	$signin_times++;
					$resp_data[$day_int][$rec['sign_group']]['signin'] = $rec;			
					
				} else {												//签退
					if (!is_null($rec['state']) && $rec['state'] != 3)	$signout_times++;
					$resp_data[$day_int][$rec['sign_group']]['signout'] = $rec;			
				}
				
				$curr_day = $rec['day'];
				if ($curr_day != $pre_day) {	//新一天，进行结算
					$target_day = substr($pre_day, 6, 2);
					$target_day = intval($target_day);
					if (!isset($resp_data[$target_day][$rec['sign_group']]['signin']) || !isset($resp_data[$target_day][$rec['sign_group']]['signout'])) {	//当天未签到/退，有异常
						$has_exp = TRUE;
					}
					
					if ($has_exp) {
						$exp_days[] = substr($pre_day, 6, 2);		//有异常
					} else {
						$full_days[] = substr($pre_day, 6, 2);		//无异常
					}
					
					$has_exp = FALSE;
					$pre_day = $curr_day;
				}
				if ($rec['state'] != 0) {	//有迟到、早退、未记录
					$has_exp = TRUE;
					if ($rec['state'] == 1) 		$late_times++;
					else if ($rec['state'] == 2)	$early_times++;
					else if ($rec['state'] == 3)	$empty_times++;
				}
			}
			//记录最后一批记录
			if (!isset($resp_data[$day_int][$rec['sign_group']]['signin']) || !isset($resp_data[$day_int][$rec['sign_group']]['signout'])) {	//当天未签到/退，有异常
				$has_exp = TRUE;
			}
			if ($has_exp) {
				$exp_days[] = substr($curr_day, 6, 2);		//有异常
			} else {
				$full_days[] = substr($curr_day, 6, 2);		//无异常
			}
			unset($rec);
		}
		
		$gone_days = ($curr_year != $year || $curr_mon != $mon) ? '-' : $gone_days;
		$result = array(
			'gone_days' => $gone_days,
			'full_days' => $full_days,
			'exp_days' => $exp_days,
			'signin_times' => $signin_times,
			'signout_times' => $signout_times,
			'late_times' => $late_times,
			'early_times' => $early_times,
			'empty_times' => $empty_times,
			'detail' => $resp_data,
			'list' => $rec_list
		);
		return $result;
	}
	
}

// end of file