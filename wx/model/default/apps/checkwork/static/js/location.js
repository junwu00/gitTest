jsApiList = ['getLocation'];
$(document).ready(function() {
	frame_obj.lock_screen('获取地理位置信息中...', true);
	
	$('#powerby').remove();
	init_map();
});


function init_map() {
	$('#bd-map').css('height', $(window).height());
	$('#bd-map').gdmap({
		zoom:				16,
		controller:			-1,
		autoFixedCenter:	-1,
		autoMarkCenter:		-1,
		defMarkerIcon:		$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',
		defMarkerIconSize:	[21,37],
		centerIcon:			$('#http-domain').val() + 'apps/legwork/static/img/outwork_map_location.png',		//中心图片ICON
		centerIconSize:		[21,37],
		afterMapLoaded:		function() {},
		afterSetCenter:		function() {}
	});
}

wx.ready(function(){
	get_local();
});

function get_local() {
/*test------------------	
	$('#ajax-url').val(transfer_url);
	var req_data = {
		lng: 113.31525,
		lat: 23.164963
	};
	frame_obj.do_ajax_post(undefined, '', req_data, function(resp) {
		if (resp.errcode == 0) {
			var data = resp.info;
			$('#bd-map').gdmap('setCenter', data);
			$('#bd-map').gdmap('addMarker', {position: data});
			var html = '经度：' + data.lng + '<br>纬度：' + data.lat;
			$('#map-title').html(html);
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
		frame_obj.unlock_screen();
	});
	return;
//.test------------------*/	
	
	wx.getLocation({
		success: function (res) {
			$('#ajax-url').val(transfer_url);
			var req_data = {
				lng: res.longitude,
				lat: res.latitude
			};
			frame_obj.do_ajax_post(undefined, '', req_data, function(resp) {
				if (resp.errcode == 0) {
					var data = resp.info;
					$('#bd-map').gdmap('setCenter', data);
					$('#bd-map').gdmap('addMarker', {position: data});
					var html = '经度：' + data.lng + '<br>纬度：' + data.lat;
					$('#map-title').html(html);
					
				} else {
					frame_obj.tips(resp.errmsg);
				}
				frame_obj.unlock_screen();
			});
			
		},
		cancel:function() {
			frame_obj.alert('要打开地理位置才能定位哦', '我知道了', get_local);
			frame_obj.unlock_screen();
		},
		complete:function() {
		},
	});
}