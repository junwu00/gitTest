$(document).ready(function() {
	init_send_words();
});

function init_send_words() {
	$('#send-words').unbind('click').bind('click', function() {
		var data = new Object();
		data.signin = $('#signin').val();
		data.words = $('textarea').val();
		if (check_data(data.words, 'notnull')) {
			frame_obj.do_ajax_post($(this), 'save', JSON.stringify(data), send_words_complete);
		} else {
			frame_obj.alert('内容不能为空');
		}
	});
}

function send_words_complete(data) {
	if (data.errcode == 0) {
		frame_obj.alert('发表成功', '我知道了', function() {
			location.href = data.info.target;
		});
	} else {
		frame_obj.alert(data.errmsg);
	}
}