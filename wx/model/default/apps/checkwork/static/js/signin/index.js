var cwork_touched = false;
var timeout_handler;

var cwork_local_data = new Object();
var get_local_complete = false;

var wait_signin = false;		//是否是点击后，等待获取位置，默认为false
var curr_sign_el;				//当前点击的按钮
var is_signing = false;
var js_sdk_ready = false;

jsApiList = ['getLocation'];
$(document).ready(function() {
	check_use_def();
	init_clock();
	init_time_select();
	init_sign_group();
	init_sign_word();
	
	document.ondragstart=function(){
		return false;
	}; document.oncontextmenu=function(){
		return false;
	};
});

//验证是否使用默认规则
function check_use_def() {
	if ($('#time-list > span').length == 0) {
		$('#time-list').html($('#def-conf .time-list').html());
		$('.sign-group-list').html($('#def-conf .group-list').html());
	}
	$('#def-conf').html('');
}

//签到事件
function do_sign(that) {
	var target = $(that).parents('.sign-group');
	
	var signtime = $(target).find('.sign-after .time').html();
	if ($.trim(signtime) != '') {
		frame_obj.tips('亲~你已经签到过啦');	return;
	}
	
	if (!check_sign_limit(target))	return;

	if (is_signing) {
		frame_obj.tips('正在签到中，请稍候');		return;
	}
	
	if (!get_local_complete) {
		is_signing = true;
		change_state(1, target);
		wait_signin = true;
		curr_sign_el = $(that);
		get_local();
		return;
	} else {
		do_signin($(that));
	}
}

//验证时间限制
function check_sign_limit(sign_group) {
	var time = $(sign_group).find('.clock').html().substr(0, 5);
	var start = $(sign_group).attr('data-start');
	var end = $(sign_group).attr('data-end');
	var reset_time = $('#reset-time').val();
	
	time = trans_sign_date(time, reset_time);
	start = trans_sign_date(start, reset_time);
	end = trans_sign_date(end, reset_time);
	
	if (Date.parse(time) < Date.parse(start)) {
		frame_obj.tips('签到时间未到，不能签到', {time: 2000});		return false;
	}
	if (Date.parse(time) >= Date.parse(end)) {
		frame_obj.tips('签到时间已过，不能签到', {time: 2000});		return false;
	}
	return true;
}

//将时间转换成Date方便比较
function trans_sign_date(time, reset_time) {
	reset_time = Number(reset_time.replace(':', ''));
	var time_str = Number(time.replace(':', ''));
	if (time_str >= reset_time || time_str == 0) {	//不跨天
		return new Date(2015, 1, 1, time.substr(0, 2), time.substr(3, 2), 0);
	} else {										//跨天
		return new Date(2015, 1, 2, time.substr(0, 2), time.substr(3, 2), 0);
	}
}

//初始化时间段选择
function init_time_select() {
	//适应宽度
	var win_width = $(window).width();
	var padding_width = parseInt(win_width * 0.06);
	var width = $('#time-list > span').length * 120;
	if (padding_width + width > win_width) {
		$('#time-list').css({width: padding_width + width});
	}
	//时间段选择
	var idx = 0;
	$('#time-list > span').each(function() {
		$(this).attr('data-idx', idx++);
	});
	$('#time-list > span').each(function() {
		$(this).unbind('click').bind('click', function() {
			if (wait_signin) {
				frame_obj.tips('正在签到中，不能切换时间段', {time: 2000});
				return;
			}
			
			var idx = $(this).attr('data-idx');
			var left = idx * win_width;
			$('#time-list > span').removeClass('active');
			$(this).addClass('active');
			$('.sign-group-list').animate({left: -left});
		});
	});
	//根据当前时间选定时间段
	var time = $('.sign-group:eq(0)').find('.clock').html().substr(0, 5);
	var reset_time = $('#reset-time').val();
	time = trans_sign_date(time, reset_time);
	var has_map = false;	//是否有匹配的时间段
	$('#time-list > span').each(function() {
		var item_time = trans_sign_date($(this).attr('data-end'), reset_time);
		if (!has_map && Date.parse(time) < Date.parse(item_time)) {		//当前时间小于时间段结束时间，显示该时间段为active
			has_map = true;
			$(this).click();
		}
	});
	if (!has_map) {	//无匹配时间段，将最后一时间段显示为active
		$('#time-list > span:last-child').click();
	}
}

//初始化签到页宽度
function init_sign_group() {
	var win_width = $(window).width();
	$('.sign-group').css({width: win_width});
	//签到过,显示签到结果
	$('.sign-group').each(function() {
		if ($.trim($(this).find('.sign-after .time').html()) != '') {	
			$(this).find('.sign-tips').html('&nbsp;');
			change_state(2, $(this));
		}
	});
}

//初始化说说
function init_sign_word() {
	$('.sign-group .btn-sm').each(function() {
		$(this).unbind('click').bind('click', function() {
			var target = $(this).parents('.sign-group');
			
			var total_h = $(target)[0].scrollHeight + 88;
			$(target).find('.sign-word').removeClass('hide');
			$('.container').animate({top: -total_h}, function() {
				$(target).find('.sign-word textarea').focus();
			});
			$('#bottom-menu').addClass('hide');
			$('html,body').css({overflow: 'hidden'});
		});
	});
	
	$('.sign-group .sign-word .cancel').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.container').animate({top: 0});
			$('#bottom-menu').removeClass('hide');
			$('html,body').css({overflow: 'auto'});
			var target = $(this).parents('.sign-group');
			$(target).find('.sign-word').addClass('hide');
		});
	});
}

//时钟
function init_clock() {
	var date = new Date();
	var h = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();
	h = h < 10 ? '0'+h : h; 
	m = m < 10 ? '0'+m : m; 
	s = s < 10 ? '0'+s : s; 
	$('.clock').html(h + ':' + m + ':' + s);
	setTimeout(init_clock, 1000);
}

//更新界面：0签到前，1签到中，2签到后, 3签到失败
function change_state(state, container) {
	switch (state) {
		case 0:	//前
			$(container).find('.sign-before').removeClass('hide');		
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
		case 1:	//中
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').removeClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').addClass('hide');
			$(container).find('.cycle').removeClass('hide');
			break;
		case 2:	//后(成功)
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').removeClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
		case 3: //失败
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').removeClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
	}
}

wx.ready(function(){
	js_sdk_ready = true;
});

function get_local() {
/*test------------------	
	cwork_local_data = {lng: 113.364269, lat: 23.226203};			
	if (wait_signin) {
		wait_signin = false;
		do_signin($(curr_sign_el));
		curr_sign_el = undefined;
	}
	return;
//.test------------------*/	
	
	if (!js_sdk_ready) {
		setTimeout(function() {
			get_local();
		},500);
		return;
	}
	wx.getLocation({
		success: function (res) {
			cwork_local_data.lng = res.longitude;
			cwork_local_data.lat = res.latitude;
						
			if (wait_signin) {
				wait_signin = false;
				do_signin($(curr_sign_el));
				curr_sign_el = undefined;
			}
		},
		cancel:function() {
			frame_obj.alert('要打开地理位置才能进行考勤哦', '我知道了', get_local);
		},
		complete:function() {	//请求完成后：不管成功失败，才能签到
			get_local_complete = true;
			$('#alert-dlg').modal('hide');	//自动关闭提示
		},
	});
}

//执行签到
function do_signin(btn) {
	is_signing = true;
	
	var target = $(btn).parents('.sign-group');
	change_state(1, target);
	var rule_time = $('#rule-time').val();	//规则更新时间，用于区分是否有规则变更
	var data = new Object();
	data.rule = $('#rule').val();
	data.rule_time = $('#rule-time').val();
	data.sign_day = $('#sign_day').val();
	data.group = $(target).attr('data-group');
//	cwork_local_data = {lng: 113.052063, lat: 23.118891};
//	cwork_local_data = {lng: 113.364269, lat: 23.226203};
	data.location = cwork_local_data || '';
	frame_obj.do_ajax_post(btn, 'signin', data, function(resp) {
		signin_complete(resp, target);
		is_signing = false;
		get_local_complete = false;
	});
}

//完成签到
function signin_complete(data, sign_group) {
	var container_time = $(sign_group).find('.sign-after .time');
	var tips = $(sign_group).find('.sign-tips');
	$(tips).html('&nbsp;');
	
	if (data.errcode == 0) {
		var info = data.info;
		var desc = info.sign_time + '(' + info.time_desc + ')';
		if (info.is_late) {
			$(container_time).html('<font color="#f5bf64">' + desc + '</font>');
			if (!info.signed) {
				$(tips).html('你今天迟到了' + Math.abs(Math.ceil(info.diff / 60)) + '分钟签到，下次要加油哦');
			}
		} else {
			$(container_time).html(desc);
			if (!info.signed && info.diff > 0) {
				$(tips).html('你今天提前了' + Math.abs(Math.ceil(info.diff / 60)) + '分钟签到，不错哦');
				
			} else if (!info.signed && info.diff < 0) {
				$(tips).html('你今天晚到了' + Math.abs(Math.ceil(info.diff / 60)) + '分钟签到，下次要加油哦');
			}
		}
		$(sign_group).attr('data-rec', info.rec);
		change_state(2, sign_group);
		
	} else if (data.errmsg == 0) {
		change_state(2, sign_group);
		
	} else if (data.info != undefined){
		$(sign_group).find('.sign-fail .fail-detail').html(data.info.info);
		change_state(3, sign_group);
		
	} else {
		$(sign_group).find('.sign-fail .fail-detail').html(data.errmsg);
		change_state(3, sign_group);
	}
	cwork_touched = false;
}

//保存说说
function sign_mood(that) {
	var target = $(that).parents('.sign-group');
	var rec = $(target).attr('data-rec');
	if (rec == '') {
		frame_obj.tips('请签到后再发布说说', {time: 2000});
		return;
	}
	
	var data = new Object();
	data.signin = rec;
	data.words = $(target).find('.sign-word textarea').val();
	frame_obj.do_ajax_post($(that), 'mood', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('发布成功', {time: 1000});
			setTimeout(function() {
				$(target).find('.sign-word .cancel').click();
			}, 500);
		}
	});
}