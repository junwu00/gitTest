var curr_year;
var curr_mon;
var curr_rec_info;
var rec_loading = false;

$(document).ready(function() {
	var today = new Date();
	var year = today.getFullYear();
	var mon = today.getMonth() + 1;
	load_mon_rec(year, mon);
	curr_year = year;
	curr_mon = mon;
	
	init_date_selector();
	init_panel_change();
});

//init date selector

function init_date_selector() {
	$("#date-selector").datetimepicker({
		format: "yyyymmdd",
	 	language: 'zh-CN',
	 	maxView: 2,
		minView: 2,
		autoclose: false,
	 });
	 $('#date-selector').datetimepicker('show');
	
	 init_change_mon();
	 init_change_date();
	 $("#date-selector").datetimepicker().on('changeMonth', function(ev){
	 	var val = $(this).val();
	 	curr_year = val.substring(0, 4);
	 	curr_mon = val.substring(4, 6);
	 	load_mon_rec(curr_year, curr_mon);
	 });
}

// load record list

function load_mon_rec(year, mon) {
	var data = new Object();
	data.year = year;
	data.mon = mon;
	rec_loading = true;
	frame_obj.do_ajax_post(undefined, 'load_mon_rec', JSON.stringify(data), load_rec_complete);
}

function load_rec_complete(data) {
	if (data.errcode == 0) {
		var info = data.info;
		curr_rec_info = info;
		init_sign_days();
		
		$('.gone-days').html(info.gone_days);
		$('#signin-count').html(info.signin_times + ' 次');
		$('#signout-count').html(info.signout_times + ' 次');
		$('#late-count').html(info.late_times > 0 ? '<font color="#f5bf64">' + info.late_times + ' 次</font>' : info.late_times + ' 次');
		$('#early-count').html(info.early_times > 0 ? '<font color="#f5bf64">' + info.early_times + ' 次</font>' : info.early_times + ' 次');
		var total_count = $('.day-cycle').length * 2;
		var empty_count = total_count - info.signin_times - info.signout_times;
		empty_count = info.empty_times > empty_count ? info.empty_times : empty_count;
		$('#empty-count').html(empty_count > 0 ? '<font color="#f5bf64">' + empty_count + ' 次</font>' : empty_count + ' 次');
		
		$('.info').removeClass('hide');
		$('.main-panel .detail').addClass('hide');
		rec_loading = false;
		
	} else {
		frame_obj.tips(data.errmsg);
	}
}

function init_panel_change() {
	var user_img = $('#user_pic_url').val();
	var user_name = $('#user_name').val();
	
	$('.datetimepicker').attr('style', 'left: 0px; z-index: 1060; top: 0px; display: block !important;');
	$('.work-days font').unbind('click').bind('click', function() {
		$('#bottom-menu').addClass('hide');
		if (rec_loading)	return;		//未加载完数据，不能点击
		
		$('.datetimepicker').attr('style', 'left: 0px; z-index: 1060; top: 0px; display: none !important;');
		var mon = curr_mon;
		if (mon < 10)	mon = '0' + mon;
		$('.sub-panel .header span').html('详细记录 (' + curr_year + '-' + mon + ')');
		$('.main-panel').addClass('hide');
		$('.sub-panel').removeClass('hide');
		
		var detail = curr_rec_info.detail;
		var html = '';
		if (detail.length == 0) {
			html = '<div class="has-no-record">' +
				'<img src="apps/common/static/images/logo_empty.png">' +
				'<span>暂时没有内容</span>' +
			'</div>';
		
		} else {
			for (var i in detail) {
				var group = detail[i];
				
				var item_html = '';
				for (var j in group) {	//分段
					var item = group[j];
					var in_time = '<font color="#f5bf64">未考勤</font>';
					if (item['signin'] && item['signin']['state'] != 3) {
						in_time = time2date(item['signin']['update_time'], 'HH:mm:ss');
						if (item['signin']['state'] != 0) {
							in_time = '<font color="#f5bf64">' + in_time + '</font>';
						}
					}
					var out_time = '<font color="#f5bf64">未考勤</font>';
					if (item['signout'] && item['signout']['state'] != 3) {
						out_time = time2date(item['signout']['update_time'], 'HH:mm:ss');
						if (item['signout']['state'] != 0) {
							out_time = '<font color="#f5bf64">' + out_time + '</font>';
						}
					}
    				var rec = item['signin'] || item['signout'];
					var limit = rec['sign_limit'].split('~');
    				if (limit.length == 2) {	//标准
    					limit = limit[0]+'<br>~<br>'+limit[1];
    				} else {					//弹性
    					limit = limit[0].split('小时');
    					limit = limit[0]+'小时<br>'+limit[1];
    				}
    				
    				item_html += '<p class="sign-line">' + 
						'<span class="limit">' + limit + '</span>' +
						'<span class="sign-detail">' +
							'<span class="signin">签到： ' + in_time + '</span>' +
							'<span class="signout">签退： ' + out_time + '</span><br>' +
						'</span>' +
					'</p>';
				}
				
				
				html += '<div class="item">' +
							'<h4>' + mon + '月' + i + '日 <span>考勤记录</span></h4>' +
							item_html +
						'</div>';
			}
		}
		
		$('.sub-panel .detail').html(html);
	});
	
	$('.sub-panel .header font').unbind('click').bind('click', function() {
		$('.datetimepicker').attr('style', 'left: 0px; z-index: 1060; top: 0px; display: block !important;');
		$('.main-panel').removeClass('hide');
		$('.sub-panel').addClass('hide');
		$('#bottom-menu').removeClass('hide');
	});
}

function init_sign_days() {
	var full_days = curr_rec_info.full_days;
	var exp_days = curr_rec_info.exp_days;
	full_days = eval('['+full_days+']');
	exp_days = eval('['+exp_days+']');
	
	$('.day').each(function() {
		var curr_day = Number($(this).html());
		if ($.inArray(curr_day, full_days) != -1) {				//正常
			var cycle = '<div class="day-cycle day-cycle-full"></div>';
			$(this).append(cycle);
			
		} else if ($.inArray(curr_day, exp_days) != -1) {		//异常
			var cycle = '<div class="day-cycle day-cycle-exp"></div>';
			$(this).append(cycle);
		}
	});
	$('.day.old').children('.day-cycle').remove();
	$('.day.new').children('.day-cycle').remove();
}

//change month

function init_change_mon() {
	$('.prev').unbind('click').bind('click', function() {
		if (curr_mon == 1) {
			curr_mon = 12;
			curr_year --;
		} else {
			curr_mon --;
		}
		load_mon_rec(curr_year, curr_mon);
	});
	$('.next').unbind('click').bind('click', function() {
		if (curr_mon == 12) {
			curr_mon = 1;
			curr_year ++;
		} else {
			curr_mon ++;
		}
		load_mon_rec(curr_year, curr_mon);
	});
}

// change date
function init_change_date() {
    $("#date-selector").datetimepicker().on('changeDate', function(ev){
    	var val = $(this).val();
    	var new_year = val.substring(0, 4);
    	var new_mon = val.substring(4, 6);
    	var sel_day = val.substring(6, 8);
    	if (new_year != curr_year || new_mon != curr_mon) {
    		curr_year = new_year;
    		curr_mon = new_mon;
    		load_mon_rec(curr_year, curr_mon);

    	} else {
    		setTimeout(init_sign_days, 10);
    		$('.info').addClass('hide');
    		$('.detail').removeClass('hide');
    		
    		var detail = curr_rec_info.detail;
    		detail = detail[parseInt(sel_day)];
    		
    		var in_time = '-';
    		var out_time = '-';
    		var work_len = '0小时0分钟';
    		var words = '-';
    		var mood = '-';
    		
    		$('.detail .sign-times').remove();
    		
    		if (detail) {	//has record
    			var html = '';
    			for (var i in detail) {
    				var item = detail[i];
    				var in_time = '<font color="#f5bf64">未考勤</font>';
    				if (item['signin'] && item['signin']['state'] != 3) {
    					in_time = time2date(item['signin']['update_time'], 'HH:mm:ss');
    					if (item['signin']['state'] != 0) {
    						in_time = '<font color="#f5bf64">' + in_time + '</font>';
    					}
    				}
    				var out_time = '<font color="#f5bf64">未考勤</font>';
    				if (item['signout'] && item['signout']['state'] != 3) {
    					out_time = time2date(item['signout']['update_time'], 'HH:mm:ss');
    					if (item['signout']['state'] != 0) {
    						out_time = '<font color="#f5bf64">' + out_time + '</font>';
    					}
    				}
    				
    				var in_mood = item['signin'] ? item['signin']['mood'] || '' : '';
    				var out_mood = item['signout'] ? item['signout']['mood'] || '' : '';
    				var rec = item['signin'] || item['signout'];
    				var limit = rec['sign_limit'].split('~');
    				if (limit.length == 2) {	//标准
    					limit = limit[0]+'<br>~<br>'+limit[1];
    				} else {					//弹性
    					limit = limit[0].split('小时');
    					limit = limit[0]+'小时<br>'+limit[1];
    				}
    				html += '<p class="sign-times">' + 
	    					'<span class="limit">' + limit + '</span>' +
	    					'<span class="sign-detail">' +
	    						'<span class="signin">签到： ' + in_time + '</span>' +
								'<span class="signout">签退： ' + out_time + '</span><br>' +
								'<span>说说： ' + in_mood + '</span><br>' +
								'<span>心情： ' + out_mood + '</span>' +
							'</span>' +
						'</p>';
    			}
    			$('.detail').append(html);
    		}
    	}
    	
    });
}