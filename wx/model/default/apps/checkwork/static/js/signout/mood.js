var mood_val = 50;

$(document).ready(function() {
	init_excanvas();
	init_send_mood();
});

function init_send_mood() {
	$('#send-mood').unbind('click').bind('click', function() {
		var data = new Object();
		data.signout = $('#signout').val();
		data.mood = mood_val;
		frame_obj.do_ajax_post($(this), 'save', JSON.stringify(data), send_mood_complete);
	});
}

function send_mood_complete(data) {
	if (data.errcode == 0) {
		frame_obj.alert('发表成功', '我知道了', function() {
			location.href = data.info.target;
		});
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function init_excanvas() {
	$(".knob").knob({
        change : function (value) {
        	mood_val = value;
        	
        	var base_0 = 255;
        	var base_1 = 2;
        	var base_2 = 2;
        	base_1 += (value * 4);
        	var diff = (base_1 > 255) ? base_1 - 255 : 0;
        	base_0 -= diff;
        	base_1 = (base_1 > 255) ? 255 : base_1;
        	var color = 'rgb(' + base_0 + ',' + base_1 + ',' + base_2 + ')';

        	$('.knob').css('color', color);
        	this.fgColor = color;
        	this.o.fgColor = color;
        	
        	if (value < 20) {
        		update_mood_desc('目前的心情不太好喔，继续增加你的心情指数吧~');
        	} else if (value < 40) {
        		update_mood_desc('没有什么可以阻挡你拥有好心情，加油！');
        	} else if (value < 60) {
        		update_mood_desc('不要掩饰你的好心情啦~');
        	} else if (value < 80) {
        		update_mood_desc('今天状态还OK喔，再给自己鼓下劲吧！');
        	} else if (value < 100) {
        		update_mood_desc('就是这样，保持好心情稳步向前吧~');
        	} else {
        		update_mood_desc('哇，心情好到爆表啦，继续保持吧！');
        	}
        },
        release : function (value) {
        },
        cancel : function () {
        },
        draw : function () {
        }
    });
}

function update_mood_desc(text) {
	$('.mood-body p:first-child').html(text);
}