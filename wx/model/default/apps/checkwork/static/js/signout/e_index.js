var cwork_touched = false;
var timeout_handler;

var cwork_local_data = new Object();
var get_local_complete = false;

var wait_signin = false;		//是否是点击后，等待获取位置，默认为false
var curr_sign_el;				//当前点击的按钮
var is_signing = false;
var js_sdk_ready = false;

jsApiList = ['getLocation'];
$(document).ready(function() {
	check_use_def();
	init_clock();
	init_time_select();
	init_sign_group();
	init_sign_word();
	
	document.ondragstart=function(){
		return false;
	}; document.oncontextmenu=function(){
		return false;
	};
});

//验证是否使用默认规则
function check_use_def() {
	if ($('#time-list > span').length == 0) {
		$('#time-list').html($('#def-conf .time-list').html());
		$('.sign-group-list').html($('#def-conf .group-list').html());
	}
	$('#def-conf').html('');
}

//签退事件
function do_sign(that) {
	var target = $(that).parents('.sign-group');

	if (is_signing) {
		frame_obj.tips('正在签退中，请稍候');		return;
	}
	
	if (!get_local_complete) {
		is_signing = true;
		change_state(1, target);
		wait_signin = true;
		curr_sign_el = $(that);
		get_local();
		return;
	} else {
		do_signin($(that));
	}
}

//初始化时间段选择
function init_time_select() {
	//适应宽度
	var win_width = $(window).width();
	var padding_width = parseInt(win_width * 0.06);
	var width = $('#time-list > span').length * 120;
	if (padding_width + width > win_width) {
		$('#time-list').css({width: padding_width + width});
	}
	//时间段选择
	var idx = 0;
	$('#time-list > span').each(function() {
		$(this).attr('data-idx', idx++);
	});
	$('#time-list > span').each(function() {
		$(this).unbind('click').bind('click', function() {
			if (wait_signin) {
				frame_obj.tips('正在签退中，不能切换时间段', {time: 2000});
				return;
			}
			
			var idx = $(this).attr('data-idx');
			var left = idx * win_width;
			$('#time-list > span').removeClass('active');
			$(this).addClass('active');
			$('.sign-group-list').animate({left: -left});
		});
	});
	//选中第一个
	$('#time-list > span:first-child').click();
}

//初始化签退页宽度
function init_sign_group() {
	var win_width = $(window).width();
	$('.sign-group').css({width: win_width});
	//签退过,显示签退结果
	$('.sign-group').each(function() {
		if ($.trim($(this).find('.sign-after .time').html()) != '') {	
			$(this).find('.sign-tips').html('&nbsp;');
			change_state(2, $(this));
		}
	});
}

//初始化说说
function init_sign_word() {
	$('.sign-group .btn-sm').each(function() {
		$(this).unbind('click').bind('click', function() {
			var target = $(this).parents('.sign-group');
			
			var total_h = $(target)[0].scrollHeight + 88;
			$(target).find('.sign-word').removeClass('hide');
			$('.container').animate({top: -total_h});
			$('#bottom-menu').addClass('hide');
			$('html,body').css({overflow: 'hidden'});
		});
	});
	
	$('.sign-group .sign-word .cancel').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.container').animate({top: 0});
			$('#bottom-menu').removeClass('hide');
			$('html,body').css({overflow: 'auto'});
			var target = $(this).parents('.sign-group');
			$(target).find('.sign-word').addClass('hide');
		});
	});
}

//时钟
function init_clock() {
	var date = new Date();
	var h = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();
	h = h < 10 ? '0'+h : h; 
	m = m < 10 ? '0'+m : m; 
	s = s < 10 ? '0'+s : s; 
	$('.clock').html(h + ':' + m + ':' + s);
	setTimeout(init_clock, 1000);
}

//更新界面：0签退前，1签退中，2签退后, 3签退失败
function change_state(state, container) {
	switch (state) {
		case 0:	//前
			$(container).find('.sign-before').removeClass('hide');		
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
		case 1:	//中
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').removeClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').addClass('hide');
			$(container).find('.cycle').removeClass('hide');
			break;
		case 2:	//后(成功)
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').removeClass('hide');
			$(container).find('.sign-fail').addClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
		case 3: //失败
			$(container).find('.sign-before').addClass('hide');			
			$(container).find('.sign-ing').addClass('hide');		
			$(container).find('.sign-after').addClass('hide');
			$(container).find('.sign-fail').removeClass('hide');
			$(container).find('.location-img').removeClass('hide');
			$(container).find('.cycle').addClass('hide');
			break;
	}
}

wx.ready(function(){
	js_sdk_ready = true;
});

function get_local() {
/*test-----------------------
	cwork_local_data = {lng: 113.364269, lat: 23.226203};			
	if (wait_signin) {
		wait_signin = false;
		do_signin($(curr_sign_el));
		curr_sign_el = undefined;
	}
	return;
//.test-----------------------*/
	
	if (!js_sdk_ready) {
		setTimeout(function() {
			get_local();
		},500);
		return;
	}
	wx.getLocation({
		success: function (res) {
			cwork_local_data.lng = res.longitude;
			cwork_local_data.lat = res.latitude;
						
			if (wait_signin) {
				wait_signin = false;
				do_signin($(curr_sign_el));
				curr_sign_el = undefined;
			}
		},
		cancel:function() {
			frame_obj.alert('要打开地理位置才能进行考勤哦', '我知道了', get_local);
		},
		complete:function() {	//请求完成后：不管成功失败，才能签退
			get_local_complete = true;
			$('#alert-dlg').modal('hide');	//自动关闭提示
		},
	});
}

//执行签退
function do_signin(btn) {
	is_signing = true;
	
	var target = $(btn).parents('.sign-group');
	change_state(1, target);
	var rule_time = $('#rule-time').val();	//规则更新时间，用于区分是否有规则变更
	var data = new Object();
	data.rule = $('#rule').val();
	data.rule_time = $('#rule-time').val();
	data.sign_day = $('#sign_day').val();
	data.group = $(target).attr('data-group');
//	cwork_local_data = {lng: 113.052063, lat: 23.118891};
//	cwork_local_data = {lng: 113.364269, lat: 23.226203};
	data.location = cwork_local_data || '';
	frame_obj.do_ajax_post(btn, 'signout', data, function(resp) {
		signin_complete(resp, target);
		is_signing = false;
		get_local_complete = false;
	});
}

//完成签退
function signin_complete(data, sign_group) {
	var container_time = $(sign_group).find('.sign-after .time');
	var tips = $(sign_group).find('.sign-tips');
	$(tips).html('&nbsp;');
	
	if (data.errcode == 0) {
		var info = data.info;
		var desc = info.sign_time + '(' + info.time_desc + ')';
		if (info.is_early) {
			$(container_time).html('<font color="#f5bf64">' + desc + '</font>');
		} else if (info.state == 4) {
			$(container_time).html('<font color="#f5bf64">' + desc + '</font>');
			$(tips).html('你今天还未签到哦');
		} else {
			$(container_time).html(desc);
		}
		if (info.is_early) {
			if (!info.signed) {
				$(tips).html('你今天早退了' + Math.abs(Math.ceil(info.diff / 60)) + '分钟，小心领导知道哦');
			}
		} else {
			if (!info.signed && info.diff > 0) {
				$(tips).html('你今天继续奋斗了' + Math.abs(Math.ceil(info.diff / 60)) + '分钟，不错哦');
			}
		}
		$(sign_group).attr('data-rec', info.rec);
		change_state(2, sign_group);
		
	} else if (data.errmsg == 0) {
		change_state(2, sign_group);
		
	} else if (data.info != undefined){
		$(sign_group).find('.sign-fail .fail-detail').html(data.info.info);
		change_state(3, sign_group);
		
	} else {
		$(sign_group).find('.sign-fail .fail-detail').html(data.errmsg);
		change_state(3, sign_group);
	}
	cwork_touched = false;
}

//保存说说
function sign_mood(that) {
	var target = $(that).parents('.sign-group');
	var rec = $(target).attr('data-rec');
	if (rec == '') {
		frame_obj.tips('请签退后再记录心情', {time: 2000});
		return;
	}
	
	var data = new Object();
	data.signout = rec;
	data.words = $(target).find('.sign-word .knob').attr('value');
	frame_obj.do_ajax_post($(that), 'mood', data, function(resp) {
		if (resp.errcode == 0) {
			frame_obj.tips('发布成功', {time: 1000});
			setTimeout(function() {
				$(target).find('.sign-word .cancel').click();
			}, 500);
		}
	});
}

//图形
$(document).ready(function() {
	init_excanvas();
	$('.sign-group .sign-word .knob').each(function() {
		var target = $(this).parents('.sign-group');
		var value = $(this).attr('value');
		if (value < 20) {
    		update_mood_desc(target, '目前的心情不太好喔，继续增加你的心情指数吧~');
    	} else if (value < 40) {
    		update_mood_desc(target, '没有什么可以阻挡你拥有好心情，加油！');
    	} else if (value < 60) {
    		update_mood_desc(target, '不要掩饰你的好心情啦~');
    	} else if (value < 80) {
    		update_mood_desc(target, '今天状态还OK喔，再给自己鼓下劲吧！');
    	} else if (value < 100) {
    		update_mood_desc(target, '就是这样，保持好心情稳步向前吧~');
    	} else {
    		update_mood_desc(target, '哇，心情好到爆表啦，继续保持吧！');
    	}
	});
});

function init_excanvas() {
	$(".knob").knob({
        change : function (value) {
        	var base_0 = 255;
        	var base_1 = 2;
        	var base_2 = 2;
        	base_1 += (value * 4);
        	var diff = (base_1 > 255) ? base_1 - 255 : 0;
        	base_0 -= diff;
        	base_1 = (base_1 > 255) ? 255 : base_1;
        	var color = 'rgb(' + base_0 + ',' + base_1 + ',' + base_2 + ')';

        	$('.knob').css('color', color);
        	this.fgColor = color;
        	this.o.fgColor = color;
        	var target = $(this.$div).parents('.sign-group');
        	
        	if (value < 20) {
        		update_mood_desc(target, '目前的心情不太好喔，继续增加你的心情指数吧~');
        	} else if (value < 40) {
        		update_mood_desc(target, '没有什么可以阻挡你拥有好心情，加油！');
        	} else if (value < 60) {
        		update_mood_desc(target, '不要掩饰你的好心情啦~');
        	} else if (value < 80) {
        		update_mood_desc(target, '今天状态还OK喔，再给自己鼓下劲吧！');
        	} else if (value < 100) {
        		update_mood_desc(target, '就是这样，保持好心情稳步向前吧~');
        	} else {
        		update_mood_desc(target, '哇，心情好到爆表啦，继续保持吧！');
        	}
        },
        release : function (value) {
        },
        cancel : function () {
        },
        draw : function () {
        }
    });
}

function update_mood_desc(target, text) {
	$(target).find('.sign-word p:first-child').html(text);
}