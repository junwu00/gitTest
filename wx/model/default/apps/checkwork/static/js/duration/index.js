var curr_year;
var curr_mon;
var year_len = 2;	//最多显示两年内记录

$(document).ready(function() {	
	var date = new Date();
	curr_year = date.getFullYear();
	curr_mon = date.getMonth() + 1;
	
	init_month_list();
	init_month_change();
	init_today();
});

function init_today() {
	update_month_select();
	load_mon_rec(undefined);
}

function update_month_select() {
	$('.labels').html('考勤统计');
	var idx = 0;
	$('.mon-list ul li').each(function() {
		var year = $(this).attr('data-year');
		var mon = $(this).attr('data-mon');
		if (year == curr_year && mon == curr_mon) {
			$(this).addClass('active');
			var pre_width = $(this)[0].clientWidth;
			var target = idx > 1 ? idx-1 : 0;
			$('.mon-list').animate({scrollLeft: pre_width * target});
		}
		idx++;
	});
}

function init_month_list() {	
	var html = '';
	var idx = year_len * 12 - 1;
	for (var i=curr_year; i>(curr_year - year_len); i--) {
		for (var j=12; j>=1; j--, idx--) {
			if (j < 10) {
				html = '<li data-year="' + i + '" data-mon="' + j + '" data-idx="' + idx + '">' + i + '年0' + j + '月' + '</li>' + html;
			} else {
				html = '<li data-year="' + i + '" data-mon="' + j + '" data-idx="' + idx + '">' + i + '年' + j + '月' + '</li>' + html;
			}
		}
	}
	$('.mon-list ul').html(html);
}

function init_month_change() {
	var win_width = $(window).width();
	var pre_width = parseInt(win_width / 3);
	$('.mon-list ul li').css({width: pre_width});
	$('.mon-list ul li').each(function() {
		$(this).unbind('click').bind('click', function() {
			$('.mon-list ul li').removeClass('active');
			$(this).addClass('active');
			
			var pre_width = $(this)[0].clientWidth;
			$('.mon-list').animate({scrollLeft: pre_width * ($(this).attr('data-idx') - 1)});
			
			curr_year = $(this).attr('data-year');
			curr_mon = $(this).attr('data-mon');
			$('.labels').html('考勤统计');
			load_mon_rec($(this));
		});
	});
}

function load_mon_rec(btn) {
	var data = new Object();
	data.year = curr_year;
	data.mon = curr_mon;
	
	frame_obj.lock_screen('加载中', true);
	frame_obj.do_ajax_post(btn, 'report', JSON.stringify(data), load_report_complete);
}

function load_report_complete(data) {
	frame_obj.unlock_screen();
	if (data.errcode == 0) {
		var info = data.info;
		$('#days').html(info.days);
		$('#total_time').html(info.total_time);
		$('#pre_time').html(info.pre_time);
		$('#late_times').html(info.late_times > 0 ? '<font color="#f5bf64">' + info.late_times + ' 次</font>' : info.late_times + ' 次');
		$('#early_times').html(info.early_times > 0 ? '<font color="#f5bf64">' + info.early_times + ' 次</font>' : info.early_times + ' 次');
		$('#empty_times').html(info.empty_times > 0 ? '<font color="#f5bf64">' + info.empty_times + ' 次</font>' : info.empty_times + ' 次');
		$('#signin_times').html(info.signin_times + ' 次');
		$('#signout_times').html(info.signout_times + ' 次');
		
	} else {
		frame_obj.alert(data.errmsg);
	}
}

