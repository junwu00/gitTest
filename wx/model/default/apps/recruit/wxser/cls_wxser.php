<?php
/**
 * 人力招聘消息处理类
 *
 * @author LiangJianMing
 * @create 2015-04-01
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『人力招聘』操作指引',
		'desc' => '欢迎使用人力招聘应用，在这里你也是HR，你不但可以内推，还可以立马获得公司的红包奖励哦，赶快来领取吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971786&idx=2&sn=3fb93bc82ea6812a9744e1f43b89a18d&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	/**
	 * 企业云盘普通消息回复入口
	 *
	 * @access protected
	 * @return void
	 */
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
			return;
		}

		log_write('开始响应文本消息');
		if (strtolower($this -> post_data['Content']) != 'hr') {
			log_write('关键字无效，不响应');
			return;
		}

		try {
			g('recruit') -> reinit($this -> com_id, $this -> post_data['FromUserName']);
			g('recruit') -> send_hr_tip();
		}catch(SCException $e) {
			log_write('响应文本消息失败，异常：[' . $e -> getMessage() . ']');
			return;
		}
		log_write('响应文本消息完成');
	}
}

// end of file