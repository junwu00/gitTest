<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'recruit_share_self.css' => array(
		SYSTEM_APPS . 'recruit/static/css/share/self.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/square/green.css',
		SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.css'
	),
    'recruit_share_self.js' => array(
        SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.js',
        SYSTEM_ROOT . 'static/js/icheck/icheck.min.js',
        SYSTEM_APPS . 'recruit/static/js/share/self.js'
    )
);

return $arr;

/* End of this file */