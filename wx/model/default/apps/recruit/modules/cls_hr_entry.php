<?php
/**
 * 人力招聘，hr入口控制器
 *
 * @author LiangJianMing
 * @create 2015-04-01
 */
class cls_hr_entry extends abs_app_base {
	public function __construct() {
		parent::__construct('recruit');
	}

	/**
	 * 查看简历列表
	 *
	 * @access public
	 * @return void
	 */
	public function lister() {
		try {
			g('recruit') -> check_hr_valid();
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		global $app, $module, $action;
		//获取简历列表接口
		$resume_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=113';
		//简历详情页面url，仍缺少id参数
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=hr_entry&a=detail';
		//更新简历状态为已读的接口，仍缺少id参数
		$ajax_read_state = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=111&state=2';
		//简历删除接口，支持批量
		$del_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=112';
		//简历已读/收藏接口
		$state_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=111';

		g('smarty') -> assign('resume_list_url', $resume_list_url);
		g('smarty') -> assign('detail_url', $detail_url);
		g('smarty') -> assign('ajax_read_state', $ajax_read_state);
		g('smarty') -> assign('del_url', $del_url);
		g('smarty') -> assign('state_url', $state_url);

		g('smarty') -> assign('title', '简历管理');
		g('smarty') -> show($this -> temp_path.'hr/index.html');
	}

	/**
	 * 查看简历详情
	 *
	 * @access public
	 * @return void
	 */
	public function detail() {
		try {
			g('recruit') -> check_hr_valid();
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		$id = (int)get_var_value('id');
		if (empty($id)) {
			cls_resp::show_err_page('无效的简历对象!');
		}

		$condition = array(
			'ri.id=' => $id,
		);

		try {
			$info = g('recruit') -> get_resume_detail($condition);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		global $app, $module, $action;
		
		//返回简历列表URL
		$lister_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=hr_entry&a=lister';
		//更改简历状态的接口，缺少state值，详见数据表表注释
		$change_state_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=hr_entry&cmd=111&id=' . $id;
		//简历删除接口
		$del_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=112&id=' . $id;
		//简历已读/收藏接口
		$state_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=111&id=' . $id;
		
		g('smarty') -> assign('change_state_url', $change_state_url);
		g('smarty') -> assign('del_url', $del_url);
		g('smarty') -> assign('lister_url', $lister_url);
		g('smarty') -> assign('state_url', $state_url);

		//简历详细信息
		g('smarty') -> assign('info', $info);

		log_write(json_encode($info));
		
		g('smarty') -> assign('title', '简历管理');
		g('smarty') -> show($this -> temp_path.'hr/resume.html');
	}
}