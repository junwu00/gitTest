<?php
/**
 * 人力招聘，推荐相关的控制器
 *
 * @author LiangJianMing
 * @create 2015-04-01
 */
class cls_recommend extends abs_app_base {
	public function __construct() {
		parent::__construct('recruit');
	}

	/**
	 * 推荐有赏活动简介页面
	 *
	 * @access public
	 * @return void
	 */
	public function preview() {
		global $app, $module, $action;
		//推荐有赏主页url
		$index_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=recommend';

		g('smarty') -> assign('index_url', $index_url);

		g('smarty') -> assign('title', '推荐有赏');
		g('smarty') -> show($this -> temp_path.'plan/index.html');
	}

	/**
	 * 推荐有赏主页
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cipher = get_var_value('cipher');
		$corpurl = !empty($_SESSION[SESSION_VISIT_CORP_URL]) ? $_SESSION[SESSION_VISIT_CORP_URL] : get_var_value('corpurl');
		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		//企业内部用户生成新的分享参数
		if ($share_info['user_type'] == 3) {
			g('recruit') -> check_user_init();
			$data = array();
			$cipher = g('recruit') -> compose_cus_data($data);
		}

		try {
			//企业信息
			$info = g('recruit') -> get_company_info($share_info['com_id']);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		global $app, $module, $action;
		//获取职位列表接口
		$ajax_job_list = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102&free=1&cipher=' . $cipher;
		//获取企业介绍信息接口
		$ajax_com_list = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=101&free=1&cipher=' . $cipher;
		//职位详情页面url，欠缺id参数
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=re_job&a=detail&free=1&cipher=' . $cipher;

		//begin---------------分享相关的数据---------------------------
		parent::oauth_jssdk();

		$to_share_url = SYSTEM_HTTP_DOMAIN . 'index.php?app=' . $app . '&m=' . $module . '&a=' . $action . '&free=1&cipher=' . $cipher . '&corpurl=' . $corpurl;

		!empty($info['logo']) and $share_icon_url = MEDIA_URL_PREFFIX . $info['logo'];
		empty($share_icon_url) and $share_icon_url = SYSTEM_HTTP_APPS_ICON.'Recommend.png';
		
		
		$share_desc = '';
		$share_title = $info['name'] . '求贤若渴，快来看看吧~！';
		
		g('smarty') -> assign('to_share_url', $to_share_url);
		g('smarty') -> assign('share_icon_url', $share_icon_url);
		g('smarty') -> assign('share_desc', $share_desc);
		g('smarty') -> assign('share_title', $share_title);
		//end-----------------分享相关的数据-------------------------------------

		g('smarty') -> assign('ajax_job_list', $ajax_job_list);
		g('smarty') -> assign('ajax_com_list', $ajax_com_list);
		g('smarty') -> assign('detail_url', $detail_url);
		

		//用户类型：1-游客，2-其他企业号用户，3-当前企业号内部员工
		g('smarty') -> assign('user_type', $share_info['user_type']);

		g('smarty') -> assign('title', '推荐有赏');
		g('smarty') -> show($this -> temp_path.'index/index.html');
	}

	/**
	 * 我的推荐
	 *
	 * @access public
	 * @return void
	 */
	public function mine() {
		try {
			//获取整体统计数据
			$user = g('user')->get_user_by_id($_SESSION[SESSION_VISIT_DEPT_ID],$_SESSION[SESSION_VISIT_USER_ID],'pic_url');
			$analyze_count = g('recruit') -> get_analyze_count();
		}catch(SCException $e) {
			$analyze_count = array();
		}

		global $app, $module, $action;
		//区分职位，获取对应的统计数据接口
		$ajax_job_count = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=104';
		//推荐有赏主页url
		$index_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=recommend';

		g('smarty') -> assign('ajax_job_count', $ajax_job_count);
		g('smarty') -> assign('index_url', $index_url);

		g('smarty') -> assign('user_pic', $user['pic_url']);
		g('smarty') -> assign('analyze_count', $analyze_count);
		g('smarty') -> assign('title', '我的推荐');
		g('smarty') -> show($this -> temp_path.'recommend/index.html');
	}
}