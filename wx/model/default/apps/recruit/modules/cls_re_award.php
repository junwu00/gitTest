<?php
/**
 * 人力招聘，奖励金钱相关的控制器
 *
 * @author LiangJianMing
 * @create 2015-04-01
 */
class cls_re_award extends abs_app_base {
	public function __construct() {
		parent::__construct('recruit');
	}

	/**
	 * 展示我的红包列表
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		global $app, $module, $action;
		
		//打开红包接口
		$open_redbag_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=117';
		//获取提现列表接口
		$extract_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=105';
		//获取红包列表接口
		$award_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=106';
		//获取用户当前余额接口
		$get_balance_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=116';
		//申请提现接口
		$apply_extract = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=110';

		g('smarty') -> assign('extract_list_url', $extract_list_url);
		g('smarty') -> assign('open_redbag_url', $open_redbag_url);
		g('smarty') -> assign('award_list_url', $award_list_url);
		g('smarty') -> assign('get_balance_url', $get_balance_url);
		g('smarty') -> assign('apply_extract', $apply_extract);

		g('smarty') -> assign('title', '我的红包');
		g('smarty') -> show($this -> temp_path.'redbag/index.html');
	}

	/**
	 * 提现列表
	 *
	 * @access public
	 * @return void
	 */
	public function extract() {
		global $app, $module, $action;
		//获取提现列表接口
		$extract_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=105';
		//获取用户当前余额接口
		$get_balance_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=116';
		//申请提现接口
		$apply_extract = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=110';

		g('smarty') -> assign('extract_list_url', $extract_list_url);
		g('smarty') -> assign('get_balance_url', $get_balance_url);
		g('smarty') -> assign('apply_extract', $apply_extract);

		g('smarty') -> assign('title', '我的提现');
		g('smarty') -> show($this -> temp_path.'plan/index.html');
	}
}