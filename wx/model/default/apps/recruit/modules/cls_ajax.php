<?php
/**
 * 全部人力招聘ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2015-03-14
 */
class cls_ajax extends abs_app_base {
	//命令对应函数名的映射表
	private $cmd_map = array(
		//获取企业宣传信息
		101 => 'get_company_info',
		//获取职位列表
		102 => 'get_job_list',
		//获取用作分析的统计数据
		103 => 'get_analyze_count',
		//以职位作为分组条件，获取统计数据
		104 => 'get_job_counts',
		//获取提现列表
		105 => 'get_extract_list',
		//获取红包列表
		106 => 'get_award_list',
		//转发次数+1
		107 => 'add_share_count',
		//点击次数+1
		108 => 'add_click_count',
		//投写简历
		109 => 'submit_resume',
		//申请提现
		110 => 'apply_extract',
		//改变简历状态：已读、收藏等
		111 => 'change_resume_state',
		//删除简历
		112 => 'delete_resume',
		//获取简历列表
		113 => 'get_resume_list',
		//获取简历详情
		114 => 'get_resume_detail',
		//获取职位相关配置
		115 => 'get_job_demand',
		//获取用户当前余额
		116 => 'get_user_balance',
		//改变红包状态：已读
		117 => 'change_award_state',
		//获取提现申请最低金额，单位：元
		118 => 'get_extract_limit',
	);

	public function __construct() {
		parent::__construct('recruit');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		empty($cmd) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$map = $this -> cmd_map;
		!isset($map[$cmd]) and cls_resp::echo_resp(cls_resp::$ForbidReq);

		$this -> $map[$cmd]();
	}

	/**
	 * 获取企业宣传信息
	 *
	 * @access public
	 * @return void
	 */
	private function get_company_info() {
		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		try {
			$info = g('recruit') -> get_company_info($share_info['com_id']);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$info = array(
			'rec' => $info,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取职位列表
	 *
	 * @access public
	 * @return void
	 */
	private function get_job_list() {
		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		try {
			$data = $this -> get_post_data();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$page = isset($data['page']) ? (int)$data['page'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$condition = array(
			'com_id=' => $share_info['com_id'],
		);

		$list = g('recruit') -> get_job_list($condition, $page, $page_size);
		!is_array($list) and $list = array();

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取整体转发相关的分析数据
	 *
	 * @access private
	 * @return void
	 */
	private function get_analyze_count() {
		try {
			$info = g('recruit') -> get_analyze_count();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$info = array(
			'rec' => $info,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 区分职位，获取对应的统计数据
	 *
	 * @access private
	 * @return void
	 */
	private function get_job_counts() {
		try {
			$data = $this -> get_post_data();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$page = isset($data['page']) ? (int)$data['page'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$list = g('recruit') -> get_job_counts($page, $page_size);
		!is_array($list) and $list = array();

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取自身的提现列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_extract_list() {
		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		if ($share_info['user_type'] != 3) {
			cls_resp::echo_err(cls_resp::$Busy, '无权进行该操作！');
		}
		try {
			$data = $this -> get_post_data();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$page = isset($data['page']) ? (int)$data['page'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$condition = array();

		$list = g('recruit') -> get_extract_list($condition, $page, $page_size);

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取自身的红包列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_award_list() {
		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		if ($share_info['user_type'] != 3) {
			cls_resp::echo_err(cls_resp::$Busy, '无权进行该操作！');
		}
		try {
			$data = $this -> get_post_data();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$page = isset($data['page']) ? (int)$data['page'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$condition = array();

		$list = g('recruit') -> get_award_list($condition, $page, $page_size);

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 增加转发统计
	 *
	 * @access private
	 * @return void
	 */
	private function add_share_count() {
		$rji_id = (int)get_var_value('id');
		if (empty($rji_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的职位对象！');
		}

		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$result = g('recruit') -> increase_job_count($rji_id, 1, $share_info['com_id']);
		$result and $share_info['user_type'] == 3 and $result = g('recruit') -> increase_user_counts($rji_id, 1, $share_info['user_id'], $share_info['com_id']);
		!$result and cls_resp::echo_err(cls_resp::$Busy, '系统正忙，请稍后再试');
		cls_resp::echo_ok();
	}
	
	/**
	 * 增加点击统计
	 *
	 * @access private
	 * @return void
	 */
	private function add_click_count() {
		$rji_id = (int)get_var_value('id');
		if (empty($rji_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的职位对象！');
		}

		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		//只有非内部员工才算点击数
		if ($share_info['user_type'] != 3) {
			$result = g('recruit') -> increase_job_count($rji_id, 2, $share_info['com_id']);
			$result and $result = g('recruit') -> increase_user_counts($rji_id, 2, $share_info['user_id'], $share_info['com_id']);
			!$result and cls_resp::echo_err(cls_resp::$Busy, '系统正忙，请稍后再试');
		}
		cls_resp::echo_ok();
	}
	
	/**
	 * 提交简历
	 *
	 * @access private
	 * @return void
	 */
	private function submit_resume() {
		$rji_id = (int)get_var_value('id');
		
		if (empty($rji_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的职位对象！');
		}

		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		if ($share_info['user_type'] == 3) {
			cls_resp::echo_err(cls_resp::$Busy, '无法进行此操作！');
		}
		if (empty($share_info['com_id']) or empty($share_info['user_id'])) {
			cls_resp::echo_err(cls_resp::$Busy, '无权进行该操作！');
		}

		try {
			g('recruit') -> check_resume_submit($rji_id);
			g('recruit') -> check_job_valid($rji_id);
			$data = $this -> get_post_data();
			log_write(json_encode($data));
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$name = isset($data['name']) ? trim($data['name']) : '';
		$mobile = isset($data['mobile']) ? trim($data['mobile']) : '';
		$portrait = isset($data['portrait']) ? trim($data['portrait']) : '';
		$gender = isset($data['gender']) ? (int)$data['gender'] : 0;
		$age = isset($data['age']) ? (int)$data['age'] : 0;
		$salary = isset($data['salary']) ? (int)$data['salary'] : 0;
		$dwell = isset($data['dwell']) ? trim($data['dwell']) : '';
		$experience = isset($data['experience']) ? trim($data['experience']) : '';
		$degree = isset($data['degree']) ? trim($data['degree']) : '';
		$job = isset($data['job']) ? trim($data['job']) : '';
		$email = isset($data['email']) ? trim($data['email']) : '';
		$other_info = isset($data['other_info']) ? $data['other_info'] : array();
		$accessory = isset($data['accessory']) ? $data['accessory'] : array();

		$tmp_arr = array();
		foreach ($other_info as $val) {
			$val['value'] != '' and $tmp_arr[$val['name']] = $val['value'];
		}
		unset($val);
		$other_info = $tmp_arr;
		
		$tmp_arr = array();
		foreach ($accessory as $val) {
			$val != '' and $tmp_arr[] = $val;
		}
		unset($val);
		$accessory = $tmp_arr;

		$args = array(
			'rji_id' => $rji_id,
			'com_id' => $share_info['com_id'],
			'user_id' => $share_info['user_id'],
			'other_info' => json_encode($other_info),
			'accessory' => json_encode($accessory),
		);
		$name != '' and $args['name'] = $name;
		$mobile != '' and $args['mobile'] = $mobile;
		$portrait != '' and $args['portrait'] = $portrait;
		$gender != 0 and $args['gender'] = $gender;
		$age != 0 and $args['age'] = $age;
		$salary != 0 and $args['salary'] = $salary;
		$dwell != '' and $args['dwell'] = $dwell;
		$experience != '' and $args['experience'] = $experience;
		$degree != '' and $args['degree'] = $degree;
		$job != '' and $args['job'] = $job;
		$email != '' and $args['email'] = $email;

		parent::log('开始提交简历');
		try {
			$result = g('recruit') -> add_resume($args);
			g('recruit') -> set_finish_cookie($rji_id);
		}catch(SCException $e) {
			parent::log('提交简历失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_exp($e);
		}
		parent::log('提交简历成功');

		$result = g('recruit') -> increase_job_count($rji_id, 3, $share_info['com_id']);
		$result and g('recruit') -> increase_user_counts($rji_id, 3, $share_info['user_id'], $share_info['com_id']);

		cls_resp::echo_ok();
	}

	/**
	 * 申请提现
	 *
	 * @access private
	 * @return void
	 */
	private function apply_extract() {
		$fields = array(
			'money',
		);
		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$money = intval((float)$data['money'] * 100);
		if (empty($money)) {
			cls_resp::echo_err(cls_resp::$Busy, '提现金额不能为0！');
		}

		$limit = g('recruit') -> get_extract_limit();
		if ($money < $limit * 100) {
			cls_resp::echo_err(cls_resp::$Busy, '提现额度不得低于' . number_format($limit) . '元');
		}

		parent::log('开始申请提现');
		try {
			$result = g('recruit') -> apply_extract($money);
		}catch(SCException $e) {
			parent::log('申请提现失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_busy(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('申请提现成功');
		cls_resp::echo_ok();
	}

	/**
	 * 更改简历状态
	 *
	 * @access private
	 * @return void
	 */
	private function change_resume_state() {
		$id = (int)get_var_value('id');
		$state = (int)get_var_value('state');
		$collected = (int)get_var_value('collected');

		if (empty($id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的简历对象！');
		}
		if (empty($state) and empty($collected)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的参数！');
		}

		if (!empty($state) and ($state < 2 or $state > 7)) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的状态值！');
		}

		!empty($collected) and ($collected < 1 or $collected > 2) and $collected = 2;

		try {
			g('recruit') -> check_hr_valid();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$args = array();
		!empty($state) and $args['state'] = $state;
		!empty($collected) and $args['is_collected'] = $collected;

		parent::log('开始更改简历状态');
		//被录用发放奖励
		if ($state == 5) {
			$check_con = array(
				'id=' => $id,
				'state=' => 5,
			);
			//简历已经是被录用状态，不能更改
			$check = g('ndb') -> record_exists(g('recruit') -> ri_table, $check_con);
			$check and cls_resp::echo_err(cls_resp::$Busy, '不能重复录用！');

			g('db') -> begin_trans();
		}

		try {
			g('recruit') -> update_resume($id, $args);
			if ($state == 5) {
				$param = array(
					'resume_id' => $id,
				);
				g('recruit') -> trigger_award(3, 0, $param);
			}
		}catch(SCException $e) {
			$state == 5 and g('db') -> rollback();
			parent::log('更改简历状态失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_exp($e);
		}
		parent::log('更改简历状态成功');

		if ($state == 5) { 
			g('db') -> commit();

			g('recruit') -> add_hire_count($id);
		}
		cls_resp::echo_ok();
	}

	/**
	 * 批量删除简历
	 *
	 * @access private
	 * @return void
	 */
	private function delete_resume(){
		//支持单个删除
		$id = (int)get_var_value('id');
		if (empty($id)) {
			$fields = array(
				'ids',
			);
			try {
				$data = $this -> get_post_data($fields);
			}catch(SCException $e) {
				cls_resp::echo_exp($e);
			}

			$ids = is_array($data['ids']) ? $data['ids'] : array();
			if (empty($ids)) {
				cls_resp::echo_err(cls_resp::$Busy, '无效的简历对象！');
			}

			$tmp_arr = array();
			foreach ($ids as $val) {
				($tmp_int = (int)$val) != 0 and $tmp_arr[] = $tmp_int;
			}
			unset($val);
			if (empty($tmp_arr)) {
				cls_resp::echo_err(cls_resp::$Busy, '无效的简历对象！');
			}

			$ids = $tmp_arr;
		}else {
			$ids = array($id);
		}

		parent::log('开始删除简历信息');
		try {
			g('recruit') -> update_resume($ids, array('state' => 7));
		}catch(SCException $e) {
			parent::log('删除简历信息失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('删除简历信息成功');

		cls_resp::echo_ok();
	}

	/**
	 * 获取简历列表
	 *
	 * @access private
	 * @return void
	 */
	private function get_resume_list() {
		try {
			$data = $this -> get_post_data();
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$state = isset($data['state']) ? (int)$data['state'] : 0;
		$is_collected = isset($data['is_collected']) ? (int)$data['is_collected'] : 0;
		$page = isset($data['page']) ? (int)$data['page'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		($state < 1 or $state > 7) and $state = 0;
		($is_collected < 1 or $is_collected > 2) and $is_collected = 0;

		$condition = array();
		!empty($state) and $condition['ri.state='] = $state;
		!empty($is_collected) and $condition['ri.is_collected='] = $is_collected;

		$list = g('recruit') -> get_resume_list($condition, $page, $page_size);

		$info = array(
			'rec' => $list,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取简历详情
	 *
	 * @access private
	 * @return void
	 */
	private function get_resume_detail() {
		$id = (int)get_var_value('id');

		if (empty($id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的简历对象！');
		}
		try {
			g('recruit') -> check_hr_valid();
			$condition = array(
				'ri.id=' => $id,
			);
			$data = g('recruit') -> get_resume_detail($condition);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}
		$info = array(
			'rec' => $data,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}
	
	/**
	 * 获取该职位的配置，用于填写简历
	 *
	 * @access private
	 * @return void
	 */
	private function get_job_demand() {
		$rji_id = (int)get_var_value('id');

		if (empty($rji_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的职位对象！');
		}
		try {
			g('recruit') -> check_job_valid($rji_id);
			$condition = array(
				'ji.rji_id=' => $rji_id,
			);
			$data = g('recruit') -> get_job_demand($condition);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$info = array(
			'rec' => $data,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 获取用户余额
	 *
	 * @access private
	 * @return void
	 */
	private function get_user_balance() {
		try {
			$balance = g('recruit') -> get_user_balance(FALSE, FALSE);
			$balance = round((float)$balance / 100, 2);
			$balance = number_format($balance, 2);
		}catch(SCException $e) {
			$balance = 0;
		}

		$info = array(
			'balance' => $balance,
		);
		cls_resp::echo_ok(NULL, 'info', $info);
	}

	/**
	 * 更改红包状态
	 *
	 * @access private
	 * @return void
	 */
	private function change_award_state() {

		$fields = array('id','is_read');
		$data = parent::get_post_data($fields);
		
		$id = (int)$data['id'];
		$is_read = (int)$data['is_read'];
		if (empty($id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的红包对象！');
		}
		if ($is_read != 1) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的状态值！');
		}

		$args = array(
			'is_read' => $is_read,
		);

		parent::log('开始更改红包状态');

		try {
			g('recruit') -> update_award($id, $args);
		}catch(SCException $e) {
			parent::log('更改红包状态失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_exp($e);
		}
		parent::log('更改红包状态成功');
		cls_resp::echo_ok();
	}

	/**
	 * 获取提现申请最低金额
	 *
	 * @access private
	 * @return void
	 */
	private function get_extract_limit() {
		//单位：元
		$limit = g('recruit') -> get_extract_limit();
		
		cls_resp::echo_ok(NULL, 'limit', $limit);
	}
}

/* End of this file */