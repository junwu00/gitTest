<?php
/**
 * 人力招聘，职位相关的控制器
 *
 * @author LiangJianMing
 * @create 2015-04-01
 */
class cls_re_job extends abs_app_base {
	public function __construct() {
		parent::__construct('recruit');
	}

	/**
	 * 填写简历
	 *
	 * @access public
	 * @return void
	 */
	public function apply() {
		$cipher = get_var_value('cipher');
		$rji_id = (int)get_var_value('id');

		if (empty($rji_id)) {
			cls_resp::show_err_page('无效的职位对象！');
		}

		try {
			$share_info = g('recruit') -> get_real_info();
			if ($share_info['user_type'] == 3) {
				cls_resp::show_err_page('无权进行该操作！');
			}
			g('recruit') -> check_job_valid($rji_id);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		$condition = array('ji.rji_id=' => $rji_id);
		try {
			//简历要求
			$info = g('recruit') -> get_job_demand($condition);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}
		
		//企业号js_sdk支持
		$this -> oauth_jssdk('', $share_info['com_id']);
		
		global $app, $module, $action;
		//提交简历的接口
		$submit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=109&free=1&id=' . $rji_id . '&cipher=' . $cipher;
		//职位详情页面url
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=re_job&a=detail&free=1&id=' . $rji_id . '&cipher=' . $cipher;
		//上传头像实现参考移动外勤，不给出接口

		g('smarty') -> assign('submit_url', $submit_url);
		g('smarty') -> assign('detail_url', $detail_url);

		// log_write(json_encode($info));
		g('smarty') -> assign('info', $info);
		g('smarty') -> assign('com_id', $share_info['com_id']);
		
		g('smarty') -> assign('title', '填写简历');
		g('smarty') -> show($this -> temp_path.'share/self.html');
	}

	/**
	 * 职位详情
	 *
	 * @access public
	 * @return void
	 */
	public function detail() {
		$rji_id = (int)get_var_value('id');
		$cipher = get_var_value('cipher');
		$corpurl = !empty($_SESSION[SESSION_VISIT_CORP_URL]) ? $_SESSION[SESSION_VISIT_CORP_URL] : get_var_value('corpurl');
		if (empty($rji_id)) {
			cls_resp::show_err_page('无效的职位对象！');
		}

		try {
			$share_info = g('recruit') -> get_real_info();
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		try {
			g('recruit') -> check_job_valid($rji_id);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		$is_first = g('recruit') -> check_first_view($rji_id);
		if ($is_first) {
			//奖励事件是否执行成功
			$is_finish = TRUE;
			if ($share_info['user_type'] != 3) {
				//发放外部人员查看奖励
				try {
					g('recruit') -> trigger_award(1, $share_info['user_id'], array('job_id' => $rji_id));
				}catch(SCException $e) {
					$is_finish = FALSE;
				}
			}
			$is_finish and g('recruit') -> set_view_cookie($rji_id);
		}

		//企业内部用户生成新的分享参数
		if ($share_info['user_type'] == 3) {
			//初始化用户数据
			g('recruit') -> check_user_init($rji_id);

			$data = array(
				'rji_id' => $rji_id,
			);
			$cipher = g('recruit') -> compose_cus_data($data);
		}

		$condition = array('ji.rji_id=' => $rji_id);
		try {
			//职位详情
			$info = g('recruit') -> get_job_detail($condition);
		}catch(SCException $e) {
			cls_resp::show_err_page($e -> getMessage());
		}

		global $app, $module, $action;
		//填写简历的url
		$apply_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=re_job&a=apply&free=1&cipher=' . $cipher . '&id=' . $rji_id;
		//更多职位url
		$more_job_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=recommend&free=1&cipher=' . $cipher;
		//获取职位列表接口
		$ajax_job_list = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102&free=1&cipher=' . $cipher;
		//点击数+1接口
		$add_click_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=108&free=1&id=' . $rji_id . '&cipher=' . $cipher;
		//分享数+1接口
		$add_share_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=107&free=1&id='.$rji_id.'&cipher='.$cipher;
		//职位详情页面url，欠缺id参数
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=re_job&a=detail&free=1&cipher=' . $cipher;

		//begin---------------分享相关的数据---------------------------
		parent::oauth_jssdk();

		$to_share_url = SYSTEM_HTTP_DOMAIN . 'index.php?app=' . $app . '&m=' . $module . '&a=' . $action . '&free=1&id=' . $rji_id . '&cipher=' . $cipher . '&corpurl=' . $corpurl;
		
		!empty($info['logo']) and $share_icon_url = MEDIA_URL_PREFFIX . $info['logo'];
		empty($share_icon_url) and $share_icon_url = SYSTEM_HTTP_APPS_ICON.'Recommend.png';
		
		$share_desc = '渴望您的加盟！';
		$share_title = $info['com_name'] . '急招《' . $info['name'] . '》';
		
		g('smarty') -> assign('to_share_url', $to_share_url);
		g('smarty') -> assign('share_icon_url', $share_icon_url);
		g('smarty') -> assign('share_desc', $share_desc);
		g('smarty') -> assign('share_title', $share_title);
		//end-----------------分享相关的数据-------------------------------------

		g('smarty') -> assign('apply_url', $apply_url);
		g('smarty') -> assign('add_click_url', $add_click_url);
		g('smarty') -> assign('add_share_url',$add_share_url);
		g('smarty') -> assign('more_job_url', $more_job_url);
		g('smarty') -> assign('ajax_job_list', $ajax_job_list);
		g('smarty') -> assign('detail_url', $detail_url);

		g('smarty') -> assign('info', $info);
		//用户类型：1-游客，2-其他企业号用户，3-当前企业号内部员工
		g('smarty') -> assign('user_type',$share_info['user_type']);

		g('smarty') -> assign('title', '职位详情');
		g('smarty') -> show($this -> temp_path.'index/hiring.html');
	}
}