$(document).ready(function(){
	to_recommend_page();
	get_job(1);
});

//获取推荐过的职位
function get_job(page){
	var data = {
			page:page
	};
	$('#ajax-url').val($('#ajax_job_count').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					show_record(response['info']['rec']);
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function show_record(data){
	var html ="";
	if(data){
		for(var i in data){
			var pos = data[i];
			html += '<div class="record_row"><div class="record_row_child record_row_r">'+
					'<span class="left">'+pos['job_name']+'</span>'+
					'<span class="right">悬赏<b>￥'+pos['hire_limit']+'</b></span>'+
					'</div>'+
					'<div class="record_row_child record_row_3r">'+
					'<span class="t_left">点击量<b>'+pos['click_count']+'</b></span>'+
					'<span class="t_center">投递量<b>'+pos['resume_count']+'</b></span>'+
					'<span class="t_right">聘请成功<b>'+pos['hire_count']+'</b></span>'+
					'</div>'+
					'<div class="clear"></div>'+
					'</div>';
		}
		if(html=="")
			html='<div style="height: 60px;line-height: 60px;text-align: center;font-weight: bold;color: #aaa;font-size: 15px;">你还没有推荐过任何职位</div>';
		$('.record_list').html(html);
	}
}

function to_recommend_page(){
	$('.recommend').unbind('clicki').bind('click',function(){
		location.href=$('#index_url').val();
	});
}
