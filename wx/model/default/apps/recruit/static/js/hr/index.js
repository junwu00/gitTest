var page = 0;
$(document).ready(function(){
	init_change();
	init_bottom_menu_change();
	get_list();
	init_scroll_load('.container', '#load_div', get_list);
});

function get_list(){
	var data = {
			page:++page,
			state:$('.active').attr('s'),
			is_collected:$('.active').attr('c'),
	};
	$('#ajax-url').val($('#resume_list_url').val());
	frame_obj.do_ajax_post(
		undefined,
		'',
		JSON.stringify(data),
		function(response){
			if(response['errcode']==0){
				show_list(response['info']['rec']);
			}else{
				frame_obj.alert(response['errmsg']);
			}
		}
	);
}

function show_list(data){
	var html ="";
	if(data){
		for(var i in data){
			var res = data[i];
			var sex = '';
			if(res['gender']==1){
				sex ="man";
			}else if(res['gender']==2){
				sex = 'women';
			}
			
			if(Number(res['is_collected'])==2)
				collect = "sc_icon_star";
			else
				collect = 'sc_icon_star_full';
			
			html += '<div class="row_content" i="'+res['id']+'">'+
					'<div class="create_date">'+time2date(res['create_time'],'yyyy-MM-dd HH:mm')+'</div>'+
					'<div class="pos_msg">'+
					'<span class="face_pic"><img src="'+$('#media-url').val()+res['portrait']+'"></span>'+
					'<div class="msg">'+
					'<span class="name">'+res['name']+'</span>';
			if(sex!=''){
				html += '<span class="red '+sex+'"></span>';
			}
			if(res['degree']!=''){
				html +=	'<span>'+res['degree']+'</span>';
			}
			if(res['experience']!=''){
				html +=	'<span class="red">'+res['experience']+'</span>';
			}
				html += '</div>'+
					'<div class="pos">'+res['job_name']+'</div>'+
					'</div>'+
					'<div class="btn_bom">'+
					'<div class="sc_icon_delete delete_resume" i='+res['id']+'></div>'+
					'<div class="'+collect+' collected"  i='+res['id']+' ></div>'+
					'</div>'+
					'<div class="clear"></div>'+
					'</div>';
		}
	}else{
		if(page==1)
			html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/logo_empty.png"><span>暂时没有数据</span></div></div>';
	}
	$('.lister').append(html);
	if(data.length)
		scroll_load_complete('#load_div',data.length);
	else
		scroll_load_complete('#load_div',0);
	init_content_click();
	init_delete_collect();
}

function init_delete_collect(){
	$('.delete_resume').unbind('click').bind('click',function(e){
		e.stopPropagation();
		var id =$(this).attr('i');
		frame_obj.comfirm('确定要删除该简历吗？',function(){
			delete_resume(id);
		});
	});
	$('.collected').unbind('click').bind('click',function(e){
		e.stopPropagation();
		var id = $(this).attr('i');
		if($(this).hasClass('sc_icon_star')){
			$(this).removeClass('sc_icon_star');
			$(this).addClass('sc_icon_star_full');
			change_collect(id,1,$(this));
		}else{
			$(this).removeClass('sc_icon_star_full');
			$(this).addClass('sc_icon_star');
			change_collect(id,2,$(this));
		}
	});
}

function change_collect(id,state,star){
	$('#ajax-url').val($('#state_url').val()+'&id='+id+'&collected='+state);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					
				}else{
					if(state==1){
						star.removeClass('sc_icon_star_full');
						star.addClass('sc_icon_star');
					}else{
						star.removeClass('sc_icon_star');
						star.addClass('sc_icon_star_full');
					}
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function delete_resume(id){
	$('#ajax-url').val($('#del_url').val()+'&id='+id);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					$('.row_content[i='+id+']').remove();
					if($('.row_content').length==0){
						var html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/logo.png"><span>暂时没有数据</span></div></div>';
						$('.lister').html(html);
					}
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function init_content_click(){
	$('.row_content').unbind('click').bind('click',function(){
		location.href=$('#detail_url').val()+'&id='+$(this).attr('i');
	});
}

function init_change(){
	$('.change').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.lister').html("<div class='pd'></div>");
			$('#load_div').html('<div id="loading"><img src="'+$('#app_common').val()+'static/images/wait.gif">加载中...</div>');
			page = 0;
			get_list();
			is_scroll_loading = true;
			is_scroll_end = false;
		});
	});
}

function init_bottom_menu_change(){
	$('.nav-pills>li').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.on').removeClass('on');
			$(this).addClass('on');
			$('.active').removeClass('active');
			$('.'+$(this).attr('c')).addClass('active');
			
			if($(this).attr('c')=="nosee"){
				$('.head').removeClass('none');
				$('.lister').html("<div class='pd'></div>");
			}
			else{
				$('.head').addClass('none');
				$('.lister').html("");
			}
			
			$('#load_div').html('<div id="loading"><img src="'+$('#app_common').val()+'static/images/wait.gif">加载中...</div>');
			page = 0;
			get_list();
			is_scroll_loading = true;
			is_scroll_end = false;
		});
	});
}
