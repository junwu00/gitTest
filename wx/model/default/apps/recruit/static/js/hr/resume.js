$(document).ready(function(){
	init_state();
	init_hire();
	init_change_state();
	init_bottom();
	show_img();
});
function show_img(){
	$('.img_list img,.face img').each(function(){
		$(this).unbind('click').bind('click',function(){
			frame_obj.show_img($(this).attr('src'));
		});
	});
}

function init_bottom(){
	$('.change_collect').unbind('click').bind('click',function(){
		var star = $('.change_collect .sc_icon_star');
		
		if(star.length!=0){
			star.removeClass('sc_icon_star');
			star.addClass('sc_icon_star_full');
			$('.change_collect .grey').addClass('yellow');
			$('.change_collect .grey').removeClass('grey');
			change_collect(1,star);
		}else{
			star = $('.change_collect .sc_icon_star_full')
			star.removeClass('sc_icon_star_full');
			star.addClass('sc_icon_star');
			$('.change_collect .yellow').addClass('grey');
			$('.change_collect .yellow').removeClass('yellow');
			change_collect(2,star);
		}
	});
}

function change_collect(state,star){
	$('#ajax-url').val($('#state_url').val()+'&collected='+state);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
				}else{
					if(state==1){
						star.removeClass('sc_icon_star_full');
						star.addClass('sc_icon_star');
					}else{
						star.removeClass('sc_icon_star');
						star.addClass('sc_icon_star_full');
					}
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function init_change_state(){
	if(Number($('#state').val())==1){
		$('#ajax-url').val($('#state_url').val()+'&state='+2);
		frame_obj.do_ajax_post(
				undefined,
				'',
				'',
				function(response){
					if(response['errcode']==0){
						
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	}
}

function init_hire(){
	$('.btn_p.p').unbind('click').bind('click',function(){
		frame_obj.comfirm('确定要聘用该面试者？',function(){
			hire(5);
		});
	});
	$('.btn_p.n').unbind('click').bind('click',function(){
		frame_obj.comfirm('确定不聘用该面试者？',function(){
			hire(6);
		});
	});
	$('.btn_p.y').unbind('click').bind('click',function(){
		invite(2);
	});
	$('.btn_p.w').unbind('click').bind('click',function(){
		invite(4);
	});
}


function invite(state){
	if(state != 4 && state != 2){
		frame_obj.alert('操作错误');
		return;
	}
	
	$('#ajax-url').val($('#state_url').val()+'&state='+state);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					if(state == 4){
						$('.btn_p.y').css('display','inline-block');
						$('.btn_p.w').css('display','none');
					}else if(state == 2){
						$('.btn_p.y').css('display','none');
						$('.btn_p.w').css('display','inline-block');
					}
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function hire(state){
	$('#ajax-url').val($('#state_url').val()+'&state='+state);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					location.reload();
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function init_state(){
	var state = $('.state').attr('s');
	var msg = "";
	switch(Number(state)){
		case 1 : $('.date').css('display','none');$('.pinyong').css('display','block');$('.btn.w').css('display','inline-block'); break;
		case 2 : $('.date').css('display','none');$('.pinyong').css('display','block');$('.btn.w').css('display','inline-block'); break;
		case 4 : $('.date').css('display','none');$('.pinyong').css('display','block');$('.btn.y').css('display','inline-block'); break;
		case 5 : msg = "已聘用";$('.date').css('display','block');break;
		case 6 : msg = "未聘用";$('.date').css('display','block');break;
	}
	$('.state').html(msg);
	
}

function delete_resume(){
	frame_obj.comfirm('确定要删除该简历？',function(){
		delete_res();
	});
}

function delete_res(){
	$('#ajax-url').val($('#state_url').val()+'&state='+7);
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					location.href=$('#lister_url').val();
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}