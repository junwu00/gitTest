var page = 0;
var scoll_over = false;
$(document).ready(function(){
	tixian();
	init_change();
	get_list();
	get_balance();
	init_scroll_load('.container', '#load_div', get_list);
});

function get_balance(){
	$('#ajax-url').val($('#get_balance_url').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			'',
			function(response){
				if(response['errcode']==0){
					$('.all_money').html(response['info']['balance']);
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function get_list(){
	var data = {
			page:++page
	};
	
	var type=$('.active').attr('t');
	if(type==1){
		$('#ajax-url').val($('#award_list_url').val());
	}else if(type==2){
		$('#ajax-url').val($('#extract_list_url').val());
	}
	
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					if(type==1)
						show_list(response['info']['rec']);
					else
						show_list_ex(response['info']['rec']);
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function show_list_ex(data){
	var html ="";
	if(data){
		for(var i in data){
			var red = data[i];
			var msg = "";
			switch(Number(red['state'])){
				case 1:
					msg ="待审核";break;
				case 2:
					msg ="待兑现";break;
				case 3:
					msg ="已兑现";break;
				case 4:
					msg ="被驳回";break;
			}
			
			html += '<div class="item">'+
					'<div class="redbag left"><img src="'+$('#app_static').val()+'images/myredbag.png"/>'+
					'</div>'+
					'<div class="t_money right">'+
					'<p class="red t_right">￥'+red['money']+'</p>'+
					'<p class="state_msg">'+msg+'</p>'+
					'</div>'+
					'<span>'+
					'<p class="msg">'+msg+'</p>'+
					'<p class="msg_date">'+time2date(red['create_time'],'yyyy-MM-dd')+'</p>'+
					'</span>'+
					'<div class="clear"></div>'+
					'</div>';
		}
		scroll_load_complete('#load_div',1);
		$('.lister_redbag').append(html);
	}else{
		if(page==1){
			html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/logo_empty.png"><span>暂时没有数据</span></div></div>';
			$('.lister_redbag').append(html);
		}
		scroll_load_complete('#load_div',0);
	}
}

function show_list(data){
	var html ="";
	var all_money =0;
	if(data){
		for(var i in data){
			var red = data[i];
			var open ="";
			var money ="";
			var open_state = '';
			if(red['is_read']==0){
				open = ' <span class="noopen">&nbsp;</span> ';
				open_state ="open_off"
				money = '<div style="display:none;" class="money right"><p class="red">￥'+red['money']+'</p><p class="state_msg">已打开</p>	</div>';
			}else{
				all_money += Number(red['money']);
				money = '<div class="money right"><p class="red">￥'+red['money']+'</p><p class="state_msg">已打开</p>	</div>';
			}
			var msg = "";
			switch(Number(red['event_type'])){
				case 1:
					msg ="分享被查看";break;
				case 2:
					msg ="投递简历";break;
				case 3:
					msg ="成功录用";break;
			}
			
			html += '<div i="'+red['id']+'" m="'+red['money']+'" class="item '+open_state+'">'+
					'<div class="redbag left"><img src="'+$('#app_static').val()+'images/myredbag.png"/>'+
					open+
					'</div>'+
					money+
					'<span>'+
					'<p class="msg">'+msg+'</p>'+
					'<p class="msg_date"><span>'+red['job_name']+'</span>'+time2date(red['create_time'],'yyyy-MM-dd')+'</p>'+
					'</span>'+
					'<div class="clear"></div>'+
					'</div>';
		}
		scroll_load_complete('#load_div',1);
		$('.lister_redbag').append(html);
		open_redbag();
	}else{
		if(page==1){
			html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/logo_empty.png"><span>暂时没有数据</span></div></div>';
			$('.lister_redbag').append(html);
		}
		scroll_load_complete('#load_div',0);
	}
}

function open_redbag(){
	$('.redbag_img').unbind('click').bind('click',function(){
		if($(this).hasClass('unopen')){
			open($(this).attr('i'));
		}else{
			$('.share_img>.test').remove();
		}
	});
	
	$('.open_off').unbind('click').bind('click',function(){
		$('.share_img').css('display','block');
		$('.redbag_img').attr('i',$(this).attr('i'));
	});
	
	
	
	
}

//打开红包方法
function open(id){
	var data = {
			id:id,
			is_read:1
	};
	$('#ajax-url').val($('#open_redbag_url').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					$('.redbag_img').css('display','none');
					$('.open').css('display','block');
					$('.money_number').css('display','block');
					$('.close_redbag').css('display','block');
					for(var i =0;i<25;i++){
						var html =  '<span class="test money_y" style="top:-'+((Math.random()*2000)+100)+'px;right:'+Math.random()*100+'%;" >&nbsp;</span>';
						$('.share_img').append(html);
					}
					$('.money_number').html('￥'+$('.item[i='+id+']').attr('m'));
					$('.item[i='+id+']>.money').css('display','block');
					$('.item[i='+id+']>.redbag>.noopen').remove();
					$('.item[i='+id+']').unbind('click');
					get_balance();
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

function hidden_redbag(){
	$('.close_redbag').css('display','none');
	$('.money_number').css('display','none');
	$('.share_img>.test').remove();
	$('.unopen').css('display','block');
	$('.open').css('display','none');
	$('.share_img').css('display','none');
}

function init_change(){
	$('.change').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.lister_redbag').html("");
			$('#load_div').html('<div id="loading"><img src="'+$('#app_common').val()+'static/images/wait.gif">加载中...</div>');
			page = 0;
			get_list();
			is_scroll_loading = true;
			is_scroll_end = false;
		});
	});
}
function tixian(){
	$('.tixian').unbind('click').bind('click',function(){
		frame_obj.comfirm('你确定要进行提现操作吗',function(){
			var money = $('.all_money').html();
			money = money.replace(',','');
			var data = {
					money:money
			};
			$('#ajax-url').val($('#apply_extract').val());
			frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						get_balance();
						frame_obj.alert('提现成功');
					}else{
						frame_obj.alert(response['errmsg']);
					}
				},undefined,undefined,undefined,'提取中……'
			);
		});
	});
}
