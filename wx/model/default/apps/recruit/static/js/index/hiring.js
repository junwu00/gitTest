$(document).ready(function(){
	share();
	to_hiring();
	get_more_job();
	hiring_self();
	add_click();
	change_textarea();
});

function change_textarea(){
	var desc_content = $(".desc_content_t").val();
	desc_content = desc_content.replace(/\n/g,"<br/>");
	$(".desc_content").html(desc_content);
}

//页面加载完成展示
function show_container(){
	$('.load_img').css('display','none');
	$('.container').css('display','block');
}


function share_add(){
	$('#ajax-url').val($('#add_share_url').val());
	frame_obj.do_ajax_post(
		undefined,
		'',
		'',
		function(response){
			if(response['errcode']==0){
				frame_obj.alert('分享成功');
			}else{
				frame_obj.alert(response['errmsg']);
			}
		}
	);
}

//点击数+1
function add_click(){
	$('#ajax-url').val($('#add_click_url').val()+'&id='+$('#rji_id').val());
	frame_obj.do_ajax_post(
		undefined,
		'',
		'',
		function(response){
			if(response['errcode']==0){
			}else{
				frame_obj.alert(response['errmsg']);
			}
		}
	);
}

//获取更多职位
function get_more_job(){
	//内部人员
	if($('#user_type').val()==3){
		var data = {
		};
		$('#ajax-url').val($('#ajax_job_list').val());
		frame_obj.do_ajax_post(
				undefined,
				'',
				JSON.stringify(data),
				function(response){
					if(response['errcode']==0){
						show_more_job(response['info']['rec']);
					}else{
						frame_obj.alert(response['errmsg']);
					}
				}
		);
	//外部人员
	}else if($('#user_type').val()==1 || $('#user_type').val()==2){
		$('.other_position').unbind('click').bind('click',function(){
			location.href=$('#more_job_url').val();
		});
	}
}
function show_more_job(data){
	var html ="";
	if(data){
		for(var i in data){
			var pos = data[i];
			if(pos['rji_id'] !=$('#rji_id').val()){
				html +='<div class="row_content"><div class="row-msg hiring" i="'+pos['rji_id']+'"><span style="-o-text-overflow: ellipsis;text-overflow: ellipsis;white-space: nowrap;overflow:hidden;height:45px;" class="t_left">'+pos['name']+'</span><span class="t_right">悬赏<a class="icon-angle-right"></a> <b>￥'+pos['hire_limit']+'</b></span>	</div></div>'
			}
		}
	}
	if(html=="")
		html = '<div style="line-height:45px;text-align:center;">暂时没有其他职位</div>';
	$('.op_content').html(html);
	to_hiring();
}
function to_hiring(){
	$('.hiring').each(function(){
		var id = $(this).attr('i');
		$(this).children('span').unbind('click').bind('click',function(){
			location.href=$('#detail_url').val()+'&id='+id;
		});
	});
}

function share(){
	$('.share_button').unbind('click').bind('click',function(){
		$('.share_img').css('display','block');
	});
}

function hiring_self(){
	$('.hiring_self').unbind('click').bind('click',function(){
		location.href=$('#apply_url').val();
	});
}
