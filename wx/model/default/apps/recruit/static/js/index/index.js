var  page = 0;
$(document).ready(function(){
	get_com_info();
	show_change();
	share();
	get_job(++page);
});

//页面加载完成展示
function show_container(){
	$('.load_img').css('display','none');
	$('.container').css('display','block');
}

//获取企业信息
function get_com_info(){
	var data = {
	};
	$('#ajax-url').val($('#ajax_com_list').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					show_com(response['info']['rec']);
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}
//展示企业信息
function show_com(data){
	html = "";
	if(data){
		
		var banner = data['banner'] == "" ? 'apps/recruit/static/images/banner.jpg' : $('#media-url').val()+data['banner'];
		var logo = data['logo'] == "" ? 'apps/recruit/static/images/logo.jpg' : $('#media-url').val() + data['logo'];
		
		$('.banner>img').attr('src',banner);
		html = '<div class="logo"><img src="' + logo + '" /></div>'+
					'<div class="com_msg">'+
						'<div class="com_msg_row s" >'+
							'<span class="msg_t">领域：</span><p>'+data['nature_desc']+'</p>'+
						'</div>'+
						'<div class="com_msg_row s">'+
							'<span class="msg_t">地址：</span><p>'+data['local_desc']+'</p>'+
						'<div class="com_msg_row s">'+
							'<span class="msg_t">规模：</span><p>'+data['scale_desc']+'</p>'+
						'</div>'+
					'</div>';
		if(data['address']){
			html += '<div class="com_msg_row l">'+
						'<span class="msg_t">详细地址：</span><p>'+data['address']+'</p>'+
					'</div>';
		}
		if(data['description']){
			html +=	'<div class="com_msg_row l">'+
						'<span class="msg_t">公司描述：</span><p>'+data['description']+'</p>'+
					'</div>';
		}
				'</div>';
		$('.content2').html(html);
	}else{
		frame_obj.alert('无法获取企业信息');
	}
}

//获取职位列表
function get_job(page){
	var data = {
			page:page
	};
	$('#ajax-url').val($('#ajax_job_list').val());
	frame_obj.do_ajax_post(
			undefined,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					show_list(response['info']['rec']);
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
	);
}

//展示列表
function show_list(data){
	var html ="";
	var user_type = Number($('#user_type').val());
	$('#get_more').remove();
	if(data.length!=0){
		for(var i in data){
			var pos = data[i];
			if(user_type==3){	//内部员工
				html += '<div class="row_content">'+
						'<div class="row-msg hiring"  i="'+pos['rji_id']+'">'+
						'<div class="msg_1">'+
						'<span style="-o-text-overflow: ellipsis;text-overflow: ellipsis;white-space: nowrap;overflow:hidden;" class="t_left">'+pos['name']+'</span>'+
						'<span class="t_center"><a class="icon-angle-right"></a>悬赏<b>￥'+pos['hire_limit']+'</b></span>'+
						'</div>'+
						'<div class="msg_2">'+
						'<span>已点击：<a>'+pos['click_count']+'</a></span>'+
						'<span>已投递：<a>'+pos['resume_count']+'</a></span>'+
						'<span>截止时间：<a>'+time2date(pos['end_time'],'yyyy-MM-dd')+'</a></span>'+
						'</div>'+
						'</div>'+
						'</div>';
				
//				html +='<div class="row_content"><div class="row-msg hiring" i="'+pos['rji_id']+'"><span style="-o-text-overflow: ellipsis;text-overflow: ellipsis;white-space: nowrap;overflow:hidden;height:45px;" class="t_left">'+pos['name']+'</span><span class="t_right"><a style="float:left;">悬赏</a><a class="icon-angle-right"></a> <b>￥'+pos['hire_limit']+'</b></span>	</div></div>'
			}else if(user_type == 2 || user_type ==1){
				html += '<div class="row_content">'+
				'<div class="row-msg hiring"  i="'+pos['rji_id']+'">'+
				'<div class="msg_1">'+
				'<span style="-o-text-overflow: ellipsis;text-overflow: ellipsis;white-space: nowrap;overflow:hidden;" class="t_left">'+pos['name']+'</span>'+
				'<span class="t_center"><a class="icon-angle-right"></a>查看详情</span>'+
				'</div>'+
				'<div class="msg_2">'+
				'<span>已点击：<a>'+pos['click_count']+'</a></span>'+
				'<span>已投递：<a>'+pos['resume_count']+'</a></span>'+
				'<span>截止时间：<a>'+time2date(pos['end_time'],'yyyy-MM-dd')+'</a></span>'+
				'</div>'+
				'</div>'+
				'</div>';
			}
		}
		html +='<div id="get_more">展示更多<a class="icon-angle-right"></a></div>';
	}else{
		if(page==1){
			html = '<div style="line-height:35px;text-align:center;">暂时没有职位</div>';
		}else{
			html = '<div style="line-height:35px;text-align:center;">已经没有更多职位</div>';
		}
	}
	$('.content1').append(html);
	to_hiring();
	to_get_more();
}

//绑定获得更多的方法
function to_get_more(){
	$('#get_more').unbind('click').bind('click',function(){
		get_job(++page);
	});
}

//给职位DIV绑定跳转到职位详情页面的方法
function to_hiring(){
	$('.hiring').each(function(){
		var id = $(this).attr('i');
		$(this).unbind('click').bind('click',function(){
			location.href=$('#detail_url').val()+'&id='+id;
		});
	});
}

function share(){
	$('.share_button').unbind('click').bind('click',function(){
		$('.share_img').css('display','block');
	});
}
function show_change(){
	$('.title>span').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.title>span').addClass('noactive');
			$(this).removeClass('noactive');
			$('.con').css('display','none');
			$($(this).attr('i')).css('display','block');;
		});
	});
}