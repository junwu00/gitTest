var img_list = new Array();
var pic_face = "";
$(document).ready(function(){
	init_choose();
	wx.ready(function(){
		$('#img-upload-item').wximgupload(wx_img_upload_opt).wximgupload('init');
		$('#img-upload-item').wximgupload('show_icon').wximgupload('set_val', img_list).wximgupload('bind_preview');
		upload_face();
	});
	init_submit_msg();
	init_data();
});

function init_data(){
	var star = '<b>*</b>';
	$('.r_t').each(function(){
		var rt = $(this);
		if(rt.attr('t')==1)
			rt.html(star + rt.html());
	});
}

function init_submit_msg(){
	$('.submit').unbind('click').bind('click',function(){
		
		if(!check_msg())
			return;
		
		var data = get_post_data();
		var btn = $(this);
		
		$('#ajax-url').val($('#submit_url').val());
		frame_obj.do_ajax_post(
			btn,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					frame_obj.comfirm('HR已经收到你的简历，请耐心等待回复吧！',function(){
						frame_obj.close_browser();
					});
				}else{
					frame_obj.alert(response['errmsg']);
				}
			}
		);
	});
}
function check_msg(){
	var state = true;
	$('.inp[t="1"]').each(function(){
		var inp = $(this);
		if($.trim(inp.val())==""){
			frame_obj.alert(inp.attr('n')+'不能为空');
			state = false;
			return false;
		}else{
			return true;
		}
	});
	if(!state)
		return false;
	
	if($('.sex_title').attr('t')==1 && $('.sex.check_active').length !=1){
		frame_obj.alert('请选择性别');
		return false;
	}
	
	if(pic_face==""){
		frame_obj.alert('请上传头像');
		return false;
	}
	
//	if($('.sayself').attr('t')==1 && $.trim($('.sayself').text())==""){
//		frame_obj.alert('请输入工作经历描述或者自我介绍');
//		return false;
//	}
	return state;
}

function get_post_data(){
	var other_info = [];
	$('.custom').each(function(){
		other_info.push({name:$(this).attr('n'),value:$(this).val()});
	});
	img_list = $('#img-upload-item').wximgupload('get_val');
	var img_arr = [];
	for (var i in img_list) {
		img_arr.push(img_list[i]);
	}
	
	var data ={
			name:$('.user_name').val(),
			mobile:$('.user_mobile').val(),
			portrait:pic_face,
			gender:$('.sex.check_active').attr('v'),
			age:$('.user_age').val(),
			salary:$('.use_salary').val(),
			dwell:$('.user_dwell').val(),
			experience:$('.use_experience').val(),
			degree:$('.user_degree').val(),
			job:$('.use_job').val(),
			email:$('.user_email').val(),
			accessory:img_arr,
			other_info:other_info,
	};
//	$('.r_t').each(function(){
//		if($(this).attr('n')=="") return true;
//		alert($(this).attr('n')+'--'+$(this).parent().children('.inp').val());
//		data[$(this).attr('n')] = $(this).parent().children('.inp').val();
//	});
	
	return data;
}

function upload_face(){
	$('.upload_face').unbind('click').bind('click',function(){
		var upload_base = $('#upload_url').val()+'&com_id='+$('#com_id').val(); 
		var opt = {};
		opt.count = 1;
		opt.sizeType = opt.sizeType || ['original', 'compressed'];
		opt.sourceType = opt.sourceType || ['album', 'camera'];
		jssdk_obj.full_upload_img(
				1,
				0,
				function(media_id) {						
					var upload_url = upload_base + '&media_id=' + media_id;
					frame_obj.simple_ajax_post(undefined, upload_url, '', 'json', function(data) {
						if (data.errcode == 0) {
							frame_obj.alert('上传成功');
							pic_face = data.data.path;
							$('.p_face').attr('src',$('#media-url').val()+pic_face);
						}
					});
				},
				function(res) {
					frame_obj.alert('上传失败');
				},
				function() {
				},
				opt
		);
	});
	$('.upload_face').css('display','inline-block');
}

function init_choose(){
	$('.check').each(function(){
		$(this).unbind('click').bind('click',function(){
			$('.check_active').removeClass('check_active');
			$(this).addClass('check_active');
		});
	});
}
