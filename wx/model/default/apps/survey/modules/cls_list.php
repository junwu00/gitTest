<?php
/**
 * 问卷调查——我的调研
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_list extends abs_app_base {
	public function __construct() {
		parent::__construct('survey');
	}
	
	/**
	 * 展示未过期且未作答调研列表
	 *
	 * @access public 
	 * @return void
	 */
	public function show() {
        header('Location: '.SYSTEM_HTTP_DOMAIN.'view_src_dist/index.html#/nhwxQuestionSurveyList');
        exit();
		global $app, $module, $action;
		$ajax_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=100';
		$detail_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=detail';

		g('smarty') -> assign('ajax_list_url', $ajax_list_url);
		g('smarty') -> assign('detail_url', $detail_url);
		// g('smarty') -> assign('APP_MENU', $this -> app_menu[0]);
		g('smarty') -> show($this -> temp_path.'list/index.html');
	}
}

// end of file