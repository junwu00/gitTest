<?php
/**
 * 微研——我的调研
 * @author yangpz
 * @date 2014-12-03
 *
 */
class cls_detail extends abs_app_base {
	public function __construct() {
		parent::__construct('survey');
	}
    /**
     * 参与调研页
     *
     * @access public
     * @return void
     */
    public function index() {
        $sui_id = (int)get_var_value('id');
        header('Location: '.SYSTEM_HTTP_DOMAIN.'view_src_dist/index.html#/nhwxQuestionSurveyDetail?&sui_id='.$sui_id);
        // g('smarty') -> show($this -> temp_path.'detail/deal.html');
    }
	/**
	 * 参与调研页
	 *
	 * @access public
	 * @return array
	 */
	public function get_index_info() {
		$sui_id = (int)get_var_value('id');
		$key_str = get_var_value('key_str');
		$key_str = trim($key_str);

		if (empty($sui_id)) {
			cls_resp::show_err_page('无效的调研对象！');
		}

		if (!empty($key_str)) {
			$key_data = g('des') -> decode($key_str);
			$key_data = json_decode($key_data, TRUE);
		}
		empty($key_data) and $key_data = array();

		if (empty($key_data) and empty($_SESSION[SESSION_VISIT_COM_ID])) {
			cls_resp::show_err_page('无权访问该链接！');
		}

		//标识用户是内部用户还是外部用户
		$is_inner = 1;
		if (!empty($key_data) and (!empty($_SESSION[SESSION_VISIT_COM_ID]) or $key_data['com_id'] != $_SESSION[SESSION_VISIT_COM_ID])) {
			$is_inner = 2;
		}

		//标识用户是否已经参与
		$attend = FALSE;
		// 返回数据
        $data = [];
		try {
			g('survey') -> check_valid($sui_id);
		}catch(SCException $e) {
			$err_code = $e -> getCode();
			//输出提示信息
			// $err_code == 1002 and cls_resp::show_warn_page($e -> getMessage());
			//输出错误信息
			// $err_code != 1001 and cls_resp::show_err_page($e -> getMessage());

			//标识用户是否已经参与
            if($err_code == 1001){
                $attend = TRUE;
                $data['attend'] = 1;
            }else{
                cls_resp::echo_exp($e);
            }
			// g('smarty') -> assign('attend', 1);
		}
		try {
			if (!$attend) {
				$info = g('survey') -> get_survey_info($sui_id, TRUE);
				log_write(json_encode($info));
				//初始化cookie信息
				g('survey') -> sheet_cookie_init($sui_id);
			}else {
                $info = g('survey') -> get_survey_answer_info($sui_id, FALSE);
				// $info = g('survey') -> get_survey_info($sui_id, FALSE);
			}
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		//begin---------------分享相关的数据---------------------------
		global $app, $module, $action;

		$share_crop_id = '';
		$share_com_id = 0;
		if (!empty($key_data)) {
			$share_crop_id = $key_data['app_id'];
			$share_com_id = $key_data['com_id'];
		}
		// g('js_sdk') -> auth_assign_jssdk_sign();

		$to_share_url = '';
		$share_icon_url = '';
		$share_desc = '请注意，该调研不可分享！';
		$share_title = '请注意，该调研不可分享！';

		if ($info['shareable'] == 1) {
			if (empty($key_data)) {
				$key_data = array(
					'app_id' => $_SESSION[SESSION_VISIT_CORP_ID],
					'com_id' => $_SESSION[SESSION_VISIT_COM_ID],
				);
				$key_str = g('des') -> encode(json_encode($key_data));
			}
			// $to_share_url = SYSTEM_HTTP_DOMAIN . 'index.php?app=' . 'survey' . '&m=' . $module . '&a=to_share_url' . '&id=' . $sui_id . '&free=1&key_str=' . $key_str;
			$to_share_url = SYSTEM_HTTP_DOMAIN ."view_src_dist/index.html#/nhwxQuestionSurveyDetail?sui_id={$sui_id}&key_str={$key_str}&free=1";
			$share_icon_url = SYSTEM_HTTP_APPS_ICON.'Survey.png';
			$share_desc = '期待您的参与！';
			$share_title = '诚挚邀请您参与调研《'. $info['title'] . '》';
		}
        $data['to_share_url'] = $to_share_url;
        $data['share_icon_url'] = $share_icon_url;
        $data['share_desc'] = $share_desc;
        $data['share_title'] = $share_title;
		// g('smarty') -> assign('to_share_url', $to_share_url);
		// g('smarty') -> assign('share_icon_url', $share_icon_url);
		// g('smarty') -> assign('share_desc', $share_desc);
		// g('smarty') -> assign('share_title', $share_title);
		//end---------------分享相关的数据-------------------------------------

		if (!$attend) {
			//用作提交问卷的接口
			$ajax_submit_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.'survey' .'&m=ajax&cmd=101&free=1';
            $data['ajax_submit_url'] = $ajax_submit_url;
			// g('smarty') -> assign('ajax_submit_url', $ajax_submit_url);
		}

		$info['type'] = $info['class_name'];
		//用作跳转返回的url
		$back_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.'survey' .'&m=list&a=show';

        $data['info'] = $info;
        $data['is_inner'] = $is_inner;
        $data['back_url'] = $back_url;
		// g('smarty') -> assign('info', $info);
		// g('smarty') -> assign('is_inner', $is_inner);
		// g('smarty') -> assign('back_url', $back_url);

        $data['errCode'] = 0;
        $data['errMsg'] = '';
        if (!$data['attend']){
            if ($info['state'] == 1) {
                $data['errCode'] = 1;
                $data['errMsg'] = '该调研尚未发布！';
            }elseif ($info['state'] == 3) {
                $data['errCode'] = 2;
                $data['errMsg'] = '该调研已被暂停！';
            }elseif ($info['state'] == 4 || $info['end_time'] <= time()) {
                $data['errCode'] = 3;
                $data['errMsg'] = '该调研已结束';
            }
        }
        cls_resp::echo_ok(NULL, 'info', isset($data) ?$data : array());
	}
}

// end of file