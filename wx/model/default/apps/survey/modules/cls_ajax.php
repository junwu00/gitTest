<?php
/**
 * 全部调研ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2015-03-04
 */
class cls_ajax extends abs_app_base {
	public function __construct() {
		parent::__construct('survey');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		$func = '';
		try{
            switch($cmd) {
                case 100 : {
                    //获取未作答且未过期的调研列表
                    $this -> get_survey_list();
                    BREAK;
                }
                case 101 : {
                    //提交调研答卷
                    $this -> submit_survey();
                    BREAK;
                }
                default : {
                    //非法的请求
                    cls_resp::echo_resp(cls_resp::$ForbidReq);
                    BREAK;
                }
            }
        }catch (\Exception $e){
            cls_resp::echo_exp($e);
        }
	}
	/**
	 * 获取调研列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_survey_list() {
		$data = $this -> get_post_data();

		$page = isset($data['page']) ? (int)$data['page'] : 1;
        $is_answer = isset($data['is_answer']) ? (int)$data['is_answer'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$list = g('survey') -> get_survey_list($page, $page_size,'',$is_answer);

		cls_resp::echo_ok(NULL, 'info', isset($list['data']) ? $list['data'] : array());
	}

	/**
	 * 提交调研问卷
	 *
	 * @access public
	 * @param integer $sui_id 调研id
	 * @param array $answer_list 答案列表
	 * 如：
	 * $answer_list = array(
	 *     //单选、打分题(相当于有5个选项的单选题)
	 *		array(
	 *			'qs_id' => 问题id,
	 *			'answer' => 选项id,
	 *		),
	 *     //多选题
	 *		array(
	 *			'qs_id' => 问题id,
	 *			'answer' => array(选项id, 选项id, ...),
	 *		),
	 *     //填空题,
	 *		array(
	 *			'qs_id' => 问题id,
	 *			'answer' => array(空1字符串, 空2字符串, ...),
	 *		),
	 *     //问答题
	 *		array(
	 *			'qs_id' => 问题id,
	 *			'answer' => 答案字符串,
	 *		),
	 * );
	 *
	 * @return void
	 */
	public function submit_survey() {
		$fields = array(
			'sui_id', 'answer_list', 
		);
		$data = $this -> get_post_data($fields);

		$sui_id = (int)$data['sui_id'];
		$answer_list = $data['answer_list'];
		if (empty($sui_id)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的调研对象！');
		}

		if (empty($answer_list)) {
			cls_resp::echo_err(cls_resp::$Busy, '无效的答卷！');
		}

		try {
			g('survey') -> check_valid($sui_id);
			$types_map = g('survey') -> get_qs_types($sui_id);
			
			$ques_list =  g('survey') -> get_question_list($sui_id);
			
			$opts_info = g('survey') -> get_options_info($sui_id);
			$info = g('survey') -> get_survey_info($sui_id);
		}catch(SCException $e) {
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}

		//过滤为空的答题，为空的答题视为没有作答
		$tmp_arr = array();
		foreach ($answer_list as $ans) {
			if (!isset($ans['answer'])) continue;
			//答案为空的答题直接过滤
			if (is_array($ans['answer']) and empty($ans['answer'])) continue;
			//避免答案为0也被判断为空的情况
			if (is_string($ans['answer']) and trim($ans['answer']) == '') continue;
			//不存在的题目直接过滤
			if (!isset($types_map[$ans['qs_id']])) continue;
			$tmp_arr[] = $ans;
		}
		unset($ans);

		$answer_list = $tmp_arr;
		unset($tmp_arr);

		if (count($answer_list) == 0) {
			cls_resp::echo_err(cls_resp::$Busy, '请至少回答一题！');
		}

		if(!$this -> check_astrict($ques_list,$answer_list)){
			cls_resp::echo_err(cls_resp::$Busy, '您还有必答的题目未作答哦！');
		}

		$name_map = g('survey') -> name_map;

		$opt_ids = array();
		foreach ($answer_list as &$ans) {
			$ans['ans_desc'] = '';

			switch ($types_map[$ans['qs_id']]) {
				//单选题
				case 1 : {
					if (!($tmp_opt = intval($ans['answer'])) || !isset($opts_info[$tmp_opt]) || $opts_info[$tmp_opt]['qs_id'] != $ans['qs_id']) {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}
					$opt_ids[] = $tmp_opt;
					$ans['ans_desc'] = $opts_info[$tmp_opt]['desc'];
					BREAK;
				}
				//多选题
				case 2 : {
					if (!is_array($ans['answer']) || empty($ans['answer'])) {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}

					$tmp_arr = array();
					foreach ($ans['answer'] as $val) {
						$tmp_opt = intval($val);
						if (empty($tmp_opt)) continue;

						if (!isset($opts_info[$tmp_opt]) || $opts_info[$tmp_opt]['qs_id'] != $ans['qs_id']) {
							cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
						}

						$tmp_arr[] = $tmp_opt;
						$opt_ids[] = $tmp_opt;
						$ans['ans_desc'] .= $opts_info[$tmp_opt]['desc'] . '；';
					}
					unset($val);

					if (empty($tmp_arr)) {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}

					$ans['ans_desc'] = mb_substr($ans['ans_desc'], 0, -1, 'UTF-8');
					$ans['answer'] = $tmp_arr;
					BREAK;
				}
				case 3 : {
					if (!($tmp_opt = intval($ans['answer'])) || !isset($opts_info[$tmp_opt]) || $opts_info[$tmp_opt]['qs_id'] != $ans['qs_id']) {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}
					$opt_ids[] = $tmp_opt;
					$ans['ans_desc'] = $name_map[$opts_info[$tmp_opt]['desc']];
					BREAK;
				}
				case 4 : {
					if (!is_array($ans['answer']) || empty($ans['answer'])) {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}

					foreach ($ans['answer'] as $key => $val) {
						$ans['ans_desc'] .= ('空' . ($key + 1) . '："' . filter_string(strval($val)) . '" ； ');
					}
					unset($val);

					!empty($ans['ans_desc']) && $ans['ans_desc'] = mb_substr($ans['ans_desc'], 0, -1, 'UTF-8');
					BREAK;
				}
				case 5 : {
					if (!is_string($ans['answer']) || trim($ans['answer']) == '') {
						cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					}

					$ans['answer'] = filter_string($ans['answer']);
					$ans['ans_desc'] = $ans['answer'];
					BREAK;
				}
				default : {
					cls_resp::echo_err(cls_resp::$Busy, '提交出错了，请稍后再试！');
					BREAK;
				}
			}
		}
		unset($ans);

		parent::log('开始提交调研问卷');

		$args = array(
			'sui_id' => $sui_id,
			'answer_list' => $answer_list,
			'option_ids' => $opt_ids,
		);

		try {
			$result = g('survey') -> submit_survey($args);
		}catch(SCException $e) {
			parent::log('提交调研问卷失败，异常：[' . $e -> getMessage() . ']');
			cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
		}
		parent::log('提交调研问卷成功');
		cls_resp::echo_ok();
	}
	
	/**
	 * 检查提交的答案是否有未答的题目
	 * @param unknown_type $ques_list		//题目列表
	 * @param unknown_type $answer_List		//问题列表
	 */
	private function check_astrict($ques_list,$answer_list){
		$astrict_state = true;
		foreach($ques_list as $ques){
			if($ques['astrict']==1){
				$astrict_state = false;
				foreach($answer_list as $ans){
					if($ques['qs_id']==$ans['qs_id']){
						$astrict_state = true;
						break;
					}
				}
				if(!$astrict_state)			//如果找不到必答题对应的答案
					break;
			}
		}
		
		return $astrict_state;
	}
	
}

/* End of this file */