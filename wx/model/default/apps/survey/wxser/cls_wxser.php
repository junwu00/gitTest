<?php
/**
 * 调研
 * 
 * @author yangpz
 * @date 2015-03-30
 *
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『问卷调查』操作指引',
		'desc' => '支持多种题目设置，单选多选答题形式，实时收集来自员工的宝贵意见，数据分析直观明了，调研结果后台自动统计分析，为领导做决策提供真实的依据。',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=3&sn=8878ccaf31e6330c53e4ba7f3a6d47f2&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
}

// end of file