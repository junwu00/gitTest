<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'survey_detail_deal.css' => array(
		SYSTEM_APPS . 'survey/static/css/detail/index.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/square/theme.css'
	),
    'survey_detail_deal.js' => array(
        SYSTEM_ROOT . 'static/js/jweixin-1.1.0.js',
        SYSTEM_ROOT . 'static/js/icheck/icheck.min.js',
        SYSTEM_APPS . 'survey/static/js/detail/deal.js'
    ),
    'survey_list_index.js' => array(
        SYSTEM_ROOT . 'static/js/format.js',
        SYSTEM_APPS . 'survey/static/js/lists/index.js'
    )
);

return $arr;

/* End of this file */