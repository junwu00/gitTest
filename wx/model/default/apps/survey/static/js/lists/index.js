var proc_type = 0;
var page = 0;

$(document).ready(function() {
	init_type_change();
	load_lists();
	init_scroll_load('.container', '#load_div', load_lists);
});

//已答 未答 切换
function init_type_change() {
	$('.head ul li').each(function() {
		$(this).unbind('click').bind('click', function() {
			if($(this).hasClass('active'))
				return;
			proc_type = $(this).attr('t'); 	
			$('.head ul li').removeClass('active');
			$(this).addClass('active');
			page = 0;
			frame_obj.lock_screen('加载中...');
			load_lists();
			frame_obj.unlock_screen();
		});
	});
}

//加载调研列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.type = proc_type;
	data.page = page;
	$('#ajax-url').val($('#ajax_list_url').val());
	frame_obj.do_ajax_post(undefined, 'load', JSON.stringify(data), show_list);
}

//显示调研列表
function show_list(data) {
	if(data['errcode']!=0){
		frame_obj.alert(data['errmsg']);
		return;
	}
	var html ="";
	if(data['info'] == "" && page == 1){
		html = '<div id="rec-list"><div class="has-no-record"><img src="apps/common/static/images/nhlogo.png"><span>暂时没有调研</span></div></div>';
		$('#rec-list').html(html);
	}else{
		for(var i = 0; i < data['info'].length; i++){
			var survey = data['info'][i];
			var create_date = time2date(survey['create_time'],'yyyy-MM-dd');
			var state = '';
			if(survey['astrict']==1)
				state = '<span class="state">必填</span>';
			
			html +='<div onclick="show_detail('+survey['sui_id']+');" class="list-item">'+
					'<div class="date"><span class="time">'+create_date+'</span></div>'+
					'<div class="item-head">'+
					'<span class="type">'+survey['type_str']+'</span>'+
					'<span class="tatol">问题数：<i style="font-style:normal;color:#333;">'+survey['qs_count']+'</i></span>'+
					state+
					'</div>'+
					'<div class="item-body">'+
					'<div  class="name">'+
					'<span><img src="'+$('#app_static').val()+'images/pen.png" /></span>'+
					'<span>'+survey['title']+'</span>'+
					'</div>'+
					'<span class="go-icon"><a class="icon-angle-right"></a></span>'+
					'</div>'+
					'</div>';
			
			
//			html +='<div onclick="show_detail('+survey['sui_id']+')"; class="list-item"><div class="item-head"><span class="type">'+
//			survey['type_str']+
//			'</span>'+
//			'<span class="time">'+
//			create_date+
//			'</span>'+
//			'<span class="tatol">问题数：'+
//			survey['qs_count']+
//			'</span>'+
//			'</div><div class="item-body"><span class="name">'+
//			survey['title']+
//			'</span><span class="select-arrow select-icon"></span></div></div>';
		}
		if(page==1)
			$('#rec-list').html(html);
		else
			$('#rec-list').append(html);
	}
	scroll_load_complete('#load_div',data['info'].length);
	return;
}

function show_detail(code) {
	location.href = $('#app-url').val() + '&m=detail&id='+code;
}