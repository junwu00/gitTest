$(document).ready(function() {
/**	标题滚动时固定在顶部效果JS代码，暂时不启用
    var startPos = $('.top_title').offset().top;
    $('.container').scroll(function() {
        var p = $('.container').scrollTop();
        $('.top_title').css('position',((p) > startPos) ? 'fixed' : 'static');
        $('.top_title').css('top',((p) > startPos) ? '0px' : '');
        $('.top_title').css('display',((p) > startPos) ? 'block' : 'none');
    });
 */
	
	if($('#attend').val()==1){
		//已作答
		$('.remind_msg').html('你已经作答,感谢参与!');
		$('#main_content').css('display','none');
		$('#share_page').css('display','block');
		if($('#share').val() == 1){			//可分享
			if($('#is_inner').val()==2){
				$('.back_click').css('display','none');
			}
		}else if($('#share').val()==2){		//不可分享
			$('.share_button').css('display','none');
		}
	}else{
		//未作答
		$('#main_content').css('display','block');
		$('.question_item').each(function(){
			switch(Number($(this).attr('t'))){
			case 1:init_simple_choose($(this).attr('c'));break;
			case 2:init_choose($(this).attr('c'));break;
			case 3:init_score_item($(this).attr('d'));break;
			case 4:init_blank($(this).attr('d'));break;
			case 5:init_problem($(this).attr('d'));break;
			}
		});
		$('#apply').unbind('click').bind('click',function(){
			init_apply($(this));
		});
	}
	$('.show_share').click(function(){
		$('.share_img').css('display','block');
	});
	$('.share_img').click(function(){
		$(this).css('display','none');
	});
});

//页面加载完成展示
function show_container(){
	$('.load_img').css('display','none');
}


//初始化单选
function init_simple_choose(cla_name){
	// debugger
	$('.'+cla_name).iCheck({
	    radioClass: 'iradio_square-theme',
	    increaseArea: '30%' // optional
	 });
}
//初始化多选
function init_choose(cla_name){
	$('.'+cla_name).iCheck({
		checkboxClass: 'icheckbox_square-theme check',
	    increaseArea: '30%' // optional
	 });
}
//初始化评分
function init_score_item(id){
	init_score('#'+id);
}
//初始化填空
function init_blank(id){
	var text = $('#'+id).html();
	console.log("问题的text:",text);
	
	// var new_text = text.replace(/（）/g,"<span class='blank'>&nbsp;</span>");
	// 在问题后面添加span标签
	// οnkeyup='this.size=(this.value.length > 10 ? this.value.length:6);'
	var new_text = text.replace(/（）/g,"<span class='blank' id='blank_span'>&nbsp;<input type='text' id='blank_content' class='_input'  size='4' ></span>");
	console.log("添加span标签后的new_text:",new_text);
	

	//显示span元素
	$('#'+id).html(new_text);
	console.log("	显示span元素$('#'+id):",	$('#'+id));
	
	// 动态获取输入框的内容
	$("body").delegate("#blank_content","propertychange input",function(){
		console.log($(this).val());

			var $this = $(this);
			console.log("$this:",$this);
			var text_length = $this.val().length;//获取当前文本框的长度
			console.log("当前文本框的长度:",text_length);
			
			var current_width = parseInt(text_length) * 18;//该18是改变前的宽度除以当前字符串的长度,算出每个字符的长度
			console.log("每个字符的长度",current_width)
			$this.css("width",current_width+"px");

			$(this).parent().parent().attr('v',$(this).val());
			console.log();
			

	})


	// 遍历span元素
	$('._input').each(function(){
		// 移除click,绑定click
		$(this).unbind('click').bind('click',function(){
			// change start
			// 显示id为win-dlg的元素
			// $('#win-dlg').modal('show');
			// 获取横线的文本
			// var text = $(this).text();
			// $(" #test ").val()
			$('#blank_content').focus();
			var text = $(" #blank_content ").val();
			console.log("点击输入框之后的text:",text);
			
			if(text == "&nbsp;")
			// 获取id为#blank_content的标签的值
				$('#blank_content').val("");
			else
				// 
				$('#blank_content').val(text);
				// id为#blank_content的标签再次获得焦点
				$('#blank_content').focus();
				init_blank_input($(this));
		});
	});

		//propertychange监听input里面的字符变化,属性改变事件
		// $('._input').bind('input propertychange', function() {
		// 	var $this = $(this);
		// 	console.log($this);
		// 	var text_length = $this.val().length;//获取当前文本框的长度
		// 	var current_width = parseInt(text_length) * 16;//该16是改变前的宽度除以当前字符串的长度,算出每个字符的长度
		// 	console.log(current_width)
		// 	$this.css("width",current_width+"px");
		// }

}
//初始化问答
function init_problem(){}

//提交
function init_apply(btn){
	var data_ok = true;
	var msg ="您还有必答的题目没有作答哦！";
	$('.question_item').each(function(index){
		if( Number($(this).attr('t')) <=5 && Number($(this).attr('t')) >=1){
			if(!check_($(this),$(this).attr('t'))){
				msg = (index + 1) + "您还有必答的题目没有作答哦！";
				data_ok = false;
				return false;
			}
		} 
	});

	// $('.question_item').each(function(){
	// 	if(keyIndex == 0){
	// 		data_ok = false;
	// 	} 
	// });
	
	if(!data_ok){
		frame_obj.alert(msg);
		return;
	}
	var answer_list = [];
	
	$('.question_item').each(function(){
		var key = $(this).attr('i');
		switch(Number($(this).attr('t'))){
			case 1:
				var val =$('.' + $(this).attr('c') +'[type=radio]:checked').val();
				answer_list.push({'qs_id':key,'answer':val});
				break;
			case 2:
				var val = [];
				$('.' + $(this).attr('c') +'[type=checkbox]:checked').each(function(){
					val.push($(this).val());
				});
				answer_list.push({'qs_id':key,'answer':val});
				break;
			case 3:
				var val = $('#'+$(this).attr('d')).attr('score_'+$('#'+$(this).attr('d')).attr('v'));
				answer_list.push({'qs_id':key,'answer':val});
				break;
			case 4:
				var val = [];
				var all_null = true;
				$('#'+$(this).attr('d')).children('.blank').each(function(){
					var text = $(this).html();
					if(!(text.replace('&nbsp;',"") == ""))
						all_null = false;
					val.push(text.replace('&nbsp;',""));
				});
				if(all_null)
					answer_list.push({'qs_id':key});
				else
					answer_list.push({'qs_id':key,'answer':val});
				break;
			case 5:
				var val = $('#'+$(this).attr('d')).val();
				answer_list.push({'qs_id':key,'answer':val});
				break;
		}
	});
	
	var data ={
		'sui_id':$('#sui_id').val(),
		'answer_list':answer_list
	};
	$('#ajax-url').val($('#ajax_submit_url').val());
	frame_obj.do_ajax_post(
			btn,
			'',
			JSON.stringify(data),
			function(response){
				if(response['errcode']==0){
					if($('#share').val() == 1){			//可分享
						$('#main_content').css('display','none');
						$('#share_page').css('display','block');
						if($('#is_inner').val()==2){
							$('.back_click').css('display','none');
						}
					}else if($('#share').val()==2){		//不可分享
						$('#main_content').css('display','none');
						$('#share_page').css('display','block');
						$('.share_button').css('display','none');
					}
				}else{
					frame_obj.alert(response['errmsg']);
				}
			},undefined,undefined,undefined,'提交问卷中……'
	);
}

//检查数据
function check_(obj,type){
	if(Number(obj.attr('a'))==2)	//非必答
		return true;
	var state = true;
	switch(Number(type)){
		case 1: if($('.' + obj.attr('c') +'[type=radio]:checked').size()!=1)state = false;break;
		case 2: if($('.' + obj.attr('c') +'[type=checkbox]:checked').size()==0)state = false;break;
		case 3: if($('#' + obj.attr('d')).attr('v')=="" || !$('#' + obj.attr('d')).attr('v'))state = false;break;
		case 4: 
			$('#' + obj.attr('d')+'> .blank').each(function(){
				if($(this).html()=='&nbsp;')
					state = false;
			});
			break;
		case 5:if( $('#'+obj.attr('d')).val()=="")state = false;break;
	}
	return state;
}

function init_blank_input(blank){
	// 对于blank-ok元素,移除click,绑定click
	// $('#blank-ok').unbind('click').bind('click',function(e){

	$('#blank_content-ok').unbind('click').bind('click',function(e){
		// 阻止事件冒泡
		e.stopPropagation();
		// 删除blank_content元素开始和末尾的空格,不为空时
		if($.trim($('#blank_content').val())!="")
		// 显示出输入的内容
			blank.html($('#blank_content').val())
		else
		// 表示没有输入新内容,继续输入&nbsp;
			$('#blank_content').val("&nbsp;");
		// hide('#win-dlg');
	});
	// 只有浏览器的宽度发生改变时，才会触发resize函数,当调整浏览器窗口的大小时，发生 resize 事件。
	// $(window).resize(function(){
	// 	var ae =Number(blank.offset().top)-100;
	// 	$('body').animate({scrollTop:ae+'px'}, 1);
	// });
}

// 
function hide(id){
	$(window).unbind('resize');
	$(id).modal('hide');
}

function init_icheck(){
	$('.icheck_red').iCheck({
		checkboxClass: 'icheckbox_square-red check',
	    radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	 });
}

// start add start

function initStar(id){
	// 点击i元素执行函数
	$('.star_div p.star i').click(function(e){
		console.log("i的内容:",$('.star_div p.star i'));
		
		// 获取i元素
		var stars = $('.star_div p.star i');
		console.log("stars:",stars);
		
		var keyIndex = stars.length;
		console.log("五角星长度keyIndex:",keyIndex);

	
		
		for(var i = 0; i < stars.length; i++){
			if($(this).is($(stars[i]))){
				keyIndex = i;
				$('.star_div p.desc span.my-count').html(keyIndex+1+'分');
				break;
			}
		}

		for(var i = 0; i < stars.length; i++){
			if(i <= keyIndex){
				$(stars[i]).removeClass('icon-star-empty');
				$(stars[i]).addClass('icon-star');
				$(this).parent().parent().attr('v',i + 1);
			}else{
				$(stars[i]).removeClass('icon-star');
				$(stars[i]).addClass('icon-star-empty');
			}
		}
	});
}

// add end
function init_score(id){
	document.ondragstart=function(){
		return false;
	};
	document.oncontextmenu=function(){
		return false;
	};
	
	$(id).addClass('score_css');
	var btn_width = 20;		//滑块的高度
	var bottom_height = 8;	//滑动区域的高度
	var score_array = ['1分','2分','3分','4分','5分'];
	var btn_width_half = get_btn_width_half(bottom_height);
	var score_array_l = get_score_array_l(score_array);
	
	var str = '<div class="score-top">';
	str += '<div><span></span></div>';
	for(var i in score_array){
		str += '<div><span>'+ score_array[i] + '</span></div>';
	}
	str += '</div>';
	$(id).append(str);
	
	var strb = '<div class="score-bottom"><div class="score-bottom-mask"></div><div class="score-bottom-btn"></div></div>';
	$(id).append(strb);
	
	var max_height = 0;
	$(id+' .score-top > div').each(function() {
		$(this).append('<span></span>');
		var height = $(this).children('span:first-child').css('height');
		height = Number(height.substr(0, height.length-2));
		//height += 20;
		if (height > max_height) {
			max_height = height;
		}
	});
	
	var pre = (100 / (score_array_l + 1)) + '%';
	var score_bottom_width_pre = (100 / (score_array_l + 1) * (score_array_l - 1)) + '%';
	$(id +' .score-top > div').each(function() {
		$(this).css({height:max_height});
		$(this).css({width:pre});
		var top = max_height - 10;
		$(this).children('span:last-child').css({top:top});
	});
	
	$(id +' .score-top').css({height:max_height});
	$(id +' .score-bottom').css({left:pre});
	$(id +' .score-bottom').css({width:score_bottom_width_pre});
	$(id +' .score-bottom').css({height:bottom_height});
	$(id +' .score-bottom-mask').css({height:bottom_height});
	$(id +' .score-bottom-btn').css({width:btn_width});
	$(id +' .score-bottom-btn').css({height:btn_width});
	//$('#scoreId .score-bottom-btn').css({border-radius:12});//动态设置圆角
	$(id +' .score-bottom-btn').css({left:-(btn_width-bottom_height)/2});
	$(id +' .score-bottom-btn').css({top:-(btn_width-bottom_height)/2});
	
	bind_score_btn(id,score_array_l,btn_width);
}
function get_btn_width_half(btn_width){
	return btn_width / 2;
}

function get_score_array_l(score_array){
	return score_array.length;
}

function bind_score_btn(id,score_array_l,btn_width) {
	var start_x = 0;
	var end_x = 0;
	var idx = 0;
	
	$(id +' .score-bottom-btn')[0].addEventListener("touchstart", 	function(e) {
		e.preventDefault();				//阻止网页默认动作（即网页滚动）
		var touch = e.touches[0];
		start_x = Number(touch.pageX);
		//window.console.log('start: ' + start_x);
	});
	
	$(id +' .score-bottom-btn')[0].addEventListener('touchmove', function(e) {
		e.preventDefault();				//阻止网页默认动作（即网页滚动）
		var width = $(this).css('width');
		width = Number(width.substr(0, width.length-2));
		var min_left = 0;
		var max_left = $(this).parent().css('width');
		max_left = Number(max_left.substr(0, max_left.length-2)) - width;
		
		var touch = e.touches[0];
		end_x = Number(touch.pageX);
		var left = $(this).css('left');
		left = left.substr(0, left.length-2);
		var diff = end_x - start_x;
		left = Number(left) + diff;
		
		left = (left < min_left) ? min_left : left;
		left = (left > max_left) ? max_left : left;
		$(this).css({left:left});
		
		$(id +' .score-bottom-mask').css({left:left});
		var mask_width_p = $(this).parent().css('width');
		mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) - left;
		$(id +' .score-bottom-mask').css({width:mask_width_p});
		
		start_x = end_x;
	});
	
	$(id +' .score-bottom-btn')[0].addEventListener('touchend', function(e) {
		e.preventDefault();	
		var score_width = $(this).parent().css('width');
		score_width = Number(score_width.substr(0,score_width.length-2));
		score_width_v = score_width / (score_array_l - 1);
		var points = [0];
		for(var i = 1; i < score_array_l; i++){
			var temp_val = score_width_v * i;
			points.push(temp_val)
		}
		var left = $(this).css('left');
		left = Number(left.substr(0, left.length-2))+ $(this).width()/2;
		var min_diff = 9999;
		//window.console.log('end_x: ' + end_x);
		for (var i in points) {
			var diff = Math.abs(left - points[i]);
			if (min_diff > diff) {
				idx = i;
				min_diff = diff;
			}
		}
		left = points[idx] - (btn_width / 2);
		$(this).css({left:left});
		
		$(id +' .score-bottom-mask').css({left:points[idx]});
		$(id).attr('v',Number(idx)+1);
		var mask_width_p = $(this).parent().css('width');
		mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) - points[idx];
		$(id +' .score-bottom-mask').css({width:mask_width_p});
	});
	
	
	$(id +' .score-bottom').bind('click', function(e) {
		var score_width = $(this).css('width');
		score_width = Number(score_width.substr(0,score_width.length-2));
		score_width_v = score_width / (score_array_l - 1);
		var points = [0];
		for(var i = 1; i < score_array_l; i++){
			var temp_val = score_width_v * i;
			points.push(temp_val)
		}
		var left = e.pageX - $(this).offset().left;
		//left = left.substr(0, left.length-2);
		var min_diff = 9999;
		//window.console.log('end_x: ' + end_x);
		for (var i in points) {
			var diff = Math.abs(left - points[i]);
			if (min_diff > diff) {
				idx = i;
				min_diff = diff;
			}
		}
		left = points[idx] - (btn_width / 2);
		$(id +' .score-bottom-btn').css({left:left});
		
		$(id +' .score-bottom-mask').css({left:points[idx]});
		$(id).attr('v',Number(idx)+1);
		var mask_width_p = $(this).css('width');
		mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) - points[idx];
		$(id +' .score-bottom-mask').css({width:mask_width_p});
	});
	
}