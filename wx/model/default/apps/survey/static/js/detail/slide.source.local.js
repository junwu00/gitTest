/*
var option = {};
option.defValue = '5分';
option.scoreArray = ['1分','2分','3分','4分','5分'];
$('#scoreId_1').ssource(option);

var value = $('#scoreId_1').ssource('get_val');
*/
(function($){
	var SSource = function(element, options) {
		var that = this;
		this.score_value = options.scoreArray[0];
		this.element = $(element);
		this.opt = options;
		this.init(element,options);
	};
	
	SSource.prototype = {
		constructor: SSource,
		init:function(element, options){
			var elementId = element.getAttribute('id');
			document.ondragstart=function(){return false;}; 
			document.oncontextmenu=function(){return false;};
			
			var btn_width = options.btnWidth;
			var score_array = options.scoreArray;
			var btn_width_half = get_btn_width_half(btn_width);
			var score_array_l = get_score_array_l(score_array);
			
			var str = '<div class="score-top">';
			str += '<div><span></span></div>';
			for(var i in score_array){
				str += '<div><span>'+ score_array[i] + '</span></div>';
			}
			str += '</div>';
			$('#'+elementId).append(str);
			
			var strb = '<div class="score-bottom"><div class="score-bottom-mask"></div><div class="score-bottom-btn"></div></div>';
			$('#'+elementId).append(strb);
			
			var max_height = 0;
			$('#'+elementId+' .score-top > div').each(function() {
				$(this).append('<span></span>');
				var height = $(this).children('span:first-child').css('height');
				height = Number(height.substr(0, height.length-2));
				// height += 20;
				if (height > max_height) {
					max_height = height;
				}
			});
			
			var pre = (100 / (score_array_l + 1)) + '%';
			var score_bottom_width_pre = (100 / (score_array_l + 1) * (score_array_l - 1)) + '%';
			$('#'+elementId+' .score-top > div').each(function() {
				$(this).css({height:max_height});
				$(this).css({width:pre});
				var top = max_height - 10;
				$(this).children('span:last-child').css({top:top});
			});
			
			$('#'+elementId+' .score-top').css({height:max_height});
			$('#'+elementId+' .score-bottom').css({left:pre});
			$('#'+elementId+' .score-bottom').css({width:score_bottom_width_pre});
			$('#'+elementId+' .score-bottom').css({height:btn_width});
			$('#'+elementId+' .score-bottom-mask').css({height:btn_width});
			$('#'+elementId+' .score-bottom-btn').css({width:btn_width});
			$('#'+elementId+' .score-bottom-btn').css({height:btn_width});
			// $('#scoreId_1
			// .score-bottom-btn').css({border-radius:12});//动态设置圆角
			$('#'+elementId+' .score-bottom-btn').css({left:-btn_width_half});
			
			this.bind_score_btn(score_array_l,btn_width,elementId);
			this.set_def_val(btn_width,elementId);
		},
		
		get_val:function(){
			return this.score_value;
		},
		
		set_def_val:function(btn_width,elementId){
			if(!this.opt.defValue || this.opt.defValue.trim() == ''){
				return;
			}
			var scoreArray = this.opt.scoreArray;
			var score_array_l = this.opt.scoreArray.length;
			var idx = 0;
			var defValue = this.opt.defValue;
			for(var i = 0; i < score_array_l; i++ ){
				if(defValue == scoreArray[i]){
					idx = i;
					break;
				}
			}
			var scoreBtn = $('#'+elementId+' .score-bottom-btn')[0];
			var score_width = $(scoreBtn).parent().css('width');
			score_width = Number(score_width.substr(0,score_width.length-2));
			var score_width_v = score_width / (score_array_l - 1);
			var points = [0];
			for(var i = 1; i < score_array_l; i++){
				var temp_val = score_width_v * i;
				points.push(temp_val)
			}
			var left = $(scoreBtn).css('left');
			left = left.substr(0, left.length-2);
			left = points[idx] - (btn_width / 2);
			$(scoreBtn).css({left:left});
			$('#'+elementId+' .score-bottom-mask').css({left:points[idx]});
			var mask_width_p = $(scoreBtn).parent().css('width');
			mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) + 1 - points[idx];
			$('#'+elementId+' .score-bottom-mask').css({width:mask_width_p});
			this.score_value = this.opt.scoreArray[idx];
		},
		
		get_btn_width_half:function(btn_width){
			return btn_width / 2;
		},
		
		get_score_array_l:function(score_array){
			return score_array.length;
		},
		
		bind_score_btn:function(score_array_l,btn_width,elementId) {
			var start_x = 0;
			var end_x = 0;
			var idx = 0;
			var that = this;
			
			$('#'+elementId+' .score-bottom-btn')[0].addEventListener("touchstart", 	function(e) {
				e.preventDefault();// 阻止网页默认动作（即网页滚动）
				var touch = e.touches[0];
				start_x = Number(touch.pageX);
				// window.console.log('start: ' + start_x);
			});
			
			$('#'+elementId+' .score-bottom-btn')[0].addEventListener('touchmove', function(e) {
				e.preventDefault();// 阻止网页默认动作（即网页滚动）
				var width = $(this).css('width');
				width = Number(width.substr(0, width.length-2));
				var min_left = 0;
				var max_left = $(this).parent().css('width');
				max_left = Number(max_left.substr(0, max_left.length-2)) - width;
				var touch = e.touches[0];
				end_x = Number(touch.pageX);
				var left = $(this).css('left');
				left = left.substr(0, left.length-2);
				var diff = end_x - start_x;
				left = Number(left) + diff;
				
				left = (left < min_left) ? min_left : left;
				left = (left > max_left) ? max_left : left;
				$(this).css({left:left});
				
				$('#'+elementId+' .score-bottom-mask').css({left:left});
				var mask_width_p = $(this).parent().css('width');
				mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) + 1 - left;
				$('#'+elementId+' .score-bottom-mask').css({width:mask_width_p});
				
				start_x = end_x;
			});
			
			$('#'+elementId+' .score-bottom-btn')[0].addEventListener('touchend', function(e) {
				var score_width = $(this).parent().css('width');
				score_width = Number(score_width.substr(0,score_width.length-2));
				score_width_v = score_width / (score_array_l - 1);
				var points = [0];
				for(var i = 1; i < score_array_l; i++){
					var temp_val = score_width_v * i;
					points.push(temp_val)
				}
				var left = $(this).css('left');
				left = left.substr(0, left.length-2);
				var min_diff = 9999;
				// window.console.log('end_x: ' + end_x);
				for (var i in points) {
					var diff = Math.abs(left - points[i]);
					if (min_diff > diff) {
						idx = i;
						min_diff = diff;
					}
				}
				left = points[idx] - (btn_width / 2);
				$(this).css({left:left});
				//console.log(left);
				$('#'+elementId+' .score-bottom-mask').css({left:points[idx]});
				var mask_width_p = $(this).parent().css('width');
				mask_width_p = Number(mask_width_p.substr(0, mask_width_p.length-2)) + 1 - points[idx];
				$('#'+elementId+' .score-bottom-mask').css({width:mask_width_p});
				//console.log('points[idx]:' + points[idx]);
				//console.log('mask_width_p:' + mask_width_p);
				that.score_value = that.opt.scoreArray[idx];
			});
		}
	};
	
	$.fn.ssource = function(option) {
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function() {
			var $this = $(this);
			var data = $this.data('source');
			var options = typeof option == 'object' && option;
			if (!data) {
				$this.data('source', (data = new SSource(this, $.extend({}, $.fn.ssource.def, options))));
			}
			if (typeof option == 'string' && typeof data[option] == 'function') {
				internal_return = data[option].apply(data, args);
				if (internal_return !== undefined) {
					return false;
				}
			}
		});
		if (internal_return !== undefined){
			return internal_return;
		}else {
			return this;
		}
	};
	
	$.fn.ssource.def ={
		'debug': false,
		'btnWidth':25,
	};
	
	$.fn.ssource.Constructor = SSource;
})(jQuery);
