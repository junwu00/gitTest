<?php
/**
 * 审批流程通用父类
 * @author yangpz
 * @date 2014-12-09
 *
 */
class abs_proc extends abs_app_base {
	/** 审批流程ID，对应proc_setting表 */
	protected $proc_id;
	/** 审批流程名称*/
	protected $proc_name;
	/** 自定义审批流程id数组 */
	protected $proc_id_list;
	
	/** 审批中 0 */
	protected static $StateWait = 0;
	/** 同意 1 */
	protected static $StateAgree = 1;
	/** 驳回 2 */
	protected static $StateBack = 2;
	/** 撤销 3 */
	protected static $StateCancel = 3;
	/** 催办 4 */
	protected static $StateWarn = 4;
	
	/** 使用的货币类型 */
	protected static $MoneyType = '￥';
	
	public function __construct($app_name) {
		$app_conf = include(SYSTEM_APPS.$app_name.'/config.php');
		isset($app_conf['proc_id']) && $this -> proc_id = $app_conf['proc_id'];
		isset($app_conf['proc_name']) && $this -> proc_name = $app_conf['proc_name'];
		g('smarty') -> assign('money_type', self::$MoneyType);
				
		parent::__construct($app_name);
	}
	
	//--------------------------------------protected--------------------------------

	/**
	 * 获取下一节点的审批人列表
	 * @param unknown_type $proc_record_id
	 */
	protected function get_next_apprval_list() {
		try {
			$data = parent::get_post_data(array('code', 'ver'));
			$rec = g('proc_record') -> get_by_code($data['code'], $_SESSION[SESSION_VISIT_COM_ID], $this -> proc_id);
			if (!$rec) {
				throw new SCException('找不到申请记录');
			}
			
			$curr_user = $rec['to_user_id'];	//当前审批人
			$next_step = $rec['step'] + 1;		//当前步骤
			$proc = g('proc_proc') -> get_by_com_proc($_SESSION[SESSION_VISIT_COM_ID], $this -> proc_id, $data['ver'], $next_step);
			if ($proc) {	//存在下一审批人
				$list = g('user') -> get_approval_user_list($_SESSION[SESSION_VISIT_COM_ID], $curr_user, $proc['recive_role'], $proc['recive_user_id']);
				log_write('lists:'.json_encode($list));
				$users = g('user') -> get_by_ids($list, 'id, name, pic_url');
				cls_resp::echo_ok(cls_resp::$OK, 'info', $users);
				
			} else {		//当前已是最后一级审批人
				cls_resp::echo_ok(cls_resp::$OK, 'info', array());
				
			}
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取申请时的审批人列表
	 */
	protected function get_apply_approval_list() {
		try {
			$data = parent::get_post_data(array('ver'));
			
			$step = 1;
			$send_user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$proc = g('proc_proc') -> get_by_com_proc($_SESSION[SESSION_VISIT_COM_ID], $this -> proc_id, $data['ver'], $step);
			if (!$proc) {
				throw new SCException('未找到审批人配置');
			}
			$list = g('user') -> get_approval_user_list($_SESSION[SESSION_VISIT_COM_ID], $send_user_id, $proc['recive_role'], $proc['recive_user_id']);
			$users = g('user') -> get_by_ids($list, 'id, name, pic_url');
			cls_resp::echo_ok(cls_resp::$OK, 'info', $users);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 获取所有自定义流程审批列表（审批人使用）
	 */
	protected function get_custom_list() {
		$procs = g('proc_setting') -> list_custom($_SESSION[SESSION_VISIT_COM_ID]);
		$this -> proc_id_list = array();
		if ($procs) {
			foreach ($procs as $proc) {
				array_push($this -> proc_id_list, $proc['id']);
			}
			unset($proc);
			return $procs;
			
		} else {
			return array();
		}
	}
	
	/**
	 * 获取员工可见自定义流程审批列表(申请人使用)
	 */
	protected function get_user_custom_list() {
		$procs = g('proc_setting') -> list_user_custom($_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID]);
		$this -> proc_id_list = array();
		if ($procs) {
			foreach ($procs as $proc) {
				array_push($this -> proc_id_list, $proc['id']);
			}
			unset($proc);
			return $procs;
		} else {
			return array();
		}
	}
	
	/**
	 * 初始化自定义流程审批信息
	 */
	protected function init_custom_proc_info($proc_id=NULL) {
		if (is_null($proc_id)) {
			$proc_id = get_var_get('pid');	
		}
		$proc = g('proc_setting') -> get_custom_by_id($_SESSION[SESSION_VISIT_COM_ID], $proc_id);
		if (!$proc) {
			return FALSE;
		}
		
		$this -> proc_id = $proc_id;
		$this -> proc_name = $proc['name'];
		return $proc;
	}
	
	/**
	 * 加载审批列表(单个审批类型)
	 */	
	protected function load_page($page_size=20) {
		try {
			$data = parent::get_post_data(array('type', 'page'));
			$page = (!empty($data['page']) && $data['page'] > 0) ? $data['page'] : 1;
			$type = $data['type'];
			switch ($type) {
				case 0:	//审批中
					$list = g('proc_record') -> get_wait_list($this -> proc_id, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 1:	//同意
					$list = g('proc_record') -> get_agress_list($this -> proc_id, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 2:	//驳回
					$list = g('proc_record') -> get_back_and_cancel($this -> proc_id, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
			}
			cls_resp::echo_ok(cls_resp::$OK, 'info', $list);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}

	/**
	 * 加载审批列表(多个审批类型)
	 */	
	protected function load_page_mulit($page_size=20) {
		try {
			$data = parent::get_post_data(array('type', 'page'));
			$page = (!empty($data['page']) && $data['page'] > 0) ? $data['page'] : 1;
			$type = $data['type'];
			switch ($type) {
				case 0:	//审批中
					$list = g('proc_record') -> get_wait_list_mulit($this -> proc_id_list, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 1:	//同意
					$list = g('proc_record') -> get_agress_list_mulit($this -> proc_id_list, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 2:	//驳回
					$list = g('proc_record') -> get_back_and_cancel_mulit($this -> proc_id_list, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 3:	//未处理
					$list = g('proc_record') -> get_undeal_list_mulit($this -> proc_id_list, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
					
				case 4:	//已处理
					$list = g('proc_record') -> get_deal_list_mulit($this -> proc_id_list, $_SESSION[SESSION_VISIT_COM_ID], $_SESSION[SESSION_VISIT_USER_ID], $page, $page_size);
					break;
			}
			cls_resp::echo_ok(cls_resp::$OK, 'info', $list);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	//--------------------------------------内部实现--------------------------------
	
}

// end of file