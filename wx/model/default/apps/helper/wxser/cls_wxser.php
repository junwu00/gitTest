<?php
/**
 * 新闻公告
 * 
 * @author yangpz
 * @date 2015-06-26
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『通知公告』操作指引',
		'desc' => '欢迎使用通知公告应用，在这里你可以随时随地查看公司的新闻、动态、通知等，不再错过任何与你有关的权益，快速查看下吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=4&sn=74d64156ea71da9109d363aa2d51eadb&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file