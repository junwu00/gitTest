var page = 0;
var p_id = 0;

$(document).ready(function() {
	$('#comment-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
	init_scroll_load('.container', '#comment-list', load_lists);
	$('#powerby').remove();
});

//加载评论列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.page = page;
	data.news = $('#news-id').val();
	data.p_id = p_id;
	is_scroll_loading = true;
	$('#ajax-url').val(comment_list_url);
	frame_obj.do_ajax_post(undefined, 'comment_list', JSON.stringify(data), show_list);
}

var curr_comment_cnt = 0;	//记录当前显示的评论数
//显示评论列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info.data;
	var total = Number(data.info.count || 0);
	scroll_load_complete('#comment-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#comment-list').html('');
		$('#comment-count').html(data.info.count);
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		html += '<div class="comment-item" onclick="to_reply('  + item['id'] + ')">' +
			'<img src="' + item.pic_url + '" onerror="this.src=\'static/image/face.png\'">' +
			'<span class="body">' +
				'<span>' + item.name + '<span class="comment-lift">' + (total-(curr_comment_cnt++)) + '楼</span></span><br>' +
				'<span>' + item.content + '</span>' +
			'</span>' +
			'<span class="reply">' +
				'<span class="time">' + frame_obj.recent_time(item.create_time) + '</span>' + 
				'回复(' + item.reply_cnt + ')' +
			'</span>' +
		'</div>';
	}
	$('#comment-list').append(html);
	
	if ($('.comment-item').length == 0) {
		has_no_record('#comment-list');
	}
}

//跳转到回复页面
function to_reply(comm_id) {
	location.href = reply_url + '&comm=' + comm_id;
}

//发表评论
function do_comment(that) {
	$('#ajax-url').val(comment_url);
	var data = new Object();
	data.p_id = p_id;
	data.news = $('#news-id').val();
	data.comment = $('#comment-text').val();
	
	if (!check_data(data.comment, 'notnull')) {
		frame_obj.tips('请填写评论内容');
		return false;
	}

	$(that).attr('style', 'background-color: #f4f4f4 !important');
	frame_obj.do_ajax_post($(that), 'add_comment', data, function(resp) {
		$(that).attr('style', 'background-color: #ffffff !important');
		if (resp.errcode == 0) {
			var comment_count = $('#comment-count').html();
			$('#comment-count').html(Number(comment_count)+1)
			$('#comment-text').val('');
			var count = $('.comment-item').length;
			var html = '<div class="comment-item">' +
				'<img src="' + $('#user-pic').val() + '" onerror="this.src=\'static/image/face.png\'">' +
				'<span class="body">' + 
					'<span>' + $('#user-name').val() + '<span class="comment-lift">' + ($('#comment-count').html()) + '楼</span></span><br>' + 
					'<span>' + data.comment + '</span>' + 
				'</span>' + 
				'<span class="reply">' +
					'<span class="time">刚刚</span>' +
					'回复(0)' +
				'</span>' +
			'</div>';
			$('.container').animate({scrollTop: 0}, 'normal', function() {
				setTimeout(function() {
					if (count == 0) {				//没有记录，直接加入一条
						$('#comment-list').html(html);
						
					} else {						//有记录，直接加入一条
						$('#comment-list .comment-item:first-child').before(html);
					}
				}, 500);
			});
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}