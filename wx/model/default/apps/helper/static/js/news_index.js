var search_params = new Object();	//查找参数

$(document).ready(function () {
	search_params.page = 1;
	search_params.offset = $('.list-item').length;
	search_params.type = type;
	search_params.title = '';
	search_params.source = '';
	search_params.sort = 'public_time';
	search_params.order = 'desc';
	search_params.limit = 20;
	search_params.start = 0;
	search_params.end = 0;
	search_params.road = $('.head ul li.active').attr('data-state');

	init_scroll_load('.container', '#rec-list', load_lists);
	init_search();

	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
	// 搜索框失焦
	searchBlur()

});

//按标题搜索
function init_search() {
	$("#search_input").unbind('input').bind('input', function () {
		$('#rec-list').html('');
		search_params.page = 0;
		search_params.title = $('#search_input').val();
		search_params.order = $('#sort').attr('data-sort') == 'down' ? 'desc' : 'asc';
		load_lists();
	});
}

//加载列表
function load_lists(btn) {
	search_params.page += 1;
	search_params.offset = $('.list-item').length;

	$('#ajax-url').val(list_url);
	frame_obj.do_ajax_post(btn, '', JSON.stringify(search_params), show_list);
}

//显示列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.tips(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  

	var html = '';
	for (var i in list) {
		var item = list[i];

		html += '<div class="list-item" onclick="show_detail(\'' + content_url + '&content=' + item.id + '\')">' +
			'<div class="item-body">' +
			'<img src="' + media_url + item.cover_url + '" alt="" onerror="this.src=\'' + def_img_url + '\'">' +
			'<div class="item-detail">' +
			'<p><b>' + item.title + '</b></p>' +
			'<p>' +
			'<span class="time">' +
			time2date(item.public_time) +
			'</span>' +
			'</p>' +
			'</div>' +
			'</div>' +
			'<i class="icon-angle-right"></i>' +
			'</div>';
	}
	$('#rec-list').append(html);

	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

function show_detail(href) {
	location.href = href;
}

//排发布时间升序/降序
function sort_div(obj) {
	var sort = $(obj).attr('data-sort');
	if (sort == 'down') {	//当前为时间降序
		$(obj).find('span').removeClass('glyphicon-arrow-down');
		$(obj).find('span').addClass('glyphicon-arrow-up');
		$(obj).attr('data-sort', 'up');
		search_params.order = 'asc';

	} else {				//当前为时间升序
		$(obj).find('span').removeClass('glyphicon-arrow-up');
		$(obj).find('span').addClass('glyphicon-arrow-down');
		$(obj).attr('data-sort', 'down');
		search_params.order = 'desc';
	}

	page = 0;
	$('#rec-list').html('');
	$('#rec-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
}

function search() {
	$('.input_search').css({ 'justify-content': 'end' });
	$('.input_search input').css({ 'width': 'calc(100% - 30px)' });
	$('.input_search input').focus();
}

// 搜索框失焦
function searchBlur() {
	$('.input_search input').blur(function () {
		if ($('.input_search input').val() === '') {
			$('.input_search').css({ 'justify-content': 'center' });
			$('.input_search input').css({ 'width': '13%' });
		} else {
			$('.input_search input').css({ 'width': 'calc(100% - 30px)', 'justify-content': 'end' });
		}
	})
}

// 清空搜索框
function clearAll() {
	$('.input_search input').val('');
	$('.clearAll').css({ 'display': 'none' });
	window.location.reload();
}

//监听搜索框值变化
function onpropertychange() {
	$(".input_search input").bind("input propertychange", function (event) {
		if ($('.input_search input').val() !== '') {
			$('.clearAll').css({ 'display': 'block' });
		} else {
			$('.clearAll').css({ 'display': 'none' });
		}
	})
}