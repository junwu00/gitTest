$(document).ready(function () {
	show_img();
	midImg();
});



//查看/放大图片
function show_img() {
	$('.content-main img').each(function () {
		$(this).unbind('click').bind('click', function () {
			var url = $(this).attr('src');
			jssdk_obj.preview_img(url, [url]);
		});
	});
}

//显示附件下载菜单
function showFileDownloadMenu(that) {
	$('#new-bottom-menu').attr('data-file', $(that).attr('data-file'));	//标识当前操作的文件

	var fileSize = $(that).find('.file-size').html();
	var bottom = 0;
	if ($(that).hasClass('file-type-img')) {	//图片类型
		$('#img-general-download').removeClass('hide');
		$('#img-origin-download').removeClass('hide');
		$('#file-download').addClass('hide');
		bottom = -160;
		$('#img-general-download span').html(fileSize);

	} else {									//其他文件
		$('#img-general-download').addClass('hide');
		$('#img-origin-download').addClass('hide');
		$('#file-download').removeClass('hide');
		bottom = -120;
		$('#file-download span').html(fileSize);
	}

	$('#new-bottom-menu').css({ bottom: bottom });
	$('#new-bottom-menu').attr('data-bottom', -160);
	$('#new-bottom-menu-wrap').css({ display: 'block' });
	$('#new-bottom-menu').animate({ bottom: 0 });
	$('html, body').css({ overflow: 'hidden' });
}

//隐藏附件下载菜单
function hideFileDownloadMenu(that) {
	$('#new-bottom-menu').animate({ bottom: $('#new-bottom-menu').attr('data-bottom') }, 'normal', function () {
		$('#new-bottom-menu-wrap').css({ display: 'none' });
		$('html, body').css({ overflow: 'auto' });
	});
}

//预览文件
function previewFile(that) {
	var file = $(that).parents('#new-bottom-menu').attr('data-file');
	file = $('.file-item[data-file=' + file + ']');
	var ext = $(file).attr('data-ext');
	var key = $(file).attr('data-key');
	var name = $(file).attr('data-name');
	if (MS_document_pre.is_pre(ext)) {
		if (platId == 1 && (ext != 'ppt' && ext != 'pptx')) {
			iosShowPre(that);	
		} else {
			MS_document_pre.init_pre(ext, key, name);
		}

	} else if (image_pre.is_pre(ext)) {
		image_pre.init_pre(ext, key);

	} else {
		frame_obj.tips('暂不支持该类型文件的预览');
	}
}

//IOS系统，除PPT外其他office文件走自搭建预览服务
function iosShowPre(that) {
	var file = $(that).parents('#new-bottom-menu').attr('data-file');
	var data = new Object();
	data.type = $('.file-item[data-file=' + file + ']').attr('data-type');
	data.content = content;
	data.file = file;
	data.is_push = 0;
	data.is_origin = 0;
	frame_obj.lock_screen('生成预览中...');
	$.ajax({
		url: downloadUrl+'&free=1',
		type: 'post',
		data: { data: data },
		dataType: 'json',
		success: function (data) {
			frame_obj.unlock_screen();
			if (data.errcode != 0) {
				frame_obj.tips(data.errmsg);
				return;
			}

			if (data.info && data.info.url) {
				location.href = data.info.url;
			}
		},
		error: function (data, status, e) {
			frame_obj.unlock_screen();
			frame_obj.tips('预览失败');
		}
	});
}

//下载附件
function downloadFile(that, isOrigin) {
	var file = $(that).parents('#new-bottom-menu').attr('data-file');
	var data = new Object();
	data.type = $('.file-item[data-file=' + file + ']').attr('data-type');
	data.content = content;
	data.file = file;
	data.is_push = platId == 1 ? 1 : 0;
	data.is_origin = isOrigin;
	frame_obj.lock_screen('下载中...');
	$.ajax({
		url: downloadUrl+'&free=1',
		type: 'post',
		data: { data: data },
		dataType: 'json',
		success: function (data) {
			frame_obj.unlock_screen();
			if (data.errcode != 0) {
				frame_obj.tips(data.errmsg);
				return;
			}

			if (data.info && data.info.url) {
				location.href = data.info.url;
				return;
			}

			frame_obj.tips('附件已成功推送到信息框！');
			hideFileDownloadMenu();
		},
		error: function (data, status, e) {
			frame_obj.unlock_screen();
			frame_obj.tips('下载失败');
			hideFileDownloadMenu();
		}
	});
}

//跳转到回复页面
function to_reply(comm_id) {
	location.href = reply_url + '&comm=' + comm_id;
}

//发表评论
function do_comment(that) {
	$('#ajax-url').val(comment_url);
	var data = new Object();
	data.p_id = 0;
	data.news = $(that).attr('data-news');
	data.comment = $('#comment-text').val();

	if (!check_data(data.comment, 'notnull')) {
		frame_obj.tips('请填写评论内容');
		return false;
	}

	$(that).attr('style', 'background-color: #f4f4f4 !important');
	frame_obj.do_ajax_post($(that), 'add_comment', data, function (resp) {
		$(that).attr('style', 'background-color: #ffffff !important');
		if (resp.errcode == 0) {
			$('#comment-text').val('');
			var count = $('.comment-item').length;
			var html = '<div class="comment-item" onclick="to_reply(' + resp.info.comm + ');">' +
				'<img src="' + $('#user-pic').val() + '" onerror="this.src=\'static/image/face.png\'">' +
				'<span class="body">' +
				'<span>' + $('#user-name').val() + '</span><br>' +
				'<span>' + data.comment + '</span>' +
				'</span>' +
				'<span class="reply">' +
				'<span class="time">刚刚</span>' +
				'<span class="number">回复(0)</span>' +
				'</span>' +
				'</div>';
			if (count == 0) {				//没有记录，直接加入一条
				$('#comment-list').html(html);

			} else if (count < 3) {			//有记录，但小于三条，直接加入一条
				$('#comment-list .comment-item:first-child').before(html);

			} else {						//超过三条，增加后要删除最后一条
				$('#comment-list .comment-item:first-child').before(html);
				$('#comment-list .comment-item:last-child').remove();
			}
			var comment_count = $('.comment-count').html();
			$('.comment-count').html(Number(comment_count) + 1);

			if (Number(comment_count) + 1 > 3) {	//评论后总数超过3条，显示"更多评论"
				$('#comment-more').removeClass('hide');
			}

		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}

// 缩略图
function midImg() {
	$('.file-list div').each(function (index) {
		if ($(this).attr('data-ext') == 'jpeg' || $(this).attr('data-ext') == 'png'
			|| $(this).attr('data-ext') == 'jpg') {
			var urlImg = $(this).attr('data-img')
			//console.log(urlImg, 'url');
			var imgDom = '<div class="imgmid" id="mid' + index + '"/>'
			$(this).append(imgDom)
			$('#mid' + index).addClass('abc' + index).css('background-image', 'url(' + urlImg + ')')
			// $('.imgmid').css('background-image', 'url(' + urlImg + ')')
		}
	})
}