var page = 0;

$(document).ready(function() {
	var main_comm_time = $('#comments-head .time').attr('data-time');
	main_comm_time = frame_obj.recent_time(main_comm_time);
	$('#comments-head .time').html(main_comm_time);
	$('#comment-list').append('<div id="loading"><img src="apps/common/static/images/wait.gif">加载中...</div>');
	load_lists();
	init_scroll_load('.container', '#comment-list', load_lists);
	$('#powerby').remove();
});

//加载回复列表
function load_lists() {
	page += 1;
	var data = new Object();
	data.page = page;
	data.news = $('#news-id').val();
	data.p_id = $('#comm-id').val();
	is_scroll_loading = true;
	$('#ajax-url').val(comment_list_url);
	frame_obj.do_ajax_post(undefined, 'comment_detail', JSON.stringify(data), show_list);
}

//显示回复列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info.data;
	scroll_load_complete('#comment-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#comment-list').html('');
		$('#comment-count').html(data.info.count);
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		html += '<div class="comment-item" onclick="pre_reply(\'' + item.name + '\')">' +
			'<img src="' + item.pic_url + '" onerror="this.src=\'static/image/face.png\'">' +
			'<span class="body">' +
				'<span>' + item.name + '</span><br>' +
				'<span>' + item.content + '</span>' +
			'</span>' +
			'<span class="time">' + frame_obj.recent_time(item.create_time) + '</span>' +
		'</div>';
	}
	$('#comment-list').append(html);
	
	if ($('.comment-item').length == 0) {
		has_no_record('#comment-list');
	}
}

//显示：回复xxx
function pre_reply(user_name, is_empty) {
	if (is_empty) {
		$('#comment-text').val('');
	} else {
		$('#comment-text').val('回复 ' + user_name + '：');
		$('#comment-text').focus();
	}
}

//回复评论
function do_reply(that) {
	$('#ajax-url').val(comment_url);
	var data = new Object();
	data.p_id = $('#comm-id').val();
	data.news = $('#news-id').val();
	data.comment = $('#comment-text').val();
	
	if (!check_data(data.comment, 'notnull')) {
		frame_obj.tips('请填写评论内容');
		return false;
	}

	$(that).attr('style', 'background-color: #f4f4f4 !important');
	frame_obj.do_ajax_post($(that), 'add_comment', data, function(resp) {
		$(that).attr('style', 'background-color: #ffffff !important');
		if (resp.errcode == 0) {
			$('#comment-text').val('');
			var count = $('.comment-item').length;
			var html = '<div class="comment-item">' +
				'<img src="' + $('#user-pic').val() + '" onerror="this.src=\'static/image/face.png\'">' +
				'<span class="body">' + 
					'<span>' + $('#user-name').val() + '</span><br>' + 
					'<span>' + data.comment + '</span>' + 
				'</span>' + 
				'<span class="time">刚刚</span>' + 
			'</div>';
			var comment_count = $('#comment-count').html();
			$('#comment-count').html(Number(comment_count)+1)
			$('.container').animate({scrollTop: 0}, 'normal', function() {
				setTimeout(function() {
					if (count == 0) {				//没有记录，直接加入一条
						$('#comment-list').html(html);
						
					} else {						//有记录，直接加入一条
						$('#comment-list .comment-item:first-child').before(html);
					}
				}, 500);
			});
			
		} else {
			frame_obj.tips(resp.errmsg);
		}
	});
}