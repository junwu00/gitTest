<?php
/**
 * 信息发布查看
 *
 * @author yangpz
 * @create 2015-06-26
 */
class cls_news extends abs_app_base {
	private $com_id;
	private $user_id;
	private $corp_url;
	
	private $mod_news_type = 'news_type';
	private $mod_news_content = 'news_content';
	
	/**
	 * 初始化
	 */
	public function __construct() {
		parent::__construct('helper');
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
		$this -> corp_url = $_SESSION[SESSION_VISIT_CORP_URL];
	}
	
	/**
	 * 文章列表
	 */
	public function index(){
	    if($this->is_parent()){
            cls_resp::show_warn_page('程序员正在开发中~<br>如需查看通知历史，请先移步到学校通知的消息中查看');
        }
		$type_name = $this -> verify_type();
		
		$type = get_var_get('type');
		g('smarty') -> assign('type', $type);
		g('smarty') -> assign('list_url', SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=news&a=lists&type='.$type.'&corpurl='.$this -> corp_url);	//分页获取数据
		g('smarty') -> assign('content_url', SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=content&a=index&type='.$type);
		g('smarty') -> assign('page_url', SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=news&a=index&type='.$type);
//		$source_list = g($this -> mod_news_content) -> list_source($this -> com_id, $type);
//		g('smarty') -> assign('sources', $source_list);
		
		//0 未读 1已读
		$road = get_var_get('road');
		$road = empty($road) ? 0 : 1;
		g('smarty') -> assign('road', $road);
		$page_data = $this -> get_first_page_data($type, $road);
		g('smarty') -> assign('rec_list', $page_data['rows']);
		g('smarty') -> assign('rec_total', $page_data['total']);
		
		g('smarty') -> assign('media_qy_url', MEDIA_URL_PREFFIX);
		g('smarty') -> assign('type_name', $type_name);
		g('smarty') -> assign('title', $type_name);
		g('smarty') -> show($this -> temp_path.'news_index.html');
	}
	
	/** 分页获取文章列表 */
	public function lists() {
        if($this->is_parent()){
            cls_resp::show_warn_page('程序员正在开发中~<br>如需查看通知历史，请先移步到学校通知的消息中查看');
        }

		$need = array('type', 'title', 'offset', 'limit', 'sort', 'order', 'start', 'end', 'source', 'road');
		$data = $this -> get_post_data($need);
		try {
			$user = g("user") -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
			$user["dept_list"] = json_decode($user["dept_list"], TRUE);
			!is_array($user["dept_list"]) && $user["dept_list"] = array();
			
			$data = g($this -> mod_news_content) -> lists($this -> com_id, $this -> user_id, $user["dept_list"], $data['type'], $data['title'], $data['offset'], $data['limit'], $data['sort'], $data['order'], $data['start'], $data['end'], $data['source'], $data['road']);
			cls_resp::echo_ok(cls_resp::$OK, 'info', $data['rows']);
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
	/** 获取第一页数据 */
	private function get_first_page_data($type, $road) {
		$data = array(
			'type' => $type,
			'title' => '',
			'offset' => 0,
			'limit' => 20,
			'sort' => 'public_time',
			'order' => 'desc',
			'start' => 0,
			'end' => 0,
			'source' => '', 
			'road' => $road
		);		
		
		$user = g("user") -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		$user["dept_list"] = json_decode($user["dept_list"], TRUE);
		!is_array($user["dept_list"]) && $user["dept_list"] = array();
		
		$ret = g($this -> mod_news_content) -> lists($this -> com_id, $this -> user_id, $user["dept_list"], $data['type'], $data['title'], $data['offset'], $data['limit'], $data['sort'], $data['order'], $data['start'], $data['end'], $data['source'], $data['road']);
		return $ret;		
	}
	
	/** 验证分类有效性 */
	private function verify_type() {
		$type = get_var_get('type');
		if (empty($type))		cls_resp::show_warn_page('页面不存在');
		
		$type = intval($type);
		if ($type == 0)			cls_resp::show_warn_page('页面不存在');
		
		$type = g($this -> mod_news_type) -> get_by_id($this -> com_id, $type);
		if (!$type)				cls_resp::show_warn_page('页面不存在');
		
		return $type['name'];
	}

	/** 获取员工可查看的文章的所有部门ID */
	private function _get_content_dept() {
		$user = g("user") -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		if(!empty($user["dept_list"])){
			$user["dept_list"] = json_decode($user["dept_list"],TRUE);
		} else {
			$user["dept_list"] = array();
		}
		$ids = g('api_dept') -> list_all_parent_ids($user["dept_list"]);	//直属及所有上级部门ID
		$ret = array();
		foreach ($ids as $group) {
			foreach ($group as $id) {
				$ret[$id] = $id;
			}unset($id);
		}unset($group);
		return array_values($ret);
	}
}