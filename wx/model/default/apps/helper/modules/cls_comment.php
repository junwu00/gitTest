<?php
/**
 * 查看文章评论
 * @author yangpz
 * @date 2016-01-26
 */
class cls_comment extends abs_app_base {
	private $user_id;
	private $com_id;
	private $mod_news_content = 'news_content';
	private $mod_news_comment = 'news_comment';
	
    /** 分页大小 20 */
    private static $PageSize = 20;
	
	public function __construct() {
		parent::__construct('helper');
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}

	/** 评论列表 */
	public function index() {
		$news_id = get_var_get('news');
		try {
			$news_id = intval($news_id);
			if (empty($news_id))			throw new SCException('未指定要评论的文章');
			
			$news_info = g($this -> mod_news_content) -> check_power($news_id);
			
		} catch (SCException $e) {
			cls_resp::show_err_page(array($e -> getMessage()));
		}
	
		$user_info = g('api_user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		$comment_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=comment&a=lists&news='.$news_id;
		$comment_url = SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=comment&a=comment&news='.$news_id;
		g('smarty') -> assign('comment_list_url', $comment_list_url);
		g('smarty') -> assign('comment_url', $comment_url);
		g('smarty') -> assign('user_info', $user_info);
		g('smarty') -> assign('news_id', $news_id);
		
		$comm_id = get_var_get('comm');
		if (empty($comm_id)) {	//评论页面
			$reply_page_url = SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=comment&news='.$news_id;
			g('smarty') -> assign('reply_page_url', $reply_page_url);
			
			g('smarty') -> assign('title', '评论列表_'.$news_info['title']);
			g('smarty') -> show($this -> temp_path.'content_comment_list.html');
			
		} else {			//回复页面
			$comm_info = g($this -> mod_news_comment) -> get_by_id($news_id, $comm_id);
			if (empty($comm_info) || $comm_info['p_id'] != 0) 				cls_resp::show_err_page(array('评论信息不存在'));

			$comm_user = g('api_user') -> get_by_id($comm_info['user_id'], NULL, 'id, name, pic_url');
			$comm_info['user_name'] = empty($comm_user) ? '' : $comm_user['name'];
			$comm_info['user_pic'] = empty($comm_user) ? '' : $comm_user['pic_url'];
			
			g('smarty') -> assign('comm_info', $comm_info);
			g('smarty') -> assign('comm_id', $comm_id);
			
			g('smarty') -> assign('title', '回复_'.$news_info['title']);
			g('smarty') -> show($this -> temp_path.'content_reply_list.html');
		}
		
	}
	
	/** 获取评论列表	 */
	public function lists(){
		try{
			$need = array('news', 'p_id', 'page');
			$data = parent::get_post_data($need);
			$id = intval($data['news']);
			g($this -> mod_news_content) -> check_power($id);
			
			$page = empty($data['page']) ? 1 : intval($data['page']);
		    $page_size = self::$PageSize;
			$comments = g($this -> mod_news_comment) -> list_by_news_id($id, $data['p_id'], $page, $page_size);
		    
		    cls_resp::echo_ok(cls_resp::$OK, 'info', $comments);
		    
		}catch (SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/** 评论 */
	public function comment() {
		try {
			$need = array('news', 'p_id', 'comment');
			$data = parent::get_post_data($need);
			if (empty($data['comment']))	throw new SCException('请填写' . ($data['p_id'] == 0 ? '评论' : '回复') . '内容');
			
			$id = intval($data['news']);
			g($this -> mod_news_content) -> check_power($id);
			$ret = g($this -> mod_news_comment) -> save($data['news'], $data['comment'], $data['p_id']);
			cls_resp::echo_ok(cls_resp::$OK, 'info', array('comm' => $ret));
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}
	
}

//end