<?php
/**
 * 查看文章详情
 * @author yangpz
 * @date 2015-06-25
 *
 */
class cls_content extends abs_app_base {
	private $user_id;
	private $com_id;
	private $mod_news_content = 'news_content';
	private $mod_news_comment = 'news_comment';
	
	public function __construct() {
		parent::__construct('helper');
		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}
	
	//文章展示
	public function index() {
		$type = get_var_get('type');
		$content_id = get_var_get('content');
		
		//增加文章阅读量
		try {
			$type_name = $this -> verify_content_type($type);

			if($this->is_parent()){
			    //家长访问
                //获取内容
                $content = g($this -> mod_news_content) -> check_parent_power($content_id, '*');
                //记录阅读记录
                g($this -> mod_news_content) -> update_read_count($this -> com_id, $type, $content_id,$this->user_id,2);
            }else{
			    //内部成员访问
                $content = g($this -> mod_news_content) -> check_power($content_id, '*');
                $is_update = g($this -> mod_news_content) -> update_read_count($this -> com_id, $type, $content_id,$this->user_id);
            }

		} catch (SCException $e) {
			cls_resp::show_warn_page($e -> getMessage());
		}
		if($is_update){
			$content["read_count"] = $content["read_count"]+1;
		}

        g('js_sdk') -> auth_assign_jssdk_sign();
		//获取文件附件
		$files = g('news_file') -> list_by_content_id($this -> com_id, $content_id);
		g('smarty') -> assign('files', $files);
        g('smarty') -> assign('media_url_host', CDN_MEDIA_URL_HOST);
		//设置下载链接
		$download_url = $this -> app_domain.'&m=content&a=download';
		g('smarty') -> assign('download_url', $download_url);
        //当前在用手机平台：0-其他、1-ios、2-android、3-wp
        $plat_id = g('common') -> get_mobile_platid();
		g('smarty') -> assign('plat_id', $plat_id);
		
		//评论概况/评论相关URL
		$comm_total = g($this -> mod_news_comment) -> get_total_by_news_id($content_id);
		g('smarty') -> assign('comm_total', $comm_total);
		$comment_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=comment&news='.$content_id;
		$comment_url = SYSTEM_HTTP_DOMAIN.'index.php?app=helper&m=comment&a=comment&news='.$content_id;
		g('smarty') -> assign('comment_list_url', $comment_list_url);
		g('smarty') -> assign('comment_url', $comment_url);
		$user_info = g('api_user') -> get_by_id($_SESSION[SESSION_VISIT_USER_ID]);
		g('smarty') -> assign('user_info', $user_info);
		$comments = g($this -> mod_news_comment) -> list_by_news_id($content_id, 0, 1, 3);
		g('smarty') -> assign('comm_list', $comments['data']);
		
		//展示文章
		$content['content'] = trans_static_path($content['content'], $GLOBALS['INVALID_CDN_MEDIA_URL_HOST']);
		g('smarty') -> assign('content', $content);
		g('smarty') -> assign('qy_media_url', MEDIA_URL_PREFFIX);
		
		g('smarty') -> assign('title', "{$type_name}_{$content['title']}");
        g('smarty') -> assign('is_parent', $this->is_parent());
		g('smarty') -> show($this -> temp_path.'content_index.html');
	}
	
	/** 下载/推送文件 */
	public function download() {
		try {
			$need = array('type', 'content', 'file', 'is_push', 'is_origin');	//文章类型ID；文章ID；文件ID；是否强制推送到微信；是否是原文件（仅图片有效）
			$data = $this -> get_post_data($need);
	
			$type_name = $this -> verify_content_type($data['type']);

			if($this->is_parent()){
                $content = g($this -> mod_news_content) -> check_parent_power($data['content'], '*');
            }else{
                $content = g($this -> mod_news_content) -> check_power($data['content'], '*');
            }

			
			$file = g('news_file') -> get_by_id($data['file'], $this -> com_id, $data['content']);
			if (empty($file)) {
				throw new SCException('文章附件不存在');
			}
	
			$file_info = array(
				'name' => $file['file_name'],
				'extname' => $file['file_ext'],
				'hash' => $file['file_key']
			);
			$size = '';
			!empty($data['is_origin']) && $size = '-1';			//-1，下载原图
			$is_push = $data['is_push'] == 0 ? FALSE : TRUE;

            if($this->is_parent()){
                parent::school_common_download($file_info, $is_push, $size);
            }else{
                parent::common_download($file_info, $is_push, $size);
            }

			
			parent::log('下载文件成功');
			cls_resp::echo_ok();
			
		} catch (SCException $e) {
			cls_resp::echo_exp($e);
		}
	}

	/** 验证分类有效性 */
	private function verify_content_type($type) {
		if (empty($type))		cls_resp::show_warn_page('页面不存在');
		
		$type = intval($type);
		if ($type == 0)			cls_resp::show_warn_page('页面不存在');
		
		$type = g('news_type') -> get_by_id($this -> com_id, $type);
		if (!$type)				cls_resp::show_warn_page('页面不存在');
		
		return $type['name'];
	}
	
}

//end