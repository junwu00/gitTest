<?php
/**
 * 应用的配置文件_民主投票
 * 
 * @author yangpz
 * @date 2014-11-17
 * 
 */

return array(
	'id' 		=> 9,											//对应sc_app表的id
	//此处表示我方定义的套件id，非版本号，由于是历史名称，故不作调整
	'combo' => g('com_app') -> get_sie_id(9),
	'name' 		=> 'vote',
	'cn_name' 	=> '民主投票',
	'icon' 		=> SYSTEM_HTTP_APPS_ICON.'Vote.png',

	'menu' => array(
	),
		
	'wx_menu' => array(
		array('name' => '参与投票', 'type' => 'view', 'url' => SYSTEM_HTTP_DOMAIN.'index.php?app=vote&m=list&a=show&corpurl=<corpurl>'),
	),
);

// end of file