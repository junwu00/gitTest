$(document).ready(function () {
	change();
	app_auto_hight();
	initPercent();
	init_img_pre();
	initTime();
	initVote();
	init_voted_list();
	autoImgWidthHeight();
	voteResult()
});

var dataValue = ''; //存储赞成或者反对
function initVote() {
	$('.vote-item div.action div.submit span.icon-mark').click(function () {
		dataValue = $(this).attr('data-value');
		// if($('#status').val() == 2){
		// 	var data = {};
		//     data.field_val = [];
		//     data.field_val.push($(this).attr('data-value'));
		//     var opt = {};
		// 	opt.url = $('#attend_url').val();
		// 	opt.data = JSON.stringify(data);
		// 	lock_screen('投票中……');
		// 	opt.success = function(response){
		// 		unlock_screen();
		// 		if(response.errcode != 0){
		// 			fail_screen(response.errmsg)
		// 		}else{
		// 			success_screen('投票成功');
		// 			setTimeout(function(){location.reload()},1000);
		// 		}
		// 	}
		// 	frame_obj.post(opt);
		// }
	});
}

// 投票后的结果
function voteResult() {
	var res = $('.vote-item-choosed div.action div.ratio span').attr('data-result')
	console.log(res, 'res');
	if (res == 'agree_1') {
		console.log('add');
		$('.vote-item-choosed div.action div.up .icon-mark i').addClass('chooseicon')
		$('.vote-item-choosed div.action div.up .icon-mark').addClass('chooseBack')
		$('.vote-item-choosed div.action div.up .desc').addClass('chooseDesc')
	}
	if (res == 'agree_2') {
		$('.vote-item-choosed div.action div.down .icon-mark i').addClass('chooseicon')
		$('.vote-item-choosed div.action div.down .icon-mark').addClass('chooseBack')
		$('.vote-item-choosed div.action div.down .desc').addClass('chooseDesc')
	}
}
// 投票
function vote() {
	if (dataValue === '') {
		var warnAlert = {};
		warnAlert.title = '投票提示';
		warnAlert.desc = "请选择赞成或者反对";
		alert(warnAlert);
		return;
	}
	if ($('#status').val() == 2) {
		var data = {};
		data.field_val = [];
		data.field_val.push(dataValue);
		var opt = {};
		opt.url = $('#attend_url').val();
		opt.data = JSON.stringify(data);
		lock_screen('投票中……');
		opt.success = function (response) {
			unlock_screen();
			if (response.errcode != 0) {
				fail_screen(response.errmsg)
			} else {
				success_screen('投票成功');
				setTimeout(function () { location.reload() }, 1000);
			}
		}
		frame_obj.post(opt);
	}
}



function initPercent() {
	$('.vote-item-choosed div.action div.ratio span').each(function () {
		$(this).css({ 'width': $(this).attr('data-percent') + '%' });
		console.log($(this).attr('data-percent'), 'data-percent');
	});
}

function init_img_pre() {
	$('.vote-item div.img img').click(function (e) {
		var img_pre_url = [];
		img_pre_url.push($(this).attr('src'));
		jssdk_obj.preview_img('', img_pre_url);
	});

	$('.vote-item-choosed div.img img').click(function (e) {
		var img_pre_url = [];
		img_pre_url.push($(this).attr('src'));
		jssdk_obj.preview_img('', img_pre_url);
	});
}

function show_voted_list(response) {
	console.log(response, 'response123');
	var html = '';
	for (var i = 0; i < response.data.list.length; i++) {
		var ob = response.data.list[i];
		var items = response.data.field_map;
		var head_img = ob.head_img;
		var name = ob.name;
		var time = ob.create_time;
		var field_val = $.parseJSON(ob.field_val)[0];
		html += '<div class="result-item">';
		html += '<img alt="" src="' + head_img + '">';
		html += '<p class="name"><span>' + name + '</span><span>' + time + '</span></p>';
		html += '<span class="choose-desc">选择</span>';
		html += '<p class="choose-item overflow-ellipsis">';
		if (field_val == "agree_1") {
			html += '<i class="icon icon-thumbs-up"></i>';
		} else {
			html += '<i class="icon icon-thumbs-down"></i>';
		}
		html += '</p>';
		html += '</div>';
	}
	$('.vote-resule').append(html);
}

function autoImgWidthHeight() {
	$('div.img img').each(function () {
		$(this).load(function () {
			imgAuto(this);
		});
		if (this.complete) {
			imgAuto(this);
		}
	});
}

function imgAuto(that) {
	var width = $(that).width();
	var height = $(that).height();
	var p_width = $(that).parent().width();
	var p_height = $(that).parent().height();
	if (width < height) {
		$(that).css({ 'width': p_width + 'px' });
	} else {
		$(that).css({ 'height': p_height + 'px' });
	}
}

//点击切换样式和切换值
function change() {
	// 赞成切换样式
	$('.vote-item div.action div.up span.icon-mark').click(function () {
		// 取消反对的样式
		$('.vote-item div.action div.down span.icon-mark').removeClass('upactive');
		$('.vote-item div.action div.down span.icon-mark i').removeClass('Iactive');
		$('.vote-item div.action div.down span.desc').removeClass('descActive');


		$(this).addClass('upactive');
		$('.vote-item div.action div.up span.icon-mark').attr('data-value', 'agree_1')
		$('.vote-item div.action div.up span.icon-mark i').addClass('Iactive');
		$('.vote-item div.action div.up span.desc').addClass('descActive');
	});

	// 反对切换样式
	$('.vote-item div.action div.down span.icon-mark').click(function () {
		// 取消赞成的样式
		$('.vote-item div.action div.up span.icon-mark').removeClass('upactive');
		$('.vote-item div.action div.up span.icon-mark i').removeClass('Iactive');
		$('.vote-item div.action div.up span.desc').removeClass('descActive');

		$(this).addClass('upactive');
		$('.vote-item div.action div.down span.icon-mark').attr('data-value', 'agree_2')
		$('.vote-item div.action div.down span.icon-mark i').addClass('Iactive');
		$('.vote-item div.action div.down span.desc').addClass('descActive');
	});


}