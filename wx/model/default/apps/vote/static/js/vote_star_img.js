$(document).ready(function() {
	app_auto_hight();
	initStar();
	init_img_pre();
	initTime();
	initVote();
	init_voted_list();
	autoImgWidthHeight();
});

function initVote(){
	if($('.vote-action button')){
		$('.vote-action button').click(function(){
			var data = {};
			data.field_val=[];
			var stars = $('.vote-item p.star i.icon-star').length;
			if(stars == 0){
				var opt1 = {};
				opt1.title='投票提示';
				opt1.desc="至少选择一颗星";
				alert(opt1);
				return;
			}
			data.field_val.push("star_" + stars);
			var opt = {};
			opt.url = $('#attend_url').val();
			opt.data = JSON.stringify(data);
			lock_screen('投票中……');
			opt.success = function(response){
				unlock_screen();
				if(response.errcode != 0){
					fail_screen(response.errmsg)
				}else{
					success_screen('投票成功');
					setTimeout(function(){location.reload()},1000);
				}
			}
			frame_obj.post(opt);
		})
	}
}

function initStar(){
	$('.vote-item p.star i').click(function(e){
		var stars = $('.vote-item p.star i');
		var keyIndex = stars.length;
		for(var i = 0; i < stars.length; i++){
			if($(this).is($(stars[i]))){
				keyIndex = i;
				$('.vote-item p.desc span.my-count').html(keyIndex+1+'分');
				break;
			}
		}
		for(var i = 0; i < stars.length; i++){
			if(i <= keyIndex){
				$(stars[i]).removeClass('icon-star-empty');
				$(stars[i]).addClass('icon-star');
			}else{
				$(stars[i]).removeClass('icon-star');
				$(stars[i]).addClass('icon-star-empty');
			}
		}
	});
}

function init_img_pre(){
	$('.vote-item div img').click(function(e){
		var img_pre_url = [];
		img_pre_url.push($(this).attr('src'));
		jssdk_obj.preview_img('',img_pre_url);
	});
	
	$('.vote-item-choosed div.img img').click(function(e){
		var img_pre_url = [];
		img_pre_url.push($(this).attr('src'));
		jssdk_obj.preview_img('',img_pre_url);
	});
}

function show_voted_list(response){
	var html = '';
	for(var i = 0; i < response.data.list.length; i++){
		var ob = response.data.list[i];
		var items = response.data.field_map;
		var head_img = ob.head_img;
		var name = ob.name;
		var time = ob.create_time;
		var field_val = $.parseJSON(ob.field_val)[0].split('_')[1];
		html +='<div class="result-item">';
		html +='<img alt="" src="'+ head_img +'">';
		html +='<p class="name"><span>'+ name +'</span><span>'+ time +'</span></p>';
		html +='<span class="choose-desc">选择</span>';
		html +='<p class="choose-item overflow-ellipsis">';
		for(var j = 0; j < 5; j++){
			if(j < field_val){
				html +='<i class="icon icon-star"></i>';
			}else{
				html +='<i class="icon icon-star-empty"></i>';
			}
		}
		html +='</p>';
		html +='</div>';
	}
	$('.vote-resule').append(html);
}

function autoImgWidthHeight(){
	$('.vote-item div.img img').each(function(){		
		$(this).load(function(){
			imgAuto(this);
		});
		if(this.complete){
			imgAuto(this);
		}
	});
	
	$('.vote-item-choosed div.img img').each(function(){		
		$(this).load(function(){
			imgAuto1(this);
		});
		if(this.complete){
			imgAuto1(this);
		}
	});
}


function imgAuto(that){
	var width = $(that).width();
	var height = $(that).height();
	var p_width = $(that).parent().width();
	var p_height = $(that).parent().height();
	if(width < height){
		$(that).css({'width':p_width+'px'});
	}else{
		$(that).css({'height':p_height+'px'});
	}
}

function imgAuto1(that){
	var width = $(that).width();
	var height = $(that).height();
	var p_width = $(that).parent().width();
	var p_height = $(that).parent().height();
	if(width < height){
		$(that).css({'width':(p_width - 70)+'px'});
	}else{
		$(that).css({'height':p_height+'px'});
	}
}