$(document).ready(function () {
	init_head();
	app_auto_hight();
	load_list();
	init_scroll_load();
});

function init_head() {
	var opt = {};
	opt.menu = { 'list': [{ 'name': '进行中', 'activekey': 2 }, { 'name': '未开始', 'activekey': 1 }, { 'name': '已结束', 'activekey': 3 }], 'fun': menu_switch };
	head_obj.init_head(opt);
	head_obj.active_head_menu($('#activekey').val());
}

function menu_switch(obj, el) {
	location.href = $('#this_url').val() + '&vote_type=' + $(el).attr('data-activekey');
}

function load_list() {
	var page = $('.vote_list').attr('data-page') - 0;
	var data = {};
	data.page = page + 1;
	data.vote_type = $('#activekey').val();
	var opt = {};
	opt.url = $('#get_list_url').val();
	opt.data = JSON.stringify(data);
	opt.success = function (response) {
		if (data.page == 1 && response.list.length == 0) {
			no_data_css();
		}
		if (response.list.length < 10) {
			scroll_load_obj.scroll_load_end($('.container'));
		}
		scroll_load_obj.remove_scroll_waiting($('.vote_list'));
		scroll_load_obj.set_is_scroll_loading_false($('.container'));
		$('.vote_list').attr('data-page', data.page);
		var html = '';
		for (var i = 0; i < response.list.length; i++) {
			var ob = response.list[i];
			var status = ob.status;
			var img_type = ob.img_type;
			var total = ob.total;
			var my_vote = ob.self_val;
			var vote_result = ob.vote_result;
			var title = ob.title;
			var time = '';
			if (status == 1) {
				time = ob.html_begin_time;
			} else if (status == 2) {
				time = bw_time(new Date(ob.end_time.replace(/-/g, '/')).getTime(), $('#server-time').val() * 1000);
			} else { }
			var is_real_url = ob.is_real == 0 ? $('#app_static').val() + 'images/wx_vote_unrealname.svg' : $('#app_static').val() + 'images/wx_vote_realname.svg';
			if (ob.type == 1) {
				// 文字投票
				var my_select = '';
				for (var j = 0; j < my_vote.length; j++) {
					my_select += (my_vote[j].itemIndex + '.' + my_vote[j].text + ';')
				}
				my_select = my_select.substring(0, my_select.length - 1);
				html += '<div class="item" onclick="go_detail(' + ob.vote_id + ')">';
				// html += '<span>文</span>';
				html += '<div class="textTitle overflow-ellipsis">' + title + '</div>';
				html += '<div class="item-vote-main">';
				// html += '<p class="desc overflow-ellipsis">快来为你喜欢的投上一票吧</p>';
				html += '<p class="type ">文字投票</p>';
				html += '<p class="num ">' + total + '人已投' + '</p>';
				if (my_vote.length == 0) {
					if (status == 1) {
						html += '<p class="time overflow-ellipsis"><span>距离开始:</span><span>' + time + '</span></p>';
					} else if (status == 2) {
						html += '<p class="time overflow-ellipsis"><span>剩余:</span><span>' + time + '</span></p>';
					} else if (status == 3) {
						html += '<p class="time overflow-ellipsis"><span>您未参与该投票</span><span></span></p>';
					}
				} else {
					html += '<p class="text-result overflow-ellipsis">我的投票: ' + my_select + '</p>';
					html += '<span class="mark-result"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_voted.svg"></span>';
				}
				html += '</div>';
				html += '<div class="item-vote-others">';
				html += '<p class="is-anonymous"><img alt="" src="' + is_real_url + '"></p>';
				if ((status == 2 && total == 0) || (status == 3 && total == 0)) {
					html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
					html += '<p class="no-vote-desc">暂无投票</p>';
				} else if (status == 2 && total != 0 && my_vote.length == 0) {
					html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_fire.svg"></p>';
					html += '<p class="no-vote-desc">' + total + '人参与</p>';
				} else if ((status == 2 && total != 0 && my_vote.length != 0) || (status == 3 && total != 0)) {
					var indexItem = vote_result.key.split('_')[1];
					// html += '<p class="more-select overflow-ellipsis">'+ indexItem + '.' + vote_result.name +'</p>';
					html += '<p class="percent">' + vote_result.value + '%</p>';
				} else { }
				html += '</div>';
				html += '</div>';
			} else {
				if (ob.img_type == 1) {
					// 赞成与反对投票
					html += '<div class="item" onclick="go_detail(' + ob.vote_id + ')">';
					html += '<div class="img">';
					html += '<img alt="" src="' + ob.first_img_url + '">';
					html += '</div>';
					html += '<div class="textTitle overflow-ellipsis">' + title + '</div>';
					html += '<div class="item-vote-main">';
					html += '<p class="type-add">赞成与反对投票</p>';
					html += '<p class="num ">' + total + '人已投' + '</p>';
					if (my_vote.length !== 0) {
						if (my_vote[0] == 'agree_1') {
							resultVote = '赞成';
						} else {
							resultVote = '不赞成';
						}
						html += '<p class="text-result overflow-ellipsis">我的投票: ' + resultVote + '</p>';
					}

					// html += '<p class="desc overflow-ellipsis">喜欢的话，赞一个吧</p>';
					if (my_vote.length == 0) {
						if (status == 1) {
							html += '<p class="time overflow-ellipsis"><span>距离开始:</span><span>' + time + '</span></p>';
						} else if (status == 2) {
							html += '<p class="time overflow-ellipsis"><span>剩余:</span><span>' + time + '</span></p>';
						} else if (status == 3) {
							html += '<p class="time overflow-ellipsis"><span>您未参与该投票</span><span></span></p>';
						}
					} else {
						var icon_class = '';
						if (my_vote[0] == 'agree_1') {
							icon_class = 'icon-thumbs-up';
						} else if (my_vote[0] == 'agree_2') {
							icon_class = 'icon-thumbs-down';
						} else { }
						html += '<p class="star-result overflow-ellipsis">';
						html += '<span class="star-result-desc">我的投票:</span>';
						html += '<i class="icon ' + icon_class + '"></i>';
						html += '</p>';
						html += '<span class="mark-result"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_voted.svg"></span>';
					}
					html += '</div>';
					html += '<div class="item-vote-others">';
					html += '<p class="is-anonymous"><img alt="" src="' + is_real_url + '"></p>';
					if ((status == 2 && total == 0) || (status == 3 && total == 0)) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
						html += '<p class="no-vote-desc">暂无投票</p>';
					} else if (status == 2 && total != 0 && my_vote.length == 0) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_fire.svg"></p>';
						html += '<p class="no-vote-desc">' + total + '人参与</p>';
					} else if ((status == 2 && total != 0 && my_vote.length != 0) || (status == 3 && total != 0)) {
						var icon_class = '';
						if (vote_result.key == 'agree_1') {
							icon_class = 'icon-thumbs-up';
						} else if (vote_result.key == 'agree_2') {
							icon_class = 'icon-thumbs-down';
						} else { }
						html += '<p class="more-select-star"><i class="icon ' + icon_class + '"></i></p>';
						html += '<p class="star-percent">' + vote_result.value + '%</p>';
					} else { }
					html += '</div>';
					html += '</div>';
				} else if (ob.img_type == 2) {
					// 星级评分
					html += '<div class="item" onclick="go_detail(' + ob.vote_id + ')">';
					html += '<div class="img">';
					html += '<img alt="" src="' + ob.first_img_url + '">';
					html += '</div>';
					html += '<div class="textTitle overflow-ellipsis">' + title + '</div>';
					html += '<div class="item-vote-main">';
					// html += '<p class="desc overflow-ellipsis">快来给它评个分吧</p>';
					html += '<p class="type">评分投票</p>';
					html += '<p class="num">' + total + '人已投' + '</p>';
					if (my_vote.length !== 0) {
						var stars = my_vote[0].split('_')[1];
						html += '<p class="price">' + '我的投票:' + stars + '分' + '</p>';
					}
					if (my_vote.length == 0) {
						if (status == 1) {
							html += '<p class="time overflow-ellipsis"><span>距离开始:</span><span>' + time + '</span></p>';
						} else if (status == 2) {
							html += '<p class="time overflow-ellipsis"><span>剩余:</span><span>' + time + '</span></p>';
						} else if (status == 3) {
							html += '<p class="time overflow-ellipsis"><span>您未参与该投票</span><span></span></p>';
						}
					} else {
						html += '<p class="star-result overflow-ellipsis">';
						html += '<span class="star-result-desc">我的投票:</span>';
						// var stars = my_vote[0].split('_')[1];
						// for (var j = 0; j < 5; j++) {
						// 	if (stars > j) {
						// 		 html += '<i class="icon icon-star"></i>';
						// 	} else {
						// 		html += '<i class="icon icon-star-empty"></i>';
						// 	}
						// }
						html += '</p>';
						html += '<span class="mark-result"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_voted.svg"></span>';
					}
					html += '</div>';
					html += '<div class="item-vote-others">';
					html += '<p class="is-anonymous"><img alt="" src="' + is_real_url + '"></p>';
					if ((status == 2 && total == 0) || (status == 3 && total == 0)) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
						html += '<p class="no-vote-desc">暂无投票</p>';
					} else if (status == 2 && total != 0 && my_vote.length == 0) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
						html += '<p class="no-vote-desc">' + total + '人参与</p>';
					} else if ((status == 2 && total != 0 && my_vote.length != 0) || (status == 3 && total != 0)) {
						var stars = vote_result.key.split('_')[1];
						html += '<p class="more-select-star"><i class="icon icon-star"></i><span class="star-count">' + stars + '</span></p>';
						html += '<p class="star-percent">' + vote_result.value + '%</p>';
					} else { }
					html += '</div>';
					html += '</div>';
				} else if (ob.img_type == 3) {
					html += '<div class="item" onclick="go_detail(' + ob.vote_id + ')">';
					html += '<div class="img">';
					html += '<img alt="" src="' + ob.first_img_url + '">';
					html += '</div>';
					html += '<div class="textTitle overflow-ellipsis">' + title + '</div>';
					html += '<div class="item-vote-main">';
					html += '<p class="type">图片投票</p>';
					html += '<p class="num">' + total + '人已投' + '</p>';
					// html += '<p class="desc overflow-ellipsis">快来为你喜欢的投上一票吧</p>';
					if (my_vote.length == 0) {
						if (status == 1) {
							html += '<p class="time overflow-ellipsis"><span>距离开始:</span><span>' + time + '</span></p>';
						} else if (status == 2) {
							html += '<p class="time overflow-ellipsis"><span>剩余:</span><span>' + time + '</span></p>';
						} else if (status == 3) {
							html += '<p class="time overflow-ellipsis"><span>您未参与该投票</span><span></span></p>';
						}
					} else {
						html += '<p class="more-pic-result overflow-ellipsis">';
						html += '<span class="more-pic-result-desc">我的投票:</span>';
						html += '<span class="imgs">';
						for (var j = 0; j < my_vote.length; j++) {
							html += '<img alt="" src="' + $('#media_url_preffix').val() + my_vote[j].img_url + '">';
						}
						html += '</span>';
						html += '</p>';
						html += '<span class="mark-result"><img alt="" src="' + $('#app_static').val() + 'images/wx_vote_voted.svg"></span>';
					}
					html += '</div>';
					html += '<div class="item-vote-others">';
					html += '<p class="is-anonymous"><img alt="" src="' + is_real_url + '"></p>';
					if ((status == 2 && total == 0) || (status == 3 && total == 0)) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
						html += '<p class="no-vote-desc">暂无投票</p>';
					} else if (status == 2 && total != 0 && my_vote.length == 0) {
						html += '<p class="no-vote"><img alt="" src="' + $('#app_static').val() + 'images/no-vote.png"></p>';
						html += '<p class="no-vote-desc">' + total + '人参与</p>';
					} else if ((status == 2 && total != 0 && my_vote.length != 0) || (status == 3 && total != 0)) {
						html += '<p class="more-select-pic"><img alt="" src="' + $('#media_url_preffix').val() + vote_result.img_url + '"></p>';
						html += '<p class="pic-percent">' + vote_result.value + '%</p>';
					} else { }
					html += '</div>';
					html += '</div>';
				}
			}
		}
		$('.vote_list').append(html);
		autoImgWidthHeight();
	}
	frame_obj.post(opt);
}

function go_detail(id) {
	location.href = $('#info_url').val() + '&vote_id=' + id;
}

function init_scroll_load() {
	scroll_load_obj.init({ scroll_target: $('.container'), target: $('.vote_list'), fun: load_list });
}

function no_data_css() {
	$('#powerby').addClass('hide');
	$('body').css({ 'background-color': '#fff' });
	var opt = {};
	opt.target = $('.vote_list');
	opt.img = $('#app_static').val() + 'images/nhlogo.png';
	if ($('#activekey').val() == 2) {
		opt.title = '暂无进行中的投票';
		opt.desc_list = ['暂时没有正在进行中的投票，管理员可通', '过后台发布投票信息'];
	}
	if ($('#activekey').val() == 1) {
		opt.title = '暂无未开始的投票';
		opt.desc_list = ['暂时没有未开始的投票，未开始的投票系', '统将在投票开始后通知'];
	}
	if ($('#activekey').val() == 3) {
		opt.title = '暂无已结束的投票';
		opt.desc_list = ['暂时没有已结束的投票，进行中的投票将', '在投票截止日期后显示在这里'];
	}
	has_no_record(opt);
	var html_height = $('html').height() - 90;
	var main_height = $('.has-no-record').height();
	var top = (html_height - main_height) / 2 - 100;
	$('.has-no-record').css({ 'margin-top': top + 'px' });
}

function autoImgWidthHeight() {
	$('.item div.img img').each(function () {
		if ($(this).attr('data-is-auto') != 'true') {
			$(this).load(function () {
				imgAuto(this);
			});
			if (this.complete) {
				imgAuto(this);
			}
		}
	});
}

function imgAuto(that) {
	var width = $(that).width();
	var height = $(that).height();
	var p_width = $(that).parent().width();
	var p_height = $(that).parent().height();
	if (width < height) {
		$(that).css({ 'width': p_width + 'px' });
	} else {
		$(that).css({ 'height': p_height + 'px' });
	}
}

function bw_time(time1, tim2) {
	var ts = time1 - tim2;							//计算剩余的毫秒数
	ts /= 1000;
	var dd = parseInt(ts / 60 / 60 / 24, 10);			//计算剩余的天数
	var hh = parseInt(ts / 60 / 60 % 24, 10);			//计算剩余的小时数
	var mm = parseInt(ts / 60 % 60, 10);				//计算剩余的分钟数
	var ss = parseInt(ts % 60, 10);
	mm = checkTime(mm);
	ss = checkTime(ss);
	if (parseInt(ss) <= 0) {
		ss = '00';
	}
	var html = '';
	if (parseInt(dd) > 0) {
		html = dd + "天" + hh + "小时" + mm + "分钟" + ss + "秒";
	} else if (parseInt(hh) > 0) {
		html = hh + "小时" + mm + "分钟" + ss + "秒";
	} else if (parseInt(mm) > 0) {
		html = mm + "分钟" + ss + "秒";
	} else {
		html = ss + "秒";
	}
	return html
}

function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}
