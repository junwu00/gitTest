function init_voted_list(){
	if($('#is_real').val() == 1){
		if(($('#view_type').val() == 1 && $('#my_vote').val() == 0) 
				|| ($('#view_type').val() == 2 && $('#status').val() == 3) 
				|| ($('#view_type').val() == 3)){
			init_scroll_load();
		}else{
			var desc = '';
			if($('#view_type').val() == 1){
				desc = '投票后即可查看别人的投票结果!';
			}else if($('#view_type').val() == 2){
				desc = '投票结束后即可查看别人的投票结果!';
			}
			var html='';
			html +='<div class="after-can-see">';
			html +='<img alt="" src="'+ $('#app_static').val() +'images/no-vote.png">';
			html +='<p>'+ desc +'</p>';
			html +='</div>';
			$('.vote-resule').append(html);
		}
	}
}

function init_scroll_load(){
	load_voted_list();
	scroll_load_obj.init({scroll_target:$('.container'),target:$('.vote-resule'),fun:load_voted_list});
}

function load_voted_list(){
	var page = $('.vote-resule').attr('data-page')-0;
	page = page+1;
	var opt = {};
	opt.url = $('#get_attendant_url').val()+'&page=' + page;
	opt.success = function(response){
		$('.vote-resule').attr('data-page',page);
		var html='';
		if(page == 1 && response.data.list.length == 0){
			html +='<div class="after-can-see">';
			html +='<img alt="" src="'+ $('#app_static').val() +'images/no-vote.png">';
			html +='<p>暂无其他人投票结果</p>';
			html +='</div>';
			scroll_load_obj.scroll_load_end($('.container'));
			$('.vote-resule').append(html);
			return;
		}
		if(response.data.list.length == 0){
			scroll_load_obj.scroll_load_end($('.container'));
		}
		scroll_load_obj.remove_scroll_waiting($('.vote-resule'));
		scroll_load_obj.set_is_scroll_loading_false($('.container'));
		$('.vote_list').attr('data-page',page);
		show_voted_list(response);
		
	}
	frame_obj.post(opt);
}

function initTime(){
	if($('#status').val() == 1){
		var time = bw_time($('#begin_time').val()*1000,$('#server_time').val()*1000);
		$('.vote-action p.start-time-notvote span:last-child').html(time)
		$('.vote-action p.start-time span:last-child').html(time)
	}else if($('#status').val() == 2){
		var time = bw_time($('#end_time').val()*1000,$('#server_time').val()*1000);
		$('.vote-action p.start-time span:last-child').html(time)
		$('.vote-action p.start-time-notvote span:last-child').html(time)
	}else{}
}

function bw_time(time1,tim2){
	var ts = time1 - tim2;							//计算剩余的毫秒数
	ts /= 1000;
	var dd = parseInt(ts / 60 / 60 / 24, 10);			//计算剩余的天数
	var hh = parseInt(ts / 60 / 60 % 24, 10);			//计算剩余的小时数
	var mm = parseInt(ts / 60 % 60, 10);				//计算剩余的分钟数
	var ss = parseInt(ts % 60, 10);	
	mm = checkTime(mm);
	ss = checkTime(ss);
	if (parseInt(ss) <= 0) {
		ss = '00';
	}
	var html = '';
	if(parseInt(dd) > 0){
		html = dd + "天" + hh + "小时" + mm + "分钟" + ss + "秒";
	}else if(parseInt(hh) > 0){
		html = hh + "小时" + mm + "分钟" + ss + "秒";
	}else if(parseInt(mm) > 0){
		html = mm + "分钟" + ss + "秒";
	}else{
		html = ss + "秒";
	}							
	return html
}

function checkTime(i) {  
	if (i < 10) {  
		i = "0" + i;  
	}  
	return i;  
}