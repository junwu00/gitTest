$(document).ready(function() {
	app_auto_hight();
	initChecked();
	initPercent();
	initTime();
	initVote();
	init_voted_list();
	init_img_pre();
	autoImgWidthHeight();
	autoLabelHeight();
});

function initChecked(){
	$('.vote-item .item label').click(function(){
		if($('#status').val() == 1){
			$(this).parent().removeClass('active');
			$(this).children('input').removeAttr('checked');
			return;
		}
		if($(this).children('input')[0].checked){
			if($(this).parent().parent().attr('data-is-single') == 1){
				$(this).parent().parent().children().removeClass('active');
				$(this).parent().addClass('active');
				var input = $(this).children('input');
				$(this).parent().parent().find('input').each(function(){
					if(!$(this).is(input)){
						$(this).removeAttr('checked');
					}
				});
			}else{
				var max_select = $(this).parent().parent().attr('data-max-select');
				var max_type = $(this).parent().parent().attr('data-max-type');
				if((max_type == 0 || max_type == 1) && max_select != 0){
					$(this).parent().addClass('active');
					var actives = $(this).parent().parent().children('div.active');
					if(actives.length > max_select){
						$(this).parent().removeClass('active');
						$(this).children('input').removeAttr('checked');
						var opt = {};
						opt.title='投票提示';
						opt.desc="该投票最多只能选择"+max_select+"项";
						alert(opt);
					}
				}else{
					$(this).parent().addClass('active');
				}
			}
		}else{
			$(this).parent().removeClass('active');
		}
	});
}

function initVote(){
	if($('.vote-action button')){
		$('.vote-action button').click(function(){
			var data = getData();
			console.log(data);
			// mrc not add
			if(checkData(data)){
				var opt = {};
				opt.url = $('#attend_url').val();
				opt.data = JSON.stringify(data);
				lock_screen('投票中……');
				opt.success = function(response){
					unlock_screen();
					if(response.errcode != 0){
						fail_screen(response.errmsg)
					}else{
						success_screen('投票成功');
						setTimeout(function(){location.reload()},1000);
					}
				}
				frame_obj.post(opt);
			}
		})
	}
}

function getData(){
	var data = {};
	data.field_val = [];
	$('.vote-item div.active').each(function(){
		data.field_val.push($(this).attr('data-key'));
	});
	return data;
}

function checkData(data){
	var is_single = $('.vote-item').attr('data-is-single');
	var max_select = $('.vote-item').attr('data-max-select');
	var max_type = $('.vote-item').attr('data-max-type');
	if(data.field_val.length == 0){
		// mrc hide
		// var opt = {};
		// opt.title='投票提示';
		// opt.desc="至少选择一项";
		// alert(opt);
		// return false;
	}
	if(is_single == 0){
		if(max_select != 0 && max_type == 0 && data.field_val.length > max_select){
			var opt = {};
			opt.title='投票提示';
			opt.desc="该投票最多只能选择"+max_select+"项";
			alert(opt);
			return false;
		}else if(max_select != 0 && max_type == 1 && data.field_val.length < max_select){
			var opt = {};
			opt.title='投票提示';
			opt.desc="该投票必须选择"+max_select+"项";
			alert(opt);
			return false;
		}else{}
	}
	return true;
}

function initPercent(){
	$('.vote-item-choosed .item').each(function(){
		if($(this).attr('data-percent') && $(this).attr('data-percent') > 0){
			$(this).children('div.parent').children('div.percent').css({'width':$(this).attr('data-percent')+'%'})
		}
	});
}

function init_img_pre(){
	$('.vote-item .item div').click(function(e){
		var img_pre_url = [];
		img_pre_url.push($(this).children('img').attr('src'));
		jssdk_obj.preview_img('',img_pre_url);
	});
	
	$('.vote-item-choosed .item div').click(function(e){
		var img_pre_url = [];
		img_pre_url.push($(this).children('img').attr('src'));
		jssdk_obj.preview_img('',img_pre_url);
	});
}

function show_voted_list(response){
	var html = '';
	for(var i = 0; i < response.data.list.length; i++){
		var ob = response.data.list[i];
		var items = response.data.field_map;
		var head_img = ob.head_img;
		var name = ob.name;
		var time = ob.create_time;
		var field_val = $.parseJSON(ob.field_val);
		html +='<div class="result-item">';
		html +='<img alt="" src="'+ head_img +'">';
		html +='<p class="name"><span>'+ name +'</span><span>'+ time +'</span></p>';
		html +='<span class="choose-desc">选择</span>';
		html +='<p class="choose-item overflow-ellipsis">';
		for(var j = 0; j < field_val.length; j++){
			html +='<img alt="" src="'+ $('#media_url_preffix').val() + items[field_val[j]]+'">';
		}
		html +='</p>';
		html +='</div>';
	}
	$('.vote-resule').append(html);
}

function autoImgWidthHeight(){
	$('.item div.img img').each(function(){		
		$(this).load(function(){
			imgAuto(this);
		});
		if(this.complete){
			imgAuto(this);
		}
	});
}

function imgAuto(that){
	var width = $(that).width();
	var height = $(that).height();
	var p_width = $(that).parent().width();
	var p_height = $(that).parent().height();
	if(width < height){
		$(that).css({'width':p_width+'px'});
	}else{
		$(that).css({'height':p_height+'px'});
	}
}

function autoLabelHeight(){
	$('.vote-item div.item label').each(function(){
		if($(this).height() > 70){
			$(this).css({'line-height':'normal'});
		}
	});
	
	$('.vote-item-choosed div.item label').each(function(){
		if($(this).height() <= 70){
			var spans = $(this).children('span');
			var top = (70 - ($(spans[0]).height() +　$(spans[1]).height()))/2;
			$(spans[0]).css({'padding-top':top + 'px'})
		}else{
			$($(this).children('span')[0]).css({'padding-top':'0px'})
		}
	});
}