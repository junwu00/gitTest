<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'vote_end_picture_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/progress.css'
	),
	'vote_end_star_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/star-rating.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/progress.css'
	),
	'vote_end_sup_opp_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/vs.css'
	),
	'vote_end_word_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/progress.css'
	),
	'vote_underway_picture_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/progress.css'
	),
	'vote_underway_star_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/star-rating.css',
		SYSTEM_APPS . 'vote/static/css/progress.css',
		SYSTEM_APPS . 'vote/static/css/end.css'
	),
	'vote_underway_sup_opp_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/end.css',
		SYSTEM_APPS . 'vote/static/css/vs.css'
	),
	'vote_underway_word_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/progress.css',
		SYSTEM_APPS . 'vote/static/css/end.css'
	),
	'vote_unproceed_picture_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css'
	),
	'vote_unproceed_star_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css',
		SYSTEM_APPS . 'vote/static/css/star-rating.css'
	),
	'vote_unproceed_sup_opp_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css'
	),
	'vote_unproceed_word_vote.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css'
	),
	'vote_index.css' => array(
		SYSTEM_APPS . 'vote/static/css/index.css',
		SYSTEM_APPS . 'vote/static/css/unproceed.css'
	),
	'vote_index.js' => array(
		SYSTEM_APPS . 'vote/static/js/index.js',
		SYSTEM_APPS . 'vote/static/js/jquery.knob.js'
	),
	'vote_underway_picture_vote.js' => array(
		SYSTEM_ROOT . 'static/js/jweixin-1.1.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_APPS . 'vote/static/js/unproceed/unproceed.js'
	),
	
);

return $arr;

/* End of this file */