<?php
/**
 * 投票信息控制器
 *
 * @author LiangJianMing
 * @create 2015-1-19
 */
class cls_info extends abs_app_base {
	public function __construct() {
		parent::__construct('vote');
	}

	/**
	 * 展示投票信息
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		$vote_id = (int)get_var_value('vote_id');

		empty($vote_id) && cls_resp::show_err_page(array('非法访问'));
		
		$check = g('vote') -> check_visible($vote_id);
		empty($check) && cls_resp::show_err_page(array('无权查看该投票！'));

		$info = g('vote') -> get_info($vote_id);
		empty($info) && cls_resp::show_err_page(array('投票不存在'));

		//是否返回投票者列表
		$with_user = FALSE;
		if ($info['is_real'] == 1) {
			if ($info['view_type'] == 3) {
				$with_user = TRUE;
			}elseif ($info['view_type'] == 2 && $info['status'] == 3) {
				$with_user = TRUE;
			}elseif ($info['view_type'] == 1 && $info['attended']) {
				$with_user = TRUE;
			}
		}
        //整个自己的投票结果
        $self_ret = g('vote') -> list_self_vote_result([$vote_id]);
        $self_sorted_ret = array();
        foreach ($self_ret as $r) {
            $self_sorted_ret[$r['vote_id']] = $r;
        }unset($r);
        unset($self_ret);

        $target = $self_sorted_ret[$vote_id];
        $field_name = json_decode($target['field_name'], TRUE);
        !is_array($field_name) && $field_name = array();

        $self_val = $target['self_val'];
        $self_val = json_decode($self_val, TRUE);
        !is_array($self_val) && $self_val = array();

        if (($target['type'] == 2 && $target['img_type'] == 3) || $target['type'] == 1) {	//多图 或 文字
            $sorted_field_name = array();
            foreach ($field_name as $f) {
                $sorted_field_name[$f['key']] = $f;
            }unset($f);

            $self_val_new = array();
            if (!empty($self_val)) {
                foreach ($self_val as $s) {
                    $self_val_new[] = $sorted_field_name[$s];
                }unset($s);
                $info['self_val'] = $self_val_new;
            }
            $self_val = $self_val_new;
            $target['type'] == 2 && $val['first_img_url'] = MEDIA_URL_PREFFIX . $field_name[0]['img_url'];	//图片返回首张图片
            $info['self_val'] = $self_val_new;

        } else {																			//点赞或评分
            $info['first_img_url'] = MEDIA_URL_PREFFIX . $field_name[0]['img_url'];	//图片返回首张图片
            $info['self_val'] = $self_val;
        }


        $param = array(
			'vote_id' 	=> $vote_id,
			'is_single' => $info['is_single'],
			'max_select' => $info['max_select'],
			'max_type' => $info['max_type'],
			'is_real' 	=> $info['is_real'],
			'end_time' 	=> $info['end_time'],
			'with_user' => $with_user,
		);

		$key_str = g('des') -> encode(json_encode($param));

		global $app, $module, $action;
		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m='.$module.'&a='.$action.'&vote_id='.$vote_id;
		$get_attendant_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=103&key='.$key_str;
		$attend_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=101&key='.$key_str;

		$time = time();
		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('get_attendant_url', $get_attendant_url);
		g('smarty') -> assign('attend_url', $attend_url);
		g('smarty') -> assign('info', $info);
		g('smarty') -> assign('server_time', $time);

		$use_dir = '';
		if ($info['status'] == 3) {
			$use_dir = 'end';
		}elseif ($info['status'] == 2) {
			$use_dir = 'underway';
		}else {
			$use_dir = 'unproceed';
		}
		$filename = '';
		if ($info['type'] == 1) {
			$filename = 'vote_text.html';
		}elseif ($info['img_type'] == 1) {
			$filename = 'vote_thumbs_img.html';
		}elseif ($info['img_type'] == 2) {
			$filename = 'vote_star_img.html';
		}elseif ($info['img_type'] == 3) {
			$filename = 'vote_more_img.html';
		}

		g('smarty') -> assign('MEDIA_URL_PREFFIX', MEDIA_URL_PREFFIX);
		g('smarty') -> assign('use_dir', $use_dir);

		g('smarty') -> show($this -> temp_path.$filename);
	}
}

/* End of this file */