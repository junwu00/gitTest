<?php
/**
 * 全部民主投票ajax行为的交互类
 *
 * @author LiangJianMing
 * @create 2014-12-12
 */
class cls_ajax extends abs_app_base {
	public function __construct() {
		parent::__construct('vote');
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)get_var_value('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				//参与投票
				$this -> attend();
				BREAK;
			}
			case 102 : {
				//获取投票活动列表
				$this -> get_vote_list();
				BREAK;
			}
			case 103 : {
				//获取已参加的人员列表
				$this -> get_attendant();
				BREAK;
			}
			default : {
				//非法的请求
				cls_resp::echo_resp(cls_resp::$ForbidReq);
				BREAK;
			}
		}
	}

	/**
	 * 用户参与投票
	 *
	 * @access public
	 * @return void
	 */
	public function attend() {
		$key = get_var_value('key');
		empty($key) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');

		$conf = g('des') -> decode($key);
		$conf_data = json_decode($conf, TRUE);
		empty($conf_data) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');

		$vote_id = (int)$conf_data['vote_id'];
		$is_single = (int)$conf_data['is_single'];
		$max_select = (int)$conf_data['max_select'];
		$max_type = (int)$conf_data['max_type'];

		empty($vote_id) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');

		$fields = array(
			'field_val',
		);

		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$field_val = $data['field_val'];

		// if (!is_array($field_val) || empty($field_val)) {
		// 	cls_resp::echo_err(cls_resp::$Busy, '非法的参数');
		// }

		if ($is_single && count($field_val) != 1) {
			cls_resp::echo_err(cls_resp::$Busy, '该投票只允许单选');
		}

		if (!$is_single) {
			if ($max_select != 0 and count($field_val) > $max_select) {
				cls_resp::echo_err(cls_resp::$Busy, "该投票最多允许选择{$max_select}项");
			}

			if ($max_type == 1 and count($field_val) != $max_select) {
				cls_resp::echo_err(cls_resp::$Busy, "该投票必须选择{$max_select}项");	
			}
		}

		$args = array(
			'vote_id' 	=> $vote_id,
			'field_val' => json_encode($field_val),
		);

		$check = g('vote') -> check_visible($vote_id);
		empty($check) && cls_resp::show_err_page(array('无权查看该投票！'));

		try {
			$result = g('vote') -> attend($args);
			cls_resp::echo_ok();
		}catch(SCException $e) {
			cls_resp::echo_busy();
		}
	}

	/**
	 * 获取可见的投票列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_vote_list() {
		$fields = array(
			'vote_type',
		);

		try {
			$data = $this -> get_post_data($fields);
		}catch(SCException $e) {
			cls_resp::echo_exp($e);
		}

		$vote_type = (int)$data['vote_type'];
		$page = (int)$data['page'];

		//1-未开始、2-进行中、3-已结束
		if ($vote_type != 1 && $vote_type != 2 && $vote_type != 3) {
			cls_resp::echo_err(cls_resp::$Busy, '非法的参数');
		}

		$page < 1 && $page = 1;
		$page_size = 10;

		$condition = array(
			'status=' => $vote_type,
		);
		$with_result = TRUE;
		$vote_type == 1 && $with_result = FALSE;

		$list = g('vote') -> get_list($condition, $page, $page_size, '', $with_result);
		!is_array($list) && $list = array(
			'data' => array(),
			'total' => 0,
		);

		$vote_ids = array();
		foreach ($list['data'] as &$val) {
			$vote_ids[] = $val['vote_id'];
			isset($val['create_time']) && $val['create_time'] = date('Y-m-d H:i:s', $val['create_time']);
			isset($val['begin_time']) && $val['begin_time'] = date('Y-m-d H:i:s', $val['begin_time']);
			isset($val['end_time']) && $val['end_time'] = date('Y-m-d H:i:s', $val['end_time']);
		}
		unset($val);
		
		//整个自己的投票结果
		$self_ret = g('vote') -> list_self_vote_result($vote_ids);
		$self_sorted_ret = array();
		foreach ($self_ret as $r) {
			$self_sorted_ret[$r['vote_id']] = $r;
		}unset($r);
		unset($self_ret);
		
		foreach ($list['data'] as &$val) {
			$target = $self_sorted_ret[$val['vote_id']];
			$field_name = json_decode($target['field_name'], TRUE);
			!is_array($field_name) && $field_name = array();
			
			$self_val = $target['self_val'];
			$self_val = json_decode($self_val, TRUE);
			!is_array($self_val) && $self_val = array();
			
			if (($target['type'] == 2 && $target['img_type'] == 3) || $target['type'] == 1) {	//多图 或 文字
				$sorted_field_name = array();
				foreach ($field_name as $f) {
					$sorted_field_name[$f['key']] = $f;
				}unset($f);
				
				$self_val_new = array();
				if (!empty($self_val)) {
					foreach ($self_val as $s) {
						$self_val_new[] = $sorted_field_name[$s];
					}unset($s);
					$val['self_val'] = $self_val_new;
				}
				$self_val = $self_val_new;
				$target['type'] == 2 && $val['first_img_url'] = MEDIA_URL_PREFFIX . $field_name[0]['img_url'];	//图片返回首张图片
				$val['self_val'] = $self_val_new;
				
			} else {																			//点赞或评分
				$val['first_img_url'] = MEDIA_URL_PREFFIX . $field_name[0]['img_url'];	//图片返回首张图片
				$val['self_val'] = $self_val;
			}

		}
		unset($val);
		
		cls_resp::echo_ok(NULL, 'list', $list['data']);
	}

	/**
	 * 获取投票的参加人员列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_attendant() {
		$key = get_var_value('key');
		empty($key) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');

		$conf = g('des') -> decode($key);
		$conf_data = json_decode($conf, TRUE);
		empty($conf_data) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');

		$vote_id = (int)$conf_data['vote_id'];
		$with_user = (int)$conf_data['with_user'];

		empty($vote_id) && cls_resp::echo_err(cls_resp::$Busy, '非法的请求');
		empty($with_user) && cls_resp::echo_ok(NULL, 'list', array());

		$page = (int)get_var_value('page');

		$page < 1 && $page = 1;
		$page_size = 10;

		$list = g('vote') -> get_attendant($vote_id, 0, $page, $page_size);
		!is_array($list) && $list = array();

		foreach ($list as &$val) {
			isset($val['create_time']) && $val['create_time'] = date('Y-m-d H:i:s', $val['create_time']);
			isset($val['begin_time']) && $val['begin_time'] = date('Y-m-d H:i:s', $val['begin_time']);
			isset($val['end_time']) && $val['end_time'] = date('Y-m-d H:i:s', $val['end_time']);
		}
		unset($val);

		$list = array_values($list);
		$conf = g('vote') -> get_conf_by_id($vote_id, 'type, field_name');
		$filed_map = array();
		if ($conf) {
			$field_name = json_decode($conf['field_name'], TRUE);
			!is_array($field_name) && $field_name = array();
			$field = $conf['type'] == 1 ? 'text' : 'img_url';	//1文字类/2图片类
			foreach ($field_name as $f) {
				$filed_map[$f['key']] = $f[$field];
			}unset($f);
		}

		cls_resp::echo_ok(NULL, 'data', array('list' => $list, 'field_map' => $filed_map));
	}
}

/* End of this file */