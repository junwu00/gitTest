<?php
/**
 * 投票列表控制器
 *
 * @author LiangJianMing
 * @create 2015-1-3
 */

class cls_list extends abs_app_base {
	public function __construct() {
		parent::__construct('vote');
	}
	
	/**
	 * 查看投票活动列表
	 *
	 * @access public
	 * @return void
	 */
	public function show() {
		$vote_type = (int)get_var_value('vote_type');

		$vote_type != 1 && $vote_type != 2 && $vote_type != 3 && $vote_type = 2;

		$page = 1;
		$page_size = 10;

		$condition = array(
			'status=' => $vote_type,
		);

		$with_result = TRUE;
		$vote_type == 1 && $with_result = FALSE;

		$list = g('vote') -> get_list($condition, $page, $page_size, '', $with_result);

		global $app, $module, $action;

		$this_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m='.$module.'&a='.$action;
		$get_list_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=ajax&cmd=102';
		$info_url = SYSTEM_HTTP_DOMAIN.'index.php?app='.$app.'&m=info&a=show';

		g('smarty') -> assign('list', $list['data']);
		g('smarty') -> assign('total', $list['total']);
		g('smarty') -> assign('vote_type', $vote_type);

		g('smarty') -> assign('this_url', $this_url);
		g('smarty') -> assign('get_list_url', $get_list_url);
		g('smarty') -> assign('info_url', $info_url);
		g('smarty') -> assign('server_time', time());

		g('smarty') -> assign('MEDIA_URL_PREFFIX', MEDIA_URL_PREFFIX);

		$filename = 'vote_list.html';
		g('smarty') -> show($this -> temp_path.$filename);
	}
}