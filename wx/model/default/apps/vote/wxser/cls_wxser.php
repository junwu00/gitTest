<?php
/**
 * 投票
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『民主投票』操作指引',
		'desc' => '欢迎使用民主投票应用，在这里你可以随时随地进行投票，结果实时展示，看看哪个才是你的菜吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213971359&idx=2&sn=c2e1cdd7064a3d271e0190533818708e&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
		    //lyc
			//echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	//--------------------------------------内部实现---------------------------
	
}

// end of file