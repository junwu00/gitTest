var proc_type = 3;
var page = 0;

$(document).ready(function() {
	//init_type_change();
	load_lists();
	
	init_scroll_load('.container', '#rec-list', load_lists);
	$('.first').click(function(){
		location.href = $('#app-url').val() + '&m=mv_list&a=get_mine_list&is_finish=0';
	});
	
	$('.middle').click(function(){
		location.href = $('#app-url').val() + '&m=mv_list&a=get_mine_list&is_finish=1';
	});
	
	$('.last').click(function(){
		location.href = $('#app-url').val() + '&m=mv_list&a=get_mine_list&is_finish=2';
	});
	
	$(document).click(function(evt){
		 if($(evt.toElement)[0].id == 'sort'){
			 return false;
		 }
		$("#sort_div").hide();
		$("#sort_finish_div").hide();
		$("#sort_draft_div").hide();
	});
	
	
	$("#search_input").unbind('input').bind('input', function() {
		page = 0;
		load_lists();
	});
	
});

//加载审批列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.is_finish = $('.active').attr('t');
	data.page = page;
	data.process_name = $("#search_input").val();
	data.sort_val =$("#sort_val").val();
	data.sort_order = $("#sort_order").val();
	$('#ajax-url').val($('#get_list_post').val());
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'get_mine_list', JSON.stringify(data), show_list);
}

//显示审批列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		var time_text = '';
		var finish_text = '';
		var finish_class = '';
		
		switch(Number(item['state'])){
			case 0:
				finish_text = '草稿';
				finish_class = 'tag-gray';
				break;
			case 1:
				finish_text = '运行中';
				finish_class = 'tag-green';
				break;
			case 2:
				finish_text = '已结束';
				finish_class = 'tag-blue';
				break;
			case 3:
				finish_text = '已终止';
				finish_class = 'tag-orange';
				break;
		}
		
		html += '<div class="list-item" onclick="show_detail('+item['id']+')">';
		html += '<div class="item-body">';
		html += '<div class="item-detail">';
		html += '<p class="formsetinst_name">'+item['formsetinst_name']+'</p>';
		html += '<p class="formsetinst_name_time">';
		html += '<span style="color: #9b9b9b;font-size: 12px;display: inline-block;padding-right: 15px;">'+time2date(item['create_time'],'yyyy-MM-dd HH:mm')+'</span>';
		html += '<span style="color: #9b9b9b;font-size: 12px">'+item['form_name']+'</span>';
		html += '</p>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
	
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}


function show_detail(formsetinst_id) {
	location.href = $('#app-url').val() + '&m=mv_open&a=mine_detail&formsetinit_id=' + formsetinst_id ;
}

function get_sort_div(obj)
{
	if($('.active').attr('t')==0){
		if($("#sort_div").css("display")=="none"){
			 $("#sort_div").css("top",$(obj).offset().top+30);
			 $("#sort_div").css("left",$(obj).offset().left-50);
			 $("#sort_div").css("display","block");
		}
		else{
			 $("#sort_div").css("display","none");
		}
	}
	else if($('.active').attr('t')==1){
		if($("#sort_finish_div").css("display")=="none"){
			 $("#sort_finish_div").css("top",$(obj).offset().top+30);
			 $("#sort_finish_div").css("left",$(obj).offset().left-50);
			 $("#sort_finish_div").css("display","block");
		}
		else{
			 $("#sort_finish_div").css("display","none");
		}
	}
	else if($('.active').attr('t')==2){
		if($("#sort_draft_div").css("display")=="none"){
			 $("#sort_draft_div").css("top",$(obj).offset().top+30);
			 $("#sort_draft_div").css("left",$(obj).offset().left-50);
			 $("#sort_draft_div").css("display","block");
		}
		else{
			 $("#sort_draft_div").css("display","none");
		}
	}
}

function sort_list(obj){
	$(".sort_div").removeClass("activediv");
	$(obj).addClass("activediv");
	$("#sort_val").val($(obj).attr("sort_val"));
	$("#sort_order").val($(obj).attr("sort_order"));
	page = 0;
	load_lists();
}