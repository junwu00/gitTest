var curr_type = 1;

$(document).ready(function() {
	big_type_no = $('#type').children('option:selected').attr('t');
	if('tour' == big_type_no){//差旅费标志
		$('#travel-special-field').show();
	}else{
		$('#travel-special-field').hide();
	}
	
	$('#place').mlocal();
	
	init_start_time();
	init_end_time();
	init_produce_time();
	init_cancel();
	init_do_apply();
	init_do_del();
	
	init_upload();
	
	init_table();
});


var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			/*
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			*/
			
			currUploadDiv = $('<div class="css_file_div js_file_div" data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info css_file_name">' + fileName + '</div>'+
							'<span class="mupload-notice" data-new=1>开始扫描文件</span>'+
						'</div>');
			$("#file_describe").after(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('.mupload-notice[data-new=1]').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('.mupload-notice[data-new=1]').html('正在上传');
		},
		process		: function(percent) {
			$('.mupload-notice[data-new=1]').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	
    		var is_exist = false;
        	var file_key = data.hash;
        	$('.js_file_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
        	
        	
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('filekey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('fext', data.file_ext);
			
			var file_ext = data.file_ext;
    		
			var optBtn = '<span class="delete_btn hide" onclick="removeFile($(this).parents(\'.js_file_div\'));">删除</span>';
    		if (isImg == 1) {
    			$(currUploadDiv).find('.file_info').before('<img style="width: 33px;height: 23px" src="'+mediaUrl+data.path+'">');
    			//optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		else{
    			$(currUploadDiv).find('.file_info').before('<div style="width: 32px;height: 32px;display: inline-block;position: relative;top: 5px;background-color: #91bef3;color: #fff;text-align: center;">'+file_ext[0].toUpperCase( )+'</div>');
    		}
    		currUploadDiv.append('<span class="file_action_open" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="open_action(this)"><span class="glyphicon glyphicon-chevron-right"></span></span>');
    		currUploadDiv.append('<span class="file_action_close hide" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="close_action(this)"><span class="glyphicon glyphicon-chevron-down"></span></span>');
    		$("#file_num").html('(' + $('.file_info').length + ')');
    		$('.mupload-notice[data-new=1]').html(optBtn);
    		$('.mupload-notice[data-new=1]').attr('data-hash',file_key);
    		$('.mupload-notice[data-new=1]').attr('data-new',0);
    		currUploadDiv.after('<div class="css_file_div file_action file_del hide" data-name="'+data.file_name+'" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="del(\'mupload-notice\',\''+file_key+'\')"><span>删除</span></div>')
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});
	
	//插入预览图
	$('.preview_img').each(function() {
		var target = $(this).parents('.file_div').find('.file_info');
		var html = '<div class="swiper-slide" data-file="' + $(target).attr('fname') + '">' +
			'<img src="' + mediaUrl + ($(target).attr('furl'))+ '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
		'</div>';
		mupload.mupload('appendPreview', $(html));		//插入到前面
	});
}

//删除文件
function removeFile(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	$(fileBox).remove();
	mupload.mupload('removePreview', targetIdx);
	$("#file_num").html('(' + $('.file_info').length + ')');
}

//滚动到目标预览图
function slideTotarget(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	mupload.mupload('slideToPreview', targetIdx, 1000, fileName);
}


//初始化差旅费开始时间
function init_start_time() {
	init_datetime_scroll('#start-time');
	$('#start-time').unbind('change').bind('change', function() {
		get_length();
	});
}

//初始化差旅费结束时间
function init_end_time() {
	init_datetime_scroll('#end-time');
	$('#end-time').unbind('change').bind('change', function() {
		get_length();
	});
}


//差旅费报销开始时间和结束时间逻辑判断
function get_length() {
	var start_time = $('#start-time').val();
	var end_time = $('#end-time').val();
	if (start_time != '' && end_time != '') {
		var start_day = new Date(start_time.replace(/-(\d*)-/, '/$1/'));
		var end_day = new Date(end_time.replace(/-(\d*)-/, '/$1/'));
		var hours = Math.ceil((end_day.getTime() - start_day.getTime()));
		if (hours <= 0) {
			frame_obj.alert('结束时间不能小于开始时间');
			$('#end-time').val('');
		}
	}
}

//申请
function init_do_apply() {
	$('#apply').unbind('click').bind('click', function() {
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		
		var is_returnback = $("#is_returnback").val();
		if(is_returnback==1){//退回
			var data = get_apply_data();
			if (check_apply_data(data)) {
				$('#callback-dlg').modal('show'); 
			}
		}
		else{
			$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
			var data = get_apply_data();
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要发送该流程到下一步吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'发送中……');
			}
		}
	});
	$('#save').unbind('click').bind('click', function() {
		console.log(mupload.mupload('hasUploading'));
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			console.log(mupload);
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		var btn = $(this);
			//frame_obj.comfirm('确定要保存该表单吗？', function() {});
		frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), to_return_back_list,undefined,undefined,undefined,'保存中……');
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		window.location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id+"&formsetInst_id="+data.formsetInst_id+"&list=1";
	} else {
		frame_obj.alert(data.errmsg);
	}
}

function to_list(data) {
	if (data.errcode == 0) {
		window.location.href=$('#app-url').val() + '&m=mv_list&a=get_mine_list&is_finish=2';
	}else {
		frame_obj.alert(data.errmsg);
	}
}

function to_return_back_list(data){
	if (data.errcode == 0) {
		var url = $('#return_list_url').val();
		if(url.indexOf("get_mine_list")>-1){
			url+="&is_finish=2";
		}
		window.location.href=url;
	}else {
		frame_obj.alert(data.errmsg);
	}
}

function to_return_back_list_not_data(){
	var url = $('#return_list_url').val();
	if(url.indexOf("get_mine_list")>-1){
		url+="&is_finish=2";
	}
	window.location.href=url;
}

//重构页面数据,组件提交数据
function get_apply_data() {
	if('' == big_type_no){
		frame_obj.alert('请先选择费用类型');
		return;
	}
	var data = new Object();
	
	data.form_id = $('#proc_form_id').val();
	data.work_id = $('#proc_work_id').val();
	data.judgement = "";
	data.form_name = $('#form_name').val();
	data.workitem_id = $('#workitem_id').val();
	data.formsetInst_id = $('#formsetInst_id').val();
	
	
	data.type = $('#type').val();
	data.name = $('#type').find("option:selected").text();
	if('tour' == big_type_no){//差旅费
		data.place = $('#place').attr('data');
		data.place_name = $('#place').val();
		data.start_time = $('#start-time').val();
		data.end_time = $('#end-time').val();
	}
	else{
		data.place = '';
		data.place_name ='';
		data.start_time = '';
		data.end_time = '';
	}
	data.reason = $('#main-reason').val();
	data.sum_money = $('#sum-money').val();
	data.sum_money_capital = $('#sum-money-capital').val();
	
	data.rec = table_arr;
	
	//附件信息
	data.files = new Array();
	
	$('.js_file_div .file_info ').each(function() {
		var item = new Object();
		item.file_name = $(this).attr("fname");
		item.file_key = $(this).attr("filekey");
		item.file_url = $(this).attr("furl");
		item.file_ext = $(this).attr("fext");
		data.files.push(item);
	});
	
	return data;
}

//校验数据合法性
function check_apply_data(data) {
	if (!check_data(data.reason, 'notnull')) {
		frame_obj.alert('请填写报销事由');
		return false;
	}
	
	if (!check_data(data.type, 'notnull')) {
		frame_obj.alert('请先选择费用类型');
		return false;
	}
	
	if('tour' == big_type_no){
		if (!check_data(data.place, 'notnull')) {
			frame_obj.alert('请填写出差地');
			return false;
		}
		if (!check_data(data.start_time, 'notnull')) {
			frame_obj.alert('请填写开始时间');
			return false;
		}
		if (!check_data(data.end_time, 'notnull')) {
			frame_obj.alert('请填写结束时间');
			return false;
		}
		if (!check_data(data.reason, 'notnull')) {
			frame_obj.alert('请填写出差事由');
			return false;
		}
	}
	
	var rec = data.rec;
	if (rec.length == 0) {
		frame_obj.alert('请填写费用详细支出');
		return false;
	}
	for ( var i = 0; i < rec.length; i++) {
		var item = rec[i];
		if (!check_data(item.type, 'notnull')) {
			frame_obj.alert('请选择详细支出' +　(i+1) + '支出费用类型');
			return false;
		}
		
		if (!check_data(item.produce_time, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '产生费用时间');
			return false;
		}
		
		if (!check_data(item.money, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '金额');
			return false;
		}
		
		if (!check_data(item.reason, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '支出事由');
			return false;
		}
	}
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	
	return true;
}

function init_cancel() {
	$('#cancel').unbind('click').bind('click', function() {
		window.location.href=$("#app-url").val()+"&m=mv_list&a=get_mine_list&is_finish=2";
	});
}

//删除
function init_do_del() {
	$('#del').unbind('click').bind('click', function() {
		var data = new Object();
		data.workitem_id = $('#workitem_id').val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.comfirm('确定要删除该工作项？', function() {
			frame_obj.do_ajax_post(btn, 'del_workitem', JSON.stringify(data), del_complete,undefined,undefined,undefined,'删除中……');
		});
	});
}

function del_complete(data) {
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_return_back_list_not_data);
		to_return_back_list_not_data();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}

function DX(num) {
	var strOutput = "";  
  	var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';  
 	num += "00";  
  	var intPos = num.indexOf('.');  
  	if (intPos >= 0) {
  		num = num.substring(0, intPos) + num.substr(intPos + 1, 2);  
  	} 
    	
  	strUnit = strUnit.substr(strUnit.length - num.length);  
  	for (var i=0; i < num.length; i++){
  		strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i,1),1) + strUnit.substr(i,1); 
  	}  
    return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");  
}

function download_file(id,url){
	var u = url.toLowerCase();
	var formsetinst_id = $("#formsetInst_id").val();
	var data = new Object();
	data.id = id;
	data.formsetinst_id = formsetinst_id;
	data.workitem_id = $("#workitem_id").val();
	data.notify_id = "";
	data.deal_type = 4;
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	$(".load_img").css("display","block");
	frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);

}

function download_complete(data){
	if (data.errcode == 0) {
		close_win();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}

function close_win(){
	wx.closeWindow();
}


function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_del[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

function del(class_key,hash) {
	console.log('del');
	$('.' + class_key +'[data-hash='+hash+']').find('span')[0].click();
	$(".file_action[data-hash='"+hash+"']").remove();
}

//改变报销类型，差旅费特殊处理
//差旅费：place，start-time，end-time
function typeChange(el){
	var value = $(el).val();
	var t = $(el).children('option[selected]').attr('t');
	big_type_no = t;
	if('tour' == t){//差旅费标志
		$('#travel-special-field').show();
	}else{
		$('#travel-special-field').hide();
	}
	
	//清理已经填的数据
	table_arr = new Array();
	change_total_money();
	set_table_html();
	
	//异步处理小类数据
	var data = new Object();
	data.type = value;
	$('#ajax-url').val($('#page_url').val());
	frame_obj.do_ajax_post(undefined, 'list_sub', JSON.stringify(data), show_list);
	
}

//处理异步查询回来数据渲染报销小类
function show_list(data){
	if (data.errcode == 0) {//0表示异步请求成功
		types = data.info;
		if (types.length == 0) {
			$('#type').val('');
			frame_obj.alert("未配置费用子类型");
			return;
		}
	
	} else {
		$frame.tips(data.errmsg,1);
	}
}


function init_table(){
	$(".js_detail_add").each(function() {
		$(this).unbind('click').bind('click', function() {
			if($('#type').val()==''){
				frame_obj.alert('请先选择费用类型');
				return;
			}
			get_table_td_edit(-1);
		});
	});
}

function get_table_td_edit(row_num){
	var table_tr_obj = table_arr[row_num];
	var size = 0;//确定第几条明细
	if(table_tr_obj===undefined){
		size = table_arr.length;//确定第几条明细
	}
	else{
		size = row_num;
	}
	
	var htmlstr="";
	htmlstr +='<div class="detailed-div-select">' + 
			'<span class="input-label">支出费用类型</span>' +
			'<div class="sc-select"><select name="detail_select">'+
				'</select>'+
			'</div><span class="right_"></span>'+
		'</div>'+
		'<div class="detailed-div">'+
			'<span class="input-label">产生费用时间</span>'+
			'<input type="text" name="produce_time" placeholder=" 请选择产生费用时间" style="text-align:right" value="';
			if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.produce_time;}
	htmlstr +='"/><span class="right_" style="top:6px"></span>'+
		'</div>'+
		'<div class="detailed-div">'+
			'<span class="input-label">金额 </span>'+
			'<input type="number" name="money" tmpValue="';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.money;}
	htmlstr +='" placeholder=" 请选择费用金额" value="';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.money;}
	htmlstr +='"/>'+
		'</div>'+
		'<div class="detailed-div-textarea">'+
			'<div class="css_input_link"><div class="css_input_title"><span>详细支出事由</span></div><div class="css_input_describe" style="margin:0 0 10px"></div></div>'+
			'<textarea rows="" cols="" placeholder="请输入详细支出事由..." name="reason">';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.reason;}
	htmlstr +='</textarea>'+
		'</div>';
	var detail_div='<div class="js_detailed detailed" style="border-top: 1px dashed #ddd"><div class="css_inputs js_inputs" style="background-color: #f4f4f4;">'+
		'<input type="hidden" id="table_edit_num" value="'+row_num+'"/>'+
		'<div class="css_table_title">'+
		'<div style="padding-top: 8px;">'+
			'<span style="" >报销明细详情'+(size+1)+'</span>'+
			'<span class="glyphicon glyphicon-remove" style="font-size:20px;color:#979797;float:right" onclick="cancelDiv()"></span>&nbsp;&nbsp;'+
		'</div>'+
	'</div>'+
	'</div>';
	
	
	if(row_num==-1){//新建
		$("#del_table_tr").css("display","none");
		$("#save_table_tr").css("display","none");
		$("#new_save_table_tr").css("display","");
		$("#del").css("display","none");
		$("#cancel").css("display","none");
		$("#save").css("display","none");
		$("#apply").css("display","none");
	}
	else{
		$("#del_table_tr").css("display","");
		$("#save_table_tr").css("display","");
		$("#new_save_table_tr").css("display","none");
		$("#del").css("display","none");
		$("#cancel").css("display","none");
		$("#save").css("display","none");
		$("#apply").css("display","none");
	}
	
	htmlstr = detail_div+htmlstr+"</div>";
	var height = $(".container").height();
	$("#table_add_div").css("top",height);
	$("#table_add_div").html(htmlstr);
	$("#table_add_div").animate({top:'0px'},"slow",function(){
		$(".container").css("display","none");
		scroll(0,0);
	});
	init_produce_time();
	
	//绑定报销小类
	var select = $('#table_add_div select[name="detail_select"]');
	select.empty();//清空
	var select_type = -1;
	if(table_tr_obj!==undefined){select_type = table_tr_obj.type;}
	for(var j = 0; j < types.length; j++){
		if(select_type==-1&&j == 0){
			select.append('<option selected="selected" value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		} 
		else if(types[j]['id'] == select_type){
			select.append('<option selected="selected" value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		}
		else{
			select.append('<option value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		}
	}
}


//初始化产生费用时间
function init_produce_time() {
	var produce_time = $('#table_add_div').find('input[name="produce_time"]');
	init_datetime_scroll(produce_time);
}


function init_datetime_scroll(element) {
  var opt = {};
	opt.datetime = { preset : 'date', stepMinute: 1};

	$(element).val($(element).val()).scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};

//自动合计汇总
function change_total_money(callback){
	var sum = 0;
	
	for(var i = 0; i < table_arr.length; i++){
		sum += (Number(table_arr[i].money));
	}
	sum = sum.toString().replace(/^([\d]+)(\.\d{1,2})?(\d)*$/, '$1$2');
	if(!check_data(sum,'money')&&sum!=0){
		frame_obj.alert('报销金额过大，请分批报销');
		
		if (typeof(callback) === 'function') {
			callback();
		}
		return false;
	}
	$('#sum-money').val(sum);
	
	$('#sum-money-capital').val(DX(sum))
}

//取消
function cancelDiv(){
	$("#del_table_tr").css("display","none");
	$("#save_table_tr").css("display","none");
	$("#new_save_table_tr").css("display","none");
	$("#del").css("display","");
	$("#cancel").css("display","");
	$("#save").css("display","");
	$("#apply").css("display","");
	$(".container").css("display","");
	//scroll(0,0);
	$("#table_add_div").animate({top:'100%'},"slow",function(){
		$("#table_add_div").html('');
	});
}

//删除
function removeDiv(){
	frame_obj.comfirm('确定要删除明细？', function() {
		var table_edit_num = $("#table_edit_num").val();
		table_arr.splice(table_edit_num,1);
		change_total_money();
		set_table_html();
		cancelDiv();
	});
}

//保存
function saveDiv(){
	var table_edit_num = $("#table_edit_num").val();
	
	var item = new Object();
	item.type = $("#table_add_div").find("select[name='detail_select']").val();
	item.name = $("#table_add_div").find("select[name='detail_select']").find("option:selected").text();
	item.produce_time = $("#table_add_div").find("input[name='produce_time']").val();
	item.money = $("#table_add_div").find("input[name='money']").val();
	item.reason = $("#table_add_div").find("textarea[name='reason']").val();
	
	if(!checkDiv(item)){
		return false;
	}
	
	if(table_edit_num ==-1){//新增
		table_arr.push(item);
	}
	else{
		table_arr[table_edit_num]=item;
	}
	change_total_money();
	set_table_html();
	cancelDiv();
}
//子表插入数据检查
function checkDiv(item){
	var is_check = true;
	if (!check_data(item.type, 'notnull')) {
		frame_obj.alert('请选择支出费用类型');
		is_check = false;
		return false;
	}
	if (!check_data(item.produce_time, 'notnull')) {
		frame_obj.alert('请选择产生费用时间');
		is_check = false;
		return false;
	}
	if (!check_data(item.money, 'notnull')) {
		frame_obj.alert('请填写金额');
		is_check = false;
		return false;
	}
	if(!check_data(item.money, 'money')){
		frame_obj.alert('输入非法金额，请重新输入!');
		is_check = false;
		return false;
	}
	if (!check_data(item.reason, 'notnull')) {
		frame_obj.alert('请选择详细支出事由');
		is_check = false;
		return false;
	}
	return is_check;
}
//插入子表数据
function set_table_html(){
	var table_html = $(".css_son_table");
	var temp_html = "<tr>";
	temp_html +="<tr>";
	temp_html +="<th>支出费用类型<em>*</em></th><th>产生费用时间<em>*</em></th><th>金额<em>*</em></th><th>详细支出事由<em>*</em></th>"
	temp_html +="<th style='min-width:80px'></th>";
	temp_html += "</tr>";
	for(var i=0;i<table_arr.length;i++){
		var item = table_arr[i];
		temp_html += "<tr>";
		temp_html +="<td>"+item.name+"</td>";
		temp_html +="<td>"+item.produce_time+"</td>";
		temp_html +="<td>"+item.money+"</td>";
		temp_html +="<td>"+item.reason.replace(/\n/g,'<br>')+"</td>";
		temp_html +="<td style='min-width:80px;text-align:center;color:#80c269' onclick='get_table_td_edit("+i+")'>编辑</td>";
		temp_html += "</tr>";
	}
	table_html.html(temp_html);
}