var proc_type = 3;
var page = 0;

$(document).ready(function() {
	//init_type_change();
	load_lists();
	
	init_scroll_load('.container', '#rec-list', load_lists);
	
	
	
	$('.first').click(function(){
		location.href = $('#app-url').val() + '&m=mv_list&a=get_do_list&is_finish=0';
	});
	
	$('.last').click(function(){
		location.href = $('#app-url').val() + '&m=mv_list&a=get_do_list&is_finish=1';
	});
	
	$(document).click(function(evt){
		 if($(evt.toElement)[0].id == 'sort'){
			 return false;
		 }
		$("#sort_div").hide();
		$("#sort_finish_div").hide();
	});
	
	
	$("#search_input").unbind('input').bind('input', function() {
		page = 0;
		load_lists();
	});
	
});

//未处理、已处理 切换
function init_type_change() {
	$('.head ul li').each(function() {
		$(this).unbind('click').bind('click', function() {
			proc_type = $(this).attr('t');
			$('.head ul li').removeClass('active');
			$(this).addClass('active');
			
			page = 0;
			frame_obj.lock_screen('加载中...');
			load_lists();
			frame_obj.unlock_screen();
		});
	});
}

//加载审批列表
function load_lists() {
	page += 1;
	
	var data = new Object();
	data.is_finish = $('.active').attr('t');
	data.page = page;
	data.process_name = $("#search_input").val();
	data.sort_val =$("#sort_val").val();
	data.sort_order = $("#sort_order").val();
	$('#ajax-url').val($('#get_list_post').val());
	is_scroll_loading = true;
	frame_obj.do_ajax_post(undefined, 'get_do_list', JSON.stringify(data), show_list);
}

//显示审批列表
function show_list(data) {
	if (data.errcode != 0) {
		frame_obj.alert(data.errmsg);
		return;
	}
	var list = data.info;
	scroll_load_complete('#rec-list', list.length);	//in scroll-load.html  
	
	if (page == 1) {
		$('#rec-list').html('');
	}
	var html = '';
	for (var i in list) {
		var item = list[i];
		var time_text = '';
		var time_str = '';
		var finish_text = '';
		var finish_class = '';
		if($('.active').attr('t')==1){
			time_text = '完成时间';
			time_str = time2date(item['complete_time'],'yyyy-MM-dd HH:mm');
			switch(Number(item['formsetinst_state'])){
				case 1:
					finish_text = '运行中';
					finish_class = 'tag-green';
					break;
				case 2:
					finish_text = '已结束';
					finish_class = 'tag-blue';
					break;
				case 3:
					finish_text = '已终止';
					finish_class = 'tag-orange';
					break;
			}
		}else{
			time_text = '收到时间';
			time_str = time2date(item['receive_time'],'yyyy-MM-dd HH:mm');
			switch(Number(item['state'])){
				case 1:
					finish_text = '新收到';
					finish_class = 'tag-orange';
					break;
				case 2:
					finish_text = '已阅读';
					finish_class = 'tag-blue';
					break;
				case 3:
					finish_text = '已保存';
					finish_class = 'tag-green';
					break;
			}
		}
		
		html += '<div class="list-item" onclick="show_detail('+item['id']+')">';
		html += '<div class="item-body">';
		html += '<div class="item-img">'; 
		html += '<img src="'+item['pic_url']+'64">';
		html += '<p style="color: #9b9b9b;font-size: 12px !important;text-align: center;line-height: 18px;">'+item['creater_name']+'</p>';
		html += '</div>';
		html += '<div class="item-detail">';
		html += '<p class="formsetinst_name">'+item['formsetinst_name']+'</p>';
		html += '<p class="formsetinst_name_time">';
		html += '<span style="color: #9b9b9b;font-size: 12px;display: inline-block;padding-right: 15px;">'+time_str+'</span>';
		html += '<span style="color: #9b9b9b;font-size: 12px">'+item['form_name']+'</span>';
		html += '</p>';
		html += '</div>';
		html += '<span class="item-state '+ finish_class +'">' + finish_text + '</span>';
		html += '</div>';
		html += '</div>';
	}
	$('#rec-list').append(html);
	
	if ($('.list-item').length == 0) {
		has_no_record('#rec-list');
	}
}

function show_detail(workitem_id) {
	location.href = $('#app-url').val() + '&m=mv_open&a=detail&workitem_id=' + workitem_id ;
}


function get_sort_div(obj)
{
	if($('.active').attr('t')==0){
		if($("#sort_div").css("display")=="none"){
			 $("#sort_div").css("top",$(obj).offset().top+30);
			 $("#sort_div").css("left",$(obj).offset().left-50);
			 $("#sort_div").css("display","block");
		}
		else{
			 $("#sort_div").css("display","none");
		}
	}
	else if($('.active').attr('t')==1){
		if($("#sort_finish_div").css("display")=="none"){
			 $("#sort_finish_div").css("top",$(obj).offset().top+30);
			 $("#sort_finish_div").css("left",$(obj).offset().left-50);
			 $("#sort_finish_div").css("display","block");
		}
		else{
			 $("#sort_finish_div").css("display","none");
		}
	}
}

function sort_list(obj){
	$(".sort_div").removeClass("activediv");
	$(obj).addClass("activediv");
	$("#sort_val").val($(obj).attr("sort_val"));
	$("#sort_order").val($(obj).attr("sort_order"));
	page = 0;
	load_lists();
}