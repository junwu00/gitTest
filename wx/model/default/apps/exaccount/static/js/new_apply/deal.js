var table_arr = new Array();
var work_type = $("#work_type").val();

$(document).ready(function() {
	inst_apply_html();
	table_arr = dataObj["input4"]["rec"];
	$('span.js_a_cnt').html('(' + $('.process-appoval .js_a_div').length + ')');
	$('span.js_n_cnt').html('(' + $('.process-appoval .js_n_div').length + ')');
	$('span.js_w_cnt').html('(' + $('.process-appoval .js_w_div').length + ')');
	
	init_table();
	init_upload();
	inst_approval();
	
	init_back_dlg();
	init_do_apply();
	init_send_back();
	init_call_back();
	init_do_del();
	init_do_press();
	init_do_notify();
	init_info_button();
	
	$('.select_work').click(function (){
		$('.select_work').addClass("icon-circle-blank");
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});
	
	$('.select_callback').click(function (){
		$('.select_callback').addClass("icon-circle-blank");
		$(this).toggleClass("icon-circle-blank");
		$(this).css("color","green");
	});
	
	$(".workitem_list").unbind('click').bind('click', function() {
		var url = $("#app-url").val()+'&m=mv_open&a=get_mv_pic&formsetinst_id='+$("#formsetinst_id").val()+"&deal_type="+$("#deal_type").val();
		window.location.href=url;
	});
	
});

function inst_approval(){
	$('#pre_back').unbind('click').bind('click', function() {
		$("#disagreelabel").css("display","inline-block");
		$("#agreelabel").css("display","none");
		
		$("#pre_agree").css("display","none");
		$("#pre_back").css("display","none");
		$("#linksign_b").css("display","none");
		$("#back").css("display","block");
		$("#cancel").css("display","block");
		
		$('#cancel').unbind('click').bind('click', function() {
			$("#back").css("display","none");
			$("#agree").css("display","none");
			$("#cancel").css("display","none");
			$("#pre_agree").css("display","block");
			$("#pre_back").css("display","block");
			$("#linksign_b").css("display","block");
			$(".process-appoval").css("display","block");
			$(".process-words").css("display","block");
			$(".process-info").css("display","block");
			$(".process-judgement").css("display","none");
			$(".process-judgement").css("top","100%");
			$("#powerby").css("display","block");
		});
		
		$(".process-judgement").css("display","block");
		$("#powerby").css("display","none");
		$(".process-judgement").css("top","0");
		$(".process-appoval").css("display","none");
		$(".process-words").css("display","none");
		$(".process-info").css("display","none");
		$("#judgement").val('');
		if(work_type==0){
			$("#judgement").attr('placeholder','请输入退回意见……');
			$("#judgement").focus().val("不同意");
		}
		else{
			$("#judgement").attr('placeholder','请输入备注信息(选填)');
		}
		/*
		$('.process-judgement').animate({top: '0'}, 'slow', function() {
			$(".process-appoval").css("display","none");
			$(".process-words").css("display","none");
			$(".process-info").css("display","none");
			$("#judgement").attr('placeholder','请输入退回意见……');
			$("#judgement").val('');
			setTimeout("$('#judgement').focus()", 3000 );
			
		});
		*/
		//$("#judgement").focus();
	});
	$('#pre_agree').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if(!check_apply_data(data,1)){
			return false;
		}
		
		$("#disagreelabel").css("display","none");
		$("#agreelabel").css("display","inline-block");
		
		$("#pre_agree").css("display","none");
		$("#pre_back").css("display","none");
		$("#agree").css("display","block");
		$("#cancel").css("display","block");
		$('#cancel').unbind('click').bind('click', function() {
			$("#back").css("display","none");
			$("#agree").css("display","none");
			$("#cancel").css("display","none");
			$("#pre_agree").css("display","block");
			$("#pre_back").css("display","block");
			$(".process-appoval").css("display","block");
			$(".process-words").css("display","block");
			$(".process-info").css("display","block");
			$(".process-judgement").css("display","none");
			$(".process-judgement").css("top","100%");
			$("#powerby").css("display","block");
		});
		$(".process-judgement").css("display","block");
		$("#powerby").css("display","none");
		$(".process-judgement").css("top","0");
		$(".process-appoval").css("display","none");
		$(".process-words").css("display","none");
		$(".process-info").css("display","none");
		$("#judgement").val('');
		if(work_type==0){
			$("#judgement").attr('placeholder','请输入同意意见……');
			$("#judgement").focus().val("同意");
		}
		else{
			$("#judgement").attr('placeholder','请输入备注信息(选填)');
		}
		
	});
	$('#linksign_b').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if(!check_apply_data(data,1)){
			return false;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), to_linksign,undefined,undefined,undefined,'保存中……');
	});
}

function to_linksign(data){
	var data = new Object();
	data.workitem_id = $('#workitem_id').val();
	data.formsetinst_id = $('#formsetinst_id').val();
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	frame_obj.do_ajax_post(undefined, 'open_linksign', JSON.stringify(data),get_redirect_url ,undefined,undefined,undefined,'保存中……');
	
	function get_redirect_url(data){
		if(data["errcode"]===0){
			location.href=data["redirect_url"];
		}
		else{
			frame_obj.alert(data.errmsg);
		}
	}
}


var mupload;
function init_upload(){
	var mediaUrl = $('#media-url').val();
	var currUploadDiv;
	mupload = $('#file_upload_file').mupload({
		form		: '#formToUpload',
		swiperEl	: '.swiper-container',
		mediaUrl	: $('#media-url').val(),
		checkUrl	: $('#unify_check_url').val(),
		uploadUrl	: $('#unify_upload_url').val(),
//		partSize	: 1024,
		scaning		: function(filePath, fileName, isImg) {
			/*
			var exists = false;
			$('.file_div').each(function() {
				if ($(this).attr('data-path') == filePath) {
					exists = true;
				}
			});
			if (exists) {
				mupload.mupload('_showTips', '该文件与已上传的文件相同');
				return false;
			}
			*/
			
			currUploadDiv = $('<div class="css_file_div js_file_div" data-img="' + isImg + '" data-file="' + fileName + '" data-path="' + filePath + '">'+
							'<div fname="' + fileName + '" class="file_info css_upload_file_name">' + fileName + '</div>'+
							'<span class="mupload-notice" data-new=1>开始扫描文件</span>'+
						'</div>');
			$("#file_describe").after(currUploadDiv);
			return true;
		},
		scaned		: function() {
			$('.mupload-notice[data-new=1]').html('扫描成功');
		},
		preview		: function(fileName, url) {
			var html = '<div class="swiper-slide" data-file="' + fileName + '">' +
				'<img src="' + mediaUrl + url + '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
			'</div>';
			mupload.mupload('prependPreview', $(html));		//插入到前面
		},
		startUpload	: function() {
			$('.mupload-notice[data-new=1]').html('正在上传');
		},
		process		: function(percent) {
			$('.mupload-notice[data-new=1]').html('上传中 ' + percent + '%');
		},
		success		: function(data, isImg) {
			$('#file_upload_file').val("");
        	$('.load_img').hide();
        	currUploadDiv.find('div').css({'max-width':'90%'});
    		var is_exist = false;
        	var file_key = data.hash;
        	$('.js_file_div .file_info').each(function() {
				var exist_key = $(this).attr("filekey");
				if(exist_key == file_key) {
					mupload.mupload('_showTips', "该文件与已上传的文件相同");
					is_exist = true;
				}
			});
        	
        	
			if(is_exist){
				removeFile(currUploadDiv);
				return false;
			}

			$(currUploadDiv).find('.file_info').attr('fname', data.file_name);
			$(currUploadDiv).find('.file_info').attr('filekey', file_key);
			$(currUploadDiv).find('.file_info').attr('furl', data.path);
			$(currUploadDiv).find('.file_info').attr('fext', data.file_ext);
			$(currUploadDiv).attr('file_name', data.file_name);
			$(currUploadDiv).attr('file_key', file_key);
			$(currUploadDiv).attr('file_url', data.path);
			$(currUploadDiv).attr('file_ext', data.file_ext);
			$(currUploadDiv).attr('file_node_id', $("#work_node_id").val());
			$(currUploadDiv).attr('file_create_id', $("#handler_id").val());
			
			var file_ext = data.file_ext;
    		
    		var optBtn = '<span class="delete_btn hide" onclick="removeFile($(this).parents(\'.file_div\'));">删除</span>';
    		if (isImg == 1) {
    			$(currUploadDiv).find('.file_info').before('<img style="width: 33px;height: 23px;position: absolute;top: 9px" src="'+mediaUrl+data.path+'">');
    			//optBtn += '<span class="preview_btn" onclick="slideTotarget($(this).parents(\'.file_div\'));">预览</span>';
    		}
    		else{
    			$(currUploadDiv).find('.file_info').before('<div style="width: 32px;height: 32px;display: inline-block;position: absolute;top: 5px;background-color: #91bef3;color: #fff;text-align: center;">'+file_ext[0].toUpperCase( )+'</div>');
    		}
    		currUploadDiv.append('<span class="file_action_open" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="open_action(this)"><span class="glyphicon glyphicon-chevron-right"></span></span>');
    		currUploadDiv.append('<span class="file_action_close hide" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="close_action(this)"><span class="glyphicon glyphicon-chevron-down"></span></span>');
    		$("#file_num").html('(' + $('.file_info').length + ')');
    		$('.mupload-notice[data-new=1]').html(optBtn);
    		$('.mupload-notice[data-new=1]').attr('data-hash',file_key);
    		$('.mupload-notice[data-new=1]').attr('data-new',0);
    		currUploadDiv.after('<div class="css_file_div file_action file_del hide" data-name="'+data.file_name+'" data-ext="'+data.file_ext+'" data-hash="'+file_key+'" onclick="remove_file(this)"><span>删除</span></div>')
		},
		error		: function(resp) {
			$(currUploadDiv).remove();
        	$('#file_upload_file').val("");
        	$('#file_upload_file').mupload('_showTips', resp.errmsg);
		}
	});
	
	//插入预览图
	$('.preview_img').each(function() {
		var target = $(this).parents('.file_div').find('.file_info');
		var html = '<div class="swiper-slide" data-file="' + $(target).attr('fname') + '">' +
			'<img src="' + mediaUrl + ($(target).attr('furl'))+ '" style="max-width: 100%; max-height: 100%; vertical-align: middle; display: inline-block;">' +
		'</div>';
		mupload.mupload('appendPreview', $(html));		//插入到前面
	});
}


function init_back_dlg() {
	$('#back').unbind('click').bind('click', function() {
		var data = get_apply_data();
		if (check_back_data(data)) {
			$('#back-dlg').modal('show');
		}
	});
}

function init_send_back() {
	$('#back-ok').unbind('click').bind('click', function() {
		var send_type=0;
		$(".select_work").each(function(){
			if(!$(this).hasClass("icon-circle-blank")){
				send_type = $(this).attr("val");
			}
		});
		if(send_type==1){
			var msg = "开始步骤";
			var method = 'callback_start';
		}
		else{
			var msg = "上一步骤";
			var method = 'callback_pre';
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		if (check_back_data(data)) {
			var btn = $(this);
			$('#back-dlg').modal('hide');
			//frame_obj.comfirm('确定要驳回当前工作项到'+msg+'吗？', function() {});
			frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback_complete,undefined,undefined,undefined,'退回中……')
				}
			);
		}
	});
}



function init_call_back() {
	$('#callback-ok').unbind('click').bind('click', function() {
		var send_type=0;
		$(".select_callback").each(function(){
			if(!$(this).hasClass("icon-circle-blank")){
				send_type = $(this).attr("val");
			}
		});
		if(send_type==1){
			var msg = "退回步骤";
			var method = 'sendback';
			var callback = sendback_complete;
		}
		else{
			var msg = "下一步骤";
			var method = 'save_workitem';
			var callback = apply_complete;
		}
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var data = get_apply_data();
		$('#callback-dlg').modal('hide');
		if($("#is_start").val()==0){//开始步骤不用填审批信息
			if (!check_apply_data(data)) {
				return false;
			}
			var btn = $(this);
			//frame_obj.comfirm('确定要发送当前工作项到'+msg+'吗？', function() {});
			if(send_type==1){
				frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……')
				});
			}
			else{
				frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……');
			}
		}
		else{
			var btn = $(this);
			//frame_obj.comfirm('确定要发送当前工作项到'+msg+'吗？', function() {});
			if(send_type==1){
				frame_obj.do_ajax_post(btn, "save_workitem", JSON.stringify(data), function(){
					frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……')
				});
			}
			else{
				frame_obj.do_ajax_post(btn, method, JSON.stringify(data), callback,undefined,undefined,undefined,'发送中……');
			}
		}
		
	});
}


//申请
function init_do_apply() {
	$('#agree').unbind('click').bind('click', function() {
		var is_returnback = $("#is_returnback").val();
		if(is_returnback==1){
			var data = get_apply_data();
			if (check_apply_data(data)) {
				$('#callback-dlg').modal('show'); 
			}
		}
		else{
			$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
			var data = get_apply_data();
			if (check_apply_data(data)) {
				var btn = $(this);
				//frame_obj.comfirm('确定要发送该工作项到下一步吗？', function() {});
				frame_obj.do_ajax_post(btn, 'save_workitem', JSON.stringify(data), apply_complete,undefined,undefined,undefined,'发送中……');
			}
		}
	});
}

function apply_complete(data) {
	if (data.errcode == 0) {
		//alert($("#app-url").val()+"&m=mv_send&a=send_page&workitemid="+data.workitem_id);
		location.href=$("#app-url").val()+"&m=mv_open&a=send_page&workitemid="+data.workitem_id;
	} else {
		frame_obj.alert(data.errmsg);
	}
}

//删除
function init_do_del() {
	$('#del').unbind('click').bind('click', function() {
		var data = new Object();
		data.workitem_id = $("#workitem_id").val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		frame_obj.comfirm('确定要删除该工作项？', function() {
			frame_obj.do_ajax_post(btn, 'del_workitem', JSON.stringify(data), del_complete,undefined,undefined,undefined,'删除中……');
		});
	});
}

function del_complete(data) {
	if (data.errcode == 0) {
		frame_obj.alert(data.errmsg,'',to_mine_list);
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}



function get_apply_data() {
	var data = new Object();
	data.form_id = "";
	data.work_id = "";
	data.form_name = "";
	data.judgement = $("#judgement").val()?$("#judgement").val():"";
	data.workitem_id = $('#workitem_id').val();
	data.formsetInst_id = $('#formsetinst_id').val();
	
	data.type = dataObj["input1"].type_num;
	data.name = dataObj["input1"].val;
	if(dataObj["input6"]!==undefined){//差旅费
		data.place = dataObj["input6"].num;
		data.place_name = dataObj["input6"].val;
		data.start_time = dataObj["input7"].val;
		data.end_time = dataObj["input8"].val;
	}
	else{
		data.place = '';
		data.place_name ='';
		data.start_time = '';
		data.end_time = '';
	}
	data.reason = dataObj["input5"].val;
	data.sum_money = dataObj["input2"].val;
	data.sum_money_capital = dataObj["input3"].val;
	
	data.rec = table_arr;
	
	//附件信息
	data.files = new Array();
	var handler_id = $("#handler_id").val();
	$('.js_file_div').each(function() {
		var file_node_id = $(this).attr("file_node_id");
		if(file_node_id == $("#work_node_id").val()&&$(this).attr("file_create_id")==handler_id){
			var item = new Object();
			item.file_name = $(this).attr("file_name");
			item.file_key = $(this).attr("file_key");
			item.file_url = $(this).attr("file_url");
			item.file_ext = $(this).attr("file_ext");
			item.node_id = $(this).attr("file_node_id");
			data.files.push(item);
		}
	});
	
	return data;
}


function check_apply_data(data,is_cj) {
	var rec = data.rec;
	if (rec.length == 0) {
		frame_obj.alert('请填写费用详细支出');
		return false;
	}
	for ( var i = 0; i < rec.length; i++) {
		var item = rec[i];
		if (!check_data(item.type, 'notnull')) {
			frame_obj.alert('请选择详细支出' +　(i+1) + '支出费用类型');
			return false;
		}
		
		if (!check_data(item.produce_time, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '产生费用时间');
			return false;
		}
		
		if (!check_data(item.money, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '金额');
			return false;
		}
		
		if (!check_data(item.reason, 'notnull')) {
			frame_obj.alert('请填写详细支出' +　(i+1) + '支出事由');
			return false;
		}
	}
	
	if($("#is_start").val()==0&&is_cj===undefined){//开始步骤不用填审批信息
		if(!check_data(data.judgement, 'notnull')&&work_type==0){
			frame_obj.alert('请填写审批意见');
			return false;
		}
	}
	return true;
}

function check_file_data(data){
	if($("#file_upload_type").val()==2){//必须上传附件
		if(data.files.length==0){
			frame_obj.alert('请上传附件');
			return false;
		}
	}
	return true;
}


function check_back_data(data) {
	if (!check_data(data.judgement, 'notnull')&&work_type==0) {
		frame_obj.alert('请填写审批意见');
		return false;
	}
	return true;
}


//退回后返回，多个步骤的时候，弹出选择退回
function callback_complete(data){
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_list);
		to_list();
	}
	else if (data.errcode == 1) {//弹出选择退回
		//alert($('#callback-cancel').attr("class"));
		$('#back-dlg').modal('hide');
		
		$("#callback_select-dlg").modal('show');
		var callback_workitem = data.callback_workitem;
		var html="";
		for(var i=0;i<callback_workitem.length;i++){
			var date = new Date(callback_workitem[i]["complete_time"]*1000);
			var year = date.getFullYear();
			var month = (date.getMonth() + 1) > 10 ? (date.getMonth() + 1) : "0"+ (date.getMonth() + 1);
			var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
			var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			var datestr = year+"-"+month+"-"+day+" "+hour+":"+minute
			html+="<span class='icon-ok-sign icon-circle-blank select_people' style='font-size: 25px;color: green' workitem_id='"+callback_workitem[i]["id"]+"' workitem_name='"+callback_workitem[i]["workitem_name"]+"' handler_name='"+callback_workitem[i]["handler_name"]+"'></span>&nbsp;&nbsp;["+callback_workitem[i]["workitem_name"]+"]"+callback_workitem[i]["handler_name"]+"于"+datestr+"发送<br>";
		}
		
		$("#callback_select-dlg .modal-body").html(html);
		
		$('.select_people').click(function (){
			$('.select_people').addClass("icon-circle-blank");
			$(this).toggleClass("icon-circle-blank");
			$(this).css("color","green");
		});
		$('#callback_select-ok').unbind('click').bind('click', function() {
			var handler_name ="";
			var workitem_name = "";
			var workitem_id = "";
			var count = 0;
			$(".select_people").each(function(){
				if(!$(this).hasClass("icon-circle-blank")){
					workitem_id = $(this).attr("workitem_id");
					workitem_name = $(this).attr("workitem_name");
					handler_name = $(this).attr("handler_name");
					count++;
				}
			});
			if(count!=1){
				frame_obj.alert('请选择退回的工作项');
				return false;
			}
			var data = new Object();
			data.workitem_id = $("#workitem_id").val();
			data.callback_workitem_id = workitem_id;
			data.judgement = $("#judgement").val()?$("#judgement").val():"";
			var btn = $(this);
			$("#callback_select-dlg").modal('hide');
			//frame_obj.comfirm('确定要发送当前工作项到上一步骤 '+handler_name+' 吗？', function() {});
			frame_obj.do_ajax_post(btn, "callback_select", JSON.stringify(data), sendback_complete,undefined,undefined,undefined,'发送中……');
		});
	}
	else{
		frame_obj.alert(data.errmsg,'');
	}
}


function sendback_complete(data){
	if (data.errcode == 0) {
		//frame_obj.alert(data.errmsg,'',to_list);
		to_list();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}


//催办
function init_do_press() {
	$('#press').unbind('click').bind('click', function() {
		var data = new Object();
		data.formsetinst_id = $("#formsetinst_id").val();
		$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
		var btn = $(this);
		//frame_obj.comfirm('确定要催办该工作项？', function() {});
		frame_obj.do_ajax_post(btn, 'press_workitem', JSON.stringify(data), press_complete,undefined,undefined,undefined,'催办中……');
	});
}

function press_complete(data) {
	frame_obj.alert(data.errmsg);
}


//知会
function init_do_notify() {
	$('#notify').unbind('click').bind('click', function() {
		//frame_obj.comfirm('确定要知会该工作项？', function() {});
		location.href=$("#app-url").val()+"&m=mv_open&a=notify_page&formsetinst_id="+$("#formsetinst_id").val()+'&workitem_id=' + $("#workitem_id").val();;
	});
}


function to_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_do_list';
}

function to_mine_list(){
	window.location.href=$('#app-url').val() + '&m=mv_list&a=get_mine_list';
}

//初始化产生费用时间
function init_produce_time() {
	var produce_time = $('#table_add_div').find('input[name="produce_time"]');
	init_datetime_scroll(produce_time);
}


function init_datetime_scroll(element) {
  var opt = {};
	opt.datetime = { preset : 'date', stepMinute: 1};

	$(element).val($(element).val()).scroller('destroy').scroller(
		$.extend(opt['datetime'], 
		{ 
			theme: 'android-ics light', 
			mode: 'scroller',
			display: 'bottom', 
			lang: 'zh',
		})
	);
};


function download_file(id,url){
	var u = url.toLowerCase();
	var formsetinst_id = $("#formsetinst_id").val();
	var data = new Object();
	data.id = id;
	data.formsetinst_id = formsetinst_id;
	data.workitem_id = $("#workitem_id").val();
	data.notify_id = "";
	data.deal_type = $("#deal_type").val();
	$("#ajax-url").val($("#app-url").val()+"&m=mv_open");
	$(".load_img").css("display","block");
	frame_obj.do_ajax_post($(this), 'download_file', JSON.stringify(data), download_complete);
	//$("#download_div").attr("src",url);
}

function download_complete(data){
	//$(".load_img").css("display","none");
	if (data.errcode == 0) {
		close_win();
	}
	else{ 
		frame_obj.alert(data.errmsg,'');
	}
}

function close_win(){
	wx.closeWindow();
}

function remove_file(that){
	if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
		mupload.mupload('_showTips', '当前还有文件正在上传');
		return;
	}
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	$(".js_file_div[file_key='" + hash + "']").remove();
	$(".file_action[data-hash='" + hash + "']").remove();
	$("#file_num").html('(' + $('.file_info').length + ')');
	save_workitem();
}

//滚动到目标预览图
function slideTotarget(fileBox) {
	var fileName = $(fileBox).attr('data-file');
	var idx = 0;
	var targetIdx = 0;
	$('.swiper-container .swiper-slide').each(function() {
		if ($(this).attr('data-file') == fileName) {
			targetIdx = idx;
		}
		idx++;
	});
	mupload.mupload('slideToPreview', targetIdx, 1000, fileName);
}

function open_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	$(".file_action_close").addClass('hide');
	$(".file_action").addClass('hide');
	$(".file_action_open").removeClass('hide');
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action_close[data-hash='" + hash + "']").removeClass('hide');
	$(".file_action.file_down[data-hash='" + hash + "']").removeClass('hide');
	if(MS_document_pre.is_pre($(that).attr('data-ext')) || image_pre.is_pre($(that).attr('data-ext'))){
		$(".file_action.file_pre[data-hash='" + hash + "']").removeClass('hide');
	}
	$(".file_action.file_del[data-hash='" + hash + "']").removeClass('hide');
}

function close_action(that){
	if (event.stopPropagation) { 
		event.stopPropagation(); 
	}
	var hash = $(that).attr('data-hash');
	$(that).addClass('hide');
	$(".file_action[data-hash='" + hash + "']").addClass('hide');
	$(".file_action_open[data-hash='" + hash + "']").removeClass('hide');
}

function file_pre(that){
	var ext = $(that).attr('data-ext');
	var name = $(that).attr('data-name');
	var hash = $(that).attr('data-hash')
	if(MS_document_pre.is_pre(ext)){
		MS_document_pre.init_pre(ext,hash);
	}else if(image_pre.is_pre(ext)){
		image_pre.init_pre(ext,hash);
	}else{
		
	}
}

//初始化页面控件
function inst_apply_html(){
	$.each(dataObj,function(key,obj){
		var input_html = inst_input_html(obj);
		$("#input_content").append(input_html);
	});
	
	$(".js_edit_div").unbind("click").bind("click",function(){
		if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
			mupload.mupload('_showTips', '当前还有文件正在上传');
			return;
		}
		var input_id = $(this).parents(".css_row").attr("id");
		get_input_edit(input_id);
	});
	
}

function inst_input_html(obj){
	var html = '';
	if(obj.type=="table"){
		html = '<div  id="'+obj.input_key+'">';
	}
	else{
		html = '<div class="css_row" id="'+obj.input_key+'">';
	}
	if(obj.input_key=="input2"||obj.input_key=="input3"||obj.input_key=="input1"){//金额 大写 大类特殊处理
		html +='<div class="css_input_deal_label">'+obj.name;
		html +='</div>';
		
		if(obj.val===undefined){
			obj.val="";
		}
		html +='<div class="css_input_deal_div js_input_deal_val">'+obj.val;
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type!="table"){
		html +='<div class="css_input_deal_label">'+obj.name;
		if(obj.edit_input==1&&(state==1||state==2||state==3)&&obj.must == 1){html +='<em>*</em>';}
		html +='</div>';
		
		if(obj.val===undefined){
			obj.val="";
		}
		var input_val = obj.val.replace(/\n/g,'<br>');
		if(obj.edit_input==1&&(state==1||state==2||state==3)){
			html +='<div class="css_input_deal_div js_edit_div">'+input_val+"<span class='css_edit_label glyphicon glyphicon-edit'></span>";
		}
		else{
			html +='<div class="css_input_deal_div">'+input_val;
		}
		html +='</div>';
		html +='<div style="clear:both"></div>';
	}
	else if(obj.type=="table"){
		html +='<div class=" css_table_title"><span style="position: relative;top: 7px">'+obj.name;
		if(obj.edit_input==1&&(state==1||state==2||state==3)&&obj.must == 1){html +='<em>*</em>';}
		html +='</span>';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){
			html+='<span class="glyphicon glyphicon-plus-sign js_detail_add" input_key = "'+obj.input_key+'" style="font-size: 26px;padding-top: 3px;float:right;color:#80C269"></span>';
		}
		html +='</div>';
		html +='<div style="overflow-x:auto;width: '+(s_width-11)+'px ;" class="son_div">';
		
		html +='<table class="css_son_table" input_key = "'+obj.input_key+'" >';
		//表头
		var th_html = '<tr>';
		th_html+='<th>支出费用类型';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){th_html+='<em>*</em>';}
		th_html+='</th><th>产生费用时间';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){th_html+='<em>*</em>';}
		th_html+='</th><th>金额';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){th_html+='<em>*</em>';}
		th_html+='</th><th>详细支出事由';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){th_html+='<em>*</em>';}
		th_html+='</th>';
		if((obj.edit_input==1)&&(state==1||state==2||state==3)){
			th_html+='<th style="min-width:80px"></th>';
		}
		th_html+='</tr>';
		html +=th_html;
		
		if(obj.rec!==undefined && obj.rec.length>0){
			var htmlstr= "";
			for(var j =0;j<obj.rec.length;j++){
				var item = obj.rec[j];
				htmlstr+="<tr class='js_tr'>";
				htmlstr+= "<td>"+item.name+"</td>";
				htmlstr+= "<td>"+item.produce_time+"</td>";
				htmlstr+= "<td>"+item.money+"</td>";
				htmlstr+= "<td>"+item.reason.replace(/\n/g,'<br>');+"</td>";
				if((obj.edit_input==1)&&(state==1||state==2||state==3)){
					htmlstr+="<td style='min-width:80px;text-align:center;color:#80c269' onclick='get_table_td_edit(\""+obj.input_key+"\","+j+")'>";
					htmlstr+="编辑";
					htmlstr+="</td>";
				}
				htmlstr+="</tr>";
			}
			
			html +=htmlstr;
			
		}
		
		html +='</table></div>';
	}
			
		
	html +='</div>';
		
	return html;

}

function DX(num) {
	var strOutput = "";  
  	var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';  
 	num += "00";  
  	var intPos = num.indexOf('.');  
  	if (intPos >= 0) {
  		num = num.substring(0, intPos) + num.substr(intPos + 1, 2);  
  	} 
    	
  	strUnit = strUnit.substr(strUnit.length - num.length);  
  	for (var i=0; i < num.length; i++){
  		strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i,1),1) + strUnit.substr(i,1); 
  	}  
    return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");  
}


function init_table(){
	$(".js_detail_add").each(function() {
		$(this).unbind('click').bind('click', function() {
			var input_key = $(this).attr("input_key");
			get_table_td_edit(input_key,-1)
		});
	});
}


function get_table_td_edit(input_key,row_num){
	if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
		mupload.mupload('_showTips', '当前还有文件正在上传');
		return;
	}
	var table_tr_obj = table_arr[row_num];
	var size = 0;//确定第几条明细
	if(table_tr_obj===undefined){
		size = table_arr.length;//确定第几条明细
	}
	else{
		size = row_num;
	}
	
	var htmlstr="";
	htmlstr +='<div class="detailed-div-select">' + 
			'<span class="input-label">支出费用类型</span>' +
			'<div class="sc-select"><select name="detail_select">'+
				'</select>'+
			'</div><span class="right_"></span>'+
		'</div>'+
		'<div class="detailed-div">'+
			'<span class="input-label">产生费用时间</span>'+
			'<input type="text" name="produce_time" placeholder=" 请选择产生费用时间" style="text-align:right" value="';
			if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.produce_time;}
	htmlstr +='"/><span class="right_" style="top:6px"></span>'+
		'</div>'+
		'<div class="detailed-div">'+
			'<span class="input-label">金额 </span>'+
			'<input type="number" name="money" tmpValue="';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.money;}
	htmlstr +='" placeholder=" 请选择费用金额" value="';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.money;}
	htmlstr +='"/>'+
		'</div>'+
		'<div class="detailed-div-textarea">'+
			'<div class="css_input_link"><div class="css_input_title"><span>详细支出事由</span></div><div class="css_input_describe" style="margin:0 0 10px"></div></div>'+
			'<textarea rows="" cols="" placeholder="请输入详细支出事由..." name="reason">';
		if(table_tr_obj!==undefined){htmlstr +=table_tr_obj.reason;}
	htmlstr +='</textarea>'+
		'</div>';
	var detail_div='<div class="js_detailed detailed" style="border-top: 1px dashed #ddd"><div class="css_inputs js_inputs" style="background-color: #f4f4f4;">'+
		'<input type="hidden" id="table_edit_num" value="'+row_num+'"/>'+
		'<input type="hidden" id="table_input_key" value="'+input_key+'"/>'+
		'<div class="css_table_title">'+
		'<div style="padding-top: 8px;">'+
			'<span style="" >报销明细详情'+(size+1)+'</span>'+
			'<span class="glyphicon glyphicon-remove" style="font-size:20px;color:#979797;float:right" onclick="cancelDiv()"></span>&nbsp;&nbsp;'+
		'</div>'+
	'</div>'+
	'</div>';
	
	
	if(row_num==-1){//新建
		$("#del_table_tr").css("display","none");
		$("#save_table_tr").css("display","none");
		$("#new_save_table_tr").css("display","");
		$("#del").css("display","none");
		$("#cancel").css("display","none");
		$("#save").css("display","none");
		$("#agree").css("display","none");
	}
	else{
		$("#del_table_tr").css("display","");
		$("#save_table_tr").css("display","");
		$("#new_save_table_tr").css("display","none");
		$("#del").css("display","none");
		$("#cancel").css("display","none");
		$("#agree").css("display","none");
	}
	
	htmlstr = detail_div+htmlstr+"</div>";
	var height = $(".container").height();
	$("#table_add_div").css("top",height);
	$("#table_add_div").html(htmlstr);
	$("#table_add_div").animate({top:'0px'},"slow",function(){
		$(".container").css("display","none");
		scroll(0,0);
	});
	init_produce_time();
	
	//绑定报销小类
	var select = $('#table_add_div select[name="detail_select"]');
	select.empty();//清空
	var select_type = -1;
	if(table_tr_obj!==undefined){select_type = table_tr_obj.type;}
	for(var j = 0; j < types.length; j++){
		if(select_type==-1&&j == 0){
			select.append('<option selected="selected" value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		} 
		else if(types[j]['id'] == select_type){
			select.append('<option selected="selected" value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		}
		else{
			select.append('<option value="'+ types[j]['id'] + '">' + types[j]['name'] + '</option>');
		}
	}
}

//自动合计汇总
function change_total_money(callback){
	var sum = 0;
	
	for(var i = 0; i < table_arr.length; i++){
		sum += (Number(table_arr[i].money));
	}
	sum = sum.toFixed(2);
	sum = sum.toString().replace(/^([\d]+)(\.\d{1,2})?(\d)*$/, '$1$2');
	if(!check_data(sum,'money')&&sum!=0){
		frame_obj.alert('报销金额过大，请分批报销');
		
		if (typeof(callback) === 'function') {
			callback();
		}
		return false;
	}
	dataObj["input2"].val = sum;
	dataObj["input3"].val = DX(sum);
	$('#input2').find(".js_input_deal_val").html(sum);
	$('#input3').find(".js_input_deal_val").html(DX(sum))
}

//取消
function cancelDiv(){
	$("#del_table_tr").css("display","none");
	$("#save_table_tr").css("display","none");
	$("#new_save_table_tr").css("display","none");
	$("#del").css("display","");
	$("#agree").css("display","");
	$("#pre_agree").css("display","");
	$("#pre_back").css("display","");
	$(".container").css("display","");
	//scroll(0,0);
	$("#table_add_div").animate({top:'100%'},"slow",function(){
		$("#table_add_div").html('');
	});
}

//删除
function removeDiv(){
	frame_obj.comfirm('确定要删除明细？', function() {
		var table_edit_num = $("#table_edit_num").val();
		table_arr.splice(table_edit_num,1);
		change_total_money();
		set_table_html();
		cancelDiv();
	});
}

//保存
function saveDiv(){
	var input_key = $("#input_key").val();
	if(input_key===undefined){
		var table_edit_num = $("#table_edit_num").val();
		
		var item = new Object();
		item.type = $("#table_add_div").find("select[name='detail_select']").val();
		item.name = $("#table_add_div").find("select[name='detail_select']").find("option:selected").text();
		item.produce_time = $("#table_add_div").find("input[name='produce_time']").val();
		item.money = $("#table_add_div").find("input[name='money']").val();
		item.reason = $("#table_add_div").find("textarea[name='reason']").val();
		
		if(!checkDiv(item)){
			return false;
		}
		
		if(table_edit_num ==-1){//新增
			table_arr.push(item);
		}
		else{
			table_arr[table_edit_num]=item;
		}
		change_total_money();
		set_table_html();
		save_workitem();
		cancelDiv();
	}
	else{
		var obj = dataObj[input_key];
		var temp_obj = $.extend(true, {}, obj);
		var obj_type = temp_obj.type;
		var obj_key = temp_obj.input_key;
		var edit_input = temp_obj.edit_input;
		if(obj_key=="input5"&&edit_input==1){
			var obj_val = $("textarea[input_key="+obj_key+"]").val();
			temp_obj.val = obj_val;
		}
		else if(obj_key=="input6"&&edit_input==1){
			temp_obj.val =  $('#place').val();
			temp_obj.num = $('#place').attr('data');
		}
		else if(obj_key=="input7"&&edit_input==1){
			var obj_val = $("input[input_key="+obj_key+"]").val();
			temp_obj.val = obj_val;
		}
		else if(obj_key=="input8"&&edit_input==1){
			var obj_val = $("input[input_key="+obj_key+"]").val();
			temp_obj.val = obj_val;
		}
		if(!check_input(temp_obj)){
			return false;
		}
		dataObj[input_key] = temp_obj;
		
		save_workitem();
		set_input_html(input_key);
		cancelDiv();
	}
	
}
//子表插入数据检查
function checkDiv(item){
	var is_check = true;
	if (!check_data(item.type, 'notnull')) {
		frame_obj.alert('请选择支出费用类型');
		is_check = false;
		return false;
	}
	if (!check_data(item.produce_time, 'notnull')) {
		frame_obj.alert('请选择产生费用时间');
		is_check = false;
		return false;
	}
	if (!check_data(item.money, 'notnull')) {
		frame_obj.alert('请填写金额');
		is_check = false;
		return false;
	}
	if(!check_data(item.money, 'money')){
		frame_obj.alert('输入非法金额，请重新输入!');
		is_check = false;
		return false;
	}
	if (!check_data(item.reason, 'notnull')) {
		frame_obj.alert('请选择详细支出事由');
		is_check = false;
		return false;
	}
	return is_check;
}
//插入子表数据
function set_table_html(){
	var table_html = $(".css_son_table");
	var temp_html = "<tr>";
	temp_html +="<tr>";
	temp_html +="<th>支出费用类型<em>*</em></th><th>产生费用时间<em>*</em></th><th>金额<em>*</em></th><th>详细支出事由<em>*</em></th>"
	temp_html +="<th style='min-width:80px'></th>";
	temp_html += "</tr>";
	for(var i=0;i<table_arr.length;i++){
		var item = table_arr[i];
		temp_html += "<tr>";
		temp_html +="<td>"+item.name+"</td>";
		temp_html +="<td>"+item.produce_time+"</td>";
		temp_html +="<td>"+item.money+"</td>";
		temp_html +="<td>"+item.reason.replace(/\n/g,'<br>')+"</td>";
		temp_html +="<td style='min-width:80px;text-align:center;color:#80c269' onclick='get_table_td_edit(\"input4\","+i+")'>编辑</td>";
		temp_html += "</tr>";
	}
	table_html.html(temp_html);
}

function check_input(input){
	var is_check = true;
	if (input.edit_input==1&&input.must == 1 && input.type != 'table' && !check_data(input.val, 'notnull')) {
		frame_obj.alert('请填写' + input.name);
		is_check =  false;
		return false;
	}
	return is_check;
}

function set_input_html(input_key){
	var input_obj = dataObj[input_key];
	$("#"+input_key).find(".js_edit_div").html(input_obj.val.replace(/\n/g,'<br>')+"<span class='css_edit_label glyphicon glyphicon-edit'></span>");
}

function get_input_edit(input_key){

	var input_obj = dataObj[input_key];
	
	var htmlstr="";
	if(input_obj.input_key=="input6"){
		htmlstr +='<div class="js_inputs css_inputs" input_key="'+input_key+'">';
		if(input_obj.val===undefined){
			input_obj.val="";
		}
		htmlstr +='<div class="css_edit_input_info"><input type="text" id="place" input_key="'+input_key+'" class="js_input_val css_input_right" data="'+input_obj.num+'" placeholder="请选择'+input_obj.name+'" /><span class="right_"></span></div>';
		htmlstr +='<div style="clear: both;"></div>';
		htmlstr +='</div>';
	}
	else if(input_obj.input_key=="input7"){
		htmlstr +='<div class="js_inputs css_inputs" input_key="'+input_key+'">';
		if(input_obj.val===undefined){
			input_obj.val="";
		}
		htmlstr +='<div class="css_edit_input_info"><input type="text" input_key="'+input_key+'" class="js_input_val css_input_right js_input_date" placeholder="请选择'+input_obj.name+'" value="'+input_obj.val+'"/><span class="right_"></span></div>';
		htmlstr +='<div style="clear: both;"></div>';
		htmlstr +='</div>';
	}
	else if(input_obj.input_key=="input8"){
		htmlstr +='<div class="js_inputs css_inputs" input_key="'+input_key+'">';
		if(input_obj.val===undefined){
			input_obj.val="";
		}
		htmlstr +='<div class="css_edit_input_info"><input type="text" input_key="'+input_key+'" class="js_input_val css_input_right js_input_date" placeholder="请选择'+input_obj.name+'" value="'+input_obj.val+'"/><span class="right_"></span></div>';
		htmlstr +='<div style="clear: both;"></div>';
		htmlstr +='</div>';
	}
	else if(input_obj.input_key=="input5"){
		htmlstr +='<div class="js_inputs css_inputs" input_key="'+input_key+'">';
		if(input_obj.val===undefined){
			input_obj.val="";
		}
		htmlstr +='<div class="css_input_all_info"><textarea style="border-top:0" input_key="'+input_key+'"  class="js_input_val " placeholder="请输入'+input_obj.name+'">'+input_obj.val+'</textarea></div>';
		htmlstr +='<div style="clear: both;"></div>';
		htmlstr +='</div>';
	}
	var detail_div='<div class=" detailed" style="border-top: 1px dashed #ddd">'+
				'<div class="css_inputs js_inputs" style="background-color: #f4f4f4;">'+
					'<input type="hidden" id="input_key" value="'+input_key+'"/>'+
					'<div class="css_table_title">'+
						'<div style="padding-top: 8px;">'+
							'<span >'+input_obj.name;
							if(input_obj.must == 1){detail_div +='<em>*</em>';}
							detail_div+='</span><span class="glyphicon glyphicon-remove" style="font-size:20px;color:#979797;float:right" onclick="cancelDiv()"></span>&nbsp;&nbsp;'+
						'</div>'+
					'</div>'+
				'</div>';
	if(input_obj.describe!==undefined&&input_obj.describe!=""){
		detail_div+='<div class="css_table_describe_div"><div class="css_table_describe">'+input_obj.describe+'</div></div>';
	}
	
	$("#del_table_tr").css("display","none");
	$("#save_table_tr").css("display","none");
	$("#new_save_table_tr").css("display","");
	$("#pre_agree").css("display","none");
	$("#pre_back").css("display","none");
	$("#del").css("display","none");
	$("#agree").css("display","none");
	
	
	htmlstr = detail_div+htmlstr+"</div>";
	var height = $(".container").height();
	$("#table_add_div").css("top",height);
	$("#table_add_div").html(htmlstr);
	$("#table_add_div").animate({top:'0px'},"slow",function(){
		$(".container").css("display","none");
		scroll(0,0);
	});
	
	$('#place').mlocal();
	init_datetime_scroll(".js_input_date");
}


function save_workitem(){
	if ($('#formToUpload').length > 0 && mupload.mupload('hasUploading')) {
		mupload.mupload('_showTips', '当前还有文件正在上传');
		return;
	}
	$("#ajax-url").val($("#app-url").val()+"&m=mv_send");
	var data = get_apply_data();
	frame_obj.do_ajax_post(undefined, 'save_workitem', JSON.stringify(data), function(){},undefined,undefined,undefined);
}


//处理异步查询回来数据渲染报销小类
function show_list(data){
	if (data.errcode == 0) {//0表示异步请求成功
		types = data.info;
		if (types.length == 0) {
			$('#type').val('');
			frame_obj.alert("未配置费用子类型");
			return;
		}
	
	} else {
		$frame.tips(data.errmsg,1);
	}
}

function init_info_button(){
	$(".js_title_btn").unbind("click").bind("click",function(){
		var s_div = $(this).attr("s_div");
		$(".js_title_btn").removeClass("css_title_active");
		$(this).addClass("css_title_active");
		$(".js_a_divs").css("display","none");
		$(".js_n_divs").css("display","none");
		$(".js_w_divs").css("display","none");
		$("."+s_div).css("display","block");
	});
}

