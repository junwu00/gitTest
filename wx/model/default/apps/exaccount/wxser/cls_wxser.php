<?php
/**
 * 费用报销
 * 
 * @author yangpz
 * @date 2014-12-04
 *
 */
class cls_wxser extends abs_app_wxser {
	/** 操作指引的图文 */
	private $help_art = array(
		'title' => '『费用报销』操作指引',
		'desc' => '欢迎使用费用报销应用，在这里你可以随时随地报销，不用跑去财务部拿几块钱了，统统跟财务同事报销拿钱吧！',
		'pic_url' => '',
		'url' => 'http://mp.weixin.qq.com/s?__biz=MjM5Nzg3OTI5Ng==&mid=213969656&idx=3&sn=4ed6a1eb7c2db9ca5d484a46c8abafec&scene=0#rd'
	);
	
	protected function reply_subscribe() {
		if (!empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}

	//返回操作指引
	protected function reply_text() {
		$text = $this -> post_data['Content'];
		$text = str_replace(' ', '', $text);
		if ($text == '移步到微' && !empty($this -> help_art) && !empty($this -> help_art['url'])) {
			echo  g('wxqy') -> get_news_reply($this -> post_data, array($this -> help_art));
		}
	}
	
	//--------------------------------------内部实现---------------------------
	
}

// end of file