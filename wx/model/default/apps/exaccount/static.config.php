<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	'exaccount_new_apply_apply.css' => array(
		SYSTEM_APPS . 'exaccount/static/css/new_apply/apply.css',
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'exaccount_new_apply_apply.js' => array(
		SYSTEM_ROOT . 'static/js/storage.js',
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/apply.js',
		SYSTEM_ROOT . 'static/js/document-pre/js/MS-document-pre.js',
		SYSTEM_ROOT . 'static/js/jweixin-1.0.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
	),
	'exaccount_new_apply_contact.css' => array(
		SYSTEM_APPS . 'exaccount/static/css/contact.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/square/green.css',
	),
	'exaccount_new_apply_contact.js' => array(
		SYSTEM_ROOT . 'static/js/icheck/icheck.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/contact.js',
	),
	'exaccount_new_apply_deal.css' => array(
		SYSTEM_APPS . 'exaccount/static/css/new_apply/deal.css',
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'exaccount_new_apply_deal.js' => array(
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.js',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
		SYSTEM_ROOT . 'static/js/icheck/icheck.js',
		SYSTEM_ROOT . 'static/js/document-pre/js/MS-document-pre.js',
		SYSTEM_ROOT . 'static/js/jweixin-1.0.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/deal.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
	),
	'exaccount_new_apply_edit.css' => array(
		SYSTEM_APPS . 'exaccount/static/css/new_apply/apply.css',
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'exaccount_new_apply_edit.js' => array(
		SYSTEM_ROOT . 'static/js/mlocal/mobile.local.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
        SYSTEM_ROOT . 'static/js/document-pre/js/MS-document-pre.js',
		SYSTEM_ROOT . 'static/js/jweixin-1.0.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/edit.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
	),
	'exaccount_new_apply_mv_pic.css' => array(
		SYSTEM_APPS . 'exaccount/static/css/new_apply/mv_pic/GooFlow.css',
		SYSTEM_APPS . 'exaccount/static/css/new_apply/mv_pic/default.css'
	),
	'exaccount_new_apply_mv_pic.js' => array(
		SYSTEM_APPS . 'exaccount/static/js/new_apply/mv_pic/GooFunc.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/mv_pic/json2.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/mv_pic/GooFlow.js'
	),
	'exaccount_new_apply_notify_deal.css' => array(
		SYSTEM_ROOT . 'static/js/wximgupload/wximgupload.css',
		SYSTEM_APPS . 'exaccount/static/css/new_apply/deal.css',
		SYSTEM_ROOT . 'static/js/icheck/skins/all.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.core-2.5.2.css',
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/css/mobiscroll.android-ics-2.5.2.css',
		//上传
		SYSTEM_ROOT . 'static/js/swiper/css/swiper.min.css',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.css'
	),
	'exaccount_new_apply_notify_deal.js' => array(
		SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.core-2.5.2-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.datetime-2.5.1-zh.js',
        SYSTEM_APPS . 'common/static/js/mobiscroll/dev/js/mobiscroll.android-ics-2.5.2.js',
        SYSTEM_ROOT . 'static/js/icheck/icheck.js',
        SYSTEM_ROOT . 'static/js/document-pre/js/MS-document-pre.js',
		SYSTEM_ROOT . 'static/js/jweixin-1.0.0.js',
		SYSTEM_ROOT . 'static/js/jssdk.js',
		SYSTEM_ROOT . 'static/js/image-pre/js/image-pre.js',
		SYSTEM_APPS . 'exaccount/static/js/new_apply/notify_deal.js',
		//上传
		SYSTEM_ROOT . 'static/js/jquery.form.js',
		SYSTEM_ROOT . 'static/js/spark-md5.js',
		SYSTEM_ROOT . 'static/js/swiper/js/swiper.min.js',
		SYSTEM_ROOT . 'static/js/mupload/mobile.upload.js'
	),
);

return $arr;

/* End of this file */