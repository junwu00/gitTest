<?php
/**
 * @author yangpz
 * @date 2014-12-25
 */
include (SYSTEM_APPS.'exaccount/modules/cls_mv_list_server.php');

class cls_mv_list extends cls_mv_list_server {
	
	public function __construct() {
		parent::__construct('exaccount');
	}
	
	/**
	 * 主页面
	 */
	public function index() {
		$act = get_var_post('ajax_act');
		switch ($act) {	
			case 'get_do_list':
				$this -> get_do_list_post(); 
				break;
			case 'get_mine_list':
				$this -> get_mine_list_post(); 
				break;
			case 'get_notify_list':
				$this -> get_notify_list_post(); 
				break;
			default:
				cls_resp::show_err_page("非法请求！");
		}
	}
	
	
	/**
	 * 获取待办列表
	 */
	public function get_do_list() {
		try {
			$is_finish = get_var_get('is_finish');//待办已办列表
			
			if(empty($is_finish)||$is_finish==0){
				$is_finish = 0;
				g('smarty') -> assign('title', '我的待办');
			}else{
				g('smarty') -> assign('title', '我的已办');
			}
			
			$get_list_post_url = SYSTEM_HTTP_DOMAIN.'index.php?app=exaccount&m=mv_list';
			
			g('smarty') -> assign('get_list_post', $get_list_post_url);
			g('smarty') -> assign('is_finish', $is_finish);
			
			g('smarty') -> show($this -> temp_path.'new_apply/lists.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	/**
	 * 我起草的流程列表
	 */
	public function get_mine_list(){
	try {
			$is_finish = get_var_get('is_finish');//待办已办列表
			
			if(empty($is_finish)||$is_finish==0){
				$is_finish = 0;
				g('smarty') -> assign('title', '运行中');
			}
			else if($is_finish==1){
				g('smarty') -> assign('title', '已结束');
			}
			else{
				g('smarty') -> assign('title', '草稿');
			}
			
			$get_list_post_url = SYSTEM_HTTP_DOMAIN.'index.php?app=exaccount&m=mv_list';
			
			g('smarty') -> assign('get_list_post', $get_list_post_url);
			g('smarty') -> assign('is_finish', $is_finish);
			
			g('smarty') -> show($this -> temp_path.'new_apply/mine.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	/**
	 * 我知会的列表
	 */
	public function get_notify_list(){
		try {
			$is_finish = get_var_get('is_finish');//待办已办列表
			
			if(empty($is_finish)||$is_finish==0){
				$is_finish = 0;
				g('smarty') -> assign('title', '未阅读');
			}
			else{
				g('smarty') -> assign('title', '已阅读');
			}
			
			$get_list_post_url = SYSTEM_HTTP_DOMAIN.'index.php?app=exaccount&m=mv_list';
			g('smarty') -> assign('get_list_post', $get_list_post_url);
			g('smarty') -> assign('is_finish', $is_finish);
			
			
			g('smarty') -> show($this -> temp_path.'new_apply/notify_list.html');
		} catch (SCException $e) {
			cls_resp::show_err_page($e);
		}
	}
	
	
	//----------------------------------------内部实现---------------------------------------
	
	
/**
	 * 异步获取我处理的列表信息
	 */
	private function get_do_list_post(){
		try{
		$data = parent::get_post_data(Array('is_finish','page','process_name','sort_val','sort_order'));//待办已办列表
			if(!isset($data['is_finish'])){
				throw new SCException('请求错误');
			}
			$page = $data['page'];
			if(empty($page)){
				throw new SCException('请求错误');
			}
			
			$condition = Array('formsetinst_name'=>$data['process_name']);
			
			$fields='mpf.formsetinst_name,mpf.create_time,mpfm.form_name,mpf.state formsetinst_state,mpw.*,su.pic_url';
			
			$sort_val = $data['sort_val'];
			$sort_order = $data['sort_order'];
			if($sort_val!="" && $sort_order!=""){
				$sort_val = 'order by '.$sort_val.' '.$sort_order;
			}
			else{
				$sort_val = 'order by receive_time desc';
			}
			
			$list = g("mv_workinst")->get_do_list($condition,$this->ComId,$this->UserId,$data['is_finish'],$this->IsProcType,$page,parent::$PageSize,$fields,$sort_val);
			cls_resp::echo_ok(cls_resp::$OK,'info',$list);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 异步获取我起草的列表信息
	 */
	private function get_mine_list_post(){
		try{
			$data = parent::get_post_data(Array('is_finish','page'));//待办已办列表
			if(!isset($data['is_finish'])){
				throw new SCException('请求错误');
			}
			$page = $data['page'];
			if(empty($page)){
				throw new SCException('请求错误');
			}
			
			$condition = Array('formsetinst_name'=>$data['process_name']);
			
			$fields='mpf.*,mpwn.work_node_name,mpfm.form_name';
			
			$sort_val = $data['sort_val'];
			$sort_order = $data['sort_order'];
			if($sort_val!="" && $sort_order!=""){
				$sort_val = ' order by '.$sort_val.' '.$sort_order;
			}
			else{
				$sort_val = ' order by mpf.create_time desc ';
			}
			
			$list = g("mv_formsetinst")->get_do_list($condition,$this->ComId,$this->UserId,$data['is_finish'],$this->IsProcType,$page,parent::$PageSize,$fields,$sort_val);
			cls_resp::echo_ok(cls_resp::$OK,'info',$list);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	/**
	 * 异步获取我知会的列表信息
	 */
	private function get_notify_list_post(){
		try{
			$data = parent::get_post_data(Array('is_finish','page','process_name','sort_val','sort_order'));//待办已办列表
			if(!isset($data['is_finish'])){
				throw new SCException('请求错误');
			}
			$page = $data['page'];
			if(empty($page)){
				throw new SCException('请求错误');
			}
			
			$condition = Array('formsetinst_name'=>$data['process_name']);
			
			$fields='mpn.*,su.pic_url,mpfm.form_name';
			
			$sort_val = $data['sort_val'];
			$sort_order = $data['sort_order'];
			if($sort_val!="" && $sort_order!=""){
				$sort_val = ' order by '.$sort_val.' '.$sort_order;
			}
			else{
				$sort_val = ' order by mpn.state,mpn.notify_time desc ';
			}
			
			
			$list = g("mv_notify")->get_do_list($condition,$this->ComId,$this->UserId,$data['is_finish'],$this->IsProcType,$page,parent::$PageSize,$fields,$sort_val);
			
			cls_resp::echo_ok(cls_resp::$OK,'info',$list);
		}catch(SCException $e){
			cls_resp::echo_exp($e);
		}
	}
	
	
	
}

//end of file