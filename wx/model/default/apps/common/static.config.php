<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	/** CSS */
	'app_common.css' => array(
		SYSTEM_ROOT . 'apps/common/static/css/common.css'
	),

	/** JS */
	'app_common.js' => array(
		SYSTEM_APPS . 'common/static/js/common.js'
	),
	
	/* --------------------------新版本UI前段资源统一管理-----------------------*/
	/** CSS 新ui版本公共css*/
	'app_new_common.css' => array(
		SYSTEM_ROOT . 'apps/common/static/css/new_common.css'
	),
	
	/** JS 新ui版本公共js*/
	'app_new_common.js' => array(
        SYSTEM_ROOT . 'static/js/bootstrap-3.2.0/dist/js/bootstrap.min.js',
        SYSTEM_ROOT . 'static/js/frame.js',
        SYSTEM_ROOT . 'static/js/format.js',
        SYSTEM_ROOT . 'static/js/check.js',
		SYSTEM_APPS . 'common/static/js/new_common.js'
	),
	
    /** JS 新ui版jssdk应用*/
    'new_jweixin_jssdk.js' => array(
        SYSTEM_ROOT . 'static/js/jweixin-1.1.0.js',
        SYSTEM_ROOT . 'static/js/jssdk.js'
    )
);

return $arr;

/* End of this file */