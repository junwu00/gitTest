var SHAKE_THRESHOLD = 3000;  
var last_update = 0;  
var x = y = z = last_x = last_y = last_z = 0;  
var shake_callback;
var shake_param_obj;

var shake_obj = {
	
	//初始化，包括触发摇动事件
	init:function(callback, param_obj) {
		if (window.DeviceMotionEvent) {  
			shake_callback = callback;
			shake_param_obj = param_obj;
	        window.addEventListener('devicemotion', shake_obj.deviceMotionHandler, false);  
	    } else {  
	        frame_obj.alert('您的手机不支持摇一摇');
	    }
	},
	
	deviceMotionHandler:function(eventData) {
		var acceleration = eventData.accelerationIncludingGravity;  
	    var curTime = new Date().getTime();  
	    if ((curTime - last_update) > 100) {  
	        var diffTime = curTime - last_update;  
	        last_update = curTime;  
	        x = acceleration.x;  
	        y = acceleration.y;  
	        z = acceleration.z;  
	        var speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;  

	        if (speed > SHAKE_THRESHOLD) {  
	        	if (shake_param_obj == undefined) {
	        		shake_callback();
	        	} else {
	        		shake_callback(shake_param_obj);
	        	}
	        }  
	        last_x = x;  
	        last_y = y;  
	        last_z = z;  
	    }
	},
	
}