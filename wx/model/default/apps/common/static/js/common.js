$(document).ready(function() {
	$(window).resize(function(){ 
		
	});
	
	if ($('.dropdown-toggle').length > 0) {
		$('.dropdown-toggle').dropdown();
	}
	init_container_heigth();
	init_power_by();
});

function init_container_heigth(){
	
}

function init_power_by(){
	$('.container').append('<div id="powerby"><a href="javascript:void(0);">' + $('#powerby-val').val() + '</a></div>');
}

function app_auto_hight() {
	var height = $(window).height();
	if ($('#bottom').length > 0) {
		$('.container').css('height', height - 45);
	} else {
		$('.container').css('height', height);
	}
	$('body').css('height', height);
	$('html').css('height', height);
}

//显示：暂时没有内容
function has_no_record(container) {
	var html = '<div class="has-no-record">' +
			'<img src="apps/common/static/images/nhlogo.png?v=20150582801">' +
			'<span>暂时没有内容</span>' +
		'</div>';
	$(container).html(html);
}

//显示：暂时没有内容
function has_no_record_2(container, img_src, guide_str, ext_html) {
	ext_html = typeof(ext_html) === 'undefined' ? '' : ext_html;
	var html = '<div class="has-no-record-2">' +
			'<img src="' + img_src + '" onerror="this.src=\'apps/common/static/images/nhlogo.png?v=20150582801\'">' +
			'<span>' + guide_str + '</span>' +
			ext_html +
		'</div>';
	$(container).html(html);
	if ($('#powerby').length > 0) {
		$('#powerby').remove();
	}
}