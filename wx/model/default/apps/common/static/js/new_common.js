$(document).ready(function() {
	$(window).resize(function(){});
	if ($('.dropdown-toggle').length > 0) {
		$('.dropdown-toggle').dropdown();
	}
	init_power_by();
	hide_mask_screen_head();
});

/* 隐藏页面loading*/
function hide_mask_screen_head(){
	document.getElementById('mask_screen_head').style.display = 'none';
}

/* 显示页面loading*/
function show_mask_screen_head(){
	document.getElementById('mask_screen_head').style.display = 'inline-block';
}

/* 初始换技术支持文案 */
function init_power_by(){
	$('.container').append('<div id="powerby"><a href="javascript:void(0);">' + $('#powerby-val').val() + '</a></div>');
}

//滚动加载初始化高度
function app_auto_hight() {
	var height = $(window).height();
	if ($('#bottom').length > 0) {
		$('.container').css('height', height - 45);
	} else {
		$('.container').css('height', height);
	}
	$('body').css('height', height);
	$('html').css('height', height);
}

//显示：暂时没有内容
function has_no_record(opt) {
	if(!opt.target){
		alert({desc:'函数has_no_record参数没有指定opt.target'});
	}
	if(!opt.target instanceof $){
		alert({desc:'函数has_no_record参数opt.target必须为Jquery对象'});
	}
	var target = opt.target;
	var img_src = opt.img || $.trim($('#app-common').val()) + 'static/images/logo_empty.png';
	var title = opt.title || '暂时没有内容';
	var desc_list = opt.desc_list || [];
	var html = '';
	html += '<div class="has-no-record">';
	if(img_src){
		html += '<img alt="" src="'+ img_src +'">';
	}
	html += '<p class="p1">'+ title +'</p>';
	for(var i = 0; i < desc_list.length; i++){
		html += '<p class="p2">'+ desc_list[i] +'</p>';
	}
	html += '</div>';
	$(target).html(html);
}

/*启动全屏遮罩*/
function lock_screen(msg){
	unlock_screen();
	msg = msg || '处理中';
	unlock_screen();
	var inner_html = '<div id="lock-screen"><div><span class="an-1"></span><span class="an-2"></span><span class="an-3"></span></div><p>' + msg + '</p></div>';
	$('body').append(inner_html);
}

/*取消全屏遮罩*/
function unlock_screen(){
	if ($('#lock-screen').length > 0) {
		$('#lock-screen').remove();
	}
}

/* 成功提示窗口 */
function success_screen(msg,hideTime){
	msg = msg || '成功';
	var inner_html = '<div id="success_screen"><div><i class="icon"></i><p>' + msg + '</p></div></div>';
	$('body').append(inner_html);
	hideTime = hideTime == undefined ? 1000 : hideTime;
	setTimeout(function(){
		$('#success_screen').remove();
	},hideTime);
}

/* 失败提示窗口 */
function fail_screen(msg,hideTime){
	msg = msg || '失败';
	var inner_html = '<div id="fail_screen"><div><i class="icon"></i><p>' + msg + '</p></div></div>';
	$('body').append(inner_html);
	hideTime = hideTime == undefined ? 1500 : hideTime;
	setTimeout(function(){
		$('#fail_screen').remove();
	},hideTime);
}

//头部插件
/*
使用方式
var opt = {};
opt.menu = {'list':[{'name':'运行中','activekey':1,'other_data':{'name1':'12123','name2':'aa'}},{'name':'运行中','activekey':3,'other_data':{'name1':'12123','name2':'aa'}},{'name':'结束','activekey':2,'other_data':{'name1':'12123','name2':'aa'}}],'fun':function(obj,el){console.log(obj);console.log(el);}};
opt.sort = {'list':[{'name':'时间','sort_name':'time'},{'name':'时间1','sort_name':'time1'},{'name':'时间3','sort_name':'time2'}],'fun':function(obj){console.log(obj)}};
opt.search = {'fun':function(key){console.log(key)}};
head_obj.init_head(opt);
head_obj.active_head_menu(2);
*/	
var head_obj = {
	//初始换头部菜单方法
	init_head:function(opt){
		var hide_menu = '';
		var hide_sort = '';
		var hide_search = '';
		if(!opt.menu || opt.menu.list.length == 0){
			hide_menu = 'hide';
		}
		if(!opt.sort || opt.sort.list.length == 0){
			hide_sort = 'hide';
		}
		if(!opt.search){
			hide_search = 'hide';
		}
		opt.hide_menu = hide_menu;
		opt.hide_sort = hide_sort;
		opt.hide_search = hide_search;
		var html_menu = '';
		if(opt.menu && opt.menu.list.length > 0){
			for(var i = 0; i < opt.menu.list.length; i++){
				var menu = opt.menu.list[i];
				var data_str = '';
				if(typeof menu.other_data === "object"){
					for(var j in menu.other_data){
						data_str += ' data-'+j+'="'+menu.other_data[j]+'"';
					}
				}
				html_menu += '<li data-activekey="' + menu.activekey + '" data-name="' + menu.name +'"'+ data_str +'><a href="javascript:void(0);">' + menu.name + '</a></li>';
			}
		}
		var html = '';
		html += '<div class="head">';
		if(hide_menu == ''){
			html += '<div class="head-menu '+ hide_menu +'">';
			html += '<ul>';
			html += html_menu;
			html += '</ul>';
			if(hide_sort == ''){
				html += '<span class="head—sort '+ hide_sort +'">';
				html += '<i is-sort-icon="true" class="icon head—sort-asc hide"></i>';
				html += '<i is-sort-icon="true" class="icon head—sort-desc hide"></i>';
				html += '<i class="icon head—sort-icon"></i>';
				html += '</span>';
				head_obj.init_sort(opt.sort.list);
			}
			html += '</div>';
		}
		if(hide_search == ''){
			html += '<div class="search-div">';
			html += '<form>';
			html += '<input AUTOCOMPLETE ="off" type="search"  placeholder="搜索">';
			html += '<span class="hide"><i class="icon"></i></span>';
			html += '</form>';
			html += '</div>';
		}
		html += '</div>';
		if($('.container').children()[0]){
			$($('.container').children()[0]).before(html)
		}else{
			$('.container').append(html);
		}
		var head_heigth = $('.head').height();
		$('.container').css({'padding-top':head_heigth+'px'})
		head_obj.init_sort_showAndHide();
		head_obj.init_menu_action(opt);
		head_obj.init_sort_action(opt);
		head_obj.init_search_action(opt);
	},
	//初始化排序
	init_sort:function(sortArray){
		var html = '';
		html += '<div id="head—sort-screen" class="hide">';
		html += '<span><i class="icon"></i></span>';
		html += '<div>';
		html += '<ul>';
		for(var i = 0; i < sortArray.length; i++){
			html += '<li sort_name="'+ sortArray[i].sort_name +'"><span>'+ sortArray[i].name +'</span>';
			html += '<span sort-type="asc" class="hide sort-icon sort-icon-asc"><i class="icon"></i></span>';
			html += '<span sort-type="desc" class="hide sort-icon sort-icon-desc"><i class="icon"></i></span></li>';
		}
		html += '</ul>';
		html += '</div>';
		html += '</div>';
		$('body').append(html);
	},
	//初始换排序显示隐藏事件
	init_sort_showAndHide:function(){
		$('.head-menu').find('span').click(function(){
			$('#head—sort-screen').removeClass('hide');
			$('#head—sort-screen').unbind().bind('click',function(){
				$(this).addClass('hide');
			});
			$('#head—sort-screen').find('div').unbind().bind('click',function(event){
				event.stopPropagation();
			});
		});
	},
	//绑定头部菜单点击事件
	init_menu_action:function(opt){
		$('.head-menu').find('ul').children().click(function(){
			$('.head-menu').find('ul').children().removeClass('active');
			$(this).addClass('active');
			var menu_obj = {};
			var lis = $('.head-menu').find('ul').children();
			for(var i = 0; i < lis.length; i++){
				if(/active/.test($(lis[i]).attr('class'))){
					menu_obj = opt.menu.list[i];
				}
			}
			if (typeof(opt.menu.fun) === 'function') {
				opt.menu.fun(menu_obj,this);
			}
			return false;
		});
	},
	//绑定点击排序方法
	init_sort_action:function(opt){
		$('#head—sort-screen').find('div').find('ul').find('li').click(function(){
			head_obj.change_sort_icon(this);
			var sort_msg = head_obj.get_sort_msg();
			head_obj.sort_hide();
			$('.head-menu').children('span').children('i[is-sort-icon="true"]').addClass('hide');
			$('.head-menu').children('span').children('i[is-sort-icon="true"]').each(function(){
				if($(this).attr('class').indexOf(sort_msg.sort_type) > 0){
					$(this).removeClass('hide');
				}
			});
			if (typeof(opt.sort.fun) === 'function') {
				opt.sort.fun(sort_msg);
			}
		});
	},
	//绑定搜索方法
	init_search_action:function(opt){
		$(".search-div").find('form').find('input').keyup(function(){
			if($.trim($(this).val()).length == 0){
				$(".search-div").find('form').find('span').addClass('hide');
			}else{
				$(".search-div").find('form').find('span').removeClass('hide');
			}
		});
		
		$(".search-div").find('form').find('span').click(function(){
			$(this).addClass('hide');
			$(".search-div").find('form').find('input').val('');
		})
		
		$(".search-div").find('form').submit(function(){
			 var key = $.trim($(this).find('input').val());
			 if (typeof(opt.search.fun) === 'function') {
				 opt.search.fun(key);
				return false;
			 }
			 return false;
		});
	},
	//激活头部菜单
	active_head_menu:function(activekey){
		$('.head-menu').find('ul').children().removeClass('active');
		$('.head-menu').find('ul').children().each(function(){
			if(activekey == $(this).attr('data-activekey')){
				$(this).addClass('active');
			}
		});
	},
	//隐藏排序
	sort_hide:function(){
		$('#head—sort-screen').addClass('hide');
	},
	//排序图标变化
	change_sort_icon:function(sort_li_item){
		$('#head—sort-screen').find('div').find('ul').find('li').removeClass('sortactive');
		$(sort_li_item).addClass('sortactive');
		if($(sort_li_item).find('.hide').length == 2){
			$($(sort_li_item).find('.sort-icon')[0]).removeClass('hide');
		}else{
			$(sort_li_item).find('.sort-icon').each(function(){
				if($(this).attr('class').indexOf('hide') >= 0){
					$(this).removeClass('hide');
				}else{
					$(this).addClass('hide');
				}
			});
		}
		$('#head—sort-screen').find('div').find('ul').find('li').each(function(){
			var class_str = $(this).attr('class') || '';
			var reg = /sortactive/;
			if(!reg.test(class_str)){
				$(this).find('.sort-icon').addClass('hide');
			}
		});
	},
	//获取排序信息
	get_sort_msg:function(){
		var sort_obj = {};
		$('#head—sort-screen').find('div').find('ul').find('li').each(function(){
			var class_str = $(this).attr('class') || '';
			var reg = /sortactive/;
			if(reg.test(class_str)){
				sort_obj.sort_name = $(this).attr('sort_name');
				$(this).find('.sort-icon').each(function(){
					if($(this).attr('class').indexOf('hide') == -1){
						sort_obj.sort_type = $(this).attr('sort-type');
					}
				});
			}
		});
		return sort_obj;
	},
	//获取激活菜单信息,返回节点信息
	get_active_menu_msg:function(){
		var lis = $('.head-menu').find('ul').children();
		for(var i = 0; i < lis.length; i++){
			if(/active/.test($(lis[i]).attr('class'))){
				return lis[i];
			}
		}
	},
	//获取搜索信息
	get_search_msg:function(){
		var search_key = $.trim($(".search-div").find('input').val());
		return search_key;
	},
	//设置排序信息
	set_sort_msg:function(sort_obj){
		var sort_name = sort_obj.sort_name;
		var sort_type = sort_obj.sort_type;
		$('#head—sort-screen').find('div').find('ul').find('li').removeClass('sortactive');
		$('#head—sort-screen').find('div').find('ul').find('li').each(function(){
			if(sort_name == $(this).attr('sort_name')){
				$(this).addClass('sortactive');
				$(this).find('.sort-icon').each(function(){
					if(sort_type == $(this).attr('sort-type')){
						$(this).removeClass('hide');
					}else{
						$(this).addClass('hide');
					}
				});
			}else{
				$(this).find('.sort-icon').addClass('hide');
			}
		});
	},
	//设置搜索信息
	set_search_msg:function(search_key){
		$(".search-div").find('input').val(search_key);
	}
};

//重写alert框
window.alert = function(opt){
	$('body').find('div[class="alert_screen"]').remove();
	opt = opt || {};
	var title = opt.title || '网页显示';
	var desc = opt.desc || '';
	var action_desc = opt.action_desc || '确定';
	var inner_html = '<div class="alert_screen"><div><p class="alert_title">'+ title +'</p><p class="alert_desc">'+ desc +'</p><p class="alert_action">'+ action_desc +'</p></div></div>';
	$('body').append(inner_html);
	$('body').find('div[class="alert_screen"]').find('p[class="alert_action"]').click(function(){
		$(this).parent().parent().remove();
		if (typeof(opt.callback) === 'function') {
			opt.callback();
		}
	});
}

//重写confirm框
window.confirm = function(opt){
	$('body').find('div[class="confirm_screen"]').remove();
	opt = opt || {};
	var title = opt.title || '确认';
	var desc = opt.desc || '';
	var ok_text = opt.ok_text || '确定';
	var cancel_text = opt.cancel_text || '取消';
	var inner_html = '<div class="confirm_screen"><div><p class="confirm_title">'+ title +'</p><p class="confirm_desc">'+ desc +'</p><div class="confirm_action"><p class="confirm_cancle">'+ cancel_text +'</p><p class="confirm_ok">'+ ok_text +'</p></div></div></div>';
	$('body').append(inner_html);
	$('body').find('div[class="confirm_screen"]').find('p[class="confirm_cancle"]').click(function(){
		$(this).parent().parent().parent().remove();
		if (typeof(opt.cancel_callback) === 'function') {
			opt.cancel_callback();
		}
	});
	$('body').find('div[class="confirm_screen"]').find('p[class="confirm_ok"]').click(function(){
		$(this).parent().parent().parent().remove();
		if (typeof(opt.ok_callback) === 'function') {
			opt.ok_callback();
		}
	});
}

//opt={default_t:'反馈已完成',placeholder:'',ok_text:"确定",cancel_text:"取消",ok_callback :function(value){console.log(value)},cancel_callback:function(){}}
//还需解决js生成节点focus()弹出浮动键盘问题。
//使用页面需要加上已下代码,body的子节点
/*
<div id="comfirm_input" class="comfirm_input hide">
	<div class="content">
		<textarea id="comfirm_input_textarea" placeholder="备注一下吧"></textarea>
	</div>
	<div class="action">
		<button class="btn-no-border cancel_b">取消</button><button class="btn-no-border ok_b">确定</button>
	</div>
</div>
*/
var confirm_input = {
	init:function(opt){
		/*
		$('body').find('div[id="comfirm_input"]').remove();
		var ok_text = opt.ok_text || '确定';
		var cancel_text = opt.cancel_text || '取消';
		var default_t = opt.default_t || '';
		var inner_html = '';
		inner_html += '<div id="comfirm_input" class="comfirm_input">';
		inner_html += '<div class="content"><textarea id="comfirm_input_textarea"></textarea></div>';
		inner_html += '<div class="action"><button class="btn-no-border cancel_b">'+ cancel_text +'</button><button class="btn-no-border ok_b">'+ ok_text +'</button></div>';
		inner_html += '</div>';
		$('body').append(inner_html);
		*/
		$('body').find('div.container').addClass('hide');
		$('body').find('div[id="comfirm_input"]').removeClass('hide');
		confirm_input.init_action(opt);
		confirm_input.show_keyboard(opt);
	},
	show_keyboard:function(opt){
		$("#comfirm_input_textarea").attr('placeholder',opt.placeholder);
		if(opt.default_t && opt.default_t.length > 0){
			$("#comfirm_input_textarea").focus().val(default_t);
		}else{
			$("#comfirm_input_textarea").focus();
		}
	},
	init_action:function(opt){
		$('body').find('div[id="comfirm_input"]').find('button.nb-btn.nb-btn-green.cancel_b').unbind('click').click(function(){
			confirm_input.hide();
			if(typeof(opt.cancel_callback) === 'function'){
				opt.cancel_callback();
			}
		});
		
		$('body').find('div[id="comfirm_input"]').find('button.nb-btn.nb-btn-green.ok_b').unbind('click').click(function(){
			if(typeof(opt.ok_callback) === 'function'){
				var textarea_value = $('body').find('div[id="comfirm_input"]').find('textarea').val();
				opt.ok_callback(textarea_value);
			}
		});
	},
	hide:function(){
		//$('body').find('div[id="comfirm_input"]').remove();
		$('body').find('div[id="comfirm_input"]').addClass('hide');
		$('body').find('div.container').removeClass('hide');
	}
}

//滚动加载??需改造，y向滚动最好采用最外层的滚动
//opt={scroll_target:$('#sdf'),target:$('#aaa');fun:function aa (){}}
var scroll_load_obj = {
	init:function(opt){
		$(opt.scroll_target).attr('is_scroll_end','false');
		$(opt.scroll_target).attr('is_scroll_loading','false');
		var sTimer;
		$(opt.scroll_target).unbind('scroll').scroll(function scrollHandler(){
			clearTimeout(sTimer);
			 sTimer = setTimeout(function() {
				 var c = $(opt.scroll_target)[0].scrollHeight;		//总高度
				 var t = $(opt.scroll_target).scrollTop();			//滚动高度
				 var h = $(opt.scroll_target)[0].clientHeight;
				 var is_scroll_end = $(opt.scroll_target).attr('is_scroll_end') == 'true' ? true : false;
				 var is_scroll_loading = $(opt.scroll_target).attr('is_scroll_loading') == 'true' ? true : false;
				 if(t + h +200 > c && !is_scroll_end && !is_scroll_loading){
					 scroll_load_obj.add_scroll_waiting(opt.target);
					 $(opt.scroll_target).attr('is_scroll_loading','true');
					 if (typeof(opt.fun) === 'function') {
						 opt.fun();
					 }
				 }
			 },100);
		});
	},
	scroll_load_end:function(scroll_target){
		$(scroll_target).attr('is_scroll_end','true');
	},
	add_scroll_waiting:function(target){
		var html = '<div id="scroll_loading"><div><span class="an-1"></span><span class="an-2"></span><span class="an-3"></span></div></div>';
		if($(target).children('#scroll_loading').length == 0){
			$(target).append(html);
		}
	},
	remove_scroll_waiting:function(target){
		if($(target).children('#scroll_loading').length > 0){
			$(target).children('#scroll_loading').remove();
		}
	},
	set_is_scroll_loading_false:function(scroll_target){
		 $(scroll_target).attr('is_scroll_loading','false');
	}
}

//地步弹出操作
//opt={menus:[{id:1,name:'ssss',menu_data:{},fun:aa,icon_html:''}],data:{}]}
//icon_html:"<span></span>";
var bottom_menu_action = {
	init:function(opt){
		var data_html = '';
		if(opt.data){
			for(var k in opt.data){
				data_html += ' data-' + k +'="' + opt.data[k] +'"';
			}
		}
		var html = '';
		html += '<div id="bottom_menu_action_wrap">';
		html += '<div id="bottom_menu_action" '+ data_html +'>';
		for(var i = 0; i < opt.menus.length; i++){
			var menu = opt.menus[i];
			var menu_data_html = '';
			if(menu.menu_data){
				for(var k in menu.menu_data){
					menu_data_html += ' data-' + k +'="' + menu.menu_data[k] +'"';
				}
			}
			if(menu.icon_html){
				html += '<div data-id="'+ menu.id +'" data-name="'+ menu.name +'" '+ menu_data_html +'">'+menu.icon_html+menu.name+'</div>';
			}else{
				html += '<div data-id="'+ menu.id +'" data-name="'+ menu.name +'" '+ menu_data_html +'">'+menu.name+'</div>';
			}
		}
		html += '<div class="bottom_menu_action_del" data-del="true"><div>取消</div></div>'
		html += '</div></div>';
		if($('body').children('#bottom_menu_action_wrap').length > 0){
			$('body').children('#bottom_menu_action_wrap').remove();
		}
		$('body').append(html);
		bottom_menu_action.init_action(opt);
		bottom_menu_action.init_hide();
	},
	init_hide:function(){
		$('#bottom_menu_action_wrap').click(function(){
			$(this).remove();
		})
	},
	init_action:function(opt){
		$('#bottom_menu_action').children().click(function(e){
			if($(this).attr("data-del") == undefined || !$(this).attr("data-del") == "true"){
				e.stopPropagation();
				var id = $(this).attr('data-id');
				for(var i = 0; i < opt.menus.length; i++){
					var menu = opt.menus[i];
					if(id == menu.id){
						if (typeof(menu.fun) === 'function') {
							menu.fun(this,menu,opt.data);
						}
						break;
					}
				}
			}
		});
	},
	hide:function(){
		$('#bottom_menu_action_wrap').remove();
	}
}