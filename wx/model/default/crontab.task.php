<?php
/**
 * 定时任务
 * 1.初始化应用菜单
 */
include_once('global.config.php');

//-------------------------------------初始化应用菜单

task_init_app_menu();

function task_init_app_menu() {
	$app_list = g('com_app') -> get_unconf_menu_app();
	if ($app_list) {
		log_write('开始初始化应用菜单', 'crontab');
		try {
			foreach ($app_list as $app) {
				$token = g('wxqy') -> get_access_token($app['corp_id'], $app['corp_secret']);
				$token = $token['token'];
				
				//获取微信菜单
				$app_conf = include(SYSTEM_APPS.$app['app_name'].'/config.php');
				if (!isset($app_conf['wx_menu']) || is_null($app_conf['wx_menu'])) {
					throw new SCException('应用自定义菜单配置找不到: app='.$app['app_name']);
				}
				$app_menu = $app_conf['wx_menu'];
				if (count($app_menu) == 0) {	//空的自定义菜单，不需要配置
					
				} else {						//更新菜单<corpurl>为数据库中对应的数据
					$app_menu = json_encode($app_menu);
					$app_menu = str_replace('<corpurl>', $app['corp_url'], $app_menu);
					$app_menu = json_decode($app_menu, TRUE);
					
					g('wxqy') -> create_menu($token, $app['wx_app_id'], $app_menu);
				}
				log_write('创建成功，com_app_id='.$app['com_app_id'], 'crontab');
				g('com_app') -> change_menu_state($app['com_app_id']);
			}
			log_write('完成初始化应用菜单', 'crontab');
			
		} catch (SCException $e) {
			log_write($e -> getMessage(), 'crontab');
		}
	}
}