<?php
/**
 * 微信消息入口
 */
include_once('global.config.php');
include_once(SYSTEM_MODS.'cls_wx_service.php');

$module = 'wx_api';
$action = get_var_value('a');
$action = is_null($action) ? 'index' : $action;

$cls_module = SYSTEM_MODS.'cls_'.$module.'.php';

log_write('微信消息入口');
log_write('微信消息入口:' . $cls_module);

if (file_exists($cls_module)) {
	
	include_once ($cls_module);				//加载对应模块类
	$module = 'cls_'.$module;

	log_write('加载对应模块类:' . $module);
	log_write('加载对应模块方法:' . $action);

	if (class_exists($module) && method_exists($module, $action)) {
		$mod = new $module();
		$mod -> $action();					//处理业务逻辑
	}
}