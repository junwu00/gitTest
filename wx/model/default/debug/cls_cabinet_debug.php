<?php
/**
 * 企业云盘接口测试类
 *
 * @author LiangJianMing
 * @create 2015-04-02
 */
class cls_cabinet_debug {
    private $cmd = 0;

    private $base_param = array();

    public function __construct() {
        $cls_name = __CLASS__;
        $app_name = str_replace('cls_', '', $cls_name);
        $app_name = str_replace('_debug', '', $app_name);

        $cmd = (int)get_var_value('cmd');
        $this -> cmd = $cmd;

        $this -> base_param = array(
            'app' => $app_name,
            'm' => 'ajax',
            'cmd' => $cmd,
        );
    }

    /**
     * 调试入口
     *
     * @access public
     * @return void
     */
    public function entry() {
        $func = 'debug_' . $this -> cmd;
        $this -> $func();
    }

    //获取目录列表
    public function debug_100() {
        $data = array(
        );

        $get_param = array(
            'id' => 0,
            'page' => 1,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //获取文件列表
    public function debug_101() {
        $data = array(
        );

        $get_param = array(
            'id' => 84,
            'page' => 1,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //获取文件详情
    public function debug_102() {
        $data = array(
        );

        $get_param = array(
            'id' => 469,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //收藏文件
    public function debug_103() {
        $data = array(
        );

        $get_param = array(
            'id' => 469,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //取消收藏文件
    public function debug_104() {
        $data = array(
        );

        $get_param = array(
            'id' => 469,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //下载文件
    public function debug_105() {
        $data = array(
        );

        $get_param = array(
            'id' => 469,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //获取收藏文件列表
    public function debug_106() {
        $data = array(
        );

        $get_param = array(
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //获取用作搜索关键字的全部文件列表
    public function debug_107() {
        $data = array(
        );

        $get_param = array(
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //进行搜索，查询文件列表
    public function debug_108() {
        $data = array(
        );

        $get_param = array(
            'key_word' => '51006-20151011周计',
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }

    //获取当前目录的导航路径
    public function debug_109() {
        $data = array(
        );

        $get_param = array(
            'id' => 84,
        );

        $get_param = array_merge($get_param, $this -> base_param);
        g('ndebug') -> debug_curl($get_param, $data);
    }
    
    public function __call($name, $args) {
        cls_resp::echo_err(cls_resp::$Busy, '类方法[' . $name . ']不存在！');
    }
}

/* End of this file */