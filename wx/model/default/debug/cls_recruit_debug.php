<?php
/**
 * 人力招聘调试demo
 *
 * @author LiangJianMing
 * @create 2015-04-02
 */
class cls_recruit_debug {
	private $cmd = 0;

	private $base_param = array();

	public function __construct() {
		$cls_name = __CLASS__;
		$app_name = str_replace('cls_', '', $cls_name);
		$app_name = str_replace('_debug', '', $app_name);

		$cmd = (int)get_var_value('cmd');
		$this -> cmd = $cmd;

		$this -> base_param = array(
			'app' => $app_name,
			'm' => 'ajax',
			'cmd' => $cmd,
		);
	}

	/**
	 * 调试入口
	 *
	 * @access public
	 * @return void
	 */
	public function entry() {
		$func = 'debug_' . $this -> cmd;
		$this -> $func();
	}

	public function debug_101() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_102() {
		$data = array(
			'page' => 1,
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_103() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_104() {
		$data = array(
			'page' => 1,
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_105() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_106() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_107() {
		$data = array(

		);

		$get_param = array(
			'id' => 53,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_108() {
		$data = array(

		);

		$get_param = array(
			'id' => 53,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_109() {
		$data = array(
			'name' => '梁坚明',
			'mobile' => '18613034345',
			'portrait' => 'image/res/dfe/4db/dfe4dba6fabf8b0df56687cd79408655.jpg',
			'gender' => 1,
			'age' => 25,
			'salary' => 13000,
			'dwell' => '华景新城怡华台',
			'experience' => '十年',
			'degree' => '本科',
			'job' => '系统架构师',
			'email' => 'myssea@163.com',
			'other_info' => array(
				'测试' => '一开始我是拒绝的',
			),
			'accessory' => array(
				'image/res/f17/af1/f17af11a462af0ab4c8f59156c275304.jpg',
				'image/res/f17/af1/f17af11a462af0ab4c8f59156c275304.jpg',
				'image/res/f17/af1/f17af11a462af0ab4c8f59156c275304.jpg',
			),
		);

		$get_param = array(
			'cipher' => 'BC2C74E41226CAB6D8037FA38104CC37A5FD52C6A49D7E4839EE1455B24BA46BED02FB1155951EF50172CC1A56861689',
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_110() {
		$data = array(
			'money' => '199.94',
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_111() {
		$data = array(
		);

		$get_param = array(
			'id' => 4,
			//'state' => 5,
			'collected' => 1,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_112() {
		$data = array(
			'ids' => array(
				1, 2
			),
		);

		$get_param = array(
			//'id' => 5,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_113() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_114() {
		$data = array(
		);

		$get_param = array(
			'id' => 1,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_115() {
		$data = array(
		);

		$get_param = array(
			'id' => 53,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_116() {
		$data = array(
		);

		$get_param = array(
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}

	public function debug_117() {
		$data = array(
		);

		$get_param = array(
			'id' => 32,
			'is_read' => 1,
		);

		$get_param = array_merge($get_param, $this -> base_param);
		g('ndebug') -> debug_curl($get_param, $data);
	}
	
	public function __call($name, $args) {
		cls_resp::echo_err(cls_resp::$Busy, '类方法[' . $name . ']不存在！');
	}
}