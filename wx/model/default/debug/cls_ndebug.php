<?php
/**
 * 常用调试方法类
 *
 * @author LiangJianMing
 * @create 2015-04-02
 */
class cls_ndebug {
	/**
	 * curl执行远程请求
	 *
	 * @access public
	 * @param string $url 远程地址
	 * @param array $data 数据集合
	 * @param array $param 其他参数集合
	 * @param integer $timeout 读取数据超时时间
	 * @param integer $con_timeout 连接超时时间
	 * @return array
	 */
	public function curl($url, $data=null, $param=array(), $timeout = 30, $con_timeout = 5) {
		$ret = array(
		    'httpcode' => 0, //返回码：0 - client主动断开
		    'content'  => '', //返回内容
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($data){
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		    !empty($param['CURLOPT_USERAGENT']) and curl_setopt($ch, CURLOPT_USERAGENT, $param['CURLOPT_USERAGENT']);
		}
		$ret['content']  = $this -> remove_utf8_bom(curl_exec($ch));
		$ret['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE); //执行之后才能获取状态码
		curl_close($ch);

		return $ret;
	}

	/**
	* 清除utf-8的bom头
	*
	* @access public
	* @param string $text 字符串  
	* @return string
	*/
	public function remove_utf8_bom($text) {
		$bom = pack('H*','EFBBBF');
		$text = preg_replace("/^{$bom}+?/", '', $text);
		return $text;
	}

	public function debug_curl(array $get_param=array(), array $data=array()) {
		$json = json_encode($data);
		$post_param = array(
			'test' => 80162,
			'data' => $json,
		);

		$curl_param = array(
			'CURLOPT_USERAGENT' => 'MicroMessenger',
		);
		//---end----------自定义参数------------------------

		$ajax_url = 'http://' . DEBUG_HOST . '/index.php?';

		if (!empty($get_param)) {
			$param_str = http_build_query($get_param);
			$ajax_url .= $param_str;
		}

		$result = $this -> curl($ajax_url, $post_param, $curl_param);

		$result['httpcode'] == 200 and die($result['content']);
		print_r($result);
		exit;
	}
}