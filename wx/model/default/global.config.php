<?php
/**
 * 全局配置文件
 *
 * 只允许添加必要的定义以及应用加载
 *
 * @author LiangJianMing
 * @create 2014-05-28
 * @version 1.0
 */
$G_CONF = require (dirname(dirname(__DIR__))) . '/config/env/env.now.config.php';

define('SYSTEM',                    $G_CONF['MAIN_NAME']);                      // 定义项目名称
define('SYSTEM_COPYRIGHT',          $G_CONF['MAIN_COPYRIGHT']);                 // 定义默认版权信息
define('SYSTEM_VERSION',            $G_CONF['MAIN_VERSION']);                   // 版本
define('SYSTEM_IS_PRIVATE',         isset($G_CONF['IS_PRIVATE']) ? $G_CONF['IS_PRIVATE'] : 0);               // 是否私有化
define('SYSTEM_ROOT',               __DIR__ . '/');                             // 根目录
define('SYSTEM_INCLUDE',            SYSTEM_ROOT.'include/');                    // include目录
define('SYSTEM_INCLUDE_BASE',       SYSTEM_ROOT.'include/base/');               // base目录
define('SYSTEM_EXTENSIONS',         SYSTEM_ROOT.'extensions/');                 // extensions目录
define('SYSTEM_DATA',               dirname(dirname(SYSTEM_ROOT)).'/data/default/');                       //数据目录
define('SYSTEM_MODS',               SYSTEM_ROOT.'modules/');                    // 模块目录
define('SYSTEM_TPL', 				SYSTEM_ROOT.'templates/');					// 模板目录
define('SYSTEM_STATIC',             SYSTEM_ROOT.'static/');                     // css images js等目录
define('SYSTEM_SET_ISLOG',          TRUE);                                 		// 是否记录日志
define('SYSTEM_HTTP_TYPE',          $G_CONF['MAIN_SCHEME']);                    // HTTP类型
define('SYSTEM_HTTP_DOMAIN',        $G_CONF['MAIN_DYNAMIC_DOMAIN']);            // HTTP域
define('SYSTEM_STATIC_DOMAIN',      $G_CONF['MAIN_STATIC_DOMAIN']);             // 静态文件域名
define('SYSTEM_COOKIE_DOMAIN',      $G_CONF['MAIN_COOKIE_DOMAIN']);             // HTTP域
ini_set('session.cookie_domain',    SYSTEM_COOKIE_DOMAIN); 						// 指定session的作用域名
ini_set('session.use_only_cookies', TRUE); 										// 只允许使用cookie来记录session_id
ini_set('session.cookie_httponly', 	TRUE); 										// session的cookie只允许使用http方式进行访问
//指定session的序列化处理器
ini_set('session.serialize_handler', 'php_serialize');

define('SYSTEM_DB_TYPE',       		'pdo');										// 定义数据库的操作方式
define('SYSTEM_HTTP_QY_DOMAIN',     $G_CONF['QY_DYNAMIC_DOMAIN']);              // qy管理端http域
define('SYSTEM_HTTP_PC_DOMAIN',     $G_CONF['PC_DYNAMIC_DOMAIN']);              // pc端http域
define('SYSTEM_APPS',               SYSTEM_ROOT.'apps/');                       // APP目录
define('SYSTEM_HTTP_APPS_ICON',     SYSTEM_STATIC_DOMAIN.'apps/common/static/images/icon/2/'); // APP图标url根地址，0为对应的图标版本
define('SYSTEM_LOG',                SYSTEM_DATA.'log/');           	 			// 日志目录
define('SYSTEM_PART_DIR',           SYSTEM_DATA.'part_upload/');                // 上传文件临时存放目录
define('SYSTEM_UNIQ_PREFIX',        'sc1');                 					// 项目唯一标识码前缀

//压缩js、css文件关键字
define('SYSTEM_COMPRESS_KEY', 'ew_cp_');

//指定页面编码
header("Content-Type: text/html; charset=UTF-8");
date_default_timezone_set('PRC');

error_reporting(-1);

include_once(SYSTEM_INCLUDE_BASE.'global.function.php');
include_once(SYSTEM_INCLUDE_BASE.'app.function.php');

//消息自动回复
include_once(SYSTEM_APPS.'abs_app_wxser.php');

//注册自动加载类函数
spl_autoload_register('auto_load');
	
//加载基础库及扩展
include_base();
load_extensions('redis');
load_extensions('redis_session');
load_extensions('redis_cookie');
load_extensions('mysql_pdo');
load_extensions('smarty');
load_extensions('email');
load_extensions('wxqy');
load_extensions('bdmap');
load_extensions('email');
load_extensions('pinyin');

/* End of file global.config.php */