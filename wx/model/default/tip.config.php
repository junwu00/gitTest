<?php
/**
 * 推送类消息的模板类
 *
 * @author LiangJianMing
 * @create 2015-06-04
 */

return array(
    //移动外勤提醒
    'legwork' => array(
        'title_model' => '移动外勤：您的下属 %name% 的外出计划%title%已%mark_type%',
        'url_pfx' => SYSTEM_HTTP_PC_DOMAIN . 'index.php?app=legwork&m=plan&a=lister_underling',
        'app_name' => 'legwork',
    ),
);