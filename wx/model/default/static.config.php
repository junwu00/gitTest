<?php
/**
 * 静态文件配置文件
 *
 * @author LiangJianMing
 * @create 2015-08-21
 */

$arr = array(
	/** CSS */
	'common.css' => array(
		SYSTEM_ROOT . 'static/default/css/bootstrap3.2.css',
		SYSTEM_ROOT . 'static/default/css/bootstrap-responsive.min.css',
		SYSTEM_ROOT . 'static/default/css/font-awesome.min.css',
		SYSTEM_ROOT . 'static/default/css/owner_fonts.css?family=Open+Sans:400,300'
	),

	/** JS */
    'common_before.js' => array(
        SYSTEM_ROOT . 'static/js/jquery.min.js',
        SYSTEM_ROOT . 'static/js/frame.js',
        SYSTEM_ROOT . 'static/js/format.js',
        SYSTEM_ROOT . 'static/js/check.js'
    ),
    
    'jweixin_jssdk.js' => array(
        SYSTEM_ROOT . 'static/js/jweixin-1.1.0.js',
        SYSTEM_ROOT . 'static/js/jssdk.js'
    )
);

return $arr;

/* End of this file */