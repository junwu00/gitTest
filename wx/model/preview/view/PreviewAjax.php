<?php
/**
 * 流程应用Ajax交互类
 *
 * @author luja
 * @create 2016-08-09
 */

namespace model\preview\view;

class PreviewAjax extends \model\api\view\ViewBase {

	// 错误提示码
	private $busy = 0;

	public function __construct($log_pre_str='') {
		echo '111';
		parent::__construct('preview',$log_pre_str);
		// 未知原因（系统繁忙）
		$this -> busy = \model\api\server\Response::$Busy;
		
	}

	/** 文件预览 */
	public function preview_file() {
		try {
			parent::log_i("文件预览");
			$id = (int)g('pkg_val') -> get_get('id', FALSE);

			$file_config = load_config('model/preview/file');

			if (!isset($file_config[$id])) throw new \Exception('页面不存在！');
			
			$url = g('api_media') -> cloud_rename_preview($file_config[$id]['name'] , $file_config[$id]['hash'], '', TRUE);

			parent::echo_ok(array('url' => $url));
		} catch (\Exception $e) {
			g('view_notice') -> error($e -> getMessage());
		}
	}


}
// end of file