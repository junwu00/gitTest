<?php
/**
 * 主页型应用单入口
 * @author 陈意浩
 * @date 2016-08-03
 *
 */
namespace model\index\view;

class PreviewIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('preview',$log_pre_str);
	}
	
	//主页面
	public function index(){
		
	}

	
}

//end