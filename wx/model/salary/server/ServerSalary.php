<?php
/**
* 工资条业务逻辑处理
*   
* @author gch
* @create 2017-09-28 15:05:24
*/

namespace model\salary\server;

class ServerSalary extends \model\api\server\ServerBase {
    
    /**
    * 获取用户信息
    *   
    * @access public
    * @param   
    * @return   
    */
    public function get_name($user_id, $user_field = 'name', $dept_field = 'name as dept') {
        $user_info = g('dao_user') -> get_by_id($this -> user_id, $user_field);
        empty($user_info) && $user_info = array();

        $dept_info = g('dao_user') -> get_main_dept($this -> user_id, FALSE, $dept_field);
        empty($dept_info) && $dept_info = array();

        $return = array_merge($user_info, $dept_info);
        return $return;
    }
}
//end of file