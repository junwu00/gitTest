<?php

/**
 * 工资条
 * @author yangpz
 * @date 2016-08-12
 */

namespace model\salary\view;

class SalaryIndex extends \model\api\view\ViewBase {

	public function __construct($log_pre_str='') {
		parent::__construct('salary',$log_pre_str);
	}

	/** 列表 */
	public function index() {
		$this -> show($this -> view_dir . 'page/salaryIndex.html');
	}

	/** 详情 */
	public function detail() {
		$this -> show($this -> view_dir . 'page/salaryDetail.html');
	}
	
}

//end