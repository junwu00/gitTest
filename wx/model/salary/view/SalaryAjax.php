<?php

/**
 * 工资条逻辑处理接口
 * 
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\salary\view;

class SalaryAjax extends \model\api\view\ViewBase {
	
	//命令对应函数名的映射表
    private $cmd_map = array(
        //获取工资条列表(某个人)
        101 => 'get_salary_list',
        //获取某条工资条详情
        102 => 'get_one_salary_detail',
    );

	public function __construct() {
		parent::__construct('salary');
	}
	
	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		try {
			$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
			if (empty($cmd)) throw new \Exception('命令参数错误');

			$map = $this -> cmd_map;
			if (!isset($map[$cmd])) throw new \Exception('命令参数错误');

			$this -> $map[$cmd]();
		} catch (\Exception $e) {
			parent::echo_err(\model\api\server\Response::$Busy, $e -> getMessage());
		}

	}

// --------------------------接口内部实现函数------------------------------------------------

	/**
	* 获取工资条列表(某个人)
	*   
	* @access public
	* @param   
	* @return   
	*/
	public function get_salary_list() {
		$need = array();
		$data = parent::get_post_data($need);

		$field = array(
			'id', 'year', 'month', 'salary_time', 'person_number','id_card',
		);

		$user_field = 'name, person_number,id_card';
		$dept_field = 'name as dept';
		
		$user_info = g('ser_salary') -> get_name($this -> user_id, $user_field, $dept_field);
		if (empty($user_info)) {
			throw new \Exception('获取用户信息失败');
		}

//		if (empty($user_info['person_number'])) {
//			throw new \Exception('您的人员编号为空，请联系管理员！');
//		}

		$cond = array(
			'info_state=' => 1,
		);

		$or_cond = array();
        if(!empty($user_info['person_number'])){
            $or_cond[] = array(
                'person_number=' => $user_info['person_number'],
                'type='=>\model\api\dao\Salary::SALARY_TYPE_PERSON_NUMBER,
            );
        }

        if(!empty($user_info['id_card'])){
            $or_cond[] = array(
                'id_card=' => $user_info['id_card'],
                'type='=>\model\api\dao\Salary::SALARY_TYPE_ID_CARD,
            );
        }

		$page = !empty($data['page']) ? intval($data['page']) : 1;
		$page_size = !empty($data['page_size']) ? intval($data['page_size']) : 20;
		$order = 'year DESC, month DESC';

		if($or_cond){
		    $cond['__OR'] = $or_cond;
            $list = g('dao_salary') -> get_data_by_cond($this -> com_id, $cond, $field, $page, $page_size, $order, TRUE);
            $log_info = json_encode($list);
            to_log('Info', '', $log_info);
        }else{
            $list = array(
                'data' => array(),
                'count' => 0,
            );
        }

		if (!empty($list['data'])) {
			$tmp_list_data = array();

			foreach ($list['data'] as $v) {
				$year = $v['year'] . '年';
				$month = $v['month'] . '月';
				$salary_date = date('Y年m月d日', $v['salary_time']);
				$tmp_list_data[$year][$month] = array(
					'id' => $v['id'],
					'key' => $month,
					'year' => $v['year'],
					'month' => $v['month'],
					'person_number' => $v['person_number'],
                    'id_card' => $v['id_card'],
					'value' => $salary_date,
				);
			}

			$list['data'] = $tmp_list_data;
		} else {
			$list = array(
				'data' => array(),
				'count' => 0,
			);
		}

		$list['name'] = $user_info['name'];
		$list['dept'] = $user_info['dept'];

		$info = array(
			'info' => $list,
		);
		parent::echo_ok($info);
	}

	/**
	* 获取某条工资条详情
	*   
	* @access public
	* @param   
	* @return   
	*/
	public function get_one_salary_detail() {
		$need = array(
			// 'salary_id'
			'year', 'month',
		);
		$data = parent::get_post_data($need);
		// $salary_id = !empty($data['salary_id']) ? intval($data['salary_id']) : 0;
		// if (empty($salary_id)) {
		// 	throw new \Exception('缺少工资条id');
		// }
		$year = !empty($data['year']) ? intval($data['year']) : 0;
		if (empty($year)) {
			throw new \Exception('缺少年份');
		}
		$month = !empty($data['month']) ? intval($data['month']) : 0;
		if (empty($month)) {
			throw new \Exception('缺少月份');
		}

		$user_field = 'name, person_number,id_card';
		$dept_field = 'name as dept';
		
		$user_info = g('ser_salary') -> get_name($this -> user_id, $user_field, $dept_field);
		if (empty($user_info)) {
			throw new \Exception('获取用户信息失败');
		}

		$field = array(
			/*'id', 'year', 'month', '`unit_name`', 'person_number',
			'name', 'total_payable', 'total_deduct', 'actual_total', 
			'print_date', 'job_wage','wage_level', 'base_pay', 
			'seniority_wage', 'technical_grade_wage', 'probationary_wages', 
			'living_wage', 'wage_pay', 'margin_difference', 
			'special_allowance', 'retention_allowance', 'post_allowance', 
			'other_additions', 'working_subsidy', 'grassroots_subsidies', 
			'incentive_performance_allowance', 'temporary_subsidy', 
			'reform_subsidy', 'living_subsidies', 'temporary_post_allowance', 
			'special_post_allowance', 'pay_only_child', 'national_pension_increase', 
			'retirement_fee_increase', 'annual_assessment_bonus', 'other', 
			'pre_tax_income', 'after_tax_income', 'exemption', 'income_tax', 
			'endowment_insurance', 'medical_insurance', 'annuity', 'pay', 
			'accumulation_fund', 'other_deduction', 'bank_name', 'bank_account', 
			'unit_social_security_number', 'personal_social_security_number', 'unit_of_value',
            'normative_subsidy','career_post_allowance','living_allowances'*/

            'id', 'name','id_card', 'type', 'year', 'month', 'unit_name','print_date', 'person_number',

            'total_payable', 'total_deduct', 'actual_total',

            'job_wage','wage_level','incentive_performance_allowance','post_wage','wage_pay',
            'base_pay','seniority_wage','living_wage','probationary_wages','other_fees','living_allowances',
            'comprehensive_benefits','retention_allowance','life_allowance','retirement_subsidy',
            'retirement_fee_increase','national_pension_increase','reform_subsidy','reform_allowance',
            'technical_grade_wage','staff_scale_salary','margin_difference','festival_bonus','living_subsidies',
            'special_post_allowance','temporary_post_allowance','normative_subsidy','special_allowance',
            'post_allowance','career_post_allowance','grassroots_subsidies','pay_only_child','other_additions',
            'other_income_1','other_income_2','other_income_3','other_income_4','other_more_less',

            'accumulation_fund','endowment_insurance','medical_insurance','unemployment_insurance','occupational_pension',
            'whole_medical','supplement_accumulation_fund','supplement_endowment_insurance','supplement_medical_insurance',
            'supplement_unemployment_insurance','supplement_occupational_pension','income_tax','other_deduction','total_deduct',

            'unit_accumulation_fund','unit_occupational_pension','unit_endowment_insurance','unit_medical_insurance',
            'unit_unemployment_insurance','unit_injury_insurance','unit_supplement_accumulation_fund','unit_supplement_occupational_pension',
            'unit_supplement_endowment_insurance','unit_supplement_medical_insurance','unit_supplement_unemployment_insurance',
            'unit_supplement_injury_insurance','total_unit_payment',
            'housing_subsidies','remark',
            'bank_name','bank_account',

            'post_allowance','working_subsidy','temporary_subsidy','annual_assessment_bonus','other','pre_tax_income','after_tax_income',
            'exemption','annuity','pay','unit_social_security_number','personal_social_security_number','unit_of_value',
		); 
		// $list = g('dao_salary') -> get_by_id($this -> com_id, $salary_id, $field);
        $or_cond = array();
		$cond = array(
			'year=' => $year,
			'month=' => $month,
		);

		if(!empty($user_info['person_number'])){
            $or_cond[] = array(
                'person_number=' => $user_info['person_number'],
                'type='=>\model\api\dao\Salary::SALARY_TYPE_PERSON_NUMBER,
            );
        }

        if(!empty($user_info['id_card'])){
            $or_cond[] = array(
                'id_card=' => $user_info['id_card'],
                'type='=>\model\api\dao\Salary::SALARY_TYPE_ID_CARD,
            );
        }


        if($or_cond){
            $cond['__OR'] = $or_cond;
            $list = g('dao_salary') -> get_data_by_cond($this -> com_id, $cond, $field);
        }

		if (!empty($list)) {
			$list = array_shift($list);
			$list['print_date'] = date('Y年m月', $list['print_date']);
            $list['id_card'] = displayIdcard($list['id_card']);
		} else {
			$list = array();
		}

		if(empty($list)){
            throw new \Exception('身份证无法匹配，请联系管理员！');
        }

		$list['name'] = $user_info['name'];
		$list['dept'] = $user_info['dept'];
		
		$info = array(
			'info' => $list,
		);
		parent::echo_ok($info);
	}

}

//end of file