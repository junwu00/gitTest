<?php
/**
 * 公用提示页面
 * @author yangpz
 * @date 2016-07-22
 *
 */
namespace model\vip\view;

class VipNotice extends \model\api\view\ViewBase {
	
	public function __construct() {
		parent::__construct('vip');
	}
	
}

//end