<?php

/**
 * vip接口
 * @author chenyihao
 * @date 2016-08-03
 */

namespace model\vip\view;

class VipAjax extends \model\api\view\ViewBase {
	
	//服务类
	private $Server = '';

	public function __construct() {
		$this -> Server = g('ser_vip');

		parent::__construct('vip');
	}
	
	/** 单入口方法 */
	public function index() {
		$cmd = g('pkg_val') -> get_get('cmd', FALSE);
		switch ($cmd) {
			case 101 : {
				//获取vip信息
				$this -> get_vip_info();
				break;
			}
			case 102 : {
				//获取vip活动列表
				$this -> get_vip_activity();
				break;
			}
			default:{
				parent::echo_busy();
				break;
			}
		}
	}
		
//-----------------------接口内部实现-----------------------------------
	
	/**
	 * 获取vip信息
	 * @return
	 */
	public function get_vip_info() {
		try {
         	parent::log_i('开始获取vip信息');
			/**
		 	 * IS_VIP：1：普通用户，2、VIP，3、SVIP
		 	 */
	 		$ret = parent::get_vip_power(false);
	 		$is_vip = $ret['is_vip']+1;
	 		$vip_state = $ret['vip_state'];
	 		$past_time = $ret['past_time'];
	 		$past_date = $vip_state ? (int)ceil(($past_time - time()) / (60 * 60 * 24)) : 0;
	 		$past_date > 30 && $past_date = 0;

			$com_info = g('dao_com')->get_by_id($this->com_id, 'name, corp_logo');

			$info = array(
				'name' 		=> $com_info['name'],
				'logo' 		=> $com_info['corp_logo'],
				'is_vip' 	=> (string)$is_vip,		
				'vip_state' => (string)$vip_state,		
				'past_time' => (string)$past_time,
				'past_date' => (string)$past_date,
				'onetime_msg' => parent::get_onetime_msg()
				);

         	parent::log_i('成功获取vip信息');
			parent::echo_ok(array('info' => $info));			
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

	/** 获取vip活动列表 */
	private function get_vip_activity() {
		try {
			parent::log_i('开始获取vip活动列表');

			$ad_list = $this -> Server -> get_vip_activity();

			parent::log_i('成功获取vip活动列表');
			parent::echo_ok(array('info' => $ad_list));			
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}


//------------------------内部实现---------------------

}
//end