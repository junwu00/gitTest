<?php
/**
 * vip
 * @author Luja
 * @date 2016-10-18
 *
 */

namespace model\vip\server;

class ServerVipServer extends \model\api\server\ServerBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('vip', $log_pre_str);
	}

	/**
	 * 是否是vip（包括svip）
	 * @param  boolean $svip   true：仅判断svip
	 * @return boolean         
	 */
	public function check_vip($svip=FALSE) {
		return g('dao_vip') -> check_vip($this -> com_id, $svip);
	}

	/**
	 * 获取vip活动列表
	 * @return
	 */
	public function get_vip_activity() {
		$fields = 'id,pic_url,link_url,create_time';
		return g('dao_vip') -> get_vip_activities($fields);
	}
}

//end