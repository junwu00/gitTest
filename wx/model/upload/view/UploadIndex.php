<?php

namespace model\upload\view;

class UploadIndex{
	
	private $api_resp;
	private $pkg_val;

	public function __construct() {
		$this -> api_resp = g('api_resp');
		$this -> pkg_val = g('pkg_val');
	}

	//命令对应函数名的映射表
    private $cmd_map = array(
        //上传文件
        101 => 'upload_file',
        //查询文件是否已存在，如果存在，则返回对应信息
        102 => 'get_file_info',
        //获取预览链接
        103 => 'get_preview_url',
        //jssdk
        104 => 'local_jssdk_upload',
    );

    /**
     * ajax行为统一调用方法
     *
     * @access public
     * @return void
     */
    public function index() {
        $cmd = (int)$this -> pkg_val -> get_value('cmd', FALSE);
        empty($cmd) and $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);

        $map = $this -> cmd_map;
        !isset($map[$cmd]) and $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);

        $this -> $map[$cmd]();
    }

    /**
     * 获取预览url
     *
     * @access public  
     * @return void
     */
    private function get_preview_url() {
        //云存储查询哈希
        $hash = $this -> pkg_val -> get_value('hash');
        $ext = $this -> pkg_val -> get_value('ext');

        $hash = trim($hash);
        $ext = strtolower(trim($ext));

        if (empty($hash) or strlen($hash) != 32) {
            cls_resp::echo_busy();
        }

        $url = g('api_media') -> cloud_rename_preview('预览文件', $hash, '', false);
        $info = array(
            'preview_url' => $url,
        );

        //已经存在
        $this -> api_resp -> echo_ok($info);
    }

    /**
     * 上传文件
     *
     * @access private
     * @return void
     */
    private function upload_file() {
        //判断上传方式
        $utype = (int)$this -> pkg_val -> get_value('utype');
        switch ($utype) {
            case 2 : {
                //js_sdk上传
                $this -> jssdk_upload();
                BREAK;
            }
            case 3 : {
                //分段上传
                $this -> part_upload();
                BREAK;
            }
            default : {
                //常规上传
                $this -> normal_upload();
                BREAK;
            }
        }
    }

    /**
     * 查询文件是否已存在，如果存在，则返回对应信息
     *
     * @access public  
     * @return void
     */
    private function get_file_info() {
        //文件哈希
        $hash = $this -> pkg_val -> get_value('hash');
        //文件扩展名
        $ext = $this -> pkg_val -> get_value('ext');

        $hash = trim($hash);
        $ext = trim($ext);

        if (empty($hash) or empty($ext)) {
            // log_write('查询云存储是否已经存在该文件，参数不合法');
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }

        try {
            $info = g('api_media') -> cloud_get_info($hash, $ext);
            // log_write("查询云存储是否已经存在该文件，查询成功");
        }catch(\Exception $e) {
            // log_write("查询云存储是否已经存在该文件，查询异常：" . $e -> getMessage());
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }

        //已经存在
        $this -> api_resp -> echo_ok($info);
    }

    /**
     * 支持断点续传的上传实现
     *
     * @access private
     * @return array
     * @throws \Exception
     */
    private function part_upload() {
        //每一片段最大为1MB
        $part_size = 1 * 1024 * 1024;
        //文件总大小的最大值
        $file_size = 20 * 1024 * 1024;

        //文件内容哈希
        $hash = $this -> pkg_val -> get_value('hash');
        //片段在源文件中的开始位置
        $offset = (int)$this -> pkg_val -> get_value('offset');
        //文件扩展名
        $ext = $this -> pkg_val -> get_value('ext');
        //文件内容片段
        $part = $this -> pkg_val -> get_value('part');
        //是否最后一条片段
        $is_end = (int)$this -> pkg_val -> get_value('is_end');

        $hash = trim($hash);
        $ext = trim($ext);

        $is_end = $is_end ? TRUE : FALSE;

        if (empty($hash) or empty($ext) or (empty($part) and !$is_end)) {
            // log_write('分段上传，参数不合法');
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }

        $data = array(
            'hash' => $hash,
            'offset' => $offset,
            'ext' => $ext,
            'part' => $part,
            'is_end' => $is_end,
        );

        $max_size = array(
            'part' => $part_size,
            'file' => $file_size,
        );

        try {
            $ret = g('api_media') -> part_upload($data, $max_size);
        }catch(\Exception $e) {
            // log_write('上传文件片段失败，异常：[' . $e -> getMessage() . ']');
            //cls_resp::echo_err(cls_resp::$Busy, $e -> getMessage());
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }

        if ($ret['status'] !== 1) {
            $info = array(
                'status' => $ret['status'],
                'offset' => $ret['offset'],
            );

            //单个片段上传完成
            $this -> api_resp -> echo_ok($info);
        }

        // log_write('分段上传文件成功');

        $info = array(
            'status' => 1,
            'data' => $ret['info'],
        );

        $this -> api_resp -> echo_ok($info);
    }

    /**
     * 常规上传实现
     *
     * @access private
     * @return void
     */
    private function normal_upload() {
        // log_write('开始常规上传文件');

        $file = array_shift($_FILES);
        if (empty($file) or !is_array($file)) {
            $tip_str = '找不到该上传文件，请重新上传！';
            // log_write($tip_str);
            $this -> api_resp -> echo_err(\model\api\server\Response::$Busy,$tip_str);
        }

        try {
            //本地测试使用
	        // $data = g('api_media') -> local_upload($file,1024*1024*10);
            //线上使用
            $data = g('api_media') -> cloud_upload('', $file);
        }catch(\Exception $e) {
            // log_write('常规上传文件失败，异常：[' . $e -> getMessage() . ']');
            $this -> api_resp -> echo_exp($e);
        }
        // log_write('常规上传文件成功');

        $data['image_path'] = MEDIA_URL_PREFFIX . $data['path'];
        $this -> api_resp -> echo_ok($data);
    }

    /**
     * jssdk获取图片，再上传图片
     *
     * @access private
     * @return void
     */
    private function jssdk_upload() {
    	//验证是否允许上传
        $is_ckeck = $this -> pkg_val -> get_value('check');
        if (!empty($is_ckeck)) {
	        $cap_info = g('dao_capacity') -> get_capacity_info();
			if ($cap_info['is_full']) {
				$forbit_time = $cap_info['notice_time'] + $cap_info['pre_buffer_time'] + $cap_info['buffer_time'];
				if ($cap_info['info_time'] >= $forbit_time) {
					$this -> api_resp -> echo_err(\model\api\server\Response::$Busy,  '您的企业服务器 <font color="#f74949">空间容量不足！</font><br>图片和文件上传功能暂时无法使用<br>请告知管理员扩容空间喔!');
				}
			}
			$this -> api_resp -> echo_ok();
        }
        
        // log_write('开始使用js_sdk上传图片');
        $media_id = $this -> pkg_val -> get_value('media_id');
        $media_id = trim($media_id);
        $com_id = (int)$this -> pkg_val -> get_value('com_id');

        if (empty($media_id)) {
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }
        $com_id === 0 and $com_id = $_SESSION[SESSION_VISIT_COM_ID];
        $token = g('api_atoken') -> get_by_com($com_id);
        if (empty($token)) {
            $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
        }

        try {
            $result = g('api_media') -> cloud_get_media($token, $media_id);
            // log_write('js_sdk上传图片成功');
            $result['image_path'] = MEDIA_URL_PREFFIX.$result['path'];
            $this -> api_resp -> echo_ok($result);
            
        }catch(\Exception $e) {
            // log_write('js_sdk上传图片失败，异常：[' . $e -> getMessage() . ']');
            $this -> api_resp -> echo_exp($e);
        }
    }

    private function local_jssdk_upload(){
        try{
            $media_id = $this -> pkg_val -> get_value('media_id');
            $media_id = trim($media_id);
            $com_id = (int)$this -> pkg_val -> get_value('com_id');

            if (empty($media_id)) {
                $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
            }
            $com_id === 0 and $com_id = $_SESSION[SESSION_VISIT_COM_ID];
            $token = g('api_atoken') -> get_by_com($com_id);
            if (empty($token)) {
                $this -> api_resp -> echo_resp(\model\api\server\Response::$Busy);
            }

            //获取企业微信素材
            $ret = g('wxqy_media')->get_media($token,$media_id);
            if(empty($ret['file']) && empty($ret['info']))
                throw new \Exception('素材下载失败');
            $ext = 'png';
            switch ($ret['info']['content_type']){
                case 'image/jpeg':
                    $ext = 'jpeg';
                    break;
                case 'image/bmp':
                    $ext = 'bmp';
                    break;
                case 'image/gif':
                    $ext ='gif';
                    break;
                case 'image/png':
                    $ext ='png';
                    break;
                case 'image/webp':
                    $ext ='webp';
                    break;
                default:
                    throw new \Exception('仅支持jpg、jpeg和png等图片类型上传');
                    break;
            }

            $file = MAIN_DATA.md5($ret['file']).'.'.$ext;
            file_put_contents($file,$ret['file']);

            $result =  g('api_media') -> cloud_upload('', $file);
            $result['image_path'] = MEDIA_URL_PREFFIX.$result['path'];

            unlink($file);
            $this -> api_resp -> echo_ok($result);
        }catch (\Exception $e){
            $this -> api_resp -> echo_exp($e);
        }
    }
}




/* End of this file */