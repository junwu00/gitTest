<?php

/**
 * 登录逻辑调整
 * 
 * @author gch
 * @date 2017-09-28 14:43:36
 */

namespace model\login\view;

class LoginIndex extends \model\api\view\ViewBase {

    private $login_server;
	
	//命令对应函数名的映射表
    private $cmd_map = array(
        //企业微信确认登录
        203 => 'wxwork_qrcode_login_confirm',
        //企业微信取消登录
        204 => 'wxwork_qrcode_login_cancel',
        //企业微信登录
        206 => 'wxwork_qrcode_login',
    );

	public function __construct() {

        $this->login_server = new \model\login\server\ServerLoginIndex();
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function ajax() {
		try {
			$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
			if (empty($cmd)) throw new \Exception('命令参数错误');

			$map = $this -> cmd_map;
			if (!isset($map[$cmd])) throw new \Exception('命令参数错误');

			$this -> $map[$cmd]();
		} catch (\Exception $e) {
			parent::echo_err(\model\api\server\Response::$Busy, $e -> getMessage());
		}

	}

// --------------------------接口内部实现函数------------------------------------------------

    /** 企业微信扫码登录 */
    private function wxwork_qrcode_login() {
        try{
            $corp_url = g('pkg_val') -> get_get('corp_url', true);
            $uid = g('pkg_val') -> get_get('uid', true);
            $src_url = g('pkg_val') -> get_get('src_url', true);
            $app_idx = g('pkg_val') -> get_get('app_idx', true);
            $code = g('pkg_val') -> get_get('code', true);

            $scanRes = $this->login_server->wxworkQrcodeLogin($corp_url, $uid, $src_url, $code, intval($app_idx));
            if (!empty($scanRes)) {
                $redirectUrl = urldecode($src_url) . "&corp_url={$scanRes['corp_url']}&uid={$scanRes['uid']}&status={$scanRes['status']}&flag={$scanRes['flag']}";
                header('Location:'.$redirectUrl);
                exit;
            }

            $redirectUrl = $this->login_server->getWxworkQrcodeLoginUrl($corp_url, $uid, $src_url, ++$app_idx);
            header('Location:'.$redirectUrl);

            exit;
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    private function wxwork_qrcode_login_confirm(){
        try{
            $corp_url = g('pkg_val') -> get_get('corp_url', true);
            $uid = g('pkg_val') -> get_get('uid', true);

            $res = $this->login_server->wxworkQrcodeLoginConfirm($corp_url,$uid);

            parent::echo_ok(['data'=>$res]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    private function wxwork_qrcode_login_cancel(){
        try{
            $corp_url = g('pkg_val') -> get_get('corp_url', true);
            $uid = g('pkg_val') -> get_get('uid', true);

            $res = $this->login_server->wxworkQrcodeLoginCancle($corp_url,$uid);

            parent::echo_ok(['data'=>$res]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

}

//end of file