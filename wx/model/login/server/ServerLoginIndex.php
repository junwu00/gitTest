<?php
/**
 * 主框架服务类
 * @author chenyihao
 * @date 2016-08-20
 *
 */

namespace model\login\server;

class ServerLoginIndex extends \model\api\server\ServerBase {
    /** 企业微信登录二维码有效时长（秒） */
    const WXWORK_LOGIN_QRCODE_ALIVE_TIME = 120;

    /** 扫码过期 */
    const QRCODE_STATUS_INVALID= 'INVALID';
    /** 等待扫码 */
    const QRCODE_STATUS_WAIT= 'WAIT';
    /** 已扫码 */
    const QRCODE_STATUS_SCANED = 'SCANED';
    /** 确认扫码登录 */
    const QRCODE_STATUS_CONFIRM = 'CONFIRM';
    /** 确认扫码取消 */
    const QRCODE_STATUS_CANCEL = 'CANCEL';
    /** 无权限 */
    const QRCODE_STATUS_NO_PERMISSION = 'NO_PERMISSION';
    /** 账号不存在 */
    const QRCODE_STATUS_ACCOUNT_INVALID = 'ACCOUNT_INVALID';

	public function __construct($log_pre_str='') {
		global $mod_name;
		parent::__construct($mod_name, $log_pre_str);
	}

    /** 企业微信扫码登录操作 */
    public function wxworkQrcodeLogin($corp_url, $uid,$srcUrl, $code,$appIdx = 0) {
        $corp_url = $this->checkCorpUrl($corp_url);

        $cacheKey = $this->genertateCacheKey($uid);
        $cacheData = g('pkg_redis')->get($cacheKey);
        if (empty($cacheData)) {
            to_log('Info','','无效二维码:缓存不存在');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => '0'];
        }
        $cacheData = json_decode($cacheData, true);
        if (empty($cacheData)) {
            to_log('Info','','无效二维码:缓存不是json');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => '1'];
        }

        if ($cacheData['status'] != self::QRCODE_STATUS_WAIT) {
            to_log('Info','','无效二维码:状态不是等待中');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => '2'];
        }

        // 依次尝试登录
        $appList = array(1,23,14,9,12,18,1000080);
        if (!isset($appList[$appIdx])) {
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_NO_PERMISSION, "flag" => "0"];
        }

        /** @var ComWxwork $comWxwork */
        $com_info = g('dao_com')->get_by_corp_url($corp_url);
        if (empty($com_info)) {
            to_log('Info','','无效二维码:企业信息不对'.$corp_url);
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => '3'];
        }

        /** @var ComApp $comApp */
        $comApp = g('dao_com_app')->get_by_app_id($appList[$appIdx],$com_info['id']);
        if (empty($comApp) || $comApp['state']!=1) {   // 应用未安装或未启用
            return [];
        }

        try {
            //通过code换取用户的身份信息
            //获取接口调用凭证
            $access_token = g('api_atoken') -> get_by_com($com_info['id'],$appList[$appIdx]);
            $wxqy_oauth_server = new \package\weixin\qy\WXQYOAuth();
            $userId = $wxqy_oauth_server->get_oauth_user_id($access_token,$code,$comApp['wx_app_id']);

        } catch (\Exception $e) {
            to_log('Info','',"扫码登录失败：corp_url={$corp_url}, app_id={$appList[$appIdx]}");
            return [];  // 表示自动跳转下一个应用尝试登录
        }

        if ($userId) {
            $cacheData['status'] = self::QRCODE_STATUS_SCANED;
            $cacheData['data']['user_id'] = $userId;

            g('pkg_redis')->set($cacheKey, json_encode($cacheData), self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_SCANED, "flag" => "0"];
        }

        if (!isset($appList[++$appIdx])) {
            $cacheData['status'] = self::QRCODE_STATUS_SCANED;
            g('pkg_redis')->set($cacheKey, json_encode($cacheData), self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_NO_PERMISSION, "flag" => "1"];
        }

        return [];  // 表示自动跳转下一个应用尝试登录
    }

    /** 获取企业微信扫码登录链接 */
    public function getWxworkQrcodeLoginUrl($corp_url, $uid, $src_url, $appIdx = 0) {
        $corp_url = $this->checkCorpUrl($corp_url);

        //获取企业信息
        $com_info = g('dao_com')->get_by_corp_url($corp_url);
        if(!$com_info)
            throw new \Exception('企业不存在');


        $cacheKey = $this->genertateCacheKey($uid);
        $cacheData = [
            'status' => self::QRCODE_STATUS_WAIT,
            'data' => [],
        ];
        g('pkg_redis')->set($cacheKey,json_encode($cacheData),self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);

        $redirectUrl = MAIN_DYNAMIC_DOMAIN . "index.php?model=login&m=index&a=ajax&cmd=206&corp_url={$corp_url}&uid={$uid}&app_idx={$appIdx}&src_url=" . urlencode($src_url);
        $redirectUrl = urlencode($redirectUrl);

        // 实际登录地址
        $realUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$com_info['corp_id']}&redirect_uri={$redirectUrl}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
        // 短登录地址，避免二维码过于密集
        $shortUrl = QY_DYNAMIC_DOMAIN . "index.php?model=login&a=ajax&cmd=205&uid={$uid}";
        $urlCacheKey = $this->genertateCacheKey($uid . ':short');
        g('pkg_redis')->set($urlCacheKey, $realUrl, self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);

        return $shortUrl;
    }

    /** 企业微信扫码登录-确认登录 */
    public function wxworkQrcodeLoginConfirm($corp_url,$uid) {
        $corp_url = $this->checkCorpUrl($corp_url);

        $cacheKey = $this->genertateCacheKey($uid);
        $cacheData = g('pkg_redis')->get($cacheKey);
        to_log('Info','',$cacheData);
        if (empty($cacheData)) {
            to_log('Info','','无效二维码:缓存不存在');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "0"];
        }
        $cacheData = json_decode($cacheData, true);
        if (empty($cacheData)) {
            to_log('Info','','无效二维码:缓存不是json');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "1"];
        }
        if ($cacheData['status'] != self::QRCODE_STATUS_SCANED) {
            to_log('Info','','无效二维码:已被扫码');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "2"];
        }
        if (!isset($cacheData['data']['user_id'])) {
            to_log('Info','','账号错误');
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_ACCOUNT_INVALID, "flag" => "3"];
        }

        $is_admin = $this->checkAdmin($corp_url,$cacheData['data']['user_id']);
        if(!$is_admin){
            $cacheData['status'] = self::QRCODE_STATUS_ACCOUNT_INVALID;
            g('pkg_redis')->set($cacheKey, json_encode($cacheData), self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_ACCOUNT_INVALID];
        }

        $token = '';
        $cacheData['status'] = self::QRCODE_STATUS_CONFIRM;
        $cacheData['data']['token'] = $token;
        g('pkg_redis')->set($cacheKey, json_encode($cacheData), self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);
        return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_CONFIRM,'token'=>$token];
    }

    /** 企业微信扫码登录-取消登录 */
    public function wxworkQrcodeLoginCancle($corp_url, $uid) {
        $corp_url = $this->checkCorpUrl($corp_url);

        $cacheKey = $this->genertateCacheKey($uid);
        $cacheData = g('pkg_redis')->get($cacheKey);

        to_log('Info','','二维码情况:'.$cacheData);

        if (empty($cacheData)) {
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "0"];
        }
        $cacheData = json_decode($cacheData, true);
        if (empty($cacheData)) {
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "1"];
        }
        if ($cacheData['status'] != self::QRCODE_STATUS_SCANED) {
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "2"];
        }
        if (!isset($cacheData['data']['user_id'])) {
            return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_INVALID, "flag" => "3"];
        }

        $cacheData['status'] = self::QRCODE_STATUS_CANCEL;
        g('pkg_redis')->set($cacheKey, json_encode($cacheData), self::WXWORK_LOGIN_QRCODE_ALIVE_TIME);
        return ["corp_url" => $corp_url, "uid" => $uid, "status" => self::QRCODE_STATUS_CANCEL];
    }

    /**
     * 完成登录
     * @param $corp_url
     * @param $userId
     */
    private function checkAdmin($corp_url, $user_id){
        try{
            //获取企业信息
            $com_info = g('dao_com')->get_by_corp_url($corp_url);
            if(!$com_info)
                throw new \Exception('企业不存在');

            //获取用户账号信息
            $user_info = g('dao_user')->get_by_acct($user_id,'*',NULL,false,$com_info['dept_id']);
            if(!$user_info)
                throw new \Exception('用户信息不存在');

            //查看是否已经绑定
            $admin_info = g('dao_admin')->check_admin($com_info['id'],$user_info['id'],'*');
            if(empty($admin_info))
                throw new \Exception('管理员不存在');

            return $admin_info;
        }catch (\Exception $e){
            to_log('Info','',$user_id.':登录不成功问题:'.$e->getMessage());
            return false;
        }
    }


    /**
     * 空corpurl 默认指定第一个企业
     * @param $corp_url
     * @return string
     */
    private function checkCorpUrl($corp_url){
        if(empty($corp_url)){
            //默认那第一个企业的corp_url
            $coms = g('dao_com')->list_company();
            $corp_url = isset($coms[0]['corp_url'])?$coms[0]['corp_url']:'';
        }

        return $corp_url;
    }


    private function genertateCacheKey($key) {
        return md5(basename(__FILE__)) . ':' . $key;
    }




}

//end