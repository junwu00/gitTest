<?php
/**
 * 模块单入口文件
 */

load_config('model/api/session');											//加载session常量声明的配置
load_config('model/api/classmap');											//引入公用的类映射配置文件
load_config('model/api/errcode');											//引入公用的类映射配置文件

g('pkg_rsession') -> start();

//加载必要的配置文件
$mod_dir = dirname(__FILE__);					//模块的根路径
$mod_name = pathinfo($mod_dir);
$mod_name = $mod_name['basename'];

$mod_dir = dirname(__FILE__);												//模块的根路径
$mod_conf = load_config('model/' . $mod_name . '/model');								//模块信息配置文件
load_config('model/' . $mod_name . '/classmap');										//引入模块内类映射配置文件
load_config('model/' . $mod_name . '/errcode');										//引入错误码文件
//登录验证

//当前模块内逻辑
$mod = g('pkg_val') -> get_get('m');
$act = g('pkg_val') -> get_get('a');
empty($mod) && $mod = 'index';
empty($act) && $act = 'index';

$cls_name = ucwords($mod_conf['name']).ucwords($mod);
$mod_file = $mod_dir . DS . 'view' . DS . $cls_name . '.php';
$mod_cls = "\\model\\{$mod_conf['name']}\\view\\{$cls_name}";

!file_exists($mod_file) && g('view_notice') -> error('页面不存在');
include_once ($mod_file);
if (!file_exists($mod_file) || !class_exists($mod_cls, FALSE) || !method_exists($mod_cls, $act)) {
	g('view_notice') -> error('页面不存在');
}

g('view_' . $mod) -> $act();	//处理业务逻辑

// end of file