<?php
/**
 * 移动外勤数据模型
 *
 * @author LiangJianMing
 * @create 2015-02-09
 */
namespace model\legwork\dao;

class DaoLegwork extends \model\api\dao\DaoBase {
	private $app_name = 'legwork';

	//数据库操作基础条件
	private $base_con	= NULL;

	private $state_on	= 1;

	//移动外勤分类表
	public $classify_table 	= 'legwork_classify';
	//移动外勤上传文件表
	public $file_table 		= 'legwork_file';
	//计划查看记录表
	public $see_table 		= 'legwork_see';
	//移动外勤配置表
	public $conf_table 	 	= 'legwork_conf';
	//移动外勤计划表
	public $plan_table 	 	= 'legwork_plan';
	//移动外勤上报记录表
	public $report_table 	= 'legwork_record';
	//用户信息表
	public $user_table 	 	= 'sc_user';
	//部门信息表
	public $dept_table 	 	= 'sc_dept';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		if (isset($_SESSION[SESSION_VISIT_COM_ID])) {
			$this -> com_id  = $_SESSION[SESSION_VISIT_COM_ID];
			$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$this -> base_con = array(
				'com_id=' => $this -> com_id,
				'user_id=' => $this -> user_id,
			);

			$this -> root_id = $_SESSION[SESSION_VISIT_DEPT_ID];
		}
	}

	

	/**
	 * 新建计划
	 *
	 * @access public
	 * @param array $data 数据集合
	 * @return boolean
	 */
	public function add_plan($data=array()) {
		if (empty($data)) return FALSE;
		$ret = g('pkg_db') -> insert($this -> plan_table, $data);
		return $ret ? $ret : FALSE;
	}

	

	/**
	 * 编辑计划
	 * @param  array  $data  
	 * @param  array  $cond  
	 * @return [type]        
	 */
	public function update_plan($data=array(), $cond=array()) {
		$ret = g('pkg_db') -> update($this -> plan_table, $cond, $data);
		return $ret ? $ret : FALSE;
	}

	/**
	 * 删除一个计划
	 *
	 * @access public
	 * @param array $cond 
	 * @param array $data 
	 * @return boolean
	 */
	public function delete_plan($data=array(), $cond=array()) {
		$result = g('pkg_db') -> update($this -> plan_table, $cond, $data);
		return $result;
	}

	/**
	 * 上报一次移动外勤状况
	 * @param array $data 
	 */
	public function add_report($data=array()) {
		if (empty($data)) return FALSE;
		$ret = g('pkg_db') -> insert($this -> report_table, $data);
		return $ret ? $ret : FALSE;
	}

	/**
	 * 保存文件
	 * @param  array  $file 
	 * @return [type]       
	 */
	public function save_file($file=array()) {
		if (empty($file)) return FALSE;
		$ret = g('pkg_db') -> insert($this -> file_table, $file);
		return $ret ? $ret : FALSE;
	}

	/**
	 * 获取计划详情
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param string $plan_fields 
	 * @param boolean $get_rep 是否获取报告列表
	 * @return mixed
	 */
	public function get_plan_info($pn_id, $plan_fields='', $get_rep=TRUE) {
		$user_id = $this -> user_id;
		$table = $this -> plan_table . ' pl, ' . $this -> classify_table . ' cl ';
		if (empty($plan_fields)) {
			$plan_fields = array(
				'pl.pn_id id', 'pl.classify_id', 'cl.name classify_name', 'pl.title', 'pl.desc', 
				'pl.state', 'pl.cple_state', 'pl.is_publish', 'cl.sign_pic_type', 'pl.create_time', 
				'pl.begin_time start_time', 'pl.end_time', 'pl.recevicer_list', 'pl.notify_list', 
				'pl.user_id creater'
				);
			$plan_fields = implode(',', $plan_fields);
		}

		$plan_cond = array(
			'pl.com_id='	=> $this -> com_id,
			'pl.pn_id='		=> $pn_id,
			'pl.is_del='	=> 0,
			'^pl.classify_id=' => 'cl.id',
		);
		// 获取计划信息
		$plan_info = g('pkg_db') -> select($table, $plan_fields, $plan_cond);
		
		if (!empty($plan_info)) {
			// 不获取报告列表直接返回数据
			if (!$get_rep) return $plan_info ? $plan_info[0] : FALSE;
			
			$rep_fields = array(
				'id', 'longt', 'lat', 'mark', 'address', 'image_list', 'create_time',
				);
			$rep_fields = implode(',', $rep_fields);
			$rep_cond = array(
				'com_id=' => $this -> com_id,
				'pn_id=' => $pn_id,
				'is_del=' => 0,
				);
			$order = ' ORDER BY id ASC';
			// 获取报告列表
			$rep_list = g('pkg_db') -> select($this -> report_table, $rep_fields, $rep_cond, 0, 0, '', $order, FALSE);

			if (!empty($rep_list)) {
				foreach ($rep_list as $key => &$record) {
					$record['image_list'] = json_decode($record['image_list'], TRUE);
				}unset($record);
			}

			empty($rep_list) && $rep_list = array();
			$plan_info && $plan_info[0]['sign_list'] = $rep_list;
		}
		
		return $plan_info ? $plan_info[0] : FALSE;
	}

	/**
	 * 检查计划是否存在
	 * @param  string $fields 
	 * @param  array  $cond   
	 * @return [type]         
	 */
	public function check_plan($fields='*', $cond=array()) {
		$cond['com_id='] = $this -> com_id;
		$cond['is_del='] = 0;

		$plan = g('pkg_db') -> select($this -> plan_table, $fields, $cond);
		return $plan[0] ? $plan[0] : FALSE;
	}

	/**
	 * 获取计划列表
	 */
	public function get_plan_list($table, $fields='', $cond=array(), $page=1, $page_size=10, $order='') {
		empty($order) && $order = ' ORDER BY pl.cple_state ASC, pl.pn_id DESC ';

		$ret = g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, '', $order, TRUE);
		return $ret;
	}

	/**
	 * 获取下属的用户id
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @return array
	 */
	public function get_subordinate($user_id = 0) {
		empty($user_id) and $user_id = $this -> user_id;
		$key_str = __FUNCTION__ . ':user_id:' . $user_id . ':com_id:' . $this -> com_id;
		$cache_key = $this -> get_cache_key($key_str);
		$ret = g('pkg_redis') -> get($cache_key);

		if ($ret) {
			$ret = json_decode($ret, TRUE);
			return $ret;
		}

		$ret = array(
			'user_ids' => array(),
			'dept_map' => array(),
		);

		//上下级关系方式
		try {
			$conf = $this -> get_conf();
			
		} catch(\Exception $e) {
			return $ret;
		}
		
		$sub_list = g('api_leader_relationship') -> list_sub_by_user_id($this -> user_id);
		$sub_dept_ids = $sub_list['depts'];
		$sub_user_ids = $sub_list['users'];
		if (isset($conf['all_junior']) and $conf['all_junior'] != 0) {	//所有下属：下级成员+下级部门及其子..部门成员
			$compare_field = 'dept_tree';
			
		} else {														//直属下属：下级成员+下级部门的直属成员
			$compare_field = 'dept_list';
		}
		
		$or_con = array();
		foreach ($sub_dept_ids as $key => $val) {
			$or_con['__' . $key . '__' .  $compare_field . ' LIKE '] = '%"' . $val . '"%';
		}
		unset($val);

		$result = array();
		if (!empty($or_con)) {
			$condition = array(
				'state!=' => 0,
				'root_id=' => $this -> root_id,
				'__OR_1' => $or_con,
			);
			$condition['id!='] = $this -> user_id;
			$where = g('pkg_db') -> compose_prepare_where($condition);
			log_write('移动外勤，查找直属下属集合，where=' . $where['sql']);
	
			$sql = 'SELECT DISTINCT `id` FROM ' . $this -> user_table . $where['sql'];
			$result = g('pkg_db') -> prepare_query($sql, 1, $where['param']);
		}
		!$result && $result = array();
		
		$ret['user_ids'] = $sub_user_ids;
		foreach ($result as $val) {
			$ret['user_ids'][] = $val['id'];
		}unset($val);
		$ret['user_ids'] = array_unique($ret['user_ids']);
		//缓存60秒
		g('pkg_redis') -> set($cache_key, json_encode($ret), 60);
		return $ret;
	}

	/**
	 * 获取已签到的计划及计划最后一次上报情况
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param boolean $is_self 是否获取自身的计划列表
	 * @return array
	 */
	public function get_plan_with_reports($user_id = 0, $page, $page_size, $is_self = TURE) {
		empty($user_id) and $user_id = $this -> user_id;
		$ret = array();

		$ids = array($user_id);
		if (!$is_self) {
			$data = $this -> get_subordinate($user_id);
			$ids = $data['user_ids'];
			if ($ids) {	//去除自己的记录
				$idx = array_search($user_id, $ids);
				if ($idx !== false) unset($ids[$idx]);
			}
		}
		
		if (empty($ids)) return $ret;

		$table = $this -> report_table . ' AS rp, ' . $this -> user_table . ' AS us, ' . $this -> plan_table . ' AS pl';
		$fields = array(
			'rp.id', 'rp.pn_id', 
			'rp.mark_type', 'rp.address', 'rp.create_time', 
			'us.name AS user_name', 'us.pic_url', 
			'us.dept_tree', 'pl.title', 
			'pl.state', 'pl.begin_time', 'pl.end_time',
		);
		$fields = implode(',', $fields);

		$condition = array(
			'rp.com_id=' 	 => $this -> com_id,
			'rp.is_last=' 	 => 1,
			'rp.user_id IN' => $ids,
			'^rp.user_id=' 	 => 'us.id',
			'^rp.pn_id=' 	 => 'pl.pn_id',
			'pl.is_del=' 	 => 0,
		);

		empty($order) and $order = ' ORDER BY rp.id DESC';

		$ret = g('pkg_db') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);
		$ret['data'] = $this -> complete_dept_info($ret['data']);
		return $ret;
	}

	/**
	 * 获取单个计划或单个分类实时定位记录列表
	 * @param  integer $id   计划或分类id
	 * @param  integer $type 1：计划，2：分类
	 * @return array 
	 */
	public function plan_class_record($id, $type) {
		$table = $this -> report_table;
		$fields = array(
			'id', 'pn_id', 'classify_id', 'type', 'longt', 'lat', 'mark', 'address', 
			'image_list', 'create_time',
		);
		$fields = implode(',', $fields);
		
		$cond = array(
			'com_id=' => $this -> com_id,
			'is_del=' => 0,
		);
		if ($type == 1) {
			$cond['pn_id='] = $id;
			$cond['type='] = 1;
		} else {
			$cond['classify_id='] = $id;
			$cond['type='] = 2;
		}

		$order = ' ORDER BY id ASC';
		$ret = g('pkg_db') -> select($table, $fields, $cond, 0, 0, '', $order, TRUE);

		if (!empty($ret['data'])) {
			foreach ($ret['data'] as $key => &$record) {
				$record['image_list'] = json_decode($record['image_list'], TRUE);
			}
		}

		return $ret ? $ret : array();
	}

	/**
	 * 获取缓存变量名
	 *
	 * @access public
	 * @param string $str 关键字符串
	 * @return string
	 */
	public function get_cache_key($str) {
		$key_str = SYSTEM_HTTP_DOMAIN . ':' . __CLASS__ . ':' . __FUNCTION__ . ':' .$str;
		$key = md5($key_str);
		return $key;
	}

    /**
     * 获取分类列表
     * @param  integer $page      
     * @param  integer $page_size 
     * @param  string  $order     升序或倒叙
     * @return array            
     */
    public function get_classify_list($page=0, $page_size=0, $order='') {
    	$fields = array(
    		'id', 'name', 'recevicer_type', 'recevicer_list', 'recevicer_edit', 'notify_type', 'notify_list', 'notify_edit'
    	);
    	$fields = implode(',', $fields);

    	$condition = array(
    		'com_id=' 		=> $this -> com_id,
    		'info_state='	=> $this -> state_on,
    		'need_plan=' 	=> 1
    	);

    	empty($order) and $order = ' ORDER BY id DESC';

		$ret = g('pkg_db') -> select($this -> classify_table, $fields, $condition, $page, $page_size, '', $order, TRUE);

		if (!empty($ret)) {
			foreach ($ret['data'] as $key => &$class) {
				if ($class['recevicer_type'] == 2) {
					$recevicer_ids = g('dao_leader') -> list_leader_by_user_id($this -> user_id);
					$class['recevicer_list'] = json_encode($recevicer_ids);
					if (empty($recevicer_ids)) {
						$class['recevicer_type'] == 0;
						$class['recevicer_edit'] == 1;
					}
				}
				if ($class['notify_type'] == 2) {
					$notify_ids = g('dao_leader') -> list_leader_by_user_id($this -> user_id);
					$class['notify_list'] = json_encode($notify_ids);
					if (empty($recevicer_ids)) {
						$class['notify_type'] == 0;
						$class['notify_edit'] == 1;
					}
				}
			}unset($class);
		}

		return $ret;
    }

    /**
     * 获取外勤分类的信息
     * @param  string $fields 
     * @param  array  $cond   
     * @return [type]         
     */
    public function get_classify($fields='*', $cond=array()) {
		$ret = g('pkg_db') -> select($this -> classify_table, $fields, $cond);
		return $ret[0] ? $ret[0] : FALSE;
    }

    /**
     * 获取外勤记录列表
     * @param  integer $page      
     * @param  integer $page_size 
     * @param  integer $type      1：我的外勤记录，2：我接收到的外勤记录
     * @return array
     */
    public function get_record_list($page, $page_size, $type) {
    	$user_id = $this -> user_id;
    	$table = $this -> report_table . ' rp LEFT JOIN ' . $this -> plan_table . ' pl ON rp.pn_id=pl.pn_id,' . $this -> classify_table . ' cl,';
    	$fields = array(
    		'rp.id', 'pl.title AS title', 'rp.create_time', 'cl.name AS classify_name', 'rp.address', 'rp.type', 'rp.pn_id AS plan_id', 'u.id AS creater_id', 'u.name AS creater_name', 'u.pic_url AS creater_pic_url', 'rp.recevicer_list', 'rp.notify_list'
    	);
    	$condition = array(
    		'rp.com_id=' 		=> $this -> com_id,
    		'rp.is_del=' 		=> 0,
    		'^rp.user_id=' 		=> 'u.id',
    		'^rp.classify_id=' 	=> 'cl.id',
    	);
    	if ($type == 1) {
    		$condition['rp.user_id='] = $user_id;
    	}
    	if ($type == 2) {
    		$table = $this -> report_table . ' rp LEFT JOIN ' . $this -> plan_table . ' pl ON rp.pn_id=pl.pn_id LEFT JOIN ' . $this -> see_table . " se ON rp.id=se.pr_id AND se.read_type=2 AND se.user_id={$user_id}, " . $this -> classify_table . ' cl,';
    		$other = array('se.id AS se_id');
    		$fields = array_merge($fields, $other);

    		$condition['__OR_1'] = array(
				'rp.recevicer_list LIKE '	=> '%"' . $user_id . '"%',
				'rp.notify_list LIKE'		=> '%"' . $user_id . '"%',
			);
    	}
    	$fields = implode(',', $fields);
    	$order = ' ORDER BY rp.id DESC';
		
    	$table .= $this -> user_table . ' AS u ';

    	$ret = g('pkg_db') -> select($table, $fields, $condition, $page, $page_size, '', $order, TRUE);
    	if (!empty($ret['data'])) {
    		foreach ($ret['data'] as &$record) {
    			$record['title'] = $record['title'] ? $record['title'] : '';
    			if ($type == 2) {
    				$recevicer_list = json_decode($record['recevicer_list'], TRUE);
    				$notify_list = json_decode($record['notify_list'], TRUE);
    				$recevie_type = 1;
    				$record['state'] = 0;
    				if (!empty($record['se_id'])) {
    					$record['state'] = (string)1;
    				}
    				if (in_array($user_id, $notify_list)) {
    					$recevie_type = 2;
    				}
    				if (in_array($user_id, $recevicer_list)) {
    					$recevie_type = 1;
    				}
    				$record['recevie_type'] = (string)$recevie_type;
    			}
    			if ($record['type'] == 2) {
    				$record['plan_id'] = (string)0;
    			}
    			unset($record['recevicer_list']);
    			unset($record['notify_list']);
    			unset($record['se_id']);
    		}unset($record);
    	}
    	
		return $ret;
    }

    /**
     * 获取外勤记录详情
     * @param  integer $id     外勤记录id
     * @param  array  $fields 
     * @return mixed        
     */
    public function get_record_info($id,  $fields='') {
    	$table = $this -> report_table . ' re LEFT JOIN ' . $this -> plan_table . ' pl ON re.pn_id=pl.pn_id,' . $this -> classify_table . ' cl ';

    	if (empty($fields)) {
    		$fields = array(
    			're.id', 're.mark AS `desc`', 're.type', 're.classify_id', 'cl.name classify_name', 
    			're.pn_id AS plan_id', 'pl.title', 're.longt', 're.lat', 're.address',
    			'pl.begin_time start_time', 'pl.end_time end_time', 're.create_time', 're.image_list',  
    			're.recevicer_list', 're.notify_list', 're.user_id AS creater'
    			);
    		$fields = implode(',', $fields);
    	}
    	
    	$cond = array(
    		're.com_id='	=> $this -> com_id,
    		'^re.classify_id=' => 'cl.id',
    		're.id='		=> $id,
    		're.is_del='	=> 0,
    	);
		
    	$ret = g('pkg_db') -> select($table, $fields, $cond);

    	if (!empty($ret[0])) {
    		if (isset($ret[0]['image_list'])) {
    			$ret[0]['image_list'] = json_decode($ret[0]['image_list'], TRUE);
    		}
    		if (isset($ret[0]['type']) && $ret[0]['type'] == 2) {
    			isset($ret[0]['title']) && $ret[0]['title'] = '分类定位';
    			if (isset($ret[0]['plan_start'])) unset($ret[0]['plan_start']);
    			if (isset($ret[0]['plan_end'])) unset($ret[0]['plan_end']);
    		}
    	}

    	return $ret ? $ret[0] : FALSE;
    }

    /**
     * 获取定位分类和计划
     * @param  integer $page      
     * @param  integer $page_size 
     * @return array
     */
    public function get_locat_plan($page=0, $page_size=0) {
    	$table = $this -> plan_table . ' p, ' . $this -> classify_table . ' c ';
    	$pfields = array(
    		'p.pn_id AS id', 'p.title', 'c.id classify_id', 'c.name classify_name', 'c.sign_pic_type', 'p.recevicer_list',
    		'p.notify_list',
    	);
    	$pfields = implode(',', $pfields);
    	$pcond = array(
    		'p.com_id=' 	=> $this -> com_id,
    		'p.user_id='	=> $this -> user_id,
    		'p.cple_state='	=> 0,
    		'p.is_del='		=> 0,
    		'p.is_publish=' => 1,
    		'^p.classify_id=' => 'c.id',
    	);

    	$order = ' ORDER BY pn_id DESC';
		
    	$plan_list = g('pkg_db') -> select($table, $pfields, $pcond, $page, $page_size, '', $order, TRUE);
    	if (!empty($plan_list['data'])) {
    		foreach ($plan_list['data'] as &$plan) {
    			$plan['recevicer_list'] = json_decode($plan['recevicer_list'], TRUE);
    			$plan['notify_list'] = json_decode($plan['notify_list'], TRUE);

    			// 排除旧数据干扰
    			if ($plan['recevicer_list'] == NULL) {
    				$plan['recevicer_list'] = array();
    			}
    			if ($plan['notify_list'] == NULL) {
    				$plan['notify_list'] = array();
    			}
    		}unset($plan);
    	}
		
    	$cfields = array(
    		'id', 'name', 'sign_pic_type', 'recevicer_type', 'recevicer_edit', 'recevicer_list', 
    		'notify_type', 'notify_edit', 'notify_list'
    	);
    	$cfields = implode(',', $cfields);
    	$ccond = array(
    		'com_id=' => $this -> com_id,
    		'need_plan=' => 0,
    		'info_state=' => 1,	
    	);
    	$order = ' ORDER BY id DESC';

    	$classify_list = g('pkg_db') -> select($this -> classify_table, $cfields, $ccond, $page, $page_size, '', $order, TRUE);
    	if (!empty($classify_list['data'])) {
    		foreach ($classify_list['data'] as &$classify) {
    			if ($classify['recevicer_type'] == 2) {
    				$recevicer_ids = g('dao_leader') -> list_leader_by_user_id($this -> user_id);
					$classify['recevicer_list'] = $recevicer_ids;
					if (empty($notify_ids)) {
						$classify['recevicer_type'] = 0;
						$classify['recevicer_edit'] = 1;
					}
    			} elseif ($classify['recevicer_type'] == 1) {
    				$classify['recevicer_list'] = json_decode($classify['recevicer_list'], TRUE);
    			} else {
    				$classify['recevicer_list'] = array();
    			}

    			if ($classify['notify_type'] == 2) {
    				$notify_ids = g('dao_leader') -> list_leader_by_user_id($this -> user_id);
					$classify['notify_list'] = $notify_ids;
					if (empty($notify_ids)) {
						$classify['notify_type'] = 0;
						$classify['notify_edit'] = 1;
					}
    			} elseif ($classify['notify_type'] == 1) {
    				$classify['notify_list'] = json_decode($classify['notify_list'], TRUE);
    			} else {
    				$classify['notify_list'] = array();
    			}
    		}unset($classify);
    	}
		
    	$ret = array(
    		'plan' 			=> !empty($plan_list['data']) ? $plan_list['data'] : array(),
    		'plan_count' 	=> (string)$plan_list['count'],
    		'classify' 		=> !empty($classify_list['data']) ? $classify_list['data'] : array(),
    		'classify_count' => (string)$classify_list['count'],
    	);
		
    	return $ret;
    }

    /**
     * 计划完成
     * @param  integer $pn_id 计划id
     * @return boolean
     */
    public function complete_plan($pn_id) {
    	$condition = $this -> base_con;
		$condition['pn_id='] = $pn_id;
		//删除状态为0和发布状态的记录才能完成
		$condition['is_del='] = 0;
		$condition['is_publish='] = 1;

		$data = array(
			'cple_state' => 1,
			'update_time' => time(),
		);

		$result = g('pkg_db') -> update($this -> plan_table, $condition, $data);
		if (!$result) throw new \Exception('完成计划失败！');
		return TRUE;
    }

    /**
     * 查看计划或外勤记录
	 * @param  integer $id   计划或记录id
	 * @param  integer $user_id   创建计划或记录的用户id
	 * @param  integer $type 1.查看计划 2.查看记录
	 * @return boolean
     */
    public function legwork_see($id, $user_id, $type) {
    	if ($user_id == 0) return FALSE;

    	$condition = array(
    		'pr_id='		=> $id,
    		'user_id='		=> $this -> user_id,
    		'com_id='		=> $this -> com_id,
    		'read_type='	=> $type,
    	);
    	// 查看记录是否存在
    	$check = g('pkg_db') -> check_exists($this -> see_table, $condition);
		
    	if (!$check) {
    		$data = array(
    			'pr_id'			=> $id,
    			'user_id'		=> $this -> user_id,
    			'com_id'		=> $this -> com_id,
    			'read_type'		=> $type,
    			'create_time'	=> time(),
    		);

    		$ret = g('pkg_db') -> insert($this -> see_table, $data);
    		return $ret ? $ret : FALSE;
    	}

		return FALSE;    	
    }

  	/**
     * 对结果集补全部门信息
     *
     * @access private
     * @param array $data 员工信息数据结果集  
     * @return array
     */
    private function complete_dept_info(array &$data) {
        if (empty($data)) return $data;

        $table = 'sc_dept';

        $ids = array();
        foreach ($data as $val) {
            $tmp_dept = json_decode($val['dept_tree'], TRUE);
            if (!is_array($tmp_dept) or empty($tmp_dept)) continue;

            foreach ($tmp_dept as $cval) {
                if (!is_array($val)) {
                    $ids[] = $val;
                }else {
                    foreach ($cval as $dcval) {
                        $ids[] = $dcval;
                    }
                    unset($dcval);
                }
            }
            unset($cval);
        }
        unset($val);
        $ids = array_unique($ids);

        $fields = array(
            'id', 'name',
        );
        $fields = implode(',', $fields);
        $condition = array(
            'id IN' => $ids,
        );

        $depts = g('pkg_db') -> select($table, $fields, $condition);
        !is_array($depts) and $depts = array();

        $tmp_depts = array();
        foreach ($depts as $val) {
            $tmp_depts[$val['id']] = $val['name'];
        }
        unset($val);
        $depts = $tmp_depts;

        foreach ($data as &$val) {
            $val['dept_desc'] = $dept_desc = array();
            
            $tmp_dept = json_decode($val['dept_tree'], TRUE);
            if (!is_array($tmp_dept) or empty($tmp_dept)) continue;

            foreach ($tmp_dept as $cval) {
                $tmp_short = isset($depts[$cval[0]]) ? $depts[$cval[0]] : '';
                $tmp_long = '';
                
                foreach ($cval as $dcval) {
                    $tmp_suffix = empty($tmp_long) ? '' : ('>' . $tmp_long);
                    $tmp_long = $depts[$dcval] . $tmp_suffix;
                }
                unset($dcval);
                $dept_desc[] = array(
                    'short' => $tmp_short,
                    'long' => $tmp_long,
                );
            }
            unset($cval);

            $val['dept_desc'] = $dept_desc;
        }
        unset($val);

        return $data;
    }
  	
  	/**
	 * 格式化用户信息
	 * @param  array  $id_list id集合列表
	 * @param  array  $fields  需要查询的字段
	 * @return array
	 */
  	public function format_user_info(array $id_list=array(), array $fields=array()) {
  		if (empty($id_list)) return array();

  		$ids = array();
  		$name_list = array();
  		foreach ($id_list as $key => $id) {
  			$ids = array_merge($ids, $id);
  			$name_list[] = $key;
  		}
  		unset($key);
  		unset($id);

  		$ids = array_unique($ids);

  		empty($fields) && $fields = array('id', 'name', 'pic_url');
  		$fields1 = implode(',', $fields);

		$infolist = g('dao_user') -> list_by_ids($ids, $fields1, TRUE);

		// 返回的用户信息分组数组
		$ret = array();
		foreach ($name_list as $val) {
			$ret[$val] = array();
		}
		$data = array();
		foreach ($infolist as $key => $userinfo) {
			foreach ($id_list as $key => $id) {
				if (in_array($userinfo['id'], $id)) {
					foreach ($fields as $val) {
						$data[$val] = $userinfo[$val];
					}unset($val);
					$ret[$key][] = $data;
				}
			}
		}
		unset($data);
		
		return $ret;
  	}
}

/* End of this file */