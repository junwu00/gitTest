<?php
/**
 * 移动外勤全部ajax交互类
 *
 * @author LiangJianMing
 * @create 2015-2-9
 */

namespace model\legwork\view;

class LegworkAjax extends \model\api\view\ViewBase {

	private $ser_legwork;
	private $pkg_val;
	private $api_resp;

	// 错误提示码
	private $busy = 0;
	private $error = 0;

	public function __construct() {
		parent::__construct('legwork');

		$this -> ser_legwork = g('ser_legwork');
		$this -> pkg_val = g('pkg_val');
		$this -> api_resp = g('api_resp');

		// 未知原因（系统繁忙）
		$this -> busy = \model\api\server\Response::$Busy;
		// 错误
		$this -> error = \model\api\server\Response::$Error;
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)$this -> pkg_val -> get_value('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				//新建外出计划
				$this -> add_plan();
				BREAK;
			}
			case 102 : {
				//更新移动外勤计划
				$this -> update_plan();
				BREAK;
			}
			case 103 : {
				//删除移动外勤计划
				$this -> delete_plan();
				BREAK;
			}
			case 104 : {
				//使用media_id上传图片
				$this -> upload();
				BREAK;
			}
			case 105 : {
				//添加一次上报信息
				$this -> add_report();
				BREAK;
			}
			case 106 : {
				//获取计划列表
				$this -> get_plan_list();
				BREAK;
			}
			case 107 : {
				//获取单个计划或单个分类实时定位上报信息列表
				$this -> plan_class_record();
				BREAK;
			}
			case 108 : {
				//获取单个计划的详细信息
				$this -> get_plan_info();
				BREAK;
			}
			case 109 : {
				//获取查岗列表
				$this -> get_plan_with_reports();
				BREAK;
			}
			case 110 : {
				//坐标转换
				$this -> trans_gps2gd();
				BREAK;
			}
			case 111 : {
				//获取分类列表
				$this -> get_classify_list();
				BREAK;
			}
			case 112 : {
				//获取外勤记录列表
				$this -> get_record_list();
				BREAK;
			}
			case 113 : {
				//获取外勤记录详情
				$this -> get_record_info();
				BREAK;
			}
			case 114 : {
				//获取定位分类和计划接口
				$this -> get_locat_plan();
				BREAK;
			}
			case 115 : {
				//计划完成
				$this -> complete_plan();
				BREAK;
			}
			case 116 : {
				//GPS经纬度转详细地址
				$this -> trans2address();
				BREAK;
			}
			default : {
				//非法的请求
				parent::echo_busy();
				BREAK;
			}
		}
	}

	/**
	 * 创建计划
	 * @access public
	 * @return void
	 */
	public function add_plan() {
		$need = array(
			'classify_id', 'title', 'recevicer_list', 'is_publish', 
			'start_time', 'end_time',
		);
		$data = $this -> get_post_data($need);

		$classify_id	= intval($data['classify_id']);
		$title			= trim($data['title']);
		$recevicer_list	= $data['recevicer_list'];
		$publish		= intval($data['is_publish']);
		$start_time		= strtotime($data['start_time']);
		$end_time		= strtotime($data['end_time']);

		$mark 			= isset($data['mark']) ? trim($data['mark']) : '';
		$notify_list	= isset($data['notify_list']) ? $data['notify_list'] : array();

		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		try {
			if (empty($classify_id)) throw new \Exception('外勤分类不能为空！');
			if (empty($title)) throw new \Exception('计划主题不能为空！');
			if (!is_array($recevicer_list) || empty($recevicer_list)) throw new \Exception('非法的接收人！');
			if (!is_array($notify_list)) throw new \Exception('非法的知会人！');
			if ($publish != 1 && $publish != 0) throw new \Exception('非法的计划状态！');
			if (empty($start_time)) throw new \Exception('非法的开始时间！');
			if (empty($end_time)) throw new \Exception('非法的结束时间！');
			if ($start_time >= $end_time) throw new \Exception('结束时间不能小于开始时间！');

			$conf = $this -> ser_legwork -> get_classify($classify_id);
			if (empty($conf)) throw new \Exception('非法的外勤分类！');
			if ($conf['need_plan'] != 1) throw new \Exception('该分类不需要计划！');
			
			if ($conf['recevicer_type'] == 2 && $conf['recevicer_edit'] == 0) {
				$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
				!empty($leader) && $recevicer_list = $leader;
			} elseif ($conf['recevicer_type'] == 1 && $conf['recevicer_edit'] == 0) {
				$user_list = json_decode($conf['recevicer_list']);
				!empty($user_list) && $recevicer_list = $user_list;
			}

			if ($conf['notify_type'] == 2 && $conf['notify_edit'] == 0) {
				$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
				!empty($leader) && $notify_list = $leader;
			} elseif ($conf['notify_type'] == 1 && $conf['notify_edit'] == 0) {
				$user_list = json_decode($conf['notify_list']);
				!empty($user_list) && $notify_list = $user_list;
			}

			// 找出知会人但不属于接收人的id集合(用于发送提醒，确保用户同时为接收人和知会人时不会收到两条提醒信息)
			$notify_ids = array();
			$notify_ids = array_diff($notify_list, $recevicer_list);
			$recevicer_ids = $recevicer_list;

			$recevicer_list = json_encode($recevicer_list);
			$notify_list = json_encode($notify_list);

			$publish != 0 && $publish = 1;

			$this -> log_i('开始创建移动外勤计划');
			$ret = $this -> ser_legwork -> add_plan($classify_id, $title, $mark, $recevicer_list, $notify_list, $publish, $start_time, $end_time);

			if ($publish == 1) {
				$this -> remind($ret, 0, 1, $title, 1, $recevicer_ids, array());
				$this -> remind($ret, 0, 2, $title, 1, $notify_ids, array());
			}
			$this -> log_i('创建移动外勤计划成功');
			parent::echo_ok();
		} catch (\Exception $e) {
			$this -> log_e('创建移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 更新计划
	 *
	 * @access public
	 * @return void
	 */
	public function update_plan() {
		$need = array(
			'id', 'classify_id', 'title', 'recevicer_list', 'is_publish', 
			'start_time', 'end_time'
		);
		$data = $this -> get_post_data($need);

		$pn_id 			= intval($data['id']);
		$classify_id 	= intval($data['classify_id']);
		$title 			= trim($data['title']);
		$recevicer_list	= $data['recevicer_list'];
		$publish		= intval($data['is_publish']);
		$start_time		= strtotime($data['start_time']);
		$end_time		= strtotime($data['end_time']);

		$mark = isset($data['mark']) ? trim($data['mark']) : '';
		$notify_list	= isset($data['notify_list']) ? $data['notify_list'] : array();
		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		try {
			if (empty($pn_id)) throw new \Exception('计划不能为空！');
			if (empty($classify_id)) throw new \Exception('非法的分类！');
			if (empty($title)) throw new \Exception('非法的计划标题！');

			$fields = ' user_id,`desc`,is_publish';
			$cond = array(
				'pn_id=' => $pn_id,
			);
			$plan = $this -> ser_legwork -> check_plan($fields, $cond);
			if (empty($plan)) throw new \Exception('非法的计划！');
			if ($plan['user_id'] != $_SESSION[SESSION_VISIT_USER_ID]) {
				throw new \Exception('没有权限修改计划！');
			}
			if (!is_array($recevicer_list) || empty($recevicer_list)) throw new \Exception('非法的接收人！');
			if (!is_array($notify_list)) throw new \Exception('非法的知会人！');
			if ($publish != 1 && $publish != 0) throw new \Exception('非法的计划状态！');
			if (empty($start_time)) throw new \Exception('非法的开始时间！');
			if (empty($end_time)) throw new \Exception('非法的结束时间！');
			if ($start_time >= $end_time) throw new \Exception('结束时间不能小于开始时间！');

			$conf = $this -> ser_legwork -> get_classify($classify_id);
			if (empty($conf)) throw new \Exception('非法的外勤分类！');
			if ($conf['need_plan'] != 1) throw new \Exception('该分类不需要计划！');
			
			if ($conf['recevicer_type'] == 2 && $conf['recevicer_edit'] == 0) {
				$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
				!empty($leader) && $recevicer_list = $leader;
			} elseif ($conf['recevicer_type'] == 1 && $conf['recevicer_edit'] == 0) {
				$user_list = json_decode($conf['recevicer_list']);
				!empty($user_list) && $recevicer_list = $user_list;
			}

			if ($conf['notify_type'] == 2 && $conf['notify_edit'] == 0) {
				$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
				!empty($leader) && $notify_list = $leader;
			} elseif ($conf['notify_type'] == 1 && $conf['notify_edit'] == 0) {
				$user_list = json_decode($conf['notify_list']);
				!empty($user_list) && $notify_list = $user_list;
			}

			// 找出知会人但不属于接收人的id集合(用于发送提醒，确保用户同时为接收人和知会人时不会收到两条提醒信息)
			$notify_ids = array();
			$notify_ids = array_diff($notify_list, $recevicer_list);
			$recevicer_ids = $recevicer_list;

			$recevicer_list = json_encode($recevicer_list);
			$notify_list = json_encode($notify_list);

			// 修改前的计划状态
			$old_publish = $plan['is_publish'];
			$publish != 0 && $publish = 1;

			$this -> log_i('开始更新外勤计划！');
			$this -> ser_legwork -> update_plan($pn_id, $classify_id, $title, $mark, $recevicer_list, $notify_list, $publish, $start_time, $end_time);

			if ($old_publish == 0 && $publish == 1) {
				$this -> remind($pn_id, 0, 1, $title, 1, $recevicer_ids, array());
				$this -> remind($pn_id, 0, 2, $title, 1, $notify_ids, array());
			}
			$this -> log_i('更新外勤计划成功！');
			parent::echo_ok();
		} catch (\Exception $e) {
			$this -> log_e('创建移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 删除计划
	 *
	 * @access public
	 * @return void
	 */
	public function delete_plan() {
		$need = array('id');
		$data = $this -> get_post_data($need);

		$id = (int)$data['id'];
		try {
			if (empty($id) || $id <= 0) throw new \Exception('非法的参数！');
			
			$this -> log_i('开始删除移动外勤计划');
			$this -> ser_legwork -> delete_plan($id);
			$this -> log_i('删除移动外勤计划成功');
			parent::echo_ok();
		} catch (\Exception $e) {
			$this -> log_e('删除移动外勤计划失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 上报实时状态(定位)
	 * type 1：计划定位，2：实时定位
	 * @access public
	 * @return void
	 */
	public function add_report() {
		$need = array(
			'type', 'id', 'longt', 'lat', 'address'
		);
		$data = $this -> get_post_data($need);

		$user_id = $_SESSION[SESSION_VISIT_USER_ID];
		try {
			$type 	= (int)$data['type'];
			$id 	= (int)$data['id'];
			$longt 	= preg_replace('/[^\d\.]+/', '', $data['longt']);
			$lat 	= preg_replace('/[^\d\.]+/', '', $data['lat']);
			$address = trim($data['address']);

			$recevicer_list = isset($data['recevicer_list']) ? $data['recevicer_list'] : array();
			$notify_list 	= isset($data['notify_list']) ? $data['notify_list'] : array();
			$mark 			= isset($data['mark']) ? trim($data['mark']) : '';
			$image_list 	= isset($data['image_list']) ? $data['image_list'] : array();

			if ($type != 1 && $type != 2) throw new \Exception('type为非法的参数！');
			if (empty($address)) throw new \Exception('非法的地址！');
			if (empty($longt) || empty($lat)) throw new \Exception('非法的坐标信息！');

			$location = array(
				'longt' 	=> $longt,
				'lat'		=> $lat,
				'address' 	=> $address,
			);

			$title = '';
			if ($type == 1) {
				$pn_id = $id;
				$pfields = ' user_id, title, classify_id, recevicer_list, notify_list, is_publish,cple_state ';
				$cond = array(
					'pn_id=' => $pn_id,
					'is_del=' => 0,
					);
				$info = $this -> ser_legwork -> check_plan($pfields, $cond);
				if (empty($info)) throw new \Exception('非法的外勤计划！');
				if ($info['is_publish'] == 0) throw new \Exception('草稿计划无法定位！');
				if ($info['cple_state'] == 1)throw new \Exception('计划已经结束！');
				if ($info['user_id'] != $user_id) throw new \Exception('没有权限定位！');

				$classify_id 	= isset($info['classify_id']) ? (int)$info['classify_id'] : 0;
				$recevicer_list = isset($info['recevicer_list']) ? json_decode($info['recevicer_list'], TRUE) : array();
				$notify_list 	= isset($info['notify_list']) ? json_decode($info['notify_list'], TRUE) : array();
				$title = isset($info['title']) ? $info['title'] : '';
			} else {
				$pn_id = 0;
				$classify_id = $id;
			}

			$conf = $this -> ser_legwork -> get_classify($classify_id);
			if (empty($conf)) throw new \Exception('非法的外勤分类！');
			if ($conf['sign_pic_type'] != 0) {
				if (empty($data['image_list'])) {
					throw new \Exception('缺少定位图片！');
				}
			}
			if (!is_array($image_list)) throw new \Exception('非法的图片参数！');

			if ($type == 2) {
				$title = $conf['name'];
				if ($conf['recevicer_type'] == 2 && $conf['recevicer_edit'] == 0) {
					$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
					!empty($leader) && $recevicer_list = $leader;
				} elseif ($conf['recevicer_type'] == 1 && $conf['recevicer_edit'] == 0) {
					$user_list = json_decode($conf['recevicer_list'], TRUE);
					!empty($user_list) && $recevicer_list = $user_list;
				}

				if ($conf['notify_type'] == 2 && $conf['notify_edit'] == 0) {
					$leader = g('dao_leader') -> list_leader_by_user_id($user_id);
					!empty($leader) && $notify_list = $leader;
				} elseif ($conf['notify_type'] == 1 && $conf['notify_edit'] == 0) {
					$user_list = json_decode($conf['notify_list'], TRUE);
					!empty($user_list) && $notify_list = $user_list;
				}
			}
			
			if (empty($recevicer_list)) throw new \Exception('非法的接收人！');

			// 找出知会人但不属于接收人的id集合(用于发送提醒，确保用户同时为接收人和知会人时不会收到两条提醒信息)
			$notify_ids = array();
			$notify_ids = array_diff($notify_list, $recevicer_list);
			$recevicer_ids = $recevicer_list;

			$recevicer_list = json_encode($recevicer_list);
			$notify_list = json_encode($notify_list);

			$this -> log_i('开始定位！');
			$ret = $this -> ser_legwork -> add_report($pn_id, $classify_id, $type, $location, $recevicer_list, $notify_list, $image_list, $mark);
			$data = array(
				'data' => array(
					'id' => $ret,
				),
			);

			// 推送提醒
			$this -> remind($pn_id, $ret, 1, $title, 2, $recevicer_ids, array());
			$this -> remind($pn_id, $ret, 2, $title, 2, $notify_ids, array());

			$this -> log_i('定位成功！');
			parent::echo_ok($data);
		} catch (\Exception $e) {
			$this -> log_e('定位失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取计划列表
	 *
	 * @access public
	 * type 1：我的计划，2：我收到的，3：草稿
	 * @return void
	 */
	public function get_plan_list() {
		$need = array('type');
		$data = $this -> get_post_data($need);

		$type = (int)$data['type'];
		if ($type < 1 || $type > 3) {
			parent::echo_err($this -> busy, '非法的参数！');
		}

		$page 		= isset($data['page']) ? (int)$data['page'] : 1;
		$key_word 	= isset($data['key_word']) ? trim($data['key_word']) : '';
		$asc 		= isset($data['asc']) ? (int)$data['asc'] : 0;
		
		$asc != 1 && $asc = 0;
		$page < 1 && $page = 1;
		$page_size = 10;

		$condition = array();
		!empty($key_word) and $condition['pl.title LIKE '] = '%' . $key_word . '%';

		$list = $this -> ser_legwork -> get_plan_list($condition, $page, $page_size, $type, $asc);
		
		$pageinfo = $this -> get_pageinfo($page, $page_size, $list['count'], count($list['data']));
		$data = array(
			'data' => array(
				'data' 		=> $list['data'],
				'pageinfo' 	=> $pageinfo,
			),
		);

		parent::echo_ok($data);
	}

	/**
	 * 获取单个计划或单个分类实时定位记录列表
	 * id，计划或分类id
	 * type，1：计划，2：分类
	 * @access public
	 * @return void
	 */
	public function  plan_class_record() {
		$need = array('id','type');
		$data = $this -> get_post_data($need);
		$id = (int)$data['id'];
		$type = (int)$data['type'];

		empty($id) && parent::echo_err(\model\api\server\Response::$Busy, '非法的计划或分类！');
		if (empty($type) || $type < 1 || $type > 2) {
			parent::echo_err(\model\api\server\Response::$Busy, '非法的访问类型！');
		}

		try {
			$this -> log_i('开始获取报告列表');
			$list = $this -> ser_legwork -> plan_class_record($id, $type);
			$this -> log_i('获取报告列表成功');
			$data = array(
				'data' => $list,
			);
			parent::echo_ok($data);
		} catch (\Exception $e) {
			$this -> log_e('获取报告列表失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());			
		}
	}

	/**
	 * 获取计划详细信息
	 *
	 * @access public  
	 * @return void
	 */
	public function get_plan_info() {
		try {
			$need = array('id');
			$data = $this -> get_post_data($need);

			$pn_id = intval($data['id']);
			if(empty($pn_id)) throw new \Exception('非法的访问！');

			$this -> log_i('开始获取移动外勤计划详细信息');
			g('pkg_db') -> begin_trans();

			$info = $this -> ser_legwork -> get_plan_info($pn_id);
			$check = $this -> ser_legwork -> legwork_see($pn_id, 1);

			g('pkg_db') -> commit();
			$this -> log_i('获取移动外勤计划详细信息成功');
			$data = array(
				'data' => $info,
			);
			parent::echo_ok($data);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			$this -> log_e('获取移动外勤计划详细信息失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取查岗列表
	 *
	 * @access public
	 * @return void
	 */
	public function get_plan_with_reports() {
		$data = $this -> get_post_data();

		$page = isset($data['page']) ? (int)$data['page'] : 1;
		$self = isset($data['self']) ? (int)$data['self'] : 1;

		$page < 1 && $page = 1;
		$page_size = 10;

		$is_self = $self ? TRUE : FALSE;

		$list = $this -> ser_legwork -> get_plan_with_reports(0, $page, $page_size, $is_self);

		$info = isset($list['data']) ? $list['data'] : array();
		$data = array(
			'data' => $info,
		);
		parent::echo_ok($data);
	}

	/**
	 * 获取分类列表
	 * @return array
	 */
	public function get_classify_list() {
		$data = $this -> get_post_data();

		// $page = isset($data['page']) ? (int)$data['page'] : 1;

		// $page < 1 && $page = 1;
		// $page_size = 10;

		$ret = $this -> ser_legwork -> get_classify_list(0, 0);

		$list = isset($ret['data']) && !empty($ret['data']) ? $ret['data'] : array();
		foreach ($list as $key => &$class) {
			$class['recevicer_list'] = json_decode($class['recevicer_list'], TRUE);
			if(empty($class['recevicer_list'])){
				$class['recevicer_edit'] = 1;
			}
			$class['notify_list'] = json_decode($class['notify_list'], TRUE);
			if(empty($class['notify_list'])){
				$class['notify_edit'] = 1;
			}
		}unset($class);
		
		$data = array(
			'data' => $list,
		);

		parent::echo_ok($data);
	}

	/**
	 * 获取外勤记录列表
	 * type 	1：我的外勤记录，2：我接收到的外勤记录
	 * @return 
	 */
	public function get_record_list() {
		try{

			$need = array('type');
			$data = $this -> get_post_data($need);
			$type = intval($data['type']);
			if ($type != 1 && $type !=2) {
				throw new \Exception('非法的定位类型！');
			}
			$page = isset($data['page']) ? (int)$data['page'] : 1;

			$page < 1 && $page = 1;
			$page_size = 10;

			$list = $this -> ser_legwork -> get_record_list($page, $page_size, $type);

			$pageinfo = $this -> get_pageinfo($page, $page_size, $list['count'], count($list['data']));

			if (!empty($list['data'])) {
				foreach ($list['data'] as &$val) {
					if ($val['classify_name'] == NULL) {
						$val['classify_name'] = '';
					}
				}unset($val);
			}

			$data = array(
				'data' => array(
					'data' => $list['data'] ? $list['data'] : array(),
					'pageinfo' => $pageinfo,
				),
			);

			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取外勤记录详情
	 * 
	 * @return 
	 */
	public function get_record_info() {
		$data = $this -> get_post_data(array('id'));
		$id = intval($data['id']);
		if (empty($id)) parent::echo_err(\model\api\server\Response::$Busy, '非法的参数！');

		try {
			$this -> log_i('开始获取移动外勤记录详情');
			g('pkg_db') -> begin_trans();

			$info = $this -> ser_legwork -> get_record_info($id);
			$check = $this -> ser_legwork -> legwork_see($id, 2);

			g('pkg_db') -> commit();
			$this -> log_i('获取移动外勤记录详情信息成功');
			$data = array(
				'data' => $info,
				);
			parent::echo_ok($data);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			$this -> log_e('获取移动外勤计划详细信息失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取定位分类和计划
	 * @return array
	 */
	public function get_locat_plan() {
		$data = $this -> get_post_data();

		$list = $this -> ser_legwork -> get_locat_plan(0, 0);
		if (!empty($list['plan'])) {
			foreach ($list['plan'] as &$plan) {
				if ($plan['classify_name'] == NULL) {
					$plan['classify_name'] = '';
				}
			}unset($plan);
		}
		$data = array('data' => $list);
		parent::echo_ok($data);
	}

	/**
	 * 计划完成
	 * @return 
	 */
	public function complete_plan() {
		$data = $this -> get_post_data(array('id'));
		$pn_id = intval($data['id']);

		if(empty($pn_id)) parent::echo_err(\model\api\server\Response::$Busy, '非法的参数！');

		try {
			$this -> log_i('开始完成计划');
			$this -> ser_legwork -> complete_plan($pn_id);
		} catch (\Exception $e) {
			$this -> log_e('完成计划失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_busy();
		}
		$this -> log_i('完成计划成功');
		parent::echo_ok();
	}


	/**
	 * 将GPS经纬度转成详细地址
	 * @return 
	 */
	public function trans2address(){
		try{
			$data = $this -> get_post_data(array('lng', 'lat'));
			// $data = array(
			// 		'lng' => '116.481488',
			// 		'lat' => '39.990464',
			// 	);
			$lng = $data['lng'];
			$lat = $data['lat'];

			$position = g('pkg_map') -> trans2gd($lng, $lat);

			$lng = $position['lng'];
			$lat = $position['lat'];

			$address = g('pkg_map') -> trans2address($lng, $lat);

			$data = array(
					'address' => $address,
				);

			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}


	/**
	 * 信息推送
	 * @param  [type] $pn_id   计划id
	 * @param  [type] $re_id   记录id
	 * @param  [type] 	$remind_type   	1:接收人 2：抄送人
	 * @param  [type] $title   提醒内容
	 * @param  [type] $type    1:创建计划提醒，2：定位提醒
	 * @param  array  $users   用户id数组
	 * @param  array  $depts   部门id数组
	 * @return [type]          
	 */
	private function remind($pn_id, $re_id, $remind_type, $title, $type, $users=array(), $depts=array()) {
		try {
			$this -> log_i('开始提醒外勤，'.json_encode($users) . '-' . json_encode($depts));
			$user_name = $_SESSION[SESSION_VISIT_USER_NAME];
			if ($remind_type == 1) {
				$remind = '发送';
			} elseif ($remind_type == 2) {
				$remind = '抄送';
			}

			if ($type == 1) {
				$wx_title = '计划新建提醒';
				$wx_desc = '移动外勤：' . $user_name . '将计划[' . $title . ']' . $remind .'给你，请及时了解计划详情。';
				$wx_url = MAIN_DYNAMIC_DOMAIN.'index.php?app=legwork&a=detail&type=2&listType=plan&id='.$pn_id;

				$pc_title = '移动外勤：' . $user_name . '将计划[' . $title . ']' . $remind .'给你，请及时了解计划详情。';
				$pc_url = PC_DYNAMIC_DOMAIN.'index.php?app=legwork&a=myReceivedList&notice=true&showMap=false&isPlan=true&id='.$pn_id;
			} else {
				$wx_title = '外勤定位提醒';
				$wx_desc =  '移动外勤：' . $user_name . '向你上报了实时定位，请及时了解定位详情。';
				$wx_url = MAIN_DYNAMIC_DOMAIN.'index.php?app=legwork&a=detail&type=2&listType=legwork&id='.$re_id;

				$pc_title = '移动外勤：' . $user_name . '向你上报了实时定位，请及时了解定位详情。';
				if ($pn_id == 0) {
					$pc_url = PC_DYNAMIC_DOMAIN.'index.php?app=legwork&a=myReceivedRecordList&notice=true&showMap=true&isPlan=false&id='.$re_id;
				} else {
					$pc_url = PC_DYNAMIC_DOMAIN.'index.php?app=legwork&a=myReceivedRecordList&notice=true&showMap=true&isPlan=true&id='.$re_id.'&planId='.$pn_id;
				}
			}

			if (empty($users) && empty($depts)) {
				$this -> log_i('接收人为空，无法推送消息');
				return TRUE;
			}

			// wx端提醒
			$this -> send_wx_news($wx_title, $wx_desc, $wx_url, '', $users, $depts);
			// pc端提醒
			$this -> send_pc_news($pc_title, $pc_url, $users, $depts);

			$this -> log_i('发送通知成功！');
		} catch (\Exception $e) {
			$this -> log_e('发送通知失败！');
		}
	}
}
/* End of this file */