<?php

namespace model\legwork\view;

class LegworkIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('legwork',$log_pre_str);
	}
	
	//创建计划
	public function create(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('pkg_smarty') -> show($this -> view_dir . 'page/createLegwork.html');
	}

	//我的计划
	public function myList(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		parent::get_menu();
		g('pkg_smarty') -> show($this -> view_dir . 'page/myPlanList.html');
	}

	//计划详情
	public function detail(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('api_jssdk') -> assign_jssdk_sign();
		g('pkg_smarty') -> show($this -> view_dir . 'page/detail.html');
	}	

	//收到的计划
	public function receivedList(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('pkg_smarty') -> show($this -> view_dir . 'page/receivedList.html');
	}

	//有计划的定位
	public function planLocation(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('api_jssdk') -> assign_jssdk_sign();
		g('pkg_smarty') -> show($this -> view_dir . 'page/planLocation.html');
	}

	//我的外勤记录
	public function myLegwork(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		parent::get_menu();
		g('pkg_smarty') -> show($this -> view_dir . 'page/myLegwork.html');
	}

	//外勤详情 定位
	public function detail_location(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('pkg_smarty') -> show($this -> view_dir . 'page/detail_location.html');
	}

	//地图
	public function map(){
		g('pkg_smarty') -> assign('title', '移动外勤');
		g('pkg_smarty') -> show($this -> view_dir . 'page/map.html');
	}
}

//end