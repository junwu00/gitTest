<?php 
/**
 * 移动外勤业务逻辑处理类
 */

namespace model\legwork\server;

class ServerLegwork extends \model\api\server\ServerBase {

	//移动外勤分类表
	public $classify_table 	= 'legwork_classify';
	//移动外勤上传文件表
	public $file_table 		= 'legwork_file';
	//计划查看记录表
	public $see_table 		= 'legwork_see';
	//移动外勤配置表
	public $conf_table 	 	= 'legwork_conf';
	//移动外勤计划表
	public $plan_table 	 	= 'legwork_plan';
	//移动外勤上报记录表
	public $report_table 	= 'legwork_record';
	//用户信息表
	public $user_table 	 	= 'sc_user';
	//部门信息表
	public $dept_table 	 	= 'sc_dept';

	//外勤dao层
	private $dao_legwork;
	//response类
	private $api_resp;
	//user类
	private $dao_user;

	public function __construct($log_pre_str='') {
		parent::__construct('legwork', $log_pre_str);

		$this -> dao_legwork = g('dao_legwork');
		$this -> api_resp = g('api_resp');
		$this -> dao_user = g('dao_user');

		$this -> com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$this -> user_id = $_SESSION[SESSION_VISIT_USER_ID];
	}

	/**
	 * 创建计划
	 * @param [type] $classify_id    
	 * @param [type] $title          
	 * @param [type] $mark           
	 * @param [type] $recevicer_list 
	 * @param [type] $notify_list    
	 * @param [type] $publish        
	 * @param [type] $start_time     
	 * @param [type] $end_time       
	 */
	public function add_plan($classify_id, $title, $mark, $recevicer_list, $notify_list, $publish, $start_time, $end_time) {
		$time = time();
		$data = array(
			'classify_id'	=> $classify_id,
			'title'			=> $title,
			// 前端传过来的是mark，数据库存储的字段名为desc
			'`desc`'			=> $mark,
			'recevicer_list' => $recevicer_list,
			'notify_list'	=> $notify_list,
			'is_publish'	=> $publish,
			'begin_time'	=> $start_time,
			'end_time'		=> $end_time,
			'com_id'		=> $this -> com_id,
			'user_id'		=> $this -> user_id,
			'create_time'	=> $time,
			'update_time'	=> $time,
		);
		$ret = $this -> dao_legwork -> add_plan($data);
		if (!$ret) throw new \Exception("创建移动外勤计划失败");
		
		return $ret;
	}

	/**
	 * 更新计划
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * @param array $data 更新数据集合
	 * @return boolean
	 */
	public function update_plan($pn_id, $classify_id, $title, $mark, $recevicer_list, $notify_list, $publish, $start_time, $end_time) {
		$data = array(
			'classify_id'	=> $classify_id,
			'title'			=> $title,
			// 前端传过来的是mark，数据库存储的字段名为desc
			'`desc`'			=> $mark,
			'recevicer_list' => $recevicer_list,
			'notify_list'	=> $notify_list,
			'is_publish'	=> $publish,
			'begin_time'	=> $start_time,
			'end_time'		=> $end_time,
			'update_time'	=> time(),
		);

		$cond = array(
			'com_id=' 	=> $this -> com_id,
			'user_id=' 	=> $this -> user_id,
			'pn_id=' 	=> $pn_id,
			//只有状态为0（未定位）的记录才能更新
			'state=' 	=> 0,
		);
		
		$ret = $this -> dao_legwork -> update_plan($data, $cond);
		if (!$ret) throw new \Exception("更新计划失败");
		
		return $ret;
	}

	/**
	 * 删除计划
	 *
	 * @access public
	 * @param integer $pn_id 计划id
	 * state：只有定位状态为0的计划才允许删除
	 * @return boolean
	 */
	public function delete_plan($id) {
		$cond = array(
			'user_id=' 	=> $this -> user_id,
			'com_id=' 	=> $this -> com_id,
			'pn_id=' 	=> $id,
			'state=' 	=> 0,
		);
	
		$data = array(
			'update_time' => time(),
			'is_del' 	=> 1,
		);

		$ret = $this -> dao_legwork -> delete_plan($data, $cond);
		if (!$ret) throw new \Exception("删除失败！");

		return $ret;
	}

	/**
	 * 上报实时状态
	 * @param [type] $pn_id          
	 * @param [type] $classify_id    
	 * @param [type] $type           
	 * @param array  $location       
	 * @param array  $recevicer_list 
	 * @param array  $notify_list    
	 * @param array  $image_list     
	 * @param [type] $mark           
	 */
	public function add_report($pn_id, $classify_id, $type, $location=array(), $recevicer_list=array(), $notify_list=array(), $image_list=array(), $mark) {
		$time = time();
		$pic_urls = array();
		if (!empty($image_list)) {
			foreach ($image_list as $key => $image) {
				$pic_urls[] = $image['url'];
			}unset($image);
		}

		$rp_data = array(
			'com_id' 			=> $this -> com_id,
			'user_id' 			=> $this -> user_id,
			'pn_id'				=> $pn_id,
			'classify_id'		=> $classify_id,
			'type'				=> $type,
			'longt' 			=> $location['longt'],
			'lat' 				=> $location['lat'],
			'address' 			=> $location['address'],
			'mark' 				=> $mark,
			'recevicer_list'	=> $recevicer_list,
			'notify_list'		=> $notify_list,
			'image_list'		=> json_encode($pic_urls),
			'create_time' 		=> $time,
		);

		try {
			g('pkg_db') -> begin_trans();

			$ret = $this -> dao_legwork -> add_report($rp_data);
			if (!$ret) throw new \Exception('定位失败！');

			// 更新plan表的state状态
			if ($type == 1) {
				$pl_cond = array(
					'com_id=' 	=> $this -> com_id,
					'user_id=' 	=> $this -> user_id,
					'pn_id=' 	=> $pn_id,
					'state=' 	=> 0,
				);
				$check_state = g('pkg_db') -> check_exists($this -> plan_table, $pl_cond);
				if ($check_state) {
					// 未定位时才需要更新
					$pl_data = array('state' => 1);
					$up_state = $this -> dao_legwork -> update_plan($pl_data, $pl_cond);
					if (!$up_state) throw new \Exception('更新定位状态失败！');
				}
			}

			// 保存文件
			if (!empty($image_list)) {
				foreach ($image_list as $key => $image) {
					$file_name 	= isset($image['name']) ? $image['name'] : '';
					$file_hash 	= isset($image['hash']) ? $image['hash'] : '';
					$file_size 	= isset($image['size']) ? $image['size'] : 0;
					$file_url 	= isset($image['url']) ? $image['url'] : '';
					$file_ext 	= isset($image['ext']) ? $image['ext'] : '';

					$file = array(
						'user_id' 		=> $this -> user_id,
						'file_name'		=> $file_name,
						'file_hash'		=> $file_hash,
						'file_size' 	=> $file_size,
						'file_url'		=> $file_url,
						'file_ext'		=> $file_ext,
						'create_time' 	=> $time,
						'update_time' 	=> $time,
						);
					$file['record_id'] = $ret;

					$file_id = $this -> dao_legwork -> save_file($file);
					if (empty($file_id)) throw new \Exception('保存文件失败！');
				}unset($image);
			}
			
			g('pkg_db') -> commit();
			return $ret;
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			throw new \Exception($e -> getMessage());
		}
	}

	/**
	 * 获取计划列表
	 *
	 * @access public
	 * @param integer $cond 条件
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param integer $type 1：我的计划，2：我收到的，3：草稿
	 * @param integer $asc 是否升序 0：按时间倒序排，1:按时间顺序排
	 * @return array
	 */
	public function get_plan_list($cond=array(), $page, $page_size, $type, $asc=0) {
		$user_id = $this -> user_id;
		$table = $this -> plan_table . ' pl, ' . $this -> classify_table . ' cl ';
		$fields = array(
			'pl.pn_id AS id', 'pl.title', 'pl.create_time', 'pl.cple_state', 'cl.name AS classify_name', 
			'u.id AS creater_id', 'u.name AS creater_name', 'u.pic_url AS creater_pic_url'
		);

		$cond['pl.com_id='] 		= $this -> com_id;
		$cond['pl.is_del='] 		= 0;
		$cond['^pl.user_id=']		= 'u.id';
		$cond['^pl.classify_id='] 	= 'cl.id';
		
		if ($type == 1) {
			$cond['pl.user_id='] = $user_id;
			$cond['pl.is_publish='] = 1;
			$other = array('pl.state');
			$fields = array_merge($fields, $other);
		} elseif ($type == 2) {
			$table = $this -> plan_table . ' pl LEFT JOIN ' . $this -> see_table . " se ON pl.pn_id=se.pr_id AND se.read_type=1 AND se.user_id={$user_id}, " . $this -> classify_table . ' cl ';
			$other = array('pl.recevicer_list', 'pl.notify_list', 'se.id AS read_id');
			$fields = array_merge($fields, $other);

			$cond['pl.is_publish='] 	= 1;
			$cond['__OR_1'] = array(
				'pl.recevicer_list LIKE '	=> '%"' . $user_id . '"%',
				'pl.notify_list LIKE'		=> '%"' . $user_id . '"%',
			);
		} elseif ($type == 3) {
			$cond['pl.user_id=']		= $user_id;
			$cond['pl.is_publish='] 	= 0;
			$other = array('pl.state');
			$fields = array_merge($fields, $other);
		}

		$table .= ',' . $this -> user_table . ' AS u ';
		$fields = implode(',', $fields);

		if ($asc == 1) {
			$order = ' ORDER BY pl.cple_state ASC, pl.pn_id ASC';
		} else {
			$order = ' ORDER BY pl.cple_state ASC, pl.pn_id DESC';
		}

		$ret = $this -> dao_legwork -> get_plan_list($table, $fields, $cond, $page, $page_size, $order);

		if (!empty($ret['data']) && $type ==2) {
			foreach ($ret['data'] as $key => &$plan) {
				if ($plan['classify_name'] === NULL) {
					$plan['classify_name'] = '';
				}
				$plan['recevicer_type'] = 0;
				$plan['recevicer_state'] = 1;
				$recevicer_list =  !empty($plan['recevicer_list']) ? json_decode($plan['recevicer_list'], TRUE) : array();
				$notify_list = !empty($plan['notify_list']) ? json_decode($plan['notify_list'], TRUE) : array();
				if (in_array($user_id, $notify_list)) {
					$plan['recevicer_type'] = 2;
				}
				if (in_array($user_id, $recevicer_list)) {
					$plan['recevicer_type'] = 1;
				}
				if (!empty($plan['read_id'])) {
					$plan['recevicer_state'] = 2;
				}
				unset($plan['recevicer_list']);
				unset($plan['notify_list']);
				unset($plan['read_id']);
			}unset($plan);
		}

		return $ret;
	}

	/**
	 * 获取单个计划或单个分类实时定位记录列表
	 * @param  integer $id   计划或分类id
	 * @param  integer $type 1：计划，2：分类
	 * @return array       
	 */
	public function  plan_class_record($id, $type) {
		return $this -> dao_legwork -> plan_class_record($id, $type);
	}

	/**
	 * 获取计划详细信息
	 *
	 * @access public  
	 * @param integer $pn_id 计划id
	 * @return mixed
	 */
	public function get_plan_info($pn_id) {
		$info = $this -> dao_legwork -> get_plan_info($pn_id);
		if(!$info) throw new \Exception('该计划不存在！');

		// 把id_list的用户id转换为所需对应的用户信息
		$creater = isset($info['creater']) ? (int)$info['creater'] : 0;
		$recevicer_list = isset($info['recevicer_list']) ? json_decode($info['recevicer_list'], TRUE) : array();
		$notify_list = isset($info['notify_list']) ? json_decode($info['notify_list'], TRUE) : array();

		$user_id = $this -> user_id;
		if ($user_id != $creater && !in_array($user_id, $recevicer_list) && !in_array($user_id, $notify_list)) {
			throw new \Exception('没有权限查看！');
		}

		$id_list = array(
			'creater'			=> array($creater),
			'recevicer_list'	=> $recevicer_list,
			'notify_list'		=> $notify_list,
			);
		$user_info = $this -> dao_legwork -> format_user_info($id_list, array('id', 'name', 'pic_url'));

		if (!empty($user_info)) {
			foreach ($user_info as $key => $value) {
				if ($key == 'creater') {
					if (count($value) == 1) {
						$info['creater'] = $value[0];
					}
				}
				if ($key == 'recevicer_list') {
					$info['recevicer_list'] = $value;
				}
				if ($key == 'notify_list') {
					$info['notify_list'] = $value;
				}
			}unset($value);
		}
		if (isset($info['creater'])) {
			$acct = g('dao_user') -> get_by_id($info['creater']['id'], 'acct');
			$info['creater']['acct'] = isset($acct['acct']) ? $acct['acct'] : '';
		}
		
		return $info;
	}

	/**
	 * 检查计划是否存在
	 * @param  string $fields 
	 * @param  array  $cond   
	 * @return [type]         
	 */
	public function check_plan($fields='*', $cond=array()) {
		return $this -> dao_legwork -> check_plan($fields, $cond);
	}

	/**
	 * 获取查岗列表
	 *
	 * @access public
	 * @param int $user_id 用户id
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @param boolean $is_self 是否获取自身的计划列表
	 * @return array
	 */
	public function get_plan_with_reports($user_id = 0, $page, $page_size, $is_self = TURE) {
		return $this -> dao_legwork -> get_plan_with_reports($user_id, $page, $page_size, $is_self);
	}

	/**
	 * 获取计划列表
	 *
	 * @access public
	 * @param integer $page 页码
	 * @param integer $page_size 每页最大条数
	 * @return array
	 */
	public function get_classify_list($page, $page_size) {
		return $this -> dao_legwork -> get_classify_list($page, $page_size);
	}

	/**
	 * 获取外勤记录列表
	 * @param  integer $page      
	 * @param  integer $page_size 
	 * @param  integer $type 	1：我的外勤记录，2：我接收到的外勤记录      
	 * @return array            
	 */
	public function get_record_list($page, $page_size, $type) {
		return $this -> dao_legwork -> get_record_list($page, $page_size, $type);
	}

	/**
	 * 获取外勤记录详情
	 * @param  integer $id 记录id
	 * @return mixed     
	 */
	public function get_record_info($id) {
		$info =  $this -> dao_legwork -> get_record_info($id);
		if(!$info) throw new \Exception('该记录不存在！');

		// 把id_list的用户id转换为所需对应的用户信息
		$creater = isset($info['creater']) ? (int)$info['creater'] : 0;
		$recevicer_list = isset($info['recevicer_list']) ? json_decode($info['recevicer_list'], TRUE) : array();
		$notify_list = isset($info['notify_list']) ? json_decode($info['notify_list'], TRUE) : array();

		$user_id = $this -> user_id;
		if ($user_id != $creater && !in_array($user_id, $recevicer_list) && !in_array($user_id, $notify_list)) {
			throw new \Exception('没有权限查看！');
		}

		$id_list = array(
			'creater'			=> array($creater),
			'recevicer_list'	=> $recevicer_list,
			'notify_list'		=> $notify_list,
			);
		$user_info = $this -> dao_legwork -> format_user_info($id_list, array('id', 'name', 'pic_url'));
		try{
			$ret = g('pkg_map') -> trans2gd($info['longt'], $info['lat']);
			$info['lat'] = $ret['lat'];
			$info['longt'] = $ret['lng'];
		}catch(\Exception $e){}

		foreach ($user_info as $key => $value) {
			if ($key == 'creater') {
				if (count($value) == 1) {
					$info['creater'] = $value[0];
				}
			}
			if ($key == 'recevicer_list') {
				$info['recevicer_list'] = $value;
			}
			if ($key == 'notify_list') {
				$info['notify_list'] = $value;
			}
		}
		if (isset($info['creater'])) {
			$acct = g('dao_user') -> get_by_id($info['creater']['id'], 'acct');
			$info['creater']['acct'] = isset($acct['acct']) ? $acct['acct'] : '';
		}

		return $info;
	}

	/**
	 * 获取定位分类和计划
	 * @param  integer $page 
	 * @param  integer $page_size 
	 * @return array
	 */
	public function get_locat_plan($page, $page_size) {
		return $this -> dao_legwork -> get_locat_plan($page, $page_size);
	}

	/**
	 * 计划完成
	 * @param  integer $pn_id 计划id
	 * @return boolean
	 */
	public function complete_plan($pn_id) {
		return $this -> dao_legwork -> complete_plan($pn_id);
	}

	/**
	 * 查看计划或外勤记录
	 * @param  integer $id   计划或记录id
	 * @param  integer $type 1.查看计划 2.查看记录
	 * @return boolean
	 */
	public function legwork_see($id, $type) {
		if ($type == 1) {
			$cond = array(
				'pn_id=' => $id,
			);
			$info = $this -> dao_legwork -> check_plan('user_id', $cond);
		} else {
			$info = $this -> dao_legwork -> get_record_info($id, 're.user_id');
		}
		$user_id = !empty($info['user_id']) ? intval($info['user_id']) : 0;
		return $this -> dao_legwork -> legwork_see($id, $user_id, $type);
	}

	/**
	 * 获取分类详情
	 * @param  [type] $id     分类id
	 * @param  string $fields 
	 * @return [type]         
	 */
	public function get_classify($classify_id, $fields='') {
		if (empty($classify_id)) throw new \Exception('非法的外勤分类ID');
    	empty($fields) && $fields = '*';
    	
    	$cond = array(
    		'com_id=' 	=> $this -> com_id,
    		'id='		=> $classify_id,
    		'info_state=' => 1,
    	);

    	return $this -> dao_legwork -> get_classify($fields, $cond);
	}
}
/* End of this file */