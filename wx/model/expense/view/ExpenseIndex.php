<?php

namespace model\expense\view;

class ExpenseIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('expense',$log_pre_str);
	}
	
	public function index(){
		$time = g('api_jssdk') -> assign_jssdk_sign();
		g('api_jssdk') -> assign_invoice_sign($time);

		/*g('pkg_smarty') -> assign('time', $time);
		g('pkg_smarty') -> assign('nonce_str', $nonce_str);
		g('pkg_smarty') -> assign('sign', $sign);*/
		

		$id = (int)g("pkg_val") -> get_get("id");

		$report_info = g('ser_expense_count') -> get_report_by_id($this -> com_id, $id, "name");
		parent::get_user_info();
		g('pkg_smarty') -> assign('title', $report_info['name']);
		g('pkg_smarty') -> show($this -> view_dir . 'page/index.html');
	}
}

//end