<?php

namespace model\expense\view;

class ExpenseAjax extends \model\api\view\AbsProc {
	
	private $server = '';

	public function __construct($log_pre_str='') {
		parent::__construct('expense',$log_pre_str);

		$this->server = g('ser_expense');
	}
	
	public function index() {
		$cmd = g('pkg_val') -> get_get('cmd', FALSE);
		switch ($cmd) {
			case 101 : {
				//获取发票信息
				$this->get_invoice();
				break; 
			}
			case 102 : {
				//保存电子发票pdf附件
				$this->save_invoice_pdf();
				break; 
			}
			case 103 : {
				//保存流程信息
				$this->save_workitem();
				break; 
			}
			case 104 : {
				//发送流程
				
				break; 
			}
			case 105 : {
				//获取我的报销记录
				$this->get_expense_record();
				break; 
			}
			
			default : {
				// 非法访问
				parent::echo_busy();
				break;
			}

		}
	}

//-----------------------------内部实现-------------------------------------

	/** 获取发票信息 */
	private function get_invoice() {
		try {
			$data = parent::get_post_data(array('card_id', 'encrypt_code'));
			if (empty($data['card_id'])) throw new \Exception('获取发票id失败');
			if (empty($data['encrypt_code'])) throw new \Exception('获取发票加密code失败');
			
			$invoice = $this->server->get_invoice($this->com_id, $data['card_id'], $data['encrypt_code']);
			if (!isset($invoice['errcode'])) throw new \Exception('获取发票信息失败');
			if ($invoice['errcode'] != 0) {
				$err = array(
					'errmsg' => $invoice['errmsg'],
					);

				parent::echo_resp($invoice['errcode'], $err);
			}

			$pdf = $invoice['user_info']['pdf_url'];
			$ret = $this->server->upload_invoice_pdf($this->com_id, $pdf);
			$name = explode('/', $ret['path']);

			$info = array(
				'file_ext' => $ret['ext'],
				'file_hash' => $ret['hash'],
				'file_name' => $name[count($name)-1],
				'file_path' => $ret['path'],
				);

			$invoice['pdf_file'] = $info;

			$redis_key = $this->_get_redis_key($data['card_id']);
			g('pkg_redis')->set($redis_key, $pdf, 180);
			
			parent::echo_ok($invoice);
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

	/** 保存电子发票pdf附件 */
	private function save_invoice_pdf() {
		try {
			$data = parent::get_post_data(array('card_id'));
			if (empty($data['card_id'])) throw new \Exception('参数错误');
			
			$redis_key = $this->_get_redis_key($data['card_id']);
			$url = g('pkg_redis')->get($redis_key);
			if (empty($url)) throw new \Exception('获取发票附件失败，请刷新页面重试');

			$ret = $this->server->upload_invoice_pdf($this->com_id, $url);
			$name = explode('/', $ret['path']);

			$info = array(
				'file_ext' => $ret['ext'],
				'file_hash' => $ret['hash'],
				'file_name' => $name[count($name)-1],
				'file_path' => $ret['path'],
				);
			parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

	/** 保存流程信息 */
	private function save_workitem() {
		try {
			$needs = array('form_id', 'form_name', 'work_id', 'form_vals');
			$data = parent::get_post_data($needs);
			!isset($data['judgement']) && $data['judgement'] = '';
			!isset($data['workitem_id']) && $data['workitem_id'] = 0;
			!isset($data['formsetInst_id']) && $data['formsetInst_id'] = 0;
			!isset($data['files']) && $data['files'] = array();
			$is_new = empty($data['formsetInst_id']) ? 1 : 0;

			parent::log_i('开始保存信息');
			g('pkg_db') -> begin_trans();

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data["workitem_id"]);
			}

			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更，请返回待办列表！');
			}

			// 获取表单模板对应的应用版本
			$form = g('mv_form') -> get_by_id($data['form_id'], $this -> com_id, 'app_version, is_other_proc');
			$data['app_version'] = $form ? $form['app_version'] : 0;
			$data['is_other_proc'] = $form['is_other_proc'];

			$ret = parent::save_workitem_parent($data);

			$this->server->save_expense_record($this->com_id, $this->user_id, $ret['formsetInst_id'], $data['form_vals'], $is_new);
			
			g('pkg_db') -> commit();

			$info = array(
				'info' => array(
					'workitem_id' => $ret['workitem_id'],
					'formsetInst_id' => $ret['formsetInst_id'],
					),
				);

			parent::log_i('保存流程信息成功');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('保存流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_exp($e);
		}
	}


	/** 获取我的报销记录 */
	private function get_expense_record() {
		try {
			$data = parent::get_post_data();

			$page = isset($data['page']) ? intval($data['page']) : 1;
			$page_size = self::$PageSize;

			$info = $this->server->get_record_list($this->com_id, $this->user_id, array(), '', $page, $page_size);
			
			parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}


	/** 获取缓存KEY */
	private function _get_redis_key($str){
		return md5("ExpenseAjax::".$str);
	}
	
}

//end