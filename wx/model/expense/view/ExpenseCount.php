<?php
/**
 * 快速报销 报表模块
 *
 * @author chenyh
 * @create 2017-03-09
 */

namespace model\expense\view;

class ExpenseCount extends \model\api\view\ViewBase {
	
	private $server = '';

	public function __construct($log_pre_str='') {
		parent::__construct('expense',$log_pre_str);

		$this-> Server = g('ser_expense_count');
	}
	
	public function index() {
		$cmd = g('pkg_val') -> get_get('cmd', FALSE);
		switch ($cmd) {
			case 101 : {
				//获取分类汇总数据
				$this->get_chart_pie();
				break; 
			}
			case 102 : {
				//获取时间汇总数据
				$this->get_chart_line();
				break; 
			}
			case 103 : {
				//获取部门汇总数据
				$this->get_table_count();
				break; 
			}
			default : {
				// 非法访问
				parent::echo_busy();
				break;
			}

		}
	}

//-----------------------------内部实现-------------------------------------

	/**
	 * 获取饼图数据
	 * @return 
	 */
	private function get_chart_pie(){
		try{
			$fields = array("report_id");
			$data = parent::get_post_data($feilds);

			$month = (isset($data['month']) && !empty($data['month']))? $data['month'] : date("Y-m");
			$report_id = intval($data["report_id"]);

			$report_info = $this -> Server -> check_report($this -> com_id, $this -> user_id, $report_id);
			if(empty($report_info)){
				throw new \Exception("报表不存在");
			}

			$admin_id = $report_info['create_id'];

			if($this -> Server -> check_admin_privilege($admin_id)){
				$admin_id = 0;
			}
			$form_id_list = $this -> Server -> get_my_form($admin_id);

			$start_time = strtotime("{$month}-01");
			$end_time = strtotime("+1 month", $start_time) - 1;

			if(!empty($form_id_list)){
				$ret = $this -> Server -> get_count_by_classify($this -> com_id, $form_id_list, $start_time, $end_time);
			}else{
				$ret = array();
			}

			$all_classidy = $this -> Server -> get_all_classify($this -> com_id);

			$sum = 0;
			$data = array();
			foreach ($all_classidy as $c) {
				$c_name = $c['name'];
				$c_val = isset($ret[$c['id']]) ? $ret[$c['id']] : 0;
				$sum += $c_val;
				$data[] = array($c_name, round($c_val, 2));
			}
			$pie_data = array(
					"month" => $start_time, 
					"sum" => $sum,
					"data" => $data,
				);
			parent::echo_ok($pie_data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/**
	 * 获取折线图数据
	 * @return 
	 */
	private function get_chart_line(){
		try{
			$fields = array("report_id");
			$data = parent::get_post_data($feilds);

			$year = (isset($data['year']) && !empty($data['year'])) ? $data['year'] : date("Y");
			$report_id = intval($data["report_id"]);
			$classify_id = !empty($data["classify_id"]) ? intval($data["classify_id"]) : 0;

			$report_info = $this -> Server -> check_report($this -> com_id, $this -> user_id, $report_id);
			if(empty($report_info)){
				throw new \Exception("报表不存在");
			}

			$admin_id = $report_info['create_id'];
			if($this -> Server -> check_admin_privilege($admin_id)){
				$admin_id = 0;
			}

			$all_classidy = $this -> Server -> get_all_classify($this -> com_id);
			$curr_classify = array();
			if(!empty($classify_id)){
				foreach ($all_classidy as $class) {
					if($classify_id == $class['id']){
						$curr_classify[] = $classify_id;
						break;
					}
				}unset($class);
				if(empty($curr_classify)){
					throw new \Exception("分类不存在");
				}
			}else{
				foreach($all_classidy as $class){
					$curr_classify[] = $class['id'];
				}unset($class);
			}

			$form_id_list = $this -> Server -> get_my_form($admin_id);
			
			$start_time = strtotime("{$year}-01-01");
			$end_time = strtotime("+1 year", $start_time) - 1;
			if(!empty($form_id_list)){
				$ret = $this -> Server -> get_count_by_time($this -> com_id, $start_time, $end_time, $form_id_list, $curr_classify, array());
			}else{
				$ret = array();
			}

			$data = array();
			for ($i=1; $i <=12 ; $i++) { 
				$val = isset($ret[$i]) ? $ret[$i] : 0;
				$data[] = array($i, $val);
			}

			$years = array();
			for ($y=2017; $y <= date("Y"); $y++) { 
				$years[] = $y;
			}

			$line_data = array(
					"years" => $years,
					"data" => $data
				);
			parent::echo_ok($line_data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/**
	 * 获取部门汇总数据
	 * @return 
	 */
	private function get_table_count(){
		try{
			$fields = array("report_id");
			$data = parent::get_post_data($feilds);

			$report_id = intval($data["report_id"]);
			$dept_id = isset($data['dept_id']) ? intval($data['dept_id']) : $_SESSION[SESSION_VISIT_DEPT_ID];

			$report_info = $this -> Server -> check_report($this -> com_id, $this -> user_id, $report_id);
			if(empty($report_info)){
				throw new \Exception("报表不存在");
			}

			$admin_id = $report_info['create_id'];
			if($this -> Server -> check_admin_privilege($admin_id)){
				$admin_id = 0;
			}
			$form_id_list = $this -> Server -> get_my_form($admin_id);

			$time_type = isset($data['range_time']) ? intval($data['range_time']) : 1; 

			//计算时间范围
			switch($time_type){
				case 0 : {
					//全部
					$start_time = 0;
					$end_time = time();
					break;
				}
				case 1 : {
					//本月
					$start_time = strtotime(date("Y-m")."-01");
					$end_time = strtotime("+1 month", $start_time) - 1; 
					break;
				}
				case 2 : {
					//上月
					$start_time = strtotime("-1 month", strtotime(date("Y-m")."-01"));
					$end_time = strtotime("+1 month", $start_time) - 1; 
					break;
				}
				case 3 : {
					//近3个月
					$start_time = strtotime("-3 month", time());
					$end_time = time();
					break;
				}
				default : {
					// 非法访问
					parent::echo_busy();
					break;
				}
			}

			$start = isset($data['start']) ? intval($data['start']) : 0;
			$page_size = isset($data['page_size']) ? intval($data['page_size']) : 20;


			if(!empty($form_id_list)){
				$ret = $this -> Server -> get_table_count($this -> com_id, $form_id_list, $dept_id, $start_time, $end_time, $start, $page_size);
			}else{
				$ret = array(
						"data" => array(),
						"count" => array(),
					);
			}

			$all_classidy = $this -> Server -> get_all_classify($this -> com_id);
			$table_data = $this -> data2table($all_classidy, $ret['data'], $ret['count']);
			$ret = array(
					"info" => array(
							"table" => $table_data,
							"dept_id" => $dept_id
						),
				);
			parent::echo_ok($ret);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}


//------------------------------------------------------内部实现---------------------------------------

	/**
	 * 将data转换成table
	 * @param   $data 
	 * @return        
	 */
	private function data2table($title, $data, $count){
		$thead = array();
		$tbody = array();
		$tfoot = array();
		$page_length = count($data);

		$tmp_title = array();
		$tmp_title[] = array(
				"key" => "title_key",
				"val" => "部门/人员",
				"type" => "text"
			);
		foreach ($title as $t) {
			$tmp_title[] = array(
					"key" => "classify_".$t['id'],
					"val" => $t['name'],
					"type" => "number"
				);
		}unset($t);
		//标题增加总费用列
		$tmp_title[] = array(
				"key" => "count_key",
				"val" => "总费用",
				"type" => "number"
			);
		$thead[] = array(
				"data" => $tmp_title
			);
		//tfoot增加总费用汇总信息
		$all_count = 0;
		$r = 0;
		foreach ($data as $row) {
			$tmp_data = array();
			$r_count = 0;
			foreach ($row as $key => $val) {
				if($key != "dept_id"){
					$r_count += $val;
				}

				$tmp_data[] = array(
						"key" => $key."_".$r,
						"val" => $val,
					);
			}unset($val);
			$all_count+= $r_count;
			$tmp_data[] = array(
					"key" => "count_".$r,
					"val" => $r_count,
				);
			$tbody[] = array(
					"data" => $tmp_data
				);
			$r++;
		}unset($val);

		//总数据行数
		$sum = 0;
		$tmp_count = array();
		foreach ($count as $key => $val) {
			if($key == "dept_id"){
				$sum = intval($val);
				$val = "汇总";
			}
			$tmp_count[] = array(
					"key" => $key."_count",
					"val" => $val
				);
		}unset($val);
		//tfoot增加总费用汇总信息
		$tmp_count[] = array(
					"key" => "all_count",
					"val" => $all_count
				);


		$tfoot[] = array(
				"data" => $tmp_count
			);

		$ret = array(
			"data" => array(
					"thead" => $thead,
					"tbody" => $tbody,
					"tfoot" => $tfoot,
				),
			"sum" => $sum,
			"page_length" => $page_length
			);
		return $ret;
	}

}

//end