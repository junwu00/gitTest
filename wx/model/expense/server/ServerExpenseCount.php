<?php
/**
 * 报销逻辑处理
 * @author chenyh
 * @date 2017-03-06
 */

namespace model\expense\server;

class ServerExpenseCount extends \model\api\server\ServerBase {


	private static $StateOff = 0;
	/** 启用 */
	private static $StateOn = 1;
	/** 数据状态正常 */
	private static $InfoOn = 1;
	/** 数据状态删除 */
	private static $InfoDelete = 0;

	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	private static $StateDraft = 0;
	/** 运行中 */
	private static $StateDone = 1;
	/** 已结束 */
	private static $StateFinish = 2;
	/** 终止 */
	private static $StateStop = 3;

	/** 表单历史版本 */
	private static $Version_history = 1;
	/** 表单最新版本 */
	private static $Version_new = 2;

	/** 新费用报销流程表单 */
	private static $IsExpense = 6;

	private static $Dept_Table = "sc_dept";

	private static $User_Table = "sc_user";

	private static $Group_Table = "sc_group";

	private static $Expense_Type_Table = "expense_type";

	//应用所有权限对应的key值
	private static $App_All_Data_Key = "new_expense";

	public function __construct($log_pre_str='') {
		global $mod_name;
		parent::__construct($mod_name, $log_pre_str);

	}


	/**
	 * 检查是否有指定报表的查看权限
	 * @param  $com_id    
	 * @param  $user_id   
	 * @param  $report_id 
	 * @return 返回报表信息         
	 */
	public function check_report($com_id, $user_id, $report_id){
		$fields = "id, create_id, type, visit_user, visit_dept, visit_group";
		$report_info = $this -> get_report_by_id($com_id, $report_id, $fields);
		
		if(empty($report_info) || $report_info['type'] != 3){
			return array();
		}
		if($this -> report_user_cache($report_info['id'], $user_id)){
			return $report_info;
		}

		$user_ids = json_decode($report_info['visit_user'], true);
		
		if(!empty($user_ids) && in_array($user_id, $user_ids)){
			$this -> report_user_cache($report_info['id'], $user_id, true);
			return $report_info;
		}

		$dept_ids = json_decode($report_info["visit_dept"], true);
		if(!empty($dept_ids)){
			$user_info = g('dao_user') -> get_by_id($user_id, "dept_tree");
			$dept_tree = json_decode($user_info['dept_tree'], true);
			$user_dept = array();
			foreach ($dept_tree as $dept) {
				$user_dept = array_merge($user_dept, $dept);
			}
			if(array_intersect($user_dept, $dept_ids)){
				$this -> report_user_cache($report_info['id'], $user_id, true);
				return $report_info;
			}
		}

		$group_ids = json_decode($report_info['visit_group'], true);
		if(!empty($group_ids)){
			$group_list = g('dao_group') -> get_by_user_id($com_id, $user_id, "id");
			$user_group = array();
			foreach ($group_list as $group) {
				$user_group[] = $group['id'];
			}
			if(array_intersect($user_group, $group_ids)){
				$this -> report_user_cache($report_info['id'], $user_id, true);
				return $report_info;
			}
		}
		throw new \Exception("没有权限查看该报表");
	}

	/**
	 * 获取当前用户有权限的表单id
	 * @param   $admin_id 管理员ID
	 * @return           
	 */
	public function get_my_form($admin_id=0){
		$cond = array(
				'version=' => self::$Version_new,
				'is_other_proc=' => self::$IsExpense,
				'info_state=' => self::$InfoOn
			);
		if(!empty($admin_id)){
			$cond["creater_id="] = $admin_id;
		}
		$fields = "id, form_history_id";
		$order = "";
		$forms = g('mv_form') -> get_form_by_cond($cond, $fields);
		$form_ids = array();
		foreach ($forms as $form) {
			$form_ids[] = $form['form_history_id'];
		}
		return $form_ids;
	}

	/**
	 * 获取所有分类信息
	 * @return 
	 */
	public function get_all_classify($com_id, $fields ="id, name"){
		$cond = array(
				"com_id=" => $com_id,
				"info_state=" => self::$InfoOn
			);
		$ret = g('pkg_db') -> select(self::$Expense_Type_Table, $fields, $cond);
		return $ret ? $ret : array();
	}

	/**
	 * 根据关键字获取员工、部门、分组信息
	 * @param   $key_word 
	 * @return            
	 */
	public function get_user_list($com_id, $root_id, $user_ids, $dept_ids, $group_ids){
		$group_list = $this -> _get_group($com_id, $group_ids);
		$user_id = array();
		foreach ($group_list as $group) {
			$user_ids = json_decode($group['user_list'], true);
			$user_id = array_merge($user_id, $user_ids);
		}
		$user_list = $this -> _get_user($root_id, $user_ids, $dept_ids);
		foreach ($user_list as $user) {
			$user_id[] = $user['id'];
		}unset($user);
		$user_id = array_unique($user_id);
		return $user_id;
	}


	/**
	 * 根据分类获取图数据
	 * @param   $com_id     
	 * @param   $form_ids   表单ID数组 （form_history_id数组）
	 * @param   $start_time 开始时间戳
	 * @param   $end_time   结束时间戳
	 * @return              
	 */
	public function get_count_by_classify($com_id, $form_ids, $start_time, $end_time){
		$form_config = $this -> _get_form_config();
		$classify_key = $form_config["classify"];
		$money_key = $form_config["money"];

		//获取查询实例数据（包含分类-金额）的SQL
		$classify_sql = $this -> _get_classify_sql($classify_key, $money_key, $form_ids);
		//获取查询支付状态的实例数据
		$pay_formsetinst_sql = $this -> _get_pay_formsetinst_sql($com_id, $start_time, $end_time);

		/*
		 *	实现逻辑：1.查询指定表单下, 已经结束的实例信息，包含（分类-金额） 	t
		 *			2.查询所有已支付的实例信息				 			t2
		 *			3. t1 和 t2 INNER 联表
		 *	t2查询所有已支付实例信息的原因：t2表无法判断该实例是否在指定的表单下，所以通过与t1 INNER联表 之后过滤掉非指定表单的数据
		 */
		$sql = <<<EOF
			SELECT {$classify_key}, SUM({$money_key}) {$money_key}
			FROM ({$classify_sql}) t INNER JOIN ({$pay_formsetinst_sql}) t2 ON t.formsetinst_id = t2.formsetinst_id
			GROUP BY {$classify_key}
EOF;
		$ret = g('pkg_db') -> query($sql);

		$data = array();
		foreach ($ret as $r) {
			$data[$r[$classify_key]] = $r[$money_key];
		}unset($r);
		return $data;
	}

	/**
	 * 根据时间获取汇总图数据
	 * @param   $com_id      
	 * @param   $year         年份
	 * @param   $form_ids     表单ID数组
	 * @param   $classify_id  分类ID
	 * @param   $user_list    员工ID数组
	 * @return               
	 */
	public function get_count_by_time($com_id, $start_time, $end_time, $form_ids, $classify_ids, $user_list){
		$form_config = $this -> _get_form_config();
		$classify_key = $form_config["classify"];
		$money_key = $form_config["money"];

		$classify_array = empty($classify_ids) ? array() : $classify_ids;

		//获取查询实例数据（包含分类-金额）的SQL
		$classify_sql = $this -> _get_classify_sql($classify_key, $money_key, $form_ids, $classify_array, $user_list);
		//获取查询支付状态的实例数据
		$pay_formsetinst_sql = $this -> _get_pay_formsetinst_sql($com_id, $start_time, $end_time);

		/**
		 * 实现逻辑：1.查询指定表单下, 已经结束的实例信息，包含（分类-金额） 	t
		 *		   2.查询所有已支付的实例信息				 			t2
		 *		   3. t1 和 t2 INNER 联表
		 * 联表之后，根据付款时间[pay_time]格式化之后，进行group by汇总
		 */
		$sql = <<<EOF
			SELECT FROM_UNIXTIME(t2.pay_time, "%m") month, SUM({$money_key}) {$money_key}
			FROM ({$classify_sql}) t INNER JOIN ({$pay_formsetinst_sql}) t2 ON t.formsetinst_id = t2.formsetinst_id
			GROUP BY month
EOF;
		$ret = g('pkg_db') -> query($sql);
		$data = array();
		foreach ($ret as $r) {
			$data[intval($r['month'])] = intval($r[$money_key]);
		}unset($r);

		return $data;
	}


	/**
	 * 获取按照部门汇总
	 * @param   $com_id      企业ID
	 * @param   $dept_id     指定部门ID
	 * @param   $start_time  支付时间-开始时间
	 * @param   $end_time    支付时间-结束时间
	 * @return              
	 */
	public function get_table_count($com_id, $form_ids, $dept_id, $start_time, $end_time, $start=0, $page_size=20){

		$child_dept = g('dao_dept') -> list_direct_child($dept_id);
		$dept_list = array();
		foreach ($child_dept as $dept) {
			$dept_list[] = $dept['id'];
		}unset($dept);

		$classify_list = $this -> get_all_classify($com_id);

		$form_config = $this -> _get_form_config();
		$classify_key = $form_config["classify"];
		$money_key = $form_config["money"];

		//获取查询实例数据（包含分类-金额）的SQL
		$classify_sql = $this -> _get_classify_sql($classify_key, $money_key, $form_ids, array(), array());
		//获取查询支付状态的实例数据
		$pay_formsetinst_sql = $this -> _get_pay_formsetinst_sql($com_id, $start_time, $end_time);

		$dept_fields = $this -> _get_dept_fields($dept_id, $dept_list);

		$classify_fields = $this -> _get_classify_fields($classify_list, $classify_key, $money_key);

		//按照部门汇总SQL
		$sql = <<<EOF
		SELECT creater_id, {$dept_fields} dept_id, {$classify_fields['fields']}
		FROM ($classify_sql) t INNER JOIN ({$pay_formsetinst_sql}) t2 ON t.formsetinst_id = t2.formsetinst_id left join sc_user u on u.id = t.creater_id left join sc_dept d on d.id = u.main_dept
		GROUP BY dept_id
		HAVING dept_id >= 0 
EOF;

		//分页获取SQL
		$limit_sql = <<<EOF
		SELECT *
		FROM ({$sql}) t
		ORDER BY dept_id DESC
		LIMIT {$start}, {$page_size}
EOF;
		$data = g('pkg_db') -> query($limit_sql);

		//汇总数据SQL
		$count_sql = <<<EOF
		SELECT count(*) dept_id, {$classify_fields['count_fields']}
		FROM ({$sql}) t
EOF;
		$count_data = $this -> _get_table_count($count_sql);

		$data = $this -> _dept_user_format($data);
		$ret = array(
				"data" => $data,
				"count" => $count_data
			);
		return $ret;
	}

	/**
	 * 检查指定管理员是否有应用所有权限
	 * @param   $admin_id 管理员id
	 * @return            
	 */
	public function check_admin_privilege($admin_id){
		return g("api_expense") -> check_admin_privilege($admin_id, self::$App_All_Data_Key);
	}

//---------------------------------------------------内部实现------------------------------------------


//begin-------------------------------------------------获取查询数据SQL-------------------------------------
	/**
	 * 获取查询 实例数据（包含 分类-金额）的SQL
	 * @param   $classify_key 
	 * @param   $money_key    
	 * @param   $form_ids     
	 * @param   $user_list    
	 * @return                
	 */
	private function _get_classify_sql($classify_key, $money_key, $form_ids, $classify_ids = array(), $user_list = array()){
		//指定表单
		if(!empty($form_ids)){
			$form_ids_str = implode(",", $form_ids);
			$form_cond = "AND form_history_id in ({$form_ids_str}) ";
		}else{
			$form_cond = "";	
		}

		//指定分类
		if(!empty($classify_ids)){
			$classify_ids_str = implode(",", $classify_ids);
			$having = " HAVING {$classify_key} in ({$classify_ids_str}) ";
		}else{
			$having = " HAVING {$classify_key} != '' ";
		}

		//指定员工
		if(!empty($user_list)){
			$user_cond_str = implode(",", $user_list);
			$user_cond = " AND creater_id IN ({$user_cond_str}) ";
		}else{
			$user_cond = "";
		}

		$sql = <<<EOF
		select formsetinst_id, creater_id, GROUP_CONCAT(IF(`input_key`='{$classify_key}', special_val, '') separator '') '{$classify_key}', GROUP_CONCAT(IF(`input_key`='{$money_key}', val, '') separator '') '{$money_key}'
		from mv_proc_formsetinst_label
		where state = 2 {$form_cond} {$user_cond} 
		group by formsetinst_id, label_id
		{$having}
EOF;
		return $sql;
	}

	/**
	 * 获取支付状态数据
	 * @param   $com_id     
	 * @param   $start_time 支付时间-开始时间
	 * @param   $end_time   支付时间-结束时间
	 * @return  
	 */
	private function _get_pay_formsetinst_sql($com_id, $start_time, $end_time){
		$time_cond = array();
		!empty($start_time) && $time_cond[] = "pay_time >= {$start_time}";
		!empty($end_time) && $time_cond[] = "pay_time <= {$end_time}";

		$cond_str = empty($time_cond) ? "" : (" AND ". implode(" AND ", $time_cond));

		$sql = <<<EOF
		SELECT formsetinst_id , pay_time
		FROM expense_record
		WHERE info_state=1 AND pay_state = 1 AND com_id = {$com_id} {$cond_str}
EOF;
		return $sql;
	}

	/**
	 * 获取按照部门汇总 的fields(dept相关)
	 * @param   $dept_list 
	 * @return             
	 */
	private function _get_dept_fields($dept_id, $dept_list){
		$fields = "if(main_dept ={$dept_id}, creater_id, -1)";
		foreach ($dept_list as $id) {
			$fields = "if(d.p_list like '%\"{$id}\"%' or main_dept={$id}, {$id}, {$fields})";
		}unset($dept);
		return $fields;
	}

	/**
	 * 获取按照部门汇总 的fields(dept相关)
	 * @param   $dept_list 
	 * @return             
	 */
	private function _get_classify_fields($classify_ids, $classify_key, $money_key){
		$fields = array();
		$count_fields = array();
		foreach ($classify_ids as $classify) {
			$count_fields[] = " SUM(classify_{$classify['id']}) classify_{$classify['id']} ";
			$fields[] = "SUM(if({$classify_key}={$classify['id']}, {$money_key}, 0)) classify_{$classify['id']}";
		}unset($id);
		$fields_str = implode(",", $fields);
		$count_fields_str = implode(",", $count_fields);

		$ret = array(
				"fields" => $fields_str,
				"count_fields" => $count_fields_str
			);
		return $ret;
	}


//end-------------------------------------------------获取查询数据SQL-------------------------------------

	/**
	 * 获取表单固定字段配置
	 * @return 
	 */
	private function _get_form_config(){
		$config = load_config("model/expense/form");
		if(empty($config) || !isset($config['Fix_expense'])){
			throw new \Exception("配置文件缺失");
		}
		return $config["Fix_expense"];
	}


//begin----------------------------------------------模糊匹配员工相关------------------------------------

	/**  根据分组ID获取分组user_id */
	private function _get_group($com_id, $group_ids){
		if(empty($group_ids)) return array();
		$cond = array(
				'com_id=' => $com_id,
				'id IN' => $group_ids,
				'state=' => self::$InfoOn
			);
		$fields = 'id, name, user_list';
		$group_list = $this -> _list_by_cond(self::$Group_Table, $cond, $fields);
		return $group_list;
	}

	private function _get_dept($root_id, $dept_ids, $fields = "id, name"){
		if(empty($dept_ids)) return array();
		$cond = array(
				"root_id=" => $root_id,
				"id IN" => $dept_ids,
				"state=" => self::$InfoOn
			);
		$dept_list = $this -> _list_by_cond(self::$Dept_Table, $cond, $fields);
		return $dept_list;
	}

	/**
	 * 根据部门id数组和员工id数组 获取员工信息
	 * @param   $root_id  
	 * @param   $user_ids 
	 * @param   $dept_ids 
	 * @return            
	 */
	private function _get_user($root_id, $user_ids, $dept_ids=array()){
		if(empty($user_ids) && empty($dept_ids)){
			return array();
		} 
		$cond = array(
				'root_id=' => $root_id,
				'state !=' => self::$InfoDelete,
				'__OR' => array()
			);
		
		if(!empty($user_ids)){
			$cond['__OR'][] = array(
					'id IN' => $user_ids,
				);
		}

		if(!empty($dept_ids)){
			foreach ($dept_ids as $key => $dept_id) {
				$cond["__OR"]["__{$key}__dept_tree like"] = '%"'.$dept_id.'"%';
			}
		}
		$fields = 'id, name';
		return $this -> _list_by_cond(self::$User_Table, $cond, $fields);
	}
//end--------------------------------------------------模糊匹配员工相关------------------------------------

	/**
	 * 将员工和部门信息 从ID转换成名称
	 * @param   $data 
	 * @return  
	 */
	private function _dept_user_format($data){
		$dept_arr = array();
		$user_arr = array();
		foreach ($data as $row) {
			if($row['creater_id'] == $row['dept_id']){
				//员工
				$user_arr[] = $row['creater_id'];
			}else{
				//部门
				$dept_arr[] = $row['dept_id'];
			}
		}unset($row);

		$user_list = !empty($user_arr) ? g("dao_user") -> get_by_ids($_SESSION[SESSION_VISIT_DEPT_ID], $user_arr, "id, name") : array();

		$dept_list = !empty($dept_arr) ? g("dao_dept") -> get_by_ids($dept_arr, "id, name") : array();

		$all_user = array();
		foreach ($user_list as $user) {
			$all_user[$user['id']] = $user['name'];
		}unset($user);

		$all_dept = array();
		foreach ($dept_list as $dept) {
			$all_dept[$dept['id']] = $dept['name'];
		}unset($dept);

		foreach ($data as $key => $val) {
			if($val['dept_id'] == $val['creater_id']){
				//员工
				$data[$key]["dept_id"] = $all_user[$val['creater_id']];
			}else{
				//部门
				$data[$key]["dept_id"] = $all_dept[$val['dept_id']];
			}
			unset($data[$key]['creater_id']);
		}unset($val);
		return $data;
	}

	/**
	 * 获取汇总数据
	 * @param   $count_sql  汇总SQL
	 * @return             
	 */
	private function _get_table_count($count_sql){
		$key = $this -> get_redis_key($count_sql);
		$data = g('pkg_redis') -> get($key);
		if(!empty($data)){
			$data = json_decode($data, true);
		}else{
			$data = g('pkg_db') -> query($count_sql);
			if(!empty($data)){
				//只获取第一行汇总数据
				$data = $data[0];
			}
			g('pkg_redis') -> set($key, json_encode($data), 60);
		}
		return $data;
	}

	/**
	 * 获取可见范围字符串
	 * @param   $users  员工ID数组
	 * @param   $depts  部门ID数组
	 * @param   $groups 分组ID数组
	 * @return          
	 */
	public function get_range_str($com_id, $root_id, $users, $depts, $groups){
		$user_str = array();
		if(!empty($users)){
			$user_list = $this -> _get_user($root_id, $users);
			foreach ($user_list as $user) {
				$user_str[] = $user['name'];
			}unset($user);
		}

		$dept_str = array();
		if(!empty($depts)){
			$dept_list = $this -> _get_dept($root_id, $depts);
			foreach ($dept_list as $dept) {
				$dept_str[] = $dept['name'];
			}unset($dept);
		}

		$group_str = array();
		if(!empty($groups)){
			$group_list = $this -> _get_group($com_id, $groups);
			foreach ($group_list as $group) {
				$group_str[] = $group['name'];
			}unset($group);
		}

		$dept_str = implode(";", $dept_str);
		!empty($dept_str) && $dept_str .= ";";
		$user_str = implode(";", $user_str);
		!empty($user_str) && $user_str .= ";";
		$group_str = implode(";", $group_str);
		!empty($group_str) && $group_str .= ";";
		return $dept_str . $user_str . $group_str;
	}

	/**
	 * 获取指定报表信息
	 * @param   $com_id    
	 * @param   $report_id 
	 * @return             
	 */
	public function get_report_by_id($com_id, $report_id, $fields="id, create_id, type, visit_user, visit_dept, visit_group"){
		$report_info = g('api_report') -> get_report_by_id($com_id, $report_id, $fields);
		return $report_info;
	}

	/**
	 * 缓存报表权限
	 * @param   $report_id 报表ID
	 * @param   $user_id   员工ID
	 * @return             
	 */
	private function report_user_cache($report_id, $user_id, $save = false){
		$str = __FUNCTION__.":{$report_id}:{$user_id}";
		$key = $this -> get_redis_key($str);

		if($save){
			//缓存2分钟
			g('pkg_redis') -> set($key, 1, 120);
		}else{
			return g('pkg_redis') -> get($key);
		}
	}

	private function get_redis_key($str){
		return md5("Expense_redis_key:".$str);
	}

}
// end of file
