<?php 
/**
 * 考勤管理业务逻辑处理类
 */

namespace model\expense\server;

class ServerExpense extends \model\api\server\ServerBase {

	/** 表单固定控件 */
	private $Fix_input = array();
	/** 表单隐藏控件 */
	private $Hide_input = array();

	public function __construct($log_pre_str='') {
		parent::__construct('expense', $log_pre_str);
	}

	/**
	 * 获取电子发票信息
	 * @param  [type] $card_id      
	 * @param  [type] $encrypt_code 
	 * @return [type]               
	 */
	public function get_invoice($com_id, $card_id, $encrypt_code) {
		$access_token = g('api_atoken') -> get_by_com($com_id);

		return g('wxqy_card')->get_invoice_info($access_token, $card_id, $encrypt_code);
	}

	/**
	 * 上传电子发票附件
	 * @param  [type] $com_id 
	 * @param  [type] $url    
	 * @return [type]         
	 */
	public function upload_invoice_pdf($com_id, $url) {
		$ret = g('pkg_curl')->get($url);
		if ($ret['httpcode'] != 200) throw new \Exception('获取发票附件信息失败');

		$file_name = md5($com_id.'-'.mt_rand()) .'.pdf';
		$file_path = $this->upload_dir.$file_name;
		file_put_contents($file_path, $ret['content']);

		$upload_file = array(
			'name' => $file_name,
			'tmp_name' => $file_path,
			'size' => filesize($file_path)
			);

		$file_info = g('api_media')->cloud_upload('', $upload_file);
		//删除本地文件
		unlink($file_path);

		return $file_info;
	}

	/**
	 * 保存报销申请表单信息
	 * @param  [type]  $com_id         
	 * @param  [type]  $user_id        
	 * @param  [type]  $formsetinst_id 
	 * @param  [type]  $form_vals      
	 * @param  integer $is_new     是否是新建，0：否：1是
	 * @return [type]                  
	 */
	public function save_expense_record($com_id, $user_id, $formsetinst_id, $form_vals, $is_new=0) {
		$this->init_form_config();
		!is_array($form_vals) && $form_vals = json_decode($form_vals, true);

		$expense_type = g('api_expense')->get_expense_type($com_id, 'id,name');
		$expense_type = array_column($expense_type, 'name', 'id');
		$electro_invs = array();
		$is_electro_inv = 0;

		$sum_input = $form_vals[$this->Fix_input['total']];
		if ($sum_input['val'] <= 0) throw new \Exception('获取报销金额失败');
		
		$sum_money = $sum_input['val'];
		$sum_money_capital = isset($sum_input['capitalized_val']) ? $sum_input['capitalized_val'] : '';
		
		$detail_input = $form_vals[$this->Fix_input['detail']];
		if (!isset($detail_input['rec']) || empty($detail_input['rec'])) {
			throw new \Exception('请添加报销明细');
		}
		foreach ($detail_input['rec'] as $rec) {
			//验证报销分类
			if (!isset($rec[$this->Fix_input['classify']]) || empty($rec[$this->Fix_input['classify']]['val'])) {
				throw new \Exception('请选择报销分类');
			}
			$type = $rec[$this->Fix_input['classify']]['val'];
			$opts_nums = explode(',', $rec[$this->Fix_input['classify']]['opts_nums']);
			$opts = explode(',', $rec[$this->Fix_input['classify']]['opts']);
			if (isset($rec[$this->Fix_input['classify']]['val_id']) && !empty($rec[$this->Fix_input['classify']]['val_id'])) {
				if (!in_array($rec[$this->Fix_input['classify']]['val_id'], $opts_nums)) {
					throw new \Exception('请选择正确的报销分类!');
				}
				$search_type = $rec[$this->Fix_input['classify']]['val_id'];
			} else {
				$search_type = $opts_nums[array_search($type, $opts)];
				if ($search_type === false) throw new \Exception('请选择正确的报销分类');
			}
			if ($is_new && !isset($expense_type[$search_type])) {
				throw new \Exception('报销分类已被删除');
			}
			
			//电子发票
			// if (isset($rec[$this->Hide_input['info']]) && !empty($rec[$this->Hide_input['info']]['val'])) {
			// 	$hide_val = json_decode($rec[$this->Hide_input['info']]['val'], true);
			// 	if (isset($hide_val['card_id'])) {
			// 		$tmp_data = array(
			// 			'card_id' 		=> $hide_val['card_id'],
			// 			'encrypt_code' 	=> $hide_val['encrypt_code'],
			// 			'file_hash' 	=> $hide_val['file_hash'],
			// 			);
			// 		$electro_invs[] = $tmp_data;
			// 		$is_electro_inv = 1;
			// 	}

			// }
		}unset($rec);

		if ($is_new) {
			//新建表单
			g('api_expense')->save_record($com_id, $user_id, $formsetinst_id, $sum_money, $sum_money_capital, $is_electro_inv);
			// 电子发票信息
			// if (!empty($electro_invs)) {
			// 	foreach ($electro_invs as $inv) {
			// 		//锁定发票
			// 		g('api_expense')->change_invoice_state($com_id, $inv['card_id'], $inv['encrypt_code'], 2);
			// 		//保存发票信息
			// 		g('api_expense')->save_invoice($com_id, $formsetinst_id, $inv['card_id'], $inv['encrypt_code'], $inv['file_hash'], 2);
			// 	}unset($inv);
			// }

		} else {
			//更新
			$record_info = g('api_expense')->get_record_by_id($com_id, $formsetinst_id);
			if (!$record_info) throw new \Exception('查询报销记录失败');
			/**
			//旧发票信息
			$o_invs = array();
			$o_card_ids = array();
			if ($record_info['is_electro_invoice']) {
				$old_invs = g('api_expense')->get_invoice_by_id($com_id, $formsetinst_id);
				foreach ($old_invs as $val) {
					$o_invs[$val['card_id']] = $val;
					$o_card_ids[] = $val['card_id'];
				}unset($val);
			}
			//新发票信息
			$n_invs = array();
			$n_card_ids = array();
			foreach ($electro_invs as $val) {
				$n_invs[$val['card_id']] = $val;
				$n_card_ids[] = $val['card_id'];
			}unset($val);
			//计算出需要删除的、插入的发票信息
			$new_cards = array_diff($n_card_ids, $o_card_ids);
			!is_array($new_cards) && $new_cards = array();
			$del_cards = array_diff($o_card_ids, $n_card_ids);
			!is_array($del_cards) && $del_cards = array();

			//删除发票信息
			foreach ($del_cards as $id) {
				//解锁发票
				g('api_expense')->change_invoice_state($com_id, $id, $o_invs[$id]['encrypt_code'], 1);
				//删除发票
				g('api_expense')->del_invoice($com_id, $formsetinst_id, '', array($o_invs[$id]['id']));
				//删除发票附件
				g('api_expense')->del_invoice_file($com_id, $user_id, $formsetinst_id, $o_invs[$id]['file_hash']);
			}unset($id);

			//添加新发票信息
			foreach ($new_cards as $id) {
				//锁定发票
				g('api_expense')->change_invoice_state($com_id, $id, $n_invs[$id]['encrypt_code'], 2);
				//保存发票信息
				g('api_expense')->save_invoice($com_id, $formsetinst_id, $id, $$n_invs[$id]['encrypt_code'], $$n_invs[$id]['file_hash'], 2);
			}unset($id);
			*/
			if ($sum_money != $record_info['sum_money'] || $sum_money_capital != $record_info['sum_money_capital'] || $is_electro_inv != $record_info['is_electro_invoice']) {
				g('api_expense')->update_record($com_id, $formsetinst_id, $sum_money, $sum_money_capital, $is_electro_inv);
			}

		}//else

	}

	/**
	 * 结束报销申请
	 * @param  [type] $com_id         
	 * @param  [type] $user_id        
	 * @param  [type] $formsetinst_id 
	 * @return [type]                 
	 */
	public function expense_finish($com_id, $formsetinst_id) {
		return $invs = g('api_expense')->expense_finish($com_id, $formsetinst_id);
	}

	/** 获取我的报销记录 */
	public function get_record_list($com_id, $user_id, $cond=array(), $fields='', $page=1, $page_size=10){
		$list = g('api_expense')->get_record_list($com_id, $user_id, $cond, $fields, $page, $page_size, '', true);
		!empty($list['data']) && empty($this->Fix_input) && $this->init_form_config();

		$check_rh = g('api_expense')->check_runheng($com_id);
		$pay_subject = array();
		if ($check_rh) {
			$rh_ids = array_column($list['data'], 'rh_id');
			!empty($rh_ids) && $pay_subject = g('api_expense')->get_subjects($com_id, 'id,name', $rh_ids, array(), true);
			$pay_subject = array_column($pay_subject, 'name', 'id');
		}
		
		$info = array();
		foreach ($list['data'] as $record) {
			$type_list = g('api_expense')->get_type_by_label($record['formsetinst_id'], 'val', 'select', $this->Fix_input['classify']);
			$type_list = array_column($type_list, 'val');
			empty($type_list) && $type_list = array();
			$type_list = array_values(array_unique($type_list));

			$pay_str = $record['pay_type'] == 1 ? "现金支付" : "银行汇款";
			$pay_str = isset($pay_subject[$record['rh_id']]) ? $pay_subject[$record['rh_id']] : $pay_str;

			$tmp = array(
				'id' 				=> $record['formsetinst_id'],
				'formsetinst_name' 	=> $record['formsetinst_name'],
				'pay_state' 		=> $record['pay_state'],
				'pay_type' 			=> $record['pay_state'] ? $pay_str : '',
				'sum_money' 		=> $record['sum_money'],
				'expense_type' 		=> implode('、', $type_list),
				'create_time' 		=> $record['create_time'],
				);

			$info[] = $tmp;
		}unset($record);

		$data = array(
			'data' => $info,
			'count' => $list['count']
			);
		return $data;
	}


//-------------------------内部实现---------------------------------

	/** 加载表单初始化、固定控件配置 */
	private function init_form_config() {
		$config = load_config("model/expense/form");
		if (!is_array($config)) throw new \Exception('加载表单配置信息出错');

		$this->Fix_input = $config['Fix_expense'];
		$this->Hide_input = $config['Hide_expense'];
	}

}
/* End of this file */