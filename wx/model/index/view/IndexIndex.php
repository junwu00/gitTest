<?php
/**
 * 主页型应用单入口
 * @author 陈意浩
 * @date 2016-08-03
 *
 */
namespace model\index\view;
use package\crypto\Des;

class IndexIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('index',$log_pre_str);
	}
	
	//主页面
	public function index(){
		g('api_jssdk') -> assign_jssdk_sign();
		parent::get_user_info();
		$platid = parent::get_mobile_platid();
		$state = g('pkg_val') -> get_get('state');
		//是否从其他页面跳转过来
		$location = (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? TRUE : FALSE;
		if(($platid == 4 || $platid == 5) && empty((int)$state) && !$location){
			//跳转到中间页面
			header('location:/index.php?&app=index&a=pcIndex');
		}

		parent::get_consume_url();
		g('pkg_smarty') -> assign('title', '流程专家');
		$this -> show($this -> view_dir . 'page/index.html');
	}

    //后端跳转到新地址
    public function home(){
        header('location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxIndex');
    }

    //后端跳转到新地址
    public function fillForm(){
        $form_id = isset($_GET['form_id'])?$_GET['form_id']:'';
        $remind_time = isset($_GET['remind'])?$_GET['remind']:'';
        header('location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/processCreate?id='.$form_id.'&remind='.$remind_time);
    }

    //后端跳转到新地址
    public function jumpToAdmin(){
        try{
            //通过账号匹配管理员账号
            $admin = g('dao_admin') -> check_admin($_SESSION[SESSION_VISIT_COM_ID],$_SESSION[SESSION_VISIT_USER_ID]);
            if(!$admin)
                throw new \Exception("非管理员无法访问");

            $cache_key = md5($_SESSION[SESSION_VISIT_USER_WXACCT].'::'.$_SESSION[SESSION_VISIT_DEPT_ID]);
            g('pkg_redis')->set($cache_key,'1',120);
            header('location:'.QY_DYNAMIC_DOMAIN.'/index.php?page_name=login.php&a=wxlogin&d='.$_SESSION[SESSION_VISIT_DEPT_ID].'&u='.$_SESSION[SESSION_VISIT_USER_WXACCT]);
        }catch (\Exception $e){
            errorPage('无权限操作，请联系管理员开通权限');
        }
    }


    //后端跳转到报表新地址
    public function report(){
        header('location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxReportList');
    }
    //后端跳转到新地址
    public function msg_detail(){
        $msg_id = isset($_GET['msg_id'])?$_GET['msg_id']:'';
        header('location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxMaterialWarn?msg_id='.$msg_id);
    }

	//vue 重写后的主页面
	public function new_index(){
		g('api_jssdk') -> assign_jssdk_sign();
		parent::get_user_info();
		$platid = parent::get_mobile_platid();
		$state = g('pkg_val') -> get_get('state');
		//是否从其他页面跳转过来
		$location = (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? TRUE : FALSE;
		if(($platid == 4 || $platid == 5) && empty((int)$state) && !$location){
			//跳转到中间页面
			header('location:/index.php?&app=index&a=pcIndex');
		}

		parent::get_consume_url();
		g('pkg_smarty') -> assign('title', '流程专家');
		header('location:/view/index/dist/index.html');
	}

	//测试跳转页面
	public function test(){
		g('pkg_smarty') -> assign('title', '测试跳转页面');
		$this -> show($this -> view_dir . 'page/test.html');
	}

	//PC WX中间页面
	public function pcIndex(){
		g('pkg_smarty') -> assign('title', '流程专家');
		$this -> show($this -> view_dir . 'page/pcIndex.html');
	}

	//报表
	public function chart(){
		g('pkg_smarty') -> assign('title', '报表');
		$this -> show($this -> view_dir . 'page/chart.html');
	}

	//登录后台
    public function loginAdmin(){
        header('location:'.QY_DYNAMIC_DOMAIN);
    }

    public function jumpToReport(){
        $id = isset($_GET['id'])?$_GET['id']:0;
        $type = isset($_GET['type'])?$_GET['type']:'';
        $com_id = isset($_GET['com_id'])?$_GET['com_id']:'';

        header('location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/pullReport?id='.$id.'&type='.$type.'&com_id='.$com_id);
    }

    public function showImage(){
        try{
            if(empty($_GET['image'])){
                throw new \Exception("图片不存在");
            }
            $image = $_GET['image'];
            $des = new Des();
            $image = $des->decode($image);

            $imghttp = get_headers($image,true);

            header("content-type:{$imghttp['Content-Type']}");
            echo file_get_contents($image);

        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

}

//end