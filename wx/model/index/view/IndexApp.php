<?php
/**
 * 流程应用Ajax交互类
 *
 * @author luja
 * @create 2016-08-09
 */

namespace model\index\view;

use model\api\dao\ComBlacklist;

class IndexApp extends \model\api\view\AbsProc {

	// 错误提示码
	private $busy = 0;

	/** 常用表单数量 */
	private static $FormUserCount = 3;
	/** 自定义常用表单数量 */
	private static $FormUserCustomCount = 8;

	/** 应用版本1.0 */
	private static $Version_v1 = 0;
	/** 应用版本2.0 */
	private static $Version_v2 = 1;

	public function __construct($log_pre_str='') {
		parent::__construct('index',$log_pre_str);
		// 未知原因（系统繁忙）
		$this -> busy = \model\api\server\Response::$Busy;
	}


	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				// 获取流程表单模板
				$this -> get_form_list();
				break;
			}
			case 102 : {
				// 获取待办流程数量
				$this -> get_handler_sum();
				break;
			}
			case 103 : {
				// 获取待办/已办流程列表
				$this -> get_handler_proc();
				break;
			}
			case 104 : {
				// 获取我发起的流程列表
				$this -> get_my_proc();
				break;
			}
			case 105 : {
				// 获取知会我的流程列表
				$this -> get_notify_proc();
				break;
			}
			case 106 : {
				// 检查有效时长
				$this -> check_workhours();
				break;	
			}
			case 107 : {
				// 获取补录时间点
				$this -> get_makeup();
				break;	
			}
			case 108 : {
				// 获取快捷时间点
				$this -> get_time_list();
				break;	
			}
			case 109 : {
				// 获取自定义表单
				$this -> get_custom_form();
				break;	
			}
			case 110 : {
				// 获取全部表单
				$this -> get_all_form();
				break;	
			}
			case 111 : {
				// 保存自定义表单
				$this -> save_custom_form();
				break;	
			}
			default : {
				//非法的请求
				parent::echo_busy();
				break;
			}
		}
	}


	/** 生成审批二维码 */
	public function qrcode() {
		try {

			$id = g('pkg_val')->get_get('id');
			$pcWxCode = g('pkg_val')->get_get('pcWxCode');

			$id = intval($id);
			// //1是审批 0 是退回
			$pcWxCode != 0 && $pcWxCode = 1;

			$corpid = $_SESSION[SESSION_VISIT_CORP_ID];
			$url =  MAIN_DYNAMIC_DOMAIN."index.php?corpid={$corpid}&app=process&a=detail&listType=1&id={$id}&activekey=0&isIndex=1&pcWxCode={$pcWxCode}&menuType=2#pDealList";

			g('pkg_qrcode')->encode_url($url);
			exit();		
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

//-------------------------------------内部实现----------------------------------------------

	/** 检查指定之间段内的有效时长 */
	private function check_workhours(){
		try{
			$fields = array('start_time', 'end_time');
			$data = parent::get_post_data($fields);

			/** 如果日期没有时分，结束时间修改至第二天00:00 */
			$start_time = $data['start_time'];
			$end_time = $data['end_time'];
			$rest_type = isset($data['rest_type']) ? $data['rest_type'] : "";
			$user_id = isset($data['user_id']) ? intval($data['user_id']) : 0;

			if(empty($start_time) || empty($end_time) || $start_time > $end_time){
				throw new \Exception("时间段错误");
			}

			empty($user_id) && $user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$hours = g('ws_core') -> get_absence_time($com_id, $user_id, $start_time, $end_time);
			$config = g('ws_core') -> get_config($com_id);
			$work_hour = $config['work_hour'];
			$day = round($hours/$work_hour, 2);

			$second = $end_time - $start_time;
			$s_day = round($second/60/60/24, 1);
			if($s_day >= 1){
				$s_day.="天";	
			}else{
				$s_day = round($second/60/60, 1)."小时";
			}

			$info = array(
					'hours' => $hours,
					'day' => $day,
					'n_day' => $s_day
				);

			if($rest_type == "年假"){
				$year_balance = g('ws_rest') -> count_year_rest($com_id, $user_id, $hours);
				$all_year = $year_balance + $hours;

				$year_balance = round($year_balance/$work_hour, 1);
				$all_year = round($all_year/$work_hour, 1);
				$info['year_balance'] = $year_balance;
				$info['all_year'] = $all_year;
			}

			parent::echo_ok($info);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	private function get_makeup(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);
			$user_id = isset($data['user_id']) ? intval($data['user_id']) : 0;

			$start_date = date("Ymd", strtotime("-1 month"));
			$end_date = date("Ymd", strtotime("+1 day"));

			empty($user_id) && $user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$shift = g('ws_shift') -> get_noclock_shift($com_id, $user_id, $start_date, $end_date, $fields="id, start_time, start_clock_time, end_time, end_clock_time");

			$makeup_time = array();
			if(!empty($shift)){
				foreach ($shift as $s) {
					if(empty($s['start_clock_time']) && $s['start_time'] < time()){
						$makeup_time[$s['start_time']] = $s['start_time'];
					}
					if(empty($s['end_clock_time']) && $s['end_time'] < time()){
						$makeup_time[$s['end_time']] = $s['end_time'];
					}
				}unset($s);
			}

			$times = array_keys($makeup_time);
			$makeup_record = g('ws_checkwork') -> get_makeup_by_time($com_id, $user_id, $times);
			foreach ($makeup_record as $record) {
				if(isset($makeup_time[$record['makeup_time']])){
					unset($makeup_time[$record['makeup_time']]);
				}
			}unset($record);

			$makeup_time = array_values($makeup_time);
			rsort($makeup_time);
			$data = array(
					'makeup' => $makeup_time
				);

			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);	
		}
	}

	/** 获取快捷选择时间段 */
	private function get_time_list(){
		try{
			$fields = array();
			$data = parent::get_post_data($fields);
			$start_time = $data['start_time'];

			if(!empty($start_time)){
				$start_date = date("Ymd", $start_time);
			}else{
				$start_date = date("Ymd");
			}

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			
			$return_time = g('ws_shift') -> get_quest_time($com_id, $user_id, $start_date);
			$now = time();
			foreach ($return_time as $key => $value) {
				if($now > $value['end_time']){
					unset($return_time[$key]);
				}
			}
			$return_time = array_values($return_time);
			$data = array(
					'time' => $return_time,
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}


	/**
	 * 获取流程表单模板
	 * 分页获取所有表单，pagesize=9
	 * @return
	 */
	private function get_form_list() {
		try {
			$data = parent::get_post_data();
			$page = isset($data['page']) ? intval($data['page']) : 1;
			$page_size = isset($data['page_size']) ? intval($data['page_size']) : 0;

			$page < 1 && $page = 1;
			$page_size <= 0 && $page_size = 9;

			parent::log_i('开始获取可使用表单');

			// 获取用户常用表单
			$user_form = $this -> get_user_form($this -> user_id, $this -> com_id);
			$form_ids = array();
			if (!empty($user_form)) {
				foreach ($user_form as &$val) {
					array_push($form_ids, $val['form_id']);
					$val['id'] = $val['form_id'];
					unset($val['form_id']);
				}unset($val);
			}

			$form_list = g('ser_index_app') -> get_form_list(0, 0);
			// 去除重复的表单
			if (!empty($form_ids) && !empty($form_list['data'])) {
				foreach ($form_list['data'] as $key => $val) {
					if (in_array($val['id'], $form_ids)) {
						unset($form_list['data'][$key]);
					}
				}unset($val);
			}

			$list = array_merge($user_form, $form_list['data']);
			$return = array(
				'count' => (string)count($list),
				'data' => $list,
				);

			parent::log_i('成功获取可使用表单');
			parent::echo_ok(array('info' => $return));
		} catch (\Exception $e) {
			parent::log_e('获取可使用表单失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取待办流程总数
	 * @return 
	 */
	private function get_handler_sum() {
		try {
			parent::log_i('开始获取待办流程总数');
			//所有新版表单类型
			$procs = $this->get_install_proc();

			$count = g('mv_workinst') -> get_handler_proc_sum($this -> com_id, $this -> user_id, $procs);
			$info = array(
				'count' => (string)$count
				);
			parent::log_i('开始获取待办流程总数');
			parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::log_e('获取待办流程总数失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取待办/已办流程列表
	 * type：0：待办，1：已办
	 * @return 
	 */
	private function get_handler_proc() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page = isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name = isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort = isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by = isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 1) throw new \Exception('非法的流程类型！');
			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;

			$fields = array(
				'mpw.id', 'mpw.formsetinit_id', 'mpf.formsetinst_name', 'mpf.create_time', 
				'mpfm.form_name', 'mpw.creater_id',  'mpw.creater_name', 
				'su.pic_url creater_pic_url', 'mpw.receive_time', 'mpf.is_other_proc'
				);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($type === 0) {
				array_push($fields, 'mpw.state');
				if ($sort == 1) {
					$order = ' ORDER BY mpw.creater_name ' . $by;
				} elseif ($sort == 2) {
					$order = ' ORDER BY mpf.create_time ' . $by;
				} elseif ($sort == 3) {
					$order = ' ORDER BY mpf.formsetinst_name ' . $by;
				} elseif ($sort == 4) {
					$order = ' ORDER BY mpw.state ' . $by;
				} else {
					$order = ' ORDER BY mpw.receive_time ' . $by;
				}
			} else {
				array_push($fields, 'mpf.state formsetinst_state');
				array_push($fields, 'mpw.complete_time');
				if ($sort == 1) {
					$order = ' ORDER BY a.creater_name ' . $by;
				} elseif ($sort == 2) {
					$order = ' ORDER BY a.create_time ' . $by;
				} elseif ($sort == 3) {
					$order = ' ORDER BY a.formsetinst_name ' . $by;
				} elseif ($sort == 4) {
					$order = ' ORDER BY a.formsetinst_state ' . $by;
				} elseif ($sort == 5) {
					$order = ' ORDER BY a.complete_time ' . $by;
				} else {
					$order = ' ORDER BY a.receive_time ' . $by;
				}
			}

			$fields = implode(',', $fields);

			// 应用版本
			$cond['app_version'] = 1;
			//获取已安装的应用
			$procs = $this->get_install_proc();

			parent::log_i('开始获取待办/已办的流程');
			
			$list = g('mv_workinst') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type, $procs, $page, parent::$PageSize, $fields, $order, TRUE);
			
			parent::log_i('成功获取待办/已办的流程');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取待办/已办的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取我发起的流程列表
	 * type: 0:运行中，1：已结束，2：草稿
	 * @return 
	 */
	private function get_my_proc() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page 		= isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name 	= isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort 		= isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by 	= isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 2) throw new \Exception('非法的流程类型！');

			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;
			
			$fields = array(
				'mpf.id', 'mpf.formsetinst_name', 'mpf.create_time', 'mpfm.form_name',
				' mpf.state', 'mpf.is_other_proc'
				);
			$fields = implode(',', $fields);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($sort == 1) {
				$order = ' ORDER BY mpf.formsetinst_name ' . $by;
			} elseif ($sort == 2) {
				$order = ' ORDER BY mpfm.form_name ' . $by;
			} else {
				$order = ' ORDER BY mpf.id ' . $by;
			}

			// 应用版本
			$cond['app_version'] = 1;
			//获取已安装的应用
			$procs = $this->get_install_proc();

			parent::log_i('开始获取我发起的流程列表');

			$list = g('mv_formsetinst') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type, $procs, $page, parent::$PageSize, $fields, $order, TRUE);

			parent::log_i('成功获取我发起的流程列表');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取我发起的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取知会我的流程
	 * type：0：未阅读，1：已阅读 
	 * @return 
	 */
	private function get_notify_proc() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page 		= isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name 	= isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort 		= isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by 	= isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 1) throw new \Exception('非法的流程类型！');

			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;

			$fields = array(
				'mpn.id', 'mpn.formsetinst_id', 'mpn.formsetinst_name', 
				'mpfm.form_name', 'mpn.notify_time', 'mpn.send_id', 
				'mpn.send_name', 'su.pic_url send_pic_url', 'mpf.is_other_proc'
				);
			$fields = implode(',', $fields);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($sort == 1) {
				$order = ' ORDER BY mpf.create_time ' . $by;
			} elseif ($sort == 2) {
				$order = ' ORDER BY mpn.send_name ' . $by;
			} elseif ($sort == 3) {
				$order = ' ORDER BY mpf.formsetinst_name ' . $by;
			} elseif ($sort == 4) {
				$order = ' ORDER BY mpfm.form_name ' . $by;
			} else {	
				$order = ' ORDER BY mpn.notify_time ' . $by;
			}

			// 应用版本
			$cond['app_version'] = 1;
			//获取已安装的应用
			$procs = $this->get_install_proc();
			
			parent::log_i('开始获取知会我的流程列表');

			$list = g('mv_notify') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type, $procs, $page, parent::$PageSize, $fields, $order, TRUE);

			parent::log_i('成功获取知会我的流程列表');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取知会我的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取用户常用表单
	 */
	private function get_user_form($user_id, $com_id) {
		parent::log_i('开始获取用户常用表单');

		$app_version = self::$Version_v2;
		$form_user = g('mv_form_user') -> get_common_form($user_id, $com_id, $app_version);
		if (empty($form_user)) {
			parent::log_i('用户常用表单为空');
			return array();
		}

		//获取可以使用的表单类型
		$procs = $this -> get_install_proc();

		$user_group = g('dao_group') -> get_by_user_id($this -> com_id, $this -> user_id);
		$group_ids = array_column($user_group, 'id');

		$tmp_form_user = array();
		foreach ($form_user as $fu) {
			if (!in_array($fu['is_other_proc'], $procs)) continue;

			$version = $fu['version'];
			if ($version == parent::$StateHistory) { 
				//如果是历史版本，则更新为最新版本的form_id
				$public_form = g('mv_form') -> get_by_his_id($fu['form_history_id'], $com_id);
				if (!empty($public_form)) {
					$fu['form_id'] 		= $new_form_id 		= $public_form['id'];
					$fu['form_name'] 	= $new_form_name 	= $public_form['form_name'];
					$fu['form_describe'] = $new_form_describe = $public_form['form_describe'];
					$fu['scope'] 		= $new_form_scope 	= $public_form['scope'];
					$fu['groups'] 		= $new_form_groups 	= $public_form['groups'];

					// 判断新版本是否有权限发起
					$new_form_scope = json_decode($new_form_scope, TRUE);
					$new_form_groups = json_decode($new_form_groups, TRUE);
					if (count($new_form_scope) == 0) {
						$dept_arr = array();
					} else {
						$dept_arr = g('dao_dept') -> list_by_ids($new_form_scope, '*', FALSE);
					}
					$user = g('dao_user') -> get_by_id($user_id);

					$is_apply = FALSE;
					if (count($dept_arr) == 0 && count($new_form_groups) == 0) {
						$is_apply = TRUE;
					} else {
						foreach ($dept_arr as $dept) {
							if (strstr($user['dept_tree'], '"'.$dept['id'].'"')) {
								$is_apply = TRUE;
								break;
							}
						}unset($dept);
						$group_list = g('dao_group') -> get_by_ids($this -> com_id, $new_form_groups);
						$group_list = array_column($group_list, 'id');
						$same = array_intersect($group_list, $group_ids);
						!empty($same) && $is_apply = TRUE;
					}

					if ($is_apply == FALSE) {
						g('mv_form_user') -> del_common_form($fu['form_history_id'], $user_id, $com_id);
					} else {
						// 更新本来旧的表单id
						g('mv_form_user') -> update_common_form_id($new_form_id, $fu['form_history_id'], $user_id, $com_id);
						$tmp_form_user[] = $fu;
					}
				}
			} else {
				$tmp_form_user[] = $fu;
			}

			if (count($tmp_form_user) >= self::$FormUserCount) break;
		} unset($fu);

		foreach ($tmp_form_user as $key => &$form) {
			 is_null($form['form_describe']) && $form['form_describe'] = '';

			unset($form['com_id']);
			unset($form['user_id']);
			unset($form['form_history_id']);
			unset($form['user_time']);
			unset($form['info_state']);
			unset($form['info_time']);
			unset($form['version']);
			unset($form['scope']);
			unset($form['id']);
			unset($form['groups']);
		}unset($form);

		parent::log_i('成功获取用户常用表单');
		return $tmp_form_user;
	}

	/** 获取自定义表单 */
	private function get_custom_form() {
		try {
			parent::log_i('开始获取自定义常用表单');
			$forms = $this -> _custom_user_form();
			if (is_null($forms)) {
				$form_list = g('ser_index_app') -> get_form_list(1, 8);
				$forms = $form_list['data'];
				$form_ids = array_column($forms, 'id');
				g('mv_form_user') -> save_custom_form($this -> com_id, $this -> user_id, $form_ids);
			} else {
				if (count($forms) > self::$FormUserCustomCount) {
					$forms = array_slice($forms, 0, self::$FormUserCustomCount);
				}
			}

			$daoComBlacklist = new ComBlacklist();
			$isShowForm = $daoComBlacklist->checkFormBlackList($this->com_id);
			
			$info = array(
				'data' => $forms,
				'count' => (string)count($forms),
                'is_show_form'=>$isShowForm,
				);
			parent::log_i('成功获取自定义常用表单');
			parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::log_e('获取自定义常用表单失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/** 获取所有表单 */
	private function get_all_form() {
		try {
			parent::log_i('开始获取自定义常用表单');
			$c_form = $this -> _custom_user_form();
			$form_list = g('ser_index_app') -> get_form_list(0, 0);
			
			if (is_null($c_form)) {
				if ($form_list['count'] > self::$FormUserCustomCount) {
					$c_form = array_slice($form_list['data'], 0, self::$FormUserCustomCount);
					array_splice($form_list['data'], 0, self::$FormUserCustomCount);
					$a_form = array_values($form_list['data']);
				} else {
					$c_form = $form_list['data'];
					$a_form = array();
				}
				$form_ids = array_column($c_form, 'id');
				g('mv_form_user') -> save_custom_form($this -> com_id, $this -> user_id, $form_ids);
			
			} else {
				if (!empty($c_form)) {
					$form_ids = array_column($c_form, 'id');
					foreach ($form_list['data'] as $key => $val) {
						if (in_array($val['id'], $form_ids)) {
							unset($form_list['data'][$key]);
						}
					}unset($val);
					$a_form = array_values($form_list['data']);
				} else {
					$a_form = $form_list['data'];
				}
			}
			$c_ids = array_column($c_form, 'id');
			!$c_ids && $c_ids = array();
			
			$info = array(
				'c_count' => (string)count($c_form),
				'c_ids' => $c_ids,
				'c_form' => $c_form,
				'a_count' => (string)count($a_form),
				'a_form' => $a_form,
				);

			parent::log_i('成功获取所有表单');
			parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::log_e('获取可使用表单失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/** 保存自定义表单 */
	private function save_custom_form() {
		try {
			$data = parent::get_post_data();
			$form_ids = isset($data['form_ids']) ? $data['form_ids'] : array();

			if (is_array($form_ids) && !empty($form_ids)) {
				$form_list = g('mv_form') -> get_forms_by_ids($form_ids, 'id');
				$ids = array_column($form_list, 'id');
				$same = array_diff($form_ids, $ids);
				if (!empty($same)) throw new \Exception('不合法的表单');
				
			} else {
				$form_ids = array();
			}

			g('mv_form_user') -> update_custom_form($this -> com_id, $this -> user_id, array_values($form_ids));
			parent::log_i('成功保存自定义表单');
			
			parent::echo_ok(array('errmsg' => '保存成功'));
		} catch (\Exception $e) {
			parent::log_e('保存自定义表单失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

//----------------------------------内部调用---------------------

	/** 获取用户自定义常用表单 */
	private function _custom_user_form() {
		$com_id = $this -> com_id;
		$user_id = $this -> user_id;

		$form_user = g('mv_form_user') -> get_custom_form($com_id, $user_id);
		if (!$form_user) {
			parent::log_i('用户没有设置常用表单');
			return NULL;
		}
		$form_ids = json_decode($form_user['form_id'], TRUE);
		if (empty($form_ids)) {
			parent::log_i('用户常用表单为空');
			return array();
		}
		$id_map = array_flip($form_ids);

		$fields = array(
			'id', 'form_history_id', 'form_name', 'form_describe', 'icon', 'scope', 
			'groups', 'version', 'is_other_proc', 'app_version', 'is_show_wx'
			);
		$form_list = g('mv_form') -> get_forms_by_ids($form_ids, $fields);

		//获取可以使用的表单类型
		$procs = $this -> get_install_proc();

		$user_group = g('dao_group') -> get_by_user_id($com_id, $user_id);
		$group_ids = array_column($user_group, 'id');

		$tmp_form_user = array();
		foreach ($form_list as $fu) {
			$old_id = $fu['id'];
			//过滤没安装的表单类型；过滤不是流程专家版本的表单；过滤禁用的表单
			if (!in_array($fu['is_other_proc'], $procs) || $fu['app_version'] != self::$Version_v2 || $fu['is_show_wx'] != parent::$StateOn) {
				continue;
			}

			if ($fu['version'] == parent::$StateHistory) {
				$public_form = g('mv_form') -> get_by_his_id($fu['form_history_id'], $com_id);
				if (!empty($public_form) && $public_form['is_show_wx'] == parent::$StateOn) {
					$fu['id'] 				= $public_form['id'];
					$fu['form_name'] 		= $public_form['form_name'];
					$fu['form_describe']  	= $public_form['form_describe'];
					$fu['scope'] 			= $public_form['scope'];
					$fu['groups'] 		 	= $public_form['groups'];
                    $fu['user_list'] 		= $public_form['user_list'];

					// 判断新版本是否有权限发起
					$new_form_scope = json_decode($fu['scope'], TRUE);
					$new_form_groups = json_decode($fu['groups'], TRUE);
                    $new_form_users = json_decode($fu['user_list'], TRUE);

                    $user_arr = array();
                    !empty($new_form_users) && $user_arr = g('dao_user') -> list_by_ids($new_form_users, '*', FALSE);

                    $dept_arr = array();
					!empty($new_form_scope) && $dept_arr = g('dao_dept') -> list_by_ids($new_form_scope, '*', FALSE);

					$group_arr = array();
					!empty($new_form_groups) && $group_arr = g('dao_group') -> get_by_ids($com_id, $new_form_groups);

					//获取用户所在的部门
					$user = g('dao_user') -> get_by_id($user_id);
					$dept_tree = json_decode($user['dept_tree'], TRUE);
					$user_dept = array();
					foreach ($dept_tree as $dept) {
						$user_dept = array_merge($user_dept, $dept);
					}unset($dept);
					$user_dept = array_unique($user_dept);

					$is_apply = FALSE;
					if (count($dept_arr) == 0 && count($group_arr) == 0 && count($user_arr)==0) {
						$is_apply = TRUE;
					} else {
                        $user_list = array_column($user_arr, 'id');
                        in_array($this->user_id,$user_list) && $is_apply=true;

						$dept_list = array_column($dept_arr, 'id');
						$same_dept = array_intersect($dept_list, $user_dept);
						!empty($same_dept) && $is_apply = TRUE;

						$group_list = array_column($group_arr, 'id');
						$same = array_intersect($group_list, $group_ids);
						!empty($same) && $is_apply = TRUE;
					}

					$is_apply && $tmp_form_user[$id_map[$old_id]] = $fu;
				}
			} else {
				$tmp_form_user[$id_map[$old_id]] = $fu;
			}

		}unset($fu);

		ksort($tmp_form_user);
		$tmp_form_user = array_values($tmp_form_user);
		foreach ($tmp_form_user as $key => &$form) {
			is_null($form['form_describe']) && $form['form_describe'] = '';

			unset($form['form_history_id']);
			unset($form['scope']);
			unset($form['groups']);
			unset($form['version']);
			unset($form['app_version']);
			unset($form['is_show_wx']);
		}unset($form);

		return $tmp_form_user;
	}

	/** 获取已经安装的表单类型 */
	private function get_install_proc() {
		//获取已安装的应用
		$apps = g('ser_index_app') -> get_install_app();
		$procs = array();
		foreach ($apps as $app) {
			if ($app == 'process') {
				$procs[] = self::$IsProc;
			} elseif ($app == 'workshift') {
				array_push($procs, self::$IsMakeup, self::$IsNewRest, self::$IsLegwork);
			} elseif ($app == 'exaccount') {
				$procs[] = self::$IsNewExaccount;
			}
		}unset($app);
		return $procs;
	}

}
// end of file