<?php
/**
 * 公用提示页面
 * @author yangpz
 * @date 2016-02-16
 *
 */
namespace model\index\view;

class IndexNotice extends \model\api\view\ViewBase {

	public function __construct() {
		parent::__construct('index');
	}
	
	public function notice($notice) {
		!is_array($notice) && $notice = array($notice);
		g('pkg_smarty') -> assign('notice_arr', $notice);
		g('pkg_smarty') -> assign('title', '提示');
		g('pkg_smarty') -> show($this -> view_dir . 'page/notice.html');
	}
	
	public function warn($warn) {
		!is_array($warn) && $warn = array($warn);
		g('pkg_smarty') -> assign('warn_arr', $warn);
		g('pkg_smarty') -> assign('title', '警告');
		g('pkg_smarty') -> show($this -> view_dir . 'page/warn.html');
	}
	
	public function error($error) {
		!is_array($error) && $error = array($error);
		g('pkg_smarty') -> assign('error_arr', $error);
		g('pkg_smarty') -> assign('title', '错误');
		g('pkg_smarty') -> show($this -> view_dir . 'page/error.html');
	}
}

//end