<?php
/**
 * 我的页面Ajax交互类
 *
 * @author luja
 * @create 2016-08-09
 */

namespace model\index\view;

class IndexMine extends \model\api\view\ViewBase {

    // 错误提示码
    private $busy = 0;

    public function __construct($log_pre_str='') {
        parent::__construct('index',$log_pre_str);
        // 未知原因（系统繁忙）
        $this -> busy = \model\api\server\Response::$Busy;
    }


    /**
     * ajax行为统一调用方法
     *
     * @access public
     * @return void
     */
    public function index() {
        $cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
        switch($cmd) {
            case 101 : {
                // 获取我的信息
                $this -> get_mine_info();
                break;
            }
            case 102 : {
                // 保存我的信息
                $this -> update_mine_info();
                break;
            }
            case 103 : {
                // 保存密码和指纹
                $this -> save_pwd();
                break;
            }
            case 104 : {
                // 开启/关闭指纹
                $this -> state_fp();
                break;
            }
            case 105 : {
                // 获取指纹设置信息
                $this -> get_fingerprint();
                break;
            }
            case 106 : {
                // 验证指纹签名
                $this -> verify_sign();
                break;
            }
            case 107 : {
                // 验证密码
                $this -> check_pwd();
                break;
            }

            case 201 :{
                //获取当前账号系统信息
                $this->getCurrSystemInfo();
                break;
            }
            default : {
                //非法的请求
                parent::echo_busy();
                break;
            }
        }
    }

    /**
     * 获取当前系统信息
     */
    private function getCurrSystemInfo(){
        try {

            $power = load_config('model/api/powerby');
            $info = array(
                'HTTP_DOMAIN' => MAIN_DYNAMIC_DOMAIN,
                'CDN_DOMAIN' => MAIN_STATIC_DOMAIN,
                'MEDIA_DOMAIN' => MEDIA_URL_PREFFIX,
                'VERSION_VER' => VERSION_VER,
                'POWERBY'   => $power['desc'],
            );

            $userInfo = $this->get_user_info(true);

            $info = array_merge($info,$userInfo);

            parent::echo_ok(array('info' => $info));
        } catch (\Exception $e) {
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }

    /** 获取我的信息 */
    private function get_mine_info() {
        try {
            parent::log_i('开始获取我的信息');

            $info = g('ser_index_mine') -> get_by_id($this -> user_id);

            /**
             * IS_VIP：1：普通用户，2、VIP，3、SVIP
             */
            $ret = $this->get_vip_power(false);
            $is_vip = $ret['is_vip']+1;
            $vip_state = $ret['vip_state'];
            $past_time = $ret['past_time'];
            $past_date = $vip_state ? (int)ceil(($past_time - time()) / (60 * 60 * 24)) : 0;
            $past_date > 30 && $past_date = 0;

            $info['is_vip'] = (string)$is_vip;
            $info['vip_state'] = (string)$vip_state;
            $info['past_time'] = (string)$past_time;
            $info['past_date'] = (string)$past_date;
            $info['onetime_msg'] = parent::get_onetime_msg();

            parent::log_i('成功获取我的信息');
            parent::echo_ok(array('info' => $info));
        } catch (\Exception $e) {
            parent::log_e('获取我的信息失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }

    /** 修改我的个人信息 */
    private function update_mine_info() {
        try {
            $data = parent::get_post_data();

            $gender = isset($data['gender']) ? $data['gender'] : NULL;
            $mobile = isset($data['mobile']) ? $data['mobile'] : '';
            $email = isset($data['email']) ? $data['email'] : '';

            $gender === '' && $gender = NULL;

            parent::log_i('开始保存我的信息');
            g('ser_index_mine') -> update_mine_info($this -> user_id, $gender, $mobile, $email);

            parent::log_i('成功保存我的信息');
            parent::echo_ok();
        } catch (\Exception $e) {
            parent::log_e('获取我的信息失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }

    /** 保存密码和指纹 */
    private function save_pwd() {
        try {
            $data = parent::get_post_data(array('pwd', 'fingerprint'));
            $pwd = trim($data['pwd']);
            $fingerprint = stripslashes($data['fingerprint']);
            $fingerprint = substr($fingerprint, 1, strlen($fingerprint) - 2);

            if (empty($pwd)) throw new \Exception('密码为空');
            if (!is_numeric($pwd)) throw new \Exception('只能输入数字密码');
            if (strlen($pwd) != 6) throw new \Exception('只能输入6位数字密码');
            if (empty($fingerprint)) throw new \Exception('指纹信息为空');

            $com_id = $this->com_id;
            $user_id = $this->user_id;
            $pwd = md5($com_id.'-'.$user_id.'-'.$data['pwd']);

            $info = g('api_fingerprint') -> get_user_fingerprint($com_id, $user_id);
            if ($info) {
                //修改指纹
                if ($pwd != $info['pwd']) throw new \Exception('密码错误');

                g('api_fingerprint')->update_fingerprint($com_id, $user_id, $fingerprint);
            } else {
                //新建保存指纹
                g('api_fingerprint')->save_pwd($com_id, $user_id, $pwd, $fingerprint);
            }

            parent::echo_ok();
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /** 开启/关闭指纹 */
    private function state_fp() {
        try {
            $data = parent::get_post_data(array('state'));
            $pwd = isset($data['pwd']) ? trim($data['pwd']) : '';
            $state = intval($data['state']);
            $state != 1 && $state = 0;

            $com_id = $this->com_id;
            $user_id = $this->user_id;
            $info = g('api_fingerprint') -> get_user_fingerprint($com_id, $user_id);
            if (!$info) throw new \Exception('查询指纹设置失败');
            if ($state == 0) {
                //关闭指纹时才需要验证密码
                if (empty($pwd)) throw new \Exception('密码为空');
                if (!is_numeric($pwd)) throw new \Exception('只能输入数字密码');
                if (strlen($pwd) != 6) throw new \Exception('只能输入6位数字密码');

                $pwd = md5($com_id.'-'.$user_id.'-'.$pwd);
                if ($pwd != $info['pwd']) throw new \Exception('密码错误');
            }

            g('api_fingerprint')->change_fingerprint($com_id, $user_id, $state);

            parent::echo_ok();
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /** 获取用户设置指纹信息 */
    private function get_fingerprint() {
        try {
            $state = 0;
            $has_fp = 0;
            $info = g('api_fingerprint') -> get_user_fingerprint($this->com_id, $this->user_id);
            if ($info) {
                $info['state'] && $state = 1;
                !empty($info['fingerprint']) && $has_fp = 1;
            }

            $data = array(
                'state' => (string)$state,
                'has_fp' => (string)$has_fp
            );

            parent::echo_ok(array('info' => $data));
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /** 验证密码 */
    private function check_pwd() {
        try {
            $data = parent::get_post_data(array('pwd'));
            $pwd = trim($data['pwd']);

            if (empty($pwd)) throw new \Exception('密码为空');
            $pwd = md5($this->com_id.'-'.$this->user_id.'-'.$pwd);

            $info = g('api_fingerprint') -> get_user_fingerprint($this->com_id, $this->user_id);
            if (!$info || $pwd != $info['pwd']) throw new \Exception('密码错误');

            parent::echo_ok();
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /** 验证指纹签名 */
    private function verify_sign() {
        try {
            $this -> log_d('开始验证指纹签名');
            $need = array('json_str', 'sign_str');
            $data = parent::get_post_data($need);

            $info = g('api_fingerprint') -> get_user_fingerprint($this->com_id, $this->user_id);
            $info = json_decode($info['fingerprint'], TRUE);

            $json_str = json_decode($data['json_str'], TRUE);

            if ($info['cpu_id'] != $json_str['cpu_id']) {
                throw new \Exception('指纹有误，如更换了手机，请及时修改指纹');
            }

            $ret = g('api_fingerprint') -> verify_soter_sign($data['json_str'], $data['sign_str']);

            !is_array($ret) && $ret = json_decode($ret, TRUE);
            if (!isset($ret['is_ok']) || $ret['is_ok'] != 'ok') {
                throw new \Exception('指纹验证失败');
            }
            parent::echo_ok(array('info'=>$ret));
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

//-----------------------内部调用-------------------------

}
// end of file