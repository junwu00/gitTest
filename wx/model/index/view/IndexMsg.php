<?php
/**
 * 流程应用消息接口
 *
 * @author chenyihao
 * @create 2016-08-10
 */

namespace model\index\view;

class IndexMsg extends \model\api\view\ViewBase {

	/** 服务类 */
	private $Server = '';

	public function __construct($log_pre_str='') {
		$this -> Server = g('ser_index_msg');
		parent::__construct('index',$log_pre_str);
	}

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
		switch($cmd) {
			case '101' : {
				// 获取我的消息
				$this -> get_msg();
				break;
			}
			case '102' : {
				// 搜索我的消息
				$this -> search_msg();
				break;
			}
			case '103' : {
				// 获取指定应用的消息
				$this -> get_by_app();
				break;
			}
			case '104' : {
				//获取轮询URL
				$this -> get_url();
			}
			case '105' : {
				// 增加消息员工映射关系
				$this -> add_map();
				break;
			}
			case '106' : {
				//获取消息(新)
				$this -> get_msg_news();
				break;
			}
			case '107' : {
				//消息阅读
				$this -> news_state_change();
				break;
			}
			case '108' : {
				//消息删除
				$this -> delete_news();
				break;
			}
			case '109' : {
				//消息阅读
				$this -> get_app_list();
				break;
			}
            case '206' : {
                //消息详情
                $this -> detail_msg_news();
                break;
            }
			default : {
				//非法的请求
				parent::echo_busy();
				break;
			}
		}
	}


	/** 获取轮询URL */
	private function get_url(){
		$url = parent::get_consume_url();
		$data = array(
				'url' => $url
			);
		parent::echo_ok($data);
	}

	/** 获取推送消息 */
	private function get_msg(){
		try{
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$msg = $this -> Server -> get_msg($com_id, $user_id);
			$data = array(
					'msg' => $msg
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取推送消息 */
	private function get_msg_news(){
		try{
			$fields = array("start_id");
			$data = parent::get_post_data($fields);

			$start_id = !empty($data['start_id']) ? (int)$data['start_id'] : 0;

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];

			$msg = $this -> Server -> get_news_list($com_id, $user_id, $start_id);
			$data = array(
					'data' => $msg
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

    /**
     * 获取消息详情
     */
	private function detail_msg_news(){
        try{
            $fields = array("msg_id");
            $data = parent::get_post_data($fields);

            $msg_id = $data['msg_id'];

            $user_id = $_SESSION[SESSION_VISIT_USER_ID];
            $com_id = $_SESSION[SESSION_VISIT_COM_ID];

            $msg = $this -> Server -> get_news_detail($com_id, $user_id, $msg_id);
            $data = array(
                'data' => $msg
            );
            parent::echo_ok($data);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }


	/** 将消息置为阅读状态 */
	private function news_state_change(){
		try{
			$fields = array("msg_id");
			$data = parent::get_post_data($fields);
			$msg_id = $data['msg_id'];

			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];

			$this -> Server -> change_news_state($com_id, $user_id, $msg_id);
			parent::echo_ok();
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 将消息置为删除状态 */
	private function delete_news(){
		try{
			$fields = array("msg_id");
			$data = parent::get_post_data($fields);
			$msg_id = $data['msg_id'];

			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];

			$ret = $this -> Server -> delete_news($com_id, $user_id, $msg_id);
			if(!$ret) throw new \Exception("删除信息失败");
			parent::echo_ok();
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 获取所有消息分类 */
	private function get_app_list(){
		try{
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];

			$ret = $this -> Server -> get_app($com_id, $user_id);
			$data = array(
					'list' => $ret
				);
			parent::echo_ok($data);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 推送消息搜索 */
	private function search_msg(){
		try{
			$fields = array('key_word');
			$data = parent::get_post_data($fields);
			$key_word = $data['key_word'];
			$app_id = isset($data['app_id']) ? (int)$data['app_id'] : null;
			$type = isset($data['type']) ? (int)$data['type'] : null;

			$page = $data['page'];
			$page_size = $data['page_size'];

			$user_id = $_SESSION[SESSION_VISIT_USER_ID];

			if($app_id === -1){
				$type = -1;
			}
			$msg = $this -> Server -> search_msg($user_id, $key_word, $app_id, $type, $page, $page_size);
			parent::echo_ok($msg);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}	
	}

	/** 通过应用获取消息 */
	private function get_by_app(){
		try{
			$fields = array('app_id', 'type');
			$data = parent::get_post_data($fields);
			$app_id = (int)$data['app_id'];
			$type = (int)$data['type'];
			$start_id = isset($data['start_id']) ? (int)$data['start_id'] : 0;
			$page_size = isset($data['page_size']) ? (int)$data['page_size'] : 20;
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];
			$fields = '*';

			$msg = $this -> Server -> get_msg_by_app($user_id, $app_id, $type, $start_id, $page_size);
			$this -> Server -> change_msg_state($user_id, $app_id, $type);

			$data = array(
					'data' => $data
				);
			parent::echo_ok($msg);
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	/** 增加映射关系 */
	private function add_map(){
		try{
			$fields = array('msg_ids');
			$data = parent::get_post_data($fields);
			$message_ids = $data['msg_ids'];
			$com_id = $_SESSION[SESSION_VISIT_COM_ID];
			$user_id = $_SESSION[SESSION_VISIT_USER_ID];

			$this -> Server -> add_map($com_id, $user_id, $message_ids);
			parent::echo_ok();
		}catch(\Exception $e){
			parent::echo_exp($e);
		}
	}

	public function send_msg(){
		$id = g('pkg_val') -> get_get('id');
		empty($id) && $id = 81873;
		parent::send_news('测试title', '测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容测试内容', 'http://shp.qpic.cn/bizmp/XX3qOqvUiaqXmlTIqHawC4MPicTy7iaPNJwnEic5rdO3bFvmBCeIibJ3E8w/', 'http://www.easywork365.com/', 0, 0, 81873, array($id), array());
		parent::echo_ok();
	}

}
// end of file