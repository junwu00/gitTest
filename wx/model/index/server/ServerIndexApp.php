<?php 
/**
 * 流程应用逻辑处理类
 * @author luja
 * @create 2016-08-09
 */
namespace model\index\server;

class ServerIndexApp extends \model\api\server\ServerBase {

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	/**
	 * 表单实例状态
	 */
	/** 草稿 */
	private static $StateFormDraft = 0;
	/** 运行中 */
	private static $StateFormDone = 1;
	/** 已结束 */
	private static $StateFormFinish = 2;
	/** 终止 */
	private static $StateFormStop = 3;

	/** 流程*/
	private static $IsProc = 0;
	/** 旧版请假*/
	private static $IsRest = 1;
	/** 旧版报销*/
	private static $IsExaccount = 2;
	/** 补录*/
	private static $IsMakeup = 3;
	/** 新请假*/
	private static $IsNewRest = 4;
	/** 外勤*/
	private static $IsLegwork = 5;
	/** 新版报销*/
	private static $IsNewExaccount = 6;

	public function __construct($log_pre_str='') {
		parent::__construct('index', $log_pre_str);

	} 

	/**
	 * 获取可使用表单
	 * @param  integer $page      
	 * @param  integer $page_size 
	 * @return array
	 */
	public function get_form_list($page=1, $page_size=9) {
		/** 获取员工部门id树 */
		$user = g('dao_user') -> get_by_id($this -> user_id, 'dept_tree');
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$dept_ids = array();
		foreach ($dept_tree as $tree) {
			$dept_ids = array_merge($dept_ids, $tree);
		}unset($tree);
		$dept_ids = array_unique($dept_ids);

		$user_group = g('dao_group') -> get_by_user_id($this -> com_id, $this -> user_id);
		$group_ids = array_column($user_group, 'id');

		// 已安装的应用
		$procs = array();
		$apps = $this -> get_install_app();
		foreach ($apps as $app) {
			if ($app == 'process') {
				$procs[] = self::$IsProc;
			} elseif ($app == 'workshift') {
				array_push($procs, self::$IsMakeup, self::$IsNewRest, self::$IsLegwork);
			} elseif ($app == 'exaccount') {
				$procs[] = self::$IsNewExaccount;
			}
		}unset($app);
		
		$fields = 'id, form_name, form_describe, icon, is_other_proc';
		$ret = g('mv_form') -> get_publish_form($this -> com_id, $dept_ids, $group_ids, $page, $page_size, $fields, $procs,[$this->user_id]);
		if (!empty($ret['data'])) {
			// 处理NULL值问题
			foreach ($ret['data'] as &$form) {
				if ($form['form_describe'] === NULL) {
					$form['form_describe'] = '';
				}
			}unset($form);
		}

		return $ret;
	}

	/** 获取已经安装应用 */
	public function get_install_app(){
		$com_id = $_SESSION[SESSION_VISIT_COM_ID];
		$fields = 'app_id';
		$auth_app = g('dao_com_child_app') -> get_auth_app($com_id, $fields);
		$app_ids = array();
		foreach ($auth_app as $val) {
			$app_ids[] = $val['app_id'];
		}unset($val);

		$app_names = array();
		$app_info = load_config('model/api/app');
		foreach ($app_info as $key => $app) {
			if($app['is_child'] && in_array($app['id'], $app_ids)){
				$app_names[] = $key;
			}
		}unset($app);
		return $app_names;
	}

}
//end of file