<?php 
/**
 * 我的页面逻辑处理类
 * @author luja
 * @create 2016-08-10
 */
namespace model\index\server;

class ServerIndexMine extends \model\api\server\ServerBase {

	public function __construct($log_pre_str='') {
		parent::__construct('index', $log_pre_str);

	} 

	/**
	 * 根据ID获取个人信息
	 * @param  [type] $user_id 
	 * @return [type]          
	 */
	public function get_by_id($user_id) {
		$table = 'sc_user su, sc_company sc ';
		$cond = array(
			'su.id=' => $user_id,
			'su.com_id=' => $this -> com_id,
			'^su.com_id=' => 'sc.id'
			);

		$fields = array(
			'su.id', 'su.name', 'su.pic_url', 'su.acct', 'su.dept_list', 'su.position', 'su.mobile', 
			'su.gender', 'su.weixin_id', 'su.state' ,'su.email', 'sc.name com_name'
			);
		$fields = implode(',', $fields);
		$ret = g('dao_index_mine') -> get_user($table, $fields, $cond);
		if (!$ret) throw new \Exception('获取个人信息失败！');
		if ($ret[0]['state'] == 0) {
			throw new \Exception('账号已被删除！');
		} elseif ($ret[0]['state'] == 2) {
			throw new \Exception('账号已被冻结！');
		}
		unset($ret[0]['state']);

		//格式化部门
		$this -> dept_format($ret);	

		return $ret[0];
	}


	/**
	 * 保存个人信息
	 * @param  [type] $user_id 
	 * @param  string $gender  性别
	 * @param  string $mobile  手机
	 * @param  string $email   
	 * @return [type]          
	 */
	public function update_mine_info($user_id, $gender=NULL, $mobile='', $email='') {
		$cond = array(
			'id=' => $user_id,
			'com_id=' => $this -> com_id,
			'state!=' => 0
			);

		$data = array(
			'update_time' => time()
			);

		$gender >0 && $gender <= 2 && $data['gender'] = $gender;
		!empty($mobile) && $data['mobile'] = $mobile;
		!empty($email) && $data['email'] = $email;

		$ret = g('dao_index_mine') -> update_user('sc_user', $cond, $data);
		if (!$ret) throw new \Exception('更新个人信息失败！');
		return $ret;
	}


//------------------------------------内部实现-------------------------------

	/**
	 * 将结果集中的dept_list格式化为中文格式 => dept_str
	 *
	 * @access private
	 * @param array 包含dept_list的结果集
	 * @return array
	 */
	private function dept_format(array &$data) {
		$dept_ids = array();
		foreach ($data as $val) {
			$dept_list = json_decode($val['dept_list'], TRUE);

			if (is_array($dept_list)) {
				foreach ($dept_list as $cval) {
					$dept_ids[] = intval($cval);
				}unset($cval);
			}
		}unset($val);

		if (!empty($dept_ids)) {
			$rets = g('dao_dept') -> list_by_ids($dept_ids, 'id,name');
			if (!empty($rets) && is_array($rets)) {
				$dept_arr = array();
				foreach ($rets as $key => $val) {
					$dept_arr[$val['id']] = $val['name'];
				}unset($val);
				
				foreach ($data as &$val) {
					$dept_list = json_decode($val['dept_list'], TRUE);
					$val['dept_str'] = '';
					if (!is_array($dept_list)) continue;

					$dept_str = array();
					foreach ($dept_list as $cval) {
						isset($dept_arr[$cval]) && $dept_str[] = $dept_arr[$cval];
					}unset($cval);

					!empty($dept_str) && $val['dept_str'] = implode('，', $dept_str);
					unset($val['dept_list']);
				}unset($val);
			}			
		}
	}

	
}
//end of file