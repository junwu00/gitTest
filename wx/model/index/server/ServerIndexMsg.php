<?php 
/**
 * 流程应用逻辑处理类
 * @author chenyihao
 * @create 2016-08-10
 */
namespace model\index\server;

class ServerIndexMsg extends \model\api\server\ServerBase {

	/** 消息数据类 */
	private $Dao_News = '';
	/** 消息员工映射数据类 */
	private $Dao_News_User = '';

	/** 禁用 0 */
	private static $StateOff = 0;
	/** 启用 1 */
	private static $StateOn = 1;

	//报表名称
	private static $Privilege_Name = 'REPORT';

	/** 考勤管理应用ID */
	private static $Workshift_App_Id = 25;
	/** //消息排序 */
	private static $msg_queue = array(
		'A' => '待办',
		'B' => '发起',
		'C' => '知会',
		'D' => '报表',
		'E' => '排班',
		'F' => '定位',
		'G' => '小助手'
		);

	public function __construct($log_pre_str='') {
		parent::__construct('index', $log_pre_str);

		$this -> Dao_News = g('dao_index_news');
		$this -> Dao_News_User = g('dao_index_news_user');
	}

	/** 获取指定员工的消息 */
	public function get_msg($com_id, $user_id){
		$fields = '*';
		//获取每个应用的最新一条消息，并对应消息阅读状态
		$map_list = $this -> Dao_News_User -> get_app_first_msg($com_id, $user_id);
		$news_state = array();
		foreach($map_list as $val) {
			$news_state[$val['news_id']] = $val['count'];
		}unset($val);

		$msg_config = load_config('model/index/msg');

		/* 检查是否有润恒报表权限 */
		if(!$this -> check_runheng($com_id)){
			$msg_config[0][1]['hide'] = 1;
		}

		/* 检查是否有报表权限 */
		if(!$this -> check_report($com_id, $user_id)){
			$msg_config[0][2]['hide'] = 1;
		}

		$platid = get_platid();
		if($platid <= 3){
			/* 检查是否有报表权限 */
			$clock_state = $this -> check_clock($com_id, $user_id, self::$Workshift_App_Id);
			if(!$clock_state){
				$msg_config[25][1]['hide'] = 1;
				$msg_config[25][2]['hide'] = 1;
			}else if($clock_state == 1){
				$msg_config[25][2]['hide'] = 1;
			}else if($clock_state == 2){
				$msg_config[25][1]['hide'] = 1;
			}
		}else{
			$msg_config[25][1]['hide'] = 1;
			$msg_config[25][2]['hide'] = 1;
		}

		$ids = array_keys($news_state);
		if(!empty($ids)){
			$news = $this -> Dao_News -> get_by_ids($ids, 'id, content title, app_id, type, success_time time');
		}else{
			$news = array();
		}
		$app_info = array();
		foreach($news as $key =>  $val) {
			$app = $msg_config[$val['app_id']][$val['type']];
			$app['count'] = (int)$news_state[$val['id']];
			$app['msg'] = array('title' => $val['title'], 'time' => $val['time']);
			$app_info[$val['app_id'].'_'.$val['type']] = $app;
		}unset($val);

		foreach ($msg_config as $app => $a) {
			foreach ($a as $type => $t) {
				if(!isset($app_info[$app.'_'.$type])){
					$t['count'] = 0;
					$t['msg'] = array();
					$app_info[$app.'_'.$type] = $t;
				}
			}unset($val);
		}unset($val);

		$tmp1 = $app_info['25_1'];
		$tmp2 = $app_info['25_2'];
		unset($app_info['25_1']);
		unset($app_info['25_2']);

		if(empty($app_info['25_3']['msg'])){
			$app_info['25_3']['hide'] = 1;
		}
		if(empty($app_info['25_4']['msg'])){
			$app_info['25_4']['hide'] = 1;
		}

		array_unshift($app_info, $tmp1);
		array_unshift($app_info, $tmp2);

		foreach ($app_info as $key => $app) {
			if(count($app) == 2){
				unset($app_info[$key]);
			}
		}

		$app_info = array_values($app_info);
		return $app_info;
	}

	/** 获取指定应用的消息 */
	public function get_msg_by_app($user_id, $app_id, $type,  $start_id =0, $page_size=20){
		$fields='*';
		$data = $this -> Dao_News_User -> get_app_by_user_id($user_id, $app_id, $type, $fields, $start_id, $page_size);
		$news_list = $data['data'];
		$news_ids = array();
		$news_state = array();
		foreach($news_list as $val) {
			$news_ids[] = $val['news_id'];
			$news_state[$val['news_id']] = $val['state'];
		}unset($val);
		$news = array();
		if(!empty($news_ids)){
			$news = $this -> Dao_News -> get_by_ids($news_ids, 'id, title, user_id, user_pic, pic, content, url, create_time time');
		}
		foreach ($news  as &$val) {
			$val['state'] = $news_state[$val['id']];
		}unset($val);
		$data['data'] = $news;
		return $data;
	}

	/** 搜索信息 */
	public function search_msg($user_id, $key_word, $app_id=null, $type=null, $page, $page_size){
		if(is_null($app_id) || is_null($type)){
			$news = $this -> Dao_News -> get_app_news($user_id, $key_word);
			$count = 0;
			$state = 2;
		}else{
			if($app_id == -1 && $type == -1){
				$app_id = null;
				$type = null;
				$state = 0;	
			}else{
				$state = null;	
			}
			$data = $this -> Dao_News -> search_news($user_id, $key_word, $app_id, $type, $state, $page, $page_size);
			$news = $data['data'];
			$count = $data['count'];
		}
		$msg_config = load_config('model/index/msg');
		$news_list = array();
		if(!empty($news)){
			if($state !== 0){
				foreach ($news as $val) {
					$app_id = $val['app_id'];
					$type = $val['type'];
					if(!isset($news_list[$app_id.'-'.$type])){
						if(!isset($msg_config[$app_id][$type])) continue;
						$news_list[$app_id.'-'.$type] = $msg_config[$app_id][$type];
						$news_list[$app_id.'-'.$type]['list'] = array();
					}
					$val['time'] = $val['news_time'];
					unset($val['news_time']);
					unset($val['app_id']);
					unset($val['type']);

					$news_list[$app_id.'-'.$type]['list'][] = $val;
				}unset($val);

				//按特定顺序排序
				$tmp_list = array();
				$i=1;
				foreach ($news_list as $new) {
					$key = array_search($new['simple_name'], self::$msg_queue);
					if ($key) {
						$tmp_list[$key] = $new;
					} else {
						$tmp_list['Z'.$i++] = $new;
					}
				}unset($new);
				ksort($tmp_list);
				$news_list = array_values($tmp_list);
			}else{
				$news_list[] = array(
						"name" => "未阅读",
			            "simple_name" => "未阅读",
			            "icon" => "",
			            "app_id" => -1,
			            "type" => -1,
			            "list" => $news
					);
			}
		}
		$data = array();
		$data['data'] = $news_list;
		!empty($data['count']) && $data['count'] = $count;
		return $data;
	}

	public function add_map($com_id, $user_id, $news_message_ids){
		$news_list = $this -> Dao_News -> get_by_message_ids($news_message_ids, 'id, create_time, app_id, type');
		if(empty($news_list)) return;
		$this -> Dao_News_User -> add_map($com_id, $user_id, $news_list);
	}

//----------------------------------------新的消息功能实现----------------------------

	/** 获取我的信息列表（新） */
	public function get_news_list($com_id, $user_id, $start_id){
		$data = array();
		if(empty($start_id)){
			$button = $this -> get_news_button($com_id, $user_id);
			if(!empty($button)){
				$data["button"] = $button;
			}
		}
		$data['list'] = $this -> Dao_News_User -> get_news_list($com_id, $user_id, $start_id);
		$data['unsee_count'] = $this -> Dao_News_User -> get_unsee_count($com_id, $user_id);
		return $data;
	}

    /** 获取我的信息详情（新） */
    public function get_news_detail($com_id, $user_id, $msg_id){

        $data = $this -> Dao_News_User -> get_news_detail($com_id, $user_id, $msg_id);

        return isset($data[0])?$data[0]:[];
    }

	/** 消息阅读状态 */
	public function change_news_state($com_id, $user_id, $msg_id){
		return $this -> Dao_News_User -> update_news_state($com_id, $user_id, $msg_id);
	}

	/** 删除消息 */
	public function delete_news($com_id, $user_id, $msg_id){
		return $this -> Dao_News_User -> delete_news($com_id, $user_id, $msg_id);
	}

	public function get_app($com_id, $user_id){
		$app_list = $this -> Dao_News_User -> get_app_group($com_id, $user_id);
		$msg_config = load_config('model/index/msg');
		$app_info = array();
		// $app_info[] = array(
		// 		'name' => "未阅读",
		// 		'app_id' => -1,
		// 		'type' => -1,
		// 	);
		$i=1;
		foreach ($app_list as $app) {
			if(isset($msg_config[$app['app_id']][$app['type']])){
				$key = array_search($msg_config[$app['app_id']][$app['type']]['simple_name'], self::$msg_queue);
				$tmp = array(
					'name' => $msg_config[$app['app_id']][$app['type']]['simple_name'],
					'app_id' => $msg_config[$app['app_id']][$app['type']]['app_id'],
					'type' => $msg_config[$app['app_id']][$app['type']]['type'],
					);
				if ($key) {
					$app_info[$key] = $tmp;
				} else {
					$app_info['Z'.$i++] = $tmp;
				}
			}
		}unset($app);
		ksort($app_info);
		return array_values($app_info);
	}


//-----------------------------------内部实现---------------------------
	
	private function get_news_button($com_id, $user_id){
		$button = array();
		$platid = get_platid();
		parent::log_i("设备类型：".$platid);
		if($platid <= 3 && $this->check_workshift($com_id)){
			$msg_config = load_config('model/index/msg');

			/* 检查是否有打卡/定位权限 */
			$clock_state = $this -> check_clock($com_id, $user_id, self::$Workshift_App_Id);
			parent::log_i("考勤状态：".$clock_state);
			if($clock_state == 1){
				$clock_check = $msg_config[25][1];
				$button = $clock_check;
			}else if($clock_state == 2){
				$clock_legwork = $msg_config[25][2];
				$button = $clock_legwork;
			}
		}
		return $button;
	}

	/** 检查考勤管理菜单状态 */
	private function check_clock($com_id, $user_id, $app_id){
		$app_info = g('dao_com_child_app') -> get_by_app_id($com_id, $app_id, 'state');
		if(!empty($app_info) && $app_info['state'] == 1){
			$legwork = g('ws_legwork') -> get_record($com_id, $user_id, $fields = 'l.*, f.state');
			if(!empty($legwork) && $legwork['state'] != 0){
				return 2;
			}
			$workshift = g('ws_workshift') -> get_workshift_by_user_id($com_id, $user_id);
			if(!empty($workshift)){
				return 1;
			}
		}
		return 0;
	}

	/** 检查是否有润衡报表权限 */
	private function check_runheng($com_id, $fields='id'){
		$cond = array(
				'com_id=' => $com_id
			);
		$table = 'runheng_acct';
		return g('pkg_db') -> select_one($table, $fields, $cond);
	}

	/** 检查是否有报表权限 */
	private function check_report($com_id, $user_id){
		$privilege = g('dao_service') -> check_privilege($com_id, self::$Privilege_Name);
		$vip_info = g('dao_vip') -> get_vip_by_com($com_id, 'is_vip, is_svip, past_time');
		if($vip_info['is_vip']==0 || ($vip_info['is_vip']==1 && $vip_info['past_time'] < time())){
			if(!$privilege){
				return false;
			}
		}

		$all_dept = $this -> get_user_dept();
		$all_group = $this -> get_user_group();

		$cond = array(
				'state=' => 1
			);
		$cond['__OR'] = array();
		$cond['__OR']['visit_user LIKE '] = '%"' .$this -> user_id. '"%'; 
		foreach ($all_dept as $key => $dept) {
			$cond['__OR']['__'.$key.'__visit_dept LIKE '] = '%"'.$dept.'"%';
		}unset($dept);
		foreach ($all_group as $key => $group) {
			$cond['__OR']['__'.$key.'__visit_group LIKE '] = '%"' .$group. '"%';
		}unset($group);

		$fields = 'id';
		$ret =  g('api_report') -> get_report_list($com_id, $cond, $fields, 1, 1);
		if(!$ret){
			return false;
		}
		return TRUE;
	}

	/** 获取当前用户的所有部门 */
	private function get_user_dept(){
		$user = g('dao_user') -> get_by_id($this -> user_id, ' dept_tree ');
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$all_dept = array();
		foreach ($dept_tree as $dept) {
			$all_dept = array_merge($all_dept, $dept);
		}unset($dept);
		$all_dept = array_unique($all_dept);
		return $all_dept;
	}

	/** 获取当前用户的所在分组 */
	private function get_user_group() {
		$ids = array();
		$groups = g('dao_group') -> get_by_user_id($this -> com_id, $this -> user_id, 'id');
		$groups && $ids = array_column($groups, 'id');
		
		return $ids;
	}

	/** 检查考勤应用权限 */
	private function check_workshift($com_id) {
		$app_config = load_config("model/api/appset");
		if ($app_config[self::$Workshift_App_Id]['open_time'] < time()) {
			//应用已开放
			return true;
		}
		$vip = g('dao_vip') -> get_vip_by_com($com_id);
		$app_key = json_decode($vip['app_key'], true);
		empty($app_key) && $app_key = array();
		if ($vip['is_vip'] == 1 && $vip['past_time'] > time()) {
			//vip用户
			return true;
		}

		if ($vip['is_svip'] && in_array($app_config[self::$Workshift_App_Id]['key'], $app_key)) {
			return true;
		}

		return false;
	}


}
//end of file