<?php
/**
 * 消息的数据库操作类
 * @author chenyihao
 * @create 2016-08-10
 */
namespace model\index\dao;

class DaoIndexNewsUser extends \model\api\dao\DaoBase {
	
	private static $Table = 'sc_news_user_map';

	private static $News_Table = 'sc_news';

	/** 数据状态 正常 */
	private static $State_On = 1;
	/** 数据状态 删除 */
	private static $State_Off = 0;

	/** 阅读状态 已读 */
	private static $State_Seed= 1;
	/** 阅读状态 未读 */
	private static $State_Unsee = 0;

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		parent::set_table(self::$Table);
	}

    /** 消息详情 */
    public function get_news_detail($com_id, $user_id, $msg_id){

        try {
            $this -> batch_repaire($com_id, $user_id);
        } catch (\Exception $e) {
            to_log(MAIN_LOG_ERROR, '', '添加消息失败：' . $e -> getMessage());
            to_log(MAIN_LOG_ERROR, '', '添加消息失败：' . $e -> getTraceAsString());
        }

        $cond = array(
            't.com_id=' => $com_id,
            't.user_id=' => $user_id,
            't.info_state=' => self::$State_On,
            'n.msg_id='=>$msg_id
        );

        $order = " ORDER BY t.news_time desc ";
        $fields = "t.id, t.app_id, t.type, t.state, n.msg_id, n.title,n.user_pic, n.pic, n.content,t.news_time time, n.url";
        $table = self::$Table ." t LEFT JOIN ". self::$News_Table .' n ON t.news_id = n.id ';

        $ret = g('pkg_db') -> select($table, $fields, $cond, 1, 1, "", $order);

        return $ret;
    }

	/** 消息列表 */
	public function get_news_list($com_id, $user_id, $start_id=0){
        try {
            $this -> batch_repaire($com_id, $user_id);
        } catch (\Exception $e) {
            to_log(MAIN_LOG_ERROR, '', '添加消息失败：' . $e -> getMessage());
            to_log(MAIN_LOG_ERROR, '', '添加消息失败：' . $e -> getTraceAsString());
        }

		$cond = array(
				't.com_id=' => $com_id,
				't.user_id=' => $user_id,
				't.info_state=' => self::$State_On
			);
		if(!empty($start_id)){
			$cond['t.id<'] = $start_id;
		}
		$order = " ORDER BY t.news_time desc ";
		$fields = "t.id, t.app_id, t.type, t.state, n.msg_id, n.title,n.user_pic, n.pic, n.content,t.news_time time, n.url";
		$table = self::$Table ." t LEFT JOIN ". self::$News_Table .' n ON t.news_id = n.id ';
		return g('pkg_db') -> select($table, $fields, $cond, 1, 20, "", $order);
	}

	/** 获取未读消息数量 */
	public function get_unsee_count($com_id, $user_id){
		$cond = array(
				'com_id=' => $com_id,
				'user_id=' => $user_id,
				'state=' => self::$State_Unsee,
				'info_state=' => self::$State_On
			);
		$fields = "count(id) count";
		$ret = g('pkg_db') -> select(self::$Table, $fields, $cond, 1, 1);
		return $ret ? $ret[0]['count'] : 0;
	}

	/** 更新消息阅读状态 */
	public function update_news_state($com_id, $user_id, $msg_id){
		$state_unsee = self::$State_Seed;
		$sql = "update sc_news_user_map m INNER JOIN sc_news n ON m.news_id = n.id and n.msg_id = '{$msg_id}' SET m.state = {$state_unsee} where m.user_id = {$user_id} and m.com_id ={$com_id}";
		return g('pkg_db') -> exec($sql);
	}

	/** 将消息置为删除状态 */
	public function delete_news($com_id, $user_id, $msg_id){
		$state_off = self::$State_Off;
		$sql = "update sc_news_user_map m INNER JOIN sc_news n ON m.news_id = n.id and n.msg_id = '{$msg_id}' SET m.info_state = {$state_off} where m.user_id = {$user_id} and m.com_id ={$com_id}";
		return g('pkg_db') -> exec($sql);
	}

	/** 获取消息所有分类 */
	public function get_app_group($com_id, $user_id){
		$cond = array(
				'com_id=' => $com_id,
				'user_id=' => $user_id,
				'info_state=' => self::$State_On
			);
		$group = " GROUP BY app_id, type ";
		$order = " ORDER BY app_id ASC, type ASC";
		$fields = "app_id, type";
		return  g('pkg_db') -> select(self::$Table, $fields, $cond, 0, 0, $group);
	}

	/** 根据user_id获取 */
	public function get_app_first_msg($com_id, $user_id){
		//获取每个类型的最新一条消息
		$sql = "SELECT news_id, type, app_id, state, news_time, count(*)-sum(state) as count FROM (SELECT news_id, type, app_id, state, news_time FROM sc_news_user_map WHERE user_id = {$user_id} and com_id = {$com_id} ORDER BY news_time desc ) t GROUP BY app_id, type ";
		$news_list = $this -> query($sql);
		if(empty($news_list)){
			return array();
		}
		return $news_list;
	}

	public function update_state($user_id, $app_id, $type){
		$cond = array(
				'user_id=' => $user_id,
				'app_id=' => $app_id,
				'type=' => $type,
				'state=' => self::$State_Unsee
			);
		$data = array(
				'state' => self::$State_Seed
			);
		return $this -> update_by_cond($cond, $data, "", FALSE);
	}

	public function get_app_by_user_id($user_id, $app_id, $type, $fields, $start_id, $page_size, $page = 1){
		$cond = array(
			'user_id=' => $user_id, 
			'app_id=' => $app_id,
			'type=' => $type,
		);	
		if(!empty($start_id)){
			$cond['news_id <'] = $start_id;
		}
		$order = 'order by news_time desc';
		return $this -> list_by_page($cond, $fields, $page, $page_size, $order, true);
	}

	/** 批量添加映射关系 */
	public function add_map($com_id, $user_id, $news_data){
		$data = array();		
		$now = time();
		$fields = array('com_id', 'user_id', 'news_id', 'app_id', 'type', 'news_time', 'state', 'info_state', 'create_time');
		foreach ($news_data as $val) {
			$news = array(
					'com_id' => $com_id,
					'user_id' => $user_id,
					'news_id' => $val['id'],
					'app_id' => $val['app_id'],
					'type' => $val['type'],
					'news_time' => $val['create_time'],
					'state' => self::$State_Unsee,
					'info_state' => self::$State_On,
					'create_time' => $now,
				);
			$data[] = $news;
		}unset($val);
		return g('pkg_db') -> batch_insert(self::$Table, $fields, $data);
	}

	// 补充消息功能丢失的数据
	private function batch_repaire($com_id, $user_id) {
        $com_id = intval($com_id);
        $user_id = intval($user_id);
        $receive_users = '["' . $user_id . '"]';

        $last_news = $this -> query(" SELECT id FROM sc_news WHERE com_id={$com_id} AND receive_users='{$receive_users}' ORDER BY id DESC LIMIT 1 ");
        if (empty($last_news))                                                              return;     // 无消息，跳过

        $last_news_map = $this -> query(" SELECT news_id FROM sc_news_user_map WHERE com_id={$com_id} AND user_id={$user_id} ORDER BY id DESC LIMIT 1 ");
        if (!empty($last_news_map) && $last_news_map[0]['news_id'] == $last_news[0]['id'])  return;     // 若当前已是最新消息，跳过

        // 从最后一条有生成映射关系的消息开始取
        $start_time = strtotime(date('Ymd')) - (60 * 24 * 3600);        // 只取近两个月不正常的数据
        $fields = 'n.id, n.app_id, n.type, n.create_time';
        $tables = self::$News_Table . ' n LEFT JOIN ' . self::$Table . ' m ON(m.news_id = n.id) ';
        $sql = <<<EOF
        SELECT {$fields} FROM {$tables}
        WHERE n.com_id = {$com_id}
            AND n.receive_users = '{$receive_users}'
            AND n.state = 1
            AND m.id IS NULL
            AND n.app_id != 0
            AND n.id > {$last_news_map[0]['news_id']}
            AND n.create_time>={$start_time}
        ORDER BY n.id
        LIMIT 20
EOF;
        $not_found = $this -> query($sql, []);
        if (!empty($not_found)) {
            $this -> add_map($com_id, $user_id, $not_found);
        }
    }

}

/* End of this file */