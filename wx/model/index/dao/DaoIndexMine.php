<?php
/**
 * 数据库操作类
 * @author Luja
 * @create 2016-08-10
 */
namespace model\index\dao;

class DaoIndexMine extends \model\api\dao\DaoBase {


	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * 根据条件获取信息
	 * @param  [type] $table  
	 * @param  string $fields 
	 * @param  array  $cond   
	 * @return [type]         
	 */
	public function get_user($table, $fields='', $cond=array()) {
		if (empty($table) || empty($fields) || empty($cond)) return FALSE;

		return g('pkg_db') -> select($table, $fields, $cond);
	}

	/**
	 * 更新信息
	 * @param  [type] $table 
	 * @param  array  $cond  
	 * @param  array  $data  
	 * @return [type]        
	 */
	public function update_user($table, $cond=array(), $data=array()) {
		if (empty($table) || empty($data) || empty($cond)) return FALSE;
		
		return g('pkg_db') -> update($table, $cond, $data);
	}


}

/* End of this file */