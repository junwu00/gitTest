<?php
/**
 * 消息的数据库操作类
 * @author chenyihao
 * @create 2016-08-10
 */
namespace model\index\dao;

class DaoIndexNews extends \model\api\dao\DaoBase {
	
	private static $Table = 'sc_news';

	/**
	 * 构造函数
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		parent::set_table(self::$Table);
	}

	/** 根据ID数组获取消息 */
	public function get_by_ids($ids, $fields = '*'){
		$cond = array(
				'id IN' => $ids,
			);
		$order = 'order by create_time desc';
		return $this -> list_by_page($cond, $fields, 0, 0, $order);
	}

	/**
	 * 搜索消息
	 * @param   $user_id 指定员工
	 * @param   $key_word 关键字 为空搜索所有
	 * @param   $app_id 指定应用
	 * @param   $type 指定类型
	 * @param   $state 指定状态 0 未阅读 1 已阅读 
	 * @param   $page
	 * @param   $page_size
	 * @return 
	 */
	public function search_news($user_id, $key_word, $app_id=null, $type=null, $state=null, $page, $page_size){
		$cond = array(
				'news_map.user_id=' => $user_id,
			);
		if(!is_null($app_id)){
			$cond['news_map.app_id='] = $app_id;
		}
		if(!is_null($type)){
			$cond['news_map.type='] = $type;
		}
		if(!is_null($state)){
			$cond['news_map.state='] = $state;
		}

		if(!empty($key_word)){
			$cond["__OR"] = array(
						'news.title LIKE' => "%".$key_word."%",
						'news.content LIKE' => "%".$key_word."%",
					);
		}
		$fields = 'news.id, news.url, news.title, news.app_id, news.type, news.content, news.pic, news.user_pic, news_map.news_time';
		$table = ' sc_news news LEFT JOIN sc_news_user_map news_map ON news.id = news_map.news_id and news_map.info_state = 1 ';
		$order = ' order by news_map.news_time desc ';
		return  g('pkg_db') -> select($table, $fields, $cond, $page, $page_size, '', $order, true);
	}

	/** 搜索消息，获取每个消息项的前两条消息 */
	public function get_app_news($user_id, $key_word, $limit = 3){
		$sql = <<<EOF
		select t1.app_id, t1.type, t1.id, t1.title, t1.url, t1.pic, t1.user_pic, t1.content, t1.news_time
		from 
		(	
			select news.app_id, news.type, news.id, news.title, news.url, news.pic, news.user_pic, news.content, news_map.news_time
			from sc_news_user_map news_map LEFT JOIN sc_news news ON news.id = news_map.news_id
			where news_map.user_id = {$user_id} and (news.title like '%{$key_word}%' OR news.content like '%{$key_word}%')
			ORDER BY news_map.news_time desc
		) t1 LEFT JOIN 
		(
			select news.app_id, news.type, news.id, news.title, news.url, news.pic, news.user_pic, news.content, news_map.news_time
			from sc_news_user_map news_map LEFT JOIN sc_news news ON news.id = news_map.news_id
			where news_map.user_id = {$user_id} and ( news.title like '%{$key_word}%' OR news.content like '%{$key_word}%')
			ORDER BY news_map.news_time desc
		) t2 on t1.app_id = t2.app_id and t1.type = t2.type and t1.news_time <= t2.news_time
		GROUP BY app_id , type, news_time
		HAVING count(t2.news_time) <= {$limit}
EOF;
		$ret = g('pkg_db') -> query($sql);
		return $ret;
	}

	/** 根据messageId来获取消息 */
	public function get_by_message_ids($message_ids, $fields = '*'){
		$cond = array(
				'msg_id IN' => $message_ids,
			);
		$order = 'order by create_time asc';
		return $this -> list_by_page($cond, $fields, 0, 0, $order);
	}

}

/* End of this file */