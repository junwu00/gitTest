<?php
/**
 * 模块单入口文件
 */

session_start();

//加载必要的配置文件
$mod_dir = dirname(__FILE__);												//模块的根路径
$mod_conf = load_config('model/weixin_entry/model');						//模块信息配置文件
load_config('model/api/classmap');											//引入公共的类映射配置文件
load_config('model/weixin_entry/classmap');									//引入模块内类映射配置文件

//当前模块内逻辑
$mod = 'entry';																//固定以Entry为入口
$act = g('pkg_val') -> get_get('a');
empty($act) && $act = 'index';

$mod_file = $mod_dir . DS . 'server' . DS . ucwords($mod) . '.php';
if (!file_exists($mod_file)) {
	g('mod_notice') -> error('页面不存在');
}
include_once ($mod_file);

$mod_cls = "\\model\\{$mod_conf['name']}\\server\\".ucwords($mod);
if (class_exists($mod_cls, FALSE) && method_exists($mod_cls, $act)) {
	g('mod_' . $mod) -> $act();	//处理业务逻辑

} else {
	g('mod_notice') -> error('页面不存在');
}

// end of file