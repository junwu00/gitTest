<?php
/**
 * 微信请求中转处理
 * 
 * @author yangpz
 * @date 2016-02-17
 *
 */
namespace model\weixin_entry\server;

class Entry extends Base {
	
	private $token;
	private $suite;
	private $corpid;
	
	private $service;
	
	public function __construct($log_pre_str='') {
		$this -> _check_token();
		$service_conf = array(
			'token'		=> $this -> token,
			'suite'		=> $this -> suite,
			'corpid'	=> $this -> corpid
		);
		$this -> service = g('mod_service', array($service_conf, $log_pre_str));
		parent::__construct($log_pre_str);
	}
	
	/**
	 * 接收消息，验证接入
	 */
	public function index() {
		$this -> service -> verify_token();
		
		if (isset($_GET["echostr"])) {	//接入验证
			echo $this -> service -> verify_signature();
			
		} else {						//消息接收及回复
			$this -> service -> do_reply();	
		}
	}
	
	/**
	 * 用户进行二次验证
	 */
	public function user_sec_verify() {}
	
	//-------------------------内部实现---------------------------------
	
	/** 验证请求必带参数并初始化 */
	private function _check_token() {
		if (!isset($_GET['token']) || empty($_GET['token'])) {
			throw new \Exception('Token未找到');
		}
		if (!isset($_GET['suite']) || empty($_GET['suite'])) {
			throw new \Exception('Suite未找到');
		}
		if (!isset($_GET['corp']) || empty($_GET['corp'])) {
			throw new \Exception('Corp未找到');
		}
		
		$this -> token 	= g('pkg_val') -> get_get('token');
		$this -> suite 	= g('pkg_val') -> get_get('suite');
		$this -> corpid	= g('pkg_val') -> get_get('corp');
		parent::log_d("当前TOKEN={$this -> token}, SUITE={$this -> suite}, CORPID={$this -> corpid}");
	}
	
}

// end of file