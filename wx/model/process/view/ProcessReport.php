<?php
/**
 * 流程报表接口ajax交互类
 *
 * @author luja
 * @create 2016-10-13
 */

namespace model\process\view;

use model\api\dao\FormAnalysisReport;
use model\api\server\ServerMedia;
use model\finstat\server\ServerFinstat;
use package\excel\Excel;
use package\excel\ExcelQy;

class ProcessReport extends \model\api\view\AbsProc {

	// 错误提示码
	private $busy = 0;

	private $server = '';

	public function __construct() {
		parent::__construct('process');

		$this -> server = g('ser_process_report');

		// 未知原因（系统繁忙）
		$this -> busy = \model\api\server\Response::$Busy;
	}

    public function jump(){
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxReportList');
    }

	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)g('pkg_val') -> get_get('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				// 获取流程报表
				$this -> get_report_list();
				break;
			}

            //获取全局周报报表数据
            case 201 :{
                $this->getGlobalAnalysisReport();
                break;
            }

            //获取学校周报报表数据
            case 202 :{
                $this->getAnalysisReport();
                break;
            }
			
			default : {
				//非法的请求
				parent::echo_busy();
				break;
			}
		}
	}


//-----------------------接口内部实现-----------------------------------------------------
    /** 获取分析报告 */
    public function getGlobalAnalysisReport(){
        try{
            $id = g('pkg_val') -> get_get('id');

            $begin_time = '';
            $end_time = '';
            //判断当前人员是否在接收范围内容
            $cond = array(
                'info_state='=>1,
                'com_id='=>$this->com_id,
                '`key`='=>'global_receiver_list',
            );
            $config = g('pkg_db')->select_one("sc_form_analysis_report_config",'*',$cond);
            if(empty($config))
                throw new \Exception('无权限');

            $user_ids = json_decode($config['value'],true);
            if(!$user_ids || !in_array($this->user_id,$user_ids))
                throw new \Exception("无权限");

            $dao = new FormAnalysisReport();
            $return = [];

            $fiedls = 'ar.model_count,ar.usage_count,ar.instance_count,ar.begin_day,ar.end_day,
            rd.model_count school_model_count,rd.usage_count school_usage_count,rd.instance_count school_instance_count,
            rd.com_id, sc.name company_name';
            $tmp_data = $dao->getReport('',$id,$fiedls);

            if($tmp_data){
                $begin_time = strtotime($tmp_data[0]['begin_day']);
                $end_time = strtotime($tmp_data[0]['end_day']);
                $curr_begin_day = $tmp_data[0]['begin_day'];
                $pre_begin_day = date('Ymd',$begin_time-7*24*3600);
                $pre_tmp_data = $dao->getReportByBeginDay('',$pre_begin_day,$fiedls);
                $global_pre_tmp_data = $pre_tmp_data?$pre_tmp_data[0]:[];
                $pre_tmp_data && $pre_tmp_data = array_column($pre_tmp_data,null,'com_id');

                foreach ($tmp_data as $tmp){
                    $return['model_count'] = $tmp['model_count'];
                    $return['usage_count'] = $tmp['usage_count'];
                    $return['instance_count'] = $tmp['instance_count'];

                    $return['pre_model_count'] = isset($global_pre_tmp_data['model_count'])?$global_pre_tmp_data['model_count']:0;
                    $return['pre_usage_count'] = isset($global_pre_tmp_data['usage_count'])?$global_pre_tmp_data['usage_count']:0;
                    $return['pre_instance_count'] = isset($global_pre_tmp_data['instance_count'])?$global_pre_tmp_data['instance_count']:0;

                    $item = [];
                    $item['model_count'] = $tmp['school_model_count'];
                    $item['usage_count'] = $tmp['school_usage_count'];
                    $item['instance_count'] = $tmp['school_instance_count'];
                    $item['pre_model_count'] = isset($pre_tmp_data[$tmp['com_id']]['school_model_count'])?$pre_tmp_data[$tmp['com_id']]['school_model_count']:0;
                    $item['pre_usage_count'] = isset($pre_tmp_data[$tmp['com_id']]['school_usage_count'])?$pre_tmp_data[$tmp['com_id']]['school_usage_count']:0;
                    $item['pre_instance_count'] = isset($pre_tmp_data[$tmp['com_id']]['school_instance_count'])?$pre_tmp_data[$tmp['com_id']]['school_instance_count']:0;
                    $item['com_id'] = $tmp['com_id'];
                    $item['company_name'] = $tmp['company_name'];

                    $return['shool_count_list'][] = $item;
                }unset($tmp);
            }

            parent::echo_ok(['data'=>$return,'begin_time'=>$begin_time,'end_time'=>$end_time]);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }

    private function getAnalysisReport(){
        try{
            $id = g('pkg_val') -> get_get('id');
            $query_com_id = g('pkg_val') -> get_get('com_id');
            !$query_com_id && $query_com_id = $this->com_id;

            //判断当前人员是否在接收范围内容
            $cond = array(
                'info_state='=>1,
                'com_id='=>$this->com_id,
                '`key`='=>'receiver_list',
            );
            $config = g('pkg_db')->select_one("sc_form_analysis_report_config",'*',$cond);
            if(empty($config))
                throw new \Exception('无权限');

            $user_ids = json_decode($config['value'],true);
            if(!$user_ids || !in_array($this->user_id,$user_ids))
                throw new \Exception("无权限");

            $dao = new FormAnalysisReport();
            $return = [];

            $fiedls = 'rd.model_count,rd.usage_count,rd.instance_count,ar.begin_day,ar.end_day,
            rd.com_id, sc.name company_name';
            $tmp_data = $dao->getReport($query_com_id,$id,$fiedls);

            if($tmp_data){
                $begin_time = strtotime($tmp_data[0]['begin_day']);
                $end_time = strtotime($tmp_data[0]['end_day']);

                $curr_begin_day = $tmp_data[0]['begin_day'];
                $pre_begin_day = date('Ymd',strtotime($curr_begin_day)-7*24*3600);

                //获取应用数据
                $app_fields = 'rd.form_history_id,rd.instance as formsetins_count,rd.usage_count user_count ';
                $app_count_data = $dao->getAppCountBeginDay($query_com_id,$curr_begin_day,$app_fields);

                //获取上期数据
                $pre_tmp_data = $dao->getReportByBeginDay($query_com_id,$pre_begin_day,$fiedls);
                $pre_tmp_data && $pre_tmp_data = array_column($pre_tmp_data,null,'com_id');

                $pre_app_count_data = $dao->getAppCountBeginDay($query_com_id,$pre_begin_day,$app_fields);
                $pre_app_count_data && $pre_app_count_data = array_column($pre_app_count_data,null,'form_history_id');

                if($app_count_data){
                    //获取所有表单模板的最新发布版表单名
                    $form_history_ids = array_column($app_count_data,'form_history_id');
                    $form_list = g('mv_form')->list_by_his_id($form_history_ids,'id,form_history_id,form_name,classify_id');
                    $form_list && $form_list = array_column($form_list,null,'form_history_id');

                    //获取表单分类列表
                    $classIds = [];
                    $form_list && $classIds = array_column($form_list,'classify_id');
                    $form_class_list = g('mv_form')->listClassByIds($classIds,'id as classify_id,classify_name');
                    $form_class_list && $form_class_list = array_column($form_class_list,null,'classify_id');

                    foreach ($app_count_data as &$item){
                        $classify_id = isset($form_list[$item['form_history_id']]['classify_id'])?$form_list[$item['form_history_id']]['classify_id']:0;
                        $item['classify_id'] = $classify_id;
                        $item['classify_name'] = isset($form_class_list[$classify_id]['classify_name'])?$form_class_list[$classify_id]['classify_name']:'默认分类';
                        $item['form_name'] = isset($form_list[$item['form_history_id']]['form_name'])?$form_list[$item['form_history_id']]['form_name']:$item['form_name'];
                        $item['per_formsetins_count'] = isset($pre_tmp_data[$query_com_id])&&isset($pre_app_count_data[$item['form_history_id']]['formsetins_count'])?$pre_app_count_data[$item['form_history_id']]['formsetins_count']:0;
                        $item['per_user_count'] = isset($pre_tmp_data[$query_com_id])&&isset($pre_app_count_data[$item['form_history_id']]['user_count'])?$pre_app_count_data[$item['form_history_id']]['user_count']:0;
                    }unset($item);
                }

                foreach ($tmp_data as $tmp){
                    $return['model_count'] = $tmp['model_count'];
                    $return['usage_count'] = $tmp['usage_count'];
                    $return['instance_count'] = $tmp['instance_count'];

                    $return['pre_model_count'] = isset($pre_tmp_data[$tmp['com_id']]['model_count'])?$pre_tmp_data[$tmp['com_id']]['model_count']:0;
                    $return['pre_usage_count'] = isset($pre_tmp_data[$tmp['com_id']]['usage_count'])?$pre_tmp_data[$tmp['com_id']]['usage_count']:0;
                    $return['pre_instance_count'] = isset($pre_tmp_data[$tmp['com_id']]['instance_count'])?$pre_tmp_data[$tmp['com_id']]['instance_count']:0;

                    $return['app_count_list'] = $app_count_data;
                }unset($tmp);
            }

            $company = g('dao_com')->get_by_id($query_com_id);
            parent::echo_ok([
                'data'=>$return,
                'begin_time'=>$begin_time,
                'end_time'=>$end_time,
                'com_id'=>$query_com_id,
                'company_name'=>$company['name'],
            ]);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }


    /**
     * 导出excel或pdf
     */
    public function export_table_v2(){
        try{
            $param = g('pkg_val')->get_get('data');
            $param = json_decode($param,true);
            $table_id = isset($param['table_id'])?$param['table_id']:'';
            $export_type = isset($param['export_type'])?$param['export_type']:'';
            $other_condition = isset($param['condition'])?$param['condition']:[];
            !is_array($other_condition) && $other_condition = json_decode($other_condition,true);

            if($other_condition){
                $tmp_o_cond = [];
                foreach ($other_condition as $item) {
                    if($item['field_val']!=='')
                        $tmp_o_cond[] = $item;
                }
                $other_condition = $tmp_o_cond;
            }

            $user_ids = [$this->user_id];
            $export_type = $export_type ? $export_type : 0;

            if(empty($table_id)){
                throw new \Exception("参数错误");
            }

            // ($export_type==1) ? 导出pdf : 导出excel
            if ($export_type==1) {
                $cre = [
                    'create_time' => time(),
                    'com_id'    => $this->com_id,
                    'admin_id'  => 0,
                    'data_id'   => $table_id,
                    'user_ids'   => json_encode($user_ids),
                ];
                $ok = g('pkg_db')->insert('report_export_pdf', $cre);
                if (!$ok) {
                    throw new \Exception("创建导出pdf任务失败");
                }
                parent::echo_ok([]);
                exit;
            }

            $table_info = $this -> server -> get_table($table_id, 'id, name, type, row_list, col_key, cond_list, join_list, other_list, order_list');
            if(empty($table_info)){
                throw new \Exception("表格已被删除");
            }
            $col_key = $table_info['col_key'];
            $row = $table_info['row_list'];
            $count = $table_info['type'] == 2 ? TRUE : FALSE;

            $order_list = array();
            $finstat_server = new ServerFinstat();
            $ret =  $finstat_server->get_table_data_for_search($table_id, $order_list, $count, 0, 0, $other_condition,true);
            if($count){
                $count_state = 0;
                foreach ($col_key as $key) {
                    if($count_state == 0){
                        $count_state = 1;
                        $ret['count'][$key] = '汇总';
                    }else{
                        $ret['count'][$key] = '';
                    }
                }
            }

            $name = $table_info['name'];

            $model = array(
                'order' => array('row'=> 'n'),
                'user_name' => array('row'=> 'n'),
                'type_name' => array('row'=> 'n'),
                'sum_money' => array('row'=> 'n'),
                'create_time' => array('row'=> 'n'),
                'state' => array('row'=> 'n'),
                '^detail' => array(
                    'name' => array('index' => 7),
                    'money' => array(),
                    'reason' => array(),
                    'produce_time' => array(),
                ),
            );

            $title = array();
            $first_row = array();
            $model = array();
            foreach ($row as $val) {
                $first_row[] = array(
                    'name' => $val['name'],
                    'row' => 0,
                );
                $model[$val['row_key']] = array("row" => 'n');
            }unset($val);

            $new_model = array();
            foreach ($ret['data'] as &$row) {
                $c = 0;
                $r = 0;
                foreach ($row as $key => $val) {
                    $c++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $c),
                            );
                        }
                        $row[$key] = array();
                        foreach ($val as $v) {
                            $row[$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                $row['row'] = $r;
            }unset($row);

            if($count){
                foreach ($ret['count'] as $key => $val) {
                    $count++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $count),
                            );
                        }
                        $ret['count'][$key] = array();
                        foreach ($val as $v) {
                            $ret['count'][$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                $ret['count']['row'] = $r;
                array_push($ret['data'], $ret['count']);
            }

            $title = array($first_row);

            $file_name = $name . time(). '.xls';
            $excelServer = new ExcelQy();
            $file = $excelServer->XML_export_xlsx($file_name, $title, $ret['data'], $new_model);

//            $mediaServer = new ServerMedia();
//            $mediaServer->sendFile2Qywx($this->com_id,[$this->user_id],$file_name,$file);
//            unlink($file);

//            parent::echo_ok([]);
        }catch(\Exception $e){
            parent::echo_exp($e);
        }
    }

    /**
     * 导出excel或pdf 作废
     */
    public function export_table(){
        try{
            $table_id = g('pkg_val') -> get_get('table_id');
            $export_type = g('pkg_val') -> get_get('export_type');

            $param = g('pkg_val')->get_get('data');
            $param = json_decode($param,true);
            $table_id = isset($param['table_id'])?$param['table_id']:'';
            $export_type = isset($param['export_type'])?$param['export_type']:'';
            $other_condition = isset($param['condition'])?$param['condition']:[];
            !is_array($other_condition) && $other_condition = json_decode($other_condition,true);

            if($other_condition){
                $tmp_o_cond = [];
                foreach ($other_condition as $item) {
                    if($item['field_val']!=='')
                        $tmp_o_cond[] = $item;
                }
                $other_condition = $tmp_o_cond;
            }

            $user_ids = [$this->user_id];
            $export_type = $export_type ? $export_type : 0;

            if(empty($table_id)){
                throw new \Exception("参数错误");
            }

            // ($export_type==1) ? 导出pdf : 导出excel
            if ($export_type==1) {
                $cre = [
                    'create_time' => time(),
                    'com_id'    => $this->com_id,
                    'admin_id'  => 0,
                    'data_id'   => $table_id,
                    'user_ids'   => json_encode($user_ids),
                ];
                $ok = g('pkg_db')->insert('report_export_pdf', $cre);
                if (!$ok) {
                    throw new \Exception("创建导出pdf任务失败");
                }
                parent::echo_ok([]);
                exit;
            }

            $table_info = $this -> server -> get_table($table_id, 'id, name, type, row_list, col_key, cond_list, join_list, other_list, order_list');
            if(empty($table_info)){
                throw new \Exception("表格已被删除");
            }
            $col_key = $table_info['col_key'];
            $row_list = $row = $table_info['row_list'];
            $col_list = $table_info['col_list'];
            $join_list = $table_info['join_list'];
            $cond_list = $table_info['cond_list'];
            $other_list = $table_info['other_list'];
            $start = isset($table_info['start']) ? $table_info['start'] : 0;
            $page_size = isset($table_info['page_size']) ? $table_info['page_size'] : 20;
            $count = $table_info['type'] == 2 ? TRUE : FALSE;

            $order_list = array();
            //$ret = $this -> server -> get_conf_data($row, $col_list, $cond_list, $join_list, $order_list, $other_list, $count, FALSE);
            $finstat_server = new ServerFinstat();
            $ret =  $finstat_server->get_table_data_for_search($table_id, $order_list, $count, 0, 0, $other_condition,true);
            if($count){
                $count_state = 0;
                foreach ($col_key as $key) {
                    if($count_state == 0){
                        $count_state = 1;
                        $ret['count'][$key] = '汇总';
                    }else{
                        $ret['count'][$key] = '';
                    }
                }
            }

            $name = $table_info['name'];

            $model = array(
                'order' => array('row'=> 'n'),
                'user_name' => array('row'=> 'n'),
                'type_name' => array('row'=> 'n'),
                'sum_money' => array('row'=> 'n'),
                'create_time' => array('row'=> 'n'),
                'state' => array('row'=> 'n'),
                '^detail' => array(
                    'name' => array('index' => 7),
                    'money' => array(),
                    'reason' => array(),
                    'produce_time' => array(),
                ),
            );

            $title = array();
            $first_row = array();
            $model = array();
            foreach ($row as $val) {
                $first_row[] = array(
                    'name' => $val['name'],
                    'row' => 0,
                );
                $model[$val['row_key']] = array("row" => 'n');
            }unset($val);

            $new_model = array();
            foreach ($ret['data'] as &$row) {
                $c = 0;
                $r = 0;
                foreach ($row as $key => $val) {

                    //判断当前控件是否未隐患控件
                    if( isset($row_list[$key]['type']) && $row_list[$key]['type']=='dangerous' ){
                        $val = json_decode($val,true);
                        $snake_list = [];
                        $str = "\n";
                        foreach ($val['snake_list'] as $i=>$item){
                            $flog = '';
                            if(!empty($val['danger_list'][$i]['val'])){
                                switch ($val['danger_list'][$i]['val']){
                                    case 1:
                                        $flog = '(√)';
                                        break;
                                    case 2:
                                        $flog = '(×)';
                                        break;
                                    case 3:
                                        $flog = '(无)';
                                        break;
                                    default :
                                        break;
                                }

                                if($val['danger_list'][$i]['val']==2){
                                    //整改前图片，整改后图片
                                    $before_img = isset($val['danger_list'][$i]['before_img'])?array_column($val['danger_list'][$i]['before_img'],'image_path'):[];
                                    $after_img = isset($val['danger_list'][$i]['after_img'])?array_column($val['danger_list'][$i]['after_img'],'image_path'):[];
                                    $before_img && $flog.= "{$str}隐患图片{$str}".implode($str,$before_img);
                                    $after_img && $flog.= "{$str}整改后图片{$str}".implode($str,$after_img);
                                }
                            }
                            $flog && $snake_list[] = $item.$flog;
                        }unset($item);
                        $row[$key] = $val = $snake_list ? implode($str,$snake_list):'--';
                    }

                    $c++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $c),
                            );
                        }
                        $row[$key] = array();
                        foreach ($val as $v) {
                            $row[$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                // $row['row'] = $r;
            }unset($row);

            if($count){
                foreach ($ret['count'] as $key => $val) {
                    $count++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $count),
                            );
                        }
                        $ret['count'][$key] = array();
                        foreach ($val as $v) {
                            $ret['count'][$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                $ret['count']['row'] = $r;
                array_push($ret['data'], $ret['count']);
            }

            $title = array($first_row);
            $file_name = $name . time(). '.xlsx';

            $data = [];

            // 标题
            $index = 'A';
            $title_data = [];
            $title = current($title);
            foreach ($title as $item){
                $title_data["{$index}1"] = $item['name'];
                $index ++;
            }
            $data[] = $title_data;

            // 数据整理
            $begin_row = 2;
            foreach ($ret['data'] as $datum){
                $tmp_row = [];
                $max_row = 0;
                //先获取一个实例最多有多少个子表记录，占多少行
                foreach ($datum as $item){
                    if(is_array($item)){
                        count($item)>$max_row && $max_row=count($item)-1;
                    }
                }unset($item);

                $col = 'A';
                $end_row = $begin_row+$max_row;
                foreach ($datum as $item){
                    if(is_array($item) ){
                        $tmp_begin_row = $begin_row;
                        foreach ($item as $value){
                            $key = $col.$tmp_begin_row;
                            $tmp_row[$key] = current($value);
                            $tmp_begin_row ++;
                        }
                    }else{
                        $key = $col.$begin_row.':'.$col.$end_row ;
                        $tmp_row[$key] = $item;
                    }
                    $col++;
                }unset($item);
                $data[] = $tmp_row;
                $begin_row = $end_row+1;
            }

            $excelServer = new Excel();
            $file = $excelServer -> exportExcel(uniqid(), $data,false,true);

            $mediaServer = new ServerMedia();
            $mediaServer->sendFile2Qywx($this->com_id,[$this->user_id],$file_name,$file);
            unlink($file);

            parent::echo_ok([]);
            // g('pkg_excel') -> XML_export_xlsx($file_name, $title, $ret['data'], $new_model);
        }catch(\Exception $e){
            parent::echo_exp($e);
            // g('view_notice') -> error($e -> getMessage());
        }
    }

    /**
     * 导出excel或pdf 作废
     */
    public function export_table_v1(){
        try{
            $table_id = g('pkg_val') -> get_get('table_id');
            $export_type = g('pkg_val') -> get_get('export_type');

            $param = g('pkg_val')->get_get('data');
            $param = json_decode($param,true);
            $table_id = isset($param['table_id'])?$param['table_id']:'';
            $export_type = isset($param['export_type'])?$param['export_type']:'';
            $other_condition = isset($param['condition'])?$param['condition']:[];
            !is_array($other_condition) && $other_condition = json_decode($other_condition,true);

            if($other_condition){
                $tmp_o_cond = [];
                foreach ($other_condition as $item) {
                    if($item['field_val']!=='')
                        $tmp_o_cond[] = $item;
                }
                $other_condition = $tmp_o_cond;
            }

            $user_ids = [$this->user_id];
            $export_type = $export_type ? $export_type : 0;

            if(empty($table_id)){
                throw new \Exception("参数错误");
            }

            // ($export_type==1) ? 导出pdf : 导出excel
            if ($export_type==1) {
                $cre = [
                    'create_time' => time(),
                    'com_id'    => $this->com_id,
                    'admin_id'  => 0,
                    'data_id'   => $table_id,
                    'user_ids'   => json_encode($user_ids),
                ];
                $ok = g('pkg_db')->insert('report_export_pdf', $cre);
                if (!$ok) {
                    throw new \Exception("创建导出pdf任务失败");
                }
                parent::echo_ok([]);
                exit;
            }

            $table_info = $this -> server -> get_table($table_id, 'id, name, type, row_list, col_key, cond_list, join_list, other_list, order_list');
            if(empty($table_info)){
                throw new \Exception("表格已被删除");
            }
            $col_key = $table_info['col_key'];
            $row = $table_info['row_list'];
            $col_list = $table_info['col_list'];
            $join_list = $table_info['join_list'];
            $cond_list = $table_info['cond_list'];
            $other_list = $table_info['other_list'];
            $start = isset($table_info['start']) ? $table_info['start'] : 0;
            $page_size = isset($table_info['page_size']) ? $table_info['page_size'] : 20;
            $count = $table_info['type'] == 2 ? TRUE : FALSE;

            $order_list = array();
            //$ret = $this -> server -> get_conf_data($row, $col_list, $cond_list, $join_list, $order_list, $other_list, $count, FALSE);
            $finstat_server = new ServerFinstat();
            $ret =  $finstat_server->get_table_data_for_search($table_id, $order_list, $count, 0, 0, $other_condition,true);
            if($count){
                $count_state = 0;
                foreach ($col_key as $key) {
                    if($count_state == 0){
                        $count_state = 1;
                        $ret['count'][$key] = '汇总';
                    }else{
                        $ret['count'][$key] = '';
                    }
                }
            }

            $name = $table_info['name'];

            $model = array(
                'order' => array('row'=> 'n'),
                'user_name' => array('row'=> 'n'),
                'type_name' => array('row'=> 'n'),
                'sum_money' => array('row'=> 'n'),
                'create_time' => array('row'=> 'n'),
                'state' => array('row'=> 'n'),
                '^detail' => array(
                    'name' => array('index' => 7),
                    'money' => array(),
                    'reason' => array(),
                    'produce_time' => array(),
                ),
            );

            $title = array();
            $first_row = array();
            $model = array();
            foreach ($row as $val) {
                $first_row[] = array(
                    'name' => $val['name'],
                    'row' => 0,
                );
                $model[$val['row_key']] = array("row" => 'n');
            }unset($val);

            $new_model = array();
            foreach ($ret['data'] as &$row) {
                $c = 0;
                $r = 0;
                foreach ($row as $key => $val) {
                    $c++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $c),
                            );
                        }
                        $row[$key] = array();
                        foreach ($val as $v) {
                            $row[$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                // $row['row'] = $r;
            }unset($row);

            if($count){
                foreach ($ret['count'] as $key => $val) {
                    $count++;
                    if(is_array($val)){
                        $r = (count($val)-1) > $r ? count($val)-1 : $r;
                        if(!isset($model['^'.$key])){
                            $new_model['^'.$key] = array(
                                "{$key}_v" => array('index' => $count),
                            );
                        }
                        $ret['count'][$key] = array();
                        foreach ($val as $v) {
                            $ret['count'][$key][] = array("{$key}_v" => $v);
                        }unset($v);
                    }else{
                        $new_model[$key] = $model[$key];
                    }
                }unset($val);
                $ret['count']['row'] = $r;
                array_push($ret['data'], $ret['count']);
            }

            $title = array($first_row);

            $file_name = $name . time(). '.xlsx';

            $data = [];
            // 标题
            $index = 'A';
            $title_data = [];
            $title = current($title);
            foreach ($title as $item){
                $title_data["{$index}1"] = $item['name'];
                $index ++;
            }
            $data[] = $title_data;


            // 数据整理
            $i = 0;
            $_data = [];
            foreach ($ret['data'] as $datum){
                $j = 0;
                foreach ($datum as $item){
                    if (is_array($item)){
                        $tmp_i = $i;
                        foreach ($item as $value){
                            $_data[$i][$j] = current($value);
                            $i ++;
                        }
                        $i = $tmp_i;
                    } else {
                        $_data[$i][$j] = $item;
                    }
                    $j++;
                }
                $i++;
            }
            // 内容
            // 合并项处理
            $mergeIndex = [];
            $mergeItem = [];
            $indexs = array_keys(current($_data));
            $k = 2;
            foreach ($_data as $datum){
                $index = 'A';
                $tmp_data = [];
                foreach ($indexs as $i){
                    $key = "{$index}{$k}";
                    // 记录合并项
                    if (isset($datum[$i])) {
                        $tmp_data[$key] = $datum[$i];
                        if (isset($mergeIndex[$index]) && $mergeIndex[$index]['begin'] != $mergeIndex[$index]['current']) {
                            $mergeItem[$mergeIndex[$index]['begin']] = $mergeIndex[$index]['begin'].":". $mergeIndex[$index]['current'];
                        }
                        $mergeIndex[$index] = [
                            'begin'=>$key,
                            'current'=>$key,
                        ];
                    } else {
                        $mergeIndex[$index]['current'] = $key;
                    }
                    $index++;
                }
                $data[] = $tmp_data;
                $k ++;
            }
            // 整理合并项
            foreach ($mergeIndex as $item) {
                if ($item['begin'] != $item['current']){
                    $mergeItem[$item['begin']] = $item['begin'].":".$item['current'];
                }
            }
            // 终极数据
            $last_data = [];
            foreach ($data as $datum) {
                $tmp_data = [];
                foreach ($datum as $key=>$value) {
                    if (isset($mergeItem[$key])){
                        $tmp_data[$mergeItem[$key]] = $value;
                    } else {
                        $tmp_data[$key] = $value;
                    }
                }
                $last_data[] = $tmp_data;
            }

            $excelServer = new Excel();
            $file = $excelServer -> exportExcel(uniqid(), $last_data,false,true);

            $mediaServer = new ServerMedia();
            $mediaServer->sendFile2Qywx($this->com_id,[$this->user_id],$file_name,$file);
            unlink($file);

            parent::echo_ok([]);
            // g('pkg_excel') -> XML_export_xlsx($file_name, $title, $ret['data'], $new_model);
        }catch(\Exception $e){
            parent::echo_exp($e);
            // g('view_notice') -> error($e -> getMessage());
        }
    }
	/**
	 * 获取流程报表
	 * @return
	 */
	private function get_report_list() {
		try {
			$data = parent::get_post_data();

			$name = isset($data['name']) ? trim($data['name']) : '';
			$page = isset($data['page']) ? intval($data['page']) : 1;
			$page < 1 && $page = 1;
			$page_size = 10;

			$cond = array();
			!empty($name) && $cond['name LIKE '] = '%' . $name . '%';

			parent::log_i('开始获取流程报表');
			$report_list = $this -> server -> get_report_list($cond, $page, $page_size);

			parent::log_i('成功获取流程报表');
			parent::echo_ok(array('info' => $report_list));
		} catch (\Exception $e) {
			parent::exp($e);
		}
	}



}
//end of file