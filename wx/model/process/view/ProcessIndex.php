<?php

namespace model\process\view;

class ProcessIndex extends \model\api\view\ViewBase {
	
	public function __construct($log_pre_str='') {
		parent::__construct('process',$log_pre_str);
	}
	
	/* 发起流程 */
	public function index(){
		parent::get_menu();
		g('pkg_smarty') -> assign('title', '发起流程');
		g('pkg_smarty') -> show($this -> view_dir . 'page/index.html');
	}
	
	
	/* 待办/已办 */
	public function dealList(){
		parent::get_menu();
		g('pkg_smarty') -> assign('title', '待办/已办');
		g('pkg_smarty') -> show($this -> view_dir . 'page/dealList.html');
	}
	
	
	/* 我的发起 */
	public function mineList(){
		parent::get_menu();
		g('pkg_smarty') -> assign('title', '我的发起');
		g('pkg_smarty') -> show($this -> view_dir . 'page/mineList.html');
	}
	
	/* 我的知会*/
	public function notifyList(){
		parent::get_menu();
		g('pkg_smarty') -> assign('title', '我的知会');
		g('pkg_smarty') -> show($this -> view_dir . 'page/notifyList.html');
	}

	/* 详情*/
	public function detail(){
	    $id = isset($_GET['id'])?$_GET['id']:'';
        $listType = isset($_GET['listType'])?$_GET['listType']:'';
        $isnew = isset($_GET['isnew'])?$_GET['isnew']:1;

        if($isnew){
            //目前统一走新的，使用if是防止对比旧的
            if($listType == 2){
                header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/processApproval?type=2&is_deal=1&formsetinst_id='.$id);
                exit;
            }elseif($listType == 1){
                header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/processApproval?type=1&workitem_id='.$id);
                exit;
            }elseif($listType == 3){
                header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/processApproval?type=3&is_deal=1&notify_id='.$id);
                exit;
            }
        }


		parent::get_user_info();
		g('api_jssdk') -> assign_jssdk_sign();
		g('pkg_smarty') -> assign('title', '查看详情');
		g('pkg_smarty') -> show($this -> view_dir . 'page/detail.html');
	}

	/* 创建*/
	public function create(){

	    $is_new = isset($_GET['isnew'])?$_GET['isnew']:1;
	    if($is_new){
            $id = isset($_GET['id'])?$_GET['id']:'';
            header('Location:/'.MAIN_STATIC_VER.'/view_src_dist/index.html#/processCreate?id='.$id);
            exit();
        }

		parent::get_user_info();
		g('pkg_smarty') -> assign('title', '发起申请');
		g('pkg_smarty') -> show($this -> view_dir . 'page/create.html');
	}

	/* 知会选择人员*/
	public function notify(){
		g('pkg_smarty') -> assign('title', '知会');
		g('pkg_smarty') -> show($this -> view_dir . 'page/notify.html');
	}

	/* 流程图*/
	public function processPic(){
		g('pkg_smarty') -> assign('title', '流程图');
		g('pkg_smarty') -> show($this -> view_dir . 'page/processPic.html');
	}

    /* 流程图*/
    public function processPicNew(){
        g('pkg_smarty') -> assign('title', '流程图');
        g('pkg_smarty') -> show($this -> view_dir . 'page/processPicNew.html');
    }

	/* 发送*/
	public function sendPage(){
		g('pkg_smarty') -> assign('title', '发送');
		g('pkg_smarty') -> show($this -> view_dir . 'page/sendPage.html');
	}
	
	/* 打印表单 */
	public function print_form() {
		$this -> show($this -> view_dir . 'page/processRecordPrint.html');
	}

	/* 物资录入 */
	public function importMaterial() {
		g('pkg_smarty') -> assign('title', '物资录入');
		$this -> show($this -> view_dir . 'page/processMaterialImport.html');
	}

	public function jumpMaterial(){
	    $showInputPage = isset($_GET['showInputPage'])?$_GET['showInputPage']:'';
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxMaterialTemplate?showInputPage='.$showInputPage);
    }

    public function jumpWarehousing(){
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxWarehousing');
    }
    
    public function jumpAgencyList(){
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxAgencyList');
    }
    public function jumpInformList(){
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxInformList');
    }
    public function jumpLaunchList(){
        header('Location: /'.MAIN_STATIC_VER.'/view_src_dist/index.html#/nhwxLaunchList');
    }
}

//end