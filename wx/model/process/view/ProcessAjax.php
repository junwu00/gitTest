<?php
/**
 * 流程审批全部ajax交互类
 *
 * @author luja
 * @create 2016-7-6
 */

namespace model\process\view;

use model\api\dao\ComForm;
use model\api\dao\ProcessSchoolContact;
use model\api\server\Media;
use model\api\server\mv_proc\Mv_Formsetinst_Name_Val;
use model\api\server\mv_proc\Mv_Work_Node;
use model\api\server\mv_proc\Mv_Workinst;
use model\api\server\ServerMedia;
use model\process\server\ServerMaterial;
use package\excel\Excel;
use package\excel\ExcelServer;

class ProcessAjax extends \model\api\view\AbsProc {

	private static $FormUserCount =5;

	private $pkg_val;

	private static $TAG_TYPE = 1;

	// 真实物资
	private static $MATERIAL_REAL = 1;
	// 物资模板
	private static $MATERIAL_MODEL = 2;


    private static $input_specification_name = '规格';
    private static $input_unit_name = '单位';
    private static $input_price_name = '价格';

	// 错误提示码
	private $busy = 0;

	public function __construct() {
		parent::__construct('process');

		$this -> pkg_val = g('pkg_val');

		// 未知原因（系统繁忙）
		$this -> busy = \model\api\server\Response::$Busy;
	}


	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$cmd = (int)$this -> pkg_val -> get_get('cmd', FALSE);
		switch($cmd) {
			case 101 : {
				// 获取流程分类及分类下的表单
				$this -> get_form_list();
				break;
			}
			// case 102 : {
			// 	// 获取用户常用表单（已弃用）
			// 	$this -> get_user_common_form();
			// 	break;
			// }
			case 103 : {
				// 根据表单id获取表单信息
				$this -> get_form_detail();
				break;
			}
			case 104 : {
				// 获取当前流程实例名称
				$this -> get_form_name();
				break;
			}
			case 105 : {
				// 保存流程
				$this -> save_workitem();
				break;
			}
			case 106 : {
				// 发送流程
				$this -> send_workitem();
				break;
			}
			case 107 : {
				// 获取下一工作步骤
				$this -> get_next_node();
				break;
			}
			case 108 : {
				// 获取流程审批详情
				$this -> get_process_detail();
				break;
			}
			case 109 : {
				// 获取流程图
				$this -> get_process_pic();
				break;
			}
			case 110 : {
				// 驳回开始步骤流程
				$this -> callback_start();
				break;
			}
			case 111 : {
				// 退回上一步骤流程
				$this -> callback_pre();
				break;
			}
			case 112 : {
				// 删除流程
				$this -> del_workitem();
				break;
			}
			case 113 : {
				// 我处理的流程列表（待办/已办）
				$this -> get_handler_list();
				break;
			}
			case 114 : {
				// 我发起的流程列表（运行中/已结束/草稿）
				$this -> get_mine_list();
				break;
			}
			case 115 : {
				// 我知会的流程列表（未阅读、已阅读）
				$this -> get_notify_list();
				break;
			}
			case 116 : {
				// 发送到退回步骤
				$this -> sendback_workitem();
				break;
			}
			case 117 : {
				// 催办流程
				$this -> press_workitem();
				break;
			}
			case 118 : {
				// 知会流程
				$this -> notify_workitem();
				break;
			}
			case 119 : {
				// 获取领签页面url
				$this -> get_linksign_url();
				break;
			}
			case 120 : {
				// 保存领签图片
				$this -> get_save_linksign();
				break;
			}
			case 121 : {
				// 下载附件
				$this -> download_file();
				break;
			}
			case 122 : {
				// 驳回指定步骤流程
				$this -> callback_select();
				break;
			}
			case 123 : {
				// 打印流程
				$this -> print_form();
				break;
			}
			case 124 : {
				// 检测是否有冲突,有则返回冲突信息
				$this -> check_conflict();
				break;
			}

            case 224 :{
                $this->callback_item_list();
                break;
            }
			// gch add material
			case 125 : {
				//获取物资表单
				$this -> getMaterialList();
				break;
			}

            case 126 :{
                //文件控件 下载文件
                $this->download_input_file();
                break;
            }

            case 128 : {
                // 终止流程
                $this -> break_workitem();
                break;
            }

            case 200 :{
                // 物资流程自动发起
                $this->automatic_process();
                break;
            }
            case 201 :{
                // 下载物资模板
                $this->download_material_model();
                break;
            }
            case 202 :{
                // 获取物资模板内容
                $this->get_xls_material_model();
            }
            case 203 :{
                // 获取物资模板内容
                $this->get_material_data();
            }

            case 210 : {
                // 撤销流程
                $this -> revoke_workitem();
                break;
            }

            case 211 : {
                // 撤销流程
                $this -> finish_revoke_workitem();
                break;
            }

            case 220 :{
                //获取互联表单学校控件可选择学校信息
                $this->listFromSchool();
                break;
            }
			default : {
				//非法的请求
				parent::echo_busy();
				break;
			}
		}
	}

    /**
     * 阅读提醒
     * @return void
     */
    public function readRemind(){
        try{
            $need = array(
                'form_id','push_time'
            );
            $param = parent::get_post_data($need);
            $form_id = isset($param['form_id'])?$param['form_id']:0;
            $push_time = isset($param['push_time'])?$param['push_time']:0;
            $fields = '*';
            //查看表单信息
            $proc = g('mv_form') -> get_by_id($form_id, $this->com_id, $fields);
            if (!$proc) throw new \Exception('该模板不存在！');

            g('mv_form') -> setRemindReadtime($this->user_id, $proc['form_history_id'], $push_time);

            parent::echo_ok([]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    /**
     *
     * @return void
     */
    public function listSchoolContact(){
        try{
            $need = array(
                'parent_unit_id','school_com_id','role_ids'
            );
            $param = parent::get_post_data($need);

            $parent_unit_id = isset($param['parent_unit_id'])?$param['parent_unit_id']:'';
            $school_com_id = isset($param['school_com_id'])?$param['school_com_id']:'';
            $role_ids = isset($param['role_ids'])?$param['role_ids']:[];

            $return = ['user_list'=>[],"dept_list"=>[]];
            //获取角色数据
            $dao_school_contact = new ProcessSchoolContact();
            $ret = $dao_school_contact->getBySIdAndRIds($parent_unit_id,$school_com_id,$role_ids);
            if($ret){
                $user_ids = [];
                $dept_ids = [];
                foreach ($ret as $item){
                    $value = json_decode($item['value'],true);
                    switch ($item['type']){
                        case ProcessSchoolContact::TYPE_USER:
                            $value && $user_ids = array_merge($user_ids,$value);
                            break;
                        case ProcessSchoolContact::TYPE_DEPT:
                            $value && $dept_ids = array_merge($dept_ids,$value);
                            break;
                    }
                }unset($item);
                $user_ids && $return['user_list'] = g('dao_user')->list_by_ids($user_ids,'id,name,pic_url');
                $dept_ids && $return['dept_list'] = g('dao_dept')->list_by_ids($dept_ids,'id,name');
            }

            parent::echo_ok(['data'=>$return]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    /**
     *
     * @return void
     */
    public function listSchoolContactWithRole(){
        try{
            $need = array(
                'parent_unit_id','school_com_id','role_ids'
            );
            $param = parent::get_post_data($need);

            $parent_unit_id = isset($param['parent_unit_id'])?$param['parent_unit_id']:'';
            $school_com_id = isset($param['school_com_id'])?$param['school_com_id']:'';
            $role_ids = isset($param['role_ids'])?$param['role_ids']:[];

            $keyword = isset($param['keyword'])?$param['keyword']:'';

            $return = [];
            //获取角色数据
            $dao_school_contact = new ProcessSchoolContact();
            $ret = $dao_school_contact->getBySIdAndRIdsWithRole($parent_unit_id,$school_com_id,$role_ids);
            if($ret){
                foreach ($ret as $item){
                    if(!isset($return[$item['role_id']])){
                        $return[$item['role_id']]['role_id'] = $item['role_id'];
                        $return[$item['role_id']]['role_name'] = $item['role_name'];
                        $return[$item['role_id']]['user_list'] = [];
                        $return[$item['role_id']]['dept_list'] = [];
                    }

                    $value = json_decode($item['value'],true);
                    if($value){
                        switch ($item['type']){
                            case ProcessSchoolContact::TYPE_USER:
                                $tmp_user_list = g('dao_user')->list_by_cond(array('id IN'=>$value,'name like'=>'%'.$keyword.'%'),'id,name,pic_url');
                                $tmp_user_list && $return[$item['role_id']]['user_list'] = $tmp_user_list;
                                break;
                            case ProcessSchoolContact::TYPE_DEPT:
                                $tmp_dept_list = g('dao_dept')->list_by_ids($value,'id,name');
                                $tmp_dept_list && $return[$item['role_id']]['dept_list'] = $tmp_dept_list;
                                break;
                        }
                    }
                }unset($item);

            }
            parent::echo_ok(['data'=>array_values($return)]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }

    /**
     * 获取上级单位通讯录
     * @return void
     */
    public function listParentUnitContact(){
        try{
            $need = array(
                'parent_unit_id','school_com_id'
            );
            $param = parent::get_post_data($need);

            $parent_unit_id = isset($param['parent_unit_id'])?$param['parent_unit_id']:'';
            $school_com_id = isset($param['school_com_id'])?$param['school_com_id']:'';
            $role_ids = isset($param['role_ids'])?$param['role_ids']:[];

            $keyword = isset($param['keyword'])?$param['keyword']:'';

            $return = ['user_list'=>[],"dept_list"=>[]];
            $fields = 'ref.perm_dept_id,ref.perm_user_id,ref.perm_tag_id';
            //获取角色数据
            $dao_school_contact = new ProcessSchoolContact();
            $ret = $dao_school_contact->getBySIdAndRIds($parent_unit_id,$school_com_id,$role_ids,$fields);
            if($ret){
                $user_ids = [];
                $dept_ids = [];
                foreach ($ret as $item){
                    $perm_user_ids = json_decode($item['perm_user_id'],true);
                    $perm_user_ids && $user_ids = array_merge($user_ids,$perm_user_ids);
                    $perm_dept_ids = json_decode($item['perm_dept_id'],true);
                    $perm_dept_ids && $dept_ids = array_merge($dept_ids,$perm_dept_ids);
                }unset($item);
                $user_ids = array_unique($user_ids);
                $user_ids && $return['user_list'] = g('dao_user')->list_by_cond(array('id IN'=>$user_ids,'name like'=>'%'.$keyword.'%'),'id,name,pic_url');
                $dept_ids && $return['dept_list'] = g('dao_dept')->list_by_ids($dept_ids,'id,name');
            }

            parent::echo_ok(['data'=>$return]);
        }catch (\Exception $e){
            parent::echo_exp($e);
        }
    }


//------------------------内部实现-------------------------------

    /**
     * 获取互联表单关联的学校信息
     * @return void
     */
    private function listFromSchool(){
        $need = array(
            'form_id'
        );
        $param = parent::get_post_data($need);
        $form_id = isset($param['form_id'])?$param['form_id']:0;
        $keyword = isset($param['keyword'])?$param['keyword']:'';

        $form_info = g('mv_form')->get_by_id($form_id,$this->com_id);
        if(!$form_info){
            throw new \Exception('表单模板不存在');
        }

        $ret = g('mv_form')->listSchoolCom($form_info['com_id'],$keyword);

        parent::echo_ok(['data'=>$ret]);
    }
    private function get_material_data()
    {
        //判断当前人员是否有权限访问
        $exist = g('dao_material')->checkConfig($this -> com_id, $this->user_id);

        $material_data = $this->_getMaterialList();
        $data = [];
        foreach ($material_data['data'] as $datum){
            foreach ($datum['tag_id'] as $key=>$tag_id){
                $data[] = [
                    "data_id" => $datum['id'],
                    "tag_id" => $tag_id,
                    "tag_name" => $datum['tag'][$key],
                    "data_name" => $datum['name'],
                    "number" => 0,
                    "specification"=>$datum['specification'],
                    "unit"=>$datum['unit'],
                    "price"=>$datum['price'],
                ];
            }
        }
        parent::echo_ok([
            'info'=>$data,
            'have_power'=>$exist?1:0,
        ]);
    }
    /** @throws  */
    private function get_xls_material_model()
    {
        try{
            g('pkg_db') -> begin_trans();

            $serverUpload = new Media();

            $file = array_shift($_FILES);
            $file = $serverUpload-> local_upload($file);
            $file_path = $file['path'];
            //获取文件总行数
            $total_count = g('pkg_excel') -> get_count_from_xlsx($file_path);
            if ($total_count <= 0) 		throw new \Exception('文件内容为空');

            //把xlsx文件转成csv文件后再读取，可减少硬件资源消耗及加快读取速度
            $csv_file_path = str_replace(array('.xlsx', '.xls'), '.csv', $file_path);
            g('pkg_excel') -> xlsx_to_csv($file_path, $csv_file_path, FALSE);

            $fdata = g('pkg_excel') -> read_from_csv($csv_file_path, 0, 0, TRUE, FALSE, TRUE);


            $material_data = $this->_getMaterialList();

            // 分类下物品
            $data_names = [];
            foreach ($material_data['data'] as $datum){
                $data_names[$datum['id']] = $datum['name'];
            }

            $data = [];
            $errMsg = [];
            foreach ($fdata as $key=>$fdatum){
                if ($key <4) {
                    continue;
                }
                if (!$fdatum[0] && !$fdatum[1] && !$fdatum[2] && !$fdatum[3] && !$fdatum[4] && !$fdatum[5] ){
                    continue;
                }

                $line = $key - 1;
                // 物品大类
                $tag = explode("_id_",$fdatum[0]);
                $tag_id = $tag[1];
                $tag_name = $tag[0];
                // 物品名称
                $_data = explode("_id_",$fdatum[1]);
                $data_id = isset($_data[1])?$_data[1]:"0";
                // 获取最新物品id
                $data_id = g('dao_material')->getVersionUseId($data_id);
                $data_id = strval($data_id);

                $data_name = $_data[0];
                // 新增数量
                $number = $fdatum[2];
                // 规格型号
                $specification = $fdatum[3];
                // 计量单位
                $unit = $fdatum[4];
                // 价格
                $price = round(intval($fdatum[5]),2);

                if ($data_id === "" || $tag_id==="" || $tag_name === "" || $data_name === "") {
                    $errMsg[] = "第{$line}行，物品id，分类id，物品名称，分类名称不能为空";
                    continue;
                }
                if ($data_id === '0') {
                    if ($material_data['idTag'][$tag_id]['name'] != $tag_name) {
                        $errMsg[] = "第{$line}行，分类名称[{$tag_name}]错误";
                        continue;
                    }
                    if (in_array($data_name,$data_names)) {
                        $errMsg[] = "第{$line}行，物品名称[{$data_name}]已存在";
                        continue;
                    }

                    $data_id = $this->saveMaterialData($data_name,$tag_name,$specification,$unit,$price);
                } else {
                    if (!isset($material_data['idData'][$data_id])) {
                        $errMsg[] = "第{$line}行，物品id[{$data_id}]错误，请重新下载模板";
                        continue;
                    }
                    if (!in_array($tag_id,$material_data['idData'][$data_id]['tag_id'])) {
                        $errMsg[] = "第{$line}行，分类id[{$tag_id}]错误，请重新下载模板";
                        continue;
                    }
                    if ($material_data['idTag'][$tag_id]['name'] != $tag_name) {
                        $errMsg[] = "第{$line}行，分类名称[{$tag_name}]错误，请重新下载模板";
                        continue;
                    }
                    if ($material_data['idData'][$data_id]['name'] != $data_name) {
                        $errMsg[] = "第{$line}行，物品名称[{$data_name}]错误，请重新下载模板";
                        continue;
                    }
                    if (in_array($data_name,$data_names) && $data_names[$data_id] != $data_name) {
                        $errMsg[] = "第{$line}行，物品名称[{$data_name}]已存在";
                        continue;
                    }

                    // 获取物品历史id
                    $material = g('dao_material') ->getMaterial($data_id);
                    $history_id = $material?$material['history_id']:0;

                    $_data = $material_data['idData'][$data_id];
                    $specification = $specification?$specification:$_data['specification'];
                    $unit = $unit?$unit:$_data['unit'];
                    $price = $price?$price:$_data['price'];
                    // 更新物资
                    $data_id = $this->saveMaterialData($data_name,$tag_name,$specification,$unit,$price,$history_id);
                }

                $data[] = [
                    "data_id"=>$data_id,
                    "tag_id"=>$tag_id,
                    "tag_name"=>$tag_name,
                    "data_name"=>$data_name,
                    "number"=>$number,
                    "specification"=>$specification,
                    "unit"=>$unit,
                    "price"=>$price,
                ];
            }
            if ($errMsg) {
                $errMsg = implode(" | ",$errMsg);
                throw new \Exception($errMsg);
            }

            g('pkg_db') ->commit();
            parent::echo_ok(['info'=>$data]);
        }catch (\Exception $e){
            g('pkg_db') -> rollback();
            parent::echo_exp($e);
        }
    }
    private function saveMaterialData($name,$tag_name,$specification,$unit,$price,$history_id=0)
    {

        $user = g('dao_user') -> get_by_id($this->user_id);
        $m = [
            '`name`'=>$name,
            '`describe`' =>"",
            '`icon`' =>"",
            'type' => self::$MATERIAL_REAL,
            '`user_list`' =>'[]',
            '`dept_list`' =>'[]',
            '`group_list`' =>'[]',
            '`is_show_wx`' =>1,
            '`creater_id`' =>0,
            '`create_name`' =>$user['name'],
        ];
        $materialInputData = [
            [
                '`name`'=>'规格',
                '`describe`'=>'规格描述',
                '`input_key`'=>'input' . (time() + rand(1000,9999)). '000',
                'type'=>'text',
                'must'=>0,
                'sub'=>[],
                'other'=>"{\"the_value\":\"{$specification}\"}",
            ],
            [
                '`name`'=>'单位',
                '`describe`'=>'单位描述',
                '`input_key`'=>'input' . (time() + rand(1000,9999)). '000',
                'type'=>'text',
                'must'=>0,
                'sub'=>[],
                'other'=>"{\"the_value\":\"{$unit}\"}",
            ],
            [
                '`name`'=>'价格',
                '`describe`'=>'价格描述',
                '`input_key`'=>'input' . (time() + rand(1000,9999)). '000',
                'type'=>'text',
                'must'=>0,
                'sub'=>[],
                'other'=>"{\"the_value\":\"{$price}\"}",
            ]
        ];
        $materialId = g('dao_material') -> saveMaterial($this->com_id, $m, $materialInputData,$history_id);

        $serverMaterial = new ServerMaterial();
        $serverMaterial->handleTag([$tag_name],$materialId,$this->com_id);

        return $materialId;
    }
    private function download_material_model()
    {
        try{
            $is_download = isset($_GET['is_download']) ? $_GET['is_download'] : 0;
            $excelServer = $this->get_material_excel_server();

            if ($is_download) {
                $excelServer->output();
                return;
            }

            $file = $excelServer->save();
            $mediaServer = new ServerMedia();
            $mediaServer->sendFile2Qywx($this->com_id,[$this->user_id],"物品入库批量表.xlsx",$file);

            unlink($file);
            parent::echo_ok([]);
        }catch (\Exception $e){
            isset($file) && unlink($file);
            parent::echo_exp($e);
        }
    }
    private function get_material_excel_server()
    {
        $data = array(
            array(
                'A1:F1:80' => "填写说明：
1.【入库更新操作】选择物品大类，系统可带出该分类下物品名称，您可以通过搜索或下拉的方式搜索到已有物品，填写新增数量即可；其中规格，单位和价格如没有变动，不需要填写；如规格，单位和价格有更新，填写上去，系统也会更新该物品的属性；
2.【新增物品操作】选择物品大类，直接填写物品名称,填写新增数量，并填写规格和单位，如果规格没有，填写“暂无”；",
            ),
            array(
                'A2' => "物品大类",
                'B2' => "物品名称",
                'C2' => "新增数量",
                'D2' => "规格",
                'E2' => "单位",
                'F2' => "价格",
            ),
        );

        $excelServer = new ExcelServer();
        $excelServer->setWidth('A',25);
        $excelServer->setWidth('B',25);
        $excelServer->setWidth('C',15);
        $excelServer->setWidth('D',30);
        $excelServer->setWidth('E',15);
        $excelServer->setWidth('F',15);
        // 标题写入
        $excelServer->writeData($data);
        $offset = 3;
        $limit = 100;

        $material_data = $this->_getMaterialList();
        $tag_replace_str = ['<','>','!','*','\'','\'','#' ,'^','"','`','$','-','!','*','=','@','%','~','#', '&','[',']','.','/'];
        // 一级分类
        $tags = [];
        foreach ($material_data['allTag'] as $datum){
            $tag = "{$datum['name']}_id_{$datum['id']}";
            $tag = str_replace($tag_replace_str,'_',$tag);
            $tags[] = $tag;
        }
        $excelServer->setComboBoxData($tags,'A',$offset,$limit,"","",function(\PHPExcel_Cell_DataValidation $objValidation){
            $objValidation->setShowErrorMessage(false);
            $objValidation->setErrorTitle('');
        });
        // 二级物品
        $data = [];
        foreach ($material_data['data'] as $datum){
            foreach ($datum['tag_id'] as $key=>$tag_id){
                $tag = "{$datum['tag'][$key]}_id_{$tag_id}";
                $good = "{$datum['name']}_id_{$datum['id']}";

                $tag = str_replace($tag_replace_str,'_',$tag);
                $data[$tag][] = [
                    "tag"=>$tag,
                    "good"=>$good,
                    "specification"=>$datum['specification'],
                    "unit"=>$datum['unit'],
                    "price"=>$datum['price'],
                ];
            }
        }

        foreach ($data as $tag => $export_datum){
            $good_data = array_column($export_datum,'good');
            $excelServer->setComboBoxData($good_data,'B',$offset,$limit,$tag,'A',function(\PHPExcel_Cell_DataValidation $objValidation){
                $objValidation->setShowErrorMessage(false);
                $objValidation->setErrorTitle('');
            });
        }

        return $excelServer;
    }
    // 自动发起物资进组流程
    private function automatic_process()
    {
        try {
            g('pkg_db') -> begin_trans();
            $data = parent::get_post_data(["user_id","materials"]);
            if (!is_array($data['materials'])){
                throw new \Exception("materials参数错误！");
            }
            $materials = [];
            foreach ($data['materials'] as $material){
                if (!isset($material['data_id']) || !isset($material['tag_id']) || !isset($material['number'])) {
                    throw new \Exception("materials参数错误！");
                }
                if ($material['number'] > 0) {
                    $materials[] = $material;
                }
            }

            $user_id = $data['user_id'];
            $user = g('dao_user') -> get_by_id($user_id);
            if (!$user) {
                throw new \Exception("申请人不存在");
            }
            $user_acct = $user['acct'];
            $user_name = $user['name'];

            // 获取物资入库表单id
            $form_id = $this->getMaterialWarehousingFormId(true);

            // 获取表单信息
            $form_data = $this->_get_form_detail([
                'form_id'=>$form_id
            ]);
            $input = $form_data['inputs'];

            $work_id = $form_data['work_id'];

            // 物品标签信息
            $material = $this->_getMaterialList();
            $material_datas = $material['idData'];
            $material_allTags= $material['idTag'];

            $material_management_config = load_config('model/process/model');
            $goods_table_key = $material_management_config["material_management"]["goods_table"];

            // 表单控件数据填充
            foreach ($input as $key => &$value) {
                switch ($value["type"]) {
                    case "people":
                        $value["ids"] = $user_id;
                        $value["val"] = $user_name;
                        break;
                    case "date":
                        $value["val"] = date('Y-m-d');
                        break;
                    default:
                        $value["val"] = "";     // 备注
                }
                // 物料
                if ($key == $goods_table_key) {
                    $recs = [];
                    foreach ($materials as $material) {
                        $tag_id = $material["tag_id"];
                        $data_id = $material["data_id"];
                        $number = $material["number"];

                        $material_data = isset($material_datas[$data_id])?$material_datas[$data_id]:[];
                        $specification = $material_data?$material_data['specification']:"";
                        $unit = $material_data?$material_data['unit']:"";
                        $price = $material_data?$material_data['price']:"";
                        $rec = $value['table_arr'];
                        foreach ($rec as &$tb) {
                            switch ($tb["type"]) {
                                case "wuzhi_chose":
                                    $materiaProperty = $material_datas[$data_id];
                                    $tb['materiaProperty'] = $materiaProperty;
                                    $tb['cat'] = $material_allTags[$tag_id];
                                    $tb["value"] = [
                                        "name" => $materiaProperty['name'],
                                        "history_id" => $materiaProperty['history_id'],
                                        "id" => $materiaProperty['id'],
                                    ];
                                    $tb["val"] = $materiaProperty['name'];
                                    break;
                                case "text":
                                    if ($tb["name"] == "数量" && $tb["must"] == 1) {
                                        $tb["val"] = $number;
                                    } else {
                                        $tb["val"] = "";
                                    }
                                    break;
                                case "wuzhi_attr":
                                    switch ($tb["name"]){
                                        case self::$input_specification_name:
                                            $tb["val"] = $specification;
                                            break;
                                        case self::$input_unit_name:
                                            $tb["val"] = $unit;
                                            break;
                                        case self::$input_price_name:
                                            $tb["val"] = $price;
                                            break;
                                    }
                                    break;
                                default:
                                    $tb["val"] = "";
                                    break;
                            }
                        }
                        $recs[] = $rec;
                    }
                    $value['rec'] = $recs;
                }
            }
            $form_names = $this->_get_form_name($form_id);
            $form_name = [];
            foreach ($form_names as $name){
                $form_name[] = $name['name'];
            }
            $form_name = implode('-',$form_name);
            // 保存流程节点
            $params_105 = [
                "form_id" => $form_id,
                "form_name" => $form_name,
                "work_id" => $work_id,
                "form_vals" => $input,
                "files" => [],
            ];
            $workitem = $this->_save_workitem($params_105);
            $workitem_info = $workitem['info'];
            // 获取下一步流程节点信息
            $node_data = $this->_get_next_node([
                "workitem_id"=>$workitem_info['workitem_id']
            ]);
            // 发送表单流程
            $this->_send_workitem([
                "workitem_id"=>$workitem_info['workitem_id'],
                "work_nodes"=>$node_data
            ]);

            g('pkg_db') ->commit();
            parent::echo_ok([]);
        } catch (\Exception $e) {
            g('pkg_db') -> rollback();
            parent::log_e('自动发起物资流程失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }

    /**
     * 获取物资表单
     * gch add material
     * @access private
     * @param
     * @return
     */
    private function _getMaterialList($data=[]) {

        $id = !empty($data['id']) ? intval($data['id']) : 0;
        $tagId = !empty($data['tag_id']) && is_array($data['tag_id']) ? $data['tag_id'] : array();
        $tagId = array_filter($tagId);
        $keyword = !empty($data['keyword']) ? trim($data['keyword']) : '';
        $page = !empty($data['page']) ? intval($data['page']) : 0;
        $pageSize = !empty($data['page_size']) ? intval($data['page_size']) : 0;
        // 固定获取真实物资
        // $type = !empty($data['type']) ? intval($data['type']) : self::$MATERIAL_REAL;
        $type = self::$MATERIAL_REAL;

        // 获取表单
        $materialFields = array(
            'id', 'name','describe','icon','history_id','creater_id','create_name','create_time',
        );
        $withCnt = $page == 0 && $pageSize == 0 ? false : true;
        $order = ' m.update_time desc ';
        $cond = array(
            'm.type=' => $type,
        );
        if (!empty($id)) {
            $cond['m1.id='] = $id;
            $withCnt = false;
        }
        if (!empty($keyword)) {
            // $key = "^(m.`name` like '%{$keyword}%' OR m.`describe` like '%{$keyword}%' OR m.create_name like '%{$keyword}%' OR EXISTS ";
            // $val = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.`name` like '%{$keyword}%'))";
            // $cond[$key] = $val;
            $cond["__OR_1"] = array(
                'm.`name` like ' => "%{$keyword}%",
                'm.`describe` like ' => "%{$keyword}%",
                'm.create_name like ' => "%{$keyword}%",
            );
        }
        if (!empty($tagId)) {
            $tagId = array_map('intval', $tagId);
            $tagSql = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.id IN (" . implode(',', $tagId) . ") AND t.info_state=1)";
            $cond['^EXISTS '] = $tagSql;
        }

        $materials = g('dao_material') -> getMaterialList($this -> com_id, $cond, $materialFields, $page, $pageSize, $order, $withCnt);
        if (!empty($materials)) {
            $tmp_data = $withCnt ? $materials['data'] : $materials;

            $materialIds = array_column($tmp_data, 'id');
            $tmp_data = array_combine($materialIds, $tmp_data);

            // 获取物资属性
            $materialLabel = g('dao_material') -> getMaterialLabel($this -> com_id, $materialIds);

            // 获取表单标签
            $tags = g('dao_tag') -> getTagByMaterialId($this -> com_id, $materialIds);

            // 同一表单的标签
            $formTags = array();
            $formTags_str = array();
            foreach ($tags as $tag) {
                if (!isset($formTags[$tag['material_id']])) {
                    $formTags[$tag['material_id']] = array();
                }

                $formTags[$tag['material_id']][] = $tag['id'];
                $formTags_str[$tag['material_id']][] = $tag['name'];
            }

            // 标签拼接到表单数据中
            foreach ($tmp_data as &$form) {
                if (isset($formTags[$form['id']])) {
                    $form['tag'] = $formTags_str[$form['id']];
                    $form['tag_id'] = $formTags[$form['id']];
                } else {
                    $form['tag'] = array("默认标签");
                    $form['tag_id'] = array(0);
                }


                $form['inputs'] = isset($materialLabel[$form['id']]) ? $materialLabel[$form['id']] : array();
            }
            unset($form);

            $tmp_data = array_values($tmp_data);
            if ($withCnt) {
                $materials['data'] = $tmp_data;
            } else {
                $materials = $tmp_data;
            }
        }

        // 获取所有标签
        $tag_fields = array(
            '`id`', '`name`',
        );
        $allTag = g('dao_tag') -> list_data_by_cond($this -> com_id, array(), $tag_fields, 0, 0, '', '', self::$TAG_TYPE, false);

        // 物品id对应数据
        $material_datas = [];
        // 物品id对应标签
        $material_allTags = [];

        foreach ($materials as &$data){
            // 获取物资规格，单位，价格
            $specification = "";
            $unit = "";
            $price = "";
            foreach ($data['inputs'] as $input){
                switch ($input['name']){
                    case self::$input_specification_name:
                        $specification = isset($input['the_value'])?$input['the_value']:'';
                        break;
                    case self::$input_unit_name:
                        $unit = isset($input['the_value'])?$input['the_value']:'';
                        break;
                    case self::$input_price_name:
                        $price = isset($input['the_value'])?$input['the_value']:'';
                        break;
                }
            }
            $data['specification'] = $specification;
            $data['unit'] = $unit;
            $data['price'] = $price;

            $material_datas[$data['id']] = $data;
        }unset($data);

        foreach ($allTag as $data){
            $material_allTags[$data['id']] = $data;
        }

        $info = array(
            'data' => $materials,
            'allTag' => $allTag,
            'idData' => $material_datas,
            'idTag' => $material_allTags
        );

        return $info;
    }
    /**
	* 获取物资表单
	* gch add material  
	* @access private
	* @param   
	* @return   
	*/	
	private function getMaterialList() {
		$need = array();
		$data = parent::get_post_data($need);

		$id = !empty($data['id']) ? intval($data['id']) : 0;
		$tagId = !empty($data['tag_id']) && is_array($data['tag_id']) ? $data['tag_id'] : array();
		$tagId = array_filter($tagId);
		$keyword = !empty($data['keyword']) ? trim($data['keyword']) : '';
		$page = !empty($data['page']) ? intval($data['page']) : 0;
		$pageSize = !empty($data['page_size']) ? intval($data['page_size']) : 0;
		// 固定获取真实物资
		// $type = !empty($data['type']) ? intval($data['type']) : self::$MATERIAL_REAL;
		$type = self::$MATERIAL_REAL;
		
		// 获取表单
		$materialFields = array(
			'id', 'name','describe','icon','history_id','creater_id','create_name','create_time',
		);
		$withCnt = $page == 0 && $pageSize == 0 ? false : true;
		$order = ' m.update_time desc ';
		$cond = array(
			'm.type=' => $type,
		);
		if (!empty($id)) {
			$cond['m1.id='] = $id;
			$withCnt = false;
		}
		if (!empty($keyword)) {
			// $key = "^(m.`name` like '%{$keyword}%' OR m.`describe` like '%{$keyword}%' OR m.create_name like '%{$keyword}%' OR EXISTS ";
			// $val = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.`name` like '%{$keyword}%'))";
			// $cond[$key] = $val;
			$cond["__OR_1"] = array(
				'm.`name` like ' => "%{$keyword}%",
				'm.`describe` like ' => "%{$keyword}%",
				'm.create_name like ' => "%{$keyword}%",
			);
		}
		if (!empty($tagId)) {
			$tagId = array_map('intval', $tagId);
			$tagSql = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.id IN (" . implode(',', $tagId) . ") AND t.info_state=1)";
			$cond['^EXISTS '] = $tagSql;
		}
		
		$materials = g('dao_material') -> getMaterialList($this -> com_id, $cond, $materialFields, $page, $pageSize, $order, $withCnt);
		if (!empty($materials)) {
			$tmp_data = $withCnt ? $materials['data'] : $materials;

			$materialIds = array_column($tmp_data, 'id');
			$tmp_data = array_combine($materialIds, $tmp_data);

			// 获取物资属性
			$materialLabel = g('dao_material') -> getMaterialLabel($this -> com_id, $materialIds);

			// 获取表单标签
			$tags = g('dao_tag') -> getTagByMaterialId($this -> com_id, $materialIds);

			// 同一表单的标签
			$formTags = array();
			$formTags_str = array();
			foreach ($tags as $tag) {
				if (!isset($formTags[$tag['material_id']])) {
					$formTags[$tag['material_id']] = array();
				}

				$formTags[$tag['material_id']][] = $tag['id'];
				$formTags_str[$tag['material_id']][] = $tag['name'];
			}

			// 标签拼接到表单数据中
			foreach ($tmp_data as &$form) {
				if (isset($formTags[$form['id']])) {
					$form['tag'] = $formTags_str[$form['id']];
					$form['tag_id'] = $formTags[$form['id']];
				} else {
					$form['tag'] = array("默认标签");
					$form['tag_id'] = array(0);
				}


				$form['inputs'] = isset($materialLabel[$form['id']]) ? $materialLabel[$form['id']] : array();
			}
			unset($form);

			$tmp_data = array_values($tmp_data);
			if ($withCnt) {
				$materials['data'] = $tmp_data;
			} else {
				$materials = $tmp_data;
			}
		}

		// 获取所有标签
		$tag_fields = array(
			'`id`', '`name`',
		);
		$allTag = g('dao_tag') -> list_data_by_cond($this -> com_id, array(), $tag_fields, 0, 0, '', '', self::$TAG_TYPE, false);

        $material_form_data = g('dao_material')->get_material_form($this->getMaterialWarehousingFormId(),$this->getMaterialApplyFormId());

        foreach ($materials as &$material){
            $history_id = $material['history_id'];
            $material['remaining'] = isset($material_form_data[$history_id])?$material_form_data[$history_id]['remaining']:0;
        }

		$info = array(
			'data' => $materials,
			'allTag' => $allTag,
		);
		parent::echo_ok($info);
	}		

	/**
	 * 获取流程分类及分类下的表单
	 * @return 
	 */
	private function get_form_list() {
		try {
			$user_id = $this -> user_id;
			$com_id = $this -> com_id;

			// 获取用户常用表单
			$user_common_form = $this -> get_user_form($user_id, $com_id);
			// 获取用户可用表单
			$cls_and_form = $this -> get_cls_and_form($user_id, $com_id);

			$form_list = array();
			$data = array();
			if (!empty($user_common_form)) {
				$data['classify_id'] = (string)0;
				$data['dominated'] = (string)1;
				$data['classify_name'] = '你最近使用的';
				$data['form_count'] = (string)count($user_common_form);
				$data['form_list'] = $user_common_form;
				$form_list[] = $data;
			}
			unset($data);
			$form_list = array_merge($form_list, $cls_and_form);

			$info = array(
				'info' => array(
					'data' => $form_list,
					'count' => (string)count($form_list),
					), 
				);
			parent::echo_ok($info);
		} catch (\Exception $e) {
			parent::log_e('获取表单列表失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取用户常用表单（已弃用）
	 * @return 
	 */
	private function get_user_common_form() {
		try {
			$user_id = $this -> user_id;
			$com_id = $this -> com_id;

			parent::log_i('开始获取用户常用表单');

			$form_user = g('mv_form_user') -> get_common_form($user_id, $com_id);
			if (empty($form_user)) {
				$info = array(
					'info' => array(
						'data' => array(),
						'count' => (string)0,
						),
					);
				parent::log_i('用户常用表单为空');
				parent::echo_ok($info);
			}

			$tmp_form_user = array();
			foreach ($form_user as $fu) {
				$version = $fu['version'];
				if ($version == parent::$StateHistory) { //如果是历史版本，则更新为最新版本的form_id
					$public_form = g('mv_form') -> get_by_his_id($fu['form_history_id'], $com_id);
					if (!empty($public_form)) {
						$fu['form_id'] 		= $new_form_id 		= $public_form['id'];
						$fu['form_name'] 	= $new_form_name 	= $public_form['form_name'];
						$fu['form_describe'] = $new_form_describe = $public_form['form_describe'];
						$fu['scope'] 		= $new_form_scope 	= $public_form['scope'];
						
						// 判断新版本是否有权限发起
						$new_form_scope = json_decode($new_form_scope, TRUE);
						if (count($new_form_scope) == 0) {
							$dept_arr = array();
						} else {
							$dept_arr = g('dao_dept') -> list_by_ids($new_form_scope, '*', FALSE);
						}
						$user = g('dao_user') -> get_by_id($user_id);

						$is_apply = FALSE;
						if (count($dept_arr) == 0) {
							$is_apply = TRUE;
						} else {
							foreach ($dept_arr as $dept) {
								if (strstr($user['dept_tree'], '"'.$dept['id'].'"')) {
									$is_apply = TRUE;
									break;
								}
							}unset($dept);
						}

						if ($is_apply == FALSE) {
							g('mv_form_user') -> del_common_form($fu['form_history_id'], $user_id, $com_id);
						} else {
							// 更新本来旧的表单id
							g('mv_form_user') -> update_common_form_id($new_form_id, $fu['form_history_id'], $user_id, $com_id);
							$tmp_form_user[] = $fu;
						}
					}
				} else {
					$tmp_form_user[] = $fu;
				}

				if (count($tmp_form_user) >= self::$FormUserCount) break;
			}unset($fu);

			foreach ($tmp_form_user as $key => &$form) {
				$form['form_describe'] === NULL && $form['form_describe'] = '';

				unset($form['com_id']);
				unset($form['user_id']);
				unset($form['form_history_id']);
				unset($form['user_time']);
				unset($form['info_state']);
				unset($form['info_time']);
				unset($form['version']);
				unset($form['scope']);
			}unset($form);

			$info = array(
				'info' => array(
					'data' => $tmp_form_user,
					'count' => count($tmp_form_user),
					),
				);

			parent::log_i('成功获取用户常用表单');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			parent::log_e('获取用户常用表单失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}
    /**
     * 根据表单id获取表单信息
     * @return
     * @throws
     */
    private function _get_form_detail($data) {
        try {

            $form_id = (int)$data['form_id'];
            if (empty($form_id)) throw new \Exception('非法的参数！');

            $proc = $this -> get_form_by_id($form_id, $this -> com_id, $this -> user_id);

            $start_node = parent::get_start_node($proc["work_id"]);
            //查看那些控件可编辑
            $this -> get_form_edit($proc["inputs"], $start_node);
            $proc['inputs'] = json_decode($proc['inputs'], TRUE);
            /**
             * 人员控件默认选中值设置为发起人上级
             * Modify by gch
             */
            // ----start
            foreach ($proc['inputs'] as &$v) {
                if (isset($v['type']) && $v['type'] == 'people' && isset($v['default_type']) && $v['default_type'] == 3) {
                    !isset($v['default_val']) && $v['default_val'] = array();
                    $this -> get_user_leader_receiver_list($v['default_val'], $this -> UserId, 'id,name,pic_url as pic');
                }
            }unset($v);
            $proc['inputs'] = json_decode(json_encode($proc['inputs']),true);
            // ----end

            $proc['file_type'] = $start_node['file_type'];

            unset($proc['form_history_id']);
            unset($proc['version']);
            unset($proc['scope']);
            unset($proc['formsetinst_name']);

            return $proc;
        } catch (\Exception $e) {
            parent::log_e('获取表单信息失败，异常：[' . $e -> getMessage() . ']');
            throw $e;
        }
    }
	/**
	 * 根据表单id获取表单信息
	 * @return 
	 */
	private function get_form_detail() {
		try {
			$data = parent::get_post_data(array('form_id'));

			$form_id = (int)$data['form_id'];
			if (empty($form_id)) throw new \Exception('非法的参数！');

			$proc = $this -> get_form_by_id($form_id, $this -> com_id, $this -> user_id);

			$start_node = parent::get_start_node($proc["work_id"]);
            $proc['work_node_id'] = $start_node['id'];
			//查看那些控件可编辑
			$this -> get_form_edit($proc["inputs"], $start_node);
			$proc['inputs'] = json_decode($proc['inputs'], TRUE);
			/**
			* 人员控件默认选中值设置为发起人上级
			* Modify by gch
			*/
			// ----start
			foreach ($proc['inputs'] as &$v) {
				if (isset($v['type']) && $v['type'] == 'people' && isset($v['default_type']) && $v['default_type'] == 3) {
					!isset($v['default_val']) && $v['default_val'] = array();
					$this -> get_user_leader_receiver_list($v['default_val'], $this -> UserId, 'id,name,pic_url as pic');
				}
			}unset($v);
			$proc['inputs'] = json_decode(json_encode($proc['inputs']));
			// ----end

			$proc['file_type'] = $start_node['file_type'];

			unset($proc['form_history_id']);
			unset($proc['version']);
			unset($proc['scope']);
			unset($proc['formsetinst_name']);

			parent::echo_ok(array('info' => $proc));			
		} catch (\Exception $e) {
			parent::log_e('获取表单信息失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
 * 获取当前表单实例名称
 * @return
 */
    private function get_form_name() {
        try {
            $data = parent::get_post_data(array());

            $keep_form = false;

            $form_id = isset($data['form_id'])?$data['form_id']:0;
            $formsetinst_id = isset($data['formsetinst_id'])?$data['formsetinst_id']:0;
            if ($formsetinst_id) {
                $formsetinst = g("mv_formsetinst")->get_by_id($formsetinst_id, "form_id");
                $form_id = $formsetinst?$formsetinst['form_id']:0;
                $keep_form = true;
            }

            if (empty($form_id)) throw new \Exception('非法的参数！');

            $proc = $this -> get_form_by_id($form_id, $this -> com_id, $this -> user_id, $keep_form);
            $is_special = 0;
            $formset_name = parent::get_formset_by_num($proc, $is_special, $proc['app_version']);

            $info = array();
            $name = array();
            foreach ($formset_name as $key => $val) {
                $name['conf_key'] = (string)$key;
                $name['name'] = $val;
                $info[] = $name;
            }

            parent::echo_ok(array('info' => $info));
        } catch (\Exception $e) {
            parent::log_e('获取表单实例名称失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }
    /**
     * 获取当前表单实例名称
     * @return array
     * @throws
     */
    private function _get_form_name($form_id) {
        try {
            if (empty($form_id)) throw new \Exception('非法的参数！');

            $proc = $this -> get_form_by_id($form_id, $this -> com_id, $this -> user_id);
            $is_special = 0;
            $formset_name = parent::get_formset_by_num($proc, $is_special, $proc['app_version']);

            $info = array();
            $name = array();
            foreach ($formset_name as $key => $val) {
                $name['conf_key'] = (string)$key;
                $name['name'] = $val;
                $info[] = $name;
            }
            return $info;
        } catch (\Exception $e) {
            parent::log_e('获取表单实例名称失败，异常：[' . $e -> getMessage() . ']');
            throw $e;
        }
    }
    /**
     * 保存流程
     * @return
     * @throws
     */
    private function _save_workitem($data) {
        $form_history_id = 0;
        $begin_trans = 0;
        try {
            $needs = array('form_id', 'form_name', 'work_id', 'form_vals');
            // $data = parent::get_post_data($needs);

            !isset($data['judgement']) && $data['judgement'] = '';
            !isset($data['workitem_id']) && $data['workitem_id'] = 0;
            !isset($data['formsetInst_id']) && $data['formsetInst_id'] = 0;
            !isset($data['files']) && $data['files'] = array();

            $is_finish = FALSE;
            if (!empty($data['workitem_id'])) {
                $is_finish = parent::is_finish($data["workitem_id"]);
            }

            if ($is_finish) {
                parent::log_i('流程状态已变更');
                throw new \Exception('流程状态已变更，请返回待办列表！');
            }

            // 获取表单模板对应的应用版本
            $form = g('mv_form') -> get_by_id($data['form_id'], $this -> com_id, 'app_version, is_other_proc,form_history_id');
            $data['app_version'] = $form ? $form['app_version'] : 0;
            $data['is_other_proc'] = $form['is_other_proc'];

            /** 物资管理限制开始 */

            $form_history_id = $form['form_history_id'];
            $config = load_config("model/process/model");

            $material_management = $config['material_management'];
            if($form_history_id == $material_management['item_claim_form'] && !$data['workitem_id']){
                // 检测锁
                while (1){
                    if(!$this->save_workitem_lock(1,$this -> com_id, $form_history_id))
                        break;

                    sleep(1);
                }
                // 加锁
                $this->save_workitem_lock(2,$this -> com_id,  $form_history_id);

                //库存判断
                $material_form_data = g('dao_material')->get_material_form($this->getMaterialWarehousingFormId(),$this->getMaterialApplyFormId());
                $check_data = $data['form_vals'][$material_management['goods_table']]['rec'];
                $warning = 0;
                $warning2 = 0;
                $content1 = [];;
                $content2 = [];
                $user_list = [];
                foreach ($check_data as $item){
                    $warning_ = 0;
                    //用户请求
                    $goods_id = $item[$material_management['goods_input_key']]['value']['history_id'];
                    $number = $item[$material_management['number_input_key']]['val'];
                    //库存 remaining
                    $material = array_key_exists($goods_id,$material_form_data)?$material_form_data[$goods_id]:[];
                    $remaining = array_key_exists("remaining",$material)?$material['remaining']:0;
                    if(!$material || $number>$remaining){
                        //库存不足以给用户
                        $warning = 1;
                        $warning_data = [
                            "remain_num" =>$remaining,
                            "number"=>$number,
                            "val"=>$item[$material_management['goods_input_key']]['val'],
                        ];
                        $content1[] = $warning_data;
                        $warning_ = 1;
                    }
                    $material_worning = g('dao_material')->get_material_worning($this -> com_id,$goods_id);
                    $prewarning_value = $material_worning?$material_worning['warning_value']:0;

                    if(($remaining-$number)<=$prewarning_value && !$warning_){
                        //库存不足以给用户
                        $warning2 = 1;
                        $warning_data = [
                            "prewarning_value" =>$prewarning_value,
                            "number"=>$remaining-$number,
                            "val"=>$item[$material_management['goods_input_key']]['val'],
                        ];
                        $content2[] = $warning_data;
                    }
                }
                if($warning || $warning2){
                    $title = "物资预警";
                    $content = "";
                    $err_content = "";
                    if($content1){
                        $content .= "以下物资库存不足<br/>";
                        foreach ($content1 as $val){
                            $content .= $val['val']."需求：".$val['number'].",库存剩余：".$val['remain_num'].";<br/>";
                            $err_content .= $val['val']."需求：".$val['number'].",库存剩余：".$val['remain_num']."<br/>";
                        }
                    }
                    if(!$content1 &&  $content2){
                        $content .= "以下物资低于预警值<br/>";
                        foreach ($content2 as $val){
                            $content .= $val['val']."预警值：".$val['prewarning_value'].",库存剩余：".$val['number'].";<br/>";
                        }
                    }
                    $material_worning_user = g('dao_material')->get_material_worning_user( $this -> com_id,"warning_user_list");
                    $user_list = $material_worning_user?json_decode($material_worning_user['warning_user_list']):[];
                    $app_id = 23;

                    $url = MAIN_DYNAMIC_DOMAIN.'/index.php?model=index&a=msg_detail&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
                    $user_list && parent::send_news($title,str_replace("<br/>","\r\n",$content), $pic_url='', $url, $app_id, 9, 0, $user_list, $dept_list=array(), $msg_title='');

                    //用户提醒
                    if($warning)throw new \Exception("{$err_content}<br/>已通知管理员");

                }
            }
            /** 物资管理限制结束 */

            parent::log_i('开始保存信息');
            $ret = parent::save_workitem_parent($data);

            $info = array(
                'info' => array(
                    'workitem_id' => $ret['workitem_id'],
                    'formsetInst_id' => $ret['formsetInst_id'],
                ),
            );

            // 解锁
            $this->save_workitem_lock(3,$this -> com_id, $form_history_id);
            parent::log_i('保存流程信息成功');
            return $info;
        } catch (\Exception $e) {

            /** 物资管理限制解锁 */
            $form_history_id && $this->save_workitem_lock(3,$this -> com_id, $form_history_id);

            parent::log_e('保存流程失败，异常：[' . $e -> getMessage() . ']');
            throw $e;
        }
    }
	/**
	 * 保存流程
	 * @return 
	 */
	private function save_workitem() {
        $form_history_id = 0;
        $begin_trans = 0;
		try {
			$needs = array('form_id', 'form_name', 'work_id', 'form_vals');
			$data = parent::get_post_data($needs);

            to_log('Info','',json_encode($data));

			!isset($data['judgement']) && $data['judgement'] = '';
			!isset($data['workitem_id']) && $data['workitem_id'] = 0;
			!isset($data['formsetInst_id']) && $data['formsetInst_id'] = 0;
			!isset($data['files']) && $data['files'] = array();

            $check_material = isset($data['check_material'])?$data['check_material']:1;

			$custom_instance = isset($data['custom_instance'])?$data['custom_instance']:'';

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data["workitem_id"]);
			}

			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更，请返回待办列表！');
			}

			// 获取表单模板对应的应用版本
			$form = g('mv_form') -> get_by_id($data['form_id'], $this -> com_id, 'app_version, is_other_proc,form_history_id,formsetinst_name');
			$data['app_version'] = $form ? $form['app_version'] : 0;
			$data['is_other_proc'] = $form['is_other_proc'];

			//新增校验实例名是否一致
            $vue_formsetinst_name = !empty($data['new_formsetinst_name'])? $data['new_formsetinst_name']: $data["form_name"];
            $php_formsetinst_name = [];
            $format_names = explode(';',$form['formsetinst_name']);
            if($format_names && strpos($form['formsetinst_name'],'input')!==false ){
                $form_vals = is_array($data['form_vals'])?$data['form_vals']:json_decode($data['form_vals'],true);
                $tmp_custom_instance = is_array($custom_instance)?$custom_instance:json_decode($custom_instance,true);
                $tmp_custom_instance = array_column($tmp_custom_instance,'name','conf_key');
                foreach ($format_names as $tmp){
                    if(strpos($tmp,'input')!==false){
                        $tmp_val = '';
                        $input = isset($form_vals[$tmp])?$form_vals[$tmp]:'';
                        if($input){
                            switch ($input['type']){
                                case 'money':
                                    $tmp_val = isset($input['val'])?$input['val']:'';
                                    if($tmp_val!==''){
                                        switch ($input['money_type']){
                                            case 0:
                                                $tmp_val = '￥'.$tmp_val;
                                                break;
                                            case 1:
                                                $tmp_val = '$'.$tmp_val;
                                                break;
                                            case 2:
                                                $tmp_val = '€'.$tmp_val;
                                                break;
                                            case 3:
                                                $tmp_val = 'HK$'.$tmp_val;
                                                break;
                                        }
                                    }
                                    break;
                                case 'muselect':
                                    $tmp_val = !empty($input['val'])?str_replace(' ',' - ',trim($input['val'])):'';
                                    break;
                                case 'dangerous':
                                    $err = [];
                                    $snake_list = isset($input['snake_list'])?$input['snake_list']:[];
                                    $vals = isset($input['val'])?$input['val']:[];
                                    foreach ($vals as $k=>$v){
                                        if($v==2){
                                            !empty($snake_list[$k]) && $err[] = $snake_list[$k];
                                        }
                                    }
                                    $tmp_val = implode(';',$err);
                                    break;
                                case 'conflict':
                                    $tmp_val = !empty($input['val'])?str_replace(' - ','-',trim($input['val'])):'';
                                    break;
                                case 'date':
                                    switch ($input['format']){
                                        case 'date':
                                            $tmp_val = !empty($input['val'])?date('Y年m月d日',strtotime($input['val'])):'';
                                            break;
                                        case 'datetime':
                                            $tmp_val = !empty($input['val'])?date('Y年m月d日 H:i',strtotime($input['val'])):'';
                                            break;
                                        default:
                                            $tmp_val = !empty($input['val'])?date('H:i',strtotime(date('Y-m-d').' '.$input['val'])):'';
                                            break;
                                    }
                                    break;
                                case 'car':
                                    $tmp_car_arr = [];
                                    $val_list = isset($input['val_list'])?$input['val_list']:[];
                                    foreach ($val_list as $v){
                                        $name = isset($v['name'])?$v['name']:'';
                                        $driver_name = isset($v['driver']['name'])?trim($v['driver']['name']):'';
                                        $seat = trim($v['seat']);

                                        if($driver_name!=='' && $seat!==''){
                                            $tmp_car_arr[]=$name.'-'.$seat.'座 司机：'.$driver_name;
                                        }
                                        if($driver_name==='' && $seat!==''){
                                            $tmp_car_arr[]=$name.'-'.$seat.'座';
                                        }
                                        if($driver_name!=='' && $seat===''){
                                            $tmp_car_arr[]=$name.' 司机：'.$driver_name;
                                        }
                                        if($driver_name==='' && $seat===''){
                                            $tmp_car_arr[] = $name;
                                        }
                                    }

                                    $tmp_val = implode(';',$tmp_car_arr);
                                    break;

                                default:
                                    $tmp_val = isset($input['val'])?$input['val']:'';
                                    break;
                            }
                        }

                        $php_formsetinst_name[] = trim($tmp_val);
                    }else{
                        $php_formsetinst_name[] = isset($tmp_custom_instance[$tmp])?trim($tmp_custom_instance[$tmp]):'';
                    }
                }unset($tmp);


                to_log('Info','','前端:'.$vue_formsetinst_name);
                to_log('Info','','后端:'.implode('-',$php_formsetinst_name));

                if($vue_formsetinst_name!=implode('-',$php_formsetinst_name)){
                    throw new \Exception('实例名组成有误，请联系系统管理员！');
                }

            }



            /** 物资管理限制开始 */

            $form_history_id = $form['form_history_id'];
            $config = load_config("model/process/model");

            $material_management = $config['material_management'];
            if($check_material && $form_history_id == $material_management['item_claim_form'] /*&& !$data['workitem_id']*/ ){
                // 检测锁
                while (1){
                    if(!$this->save_workitem_lock(1,$this -> com_id, $form_history_id))
                        break;

                    sleep(1);
                }
                // 加锁
                $this->save_workitem_lock(2,$this -> com_id,  $form_history_id);

                //库存判断
                $material_form_data = g('dao_material')->get_material_form($this->getMaterialWarehousingFormId(),$this->getMaterialApplyFormId());
                $check_data = $data['form_vals'][$material_management['goods_table']]['rec'];
                $warning = 0;
                $warning2 = 0;
                $content1 = [];
                $content2 = [];
                $user_list = [];

                //对物资的数量进行归总
                $tmp_check_data = [];
                foreach ($check_data as $item){
                    //用户请求
                    $goods_id = $item[$material_management['goods_input_key']]['value']['history_id'];
                    $number = $item[$material_management['number_input_key']]['val'];

                    if(!isset($tmp_check_data[$goods_id])){
                        $tmp_check_data[$goods_id] = $item;
                    }else{
                        $tmp_check_data[$goods_id][$material_management['number_input_key']]['val'] += $number;
                    }
                }unset($item);
                $check_data = $tmp_check_data;

                foreach ($check_data as $item){
                    $warning_ = 0;
                    //用户请求
                    $goods_id = $item[$material_management['goods_input_key']]['value']['history_id'];
                    $number = $item[$material_management['number_input_key']]['val'];
                    //库存 remaining
                    $material = array_key_exists($goods_id,$material_form_data)?$material_form_data[$goods_id]:[];
                    $remaining = array_key_exists("remaining",$material)?$material['remaining']:0;
                    if(!$material || $number>$remaining){
                        //库存不足以给用户
                        $warning = 1;
                        $warning_data = [
                            "remain_num" =>$remaining,
                            "number"=>$number,
                            "val"=>$item[$material_management['goods_input_key']]['val'],
                        ];
                        $content1[] = $warning_data;
                        $warning_ = 1;
                    }
//                    $material_worning = g('dao_material')->get_material_worning($this -> com_id,$goods_id);
//                    $prewarning_value = $material_worning?$material_worning['warning_value']:0;

//                    if(($remaining-$number)<=$prewarning_value && !$warning_){
//                        //库存不足以给用户
//                        $warning2 = 1;
//                        $warning_data = [
//                            "prewarning_value" =>$prewarning_value,
//                            "number"=>$remaining-$number,
//                            "val"=>$item[$material_management['goods_input_key']]['val'],
//                        ];
//                        $content2[] = $warning_data;
//                    }
                }
                if($warning || $warning2){
                    $title = "物资预警";
                    $content = "";
                    $err_content = "";
                    if($content1){
                        $content .= "以下物资库存不足<br/>";
                        foreach ($content1 as $val){
                            $content .= $val['val']."需求：".$val['number'].",库存剩余：".$val['remain_num'].";<br/>";
                            $err_content .= $val['val']."需求：".$val['number'].",库存剩余：".$val['remain_num']."<br/>";
                        }
                    }
                    //预警推送需要调整到审批结束
//                    if(!$content1 &&  $content2){
//                        $content .= "以下物资低于预警值<br/>";
//                        foreach ($content2 as $val){
//                            $content .= $val['val']."预警值：".$val['prewarning_value'].",库存剩余：".$val['number'].";<br/>";
//                        }
//                    }
                    $material_worning_user = g('dao_material')->get_material_worning_user( $this -> com_id,"warning_user_list");
                    $user_list = $material_worning_user?json_decode($material_worning_user['warning_user_list']):[];
                    $app_id = 23;

                    $url = MAIN_DYNAMIC_DOMAIN.'/index.php?model=index&a=msg_detail&corpurl='.$_SESSION[SESSION_VISIT_CORP_URL];
                    $user_list && parent::send_news($title,str_replace("<br/>","\r\n",$content), $pic_url='', $url, $app_id, 9, 0, $user_list, $dept_list=array(), $msg_title='');

                    //用户提醒
                    if($warning)throw new \Exception("{$err_content}<br/>已通知管理员");

                }
            }
            /** 物资管理限制结束 */

            parent::log_i('开始保存信息');
            g('pkg_db') -> begin_trans();
            $begin_trans = 1;
			$ret = parent::save_workitem_parent($data);
			g('pkg_db') -> commit();

            // 保存自定义值
            if ($custom_instance  && strpos($form['formsetinst_name'],'input')!==false ) {
                $custom_instance = is_array($custom_instance)?json_encode($custom_instance):$custom_instance;
                $dao_val = new Mv_Formsetinst_Name_Val();
                $dao_val->save($this->com_id,$ret['formsetInst_id'],self::CUSTOM_INSTANCE,$custom_instance);
            }

			$info = array(
				'info' => array(
					'workitem_id' => $ret['workitem_id'],
					'formsetInst_id' => $ret['formsetInst_id'],
					),
				);

            // 解锁
            $this->save_workitem_lock(3,$this -> com_id, $form_history_id);
			parent::log_i('保存流程信息成功');
			parent::echo_ok($info);
		} catch (\Exception $e) {

            /** 物资管理限制解锁 */
            $form_history_id && $this->save_workitem_lock(3,$this -> com_id, $form_history_id);

            $begin_trans && g('pkg_db') -> rollback();
			parent::log_e('保存流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}
    /**
     * 发送流程
     * @return
     */
    private function _send_workitem($data) {
        try {
            // $needs = array('workitem_id', 'work_nodes');

            parent::log_i('开始发送流程');

            $ret = parent::send_workitem_parent($data);

            $msg = '';
            if ($ret['return_arr'][0] == 'TRUE') {
                $msg = '流程流转结束';
            } else {
                $msg = '流程发送成功，下一步骤为'. $ret['return_arr'][1] . ' 接收人为' . $ret['return_arr'][2];
            }

            parent::log_i($msg);
            return true;
        } catch (\Exception $e) {
            parent::log_e('发送流程失败，异常：[' . $e -> getMessage() . ']');
            throw $e;
        }
    }
	/**
	 * 发送流程
	 * @return 
	 */
	private function send_workitem() {
		try {
			$needs = array('workitem_id', 'work_nodes');
			$data = parent::get_post_data($needs);

			parent::log_i('开始发送流程');
			g('pkg_db') -> begin_trans();

			$ret = parent::send_workitem_parent($data);

            // 保存表单节点信息
            g('mv_workinst')->save_node($data['workitem_id'],$data['work_nodes'][0]);

			$msg = '';
			if ($ret['return_arr'][0] == 'TRUE') {
				$msg = '流程流转结束';
			} else {
				$msg = '流程发送成功，下一步骤为'. $ret['return_arr'][1] . ' 接收人为' . $ret['return_arr'][2];
			}
            g('pkg_db') -> commit();

			parent::log_i($msg);
			parent::echo_ok(['info'=>$ret]);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('发送流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}
	/**
	 * 获取下一工作步骤
	 * @return 
	 */
	private function get_next_node() {
		try {
            $data = parent::get_post_data(array('workitem_id'));

			$workitem_id = (int)$data['workitem_id'];
			if (empty($workitem_id)) throw new \Exception('非法的流程！');

			parent::log_i('开始获取下一工作步骤');

			// 判断是否为撤销流程，撤销流程从原流程获取节点信息
            list($is_revoke,$node_data) = g("mv_workinst")->check_revoke_workitem($workitem_id);
            if ($is_revoke) {
                parent::log_i('撤销流程：成功获取下一工作步骤');
                parent::echo_ok(array('info' => [
                    'is_send'=>true,
                    'node_list'=>[
                        $node_data['node_list']
                    ],
                    'workitem_id'=>$workitem_id,
                ]));
            }

			$next_node_id_arr = parent::get_next_workitem($workitem_id);
			$is_send = parent::is_send($next_node_id_arr);

			$fields = array('id', 'work_node_name', 'receive_array', 'select_array', 'is_end','relation_node_type');
			$info = array(
				'workitem_id' 	=> (string)$workitem_id,
				'is_send' 		=> $is_send === "TRUE" ? TRUE : FALSE,
				'node_list' 	=> array(),
				);

			foreach ($next_node_id_arr as $val) {
				$return = array(
					'id' 				=> 0,
					'work_node_name' 	=> '',
                    'relation_node_type'=> '',
					'is_end' 			=> FALSE,
					'select_array' 		=> array(),
					'receive_array' 	=> array(),
					);
				foreach ($val as $key => $node) {
					if (in_array($key, $fields)) {
						if ($key == 'select_array') {
							foreach ($node as $key1 => $u) {
								$tmp_data = array();
								$tmp_data['id'] = $u['id'];
								$tmp_data['name'] = $u['name'];
								$tmp_data['pic_url'] = $u['pic_url'];
								$return[$key][] = $tmp_data;
							}unset($u);
							unset($tmp_data);
						} elseif ($key == 'receive_array') {
							foreach ($node as $key2 => $u1) {
								$tmp_data = array();
								$tmp_data['id'] = $u1['id'];
								$tmp_data['name'] = $u1['name'];
								$tmp_data['pic_url'] = $u1['pic_url'];
								$return[$key][] = $tmp_data;
							}unset($u1);
							unset($tmp_data);
						} else {
							$return[$key] = $node;
						}
					}
				}unset($node);
				$info['node_list'][] = $return;
			}unset($val);

			parent::log_i('成功获取下一工作步骤');
            parent::echo_ok(array('info' => $info));
		} catch (\Exception $e) {
			parent::log_e('发送流程失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

    /**
     * 获取下一工作步骤
     * @return
     */
    private function _get_next_node($data) {
        try {
            $workitem_id = (int)$data['workitem_id'];
            if (empty($workitem_id)) throw new \Exception('非法的流程！');

            parent::log_i('开始获取下一工作步骤');

            // 判断是否为撤销流程，撤销流程从原流程获取节点信息
            list($is_revoke,$node_data) = g("mv_workinst")->check_revoke_workitem($workitem_id);
            if ($is_revoke) {
                parent::log_i('撤销流程：成功获取下一工作步骤');
                return [$node_data['node']];
            }

            $next_node_id_arr = parent::get_next_workitem($workitem_id);
            $is_send = parent::is_send($next_node_id_arr);

            $fields = array('id', 'work_node_name', 'receive_array', 'select_array', 'is_end');
            $info = array(
                'workitem_id' 	=> (string)$workitem_id,
                'is_send' 		=> $is_send === "TRUE" ? TRUE : FALSE,
                'node_list' 	=> array(),
            );

            foreach ($next_node_id_arr as $val) {
                $return = array(
                    'id' 				=> 0,
                    'work_node_name' 	=> '',
                    'is_end' 			=> FALSE,
                    'select_array' 		=> array(),
                    'receive_array' 	=> array(),
                );
                foreach ($val as $key => $node) {
                    if (in_array($key, $fields)) {
                        if ($key == 'select_array') {
                            foreach ($node as $key1 => $u) {
                                $tmp_data = array();
                                $tmp_data['id'] = $u['id'];
                                $tmp_data['name'] = $u['name'];
                                $tmp_data['pic_url'] = $u['pic_url'];
                                $return[$key][] = $tmp_data;
                            }unset($u);
                            unset($tmp_data);
                        } elseif ($key == 'receive_array') {
                            foreach ($node as $key2 => $u1) {
                                $tmp_data = array();
                                $tmp_data['id'] = $u1['id'];
                                $tmp_data['name'] = $u1['name'];
                                $tmp_data['pic_url'] = $u1['pic_url'];
                                $return[$key][] = $tmp_data;
                            }unset($u1);
                            unset($tmp_data);
                        } else {
                            $return[$key] = $node;
                        }
                    }
                }unset($node);
                $info['node_list'][] = $return;
            }unset($val);

            // 数据整理
            $node = [];
            foreach ($info["node_list"] as $value){
                $receiver = [];
                foreach ($value['receive_array'] as $item){
                    $receiver[] = [
                        "receiver_id"=>$item['id'],
                        "receiver_name"=>$item['name']
                    ];
                }
                $node[] = [
                    "id"=>$value["id"],
                    "receiver_arr"=>$receiver
                ];
            }
            parent::log_i('成功获取下一工作步骤');
            return $node;
        } catch (\Exception $e) {
            parent::log_e('发送流程失败，异常：[' . $e -> getMessage() . ']');
            throw $e;
        }
    }
	/**
	 * 我发起的流程列表（运行中/已结束/草稿）
	 * type: 0:运行中，1：已结束，2：草稿
	 * sort:1：流程实例名排序，2：表单名排序，0：时间倒序
	 * @return 
	 */
	private function get_mine_list() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page 		= isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name 	= isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort 		= isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by 	= isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 2) throw new \Exception('非法的流程类型！');

			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;
			
			$fields = array(
				'mpf.id', 'mpf.formsetinst_name', 'mpf.create_time', 'mpfm.form_name',
				' mpf.state'
				);
			$fields = implode(',', $fields);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($sort == 1) {
				$order = ' ORDER BY mpf.formsetinst_name ' . $by;
			} elseif ($sort == 2) {
				$order = ' ORDER BY mpfm.form_name ' . $by;
			} else {
				$order = ' ORDER BY mpf.id ' . $by;
			}

			// 获取旧版本应用数据
			$cond['app_version'] = 0;

			parent::log_i('开始获取我发起的流程列表');

			$list = g('mv_formsetinst') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type, $this -> IsProcType, $page, parent::$PageSize, $fields, $order, TRUE);

			parent::log_i('成功获取我发起的流程列表');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取我发起的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 我处理的流程列表（待办/已办）
	 * type：0：待办，1：已办
	 * @return 
	 */
	private function get_handler_list() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page 		= isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name 	= isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort 		= isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by 	= isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 1) throw new \Exception('非法的流程类型！');

			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;

			$fields = array(
				'mpw.id', 'mpw.formsetinit_id', 'mpf.formsetinst_name', 'mpf.create_time', 
				'mpfm.form_name', 'mpw.creater_id',  'mpw.creater_name', 
				'su.pic_url creater_pic_url', 'mpw.receive_time', 
				);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($type === 0) {
				array_push($fields, 'mpw.state');
				if ($sort == 1) {
					$order = ' ORDER BY mpw.creater_name ' . $by;
				} elseif ($sort == 2) {
					$order = ' ORDER BY mpf.create_time ' . $by;
				} elseif ($sort == 3) {
					$order = ' ORDER BY mpf.formsetinst_name ' . $by;
				} elseif ($sort == 4) {
					$order = ' ORDER BY mpw.state ' . $by;
				} else {
					$order = ' ORDER BY mpw.receive_time ' . $by;
				}
			} else {
				array_push($fields, 'mpf.state formsetinst_state');
				array_push($fields, 'mpw.complete_time');
				if ($sort == 1) {
					$order = ' ORDER BY a.creater_name ' . $by;
				} elseif ($sort == 2) {
					$order = ' ORDER BY a.create_time ' . $by;
				} elseif ($sort == 3) {
					$order = ' ORDER BY a.formsetinst_name ' . $by;
				} elseif ($sort == 4) {
					$order = ' ORDER BY a.formsetinst_state ' . $by;
				} elseif ($sort == 5) {
					$order = ' ORDER BY a.complete_time ' . $by;
				} else {
					$order = ' ORDER BY a.receive_time ' . $by;
				}
			}

			$fields = implode(',', $fields);

			// 获取旧版本应用数据
			$cond['app_version'] = 0;

			parent::log_i('开始获取我待办/已办的流程');
			
			$list = g('mv_workinst') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type,$this -> IsProcType, $page, parent::$PageSize, $fields, $order, TRUE);
			
			parent::log_i('成功获取我待办/已办的流程');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取我待办/已办的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 我知会的流程列表（未阅读、已阅读）
	 * type：0：未阅读，1：已阅读
	 * @return 
	 */
	private function get_notify_list() {
		try {
			$data = parent::get_post_data(array('type'));

			$type = (int)$data['type'];
			$page 		= isset($data['page']) ? (int)$data['page'] : 1;
			$pro_name 	= isset($data['pro_name']) ? trim($data['pro_name']) : '';
			$sort 		= isset($data['sort']) ? (int)$data['sort'] : 0;
			$sort_by 	= isset($data['order']) ? (int)$data['order'] : 0;

			if ($type < 0 || $type > 1) throw new \Exception('非法的流程类型！');

			$page < 1 && $page = 1;

			$cond = array();
			!empty($pro_name) && $cond['formsetinst_name'] = $pro_name;

			$fields = array(
				'mpn.id', 'mpn.formsetinst_id', 'mpn.formsetinst_name', 
				'mpfm.form_name', 'mpn.notify_time', 'mpn.send_id', 
				'mpn.send_name', 'su.pic_url send_pic_url'
				);
			$fields = implode(',', $fields);

			$order = '';
			$by = $sort_by === 0 ? ' ASC ' : ' DESC ';
			if ($sort == 1) {
				$order = ' ORDER BY mpf.create_time ' . $by;
			} elseif ($sort == 2) {
				$order = ' ORDER BY mpn.send_name ' . $by;
			} elseif ($sort == 3) {
				$order = ' ORDER BY mpf.formsetinst_name ' . $by;
			} elseif ($sort == 4) {
				$order = ' ORDER BY mpfm.form_name ' . $by;
			} else {	
				$order = ' ORDER BY mpn.notify_time ' . $by;
			}

			// 获取旧版本应用数据
			$cond['app_version'] = 0;
			
			parent::log_i('开始获取我知会的流程列表');

			$list = g('mv_notify') -> get_do_list($cond, $this -> com_id, $this -> user_id, $type, $this -> IsProcType, $page, parent::$PageSize, $fields, $order, TRUE);

			parent::log_i('成功获取我知会的流程列表');
			parent::echo_ok(array('info' => $list));
		} catch (\Exception $e) {
			parent::log_e('获取我知会的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取流程审批详情
	 * @return 
	 */
	private function get_process_detail() {
		$data = parent::get_post_data();
		$workitem_id 	= isset($data['workitem_id']) ? (int)$data['workitem_id'] : 0;
		$formsetinst_id = isset($data['formsetinst_id']) ? (int)$data['formsetinst_id'] : 0;
		$notify_id 		= isset($data['notify_id']) ? (int)$data['notify_id'] : 0;

		if ($workitem_id > 0) {
			// 获取我处理的流程审批详情
			$this -> get_do_workitem_detail($workitem_id);
		} elseif ($formsetinst_id > 0) {
			// 获取我发起的流程详情
			$this -> get_my_workitem_detail($formsetinst_id);
		} elseif ($notify_id > 0) {
			// 获取知会我的流程详情
			$this -> get_notify_workitem_detail($notify_id);
		} else {
			parent::log_e('非法的访问！');
			parent::echo_err($this -> busy, '非法的访问！');
		}
	}

	/**
	 * 获取流程图
	 * @return 
	 */
	private function get_process_pic() {
		try {
			$data = parent::get_post_data(array('formsetinst_id'));

			$formsetinst_id = (int)$data['formsetinst_id'];
			if (empty($formsetinst_id)) throw new \Exception('非法的流程！');

			parent::log_i('开始获取流程图');
			
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id);
			$work_model = parent::get_work_node($formsetinst['work_id']);
			$worknodes_arr = parent::get_pic_rule($formsetinst);

            $curr_handler = array();
            if ($formsetinst['state'] == 1) {
                //运行中的实例获取当前节点处理人信息
                $curr_handler = parent::get_workitems_by_nodeid($formsetinst_id, $formsetinst['curr_node_id']);
            }

			$info = array(
				'info' => array(
					'formsetinst_id' 	=> $formsetinst['id'],
					'formsetinst_name' 	=> $formsetinst['formsetinst_name'],
					'state' 			=> $formsetinst['state'],
					'work_pic' 			=> $work_model['work_pic_json'],
					'worknodes_arr' 	=> $worknodes_arr,
                    'curr_handler'		=> $curr_handler
					),
				);

			parent::log_i('成功获取流程图');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			parent::log_e('获取流程图失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 驳回开始步骤流程
	 * @return 
	 */
	private function callback_start() {
		try {
			$data = parent::get_post_data(array('workitem_id'));
			!isset($data['judgement']) && $data['judgement'] = '';

			parent::log_i('开始驳回意见信息');
			g('pkg_db') -> begin_trans();

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data['workitem_id']);
			}
			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更！');
			}

			parent::callback_start_parent($data);
			
			g('pkg_db') -> commit();
			parent::log_i('成功驳回开始步骤流程');
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('驳回开始步骤流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 退回上一步骤流程
	 * @return 
	 */
	private function callback_pre() {
		try {
			$data = parent::get_post_data(array('workitem_id'));
			!isset($data['judgement']) && $data['judgement'] = '';

			parent::log_i('开始驳回意见信息');
			g('pkg_db') -> begin_trans();

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data['workitem_id']);
			}
			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更！');
			}
			$ret = parent::callback_pre_parent($data);

			g('pkg_db') -> commit();

			if ($ret['errcode'] == 1) {
				$back_item = array(
					'workitem_id' 	=> $data['workitem_id'],
					'judgement' 	=> $data['judgement'],
					'back_item_list'=> array(),
					);
				foreach ($ret['callback_workitem'] as $key => $val) {
					$back = array(
						'item_id' 		=> $val['id'],
						'item_name' 	=> $val['workitem_name'],
						'handler_name' 	=> $val['handler_name'],
						'complete_time' => $val['complete_time'],
						);
					$back_item['back_item_list'][] = $back;
				}unset($val);
				unset($back);

				parent::log_i('选择流程退回步骤');
				parent::echo_err($ret['errcode'], $ret['errmsg'], 'info', $back_item);
			}

			parent::log_i('成功驳回上一步步骤流程');
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('退回上一步骤流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 驳回指定步骤流程
	 * @return 
	 */
	private function callback_select() {
		try {
			$needs = array('workitem_id', 'callback_workitem_id');
			$data = parent::get_post_data($needs);
			!isset($data['judgement']) && $data['judgement'] = '';
            !isset($data['judgement_desc']) && $data['judgement_desc'] = '';

			parent::log_i('开始驳回到选择的步骤');
			g('pkg_db') -> begin_trans();
//			$ret = parent::callback_select_parent($data);
            $ret = parent::callback_parent($data['workitem_id'], $data['callback_workitem_id'], $data['judgement'], $data['judgement_desc']);

			g('pkg_db') -> commit();
			parent::log_i('成功驳回到选择的步骤');
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('驳回到选择的流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 删除流程工作项
	 * @return 
	 */
	private function del_workitem() {
		try {
			$data = parent::get_post_data(array('workitem_id'));

			parent::log_i('开始删除工作项');
			g('pkg_db') -> begin_trans();

			$ret = parent::del_workitem_parent($data);
			if ($ret['errcode'] == 1) throw new \Exception($ret['errmsg']);
			
			g('pkg_db') -> commit();
			parent::log_i('成功删除工作项');
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('删除流程工作项失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 发送到退回步骤
	 * @return 
	 */
	private function sendback_workitem() {
		try {
			$data = parent::get_post_data(array('workitem_id'));
			!isset($data['judgement']) && $data['judgement'] = '';

			parent::log_i('开始发送到退回节点');
			g(pkg_db) -> begin_trans();

			$is_finish = FALSE;
			if (!empty($data['workitem_id'])) {
				$is_finish = parent::is_finish($data['workitem_id']);
			}
			if ($is_finish) {
				parent::log_i('流程状态已变更');
				throw new \Exception('流程状态已变更！');
			} 
			$ret = parent::sendback_workitem_parent($data);

			g('pkg_db') -> commit();
			parent::log_i('成功发送到退回节点');
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('发送到退回步骤失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 催办流程
	 * @return 
	 */
	private function press_workitem() {
		try {
			$data = parent::get_post_data(array('formsetinst_id'));
			parent::log_i('开始催办流程');

			parent::press_workitem_parent($data);

			parent::log_i('成功催办流程');
			parent::echo_ok();
		} catch (\Exception $e) {
			parent::log_e('催办流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 知会流程
	 * @return 
	 */
	private function notify_workitem() {
		try {
			$needs = array('formsetinst_id', 'receivers');
			$data = parent::get_post_data($needs);

			parent::log_i('开始知会流程');
			g('pkg_db') -> begin_trans();

			parent::notify_workitem_parent($data);

			g('pkg_db') -> commit();
			parent::echo_ok();
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('知会流程失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取领签url
	 * @return 
	 */
	private function get_linksign_url() {
		try {
			$needs = array('workitem_id', 'formsetinst_id');
			$data = parent::get_post_data($needs);

			$workitem_id 	= intval($data['workitem_id']);
			$formsetinst_id = intval($data['formsetinst_id']);
			$user_name 		= $_SESSION[SESSION_VISIT_USER_NAME];

			// 控制页面跳转参数
			$isIndex = isset($data['isIndex']) ? $data['isIndex'] : NULL;
			$menuType = isset($data['menuType']) ? $data['menuType'] : NULL;
			$hash = isset($data['hash']) ? $data['hash'] : NULL;

			$redirect = MAIN_DYNAMIC_DOMAIN."index.php?model=".$this->AppName."&m=ajax&a=index&cmd=120&workitem_id=".$workitem_id."&formsetinst_id=".$formsetinst_id;
			// 添加跳转参数
			if (!is_null($isIndex)) {
				$redirect .= "&isIndex=" . $isIndex;
			}
			if (!is_null($menuType)) {
				$redirect .= "&menuType=" . $menuType;
			}
			if (!is_null($hash)) {
				$redirect .= "&hash=" . urlencode($hash);
			}

			parent::log_i('开始获取领签url');
			$linksign = g("api_sign") -> get_sign_url($user_name,$redirect);
			if(empty($linksign)) throw new \Exception('链接为空！'); 

			$info = array(
				'info' => array(
					'redirect_url' => $linksign["redirect"],
					),
				);
			parent::log_i('成功获取领签url');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			parent::log_e('获取领签url失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 保存领签
	 * @return 
	 */
	private function get_save_linksign() {
		$workitem_id 	= $this -> pkg_val -> get_get("workitem_id");
		$workitem_id 	= intval($workitem_id);
		$formsetinst_id = $this -> pkg_val -> get_get("formsetinst_id");
		$formsetinst_id = intval($formsetinst_id);
		$signatureId 	= $this -> pkg_val -> get_get("signatureId");
		$cancel 		= $this -> pkg_val -> get_get("cancel");
		$isIndex 		= $this -> pkg_val -> get_get("isIndex");
		$menuType 		= $this -> pkg_val -> get_get("menuType");
		$hash 			= $this -> pkg_val -> get_get("hash");
		if ($hash !== FALSE)  $hash = urldecode($hash);

		if($cancel=="yes"){
			$send_url = "index.php?app=".$this->AppName."&a=detail&listType=1&activekey=0&id=".$workitem_id;
			
			// 跳转控制
			if ($isIndex !== FALSE) {
				$send_url .= "&isIndex=" . $isIndex;
			}
			if ($menuType !== FALSE) {
				$send_url .= "&menuType=" . $menuType;
			}
			if ($hash !== FALSE) {
				$send_url .= $hash;
			}

			header("Location:".$send_url);
		} else{
			try{
				$img_url = g("api_sign") -> get_image($signatureId);
				if(!empty($img_url)){
					parent::save_linksign($workitem_id, $img_url);
					$send_url = "index.php?app=".$this->AppName. "&a=sendPage&workitemId=" . $workitem_id . "&formsetInstId=". $formsetinst_id;
					// 跳转控制
					if ($isIndex !== FALSE) {
						$send_url .= "&isIndex=" . $isIndex;
					}
					if ($menuType !== FALSE) {
						$send_url .= "&menuType=" . $menuType;
					}
					if ($hash !== FALSE) {
						$send_url .= $hash;
					}

					header("Location:".$send_url);
				}
			}catch (\Exception $e){
				parent::log_e('保存领签url失败，异常：[' . $e -> getMessage() . ']');
				parent::echo_err($this -> busy, $e -> getMessage());
			}
		}
	}

	/**
	 * 下载附件
	 * @return 
	 */
	private function download_file() {
		try {
			$formsetinst_id =  (int)$this -> pkg_val -> get_get('formsetinst_id', FALSE);
			$cf_id =  (int)$this -> pkg_val -> get_get('cf_id', FALSE);
			$origin =  (int)$this -> pkg_val -> get_get('origin', FALSE);

			if (empty($cf_id) || empty($formsetinst_id)) {
				throw new \Exception('无效的文件对象！');
			}

			//仅当下载图片时生效是否下载原图
			$size = $origin == 1 ? -1 : '';

			parent::log_i('开始下载文件');
			$url = g('mv_file') -> download_file($cf_id, $this -> com_id, $formsetinst_id, $this -> IsProcType, $size);
			parent::log_i('下载文件成功');
			parent::echo_ok(array('url' => $url));
		} catch (\Exception $e) {
			parent::log_e('下载文件失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

    /**
     * 文件控件下载文件
     * @return
     */
    private function download_input_file() {
        try {
            $hash =  $this -> pkg_val -> get_get('hash', FALSE);
            $file_name =  $this -> pkg_val -> get_get('file_name', FALSE);
            $ext_name =  $this -> pkg_val -> get_get('ext_name', FALSE);

            if (empty($hash) &&  empty($file_name) && empty($ext_name) ) {
                throw new \Exception('缺少请求参数');
            }

            parent::log_i('开始下载文件');
            $url = $url = g('api_download') -> download_file('', $hash, $file_name, $ext_name, -1, TRUE);

            parent::log_i('下载文件成功');
            parent::echo_ok(array('url' => $url));
        } catch (\Exception $e) {
            parent::log_e('下载文件失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }

    /**
     * 终止流程
     */
    private function break_workitem(){
        try{
            $data = parent::get_post_data(array('workitem_id','judgement'));

            g('pkg_db')->begin_trans();

            $ret = parent::breakWorkitem($data);
            if(!$ret) throw new \Exception('操作失败');

            g('pkg_db')->commit();
            parent::echo_ok();
        }catch (\Exception $e){

            g('pkg_db')->rollback();
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }
    /**
     *  结束撤销流程
     */
    private function finish_revoke_workitem()
    {
        try{
            $data = parent::get_post_data(array('formsetinst_id','revoke_reason'));
            g('pkg_db') ->begin_trans();
            // 原表单发起撤销流程中
            $formsetinst = parent::revokeWorkitem($data['formsetinst_id'],true);

            $com_id = $this->com_id;
            $formsetinst_name = $formsetinst['formsetinst_name'];
            $formsetinst_id = $data['formsetinst_id'];
            $work_id = $formsetinst['work_id'];

            // 获取撤销流程表单id
            $dao_com_form = new ComForm();
            $form = $dao_com_form->getByComId($com_id,ComForm::TYPE_REVOKE);
            if (!$form) {
                throw new \Exception("未配置撤销流程表单功能，请联系管理员！");
            }
            $public_form = g('mv_form') -> get_by_his_id($form['form_history_id'], $com_id);
            $form_id = $public_form['id'];

            // 保存流程节点
            $params_105 = [
                "form_id" => $form_id,
                "form_name" => "撤销申请-{$formsetinst_name}",
                "work_id" => $work_id,
                "form_vals" => g('mv_formsetinst')->getRevokeFormsetinstLabel($formsetinst_name,$data['revoke_reason']),
                "files" => [],
            ];
            $workitem = $this->_save_workitem($params_105);
            $workitem_info = $workitem['info'];
            // 撤销实例关联原表单实例
            g('mv_formsetinst') ->update_relate_formsetinst_id($workitem_info['formsetInst_id'],$formsetinst_id);
            // 获取下一步流程节点信息
            $node_data = $this->_get_next_node([
                "workitem_id"=>$workitem_info['workitem_id']
            ]);
            // 发送表单流程
            $this->_send_workitem([
                "workitem_id"=>$workitem_info['workitem_id'],
                "work_nodes"=>$node_data
            ]);

            g('pkg_db') ->commit();
            parent::echo_ok([]);
        } catch (\Exception $e) {
            g('pkg_db') -> rollback();
            parent::log_e('自动发起物资流程失败，异常：[' . $e -> getMessage() . ']');
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }
    /**
     * 撤销流程
     */
    private function revoke_workitem(){
        try{
            $data = parent::get_post_data(array('formsetinst_id'));

            g('pkg_db')->begin_trans();

            parent::revokeWorkitem($data['formsetinst_id']);

            g('pkg_db')->commit();
            parent::echo_ok();
        }catch (\Exception $e){

            g('pkg_db')->rollback();
            parent::echo_err($this -> busy, $e -> getMessage());
        }
    }
	/**
	 * 打印流程
	 * @return 
	 */
	private function print_form() {
		try {
			$data = parent::get_post_data(array('formsetinst_id'));
			$formsetinst_id = intval($data['formsetinst_id']);

			if (empty($formsetinst_id)) throw new \Exception('非法的流程！');
			parent::log_i('开始打印预览流程审批');
			// 获取表单实例信息
			$form_fields = array(
				'mpf.id', 'mpf.work_id','mpf.formsetinst_name', 'mpf.state', 'mpf.create_time', 'mpfm.id form_id', 
				'mpfm.form_name', 'mpfm.form_describe', 'mpfm.is_linksign', 'mpf.form_vals', 'mpf.info_state', 
				'mpf.workitem_rule', 'mpf.creater_name', 'mpf.creater_dept_name'
				);
			$form_fields = implode(',', $form_fields);
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id, $form_fields);
			
			$form_vals = parent::get_form_vals($formsetinst_id);
			// 处理NULL值问题
			foreach ($formsetinst as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);

            $start_node = g('mv_work_node')->getStartNode($formsetinst['work_id'],'id');
            $start_node_id = isset($start_node['id'])?$start_node['id']:'';

			// 获取已经审批的步骤
			$workitem_list = parent::get_workitem_list_parent($formsetinst_id);
			$workitems = array();
			if (!empty($workitem_list)) {
				foreach ($workitem_list as $key => $workitem) {
					foreach ($workitem['workitem_list'] as $key1 => $work) {
						if (!empty($work['judgement']) || $work['work_type'] == 1) {
							$data = array(
								'state' 		=> $work['state'],
								'workitem_name' => $work['workitem_name'],
								'work_type' 	=> $work['work_type'],
								'is_linksign' 	=> $work['is_linksign'],
								'handler_name' 	=> $work['handler_name'],
								'complete_time' => $work['complete_time'],
								'judgement' 	=> $work['judgement'] ? $work['judgement'] : '',
								'target_node' 	=> $work['target_node'],
                                'is_start'      => $work['work_node_id'] == $start_node_id?1:0,
								);
							$workitems[] = $data;
						}
					}unset($work);
				}unset($workitem);
			}

			// 获取实例的附件信息
			$files_list = g('mv_file') -> get_files($this -> com_id, $formsetinst_id);

			$files = array();
			if (!empty($files_list)) {
				foreach ($files_list as $key => $file) {
					$data = array(
						'file_name' 	=> $file['file_name'],
						'create_name' 	=> $file['create_name'],
						'create_time' 	=> $file['create_time'],
						'work_node_name' => $file['work_node_name'],
						);
					$files[] = $data;
				}unset($file);
			}

			$return = array(
				'id' 				=> $formsetinst['id'],
				'state' 			=> $formsetinst['state'],
				'form_name' 		=> $formsetinst['form_name'],
				'formsetinst_name' 	=> $formsetinst['formsetinst_name'],
				'create_name' 		=> $formsetinst['creater_name'],
				'depts' 			=> $formsetinst['creater_dept_name'] ? $formsetinst['creater_dept_name'] : '',
				'create_time' 		=> $formsetinst['create_time'],
				'print_name'		=> $_SESSION[SESSION_VISIT_USER_NAME],
				'print_time'		=> (string)time(),
				'linksign_url' 		=> $this -> LinksignUrl,
				'form_vals' 		=> $form_vals,
				'files' 			=> $files,
				'workitems' 		=> $workitems,
				);

			parent::log_i('成功打印预览流程审批');
			parent::echo_ok(array('info' => $return));
		} catch (\Exception $e) {
			parent::log_e('流程审批打印预览失败，异常：【' . $e -> getMessage() . '】');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	*	检测是否有冲突,有则返回冲突信息
	*	./index.php?model=process&m=ajax&cmd=124
	*	@access private
	*	@param int form_id 表单id 
	*	@param int formsetinst_name 表单实例名称 
	*	@param int form_vals 控件 
	*	@return array
	*	@author gch 2017-03-30
	*/
	private function check_conflict() {
		try {
			$needs = array('form_id', 'form_name', 'form_vals');
			$data = parent::get_post_data($needs);

			parent::log_i('开始检查表单实例是否有冲突');
			// 表单实例信息
			$formsetinst = $data;
			// 表单模板id
			$form_id = intval($formsetinst['form_id']);
			// 表单实例名称
			// $formsetinst_name = $formsetinst['form_name'];
			// 表单实例id
			$formsetinst_id = intval($formsetinst['formsetInst_id']);

			// 获取需要检查的表单实例信息
			// 时间段
			$conflict_time_start = array();
			$c_conflict_time_start = array();
			$conflict_time_end = array();
			$c_conflict_time_end = array();
			// 冲突对象
			$conflict_object = array();
			$c_conflict_object = array();
            $conflict_vehicle_assignment_object = [];  // $formsetinst['form_vals']['input_key'] 冲突控件绑定的input_key得到
            // 座位建议提醒
            $conflict_seat_data = [];
            foreach ($formsetinst['form_vals'] as $key => $value) {
				if (strcmp($value['type'], 'conflict') == 0 || strcmp($value['type'], 'table') == 0) {
				    // 检测子表单是否有冲突控件
    				if (strcmp($value['type'], 'table') == 0) {
    					$child_data = $value['rec'];
    					foreach ($child_data as $ck => $cv) {
    					    foreach ($cv as $ck1 => $cv1) {
        						if (strcmp($cv1['type'], 'conflict') == 0) {
            						$c_time_val = explode(' - ', $cv1['val']);
            						if ( empty($c_time_val[0]) || empty($c_time_val[1]) ) {
            							continue;
            						}
            						$c_conflict_time_start[$ck1] = $this->strToTime($c_time_val[0]);
            						$c_conflict_time_end[$ck1] = $this->strToTime($c_time_val[1]);
                                    $c_conflict_object[$ck1] = $cv1['conflict_type'];
            						// 绑定车辆控件；目前暂不开发
        						}
    					    }
    					}
    				}
                    // 检测表单是否有冲突控件
    				if (strcmp($value['type'], 'conflict') == 0) {
    					$time_val = explode(' - ', $value['val']);
    					if ( empty($time_val[0]) || empty($time_val[1]) ) {
    						continue;
    					}

    					$conflict_time_start[$key] = $this->strToTime($time_val[0]);
    					$conflict_time_end[$key] = $this->strToTime($time_val[1]);
                        // 绑定车辆控件
    					if (isset($value['bindkey']) && $value['bindkey'] && $formsetinst['form_vals'][$value['bindkey']]['type'] == 'car') {
                            $conflict_vehicle_assignment_object[$key] = $formsetinst['form_vals'][$value['bindkey']];
                        } else {
                            $conflict_object[$key] = $value['conflict_type'];
                        }
    				}
				}

                // 座位限制提醒
                // 检查是否有车辆控件
                if (strcmp($value['type'], 'car') == 0 && count($value['val_list']) == 1 && isset($formsetinst['form_vals'][$value['bindkey']]) && intval($formsetinst['form_vals'][$value['bindkey']]['val']) > 0) {
                    // 获取绑定的车辆人数
                    $number = intval($formsetinst['form_vals'][$value['bindkey']]['val']);

                    $val = current($value['val_list']);
                    $diff_number = $val['seat'] - $number;
                    $bast_val = [];
                    foreach ($value['car_list'] as $car){
                        $car_diff_number = $car['seat'] - $number;
                        if ($car['seat'] >= $number && ($car_diff_number < $diff_number)) {
                            $bast_val = $car;
                            $diff_number = $car_diff_number;
                        }
                    }

                    // 获取同座的车辆
                    $str_bast_val = [];
                    foreach ($value['car_list'] as $car){
                        if ($bast_val['seat'] == $car['seat']) {
                            $seat_str = $car['seat']?"-{$car['seat']}座":"";
                            $str_bast_val[] = "{$car['name']}{$seat_str}";
                        }
                    }

                    if ($bast_val) {
                        $seat_str = $val['seat']?"-{$val['seat']}座":"";
                        $conflict_seat_data[] = [
                            'title' => "座位限制提醒",
                            'number' => $number,
                            'obj' => "{$val['name']}{$seat_str}",
                            'advise_obj' => implode('、',$str_bast_val),
                            'type' => 4,
                        ];
                    }
                }
			}unset($value);

			if (empty($conflict_object) && empty($c_conflict_object) && empty($conflict_vehicle_assignment_object)) {
				$info = array(
					'flag' 	=> $conflict_seat_data?1:0,//1为冲突,0为没有冲突
					'data' 	=> array(),
                    'vehicle_data' 	=> [],
                    'driver_data' 	=> [],
                    'seat_data' 	=> $conflict_seat_data,
				);
				parent::echo_ok($info);
			}

            $conflict_object = array_merge($conflict_object, $c_conflict_object);
            $conflict_time_start = array_merge($conflict_time_start, $c_conflict_time_start);
            $conflict_time_end = array_merge($conflict_time_end, $c_conflict_time_end);
            $conflict_data = array();
            $conflict_vehicle_data = array();
            $conflict_driver_data = array();

            $conflict_id_arr = array();
            // 获取历史版本id
            $form_history_ret = g('mv_form') -> get_by_id($form_id, $this -> ComId, 'form_history_id');
            $form_history_id = $form_history_ret['form_history_id'];
            // 获取全部有可能冲突的控件
            $formsetinst_info = g('ser_process_report') -> get_conflict_by_id($form_history_id);


            foreach ($formsetinst_info as $v) {
                $other = json_decode($v['other'], true);
                // 是否是冲突控件
                if ($other['is_conflict'] != 1) {
                    continue;
                }
                // 防止自身冲突
                if ($formsetinst_id == $v['id']) {
                    continue;
                }
                // 去除重复
                if (in_array($v['id'], $conflict_id_arr)) {
                    continue;
                }
                $conflict_id_arr[] = $v['id'];

                // 控件开始时间&结束时间
                $ctime_val = explode(' - ', $v['val']);
                if ( empty($ctime_val[0]) || empty($ctime_val[1]) ) {
                    continue;
                }
                $ctime_start = $this->strToTime($ctime_val[0]);
                $ctime_end = $this->strToTime($ctime_val[1]);

                if (isset($other['bindkey'])) {
                    // 获取已有时间冲突绑定的车辆控件
                    $car_input = g('ser_process_report') -> get_formsetinst_input($v['id'],$other['bindkey']);
                    $car_input_other = json_decode($car_input['other'],true);

                    foreach ($conflict_vehicle_assignment_object as $k2=>$object) {
                        // 当前选择的车辆
                        $selected_vehicle = array_column($object['val_list'],'name');
                        // 当前选择的司机
                        $selected_driver = array_column($object['val_list'],'driver');
                        $selected_driver_ids = array_column($selected_driver,'id');

                        // - 时间冲突
                        if ($conflict_time_end[$k2] < $ctime_start || $conflict_time_start[$k2] > $ctime_end) {
                            continue;
                        }
                        if ($v['creater_id'] == $this->user_id) {
                            $conflict_data[] = array(
                                'obj' =>"",
                                'title' => "时间冲突",
                                'time' => $v['val'],
                                'name' => $v['creater_name'],
                                'type' => 1,
                            );
                        }

                        // - 车辆冲突
                        foreach ($car_input_other['val_list'] as $item){
                            if (in_array($item['name'],$selected_vehicle)) {
                                $seat_str = $item['seat']?"-{$item['seat']}座":"";
                                $conflict_vehicle_data[] = [
                                    'title' => "车辆冲突",
                                    'obj' =>"{$item['name']}{$seat_str}",
                                    'time' => $v['val'],
                                    'name' => $v['creater_name'],
                                    'driver'=>$item['driver']['name'],
                                    'type' => 2,
                                ];
                            }
                        }
                        // - 司机冲突
                        foreach ($car_input_other['val_list'] as $item){
                            if (in_array($item['driver']['id'],$selected_driver_ids)){
                                $seat_str = $item['seat']?"-{$item['seat']}座":"";
                                $conflict_driver_data[] = [
                                    'title' => "司机冲突",
                                    'obj' =>"{$item['name']}{$seat_str}",
                                    'time' => $v['val'],
                                    'name' => $v['creater_name'],
                                    'driver'=>$item['driver']['name'],
                                    'type' => 3,
                                ];
                            }
                        }
                    }
                } else {
                    foreach ($conflict_object as $k2 => $value) {
                        // 冲突对象是否相同
                        if ($other['conflict_type'] != $value) {
                            continue;
                        }
                        // 时间冲突
                        if ($conflict_time_end[$k2] < $ctime_start || $conflict_time_start[$k2] > $ctime_end) {
                            continue;
                        }
                        $conflict_data[] = array(
                            'obj' => $value,
                            'time' => $v['val'],
                            'name' => $v['creater_name'],
                        );
                    }
                }
            }

            $info = array(
                'flag' 	=> ($conflict_data || $conflict_vehicle_data || $conflict_driver_data || $conflict_seat_data) ? 1 : 0,//1为冲突,0为没有冲突
                'data' 	=> $conflict_data,
                'vehicle_data' 	=> $conflict_vehicle_data,
                'driver_data' 	=> $conflict_driver_data,
                'seat_data' 	=> $conflict_seat_data,
            );
			parent::echo_ok($info);
		} catch (\Exception $e) {
			parent::log_e('检查表单实例是否有冲突失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}

	}

	private function strToTime($str){
        $str = str_replace(['年','月'],'-',$str);
        $str = str_replace(['日'],' ',$str);
	    return strtotime($str);
    }


//----------------------------------------------------------------------------------------------------------
    //获取审批流程节点内容
    private function getWrokitemJudgement($formsetinst_id,$workitems,&$judgement,&$work_control){
        //获取表单实例详情内容
        $formsetinst = $this->get_formsetinst_by_id($formsetinst_id);
        $start_node = g('mv_work_node')->getStartNode($formsetinst['work_id'],'id');
        $start_node_id = isset($start_node['id'])?$start_node['id']:'';

        //获取期望完成时间，备注
        foreach ($workitems as $key => $work) {
            $w_data = array(
                'workitem_name' => $work['workitem_name'],
                'state' => $work['state'],
            );
            foreach ($work['workitem_list'] as $key1 => $w) {
                if (
                    (!empty($w['judgement']) && $w['pre_work_node_id'] && in_array($w['state'],self::$StateWorkitemJudgement) )
                    or
                    ( !empty($w['judgement']) && !empty($formsetinst['finish_time_remark']) && $w['work_node_id']==$start_node_id && in_array($w['state'],self::$StateWorkitemJudgement) )
                ) {
                    $j_data = array(
                        'handler_id'	=> $w['handler_id'],
                        'handler_name'	=> $w['handler_name'],
                        'handler_pic_url'=> $w['pic_url'],
                        'state'			=> $w['state'],
                        'judgement'		=> $w['judgement'],
                        'workitem_name'	=> $w['workitem_name'],
                        'work_type'		=> $w['work_type'],
                        'is_linksign'	=> $w['is_linksign'],
                        'linksign_url'	=> $this -> LinksignUrl,
                        'complete_time'	=> $w['complete_time'],
                    );
                    $judgement[] = $j_data;
                }
                $w_data['workitem_list'][] = array(
                    'handler_id' 		=> $w['handler_id'],
                    'handler_name' 		=> $w['handler_name'],
                    'handler_pic_url' 	=> $w['pic_url'],
                    'state' 			=> $w['state'],
                    'complete_time' 	=> $w['complete_time'] ? $w['complete_time'] : '',
                );
            }unset($w);
            $work_control[] = $w_data;
        }unset($work);
    }

	/**
	 * 获取我处理的流程审批详情
	 * @return 
	 */
	private function get_do_workitem_detail($workitem_id) {
		try {
			if (empty($workitem_id)) throw new \Exception('非法的流程！');
			
			parent::log_i('开始获取流程详情');
			g('pkg_db') -> begin_trans();

			// 获取当前工作步骤信息
			$cur_fields = array(
				'mpw.id', 'mpw.is_returnback', 'mpwn.is_start', 'mpw.work_node_id', 'mpwn.file_type',
				'mpw.handler_id', 'mpw.handler_name', 'mpw.creater_id', 'mpw.creater_name', 
				'su.pic_url creater_pic_url', 'mpwn.work_type', 'mpw.state', 'mpwn.work_node_describe', 
				'mpw.info_state', 'mpw.formsetinit_id', 'mpwn.input_visble_rule', 'mpwn.input_visit_rule',
                'mpwn.work_id','mpwn.break_state','mpwn.break_button_name','mpwn.revoke_state','mpwn.revoke_button_name', 'mpwn.notify_state','mpwn.notify_button_name',
                'mpwn.finish_revoke_state','mpwn.check_dangerous_image','mpwn.approval_button_name','mpwn.work_type',
				);
			$cur_fields = implode(',', $cur_fields);
			$cur_workitem = parent::get_curr_workitem($workitem_id, $cur_fields);

			// 代办无撤销操作
            $cur_workitem['revoke_state'] = 0;
            $cur_workitem['finish_revoke_state'] = 0;

            // 处理NULL值问题
			foreach ($cur_workitem as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);

			$formsetinst_id = $cur_workitem['formsetinit_id'];
			// 获取表单实例信息
			$form_fields = array(
				'mpf.id', 'mpf.work_id','mpf.formsetinst_name', 'mpf.state', 'mpf.create_time', 'mpfm.id form_id', 
				'mpfm.form_name', 'mpfm.form_describe', 'mpfm.is_linksign', 'mpf.form_vals', 'mpf.info_state', 'mpf.is_other_proc',
                'mpf.relate_formsetinst_id','mpf.finish_time','mpf.finish_time_remark','mpfm.complete_date_status'
				);
			$form_fields = implode(',', $form_fields);
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id, $form_fields);
			if( !empty($formsetinst['form_id']) ){
                $formsetinst['form_rule'] = $this->getFormRule($this->com_id,$formsetinst['form_id']);
            }
            // 获取关联实例状态
            $relate_formsetinst =  g("mv_formsetinst")->get_by_id($formsetinst['relate_formsetinst_id']);
            $formsetinst['relate_formsetinst_state'] = $relate_formsetinst['info_state'];

			// 获取关联工作项ID
            $formsetinst['relate_workitem_id'] = g('mv_workinst')->getLastWorkitemId($formsetinst['relate_formsetinst_id'],$this->user_id);

            // 撤销流程无退回操作
            $formsetinst['no_back'] = $formsetinst['relate_formsetinst_id']?true:false;
            $formsetinst['no_notice'] = $formsetinst['relate_formsetinst_id']?true:false;

            // 撤销流程显示驳回
            $dao_com_form = new ComForm();
            $revoke_form_id = $dao_com_form->getRevokePublicFormId($this->com_id);
            if ($revoke_form_id && $revoke_form_id == $formsetinst['form_id']){
                $cur_workitem['break_state'] = self::$BreakYes;
                $cur_workitem['break_button_name'] = self::$BreakButtonName;
            }
            // 非待办不显示驳回
            if (!in_array($cur_workitem['state'],Mv_Workinst::$StateWorkingWorkitem)) {
                $cur_workitem['break_state'] = self::$BreakNo;
            }
            // 非运行中不显示驳回 (通过的需判断权限)
            if ($formsetinst['state'] != self::$StateFormDone) {
                $cur_workitem['break_state'] = self::$BreakNo;
            }
			// 已通过时，驳回权限判断
            if ($formsetinst['state'] == self::$StateFormFinish) {
                $end_node =  g('mv_work_node') -> get_end_by_workid($this->ComId, $formsetinst['work_id']);
                $cur_workitem['break_state'] = $end_node['break_state'];
                $cur_workitem['break_button_name'] = $end_node['break_button_name'];
                if ($end_node['break_state'] == self::$BreakYes) {
                    $break_user_list = json_decode($end_node['break_user_list'],true);
                    $break_user_list = $break_user_list ? array_column($break_user_list,'id') : [];
                    $cur_workitem['break_state'] = in_array($this->user_id,$break_user_list) ? self::$BreakYes : self::$BreakNo;
                }
            }

            $default_approval_button_name = $cur_workitem['work_type'] == 1?'确认':'同意';
            $cur_workitem['approval_button_name'] = $cur_workitem['approval_button_name']?$cur_workitem['approval_button_name']:$default_approval_button_name;

			$form_vals = parent::get_form_vals($formsetinst_id);
			$formsetinst['form_vals'] = json_encode($form_vals);
			// 处理NULL值问题
			foreach ($formsetinst as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);
			
			// 查看那些控件可编辑
			$this -> get_form_edit($formsetinst['form_vals'], $cur_workitem);
			$formsetinst['form_vals'] = json_decode($formsetinst['form_vals'], TRUE);

			//特殊表单控件处理
			parent::change_rest_input($formsetinst['is_other_proc'], $cur_workitem['is_start'], $cur_workitem['is_returnback'], $formsetinst['form_vals']);			

			// 查看附件内容
			$files_fields = array(
				'id', 'file_name', 'file_url file_path', 'file_key file_hash', 'extname file_ext', 
				'work_node_id', 'create_id', 'create_name'
				);
			$files_fields = implode(',', $files_fields);
			$files = g('mv_file') -> get_file_by_formsetid($this -> com_id, $formsetinst_id, $files_fields);

			if ($cur_workitem['state'] == parent::$StateNew) {
				// 更新工作状态
				g('mv_workinst') -> update_workitem_state($cur_workitem['id'], $this -> com_id, parent::$StateRead);
			}

			// 获取已经审批的步骤
			$workitems = parent::get_workitem_list_parent($formsetinst_id);

			// 处理意见
			$judgement = array();
			// 流程监控
			$work_control = array();
			$this->getWrokitemJudgement($formsetinst_id,$workitems,$judgement,$work_control);

            // 撤销状态判断
            // if ($cur_workitem['break_state'] == Mv_Work_Node::$BreakStateOff && count($work_control) == 2) {
            //     $is_self = $cur_workitem['is_start'] == Mv_Work_Node::$StateStart && $this->user_id = $cur_workitem['creater_id'];
            //     $is_self && $cur_workitem['break_state'] = Mv_Work_Node::$BreakStateOn;
            // }
//			foreach ($workitems as $key => $work) {
//				$w_data = array(
//					'workitem_name' => $work['workitem_name'],
//					'state' => $work['state'],
//					);
//				foreach ($work['workitem_list'] as $key1 => $w) {
//					if (!empty($w['judgement']) && $w['pre_work_node_id'] && ($w['state'] == 7 || $w['state'] == 8)) {
//						$j_data = array(
//							'handler_id'	=> $w['handler_id'],
//							'handler_name'	=> $w['handler_name'],
//							'handler_pic_url'=> $w['pic_url'],
//							'state'			=> $w['state'],
//							'judgement'		=> $w['judgement'],
//							'workitem_name'	=> $w['workitem_name'],
//							'work_type'		=> $w['work_type'],
//							'is_linksign'	=> $w['is_linksign'],
//							'linksign_url'	=> $this -> LinksignUrl,
//							'complete_time'	=> $w['complete_time'],
//							);
//						$judgement[] = $j_data;
//					}
//
//					$w_data['workitem_list'][] = array(
//						'handler_id' 		=> $w['handler_id'],
//						'handler_name' 		=> $w['handler_name'],
//						'handler_pic_url' 	=> $w['pic_url'],
//						'state' 			=> $w['state'],
//						'complete_time' 	=> $w['complete_time'] ? $w['complete_time'] : '',
//						);
//				}unset($w);
//				$work_control[] = $w_data;
//			}unset($work);
			
			// 获取知会信息
			$notify_fields = array(
				'mpn.id', 'mpn.notify_id', 'mpn.notify_name', 'su.pic_url notify_pic_url', 'mpn.state'
				);
			$notify_fields = implode(',', $notify_fields);
			$notifys = g('mv_notify') -> get_notify_by_formset_id($formsetinst_id, $this -> com_id, $notify_fields);

			// 去除不必要字段
			unset($cur_workitem['info_state']);
			unset($cur_workitem['formsetinit_id']);
			unset($cur_workitem['input_visble_rule']);
			unset($cur_workitem['input_visit_rule']);
			unset($formsetinst['info_state']);

			//是否开启指纹审批
			$verify_finger = parent::check_fingerprint(true);
			// gch add
			// 检查是否冲突
			// $conflict_info = $this -> check_conflict($formsetinst);

            // 获取自定义控件值
            $dao_val = new Mv_Formsetinst_Name_Val();
            $custom_instance = $dao_val->get($this->com_id,$formsetinst_id,self::CUSTOM_INSTANCE);

			$info = array(
				'info' => array(
					'workitem' 		=> $cur_workitem,
					'formsetinst' 	=> $formsetinst,
					'files' 		=> $files,
					'judgement' 	=> $judgement,
					'notifys' 		=> $notifys,
					'workitems' 	=> $work_control,
					'conflict_info' => $conflict_info,
					'fingerprint'	=> $verify_finger ? '1' : '0',
                    'custom_instance' => $custom_instance?$custom_instance['name_value']:'',
					), 

				);
			parent::log_i('成功获取流程详情');
			g('pkg_db') -> commit();

			// 检查领签是否过期，若过期则把当前节点更改为普通审批模式
			if ($formsetinst['is_linksign'] == 1) {
				try {
					$check = g('api_sign') -> check_privirage();
				} catch (\Exception $e) {
					$check = FALSE;
				}
				!$check && $info['info']['formsetinst']['is_linksign'] = (string)0;
			}

			parent::echo_ok($info);			
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('获取流程详情失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}
	/**
	 * 获取我发起的流程详情
	 * @return 
	 */
	private function get_my_workitem_detail($formsetinst_id) {
		try {
			if (empty($formsetinst_id)) throw new \Exception('非法的流程类型！');
	
			parent::log_i('开始获取流程详情');
			g('pkg_db') -> begin_trans();

			// 获取表单实例信息
			$form_fields = array(
				'mpf.id', 'mpf.work_id','mpf.formsetinst_name', 'mpf.state', 'mpf.create_time', 'mpfm.id form_id', 
				'mpfm.form_name', 'mpfm.form_describe', 'mpfm.is_linksign', 'mpf.form_vals', 'mpf.info_state', 
				'mpf.workitem_rule', 'mpf.is_other_proc','mpf.relate_formsetinst_id','mpf.finish_time','mpf.finish_time_remark','mpfm.complete_date_status'
				);
			$form_fields = implode(',', $form_fields);
			$formsetinst = parent::get_formsetinst_by_id($formsetinst_id, $form_fields);
			if( !empty($formsetinst['form_id']) ){
                $formsetinst['form_rule'] = $this->getFormRule($this->com_id,$formsetinst['form_id']);
            }
			// 获取关联实例状态
            $relate_formsetinst =  g("mv_formsetinst")->get_by_id($formsetinst['relate_formsetinst_id']);
            $formsetinst['relate_formsetinst_state'] = $relate_formsetinst['info_state'];

            $formsetinst['no_back'] = $formsetinst['relate_formsetinst_id']?true:false;
            $formsetinst['no_notice'] = $formsetinst['relate_formsetinst_id']?true:false;

			$form_vals = parent::get_form_vals($formsetinst_id);
			$formsetinst['form_vals'] = json_encode($form_vals);
			// 处理NULL值问题
			foreach ($formsetinst as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);
			// 获取当前工作步骤信息
			$start_fields = array(
				'mpw.id', 'mpw.is_returnback', 'mpwn.is_start', 'mpw.work_node_id', 'mpwn.file_type',
				'mpw.handler_id', 'mpw.handler_name', 'mpw.creater_id', 'mpw.creater_name', 
				'su.pic_url creater_pic_url', 'mpwn.work_type', 'mpw.state', 'mpwn.work_node_describe', 
				'mpw.info_state', 'mpw.formsetinit_id', 'mpwn.input_visble_rule', 'mpwn.input_visit_rule',
                'mpwn.revoke_state','mpwn.revoke_button_name','mpwn.finish_revoke_state','mpwn.notify_state','mpwn.notify_button_name',
            );
			$start_fields = implode(',', $start_fields);
//			$start_workitem = parent::get_start_item($formsetinst['workitem_rule'], $formsetinst_id, $start_fields);
            $start_workitem = parent::get_start_item($formsetinst['work_id'], $formsetinst_id, $start_fields);

            if ($formsetinst['state'] != self::$StateFormDone) {
                $start_workitem['revoke_state'] = self::$RevokeNo;
            }

            if ($formsetinst['state'] != self::$StateFormFinish) {
                $start_workitem['finish_revoke_state'] = self::$RevokeNo;
            }

            if ($formsetinst['relate_formsetinst_id']) {
                $start_workitem['revoke_state'] = self::$RevokeNo;
                $start_workitem['finish_revoke_state'] = self::$RevokeNo;
            }


			// 处理NULL值问题
			foreach ($start_workitem as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);

			// 查看附件内容
			$files_fields = array(
				'id', 'file_name', 'file_url file_path', 'file_key file_hash', 'extname file_ext', 
				'work_node_id', 'create_id', 'create_name'
				);
			$files_fields = implode(',', $files_fields);
			$files = g('mv_file') -> get_file_by_formsetid($this -> com_id, $formsetinst_id, $files_fields);



            // 获取自定义控件值
            $dao_val = new Mv_Formsetinst_Name_Val();
            $custom_instance = $dao_val->get($this->com_id,$formsetinst_id,self::CUSTOM_INSTANCE);

			// 草稿
			if ($formsetinst['state'] == parent::$StateFormDraft) {
				// 查看哪些控件可编辑
				$this -> get_form_edit($formsetinst['form_vals'], $start_workitem);
				$formsetinst['form_vals'] = json_decode($formsetinst['form_vals'], TRUE);

				unset($start_workitem['formsetinit_id']);
				unset($start_workitem['info_state']);
				unset($start_workitem['input_visble_rule']);
				unset($start_workitem['input_visit_rule']);
				unset($formsetinst['info_state']);
				unset($formsetinst['workitem_rule']);

				$return = array(
					'workitem' 		=> $start_workitem,
					'formsetinst' 	=> $formsetinst,
					'files' 		=> $files,
                    'custom_instance' => $custom_instance?$custom_instance['name_value']:"",
					);
			} else {
				if ($start_workitem['state'] == parent::$StateNew) {
					// 更新工作状态
					g('mv_workinst') -> update_workitem_state($start_workitem['id'], $this -> com_id, parent::$StateRead);
				}
				// 查看哪些控件可编辑
				$this -> get_form_edit($formsetinst['form_vals'], $start_workitem);
				$formsetinst['form_vals'] = json_decode($formsetinst['form_vals'], TRUE);

				//特殊表单控件处理
				parent::change_rest_input($formsetinst['is_other_proc'], $start_workitem['is_start'], $start_workitem['is_returnback'], $formsetinst['form_vals']);	

				// 获取已经审批的步骤
				$workitems = parent::get_workitem_list_parent($formsetinst_id);

				// 处理意见
				$judgement = array();
				// 流程监控
				$work_control = array();
                $this->getWrokitemJudgement($formsetinst_id,$workitems,$judgement,$work_control);

//				foreach ($workitems as $key => $work) {
//					$w_data = array(
//						'workitem_name' => $work['workitem_name'],
//						'state' => $work['state'],
//						);
//					foreach ($work['workitem_list'] as $key1 => $w) {
//						if (!empty($w['judgement']) && $w['pre_work_node_id'] && ($w['state'] == 7 || $w['state'] == 8)) {
//							$j_data = array(
//								'handler_id'	=> $w['handler_id'],
//								'handler_name'	=> $w['handler_name'],
//								'handler_pic_url'=> $w['pic_url'],
//								'state'			=> $w['state'],
//								'judgement'		=> $w['judgement'],
//								'workitem_name'	=> $w['workitem_name'],
//								'work_type'		=> $w['work_type'],
//								'is_linksign'	=> $w['is_linksign'],
//								'linksign_url'	=> $this -> LinksignUrl,
//								'complete_time'	=> $w['complete_time'],
//								);
//							$judgement[] = $j_data;
//						}
//
//						$w_data['workitem_list'][] = array(
//							'handler_id' 		=> $w['handler_id'],
//							'handler_name' 		=> $w['handler_name'],
//							'handler_pic_url' 	=> $w['pic_url'],
//							'state' 			=> $w['state'],
//							'complete_time' 	=> $w['complete_time'] ? $w['complete_time'] : '',
//							);
//					}unset($w);
//					$work_control[] = $w_data;
//				}unset($work);

				// 获取知会信息
				$notify_fields = array(
					'mpn.id', 'mpn.notify_id', 'mpn.notify_name', 'su.pic_url notify_pic_url', 'mpn.state'
					);
				$notify_fields = implode(',', $notify_fields);
				$notifys = g('mv_notify') -> get_notify_by_formset_id($formsetinst_id, $this -> com_id, $notify_fields);

				unset($start_workitem['info_state']);
				unset($start_workitem['formsetinit_id']);
				unset($start_workitem['input_visble_rule']);
				unset($start_workitem['input_visit_rule']);
				unset($formsetinst['info_state']);
				unset($formsetinst['workitem_rule']);

				$return = array(
					'workitem' 		=> $start_workitem,
					'formsetinst' 	=> $formsetinst,
					'files' 		=> $files,
					'judgement' 	=> $judgement,
					'notifys' 		=> $notifys,
					'workitems' 	=> $work_control,
                    'custom_instance' => $custom_instance?$custom_instance['name_value']:'',
					);
			}
			
			parent::log_i('成功获取流程详情');
			g('pkg_db') -> commit();

			parent::echo_ok(array('info' => $return));
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('获取我发起的流程详情失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取知会我的流程详情
	 * @return 
	 */
	private function get_notify_workitem_detail($notify_id) {
		try {
			if (empty($notify_id)) throw new \Exception('非法的知会信息！');

			parent::log_i('开始获取流程详情');
			g('pkg_db') -> begin_trans();

			$fields = array(
				'mpn.id', 'mpf.work_id','mpn.formsetinst_id', 'mpn.formsetinst_name', 'mpf.state formsetinst_state', 
				'mpn.send_id', 'mpn.send_name', 'mpn.notify_time', 'mpn.state', 'mpf.creater_id', 
				'mpf.creater_name', 'su.pic_url creater_pic_url', 'mpf.create_time', 'mpfm.id form_id', 
				'mpfm.form_name',  'mpfm.form_describe', 'mpf.form_vals', 'mpn.info_state', 'mpf.is_other_proc','mpf.finish_time','mpf.finish_time_remark',
                'mpfm.complete_date_status'
				);
			$fields = implode(',', $fields);
			$notify = parent::get_notify_item($notify_id, $fields);
			if( !empty($notify['form_id']) ){
                $notify['form_rule'] = $this->getFormRule($this->com_id,$notify['form_id']);
            }
			$form_vals = parent::get_form_vals($notify['formsetinst_id']);
			$notify['form_vals'] = json_encode($form_vals);
			// 处理NULL值问题
			foreach ($notify as &$val) {
				if ($val === NULL) {
					$val = '';
				}
			}unset($val);

			// 查看哪些控件可编辑
			$this -> get_form_edit($notify['form_vals'], NULL);
			$notify['form_vals'] = json_decode($notify['form_vals'], TRUE);

			/**
			* 隐藏知会控件
			* Modify by gch
			*/
			// ----start
			  
			// $notify_input_keys = g('ser_process_report') -> get_notify_inputs($notify['work_id']);
			// if (!empty($notify_input_keys)) {
			// 	$tmp_form_vals = array();
			// 	foreach ($notify['form_vals'] as $k => $v) {
			// 		if (in_array($k, $notify_input_keys)) {
			// 			continue;
			// 		}
            //
			// 		$tmp_form_vals[$k] = $v;
			// 	}unset($v);
			// 	$notify['form_vals'] = $tmp_form_vals;
			// }
			
			// ----end
			

			if ($notify['state'] == parent::$StateReading) {
				parent::update_notify_state_parent($notify_id);
			}
			// 获取已经审批的步骤
			$workitems = parent::get_workitem_list_parent($notify['formsetinst_id']);

			// 处理意见
			$judgement = array();
			// 流程监控
			$work_control = array();
            $this->getWrokitemJudgement($notify['formsetinst_id'],$workitems,$judgement,$work_control);

//			foreach ($workitems as $key => $work) {
//				$w_data = array(
//					'workitem_name' => $work['workitem_name'],
//					'state' => $work['state'],
//					);
//				foreach ($work['workitem_list'] as $key1 => $w) {
//					if (!empty($w['judgement']) && $w['pre_work_node_id'] && ($w['state'] == 7 || $w['state'] == 8)) {
//						$j_data = array(
//							'handler_id'	=> $w['handler_id'],
//							'handler_name'	=> $w['handler_name'],
//							'handler_pic_url'=> $w['pic_url'],
//							'state'			=> $w['state'],
//							'judgement'		=> $w['judgement'],
//							'workitem_name'	=> $w['workitem_name'],
//							'work_type'		=> $w['work_type'],
//							'is_linksign'	=> $w['is_linksign'],
//							'linksign_url'	=> $this -> LinksignUrl,
//							'complete_time'	=> $w['complete_time'],
//							);
//						$judgement[] = $j_data;
//					}
//
//					$w_data['workitem_list'][] = array(
//						'handler_id' 		=> $w['handler_id'],
//						'handler_name' 		=> $w['handler_name'],
//						'handler_pic_url' 	=> $w['pic_url'],
//						'state' 			=> $w['state'],
//						'complete_time' 	=> $w['complete_time'] ? $w['complete_time'] : '',
//						);
//				}unset($w);
//				$work_control[] = $w_data;
//			}unset($work);

			// 知会信息
			$notify_fields = array(
				'mpn.id', 'mpn.notify_id', 'mpn.notify_name', 'su.pic_url notify_pic_url', 'mpn.state'
				);
			$notify_fields = implode(',', $notify_fields);
			$notifys = g('mv_notify') -> get_notify_by_formset_id($notify['formsetinst_id'], $this -> com_id, $notify_fields);

			// 获取附件信息
			$files_fields = array(
				'id', 'file_name', 'file_url file_path', 'file_key file_hash', 'extname file_ext', 
				'work_node_id', 'create_id', 'create_name'
				);
			$files_fields = implode(',', $files_fields);
			$files = g('mv_file') -> get_file_by_formsetid($this -> com_id, $notify['formsetinst_id'], $files_fields);

			unset($notify['info_state']);
			$info = array(
				'info' => array(
					'notify'		=> $notify,
					'files' 		=> $files,
					'judgement' 	=> $judgement,
					'notifys' 		=> $notifys,
					'workitems' 	=> $work_control,
					),
				);
			g('pkg_db') -> commit();
			parent::log_i('成功获取流程详情');
			parent::echo_ok($info);
		} catch (\Exception $e) {
			g('pkg_db') -> rollback();
			parent::log_e('获取知会流程详情失败，异常：[' . $e -> getMessage() . ']');
			parent::echo_err($this -> busy, $e -> getMessage());
		}
	}

	/**
	 * 获取流程分类及分类下的表单
	 */
	private function get_cls_and_form($user_id, $com_id) {
		parent::log_i('开始获取分类及表单');

		$classify_list = g('mv_form') -> get_classify($com_id, $user_id);
		if (empty($classify_list)) {
			parent::log_i('分类为空');
			return array();
		}

		$fields = array(
			'id form_id', 'form_name', 'form_describe', 'scope'
			);
		$fields = implode(',', $fields);
		foreach ($classify_list as $key => &$classify) {
			$form_list = g('mv_form') -> get_form_by_classify($classify['classify_id'], $com_id, $user_id, $fields);

			if (!$form_list || count($form_list) == 0) {
				unset($classify_list[$key]);
				continue;
			}

			// 员工可见流程
			$user = g('dao_user') -> get_by_id($user_id);
			$dept_tree = json_decode($user['dept_tree'], TRUE);

			$depts = array();
			foreach ($dept_tree as $dept) {
				$depts = array_merge($depts, $dept);
			}unset($dept);

			//遍历所有流程
			foreach ($form_list as $form_key => &$form_model) {
				$form_model['form_describe'] === NULL && $form_model['form_describe'] = '';
				$scope = json_decode($form_model['scope'], TRUE);

				if (count($scope) == 0) { 
				// 企业内可见	
					unset($form_list[$form_key]);
				} else {
					$is_contain = FALSE;
					foreach ($scope as $s) { 
					// 记录可见流程
						if (in_array($s, $depts)) {
							$is_contain = TRUE;
							break;
						}
					} unset($s);
					if (!$is_contain) unset($form_list[$form_key]);
				}
				unset($form_model['scope']);
			} unset($form_model);

			$classify['form_count'] = (string)count($form_list);
			$form_list = array_values($form_list);
			if ($classify['form_count'] != 0) {
				$classify['form_list'] = $form_list;
			} else {
				unset($classify_list[$key]);
			}
		} unset($classify);

		parent::log_i('成功获取分类及表单');
		return $classify_list;
	}

	/**
	 * 获取用户常用表单
	 */
	private function get_user_form($user_id, $com_id) {
		parent::log_i('开始获取用户常用表单');

		$form_user = g('mv_form_user') -> get_common_form($user_id, $com_id);
		if (empty($form_user)) {
			parent::log_i('用户常用表单为空');
			return array();
		}

		$tmp_form_user = array();
		foreach ($form_user as $fu) {
			$version = $fu['version'];
			if ($version == parent::$StateHistory) { 
				//如果是历史版本，则更新为最新版本的form_id
				$public_form = g('mv_form') -> get_by_his_id($fu['form_history_id'], $com_id);
				if (!empty($public_form)) {
					$fu['form_id'] 		= $new_form_id 		= $public_form['id'];
					$fu['form_name'] 	= $new_form_name 	= $public_form['form_name'];
					$fu['form_describe'] = $new_form_describe = $public_form['form_describe'];
					$fu['scope'] 		= $new_form_scope 	= $public_form['scope'];

						// 判断新版本是否有权限发起
					$new_form_scope = json_decode($new_form_scope, TRUE);
					if (count($new_form_scope) == 0) {
						$dept_arr = array();
					} else {
						$dept_arr = g('dao_dept') -> list_by_ids($new_form_scope, '*', FALSE);
					}
					$user = g('dao_user') -> get_by_id($user_id);

					$is_apply = FALSE;
					if (count($dept_arr) == 0) {
						$is_apply = TRUE;
					} else {
						foreach ($dept_arr as $dept) {
							if (strstr($user['dept_tree'], '"'.$dept['id'].'"')) {
								$is_apply = TRUE;
								break;
							}
						}unset($dept);
					}

					if ($is_apply == FALSE) {
						g('mv_form_user') -> del_common_form($fu['form_history_id'], $user_id, $com_id);
					} else {
							// 更新本来旧的表单id
						g('mv_form_user') -> update_common_form_id($new_form_id, $fu['form_history_id'], $user_id, $com_id);
						$tmp_form_user[] = $fu;
					}
				}
			} else {
				$tmp_form_user[] = $fu;
			}

			if (count($tmp_form_user) >= self::$FormUserCount) break;
		} unset($fu);

		foreach ($tmp_form_user as $key => &$form) {
			$form['form_describe'] === NULL && $form['form_describe'] = '';

			unset($form['com_id']);
			unset($form['user_id']);
			unset($form['form_history_id']);
			unset($form['user_time']);
			unset($form['info_state']);
			unset($form['info_time']);
			unset($form['version']);
			unset($form['scope']);
			unset($form['id']);
			unset($form['icon']);
			unset($form['groups']);
		}unset($form);

		parent::log_i('成功获取用户常用表单');
		return $tmp_form_user;
	}

	/**
	 * 获取表单详情
	 * @param  [type] $form_id 
	 * @param  [type] $com_id  
	 * @param  [type] $user_id 
	 * @return [type]          
	 */
	private function get_form_by_id($form_id, $com_id, $user_id, $keep_form=false) {
		$fields = array(
			'id', 'form_name', 'form_describe', 'scope', 'inputs', 
			'form_history_id', 'version', 'app_version',
			'classify_id', 'work_id', 'is_other_proc', 'is_linksign',
			'formsetinst_name', 'groups', 'user_list','complete_date_status','type as from_type',
			);
		$fields = implode(',', $fields);
		$proc = g('mv_form') -> get_by_id($form_id, $com_id, $fields);
		if (!$proc) throw new \Exception('该模板不存在！');

		//获取最新版本的流程
		if ($proc['version'] == self::$StateHistory && !$keep_form) {
			$proc = g('mv_form') -> get_by_his_id($proc['form_history_id'], $com_id, $fields);
			if (!$proc) throw new \Exception('该模板不存在！');
		}

		//获取表单控件
		$inputs = g('mv_form') -> get_inputs_by_form($proc['id']);
		$proc['inputs'] = json_encode($inputs);

        $user_list = json_decode($proc['scope'], TRUE);

		$scope = json_decode($proc['scope'], TRUE);
		if (count($scope) == 0) {
			$proc['scope'] = array();
		} else {
			$proc['scope'] = g('dao_dept') -> list_by_ids($scope);
		}

		$user = g('dao_user') -> get_by_id($user_id);

		// 判断是否有权限发起该流程
		$dept_arr = $proc['scope'];
		$is_apply = FALSE;

        //判断适用人员范围是否有权限
        $users = json_decode($proc['user_list'], true);
        is_array($users) && in_array($user_id, $users) && $is_apply = true;

		if(!$is_apply){
            foreach ($dept_arr as $dept) {
                if (strstr($user['dept_tree'], '"'.$dept["id"].'"')) {
                    $is_apply = TRUE;
                    break;
                }
            }unset($dept);
        }
		// 判断分组权限是否有权限发起该流程
		if (!$is_apply) {
			$user_group = g('dao_group') -> get_by_user_id($com_id, $user_id);
			$group_ids = array_column($user_group, 'id');
			$groups = $proc['groups'] === NULL ? array() : json_decode($proc['groups'], TRUE);
			$same = array_intersect($group_ids, $groups);
			!empty($same) && $is_apply = TRUE;
		}

        if(!$is_apply && $user_list && in_array($user_id,$user_list) ){
            $is_apply = true;
        }

		if (!$is_apply && !$keep_form) throw new \Exception('没有权限！');

        //获取表单模板的显隐规则
        $proc['form_rule'] = $this->getFormRule($com_id,$proc['id']);

		return $proc;
	}

	/**
	 *
	 * 获取该节点中那些控件不可编辑
	 * @param string $form_vals 表单(字符串)
	 * @param array $work_node 步骤节点信息
	 */
	private function get_form_edit(&$form_vals,$work_node){
		if(empty($work_node)){
			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}

				if($form_val['type']=='file' && is_string($form_val['val'])){
                    $form_val['val'] = is_array(json_decode($form_val['val'],true))?json_decode($form_val['val'],true):$form_val['val'];
                }

				$form_val["visit_input"] = parent::$VisitInput;
				$form_val["edit_input"] = parent::$NotEditInput;
				if($form_val["type"]=="table"){
					$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
					foreach ($table_arr as $table_key=>&$table_td){
						$table_td["edit_input"] = parent::$NotEditInput;
						$table_td["visit_input"] = parent::$VisitInput;
					}
					$form_val["table_arr"]=$table_arr;

                    $table_rec = isset($form_val["rec"])?$form_val["rec"]:array();
                    $form_val["rec"]=$this->_formsetinst_table_label_rec_deal($table_rec);
				}
			} unset($form_val);
			$form_vals = json_encode($form_vals);
		} else {
			$input_visble_rule = $work_node["input_visble_rule"];
			$input_visble_rule = json_decode($input_visble_rule, TRUE);

			$input_visit_rule = $work_node["input_visit_rule"];
			$input_visit_rule = json_decode($input_visit_rule, TRUE);

			if(!is_array($form_vals)){
				$form_vals = json_decode($form_vals, TRUE);
			}

			$is_start = $work_node["is_start"];
			foreach ($form_vals as $input_key => &$form_val){
				if(!isset($form_val["type"])){
					continue;
				}

                if($form_val['type']=='file' && is_string($form_val['val'])){
                    $form_val['val'] = is_array(json_decode($form_val['val'],true))?json_decode($form_val['val'],true):$form_val['val'];
                }

				if(!empty($input_visit_rule)&&in_array($input_key,$input_visit_rule) && !in_array($work_node['state'],parent::$StateFinish)){
					$form_val["edit_input"] = parent::$NotEditInput;
					$form_val["visit_input"] = parent::$NotVisitInput;
				}
				else{
					$form_val["visit_input"] = parent::$VisitInput;
					if($is_start == parent::$StartNode){
					    //判断发起人是否可见控件
                        if( !empty($input_visit_rule)&&in_array($form_val['input_key'],$input_visit_rule) ){
                            $form_val["visit_input"] = parent::$NotVisitInput;
                        }

						//开始节点，如果存在，则为不可编辑控件
						if(!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = parent::$NotEditInput;
						}
						else{
							$form_val["edit_input"] = parent::$EditInput;
						}
						if($form_val["type"]=="table"){
							//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = parent::$NotVisitInput;
									$table_td["edit_input"] = parent::$NotEditInput;
								} else {
									$table_td["visit_input"] = parent::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = parent::$NotEditInput;
									} else {
										$table_td["edit_input"] = parent::$EditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;

                            $table_rec = isset($form_val["rec"])?$form_val["rec"]:array();
                            $form_val["rec"]=$this->_formsetinst_table_label_rec_deal($table_rec);

						}
					} else {
						//其他节点，如果存在，则为可编辑控件
						if (!empty($input_visble_rule)&&in_array($input_key,$input_visble_rule)){
							$form_val["edit_input"] = parent::$EditInput;
						} else {
							$form_val["edit_input"] = parent::$NotEditInput;
						}
						if ($form_val["type"]=="table"){
							//子表要检查子表元素的可编辑性
							$table_arr = isset($form_val["table_arr"])?$form_val["table_arr"]:array();
							foreach ($table_arr as $table_key=>&$table_td){
								if(!empty($input_visit_rule)&&in_array($table_key,$input_visit_rule)){
									$table_td["visit_input"] = parent::$NotVisitInput;
									$table_td["edit_input"] = parent::$NotEditInput;
								} else {
									$table_td["visit_input"] = parent::$VisitInput;
									if(!empty($input_visble_rule)&&in_array($table_key,$input_visble_rule)){
										$table_td["edit_input"] = parent::$EditInput;
									} else {
										$table_td["edit_input"] = parent::$NotEditInput;
									}
								}
							}
							$form_val["table_arr"]=$table_arr;

                            $table_rec = isset($form_val["rec"])?$form_val["rec"]:array();
                            $form_val["rec"]=$this->_formsetinst_table_label_rec_deal($table_rec);
						}
					}
				}
			} unset($form_val);
			$form_vals = json_encode($form_vals);
		}
	}

    /**
     * 流程实例子表单数值处理
     * @param $rec
     */
	private function _formsetinst_table_label_rec_deal($rec){
        if(!is_array($rec))
            return $rec;

        foreach ($rec as &$inputs){
            if(is_array($inputs)){
                foreach ($inputs as &$item){
                    if($item['type']=='file' && is_string($item['val']) ){
                        $item['val'] = is_array(json_decode($item['val'],true))?json_decode($item['val'],true):$item['val'];
                    }
                }
            }
        }unset($item);

        return $rec;
    }

	/** ————————————————————内部实现方法—————————————————————————— */
    /**
     * 检查更新锁
     * @param int $type 1:检查 2:加锁 3:解锁
     * @param $com_id
     * @param $id
     * @param $user_id
     * @return bool
     * @throws Exception
     */
    public function save_workitem_lock($type,$com_id, $id){

        $key = "save_workitem_lock.{$com_id}.{$id}";
        $key = md5($key);

        $redis = g('pkg_redis');

        if($type == 1){
            return $redis->get($key);
        }elseif($type == 2){
            return $redis->set($key,time(),20);
        }elseif($type == 3){
            return $redis->delete($key);
        }else{
            throw new \Exception("没有该类型锁");
        }
    }

    /** 获取所有可驳回的节点 */
    private function callback_item_list() {
        try {
            $data = parent::get_post_data(array('workitem_id'));

            $workitem_id = intval($data['workitem_id']);
            if (empty($workitem_id)) throw new \Exception("流程状态错误");

            $is_finish = parent::is_finish($data['workitem_id']);
            if ($is_finish) throw new \Exception('流程状态已变更！');

            //获取当前步骤的信息
            $item = parent::get_curr_workitem($workitem_id, "mpw.formsetinit_id,mpw.info_state");

            $callback = $this->get_callback_items($item['formsetinit_id'], $workitem_id);

            parent::echo_ok(array('info' => $callback));
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }

    /**
     * 获取回退节点集合
     * @param $formsetinit_id
     * @param $workitem_id
     * @return array
     */
    private function get_callback_items($formsetinit_id,$workitem_id){
        $fields = "id,work_node_id,handler_id,workitem_name,complete_time";
        $item_list = parent::get_callback_parent($formsetinit_id, $workitem_id, -1, $fields);

        $handler_id = array_column($item_list, 'handler_id');
        $users = g('dao_user')->list_by_ids(array_unique($handler_id), 'id,name,pic_url,acct');

        $tmp = array();
        foreach ($users as $user) {
            $tmp[$user['id']] = $user;
        }unset($user);
        $users = $tmp;

        //获取退回节点信息，用于判断是否为机器人
        $robbot_nodes = array();
        $start_nodes = array();
        if($item_list){
            $work_node_ids = array_column($item_list, 'work_node_id');
            $work_nodes = g('mv_work_node')->get_work_node_by_id($this->ComId,implode(',',$work_node_ids));
            foreach ($work_nodes as $item){
                if($item['is_robbot'])
                    $robbot_nodes[] = $item['id'];

                if($item['is_start'])
                    $start_nodes[] = $item['id'];
            }
        }

        //合并相同节点的工作项
        $callback = array();
        foreach ($item_list as &$val) {
            if(in_array($val['work_node_id'],$robbot_nodes))
                continue ; //机器人节点不给予退回

            $val['hander_name'] = isset($users[$val['handler_id']]['name']) ? $users[$val['handler_id']]['name'] : '';
            $val['hander_pic_url'] = isset($users[$val['handler_id']]['pic_url']) ? $users[$val['handler_id']]['pic_url'] : '';
            $val['hander_acct'] = isset($users[$val['handler_id']]['acct']) ? $users[$val['handler_id']]['acct'] : '';
            $data = array(
                'workitem_id' 		=> $val['id'],
                'handler_id' 		=> $val['handler_id'],
                'hander_name' 		=> $val['hander_name'],
                'hander_acct' 		=> $val['hander_acct'],
                'hander_pic_url' 	=> $val['hander_pic_url'],
                'complete_time' 	=> $val['complete_time'],
                'is_robbot'         => in_array($val['work_node_id'],$robbot_nodes)?1:0,
            );

            if (isset($callback[$val['work_node_id']])) {
                $callback[$val['work_node_id']]['back_item_list'][] = $data;
            } else {
                $tmp_data = array(
                    'workitem_name' 	=> $val['workitem_name'],
                    'is_start' 	=> in_array($val['work_node_id'],$start_nodes)?1:0,
                    'back_item_list' 	=> array($data)
                );
                $callback[$val['work_node_id']] = $tmp_data;
            }
        }unset($val);
        // 按节点倒序,开始节点提前
        krsort($callback);
        $callback = array_values($callback);
        $tmp = $callback[count($callback) - 1];
        unset($callback[count($callback) - 1]);
        $callback = array_merge(array($tmp), $callback);


        //对同个节点同个处理人去重
        if($callback){
            foreach ($callback as &$item){
                $tmp_back_item_list = [];
                foreach ($item['back_item_list'] as $citem){
                    if(isset($tmp_back_item_list[$citem['handler_id']]) ){
                        if($citem['workitem_id'] > $tmp_back_item_list[$citem['handler_id']]['workitem_id'])
                            $tmp_back_item_list[$citem['handler_id']] = $citem;
                    }else{
                        $tmp_back_item_list[$citem['handler_id']] = $citem;
                    }
                }
                $item['back_item_list'] = array_values($tmp_back_item_list);
            }
        }
        return $callback;
    }
}
// end of file