<?php

/**
 * 物资处理接口
 * 
 * @author gch
 * @date 2018-01-31 10:31:57
 */

namespace model\process\view;


class ProcessMaterial extends \model\api\view\ViewBase {
	//EXCEL文件后缀
	private static $ExcelExtList = array('xlsx', 'xls');	
	//10M
	private static $MaxSize = 10485760;				

	/** 每次导入的行数 */
	private static $ImportSize = 50;

	// excel控件对应的数据库字段
	private static $inputKey = array('type', 'name', 'describe', 'the_value');
	// excel控件类型对应关系
	private static $inputTypeMap = array(
		'单行文本' => 'text',
		'规格' => 'select',
		'单位' => 'select',
	);
	// excel物资信息对应关系
	private static $materialInfoMap = array(
		'物资名称' => '`name`',
		'物资描述' => '`describe`',
		'大类(以全角英文逗号隔开)' => 'tag',
	);

	private static $TAG_TYPE = 1;
	// 真实物资
	private static $MATERIAL_REAL = 1;
	// 物资模板
	private static $MATERIAL_MODEL = 2;	

	//命令对应函数名的映射表
	private $cmd_map = array(
		// 获取物资标签
		101 => 'getTag',
		// 获取物资
		102 => 'getMaterialList',
		// 保存物资
		103 => 'saveMaterial',
		// 删除/更新标签
		104 => 'editTag',
        // 导入execl表
		105 => 'importMaterial',
        // 删除物资
        107 => 'deleteMaterial',
	);

	public function __construct() {
		parent::__construct('process');
	}
	
	/**
	 * ajax行为统一调用方法
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		try {
			$cmd = g('pkg_val') -> get_get('cmd', FALSE);
			$cmd = (int)$cmd;
			if (empty($cmd)) throw new \Exception('非法请求');
	
			$map = $this -> cmd_map;
			if (!isset($map[$cmd])) throw new \Exception('非法请求');
	
			$this -> $map[$cmd]();

		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

// --------------------------接口内部实现函数------------------------------------------------

	/**
	* 获取标签
	*   
	* @access private
	* @param   
	* @return   
	*/	
	private function getTag() {
		$need = array();
		$data = parent::get_post_data($need);

		$keyword = !empty($data['keyword']) ? trim($data['keyword']) : '';
		$page = !empty($data['page']) ? intval($data['page']) : 0;
		$pageSize = !empty($data['page_size']) ? intval($data['page_size']) : 0;
		$withCnt = $page == 0 && $pageSize == 0 ? false : true;
		
		$cond = array();
		!empty($keyword) && $cond['name like '] = "%{$keyword}%";

		$tag_fields = array(
			'`id`', '`name`',
		);
		$order = ' id desc ';
		$tags = g('dao_tag') -> list_data_by_cond($this -> com_id, $cond, $tag_fields, $page, $pageSize, $order, '', self::$TAG_TYPE, $withCnt);

		$info = array(
			'data' => $tags,
		);
		parent::echo_ok($info);
	}

	/**
	* 获取物资表单
	*   
	* @access private
	* @param   
	* @return   
	*/	
	private function getMaterialList() {
		$need = array();
		$data = parent::get_post_data($need);

		$id = !empty($data['id']) ? intval($data['id']) : 0;
		$tagId = !empty($data['tag_id']) && is_array($data['tag_id']) ? $data['tag_id'] : array();
		$tagId = array_filter($tagId);
		$keyword = !empty($data['keyword']) ? trim($data['keyword']) : '';
		$page = !empty($data['page']) ? intval($data['page']) : 0;
		$pageSize = !empty($data['page_size']) ? intval($data['page_size']) : 0;
		$type = !empty($data['type']) ? intval($data['type']) : self::$MATERIAL_MODEL;
		
		// 获取表单
		$materialFields = array(
			'id', 'name','describe','icon','history_id','creater_id','create_name','create_time',
		);
		$withCnt = $page == 0 && $pageSize == 0 ? false : true;
		$order = ' m.update_time desc ';
		$cond = array(
			'm.type=' => $type,
		);
		if (!empty($id)) {
			$cond['m1.id='] = $id;
			$withCnt = false;
		}
		if (!empty($keyword)) {
			// $key = "^(m.`name` like '%{$keyword}%' OR m.`describe` like '%{$keyword}%' OR m.create_name like '%{$keyword}%' OR EXISTS ";
			// $val = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.`name` like '%{$keyword}%'))";
			// $cond[$key] = $val;
			$cond["__OR_1"] = array(
				'm.`name` like ' => "%{$keyword}%",
				'm.`describe` like ' => "%{$keyword}%",
				'm.create_name like ' => "%{$keyword}%",
			);
		}
		if (!empty($tagId)) {
			$tagId = array_map('intval', $tagId);
			$tagSql = "(SELECT * from mv_proc_material_tag_map AS map LEFT JOIN mv_proc_tag AS t ON map.tag_id = t.id WHERE map.material_id = m.id AND t.id IN (" . implode(',', $tagId) . ") AND t.info_state=1)";
			$cond['^EXISTS '] = $tagSql;
		}
		
		$materials = g('dao_material') -> getMaterialList($this -> com_id, $cond, $materialFields, $page, $pageSize, $order, $withCnt);
		if (!empty($materials)) {
			$tmp_data = $withCnt ? $materials['data'] : $materials;

			$materialIds = array_column($tmp_data, 'id');
			$tmp_data = array_combine($materialIds, $tmp_data);

			// 获取物资属性
			$materialLabel = g('dao_material') -> getMaterialLabel($this -> com_id, $materialIds);

			// 获取表单标签
			$tags = g('dao_tag') -> getTagByMaterialId($this -> com_id, $materialIds);

			// 同一表单的标签
			$formTags = array();
			$formTags_str = array();
			foreach ($tags as $tag) {
				if (!isset($formTags[$tag['material_id']])) {
					$formTags[$tag['material_id']] = array();
				}

				$formTags[$tag['material_id']][] = $tag['id'];
				$formTags_str[$tag['material_id']][] = $tag['name'];
			}

			// 标签拼接到表单数据中
			foreach ($tmp_data as &$form) {
				if (isset($formTags[$form['id']])) {
					$form['tag'] = $formTags_str[$form['id']];
					$form['tag_id'] = $formTags[$form['id']];
				} else {
					$form['tag'] = array("默认标签");
					$form['tag_id'] = array(0);
				}


				$form['inputs'] = isset($materialLabel[$form['id']]) ? $materialLabel[$form['id']] : array();
			}
			unset($form);

			$tmp_data = array_values($tmp_data);
			if ($withCnt) {
				$materials['data'] = $tmp_data;
			} else {
				$materials = $tmp_data;
			}
		}

		// 获取所有标签
		$tag_fields = array(
			'`id`', '`name`',
		);
		$allTag = g('dao_tag') -> list_data_by_cond($this -> com_id, array(), $tag_fields, 0, 0, '', '', self::$TAG_TYPE, false);

		$info = array(
			'data' => $materials,
			'allTag' => $allTag,
		);
		parent::echo_ok($info);
	}

	/**
	* 保存物资设置
	*   
	* @access private
	* @param   
	* @return   
	*/	
	private function saveMaterial() {
		$need = array(
			'form_name',
			// 'history_id',
			'icon','inputs', 'tag',
		);
		$data = parent::get_post_data($need);
		
		if (empty($data['form_name'])) {
			throw new \Exception("缺少物资名称");
		}
		$name = trim($data['form_name']);
		if (empty($data['inputs']) || !is_array($data['inputs'])) {
			throw new \Exception("物资属性格式错误");
		}
		$inputs = $data['inputs'];
		if (empty($data['tag']) || !is_array($data['tag'])) {
			throw new \Exception("标签格式错误");
		}
		$tag_name = $data['tag'];
		
		// $historyId = intval($data['history_id']);
		// 默认为新建
		$historyId = 0;
		$icon = !empty($data['icon']) ? trim($data['icon']) : '';
		$is_show_wx = !empty($data['is_show_wx']) ? intval($data['is_show_wx']) : 1;
		$describe = !empty($data['form_describe']) ? trim($data['form_describe']) : '';
	
		$materialData = array(
			'`name`' => $name,
			'`describe`' => $describe,
			'icon' => $icon,
			'user_list' => '[]',
			'dept_list' => '[]',
			// 微信端新增的都是物资模板
			'type' => self::$MATERIAL_REAL,
			'group_list' => '[]',
			'is_show_wx' => $is_show_wx,
			'creater_id' => 0,
			'create_name' => $_SESSION[SESSION_VISIT_USER_NAME],
		);

		// 处理物资属性数据
		$materialInputData = array();
		foreach ($inputs as $i) {
			$type = trim($i['type']);
			$input_key = trim($i['input_key']);
			$tableArr = $type == 'table' && !empty($i['table_arr']) && is_array($i['table_arr']) ? $i['table_arr'] : array();

			$tmp = array(
				'`name`' => trim($i['name']),
				'`describe`' => trim($i['describe']),
				'input_key' => $input_key,
				'type' => $type,
				'sub' => array(),
				'must' => intval($i['must']),
			);
			unset($i['name'], $i['describe'], $i['input_key'], $i['type'], $i['must'], $i['table_arr']);
			$tmp['other'] = !empty($i) && is_array($i) ? $i : array();
			$tmp['other'] = json_encode($tmp['other']);
			$materialInputData[$input_key] = $tmp;

			// 子表单
			if (!empty($tableArr)) {
				foreach ($tableArr as $t) {
					$subTmp = array(
						'`name`' => trim($t['name']),
						'`describe`' => '',
						'input_key' => trim($t['th_key']),
						'type' => trim($t['type']),
						'must' => intval($t['must']),
					);
					unset($t['name'], $t['th_key'], $t['type'], $t['must']);
					$subTmp['other'] = !empty($t) && is_array($t) ? $t : array();
					$subTmp['other'] = json_encode($subTmp['other']);
					$materialInputData[$input_key]['sub'][] = $subTmp;
				}
			}
		}
	
		$materialId = g('dao_material') -> saveMaterial($this -> com_id, $materialData, $materialInputData, $historyId);

		// 处理标签
		if (!empty($tag_name)) {
			$this -> handleTag($tag_name, $materialId, $this -> com_id);
		}

		$info = array(
			'id' => $materialId,
		);
		parent::echo_ok($info);
	}

	/**
	* 删除/更新标签
	*   
	* @access private
	* @param int id 标签id
	* @param int type 1-删除 2-更新
	* @return   
	*/	
	private function editTag() {
		$need = array(
			'id', 'type'
		);
		$data = parent::get_post_data($need);
		
		if (empty($data['id']) || !is_array($data['id'])) {
			throw new \Exception("标签id格式错误");
		}
		$ids = $data['id'];
		if (empty($data['type'])) {
			throw new \Exception("缺少修改类型type");
		}
		$type = $data['type'];

		if ($type == 2 && empty($data['name'])) {
			throw new \Exception("缺少标签名");
		}
		$name = !empty($data['name']) ? trim($data['name']) : '';

		$udata = array();
		if ($type == 1) {
			// 删除
			$udata['info_state'] = 0;
		} elseif ($type == 2) {
			// 更新
			$udata['`name`'] = $name;
		} else {
			throw new \Exception("type格式错误");
		}
		
		$flag = g('dao_tag') -> update_tag($this -> com_id, $ids, $udata);
		if ($flag === false) {
			throw new \Exception("操作失败");
		}

		parent::echo_ok();
	}

	/**
	* 上传execl表
	*   
	* @access public
	* @param   
	* @return   
	*/
	public function importMaterial() {
		try {
			$file = g('api_ser_upload') -> upload(self::$MaxSize, self::$ExcelExtList);
			g('pkg_excel') -> set_file_type($file['ext']);

			//获取文件总行数
			$total_count = g('pkg_excel') -> get_count_from_xlsx($file['real_path']);
			if ($total_count <= 0) 		throw new \Exception('文件内容为空');
						
			//把xlsx文件转成csv文件后再读取，可减少硬件资源消耗及加快读取速度
			$csv_file_path = str_replace(array('.xlsx', '.xls'), '.csv', $file['real_path']);
			$csv_file = g('pkg_excel') -> xlsx_to_csv($file['real_path'], $csv_file_path, FALSE);
			
			$fdata = g('pkg_excel') -> read_from_csv($csv_file_path, 0, 0, TRUE, FALSE, TRUE);
			
			$import_ret = $this -> handleImportMaterial($this -> com_id, $fdata);

			$fail_path = str_replace('.xlsx', '_f.xlsx', $file['rela_path']);
			$success_path = str_replace('.xlsx', '_s.xlsx', $file['rela_path']);

			$info = array(
				'file' => $file['hash'],
				'total' => $total_count,
				'idx' => 0,
				'success' => 0,
				'fail' => 0,
				'fail_file' => rtrim(MAIN_DYNAMIC_DOMAIN, '/') . '/data/'. $fail_path,
				'success_file' => rtrim(MAIN_DYNAMIC_DOMAIN, '/') . '/data/'. $success_path,
			);
			$data = array(
					'info' => $info,
			);
			parent::echo_ok($data);
			
		} catch (\Exception $e) {
			parent::echo_exp($e);
		}
	}

	/**
	* 删除物资
	*   
	* @access public
	* @param   
	* @return   
	*/
	public function deleteMaterial() {
		$need = array(
			'id',
		);
		$data = parent::get_post_data($need);
		
		if (empty($data['id']) || !is_array($data['id'])) {
			throw new \Exception("标签id格式错误");
		}
		$ids = $data['id'];

		$flag = g('dao_material') -> deleteMaterial($this -> com_id, $ids);
		if ($flag === false) {
			throw new \Exception("操作失败");
		}

		parent::echo_ok();
	}

	/**
	* 导入物资逻辑处理
	*   
	* @access private
	* @param   
	* @return   
	*/
	private function handleImportMaterial($comId, $rawData) {
		// 物资集合
		$materialInfo = array();
		// 物资控件集合
		$materialInputInfo = array();
		// 控件和物资分割行标识
		$divisionLineFlag = false;
		// 控件和物资分割行行号
		$divisionLine = 0;
		// 物资信息列对应关系
		$materialTitileMap = array();

		foreach ($rawData as $l => $rd) {
			// 第一行不处理
			if ($l == 0) {
				continue;
			}

			$rd = array_map('trim', $rd);

			// 收集控件集合
			$firstColumn = reset($rd);
			if ($firstColumn == '!input_model') {
				array_shift($rd);
				$rd = array_chunk($rd, count(self::$inputKey));
				$rd = array_shift($rd);
				$input = array_combine(self::$inputKey, $rd);
				$input['type'] = self::$inputTypeMap[$input['type']];
				$materialInputInfo[$input['name']] = $input;
				continue;
			}

			// 判断分割行
			$dl = array_filter($rd);
			if (empty($dl)) {
				$divisionLine = $l;
				$divisionLineFlag = true;
				continue;
			}

			// 未过分割行，不开始处理物资信息
			if (!$divisionLineFlag) {
				continue;
			}

			// 收集物资信息列对应关系,分割行的下一行
			if ($l == ($divisionLine + 1)) {
				$materialTitileMap = $rd;
				continue;
			}

			// 未获取到物资标题对应关系和控件信息不获取物资信息
			if (empty($materialTitileMap) || empty($materialInputInfo)) {
				continue;
			}

			$rd = array_values($rd);
			$rd = array_filter($rd);

			// 物资信息
			$tmpMaterialInfo = array(
				'`name`' => '',
				'`describe`' => '',
				'tag' => '',
				'icon' => '',
				'type' => self::$MATERIAL_REAL,
				'user_list' => '[]',
				'dept_list' => '[]',
				'group_list' => '[]',
				'is_show_wx' => 1,
				'creater_id' => 0,
				'create_name' => $_SESSION[SESSION_VISIT_USER_NAME],
				'input' => array(),
			);

			foreach ($rd as $k => $v) {
				if (!isset($materialTitileMap[$k]) && !isset($materialInputInfo[$title])) {
					continue;
				}

				$title = $materialTitileMap[$k];
				
				// 收集物资信息
				if (isset(self::$materialInfoMap[$title])) {
					$tmpMaterialInfo[self::$materialInfoMap[$title]] = $v;
					continue;
				}
				
				// 收集物资控件信息
				if (isset($materialInputInfo[$title])) {
					$type = $materialInputInfo[$title]['type'];
					if ($type == 'text') {
						$other = array(
							'the_value' => str_replace('，', ',', $v),
						);
					} elseif ($type == 'select') {
						$opts = explode('，', $v);
						$optsKey = count($opts);
						$optsNums = implode(',', array_keys($opts)) . ',' . $optsKey;
						$optsNums = ltrim($optsNums, '0,');
						
						$other = array(
							'opts' => implode(',', $opts),
							'opts_key' => $optsKey,
							'opts_nums' => $optsNums,
						);
					} else {
						$other = array();
					}
					
					$tmp = array(
						'`name`' => $materialInputInfo[$title]['name'],
						'`describe`' => $materialInputInfo[$title]['describe'],
						'input_key' => 'input' . time() + $k . '000',
						'type' => $type,
						'sub' => array(),
						'must' => 0,
						'other' => json_encode($other),
					);
					$tmpMaterialInfo['input'][] = $tmp;
					continue;
				}

			}

			$materialInfo[] = $tmpMaterialInfo;
		}
		
		// 插入数据
		foreach ($materialInfo as $m) {
			$tagName = $m['tag'];
			$materialInputData = $m['input'];
			unset($m['input'], $m['tag']);
			$materialId = g('dao_material') -> saveMaterial($comId, $m, $materialInputData);
			
			if (!empty($tagName)) {
				$tagName = explode('，', $tagName);
				$this -> handleTag($tagName, $materialId, $comId);
			}
		}


		return array('success' => $rawData, 'fail' => array());

	}

	private function handleTag($tag_name, $materialId, $comId) {
		$tag_name = array_map('trim', $tag_name);
		$tag_name = array_filter($tag_name);
		$tag_fields = array(
			'id', '`name`',
		);
		$tag_cond = array(
			'`name` IN' => $tag_name,
		);

		// 检测未有的标签，若没有则新增
		$tag_info = g('dao_tag') -> list_data_by_cond($comId,$tag_cond,$tag_fields, 0, 0, '', '', self::$TAG_TYPE);
		$exist_tag_name = array_column($tag_info['data'], 'name');
		$diff_tag_name = array_diff($tag_name, $exist_tag_name);
		if (!empty($diff_tag_name)) {
			$save_tag = g('dao_tag') -> batch_insert_tag($comId, 0, $_SESSION[SESSION_VISIT_USER_NAME], $diff_tag_name, self::$TAG_TYPE);
			!$save_tag && parent::log_i('新增标签失败:' . json_encode($diff_tag_name));
		}

		// 根据标签名获取
		$tag_info = g('dao_tag') -> list_data_by_cond($comId,$tag_cond,$tag_fields, 0, 0, '', '', self::$TAG_TYPE);
		if (!empty($tag_info['data'])) {
			$tag_ids = array_column($tag_info['data'], 'id');
			g('dao_tag') -> delete_map_by_ids($comId, $tag_ids, $materialId, self::$TAG_TYPE);
			$tag_map = g('dao_tag') -> insert_material_tag_map($comId, $materialId, $tag_ids);
			!$tag_map && parent::log_i('保存标签-表单映射失败');
		}

	}
}

//end of file