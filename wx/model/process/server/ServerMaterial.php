<?php 
/**
 * 流程报表业务逻辑处理类
 * @author luja
 * @create 2016-10-13
 */

namespace model\process\server;

class ServerMaterial extends \model\api\server\ServerBase {

    private static $TAG_TYPE = 1;
    // 真实物资
    private static $MATERIAL_REAL = 1;
    // 物资模板
    private static $MATERIAL_MODEL = 2;

    public function __construct($log_pre_str='') {
        parent::__construct('process', $log_pre_str);
    }

    public function handleTag($tag_name, $materialId, $comId) {
        $tag_name = array_map('trim', $tag_name);
        $tag_name = array_filter($tag_name);
        $tag_fields = array(
            'id', '`name`',
        );
        $tag_cond = array(
            '`name` IN' => $tag_name,
        );

        // 检测未有的标签，若没有则新增
        $tag_info = g('dao_tag') -> list_data_by_cond($comId,$tag_cond,$tag_fields, 0, 0, '', '', self::$TAG_TYPE);
        $exist_tag_name = array_column($tag_info['data'], 'name');
        $diff_tag_name = array_diff($tag_name, $exist_tag_name);
        if (!empty($diff_tag_name)) {
            $save_tag = g('dao_tag') -> batch_insert_tag($comId, 0, $_SESSION[SESSION_VISIT_USER_NAME], $diff_tag_name, self::$TAG_TYPE);
            !$save_tag && parent::log_i('新增标签失败:' . json_encode($diff_tag_name));
        }

        // 根据标签名获取
        $tag_info = g('dao_tag') -> list_data_by_cond($comId,$tag_cond,$tag_fields, 0, 0, '', '', self::$TAG_TYPE);
        if (!empty($tag_info['data'])) {
            $tag_ids = array_column($tag_info['data'], 'id');
            g('dao_tag') -> delete_map_by_ids($comId, $tag_ids, $materialId, self::$TAG_TYPE);
            $tag_map = g('dao_tag') -> insert_material_tag_map($comId, $materialId, $tag_ids);
            !$tag_map && parent::log_i('保存标签-表单映射失败');
        }

    }
}
//end of file