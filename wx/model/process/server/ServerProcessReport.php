<?php 
/**
 * 流程报表业务逻辑处理类
 * @author luja
 * @create 2016-10-13
 */

namespace model\process\server;

class ServerProcessReport extends \model\api\server\ServerBase {

	/** 禁用 */
	private static $Disable = 2;
	/** 启用 */
	private static $Enable = 1;
	
	/** 正常 */
	private static $StateOn = 1;
	/** 删除 */
	private static $StateOff = 0;


	public function __construct($log_pre_str='') {
		parent::__construct('process', $log_pre_str);
	}
    /** 获取表格/图的数据 */
    public function get_conf_data($row_list, $col_key, $cond_list, $join_list, $order, $other_cond, $count, $sum, $start=0, $page_size=0){
        $ret = g('api_report') -> get_conf_data($row_list, $col_key, $cond_list, $join_list, $order, $other_cond, $count, $sum, $start, $page_size);

        $data = array(
            'data' => $ret,
            'sum' => $sum,
            'count' => $count
        );
        return $data;
    }

    /** 获取表格信息 */
    public function get_table($table_id, $fields = '*'){
        $table_info = g('api_report') -> get_table_by_id($this -> com_id, $table_id, $fields);
        if(empty($table_info)) return array();

        $table_info['row_list'] = json_decode($table_info['row_list'], TRUE);
        $table_info['join_list'] = json_decode($table_info['join_list'], TRUE);
        $table_info['cond_list'] = json_decode($table_info['cond_list'], TRUE);
        $table_info['other_list'] = json_decode($table_info['other_list'], TRUE);
        $table_info['col_key'] = json_decode($table_info['col_key'], TRUE);
        empty($table_info['col_key']) && $table_info['col_key'] = array();

        $all_col_list = array();

        foreach ($table_info['row_list'] as &$val) {
            $input_key = explode("=", $val['input_key']);
            $val['input_key'] = array_shift($input_key);
        }unset($val);

        foreach ($table_info['col_key'] as $key) {
            $table_info['row_list'][$key]['count_type'] = 0;
            $all_col_list = array_merge($all_col_list, $table_info['row_list'][$key]['list_key']);
            // $all_col_list[] = $table_info['row_list'][$key]['input_key'];
        }unset($key);

        $table_info['col_list'] = $all_col_list;
        return $table_info;
    }
	/**
	 * 获取流程报表
	 * @param  [type] $page      
	 * @param  [type] $page_size 
	 * @return [type]            
	 */
	public function get_report_list($cond=array(), $page=1, $page_size=10) {
		$com_id = $this -> com_id;
		/** 获取员工部门id树 */
		$user = g('dao_user') -> get_by_id($this -> user_id, 'dept_tree');
		$dept_tree = json_decode($user['dept_tree'], TRUE);
		$dept_ids = array();
		foreach ($dept_tree as $tree) {
			$dept_ids = array_merge($dept_ids, $tree);
		}unset($tree);
		$dept_ids = array_unique($dept_ids);

		foreach ($dept_ids as $id) {
			$cond['__OR'][]['visit_dept LIKE '] = '%"' . $id . '"%';
		}unset($id);
		$cond['state='] = self::$Enable;

		$fields = 'id,name,`desc`,type,has_table,has_chart,create_time';
		$report_list = g('mv_report') -> get_report_list($com_id, $cond, $fields, $page, $page_size, '', TRUE);
		if (!$report_list) {
			$report_list = array(
				'data' => array(),
				'count' => 0
				);
		}

		return $report_list;
	}

	/**
	*	根据表单实例formsetinst_id获取workitem_id
	*	
	*	@access public
	*	@param int formsetinst_id 表单实例formsetinst_id
	*	@return array
	*	@author gch 2017-03-31
	*/
	public function get_workitem_id_by_formsetinst_id($formsetinst_id, $com_id)
	{
		$return = array();
		$cond['formsetinit_id='] = intval($formsetinst_id);
		$cond['com_id='] = intval($com_id);

		$workitem_id_list = g('pkg_db') -> select('mv_proc_workitem', 'id', $cond);

		!is_array($workitem_id_list) && $workitem_id_list = array();

		foreach ($workitem_id_list as $value) {
			$return[] = $value['id'];
		}

		return $return;
	}

	/**
	 * 获取当前实例new
	 * 
	 * @access public
	 * @param int $formsetinst_id 实例id
	 * @return array
	 * @author gch 2017-03-31
	 */
	public function get_formsetinst_by_id($formsetinst_id, $com_id, $fields='mpf.*,mpfm.form_name,mpfm.form_describe,mpfm.is_linksign,mpfm.form_history_id ') {

		$formsetinstsql = "select ".$fields."  
		from mv_proc_formsetinst mpf,mv_proc_form_model mpfm 
		where mpfm.id = mpf.form_id and mpf.id = ".$formsetinst_id." and mpf.com_id = ". intval($com_id);
		$formsetinst = g('pkg_db') -> query($formsetinstsql);
		$formsetinst = $formsetinst[0];

		if(is_array($formsetinst)){
			if($formsetinst["info_state"]==self::$StateOff){
				throw new \Exception('该实例已被删除!');
			}
		}
		else{
			throw new \Exception('没有权限查看该步骤！');
		}
		
		return $formsetinst;
	}
    /**
     * 获取input
     * @access public
     * @param int $formsetinst_id 实例id
     * @return array
     */
    public function get_formsetinst_input($formsetinst_id,$input_key) {
        $cond = [
            'formsetinst_id='=>$formsetinst_id,
            'input_key='=>$input_key,
        ];
        $ret = g('pkg_db') -> select_one('mv_proc_formsetinst_label', "*", $cond);
        !is_array($ret) && $ret = array();
        return $ret;
    }
	/**
	 * 获取冲突
	 * 
	 * @access public
	 * @param int $form_history_id 历史版本id
	 * @return array
	 * @author gch 2017-03-31
	 */
	public function get_conflict_by_id($form_history_id) {
		$com_id = $this -> com_id;
		$table = 'mv_proc_formsetinst_label mpfl,mv_proc_formsetinst mpf';
		$fields = 'mpf.id,mpf.creater_id,mpf.creater_name,mpfl.type,mpfl.val,mpfl.other';

		$cond = array();
		$cond['mpfl.form_history_id='] = intval($form_history_id);
		$cond['mpf.com_id='] = $com_id;
		$cond['mpfl.info_state='] = 1;
		$cond['mpf.info_state='] = 1;
        $cond['mpf.state!='] = 0;
		$cond['^mpfl.type='] = '\'conflict\'';
		$cond['^mpf.id='] = 'mpfl.formsetinst_id';

		$ret = g('pkg_db') -> select($table, $fields, $cond);
		!is_array($ret) && $ret = array();

		return $ret;
	}

	/**
	 * 根据work_id获取全部自动知会控件
	 * 
	 * @access public
	 * @param int $work_id 流程模板表
	 * @return 知会控件key
	 * @author gch 2017-09-16 15:39:14
	 */
	public function get_notify_inputs($work_id) {
		$return = array();

		$notify_inputs = g('mv_work_node') -> get_worknode_by_condition($this -> com_id, $work_id, array(), 'notify_input');
		if (empty($notify_inputs)) {
			return $return;
		} else {
			$notify_inputs = array_column($notify_inputs, 'notify_input');
			foreach ($notify_inputs as $v) {
				$tmp_inputs = explode(',', $v);
				$return = array_merge($return, $tmp_inputs);
			}unset($v);
			$return = array_unique($return);
		}
		
		return $return;
	}

}
//end of file