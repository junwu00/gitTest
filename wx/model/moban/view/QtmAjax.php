<?php
/**
 * 提案接口
 * @author hwp
 * @date 2018-05-30
 */

namespace model\qtm\view;

class QtmAjax extends \model\api\view\ViewBase {

    //命令对应函数名的映射表
    private $cmd_map = array(
        //获取jssdk签名
        201 => 'getJssdkSign',
    );


    private $server_project;

    public function __construct() {
        global $mod_name;
        parent::__construct($mod_name);
    }

    /** ajax行为统一调用方法 */
    public function index() {
        try {

            $cmd = intval( g('pkg_val') -> get_get('cmd', FALSE) );
            if (empty($cmd)) throw new \Exception('命令参数错误');

            $map = $this -> cmd_map;
            if (!isset($map[$cmd])) throw new \Exception('命令参数错误');

            $this -> $map[$cmd]();                                      //进入方法
        } catch (\Exception $e) {
            parent::echo_exp($e);
        }
    }


// --------------------------接口内部实现函数------------------------------------------------


    /**
     * 获取jssdk签名
     */
    private function getJssdkSign(){
        $request_data = parent::get_post_data(["url"]);
        $url = trim($request_data['url']) ;

        //生成签名等数据
        try{
            $mod_conf = load_config('model/archive/model');
            $sc_app_id = isset($mod_conf["app_id"])?$mod_conf["app_id"]:0;

            $nonceStr = $this -> get_rand_string(32) ;
            $timestamp = time() ;
            $data = array(
                'jsapi_ticket' => g('api_jssdk') -> get_jsapi_ticket($this->com_id,$sc_app_id),
                'noncestr' => $nonceStr ,
                'timestamp' => $timestamp,
                'url' => $url,
            ) ;
            
            $signature = g('api_jssdk') -> get_jsapi_sign($data,$this->com_id,$sc_app_id) ;

            //返回数据给前端
            $appId = $_SESSION[SESSION_VISIT_CORP_ID];
            $result = array(
                'data' => array(
                    'appId' => $appId,
                    'timestamp' => $timestamp ,
                    'nonceStr' => $nonceStr ,
                    'signature' => $signature,
                )
            ) ;
            parent::echo_ok($result);
        } catch (Exception $e) {
            throw new Exception('jssdk 错误');
        }
    }

    /**
     * 生成随机数
     *
     */
    private function get_rand_string($leng, $type=7, $dark=FALSE){
        $tmp_array = array(
            '1' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            '2' => 'abcdefghijklmnopqrstuvwxyz',
            '4' => '0123456789',
            '8' => '~!@$&()_+-=,./<>?;\'\\:"|[]{}`'
        );
        $return = $target_string = '';
        $array = array();
        $bin_string = decbin($type);
        $bin_leng  = strlen($bin_string);
        for($i = 0; $i < $bin_leng; $i++) if($bin_string{$i} == 1) $array[] = pow(2, $bin_leng - $i - 1);
        if(in_array(1, $array, TRUE)) $target_string .= $tmp_array['1'];
        if(in_array(2, $array, TRUE)) $target_string .= $tmp_array['2'];
        if(in_array(4, $array, TRUE)) $target_string .= $tmp_array['4'];
        if(in_array(8, $array, TRUE)) $target_string .= $tmp_array['8'];
        $target_leng = strlen($target_string);
        mt_srand((double)microtime()*1000000);
        while(strlen($return) < $leng){
            $tmp_string = substr($target_string, mt_rand(0, $target_leng), 1);
            $dark && $tmp_string = (in_array($tmp_string, array('0', 'O', 'o'))) ? '' : $tmp_string;
            $return .= $tmp_string;
        }
        return $return;
    }



}

//end