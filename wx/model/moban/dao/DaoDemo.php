<?php
/**
 * Created by PhpStorm.
 * User: chenxintian
 * Date: 2018/6/28
 * Time: 18:25
 */
namespace model\qtm\dao;
class DaoDemo extends  \model\api\dao\DaoBase{
    /** 对应的库表名称 */
    private static $Table = 'table';

    /** 禁用 0 */
    private static $StateOff = 0;
    /** 启用 1 */
    private static $StateOn = 1;

    //场景类型 预设场景
    private static $scene_preset_type = 1;

    public function __construct(){
        parent::__construct();
        $this -> set_table(self::$Table);
    }

    /**
     * 获取每页列表
     */
    public function listByPage($com_id,$cond=array(),$page=0,$page_size=0,$field='*'){
        $order_by = 'id desc';
        $cond = array(
            "com_id="=>$com_id,
            "info_state="=>self::$StateOn,
        );


        return $this->list_by_page($cond,$field,$page,$page_size,$order_by,"",true);
    }

    /**
     * 通过id获取信息
     */
    public function getById($id,$field='*'){
        $cond = array(
            "id="=>$id,
            "info_state="=>self::$StateOn,
        );



        return $this->get_by_cond($cond,$field);
    }

    /**
     * 通过id获取场景名
     * @param $ids
     * @param string $field
     * @param bool $with_del
     * @return array
     */
    public function getByIds($ids,$field='*',$with_del=true){
        $cond = array(
            "id IN"=>$ids,
        );

        if(!$with_del){
            $cond["info_state="] = self::$StateOn;
        }

        return $this->list_by_cond($cond,$field);
    }

    /**
     * 添加数据
     * @param $data
     * @return bool
     */
    public function add($data){
        $time = time();
        $data['create_time'] = $time;
        $data['update_time'] = $time;

        return $this->insert($data);
    }
    /**
     * 更改数据
     */
    public function update($id,$data){
        $time = time();
        $data["update_time"] = $time;

        $cond = array(
            "id="=>$id,
            "info_state="=>self::$StateOn,
        );

        return $this->update_by_cond($cond,$data);
    }

    /**
     * 通过id删除数据
     */
    public function deleteById($id){
        $cond = array(
            "info_state="=>self::$StateOn,
            "id="=>$id,
        );

        //delete
        $data = array(
            "info_state"=>self::$StateOff,
        );

        return $this->update_by_cond($cond,$data);
    }

}