<?php

/**
 * 主页型应用异常类
 * @author chenyihao
 * @date 2016-06-22
 *
 */
require_once(MAIN_MODEL . 'api/exception/ExceptionBase.php');

class DemoException extends \ExceptionBase {
	
	public function __construct(array $custom_exp_info) {
		parent::__construct($custom_exp_info);
	}
	
}

//end