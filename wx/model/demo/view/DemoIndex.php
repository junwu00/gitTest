<?php
/**
 * 主页型应用单入口
 * @author 陈意浩
 * @date 2016-08-03
 *
 */
namespace model\demo\view;

class DemoIndex extends \model\api\view\ViewBase {
	
	private static $IMAGE_URL = 'session_image_url';

	public function __construct($log_pre_str='') {
		parent::__construct('demo',$log_pre_str);
		$_SESSION[SESSION_VISIT_COM_ID] = 1;
		$_SESSION[SESSION_VISIT_USER_ID] = 81873;
	}
	
	//主页面
	public function index(){
		parent::get_consume_url();
		if(isset($_SESSION[self::$IMAGE_URL]) && !empty($_SESSION[self::$IMAGE_URL])){
			$image_url = $_SESSION[self::$IMAGE_URL];
			$url = '';
			$_SESSION[self::$IMAGE_URL] = '';
		}else{
			$image_url = '';
			$url = $this -> get_sign_url();
		}
		parent::assign('SIGN_URL', $url['redirect']);
		parent::assign('IMAGE_URL', $image_url);
		g('pkg_smarty') -> assign('title', '流程专家');
		$this -> show($this -> view_dir . 'page/index.html');
	}



	/** 获取签名图片 */
	public function get_image(){
		$signatureId = g('pkg_val') -> get_get('signatureId');
		$image_url = g('api_sign') -> get_image($signatureId);

		$_SESSION[self::$IMAGE_URL] = SIGN_MEDIA_URL_HOST . $image_url;
		header("location:index.php?app=demo&a=index#tDetail");
	}

//-------------------------------------------内部实现---------------------------

	/** 获取领签URL */
	private function get_sign_url(){
		$redirect_url = MAIN_DYNAMIC_DOMAIN . 'index.php?app=demo&a=get_image';
		return g('api_sign') -> get_sign_url('领导', $redirect_url);
	}

}

//end